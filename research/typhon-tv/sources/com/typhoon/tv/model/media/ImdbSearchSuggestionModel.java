package com.typhoon.tv.model.media;

import java.util.List;

public class ImdbSearchSuggestionModel {
    private List<DBean> d;
    private String q;
    private int v;

    public static class DBean {
        private List<String> i;
        private String id;
        private String l;
        private String q;
        private String s;
        private int y;

        public List<String> getI() {
            return this.i;
        }

        public String getId() {
            return this.id;
        }

        public String getL() {
            return this.l;
        }

        public String getQ() {
            return this.q;
        }

        public String getS() {
            return this.s;
        }

        public int getY() {
            return this.y;
        }

        public void setI(List<String> i2) {
            this.i = i2;
        }

        public void setId(String id2) {
            this.id = id2;
        }

        public void setL(String l2) {
            this.l = l2;
        }

        public void setQ(String q2) {
            this.q = q2;
        }

        public void setS(String s2) {
            this.s = s2;
        }

        public void setY(int y2) {
            this.y = y2;
        }
    }

    public List<DBean> getD() {
        return this.d;
    }

    public String getQ() {
        return this.q;
    }

    public int getV() {
        return this.v;
    }

    public void setD(List<DBean> d2) {
        this.d = d2;
    }

    public void setQ(String q2) {
        this.q = q2;
    }

    public void setV(int v2) {
        this.v = v2;
    }
}
