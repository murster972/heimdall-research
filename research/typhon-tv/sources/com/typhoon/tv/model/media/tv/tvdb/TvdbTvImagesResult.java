package com.typhoon.tv.model.media.tv.tvdb;

import java.util.List;

public class TvdbTvImagesResult {
    private List<DataBean> data;

    public static class DataBean {
        private String fileName;
        private long id;
        private String keyType;
        private RatingsInfoBean ratingsInfo;
        private String resolution;
        private String subKey;
        private String thumbnail;

        public static class RatingsInfoBean {
            private double average;
            private int count;

            public double getAverage() {
                return this.average;
            }

            public int getCount() {
                return this.count;
            }

            public void setAverage(double d) {
                this.average = d;
            }

            public void setCount(int i) {
                this.count = i;
            }
        }

        public String getFileName() {
            return this.fileName;
        }

        public long getId() {
            return this.id;
        }

        public String getKeyType() {
            return this.keyType;
        }

        public RatingsInfoBean getRatingsInfo() {
            return this.ratingsInfo;
        }

        public String getResolution() {
            return this.resolution;
        }

        public String getSubKey() {
            return this.subKey;
        }

        public String getThumbnail() {
            return this.thumbnail;
        }

        public void setFileName(String str) {
            this.fileName = str;
        }

        public void setId(long j) {
            this.id = j;
        }

        public void setKeyType(String str) {
            this.keyType = str;
        }

        public void setRatingsInfo(RatingsInfoBean ratingsInfoBean) {
            this.ratingsInfo = ratingsInfoBean;
        }

        public void setResolution(String str) {
            this.resolution = str;
        }

        public void setSubKey(String str) {
            this.subKey = str;
        }

        public void setThumbnail(String str) {
            this.thumbnail = str;
        }
    }

    public List<DataBean> getData() {
        return this.data;
    }

    public void setData(List<DataBean> list) {
        this.data = list;
    }
}
