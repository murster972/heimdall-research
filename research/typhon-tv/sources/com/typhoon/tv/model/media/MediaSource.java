package com.typhoon.tv.model.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.Formatter;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.helper.GoogleVideoHelper;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;

public class MediaSource implements Parcelable {
    public static final Parcelable.Creator<MediaSource> CREATOR = new Parcelable.Creator<MediaSource>() {
        public MediaSource createFromParcel(Parcel parcel) {
            return new MediaSource(parcel);
        }

        public MediaSource[] newArray(int i) {
            return new MediaSource[i];
        }
    };
    private boolean debrid;
    private long fileSize;
    private Boolean hls;
    private String hostName;
    private boolean neededToResolve;
    private MediaSource originalMediaSource;
    private HashMap<String, String> playHeader;
    private String providerName;
    private String quality;
    private boolean resolved;
    private String streamLink;

    public MediaSource(MediaSource in) {
        this.providerName = in.getProviderName();
        this.hostName = in.getHostName();
        this.quality = in.getQuality();
        this.streamLink = in.getStreamLink();
        this.neededToResolve = in.isNeededToResolve();
        this.resolved = in.isResolved();
        this.playHeader = in.getPlayHeader();
        this.fileSize = in.getFileSize();
        this.hls = in.getHLSBase();
        this.debrid = in.getDebridBase();
        this.originalMediaSource = in.getOriginalMediaSource();
    }

    public MediaSource(String providerName2, String hostName2, boolean neededToResolve2) {
        this.providerName = providerName2;
        this.hostName = hostName2;
        this.neededToResolve = neededToResolve2;
        this.resolved = !this.neededToResolve;
        this.fileSize = -1;
        this.hls = null;
        this.debrid = false;
        if (this.providerName.isEmpty() || this.hostName.isEmpty()) {
            this.quality = "HQ";
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.typhoon.tv.model.media.MediaSource} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.typhoon.tv.model.media.MediaSource cloneDeeply() {
        /*
            r5 = this;
            r2 = 0
            r3 = 0
            android.os.Parcel r3 = android.os.Parcel.obtain()     // Catch:{ Exception -> 0x0028 }
            r3.writeValue(r5)     // Catch:{ Exception -> 0x0028 }
            r4 = 0
            r3.setDataPosition(r4)     // Catch:{ Exception -> 0x0028 }
            java.lang.Class<com.typhoon.tv.model.media.MediaSource> r4 = com.typhoon.tv.model.media.MediaSource.class
            java.lang.ClassLoader r4 = r4.getClassLoader()     // Catch:{ Exception -> 0x0028 }
            java.lang.Object r4 = r3.readValue(r4)     // Catch:{ Exception -> 0x0028 }
            r0 = r4
            com.typhoon.tv.model.media.MediaSource r0 = (com.typhoon.tv.model.media.MediaSource) r0     // Catch:{ Exception -> 0x0028 }
            r2 = r0
            if (r3 == 0) goto L_0x0020
            r3.recycle()
        L_0x0020:
            if (r2 != 0) goto L_0x0027
            com.typhoon.tv.model.media.MediaSource r2 = new com.typhoon.tv.model.media.MediaSource
            r2.<init>((com.typhoon.tv.model.media.MediaSource) r5)
        L_0x0027:
            return r2
        L_0x0028:
            r1 = move-exception
            r4 = 0
            boolean[] r4 = new boolean[r4]     // Catch:{ all -> 0x0035 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r4)     // Catch:{ all -> 0x0035 }
            if (r3 == 0) goto L_0x0020
            r3.recycle()
            goto L_0x0020
        L_0x0035:
            r4 = move-exception
            if (r3 == 0) goto L_0x003b
            r3.recycle()
        L_0x003b:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.model.media.MediaSource.cloneDeeply():com.typhoon.tv.model.media.MediaSource");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MediaSource that = (MediaSource) o;
        if (this.streamLink == null ? that.streamLink != null : !this.streamLink.equals(that.streamLink)) {
            return false;
        }
        return this.playHeader != null ? this.playHeader.equals(that.playHeader) : that.playHeader == null;
    }

    public boolean getDebridBase() {
        return this.debrid;
    }

    public long getFileSize() {
        return this.fileSize;
    }

    public String getFileSizeString() {
        return this.fileSize < 0 ? "" : "[" + Formatter.formatFileSize(TVApplication.m6288(), this.fileSize) + "]";
    }

    public Boolean getHLSBase() {
        return this.hls;
    }

    public String getHostName() {
        return this.hostName;
    }

    public MediaSource getOriginalMediaSource() {
        return this.originalMediaSource;
    }

    public HashMap<String, String> getPlayHeader() {
        return this.playHeader;
    }

    public String getProviderName() {
        return this.providerName;
    }

    public String getQuality() {
        return this.quality;
    }

    public String getStreamLink() {
        return this.streamLink;
    }

    public String getStringToBeCompared() {
        String str = getQuality().trim().toLowerCase().replace("4k", "2160p").replace("2k", "1440p").replace("quadhd", "1440p").replace("3d", "1080p").replace("hd", "720p").replace("sd", "480p").replace("hq", "360p") + " [" + this.fileSize + "]";
        String str2 = (GoogleVideoHelper.m15955(getStreamLink()) ? str + " [AAA]" : isDebrid() ? str + " [BBB]" : str + " [XXX]") + " - " + this.providerName + " [" + this.hostName + "]";
        return isHLS() ? str2 + " [HLS]" : str2;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.streamLink != null ? this.streamLink.hashCode() : 0) * 31;
        if (this.playHeader != null) {
            i = this.playHeader.hashCode();
        }
        return hashCode + i;
    }

    public boolean isDebrid() {
        return this.debrid || (this.hostName != null && (this.hostName.contains("[DEB]") || this.hostName.contains("-DEB")));
    }

    public boolean isHLS() {
        return (this.hls != null && this.hls.booleanValue()) || (this.hls == null && this.streamLink.trim().toLowerCase().contains(".m3u8"));
    }

    public boolean isNeededToResolve() {
        return this.neededToResolve;
    }

    public boolean isResolved() {
        return this.resolved;
    }

    public void setDebrid(boolean z) {
        this.debrid = z;
    }

    public void setFileSize(long fileSize2) {
        this.fileSize = fileSize2;
    }

    public void setHLS(boolean z) {
        this.hls = Boolean.valueOf(z);
    }

    public void setHostName(String hostName2) {
        this.hostName = hostName2;
    }

    public void setNeededToResolve(boolean z) {
        this.neededToResolve = z;
    }

    public void setOriginalMediaSource(MediaSource originalMediaSource2) {
        this.originalMediaSource = originalMediaSource2;
    }

    public void setPlayHeader(HashMap<String, String> hashMap) {
        this.playHeader = hashMap;
    }

    public void setProviderName(String str) {
        this.providerName = str;
    }

    public void setQuality(int i) {
        setQuality(String.valueOf(i));
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0097, code lost:
        if (r0.equals("1440p") != false) goto L_0x008a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setQuality(java.lang.String r8) {
        /*
            r7 = this;
            r4 = 2
            r3 = 1
            r1 = 0
            r2 = -1
            java.lang.String r5 = r8.trim()
            java.lang.String r0 = r5.toLowerCase()
            java.lang.String r5 = "p"
            boolean r5 = r0.endsWith(r5)
            if (r5 != 0) goto L_0x0069
            boolean r5 = com.typhoon.tv.utils.Utils.m6426((java.lang.String) r0)
            if (r5 == 0) goto L_0x0069
            int r5 = r0.hashCode()
            switch(r5) {
                case 1511391: goto L_0x003c;
                case 1513189: goto L_0x0047;
                case 1538361: goto L_0x0052;
                default: goto L_0x0022;
            }
        L_0x0022:
            switch(r2) {
                case 0: goto L_0x005d;
                case 1: goto L_0x005d;
                case 2: goto L_0x0063;
                default: goto L_0x0025;
            }
        L_0x0025:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r2 = "p"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r7.quality = r1
        L_0x003b:
            return
        L_0x003c:
            java.lang.String r3 = "1440"
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x0022
            r2 = r1
            goto L_0x0022
        L_0x0047:
            java.lang.String r1 = "1600"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0022
            r2 = r3
            goto L_0x0022
        L_0x0052:
            java.lang.String r1 = "2160"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0022
            r2 = r4
            goto L_0x0022
        L_0x005d:
            java.lang.String r1 = "2K"
            r7.quality = r1
            goto L_0x003b
        L_0x0063:
            java.lang.String r1 = "4K"
            r7.quality = r1
            goto L_0x003b
        L_0x0069:
            java.lang.String r5 = "p"
            boolean r5 = r0.endsWith(r5)
            if (r5 == 0) goto L_0x00bc
            java.lang.String r5 = "p"
            java.lang.String r6 = ""
            java.lang.String r5 = r0.replace(r5, r6)
            boolean r5 = com.typhoon.tv.utils.Utils.m6426((java.lang.String) r5)
            if (r5 == 0) goto L_0x00bc
            int r5 = r0.hashCode()
            switch(r5) {
                case 46853233: goto L_0x0090;
                case 46908971: goto L_0x009a;
                case 47689303: goto L_0x00a5;
                default: goto L_0x0089;
            }
        L_0x0089:
            r1 = r2
        L_0x008a:
            switch(r1) {
                case 0: goto L_0x00b0;
                case 1: goto L_0x00b0;
                case 2: goto L_0x00b6;
                default: goto L_0x008d;
            }
        L_0x008d:
            r7.quality = r0
            goto L_0x003b
        L_0x0090:
            java.lang.String r3 = "1440p"
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x0089
            goto L_0x008a
        L_0x009a:
            java.lang.String r1 = "1600p"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0089
            r1 = r3
            goto L_0x008a
        L_0x00a5:
            java.lang.String r1 = "2160p"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0089
            r1 = r4
            goto L_0x008a
        L_0x00b0:
            java.lang.String r1 = "2K"
            r7.quality = r1
            goto L_0x003b
        L_0x00b6:
            java.lang.String r1 = "4K"
            r7.quality = r1
            goto L_0x003b
        L_0x00bc:
            java.lang.String r1 = "quadhd"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00cc
            java.lang.String r1 = "2K"
            r7.quality = r1
            goto L_0x003b
        L_0x00cc:
            java.lang.String r1 = "4k"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0102
            java.lang.String r1 = "2k"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0102
            java.lang.String r1 = "3d"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0102
            java.lang.String r1 = "hd"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0102
            java.lang.String r1 = "hq"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0102
            java.lang.String r1 = "sd"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x010a
        L_0x0102:
            java.lang.String r1 = r0.toUpperCase()
            r7.quality = r1
            goto L_0x003b
        L_0x010a:
            java.lang.String r1 = "HQ"
            r7.quality = r1
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.model.media.MediaSource.setQuality(java.lang.String):void");
    }

    public void setResolved(boolean z) {
        this.resolved = z;
    }

    public void setStreamLink(String str) {
        this.streamLink = str;
    }

    public String toString() {
        String str = this.quality + " - " + this.providerName + " [" + this.hostName + "] ";
        if (isHLS()) {
            str = str + "[HLS] ";
        }
        if (this.debrid) {
            str = str + "[DEB] ";
        }
        String str2 = str + getFileSizeString();
        return (str2.length() <= 0 || !str2.endsWith(StringUtils.SPACE)) ? str2 : str2.substring(0, str2.length() - 1);
    }

    public String toStringAllObjs() {
        return "MediaSource{providerName='" + this.providerName + '\'' + ", hostName='" + this.hostName + '\'' + ", quality='" + this.quality + '\'' + ", streamLink='" + this.streamLink + '\'' + ", neededToResolve=" + this.neededToResolve + ", resolved=" + this.resolved + ", playHeader=" + this.playHeader + ", fileSize=" + this.fileSize + ", debrid=" + this.debrid + ", originalMediaSource=" + this.originalMediaSource + '}';
    }

    public void writeToParcel(Parcel dest, int flags) {
        byte b = 1;
        dest.writeString(this.providerName);
        dest.writeString(this.hostName);
        dest.writeString(this.quality);
        dest.writeString(this.streamLink);
        dest.writeByte(this.neededToResolve ? (byte) 1 : 0);
        dest.writeByte(this.resolved ? (byte) 1 : 0);
        dest.writeSerializable(this.playHeader);
        dest.writeLong(this.fileSize);
        dest.writeInt(this.hls == null ? -1 : this.hls.booleanValue() ? 1 : 0);
        if (!this.debrid) {
            b = 0;
        }
        dest.writeByte(b);
        dest.writeParcelable(this.originalMediaSource, flags);
    }

    public MediaSource(Parcel in) {
        boolean z = true;
        this.providerName = in.readString();
        this.hostName = in.readString();
        this.quality = in.readString();
        this.streamLink = in.readString();
        this.neededToResolve = in.readByte() != 0;
        this.resolved = in.readByte() != 0;
        this.playHeader = (HashMap) in.readSerializable();
        this.fileSize = in.readLong();
        switch (in.readInt()) {
            case 0:
                this.hls = false;
                break;
            case 1:
                this.hls = true;
                break;
            default:
                this.hls = null;
                break;
        }
        this.debrid = in.readByte() == 0 ? false : z;
        this.originalMediaSource = (MediaSource) in.readParcelable(MediaSource.class.getClassLoader());
    }
}
