package com.typhoon.tv.model.media.tv.tmdb;

import java.util.List;

public class TmdbSeasonInfoResult {
    private String _id;
    private String air_date;
    private List<EpisodesBean> episodes;
    private long id;
    private String name;
    private String overview;
    private String poster_path;
    private int season_number;

    public static class EpisodesBean {
        private String air_date;
        private int episode_number;
        private long id;
        private String name;
        private String overview;
        private Object production_code;
        private int season_number;
        private String still_path;
        private double vote_average;
        private long vote_count;

        public String getAir_date() {
            return this.air_date;
        }

        public int getEpisode_number() {
            return this.episode_number;
        }

        public long getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public String getOverview() {
            return this.overview;
        }

        public Object getProduction_code() {
            return this.production_code;
        }

        public int getSeason_number() {
            return this.season_number;
        }

        public String getStill_path() {
            return this.still_path;
        }

        public double getVote_average() {
            return this.vote_average;
        }

        public long getVote_count() {
            return this.vote_count;
        }

        public void setAir_date(String str) {
            this.air_date = str;
        }

        public void setEpisode_number(int i) {
            this.episode_number = i;
        }

        public void setId(long j) {
            this.id = j;
        }

        public void setName(String str) {
            this.name = str;
        }

        public void setOverview(String str) {
            this.overview = str;
        }

        public void setProduction_code(Object obj) {
            this.production_code = obj;
        }

        public void setSeason_number(int i) {
            this.season_number = i;
        }

        public void setStill_path(String str) {
            this.still_path = str;
        }

        public void setVote_average(double d) {
            this.vote_average = d;
        }

        public void setVote_count(long j) {
            this.vote_count = j;
        }
    }

    public String getAir_date() {
        return this.air_date;
    }

    public List<EpisodesBean> getEpisodes() {
        return this.episodes;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getOverview() {
        return this.overview;
    }

    public String getPoster_path() {
        return this.poster_path;
    }

    public int getSeason_number() {
        return this.season_number;
    }

    public String get_id() {
        return this._id;
    }

    public void setAir_date(String str) {
        this.air_date = str;
    }

    public void setEpisodes(List<EpisodesBean> list) {
        this.episodes = list;
    }

    public void setId(long j) {
        this.id = j;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setOverview(String str) {
        this.overview = str;
    }

    public void setPoster_path(String str) {
        this.poster_path = str;
    }

    public void setSeason_number(int i) {
        this.season_number = i;
    }

    public void set_id(String str) {
        this._id = str;
    }
}
