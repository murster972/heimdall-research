package com.typhoon.tv.model.media.tv;

import android.os.Parcel;
import android.os.Parcelable;
import com.typhoon.tv.I18N;
import com.typhoon.tv.R;
import com.typhoon.tv.utils.Utils;

public class TvSeasonInfo implements Parcelable {
    public static final Parcelable.Creator<TvSeasonInfo> CREATOR = new Parcelable.Creator<TvSeasonInfo>() {
        public TvSeasonInfo createFromParcel(Parcel parcel) {
            return new TvSeasonInfo(parcel);
        }

        public TvSeasonInfo[] newArray(int i) {
            return new TvSeasonInfo[i];
        }
    };
    private String airDate;
    private String bannerUrl;
    private Integer seasonNum;
    private Integer seasonYear;

    private TvSeasonInfo(Parcel parcel) {
        this.seasonNum = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.seasonYear = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.airDate = parcel.readString();
        this.bannerUrl = parcel.readString();
    }

    public TvSeasonInfo(Integer num) {
        this.seasonNum = num;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TvSeasonInfo tvSeasonInfo = (TvSeasonInfo) obj;
        return Utils.m6425((Object) Integer.valueOf(getSeasonNum()), (Object) Integer.valueOf(tvSeasonInfo.getSeasonNum())) && Utils.m6425((Object) getAirDate(), (Object) tvSeasonInfo.getAirDate()) && Utils.m6425((Object) getSeasonYear(), (Object) tvSeasonInfo.getSeasonYear());
    }

    public String getAirDate() {
        return this.airDate;
    }

    public String getBannerUrl() {
        return this.bannerUrl;
    }

    public String getSeasonName() {
        if (this.seasonNum.intValue() == 0) {
            return I18N.m15706(R.string.season_special);
        }
        return I18N.m15707(R.string.season, this.seasonNum);
    }

    public int getSeasonNum() {
        return this.seasonNum.intValue();
    }

    public Integer getSeasonYear() {
        return this.seasonYear;
    }

    public void setAirDate(String str) {
        this.airDate = str;
    }

    public void setBannerUrl(String str) {
        this.bannerUrl = str;
    }

    public void setSeasonNum(Integer num) {
        this.seasonNum = num;
    }

    public void setSeasonYear(Integer num) {
        this.seasonYear = num;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.seasonNum);
        parcel.writeValue(this.seasonYear);
        parcel.writeString(this.airDate);
        parcel.writeString(this.bannerUrl);
    }
}
