package com.typhoon.tv.model.media.tv.tvdb;

import java.util.List;

public class TvdbEpisodeInfoResult {
    private List<DataBean> data;
    private LinksBean links;

    public static class DataBean {
        private int absoluteNumber;
        private int airedEpisodeNumber;
        private int airedSeason;
        private int dvdEpisodeNumber;
        private int dvdSeason;
        private String episodeName;
        private String firstAired;
        private String overview;

        public static class LanguageBean {
            private String episodeName;
            private String overview;

            public String getEpisodeName() {
                return this.episodeName;
            }

            public String getOverview() {
                return this.overview;
            }

            public void setEpisodeName(String str) {
                this.episodeName = str;
            }

            public void setOverview(String str) {
                this.overview = str;
            }
        }

        public int getAbsoluteNumber() {
            return this.absoluteNumber;
        }

        public int getAiredEpisodeNumber() {
            return this.airedEpisodeNumber;
        }

        public int getAiredSeason() {
            return this.airedSeason;
        }

        public int getDvdEpisodeNumber() {
            return this.dvdEpisodeNumber;
        }

        public int getDvdSeason() {
            return this.dvdSeason;
        }

        public String getEpisodeName() {
            return this.episodeName;
        }

        public String getFirstAired() {
            return this.firstAired;
        }

        public String getOverview() {
            return this.overview;
        }

        public void setAbsoluteNumber(int i) {
            this.absoluteNumber = i;
        }

        public void setAiredEpisodeNumber(int i) {
            this.airedEpisodeNumber = i;
        }

        public void setAiredSeason(int i) {
            this.airedSeason = i;
        }

        public void setDvdEpisodeNumber(int i) {
            this.dvdEpisodeNumber = i;
        }

        public void setDvdSeason(int i) {
            this.dvdSeason = i;
        }

        public void setEpisodeName(String str) {
            this.episodeName = str;
        }

        public void setFirstAired(String str) {
            this.firstAired = str;
        }

        public void setOverview(String str) {
            this.overview = str;
        }
    }

    public static class LinksBean {
        private int first;
        private int last;
        private Object next;
        private Object prev;

        public int getFirst() {
            return this.first;
        }

        public int getLast() {
            return this.last;
        }

        public Object getNext() {
            return this.next;
        }

        public Object getPrev() {
            return this.prev;
        }

        public void setFirst(int i) {
            this.first = i;
        }

        public void setLast(int i) {
            this.last = i;
        }

        public void setNext(Object obj) {
            this.next = obj;
        }

        public void setPrev(Object obj) {
            this.prev = obj;
        }
    }

    public List<DataBean> getData() {
        return this.data;
    }

    public LinksBean getLinks() {
        return this.links;
    }

    public void setData(List<DataBean> list) {
        this.data = list;
    }

    public void setLinks(LinksBean linksBean) {
        this.links = linksBean;
    }
}
