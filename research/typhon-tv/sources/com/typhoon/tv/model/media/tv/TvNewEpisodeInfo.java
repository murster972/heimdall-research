package com.typhoon.tv.model.media.tv;

import android.os.Parcel;
import android.os.Parcelable;
import com.typhoon.tv.model.media.MediaInfo;

public class TvNewEpisodeInfo implements Parcelable {
    public static final Parcelable.Creator<TvNewEpisodeInfo> CREATOR = new Parcelable.Creator<TvNewEpisodeInfo>() {
        public TvNewEpisodeInfo createFromParcel(Parcel parcel) {
            return new TvNewEpisodeInfo(parcel);
        }

        public TvNewEpisodeInfo[] newArray(int i) {
            return new TvNewEpisodeInfo[i];
        }
    };
    private int episode;
    private int episodeAbs;
    private MediaInfo mediaInfo;
    private String overview;
    private int runtime;
    private int season;
    private String title;

    protected TvNewEpisodeInfo(Parcel parcel) {
        this.mediaInfo = (MediaInfo) parcel.readParcelable(MediaInfo.class.getClassLoader());
        this.season = parcel.readInt();
        this.episode = parcel.readInt();
        this.episodeAbs = parcel.readInt();
        this.runtime = parcel.readInt();
        this.title = parcel.readString();
        this.overview = parcel.readString();
    }

    public TvNewEpisodeInfo(MediaInfo mediaInfo2, int i, int i2) {
        this.mediaInfo = mediaInfo2;
        this.season = i;
        this.episode = i2;
    }

    public int describeContents() {
        return 0;
    }

    public int getEpisode() {
        return this.episode;
    }

    public int getEpisodeAbs() {
        return this.episodeAbs;
    }

    public MediaInfo getMediaInfo() {
        return this.mediaInfo;
    }

    public String getOverview() {
        return this.overview;
    }

    public int getRuntime() {
        return this.runtime;
    }

    public int getSeason() {
        return this.season;
    }

    public String getTitle() {
        return this.title;
    }

    public void setEpisode(int i) {
        this.episode = i;
    }

    public void setEpisodeAbs(int i) {
        this.episodeAbs = i;
    }

    public void setMediaInfo(MediaInfo mediaInfo2) {
        this.mediaInfo = mediaInfo2;
    }

    public void setOverview(String str) {
        this.overview = str;
    }

    public void setRuntime(int i) {
        this.runtime = i;
    }

    public void setSeason(int i) {
        this.season = i;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public String toString() {
        return "TvNewEpisodeInfo{mediaInfo=" + this.mediaInfo + ", season=" + this.season + ", episode=" + this.episode + ", episodeAbs=" + this.episodeAbs + ", runtime=" + this.runtime + ", title='" + this.title + '\'' + ", overview='" + this.overview + '\'' + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.mediaInfo, i);
        parcel.writeInt(this.season);
        parcel.writeInt(this.episode);
        parcel.writeInt(this.episodeAbs);
        parcel.writeInt(this.runtime);
        parcel.writeString(this.title);
        parcel.writeString(this.overview);
    }
}
