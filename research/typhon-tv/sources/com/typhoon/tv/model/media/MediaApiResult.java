package com.typhoon.tv.model.media;

import java.util.ArrayList;

public class MediaApiResult {
    private ArrayList<MediaInfo> mediaInfoList;
    private int totalPage;

    public MediaApiResult(ArrayList<MediaInfo> mediaInfoList2, int i) {
        this.mediaInfoList = mediaInfoList2;
        this.totalPage = i;
    }

    public ArrayList<MediaInfo> getMediaInfoList() {
        return this.mediaInfoList;
    }

    public int getTotalPage() {
        return this.totalPage;
    }

    public void setMediaInfoList(ArrayList<MediaInfo> mediaInfoList2) {
        this.mediaInfoList = mediaInfoList2;
    }

    public void setTotalPage(int totalPage2) {
        this.totalPage = totalPage2;
    }
}
