package com.typhoon.tv.model.media.tv;

import android.os.Parcel;
import android.os.Parcelable;

public class TvEpisodeInfo implements Parcelable {
    public static final Parcelable.Creator<TvEpisodeInfo> CREATOR = new Parcelable.Creator<TvEpisodeInfo>() {
        public TvEpisodeInfo createFromParcel(Parcel parcel) {
            return new TvEpisodeInfo(parcel);
        }

        public TvEpisodeInfo[] newArray(int i) {
            return new TvEpisodeInfo[i];
        }
    };
    private String airDate;
    private String bannerUrl;
    private Integer episode;
    private String name;
    private String overview;
    private Integer season;

    private TvEpisodeInfo(Parcel parcel) {
        this.season = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.episode = (Integer) parcel.readValue(Integer.class.getClassLoader());
        this.name = parcel.readString();
        this.airDate = parcel.readString();
        this.bannerUrl = parcel.readString();
        this.overview = parcel.readString();
    }

    public TvEpisodeInfo(Integer num, Integer num2, String str, String str2) {
        this.season = num;
        this.episode = num2;
        this.name = str;
        this.bannerUrl = str2;
    }

    public int describeContents() {
        return 0;
    }

    public String getAirDate() {
        return this.airDate;
    }

    public String getBannerUrl() {
        return this.bannerUrl;
    }

    public int getEpisode() {
        return this.episode.intValue();
    }

    public String getName() {
        return this.name;
    }

    public String getOverview() {
        return this.overview;
    }

    public int getSeason() {
        return this.season.intValue();
    }

    public void setAirDate(String str) {
        this.airDate = str;
    }

    public void setBannerUrl(String str) {
        this.bannerUrl = str;
    }

    public void setEpisode(Integer num) {
        this.episode = num;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setOverview(String str) {
        this.overview = str;
    }

    public void setSeason(Integer num) {
        this.season = num;
    }

    public String toString() {
        return "TvEpisodeInfo{season=" + this.season + ", episode=" + this.episode + ", name='" + this.name + '\'' + ", airDate='" + this.airDate + '\'' + ", bannerUrl='" + this.bannerUrl + '\'' + ", overview='" + this.overview + '\'' + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.season);
        parcel.writeValue(this.episode);
        parcel.writeString(this.name);
        parcel.writeString(this.airDate);
        parcel.writeString(this.bannerUrl);
        parcel.writeString(this.overview);
    }
}
