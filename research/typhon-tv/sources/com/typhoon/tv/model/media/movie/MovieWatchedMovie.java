package com.typhoon.tv.model.media.movie;

import com.orm.SugarRecord;

public class MovieWatchedMovie extends SugarRecord {
    private String imdbId;
    private int tmdbId;

    public MovieWatchedMovie() {
    }

    public MovieWatchedMovie(int i, String str) {
        this.tmdbId = i;
        this.imdbId = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MovieWatchedMovie movieWatchedMovie = (MovieWatchedMovie) obj;
        if (this.tmdbId != movieWatchedMovie.tmdbId) {
            return false;
        }
        return this.imdbId != null ? this.imdbId.equals(movieWatchedMovie.imdbId) : movieWatchedMovie.imdbId == null;
    }

    public String getImdbId() {
        return this.imdbId;
    }

    public int getTmdbId() {
        return this.tmdbId;
    }

    public int hashCode() {
        return (this.tmdbId * 31) + (this.imdbId != null ? this.imdbId.hashCode() : 0);
    }

    public void setImdbId(String str) {
        this.imdbId = str;
    }

    public void setTmdbId(int i) {
        this.tmdbId = i;
    }
}
