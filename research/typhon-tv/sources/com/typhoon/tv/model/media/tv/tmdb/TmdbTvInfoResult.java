package com.typhoon.tv.model.media.tv.tmdb;

import com.typhoon.tv.model.media.TmdbVideosBean;
import java.util.List;

public class TmdbTvInfoResult {
    private String backdrop_path;
    private ContentRatingsBean content_ratings;
    private List<Integer> episode_run_time;
    private ExternalIdsBean external_ids;
    private String first_air_date;
    private List<GenresBean> genres;
    private int id;
    private boolean in_production;
    private List<String> languages;
    private String last_air_date;
    private String name;
    private List<NetworksBean> networks;
    private int number_of_episodes;
    private int number_of_seasons;
    private List<String> origin_country;
    private String original_language;
    private String original_name;
    private String overview;
    private double popularity;
    private String poster_path;
    private List<SeasonsBean> seasons;
    private String status;
    private String type;
    private TmdbVideosBean videos;

    public static class ContentRatingsBean {
        private List<ResultsBean> results;

        public static class ResultsBean {
            private String iso_3166_1;
            private String rating;

            public String getIso_3166_1() {
                return this.iso_3166_1;
            }

            public String getRating() {
                return this.rating;
            }

            public void setIso_3166_1(String str) {
                this.iso_3166_1 = str;
            }

            public void setRating(String str) {
                this.rating = str;
            }
        }

        public List<ResultsBean> getResults() {
            return this.results;
        }

        public void setResults(List<ResultsBean> list) {
            this.results = list;
        }
    }

    public static class ExternalIdsBean {
        private String imdb_id;
        private int tvdb_id;

        public String getImdb_id() {
            return this.imdb_id;
        }

        public int getTvdb_id() {
            return this.tvdb_id;
        }

        public void setImdb_id(String str) {
            this.imdb_id = str;
        }

        public void setTvdb_id(int i) {
            this.tvdb_id = i;
        }
    }

    public static class GenresBean {
        private int id;
        private String name;

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public void setId(int i) {
            this.id = i;
        }

        public void setName(String str) {
            this.name = str;
        }
    }

    public static class NetworksBean {
        private int id;
        private String name;

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public void setId(int i) {
            this.id = i;
        }

        public void setName(String str) {
            this.name = str;
        }
    }

    public static class SeasonsBean {
        private String air_date;
        private int episode_count;
        private long id;
        private String poster_path;
        private int season_number;

        public String getAir_date() {
            return this.air_date;
        }

        public int getEpisode_count() {
            return this.episode_count;
        }

        public long getId() {
            return this.id;
        }

        public String getPoster_path() {
            return this.poster_path;
        }

        public int getSeason_number() {
            return this.season_number;
        }

        public void setAir_date(String str) {
            this.air_date = str;
        }

        public void setEpisode_count(int i) {
            this.episode_count = i;
        }

        public void setId(long j) {
            this.id = j;
        }

        public void setPoster_path(String str) {
            this.poster_path = str;
        }

        public void setSeason_number(int i) {
            this.season_number = i;
        }
    }

    public String getBackdrop_path() {
        return this.backdrop_path;
    }

    public ContentRatingsBean getContent_ratings() {
        return this.content_ratings;
    }

    public List<Integer> getEpisode_run_time() {
        return this.episode_run_time;
    }

    public ExternalIdsBean getExternal_ids() {
        return this.external_ids;
    }

    public String getFirst_air_date() {
        return this.first_air_date;
    }

    public List<GenresBean> getGenres() {
        return this.genres;
    }

    public int getId() {
        return this.id;
    }

    public List<String> getLanguages() {
        return this.languages;
    }

    public String getLast_air_date() {
        return this.last_air_date;
    }

    public String getName() {
        return this.name;
    }

    public List<NetworksBean> getNetworks() {
        return this.networks;
    }

    public int getNumber_of_episodes() {
        return this.number_of_episodes;
    }

    public int getNumber_of_seasons() {
        return this.number_of_seasons;
    }

    public List<String> getOrigin_country() {
        return this.origin_country;
    }

    public String getOriginal_language() {
        return this.original_language;
    }

    public String getOriginal_name() {
        return this.original_name;
    }

    public String getOverview() {
        return this.overview;
    }

    public double getPopularity() {
        return this.popularity;
    }

    public String getPoster_path() {
        return this.poster_path;
    }

    public List<SeasonsBean> getSeasons() {
        return this.seasons;
    }

    public String getStatus() {
        return this.status;
    }

    public String getType() {
        return this.type;
    }

    public TmdbVideosBean getVideos() {
        return this.videos;
    }

    public boolean isIn_production() {
        return this.in_production;
    }

    public void setBackdrop_path(String str) {
        this.backdrop_path = str;
    }

    public void setContent_ratings(ContentRatingsBean contentRatingsBean) {
        this.content_ratings = contentRatingsBean;
    }

    public void setEpisode_run_time(List<Integer> list) {
        this.episode_run_time = list;
    }

    public void setExternal_ids(ExternalIdsBean externalIdsBean) {
        this.external_ids = externalIdsBean;
    }

    public void setFirst_air_date(String str) {
        this.first_air_date = str;
    }

    public void setGenres(List<GenresBean> list) {
        this.genres = list;
    }

    public void setId(int i) {
        this.id = i;
    }

    public void setIn_production(boolean z) {
        this.in_production = z;
    }

    public void setLanguages(List<String> list) {
        this.languages = list;
    }

    public void setLast_air_date(String str) {
        this.last_air_date = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setNetworks(List<NetworksBean> list) {
        this.networks = list;
    }

    public void setNumber_of_episodes(int i) {
        this.number_of_episodes = i;
    }

    public void setNumber_of_seasons(int i) {
        this.number_of_seasons = i;
    }

    public void setOrigin_country(List<String> list) {
        this.origin_country = list;
    }

    public void setOriginal_language(String str) {
        this.original_language = str;
    }

    public void setOriginal_name(String str) {
        this.original_name = str;
    }

    public void setOverview(String str) {
        this.overview = str;
    }

    public void setPopularity(double d) {
        this.popularity = d;
    }

    public void setPoster_path(String str) {
        this.poster_path = str;
    }

    public void setSeasons(List<SeasonsBean> list) {
        this.seasons = list;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public void setType(String str) {
        this.type = str;
    }

    public void setVideos(TmdbVideosBean tmdbVideosBean) {
        this.videos = tmdbVideosBean;
    }
}
