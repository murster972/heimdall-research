package com.typhoon.tv.model.media;

public class MediaRatingsModel {
    private String imdbRating;
    private String rottenTomatoesRating;

    public String getImdbRating() {
        return this.imdbRating;
    }

    public String getRottenTomatoesRating() {
        return this.rottenTomatoesRating;
    }

    public void setImdbRating(String imdbRating2) {
        this.imdbRating = imdbRating2;
    }

    public void setRottenTomatoesRating(String rottenTomatoesRating2) {
        this.rottenTomatoesRating = rottenTomatoesRating2;
    }
}
