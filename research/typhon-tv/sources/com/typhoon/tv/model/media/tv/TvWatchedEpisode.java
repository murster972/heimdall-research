package com.typhoon.tv.model.media.tv;

import com.orm.SugarRecord;

public class TvWatchedEpisode extends SugarRecord {
    private int episode;
    private int season;
    private int tmdbId;

    public TvWatchedEpisode() {
    }

    public TvWatchedEpisode(int i, int i2, int i3) {
        this.tmdbId = i;
        this.season = i2;
        this.episode = i3;
    }

    public int getEpisode() {
        return this.episode;
    }

    public int getSeason() {
        return this.season;
    }

    public int getTmdbId() {
        return this.tmdbId;
    }

    public void setEpisode(int i) {
        this.episode = i;
    }

    public void setSeason(int i) {
        this.season = i;
    }

    public void setTmdbId(int i) {
        this.tmdbId = i;
    }
}
