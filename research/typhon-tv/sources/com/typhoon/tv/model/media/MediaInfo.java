package com.typhoon.tv.model.media;

import android.os.Parcel;
import android.os.Parcelable;
import com.mopub.common.TyphoonApp;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.typhoon.tv.helper.TitleHelper;

public class MediaInfo extends SugarRecord implements Parcelable {
    @Ignore
    public static final Parcelable.Creator<MediaInfo> CREATOR = new Parcelable.Creator<MediaInfo>() {
        public MediaInfo createFromParcel(Parcel source) {
            return new MediaInfo(source);
        }

        public MediaInfo[] newArray(int size) {
            return new MediaInfo[size];
        }
    };
    @Ignore
    public static final int SERVICE_TMDB = 1;
    @Ignore
    public static final int SERVICE_TRAKT = 2;
    @Ignore
    public static final int TYPE_MOVIE = 1;
    @Ignore
    public static final int TYPE_TV_SHOW = 0;
    private String bannerUrl;
    private String imdbId;
    private String name;
    @Ignore
    private String originalName;
    private String posterUrl;
    private int service;
    private int tmdbId;
    private int tvdbId;
    private int type;
    private int year;

    public MediaInfo() {
    }

    public MediaInfo(int type2, int service2, int tmdbId2, String name2, int year2) {
        this.type = type2;
        this.service = service2;
        this.tmdbId = tmdbId2;
        this.name = TitleHelper.m15967(replaceSpecialName(name2));
        this.year = year2;
    }

    public MediaInfo(Parcel in) {
        this.type = in.readInt();
        this.service = in.readInt();
        this.tmdbId = in.readInt();
        this.tvdbId = in.readInt();
        this.imdbId = in.readString();
        this.name = TitleHelper.m15967(replaceSpecialName(in.readString()));
        this.originalName = in.readString();
        this.year = in.readInt();
        this.bannerUrl = in.readString();
        this.posterUrl = in.readString();
    }

    public MediaInfo(MediaInfo in) {
        this.type = in.getType();
        this.service = in.getService();
        this.tmdbId = in.getTmdbId();
        this.tvdbId = in.getTvdbId();
        this.imdbId = in.getImdbId();
        this.name = TitleHelper.m15967(replaceSpecialName(in.getName()));
        this.originalName = in.getOriginalName();
        this.year = in.getYear();
        this.bannerUrl = in.getBannerUrlBase();
        this.posterUrl = in.getPosterUrlBase();
    }

    private String getImageUrl(String imagePath) {
        return (imagePath == null || imagePath.trim().isEmpty()) ? "" : !imagePath.startsWith(TyphoonApp.HTTP) ? "https://image.tmdb.org/t/p/w500" + imagePath : imagePath;
    }

    private String replaceSpecialName(String name2) {
        if (name2 == null || name2.isEmpty()) {
            String str = name2;
            return name2;
        } else if (name2.equals("Alien³")) {
            String str2 = name2;
            return "Alien 3";
        } else if (name2.equals("Man v. Food")) {
            String str3 = name2;
            return "Man vs Food";
        } else {
            String name3 = name2.replace("°", "");
            String str4 = name3;
            return name3;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.typhoon.tv.model.media.MediaInfo} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.typhoon.tv.model.media.MediaInfo cloneDeeply() {
        /*
            r5 = this;
            r2 = 0
            r3 = 0
            android.os.Parcel r3 = android.os.Parcel.obtain()     // Catch:{ Exception -> 0x0028 }
            r3.writeValue(r5)     // Catch:{ Exception -> 0x0028 }
            r4 = 0
            r3.setDataPosition(r4)     // Catch:{ Exception -> 0x0028 }
            java.lang.Class<com.typhoon.tv.model.media.MediaInfo> r4 = com.typhoon.tv.model.media.MediaInfo.class
            java.lang.ClassLoader r4 = r4.getClassLoader()     // Catch:{ Exception -> 0x0028 }
            java.lang.Object r4 = r3.readValue(r4)     // Catch:{ Exception -> 0x0028 }
            r0 = r4
            com.typhoon.tv.model.media.MediaInfo r0 = (com.typhoon.tv.model.media.MediaInfo) r0     // Catch:{ Exception -> 0x0028 }
            r2 = r0
            if (r3 == 0) goto L_0x0020
            r3.recycle()
        L_0x0020:
            if (r2 != 0) goto L_0x0027
            com.typhoon.tv.model.media.MediaInfo r2 = new com.typhoon.tv.model.media.MediaInfo
            r2.<init>((com.typhoon.tv.model.media.MediaInfo) r5)
        L_0x0027:
            return r2
        L_0x0028:
            r1 = move-exception
            r4 = 0
            boolean[] r4 = new boolean[r4]     // Catch:{ all -> 0x0035 }
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r1, (boolean[]) r4)     // Catch:{ all -> 0x0035 }
            if (r3 == 0) goto L_0x0020
            r3.recycle()
            goto L_0x0020
        L_0x0035:
            r4 = move-exception
            if (r3 == 0) goto L_0x003b
            r3.recycle()
        L_0x003b:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.model.media.MediaInfo.cloneDeeply():com.typhoon.tv.model.media.MediaInfo");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MediaInfo mediaInfo = (MediaInfo) o;
        if (this.type != mediaInfo.type || this.service != mediaInfo.service || this.tmdbId != mediaInfo.tmdbId || this.tvdbId != mediaInfo.tvdbId || this.year != mediaInfo.year) {
            return false;
        }
        if (this.imdbId != null) {
            if (!this.imdbId.equals(mediaInfo.imdbId)) {
                return false;
            }
        } else if (mediaInfo.imdbId != null) {
            return false;
        }
        if (this.name != null) {
            z = this.name.equals(mediaInfo.name);
        } else if (mediaInfo.name != null) {
            z = false;
        }
        return z;
    }

    public String getBannerUrl() {
        return getImageUrl(this.bannerUrl);
    }

    public String getBannerUrlBase() {
        return this.bannerUrl;
    }

    public String getImdbId() {
        return this.imdbId;
    }

    public String getName() {
        return this.name;
    }

    public String getNameAndYear() {
        return this.year > 0 ? this.name + " (" + this.year + ")" : this.name;
    }

    public String getOriginalName() {
        return this.originalName;
    }

    public String getPosterUrl() {
        return getImageUrl(this.posterUrl);
    }

    public String getPosterUrlBase() {
        return this.posterUrl;
    }

    public int getService() {
        return this.service;
    }

    public int getTmdbId() {
        return this.tmdbId;
    }

    public int getTvdbId() {
        return this.tvdbId;
    }

    public int getType() {
        return this.type;
    }

    public int getYear() {
        return this.year;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((((((this.type * 31) + this.service) * 31) + this.tmdbId) * 31) + this.tvdbId) * 31) + (this.imdbId != null ? this.imdbId.hashCode() : 0)) * 31;
        if (this.name != null) {
            i = this.name.hashCode();
        }
        return ((hashCode + i) * 31) + this.year;
    }

    public void setBannerUrl(String url) {
        this.bannerUrl = url;
    }

    public void setImdbId(String imdbId2) {
        this.imdbId = imdbId2;
    }

    public void setName(String name2) {
        this.name = TitleHelper.m15967(replaceSpecialName(name2));
    }

    public void setOriginalName(String originalName2) {
        this.originalName = originalName2;
    }

    public void setPosterUrl(String url) {
        this.posterUrl = url;
    }

    public void setTmdbId(int tmdbId2) {
        this.tmdbId = tmdbId2;
    }

    public void setTvdbId(int tvdbId2) {
        this.tvdbId = tvdbId2;
    }

    public void setYear(int year2) {
        this.year = year2;
    }

    public String toString() {
        return "MediaInfo{type=" + this.type + ", service=" + this.service + ", tmdbId=" + this.tmdbId + ", tvdbId=" + this.tvdbId + ", imdbId='" + this.imdbId + '\'' + ", name='" + this.name + '\'' + ", originalName='" + this.originalName + '\'' + ", year=" + this.year + ", bannerUrl='" + this.bannerUrl + '\'' + ", posterUrl='" + this.posterUrl + '\'' + '}';
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeInt(this.service);
        dest.writeInt(this.tmdbId);
        dest.writeInt(this.tvdbId);
        dest.writeString(this.imdbId);
        dest.writeString(this.name);
        dest.writeString(this.originalName);
        dest.writeInt(this.year);
        dest.writeString(this.bannerUrl);
        dest.writeString(this.posterUrl);
    }
}
