package com.typhoon.tv.model.media.movie.tmdb;

import com.typhoon.tv.model.media.TmdbVideosBean;
import java.util.List;

public class TmdbMovieInfoResult {
    private boolean adult;
    private String backdrop_path;
    private BelongsToCollectionBean belongs_to_collection;
    private long budget;
    private List<GenresBean> genres;
    private String homepage;
    private int id;
    private String imdb_id;
    private String original_language;
    private String original_title;
    private String overview;
    private double popularity;
    private String poster_path;
    private List<ProductionCompaniesBean> production_companies;
    private List<ProductionCountriesBean> production_countries;
    private String release_date;
    private ReleasesBean releases;
    private long revenue;
    private int runtime;
    private List<SpokenLanguagesBean> spoken_languages;
    private String status;
    private String tagline;
    private String title;
    private boolean video;
    private TmdbVideosBean videos;
    private double vote_average;
    private int vote_count;

    public static class BelongsToCollectionBean {
        private String backdrop_path;
        private int id;
        private String name;
        private String poster_path;

        public String getBackdrop_path() {
            return this.backdrop_path;
        }

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public String getPoster_path() {
            return this.poster_path;
        }

        public void setBackdrop_path(String backdrop_path2) {
            this.backdrop_path = backdrop_path2;
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public void setPoster_path(String poster_path2) {
            this.poster_path = poster_path2;
        }
    }

    public static class GenresBean {
        private int id;
        private String name;

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public void setId(int i) {
            this.id = i;
        }

        public void setName(String str) {
            this.name = str;
        }
    }

    public static class ProductionCompaniesBean {
        private int id;
        private String name;

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public void setId(int i) {
            this.id = i;
        }

        public void setName(String str) {
            this.name = str;
        }
    }

    public static class ProductionCountriesBean {
        private String iso_3166_1;
        private String name;

        public String getIso_3166_1() {
            return this.iso_3166_1;
        }

        public String getName() {
            return this.name;
        }

        public void setIso_3166_1(String str) {
            this.iso_3166_1 = str;
        }

        public void setName(String str) {
            this.name = str;
        }
    }

    public static class ReleasesBean {
        private List<CountriesBean> countries;

        public static class CountriesBean {
            private String certification;
            private String iso_3166_1;
            private boolean primary;
            private String release_date;

            public String getCertification() {
                return this.certification;
            }

            public String getIso_3166_1() {
                return this.iso_3166_1;
            }

            public String getRelease_date() {
                return this.release_date;
            }

            public boolean isPrimary() {
                return this.primary;
            }

            public void setCertification(String str) {
                this.certification = str;
            }

            public void setIso_3166_1(String str) {
                this.iso_3166_1 = str;
            }

            public void setPrimary(boolean z) {
                this.primary = z;
            }

            public void setRelease_date(String str) {
                this.release_date = str;
            }
        }

        public List<CountriesBean> getCountries() {
            return this.countries;
        }

        public void setCountries(List<CountriesBean> list) {
            this.countries = list;
        }
    }

    public static class SpokenLanguagesBean {
        private String iso_639_1;
        private String name;

        public String getIso_639_1() {
            return this.iso_639_1;
        }

        public String getName() {
            return this.name;
        }

        public void setIso_639_1(String str) {
            this.iso_639_1 = str;
        }

        public void setName(String str) {
            this.name = str;
        }
    }

    public String getBackdrop_path() {
        return this.backdrop_path;
    }

    public BelongsToCollectionBean getBelongs_to_collection() {
        return this.belongs_to_collection;
    }

    public long getBudget() {
        return this.budget;
    }

    public List<GenresBean> getGenres() {
        return this.genres;
    }

    public String getHomepage() {
        return this.homepage;
    }

    public int getId() {
        return this.id;
    }

    public String getImdb_id() {
        return this.imdb_id;
    }

    public String getOriginal_language() {
        return this.original_language;
    }

    public String getOriginal_title() {
        return this.original_title;
    }

    public String getOverview() {
        return this.overview;
    }

    public double getPopularity() {
        return this.popularity;
    }

    public String getPoster_path() {
        return this.poster_path;
    }

    public List<ProductionCompaniesBean> getProduction_companies() {
        return this.production_companies;
    }

    public List<ProductionCountriesBean> getProduction_countries() {
        return this.production_countries;
    }

    public String getRelease_date() {
        return this.release_date;
    }

    public ReleasesBean getReleases() {
        return this.releases;
    }

    public long getRevenue() {
        return this.revenue;
    }

    public int getRuntime() {
        return this.runtime;
    }

    public List<SpokenLanguagesBean> getSpoken_languages() {
        return this.spoken_languages;
    }

    public String getStatus() {
        return this.status;
    }

    public String getTagline() {
        return this.tagline;
    }

    public String getTitle() {
        return this.title;
    }

    public TmdbVideosBean getVideos() {
        return this.videos;
    }

    public double getVote_average() {
        return this.vote_average;
    }

    public int getVote_count() {
        return this.vote_count;
    }

    public boolean isAdult() {
        return this.adult;
    }

    public boolean isVideo() {
        return this.video;
    }

    public void setAdult(boolean adult2) {
        this.adult = adult2;
    }

    public void setBackdrop_path(String backdrop_path2) {
        this.backdrop_path = backdrop_path2;
    }

    public void setBelongs_to_collection(BelongsToCollectionBean belongs_to_collection2) {
        this.belongs_to_collection = belongs_to_collection2;
    }

    public void setBudget(int budget2) {
        this.budget = (long) budget2;
    }

    public void setGenres(List<GenresBean> genres2) {
        this.genres = genres2;
    }

    public void setHomepage(String homepage2) {
        this.homepage = homepage2;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public void setImdb_id(String imdb_id2) {
        this.imdb_id = imdb_id2;
    }

    public void setOriginal_language(String original_language2) {
        this.original_language = original_language2;
    }

    public void setOriginal_title(String original_title2) {
        this.original_title = original_title2;
    }

    public void setOverview(String overview2) {
        this.overview = overview2;
    }

    public void setPopularity(double popularity2) {
        this.popularity = popularity2;
    }

    public void setPoster_path(String poster_path2) {
        this.poster_path = poster_path2;
    }

    public void setProduction_companies(List<ProductionCompaniesBean> production_companies2) {
        this.production_companies = production_companies2;
    }

    public void setProduction_countries(List<ProductionCountriesBean> production_countries2) {
        this.production_countries = production_countries2;
    }

    public void setRelease_date(String release_date2) {
        this.release_date = release_date2;
    }

    public void setReleases(ReleasesBean releases2) {
        this.releases = releases2;
    }

    public void setRevenue(int revenue2) {
        this.revenue = (long) revenue2;
    }

    public void setRuntime(int runtime2) {
        this.runtime = runtime2;
    }

    public void setSpoken_languages(List<SpokenLanguagesBean> spoken_languages2) {
        this.spoken_languages = spoken_languages2;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public void setTagline(String tagline2) {
        this.tagline = tagline2;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public void setVideo(boolean video2) {
        this.video = video2;
    }

    public void setVideos(TmdbVideosBean videos2) {
        this.videos = videos2;
    }

    public void setVote_average(double vote_average2) {
        this.vote_average = vote_average2;
    }

    public void setVote_count(int vote_count2) {
        this.vote_count = vote_count2;
    }
}
