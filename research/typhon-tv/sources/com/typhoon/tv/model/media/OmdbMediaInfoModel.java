package com.typhoon.tv.model.media;

import java.util.List;

public class OmdbMediaInfoModel {
    private String Actors;
    private String Awards;
    private String BoxOffice;
    private String Country;
    private String DVD;
    private String Director;
    private String Genre;
    private String Language;
    private String Metascore;
    private String Plot;
    private String Poster;
    private String Production;
    private String Rated;
    private List<RatingsBean> Ratings;
    private String Released;
    private String Response;
    private String Runtime;
    private String Title;
    private String Type;
    private String Website;
    private String Writer;
    private String Year;
    private String imdbID;
    private String imdbRating;
    private String imdbVotes;
    private String tomatoConsensus;
    private String tomatoFresh;
    private String tomatoImage;
    private String tomatoMeter;
    private String tomatoRating;
    private String tomatoReviews;
    private String tomatoRotten;
    private String tomatoURL;
    private String tomatoUserMeter;
    private String tomatoUserRating;
    private String tomatoUserReviews;

    public static class RatingsBean {
        private String Source;
        private String Value;

        public String getSource() {
            return this.Source;
        }

        public String getValue() {
            return this.Value;
        }

        public void setSource(String Source2) {
            this.Source = Source2;
        }

        public void setValue(String Value2) {
            this.Value = Value2;
        }
    }

    public String getActors() {
        return this.Actors;
    }

    public String getAwards() {
        return this.Awards;
    }

    public String getBoxOffice() {
        return this.BoxOffice;
    }

    public String getCountry() {
        return this.Country;
    }

    public String getDVD() {
        return this.DVD;
    }

    public String getDirector() {
        return this.Director;
    }

    public String getGenre() {
        return this.Genre;
    }

    public String getImdbID() {
        return this.imdbID;
    }

    public String getImdbRating() {
        return this.imdbRating;
    }

    public String getImdbVotes() {
        return this.imdbVotes;
    }

    public String getLanguage() {
        return this.Language;
    }

    public String getMetascore() {
        return this.Metascore;
    }

    public String getPlot() {
        return this.Plot;
    }

    public String getPoster() {
        return this.Poster;
    }

    public String getProduction() {
        return this.Production;
    }

    public String getRated() {
        return this.Rated;
    }

    public List<RatingsBean> getRatings() {
        return this.Ratings;
    }

    public String getReleased() {
        return this.Released;
    }

    public String getResponse() {
        return this.Response;
    }

    public String getRuntime() {
        return this.Runtime;
    }

    public String getTitle() {
        return this.Title;
    }

    public String getTomatoConsensus() {
        return this.tomatoConsensus;
    }

    public String getTomatoFresh() {
        return this.tomatoFresh;
    }

    public String getTomatoImage() {
        return this.tomatoImage;
    }

    public String getTomatoMeter() {
        return this.tomatoMeter;
    }

    public String getTomatoRating() {
        return this.tomatoRating;
    }

    public String getTomatoReviews() {
        return this.tomatoReviews;
    }

    public String getTomatoRotten() {
        return this.tomatoRotten;
    }

    public String getTomatoURL() {
        return this.tomatoURL;
    }

    public String getTomatoUserMeter() {
        return this.tomatoUserMeter;
    }

    public String getTomatoUserRating() {
        return this.tomatoUserRating;
    }

    public String getTomatoUserReviews() {
        return this.tomatoUserReviews;
    }

    public String getType() {
        return this.Type;
    }

    public String getWebsite() {
        return this.Website;
    }

    public String getWriter() {
        return this.Writer;
    }

    public String getYear() {
        return this.Year;
    }

    public void setActors(String Actors2) {
        this.Actors = Actors2;
    }

    public void setAwards(String Awards2) {
        this.Awards = Awards2;
    }

    public void setBoxOffice(String BoxOffice2) {
        this.BoxOffice = BoxOffice2;
    }

    public void setCountry(String Country2) {
        this.Country = Country2;
    }

    public void setDVD(String DVD2) {
        this.DVD = DVD2;
    }

    public void setDirector(String Director2) {
        this.Director = Director2;
    }

    public void setGenre(String Genre2) {
        this.Genre = Genre2;
    }

    public void setImdbID(String imdbID2) {
        this.imdbID = imdbID2;
    }

    public void setImdbRating(String imdbRating2) {
        this.imdbRating = imdbRating2;
    }

    public void setImdbVotes(String imdbVotes2) {
        this.imdbVotes = imdbVotes2;
    }

    public void setLanguage(String Language2) {
        this.Language = Language2;
    }

    public void setMetascore(String Metascore2) {
        this.Metascore = Metascore2;
    }

    public void setPlot(String Plot2) {
        this.Plot = Plot2;
    }

    public void setPoster(String Poster2) {
        this.Poster = Poster2;
    }

    public void setProduction(String Production2) {
        this.Production = Production2;
    }

    public void setRated(String Rated2) {
        this.Rated = Rated2;
    }

    public void setRatings(List<RatingsBean> Ratings2) {
        this.Ratings = Ratings2;
    }

    public void setReleased(String Released2) {
        this.Released = Released2;
    }

    public void setResponse(String Response2) {
        this.Response = Response2;
    }

    public void setRuntime(String Runtime2) {
        this.Runtime = Runtime2;
    }

    public void setTitle(String Title2) {
        this.Title = Title2;
    }

    public void setTomatoConsensus(String tomatoConsensus2) {
        this.tomatoConsensus = tomatoConsensus2;
    }

    public void setTomatoFresh(String tomatoFresh2) {
        this.tomatoFresh = tomatoFresh2;
    }

    public void setTomatoImage(String tomatoImage2) {
        this.tomatoImage = tomatoImage2;
    }

    public void setTomatoMeter(String tomatoMeter2) {
        this.tomatoMeter = tomatoMeter2;
    }

    public void setTomatoRating(String tomatoRating2) {
        this.tomatoRating = tomatoRating2;
    }

    public void setTomatoReviews(String tomatoReviews2) {
        this.tomatoReviews = tomatoReviews2;
    }

    public void setTomatoRotten(String tomatoRotten2) {
        this.tomatoRotten = tomatoRotten2;
    }

    public void setTomatoURL(String tomatoURL2) {
        this.tomatoURL = tomatoURL2;
    }

    public void setTomatoUserMeter(String tomatoUserMeter2) {
        this.tomatoUserMeter = tomatoUserMeter2;
    }

    public void setTomatoUserRating(String tomatoUserRating2) {
        this.tomatoUserRating = tomatoUserRating2;
    }

    public void setTomatoUserReviews(String tomatoUserReviews2) {
        this.tomatoUserReviews = tomatoUserReviews2;
    }

    public void setType(String Type2) {
        this.Type = Type2;
    }

    public void setWebsite(String Website2) {
        this.Website = Website2;
    }

    public void setWriter(String Writer2) {
        this.Writer = Writer2;
    }

    public void setYear(String Year2) {
        this.Year = Year2;
    }
}
