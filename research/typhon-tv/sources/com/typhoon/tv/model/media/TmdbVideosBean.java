package com.typhoon.tv.model.media;

import java.util.List;

public class TmdbVideosBean {
    private List<ResultsBean> results;

    public static class ResultsBean {
        private String id;
        private String iso_3166_1;
        private String iso_639_1;
        private String key;
        private String name;
        private String site;
        private int size;
        private String type;

        public String getId() {
            return this.id;
        }

        public String getIso_3166_1() {
            return this.iso_3166_1;
        }

        public String getIso_639_1() {
            return this.iso_639_1;
        }

        public String getKey() {
            return this.key;
        }

        public String getName() {
            return this.name;
        }

        public String getSite() {
            return this.site;
        }

        public int getSize() {
            return this.size;
        }

        public String getType() {
            return this.type;
        }

        public void setId(String id2) {
            this.id = id2;
        }

        public void setIso_3166_1(String iso_3166_12) {
            this.iso_3166_1 = iso_3166_12;
        }

        public void setIso_639_1(String iso_639_12) {
            this.iso_639_1 = iso_639_12;
        }

        public void setKey(String key2) {
            this.key = key2;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public void setSite(String site2) {
            this.site = site2;
        }

        public void setSize(int size2) {
            this.size = size2;
        }

        public void setType(String type2) {
            this.type = type2;
        }
    }

    public List<ResultsBean> getResults() {
        return this.results;
    }

    public void setResults(List<ResultsBean> results2) {
        this.results = results2;
    }
}
