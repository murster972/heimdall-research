package com.typhoon.tv.model.media.tv.tvmaze;

public class TvMazeScheduleResult {
    private String airdate;
    private String airstamp;
    private String airtime;
    private Object image;
    private String name;
    private int number;
    private int runtime;
    private int season;
    private ShowBean show;
    private String summary;

    public static class ShowBean {
        private ExternalsBean externals;
        private ImageBean image;
        private String language;
        private String name;
        private NetworkBean network;
        private String premiered;
        private int runtime;
        private String status;
        private String summary;
        private String type;

        public static class ExternalsBean {
            private Object imdb;
            private Object thetvdb;

            public Object getImdb() {
                return this.imdb;
            }

            public Object getThetvdb() {
                return this.thetvdb;
            }

            public void setImdb(Object obj) {
                this.imdb = obj;
            }

            public void setThetvdb(Object obj) {
                this.thetvdb = obj;
            }
        }

        public static class ImageBean {
            private String medium;
            private String original;

            public String getMedium() {
                return this.medium;
            }

            public String getOriginal() {
                return this.original;
            }

            public void setMedium(String str) {
                this.medium = str;
            }

            public void setOriginal(String str) {
                this.original = str;
            }
        }

        public static class NetworkBean {
            private CountryBean country;
            private int id;
            private String name;

            public static class CountryBean {
                private String code;
                private String name;
                private String timezone;

                public String getCode() {
                    return this.code;
                }

                public String getName() {
                    return this.name;
                }

                public String getTimezone() {
                    return this.timezone;
                }

                public void setCode(String str) {
                    this.code = str;
                }

                public void setName(String str) {
                    this.name = str;
                }

                public void setTimezone(String str) {
                    this.timezone = str;
                }
            }

            public CountryBean getCountry() {
                return this.country;
            }

            public int getId() {
                return this.id;
            }

            public String getName() {
                return this.name;
            }

            public void setCountry(CountryBean countryBean) {
                this.country = countryBean;
            }

            public void setId(int i) {
                this.id = i;
            }

            public void setName(String str) {
                this.name = str;
            }
        }

        public ExternalsBean getExternals() {
            return this.externals;
        }

        public ImageBean getImage() {
            return this.image;
        }

        public String getLanguage() {
            return this.language;
        }

        public String getName() {
            return this.name;
        }

        public NetworkBean getNetwork() {
            return this.network;
        }

        public String getPremiered() {
            return this.premiered;
        }

        public int getRuntime() {
            return this.runtime;
        }

        public String getStatus() {
            return this.status;
        }

        public String getSummary() {
            return this.summary;
        }

        public String getType() {
            return this.type;
        }

        public void setExternals(ExternalsBean externalsBean) {
            this.externals = externalsBean;
        }

        public void setImage(ImageBean imageBean) {
            this.image = imageBean;
        }

        public void setLanguage(String str) {
            this.language = str;
        }

        public void setName(String str) {
            this.name = str;
        }

        public void setNetwork(NetworkBean networkBean) {
            this.network = networkBean;
        }

        public void setPremiered(String str) {
            this.premiered = str;
        }

        public void setRuntime(int i) {
            this.runtime = i;
        }

        public void setStatus(String str) {
            this.status = str;
        }

        public void setSummary(String str) {
            this.summary = str;
        }

        public void setType(String str) {
            this.type = str;
        }
    }

    public String getAirdate() {
        return this.airdate;
    }

    public String getAirstamp() {
        return this.airstamp;
    }

    public String getAirtime() {
        return this.airtime;
    }

    public Object getImage() {
        return this.image;
    }

    public String getName() {
        return this.name;
    }

    public int getNumber() {
        return this.number;
    }

    public int getRuntime() {
        return this.runtime;
    }

    public int getSeason() {
        return this.season;
    }

    public ShowBean getShow() {
        return this.show;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setAirdate(String str) {
        this.airdate = str;
    }

    public void setAirstamp(String str) {
        this.airstamp = str;
    }

    public void setAirtime(String str) {
        this.airtime = str;
    }

    public void setImage(Object obj) {
        this.image = obj;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setNumber(int i) {
        this.number = i;
    }

    public void setRuntime(int i) {
        this.runtime = i;
    }

    public void setSeason(int i) {
        this.season = i;
    }

    public void setShow(ShowBean showBean) {
        this.show = showBean;
    }

    public void setSummary(String str) {
        this.summary = str;
    }
}
