package com.typhoon.tv.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;
import com.typhoon.tv.model.media.MediaInfo;

public class CheckEpisodeResult extends SugarRecord {
    private int lastEpisode;
    private int lastSeason;
    @Unique
    private int tmdbId;

    public CheckEpisodeResult() {
    }

    public CheckEpisodeResult(int i) {
        this.tmdbId = i;
    }

    public CheckEpisodeResult(MediaInfo mediaInfo) {
        this.tmdbId = mediaInfo.getTmdbId();
    }

    public int getLastEpisode() {
        return this.lastEpisode;
    }

    public int getLastSeason() {
        return this.lastSeason;
    }

    public int getTmdbId() {
        return this.tmdbId;
    }

    public void setLastEpisode(int i) {
        this.lastEpisode = i;
    }

    public void setLastSeason(int i) {
        this.lastSeason = i;
    }

    public String toString() {
        return "CheckEpisodeResult{lastSeason=" + this.lastSeason + ", lastEpisode=" + this.lastEpisode + '}';
    }
}
