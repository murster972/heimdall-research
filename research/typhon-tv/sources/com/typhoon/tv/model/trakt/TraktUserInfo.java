package com.typhoon.tv.model.trakt;

import com.google.gson.annotations.SerializedName;

public class TraktUserInfo {
    private IdsBean ids;
    private Object name;
    @SerializedName("private")
    private boolean privateX;
    private String username;
    private boolean vip;
    private boolean vip_ep;

    public static class IdsBean {
        private String slug;

        public String getSlug() {
            return this.slug;
        }

        public void setSlug(String str) {
            this.slug = str;
        }
    }

    public IdsBean getIds() {
        return this.ids;
    }

    public Object getName() {
        return this.name;
    }

    public String getUsername() {
        return this.username;
    }

    public boolean isPrivateX() {
        return this.privateX;
    }

    public boolean isTyphoon() {
        return this.vip;
    }

    public boolean isTyphoon_ep() {
        return this.vip_ep;
    }

    public void setIds(IdsBean idsBean) {
        this.ids = idsBean;
    }

    public void setName(Object obj) {
        this.name = obj;
    }

    public void setPrivateX(boolean z) {
        this.privateX = z;
    }

    public void setTyphoon(boolean z) {
        this.vip = z;
    }

    public void setTyphoon_ep(boolean z) {
        this.vip_ep = z;
    }

    public void setUsername(String str) {
        this.username = str;
    }
}
