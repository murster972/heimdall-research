package com.typhoon.tv.model.trakt;

public class TraktCredentialsInfo {
    private String accessToken;
    private String refreshToken;
    private String user;

    public TraktCredentialsInfo() {
    }

    public TraktCredentialsInfo(String str, String str2, String str3) {
        this.user = str;
        this.accessToken = str2;
        this.refreshToken = str3;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    public String getUser() {
        return this.user;
    }

    public boolean isValid() {
        return this.accessToken != null && !this.accessToken.isEmpty() && this.refreshToken != null && !this.refreshToken.isEmpty();
    }

    public void setAccessToken(String str) {
        this.accessToken = str;
    }

    public void setRefreshToken(String str) {
        this.refreshToken = str;
    }

    public void setUser(String str) {
        this.user = str;
    }

    public String toString() {
        return "TraktCredentialsInfo{user='" + this.user + '\'' + ", accessToken='" + this.accessToken + '\'' + ", refreshToken='" + this.refreshToken + '\'' + '}';
    }
}
