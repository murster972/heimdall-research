package com.typhoon.tv.model;

import com.orm.SugarRecord;
import com.typhoon.tv.helper.DateTimeHelper;

public class LastPlaybackInfo extends SugarRecord {
    private long duration;
    private int episode;
    private long position;
    private int season;
    private long timestamp;
    private int tmdbId;

    public LastPlaybackInfo() {
        this.timestamp = Long.parseLong(DateTimeHelper.m15934());
    }

    public LastPlaybackInfo(int i, long j) {
        this();
        this.tmdbId = i;
        this.position = j;
    }

    public long getDuration() {
        return this.duration;
    }

    public int getEpisode() {
        return this.episode;
    }

    public long getPosition() {
        return this.position;
    }

    public int getSeason() {
        return this.season;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public int getTmdbId() {
        return this.tmdbId;
    }

    public void setDuration(long j) {
        this.duration = j;
    }

    public void setEpisode(int i) {
        this.episode = i;
    }

    public void setPosition(long j) {
        this.position = j;
    }

    public void setSeason(int i) {
        this.season = i;
    }

    public void setTimestamp(long j) {
        this.timestamp = j;
    }

    public void setTmdbId(int i) {
        this.tmdbId = i;
    }

    public String toString() {
        return "LastPlaybackInfo{tmdbId=" + this.tmdbId + ", season=" + this.season + ", episode=" + this.episode + ", position=" + this.position + ", duration=" + this.duration + ", timestamp=" + this.timestamp + '}';
    }
}
