package com.typhoon.tv.model.download;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class DownloadIDRecord extends SugarRecord {
    @Unique
    private long downloadId;
    private String downloadPath;
    private String posterUrl;

    public long getDownloadId() {
        return this.downloadId;
    }

    public String getDownloadPath() {
        return this.downloadPath;
    }

    public String getPosterUrl() {
        return this.posterUrl;
    }

    public void setDownloadId(long downloadId2) {
        this.downloadId = downloadId2;
    }

    public void setDownloadPath(String downloadPath2) {
        this.downloadPath = downloadPath2;
    }

    public void setPosterUrl(String posterUrl2) {
        this.posterUrl = posterUrl2;
    }

    public String toString() {
        return "DownloadIDRecord{downloadId=" + this.downloadId + ", downloadPath='" + this.downloadPath + '\'' + ", posterUrl='" + this.posterUrl + '\'' + '}';
    }
}
