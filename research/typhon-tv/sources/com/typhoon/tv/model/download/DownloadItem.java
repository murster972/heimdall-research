package com.typhoon.tv.model.download;

import android.os.Parcel;
import android.os.Parcelable;

public class DownloadItem implements Parcelable {
    public static final Parcelable.Creator<DownloadItem> CREATOR = new Parcelable.Creator<DownloadItem>() {
        public DownloadItem createFromParcel(Parcel parcel) {
            return new DownloadItem(parcel);
        }

        public DownloadItem[] newArray(int i) {
            return new DownloadItem[i];
        }
    };
    private long downloadId;
    private String errorMessage;
    private boolean isDownloading;
    private String localUri;
    private String posterUrl;
    private String title;
    private long totalSize;

    public DownloadItem() {
    }

    protected DownloadItem(Parcel parcel) {
        this.downloadId = parcel.readLong();
        this.title = parcel.readString();
        this.localUri = parcel.readString();
        this.posterUrl = parcel.readString();
        this.totalSize = parcel.readLong();
        this.isDownloading = parcel.readByte() != 0;
        this.errorMessage = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public long getDownloadId() {
        return this.downloadId;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String getLocalUri() {
        return this.localUri;
    }

    public String getPosterUrl() {
        return this.posterUrl;
    }

    public String getTitle() {
        return this.title;
    }

    public long getTotalSize() {
        return this.totalSize;
    }

    public boolean isDownloading() {
        return this.isDownloading;
    }

    public void setDownloadId(long j) {
        this.downloadId = j;
    }

    public void setDownloading(boolean z) {
        this.isDownloading = z;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public void setLocalUri(String str) {
        this.localUri = str;
    }

    public void setPosterUrl(String str) {
        this.posterUrl = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setTotalSize(long j) {
        this.totalSize = j;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.downloadId);
        parcel.writeString(this.title);
        parcel.writeString(this.localUri);
        parcel.writeString(this.posterUrl);
        parcel.writeLong(this.totalSize);
        parcel.writeByte(this.isDownloading ? (byte) 1 : 0);
        parcel.writeString(this.errorMessage);
    }
}
