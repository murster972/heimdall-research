package com.typhoon.tv.extractor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import junrar.Archive;
import junrar.exception.RarException;
import junrar.rarfile.FileHeader;
import org.apache.commons.logging.Log;

public class RarExtractArchive {

    /* renamed from: 龘  reason: contains not printable characters */
    private Log f12614;

    /* renamed from: 靐  reason: contains not printable characters */
    private File m15908(FileHeader fileHeader, File file) {
        String r1 = (!fileHeader.m19527() || !fileHeader.m19526()) ? fileHeader.m19516() : fileHeader.m19531();
        File file2 = new File(file, r1);
        if (file2.exists()) {
            return file2;
        }
        try {
            return m15911(file, r1);
        } catch (IOException e) {
            m15913((Exception) e, "error creating the new file: " + file2.getName());
            return file2;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m15909(File file, String str) {
        String[] split = str.split("\\\\");
        String str2 = "";
        int length = split.length;
        for (int i = 0; i < length; i++) {
            str2 = str2 + File.separator + split[i];
            new File(file, str2).mkdir();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m15910(String str) {
        if (this.f12614 != null) {
            this.f12614.info(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static File m15911(File file, String str) throws IOException {
        String[] split = str.split("\\\\");
        String str2 = "";
        int length = split.length;
        if (length == 1) {
            return new File(file, str);
        }
        if (length <= 1) {
            return null;
        }
        for (int i = 0; i < split.length - 1; i++) {
            str2 = str2 + File.separator + split[i];
            new File(file, str2).mkdir();
        }
        File file2 = new File(file, str2 + File.separator + split[split.length - 1]);
        file2.createNewFile();
        return file2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15912(Exception exc) {
        if (this.f12614 != null) {
            this.f12614.error(exc);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15913(Exception exc, String str) {
        if (this.f12614 != null) {
            this.f12614.error(str, exc);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m15914(String str) {
        if (this.f12614 != null) {
            this.f12614.warn(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m15915(FileHeader fileHeader, File file) {
        if (!fileHeader.m19530() || !fileHeader.m19526()) {
            if (fileHeader.m19530() && !fileHeader.m19526() && !new File(file, fileHeader.m19516()).exists()) {
                m15909(file, fileHeader.m19516());
            }
        } else if (!new File(file, fileHeader.m19531()).exists()) {
            m15909(file, fileHeader.m19531());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m15916(File file, File file2) {
        Archive archive = null;
        try {
            archive = new Archive(file);
        } catch (IOException | RarException e) {
            m15912(e);
        }
        if (archive == null) {
            return;
        }
        if (archive.m19465()) {
            m15914("archive is encrypted cannot extreact");
            return;
        }
        for (FileHeader next : archive.m19466()) {
            if (next != null) {
                String r4 = next.m19516();
                if (next.m19524()) {
                    m15914("file is encrypted cannot extract: " + r4);
                } else {
                    m15910("extracting: " + r4);
                    try {
                        if (next.m19530()) {
                            m15915(next, file2);
                        } else {
                            FileOutputStream fileOutputStream = new FileOutputStream(m15908(next, file2));
                            archive.m19469(next, (OutputStream) fileOutputStream);
                            fileOutputStream.close();
                        }
                    } catch (IOException e2) {
                        m15913((Exception) e2, "error extracting the file");
                    } catch (RarException e3) {
                        m15913((Exception) e3, "error extraction the file");
                    }
                }
            }
        }
    }
}
