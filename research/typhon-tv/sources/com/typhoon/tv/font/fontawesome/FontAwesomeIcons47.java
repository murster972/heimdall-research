package com.typhoon.tv.font.fontawesome;

import com.joanzapata.iconify.Icon;

public enum FontAwesomeIcons47 implements Icon {
    fa_imdb(62168),
    fa_facebook_official(62000),
    fa_youtube_play(61802);
    
    private final char character;

    private FontAwesomeIcons47(char c) {
        this.character = c;
    }

    public char character() {
        return this.character;
    }

    public String key() {
        return name().replace('_', '-');
    }
}
