package com.typhoon.tv.font.fontawesome;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconFontDescriptor;

public class FontAwesomeModule47 implements IconFontDescriptor {
    public Icon[] characters() {
        return FontAwesomeIcons47.values();
    }

    public String ttfFileName() {
        return "fonts/fontawesome-webfont-47.ttf";
    }
}
