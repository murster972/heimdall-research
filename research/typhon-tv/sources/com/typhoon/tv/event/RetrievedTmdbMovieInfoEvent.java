package com.typhoon.tv.event;

import com.typhoon.tv.model.media.movie.tmdb.TmdbMovieInfoResult;

public class RetrievedTmdbMovieInfoEvent {
    private final int activityId;
    private final TmdbMovieInfoResult info;

    public RetrievedTmdbMovieInfoEvent(TmdbMovieInfoResult tmdbMovieInfoResult, int i) {
        this.info = tmdbMovieInfoResult;
        this.activityId = i;
    }

    public int getActivityId() {
        return this.activityId;
    }

    public TmdbMovieInfoResult getInfo() {
        return this.info;
    }
}
