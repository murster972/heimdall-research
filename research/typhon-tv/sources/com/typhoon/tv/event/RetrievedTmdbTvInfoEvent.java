package com.typhoon.tv.event;

import com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult;

public class RetrievedTmdbTvInfoEvent {
    private final int acyivityId;
    private final TmdbTvInfoResult info;

    public RetrievedTmdbTvInfoEvent(TmdbTvInfoResult tmdbTvInfoResult, int i) {
        this.info = tmdbTvInfoResult;
        this.acyivityId = i;
    }

    public int getActivityId() {
        return this.acyivityId;
    }

    public TmdbTvInfoResult getInfo() {
        return this.info;
    }
}
