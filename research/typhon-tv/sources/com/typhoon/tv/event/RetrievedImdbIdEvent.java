package com.typhoon.tv.event;

public class RetrievedImdbIdEvent {
    private final int activityId;
    private final String imdbId;

    public RetrievedImdbIdEvent(String imdbId2, int activityId2) {
        this.imdbId = imdbId2;
        this.activityId = activityId2;
    }

    public int getActivityId() {
        return this.activityId;
    }

    public String getImdbId() {
        return this.imdbId;
    }
}
