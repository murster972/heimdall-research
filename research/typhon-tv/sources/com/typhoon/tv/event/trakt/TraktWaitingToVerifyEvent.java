package com.typhoon.tv.event.trakt;

public class TraktWaitingToVerifyEvent {
    private String userCode;
    private String verificationUrl;

    public TraktWaitingToVerifyEvent(String str, String str2) {
        this.verificationUrl = str;
        this.userCode = str2;
    }

    public String getUserCode() {
        return this.userCode;
    }

    public String getVerificationUrl() {
        return this.verificationUrl;
    }

    public void setUserCode(String str) {
        this.userCode = str;
    }

    public void setVerificationUrl(String str) {
        this.verificationUrl = str;
    }
}
