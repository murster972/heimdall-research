package com.typhoon.tv.event;

public class ReCaptchaRequiredEvent {
    private String providerName;
    private String url;
    private String userAgent;

    public ReCaptchaRequiredEvent(String str, String str2) {
        this(str, str2, "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0");
    }

    public ReCaptchaRequiredEvent(String str, String str2, String str3) {
        this.providerName = str;
        this.url = str2;
        this.userAgent = str3 == null ? "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0" : str3;
    }

    public String getProviderName() {
        return this.providerName;
    }

    public String getUrl() {
        return this.url;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public void setProviderName(String str) {
        this.providerName = str;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void setUserAgent(String str) {
        this.userAgent = str;
    }
}
