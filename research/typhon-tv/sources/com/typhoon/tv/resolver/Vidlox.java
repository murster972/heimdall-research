package com.typhoon.tv.resolver;

import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import java.util.Iterator;
import java.util.List;
import rx.Observable;
import rx.Subscriber;

public class Vidlox extends BaseResolver {
    /* renamed from: 靐  reason: contains not printable characters */
    public String m16738() {
        return "Vidlox";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16739(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                List<ResolveResult> r4;
                String r1 = Regex.m17801(str, "(?://|\\.)(vidlox\\.(?:tv|me))/(?:embed-)?([0-9a-zA-Z]+)", 2, 2);
                if (r1.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                if (RealDebridCredentialsHelper.m15838().isValid() && (r4 = RealDebridUserApi.m15845().m15850(str, Vidlox.this.m16738())) != null) {
                    for (ResolveResult onNext : r4) {
                        subscriber.onNext(onNext);
                    }
                }
                String str = "https://vidlox.tv/embed-" + r1 + ".html";
                Iterator it2 = Regex.m17805(Regex.m17802(HttpHelper.m6343().m6358(str, str), "sources\\s*:\\s*\\[(.+?)\\]", 1, true), "[\"'](http[^\"']+)", 1, true).get(0).iterator();
                while (it2.hasNext()) {
                    subscriber.onNext(new ResolveResult(Vidlox.this.m16738(), (String) it2.next(), "HQ"));
                }
                subscriber.onCompleted();
            }
        });
    }
}
