package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class HxLoad extends GenericResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16597() {
        return "https://hxload.to";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16596() {
        return "(?://|\\.)(hxload\\.to)/(?:embed/)?([0-9a-zA-Z.?=&%]+)";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16598(String baseUrl, String mediaId) {
        return "https://hxload.to/embed/" + mediaId;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16594() {
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16595() {
        return "HxLoad";
    }
}
