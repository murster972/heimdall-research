package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class JetLoad extends GenericResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16602() {
        return "https://jetload.net";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16601() {
        return "(?://|\\.)(jetload\\.net)/(?:e|p)/([0-9a-zA-Z-_]+)";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16603(String baseUrl, String mediaId) {
        return "https://jetload.net/p/" + mediaId;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16599() {
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16600() {
        return "JetLoad";
    }
}
