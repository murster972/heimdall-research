package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class SendVid extends GenericResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16654() {
        return "https://sendvid.com";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16653() {
        return "(?://|\\.)(sendvid\\.com)/(?:embed/)?([0-9a-zA-Z.?=&%]+)";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16655(String baseUrl, String mediaId) {
        return "https://sendvid.com/embed/" + mediaId;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16651() {
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16652() {
        return "SendVid";
    }
}
