package com.typhoon.tv.resolver;

import android.util.Base64;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.duktape.Duktape;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;

public class TheVideo extends BaseResolver {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static String f13017;

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f13018;

    private interface Http {
        String get(String str, String str2);
    }

    private interface Log {
        void d(String str);
    }

    private interface TheVideoDecoder {
        String decode(String str, String str2);

        boolean isEnabled();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16683(final String url) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String js;
                List<ResolveResult> rdResultList;
                String mediaId = Regex.m17801(url, "(?://|\\.)((?:vev|thevideo)\\.(?:io|red|me))/(?:embed/)?([0-9a-zA-Z]+)", 2, 2);
                if (mediaId.isEmpty() || !TyphoonApp.f5859) {
                    subscriber.onCompleted();
                    return;
                }
                if (RealDebridCredentialsHelper.m15838().isValid() && (rdResultList = RealDebridUserApi.m15845().m15850(url, TheVideo.this.m16681())) != null) {
                    for (ResolveResult resolveResult : rdResultList) {
                        subscriber.onNext(resolveResult);
                    }
                }
                String playPageUrl = "https://vev.io/" + mediaId + "-640x360.html";
                String playPageHtml = HttpHelper.m6343().m6351(playPageUrl, (Map<String, String>[]) new Map[0]);
                if (playPageHtml.isEmpty() || playPageHtml.contains("File was deleted") || playPageHtml.contains("File not found") || playPageHtml.contains("Page Cannot Be Found")) {
                    subscriber.onCompleted();
                    return;
                }
                if (TheVideo.f13017 == null || TheVideo.f13017.isEmpty()) {
                    js = TheVideo.f13017 = HttpHelper.m6343().m6351(TheVideo.this.m16677(), (Map<String, String>[]) new Map[0]);
                } else {
                    js = TheVideo.f13017;
                }
                if (js.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                String playLinkArrJson = "";
                Duktape duktape = null;
                try {
                    duktape = Duktape.create();
                    duktape.set("Log", Log.class, new Log() {
                        public void d(String str) {
                        }
                    });
                    duktape.set("Http", Http.class, new Http() {
                        public String get(String url, String referer) {
                            return HttpHelper.m6343().m6358(url, referer);
                        }
                    });
                    duktape.evaluate(js);
                    TheVideoDecoder decoder = (TheVideoDecoder) duktape.get("TheVideoDecoder", TheVideoDecoder.class);
                    if (!decoder.isEnabled()) {
                        subscriber.onCompleted();
                        if (duktape != null) {
                            duktape.close();
                            return;
                        }
                        return;
                    }
                    playLinkArrJson = decoder.decode(playPageUrl, playPageHtml);
                    if (duktape != null) {
                        duktape.close();
                    }
                    if (playLinkArrJson.isEmpty()) {
                        subscriber.onCompleted();
                        return;
                    }
                    Iterator<JsonElement> it2 = new JsonParser().parse(playLinkArrJson).getAsJsonArray().iterator();
                    while (it2.hasNext()) {
                        try {
                            JsonObject jObj = it2.next().getAsJsonObject();
                            JsonElement jEleQuality = jObj.get("quality");
                            JsonElement jEleLink = jObj.get("link");
                            if (jEleLink != null && !jEleLink.isJsonNull() && jEleQuality != null && !jEleQuality.isJsonNull()) {
                                String quality = jEleQuality.getAsString().trim();
                                String link = jEleLink.getAsString().trim();
                                if (TheVideo.this.m16779(link) && !link.contains(".srt") && !link.contains(".vtt") && !link.contains(".ass") && !link.contains(".zip") && !link.contains(".rar") && !link.contains(".7z")) {
                                    subscriber.onNext(new ResolveResult(TheVideo.this.m16681(), link, quality));
                                }
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                    subscriber.onCompleted();
                } catch (Throwable th) {
                    if (duktape != null) {
                        duktape.close();
                    }
                    throw th;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16677() {
        if (f13018 == null || f13018.isEmpty()) {
            try {
                f13018 = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Jlc29sdmVyL3RoZXZpZGVvbWUuanM=", 10), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                f13018 = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Jlc29sdmVyL3RoZXZpZGVvbWUuanM=", 10));
            }
        }
        return f13018;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16682() {
        return "HD";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16681() {
        return "TheVideo";
    }
}
