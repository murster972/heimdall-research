package com.typhoon.tv.resolver;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.commons.lang3.StringUtils;
import rx.Observable;
import rx.Subscriber;

public class OK extends BaseResolver {
    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public HashMap<String, String> m16624() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("mobile", "144p");
        hashMap.put("lowest", "240p");
        hashMap.put("low", "360p");
        hashMap.put("sd", "480p");
        hashMap.put("hd", "720p");
        hashMap.put("full", "1080p");
        hashMap.put("quad", "QuadHD");
        hashMap.put("ultra", "4K");
        return hashMap;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16626() {
        return "ok.ru";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16627() {
        return "HD";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16628(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String r6 = Regex.m17801(str, "(?://|\\.)(ok\\.ru|odnoklassniki\\.ru)/(?:videoembed|video|moviePlayer)/([A-Za-z0-9-]+)", 2, 2);
                if (r6.isEmpty()) {
                    r6 = Regex.m17800(str, "[\\?\\&]mid=(\\d+)", 1);
                    if (r6.isEmpty()) {
                        r6 = Regex.m17800(str, "st\\.mvId=(\\d+)", 1);
                    }
                }
                if (r6.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                HashMap hashMap = new HashMap();
                hashMap.put("Referer", "https://www.ok.ru/videoembed/" + r6);
                String replaceAll = HttpHelper.m6343().m6350("https://www.ok.ru/dk?cmd=videoPlayerMetadata&mid=" + Utils.m6414(r6, new boolean[0]), "cmd=videoPlayerMetadata&mid=" + Utils.m6414(r6, new boolean[0]), hashMap).replaceAll("[^\\x00-\\x7F]+", StringUtils.SPACE);
                if (replaceAll.isEmpty() || replaceAll.contains("error")) {
                    subscriber.onCompleted();
                    return;
                }
                JsonElement parse = new JsonParser().parse(replaceAll);
                if (parse == null || parse.isJsonNull() || !parse.isJsonObject()) {
                    subscriber.onCompleted();
                    return;
                }
                JsonElement jsonElement = parse.getAsJsonObject().get("videos");
                if (jsonElement == null || jsonElement.isJsonNull() || !jsonElement.isJsonArray()) {
                    subscriber.onCompleted();
                    return;
                }
                HashMap r14 = OK.this.m16624();
                Iterator<JsonElement> it2 = jsonElement.getAsJsonArray().iterator();
                while (it2.hasNext()) {
                    JsonElement next = it2.next();
                    if (next != null && !next.isJsonNull() && next.isJsonObject()) {
                        String lowerCase = next.getAsJsonObject().get("name").getAsString().trim().toLowerCase();
                        String asString = next.getAsJsonObject().get("url").getAsString();
                        if (!asString.isEmpty()) {
                            String str = "HQ";
                            if (r14.containsKey(lowerCase)) {
                                str = (String) r14.get(lowerCase);
                            }
                            ResolveResult resolveResult = new ResolveResult(OK.this.m16626(), asString, str);
                            HashMap hashMap2 = new HashMap();
                            hashMap2.put(AbstractSpiCall.HEADER_ACCEPT, "*/*");
                            hashMap2.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                            hashMap2.put("Origin", "https://www.ok.ru");
                            hashMap2.put("Referer", "https://www.ok.ru/");
                            hashMap2.put("Cookie", HttpHelper.m6343().m6349(asString));
                            resolveResult.setPlayHeader(hashMap2);
                            subscriber.onNext(resolveResult);
                        }
                    }
                }
                subscriber.onCompleted();
            }
        });
    }
}
