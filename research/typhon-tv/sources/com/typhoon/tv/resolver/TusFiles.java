package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class TusFiles extends GenericResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16688() {
        return "https://tusfiles.com";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16687() {
        return "(?://|\\.)(tusfiles\\.com)/(?:embed-)?([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16685() {
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16686() {
        return "TusFiles";
    }
}
