package com.typhoon.tv.resolver;

import android.util.Base64;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.squareup.duktape.Duktape;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;

public class Openload extends BaseResolver {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static String f13005;

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f13006;

    private interface JavaRegex {
        String findAll(String str, String str2, int i);

        String findAllWithMode(String str, String str2, int i, int i2);
    }

    private interface JavaUrlDecoder {
        String decode(String str);
    }

    private interface Log {
        void d(String str);
    }

    private interface OpenloadDecoder {
        String decode(String str);

        boolean isEnabled();
    }

    private interface TTVHttp {
        String get(String str, String str2);

        String post(String str, String str2, String str3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16641(final String url) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String js;
                List<ResolveResult> rdResultList;
                if (RealDebridCredentialsHelper.m15838().isValid() && (rdResultList = RealDebridUserApi.m15845().m15850(url, Openload.this.m16639())) != null) {
                    for (ResolveResult resolveResult : rdResultList) {
                        subscriber.onNext(resolveResult);
                    }
                }
                String mediaId = Regex.m17801(url, "(?://|\\.)(e(?:asy)??load\\.(?:io|co|tv|stream|link|site|xyz|win))/(?:e|f)/([0-9a-zA-Z-_]+)", 2, 2);
                if (mediaId.isEmpty() || !TyphoonApp.f5859 || !TyphoonApp.f5861) {
                    subscriber.onCompleted();
                    return;
                }
                String playPageHtml = HttpHelper.m6343().m6351("https://easyload.io/e/" + mediaId, (Map<String, String>[]) new Map[0]);
                if (playPageHtml.isEmpty() || playPageHtml.contains("File not found") || playPageHtml.contains("deleted by the owner")) {
                    playPageHtml = HttpHelper.m6343().m6351("https://easyload.io/f/" + mediaId, (Map<String, String>[]) new Map[0]);
                    if (playPageHtml.isEmpty() || playPageHtml.contains("File not found") || playPageHtml.contains("deleted by the owner")) {
                        subscriber.onCompleted();
                        return;
                    }
                }
                if (Openload.f13005 == null || Openload.f13005.isEmpty()) {
                    js = Openload.f13005 = HttpHelper.m6343().m6351(Openload.this.m16635(), (Map<String, String>[]) new Map[0]);
                } else {
                    js = Openload.f13005;
                }
                if (js.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                String playLinkArrJson = "";
                Duktape duktape = null;
                try {
                    duktape = Duktape.create();
                    duktape.set("Log", Log.class, new Log() {
                        public void d(String str) {
                        }
                    });
                    duktape.set("JavaRegex", JavaRegex.class, new JavaRegex() {
                        public String findAll(String str, String regex, int group) {
                            return new Gson().toJson((Object) Regex.m17803(str, regex, group).get(Math.max(0, group - 1)));
                        }

                        public String findAllWithMode(String str, String regex, int group, int mode) {
                            return new Gson().toJson((Object) Regex.m17804(str, regex, group, mode).get(Math.max(0, group - 1)));
                        }
                    });
                    duktape.set("JavaUrlDecoder", JavaUrlDecoder.class, new JavaUrlDecoder() {
                        public String decode(String s) {
                            try {
                                return URLDecoder.decode(s, "UTF-8");
                            } catch (Exception e) {
                                String res = URLDecoder.decode(s);
                                Logger.m6281((Throwable) e, new boolean[0]);
                                return res;
                            }
                        }
                    });
                    duktape.set("TTVHttp", TTVHttp.class, new TTVHttp() {
                        public String get(String url, String referer) {
                            return HttpHelper.m6343().m6358(url, referer);
                        }

                        public String post(String url, String postData, String referer) {
                            if (referer == null || referer.trim().isEmpty()) {
                                return HttpHelper.m6343().m6360(url, postData, true, (Map<String, String>[]) new Map[0]);
                            }
                            HashMap<String, String> postHeaders = new HashMap<>();
                            postHeaders.put("Referer", referer);
                            return HttpHelper.m6343().m6360(url, postData, true, (Map<String, String>[]) new Map[]{postHeaders});
                        }
                    });
                    duktape.evaluate(js);
                    OpenloadDecoder decoder = (OpenloadDecoder) duktape.get("OpenloadDecoder", OpenloadDecoder.class);
                    if (!decoder.isEnabled()) {
                        subscriber.onCompleted();
                        if (duktape != null) {
                            duktape.close();
                            return;
                        }
                        return;
                    }
                    playLinkArrJson = decoder.decode(playPageHtml);
                    if (duktape != null) {
                        duktape.close();
                    }
                    if (playLinkArrJson.isEmpty()) {
                        subscriber.onCompleted();
                        return;
                    }
                    Iterator<JsonElement> it2 = new JsonParser().parse(playLinkArrJson).getAsJsonArray().iterator();
                    while (it2.hasNext()) {
                        try {
                            String playLink = it2.next().getAsString();
                            String redirectedUrl = HttpHelper.m6343().m6363(playLink, false, (Map<String, String>[]) new Map[0]);
                            if (Openload.this.m16779(redirectedUrl) && !redirectedUrl.contains(".srt?") && !redirectedUrl.contains(".vtt?") && !redirectedUrl.contains(".ass?") && !redirectedUrl.contains(".zip?") && !redirectedUrl.contains(".rar?") && !redirectedUrl.contains(".7z?")) {
                                subscriber.onNext(new ResolveResult(Openload.this.m16639(), playLink, ""));
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                    }
                    subscriber.onCompleted();
                } catch (Throwable th) {
                    if (duktape != null) {
                        duktape.close();
                    }
                    throw th;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16635() {
        if (f13006 == null || f13006.isEmpty()) {
            try {
                f13006 = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Jlc29sdmVyL29sNC5qcw==", 10), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                f13006 = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Jlc29sdmVyL29sNC5qcw==", 10));
            }
        }
        return f13006;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16640() {
        return "HD";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16639() {
        return "Openload";
    }
}
