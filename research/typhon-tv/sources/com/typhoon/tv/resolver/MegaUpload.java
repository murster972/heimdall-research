package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class MegaUpload extends GenericResolver {
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16608() {
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16609() {
        return "MegaUpload";
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m16610() {
        return "(?://|\\.)(vup\\.(?:to|cc|si))/(?:embed-)?([0-9a-zA-Z]+)";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m16611() {
        return "https://vup.to";
    }
}
