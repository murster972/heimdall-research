package com.typhoon.tv.resolver.base;

import android.webkit.MimeTypeMap;
import com.mopub.common.TyphoonApp;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.resolver.AbcVideo;
import com.typhoon.tv.resolver.ClipWatching;
import com.typhoon.tv.resolver.CloudHost;
import com.typhoon.tv.resolver.CloudVideo;
import com.typhoon.tv.resolver.DatafileHost;
import com.typhoon.tv.resolver.DoodWatch;
import com.typhoon.tv.resolver.DropAPK;
import com.typhoon.tv.resolver.EnterVideo;
import com.typhoon.tv.resolver.EuropeUP;
import com.typhoon.tv.resolver.FastPlay;
import com.typhoon.tv.resolver.FileCad;
import com.typhoon.tv.resolver.FileRio;
import com.typhoon.tv.resolver.FileUP;
import com.typhoon.tv.resolver.FilesIM;
import com.typhoon.tv.resolver.GamoVideo;
import com.typhoon.tv.resolver.GoUnlimited;
import com.typhoon.tv.resolver.HxLoad;
import com.typhoon.tv.resolver.JetLoad;
import com.typhoon.tv.resolver.LetsUpload;
import com.typhoon.tv.resolver.MegaUpload;
import com.typhoon.tv.resolver.MixDrop;
import com.typhoon.tv.resolver.MixLoads;
import com.typhoon.tv.resolver.MpFour;
import com.typhoon.tv.resolver.OK;
import com.typhoon.tv.resolver.OnlyStream;
import com.typhoon.tv.resolver.Openload;
import com.typhoon.tv.resolver.Putload;
import com.typhoon.tv.resolver.SamaUp;
import com.typhoon.tv.resolver.SendVid;
import com.typhoon.tv.resolver.SenditCloud;
import com.typhoon.tv.resolver.SevenUP;
import com.typhoon.tv.resolver.SolidFiles;
import com.typhoon.tv.resolver.StreamWire;
import com.typhoon.tv.resolver.SuperVideo;
import com.typhoon.tv.resolver.TheVideo;
import com.typhoon.tv.resolver.TusFiles;
import com.typhoon.tv.resolver.UpStream;
import com.typhoon.tv.resolver.UpToBox;
import com.typhoon.tv.resolver.UqLoad;
import com.typhoon.tv.resolver.UsersCloud;
import com.typhoon.tv.resolver.VK;
import com.typhoon.tv.resolver.VShareEU;
import com.typhoon.tv.resolver.VidDo;
import com.typhoon.tv.resolver.VidFast;
import com.typhoon.tv.resolver.VideYO;
import com.typhoon.tv.resolver.VideoBee;
import com.typhoon.tv.resolver.VideoBin;
import com.typhoon.tv.resolver.VidiaTV;
import com.typhoon.tv.resolver.Vidlox;
import com.typhoon.tv.resolver.Vidoza;
import com.typhoon.tv.resolver.ViduPlayer;
import com.typhoon.tv.resolver.Yandex;
import com.typhoon.tv.resolver.YoudBox;
import com.typhoon.tv.resolver.YourUpload;
import com.typhoon.tv.resolver.debrid.realdebrid.AlfaFile;
import com.typhoon.tv.resolver.debrid.realdebrid.AllTheRest;
import com.typhoon.tv.resolver.debrid.realdebrid.BdUpload;
import com.typhoon.tv.resolver.debrid.realdebrid.ClicknUpload;
import com.typhoon.tv.resolver.debrid.realdebrid.FileFactory;
import com.typhoon.tv.resolver.debrid.realdebrid.FlashX;
import com.typhoon.tv.resolver.debrid.realdebrid.KatFile;
import com.typhoon.tv.resolver.debrid.realdebrid.KeepShare;
import com.typhoon.tv.resolver.debrid.realdebrid.MediaFire;
import com.typhoon.tv.resolver.debrid.realdebrid.NitroFlare;
import com.typhoon.tv.resolver.debrid.realdebrid.Oboom;
import com.typhoon.tv.resolver.debrid.realdebrid.OneFichier;
import com.typhoon.tv.resolver.debrid.realdebrid.RapidGator;
import com.typhoon.tv.resolver.debrid.realdebrid.RgTo;
import com.typhoon.tv.resolver.debrid.realdebrid.RockFile;
import com.typhoon.tv.resolver.debrid.realdebrid.Rotate;
import com.typhoon.tv.resolver.debrid.realdebrid.TurboBit;
import com.typhoon.tv.resolver.debrid.realdebrid.Uploaded;
import com.typhoon.tv.resolver.debrid.realdebrid.Uploadev;
import com.typhoon.tv.resolver.debrid.realdebrid.WipFiles;
import com.typhoon.tv.resolver.debrid.realdebrid.WupFile;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.SourceObservableUtils;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import okhttp3.Response;
import org.apache.oltu.oauth2.common.OAuth;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

public abstract class BaseResolver {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f13041 = {".jpg", ".jpeg", ".gif", ".png", ".js", ".css", ".htm", ".html", ".php", ".srt", ".sub", ".xml", ".swf", ".vtt"};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f13042 = {"[\"']?\\s*file\\s*[\"']?\\s*[:=,]?\\s*[\"']([^\"']+)(?:[^\\}>\\]]+)[\"']?\\s*label\\s*[\"']?\\s*[:=]\\s*[\"']?([^\"',]+)", "[\"']?src[\"']?\\s*:\\s*[\"']?([^\\}\"']+)[\"']?\\s*,\\s*[\"']?height[\"']?\\s*:\\s*['\"]?\\s*(\\d+)\\s*['\"]?", "src:[\"']([^\"']+)[^\\{\\}]+(?<=height:)(\\d+)", "source\\s+src\\s*=\\s*['\"]([^'\"]+)['\"](?:.*?data-res\\s*=\\s*['\"]([^'\"]+))?", "video[^><]+src\\s*=\\s*['\"]([^'\"]+)", "sources\\s*:\\s*\\[\\s*['\"]\\s*(.*?)\\s*['\"],", "sources\\s*:\\s*\\[.*?\\s*,\\s*['\"]\\s*(.*?)\\s*['\"]\\s*,?.*?\\]", "sources\\s*:\\s*\\[\\s*\\{\\s*['\"]?src['\"]?\\s*:\\s*['\"](.*?)['\"]\\s*,\\s*type\\s*:", "[\"']?\\s*(?:file|url)\\s*[\"']?\\s*[:=]\\s*[\"']([^\"']+)", "param\\s+name\\s*=\\s*\"src\"\\s*value\\s*=\\s*\"([^\"]+)"};

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract String m16778();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Observable<ResolveResult> m16783(String str);

    /* renamed from: 龘  reason: contains not printable characters */
    public static Observable<MediaSource> m16774(final MediaSource mediaSource, final boolean fromObs) {
        final String url = mediaSource.getStreamLink();
        if (GoogleVideoHelper.m15955(url)) {
            mediaSource.setQuality(GoogleVideoHelper.m15949(url));
            mediaSource.setNeededToResolve(false);
            mediaSource.setResolved(true);
            return Observable.m7356(mediaSource);
        }
        BaseResolver resolver = m16773(url);
        if (resolver == null) {
            if (fromObs && url.trim().toLowerCase().contains(".m3u8")) {
                return Observable.m7349();
            }
            String extension = MimeTypeMap.getFileExtensionFromUrl(url);
            if (extension == null || extension.isEmpty()) {
                extension = Utils.m6408(url);
            }
            String mimeType = !extension.isEmpty() ? MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension) : null;
            if (mimeType == null || !mimeType.trim().toLowerCase().contains("video")) {
                return Observable.m7349();
            }
            return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void call(Subscriber<? super MediaSource> subscriber) {
                    String quality;
                    Map<String, List<String>> headers;
                    List<String> contentTypes;
                    Response response = HttpHelper.m6343().m6367(url, SourceObservableUtils.m17832(url), (Map<String, String>) null);
                    if (response == null) {
                        subscriber.onCompleted();
                        return;
                    }
                    boolean isValidSource = false;
                    if (response.m7055() != null && (headers = response.m7055().m6933()) != null && (headers.containsKey(OAuth.HeaderType.CONTENT_TYPE) || headers.containsKey("content-type"))) {
                        if (headers.containsKey(OAuth.HeaderType.CONTENT_TYPE)) {
                            contentTypes = headers.get(OAuth.HeaderType.CONTENT_TYPE);
                        } else {
                            contentTypes = headers.get("content-type");
                        }
                        if (contentTypes != null) {
                            Iterator<String> it2 = contentTypes.iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (it2.next().startsWith("video")) {
                                        isValidSource = true;
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                    if (isValidSource) {
                        boolean isGoogle = GoogleVideoHelper.m15955(url);
                        MediaSource retSrc = new MediaSource(mediaSource.getProviderName(), isGoogle ? "GoogleVideo" : "CDN", false);
                        retSrc.setStreamLink(url);
                        if (isGoogle) {
                            quality = GoogleVideoHelper.m15949(url);
                        } else {
                            quality = mediaSource.getQuality().isEmpty() ? "HD" : mediaSource.getQuality();
                        }
                        retSrc.setQuality(quality);
                        retSrc.setPlayHeader(mediaSource.getPlayHeader());
                        retSrc.setNeededToResolve(false);
                        retSrc.setResolved(true);
                        subscriber.onNext(retSrc);
                    }
                    if (response.m7056() != null) {
                        response.m7056().close();
                    }
                    subscriber.onCompleted();
                }
            });
        } else if (!m16776(resolver)) {
            return Observable.m7349();
        } else {
            return resolver.m16783(url).m7396(new Func1<ResolveResult, Observable<MediaSource>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Observable<MediaSource> call(ResolveResult result) {
                    String resolvedLink = result.getResolvedLink();
                    if (fromObs && resolvedLink.trim().toLowerCase().contains(".m3u8")) {
                        return Observable.m7349();
                    }
                    if (resolvedLink.startsWith("//")) {
                        resolvedLink = "http:" + resolvedLink;
                    } else if (resolvedLink.startsWith(":")) {
                        resolvedLink = TyphoonApp.HTTP + resolvedLink;
                    }
                    MediaSource retSrc = new MediaSource(mediaSource.getProviderName(), result.getResolverName(), false);
                    retSrc.setStreamLink(resolvedLink);
                    if ((result.getResolvedQuality() == null || result.getResolvedQuality().isEmpty()) && mediaSource.getQuality() != null && !mediaSource.getQuality().isEmpty()) {
                        retSrc.setQuality(mediaSource.getQuality());
                    } else if (result.getResolvedQuality() == null || result.getResolvedQuality().isEmpty()) {
                        retSrc.setQuality("HQ");
                    } else {
                        retSrc.setQuality(result.getResolvedQuality());
                    }
                    if (result.getPlayHeader() != null) {
                        retSrc.setPlayHeader(new HashMap(result.getPlayHeader()));
                    }
                    retSrc.setResolved(true);
                    retSrc.setDebrid(result.isDebrid());
                    retSrc.setOriginalMediaSource(mediaSource);
                    return Observable.m7356(retSrc);
                }
            });
        }
    }

    /* JADX WARNING: type inference failed for: r6v0, types: [rx.functions.Action1<com.typhoon.tv.model.media.MediaSource>, rx.functions.Action1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static rx.Observable<com.typhoon.tv.model.media.MediaSource> m16775(java.util.ArrayList<com.typhoon.tv.model.media.MediaSource> r5, rx.functions.Action1<com.typhoon.tv.model.media.MediaSource> r6) {
        /*
            r4 = 0
            rx.Observable r0 = rx.Observable.m7355(r5)
            rx.Observable r0 = r0.m7384(r6)
            com.typhoon.tv.resolver.base.BaseResolver$5 r1 = new com.typhoon.tv.resolver.base.BaseResolver$5
            r1.<init>()
            rx.Observable r0 = r0.m7413(r1)
            rx.functions.Func1 r1 = com.typhoon.tv.utils.SourceObservableUtils.m17831((boolean) r4)
            rx.Observable r0 = r0.m7413(r1)
            com.typhoon.tv.resolver.base.BaseResolver$4 r1 = new com.typhoon.tv.resolver.base.BaseResolver$4
            r1.<init>()
            rx.Observable r0 = r0.m7385(r1)
            rx.Observable r0 = r0.m7367()
            com.typhoon.tv.resolver.base.BaseResolver$3 r1 = new com.typhoon.tv.resolver.base.BaseResolver$3
            r1.<init>()
            rx.Observable r0 = r0.m7412((rx.functions.Action1<? super java.lang.Throwable>) r1)
            rx.Observable r1 = rx.Observable.m7349()
            rx.Observable r0 = r0.m7375(r1)
            com.typhoon.tv.model.media.MediaSource r1 = new com.typhoon.tv.model.media.MediaSource
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            r1.<init>(r2, r3, r4)
            rx.Observable r0 = r0.m7381(r1)
            rx.Observable r0 = r0.m7374()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.resolver.base.BaseResolver.m16775(java.util.ArrayList, rx.functions.Action1):rx.Observable");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m16776(BaseResolver resolver) {
        List<String> enabledResolversStrList = null;
        String enabledResolversStr = TVApplication.m6285().getString("pref_enabled_resolvers", (String) null);
        if (enabledResolversStr != null) {
            enabledResolversStrList = enabledResolversStr.contains(",") ? Arrays.asList(enabledResolversStr.split(",")) : Collections.singletonList(enabledResolversStr);
        }
        return enabledResolversStrList == null || enabledResolversStrList.contains(resolver.m16778());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static BaseResolver m16773(String url) {
        String url2 = url.toLowerCase();
        boolean hdOnly = TVApplication.m6285().getBoolean("pref_show_hd_sources_only", false);
        if (RealDebridCredentialsHelper.m15838().isValid()) {
            if (url2.contains("hexupload")) {
                return new Oboom();
            }
            if (url2.contains("nitroflare.")) {
                return new NitroFlare();
            }
            if (url2.contains("filefactory.")) {
                return new FileFactory();
            }
            if (url2.contains("real-debrid.")) {
                return new AlfaFile();
            }
            if (url2.contains("earn4files")) {
                return new TurboBit();
            }
            if (url2.contains("ddownload") || url2.contains("ddl.to") || url2.contains("usersdrive")) {
                return new OneFichier();
            }
            if (url2.contains("uploaded.") || url2.contains("ul.to")) {
                return new Uploaded();
            }
            if (url2.contains("depositfiles.") || url2.contains("mega.nz") || url2.contains("sendspace") || url2.contains("uloz") || url2.contains("mediafire") || url2.contains(".free.") || url2.contains("4shared")) {
                return new AllTheRest();
            }
            if (url2.contains("rapidgator.") || url2.contains("rg.to")) {
                return new RapidGator();
            }
            if (url2.contains("clicknupload.")) {
                return new ClicknUpload();
            }
            if (url2.contains("dailyuploads.")) {
                return new RockFile();
            }
            if (url2.contains("uploadgig.")) {
                return new FlashX();
            }
            if (url2.contains("turbobit.")) {
                return new MediaFire();
            }
            if (url2.contains("speed-down.")) {
                return new WupFile();
            }
            if (url2.contains("zippyshare.")) {
                return new WipFiles();
            }
            if (url2.contains("uploadev.")) {
                return new Uploadev();
            }
            if (url2.contains("fichier")) {
                return new RgTo();
            }
            if (url2.contains("katfile.")) {
                return new KatFile();
            }
            if (url2.contains("daofile.")) {
                return new Rotate();
            }
            if (url2.contains("bdupload.")) {
                return new BdUpload();
            }
            if (url2.contains("k2s.") || url2.contains("keep2share")) {
                return new KeepShare();
            }
            if (url2.contains("easyload.")) {
                return new Openload();
            }
        }
        if (hdOnly) {
            if (url2.contains("vk.com")) {
                return new VK();
            }
            if (url2.contains("ok.ru") || url2.contains("odnoklassniki.")) {
                return new OK();
            }
            if (com.typhoon.tv.TyphoonApp.f5861 && url2.contains("easyload.")) {
                return new Openload();
            }
            if (url2.contains("hxload.")) {
                return new HxLoad();
            }
            if (url2.contains("onlystream.")) {
                return new OnlyStream();
            }
            if (url2.contains("vidia.")) {
                return new VidiaTV();
            }
            if (url2.contains("videobin.")) {
                return new VideoBin();
            }
            if (url2.contains("tusfiles.")) {
                return new TusFiles();
            }
            if (url2.contains("files.im")) {
                return new FilesIM();
            }
            if (url2.contains("upstream.")) {
                return new UpStream();
            }
            if (url2.contains("mixdrop.")) {
                return new MixDrop();
            }
            if (url2.contains("filecad.")) {
                return new FileCad();
            }
            if (url2.contains("uptobox.") || url2.contains("uptostream.")) {
                return new UpToBox();
            }
            if (url2.contains("youdbox.")) {
                return new YoudBox();
            }
            if (url2.contains("fastplay.")) {
                return new FastPlay();
            }
            if (url2.contains("cloudvideo.")) {
                return new CloudVideo();
            }
            if (url2.contains("vidoza.")) {
                return new Vidoza();
            }
            if (url2.contains("abcvideo.")) {
                return new AbcVideo();
            }
            if (url2.contains("thevideo.me") || url2.contains("vev.")) {
                return new TheVideo();
            }
            if (url2.contains("samaup.")) {
                return new SamaUp();
            }
            if (url2.contains("filerio.")) {
                return new FileRio();
            }
            if (url2.contains("yandex.")) {
                return new Yandex();
            }
            if (url2.contains("letsupload.")) {
                return new LetsUpload();
            }
            if (url2.contains("mixloads.")) {
                return new MixLoads();
            }
            if (url2.contains("cloudhost.")) {
                return new CloudHost();
            }
            if (url2.contains("yourupload.")) {
                return new YourUpload();
            }
        }
        if (url2.contains("vk.com")) {
            return new VK();
        }
        if (url2.contains("hxload.")) {
            return new HxLoad();
        }
        if (url2.contains("dropapk.")) {
            return new DropAPK();
        }
        if (url2.contains("ok.ru") || url2.contains("odnoklassniki.")) {
            return new OK();
        }
        if (url2.contains("thevideobee.")) {
            return new VideoBee();
        }
        if (url2.contains("videobin.")) {
            return new VideoBin();
        }
        if (url2.contains("vidia.")) {
            return new VidiaTV();
        }
        if (url2.contains("doodstream.") || url2.contains("dood.")) {
            return new DoodWatch();
        }
        if (url2.contains("europeup.")) {
            return new EuropeUP();
        }
        if (url2.contains("viduplayer.")) {
            return new ViduPlayer();
        }
        if (url2.contains("jetload.")) {
            return new JetLoad();
        }
        if (url2.contains("eplayvid.")) {
            return new EnterVideo();
        }
        if (url2.contains("onlystream.")) {
            return new OnlyStream();
        }
        if (url2.contains("sendvid.")) {
            return new SendVid();
        }
        if (url2.contains("vidtodo.") || url2.contains("vidtodu.") || url2.contains("vidtudu.") || url2.contains("allvids.")) {
            return new VidDo();
        }
        if (url2.contains("vup.")) {
            return new MegaUpload();
        }
        if (url2.contains("mp4upload.")) {
            return new MpFour();
        }
        if (url2.contains("file-up.org")) {
            return new FileUP();
        }
        if (url2.contains("vshare.")) {
            return new VShareEU();
        }
        if (com.typhoon.tv.TyphoonApp.f5861 && url2.contains("easyload.")) {
            return new Openload();
        }
        if (url2.contains("solidfiles.") || url2.contains("sdfiles.")) {
            return new SolidFiles();
        }
        if (url2.contains("hitfile") || url2.contains("datafilehost")) {
            return new DatafileHost();
        }
        if (url2.contains("gounlimited.") || url2.contains("tazmovies.")) {
            return new GoUnlimited();
        }
        if (url2.contains("sendit")) {
            return new SenditCloud();
        }
        if (url2.contains("tusfiles.")) {
            return new TusFiles();
        }
        if (url2.contains("files.im")) {
            return new FilesIM();
        }
        if (url2.contains("upstream.")) {
            return new UpStream();
        }
        if (url2.contains("clipwatching.")) {
            return new ClipWatching();
        }
        if (url2.contains("userscloud.")) {
            return new UsersCloud();
        }
        if (url2.contains("mixdrop.")) {
            return new MixDrop();
        }
        if (url2.contains("filecad.")) {
            return new FileCad();
        }
        if (url2.contains("uptobox.") || url2.contains("uptostream.")) {
            return new UpToBox();
        }
        if (url2.contains("youdbox.")) {
            return new YoudBox();
        }
        if (url2.contains("fastplay.")) {
            return new FastPlay();
        }
        if (url2.contains("cloudvideo.")) {
            return new CloudVideo();
        }
        if (url2.contains("abcvideo.")) {
            return new AbcVideo();
        }
        if (url2.contains("putload.")) {
            return new Putload();
        }
        if (url2.contains("vidoza.")) {
            return new Vidoza();
        }
        if (url2.contains("thevideo.me") || url2.contains("vev.")) {
            return new TheVideo();
        }
        if (url2.contains("samaup.")) {
            return new SamaUp();
        }
        if (url2.contains("vidlox.")) {
            return new Vidlox();
        }
        if (url2.contains("vidfast.")) {
            return new VidFast();
        }
        if (url2.contains("videyo.")) {
            return new VideYO();
        }
        if (url2.contains("7-up.")) {
            return new SevenUP();
        }
        if (url2.contains("filerio.")) {
            return new FileRio();
        }
        if (url2.contains("yandex.")) {
            return new Yandex();
        }
        if (url2.contains("letsupload.")) {
            return new LetsUpload();
        }
        if (url2.contains("gcloud.")) {
            return new UqLoad();
        }
        if (url2.contains("mixloads.")) {
            return new MixLoads();
        }
        if (url2.contains("cloudhost.")) {
            return new CloudHost();
        }
        if (url2.contains("yourupload.")) {
            return new YourUpload();
        }
        if (url2.contains("supervideo.")) {
            return new SuperVideo();
        }
        if (url2.contains("streamwire.")) {
            return new StreamWire();
        }
        if (url2.contains("gamovideo.")) {
            return new GamoVideo();
        }
        return null;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static String[] m16769() {
        return m16777(TVApplication.m6285().getBoolean("pref_show_hd_sources_only", false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] m16777(boolean hdOnly) {
        if (hdOnly) {
            return new String[]{"tusfiles", "uptobox", "uptostream", "upstream", "onlystream", "easyload", "hxload", "hxfile", "files.im", "yourupload", "cloudhost", "mixloads", "mixdrop", "vk.com", "ok.ru", "odnoklassniki", "filecad", ".24hd.", "youdbox", "fastplay", "letsupload", "cloudvideo", "vidoza", "abcvideo", "thevideo.me", "vev.", "samaup", "vidia", "videobin", "filerio", "yandex"};
        }
        return new String[]{"tusfiles", "uptobox", "uptostream", "upstream", "onlystream", "easyload", "hxload", "hxfile", "gamovideo", "dropapk", "streamwire", "supervideo", "vk.com", "ok.ru", "odnoklassniki", "7-up", "doodstream", "dood.watch", "files.im", "jetload", "powvldeo", "yourupload", "cloudhost", "eplayvid", "mixloads", "viduplayer", "sendvid", "vup", "vidtodo", "vidtodu", "vidtudu", "vidtodop", "mp4upload", "bgupload.", "bcupload", "file-up.org", "filerio", "vshare", "letsupload", "hitfile", "datafilehost", "gounlimited", "tazmovies", "sendit", "videobin", "thevideobee", "videyo", "vidia", "vidfast", "clipwatching", "mixdrop", "filecad", "solidfiles", "youdbox", "fastplay", "vidlox", "cloudvideo", "userscloud", "abcvideo", "putload", "vidoza", "thevideo.me", "vev.", "samaup", "sdfiles.", "gcloud", "europeup", "yandex"};
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static String[] m16770() {
        return new String[]{"uploaded", "ul.to", "vk.com", "rapidgator", "rg.to", "uptobox", "filerio", "dailyuploads", "clicknupload", "hexupload", "nitroflare", "filefactory", "real-debrid", "turbobit", "userscloud", "ddownload", "ddl.to", "usersdrive", "uploadgig", "tusfiles", "uploadev", "vidlox", "solidfiles", "fichier", "earn4files", "zippyshare", "vidoza", "speed-down", "hitfile", "onlystream", "depositfiles", "mega.nz", "sendspace", "uloz", "mediafire", ".free.", "4shared", "katfile", "daofile", "bdupload", "k2s", "keep2share"};
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static HashMap<String, String> m16771(String html, String formId) {
        String pattern;
        HashMap<String, String> map = new HashMap<>();
        if (formId == null || formId.isEmpty()) {
            pattern = "(?s)<form[^>]*>(.*?)</form>";
        } else {
            pattern = "(?s)<form [^>]*(?:id|name)\\s*=\\s*['\"]?" + formId + "['\"]?[^>]*>(.*?)</form>";
        }
        Iterator it2 = Regex.m17804(html, pattern, 1, 34).get(0).iterator();
        while (it2.hasNext()) {
            Iterator it3 = Regex.m17803((String) it2.next(), "<input[^>]*type=['\"]?hidden['\"]?[^>]*>", 0).get(0).iterator();
            while (it3.hasNext()) {
                String fileId = (String) it3.next();
                String match = Regex.m17800(fileId, "name\\s*=\\s*['\"]([^'\"]+)", 1);
                String match1 = Regex.m17800(fileId, "value\\s*=\\s*['\"]([^'\"]*)", 1);
                if (!match.isEmpty() && !match1.isEmpty()) {
                    map.put(match, match1);
                }
            }
        }
        return map;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<ResolveResult> m16781(String playPageUrl, String htmlPage, boolean isNeededToAppendHeaders, HashMap<String, String> playHeaders, String[]... customBlacklist) {
        ArrayList<String> htmlPages = new ArrayList<>();
        htmlPages.add(htmlPage);
        String[][] strArr = new String[1][];
        strArr[0] = (customBlacklist == null || customBlacklist.length <= 0) ? null : customBlacklist[0];
        return m16782(playPageUrl, htmlPages, isNeededToAppendHeaders, playHeaders, strArr);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0095, code lost:
        if (m16772(r25) != false) goto L_0x0097;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.typhoon.tv.model.ResolveResult> m16782(java.lang.String r25, java.util.ArrayList<java.lang.String> r26, boolean r27, java.util.HashMap<java.lang.String, java.lang.String> r28, java.lang.String[]... r29) {
        /*
            r24 = this;
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            java.util.ArrayList r15 = new java.util.ArrayList
            r15.<init>()
            java.lang.String[] r3 = f13041
            if (r29 == 0) goto L_0x0030
            r0 = r29
            int r0 = r0.length
            r17 = r0
            if (r17 <= 0) goto L_0x0030
            r17 = 0
            r17 = r29[r17]
            if (r17 == 0) goto L_0x0030
            r17 = 0
            r17 = r29[r17]
            r0 = r17
            int r0 = r0.length
            r17 = r0
            if (r17 <= 0) goto L_0x0030
            java.lang.String[] r17 = f13041
            r18 = 0
            r18 = r29[r18]
            java.lang.String[] r3 = com.typhoon.tv.utils.Utils.m6428((java.lang.String[]) r17, (java.lang.String[]) r18)
        L_0x0030:
            java.util.Iterator r19 = r26.iterator()
        L_0x0034:
            boolean r17 = r19.hasNext()
            if (r17 == 0) goto L_0x01f4
            java.lang.Object r9 = r19.next()
            java.lang.String r9 = (java.lang.String) r9
            java.lang.String[] r20 = f13042
            r0 = r20
            int r0 = r0.length
            r21 = r0
            r17 = 0
            r18 = r17
        L_0x004b:
            r0 = r18
            r1 = r21
            if (r0 >= r1) goto L_0x0034
            r10 = r20[r18]
            java.lang.String r17 = "\\/"
            java.lang.String r22 = "/"
            r0 = r17
            r1 = r22
            java.lang.String r9 = r9.replace(r0, r1)     // Catch:{ Exception -> 0x0171 }
            r17 = 32
            r0 = r17
            java.util.regex.Pattern r17 = java.util.regex.Pattern.compile(r10, r0)     // Catch:{ Exception -> 0x0171 }
            r0 = r17
            java.util.regex.Matcher r7 = r0.matcher(r9)     // Catch:{ Exception -> 0x0171 }
            java.lang.String r11 = ""
            java.lang.String r12 = ""
        L_0x0075:
            boolean r17 = r7.find()     // Catch:{ Exception -> 0x0171 }
            if (r17 == 0) goto L_0x017f
            int r17 = r7.groupCount()     // Catch:{ Exception -> 0x01b3 }
            r22 = 1
            r0 = r17
            r1 = r22
            if (r0 != r1) goto L_0x0185
            r17 = 1
            r0 = r17
            java.lang.String r11 = r7.group(r0)     // Catch:{ Exception -> 0x01b3 }
        L_0x008f:
            if (r12 == 0) goto L_0x0097
            boolean r17 = r24.m16772(r25)     // Catch:{ Exception -> 0x0171 }
            if (r17 == 0) goto L_0x009a
        L_0x0097:
            java.lang.String r12 = ""
        L_0x009a:
            boolean r17 = r11.isEmpty()     // Catch:{ Exception -> 0x0171 }
            if (r17 != 0) goto L_0x0075
            java.lang.String r17 = "\\/"
            java.lang.String r22 = "/"
            r0 = r17
            r1 = r22
            java.lang.String r17 = r11.replace(r0, r1)     // Catch:{ Exception -> 0x0171 }
            java.lang.String r22 = "\\\\"
            java.lang.String r23 = ""
            r0 = r17
            r1 = r22
            r2 = r23
            java.lang.String r17 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0171 }
            java.lang.String r22 = "&amp;"
            java.lang.String r23 = "&"
            r0 = r17
            r1 = r22
            r2 = r23
            java.lang.String r11 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0171 }
            java.lang.String r17 = "/"
            r0 = r17
            boolean r17 = r11.contains(r0)     // Catch:{ Exception -> 0x0171 }
            if (r17 == 0) goto L_0x0113
            java.lang.String r17 = "/"
            r0 = r17
            java.lang.String[] r16 = r11.split(r0)     // Catch:{ Exception -> 0x0171 }
            r0 = r16
            int r0 = r0.length     // Catch:{ Exception -> 0x0171 }
            r17 = r0
            r22 = 2
            r0 = r17
            r1 = r22
            if (r0 < r1) goto L_0x0113
            r0 = r16
            int r0 = r0.length     // Catch:{ Exception -> 0x0171 }
            r17 = r0
            int r17 = r17 + -1
            r17 = r16[r17]     // Catch:{ Exception -> 0x0171 }
            java.lang.String r5 = r17.toLowerCase()     // Catch:{ Exception -> 0x0171 }
            r8 = 0
            int r0 = r3.length     // Catch:{ Exception -> 0x0171 }
            r22 = r0
            r17 = 0
        L_0x0102:
            r0 = r17
            r1 = r22
            if (r0 >= r1) goto L_0x0111
            r6 = r3[r17]     // Catch:{ Exception -> 0x0171 }
            boolean r23 = r5.contains(r6)     // Catch:{ Exception -> 0x0171 }
            if (r23 == 0) goto L_0x01c9
            r8 = 1
        L_0x0111:
            if (r8 != 0) goto L_0x0075
        L_0x0113:
            java.lang.String r17 = "//"
            r0 = r17
            boolean r17 = r11.startsWith(r0)     // Catch:{ Exception -> 0x0171 }
            if (r17 == 0) goto L_0x01cd
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0171 }
            r17.<init>()     // Catch:{ Exception -> 0x0171 }
            java.lang.String r22 = "http:"
            r0 = r17
            r1 = r22
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ Exception -> 0x0171 }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r11)     // Catch:{ Exception -> 0x0171 }
            java.lang.String r11 = r17.toString()     // Catch:{ Exception -> 0x0171 }
        L_0x0138:
            java.lang.String r17 = "://"
            r0 = r17
            boolean r17 = r11.contains(r0)     // Catch:{ Exception -> 0x0171 }
            if (r17 == 0) goto L_0x0075
            java.lang.String r17 = " "
            java.lang.String r22 = "%20"
            r0 = r17
            r1 = r22
            java.lang.String r11 = r11.replace(r0, r1)     // Catch:{ Exception -> 0x0171 }
            boolean r17 = r15.contains(r11)     // Catch:{ Exception -> 0x0171 }
            if (r17 != 0) goto L_0x0075
            r15.add(r11)     // Catch:{ Exception -> 0x0171 }
            com.typhoon.tv.model.ResolveResult r14 = new com.typhoon.tv.model.ResolveResult     // Catch:{ Exception -> 0x0171 }
            java.lang.String r17 = r24.m16778()     // Catch:{ Exception -> 0x0171 }
            r0 = r17
            r14.<init>((java.lang.String) r0, (java.lang.String) r11, (java.lang.String) r12)     // Catch:{ Exception -> 0x0171 }
            if (r27 == 0) goto L_0x016c
            r0 = r28
            r14.setPlayHeader(r0)     // Catch:{ Exception -> 0x0171 }
        L_0x016c:
            r13.add(r14)     // Catch:{ Exception -> 0x0171 }
            goto L_0x0075
        L_0x0171:
            r4 = move-exception
            r17 = 0
            r0 = r17
            boolean[] r0 = new boolean[r0]
            r17 = r0
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r0)
        L_0x017f:
            int r17 = r18 + 1
            r18 = r17
            goto L_0x004b
        L_0x0185:
            int r17 = r7.groupCount()     // Catch:{ Exception -> 0x01b3 }
            r22 = 2
            r0 = r17
            r1 = r22
            if (r0 != r1) goto L_0x008f
            r17 = 1
            r0 = r17
            java.lang.String r11 = r7.group(r0)     // Catch:{ Exception -> 0x01b3 }
            r17 = 2
            r0 = r17
            java.lang.String r12 = r7.group(r0)     // Catch:{ Exception -> 0x01a3 }
            goto L_0x008f
        L_0x01a3:
            r4 = move-exception
            r17 = 0
            r0 = r17
            boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x01b3 }
            r17 = r0
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r0)     // Catch:{ Exception -> 0x01b3 }
            goto L_0x008f
        L_0x01b3:
            r4 = move-exception
            r17 = 1
            r0 = r17
            boolean[] r0 = new boolean[r0]     // Catch:{ Exception -> 0x0171 }
            r17 = r0
            r22 = 0
            r23 = 1
            r17[r22] = r23     // Catch:{ Exception -> 0x0171 }
            r0 = r17
            com.typhoon.tv.Logger.m6281((java.lang.Throwable) r4, (boolean[]) r0)     // Catch:{ Exception -> 0x0171 }
            goto L_0x008f
        L_0x01c9:
            int r17 = r17 + 1
            goto L_0x0102
        L_0x01cd:
            java.lang.String r17 = ":"
            r0 = r17
            boolean r17 = r11.startsWith(r0)     // Catch:{ Exception -> 0x0171 }
            if (r17 == 0) goto L_0x0138
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0171 }
            r17.<init>()     // Catch:{ Exception -> 0x0171 }
            java.lang.String r22 = "http"
            r0 = r17
            r1 = r22
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ Exception -> 0x0171 }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r11)     // Catch:{ Exception -> 0x0171 }
            java.lang.String r11 = r17.toString()     // Catch:{ Exception -> 0x0171 }
            goto L_0x0138
        L_0x01f4:
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.typhoon.tv.resolver.base.BaseResolver.m16782(java.lang.String, java.util.ArrayList, boolean, java.util.HashMap, java.lang.String[][]):java.util.ArrayList");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m16772(String playPageUrl) {
        if (playPageUrl.trim().toLowerCase().contains("vidoza.")) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m16779(String url) {
        Map<String, List<String>> headers;
        List<String> lengthValues;
        String lengthStr;
        boolean isValid = false;
        HashMap<String, String> checkIsValidHeaders = new HashMap<>();
        checkIsValidHeaders.put("Range", "bytes=0-1");
        Response response = HttpHelper.m6343().m6367(url, false, (Map<String, String>) checkIsValidHeaders);
        if (response == null) {
            return false;
        }
        if (response.m7056() != null) {
            response.m7056().close();
        }
        try {
            if (!(response.m7055() == null || (headers = response.m7055().m6933()) == null || ((!checkIsValidHeaders.containsKey("Range") && !checkIsValidHeaders.containsKey("range")) || (!headers.containsKey("Content-Range") && !headers.containsKey("content-range"))))) {
                if (headers.containsKey("Content-Range")) {
                    lengthValues = headers.get("Content-Range");
                } else {
                    lengthValues = headers.get("content-range");
                }
                if (lengthValues != null && lengthValues.size() > 0 && (lengthStr = lengthValues.get(0)) != null && !lengthStr.isEmpty()) {
                    String[] splittedContentRangeValue = null;
                    if (lengthStr.contains(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                        splittedContentRangeValue = lengthStr.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
                    }
                    if (splittedContentRangeValue != null && splittedContentRangeValue.length == 2) {
                        lengthStr = splittedContentRangeValue[1].trim();
                    }
                    long contentLength = -1;
                    if (Utils.m6426(lengthStr)) {
                        contentLength = Long.valueOf(lengthStr).longValue();
                    }
                    isValid = contentLength >= 52428800;
                }
            }
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        boolean z = isValid;
        return isValid;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16780() {
        return "HQ";
    }
}
