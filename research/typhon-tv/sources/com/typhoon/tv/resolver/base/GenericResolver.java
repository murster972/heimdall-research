package com.typhoon.tv.resolver.base;

import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.js.JsUnpacker;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.utils.Regex;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;

public abstract class GenericResolver extends BaseResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public abstract boolean m16791();

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public abstract String m16792();

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract String m16793();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16795(final String url) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String baseUrl = GenericResolver.this.m16793();
                String urlPattern = GenericResolver.this.m16792();
                String mediaId = Regex.m17800(url, urlPattern, 2);
                if (mediaId.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                if (baseUrl.isEmpty()) {
                    baseUrl = "http://" + Regex.m17800(url, urlPattern, 1);
                }
                String playPageUrl = GenericResolver.this.m16794(baseUrl, mediaId);
                ArrayList<String> htmlPages = new ArrayList<>();
                String playPageHtml = HttpHelper.m6343().m6351(playPageUrl, (Map<String, String>[]) new Map[0]);
                htmlPages.add(playPageHtml);
                if (JsUnpacker.m16026(playPageHtml)) {
                    htmlPages.addAll(JsUnpacker.m16022(playPageHtml));
                }
                HashMap<String, String> playHeaders = null;
                if (GenericResolver.this.m16791()) {
                    playHeaders = new HashMap<>();
                    playHeaders.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                    playHeaders.put("Referer", playPageUrl);
                    playHeaders.put("Cookie", HttpHelper.m6343().m6349(playPageUrl));
                }
                Iterator<ResolveResult> it2 = GenericResolver.this.m16782(playPageUrl, htmlPages, GenericResolver.this.m16791(), playHeaders, GenericResolver.this.m16790()).iterator();
                while (it2.hasNext()) {
                    ResolveResult result = it2.next();
                    if (result.getResolvedQuality() != null && result.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                        result.setResolvedQuality("HQ");
                    }
                    subscriber.onNext(result);
                }
                subscriber.onCompleted();
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16794(String baseUrl, String mediaId) {
        return baseUrl + "/embed-" + mediaId + ".html";
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public String[] m16790() {
        return null;
    }
}
