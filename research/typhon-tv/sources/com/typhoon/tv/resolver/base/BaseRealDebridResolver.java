package com.typhoon.tv.resolver.base;

import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.model.ResolveResult;
import java.util.List;
import rx.Observable;
import rx.Subscriber;

public abstract class BaseRealDebridResolver extends BaseResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16767(final String url) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                if (!RealDebridCredentialsHelper.m15838().isValid()) {
                    subscriber.onCompleted();
                    return;
                }
                List<ResolveResult> rdLinks = RealDebridUserApi.m15845().m15850(url, BaseRealDebridResolver.this.m16778());
                if (rdLinks == null || rdLinks.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                for (ResolveResult resolveResult : rdLinks) {
                    subscriber.onNext(resolveResult);
                }
                subscriber.onCompleted();
            }
        });
    }
}
