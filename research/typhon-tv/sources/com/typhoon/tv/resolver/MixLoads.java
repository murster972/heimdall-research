package com.typhoon.tv.resolver;

import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.js.JsUnpacker;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;

public class MixLoads extends BaseResolver {
    /* renamed from: 靐  reason: contains not printable characters */
    public String m16617() {
        return "MixLoads";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16618(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                List<ResolveResult> r8;
                String r6 = Regex.m17800(str, "(?://|\\.)(mixloads\\.com)/(?:embed-)?([0-9a-zA-Z]+)", 2);
                if (r6.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                if (RealDebridCredentialsHelper.m15838().isValid() && (r8 = RealDebridUserApi.m15845().m15850(str, MixLoads.this.m16617())) != null) {
                    for (ResolveResult onNext : r8) {
                        subscriber.onNext(onNext);
                    }
                }
                String str = "https://mixloads.com/embed-" + r6 + ".html";
                ArrayList arrayList = new ArrayList();
                String r7 = HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[0]);
                arrayList.add(r7);
                if (JsUnpacker.m16026(r7)) {
                    arrayList.addAll(JsUnpacker.m16022(r7));
                }
                Iterator it2 = MixLoads.this.m16782(str, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
                while (it2.hasNext()) {
                    ResolveResult resolveResult = (ResolveResult) it2.next();
                    if (resolveResult.getResolvedQuality() != null && resolveResult.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                        resolveResult.setResolvedQuality("HQ");
                    }
                    subscriber.onNext(resolveResult);
                }
                subscriber.onCompleted();
            }
        });
    }
}
