package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class VidiaTV extends GenericResolver {
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16737() {
        return "https://vidia.tv";
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m16736() {
        return "(?://|\\.)(vidia\\.tv)/(?:embed-)?([0-9a-zA-Z-]+)";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16734() {
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16735() {
        return "VidiaTV";
    }
}
