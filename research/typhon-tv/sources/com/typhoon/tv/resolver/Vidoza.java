package com.typhoon.tv.resolver;

import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.js.JsUnpacker;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;

public class Vidoza extends BaseResolver {
    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m16742() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public String[] m16743() {
        return new String[]{"sub_id", "empty.srt"};
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16744() {
        return "Vidoza";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16745() {
        return "(?://|\\.)(vidoza\\.(?:net|org))/(?:embed-)?([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16746() {
        return "https://vidoza.org";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16747() {
        return "HD";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m16748(String str, String str2) {
        return str + "/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16749(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                List<ResolveResult> r10;
                String r7 = Vidoza.this.m16746();
                String r14 = Vidoza.this.m16745();
                String r8 = Regex.m17800(str, r14, 2);
                if (r8.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                if (r7.isEmpty()) {
                    r7 = "http://" + Regex.m17800(str, r14, 1);
                }
                if (RealDebridCredentialsHelper.m15838().isValid() && (r10 = RealDebridUserApi.m15845().m15850(str, Vidoza.this.m16744())) != null) {
                    for (ResolveResult onNext : r10) {
                        subscriber.onNext(onNext);
                    }
                }
                String r2 = Vidoza.this.m16748(r7, r8);
                ArrayList arrayList = new ArrayList();
                String r9 = HttpHelper.m6343().m6351(r2, (Map<String, String>[]) new Map[0]);
                arrayList.add(r9);
                if (JsUnpacker.m16026(r9)) {
                    arrayList.addAll(JsUnpacker.m16022(r9));
                }
                HashMap hashMap = null;
                if (Vidoza.this.m16742()) {
                    hashMap = new HashMap();
                    hashMap.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                    hashMap.put("Referer", r2);
                    hashMap.put("Cookie", HttpHelper.m6343().m6349(r2));
                }
                Iterator it2 = Vidoza.this.m16782(r2, (ArrayList<String>) arrayList, Vidoza.this.m16742(), (HashMap<String, String>) hashMap, Vidoza.this.m16743()).iterator();
                while (it2.hasNext()) {
                    ResolveResult resolveResult = (ResolveResult) it2.next();
                    if (resolveResult.getResolvedQuality() != null && resolveResult.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                        resolveResult.setResolvedQuality("HQ");
                    }
                    subscriber.onNext(resolveResult);
                }
                subscriber.onCompleted();
            }
        });
    }
}
