package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class ViduPlayer extends GenericResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16754() {
        return "https://viduplayer.com";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16753() {
        return "(?://|\\.)(viduplayer\\.com)/(?:embed-|hls)?([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16751() {
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16752() {
        return "ViduPlayer";
    }
}
