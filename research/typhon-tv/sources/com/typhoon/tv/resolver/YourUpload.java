package com.typhoon.tv.resolver;

import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.js.JsUnpacker;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import rx.Observable;
import rx.Subscriber;

public class YourUpload extends BaseResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16765(final String url) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String mediaId = Regex.m17800(url, "(?://|\\.)(yourupload\\.com)/(?:watch|embed)?/?([0-9A-Za-z]+)", 2);
                if (mediaId.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                String playPageUrl = "https://www.yourupload.com/embed/" + mediaId;
                ArrayList<String> htmlPages = new ArrayList<>();
                String playPageHtml = HttpHelper.m6343().m6351(playPageUrl, (Map<String, String>[]) new Map[0]);
                htmlPages.add(playPageHtml);
                if (JsUnpacker.m16026(playPageHtml)) {
                    htmlPages.addAll(JsUnpacker.m16022(playPageHtml));
                }
                HashMap<String, String> playHeaders = new HashMap<>();
                playHeaders.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                playHeaders.put("Referer", playPageUrl);
                playHeaders.put("Cookie", HttpHelper.m6343().m6349(playPageUrl));
                Iterator<String> it2 = htmlPages.iterator();
                while (it2.hasNext()) {
                    Iterator it3 = Regex.m17804(it2.next(), "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(0).iterator();
                    while (it3.hasNext()) {
                        String streamUrl = (String) it3.next();
                        if (!streamUrl.isEmpty() && !streamUrl.endsWith(".srt") && !streamUrl.endsWith(".vtt") && !streamUrl.endsWith(".png") && !streamUrl.endsWith(".jpg")) {
                            ResolveResult resolveResult = new ResolveResult(YourUpload.this.m16763(), streamUrl, "HD");
                            resolveResult.setPlayHeader(playHeaders);
                            subscriber.onNext(resolveResult);
                        }
                    }
                }
                subscriber.onCompleted();
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16764() {
        return "HD";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16763() {
        return "YourUpload";
    }
}
