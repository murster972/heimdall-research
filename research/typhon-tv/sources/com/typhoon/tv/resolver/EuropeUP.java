package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class EuropeUP extends GenericResolver {
    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m16565() {
        return "https://www.europeup.com";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m16564() {
        return "(?://|\\.)(europeup\\.com)/(?:embed-)?([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16562() {
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16563() {
        return "EuropeUP";
    }
}
