package com.typhoon.tv.resolver;

import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import rx.Observable;
import rx.Subscriber;

public class VK extends BaseResolver {
    /* renamed from: 靐  reason: contains not printable characters */
    public String m16706() {
        return "VK";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16707() {
        return "HD";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16708(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                List<ResolveResult> r15;
                if (RealDebridCredentialsHelper.m15838().isValid() && (r15 = RealDebridUserApi.m15845().m15850(str, VK.this.m16706())) != null) {
                    for (ResolveResult onNext : r15) {
                        subscriber.onNext(onNext);
                    }
                }
                String r10 = Regex.m17800(str, "(?://|\\.)(vk\\.com)/(?:video_ext\\.php\\?|video)(.+)", 2);
                if (r10.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                String str = "";
                String str2 = "";
                try {
                    Map<String, String> r14 = Utils.m6418(new URL("https://vk.com/video?=" + r10));
                    str = r14.get("oid");
                    str2 = r14.get("id");
                } catch (MalformedURLException e) {
                }
                if (str == null || str.isEmpty()) {
                    str = Regex.m17800(str, "(.*)_(.*)", 1);
                }
                if (str2 == null || str2.isEmpty()) {
                    str2 = Regex.m17800(str, "(.*)_(.*)", 2);
                }
                if (str == null || str.isEmpty() || str2 == null || str2.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                String replaceAll = HttpHelper.m6343().m6351("https://vk.com/video?act=show_inline&al=1&video=" + str + EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR + str2, (Map<String, String>[]) new Map[0]).replaceAll("[^\\x00-\\x7F]+", StringUtils.SPACE);
                ArrayList<ArrayList<String>> r4 = Regex.m17805(replaceAll, "(\\d+)x\\d~+.+?(http.+?\\.~m3u8.+?)n", 2, true);
                ArrayList arrayList = r4.get(0);
                ArrayList arrayList2 = r4.get(1);
                if (arrayList.isEmpty() || arrayList2.isEmpty()) {
                    ArrayList<ArrayList<String>> r42 = Regex.m17805(replaceAll, "\"url(\\d+)\"\\s*:\\s*\"(.+?)\"", 2, true);
                    arrayList = r42.get(0);
                    arrayList2 = r42.get(1);
                }
                if (arrayList.isEmpty() || arrayList2.isEmpty()) {
                    Iterator it2 = Regex.m17805(replaceAll, "src=\"([^\"]+)", 1, true).get(0).iterator();
                    while (it2.hasNext()) {
                        String str3 = (String) it2.next();
                        String r13 = Regex.m17800(str3, "\\.(\\d{3,4})\\.", 1);
                        String r21 = VK.this.m16706();
                        if (r13.isEmpty()) {
                            r13 = "HD";
                        }
                        subscriber.onNext(new ResolveResult(r21, str3, r13));
                    }
                } else {
                    for (int i = 0; i < arrayList.size(); i++) {
                        String replace = ((String) arrayList2.get(i)).replace("\\", "");
                        subscriber.onNext(new ResolveResult(VK.this.m16706(), replace, (String) arrayList.get(i)));
                    }
                }
                subscriber.onCompleted();
            }
        });
    }
}
