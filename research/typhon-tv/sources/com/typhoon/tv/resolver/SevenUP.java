package com.typhoon.tv.resolver;

import com.typhoon.tv.resolver.base.GenericResolver;

public class SevenUP extends GenericResolver {
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m16660() {
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m16661() {
        return "SevenUP";
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m16662() {
        return "(?://|\\.)(7-up\\.net)/(?:embed-)?([0-9a-zA-Z-]+)";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m16663() {
        return "https://7-up.net";
    }
}
