package com.typhoon.tv.resolver;

import com.mopub.common.TyphoonApp;
import com.typhoon.tv.Logger;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import rx.Observable;
import rx.Subscriber;

public class Yandex extends BaseResolver {
    /* renamed from: 靐  reason: contains not printable characters */
    public String m16755() {
        return "Yandex";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16756() {
        return "HD";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16757(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String r9 = Regex.m17800(str, "(?://|\\.)((?:disk|mc|drive)\\.yandex\\.ru)/(?:watch/)?([di])/([^/?#&]+)", 2);
                String r8 = Regex.m17800(str, "(?://|\\.)((?:disk|mc|drive)\\.yandex\\.ru)/(?:watch)?([di])/([^/?#&]+)", 3);
                if (r9.isEmpty() || r8.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                ArrayList<ArrayList<String>> r2 = Regex.m17804(HttpHelper.m6343().m6351("https://mc.yandex.ru/" + r9 + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + r8, (Map<String, String>[]) new Map[0]), "dimension['\"]\\s*:\\s*['\"]((?:\\d{3,4}|adaptive))p?['\"].+?['\"]url['\"]\\s*:\\s*['\"]([^'\"]+)", 2, 2);
                ArrayList arrayList = r2.get(0);
                ArrayList arrayList2 = r2.get(1);
                int i = 0;
                while (i < arrayList.size()) {
                    try {
                        String str = (String) arrayList.get(i);
                        String str2 = (String) arrayList2.get(i);
                        if (arrayList.size() < 2 || !str.equalsIgnoreCase("adaptive")) {
                            if (str2.startsWith("//")) {
                                str2 = "http:" + str2;
                            } else if (str2.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
                                str2 = "https://mc.yandex.ru" + str2;
                            } else if (!str2.startsWith(TyphoonApp.HTTP)) {
                                str2 = "https://mc.yandex.ru/watch/" + str2;
                            }
                            String str3 = "HD";
                            if (!str.isEmpty() && Utils.m6426(str)) {
                                str3 = str + TtmlNode.TAG_P;
                            }
                            subscriber.onNext(new ResolveResult(Yandex.this.m16755(), str2, str3));
                            i++;
                        } else {
                            i++;
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                subscriber.onCompleted();
            }
        });
    }
}
