package com.typhoon.tv.resolver;

import com.typhoon.tv.Logger;
import com.typhoon.tv.debrid.realdebrid.RealDebridCredentialsHelper;
import com.typhoon.tv.debrid.realdebrid.RealDebridUserApi;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;
import rx.Observable;
import rx.Subscriber;

public class UpToBox extends BaseResolver {
    /* renamed from: 靐  reason: contains not printable characters */
    public String m16694() {
        return "UpToBox";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m16695() {
        return "HD";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16696(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String r14;
                String r18;
                List<ResolveResult> r19;
                if (RealDebridCredentialsHelper.m15838().isValid() && (r19 = RealDebridUserApi.m15845().m15850(str, UpToBox.this.m16694())) != null) {
                    for (ResolveResult onNext : r19) {
                        subscriber.onNext(onNext);
                    }
                }
                String r15 = Regex.m17800(str, "(?://|\\.)(uptobox\\.com|uptostream\\.com)/(?:iframe/)?([0-9A-Za-z_]+)", 2);
                if (r15.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                ArrayList arrayList = new ArrayList();
                String str = "https://uptostream.com/iframe/" + r15;
                String r4 = HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[0]);
                try {
                    arrayList.addAll(UpToBox.this.m16781(str, r4, false, (HashMap<String, String>) null, new String[0][]));
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                try {
                    String replace = Regex.m17802(r4, "window.sources.*=.*(\\[.*?\\])", 1, true).replace("\\/", InternalZipTyphoonApp.ZIP_FILE_SEPARATOR).replace("\\\"", "\"");
                    if (!replace.isEmpty()) {
                        Iterator it2 = Regex.m17805(replace, "\\{(.+?)\\}", 1, true).get(0).iterator();
                        while (it2.hasNext()) {
                            String str2 = (String) it2.next();
                            if (!str2.isEmpty() && (!str2.trim().toLowerCase().contains("video/") || str2.trim().toLowerCase().contains("video."))) {
                                String replace2 = Regex.m17802(str2, "['\"]?src['\"]\\s*:\\s*['\"]([^'\"]+)['\"].*?['\"]?label['\"]?\\s*:\\s*['\"]?(\\d{3,4})p?['\"]?", 1, true).replace(StringUtils.SPACE, "%20");
                                String r22 = Regex.m17802(str2, "['\"]?src['\"]\\s*:\\s*['\"]([^'\"]+)['\"].*?['\"]?label['\"]?\\s*:\\s*['\"]?(\\d{3,4})p?['\"]?", 2, true);
                                if (r22.isEmpty()) {
                                    r22 = Regex.m17800(replace2, "/(\\d{3,4})/", 1);
                                    if (!r22.equals("2160") && !r22.equals("1080") && !r22.equals("720") && !r22.equals("480") && !r22.equals("360")) {
                                        r22 = "HD";
                                    }
                                }
                                arrayList.add(new ResolveResult(UpToBox.this.m16694(), replace2, r22));
                            }
                        }
                    }
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
                if (arrayList.isEmpty()) {
                    try {
                        String str3 = "https://uptobox.com/" + r15;
                        String r24 = HttpHelper.m6343().m6358(str3, str3);
                        if (!r24.isEmpty() && !r24.contains("not available in your country") && !r24.contains("You have to wait ") && !r24.contains("or you can wait") && !r24.contains("the file you want is not available") && !r24.contains("the video you want to see is not available") && !r24.contains("This stream doesn") && !r24.contains((CharSequence) null)) {
                            String r16 = Utils.m6399((Map<String, String>) BaseResolver.m16771(r24, (String) null));
                            HashMap hashMap = new HashMap();
                            hashMap.put("Referer", str3);
                            int i = 0;
                            while (true) {
                                if (i >= 3) {
                                    break;
                                }
                                try {
                                    r14 = Regex.m17800(HttpHelper.m6343().m6360(str3, r16, true, (Map<String, String>[]) new Map[]{hashMap}), "href\\s*=\\s*['\"]([^'\"]+)[^>]+>\\s*<span[^>]+class\\s*=\\s*['\"]button_upload green['\"]", 1);
                                    if (!r14.trim().isEmpty()) {
                                        r18 = Regex.m17800(r14, "[\\. ](\\d{3,4})p[\\. ]", 1);
                                        if (r18.isEmpty()) {
                                            r18 = "";
                                            break;
                                        } else if (Utils.m6426(r18)) {
                                            r18 = r18 + TtmlNode.TAG_P;
                                        }
                                    } else {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (Exception e3) {
                                            Logger.m6281((Throwable) e3, new boolean[0]);
                                        }
                                        i++;
                                    }
                                } catch (Exception e4) {
                                    Logger.m6281((Throwable) e4, new boolean[0]);
                                }
                            }
                            arrayList.add(new ResolveResult(UpToBox.this.m16694(), r14.replace(StringUtils.SPACE, "%20"), r18));
                        }
                    } catch (Exception e5) {
                        Logger.m6281((Throwable) e5, new boolean[0]);
                    }
                }
                Iterator it3 = Utils.m6415(arrayList).iterator();
                while (it3.hasNext()) {
                    ResolveResult resolveResult = (ResolveResult) it3.next();
                    String resolvedQuality = resolveResult.getResolvedQuality();
                    if (resolvedQuality != null && !resolvedQuality.isEmpty() && resolvedQuality.trim().startsWith("2160")) {
                        resolveResult.setResolvedQuality("4K");
                    }
                    subscriber.onNext(resolveResult);
                }
                subscriber.onCompleted();
            }
        });
    }
}
