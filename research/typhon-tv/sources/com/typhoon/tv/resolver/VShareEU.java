package com.typhoon.tv.resolver;

import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.helper.js.JsUnpacker;
import com.typhoon.tv.model.ResolveResult;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.lang3.StringUtils;
import rx.Observable;
import rx.Subscriber;

public class VShareEU extends BaseResolver {
    /* renamed from: 靐  reason: contains not printable characters */
    public String m16711() {
        return "VShare.eu";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Observable<ResolveResult> m16712(final String str) {
        return Observable.m7359(new Observable.OnSubscribe<ResolveResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ResolveResult> subscriber) {
                String r12 = Regex.m17801(str, "(?://|\\.)(vshare\\.(?:eu|io))/(?:embed-)?([0-9a-zA-Z/]+)", 2, 2);
                if (r12.isEmpty()) {
                    subscriber.onCompleted();
                    return;
                }
                String str = "https://vshare.eu/" + r12;
                HashMap hashMap = new HashMap();
                hashMap.put("Upgrade-Insecure-Requests", PubnativeRequest.LEGACY_ZONE_ID);
                String r13 = HttpHelper.m6343().m6351(str, (Map<String, String>[]) new Map[]{hashMap});
                if (r13.contains("404 Not Found") || r13.contains("could not be found") || r13.contains("either expired or has been deleted") || r13.contains("<div id=\"deleted\">")) {
                    subscriber.onCompleted();
                    return;
                }
                hashMap.put("Referer", str);
                HashMap<String, String> r9 = BaseResolver.m16771(r13, (String) null);
                r9.put("method_free", "Proceed to video");
                String r132 = HttpHelper.m6343().m6350(str, Utils.m6399((Map<String, String>) r9), hashMap);
                ArrayList arrayList = new ArrayList();
                arrayList.add(r132);
                if (JsUnpacker.m16026(r132)) {
                    arrayList.addAll(JsUnpacker.m16022(r132));
                }
                ArrayList arrayList2 = new ArrayList();
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    String str2 = (String) it2.next();
                    ArrayList arrayList3 = Regex.m17804(str2, "file\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*flashplayer", 1, 34).get(0);
                    arrayList3.addAll(Regex.m17805(str2, "config\\s*:\\s*\\{\\s*file\\s*:\\s*[\"']([^\"']+)", 1, true).get(0));
                    arrayList3.addAll(Regex.m17805(str2, "<source\\s+src=['\"]([^'\"]+)['\"]", 1, true).get(0));
                    Iterator it3 = arrayList3.iterator();
                    while (it3.hasNext()) {
                        String replace = ((String) it3.next()).replace(StringUtils.SPACE, "%20");
                        if (!replace.endsWith(".vtt") && !replace.endsWith(".srt") && !replace.endsWith(".png") && !replace.endsWith(".jpg") && !arrayList2.contains(replace)) {
                            arrayList2.add(replace);
                            subscriber.onNext(new ResolveResult(VShareEU.this.m16711(), replace, "HQ"));
                        }
                    }
                    Iterator it4 = VShareEU.this.m16781(str, str2, false, (HashMap<String, String>) null, new String[0][]).iterator();
                    while (it4.hasNext()) {
                        subscriber.onNext((ResolveResult) it4.next());
                    }
                }
                subscriber.onCompleted();
            }
        });
    }
}
