package com.typhoon.tv;

import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.model.CheckEpisodeResult;
import com.typhoon.tv.model.CheckNewMovieReleaseResult;
import com.typhoon.tv.model.LastPlaybackInfo;
import com.typhoon.tv.model.download.DownloadIDRecord;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.movie.MovieWatchedMovie;
import com.typhoon.tv.model.media.tv.TvLatestPlayed;
import com.typhoon.tv.model.media.tv.TvWatchedEpisode;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TVDatabase {
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6319(MediaInfo info) {
        return m6320(Integer.valueOf(info.getType()), Integer.valueOf(info.getTmdbId()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6320(Integer type, Integer tmdbId) {
        return MediaInfo.find(MediaInfo.class, "type = ? and tmdb_id = ?", type.toString(), tmdbId.toString()).size() > 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6299(MediaInfo info) {
        info.save();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m6309(MediaInfo info) {
        m6304(info);
        m6299(info);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m6304(MediaInfo info) {
        m6292(Integer.valueOf(info.getType()), Integer.valueOf(info.getTmdbId()));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m6292(Integer type, Integer tmdbId) {
        try {
            m6305(tmdbId);
        } catch (Exception e) {
            Logger.m6281((Throwable) e, true);
        }
        List<MediaInfo> infoList = MediaInfo.find(MediaInfo.class, "type = ? and tmdb_id = ?", type.toString(), tmdbId.toString());
        if (infoList != null) {
            for (MediaInfo info : infoList) {
                info.delete();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ArrayList<MediaInfo> m6312(Integer type) {
        List<MediaInfo> mediaInfoList = MediaInfo.find(MediaInfo.class, "type = ?", type.toString());
        if (mediaInfoList != null) {
            return new ArrayList<>(mediaInfoList);
        }
        return new ArrayList<>();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ArrayList<MediaInfo> m6297(Integer type, Integer tmdbId) {
        List<MediaInfo> mediaInfoList = MediaInfo.find(MediaInfo.class, "type = ? and tmdb_id = ?", type.toString(), tmdbId.toString());
        if (mediaInfoList != null) {
            return new ArrayList<>(mediaInfoList);
        }
        return new ArrayList<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6315(Integer tmdbId, int season, int episode) {
        List<TvLatestPlayed> records = TvLatestPlayed.find(TvLatestPlayed.class, "tmdb_id = ?", tmdbId.toString());
        if (records == null || records.size() <= 0) {
            new TvLatestPlayed(tmdbId.intValue(), season, episode).save();
            return;
        }
        TvLatestPlayed record = records.get(0);
        record.setSeason(season);
        record.setEpisode(episode);
        record.save();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public TvLatestPlayed m6296(Integer tmdbId) {
        List<TvLatestPlayed> records = TvLatestPlayed.find(TvLatestPlayed.class, "tmdb_id = ?", tmdbId.toString());
        if (records.size() > 0) {
            return records.get(0);
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public TvLatestPlayed m6307(Integer tmdbId, Integer season) {
        List<TvLatestPlayed> records = TvLatestPlayed.find(TvLatestPlayed.class, "tmdb_id = ? and season = ?", tmdbId.toString(), season.toString());
        if (records.size() > 0) {
            return records.get(0);
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6322(Integer tmdbId, String imdbId) {
        List<MovieWatchedMovie> list1;
        List<MovieWatchedMovie> infoList = new ArrayList<>();
        if (!(imdbId == null || !imdbId.startsWith(TtmlNode.TAG_TT) || (list1 = MovieWatchedMovie.find(MovieWatchedMovie.class, "tmdb_id = ? and imdb_id = ?", tmdbId.toString(), imdbId)) == null)) {
            infoList.addAll(list1);
        }
        List<MovieWatchedMovie> list2 = MovieWatchedMovie.find(MovieWatchedMovie.class, "tmdb_id = ?", tmdbId.toString());
        if (list2 != null) {
            infoList.addAll(list2);
        }
        if (infoList.size() > 0) {
            return true;
        }
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6301(Integer tmdbId, String imdbId) {
        if (!m6322(tmdbId, imdbId)) {
            new MovieWatchedMovie(tmdbId.intValue(), imdbId).save();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m6311(Integer tmdbId, String imdbId) {
        List<MovieWatchedMovie> list1;
        List<MovieWatchedMovie> infoList = new ArrayList<>();
        if (!(imdbId == null || !imdbId.startsWith(TtmlNode.TAG_TT) || (list1 = MovieWatchedMovie.find(MovieWatchedMovie.class, "tmdb_id = ? and imdb_id = ?", tmdbId.toString(), imdbId)) == null)) {
            infoList.addAll(list1);
        }
        List<MovieWatchedMovie> list2 = MovieWatchedMovie.find(MovieWatchedMovie.class, "tmdb_id = ?", tmdbId.toString());
        if (list2 != null) {
            infoList.addAll(list2);
        }
        for (MovieWatchedMovie info : Utils.m6415(infoList)) {
            info.delete();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6321(Integer tmdbId, Integer season, Integer episode) {
        return TvWatchedEpisode.find(TvWatchedEpisode.class, "tmdb_id = ? and season = ? and episode = ?", tmdbId.toString(), season.toString(), episode.toString()).size() > 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<TvWatchedEpisode> m6313() {
        return Utils.m6417(TvWatchedEpisode.findAll(TvWatchedEpisode.class));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public List<TvWatchedEpisode> m6303(Integer tmdbId, Integer season) {
        return TvWatchedEpisode.find(TvWatchedEpisode.class, "tmdb_id = ? and season = ?", tmdbId.toString(), season.toString());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6300(Integer tmdbId, Integer season, Integer episode) {
        if (!m6321(tmdbId, season, episode)) {
            new TvWatchedEpisode(tmdbId.intValue(), season.intValue(), episode.intValue()).save();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m6310(Integer tmdbId, Integer season, Integer episode) {
        List<TvWatchedEpisode> infoList = TvWatchedEpisode.find(TvWatchedEpisode.class, "tmdb_id = ? and season = ? and episode = ?", tmdbId.toString(), season.toString(), episode.toString());
        if (infoList != null) {
            for (TvWatchedEpisode info : infoList) {
                info.delete();
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m6293(Integer tmdbId, Integer season) {
        TvWatchedEpisode twe;
        List<TvWatchedEpisode> infoList = TvWatchedEpisode.find(TvWatchedEpisode.class, "tmdb_id = ? and season = ?", tmdbId.toString(), season.toString());
        if (infoList == null || infoList.size() <= 0 || (twe = (TvWatchedEpisode) Collections.max(infoList, new Comparator<TvWatchedEpisode>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public int compare(TvWatchedEpisode tvWatchedEpisode, TvWatchedEpisode tvWatchedEpisode2) {
                return Utils.m6411(tvWatchedEpisode.getEpisode(), tvWatchedEpisode2.getEpisode());
            }
        })) == null) {
            return -1;
        }
        return twe.getEpisode();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public CheckEpisodeResult m6306(Integer tmdbId) {
        List<CheckEpisodeResult> resultList = CheckEpisodeResult.find(CheckEpisodeResult.class, "tmdb_id = ?", tmdbId.toString());
        if (resultList == null || resultList.size() <= 0) {
            return null;
        }
        return resultList.get(0);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m6305(Integer tmdbId) {
        List<CheckEpisodeResult> resultList = CheckEpisodeResult.find(CheckEpisodeResult.class, "tmdb_id = ?", tmdbId.toString());
        if (resultList != null) {
            for (CheckEpisodeResult result : resultList) {
                result.delete();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public CheckNewMovieReleaseResult m6294() {
        List<CheckNewMovieReleaseResult> resultList = CheckNewMovieReleaseResult.find(CheckNewMovieReleaseResult.class, (String) null, new String[0]);
        if (resultList == null || resultList.size() <= 0) {
            return null;
        }
        return resultList.get(0);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public List<CheckNewMovieReleaseResult> m6308() {
        return CheckNewMovieReleaseResult.find(CheckNewMovieReleaseResult.class, (String) null, new String[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6318(long id) {
        return DownloadIDRecord.find(DownloadIDRecord.class, "download_id = ?", Long.toString(id)).size() > 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6314(DownloadIDRecord record) {
        if (!m6318(record.getDownloadId())) {
            record.save();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6298(long id) {
        List<DownloadIDRecord> resultList = DownloadIDRecord.find(DownloadIDRecord.class, "download_id = ?", Long.toString(id));
        if (resultList != null) {
            for (DownloadIDRecord record : resultList) {
                record.delete();
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public List<DownloadIDRecord> m6302() {
        return Utils.m6417(DownloadIDRecord.findAll(DownloadIDRecord.class));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6317(boolean isMovie, Integer tmdbId, Integer season, Integer episode, long position, long duration) {
        List<LastPlaybackInfo> infoList;
        if (tmdbId.intValue() <= 0) {
            return;
        }
        if (isMovie || (season.intValue() >= 0 && episode.intValue() >= 0)) {
            if (isMovie) {
                Class<LastPlaybackInfo> cls = LastPlaybackInfo.class;
                try {
                    infoList = LastPlaybackInfo.find(cls, "tmdb_id = ?", tmdbId.toString());
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    return;
                }
            } else {
                Class<LastPlaybackInfo> cls2 = LastPlaybackInfo.class;
                infoList = LastPlaybackInfo.find(cls2, "tmdb_id = ? and season = ? and episode = ?", tmdbId.toString(), season.toString(), episode.toString());
            }
            if (infoList == null || infoList.size() <= 0) {
                LastPlaybackInfo info = new LastPlaybackInfo(tmdbId.intValue(), position);
                info.setSeason(season.intValue());
                info.setEpisode(episode.intValue());
                info.setDuration(duration);
                info.save();
                return;
            }
            LastPlaybackInfo info2 = infoList.get(0);
            info2.setSeason(season.intValue());
            info2.setEpisode(episode.intValue());
            info2.setPosition(position);
            info2.setDuration(duration);
            info2.setTimestamp(Long.parseLong(DateTimeHelper.m15934()));
            info2.save();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6316(boolean isMovie, Integer tmdbId, Integer season, Integer episode) {
        List<LastPlaybackInfo> infoList;
        if (tmdbId.intValue() <= 0) {
            return;
        }
        if (isMovie || (season.intValue() >= 0 && episode.intValue() >= 0)) {
            if (isMovie) {
                Class<LastPlaybackInfo> cls = LastPlaybackInfo.class;
                try {
                    infoList = LastPlaybackInfo.find(cls, "tmdb_id = ?", tmdbId.toString());
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    return;
                }
            } else {
                Class<LastPlaybackInfo> cls2 = LastPlaybackInfo.class;
                infoList = LastPlaybackInfo.find(cls2, "tmdb_id = ? and season = ? and episode = ?", tmdbId.toString(), season.toString(), episode.toString());
            }
            if (infoList != null) {
                for (LastPlaybackInfo info : infoList) {
                    info.delete();
                }
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public LastPlaybackInfo m6295(boolean isMovie, Integer tmdbId, Integer season, Integer episode) {
        List<LastPlaybackInfo> infoList;
        if (tmdbId.intValue() <= 0) {
            return null;
        }
        if (!isMovie && (season.intValue() < 0 || episode.intValue() < 0)) {
            return null;
        }
        if (isMovie) {
            try {
                infoList = LastPlaybackInfo.find(LastPlaybackInfo.class, "tmdb_id = ?", tmdbId.toString());
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                return null;
            }
        } else {
            infoList = LastPlaybackInfo.find(LastPlaybackInfo.class, "tmdb_id = ? and season = ? and episode = ?", tmdbId.toString(), season.toString(), episode.toString());
        }
        return (infoList == null || infoList.size() <= 0) ? null : infoList.get(0);
    }
}
