package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.model.media.MediaApiResult;
import com.typhoon.tv.presenter.ISearchPresenter;
import com.typhoon.tv.view.ISearchView;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SearchPresenterImpl implements ISearchPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12725;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public ISearchView f12726;

    public SearchPresenterImpl(ISearchView iSearchView) {
        this.f12726 = iSearchView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16211() {
        if (this.f12725 != null && !this.f12725.isUnsubscribed()) {
            this.f12725.unsubscribe();
        }
        this.f12725 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16212() {
        m16211();
        this.f12726 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16213(final int i, final String str, final int i2) {
        m16211();
        this.f12725 = Observable.m7359(new Observable.OnSubscribe<MediaApiResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaApiResult> subscriber) {
                MediaApiResult r0 = i == 0 ? TmdbApi.m15742().m15757(str, Integer.valueOf(i2)) : TmdbApi.m15742().m15750(str, Integer.valueOf(i2));
                if (r0 == null) {
                    subscriber.onError(new Exception());
                } else {
                    subscriber.onNext(r0);
                }
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<MediaApiResult>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                SearchPresenterImpl.this.f12726.m17868();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(MediaApiResult mediaApiResult) {
                SearchPresenterImpl.this.f12726.m17869(mediaApiResult);
            }
        });
    }
}
