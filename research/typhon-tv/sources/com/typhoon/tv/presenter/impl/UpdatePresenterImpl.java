package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.UpdateService;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.UpdateInfo;
import com.typhoon.tv.presenter.IUpdatePresenter;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.IUpdateView;
import java.io.File;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class UpdatePresenterImpl implements IUpdatePresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private CompositeSubscription f12803;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public IUpdateView f12804;

    public UpdatePresenterImpl(IUpdateView updateView) {
        this.f12804 = updateView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16275() {
        if (this.f12803 != null) {
            this.f12803.unsubscribe();
        }
        this.f12803 = null;
        HttpHelper.m6343().m6370((Object) "downloadUpdate");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m16276() {
        m16275();
        this.f12804 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16277() {
        if (this.f12803 == null) {
            this.f12803 = new CompositeSubscription();
        }
        this.f12803.m25086(Observable.m7359(new Observable.OnSubscribe<UpdateInfo>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super UpdateInfo> subscriber) {
                subscriber.onNext(UpdateService.m15717());
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<UpdateInfo>() {
            public void onCompleted() {
                if (UpdatePresenterImpl.this.f12804 != null) {
                    UpdatePresenterImpl.this.f12804.m17888();
                }
            }

            public void onError(Throwable e) {
                Logger.m6281(e, new boolean[0]);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(UpdateInfo updateInfo) {
                if (UpdatePresenterImpl.this.f12804 != null && updateInfo != null) {
                    UpdatePresenterImpl.this.f12804.m17885(updateInfo);
                }
            }
        }));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16278(UpdateInfo info) {
        m16275();
        this.f12804.m17884(info.getVersion());
        File outputFolder = new File(UpdateService.f12497);
        if (!outputFolder.exists() && !outputFolder.mkdirs()) {
            outputFolder.mkdir();
        }
        if (outputFolder.listFiles() != null) {
            for (File file : outputFolder.listFiles()) {
                if (file.getName().trim().toLowerCase().endsWith(".apk")) {
                    file.delete();
                }
            }
        }
        try {
            Utils.m6421(new File(UpdateService.f12497));
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        this.f12803 = new CompositeSubscription();
        this.f12803.m25086(UpdateService.m15718(info).m7394(1, TimeUnit.SECONDS).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<Integer>() {
            public void onCompleted() {
                UpdatePresenterImpl.this.f12804.m17883();
                UpdatePresenterImpl.this.f12804.m17889();
            }

            public void onError(Throwable th) {
                UpdatePresenterImpl.this.f12804.m17883();
                UpdatePresenterImpl.this.f12804.m17887(th);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(Integer num) {
                UpdatePresenterImpl.this.f12804.m17886(num);
            }
        }));
    }
}
