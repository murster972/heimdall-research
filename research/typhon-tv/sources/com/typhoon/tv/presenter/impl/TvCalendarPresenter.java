package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.TvMazeApi;
import com.typhoon.tv.helper.DateTimeHelper;
import com.typhoon.tv.model.media.tv.TvNewEpisodeInfo;
import com.typhoon.tv.presenter.ITvCalendarPresenter;
import com.typhoon.tv.view.ITvCalendarView;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class TvCalendarPresenter implements ITvCalendarPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12796;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public ITvCalendarView f12797;

    public TvCalendarPresenter(ITvCalendarView iTvCalendarView) {
        this.f12797 = iTvCalendarView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16268() {
        m16269();
        this.f12797 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16269() {
        if (this.f12796 != null && !this.f12796.isUnsubscribed()) {
            this.f12796.unsubscribe();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16270(final String str, final int i) {
        m16269();
        this.f12796 = Observable.m7359(new Observable.OnSubscribe<List<TvNewEpisodeInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super List<TvNewEpisodeInfo>> subscriber) {
                subscriber.onNext(TvMazeApi.m15810().m15812(DateTimeHelper.m15937(str).toDateTime(DateTimeZone.forTimeZone(TimeZone.getTimeZone("America/Los_Angeles"))).toString(DateTimeFormat.m21228("yyyy-MM-dd")), i));
                subscriber.onCompleted();
            }
        }).m7376(new Func1<Throwable, List<TvNewEpisodeInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public List<TvNewEpisodeInfo> call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return new ArrayList();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7415(new Observer<List<TvNewEpisodeInfo>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                TvCalendarPresenter.this.f12797.m17881();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(List<TvNewEpisodeInfo> list) {
                if (list.isEmpty()) {
                    TvCalendarPresenter.this.f12797.m17881();
                } else {
                    TvCalendarPresenter.this.f12797.m17882(list);
                }
            }
        });
    }
}
