package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.OmdbApi;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaRatingsModel;
import com.typhoon.tv.presenter.IMediaRatingsPresenter;
import com.typhoon.tv.view.IMediaRatingsView;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MediaRatingsPresenterImpl implements IMediaRatingsPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12710;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public IMediaRatingsView f12711;

    public MediaRatingsPresenterImpl(IMediaRatingsView iMediaRatingsView) {
        this.f12711 = iMediaRatingsView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16196() {
        if (this.f12710 != null && !this.f12710.isUnsubscribed()) {
            this.f12710.unsubscribe();
        }
        this.f12710 = null;
        this.f12711 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16197(final MediaInfo mediaInfo) {
        this.f12710 = Observable.m7359(new Observable.OnSubscribe<MediaRatingsModel>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaRatingsModel> subscriber) {
                MediaRatingsModel mediaRatingsModel = mediaInfo.getType() == 1 ? (mediaInfo.getImdbId() == null || !mediaInfo.getImdbId().startsWith(TtmlNode.TAG_TT)) ? OmdbApi.m15735().m15739(mediaInfo.getName(), mediaInfo.getYear()) : OmdbApi.m15735().m15738(mediaInfo.getImdbId()) : (mediaInfo.getImdbId() == null || !mediaInfo.getImdbId().startsWith(TtmlNode.TAG_TT)) ? OmdbApi.m15735().m15737(mediaInfo.getName(), mediaInfo.getYear()) : OmdbApi.m15735().m15736(mediaInfo.getImdbId());
                if (mediaRatingsModel == null) {
                    subscriber.onError(new Exception());
                } else {
                    subscriber.onNext(mediaRatingsModel);
                }
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<MediaRatingsModel>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                MediaRatingsPresenterImpl.this.f12711.m17864();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(MediaRatingsModel mediaRatingsModel) {
                MediaRatingsPresenterImpl.this.f12711.m17865(mediaRatingsModel);
            }
        });
    }
}
