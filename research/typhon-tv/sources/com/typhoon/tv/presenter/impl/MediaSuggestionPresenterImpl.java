package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.model.media.MediaApiResult;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.presenter.IMediaSuggestionPresenter;
import com.typhoon.tv.view.IMediaSuggestionView;
import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MediaSuggestionPresenterImpl implements IMediaSuggestionPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12715;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public IMediaSuggestionView f12716;

    public MediaSuggestionPresenterImpl(IMediaSuggestionView iMediaSuggestionView) {
        this.f12716 = iMediaSuggestionView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16201(final int i) {
        this.f12715 = Observable.m7359(new Observable.OnSubscribe<MediaApiResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaApiResult> subscriber) {
                MediaApiResult r0 = TmdbApi.m15742().m15747(i);
                if (r0 == null) {
                    subscriber.onError(new Exception("MediaApiResult is null while calling getTvShowSuggestionById"));
                } else {
                    subscriber.onNext(r0);
                }
                subscriber.onCompleted();
            }
        }).m7396(new Func1<MediaApiResult, Observable<ArrayList<MediaInfo>>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<ArrayList<MediaInfo>> call(MediaApiResult mediaApiResult) {
                return Observable.m7356(mediaApiResult.getMediaInfoList());
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<List<MediaInfo>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                if (MediaSuggestionPresenterImpl.this.f12716 != null) {
                    MediaSuggestionPresenterImpl.this.f12716.m17866();
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(List<MediaInfo> list) {
                if (MediaSuggestionPresenterImpl.this.f12716 != null) {
                    MediaSuggestionPresenterImpl.this.f12716.m17867(list);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16202() {
        if (this.f12715 != null && !this.f12715.isUnsubscribed()) {
            this.f12715.unsubscribe();
        }
        this.f12715 = null;
        this.f12716 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16203(final int i) {
        this.f12715 = Observable.m7359(new Observable.OnSubscribe<MediaApiResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaApiResult> subscriber) {
                MediaApiResult r0 = TmdbApi.m15742().m15746(i);
                if (r0 == null) {
                    subscriber.onError(new Exception());
                } else {
                    subscriber.onNext(r0);
                }
                subscriber.onCompleted();
            }
        }).m7396(new Func1<MediaApiResult, Observable<ArrayList<MediaInfo>>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<ArrayList<MediaInfo>> call(MediaApiResult mediaApiResult) {
                return Observable.m7356(mediaApiResult.getMediaInfoList());
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<List<MediaInfo>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                if (MediaSuggestionPresenterImpl.this.f12716 != null) {
                    MediaSuggestionPresenterImpl.this.f12716.m17866();
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(List<MediaInfo> list) {
                if (MediaSuggestionPresenterImpl.this.f12716 != null) {
                    MediaSuggestionPresenterImpl.this.f12716.m17867(list);
                }
            }
        });
    }
}
