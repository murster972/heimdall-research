package com.typhoon.tv.presenter.impl;

import com.google.android.exoplayer2.C;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.api.JuicyApi;
import com.typhoon.tv.helper.GoogleVideoHelper;
import com.typhoon.tv.helper.LimelightNetworksHelper;
import com.typhoon.tv.helper.TitleHelper;
import com.typhoon.tv.helper.TyphoonTV;
import com.typhoon.tv.helper.VidCDNHelper;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.MediaSource;
import com.typhoon.tv.presenter.ISourcePresenter;
import com.typhoon.tv.provider.BaseProvider;
import com.typhoon.tv.resolver.base.BaseResolver;
import com.typhoon.tv.utils.DeviceUtils;
import com.typhoon.tv.utils.Regex;
import com.typhoon.tv.utils.SourceObservableUtils;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.ISourceView;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class SourcePresenterImpl implements ISourcePresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12737;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public ISourceView f12738;

    public SourcePresenterImpl(ISourceView iSourceView) {
        this.f12738 = iSourceView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m16223(MediaInfo mediaInfo) {
        if (mediaInfo.getType() == 1 && mediaInfo.getTmdbId() == 270303) {
            mediaInfo.setYear(2014);
        }
        if (mediaInfo.getType() == 1 && mediaInfo.getTmdbId() == 1891) {
            mediaInfo.setName("Star Wars: Episode V - The Empire Strikes Back");
        }
        if (mediaInfo.getType() == 1 && mediaInfo.getTmdbId() == 1892) {
            mediaInfo.setName("Star Wars: Episode VI - Return of the Jedi");
        }
        if (mediaInfo.getType() == 0 && mediaInfo.getTmdbId() == 67744) {
            mediaInfo.setName("Mindhunter");
        }
        if (mediaInfo.getType() == 0 && mediaInfo.getName().equalsIgnoreCase("Will and Grace")) {
            mediaInfo.setName("Will & Grace");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16224() {
        m16225();
        this.f12738 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16225() {
        HttpHelper.m6343().m6347();
        if (this.f12737 != null && !this.f12737.isUnsubscribed()) {
            this.f12737.unsubscribe();
        }
        this.f12737 = null;
        try {
            System.gc();
        } catch (Throwable th) {
            Logger.m6281(th, true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16226(MediaInfo mediaInfo, int i, int i2) {
        MediaInfo mediaInfo2;
        Observable r17;
        boolean z = TVApplication.m6285().getBoolean("pref_parallel_loading_sources", true);
        boolean z2 = TVApplication.m6285().getBoolean("pref_show_hd_sources_only", false);
        int i3 = TVApplication.m6285().getInt("pref_sources_list_refresh_interval", Utils.m6394());
        try {
            mediaInfo2 = mediaInfo.cloneDeeply();
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            mediaInfo2 = mediaInfo;
        }
        final boolean z3 = mediaInfo2.getType() == 1;
        final TyphoonTV typhoonTV = new TyphoonTV();
        BaseProvider[] r11 = z3 ? TyphoonApp.m6328() : TyphoonApp.m6331();
        String string = z3 ? TVApplication.m6285().getString("pref_enabled_movies_providers", (String) null) : TVApplication.m6285().getString("pref_enabled_tv_shows_providers", (String) null);
        List asList = string != null ? string.contains(",") ? Arrays.asList(string.split(",")) : Collections.singletonList(string) : null;
        ArrayList arrayList = new ArrayList();
        if (asList != null) {
            for (BaseProvider baseProvider : r11) {
                if (asList.contains(baseProvider.m16284())) {
                    arrayList.add(baseProvider);
                }
            }
        } else {
            arrayList.addAll(Arrays.asList(r11));
        }
        if (z3) {
            mediaInfo2.setName(TitleHelper.m15966(mediaInfo2.getName()));
        }
        m16223(mediaInfo2);
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = null;
        for (int i4 = 0; i4 < arrayList.size(); i4++) {
            BaseProvider baseProvider2 = (BaseProvider) arrayList.get(i4);
            Observable<MediaSource> r26 = (z3 ? baseProvider2.m16286(mediaInfo2) : baseProvider2.m16287(mediaInfo2, i, i2)).m7376(new Func1<Throwable, MediaSource>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public MediaSource call(Throwable th) {
                    Logger.m6280(th, "An error occurred while getting sources from provider", new boolean[0]);
                    return null;
                }
            }).m7367().m7402(300, TimeUnit.SECONDS, Observable.m7349());
            if (z) {
                arrayList2.add(r26.m7382(Schedulers.io()));
            } else {
                if (arrayList3 == null) {
                    arrayList3 = new ArrayList();
                }
                if (i4 % 2 == 0) {
                    arrayList2.add(r26);
                } else {
                    arrayList3.add(r26);
                }
            }
        }
        if (z) {
            r17 = Observable.m7345(arrayList2);
        } else {
            r17 = Observable.m7348(Observable.m7345(arrayList2).m7382(Schedulers.io()), arrayList3 != null ? Observable.m7345(arrayList3).m7382(Schedulers.io()) : Observable.m7349().m7382(Schedulers.io()));
        }
        final MediaInfo mediaInfo3 = mediaInfo2;
        final boolean z4 = z2;
        final boolean z5 = TVApplication.m6285().getBoolean("pref_filter_out_cam_version_links", false);
        final int i5 = i;
        final int i6 = i2;
        this.f12737 = r17.m7385(new Func1<MediaSource, Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Boolean call(MediaSource mediaSource) {
                return Boolean.valueOf(mediaSource != null && mediaSource.getStreamLink() != null && !mediaSource.getStreamLink().isEmpty() && (mediaSource.getStreamLink().trim().toLowerCase().startsWith("http://") || mediaSource.getStreamLink().trim().toLowerCase().startsWith("https://")));
            }
        }).m7396(new Func1<MediaSource, Observable<MediaSource>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaSource> call(MediaSource mediaSource) {
                boolean z;
                if (mediaSource.isNeededToResolve()) {
                    if (TVApplication.m6285().getBoolean("pref_resolve_all_links_immediately", true)) {
                        if (TVApplication.m6285().getBoolean("pref_auto_resolve_hd_links_only", !DeviceUtils.m6387())) {
                            String quality = mediaSource.getQuality();
                            if (quality == null || quality.isEmpty()) {
                                BaseResolver r2 = BaseResolver.m16773(mediaSource.getStreamLink());
                                if (r2 == null || !BaseResolver.m16776(r2) || r2.m16780() == null || r2.m16780().isEmpty()) {
                                    z = false;
                                } else {
                                    mediaSource.setQuality(r2.m16780());
                                    z = r2.m16780().equalsIgnoreCase("HD");
                                }
                            } else {
                                String trim = quality.replace(TtmlNode.TAG_P, "").trim();
                                z = !trim.equalsIgnoreCase("HQ") && !trim.equalsIgnoreCase("SD") && (!Utils.m6426(trim) || Integer.parseInt(trim) >= 600);
                            }
                        } else {
                            z = true;
                        }
                    } else {
                        z = false;
                    }
                    if (z) {
                        return BaseResolver.m16774(mediaSource, false).m7402(48, TimeUnit.SECONDS, Observable.m7349()).m7375(Observable.m7349()).m7367().m7382(Schedulers.io());
                    }
                    BaseResolver r22 = BaseResolver.m16773(mediaSource.getStreamLink());
                    if (r22 == null || !BaseResolver.m16776(r22)) {
                        return Observable.m7349();
                    }
                    mediaSource.setHostName(r22.m16778());
                    if (mediaSource.getQuality() != null && mediaSource.getQuality().equalsIgnoreCase("HD") && r22.m16780() != null && r22.m16780().equalsIgnoreCase("HD")) {
                        mediaSource.setQuality("HD");
                    } else if (mediaSource.getQuality() == null || mediaSource.getQuality().isEmpty()) {
                        mediaSource.setQuality(r22.m16780());
                    } else {
                        mediaSource.setQuality(mediaSource.getQuality());
                    }
                    mediaSource.setResolved(false);
                } else {
                    mediaSource.setResolved(true);
                }
                return Observable.m7356(mediaSource);
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                if ((mediaSource.getQuality() == null || mediaSource.getQuality().isEmpty()) && mediaSource.getStreamLink() != null && !mediaSource.getStreamLink().isEmpty()) {
                    if (GoogleVideoHelper.m15955(mediaSource.getStreamLink())) {
                        mediaSource.setQuality(GoogleVideoHelper.m15949(mediaSource.getStreamLink()));
                        return mediaSource;
                    }
                    mediaSource.setQuality("HQ");
                    return mediaSource;
                } else if (mediaSource.getQuality() == null || mediaSource.getQuality().isEmpty()) {
                    return null;
                } else {
                    return mediaSource;
                }
            }
        }).m7385(new Func1<MediaSource, Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Boolean call(MediaSource mediaSource) {
                boolean z = true;
                if (mediaSource == null) {
                    return false;
                }
                if (!z4) {
                    return true;
                }
                String lowerCase = mediaSource.getQuality().trim().toLowerCase();
                if (lowerCase.equals("sd") || lowerCase.equals("hq") || (Utils.m6426(lowerCase.replace(TtmlNode.TAG_P, "")) && Integer.parseInt(lowerCase.replace(TtmlNode.TAG_P, "")) < 720)) {
                    z = false;
                }
                return Boolean.valueOf(z);
            }
        }).m7385(new Func1<MediaSource, Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Boolean call(MediaSource mediaSource) {
                if (z5) {
                    String providerName = mediaSource.getProviderName();
                    String hostName = mediaSource.getHostName();
                    if (providerName.contains("(CAM)") || hostName.contains("(CAM)")) {
                        return false;
                    }
                }
                return true;
            }
        }).m7385(new Func1<MediaSource, Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Boolean call(MediaSource mediaSource) {
                boolean z = false;
                if (mediaSource == null) {
                    return false;
                }
                String streamLink = mediaSource.getStreamLink();
                if (streamLink != null && !streamLink.trim().isEmpty() && (streamLink.trim().toLowerCase().startsWith("http://") || streamLink.trim().toLowerCase().startsWith("https://"))) {
                    z = true;
                }
                return Boolean.valueOf(z);
            }
        }).m7396(new Func1<MediaSource, Observable<MediaSource>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaSource> call(final MediaSource mediaSource) {
                return JuicyApi.m15732().m15733(mediaSource.getStreamLink()) ? Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void call(Subscriber<? super MediaSource> subscriber) {
                        try {
                            String r1 = HttpHelper.m6343().m6363(mediaSource.getStreamLink(), false, (Map<String, String>[]) new Map[0]);
                            mediaSource.setStreamLink(r1);
                            mediaSource.setQuality(GoogleVideoHelper.m15955(r1) ? GoogleVideoHelper.m15949(r1) : mediaSource.getQuality());
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        }
                        subscriber.onNext(mediaSource);
                        subscriber.onCompleted();
                    }
                }).m7376(new Func1<Throwable, MediaSource>() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public MediaSource call(Throwable th) {
                        Logger.m6281(th, true);
                        return mediaSource;
                    }
                }).m7367().m7382(Schedulers.io()) : Observable.m7356(mediaSource);
            }
        }).m7396(new Func1<MediaSource, Observable<MediaSource>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaSource> call(final MediaSource mediaSource) {
                String replace = mediaSource.getStreamLink().replace(StringUtils.LF, "");
                if (!GoogleVideoHelper.m15946(replace)) {
                    return Observable.m7356(mediaSource);
                }
                final String r0 = GoogleVideoHelper.m15944(replace);
                if (r0 == null) {
                    return Observable.m7356(mediaSource);
                }
                mediaSource.setHostName("GoogleVideo");
                return Observable.m7359(new Observable.OnSubscribe<MediaSource>() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void call(Subscriber<? super MediaSource> subscriber) {
                        try {
                            Response r0 = HttpHelper.m6343().m6366(r0);
                            if (r0 != null) {
                                if (!r0.m7065()) {
                                    subscriber.onNext(mediaSource);
                                } else if (HttpHelper.m6342(r0, false, new boolean[0]) > 20971520) {
                                    MediaSource cloneDeeply = mediaSource.cloneDeeply();
                                    cloneDeeply.setStreamLink(r0);
                                    cloneDeeply.setQuality(GoogleVideoHelper.m15949(r0));
                                    cloneDeeply.setHostName("GoogleVideo");
                                    subscriber.onNext(cloneDeeply);
                                } else {
                                    subscriber.onNext(mediaSource);
                                }
                                if (r0.m7056() != null) {
                                    r0.m7056().close();
                                }
                            } else {
                                subscriber.onNext(mediaSource);
                            }
                        } catch (Exception e) {
                            Logger.m6281((Throwable) e, new boolean[0]);
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                            subscriber.onNext(mediaSource);
                        }
                        subscriber.onCompleted();
                    }
                }).m7376(new Func1<Throwable, MediaSource>() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public MediaSource call(Throwable th) {
                        Logger.m6281(th, true);
                        return mediaSource;
                    }
                }).m7367().m7382(Schedulers.io());
            }
        }).m7396(new Func1<MediaSource, Observable<MediaSource>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaSource> call(MediaSource mediaSource) {
                String streamLink = mediaSource.getStreamLink();
                if (!VidCDNHelper.m15996(streamLink)) {
                    return Observable.m7356(mediaSource);
                }
                String r1 = VidCDNHelper.m15995(streamLink);
                if (r1 == null) {
                    return Observable.m7356(mediaSource);
                }
                MediaSource cloneDeeply = mediaSource.cloneDeeply();
                cloneDeeply.setStreamLink(r1);
                return Observable.m7357(mediaSource, cloneDeeply);
            }
        }).m7396(new Func1<MediaSource, Observable<MediaSource>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<MediaSource> call(MediaSource mediaSource) {
                String streamLink = mediaSource.getStreamLink();
                String r2 = Regex.m17801(streamLink, "(.*/SUB\\.\\d+\\.)(\\d+)(P\\..*)", 2, 34);
                if (r2.isEmpty()) {
                    return Observable.m7356(mediaSource);
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(mediaSource);
                for (String str : new String[]{"1080", "720", "480", "360", "240"}) {
                    try {
                        if (!r2.equalsIgnoreCase(str)) {
                            String replaceAll = streamLink.replaceAll("(?si)(.*/SUB\\.\\d+\\.)(\\d+)(P\\..*)", "$1" + str + "$3");
                            MediaSource cloneDeeply = mediaSource.cloneDeeply();
                            cloneDeeply.setStreamLink(replaceAll);
                            cloneDeeply.setQuality(str + TtmlNode.TAG_P);
                            arrayList.add(cloneDeeply);
                        }
                    } catch (Exception e) {
                        Logger.m6281((Throwable) e, new boolean[0]);
                    }
                }
                return Observable.m7355(arrayList).m7367();
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                String replace = mediaSource.getStreamLink().replace(StringUtils.LF, "");
                if (GoogleVideoHelper.m15955(replace)) {
                    replace = replace.replaceAll("\\?e=([^&]+)&", "?").replaceAll("\\?title=([^&]+)&", "?").replaceAll("\\?filename=([^&]+)&", "?").replaceAll("\\?e=([^&]+)$", "").replaceAll("\\?title=([^&]+)$", "").replaceAll("\\?filename=([^&]+)$", "").replaceAll("&e=([^&]+)", "").replaceAll("&title=([^&]+)", "").replaceAll("&filename=([^&]+)", "");
                } else if (replace.trim().toLowerCase().contains("amazonaws")) {
                    replace = replace.replace("?download=TRUE&", "?").replace("?download=TRUE", "").replace("?download=true&", "?").replace("?download=true", "").replace("&download=TRUE", "").replace("&download=true", "");
                }
                mediaSource.setStreamLink(replace);
                return mediaSource;
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                try {
                    String lowerCase = mediaSource.getStreamLink().trim().toLowerCase();
                    if (!Regex.m17800(lowerCase, "//[^/]*(ntcdn|micetop|cdn\\.vidnode)\\.[^/]{2,8}/", 1).isEmpty()) {
                        HashMap<String, String> playHeader = mediaSource.getPlayHeader();
                        if (playHeader == null) {
                            playHeader = new HashMap<>();
                        }
                        String str = "";
                        if (playHeader.containsKey("Referer")) {
                            str = playHeader.get("Referer");
                        } else if (playHeader.containsKey("referer")) {
                            str = playHeader.get("referer");
                        }
                        String lowerCase2 = str == null ? "" : str.toLowerCase();
                        if (lowerCase.contains("vidcdn_pro/") && !lowerCase2.contains("vidnode.net")) {
                            playHeader.put("Referer", "https://vidnode.net/");
                        } else if (lowerCase.contains("s7_ntcdn_us/") && !lowerCase2.contains("m4ufree.info")) {
                            playHeader.put("Referer", "http://m4ufree.info/");
                        }
                        if (!playHeader.containsKey(AbstractSpiCall.HEADER_USER_AGENT) && !playHeader.containsKey("user-agent")) {
                            playHeader.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                        }
                        mediaSource.setPlayHeader(playHeader);
                    } else if (!Regex.m17800(lowerCase, "//[^/]*(vidcdn)\\.[^/]{2,8}/", 1).isEmpty() && !lowerCase.contains(".m3u8")) {
                        HashMap<String, String> playHeader2 = mediaSource.getPlayHeader();
                        if (playHeader2 == null) {
                            playHeader2 = new HashMap<>();
                        }
                        String str2 = "";
                        if (playHeader2.containsKey("Referer")) {
                            str2 = playHeader2.get("Referer");
                        } else if (playHeader2.containsKey("referer")) {
                            str2 = playHeader2.get("referer");
                        }
                        if (!(str2 == null ? "" : str2.toLowerCase()).contains("vidnode.net")) {
                            playHeader2.put("Referer", "https://vidnode.net/");
                        }
                        if (!playHeader2.containsKey(AbstractSpiCall.HEADER_USER_AGENT) && !playHeader2.containsKey("user-agent")) {
                            playHeader2.put(AbstractSpiCall.HEADER_USER_AGENT, TyphoonApp.f5835);
                        }
                        mediaSource.setPlayHeader(playHeader2);
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                return mediaSource;
            }
        }).m7396(SourceObservableUtils.m17831(true)).m7385(new Func1<MediaSource, Boolean>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Boolean call(MediaSource mediaSource) {
                return Boolean.valueOf((mediaSource == null || mediaSource.getStreamLink() == null || mediaSource.getStreamLink().isEmpty()) ? false : true);
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                String lowerCase = mediaSource.getStreamLink().trim().toLowerCase();
                String hostName = mediaSource.getHostName();
                if (LimelightNetworksHelper.m15957(mediaSource.getStreamLink()) && !hostName.equalsIgnoreCase("LLCDN-FastServer")) {
                    mediaSource.setHostName("LLCDN-FastServer");
                } else if (lowerCase.contains("amazonaws") && !hostName.equalsIgnoreCase("AWS-FastServer") && !hostName.equalsIgnoreCase("AmazonDrive")) {
                    mediaSource.setHostName("AWS-FastServer");
                } else if (lowerCase.contains("fbcdn.") && !hostName.equalsIgnoreCase("FB-FastServer")) {
                    mediaSource.setHostName("FB-FastServer");
                } else if (lowerCase.contains("yandex") && !hostName.equalsIgnoreCase("Yandex-FastServer") && !hostName.equalsIgnoreCase("Yandex")) {
                    mediaSource.setHostName("Yandex-FastServer");
                } else if (VidCDNHelper.m15996(lowerCase) && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("ahcdn.") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("ntcdn.") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("micetop.") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("cdn.vidnode.") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("m4ukido.") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("akamaized") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("cloudfront.") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if ((lowerCase.contains("tunefiles") || lowerCase.contains("tune.pk")) && !hostName.equalsIgnoreCase("Tune.pk") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("mycdn.") && !hostName.equalsIgnoreCase("ok.ru") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("archive.org/") && !hostName.equalsIgnoreCase("CDN-FastServer")) {
                    mediaSource.setHostName("CDN-FastServer");
                } else if (lowerCase.contains("mediafire.") && !hostName.equalsIgnoreCase("MF-FastServer")) {
                    mediaSource.setHostName("MF-FastServer");
                } else if (lowerCase.contains("dropboxusercontent.com/") && !hostName.equalsIgnoreCase("DropBox-FastServer")) {
                    mediaSource.setHostName("DropBox-FastServer");
                }
                return mediaSource;
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                String streamLink = mediaSource.getStreamLink();
                if (!mediaSource.getHostName().contains("Google") && streamLink.contains("videoplayback")) {
                    String quality = mediaSource.getQuality();
                    if (quality.equalsIgnoreCase("HQ") || quality.equalsIgnoreCase("HD")) {
                        String r0 = GoogleVideoHelper.m15949(streamLink);
                        if (!r0.equalsIgnoreCase("HQ") && !r0.equalsIgnoreCase("HD")) {
                            mediaSource.setQuality(r0);
                        }
                    }
                }
                return mediaSource;
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                try {
                    String streamLink = mediaSource.getStreamLink();
                    String lowerCase = streamLink.trim().toLowerCase();
                    String quality = mediaSource.getQuality();
                    if ((quality.equalsIgnoreCase("HD") || quality.equalsIgnoreCase("HQ") || quality.equalsIgnoreCase("SD")) && (lowerCase.contains("googleapis") || lowerCase.contains("vidcdn.") || lowerCase.contains("ahcdn.") || lowerCase.contains("ntcdn.") || lowerCase.contains("micetop.") || lowerCase.contains("azmovies.") || lowerCase.contains("cdn.vidnode.") || GoogleVideoHelper.m15945(streamLink) || LimelightNetworksHelper.m15957(streamLink))) {
                        String r7 = Regex.m17802(streamLink.trim().replace(StringUtils.LF, ""), "(http(?:s)?://.*?)(?:\\?|$)", 1, true);
                        TyphoonTV.ParsedLinkModel r8 = z3 ? typhoonTV.m15979(r7) : typhoonTV.m15980(r7);
                        if (r8 == null || !typhoonTV.m15981(r8, mediaInfo3, i5, i6)) {
                            String r2 = Regex.m17800(r7, "[/\\.\\- ](\\d{3,4})p[\\.\\- ]", 1);
                            if (r2.isEmpty() && (lowerCase.contains("vidcdn.") || lowerCase.contains("ahcdn.") || lowerCase.contains("ntcdn.") || lowerCase.contains("micetop.") || lowerCase.contains("cdn.vidnode.") || lowerCase.contains("azmovies."))) {
                                String r0 = Regex.m17800(streamLink, "[/\\-\\.](\\d{3,4})p?\\.\\w{3,4}(?:$|\\?)", 1);
                                if (r0.equals("240") || r0.equals("360") || r0.equals("480") || r0.equals("720") || r0.equals("1080")) {
                                    r2 = r0;
                                }
                            }
                            if (!r2.isEmpty() && Utils.m6426(r2) && Integer.parseInt(r2) <= 1200 && Integer.parseInt(r2) >= 240) {
                                mediaSource.setQuality(r2 + TtmlNode.TAG_P);
                            }
                        } else {
                            String r4 = Regex.m17800(r8.m15985(), "\\s*(\\d+)\\s*", 1);
                            if (!r4.isEmpty() && Utils.m6426(r4) && Integer.parseInt(r4) <= 1200 && Integer.parseInt(r4) >= 240) {
                                mediaSource.setQuality(r4 + TtmlNode.TAG_P);
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                return mediaSource;
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                try {
                    if (mediaSource.isHLS()) {
                        String streamLink = mediaSource.getStreamLink();
                        String quality = mediaSource.getQuality();
                        if (quality.equalsIgnoreCase("HD") || quality.equalsIgnoreCase("HQ") || quality.equalsIgnoreCase("SD")) {
                            String r1 = Regex.m17800(streamLink, "\\-(\\d{3,4})p?\\.[A-Za-z0-9]{3,4}/", 1);
                            if (!r1.isEmpty() && Utils.m6426(r1) && Integer.parseInt(r1) <= 1200 && Integer.parseInt(r1) >= 240) {
                                mediaSource.setQuality(r1 + TtmlNode.TAG_P);
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                return mediaSource;
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                try {
                    if (mediaSource.isResolved() && mediaSource.isDebrid() && (mediaSource.getQuality().equalsIgnoreCase("HQ") || mediaSource.getQuality().equalsIgnoreCase("SD"))) {
                        String streamLink = mediaSource.getStreamLink();
                        long fileSize = mediaSource.getFileSize();
                        if (streamLink.toLowerCase().contains("real-debrid")) {
                            if ((streamLink.contains(".1080.") || streamLink.toLowerCase().contains("1080p")) && (fileSize < 0 || (!z3 ? fileSize >= 800000000 : fileSize >= C.NANOS_PER_SECOND))) {
                                mediaSource.setQuality("1080p");
                            } else if ((streamLink.contains(".720.") || streamLink.toLowerCase().contains("720p")) && (fileSize < 0 || (!z3 ? fileSize >= 400000000 : fileSize >= 800000000))) {
                                mediaSource.setQuality("720p");
                            } else if (fileSize >= 800000000) {
                                mediaSource.setQuality("HD");
                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                return mediaSource;
            }
        }).m7392(new Func1<MediaSource, MediaSource>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public MediaSource call(MediaSource mediaSource) {
                try {
                    if (mediaSource.isResolved() && !mediaSource.isDebrid() && ((mediaSource.getQuality().equalsIgnoreCase("HQ") || mediaSource.getQuality().equalsIgnoreCase("SD") || mediaSource.getQuality().equalsIgnoreCase("HD")) && mediaSource.getHostName() != null && (mediaSource.getHostName().equalsIgnoreCase("Openload") || mediaSource.getHostName().equalsIgnoreCase("Streamango")))) {
                        long fileSize = mediaSource.getFileSize();
                        if (!z3 ? fileSize >= C.NANOS_PER_SECOND : fileSize >= 1800000000) {
                            mediaSource.setQuality("1080p");
                        } else if (!mediaSource.getQuality().equalsIgnoreCase("HD") && (!z3 ? fileSize >= 700000000 : fileSize >= C.NANOS_PER_SECOND)) {
                            mediaSource.setQuality("HD");
                        }
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
                return mediaSource;
            }
        }).m7379((long) i3, TimeUnit.SECONDS).m7367().m7412((Action1<? super Throwable>) new Action1<Throwable>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Throwable th) {
                Logger.m6280(th, "An error occurred!", new boolean[0]);
            }
        }).m7407(AndroidSchedulers.m24520()).m7395((Action0) new Action0() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m16245() {
                SourcePresenterImpl.this.f12738.m17872();
            }
        }).m7386(new Subscriber<List<MediaSource>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(List<MediaSource> list) {
                SourcePresenterImpl.this.f12738.m17873(list);
            }
        });
    }
}
