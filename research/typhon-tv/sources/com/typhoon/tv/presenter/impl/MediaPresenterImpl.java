package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.model.media.movie.tmdb.TmdbMovieInfoResult;
import com.typhoon.tv.model.media.tv.tmdb.TmdbTvInfoResult;
import com.typhoon.tv.presenter.IMediaPresenter;
import com.typhoon.tv.view.IMediaInfoView;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MediaPresenterImpl implements IMediaPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12702;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public IMediaInfoView f12703;

    public MediaPresenterImpl(IMediaInfoView iMediaInfoView) {
        this.f12703 = iMediaInfoView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16188(final int i) {
        this.f12702 = Observable.m7359(new Observable.OnSubscribe<TmdbMovieInfoResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super TmdbMovieInfoResult> subscriber) {
                subscriber.onNext(TmdbApi.m15742().m15751(i));
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<TmdbMovieInfoResult>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, true);
                MediaPresenterImpl.this.f12703.m17854();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(TmdbMovieInfoResult tmdbMovieInfoResult) {
                if (tmdbMovieInfoResult != null) {
                    MediaPresenterImpl.this.f12703.m17856(tmdbMovieInfoResult);
                } else {
                    MediaPresenterImpl.this.f12703.m17854();
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16189() {
        if (this.f12702 != null && !this.f12702.isUnsubscribed()) {
            this.f12702.unsubscribe();
        }
        this.f12702 = null;
        this.f12703 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16190(final int i) {
        this.f12702 = Observable.m7359(new Observable.OnSubscribe<TmdbTvInfoResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super TmdbTvInfoResult> subscriber) {
                subscriber.onNext(TmdbApi.m15742().m15759(i));
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<TmdbTvInfoResult>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, true);
                MediaPresenterImpl.this.f12703.m17855();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(TmdbTvInfoResult tmdbTvInfoResult) {
                if (tmdbTvInfoResult != null) {
                    MediaPresenterImpl.this.f12703.m17857(tmdbTvInfoResult);
                } else {
                    MediaPresenterImpl.this.f12703.m17855();
                }
            }
        });
    }
}
