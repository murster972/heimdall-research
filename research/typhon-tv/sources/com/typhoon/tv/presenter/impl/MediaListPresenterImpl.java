package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.api.TraktApi;
import com.typhoon.tv.model.media.MediaApiResult;
import com.typhoon.tv.presenter.IBookmarkPresenter;
import com.typhoon.tv.presenter.IMediaListPresenter;
import com.typhoon.tv.view.IBookmarkView;
import com.typhoon.tv.view.IMediaListView;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MediaListPresenterImpl implements IMediaListPresenter {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public IMediaListView f12691;

    /* renamed from: 麤  reason: contains not printable characters */
    private Subscription f12692;

    /* renamed from: 齉  reason: contains not printable characters */
    private IBookmarkPresenter f12693;

    /* renamed from: 龘  reason: contains not printable characters */
    private final IBookmarkView f12694;

    public MediaListPresenterImpl(IMediaListView iMediaListView, IBookmarkView iBookmarkView) {
        this.f12691 = iMediaListView;
        this.f12694 = iBookmarkView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16182() {
        m16183();
        this.f12691 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16183() {
        if (this.f12692 != null && !this.f12692.isUnsubscribed()) {
            this.f12692.unsubscribe();
        }
        this.f12692 = null;
        if (this.f12693 != null) {
            this.f12693.m16125();
        }
        this.f12693 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16184(int i, int i2, int i3, int i4, final boolean z) {
        m16183();
        if (i2 == 0 || i2 == 0) {
            if (this.f12693 == null) {
                this.f12693 = new BookmarkPresenterImpl(this.f12694);
            }
            this.f12693.m16126(i);
            return;
        }
        final int i5 = i;
        final int i6 = i2;
        final int i7 = i3;
        final int i8 = i4;
        this.f12692 = Observable.m7359(new Observable.OnSubscribe<MediaApiResult>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super MediaApiResult> subscriber) {
                if (i5 != 0) {
                    MediaApiResult r0 = TmdbApi.m15742().m15748(i6, i7, i8);
                    if (r0 == null) {
                        subscriber.onError(new Exception("Result is null"));
                    } else {
                        subscriber.onNext(r0);
                    }
                } else if (i6 < 10 || i6 > 27) {
                    switch (i6) {
                        case 1:
                        case 6:
                        case 7:
                        case 8:
                            MediaApiResult r02 = TraktApi.m15764().m15768(i6, i7, i8);
                            if (r02 != null) {
                                subscriber.onNext(r02);
                                break;
                            } else {
                                subscriber.onError(new Exception());
                                break;
                            }
                        case 2:
                        case 5:
                        case 9:
                            MediaApiResult r1 = TmdbApi.m15742().m15755(i6, i8);
                            if (r1 != null) {
                                subscriber.onNext(r1);
                                break;
                            } else {
                                subscriber.onError(new Exception());
                                break;
                            }
                        default:
                            MediaApiResult r2 = TraktApi.m15764().m15768(i6, i7, i8);
                            if (r2 != null) {
                                subscriber.onNext(r2);
                                break;
                            } else {
                                subscriber.onError(new Exception());
                                break;
                            }
                    }
                } else {
                    MediaApiResult r03 = TmdbApi.m15742().m15756(i6, i7, i8);
                    if (r03 == null) {
                        subscriber.onError(new Exception());
                    } else {
                        subscriber.onNext(r03);
                    }
                }
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<MediaApiResult>() {
            public void onCompleted() {
                MediaListPresenterImpl.this.f12691.m17862();
            }

            public void onError(Throwable th) {
                Logger.m6280(th, "onError", new boolean[0]);
                MediaListPresenterImpl.this.f12691.m17860();
                MediaListPresenterImpl.this.f12691.m17858();
                MediaListPresenterImpl.this.f12691.m17862();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(MediaApiResult mediaApiResult) {
                if (z) {
                    MediaListPresenterImpl.this.f12691.m17861(mediaApiResult.getMediaInfoList());
                } else {
                    MediaListPresenterImpl.this.f12691.m17859(mediaApiResult.getMediaInfoList());
                }
                MediaListPresenterImpl.this.f12691.m17863(mediaApiResult.getTotalPage());
            }
        });
    }
}
