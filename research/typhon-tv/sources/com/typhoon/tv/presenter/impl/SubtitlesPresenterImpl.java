package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import com.typhoon.tv.model.SubtitlesInfo;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.presenter.ISubtitlesPresenter;
import com.typhoon.tv.subtitles.BaseSubtitlesService;
import com.typhoon.tv.subtitles.chinese.Makedie;
import com.typhoon.tv.subtitles.chinese.SubHD;
import com.typhoon.tv.subtitles.international.OpenSubtitles;
import com.typhoon.tv.subtitles.international.Subscene;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.ISubtitlesView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class SubtitlesPresenterImpl implements ISubtitlesPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12780;

    /* renamed from: 齉  reason: contains not printable characters */
    private Subscription f12781;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public ISubtitlesView f12782;

    public SubtitlesPresenterImpl(ISubtitlesView iSubtitlesView) {
        this.f12782 = iSubtitlesView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16255() {
        if (this.f12781 != null && !this.f12781.isUnsubscribed()) {
            this.f12781.unsubscribe();
            this.f12781 = null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m16256() {
        m16257();
        m16255();
        this.f12782 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16257() {
        if (this.f12780 != null && !this.f12780.isUnsubscribed()) {
            this.f12780.unsubscribe();
            this.f12780 = null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16258(int i, final String str) {
        final BaseSubtitlesService subHD;
        m16255();
        this.f12782.m17877();
        this.f12782.m17878();
        switch (i) {
            case 1:
                subHD = new Makedie();
                break;
            case 2:
                subHD = new SubHD();
                break;
            case 3:
                subHD = new OpenSubtitles();
                break;
            case 4:
                subHD = new Subscene();
                break;
            default:
                subHD = new OpenSubtitles();
                break;
        }
        this.f12781 = Observable.m7359(new Observable.OnSubscribe<ArrayList<String>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ArrayList<String>> subscriber) {
                ArrayList<String> r0 = subHD.m6377(str);
                if (r0 == null) {
                    subscriber.onError(new Exception("Subtitles download failed"));
                } else {
                    subscriber.onNext(r0);
                }
                subscriber.onCompleted();
            }
        }).m7392(new Func1<ArrayList<String>, ArrayList<String>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public ArrayList<String> call(ArrayList<String> arrayList) {
                ArrayList<String> arrayList2 = new ArrayList<>();
                Iterator<String> it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    String next = it2.next();
                    if (!next.trim().toLowerCase().endsWith(".nfo")) {
                        String r0 = Utils.m6405(next);
                        char c = 65535;
                        switch (r0.hashCode()) {
                            case 1467283:
                                if (r0.equals(".ass")) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 1484551:
                                if (r0.equals(".srt")) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case 1484563:
                                if (r0.equals(".ssa")) {
                                    c = 2;
                                    break;
                                }
                                break;
                            case 1487496:
                                if (r0.equals(".vtt")) {
                                    c = 6;
                                    break;
                                }
                                break;
                            case 1489193:
                                if (r0.equals(".xml")) {
                                    c = 5;
                                    break;
                                }
                                break;
                            case 45562920:
                                if (r0.equals(".dfxp")) {
                                    c = 4;
                                    break;
                                }
                                break;
                            case 46052685:
                                if (r0.equals(".ttml")) {
                                    c = 3;
                                    break;
                                }
                                break;
                        }
                        switch (c) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                arrayList2.add(next);
                                break;
                        }
                    }
                }
                return arrayList2;
            }
        }).m7376(new Func1<Throwable, ArrayList<String>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public ArrayList<String> call(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                return new ArrayList<>();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<ArrayList<String>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                SubtitlesPresenterImpl.this.f12782.m17877();
                SubtitlesPresenterImpl.this.f12782.m17874();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(ArrayList<String> arrayList) {
                SubtitlesPresenterImpl.this.f12782.m17877();
                if (arrayList.isEmpty()) {
                    SubtitlesPresenterImpl.this.f12782.m17875();
                } else {
                    SubtitlesPresenterImpl.this.f12782.m17879(arrayList);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16259(MediaInfo mediaInfo, int i, int i2, String str) {
        m16257();
        BaseSubtitlesService baseSubtitlesService = null;
        BaseSubtitlesService[] r1 = TyphoonApp.m6327();
        int length = r1.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                break;
            }
            BaseSubtitlesService baseSubtitlesService2 = r1[i3];
            if (baseSubtitlesService2.m6379().equals(str)) {
                baseSubtitlesService = baseSubtitlesService2;
                break;
            }
            i3++;
        }
        final BaseSubtitlesService baseSubtitlesService3 = baseSubtitlesService;
        final MediaInfo mediaInfo2 = mediaInfo;
        final int i4 = i;
        final int i5 = i2;
        this.f12780 = Observable.m7359(new Observable.OnSubscribe<List<SubtitlesInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super List<SubtitlesInfo>> subscriber) {
                if (baseSubtitlesService3 == null) {
                    subscriber.onCompleted();
                    return;
                }
                ArrayList<SubtitlesInfo> r0 = baseSubtitlesService3.m6382(mediaInfo2, i4, i5);
                if (r0 != null && !r0.isEmpty() && (baseSubtitlesService3.m6379().equals(new OpenSubtitles().m16834()) || baseSubtitlesService3.m6379().equals(new Subscene().m16843()))) {
                    Collections.sort(r0, new Comparator<SubtitlesInfo>() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public int compare(SubtitlesInfo subtitlesInfo, SubtitlesInfo subtitlesInfo2) {
                            int compareTo = subtitlesInfo.getLanguage().compareTo(subtitlesInfo2.getLanguage());
                            if (compareTo == 0 && (subtitlesInfo instanceof OpenSubtitles.OpenSubtitlesInfo) && (subtitlesInfo2 instanceof OpenSubtitles.OpenSubtitlesInfo)) {
                                compareTo = Utils.m6411(((OpenSubtitles.OpenSubtitlesInfo) subtitlesInfo2).m16836(), ((OpenSubtitles.OpenSubtitlesInfo) subtitlesInfo).m16836());
                            }
                            return compareTo == 0 ? subtitlesInfo.getName().compareTo(subtitlesInfo2.getName()) : compareTo;
                        }
                    });
                }
                subscriber.onNext(r0);
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<List<SubtitlesInfo>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                SubtitlesPresenterImpl.this.f12782.m17876();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(List<SubtitlesInfo> list) {
                SubtitlesPresenterImpl.this.f12782.m17880(list);
            }
        });
    }
}
