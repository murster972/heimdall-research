package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvEpisodeInfo;
import com.typhoon.tv.presenter.IEpisodePresenter;
import com.typhoon.tv.view.IEpisodeView;
import java.util.ArrayList;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EpisodePresenterImpl implements IEpisodePresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12685;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public IEpisodeView f12686;

    public EpisodePresenterImpl(IEpisodeView iEpisodeView) {
        this.f12686 = iEpisodeView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16176() {
        m16177();
        this.f12686 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16177() {
        if (this.f12685 != null && !this.f12685.isUnsubscribed()) {
            this.f12685.unsubscribe();
        }
        this.f12685 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16178(final MediaInfo mediaInfo, final int i) {
        m16177();
        this.f12685 = Observable.m7359(new Observable.OnSubscribe<ArrayList<TvEpisodeInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ArrayList<TvEpisodeInfo>> subscriber) {
                subscriber.onNext(TmdbApi.m15742().m15760(mediaInfo, Integer.valueOf(i), new boolean[0]));
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<ArrayList<TvEpisodeInfo>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, new boolean[0]);
                EpisodePresenterImpl.this.f12686.m17852();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(ArrayList<TvEpisodeInfo> arrayList) {
                EpisodePresenterImpl.this.f12686.m17853(arrayList);
            }
        });
    }
}
