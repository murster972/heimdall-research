package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.TVApplication;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.presenter.IBookmarkPresenter;
import com.typhoon.tv.utils.comparator.MediaAlphanumComparator;
import com.typhoon.tv.view.IBookmarkView;
import java.util.ArrayList;
import java.util.Collections;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class BookmarkPresenterImpl implements IBookmarkPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12676;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public IBookmarkView f12677;

    public BookmarkPresenterImpl(IBookmarkView iBookmarkView) {
        this.f12677 = iBookmarkView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16165() {
        if (this.f12676 != null && !this.f12676.isUnsubscribed()) {
            this.f12676.unsubscribe();
        }
        this.f12676 = null;
        this.f12677 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16166(final int i) {
        this.f12676 = Observable.m7359(new Observable.OnSubscribe<ArrayList<MediaInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ArrayList<MediaInfo>> subscriber) {
                subscriber.onNext(TVApplication.m6287().m6312(Integer.valueOf(i)));
                subscriber.onCompleted();
            }
        }).m7392(new Func1<ArrayList<MediaInfo>, ArrayList<MediaInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public ArrayList<MediaInfo> call(ArrayList<MediaInfo> arrayList) {
                Collections.sort(arrayList, new MediaAlphanumComparator());
                return arrayList;
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7397(new Action1<ArrayList<MediaInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(ArrayList<MediaInfo> arrayList) {
                BookmarkPresenterImpl.this.f12677.m17846(arrayList);
            }
        });
    }
}
