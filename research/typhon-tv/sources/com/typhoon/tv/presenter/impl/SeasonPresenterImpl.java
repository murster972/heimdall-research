package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.api.TmdbApi;
import com.typhoon.tv.model.media.MediaInfo;
import com.typhoon.tv.model.media.tv.TvSeasonInfo;
import com.typhoon.tv.presenter.ISeasonPresenter;
import com.typhoon.tv.view.ISeasonView;
import java.util.ArrayList;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SeasonPresenterImpl implements ISeasonPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private Subscription f12732;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public ISeasonView f12733;

    public SeasonPresenterImpl(ISeasonView iSeasonView) {
        this.f12733 = iSeasonView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16217() {
        m16218();
        this.f12733 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16218() {
        if (this.f12732 != null && !this.f12732.isUnsubscribed()) {
            this.f12732.unsubscribe();
        }
        this.f12732 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16219(final MediaInfo mediaInfo) {
        m16218();
        this.f12732 = Observable.m7359(new Observable.OnSubscribe<ArrayList<TvSeasonInfo>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super ArrayList<TvSeasonInfo>> subscriber) {
                subscriber.onNext(TmdbApi.m15742().m15761(mediaInfo, new boolean[0]));
                subscriber.onCompleted();
            }
        }).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<ArrayList<TvSeasonInfo>>() {
            public void onCompleted() {
            }

            public void onError(Throwable th) {
                Logger.m6281(th, true);
                SeasonPresenterImpl.this.f12733.m17870();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(ArrayList<TvSeasonInfo> arrayList) {
                SeasonPresenterImpl.this.f12733.m17871(arrayList);
            }
        });
    }
}
