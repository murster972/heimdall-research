package com.typhoon.tv.presenter.impl;

import com.typhoon.tv.Logger;
import com.typhoon.tv.YesPlayerDownloader;
import com.typhoon.tv.helper.http.HttpHelper;
import com.typhoon.tv.presenter.IDownloadYPPresenter;
import com.typhoon.tv.utils.Utils;
import com.typhoon.tv.view.IDownloadYPView;
import java.io.File;
import java.util.concurrent.TimeUnit;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DownloadYPPresenterImpl implements IDownloadYPPresenter {

    /* renamed from: 靐  reason: contains not printable characters */
    private CompositeSubscription f12682;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public IDownloadYPView f12683;

    public DownloadYPPresenterImpl(IDownloadYPView iDownloadYPView) {
        this.f12683 = iDownloadYPView;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m16171() {
        m16172();
        this.f12683 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16172() {
        if (this.f12682 != null) {
            this.f12682.unsubscribe();
        }
        this.f12682 = null;
        HttpHelper.m6343().m6370((Object) "downloadYesPlayer");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m16173(boolean z) {
        m16172();
        this.f12683.m17851(z);
        File file = new File(YesPlayerDownloader.f12500);
        if (!file.exists() && !file.mkdirs()) {
            file.mkdir();
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.getName().trim().toLowerCase().endsWith(".apk")) {
                    file2.delete();
                }
            }
        }
        try {
            Utils.m6421(new File(YesPlayerDownloader.f12501));
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
        }
        this.f12682 = new CompositeSubscription();
        this.f12682.m25086(YesPlayerDownloader.m15721().m7394(1, TimeUnit.SECONDS).m7382(Schedulers.io()).m7407(AndroidSchedulers.m24520()).m7386(new Subscriber<Integer>() {
            public void onCompleted() {
                DownloadYPPresenterImpl.this.f12683.m17847();
                DownloadYPPresenterImpl.this.f12683.m17848();
            }

            public void onError(Throwable th) {
                DownloadYPPresenterImpl.this.f12683.m17847();
                DownloadYPPresenterImpl.this.f12683.m17850(th);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(Integer num) {
                DownloadYPPresenterImpl.this.f12683.m17849(num);
            }
        }));
    }
}
