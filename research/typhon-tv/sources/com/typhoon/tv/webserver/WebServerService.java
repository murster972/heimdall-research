package com.typhoon.tv.webserver;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import com.typhoon.tv.Logger;
import com.typhoon.tv.chromecast.CastHelper;

public class WebServerService extends Service {
    /* renamed from: 靐  reason: contains not printable characters */
    private void m17901() {
        WebServerManager.m17893().m17895();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m17902() {
        boolean r0 = WebServerManager.m17893().m17897();
        if (!r0) {
            Logger.m6281((Throwable) new RuntimeException("Failed to start subtitles server..."), true);
        }
        return r0;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
        m17901();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (m17902() && intent != null) {
            try {
                if (intent.getExtras() != null) {
                    Bundle extras = intent.getExtras();
                    if (!extras.isEmpty() && extras.containsKey("isNeededToRefreshTracks") && extras.getBoolean("isNeededToRefreshTracks", false) && extras.containsKey("videoAndSubTrackIdArray") && extras.containsKey("videoOnlyTrackIdArray")) {
                        CastHelper.m15835(this, extras.getLongArray("videoAndSubTrackIdArray"), extras.getLongArray("videoOnlyTrackIdArray"));
                    }
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, true);
            }
        }
        return 1;
    }
}
