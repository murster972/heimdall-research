package com.typhoon.tv.webserver;

import com.google.android.exoplayer2.util.MimeTypes;
import com.typhoon.tv.Logger;
import fi.iki.elonen.NanoHTTPD;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.oltu.oauth2.common.OAuth;

public class CastSubtitlesWebServer extends NanoHTTPD {

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, String> f14058;

    public CastSubtitlesWebServer(int i) {
        super(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public NanoHTTPD.Response m17890(NanoHTTPD.IHTTPSession iHTTPSession) {
        if (this.f14058 == null || this.f14058.isEmpty()) {
            Logger.m6281((Throwable) new RuntimeException("mSubsMap is null"), true);
            return m6657(NanoHTTPD.Response.Status.NOT_FOUND, "text/plain", "");
        }
        String r2 = iHTTPSession.m6689();
        if (r2.startsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)) {
            r2 = r2.substring(1, r2.length());
        }
        if (!r2.endsWith(".ttml") && r2.contains(".ttml")) {
            r2 = r2.substring(0, r2.lastIndexOf(".ttml"));
        }
        String str = null;
        if (this.f14058.containsKey(r2)) {
            str = this.f14058.get(r2);
        }
        if (str == null || str.isEmpty()) {
            Logger.m6281((Throwable) new RuntimeException("mSubsMap doesn't contain the corresponding subtitles key"), true);
            return m6657(NanoHTTPD.Response.Status.NOT_FOUND, "text/plain", "");
        }
        NanoHTTPD.Response r0 = m6657(NanoHTTPD.Response.Status.OK, MimeTypes.APPLICATION_TTML, str);
        r0.m18889("Access-Control-Allow-Origin", "*");
        r0.m18889(OAuth.HeaderType.CONTENT_TYPE, "application/ttml+xml; charset=utf-8");
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Map<String, String> m17891() {
        return this.f14058;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17892(Map<String, String> map) {
        this.f14058 = map;
    }
}
