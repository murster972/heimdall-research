package com.typhoon.tv.webserver;

import com.typhoon.tv.Logger;
import fi.iki.elonen.NanoHTTPD;
import java.util.Map;

public class WebServerManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile WebServerManager f14059;

    /* renamed from: 靐  reason: contains not printable characters */
    private NanoHTTPD f14060;

    /* renamed from: 龘  reason: contains not printable characters */
    public static WebServerManager m17893() {
        WebServerManager webServerManager = f14059;
        if (webServerManager == null) {
            synchronized (WebServerManager.class) {
                try {
                    webServerManager = f14059;
                    if (webServerManager == null) {
                        WebServerManager webServerManager2 = new WebServerManager();
                        try {
                            f14059 = webServerManager2;
                            webServerManager = webServerManager2;
                        } catch (Throwable th) {
                            th = th;
                            WebServerManager webServerManager3 = webServerManager2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return webServerManager;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Map<String, String> m17894() {
        if (this.f14060 != null && (this.f14060 instanceof CastSubtitlesWebServer)) {
            return ((CastSubtitlesWebServer) this.f14060).m17891();
        }
        throw new IllegalArgumentException("mServer has not yet initialized or not a CastSubtitlesWebServer");
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m17895() {
        if (this.f14060 != null && this.f14060.m6662()) {
            this.f14060.m6663();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public NanoHTTPD m17896() {
        return this.f14060;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m17897() {
        if (this.f14060 != null) {
            try {
                if (this.f14060.m6662()) {
                    return true;
                }
                this.f14060.m6669(45000, true);
                return true;
            } catch (Exception e) {
                Logger.m6281((Throwable) e, true);
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m17898() {
        return this.f14060 != null && this.f14060.m6662();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17899(NanoHTTPD nanoHTTPD) {
        if (this.f14060 != null) {
            this.f14060.m6663();
        }
        this.f14060 = nanoHTTPD;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17900(Map<String, String> map) {
        if (this.f14060 != null && (this.f14060 instanceof CastSubtitlesWebServer)) {
            ((CastSubtitlesWebServer) this.f14060).m17892(map);
        }
    }
}
