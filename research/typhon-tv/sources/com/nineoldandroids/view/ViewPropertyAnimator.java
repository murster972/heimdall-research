package com.nineoldandroids.view;

import android.os.Build;
import android.view.View;
import com.nineoldandroids.animation.Animator;
import java.util.WeakHashMap;

public abstract class ViewPropertyAnimator {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final WeakHashMap<View, ViewPropertyAnimator> f11655 = new WeakHashMap<>(0);

    /* renamed from: 龘  reason: contains not printable characters */
    public static ViewPropertyAnimator m14660(View view) {
        ViewPropertyAnimator viewPropertyAnimator = f11655.get(view);
        if (viewPropertyAnimator == null) {
            int intValue = Integer.valueOf(Build.VERSION.SDK).intValue();
            viewPropertyAnimator = intValue >= 14 ? new ViewPropertyAnimatorICS(view) : intValue >= 11 ? new ViewPropertyAnimatorHC(view) : new ViewPropertyAnimatorPreHC(view);
            f11655.put(view, viewPropertyAnimator);
        }
        return viewPropertyAnimator;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public abstract ViewPropertyAnimator m14661(float f);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ViewPropertyAnimator m14662(float f);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ViewPropertyAnimator m14663(long j);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ViewPropertyAnimator m14664(Animator.AnimatorListener animatorListener);
}
