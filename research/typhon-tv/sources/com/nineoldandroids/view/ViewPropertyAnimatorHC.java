package com.nineoldandroids.view;

import android.support.v4.app.FrameMetricsAggregator;
import android.view.View;
import android.view.animation.Interpolator;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ValueAnimator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class ViewPropertyAnimatorHC extends ViewPropertyAnimator {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f11656 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Interpolator f11657;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f11658 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public HashMap<Animator, PropertyBundle> f11659 = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public Animator.AnimatorListener f11660 = null;

    /* renamed from: ٴ  reason: contains not printable characters */
    private AnimatorEventListener f11661 = new AnimatorEventListener();

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Runnable f11662 = new Runnable() {
        public void run() {
            ViewPropertyAnimatorHC.this.m14671();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private long f11663 = 0;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final WeakReference<View> f11664;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f11665 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f11666;

    /* renamed from: 龘  reason: contains not printable characters */
    ArrayList<NameValuesHolder> f11667 = new ArrayList<>();

    private class AnimatorEventListener implements Animator.AnimatorListener, ValueAnimator.AnimatorUpdateListener {
        private AnimatorEventListener() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m14680(Animator animator) {
            if (ViewPropertyAnimatorHC.this.f11660 != null) {
                ViewPropertyAnimatorHC.this.f11660.m14553(animator);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m14681(Animator animator) {
            if (ViewPropertyAnimatorHC.this.f11660 != null) {
                ViewPropertyAnimatorHC.this.f11660.m14554(animator);
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m14682(Animator animator) {
            if (ViewPropertyAnimatorHC.this.f11660 != null) {
                ViewPropertyAnimatorHC.this.f11660.m14555(animator);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14683(Animator animator) {
            if (ViewPropertyAnimatorHC.this.f11660 != null) {
                ViewPropertyAnimatorHC.this.f11660.m14556(animator);
            }
            ViewPropertyAnimatorHC.this.f11659.remove(animator);
            if (ViewPropertyAnimatorHC.this.f11659.isEmpty()) {
                Animator.AnimatorListener unused = ViewPropertyAnimatorHC.this.f11660 = null;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14684(ValueAnimator valueAnimator) {
            View view;
            float r1 = valueAnimator.m14633();
            PropertyBundle propertyBundle = (PropertyBundle) ViewPropertyAnimatorHC.this.f11659.get(valueAnimator);
            if (!((propertyBundle.f11674 & FrameMetricsAggregator.EVERY_DURATION) == 0 || (view = (View) ViewPropertyAnimatorHC.this.f11664.get()) == null)) {
                view.invalidate();
            }
            ArrayList<NameValuesHolder> arrayList = propertyBundle.f11673;
            if (arrayList != null) {
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    NameValuesHolder nameValuesHolder = arrayList.get(i);
                    ViewPropertyAnimatorHC.this.m14666(nameValuesHolder.f11672, nameValuesHolder.f11670 + (nameValuesHolder.f11671 * r1));
                }
            }
            View view2 = (View) ViewPropertyAnimatorHC.this.f11664.get();
            if (view2 != null) {
                view2.invalidate();
            }
        }
    }

    private static class NameValuesHolder {

        /* renamed from: 靐  reason: contains not printable characters */
        float f11670;

        /* renamed from: 齉  reason: contains not printable characters */
        float f11671;

        /* renamed from: 龘  reason: contains not printable characters */
        int f11672;

        NameValuesHolder(int i, float f, float f2) {
            this.f11672 = i;
            this.f11670 = f;
            this.f11671 = f2;
        }
    }

    private static class PropertyBundle {

        /* renamed from: 靐  reason: contains not printable characters */
        ArrayList<NameValuesHolder> f11673;

        /* renamed from: 龘  reason: contains not printable characters */
        int f11674;

        PropertyBundle(int i, ArrayList<NameValuesHolder> arrayList) {
            this.f11674 = i;
            this.f11673 = arrayList;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m14685(int i) {
            if (!((this.f11674 & i) == 0 || this.f11673 == null)) {
                int size = this.f11673.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (this.f11673.get(i2).f11672 == i) {
                        this.f11673.remove(i2);
                        this.f11674 &= i ^ -1;
                        return true;
                    }
                }
            }
            return false;
        }
    }

    ViewPropertyAnimatorHC(View view) {
        this.f11664 = new WeakReference<>(view);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m14666(int i, float f) {
        View view = (View) this.f11664.get();
        if (view != null) {
            switch (i) {
                case 1:
                    view.setTranslationX(f);
                    return;
                case 2:
                    view.setTranslationY(f);
                    return;
                case 4:
                    view.setScaleX(f);
                    return;
                case 8:
                    view.setScaleY(f);
                    return;
                case 16:
                    view.setRotation(f);
                    return;
                case 32:
                    view.setRotationX(f);
                    return;
                case 64:
                    view.setRotationY(f);
                    return;
                case 128:
                    view.setX(f);
                    return;
                case 256:
                    view.setY(f);
                    return;
                case 512:
                    view.setAlpha(f);
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private float m14669(int i) {
        View view = (View) this.f11664.get();
        if (view != null) {
            switch (i) {
                case 1:
                    return view.getTranslationX();
                case 2:
                    return view.getTranslationY();
                case 4:
                    return view.getScaleX();
                case 8:
                    return view.getScaleY();
                case 16:
                    return view.getRotation();
                case 32:
                    return view.getRotationX();
                case 64:
                    return view.getRotationY();
                case 128:
                    return view.getX();
                case 256:
                    return view.getY();
                case 512:
                    return view.getAlpha();
            }
        }
        return 0.0f;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14671() {
        ValueAnimator r0 = ValueAnimator.m14625(1.0f);
        ArrayList arrayList = (ArrayList) this.f11667.clone();
        this.f11667.clear();
        int i = 0;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            i |= ((NameValuesHolder) arrayList.get(i2)).f11672;
        }
        this.f11659.put(r0, new PropertyBundle(i, arrayList));
        r0.m14648((ValueAnimator.AnimatorUpdateListener) this.f11661);
        r0.m14552(this.f11661);
        if (this.f11656) {
            r0.m14643(this.f11663);
        }
        if (this.f11665) {
            r0.m14644(this.f11666);
        }
        if (this.f11658) {
            r0.m14647(this.f11657);
        }
        r0.m14645();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14672(int i, float f) {
        float r1 = m14669(i);
        m14673(i, r1, f - r1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14673(int i, float f, float f2) {
        if (this.f11659.size() > 0) {
            Animator animator = null;
            Iterator<Animator> it2 = this.f11659.keySet().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Animator next = it2.next();
                PropertyBundle propertyBundle = this.f11659.get(next);
                if (propertyBundle.m14685(i) && propertyBundle.f11674 == 0) {
                    animator = next;
                    break;
                }
            }
            if (animator != null) {
                animator.m14549();
            }
        }
        this.f11667.add(new NameValuesHolder(i, f, f2));
        View view = (View) this.f11664.get();
        if (view != null) {
            view.removeCallbacks(this.f11662);
            view.post(this.f11662);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ViewPropertyAnimator m14676(float f) {
        m14672(512, f);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14677(float f) {
        m14672(1, f);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14678(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("Animators cannot have negative duration: " + j);
        }
        this.f11665 = true;
        this.f11666 = j;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14679(Animator.AnimatorListener animatorListener) {
        this.f11660 = animatorListener;
        return this;
    }
}
