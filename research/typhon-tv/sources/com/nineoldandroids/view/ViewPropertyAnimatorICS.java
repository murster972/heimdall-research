package com.nineoldandroids.view;

import android.animation.Animator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import com.nineoldandroids.animation.Animator;
import java.lang.ref.WeakReference;

class ViewPropertyAnimatorICS extends ViewPropertyAnimator {

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<ViewPropertyAnimator> f11675;

    ViewPropertyAnimatorICS(View view) {
        this.f11675 = new WeakReference<>(view.animate());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ViewPropertyAnimator m14686(float f) {
        ViewPropertyAnimator viewPropertyAnimator = (ViewPropertyAnimator) this.f11675.get();
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.alpha(f);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14687(float f) {
        ViewPropertyAnimator viewPropertyAnimator = (ViewPropertyAnimator) this.f11675.get();
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.translationX(f);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14688(long j) {
        ViewPropertyAnimator viewPropertyAnimator = (ViewPropertyAnimator) this.f11675.get();
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.setDuration(j);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14689(final Animator.AnimatorListener animatorListener) {
        ViewPropertyAnimator viewPropertyAnimator = (ViewPropertyAnimator) this.f11675.get();
        if (viewPropertyAnimator != null) {
            if (animatorListener == null) {
                viewPropertyAnimator.setListener((Animator.AnimatorListener) null);
            } else {
                viewPropertyAnimator.setListener(new Animator.AnimatorListener() {
                    public void onAnimationCancel(android.animation.Animator animator) {
                        animatorListener.m14555((com.nineoldandroids.animation.Animator) null);
                    }

                    public void onAnimationEnd(android.animation.Animator animator) {
                        animatorListener.m14556((com.nineoldandroids.animation.Animator) null);
                    }

                    public void onAnimationRepeat(android.animation.Animator animator) {
                        animatorListener.m14554((com.nineoldandroids.animation.Animator) null);
                    }

                    public void onAnimationStart(android.animation.Animator animator) {
                        animatorListener.m14553((com.nineoldandroids.animation.Animator) null);
                    }
                });
            }
        }
        return this;
    }
}
