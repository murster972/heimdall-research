package com.nineoldandroids.view;

import android.view.View;
import com.nineoldandroids.view.animation.AnimatorProxy;

public final class ViewHelper {

    private static final class Honeycomb {
        /* renamed from: 靐  reason: contains not printable characters */
        static void m14658(View view, float f) {
            view.setTranslationX(f);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static void m14659(View view, float f) {
            view.setAlpha(f);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m14656(View view, float f) {
        if (AnimatorProxy.f11699) {
            AnimatorProxy.m14713(view).m14719(f);
        } else {
            Honeycomb.m14658(view, f);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m14657(View view, float f) {
        if (AnimatorProxy.f11699) {
            AnimatorProxy.m14713(view).m14735(f);
        } else {
            Honeycomb.m14659(view, f);
        }
    }
}
