package com.nineoldandroids.view.animation;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public final class AnimatorProxy extends Animation {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final WeakHashMap<View, AnimatorProxy> f11698 = new WeakHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final boolean f11699 = (Integer.valueOf(Build.VERSION.SDK).intValue() < 11);

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f11700 = 1.0f;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f11701;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f11702;

    /* renamed from: ʾ  reason: contains not printable characters */
    private float f11703 = 1.0f;

    /* renamed from: ʿ  reason: contains not printable characters */
    private float f11704;

    /* renamed from: ˈ  reason: contains not printable characters */
    private float f11705 = 1.0f;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final RectF f11706 = new RectF();

    /* renamed from: ˋ  reason: contains not printable characters */
    private final Matrix f11707 = new Matrix();

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f11708;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f11709;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private float f11710;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f11711;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Camera f11712 = new Camera();

    /* renamed from: 齉  reason: contains not printable characters */
    private final WeakReference<View> f11713;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private float f11714;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final RectF f11715 = new RectF();

    private AnimatorProxy(View view) {
        setDuration(0);
        setFillAfter(true);
        view.setAnimation(this);
        this.f11713 = new WeakReference<>(view);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m14711() {
        View view = (View) this.f11713.get();
        if (view != null && view.getParent() != null) {
            RectF rectF = this.f11706;
            m14715(rectF, view);
            rectF.union(this.f11715);
            ((View) view.getParent()).invalidate((int) Math.floor((double) rectF.left), (int) Math.floor((double) rectF.top), (int) Math.ceil((double) rectF.right), (int) Math.ceil((double) rectF.bottom));
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m14712() {
        View view = (View) this.f11713.get();
        if (view != null) {
            m14715(this.f11715, view);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static AnimatorProxy m14713(View view) {
        AnimatorProxy animatorProxy = f11698.get(view);
        if (animatorProxy != null && animatorProxy == view.getAnimation()) {
            return animatorProxy;
        }
        AnimatorProxy animatorProxy2 = new AnimatorProxy(view);
        f11698.put(view, animatorProxy2);
        return animatorProxy2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14714(Matrix matrix, View view) {
        float width = (float) view.getWidth();
        float height = (float) view.getHeight();
        boolean z = this.f11711;
        float f = z ? this.f11701 : width / 2.0f;
        float f2 = z ? this.f11702 : height / 2.0f;
        float f3 = this.f11708;
        float f4 = this.f11709;
        float f5 = this.f11710;
        if (!(f3 == 0.0f && f4 == 0.0f && f5 == 0.0f)) {
            Camera camera = this.f11712;
            camera.save();
            camera.rotateX(f3);
            camera.rotateY(f4);
            camera.rotateZ(-f5);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-f, -f2);
            matrix.postTranslate(f, f2);
        }
        float f6 = this.f11705;
        float f7 = this.f11703;
        if (!(f6 == 1.0f && f7 == 1.0f)) {
            matrix.postScale(f6, f7);
            matrix.postTranslate((-(f / width)) * ((f6 * width) - width), (-(f2 / height)) * ((f7 * height) - height));
        }
        matrix.postTranslate(this.f11704, this.f11714);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14715(RectF rectF, View view) {
        rectF.set(0.0f, 0.0f, (float) view.getWidth(), (float) view.getHeight());
        Matrix matrix = this.f11707;
        matrix.reset();
        m14714(matrix, view);
        this.f11707.mapRect(rectF);
        rectF.offset((float) view.getLeft(), (float) view.getTop());
        if (rectF.right < rectF.left) {
            float f = rectF.right;
            rectF.right = rectF.left;
            rectF.left = f;
        }
        if (rectF.bottom < rectF.top) {
            float f2 = rectF.top;
            rectF.top = rectF.bottom;
            rectF.bottom = f2;
        }
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        View view = (View) this.f11713.get();
        if (view != null) {
            transformation.setAlpha(this.f11700);
            m14714(transformation.getMatrix(), view);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public float m14716() {
        return this.f11703;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m14717(float f) {
        if (this.f11703 != f) {
            m14712();
            this.f11703 = f;
            m14711();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public float m14718() {
        return this.f11704;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m14719(float f) {
        if (this.f11704 != f) {
            m14712();
            this.f11704 = f;
            m14711();
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public float m14720() {
        return this.f11714;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m14721(float f) {
        if (this.f11714 != f) {
            m14712();
            this.f11714 = f;
            m14711();
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public float m14722() {
        View view = (View) this.f11713.get();
        if (view == null) {
            return 0.0f;
        }
        return ((float) view.getLeft()) + this.f11704;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m14723(float f) {
        View view = (View) this.f11713.get();
        if (view != null) {
            m14719(f - ((float) view.getLeft()));
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public float m14724() {
        View view = (View) this.f11713.get();
        if (view == null) {
            return 0.0f;
        }
        return ((float) view.getTop()) + this.f11714;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m14725(float f) {
        View view = (View) this.f11713.get();
        if (view != null) {
            m14721(f - ((float) view.getTop()));
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public float m14726() {
        return this.f11705;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m14727(float f) {
        if (this.f11705 != f) {
            m14712();
            this.f11705 = f;
            m14711();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public float m14728() {
        return this.f11710;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14729(float f) {
        if (this.f11710 != f) {
            m14712();
            this.f11710 = f;
            m14711();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public float m14730() {
        return this.f11709;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m14731(float f) {
        if (this.f11709 != f) {
            m14712();
            this.f11709 = f;
            m14711();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public float m14732() {
        return this.f11708;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m14733(float f) {
        if (this.f11708 != f) {
            m14712();
            this.f11708 = f;
            m14711();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public float m14734() {
        return this.f11700;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14735(float f) {
        if (this.f11700 != f) {
            this.f11700 = f;
            View view = (View) this.f11713.get();
            if (view != null) {
                view.invalidate();
            }
        }
    }
}
