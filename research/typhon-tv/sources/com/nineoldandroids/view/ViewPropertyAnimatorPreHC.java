package com.nineoldandroids.view;

import android.support.v4.app.FrameMetricsAggregator;
import android.view.View;
import android.view.animation.Interpolator;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.animation.AnimatorProxy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class ViewPropertyAnimatorPreHC extends ViewPropertyAnimator {

    /* renamed from: ʻ  reason: contains not printable characters */
    private long f11678 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f11679 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Interpolator f11680;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public HashMap<Animator, PropertyBundle> f11681 = new HashMap<>();

    /* renamed from: ˈ  reason: contains not printable characters */
    private Runnable f11682 = new Runnable() {
        public void run() {
            ViewPropertyAnimatorPreHC.this.m14696();
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f11683 = false;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public Animator.AnimatorListener f11684 = null;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private AnimatorEventListener f11685 = new AnimatorEventListener();

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f11686 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final AnimatorProxy f11687;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f11688;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final WeakReference<View> f11689;

    /* renamed from: 龘  reason: contains not printable characters */
    ArrayList<NameValuesHolder> f11690 = new ArrayList<>();

    private class AnimatorEventListener implements Animator.AnimatorListener, ValueAnimator.AnimatorUpdateListener {
        private AnimatorEventListener() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m14705(Animator animator) {
            if (ViewPropertyAnimatorPreHC.this.f11684 != null) {
                ViewPropertyAnimatorPreHC.this.f11684.m14553(animator);
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public void m14706(Animator animator) {
            if (ViewPropertyAnimatorPreHC.this.f11684 != null) {
                ViewPropertyAnimatorPreHC.this.f11684.m14554(animator);
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m14707(Animator animator) {
            if (ViewPropertyAnimatorPreHC.this.f11684 != null) {
                ViewPropertyAnimatorPreHC.this.f11684.m14555(animator);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14708(Animator animator) {
            if (ViewPropertyAnimatorPreHC.this.f11684 != null) {
                ViewPropertyAnimatorPreHC.this.f11684.m14556(animator);
            }
            ViewPropertyAnimatorPreHC.this.f11681.remove(animator);
            if (ViewPropertyAnimatorPreHC.this.f11681.isEmpty()) {
                Animator.AnimatorListener unused = ViewPropertyAnimatorPreHC.this.f11684 = null;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14709(ValueAnimator valueAnimator) {
            View view;
            float r1 = valueAnimator.m14633();
            PropertyBundle propertyBundle = (PropertyBundle) ViewPropertyAnimatorPreHC.this.f11681.get(valueAnimator);
            if (!((propertyBundle.f11697 & FrameMetricsAggregator.EVERY_DURATION) == 0 || (view = (View) ViewPropertyAnimatorPreHC.this.f11689.get()) == null)) {
                view.invalidate();
            }
            ArrayList<NameValuesHolder> arrayList = propertyBundle.f11696;
            if (arrayList != null) {
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    NameValuesHolder nameValuesHolder = arrayList.get(i);
                    ViewPropertyAnimatorPreHC.this.m14691(nameValuesHolder.f11695, nameValuesHolder.f11693 + (nameValuesHolder.f11694 * r1));
                }
            }
            View view2 = (View) ViewPropertyAnimatorPreHC.this.f11689.get();
            if (view2 != null) {
                view2.invalidate();
            }
        }
    }

    private static class NameValuesHolder {

        /* renamed from: 靐  reason: contains not printable characters */
        float f11693;

        /* renamed from: 齉  reason: contains not printable characters */
        float f11694;

        /* renamed from: 龘  reason: contains not printable characters */
        int f11695;

        NameValuesHolder(int i, float f, float f2) {
            this.f11695 = i;
            this.f11693 = f;
            this.f11694 = f2;
        }
    }

    private static class PropertyBundle {

        /* renamed from: 靐  reason: contains not printable characters */
        ArrayList<NameValuesHolder> f11696;

        /* renamed from: 龘  reason: contains not printable characters */
        int f11697;

        PropertyBundle(int i, ArrayList<NameValuesHolder> arrayList) {
            this.f11697 = i;
            this.f11696 = arrayList;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m14710(int i) {
            if (!((this.f11697 & i) == 0 || this.f11696 == null)) {
                int size = this.f11696.size();
                for (int i2 = 0; i2 < size; i2++) {
                    if (this.f11696.get(i2).f11695 == i) {
                        this.f11696.remove(i2);
                        this.f11697 &= i ^ -1;
                        return true;
                    }
                }
            }
            return false;
        }
    }

    ViewPropertyAnimatorPreHC(View view) {
        this.f11689 = new WeakReference<>(view);
        this.f11687 = AnimatorProxy.m14713(view);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m14691(int i, float f) {
        switch (i) {
            case 1:
                this.f11687.m14719(f);
                return;
            case 2:
                this.f11687.m14721(f);
                return;
            case 4:
                this.f11687.m14727(f);
                return;
            case 8:
                this.f11687.m14717(f);
                return;
            case 16:
                this.f11687.m14729(f);
                return;
            case 32:
                this.f11687.m14733(f);
                return;
            case 64:
                this.f11687.m14731(f);
                return;
            case 128:
                this.f11687.m14723(f);
                return;
            case 256:
                this.f11687.m14725(f);
                return;
            case 512:
                this.f11687.m14735(f);
                return;
            default:
                return;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private float m14694(int i) {
        switch (i) {
            case 1:
                return this.f11687.m14718();
            case 2:
                return this.f11687.m14720();
            case 4:
                return this.f11687.m14726();
            case 8:
                return this.f11687.m14716();
            case 16:
                return this.f11687.m14728();
            case 32:
                return this.f11687.m14732();
            case 64:
                return this.f11687.m14730();
            case 128:
                return this.f11687.m14722();
            case 256:
                return this.f11687.m14724();
            case 512:
                return this.f11687.m14734();
            default:
                return 0.0f;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14696() {
        ValueAnimator r0 = ValueAnimator.m14625(1.0f);
        ArrayList arrayList = (ArrayList) this.f11690.clone();
        this.f11690.clear();
        int i = 0;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            i |= ((NameValuesHolder) arrayList.get(i2)).f11695;
        }
        this.f11681.put(r0, new PropertyBundle(i, arrayList));
        r0.m14648((ValueAnimator.AnimatorUpdateListener) this.f11685);
        r0.m14552(this.f11685);
        if (this.f11679) {
            r0.m14643(this.f11678);
        }
        if (this.f11686) {
            r0.m14644(this.f11688);
        }
        if (this.f11683) {
            r0.m14647(this.f11680);
        }
        r0.m14645();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14697(int i, float f) {
        float r1 = m14694(i);
        m14698(i, r1, f - r1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14698(int i, float f, float f2) {
        if (this.f11681.size() > 0) {
            Animator animator = null;
            Iterator<Animator> it2 = this.f11681.keySet().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Animator next = it2.next();
                PropertyBundle propertyBundle = this.f11681.get(next);
                if (propertyBundle.m14710(i) && propertyBundle.f11697 == 0) {
                    animator = next;
                    break;
                }
            }
            if (animator != null) {
                animator.m14549();
            }
        }
        this.f11690.add(new NameValuesHolder(i, f, f2));
        View view = (View) this.f11689.get();
        if (view != null) {
            view.removeCallbacks(this.f11682);
            view.post(this.f11682);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ViewPropertyAnimator m14701(float f) {
        m14697(512, f);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14702(float f) {
        m14697(1, f);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14703(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("Animators cannot have negative duration: " + j);
        }
        this.f11686 = true;
        this.f11688 = j;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ViewPropertyAnimator m14704(Animator.AnimatorListener animatorListener) {
        this.f11684 = animatorListener;
        return this;
    }
}
