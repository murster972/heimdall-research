package com.nineoldandroids.animation;

public class FloatEvaluator implements TypeEvaluator<Number> {
    /* renamed from: 龘  reason: contains not printable characters */
    public Float m14562(float f, Number number, Number number2) {
        float floatValue = number.floatValue();
        return Float.valueOf(((number2.floatValue() - floatValue) * f) + floatValue);
    }
}
