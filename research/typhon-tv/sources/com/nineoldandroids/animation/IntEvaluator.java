package com.nineoldandroids.animation;

public class IntEvaluator implements TypeEvaluator<Integer> {
    /* renamed from: 龘  reason: contains not printable characters */
    public Integer m14568(float f, Integer num, Integer num2) {
        int intValue = num.intValue();
        return Integer.valueOf((int) (((float) intValue) + (((float) (num2.intValue() - intValue)) * f)));
    }
}
