package com.nineoldandroids.animation;

import android.view.animation.Interpolator;
import com.nineoldandroids.animation.Keyframe;
import java.util.ArrayList;

class FloatKeyframeSet extends KeyframeSet {

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f11585;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f11586;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f11587;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f11588 = true;

    public FloatKeyframeSet(Keyframe.FloatKeyframe... floatKeyframeArr) {
        super(floatKeyframeArr);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public float m14563(float f) {
        if (this.f11604 == 2) {
            if (this.f11588) {
                this.f11588 = false;
                this.f11585 = ((Keyframe.FloatKeyframe) this.f11600.get(0)).m14583();
                this.f11586 = ((Keyframe.FloatKeyframe) this.f11600.get(1)).m14583();
                this.f11587 = this.f11586 - this.f11585;
            }
            if (this.f11602 != null) {
                f = this.f11602.getInterpolation(f);
            }
            return this.f11599 == null ? this.f11585 + (this.f11587 * f) : ((Number) this.f11599.m14614(f, Float.valueOf(this.f11585), Float.valueOf(this.f11586))).floatValue();
        } else if (f <= 0.0f) {
            Keyframe.FloatKeyframe floatKeyframe = (Keyframe.FloatKeyframe) this.f11600.get(0);
            Keyframe.FloatKeyframe floatKeyframe2 = (Keyframe.FloatKeyframe) this.f11600.get(1);
            float r8 = floatKeyframe.m14583();
            float r5 = floatKeyframe2.m14583();
            float r6 = floatKeyframe.m14577();
            float r3 = floatKeyframe2.m14577();
            Interpolator r1 = floatKeyframe2.m14579();
            if (r1 != null) {
                f = r1.getInterpolation(f);
            }
            float f2 = (f - r6) / (r3 - r6);
            return this.f11599 == null ? ((r5 - r8) * f2) + r8 : ((Number) this.f11599.m14614(f2, Float.valueOf(r8), Float.valueOf(r5))).floatValue();
        } else if (f >= 1.0f) {
            Keyframe.FloatKeyframe floatKeyframe3 = (Keyframe.FloatKeyframe) this.f11600.get(this.f11604 - 2);
            Keyframe.FloatKeyframe floatKeyframe4 = (Keyframe.FloatKeyframe) this.f11600.get(this.f11604 - 1);
            float r82 = floatKeyframe3.m14583();
            float r52 = floatKeyframe4.m14583();
            float r62 = floatKeyframe3.m14577();
            float r32 = floatKeyframe4.m14577();
            Interpolator r12 = floatKeyframe4.m14579();
            if (r12 != null) {
                f = r12.getInterpolation(f);
            }
            float f3 = (f - r62) / (r32 - r62);
            return this.f11599 == null ? ((r52 - r82) * f3) + r82 : ((Number) this.f11599.m14614(f3, Float.valueOf(r82), Float.valueOf(r52))).floatValue();
        } else {
            Keyframe.FloatKeyframe floatKeyframe5 = (Keyframe.FloatKeyframe) this.f11600.get(0);
            for (int i = 1; i < this.f11604; i++) {
                Keyframe.FloatKeyframe floatKeyframe6 = (Keyframe.FloatKeyframe) this.f11600.get(i);
                if (f < floatKeyframe6.m14577()) {
                    Interpolator r13 = floatKeyframe6.m14579();
                    if (r13 != null) {
                        f = r13.getInterpolation(f);
                    }
                    float r2 = (f - floatKeyframe5.m14577()) / (floatKeyframe6.m14577() - floatKeyframe5.m14577());
                    float r83 = floatKeyframe5.m14583();
                    float r53 = floatKeyframe6.m14583();
                    return this.f11599 == null ? ((r53 - r83) * r2) + r83 : ((Number) this.f11599.m14614(r2, Float.valueOf(r83), Float.valueOf(r53))).floatValue();
                }
                floatKeyframe5 = floatKeyframe6;
            }
            return ((Number) ((Keyframe) this.f11600.get(this.f11604 - 1)).m14580()).floatValue();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public FloatKeyframeSet m14564() {
        ArrayList arrayList = this.f11600;
        int size = this.f11600.size();
        Keyframe.FloatKeyframe[] floatKeyframeArr = new Keyframe.FloatKeyframe[size];
        for (int i = 0; i < size; i++) {
            floatKeyframeArr[i] = (Keyframe.FloatKeyframe) ((Keyframe) arrayList.get(i)).clone();
        }
        return new FloatKeyframeSet(floatKeyframeArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m14566(float f) {
        return Float.valueOf(m14563(f));
    }
}
