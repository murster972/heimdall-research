package com.nineoldandroids.animation;

import com.nineoldandroids.util.Property;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class PropertyValuesHolder implements Cloneable {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static Class[] f11605 = {Double.TYPE, Double.class, Float.TYPE, Integer.TYPE, Float.class, Integer.class};

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final HashMap<Class, HashMap<String, Method>> f11606 = new HashMap<>();

    /* renamed from: ˈ  reason: contains not printable characters */
    private static Class[] f11607 = {Integer.TYPE, Integer.class, Float.TYPE, Double.TYPE, Float.class, Double.class};

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final TypeEvaluator f11608 = new IntEvaluator();

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final TypeEvaluator f11609 = new FloatEvaluator();

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static Class[] f11610 = {Float.TYPE, Float.class, Double.TYPE, Integer.TYPE, Double.class, Integer.class};

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final HashMap<Class, HashMap<String, Method>> f11611 = new HashMap<>();

    /* renamed from: ʻ  reason: contains not printable characters */
    final ReentrantReadWriteLock f11612;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Object[] f11613;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Method f11614;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Object f11615;

    /* renamed from: 连任  reason: contains not printable characters */
    KeyframeSet f11616;

    /* renamed from: 靐  reason: contains not printable characters */
    protected Property f11617;

    /* renamed from: 麤  reason: contains not printable characters */
    Class f11618;

    /* renamed from: 齉  reason: contains not printable characters */
    Method f11619;

    /* renamed from: 龘  reason: contains not printable characters */
    String f11620;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private TypeEvaluator f11621;

    static class FloatPropertyValuesHolder extends PropertyValuesHolder {

        /* renamed from: ʽ  reason: contains not printable characters */
        FloatKeyframeSet f11622;

        /* renamed from: ˑ  reason: contains not printable characters */
        float f11623;

        public FloatPropertyValuesHolder(String str, float... fArr) {
            super(str);
            m14608(fArr);
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public FloatPropertyValuesHolder m14606() {
            FloatPropertyValuesHolder floatPropertyValuesHolder = (FloatPropertyValuesHolder) PropertyValuesHolder.super.clone();
            floatPropertyValuesHolder.f11622 = (FloatKeyframeSet) floatPropertyValuesHolder.f11616;
            return floatPropertyValuesHolder;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public Object m14605() {
            return Float.valueOf(this.f11623);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m14607(float f) {
            this.f11623 = this.f11622.m14563(f);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14608(float... fArr) {
            PropertyValuesHolder.super.m14602(fArr);
            this.f11622 = (FloatKeyframeSet) this.f11616;
        }
    }

    static class IntPropertyValuesHolder extends PropertyValuesHolder {

        /* renamed from: ʽ  reason: contains not printable characters */
        IntKeyframeSet f11624;

        /* renamed from: ˑ  reason: contains not printable characters */
        int f11625;

        public IntPropertyValuesHolder(String str, int... iArr) {
            super(str);
            m14613(iArr);
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public IntPropertyValuesHolder m14611() {
            IntPropertyValuesHolder intPropertyValuesHolder = (IntPropertyValuesHolder) PropertyValuesHolder.super.clone();
            intPropertyValuesHolder.f11624 = (IntKeyframeSet) intPropertyValuesHolder.f11616;
            return intPropertyValuesHolder;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public Object m14610() {
            return Integer.valueOf(this.f11625);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m14612(float f) {
            this.f11625 = this.f11624.m14569(f);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14613(int... iArr) {
            PropertyValuesHolder.super.m14603(iArr);
            this.f11624 = (IntKeyframeSet) this.f11616;
        }
    }

    private PropertyValuesHolder(String str) {
        this.f11619 = null;
        this.f11614 = null;
        this.f11616 = null;
        this.f11612 = new ReentrantReadWriteLock();
        this.f11613 = new Object[1];
        this.f11620 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PropertyValuesHolder m14595(String str, float... fArr) {
        return new FloatPropertyValuesHolder(str, fArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static PropertyValuesHolder m14596(String str, int... iArr) {
        return new IntPropertyValuesHolder(str, iArr);
    }

    public String toString() {
        return this.f11620 + ": " + this.f11616.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m14597() {
        if (this.f11621 == null) {
            this.f11621 = this.f11618 == Integer.class ? f11608 : this.f11618 == Float.class ? f11609 : null;
        }
        if (this.f11621 != null) {
            this.f11616.m14594(this.f11621);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public Object m14598() {
        return this.f11615;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m14599() {
        return this.f11620;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PropertyValuesHolder clone() {
        try {
            PropertyValuesHolder propertyValuesHolder = (PropertyValuesHolder) super.clone();
            propertyValuesHolder.f11620 = this.f11620;
            propertyValuesHolder.f11617 = this.f11617;
            propertyValuesHolder.f11616 = this.f11616.clone();
            propertyValuesHolder.f11621 = this.f11621;
            return propertyValuesHolder;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14601(float f) {
        this.f11615 = this.f11616.m14593(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14602(float... fArr) {
        this.f11618 = Float.TYPE;
        this.f11616 = KeyframeSet.m14590(fArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14603(int... iArr) {
        this.f11618 = Integer.TYPE;
        this.f11616 = KeyframeSet.m14591(iArr);
    }
}
