package com.nineoldandroids.animation;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AndroidRuntimeException;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.nineoldandroids.animation.Animator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ValueAnimator extends Animator {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static ThreadLocal<AnimationHandler> f11626 = new ThreadLocal<>();
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final ThreadLocal<ArrayList<ValueAnimator>> f11627 = new ThreadLocal<ArrayList<ValueAnimator>>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public ArrayList<ValueAnimator> initialValue() {
            return new ArrayList<>();
        }
    };

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final Interpolator f11628 = new AccelerateDecelerateInterpolator();
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final ThreadLocal<ArrayList<ValueAnimator>> f11629 = new ThreadLocal<ArrayList<ValueAnimator>>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public ArrayList<ValueAnimator> initialValue() {
            return new ArrayList<>();
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final ThreadLocal<ArrayList<ValueAnimator>> f11630 = new ThreadLocal<ArrayList<ValueAnimator>>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public ArrayList<ValueAnimator> initialValue() {
            return new ArrayList<>();
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public static final ThreadLocal<ArrayList<ValueAnimator>> f11631 = new ThreadLocal<ArrayList<ValueAnimator>>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public ArrayList<ValueAnimator> initialValue() {
            return new ArrayList<>();
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final ThreadLocal<ArrayList<ValueAnimator>> f11632 = new ThreadLocal<ArrayList<ValueAnimator>>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public ArrayList<ValueAnimator> initialValue() {
            return new ArrayList<>();
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static long f11633 = 10;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final TypeEvaluator f11634 = new IntEvaluator();

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final TypeEvaluator f11635 = new FloatEvaluator();

    /* renamed from: ʻ  reason: contains not printable characters */
    PropertyValuesHolder[] f11636;

    /* renamed from: ʼ  reason: contains not printable characters */
    HashMap<String, PropertyValuesHolder> f11637;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f11638 = false;

    /* renamed from: ˉ  reason: contains not printable characters */
    private long f11639;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f11640 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f11641 = 0;

    /* renamed from: ˎ  reason: contains not printable characters */
    private float f11642 = 0.0f;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean f11643 = false;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f11644 = false;

    /* renamed from: ـ  reason: contains not printable characters */
    private long f11645 = 300;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public long f11646 = 0;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int f11647 = 0;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f11648 = 1;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private Interpolator f11649 = f11628;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean f11650 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    long f11651;

    /* renamed from: 麤  reason: contains not printable characters */
    int f11652 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    long f11653 = -1;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private ArrayList<AnimatorUpdateListener> f11654 = null;

    private static class AnimationHandler extends Handler {
        private AnimationHandler() {
        }

        public void handleMessage(Message message) {
            boolean z = true;
            ArrayList arrayList = (ArrayList) ValueAnimator.f11630.get();
            ArrayList arrayList2 = (ArrayList) ValueAnimator.f11632.get();
            switch (message.what) {
                case 0:
                    ArrayList arrayList3 = (ArrayList) ValueAnimator.f11631.get();
                    if (arrayList.size() > 0 || arrayList2.size() > 0) {
                        z = false;
                    }
                    while (arrayList3.size() > 0) {
                        ArrayList arrayList4 = (ArrayList) arrayList3.clone();
                        arrayList3.clear();
                        int size = arrayList4.size();
                        for (int i = 0; i < size; i++) {
                            ValueAnimator valueAnimator = (ValueAnimator) arrayList4.get(i);
                            if (valueAnimator.f11646 == 0) {
                                valueAnimator.m14631();
                            } else {
                                arrayList2.add(valueAnimator);
                            }
                        }
                    }
                    break;
                case 1:
                    break;
                default:
                    return;
            }
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            ArrayList arrayList5 = (ArrayList) ValueAnimator.f11627.get();
            ArrayList arrayList6 = (ArrayList) ValueAnimator.f11629.get();
            int size2 = arrayList2.size();
            for (int i2 = 0; i2 < size2; i2++) {
                ValueAnimator valueAnimator2 = (ValueAnimator) arrayList2.get(i2);
                if (valueAnimator2.m14621(currentAnimationTimeMillis)) {
                    arrayList5.add(valueAnimator2);
                }
            }
            int size3 = arrayList5.size();
            if (size3 > 0) {
                for (int i3 = 0; i3 < size3; i3++) {
                    ValueAnimator valueAnimator3 = (ValueAnimator) arrayList5.get(i3);
                    valueAnimator3.m14631();
                    boolean unused = valueAnimator3.f11643 = true;
                    arrayList2.remove(valueAnimator3);
                }
                arrayList5.clear();
            }
            int size4 = arrayList.size();
            int i4 = 0;
            while (i4 < size4) {
                ValueAnimator valueAnimator4 = (ValueAnimator) arrayList.get(i4);
                if (valueAnimator4.m14641(currentAnimationTimeMillis)) {
                    arrayList6.add(valueAnimator4);
                }
                if (arrayList.size() == size4) {
                    i4++;
                } else {
                    size4--;
                    arrayList6.remove(valueAnimator4);
                }
            }
            if (arrayList6.size() > 0) {
                for (int i5 = 0; i5 < arrayList6.size(); i5++) {
                    ((ValueAnimator) arrayList6.get(i5)).m14630();
                }
                arrayList6.clear();
            }
            if (!z) {
                return;
            }
            if (!arrayList.isEmpty() || !arrayList2.isEmpty()) {
                sendEmptyMessageDelayed(1, Math.max(0, ValueAnimator.f11633 - (AnimationUtils.currentAnimationTimeMillis() - currentAnimationTimeMillis)));
            }
        }
    }

    public interface AnimatorUpdateListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m14655(ValueAnimator valueAnimator);
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m14621(long j) {
        if (!this.f11638) {
            this.f11638 = true;
            this.f11639 = j;
        } else {
            long j2 = j - this.f11639;
            if (j2 > this.f11646) {
                this.f11651 = j - (j2 - this.f11646);
                this.f11652 = 1;
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ValueAnimator m14625(float... fArr) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.m14638(fArr);
        return valueAnimator;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ValueAnimator m14626(int... iArr) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.m14639(iArr);
        return valueAnimator;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14627(boolean z) {
        if (Looper.myLooper() == null) {
            throw new AndroidRuntimeException("Animators may only be run on Looper threads");
        }
        this.f11640 = z;
        this.f11641 = 0;
        this.f11652 = 0;
        this.f11644 = true;
        this.f11638 = false;
        f11631.get().add(this);
        if (this.f11646 == 0) {
            m14637(m14635());
            this.f11652 = 0;
            this.f11643 = true;
            if (this.f11584 != null) {
                ArrayList arrayList = (ArrayList) this.f11584.clone();
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    ((Animator.AnimatorListener) arrayList.get(i)).m14553(this);
                }
            }
        }
        AnimationHandler animationHandler = f11626.get();
        if (animationHandler == null) {
            animationHandler = new AnimationHandler();
            f11626.set(animationHandler);
        }
        animationHandler.sendEmptyMessage(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public void m14630() {
        f11630.get().remove(this);
        f11631.get().remove(this);
        f11632.get().remove(this);
        this.f11652 = 0;
        if (this.f11643 && this.f11584 != null) {
            ArrayList arrayList = (ArrayList) this.f11584.clone();
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ((Animator.AnimatorListener) arrayList.get(i)).m14556(this);
            }
        }
        this.f11643 = false;
        this.f11644 = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public void m14631() {
        m14640();
        f11630.get().add(this);
        if (this.f11646 > 0 && this.f11584 != null) {
            ArrayList arrayList = (ArrayList) this.f11584.clone();
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                ((Animator.AnimatorListener) arrayList.get(i)).m14553(this);
            }
        }
    }

    public String toString() {
        String str = "ValueAnimator@" + Integer.toHexString(hashCode());
        if (this.f11636 != null) {
            for (int i = 0; i < this.f11636.length; i++) {
                str = str + "\n    " + this.f11636[i].toString();
            }
        }
        return str;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m14632() {
        if (this.f11636 == null || this.f11636.length <= 0) {
            return null;
        }
        return this.f11636[0].m14598();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public float m14633() {
        return this.f11642;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public ValueAnimator m14642() {
        ValueAnimator valueAnimator = (ValueAnimator) super.clone();
        if (this.f11654 != null) {
            ArrayList<AnimatorUpdateListener> arrayList = this.f11654;
            valueAnimator.f11654 = new ArrayList<>();
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                valueAnimator.f11654.add(arrayList.get(i));
            }
        }
        valueAnimator.f11653 = -1;
        valueAnimator.f11640 = false;
        valueAnimator.f11641 = 0;
        valueAnimator.f11650 = false;
        valueAnimator.f11652 = 0;
        valueAnimator.f11638 = false;
        PropertyValuesHolder[] propertyValuesHolderArr = this.f11636;
        if (propertyValuesHolderArr != null) {
            int length = propertyValuesHolderArr.length;
            valueAnimator.f11636 = new PropertyValuesHolder[length];
            valueAnimator.f11637 = new HashMap<>(length);
            for (int i2 = 0; i2 < length; i2++) {
                PropertyValuesHolder r2 = propertyValuesHolderArr[i2].clone();
                valueAnimator.f11636[i2] = r2;
                valueAnimator.f11637.put(r2.m14599(), r2);
            }
        }
        return valueAnimator;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m14635() {
        if (!this.f11650 || this.f11652 == 0) {
            return 0;
        }
        return AnimationUtils.currentAnimationTimeMillis() - this.f11651;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14636() {
        if (this.f11652 != 0 || f11631.get().contains(this) || f11632.get().contains(this)) {
            if (this.f11643 && this.f11584 != null) {
                Iterator it2 = ((ArrayList) this.f11584.clone()).iterator();
                while (it2.hasNext()) {
                    ((Animator.AnimatorListener) it2.next()).m14555(this);
                }
            }
            m14630();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14637(long j) {
        m14640();
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        if (this.f11652 != 1) {
            this.f11653 = j;
            this.f11652 = 2;
        }
        this.f11651 = currentAnimationTimeMillis - j;
        m14641(currentAnimationTimeMillis);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14638(float... fArr) {
        if (fArr != null && fArr.length != 0) {
            if (this.f11636 == null || this.f11636.length == 0) {
                m14649(PropertyValuesHolder.m14595("", fArr));
            } else {
                this.f11636[0].m14602(fArr);
            }
            this.f11650 = false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14639(int... iArr) {
        if (iArr != null && iArr.length != 0) {
            if (this.f11636 == null || this.f11636.length == 0) {
                m14649(PropertyValuesHolder.m14596("", iArr));
            } else {
                this.f11636[0].m14603(iArr);
            }
            this.f11650 = false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m14640() {
        if (!this.f11650) {
            for (PropertyValuesHolder r2 : this.f11636) {
                r2.m14597();
            }
            this.f11650 = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m14641(long j) {
        boolean z = false;
        if (this.f11652 == 0) {
            this.f11652 = 1;
            if (this.f11653 < 0) {
                this.f11651 = j;
            } else {
                this.f11651 = j - this.f11653;
                this.f11653 = -1;
            }
        }
        switch (this.f11652) {
            case 1:
            case 2:
                float f = this.f11645 > 0 ? ((float) (j - this.f11651)) / ((float) this.f11645) : 1.0f;
                if (f >= 1.0f) {
                    if (this.f11641 < this.f11647 || this.f11647 == -1) {
                        if (this.f11584 != null) {
                            int size = this.f11584.size();
                            for (int i = 0; i < size; i++) {
                                ((Animator.AnimatorListener) this.f11584.get(i)).m14554(this);
                            }
                        }
                        if (this.f11648 == 2) {
                            this.f11640 = !this.f11640;
                        }
                        this.f11641 += (int) f;
                        f %= 1.0f;
                        this.f11651 += this.f11645;
                    } else {
                        z = true;
                        f = Math.min(f, 1.0f);
                    }
                }
                if (this.f11640) {
                    f = 1.0f - f;
                }
                m14646(f);
                break;
        }
        return z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m14643(long j) {
        this.f11646 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ValueAnimator m14644(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("Animators cannot have negative duration: " + j);
        }
        this.f11645 = j;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14645() {
        m14627(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14646(float f) {
        float interpolation = this.f11649.getInterpolation(f);
        this.f11642 = interpolation;
        for (PropertyValuesHolder r3 : this.f11636) {
            r3.m14601(interpolation);
        }
        if (this.f11654 != null) {
            int size = this.f11654.size();
            for (int i = 0; i < size; i++) {
                this.f11654.get(i).m14655(this);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14647(Interpolator interpolator) {
        if (interpolator != null) {
            this.f11649 = interpolator;
        } else {
            this.f11649 = new LinearInterpolator();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14648(AnimatorUpdateListener animatorUpdateListener) {
        if (this.f11654 == null) {
            this.f11654 = new ArrayList<>();
        }
        this.f11654.add(animatorUpdateListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14649(PropertyValuesHolder... propertyValuesHolderArr) {
        this.f11636 = propertyValuesHolderArr;
        this.f11637 = new HashMap<>(r1);
        for (PropertyValuesHolder propertyValuesHolder : propertyValuesHolderArr) {
            this.f11637.put(propertyValuesHolder.m14599(), propertyValuesHolder);
        }
        this.f11650 = false;
    }
}
