package com.nineoldandroids.animation;

import android.view.animation.Interpolator;

public abstract class Keyframe implements Cloneable {

    /* renamed from: 靐  reason: contains not printable characters */
    Class f11593;

    /* renamed from: 麤  reason: contains not printable characters */
    private Interpolator f11594 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f11595 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    float f11596;

    static class FloatKeyframe extends Keyframe {

        /* renamed from: 麤  reason: contains not printable characters */
        float f11597;

        FloatKeyframe(float f) {
            this.f11596 = f;
            this.f11593 = Float.TYPE;
        }

        FloatKeyframe(float f, float f2) {
            this.f11596 = f;
            this.f11597 = f2;
            this.f11593 = Float.TYPE;
            this.f11595 = true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public FloatKeyframe m14584() {
            FloatKeyframe floatKeyframe = new FloatKeyframe(m14577(), this.f11597);
            floatKeyframe.m14581(m14579());
            return floatKeyframe;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public float m14583() {
            return this.f11597;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Object m14585() {
            return Float.valueOf(this.f11597);
        }
    }

    static class IntKeyframe extends Keyframe {

        /* renamed from: 麤  reason: contains not printable characters */
        int f11598;

        IntKeyframe(float f) {
            this.f11596 = f;
            this.f11593 = Integer.TYPE;
        }

        IntKeyframe(float f, int i) {
            this.f11596 = f;
            this.f11598 = i;
            this.f11593 = Integer.TYPE;
            this.f11595 = true;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public IntKeyframe m14588() {
            IntKeyframe intKeyframe = new IntKeyframe(m14577(), this.f11598);
            intKeyframe.m14581(m14579());
            return intKeyframe;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public int m14587() {
            return this.f11598;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Object m14589() {
            return Integer.valueOf(this.f11598);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Keyframe m14573(float f) {
        return new FloatKeyframe(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Keyframe m14574(float f) {
        return new IntKeyframe(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Keyframe m14575(float f, float f2) {
        return new FloatKeyframe(f, f2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Keyframe m14576(float f, int i) {
        return new IntKeyframe(f, i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public float m14577() {
        return this.f11596;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract Keyframe clone();

    /* renamed from: 齉  reason: contains not printable characters */
    public Interpolator m14579() {
        return this.f11594;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Object m14580();

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14581(Interpolator interpolator) {
        this.f11594 = interpolator;
    }
}
