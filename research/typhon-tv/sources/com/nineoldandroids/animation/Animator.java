package com.nineoldandroids.animation;

import java.util.ArrayList;

public abstract class Animator implements Cloneable {

    /* renamed from: 龘  reason: contains not printable characters */
    ArrayList<AnimatorListener> f11584 = null;

    public interface AnimatorListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m14553(Animator animator);

        /* renamed from: 麤  reason: contains not printable characters */
        void m14554(Animator animator);

        /* renamed from: 齉  reason: contains not printable characters */
        void m14555(Animator animator);

        /* renamed from: 龘  reason: contains not printable characters */
        void m14556(Animator animator);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14549() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Animator clone() {
        try {
            Animator animator = (Animator) super.clone();
            if (this.f11584 != null) {
                ArrayList<AnimatorListener> arrayList = this.f11584;
                animator.f11584 = new ArrayList<>();
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    animator.f11584.add(arrayList.get(i));
                }
            }
            return animator;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14551() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14552(AnimatorListener animatorListener) {
        if (this.f11584 == null) {
            this.f11584 = new ArrayList<>();
        }
        this.f11584.add(animatorListener);
    }
}
