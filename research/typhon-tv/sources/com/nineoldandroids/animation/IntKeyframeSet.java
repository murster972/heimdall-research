package com.nineoldandroids.animation;

import android.view.animation.Interpolator;
import com.nineoldandroids.animation.Keyframe;
import java.util.ArrayList;

class IntKeyframeSet extends KeyframeSet {

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f11589;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f11590;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f11591;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f11592 = true;

    public IntKeyframeSet(Keyframe.IntKeyframe... intKeyframeArr) {
        super(intKeyframeArr);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m14569(float f) {
        if (this.f11604 == 2) {
            if (this.f11592) {
                this.f11592 = false;
                this.f11589 = ((Keyframe.IntKeyframe) this.f11600.get(0)).m14587();
                this.f11590 = ((Keyframe.IntKeyframe) this.f11600.get(1)).m14587();
                this.f11591 = this.f11590 - this.f11589;
            }
            if (this.f11602 != null) {
                f = this.f11602.getInterpolation(f);
            }
            return this.f11599 == null ? this.f11589 + ((int) (((float) this.f11591) * f)) : ((Number) this.f11599.m14614(f, Integer.valueOf(this.f11589), Integer.valueOf(this.f11590))).intValue();
        } else if (f <= 0.0f) {
            Keyframe.IntKeyframe intKeyframe = (Keyframe.IntKeyframe) this.f11600.get(0);
            Keyframe.IntKeyframe intKeyframe2 = (Keyframe.IntKeyframe) this.f11600.get(1);
            int r8 = intKeyframe.m14587();
            int r5 = intKeyframe2.m14587();
            float r6 = intKeyframe.m14577();
            float r3 = intKeyframe2.m14577();
            Interpolator r1 = intKeyframe2.m14579();
            if (r1 != null) {
                f = r1.getInterpolation(f);
            }
            float f2 = (f - r6) / (r3 - r6);
            return this.f11599 == null ? ((int) (((float) (r5 - r8)) * f2)) + r8 : ((Number) this.f11599.m14614(f2, Integer.valueOf(r8), Integer.valueOf(r5))).intValue();
        } else if (f >= 1.0f) {
            Keyframe.IntKeyframe intKeyframe3 = (Keyframe.IntKeyframe) this.f11600.get(this.f11604 - 2);
            Keyframe.IntKeyframe intKeyframe4 = (Keyframe.IntKeyframe) this.f11600.get(this.f11604 - 1);
            int r82 = intKeyframe3.m14587();
            int r52 = intKeyframe4.m14587();
            float r62 = intKeyframe3.m14577();
            float r32 = intKeyframe4.m14577();
            Interpolator r12 = intKeyframe4.m14579();
            if (r12 != null) {
                f = r12.getInterpolation(f);
            }
            float f3 = (f - r62) / (r32 - r62);
            return this.f11599 == null ? ((int) (((float) (r52 - r82)) * f3)) + r82 : ((Number) this.f11599.m14614(f3, Integer.valueOf(r82), Integer.valueOf(r52))).intValue();
        } else {
            Keyframe.IntKeyframe intKeyframe5 = (Keyframe.IntKeyframe) this.f11600.get(0);
            for (int i = 1; i < this.f11604; i++) {
                Keyframe.IntKeyframe intKeyframe6 = (Keyframe.IntKeyframe) this.f11600.get(i);
                if (f < intKeyframe6.m14577()) {
                    Interpolator r13 = intKeyframe6.m14579();
                    if (r13 != null) {
                        f = r13.getInterpolation(f);
                    }
                    float r2 = (f - intKeyframe5.m14577()) / (intKeyframe6.m14577() - intKeyframe5.m14577());
                    int r83 = intKeyframe5.m14587();
                    int r53 = intKeyframe6.m14587();
                    return this.f11599 == null ? ((int) (((float) (r53 - r83)) * r2)) + r83 : ((Number) this.f11599.m14614(r2, Integer.valueOf(r83), Integer.valueOf(r53))).intValue();
                }
                intKeyframe5 = intKeyframe6;
            }
            return ((Number) ((Keyframe) this.f11600.get(this.f11604 - 1)).m14580()).intValue();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public IntKeyframeSet m14570() {
        ArrayList arrayList = this.f11600;
        int size = this.f11600.size();
        Keyframe.IntKeyframe[] intKeyframeArr = new Keyframe.IntKeyframe[size];
        for (int i = 0; i < size; i++) {
            intKeyframeArr[i] = (Keyframe.IntKeyframe) ((Keyframe) arrayList.get(i)).clone();
        }
        return new IntKeyframeSet(intKeyframeArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m14572(float f) {
        return Integer.valueOf(m14569(f));
    }
}
