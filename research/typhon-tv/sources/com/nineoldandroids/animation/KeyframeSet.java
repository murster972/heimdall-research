package com.nineoldandroids.animation;

import android.view.animation.Interpolator;
import com.nineoldandroids.animation.Keyframe;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;

class KeyframeSet {

    /* renamed from: ʻ  reason: contains not printable characters */
    TypeEvaluator f11599;

    /* renamed from: 连任  reason: contains not printable characters */
    ArrayList<Keyframe> f11600 = new ArrayList<>();

    /* renamed from: 靐  reason: contains not printable characters */
    Keyframe f11601;

    /* renamed from: 麤  reason: contains not printable characters */
    Interpolator f11602;

    /* renamed from: 齉  reason: contains not printable characters */
    Keyframe f11603;

    /* renamed from: 龘  reason: contains not printable characters */
    int f11604;

    public KeyframeSet(Keyframe... keyframeArr) {
        this.f11604 = keyframeArr.length;
        this.f11600.addAll(Arrays.asList(keyframeArr));
        this.f11601 = this.f11600.get(0);
        this.f11603 = this.f11600.get(this.f11604 - 1);
        this.f11602 = this.f11603.m14579();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static KeyframeSet m14590(float... fArr) {
        int length = fArr.length;
        Keyframe.FloatKeyframe[] floatKeyframeArr = new Keyframe.FloatKeyframe[Math.max(length, 2)];
        if (length == 1) {
            floatKeyframeArr[0] = (Keyframe.FloatKeyframe) Keyframe.m14573(0.0f);
            floatKeyframeArr[1] = (Keyframe.FloatKeyframe) Keyframe.m14575(1.0f, fArr[0]);
        } else {
            floatKeyframeArr[0] = (Keyframe.FloatKeyframe) Keyframe.m14575(0.0f, fArr[0]);
            for (int i = 1; i < length; i++) {
                floatKeyframeArr[i] = (Keyframe.FloatKeyframe) Keyframe.m14575(((float) i) / ((float) (length - 1)), fArr[i]);
            }
        }
        return new FloatKeyframeSet(floatKeyframeArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static KeyframeSet m14591(int... iArr) {
        int length = iArr.length;
        Keyframe.IntKeyframe[] intKeyframeArr = new Keyframe.IntKeyframe[Math.max(length, 2)];
        if (length == 1) {
            intKeyframeArr[0] = (Keyframe.IntKeyframe) Keyframe.m14574(0.0f);
            intKeyframeArr[1] = (Keyframe.IntKeyframe) Keyframe.m14576(1.0f, iArr[0]);
        } else {
            intKeyframeArr[0] = (Keyframe.IntKeyframe) Keyframe.m14576(0.0f, iArr[0]);
            for (int i = 1; i < length; i++) {
                intKeyframeArr[i] = (Keyframe.IntKeyframe) Keyframe.m14576(((float) i) / ((float) (length - 1)), iArr[i]);
            }
        }
        return new IntKeyframeSet(intKeyframeArr);
    }

    public String toString() {
        String str = StringUtils.SPACE;
        for (int i = 0; i < this.f11604; i++) {
            str = str + this.f11600.get(i).m14580() + "  ";
        }
        return str;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public KeyframeSet clone() {
        ArrayList<Keyframe> arrayList = this.f11600;
        int size = this.f11600.size();
        Keyframe[] keyframeArr = new Keyframe[size];
        for (int i = 0; i < size; i++) {
            keyframeArr[i] = arrayList.get(i).clone();
        }
        return new KeyframeSet(keyframeArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m14593(float f) {
        if (this.f11604 == 2) {
            if (this.f11602 != null) {
                f = this.f11602.getInterpolation(f);
            }
            return this.f11599.m14614(f, this.f11601.m14580(), this.f11603.m14580());
        } else if (f <= 0.0f) {
            Keyframe keyframe = this.f11600.get(1);
            Interpolator r1 = keyframe.m14579();
            if (r1 != null) {
                f = r1.getInterpolation(f);
            }
            float r4 = this.f11601.m14577();
            return this.f11599.m14614((f - r4) / (keyframe.m14577() - r4), this.f11601.m14580(), keyframe.m14580());
        } else if (f >= 1.0f) {
            Keyframe keyframe2 = this.f11600.get(this.f11604 - 2);
            Interpolator r12 = this.f11603.m14579();
            if (r12 != null) {
                f = r12.getInterpolation(f);
            }
            float r42 = keyframe2.m14577();
            return this.f11599.m14614((f - r42) / (this.f11603.m14577() - r42), keyframe2.m14580(), this.f11603.m14580());
        } else {
            Keyframe keyframe3 = this.f11601;
            for (int i = 1; i < this.f11604; i++) {
                Keyframe keyframe4 = this.f11600.get(i);
                if (f < keyframe4.m14577()) {
                    Interpolator r13 = keyframe4.m14579();
                    if (r13 != null) {
                        f = r13.getInterpolation(f);
                    }
                    float r43 = keyframe3.m14577();
                    return this.f11599.m14614((f - r43) / (keyframe4.m14577() - r43), keyframe3.m14580(), keyframe4.m14580());
                }
                keyframe3 = keyframe4;
            }
            return this.f11603.m14580();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14594(TypeEvaluator typeEvaluator) {
        this.f11599 = typeEvaluator;
    }
}
