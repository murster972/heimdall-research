package com.mopub.volley;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class VolleyLog {
    public static boolean DEBUG = Log.isLoggable(TAG, 2);
    public static String TAG = "Volley";

    static class MarkerLog {
        public static final boolean ENABLED = VolleyLog.DEBUG;

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f21446 = false;

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<Marker> f21447 = new ArrayList();

        private static class Marker {
            public final String name;
            public final long thread;
            public final long time;

            public Marker(String str, long j, long j2) {
                this.name = str;
                this.thread = j;
                this.time = j2;
            }
        }

        MarkerLog() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private long m27273() {
            if (this.f21447.size() == 0) {
                return 0;
            }
            return this.f21447.get(this.f21447.size() - 1).time - this.f21447.get(0).time;
        }

        public synchronized void add(String str, long j) {
            if (this.f21446) {
                throw new IllegalStateException("Marker added to finished log");
            }
            this.f21447.add(new Marker(str, j, SystemClock.elapsedRealtime()));
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            if (!this.f21446) {
                finish("Request on the loose");
                VolleyLog.e("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }

        public synchronized void finish(String str) {
            this.f21446 = true;
            long r0 = m27273();
            if (r0 > 0) {
                long j = this.f21447.get(0).time;
                VolleyLog.d("(%-4d ms) %s", Long.valueOf(r0), str);
                for (Marker next : this.f21447) {
                    long j2 = next.time;
                    VolleyLog.d("(+%-4d) [%2d] %s", Long.valueOf(j2 - j), Long.valueOf(next.thread), next.name);
                    j = j2;
                }
            }
        }
    }

    public static void d(String str, Object... objArr) {
        Log.d(TAG, m27272(str, objArr));
    }

    public static void e(String str, Object... objArr) {
        Log.e(TAG, m27272(str, objArr));
    }

    public static void e(Throwable th, String str, Object... objArr) {
        Log.e(TAG, m27272(str, objArr), th);
    }

    public static void setTag(String str) {
        d("Changing log tag to %s", str);
        TAG = str;
        DEBUG = Log.isLoggable(TAG, 2);
    }

    public static void v(String str, Object... objArr) {
        if (DEBUG) {
            Log.v(TAG, m27272(str, objArr));
        }
    }

    public static void wtf(String str, Object... objArr) {
        Log.wtf(TAG, m27272(str, objArr));
    }

    public static void wtf(Throwable th, String str, Object... objArr) {
        Log.wtf(TAG, m27272(str, objArr), th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m27272(String str, Object... objArr) {
        String format = objArr == null ? str : String.format(Locale.US, str, objArr);
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        String str2 = "<unknown>";
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                break;
            } else if (!stackTrace[i].getClass().equals(VolleyLog.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = substring.substring(substring.lastIndexOf(36) + 1) + "." + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", new Object[]{Long.valueOf(Thread.currentThread().getId()), str2, format});
    }
}
