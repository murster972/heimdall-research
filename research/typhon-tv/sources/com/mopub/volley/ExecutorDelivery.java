package com.mopub.volley;

import android.os.Handler;
import java.util.concurrent.Executor;

public class ExecutorDelivery implements ResponseDelivery {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Executor f21418;

    private class ResponseDeliveryRunnable implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Request f21421;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Runnable f21422;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Response f21423;

        public ResponseDeliveryRunnable(Request request, Response response, Runnable runnable) {
            this.f21421 = request;
            this.f21423 = response;
            this.f21422 = runnable;
        }

        public void run() {
            if (this.f21421.isCanceled()) {
                this.f21421.m6259("canceled-at-delivery");
                return;
            }
            if (this.f21423.isSuccess()) {
                this.f21421.deliverResponse(this.f21423.result);
            } else {
                this.f21421.deliverError(this.f21423.error);
            }
            if (this.f21423.intermediate) {
                this.f21421.addMarker("intermediate-response");
            } else {
                this.f21421.m6259("done");
            }
            if (this.f21422 != null) {
                this.f21422.run();
            }
        }
    }

    public ExecutorDelivery(final Handler handler) {
        this.f21418 = new Executor() {
            public void execute(Runnable runnable) {
                handler.post(runnable);
            }
        };
    }

    public ExecutorDelivery(Executor executor) {
        this.f21418 = executor;
    }

    public void postError(Request<?> request, VolleyError volleyError) {
        request.addMarker("post-error");
        this.f21418.execute(new ResponseDeliveryRunnable(request, Response.error(volleyError), (Runnable) null));
    }

    public void postResponse(Request<?> request, Response<?> response) {
        postResponse(request, response, (Runnable) null);
    }

    public void postResponse(Request<?> request, Response<?> response, Runnable runnable) {
        request.markDelivered();
        request.addMarker("post-response");
        this.f21418.execute(new ResponseDeliveryRunnable(request, response, runnable));
    }
}
