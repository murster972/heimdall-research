package com.mopub.volley;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.mopub.volley.Cache;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyLog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public abstract class Request<T> implements Comparable<Request<T>> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Integer f5799;

    /* renamed from: ʼ  reason: contains not printable characters */
    private RequestQueue f5800;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f5801;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Cache.Entry f5802;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Object f5803;

    /* renamed from: ˈ  reason: contains not printable characters */
    private RetryPolicy f5804;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f5805;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f5806;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f5807;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Response.ErrorListener f5808;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f5809;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f5810;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f5811;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final VolleyLog.MarkerLog f5812;

    public interface Method {
        public static final int DELETE = 3;
        public static final int DEPRECATED_GET_OR_POST = -1;
        public static final int GET = 0;
        public static final int HEAD = 4;
        public static final int OPTIONS = 5;
        public static final int PATCH = 7;
        public static final int POST = 1;
        public static final int PUT = 2;
        public static final int TRACE = 6;
    }

    public enum Priority {
        LOW,
        NORMAL,
        HIGH,
        IMMEDIATE
    }

    public Request(int i, String str, Response.ErrorListener errorListener) {
        this.f5812 = VolleyLog.MarkerLog.ENABLED ? new VolleyLog.MarkerLog() : null;
        this.f5801 = true;
        this.f5805 = false;
        this.f5806 = false;
        this.f5807 = 0;
        this.f5802 = null;
        this.f5809 = i;
        this.f5811 = str;
        this.f5808 = errorListener;
        setRetryPolicy(new DefaultRetryPolicy());
        this.f5810 = m6255(str);
    }

    @Deprecated
    public Request(String str, Response.ErrorListener errorListener) {
        this(-1, str, errorListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m6255(String str) {
        Uri parse;
        String host;
        if (TextUtils.isEmpty(str) || (parse = Uri.parse(str)) == null || (host = parse.getHost()) == null) {
            return 0;
        }
        return host.hashCode();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private byte[] m6257(Map<String, String> map, String str) {
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry next : map.entrySet()) {
                sb.append(URLEncoder.encode((String) next.getKey(), str));
                sb.append('=');
                sb.append(URLEncoder.encode((String) next.getValue(), str));
                sb.append('&');
            }
            return sb.toString().getBytes(str);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding not supported: " + str, e);
        }
    }

    public void addMarker(String str) {
        if (VolleyLog.MarkerLog.ENABLED) {
            this.f5812.add(str, Thread.currentThread().getId());
        } else if (this.f5807 == 0) {
            this.f5807 = SystemClock.elapsedRealtime();
        }
    }

    public void cancel() {
        this.f5805 = true;
    }

    public int compareTo(Request<T> request) {
        Priority priority = getPriority();
        Priority priority2 = request.getPriority();
        return priority == priority2 ? this.f5799.intValue() - request.f5799.intValue() : priority2.ordinal() - priority.ordinal();
    }

    public void deliverError(VolleyError volleyError) {
        if (this.f5808 != null) {
            this.f5808.onErrorResponse(volleyError);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void deliverResponse(T t);

    public byte[] getBody() throws AuthFailureError {
        Map<String, String> r0 = m6264();
        if (r0 == null || r0.size() <= 0) {
            return null;
        }
        return m6257(r0, m6260());
    }

    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=" + m6260();
    }

    public Cache.Entry getCacheEntry() {
        return this.f5802;
    }

    public String getCacheKey() {
        return getUrl();
    }

    public Response.ErrorListener getErrorListener() {
        return this.f5808;
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        return Collections.emptyMap();
    }

    public int getMethod() {
        return this.f5809;
    }

    @Deprecated
    public byte[] getPostBody() throws AuthFailureError {
        Map<String, String> r0 = m6258();
        if (r0 == null || r0.size() <= 0) {
            return null;
        }
        return m6257(r0, m6261());
    }

    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    public Priority getPriority() {
        return Priority.NORMAL;
    }

    public RetryPolicy getRetryPolicy() {
        return this.f5804;
    }

    public final int getSequence() {
        if (this.f5799 != null) {
            return this.f5799.intValue();
        }
        throw new IllegalStateException("getSequence called before setSequence");
    }

    public Object getTag() {
        return this.f5803;
    }

    public final int getTimeoutMs() {
        return this.f5804.getCurrentTimeout();
    }

    public int getTrafficStatsTag() {
        return this.f5810;
    }

    public String getUrl() {
        return this.f5811;
    }

    public boolean hasHadResponseDelivered() {
        return this.f5806;
    }

    public boolean isCanceled() {
        return this.f5805;
    }

    public void markDelivered() {
        this.f5806 = true;
    }

    public Request<?> setCacheEntry(Cache.Entry entry) {
        this.f5802 = entry;
        return this;
    }

    public Request<?> setRequestQueue(RequestQueue requestQueue) {
        this.f5800 = requestQueue;
        return this;
    }

    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        this.f5804 = retryPolicy;
        return this;
    }

    public final Request<?> setSequence(int i) {
        this.f5799 = Integer.valueOf(i);
        return this;
    }

    public final Request<?> setShouldCache(boolean z) {
        this.f5801 = z;
        return this;
    }

    public Request<?> setTag(Object obj) {
        this.f5803 = obj;
        return this;
    }

    public final boolean shouldCache() {
        return this.f5801;
    }

    public String toString() {
        return (this.f5805 ? "[X] " : "[ ] ") + getUrl() + StringUtils.SPACE + ("0x" + Integer.toHexString(getTrafficStatsTag())) + StringUtils.SPACE + getPriority() + StringUtils.SPACE + this.f5799;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    /* renamed from: 靐  reason: contains not printable characters */
    public Map<String, String> m6258() throws AuthFailureError {
        return m6264();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6259(final String str) {
        if (this.f5800 != null) {
            this.f5800.m27270(this);
        }
        if (VolleyLog.MarkerLog.ENABLED) {
            final long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        Request.this.f5812.add(str, id);
                        Request.this.f5812.finish(toString());
                    }
                });
                return;
            }
            this.f5812.add(str, id);
            this.f5812.finish(toString());
            return;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.f5807;
        if (elapsedRealtime >= 3000) {
            VolleyLog.d("%d ms: %s", Long.valueOf(elapsedRealtime), toString());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m6260() {
        return "UTF-8";
    }

    /* access modifiers changed from: protected */
    @Deprecated
    /* renamed from: 齉  reason: contains not printable characters */
    public String m6261() {
        return m6260();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Response<T> m6262(NetworkResponse networkResponse);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public VolleyError m6263(VolleyError volleyError) {
        return volleyError;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Map<String, String> m6264() throws AuthFailureError {
        return null;
    }
}
