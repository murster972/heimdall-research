package com.mopub.volley;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestQueue {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Cache f21434;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Network f21435;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ResponseDelivery f21436;

    /* renamed from: ˑ  reason: contains not printable characters */
    private NetworkDispatcher[] f21437;

    /* renamed from: ٴ  reason: contains not printable characters */
    private CacheDispatcher f21438;

    /* renamed from: 连任  reason: contains not printable characters */
    private final PriorityBlockingQueue<Request<?>> f21439;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<String, Queue<Request<?>>> f21440;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PriorityBlockingQueue<Request<?>> f21441;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Set<Request<?>> f21442;

    /* renamed from: 龘  reason: contains not printable characters */
    private AtomicInteger f21443;

    public interface RequestFilter {
        boolean apply(Request<?> request);
    }

    public RequestQueue(Cache cache, Network network) {
        this(cache, network, 4);
    }

    public RequestQueue(Cache cache, Network network, int i) {
        this(cache, network, i, new ExecutorDelivery(new Handler(Looper.getMainLooper())));
    }

    public RequestQueue(Cache cache, Network network, int i, ResponseDelivery responseDelivery) {
        this.f21443 = new AtomicInteger();
        this.f21440 = new HashMap();
        this.f21442 = new HashSet();
        this.f21441 = new PriorityBlockingQueue<>();
        this.f21439 = new PriorityBlockingQueue<>();
        this.f21434 = cache;
        this.f21435 = network;
        this.f21437 = new NetworkDispatcher[i];
        this.f21436 = responseDelivery;
    }

    public <T> Request<T> add(Request<T> request) {
        request.setRequestQueue(this);
        synchronized (this.f21442) {
            this.f21442.add(request);
        }
        request.setSequence(getSequenceNumber());
        request.addMarker("add-to-queue");
        if (!request.shouldCache()) {
            this.f21439.add(request);
        } else {
            synchronized (this.f21440) {
                String cacheKey = request.getCacheKey();
                if (this.f21440.containsKey(cacheKey)) {
                    Queue queue = this.f21440.get(cacheKey);
                    if (queue == null) {
                        queue = new LinkedList();
                    }
                    queue.add(request);
                    this.f21440.put(cacheKey, queue);
                    if (VolleyLog.DEBUG) {
                        VolleyLog.v("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                    }
                } else {
                    this.f21440.put(cacheKey, (Object) null);
                    this.f21441.add(request);
                }
            }
        }
        return request;
    }

    public void cancelAll(RequestFilter requestFilter) {
        synchronized (this.f21442) {
            for (Request next : this.f21442) {
                if (requestFilter.apply(next)) {
                    next.cancel();
                }
            }
        }
    }

    public void cancelAll(final Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot cancelAll with a null tag");
        }
        cancelAll((RequestFilter) new RequestFilter() {
            public boolean apply(Request<?> request) {
                return request.getTag() == obj;
            }
        });
    }

    public Cache getCache() {
        return this.f21434;
    }

    public int getSequenceNumber() {
        return this.f21443.incrementAndGet();
    }

    public void start() {
        stop();
        this.f21438 = new CacheDispatcher(this.f21441, this.f21439, this.f21434, this.f21436);
        this.f21438.start();
        for (int i = 0; i < this.f21437.length; i++) {
            NetworkDispatcher networkDispatcher = new NetworkDispatcher(this.f21439, this.f21435, this.f21434, this.f21436);
            this.f21437[i] = networkDispatcher;
            networkDispatcher.start();
        }
    }

    public void stop() {
        if (this.f21438 != null) {
            this.f21438.quit();
        }
        for (int i = 0; i < this.f21437.length; i++) {
            if (this.f21437[i] != null) {
                this.f21437[i].quit();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27270(Request<?> request) {
        synchronized (this.f21442) {
            this.f21442.remove(request);
        }
        if (request.shouldCache()) {
            synchronized (this.f21440) {
                String cacheKey = request.getCacheKey();
                Queue remove = this.f21440.remove(cacheKey);
                if (remove != null) {
                    if (VolleyLog.DEBUG) {
                        VolleyLog.v("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), cacheKey);
                    }
                    this.f21441.addAll(remove);
                }
            }
        }
    }
}
