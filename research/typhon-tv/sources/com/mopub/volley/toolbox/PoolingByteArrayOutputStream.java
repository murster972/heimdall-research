package com.mopub.volley.toolbox;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PoolingByteArrayOutputStream extends ByteArrayOutputStream {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ByteArrayPool f21514;

    public PoolingByteArrayOutputStream(ByteArrayPool byteArrayPool) {
        this(byteArrayPool, 256);
    }

    public PoolingByteArrayOutputStream(ByteArrayPool byteArrayPool, int i) {
        this.f21514 = byteArrayPool;
        this.buf = this.f21514.getBuf(Math.max(i, 256));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27331(int i) {
        if (this.count + i > this.buf.length) {
            byte[] buf = this.f21514.getBuf((this.count + i) * 2);
            System.arraycopy(this.buf, 0, buf, 0, this.count);
            this.f21514.returnBuf(this.buf);
            this.buf = buf;
        }
    }

    public void close() throws IOException {
        this.f21514.returnBuf(this.buf);
        this.buf = null;
        super.close();
    }

    public void finalize() {
        this.f21514.returnBuf(this.buf);
    }

    public synchronized void write(int i) {
        m27331(1);
        super.write(i);
    }

    public synchronized void write(byte[] bArr, int i, int i2) {
        m27331(i2);
        super.write(bArr, i, i2);
    }
}
