package com.mopub.volley.toolbox;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyError;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class ImageLoader {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Handler f21472 = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public Runnable f21473;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final HashMap<String, BatchedImageRequest> f21474 = new HashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private int f21475 = 100;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final HashMap<String, BatchedImageRequest> f21476 = new HashMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private final ImageCache f21477;

    /* renamed from: 龘  reason: contains not printable characters */
    private final RequestQueue f21478;

    private class BatchedImageRequest {
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public final LinkedList<ImageContainer> f21487 = new LinkedList<>();

        /* renamed from: 靐  reason: contains not printable characters */
        private final Request<?> f21488;

        /* renamed from: 麤  reason: contains not printable characters */
        private VolleyError f21489;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public Bitmap f21490;

        public BatchedImageRequest(Request<?> request, ImageContainer imageContainer) {
            this.f21488 = request;
            this.f21487.add(imageContainer);
        }

        public void addContainer(ImageContainer imageContainer) {
            this.f21487.add(imageContainer);
        }

        public VolleyError getError() {
            return this.f21489;
        }

        public boolean removeContainerAndCancelIfNecessary(ImageContainer imageContainer) {
            this.f21487.remove(imageContainer);
            if (this.f21487.size() != 0) {
                return false;
            }
            this.f21488.cancel();
            return true;
        }

        public void setError(VolleyError volleyError) {
            this.f21489 = volleyError;
        }
    }

    public interface ImageCache {
        Bitmap getBitmap(String str);

        void putBitmap(String str, Bitmap bitmap);
    }

    public class ImageContainer {

        /* renamed from: 连任  reason: contains not printable characters */
        private final String f21492;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public Bitmap f21493;

        /* renamed from: 麤  reason: contains not printable characters */
        private final String f21494;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final ImageListener f21495;

        public ImageContainer(Bitmap bitmap, String str, String str2, ImageListener imageListener) {
            this.f21493 = bitmap;
            this.f21492 = str;
            this.f21494 = str2;
            this.f21495 = imageListener;
        }

        public void cancelRequest() {
            if (this.f21495 != null) {
                BatchedImageRequest batchedImageRequest = (BatchedImageRequest) ImageLoader.this.f21476.get(this.f21494);
                if (batchedImageRequest == null) {
                    BatchedImageRequest batchedImageRequest2 = (BatchedImageRequest) ImageLoader.this.f21474.get(this.f21494);
                    if (batchedImageRequest2 != null) {
                        batchedImageRequest2.removeContainerAndCancelIfNecessary(this);
                        if (batchedImageRequest2.f21487.size() == 0) {
                            ImageLoader.this.f21474.remove(this.f21494);
                        }
                    }
                } else if (batchedImageRequest.removeContainerAndCancelIfNecessary(this)) {
                    ImageLoader.this.f21476.remove(this.f21494);
                }
            }
        }

        public Bitmap getBitmap() {
            return this.f21493;
        }

        public String getRequestUrl() {
            return this.f21492;
        }
    }

    public interface ImageListener extends Response.ErrorListener {
        void onResponse(ImageContainer imageContainer, boolean z);
    }

    public ImageLoader(RequestQueue requestQueue, ImageCache imageCache) {
        this.f21478 = requestQueue;
        this.f21477 = imageCache;
    }

    public static ImageListener getImageListener(final ImageView imageView, final int i, final int i2) {
        return new ImageListener() {
            public void onErrorResponse(VolleyError volleyError) {
                if (i2 != 0) {
                    imageView.setImageResource(i2);
                }
            }

            public void onResponse(ImageContainer imageContainer, boolean z) {
                if (imageContainer.getBitmap() != null) {
                    imageView.setImageBitmap(imageContainer.getBitmap());
                } else if (i != 0) {
                    imageView.setImageResource(i);
                }
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m27307(String str, int i, int i2) {
        return new StringBuilder(str.length() + 12).append("#W").append(i).append("#H").append(i2).append(str).toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27309() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("ImageLoader must be invoked from the main thread.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27310(String str, BatchedImageRequest batchedImageRequest) {
        this.f21474.put(str, batchedImageRequest);
        if (this.f21473 == null) {
            this.f21473 = new Runnable() {
                public void run() {
                    for (BatchedImageRequest batchedImageRequest : ImageLoader.this.f21474.values()) {
                        Iterator it2 = batchedImageRequest.f21487.iterator();
                        while (it2.hasNext()) {
                            ImageContainer imageContainer = (ImageContainer) it2.next();
                            if (imageContainer.f21495 != null) {
                                if (batchedImageRequest.getError() == null) {
                                    Bitmap unused = imageContainer.f21493 = batchedImageRequest.f21490;
                                    imageContainer.f21495.onResponse(imageContainer, false);
                                } else {
                                    imageContainer.f21495.onErrorResponse(batchedImageRequest.getError());
                                }
                            }
                        }
                    }
                    ImageLoader.this.f21474.clear();
                    Runnable unused2 = ImageLoader.this.f21473 = null;
                }
            };
            this.f21472.postDelayed(this.f21473, (long) this.f21475);
        }
    }

    public ImageContainer get(String str, ImageListener imageListener) {
        return get(str, imageListener, 0, 0);
    }

    public ImageContainer get(String str, ImageListener imageListener, int i, int i2) {
        m27309();
        String r7 = m27307(str, i, i2);
        Bitmap bitmap = this.f21477.getBitmap(r7);
        if (bitmap != null) {
            ImageContainer imageContainer = new ImageContainer(bitmap, str, (String) null, (ImageListener) null);
            imageListener.onResponse(imageContainer, true);
            return imageContainer;
        }
        ImageContainer imageContainer2 = new ImageContainer((Bitmap) null, str, r7, imageListener);
        imageListener.onResponse(imageContainer2, true);
        BatchedImageRequest batchedImageRequest = this.f21476.get(r7);
        if (batchedImageRequest != null) {
            batchedImageRequest.addContainer(imageContainer2);
            return imageContainer2;
        }
        Request<Bitmap> r9 = m27311(str, i, i2, r7);
        this.f21478.add(r9);
        this.f21476.put(r7, new BatchedImageRequest(r9, imageContainer2));
        return imageContainer2;
    }

    public boolean isCached(String str, int i, int i2) {
        m27309();
        return this.f21477.getBitmap(m27307(str, i, i2)) != null;
    }

    public void setBatchedResponseDelay(int i) {
        this.f21475 = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Request<Bitmap> m27311(String str, int i, int i2, final String str2) {
        return new ImageRequest(str, new Response.Listener<Bitmap>() {
            public void onResponse(Bitmap bitmap) {
                ImageLoader.this.m27312(str2, bitmap);
            }
        }, i, i2, Bitmap.Config.RGB_565, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                ImageLoader.this.m27313(str2, volleyError);
            }
        });
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27312(String str, Bitmap bitmap) {
        this.f21477.putBitmap(str, bitmap);
        BatchedImageRequest remove = this.f21476.remove(str);
        if (remove != null) {
            Bitmap unused = remove.f21490 = bitmap;
            m27310(str, remove);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27313(String str, VolleyError volleyError) {
        BatchedImageRequest remove = this.f21476.remove(str);
        if (remove != null) {
            remove.setError(volleyError);
            m27310(str, remove);
        }
    }
}
