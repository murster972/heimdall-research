package com.mopub.volley.toolbox;

import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyLog;
import java.io.UnsupportedEncodingException;

public abstract class JsonRequest<T> extends Request<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f21502 = String.format("application/json; charset=%s", new Object[]{"utf-8"});

    /* renamed from: 靐  reason: contains not printable characters */
    private final Response.Listener<T> f21503;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f21504;

    public JsonRequest(int i, String str, String str2, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(i, str, errorListener);
        this.f21503 = listener;
        this.f21504 = str2;
    }

    public JsonRequest(String str, String str2, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(-1, str, str2, listener, errorListener);
    }

    /* access modifiers changed from: protected */
    public void deliverResponse(T t) {
        this.f21503.onResponse(t);
    }

    public byte[] getBody() {
        try {
            if (this.f21504 == null) {
                return null;
            }
            return this.f21504.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", this.f21504, "utf-8");
            return null;
        }
    }

    public String getBodyContentType() {
        return f21502;
    }

    public byte[] getPostBody() {
        return getBody();
    }

    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Response<T> m27326(NetworkResponse networkResponse);
}
