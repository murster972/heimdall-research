package com.mopub.volley.toolbox;

import com.mopub.volley.Request;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyError;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RequestFuture<T> implements Response.ErrorListener, Response.Listener<T>, Future<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f21515 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private VolleyError f21516;

    /* renamed from: 齉  reason: contains not printable characters */
    private T f21517;

    /* renamed from: 龘  reason: contains not printable characters */
    private Request<?> f21518;

    private RequestFuture() {
    }

    public static <E> RequestFuture<E> newFuture() {
        return new RequestFuture<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized T m27332(Long l) throws InterruptedException, ExecutionException, TimeoutException {
        T t;
        if (this.f21516 != null) {
            throw new ExecutionException(this.f21516);
        } else if (this.f21515) {
            t = this.f21517;
        } else {
            if (l == null) {
                wait(0);
            } else if (l.longValue() > 0) {
                wait(l.longValue());
            }
            if (this.f21516 != null) {
                throw new ExecutionException(this.f21516);
            } else if (!this.f21515) {
                throw new TimeoutException();
            } else {
                t = this.f21517;
            }
        }
        return t;
    }

    public synchronized boolean cancel(boolean z) {
        boolean z2 = false;
        synchronized (this) {
            if (this.f21518 != null) {
                if (!isDone()) {
                    this.f21518.cancel();
                    z2 = true;
                }
            }
        }
        return z2;
    }

    public T get() throws InterruptedException, ExecutionException {
        try {
            return m27332((Long) null);
        } catch (TimeoutException e) {
            throw new AssertionError(e);
        }
    }

    public T get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return m27332(Long.valueOf(TimeUnit.MILLISECONDS.convert(j, timeUnit)));
    }

    public boolean isCancelled() {
        if (this.f21518 == null) {
            return false;
        }
        return this.f21518.isCanceled();
    }

    public synchronized boolean isDone() {
        return this.f21515 || this.f21516 != null || isCancelled();
    }

    public synchronized void onErrorResponse(VolleyError volleyError) {
        this.f21516 = volleyError;
        notifyAll();
    }

    public synchronized void onResponse(T t) {
        this.f21515 = true;
        this.f21517 = t;
        notifyAll();
    }

    public void setRequest(Request<?> request) {
        this.f21518 = request;
    }
}
