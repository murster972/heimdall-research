package com.mopub.volley.toolbox;

import android.os.Handler;
import android.os.Looper;
import com.mopub.volley.Cache;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Response;

public class ClearCacheRequest extends Request<Object> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Runnable f21462;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Cache f21463;

    public ClearCacheRequest(Cache cache, Runnable runnable) {
        super(0, (String) null, (Response.ErrorListener) null);
        this.f21463 = cache;
        this.f21462 = runnable;
    }

    /* access modifiers changed from: protected */
    public void deliverResponse(Object obj) {
    }

    public Request.Priority getPriority() {
        return Request.Priority.IMMEDIATE;
    }

    public boolean isCanceled() {
        this.f21463.clear();
        if (this.f21462 == null) {
            return true;
        }
        new Handler(Looper.getMainLooper()).postAtFrontOfQueue(this.f21462);
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Response<Object> m27280(NetworkResponse networkResponse) {
        return null;
    }
}
