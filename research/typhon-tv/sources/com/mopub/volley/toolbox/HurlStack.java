package com.mopub.volley.toolbox;

import com.mopub.common.TyphoonApp;
import com.mopub.volley.AuthFailureError;
import com.mopub.volley.Request;
import com.mopub.volley.toolbox.HttpClientStack;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.apache.oltu.oauth2.common.OAuth;

public class HurlStack implements HttpStack {

    /* renamed from: 靐  reason: contains not printable characters */
    private final SSLSocketFactory f21470;

    /* renamed from: 龘  reason: contains not printable characters */
    private final UrlRewriter f21471;

    public interface UrlRewriter {
        String rewriteUrl(String str);
    }

    public HurlStack() {
        this((UrlRewriter) null);
    }

    public HurlStack(UrlRewriter urlRewriter) {
        this(urlRewriter, (SSLSocketFactory) null);
    }

    public HurlStack(UrlRewriter urlRewriter, SSLSocketFactory sSLSocketFactory) {
        this.f21471 = urlRewriter;
        this.f21470 = sSLSocketFactory;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static void m27300(HttpURLConnection httpURLConnection, Request<?> request) throws IOException, AuthFailureError {
        byte[] body = request.getBody();
        if (body != null) {
            httpURLConnection.setDoOutput(true);
            httpURLConnection.addRequestProperty(OAuth.HeaderType.CONTENT_TYPE, request.getBodyContentType());
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(body);
            dataOutputStream.close();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private HttpURLConnection m27301(URL url, Request<?> request) throws IOException {
        HttpURLConnection r0 = m27304(url);
        int timeoutMs = request.getTimeoutMs();
        r0.setConnectTimeout(timeoutMs);
        r0.setReadTimeout(timeoutMs);
        r0.setUseCaches(false);
        r0.setDoInput(true);
        if (TyphoonApp.HTTPS.equals(url.getProtocol()) && this.f21470 != null) {
            ((HttpsURLConnection) r0).setSSLSocketFactory(this.f21470);
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static HttpEntity m27302(HttpURLConnection httpURLConnection) {
        InputStream errorStream;
        BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
        try {
            errorStream = httpURLConnection.getInputStream();
        } catch (IOException e) {
            errorStream = httpURLConnection.getErrorStream();
        }
        basicHttpEntity.setContent(errorStream);
        basicHttpEntity.setContentLength((long) httpURLConnection.getContentLength());
        basicHttpEntity.setContentEncoding(httpURLConnection.getContentEncoding());
        basicHttpEntity.setContentType(httpURLConnection.getContentType());
        return basicHttpEntity;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m27303(HttpURLConnection httpURLConnection, Request<?> request) throws IOException, AuthFailureError {
        switch (request.getMethod()) {
            case -1:
                byte[] postBody = request.getPostBody();
                if (postBody != null) {
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setRequestMethod(OAuth.HttpMethod.POST);
                    httpURLConnection.addRequestProperty(OAuth.HeaderType.CONTENT_TYPE, request.getPostBodyContentType());
                    DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                    dataOutputStream.write(postBody);
                    dataOutputStream.close();
                    return;
                }
                return;
            case 0:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.GET);
                return;
            case 1:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.POST);
                m27300(httpURLConnection, request);
                return;
            case 2:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.PUT);
                m27300(httpURLConnection, request);
                return;
            case 3:
                httpURLConnection.setRequestMethod(OAuth.HttpMethod.DELETE);
                return;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                return;
            case 5:
                httpURLConnection.setRequestMethod("OPTIONS");
                return;
            case 6:
                httpURLConnection.setRequestMethod("TRACE");
                return;
            case 7:
                httpURLConnection.setRequestMethod(HttpClientStack.HttpPatch.METHOD_NAME);
                m27300(httpURLConnection, request);
                return;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }

    public HttpResponse performRequest(Request<?> request, Map<String, String> map) throws IOException, AuthFailureError {
        String url = request.getUrl();
        HashMap hashMap = new HashMap();
        hashMap.putAll(request.getHeaders());
        hashMap.putAll(map);
        if (this.f21471 != null) {
            String rewriteUrl = this.f21471.rewriteUrl(url);
            if (rewriteUrl == null) {
                throw new IOException("URL blocked by rewriter: " + url);
            }
            url = rewriteUrl;
        }
        HttpURLConnection r2 = m27301(new URL(url), request);
        for (String str : hashMap.keySet()) {
            r2.addRequestProperty(str, (String) hashMap.get(str));
        }
        m27303(r2, request);
        ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 1, 1);
        if (r2.getResponseCode() == -1) {
            throw new IOException("Could not retrieve response code from HttpUrlConnection.");
        }
        BasicHttpResponse basicHttpResponse = new BasicHttpResponse(new BasicStatusLine(protocolVersion, r2.getResponseCode(), r2.getResponseMessage()));
        basicHttpResponse.setEntity(m27302(r2));
        for (Map.Entry entry : r2.getHeaderFields().entrySet()) {
            if (entry.getKey() != null) {
                basicHttpResponse.addHeader(new BasicHeader((String) entry.getKey(), (String) ((List) entry.getValue()).get(0)));
            }
        }
        return basicHttpResponse;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public HttpURLConnection m27304(URL url) throws IOException {
        return (HttpURLConnection) url.openConnection();
    }
}
