package com.mopub.volley.toolbox;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.ImageLoader;

public class NetworkImageView extends ImageView {

    /* renamed from: 连任  reason: contains not printable characters */
    private ImageLoader.ImageContainer f21505;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public int f21506;

    /* renamed from: 麤  reason: contains not printable characters */
    private ImageLoader f21507;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public int f21508;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f21509;

    public NetworkImageView(Context context) {
        this(context, (AttributeSet) null);
    }

    public NetworkImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NetworkImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27329() {
        if (this.f21506 != 0) {
            setImageResource(this.f21506);
        } else {
            setImageBitmap((Bitmap) null);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.f21505 != null) {
            this.f21505.cancelRequest();
            setImageBitmap((Bitmap) null);
            this.f21505 = null;
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        m27330(true);
    }

    public void setDefaultImageResId(int i) {
        this.f21506 = i;
    }

    public void setErrorImageResId(int i) {
        this.f21508 = i;
    }

    public void setImageUrl(String str, ImageLoader imageLoader) {
        this.f21509 = str;
        this.f21507 = imageLoader;
        m27330(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27330(final boolean z) {
        int width = getWidth();
        int height = getHeight();
        boolean z2 = false;
        boolean z3 = false;
        if (getLayoutParams() != null) {
            z2 = getLayoutParams().width == -2;
            z3 = getLayoutParams().height == -2;
        }
        boolean z4 = z2 && z3;
        if (width != 0 || height != 0 || z4) {
            if (TextUtils.isEmpty(this.f21509)) {
                if (this.f21505 != null) {
                    this.f21505.cancelRequest();
                    this.f21505 = null;
                }
                m27329();
                return;
            }
            if (!(this.f21505 == null || this.f21505.getRequestUrl() == null)) {
                if (!this.f21505.getRequestUrl().equals(this.f21509)) {
                    this.f21505.cancelRequest();
                    m27329();
                } else {
                    return;
                }
            }
            this.f21505 = this.f21507.get(this.f21509, new ImageLoader.ImageListener() {
                public void onErrorResponse(VolleyError volleyError) {
                    if (NetworkImageView.this.f21508 != 0) {
                        NetworkImageView.this.setImageResource(NetworkImageView.this.f21508);
                    }
                }

                public void onResponse(final ImageLoader.ImageContainer imageContainer, boolean z) {
                    if (z && z) {
                        NetworkImageView.this.post(new Runnable() {
                            public void run() {
                                AnonymousClass1.this.onResponse(imageContainer, false);
                            }
                        });
                    } else if (imageContainer.getBitmap() != null) {
                        NetworkImageView.this.setImageBitmap(imageContainer.getBitmap());
                    } else if (NetworkImageView.this.f21506 != 0) {
                        NetworkImageView.this.setImageResource(NetworkImageView.this.f21506);
                    }
                }
            }, z2 ? 0 : width, z3 ? 0 : height);
        }
    }
}
