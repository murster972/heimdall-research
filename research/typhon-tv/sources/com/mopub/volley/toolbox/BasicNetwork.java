package com.mopub.volley.toolbox;

import com.mopub.volley.Cache;
import com.mopub.volley.Network;
import com.mopub.volley.Request;
import com.mopub.volley.RetryPolicy;
import com.mopub.volley.ServerError;
import com.mopub.volley.VolleyError;
import com.mopub.volley.VolleyLog;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.impl.cookie.DateUtils;

public class BasicNetwork implements Network {

    /* renamed from: 连任  reason: contains not printable characters */
    private static int f21452 = 4096;

    /* renamed from: 麤  reason: contains not printable characters */
    private static int f21453 = 3000;

    /* renamed from: 龘  reason: contains not printable characters */
    protected static final boolean f21454 = VolleyLog.DEBUG;

    /* renamed from: 靐  reason: contains not printable characters */
    protected final HttpStack f21455;

    /* renamed from: 齉  reason: contains not printable characters */
    protected final ByteArrayPool f21456;

    public BasicNetwork(HttpStack httpStack) {
        this(httpStack, new ByteArrayPool(f21452));
    }

    public BasicNetwork(HttpStack httpStack, ByteArrayPool byteArrayPool) {
        this.f21455 = httpStack;
        this.f21456 = byteArrayPool;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static Map<String, String> m27274(Header[] headerArr) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headerArr.length; i++) {
            treeMap.put(headerArr[i].getName(), headerArr[i].getValue());
        }
        return treeMap;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27275(long j, Request<?> request, byte[] bArr, StatusLine statusLine) {
        if (f21454 || j > ((long) f21453)) {
            Object[] objArr = new Object[5];
            objArr[0] = request;
            objArr[1] = Long.valueOf(j);
            objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
            objArr[3] = Integer.valueOf(statusLine.getStatusCode());
            objArr[4] = Integer.valueOf(request.getRetryPolicy().getCurrentRetryCount());
            VolleyLog.d("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", objArr);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m27276(String str, Request<?> request, VolleyError volleyError) throws VolleyError {
        RetryPolicy retryPolicy = request.getRetryPolicy();
        int timeoutMs = request.getTimeoutMs();
        try {
            retryPolicy.retry(volleyError);
            request.addMarker(String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(timeoutMs)}));
        } catch (VolleyError e) {
            request.addMarker(String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(timeoutMs)}));
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27277(Map<String, String> map, Cache.Entry entry) {
        if (entry != null) {
            if (entry.etag != null) {
                map.put("If-None-Match", entry.etag);
            }
            if (entry.serverDate > 0) {
                map.put("If-Modified-Since", DateUtils.formatDate(new Date(entry.serverDate)));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private byte[] m27278(HttpEntity httpEntity) throws IOException, ServerError {
        PoolingByteArrayOutputStream poolingByteArrayOutputStream = new PoolingByteArrayOutputStream(this.f21456, (int) httpEntity.getContentLength());
        byte[] bArr = null;
        try {
            InputStream content = httpEntity.getContent();
            if (content == null) {
                throw new ServerError();
            }
            bArr = this.f21456.getBuf(1024);
            while (true) {
                int read = content.read(bArr);
                if (read == -1) {
                    break;
                }
                poolingByteArrayOutputStream.write(bArr, 0, read);
            }
            byte[] byteArray = poolingByteArrayOutputStream.toByteArray();
            try {
            } catch (IOException e) {
                VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
            }
            return byteArray;
        } finally {
            try {
                httpEntity.consumeContent();
            } catch (IOException e2) {
                VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
            }
            this.f21456.returnBuf(bArr);
            poolingByteArrayOutputStream.close();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0148 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.mopub.volley.NetworkResponse performRequest(com.mopub.volley.Request<?> r27) throws com.mopub.volley.VolleyError {
        /*
            r26 = this;
            long r24 = android.os.SystemClock.elapsedRealtime()
        L_0x0004:
            r22 = 0
            r23 = 0
            java.util.Map r6 = java.util.Collections.emptyMap()
            java.util.HashMap r21 = new java.util.HashMap     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r21.<init>()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            com.mopub.volley.Cache$Entry r3 = r27.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r0 = r26
            r1 = r21
            r0.m27277(r1, r3)     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r0 = r26
            com.mopub.volley.toolbox.HttpStack r3 = r0.f21455     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r0 = r27
            r1 = r21
            org.apache.http.HttpResponse r22 = r3.performRequest(r0, r1)     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            org.apache.http.StatusLine r12 = r22.getStatusLine()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            int r14 = r12.getStatusCode()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            org.apache.http.Header[] r3 = r22.getAllHeaders()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            java.util.Map r6 = m27274((org.apache.http.Header[]) r3)     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r3 = 304(0x130, float:4.26E-43)
            if (r14 != r3) goto L_0x0075
            com.mopub.volley.Cache$Entry r20 = r27.getCacheEntry()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            if (r20 != 0) goto L_0x0054
            com.mopub.volley.NetworkResponse r3 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r4 = 304(0x130, float:4.26E-43)
            r5 = 0
            r7 = 1
            long r16 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            long r8 = r16 - r24
            r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r11 = r23
        L_0x0053:
            return r3
        L_0x0054:
            r0 = r20
            java.util.Map<java.lang.String, java.lang.String> r3 = r0.responseHeaders     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r3.putAll(r6)     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            com.mopub.volley.NetworkResponse r7 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r8 = 304(0x130, float:4.26E-43)
            r0 = r20
            byte[] r9 = r0.data     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r0 = r20
            java.util.Map<java.lang.String, java.lang.String> r10 = r0.responseHeaders     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r11 = 1
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            long r12 = r4 - r24
            r7.<init>(r8, r9, r10, r11, r12)     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r11 = r23
            r3 = r7
            goto L_0x0053
        L_0x0075:
            org.apache.http.HttpEntity r3 = r22.getEntity()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            if (r3 == 0) goto L_0x00b0
            org.apache.http.HttpEntity r3 = r22.getEntity()     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            r0 = r26
            byte[] r11 = r0.m27278((org.apache.http.HttpEntity) r3)     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
        L_0x0085:
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
            long r8 = r4 - r24
            r7 = r26
            r10 = r27
            r7.m27275(r8, r10, r11, r12)     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
            r3 = 200(0xc8, float:2.8E-43)
            if (r14 < r3) goto L_0x009a
            r3 = 299(0x12b, float:4.19E-43)
            if (r14 <= r3) goto L_0x00b4
        L_0x009a:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
            r3.<init>()     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
            throw r3     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
        L_0x00a0:
            r2 = move-exception
        L_0x00a1:
            java.lang.String r3 = "socket"
            com.mopub.volley.TimeoutError r4 = new com.mopub.volley.TimeoutError
            r4.<init>()
            r0 = r27
            m27276(r3, r0, r4)
            goto L_0x0004
        L_0x00b0:
            r3 = 0
            byte[] r11 = new byte[r3]     // Catch:{ SocketTimeoutException -> 0x0162, ConnectTimeoutException -> 0x00c6, MalformedURLException -> 0x00d8, IOException -> 0x00f9 }
            goto L_0x0085
        L_0x00b4:
            com.mopub.volley.NetworkResponse r13 = new com.mopub.volley.NetworkResponse     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
            r17 = 0
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
            long r18 = r4 - r24
            r15 = r11
            r16 = r6
            r13.<init>(r14, r15, r16, r17, r18)     // Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x015f, MalformedURLException -> 0x015c, IOException -> 0x015a }
            r3 = r13
            goto L_0x0053
        L_0x00c6:
            r2 = move-exception
            r11 = r23
        L_0x00c9:
            java.lang.String r3 = "connection"
            com.mopub.volley.TimeoutError r4 = new com.mopub.volley.TimeoutError
            r4.<init>()
            r0 = r27
            m27276(r3, r0, r4)
            goto L_0x0004
        L_0x00d8:
            r2 = move-exception
            r11 = r23
        L_0x00db:
            java.lang.RuntimeException r3 = new java.lang.RuntimeException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Bad URL "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r27.getUrl()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.<init>(r4, r2)
            throw r3
        L_0x00f9:
            r2 = move-exception
            r11 = r23
        L_0x00fc:
            r14 = 0
            r13 = 0
            if (r22 == 0) goto L_0x0148
            org.apache.http.StatusLine r3 = r22.getStatusLine()
            int r14 = r3.getStatusCode()
            java.lang.String r3 = "Unexpected response code %d for %s"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r14)
            r4[r5] = r7
            r5 = 1
            java.lang.String r7 = r27.getUrl()
            r4[r5] = r7
            com.mopub.volley.VolleyLog.e(r3, r4)
            if (r11 == 0) goto L_0x0154
            com.mopub.volley.NetworkResponse r13 = new com.mopub.volley.NetworkResponse
            r17 = 0
            long r4 = android.os.SystemClock.elapsedRealtime()
            long r18 = r4 - r24
            r15 = r11
            r16 = r6
            r13.<init>(r14, r15, r16, r17, r18)
            r3 = 401(0x191, float:5.62E-43)
            if (r14 == r3) goto L_0x0139
            r3 = 403(0x193, float:5.65E-43)
            if (r14 != r3) goto L_0x014e
        L_0x0139:
            java.lang.String r3 = "auth"
            com.mopub.volley.AuthFailureError r4 = new com.mopub.volley.AuthFailureError
            r4.<init>((com.mopub.volley.NetworkResponse) r13)
            r0 = r27
            m27276(r3, r0, r4)
            goto L_0x0004
        L_0x0148:
            com.mopub.volley.NoConnectionError r3 = new com.mopub.volley.NoConnectionError
            r3.<init>(r2)
            throw r3
        L_0x014e:
            com.mopub.volley.ServerError r3 = new com.mopub.volley.ServerError
            r3.<init>(r13)
            throw r3
        L_0x0154:
            com.mopub.volley.NetworkError r3 = new com.mopub.volley.NetworkError
            r3.<init>((com.mopub.volley.NetworkResponse) r13)
            throw r3
        L_0x015a:
            r2 = move-exception
            goto L_0x00fc
        L_0x015c:
            r2 = move-exception
            goto L_0x00db
        L_0x015f:
            r2 = move-exception
            goto L_0x00c9
        L_0x0162:
            r2 = move-exception
            r11 = r23
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.volley.toolbox.BasicNetwork.performRequest(com.mopub.volley.Request):com.mopub.volley.NetworkResponse");
    }
}
