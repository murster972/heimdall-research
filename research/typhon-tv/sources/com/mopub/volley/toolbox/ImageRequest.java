package com.mopub.volley.toolbox;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.ParseError;
import com.mopub.volley.Request;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyLog;

public class ImageRequest extends Request<Bitmap> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Object f21497 = new Object();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Bitmap.Config f21498;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f21499;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f21500;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Response.Listener<Bitmap> f21501;

    public ImageRequest(String str, Response.Listener<Bitmap> listener, int i, int i2, Bitmap.Config config, Response.ErrorListener errorListener) {
        super(0, str, errorListener);
        setRetryPolicy(new DefaultRetryPolicy(1000, 2, 2.0f));
        this.f21501 = listener;
        this.f21498 = config;
        this.f21500 = i;
        this.f21499 = i2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m27319(int i, int i2, int i3, int i4) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (i == 0) {
            return (int) (((double) i3) * (((double) i2) / ((double) i4)));
        } else if (i2 == 0) {
            return i;
        } else {
            double d = ((double) i4) / ((double) i3);
            int i5 = i;
            if (((double) i5) * d > ((double) i2)) {
                i5 = (int) (((double) i2) / d);
            }
            return i5;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Response<Bitmap> m27320(NetworkResponse networkResponse) {
        Bitmap bitmap;
        byte[] bArr = networkResponse.data;
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (this.f21500 == 0 && this.f21499 == 0) {
            options.inPreferredConfig = this.f21498;
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } else {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i = options.outWidth;
            int i2 = options.outHeight;
            int r6 = m27319(this.f21500, this.f21499, i, i2);
            int r5 = m27319(this.f21499, this.f21500, i2, i);
            options.inJustDecodeBounds = false;
            options.inSampleSize = m27321(i, i2, r6, r5);
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            if (decodeByteArray == null || (decodeByteArray.getWidth() <= r6 && decodeByteArray.getHeight() <= r5)) {
                bitmap = decodeByteArray;
            } else {
                bitmap = Bitmap.createScaledBitmap(decodeByteArray, r6, r5, true);
                decodeByteArray.recycle();
            }
        }
        return bitmap == null ? Response.error(new ParseError(networkResponse)) : Response.success(bitmap, HttpHeaderParser.parseCacheHeaders(networkResponse));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m27321(int i, int i2, int i3, int i4) {
        float f = 1.0f;
        while (((double) (2.0f * f)) <= Math.min(((double) i) / ((double) i3), ((double) i2) / ((double) i4))) {
            f *= 2.0f;
        }
        return (int) f;
    }

    public Request.Priority getPriority() {
        return Request.Priority.LOW;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Response<Bitmap> m27322(NetworkResponse networkResponse) {
        Response<Bitmap> error;
        synchronized (f21497) {
            try {
                error = m27320(networkResponse);
            } catch (OutOfMemoryError e) {
                VolleyLog.e("Caught OOM for %d byte image, url=%s", Integer.valueOf(networkResponse.data.length), getUrl());
                error = Response.error(new ParseError((Throwable) e));
            }
        }
        return error;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void deliverResponse(Bitmap bitmap) {
        this.f21501.onResponse(bitmap);
    }
}
