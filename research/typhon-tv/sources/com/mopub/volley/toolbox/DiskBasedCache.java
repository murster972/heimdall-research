package com.mopub.volley.toolbox;

import android.os.SystemClock;
import com.mopub.volley.Cache;
import com.mopub.volley.VolleyLog;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class DiskBasedCache implements Cache {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f21464;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f21465;

    /* renamed from: 齉  reason: contains not printable characters */
    private final File f21466;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, CacheHeader> f21467;

    static class CacheHeader {
        public String etag;
        public String key;
        public Map<String, String> responseHeaders;
        public long serverDate;
        public long size;
        public long softTtl;
        public long ttl;

        private CacheHeader() {
        }

        public CacheHeader(String str, Cache.Entry entry) {
            this.key = str;
            this.size = (long) entry.data.length;
            this.etag = entry.etag;
            this.serverDate = entry.serverDate;
            this.ttl = entry.ttl;
            this.softTtl = entry.softTtl;
            this.responseHeaders = entry.responseHeaders;
        }

        public static CacheHeader readHeader(InputStream inputStream) throws IOException {
            CacheHeader cacheHeader = new CacheHeader();
            if (DiskBasedCache.m27286(inputStream) != 538183203) {
                throw new IOException();
            }
            cacheHeader.key = DiskBasedCache.m27285(inputStream);
            cacheHeader.etag = DiskBasedCache.m27285(inputStream);
            if (cacheHeader.etag.equals("")) {
                cacheHeader.etag = null;
            }
            cacheHeader.serverDate = DiskBasedCache.m27282(inputStream);
            cacheHeader.ttl = DiskBasedCache.m27282(inputStream);
            cacheHeader.softTtl = DiskBasedCache.m27282(inputStream);
            cacheHeader.responseHeaders = DiskBasedCache.m27284(inputStream);
            return cacheHeader;
        }

        public Cache.Entry toCacheEntry(byte[] bArr) {
            Cache.Entry entry = new Cache.Entry();
            entry.data = bArr;
            entry.etag = this.etag;
            entry.serverDate = this.serverDate;
            entry.ttl = this.ttl;
            entry.softTtl = this.softTtl;
            entry.responseHeaders = this.responseHeaders;
            return entry;
        }

        public boolean writeHeader(OutputStream outputStream) {
            try {
                DiskBasedCache.m27289(outputStream, 538183203);
                DiskBasedCache.m27291(outputStream, this.key);
                DiskBasedCache.m27291(outputStream, this.etag == null ? "" : this.etag);
                DiskBasedCache.m27290(outputStream, this.serverDate);
                DiskBasedCache.m27290(outputStream, this.ttl);
                DiskBasedCache.m27290(outputStream, this.softTtl);
                DiskBasedCache.m27293(this.responseHeaders, outputStream);
                outputStream.flush();
                return true;
            } catch (IOException e) {
                VolleyLog.d("%s", e.toString());
                return false;
            }
        }
    }

    private static class CountingInputStream extends FilterInputStream {
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public int f21468;

        private CountingInputStream(InputStream inputStream) {
            super(inputStream);
            this.f21468 = 0;
        }

        public int read() throws IOException {
            int read = super.read();
            if (read != -1) {
                this.f21468++;
            }
            return read;
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = super.read(bArr, i, i2);
            if (read != -1) {
                this.f21468 += read;
            }
            return read;
        }
    }

    public DiskBasedCache(File file) {
        this(file, 5242880);
    }

    public DiskBasedCache(File file, int i) {
        this.f21467 = new LinkedHashMap(16, 0.75f, true);
        this.f21464 = 0;
        this.f21466 = file;
        this.f21465 = i;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static int m27281(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static long m27282(InputStream inputStream) throws IOException {
        return 0 | ((((long) m27281(inputStream)) & 255) << 0) | ((((long) m27281(inputStream)) & 255) << 8) | ((((long) m27281(inputStream)) & 255) << 16) | ((((long) m27281(inputStream)) & 255) << 24) | ((((long) m27281(inputStream)) & 255) << 32) | ((((long) m27281(inputStream)) & 255) << 40) | ((((long) m27281(inputStream)) & 255) << 48) | ((((long) m27281(inputStream)) & 255) << 56);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m27283(String str) {
        CacheHeader cacheHeader = this.f21467.get(str);
        if (cacheHeader != null) {
            this.f21464 -= cacheHeader.size;
            this.f21467.remove(str);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    static Map<String, String> m27284(InputStream inputStream) throws IOException {
        int r3 = m27286(inputStream);
        Map<String, String> emptyMap = r3 == 0 ? Collections.emptyMap() : new HashMap<>(r3);
        for (int i = 0; i < r3; i++) {
            emptyMap.put(m27285(inputStream).intern(), m27285(inputStream).intern());
        }
        return emptyMap;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static String m27285(InputStream inputStream) throws IOException {
        return new String(m27294(inputStream, (int) m27282(inputStream)), "UTF-8");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m27286(InputStream inputStream) throws IOException {
        return 0 | (m27281(inputStream) << 0) | (m27281(inputStream) << 8) | (m27281(inputStream) << 16) | (m27281(inputStream) << 24);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m27287(String str) {
        int length = str.length() / 2;
        return String.valueOf(str.substring(0, length).hashCode()) + String.valueOf(str.substring(length).hashCode());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27288(int i) {
        if (this.f21464 + ((long) i) >= ((long) this.f21465)) {
            if (VolleyLog.DEBUG) {
                VolleyLog.v("Pruning old cache entries.", new Object[0]);
            }
            long j = this.f21464;
            int i2 = 0;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            Iterator<Map.Entry<String, CacheHeader>> it2 = this.f21467.entrySet().iterator();
            while (it2.hasNext()) {
                CacheHeader cacheHeader = (CacheHeader) it2.next().getValue();
                if (getFileForKey(cacheHeader.key).delete()) {
                    this.f21464 -= cacheHeader.size;
                } else {
                    VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", cacheHeader.key, m27287(cacheHeader.key));
                }
                it2.remove();
                i2++;
                if (((float) (this.f21464 + ((long) i))) < ((float) this.f21465) * 0.9f) {
                    break;
                }
            }
            if (VolleyLog.DEBUG) {
                VolleyLog.v("pruned %d files, %d bytes, %d ms", Integer.valueOf(i2), Long.valueOf(this.f21464 - j), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m27289(OutputStream outputStream, int i) throws IOException {
        outputStream.write((i >> 0) & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write((i >> 24) & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m27290(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) (j >>> 0)));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m27291(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        m27290(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27292(String str, CacheHeader cacheHeader) {
        if (!this.f21467.containsKey(str)) {
            this.f21464 += cacheHeader.size;
        } else {
            this.f21464 += cacheHeader.size - this.f21467.get(str).size;
        }
        this.f21467.put(str, cacheHeader);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m27293(Map<String, String> map, OutputStream outputStream) throws IOException {
        if (map != null) {
            m27289(outputStream, map.size());
            for (Map.Entry next : map.entrySet()) {
                m27291(outputStream, (String) next.getKey());
                m27291(outputStream, (String) next.getValue());
            }
            return;
        }
        m27289(outputStream, 0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static byte[] m27294(InputStream inputStream, int i) throws IOException {
        byte[] bArr = new byte[i];
        int i2 = 0;
        while (i2 < i) {
            int read = inputStream.read(bArr, i2, i - i2);
            if (read == -1) {
                break;
            }
            i2 += read;
        }
        if (i2 == i) {
            return bArr;
        }
        throw new IOException("Expected " + i + " bytes, read " + i2 + " bytes");
    }

    public synchronized void clear() {
        File[] listFiles = this.f21466.listFiles();
        if (listFiles != null) {
            for (File delete : listFiles) {
                delete.delete();
            }
        }
        this.f21467.clear();
        this.f21464 = 0;
        VolleyLog.d("Cache cleared.", new Object[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0063 A[SYNTHETIC, Splitter:B:29:0x0063] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.mopub.volley.Cache.Entry get(java.lang.String r13) {
        /*
            r12 = this;
            r7 = 0
            monitor-enter(r12)
            java.util.Map<java.lang.String, com.mopub.volley.toolbox.DiskBasedCache$CacheHeader> r8 = r12.f21467     // Catch:{ all -> 0x0067 }
            java.lang.Object r4 = r8.get(r13)     // Catch:{ all -> 0x0067 }
            com.mopub.volley.toolbox.DiskBasedCache$CacheHeader r4 = (com.mopub.volley.toolbox.DiskBasedCache.CacheHeader) r4     // Catch:{ all -> 0x0067 }
            if (r4 != 0) goto L_0x000e
        L_0x000c:
            monitor-exit(r12)
            return r7
        L_0x000e:
            java.io.File r5 = r12.getFileForKey(r13)     // Catch:{ all -> 0x0067 }
            r0 = 0
            com.mopub.volley.toolbox.DiskBasedCache$CountingInputStream r1 = new com.mopub.volley.toolbox.DiskBasedCache$CountingInputStream     // Catch:{ IOException -> 0x003d }
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ IOException -> 0x003d }
            r8.<init>(r5)     // Catch:{ IOException -> 0x003d }
            r9 = 0
            r1.<init>(r8)     // Catch:{ IOException -> 0x003d }
            com.mopub.volley.toolbox.DiskBasedCache.CacheHeader.readHeader(r1)     // Catch:{ IOException -> 0x006f, all -> 0x006c }
            long r8 = r5.length()     // Catch:{ IOException -> 0x006f, all -> 0x006c }
            int r10 = r1.f21468     // Catch:{ IOException -> 0x006f, all -> 0x006c }
            long r10 = (long) r10     // Catch:{ IOException -> 0x006f, all -> 0x006c }
            long r8 = r8 - r10
            int r8 = (int) r8     // Catch:{ IOException -> 0x006f, all -> 0x006c }
            byte[] r2 = m27294((java.io.InputStream) r1, (int) r8)     // Catch:{ IOException -> 0x006f, all -> 0x006c }
            com.mopub.volley.Cache$Entry r8 = r4.toCacheEntry(r2)     // Catch:{ IOException -> 0x006f, all -> 0x006c }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x003b }
        L_0x0039:
            r7 = r8
            goto L_0x000c
        L_0x003b:
            r6 = move-exception
            goto L_0x000c
        L_0x003d:
            r3 = move-exception
        L_0x003e:
            java.lang.String r8 = "%s: %s"
            r9 = 2
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ all -> 0x0060 }
            r10 = 0
            java.lang.String r11 = r5.getAbsolutePath()     // Catch:{ all -> 0x0060 }
            r9[r10] = r11     // Catch:{ all -> 0x0060 }
            r10 = 1
            java.lang.String r11 = r3.toString()     // Catch:{ all -> 0x0060 }
            r9[r10] = r11     // Catch:{ all -> 0x0060 }
            com.mopub.volley.VolleyLog.d(r8, r9)     // Catch:{ all -> 0x0060 }
            r12.remove(r13)     // Catch:{ all -> 0x0060 }
            if (r0 == 0) goto L_0x000c
            r0.close()     // Catch:{ IOException -> 0x005e }
            goto L_0x000c
        L_0x005e:
            r6 = move-exception
            goto L_0x000c
        L_0x0060:
            r8 = move-exception
        L_0x0061:
            if (r0 == 0) goto L_0x0066
            r0.close()     // Catch:{ IOException -> 0x006a }
        L_0x0066:
            throw r8     // Catch:{ all -> 0x0067 }
        L_0x0067:
            r7 = move-exception
            monitor-exit(r12)
            throw r7
        L_0x006a:
            r6 = move-exception
            goto L_0x000c
        L_0x006c:
            r8 = move-exception
            r0 = r1
            goto L_0x0061
        L_0x006f:
            r3 = move-exception
            r0 = r1
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.volley.toolbox.DiskBasedCache.get(java.lang.String):com.mopub.volley.Cache$Entry");
    }

    public File getFileForKey(String str) {
        return new File(this.f21466, m27287(str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x005d A[SYNTHETIC, Splitter:B:29:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0062 A[SYNTHETIC, Splitter:B:32:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006b A[SYNTHETIC, Splitter:B:37:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0054 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void initialize() {
        /*
            r13 = this;
            monitor-enter(r13)
            java.io.File r9 = r13.f21466     // Catch:{ all -> 0x006f }
            boolean r9 = r9.exists()     // Catch:{ all -> 0x006f }
            if (r9 != 0) goto L_0x0025
            java.io.File r9 = r13.f21466     // Catch:{ all -> 0x006f }
            boolean r9 = r9.mkdirs()     // Catch:{ all -> 0x006f }
            if (r9 != 0) goto L_0x0023
            java.lang.String r9 = "Unable to create cache dir %s"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ all -> 0x006f }
            r11 = 0
            java.io.File r12 = r13.f21466     // Catch:{ all -> 0x006f }
            java.lang.String r12 = r12.getAbsolutePath()     // Catch:{ all -> 0x006f }
            r10[r11] = r12     // Catch:{ all -> 0x006f }
            com.mopub.volley.VolleyLog.e(r9, r10)     // Catch:{ all -> 0x006f }
        L_0x0023:
            monitor-exit(r13)
            return
        L_0x0025:
            java.io.File r9 = r13.f21466     // Catch:{ all -> 0x006f }
            java.io.File[] r4 = r9.listFiles()     // Catch:{ all -> 0x006f }
            if (r4 == 0) goto L_0x0023
            r0 = r4
            int r8 = r0.length     // Catch:{ all -> 0x006f }
            r7 = 0
        L_0x0030:
            if (r7 >= r8) goto L_0x0023
            r3 = r0[r7]     // Catch:{ all -> 0x006f }
            r5 = 0
            java.io.BufferedInputStream r6 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x005a }
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ IOException -> 0x005a }
            r9.<init>(r3)     // Catch:{ IOException -> 0x005a }
            r6.<init>(r9)     // Catch:{ IOException -> 0x005a }
            com.mopub.volley.toolbox.DiskBasedCache$CacheHeader r2 = com.mopub.volley.toolbox.DiskBasedCache.CacheHeader.readHeader(r6)     // Catch:{ IOException -> 0x0077, all -> 0x0074 }
            long r10 = r3.length()     // Catch:{ IOException -> 0x0077, all -> 0x0074 }
            r2.size = r10     // Catch:{ IOException -> 0x0077, all -> 0x0074 }
            java.lang.String r9 = r2.key     // Catch:{ IOException -> 0x0077, all -> 0x0074 }
            r13.m27292((java.lang.String) r9, (com.mopub.volley.toolbox.DiskBasedCache.CacheHeader) r2)     // Catch:{ IOException -> 0x0077, all -> 0x0074 }
            if (r6 == 0) goto L_0x0053
            r6.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0053:
            r5 = r6
        L_0x0054:
            int r7 = r7 + 1
            goto L_0x0030
        L_0x0057:
            r9 = move-exception
            r5 = r6
            goto L_0x0054
        L_0x005a:
            r1 = move-exception
        L_0x005b:
            if (r3 == 0) goto L_0x0060
            r3.delete()     // Catch:{ all -> 0x0068 }
        L_0x0060:
            if (r5 == 0) goto L_0x0054
            r5.close()     // Catch:{ IOException -> 0x0066 }
            goto L_0x0054
        L_0x0066:
            r9 = move-exception
            goto L_0x0054
        L_0x0068:
            r9 = move-exception
        L_0x0069:
            if (r5 == 0) goto L_0x006e
            r5.close()     // Catch:{ IOException -> 0x0072 }
        L_0x006e:
            throw r9     // Catch:{ all -> 0x006f }
        L_0x006f:
            r9 = move-exception
            monitor-exit(r13)
            throw r9
        L_0x0072:
            r10 = move-exception
            goto L_0x006e
        L_0x0074:
            r9 = move-exception
            r5 = r6
            goto L_0x0069
        L_0x0077:
            r1 = move-exception
            r5 = r6
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.volley.toolbox.DiskBasedCache.initialize():void");
    }

    public synchronized void invalidate(String str, boolean z) {
        Cache.Entry entry = get(str);
        if (entry != null) {
            entry.softTtl = 0;
            if (z) {
                entry.ttl = 0;
            }
            put(str, entry);
        }
    }

    public synchronized void put(String str, Cache.Entry entry) {
        m27288(entry.data.length);
        File fileForKey = getFileForKey(str);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileForKey);
            CacheHeader cacheHeader = new CacheHeader(str, entry);
            if (!cacheHeader.writeHeader(fileOutputStream)) {
                fileOutputStream.close();
                VolleyLog.d("Failed to write header for %s", fileForKey.getAbsolutePath());
                throw new IOException();
            }
            fileOutputStream.write(entry.data);
            fileOutputStream.close();
            m27292(str, cacheHeader);
        } catch (IOException e) {
            if (!fileForKey.delete()) {
                VolleyLog.d("Could not clean up file %s", fileForKey.getAbsolutePath());
            }
        }
    }

    public synchronized void remove(String str) {
        boolean delete = getFileForKey(str).delete();
        m27283(str);
        if (!delete) {
            VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", str, m27287(str));
        }
    }
}
