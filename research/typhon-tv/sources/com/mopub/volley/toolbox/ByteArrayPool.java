package com.mopub.volley.toolbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ByteArrayPool {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static final Comparator<byte[]> f21457 = new Comparator<byte[]>() {
        public int compare(byte[] bArr, byte[] bArr2) {
            return bArr.length - bArr2.length;
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f21458;

    /* renamed from: 靐  reason: contains not printable characters */
    private List<byte[]> f21459 = new LinkedList();

    /* renamed from: 麤  reason: contains not printable characters */
    private int f21460 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    private List<byte[]> f21461 = new ArrayList(64);

    public ByteArrayPool(int i) {
        this.f21458 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m27279() {
        while (this.f21460 > this.f21458) {
            byte[] remove = this.f21459.remove(0);
            this.f21461.remove(remove);
            this.f21460 -= remove.length;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r0 = new byte[r5];
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized byte[] getBuf(int r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r1 = 0
        L_0x0002:
            java.util.List<byte[]> r2 = r4.f21461     // Catch:{ all -> 0x002d }
            int r2 = r2.size()     // Catch:{ all -> 0x002d }
            if (r1 >= r2) goto L_0x002a
            java.util.List<byte[]> r2 = r4.f21461     // Catch:{ all -> 0x002d }
            java.lang.Object r0 = r2.get(r1)     // Catch:{ all -> 0x002d }
            byte[] r0 = (byte[]) r0     // Catch:{ all -> 0x002d }
            int r2 = r0.length     // Catch:{ all -> 0x002d }
            if (r2 < r5) goto L_0x0027
            int r2 = r4.f21460     // Catch:{ all -> 0x002d }
            int r3 = r0.length     // Catch:{ all -> 0x002d }
            int r2 = r2 - r3
            r4.f21460 = r2     // Catch:{ all -> 0x002d }
            java.util.List<byte[]> r2 = r4.f21461     // Catch:{ all -> 0x002d }
            r2.remove(r1)     // Catch:{ all -> 0x002d }
            java.util.List<byte[]> r2 = r4.f21459     // Catch:{ all -> 0x002d }
            r2.remove(r0)     // Catch:{ all -> 0x002d }
        L_0x0025:
            monitor-exit(r4)
            return r0
        L_0x0027:
            int r1 = r1 + 1
            goto L_0x0002
        L_0x002a:
            byte[] r0 = new byte[r5]     // Catch:{ all -> 0x002d }
            goto L_0x0025
        L_0x002d:
            r2 = move-exception
            monitor-exit(r4)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.volley.toolbox.ByteArrayPool.getBuf(int):byte[]");
    }

    public synchronized void returnBuf(byte[] bArr) {
        if (bArr != null) {
            if (bArr.length <= this.f21458) {
                this.f21459.add(bArr);
                int binarySearch = Collections.binarySearch(this.f21461, bArr, f21457);
                if (binarySearch < 0) {
                    binarySearch = (-binarySearch) - 1;
                }
                this.f21461.add(binarySearch, bArr);
                this.f21460 += bArr.length;
                m27279();
            }
        }
    }
}
