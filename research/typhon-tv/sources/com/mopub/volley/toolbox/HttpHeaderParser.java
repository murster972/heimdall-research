package com.mopub.volley.toolbox;

import com.mopub.volley.Cache;
import com.mopub.volley.NetworkResponse;
import java.util.Map;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.oltu.oauth2.common.OAuth;

public class HttpHeaderParser {
    public static Cache.Entry parseCacheHeaders(NetworkResponse networkResponse) {
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = networkResponse.headers;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        boolean z = false;
        String str = map.get("Date");
        if (str != null) {
            j = parseDateAsEpoch(str);
        }
        String str2 = map.get("Cache-Control");
        if (str2 != null) {
            z = true;
            String[] split = str2.split(",");
            for (String trim : split) {
                String trim2 = trim.trim();
                if (trim2.equals("no-cache") || trim2.equals("no-store")) {
                    return null;
                }
                if (trim2.startsWith("max-age=")) {
                    try {
                        j4 = Long.parseLong(trim2.substring(8));
                    } catch (Exception e) {
                    }
                } else if (trim2.equals("must-revalidate") || trim2.equals("proxy-revalidate")) {
                    j4 = 0;
                }
            }
        }
        String str3 = map.get("Expires");
        if (str3 != null) {
            j2 = parseDateAsEpoch(str3);
        }
        String str4 = map.get("ETag");
        if (z) {
            j3 = currentTimeMillis + (1000 * j4);
        } else if (j > 0 && j2 >= j) {
            j3 = currentTimeMillis + (j2 - j);
        }
        Cache.Entry entry = new Cache.Entry();
        entry.data = networkResponse.data;
        entry.etag = str4;
        entry.softTtl = j3;
        entry.ttl = entry.softTtl;
        entry.serverDate = j;
        entry.responseHeaders = map;
        return entry;
    }

    public static String parseCharset(Map<String, String> map) {
        String str = map.get(OAuth.HeaderType.CONTENT_TYPE);
        if (str != null) {
            String[] split = str.split(";");
            for (int i = 1; i < split.length; i++) {
                String[] split2 = split[i].trim().split("=");
                if (split2.length == 2 && split2[0].equals("charset")) {
                    return split2[1];
                }
            }
        }
        return "ISO-8859-1";
    }

    public static long parseDateAsEpoch(String str) {
        try {
            return DateUtils.parseDate(str).getTime();
        } catch (DateParseException e) {
            return 0;
        }
    }
}
