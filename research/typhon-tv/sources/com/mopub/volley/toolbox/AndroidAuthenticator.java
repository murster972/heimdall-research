package com.mopub.volley.toolbox;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.mopub.common.TyphoonApp;
import com.mopub.volley.AuthFailureError;

public class AndroidAuthenticator implements Authenticator {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Account f21448;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f21449;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f21450;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f21451;

    public AndroidAuthenticator(Context context, Account account, String str) {
        this(context, account, str, false);
    }

    public AndroidAuthenticator(Context context, Account account, String str, boolean z) {
        this.f21451 = context;
        this.f21448 = account;
        this.f21450 = str;
        this.f21449 = z;
    }

    public Account getAccount() {
        return this.f21448;
    }

    public String getAuthToken() throws AuthFailureError {
        AccountManagerFuture<Bundle> authToken = AccountManager.get(this.f21451).getAuthToken(this.f21448, this.f21450, this.f21449, (AccountManagerCallback) null, (Handler) null);
        try {
            Bundle result = authToken.getResult();
            String str = null;
            if (authToken.isDone() && !authToken.isCancelled()) {
                if (result.containsKey(TyphoonApp.INTENT_SCHEME)) {
                    throw new AuthFailureError((Intent) result.getParcelable(TyphoonApp.INTENT_SCHEME));
                }
                str = result.getString("authtoken");
            }
            if (str != null) {
                return str;
            }
            throw new AuthFailureError("Got null auth token for type: " + this.f21450);
        } catch (Exception e) {
            throw new AuthFailureError("Error while retrieving auth token", e);
        }
    }

    public void invalidateAuthToken(String str) {
        AccountManager.get(this.f21451).invalidateAuthToken(this.f21448.type, str);
    }
}
