package com.mopub.volley;

import android.annotation.TargetApi;
import android.net.TrafficStats;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

public class NetworkDispatcher extends Thread {

    /* renamed from: 连任  reason: contains not printable characters */
    private volatile boolean f21425 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Network f21426;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ResponseDelivery f21427;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Cache f21428;

    /* renamed from: 龘  reason: contains not printable characters */
    private final BlockingQueue<Request<?>> f21429;

    public NetworkDispatcher(BlockingQueue<Request<?>> blockingQueue, Network network, Cache cache, ResponseDelivery responseDelivery) {
        this.f21429 = blockingQueue;
        this.f21426 = network;
        this.f21428 = cache;
        this.f21427 = responseDelivery;
    }

    @TargetApi(14)
    /* renamed from: 龘  reason: contains not printable characters */
    private void m27268(Request<?> request) {
        if (Build.VERSION.SDK_INT >= 14) {
            TrafficStats.setThreadStatsTag(request.getTrafficStatsTag());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27269(Request<?> request, VolleyError volleyError) {
        this.f21427.postError(request, request.m6263(volleyError));
    }

    public void quit() {
        this.f21425 = true;
        interrupt();
    }

    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            try {
                Request take = this.f21429.take();
                try {
                    take.addMarker("network-queue-take");
                    if (take.isCanceled()) {
                        take.m6259("network-discard-cancelled");
                    } else {
                        m27268(take);
                        NetworkResponse performRequest = this.f21426.performRequest(take);
                        take.addMarker("network-http-complete");
                        if (!performRequest.notModified || !take.hasHadResponseDelivered()) {
                            Response r3 = take.m6262(performRequest);
                            take.addMarker("network-parse-complete");
                            if (take.shouldCache() && r3.cacheEntry != null) {
                                this.f21428.put(take.getCacheKey(), r3.cacheEntry);
                                take.addMarker("network-cache-written");
                            }
                            take.markDelivered();
                            this.f21427.postResponse(take, r3);
                        } else {
                            take.m6259("not-modified");
                        }
                    }
                } catch (VolleyError e) {
                    e.m27271(SystemClock.elapsedRealtime() - elapsedRealtime);
                    m27269(take, e);
                } catch (Exception e2) {
                    VolleyLog.e(e2, "Unhandled exception %s", e2.toString());
                    VolleyError volleyError = new VolleyError((Throwable) e2);
                    volleyError.m27271(SystemClock.elapsedRealtime() - elapsedRealtime);
                    this.f21427.postError(take, volleyError);
                }
            } catch (InterruptedException e3) {
                if (this.f21425) {
                    return;
                }
            }
        }
    }
}
