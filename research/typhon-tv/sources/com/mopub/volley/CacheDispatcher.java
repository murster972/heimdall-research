package com.mopub.volley;

import android.os.Process;
import com.mopub.volley.Cache;
import java.util.concurrent.BlockingQueue;

public class CacheDispatcher extends Thread {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final boolean f21406 = VolleyLog.DEBUG;

    /* renamed from: ʻ  reason: contains not printable characters */
    private volatile boolean f21407 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private final ResponseDelivery f21408;

    /* renamed from: 靐  reason: contains not printable characters */
    private final BlockingQueue<Request<?>> f21409;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Cache f21410;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final BlockingQueue<Request<?>> f21411;

    public CacheDispatcher(BlockingQueue<Request<?>> blockingQueue, BlockingQueue<Request<?>> blockingQueue2, Cache cache, ResponseDelivery responseDelivery) {
        this.f21409 = blockingQueue;
        this.f21411 = blockingQueue2;
        this.f21410 = cache;
        this.f21408 = responseDelivery;
    }

    public void quit() {
        this.f21407 = true;
        interrupt();
    }

    public void run() {
        if (f21406) {
            VolleyLog.v("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.f21410.initialize();
        while (true) {
            try {
                final Request take = this.f21409.take();
                take.addMarker("cache-queue-take");
                if (take.isCanceled()) {
                    take.m6259("cache-discard-canceled");
                } else {
                    Cache.Entry entry = this.f21410.get(take.getCacheKey());
                    if (entry == null) {
                        take.addMarker("cache-miss");
                        this.f21411.put(take);
                    } else if (entry.isExpired()) {
                        take.addMarker("cache-hit-expired");
                        take.setCacheEntry(entry);
                        this.f21411.put(take);
                    } else {
                        take.addMarker("cache-hit");
                        Response r3 = take.m6262(new NetworkResponse(entry.data, entry.responseHeaders));
                        take.addMarker("cache-hit-parsed");
                        if (!entry.refreshNeeded()) {
                            this.f21408.postResponse(take, r3);
                        } else {
                            take.addMarker("cache-hit-refresh-needed");
                            take.setCacheEntry(entry);
                            r3.intermediate = true;
                            this.f21408.postResponse(take, r3, new Runnable() {
                                public void run() {
                                    try {
                                        CacheDispatcher.this.f21411.put(take);
                                    } catch (InterruptedException e) {
                                    }
                                }
                            });
                        }
                    }
                }
            } catch (InterruptedException e) {
                if (this.f21407) {
                    return;
                }
            }
        }
    }
}
