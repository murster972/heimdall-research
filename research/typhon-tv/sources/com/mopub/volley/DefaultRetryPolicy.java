package com.mopub.volley;

public class DefaultRetryPolicy implements RetryPolicy {
    public static final float DEFAULT_BACKOFF_MULT = 1.0f;
    public static final int DEFAULT_MAX_RETRIES = 1;
    public static final int DEFAULT_TIMEOUT_MS = 2500;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f21414;

    /* renamed from: 麤  reason: contains not printable characters */
    private final float f21415;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f21416;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f21417;

    public DefaultRetryPolicy() {
        this(2500, 1, 1.0f);
    }

    public DefaultRetryPolicy(int i, int i2, float f) {
        this.f21417 = i;
        this.f21416 = i2;
        this.f21415 = f;
    }

    public float getBackoffMultiplier() {
        return this.f21415;
    }

    public int getCurrentRetryCount() {
        return this.f21414;
    }

    public int getCurrentTimeout() {
        return this.f21417;
    }

    public void retry(VolleyError volleyError) throws VolleyError {
        this.f21414++;
        this.f21417 = (int) (((float) this.f21417) + (((float) this.f21417) * this.f21415));
        if (!m27267()) {
            throw volleyError;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m27267() {
        return this.f21414 <= this.f21416;
    }
}
