package com.mopub.network;

import android.os.Looper;
import com.mopub.network.ScribeRequest;
import com.mopub.volley.Request;
import com.mopub.volley.VolleyError;

public class ScribeRequestManager extends RequestManager<ScribeRequest.ScribeRequestFactory> implements ScribeRequest.Listener {
    public ScribeRequestManager(Looper looper) {
        super(looper);
    }

    public void onErrorResponse(final VolleyError volleyError) {
        this.f21397.post(new Runnable() {
            public void run() {
                try {
                    ScribeRequestManager.this.f21398.backoff(volleyError);
                    ScribeRequestManager.this.m27260();
                } catch (VolleyError e) {
                    ScribeRequestManager.this.m27261();
                }
            }
        });
    }

    public void onResponse() {
        this.f21397.post(new Runnable() {
            public void run() {
                ScribeRequestManager.this.m27261();
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Request<?> m27264() {
        return ((ScribeRequest.ScribeRequestFactory) this.f21396).createRequest(this);
    }
}
