package com.mopub.network;

import android.os.Handler;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.volley.Cache;
import com.mopub.volley.Network;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MoPubRequestQueue extends RequestQueue {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Map<Request<?>, DelayedRequestHelper> f21376 = new HashMap(10);

    class DelayedRequestHelper {

        /* renamed from: 靐  reason: contains not printable characters */
        final Handler f21381;

        /* renamed from: 齉  reason: contains not printable characters */
        final Runnable f21383;

        /* renamed from: 龘  reason: contains not printable characters */
        final int f21384;

        DelayedRequestHelper(MoPubRequestQueue moPubRequestQueue, Request<?> request, int i) {
            this(request, i, new Handler());
        }

        @VisibleForTesting
        DelayedRequestHelper(final Request<?> request, int i, Handler handler) {
            this.f21384 = i;
            this.f21381 = handler;
            this.f21383 = new Runnable(MoPubRequestQueue.this) {
                public void run() {
                    MoPubRequestQueue.this.f21376.remove(request);
                    MoPubRequestQueue.this.add(request);
                }
            };
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m27257() {
            this.f21381.removeCallbacks(this.f21383);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m27258() {
            this.f21381.postDelayed(this.f21383, (long) this.f21384);
        }
    }

    MoPubRequestQueue(Cache cache, Network network) {
        super(cache, network);
    }

    public void addDelayedRequest(Request<?> request, int i) {
        Preconditions.checkNotNull(request);
        m27256(request, new DelayedRequestHelper(this, request, i));
    }

    public void cancel(final Request<?> request) {
        Preconditions.checkNotNull(request);
        cancelAll((RequestQueue.RequestFilter) new RequestQueue.RequestFilter() {
            public boolean apply(Request<?> request) {
                return request == request;
            }
        });
    }

    public void cancelAll(RequestQueue.RequestFilter requestFilter) {
        Preconditions.checkNotNull(requestFilter);
        super.cancelAll(requestFilter);
        Iterator<Map.Entry<Request<?>, DelayedRequestHelper>> it2 = this.f21376.entrySet().iterator();
        while (it2.hasNext()) {
            Map.Entry next = it2.next();
            if (requestFilter.apply((Request) next.getKey())) {
                ((Request) next.getKey()).cancel();
                ((DelayedRequestHelper) next.getValue()).m27257();
                it2.remove();
            }
        }
    }

    public void cancelAll(final Object obj) {
        Preconditions.checkNotNull(obj);
        super.cancelAll(obj);
        cancelAll((RequestQueue.RequestFilter) new RequestQueue.RequestFilter() {
            public boolean apply(Request<?> request) {
                return request.getTag() == obj;
            }
        });
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27256(Request<?> request, DelayedRequestHelper delayedRequestHelper) {
        Preconditions.checkNotNull(delayedRequestHelper);
        if (this.f21376.containsKey(request)) {
            cancel(request);
        }
        delayedRequestHelper.m27258();
        this.f21376.put(request, delayedRequestHelper);
    }
}
