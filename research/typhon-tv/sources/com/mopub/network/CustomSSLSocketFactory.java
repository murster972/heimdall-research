package com.mopub.network;

import android.net.SSLCertificateSocketFactory;
import android.net.SSLSessionCache;
import android.os.Build;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Reflection;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class CustomSSLSocketFactory extends SSLSocketFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    private SSLSocketFactory f21373;

    private CustomSSLSocketFactory() {
    }

    public static CustomSSLSocketFactory getDefault(int i) {
        CustomSSLSocketFactory customSSLSocketFactory = new CustomSSLSocketFactory();
        customSSLSocketFactory.f21373 = SSLCertificateSocketFactory.getDefault(i, (SSLSessionCache) null);
        return customSSLSocketFactory;
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static void m27248(SSLCertificateSocketFactory sSLCertificateSocketFactory, SSLSocket sSLSocket, String str) {
        Preconditions.checkNotNull(sSLCertificateSocketFactory);
        Preconditions.checkNotNull(sSLSocket);
        if (Build.VERSION.SDK_INT >= 17) {
            sSLCertificateSocketFactory.setHostname(sSLSocket, str);
            return;
        }
        try {
            new Reflection.MethodBuilder(sSLSocket, "setHostname").addParam(String.class, str).execute();
        } catch (Exception e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27249(Socket socket) {
        if (socket instanceof SSLSocket) {
            SSLSocket sSLSocket = (SSLSocket) socket;
            sSLSocket.setEnabledProtocols(sSLSocket.getSupportedProtocols());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27250(Socket socket, String str) throws IOException {
        Preconditions.checkNotNull(socket);
        if (this.f21373 == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        } else if (socket instanceof SSLSocket) {
            SSLSocket sSLSocket = (SSLSocket) socket;
            m27248((SSLCertificateSocketFactory) this.f21373, sSLSocket, str);
            m27251(sSLSocket, str);
        }
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static void m27251(SSLSocket sSLSocket, String str) throws IOException {
        Preconditions.checkNotNull(sSLSocket);
        sSLSocket.startHandshake();
        if (!HttpsURLConnection.getDefaultHostnameVerifier().verify(str, sSLSocket.getSession())) {
            throw new SSLHandshakeException("Server Name Identification failed.");
        }
    }

    public Socket createSocket() throws IOException {
        if (this.f21373 == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        }
        Socket createSocket = this.f21373.createSocket();
        m27249(createSocket);
        return createSocket;
    }

    public Socket createSocket(String str, int i) throws IOException, UnknownHostException {
        if (this.f21373 == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        }
        Socket createSocket = this.f21373.createSocket(str, i);
        m27249(createSocket);
        return createSocket;
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException, UnknownHostException {
        if (this.f21373 == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        }
        Socket createSocket = this.f21373.createSocket(str, i, inetAddress, i2);
        m27249(createSocket);
        return createSocket;
    }

    public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        if (this.f21373 == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        }
        Socket createSocket = this.f21373.createSocket(inetAddress, i);
        m27249(createSocket);
        return createSocket;
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        if (this.f21373 == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        }
        Socket createSocket = this.f21373.createSocket(inetAddress, i, inetAddress2, i2);
        m27249(createSocket);
        return createSocket;
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        if (this.f21373 == null) {
            throw new SocketException("SSLSocketFactory was null. Unable to create socket.");
        } else if (Build.VERSION.SDK_INT < 23) {
            if (z && socket != null) {
                socket.close();
            }
            Socket createSocket = this.f21373.createSocket(InetAddressUtils.getInetAddressByName(str), i);
            m27249(createSocket);
            m27250(createSocket, str);
            Socket socket2 = createSocket;
            return createSocket;
        } else {
            Socket createSocket2 = this.f21373.createSocket(socket, str, i, z);
            m27249(createSocket2);
            Socket socket3 = createSocket2;
            return createSocket2;
        }
    }

    public String[] getDefaultCipherSuites() {
        return this.f21373 == null ? new String[0] : this.f21373.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.f21373 == null ? new String[0] : this.f21373.getSupportedCipherSuites();
    }
}
