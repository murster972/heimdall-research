package com.mopub.network;

import android.content.Context;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.event.BaseEvent;
import com.mopub.mobileads.VastErrorCode;
import com.mopub.mobileads.VastMacroHelper;
import com.mopub.mobileads.VastTracker;
import com.mopub.network.MoPubNetworkError;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.HttpHeaderParser;
import java.util.ArrayList;
import java.util.List;

public class TrackingRequest extends Request<Void> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Listener f21404;

    public interface Listener extends Response.ErrorListener {
        void onResponse(String str);
    }

    private TrackingRequest(String str, Listener listener) {
        super(0, str, listener);
        this.f21404 = listener;
        setShouldCache(false);
        setRetryPolicy(new DefaultRetryPolicy(2500, 0, 1.0f));
    }

    public static void makeTrackingHttpRequest(Iterable<String> iterable, Context context) {
        makeTrackingHttpRequest(iterable, context, (Listener) null, (BaseEvent.Name) null);
    }

    public static void makeTrackingHttpRequest(Iterable<String> iterable, Context context, BaseEvent.Name name) {
        makeTrackingHttpRequest(iterable, context, (Listener) null, name);
    }

    public static void makeTrackingHttpRequest(Iterable<String> iterable, Context context, final Listener listener, BaseEvent.Name name) {
        if (iterable != null && context != null) {
            MoPubRequestQueue requestQueue = Networking.getRequestQueue(context);
            for (String next : iterable) {
                if (!TextUtils.isEmpty(next)) {
                    requestQueue.add(new TrackingRequest(next, new Listener() {
                        public void onErrorResponse(VolleyError volleyError) {
                            if (listener != null) {
                                listener.onErrorResponse(volleyError);
                            }
                        }

                        public void onResponse(String str) {
                            if (listener != null) {
                                listener.onResponse(str);
                            }
                        }
                    }));
                }
            }
        }
    }

    public static void makeTrackingHttpRequest(String str, Context context) {
        makeTrackingHttpRequest(str, context, (Listener) null, (BaseEvent.Name) null);
    }

    public static void makeTrackingHttpRequest(String str, Context context, BaseEvent.Name name) {
        makeTrackingHttpRequest(str, context, (Listener) null, name);
    }

    public static void makeTrackingHttpRequest(String str, Context context, Listener listener) {
        makeTrackingHttpRequest(str, context, listener, (BaseEvent.Name) null);
    }

    public static void makeTrackingHttpRequest(String str, Context context, Listener listener, BaseEvent.Name name) {
        if (str != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(str);
            makeTrackingHttpRequest((Iterable<String>) arrayList, context, listener, name);
        }
    }

    public static void makeVastTrackingHttpRequest(List<VastTracker> list, VastErrorCode vastErrorCode, Integer num, String str, Context context) {
        Preconditions.checkNotNull(list);
        ArrayList arrayList = new ArrayList(list.size());
        for (VastTracker next : list) {
            if (next != null && (!next.isTracked() || next.isRepeatable())) {
                arrayList.add(next.getTrackingUrl());
                next.setTracked();
            }
        }
        makeTrackingHttpRequest((Iterable<String>) new VastMacroHelper(arrayList).withErrorCode(vastErrorCode).withContentPlayHead(num).withAssetUri(str).getUris(), context);
    }

    public void deliverResponse(Void voidR) {
        if (this.f21404 != null) {
            this.f21404.onResponse(getUrl());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Response<Void> m27265(NetworkResponse networkResponse) {
        return networkResponse.statusCode != 200 ? Response.error(new MoPubNetworkError("Failed to log tracking request. Response code: " + networkResponse.statusCode + " for url: " + getUrl(), MoPubNetworkError.Reason.TRACKING_FAILURE)) : Response.success(null, HttpHeaderParser.parseCacheHeaders(networkResponse));
    }
}
