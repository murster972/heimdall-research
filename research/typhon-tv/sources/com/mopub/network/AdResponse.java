package com.mopub.network;

import com.mopub.common.MoPub;
import com.mopub.common.event.EventDetails;
import com.mopub.common.util.DateAndTime;
import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONObject;

public class AdResponse implements Serializable {
    private static final long serialVersionUID = 1;
    private final Integer mAdTimeoutDelayMillis;
    private final String mAdType;
    private final String mAdUnitId;
    private final MoPub.BrowserAgent mBrowserAgent;
    private final String mClickTrackingUrl;
    private final String mCustomEventClassName;
    private final String mDspCreativeId;
    private final EventDetails mEventDetails;
    private final String mFailoverUrl;
    private final String mFullAdType;
    private final Integer mHeight;
    private final String mImpressionTrackingUrl;
    private final JSONObject mJsonBody;
    private final String mNetworkType;
    private final String mRedirectUrl;
    private final Integer mRefreshTimeMillis;
    private final String mRequestId;
    private final String mResponseBody;
    private final String mRewardedCurrencies;
    private final Integer mRewardedDuration;
    private final String mRewardedVideoCompletionUrl;
    private final String mRewardedVideoCurrencyAmount;
    private final String mRewardedVideoCurrencyName;
    private final boolean mScrollable;
    private final Map<String, String> mServerExtras;
    private final boolean mShouldRewardOnClick;
    private final long mTimestamp;
    private final Integer mWidth;

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public String f21341;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public String f21342;
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public String f21343;
        /* access modifiers changed from: private */

        /* renamed from: ʾ  reason: contains not printable characters */
        public String f21344;
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public String f21345;
        /* access modifiers changed from: private */

        /* renamed from: ˆ  reason: contains not printable characters */
        public String f21346;
        /* access modifiers changed from: private */

        /* renamed from: ˈ  reason: contains not printable characters */
        public String f21347;
        /* access modifiers changed from: private */

        /* renamed from: ˉ  reason: contains not printable characters */
        public boolean f21348 = false;
        /* access modifiers changed from: private */

        /* renamed from: ˊ  reason: contains not printable characters */
        public Integer f21349;
        /* access modifiers changed from: private */

        /* renamed from: ˋ  reason: contains not printable characters */
        public Integer f21350;
        /* access modifiers changed from: private */

        /* renamed from: ˎ  reason: contains not printable characters */
        public Integer f21351;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public String f21352;
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public Integer f21353;
        /* access modifiers changed from: private */

        /* renamed from: י  reason: contains not printable characters */
        public JSONObject f21354;
        /* access modifiers changed from: private */

        /* renamed from: ـ  reason: contains not printable characters */
        public EventDetails f21355;
        /* access modifiers changed from: private */

        /* renamed from: ٴ  reason: contains not printable characters */
        public boolean f21356;
        /* access modifiers changed from: private */

        /* renamed from: ᐧ  reason: contains not printable characters */
        public String f21357;
        /* access modifiers changed from: private */

        /* renamed from: ᴵ  reason: contains not printable characters */
        public String f21358;
        /* access modifiers changed from: private */

        /* renamed from: ᵎ  reason: contains not printable characters */
        public MoPub.BrowserAgent f21359;
        /* access modifiers changed from: private */

        /* renamed from: ᵔ  reason: contains not printable characters */
        public Map<String, String> f21360 = new TreeMap();
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public String f21361;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public String f21362;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public String f21363;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public String f21364;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public String f21365;
        /* access modifiers changed from: private */

        /* renamed from: ﹶ  reason: contains not printable characters */
        public String f21366;
        /* access modifiers changed from: private */

        /* renamed from: ﾞ  reason: contains not printable characters */
        public Integer f21367;

        public AdResponse build() {
            return new AdResponse(this);
        }

        public Builder setAdTimeoutDelayMilliseconds(Integer num) {
            this.f21350 = num;
            return this;
        }

        public Builder setAdType(String str) {
            this.f21365 = str;
            return this;
        }

        public Builder setAdUnitId(String str) {
            this.f21362 = str;
            return this;
        }

        public Builder setBrowserAgent(MoPub.BrowserAgent browserAgent) {
            this.f21359 = browserAgent;
            return this;
        }

        public Builder setClickTrackingUrl(String str) {
            this.f21347 = str;
            return this;
        }

        public Builder setCustomEventClassName(String str) {
            this.f21358 = str;
            return this;
        }

        public Builder setDimensions(Integer num, Integer num2) {
            this.f21367 = num;
            this.f21349 = num2;
            return this;
        }

        public Builder setDspCreativeId(String str) {
            this.f21346 = str;
            return this;
        }

        public Builder setEventDetails(EventDetails eventDetails) {
            this.f21355 = eventDetails;
            return this;
        }

        public Builder setFailoverUrl(String str) {
            this.f21345 = str;
            return this;
        }

        public Builder setFullAdType(String str) {
            this.f21364 = str;
            return this;
        }

        public Builder setImpressionTrackingUrl(String str) {
            this.f21344 = str;
            return this;
        }

        public Builder setJsonBody(JSONObject jSONObject) {
            this.f21354 = jSONObject;
            return this;
        }

        public Builder setNetworkType(String str) {
            this.f21363 = str;
            return this;
        }

        public Builder setRedirectUrl(String str) {
            this.f21357 = str;
            return this;
        }

        public Builder setRefreshTimeMilliseconds(Integer num) {
            this.f21351 = num;
            return this;
        }

        public Builder setRequestId(String str) {
            this.f21366 = str;
            return this;
        }

        public Builder setResponseBody(String str) {
            this.f21352 = str;
            return this;
        }

        public Builder setRewardedCurrencies(String str) {
            this.f21342 = str;
            return this;
        }

        public Builder setRewardedDuration(Integer num) {
            this.f21353 = num;
            return this;
        }

        public Builder setRewardedVideoCompletionUrl(String str) {
            this.f21343 = str;
            return this;
        }

        public Builder setRewardedVideoCurrencyAmount(String str) {
            this.f21341 = str;
            return this;
        }

        public Builder setRewardedVideoCurrencyName(String str) {
            this.f21361 = str;
            return this;
        }

        public Builder setScrollable(Boolean bool) {
            this.f21348 = bool == null ? this.f21348 : bool.booleanValue();
            return this;
        }

        public Builder setServerExtras(Map<String, String> map) {
            if (map == null) {
                this.f21360 = new TreeMap();
            } else {
                this.f21360 = new TreeMap(map);
            }
            return this;
        }

        public Builder setShouldRewardOnClick(boolean z) {
            this.f21356 = z;
            return this;
        }
    }

    private AdResponse(Builder builder) {
        this.mAdType = builder.f21365;
        this.mAdUnitId = builder.f21362;
        this.mFullAdType = builder.f21364;
        this.mNetworkType = builder.f21363;
        this.mRewardedVideoCurrencyName = builder.f21361;
        this.mRewardedVideoCurrencyAmount = builder.f21341;
        this.mRewardedCurrencies = builder.f21342;
        this.mRewardedVideoCompletionUrl = builder.f21343;
        this.mRewardedDuration = builder.f21353;
        this.mShouldRewardOnClick = builder.f21356;
        this.mRedirectUrl = builder.f21357;
        this.mClickTrackingUrl = builder.f21347;
        this.mImpressionTrackingUrl = builder.f21344;
        this.mFailoverUrl = builder.f21345;
        this.mRequestId = builder.f21366;
        this.mWidth = builder.f21367;
        this.mHeight = builder.f21349;
        this.mAdTimeoutDelayMillis = builder.f21350;
        this.mRefreshTimeMillis = builder.f21351;
        this.mDspCreativeId = builder.f21346;
        this.mScrollable = builder.f21348;
        this.mResponseBody = builder.f21352;
        this.mJsonBody = builder.f21354;
        this.mEventDetails = builder.f21355;
        this.mCustomEventClassName = builder.f21358;
        this.mBrowserAgent = builder.f21359;
        this.mServerExtras = builder.f21360;
        this.mTimestamp = DateAndTime.now().getTime();
    }

    public Integer getAdTimeoutMillis() {
        return this.mAdTimeoutDelayMillis;
    }

    public String getAdType() {
        return this.mAdType;
    }

    public String getAdUnitId() {
        return this.mAdUnitId;
    }

    public MoPub.BrowserAgent getBrowserAgent() {
        return this.mBrowserAgent;
    }

    public String getClickTrackingUrl() {
        return this.mClickTrackingUrl;
    }

    public String getCustomEventClassName() {
        return this.mCustomEventClassName;
    }

    public String getDspCreativeId() {
        return this.mDspCreativeId;
    }

    public EventDetails getEventDetails() {
        return this.mEventDetails;
    }

    public String getFailoverUrl() {
        return this.mFailoverUrl;
    }

    public String getFullAdType() {
        return this.mFullAdType;
    }

    public Integer getHeight() {
        return this.mHeight;
    }

    public String getImpressionTrackingUrl() {
        return this.mImpressionTrackingUrl;
    }

    public JSONObject getJsonBody() {
        return this.mJsonBody;
    }

    public String getNetworkType() {
        return this.mNetworkType;
    }

    public String getRedirectUrl() {
        return this.mRedirectUrl;
    }

    public Integer getRefreshTimeMillis() {
        return this.mRefreshTimeMillis;
    }

    public String getRequestId() {
        return this.mRequestId;
    }

    public String getRewardedCurrencies() {
        return this.mRewardedCurrencies;
    }

    public Integer getRewardedDuration() {
        return this.mRewardedDuration;
    }

    public String getRewardedVideoCompletionUrl() {
        return this.mRewardedVideoCompletionUrl;
    }

    public String getRewardedVideoCurrencyAmount() {
        return this.mRewardedVideoCurrencyAmount;
    }

    public String getRewardedVideoCurrencyName() {
        return this.mRewardedVideoCurrencyName;
    }

    public Map<String, String> getServerExtras() {
        return new TreeMap(this.mServerExtras);
    }

    public String getStringBody() {
        return this.mResponseBody;
    }

    public long getTimestamp() {
        return this.mTimestamp;
    }

    public Integer getWidth() {
        return this.mWidth;
    }

    public boolean hasJson() {
        return this.mJsonBody != null;
    }

    public boolean isScrollable() {
        return this.mScrollable;
    }

    public boolean shouldRewardOnClick() {
        return this.mShouldRewardOnClick;
    }

    public Builder toBuilder() {
        return new Builder().setAdType(this.mAdType).setNetworkType(this.mNetworkType).setRewardedVideoCurrencyName(this.mRewardedVideoCurrencyName).setRewardedVideoCurrencyAmount(this.mRewardedVideoCurrencyAmount).setRewardedCurrencies(this.mRewardedCurrencies).setRewardedVideoCompletionUrl(this.mRewardedVideoCompletionUrl).setRewardedDuration(this.mRewardedDuration).setShouldRewardOnClick(this.mShouldRewardOnClick).setRedirectUrl(this.mRedirectUrl).setClickTrackingUrl(this.mClickTrackingUrl).setImpressionTrackingUrl(this.mImpressionTrackingUrl).setFailoverUrl(this.mFailoverUrl).setDimensions(this.mWidth, this.mHeight).setAdTimeoutDelayMilliseconds(this.mAdTimeoutDelayMillis).setRefreshTimeMilliseconds(this.mRefreshTimeMillis).setDspCreativeId(this.mDspCreativeId).setScrollable(Boolean.valueOf(this.mScrollable)).setResponseBody(this.mResponseBody).setJsonBody(this.mJsonBody).setEventDetails(this.mEventDetails).setCustomEventClassName(this.mCustomEventClassName).setBrowserAgent(this.mBrowserAgent).setServerExtras(this.mServerExtras);
    }
}
