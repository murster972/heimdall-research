package com.mopub.network;

import com.mopub.common.VisibleForTesting;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.NoConnectionError;
import com.mopub.volley.VolleyError;

public class ScribeBackoffPolicy extends BackoffPolicy {
    public ScribeBackoffPolicy() {
        this(60000, 5, 2);
    }

    @VisibleForTesting
    ScribeBackoffPolicy(int i, int i2, int i3) {
        this.f21371 = i;
        this.f21368 = i2;
        this.f21369 = i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27263() {
        this.f21372 = (int) (((double) this.f21371) * Math.pow((double) this.f21369, (double) this.f21370));
        this.f21370++;
    }

    public void backoff(VolleyError volleyError) throws VolleyError {
        if (!hasAttemptRemaining()) {
            throw volleyError;
        } else if (volleyError instanceof NoConnectionError) {
            m27263();
        } else {
            NetworkResponse networkResponse = volleyError.networkResponse;
            if (networkResponse == null || !(networkResponse.statusCode == 503 || networkResponse.statusCode == 504)) {
                throw volleyError;
            }
            m27263();
        }
    }
}
