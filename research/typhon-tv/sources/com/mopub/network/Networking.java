package com.mopub.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import android.support.v4.util.LruCache;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;
import com.mopub.common.TyphoonApp;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.DeviceUtils;
import com.mopub.volley.toolbox.BasicNetwork;
import com.mopub.volley.toolbox.DiskBasedCache;
import com.mopub.volley.toolbox.ImageLoader;
import java.io.File;

public class Networking {

    /* renamed from: 连任  reason: contains not printable characters */
    private static boolean f21388 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile MoPubRequestQueue f21389;

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile MaxWidthImageLoader f21390;

    /* renamed from: 齉  reason: contains not printable characters */
    private static volatile String f21391;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f21392 = System.getProperty("http.agent");

    @VisibleForTesting
    public static synchronized void clearForTesting() {
        synchronized (Networking.class) {
            f21389 = null;
            f21390 = null;
            f21391 = null;
        }
    }

    public static String getBaseUrlScheme() {
        return TyphoonApp.HTTP;
    }

    public static String getCachedUserAgent() {
        String str = f21391;
        return str == null ? f21392 : str;
    }

    public static ImageLoader getImageLoader(Context context) {
        MaxWidthImageLoader maxWidthImageLoader = f21390;
        if (maxWidthImageLoader == null) {
            synchronized (Networking.class) {
                try {
                    maxWidthImageLoader = f21390;
                    if (maxWidthImageLoader == null) {
                        MoPubRequestQueue requestQueue = getRequestQueue(context);
                        final AnonymousClass1 r1 = new LruCache<String, Bitmap>(DeviceUtils.memoryCacheSizeBytes(context)) {
                            /* access modifiers changed from: protected */
                            /* renamed from: 龘  reason: contains not printable characters */
                            public int sizeOf(String str, Bitmap bitmap) {
                                return bitmap != null ? bitmap.getRowBytes() * bitmap.getHeight() : super.sizeOf(str, bitmap);
                            }
                        };
                        MaxWidthImageLoader maxWidthImageLoader2 = new MaxWidthImageLoader(requestQueue, context, new ImageLoader.ImageCache() {
                            public Bitmap getBitmap(String str) {
                                return (Bitmap) r1.get(str);
                            }

                            public void putBitmap(String str, Bitmap bitmap) {
                                r1.put(str, bitmap);
                            }
                        });
                        try {
                            f21390 = maxWidthImageLoader2;
                            maxWidthImageLoader = maxWidthImageLoader2;
                        } catch (Throwable th) {
                            th = th;
                            MaxWidthImageLoader maxWidthImageLoader3 = maxWidthImageLoader2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return maxWidthImageLoader;
    }

    public static MoPubRequestQueue getRequestQueue() {
        return f21389;
    }

    public static MoPubRequestQueue getRequestQueue(Context context) {
        MoPubRequestQueue moPubRequestQueue = f21389;
        if (moPubRequestQueue == null) {
            synchronized (Networking.class) {
                try {
                    moPubRequestQueue = f21389;
                    if (moPubRequestQueue == null) {
                        BasicNetwork basicNetwork = new BasicNetwork(new RequestQueueHttpStack(getUserAgent(context.getApplicationContext()), new PlayServicesUrlRewriter(ClientMetadata.getInstance(context).getDeviceId(), context), CustomSSLSocketFactory.getDefault(10000)));
                        File file = new File(context.getCacheDir().getPath() + File.separator + "mopub-volley-cache");
                        MoPubRequestQueue moPubRequestQueue2 = new MoPubRequestQueue(new DiskBasedCache(file, (int) DeviceUtils.diskCacheSizeBytes(file, 10485760)), basicNetwork);
                        try {
                            f21389 = moPubRequestQueue2;
                            moPubRequestQueue2.start();
                            moPubRequestQueue = moPubRequestQueue2;
                        } catch (Throwable th) {
                            th = th;
                            MoPubRequestQueue moPubRequestQueue3 = moPubRequestQueue2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return moPubRequestQueue;
    }

    public static String getScheme() {
        return useHttps() ? TyphoonApp.HTTPS : TyphoonApp.HTTP;
    }

    public static String getUserAgent(Context context) {
        Preconditions.checkNotNull(context);
        String str = f21391;
        if (str == null) {
            synchronized (Networking.class) {
                str = f21391;
                if (str == null) {
                    if (Build.VERSION.SDK_INT >= 17) {
                        try {
                            str = WebSettings.getDefaultUserAgent(context);
                        } catch (Exception e) {
                            str = f21392;
                        }
                    } else if (Looper.myLooper() == Looper.getMainLooper()) {
                        try {
                            str = new WebView(context).getSettings().getUserAgentString();
                        } catch (Exception e2) {
                            str = f21392;
                        }
                    } else {
                        str = f21392;
                    }
                    f21391 = str;
                }
            }
        }
        return str;
    }

    @VisibleForTesting
    public static synchronized void setImageLoaderForTesting(MaxWidthImageLoader maxWidthImageLoader) {
        synchronized (Networking.class) {
            f21390 = maxWidthImageLoader;
        }
    }

    @VisibleForTesting
    public static synchronized void setRequestQueueForTesting(MoPubRequestQueue moPubRequestQueue) {
        synchronized (Networking.class) {
            f21389 = moPubRequestQueue;
        }
    }

    @VisibleForTesting
    public static synchronized void setUserAgentForTesting(String str) {
        synchronized (Networking.class) {
            f21391 = str;
        }
    }

    public static void useHttps(boolean z) {
        f21388 = z;
    }

    public static boolean useHttps() {
        return f21388;
    }
}
