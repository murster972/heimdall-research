package com.mopub.network;

import com.mopub.common.util.ResponseHeader;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

public class HeaderUtils {
    public static boolean extractBooleanHeader(Map<String, String> map, ResponseHeader responseHeader, boolean z) {
        return m27254(extractHeader(map, responseHeader), z);
    }

    public static boolean extractBooleanHeader(HttpResponse httpResponse, ResponseHeader responseHeader, boolean z) {
        return m27254(extractHeader(httpResponse, responseHeader), z);
    }

    public static String extractHeader(Map<String, String> map, ResponseHeader responseHeader) {
        return map.get(responseHeader.getKey());
    }

    public static String extractHeader(HttpResponse httpResponse, ResponseHeader responseHeader) {
        Header firstHeader = httpResponse.getFirstHeader(responseHeader.getKey());
        if (firstHeader != null) {
            return firstHeader.getValue();
        }
        return null;
    }

    public static int extractIntHeader(HttpResponse httpResponse, ResponseHeader responseHeader, int i) {
        Integer extractIntegerHeader = extractIntegerHeader(httpResponse, responseHeader);
        return extractIntegerHeader == null ? i : extractIntegerHeader.intValue();
    }

    public static Integer extractIntegerHeader(Map<String, String> map, ResponseHeader responseHeader) {
        return m27253(extractHeader(map, responseHeader));
    }

    public static Integer extractIntegerHeader(HttpResponse httpResponse, ResponseHeader responseHeader) {
        return m27253(extractHeader(httpResponse, responseHeader));
    }

    public static Integer extractPercentHeader(Map<String, String> map, ResponseHeader responseHeader) {
        return m27252(extractHeader(map, responseHeader));
    }

    public static String extractPercentHeaderString(Map<String, String> map, ResponseHeader responseHeader) {
        Integer extractPercentHeader = extractPercentHeader(map, responseHeader);
        if (extractPercentHeader != null) {
            return extractPercentHeader.toString();
        }
        return null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Integer m27252(String str) {
        if (str == null) {
            return null;
        }
        Integer r0 = m27253(str.replace("%", ""));
        if (r0 == null || r0.intValue() < 0 || r0.intValue() > 100) {
            return null;
        }
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Integer m27253(String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (Exception e) {
            NumberFormat instance = NumberFormat.getInstance(Locale.US);
            instance.setParseIntegerOnly(true);
            try {
                return Integer.valueOf(instance.parse(str.trim()).intValue());
            } catch (Exception e2) {
                return null;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m27254(String str, boolean z) {
        return str == null ? z : str.equals(PubnativeRequest.LEGACY_ZONE_ID);
    }
}
