package com.mopub.network;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.GpsHelper;
import com.mopub.volley.toolbox.HurlStack;
import net.pubnative.library.request.PubnativeRequest;

public class PlayServicesUrlRewriter implements HurlStack.UrlRewriter {
    public static final String DO_NOT_TRACK_TEMPLATE = "mp_tmpl_do_not_track";
    public static final String UDID_TEMPLATE = "mp_tmpl_advertising_id";

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f21394;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f21395;

    public PlayServicesUrlRewriter(String str, Context context) {
        this.f21395 = str;
        this.f21394 = context.getApplicationContext();
    }

    public String rewriteUrl(String str) {
        GpsHelper.AdvertisingInfo fetchAdvertisingInfoSync;
        if (!str.contains(UDID_TEMPLATE) && !str.contains(DO_NOT_TRACK_TEMPLATE)) {
            return str;
        }
        String str2 = "";
        GpsHelper.AdvertisingInfo advertisingInfo = new GpsHelper.AdvertisingInfo(this.f21395, false);
        if (GpsHelper.isPlayServicesAvailable(this.f21394) && (fetchAdvertisingInfoSync = GpsHelper.fetchAdvertisingInfoSync(this.f21394)) != null) {
            str2 = "ifa:";
            advertisingInfo = fetchAdvertisingInfoSync;
        }
        return str.replace(UDID_TEMPLATE, Uri.encode(str2 + advertisingInfo.advertisingId)).replace(DO_NOT_TRACK_TEMPLATE, advertisingInfo.limitAdTracking ? PubnativeRequest.LEGACY_ZONE_ID : "0");
    }
}
