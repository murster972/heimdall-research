package com.mopub.network;

import android.os.Handler;
import android.os.Looper;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.network.RequestManager.RequestFactory;
import com.mopub.volley.Request;

public abstract class RequestManager<T extends RequestFactory> {

    /* renamed from: 靐  reason: contains not printable characters */
    protected T f21396;

    /* renamed from: 麤  reason: contains not printable characters */
    protected Handler f21397;

    /* renamed from: 齉  reason: contains not printable characters */
    protected BackoffPolicy f21398;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Request<?> f21399;

    public interface RequestFactory {
    }

    public RequestManager(Looper looper) {
        this.f21397 = new Handler(looper);
    }

    public void cancelRequest() {
        MoPubRequestQueue requestQueue = Networking.getRequestQueue();
        if (!(requestQueue == null || this.f21399 == null)) {
            requestQueue.cancel(this.f21399);
        }
        m27261();
    }

    public boolean isAtCapacity() {
        return this.f21399 != null;
    }

    public void makeRequest(T t, BackoffPolicy backoffPolicy) {
        Preconditions.checkNotNull(t);
        Preconditions.checkNotNull(backoffPolicy);
        cancelRequest();
        this.f21396 = t;
        this.f21398 = backoffPolicy;
        m27260();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27260() {
        this.f21399 = m27262();
        MoPubRequestQueue requestQueue = Networking.getRequestQueue();
        if (requestQueue == null) {
            m27261();
        } else if (this.f21398.getRetryCount() == 0) {
            requestQueue.add(this.f21399);
        } else {
            requestQueue.addDelayedRequest(this.f21399, this.f21398.getBackoffMs());
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 齉  reason: contains not printable characters */
    public void m27261() {
        this.f21399 = null;
        this.f21396 = null;
        this.f21398 = null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract Request<?> m27262();
}
