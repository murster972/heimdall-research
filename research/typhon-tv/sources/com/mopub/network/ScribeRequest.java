package com.mopub.network;

import com.mopub.common.VisibleForTesting;
import com.mopub.common.event.BaseEvent;
import com.mopub.common.event.EventSerializer;
import com.mopub.network.RequestManager;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Response;
import com.mopub.volley.toolbox.HttpHeaderParser;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

public class ScribeRequest extends Request<Void> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final EventSerializer f5796;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Listener f5797;

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<BaseEvent> f5798;

    public interface Listener extends Response.ErrorListener {
        void onResponse();
    }

    public interface ScribeRequestFactory extends RequestManager.RequestFactory {
        ScribeRequest createRequest(Listener listener);
    }

    public ScribeRequest(String str, List<BaseEvent> list, EventSerializer eventSerializer, Listener listener) {
        super(1, str, listener);
        this.f5798 = list;
        this.f5796 = eventSerializer;
        this.f5797 = listener;
        setShouldCache(false);
        setRetryPolicy(new DefaultRetryPolicy());
    }

    @Deprecated
    @VisibleForTesting
    public List<BaseEvent> getEvents() {
        return this.f5798;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Response<Void> m6252(NetworkResponse networkResponse) {
        return Response.success(null, HttpHeaderParser.parseCacheHeaders(networkResponse));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Map<String, String> m6253() {
        JSONArray serializeAsJson = this.f5796.serializeAsJson(this.f5798);
        HashMap hashMap = new HashMap();
        hashMap.put("log", serializeAsJson.toString());
        return hashMap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void deliverResponse(Void voidR) {
        this.f5797.onResponse();
    }
}
