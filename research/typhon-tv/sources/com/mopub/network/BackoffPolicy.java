package com.mopub.network;

import com.mopub.volley.VolleyError;

public abstract class BackoffPolicy {

    /* renamed from: 连任  reason: contains not printable characters */
    protected int f21368;

    /* renamed from: 靐  reason: contains not printable characters */
    protected int f21369;

    /* renamed from: 麤  reason: contains not printable characters */
    protected int f21370;

    /* renamed from: 齉  reason: contains not printable characters */
    protected int f21371;

    /* renamed from: 龘  reason: contains not printable characters */
    protected int f21372;

    public abstract void backoff(VolleyError volleyError) throws VolleyError;

    public int getBackoffMs() {
        return this.f21372;
    }

    public int getRetryCount() {
        return this.f21370;
    }

    public boolean hasAttemptRemaining() {
        return this.f21370 < this.f21368;
    }
}
