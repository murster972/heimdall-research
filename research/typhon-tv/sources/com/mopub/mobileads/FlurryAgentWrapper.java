package com.mopub.mobileads;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.Pinkamena;
import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryAgentListener;
import com.flurry.android.FlurryConsent;
import java.util.Map;

public final class FlurryAgentWrapper {
    public static final String PARAM_AD_SPACE_NAME = "adSpaceName";
    public static final String PARAM_API_KEY = "apiKey";

    /* renamed from: 龘  reason: contains not printable characters */
    private FlurryAgent.Builder f20755;

    private static class FlurryAgentLoader {
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public static final FlurryAgentWrapper f20756 = new FlurryAgentWrapper();
    }

    private FlurryAgentWrapper() {
        this.f20755 = new FlurryAgent.Builder().withCaptureUncaughtExceptions(false).withPulseEnabled(false).withIncludeBackgroundSessionsInMetrics(false).withLogEnabled(false).withLogLevel(2);
        try {
            this.f20755 = this.f20755.withConsent(new FlurryConsent(false, (Map) null));
        } catch (Exception e) {
        }
        FlurryAgent.addOrigin("Flurry_Mopub_Android", "6.5.0");
    }

    public static FlurryAgentWrapper getInstance() {
        return FlurryAgentLoader.f20756;
    }

    public synchronized void endSession(Context context) {
        if (context != null) {
            if (FlurryAgent.isSessionActive() && Build.VERSION.SDK_INT < 14) {
                Pinkamena.DianePie();
            }
        }
    }

    public synchronized boolean isSessionActive() {
        return FlurryAgent.isSessionActive();
    }

    public synchronized void startSession(Context context, String str, FlurryAgentListener flurryAgentListener) {
        if (!TextUtils.isEmpty(str)) {
            if (this.f20755 != null && !FlurryAgent.isSessionActive()) {
                this.f20755.withListener(flurryAgentListener).build(context, str);
                if (Build.VERSION.SDK_INT < 14) {
                    Pinkamena.DianePie();
                }
            }
        }
    }
}
