package com.mopub.mobileads;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.mopub.common.util.ImageUtils;

public class VastVideoBlurLastVideoFrameTask extends AsyncTask<String, Void, Boolean> {

    /* renamed from: 连任  reason: contains not printable characters */
    private Bitmap f20874;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ImageView f20875;

    /* renamed from: 麤  reason: contains not printable characters */
    private Bitmap f20876;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f20877;

    /* renamed from: 龘  reason: contains not printable characters */
    private final MediaMetadataRetriever f20878;

    public VastVideoBlurLastVideoFrameTask(MediaMetadataRetriever mediaMetadataRetriever, ImageView imageView, int i) {
        this.f20878 = mediaMetadataRetriever;
        this.f20875 = imageView;
        this.f20877 = i;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Boolean doInBackground(String... strArr) {
        if (strArr == null || strArr.length == 0 || strArr[0] == null) {
            return false;
        }
        try {
            this.f20878.setDataSource(strArr[0]);
            this.f20876 = this.f20878.getFrameAtTime((long) ((this.f20877 * 1000) - 200000), 3);
            if (this.f20876 == null) {
                return false;
            }
            this.f20874 = ImageUtils.applyFastGaussianBlurToBitmap(this.f20876, 4);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void onPostExecute(Boolean bool) {
        if (isCancelled()) {
            onCancelled();
        } else if (bool != null && bool.booleanValue()) {
            this.f20875.setImageBitmap(this.f20874);
            ImageUtils.setImageViewAlpha(this.f20875, 100);
        }
    }
}
