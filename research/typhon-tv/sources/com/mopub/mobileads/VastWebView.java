package com.mopub.mobileads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.Pinkamena;
import com.mopub.common.Preconditions;
import com.mopub.common.TyphoonApp;
import com.mopub.common.util.Utils;
import com.mopub.common.util.VersionCode;
import com.mopub.network.Networking;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

class VastWebView extends BaseWebView {

    /* renamed from: 靐  reason: contains not printable characters */
    VastWebViewClickListener f20950;

    interface VastWebViewClickListener {
        void onVastWebViewClick();
    }

    class VastWebViewOnTouchListener implements View.OnTouchListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean f20951;

        VastWebViewOnTouchListener() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.f20951 = true;
                    break;
                case 1:
                    if (this.f20951) {
                        this.f20951 = false;
                        if (VastWebView.this.f20950 != null) {
                            VastWebView.this.f20950.onVastWebViewClick();
                            break;
                        }
                    }
                    break;
            }
            return false;
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    VastWebView(Context context) {
        super(context);
        m26864();
        getSettings().setJavaScriptEnabled(true);
        if (VersionCode.currentApiLevel().isAtLeast(VersionCode.ICE_CREAM_SANDWICH)) {
            m6210(true);
        }
        setBackgroundColor(0);
        setOnTouchListener(new VastWebViewOnTouchListener());
        setId((int) Utils.generateUniqueId());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26864() {
        setHorizontalScrollBarEnabled(false);
        setHorizontalScrollbarOverlay(false);
        setVerticalScrollBarEnabled(false);
        setVerticalScrollbarOverlay(false);
        getSettings().setSupportZoom(false);
        setScrollBarStyle(0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static VastWebView m26865(Context context, VastResource vastResource) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(vastResource);
        VastWebView vastWebView = new VastWebView(context);
        vastResource.initializeWebView(vastWebView);
        return vastWebView;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26866(VastWebViewClickListener vastWebViewClickListener) {
        this.f20950 = vastWebViewClickListener;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26867(String str) {
        String str2 = Networking.getBaseUrlScheme() + "://" + TyphoonApp.HOST + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
        String str3 = str;
        Pinkamena.DianePie();
    }
}
