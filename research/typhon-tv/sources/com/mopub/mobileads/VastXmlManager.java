package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.util.DeviceUtils;
import com.mopub.mobileads.util.XmlUtils;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

class VastXmlManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private Document f20953;

    VastXmlManager() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public String m26869() {
        return XmlUtils.getFirstMatchingStringData(this.f20953, "MoPubCloseIcon");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public DeviceUtils.ForceOrientation m26870() {
        return DeviceUtils.ForceOrientation.getForceOrientation(XmlUtils.getFirstMatchingStringData(this.f20953, "MoPubForceOrientation"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public String m26871() {
        String firstMatchingStringData = XmlUtils.getFirstMatchingStringData(this.f20953, "MoPubSkipText");
        if (firstMatchingStringData == null || firstMatchingStringData.length() > 8) {
            return null;
        }
        return firstMatchingStringData;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public VastTracker m26872() {
        if (this.f20953 == null) {
            return null;
        }
        String firstMatchingStringData = XmlUtils.getFirstMatchingStringData(this.f20953, "Error");
        if (!TextUtils.isEmpty(firstMatchingStringData)) {
            return new VastTracker(firstMatchingStringData);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m26873() {
        String firstMatchingStringData = XmlUtils.getFirstMatchingStringData(this.f20953, "MoPubCtaText");
        if (firstMatchingStringData == null || firstMatchingStringData.length() > 15) {
            return null;
        }
        return firstMatchingStringData;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public List<VastTracker> m26874() {
        List<String> stringDataAsList = XmlUtils.getStringDataAsList(this.f20953, "MP_TRACKING_URL");
        ArrayList arrayList = new ArrayList(stringDataAsList.size());
        for (String vastTracker : stringDataAsList) {
            arrayList.add(new VastTracker(vastTracker));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<VastAdXmlManager> m26875() {
        ArrayList arrayList = new ArrayList();
        if (this.f20953 != null) {
            NodeList elementsByTagName = this.f20953.getElementsByTagName("Ad");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                arrayList.add(new VastAdXmlManager(elementsByTagName.item(i)));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26876(String str) throws ParserConfigurationException, IOException, SAXException {
        Preconditions.checkNotNull(str, "xmlString cannot be null");
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        newInstance.setCoalescing(true);
        this.f20953 = newInstance.newDocumentBuilder().parse(new InputSource(new StringReader("<MPMoVideoXMLDocRoot>" + str.replaceFirst("<\\?.*\\?>", "") + "</MPMoVideoXMLDocRoot>")));
    }
}
