package com.mopub.mobileads;

import android.content.Context;
import android.os.Handler;
import com.Pinkamena;
import com.mopub.common.AdReport;
import com.mopub.common.DataKeys;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.factories.CustomEventInterstitialFactory;
import java.util.Map;
import java.util.TreeMap;

public class CustomEventInterstitialAdapter implements CustomEventInterstitial.CustomEventInterstitialListener {
    public static final int DEFAULT_INTERSTITIAL_TIMEOUT_DELAY = 30000;

    /* renamed from: ʻ  reason: contains not printable characters */
    private CustomEventInterstitial f20743;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Context f20744;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Map<String, Object> f20745;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Map<String, String> f20746;

    /* renamed from: 连任  reason: contains not printable characters */
    private CustomEventInterstitialAdapterListener f20747;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Handler f20748 = new Handler();

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f20749;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Runnable f20750;

    /* renamed from: 龘  reason: contains not printable characters */
    private final MoPubInterstitial f20751;

    interface CustomEventInterstitialAdapterListener {
        void onCustomEventInterstitialClicked();

        void onCustomEventInterstitialDismissed();

        void onCustomEventInterstitialFailed(MoPubErrorCode moPubErrorCode);

        void onCustomEventInterstitialLoaded();

        void onCustomEventInterstitialShown();
    }

    public CustomEventInterstitialAdapter(MoPubInterstitial moPubInterstitial, String str, Map<String, String> map, long j, AdReport adReport) {
        Preconditions.checkNotNull(map);
        this.f20751 = moPubInterstitial;
        this.f20744 = this.f20751.getActivity();
        this.f20750 = new Runnable() {
            public void run() {
                CustomEventInterstitialAdapter.this.onInterstitialFailed(MoPubErrorCode.NETWORK_TIMEOUT);
                CustomEventInterstitialAdapter.this.m26666();
            }
        };
        try {
            this.f20743 = CustomEventInterstitialFactory.create(str);
            this.f20746 = new TreeMap(map);
            this.f20745 = this.f20751.getLocalExtras();
            if (this.f20751.getLocation() != null) {
                this.f20745.put("location", this.f20751.getLocation());
            }
            this.f20745.put(DataKeys.BROADCAST_IDENTIFIER_KEY, Long.valueOf(j));
            this.f20745.put(DataKeys.AD_REPORT_KEY, adReport);
        } catch (Exception e) {
            this.f20751.onCustomEventInterstitialFailed(MoPubErrorCode.ADAPTER_NOT_FOUND);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m26662() {
        if (this.f20751 == null || this.f20751.m26706() == null || this.f20751.m26706().intValue() < 0) {
            return 30000;
        }
        return this.f20751.m26706().intValue() * 1000;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m26663() {
        this.f20748.removeCallbacks(this.f20750);
    }

    public void onInterstitialClicked() {
        if (!m26665() && this.f20747 != null) {
            this.f20747.onCustomEventInterstitialClicked();
        }
    }

    public void onInterstitialDismissed() {
        if (!m26665() && this.f20747 != null) {
            this.f20747.onCustomEventInterstitialDismissed();
        }
    }

    public void onInterstitialFailed(MoPubErrorCode moPubErrorCode) {
        if (!m26665() && this.f20747 != null) {
            if (moPubErrorCode == null) {
                moPubErrorCode = MoPubErrorCode.UNSPECIFIED;
            }
            m26663();
            this.f20747.onCustomEventInterstitialFailed(moPubErrorCode);
        }
    }

    public void onInterstitialLoaded() {
        if (!m26665()) {
            m26663();
            if (this.f20747 != null) {
                this.f20747.onCustomEventInterstitialLoaded();
            }
        }
    }

    public void onInterstitialShown() {
        if (!m26665() && this.f20747 != null) {
            this.f20747.onCustomEventInterstitialShown();
        }
    }

    public void onLeaveApplication() {
        onInterstitialClicked();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26664() {
        if (!m26665() && this.f20743 != null) {
            try {
                CustomEventInterstitial customEventInterstitial = this.f20743;
                Pinkamena.DianePie();
            } catch (Exception e) {
                onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m26665() {
        return this.f20749;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m26666() {
        if (this.f20743 != null) {
            try {
                this.f20743.onInvalidate();
            } catch (Exception e) {
            }
        }
        this.f20743 = null;
        this.f20744 = null;
        this.f20746 = null;
        this.f20745 = null;
        this.f20747 = null;
        this.f20749 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26667() {
        if (!m26665() && this.f20743 != null) {
            this.f20748.postDelayed(this.f20750, (long) m26662());
            try {
                CustomEventInterstitial customEventInterstitial = this.f20743;
                Context context = this.f20744;
                Map<String, Object> map = this.f20745;
                Map<String, String> map2 = this.f20746;
                Pinkamena.DianePie();
            } catch (Exception e) {
                onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26668(CustomEventInterstitialAdapterListener customEventInterstitialAdapterListener) {
        this.f20747 = customEventInterstitialAdapterListener;
    }
}
