package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.mopub.common.IntentActions;
import com.mopub.common.logging.MoPubLog;

public abstract class BaseVideoViewController {

    /* renamed from: 靐  reason: contains not printable characters */
    private final RelativeLayout f20729 = new RelativeLayout(this.f20732);

    /* renamed from: 麤  reason: contains not printable characters */
    private Long f20730;

    /* renamed from: 齉  reason: contains not printable characters */
    private final BaseVideoViewControllerListener f20731;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f20732;

    public interface BaseVideoViewControllerListener {
        void onFinish();

        void onSetContentView(View view);

        void onSetRequestedOrientation(int i);

        void onStartActivityForResult(Class<? extends Activity> cls, int i, Bundle bundle);
    }

    protected BaseVideoViewController(Context context, Long l, BaseVideoViewControllerListener baseVideoViewControllerListener) {
        this.f20732 = context;
        this.f20730 = l;
        this.f20731 = baseVideoViewControllerListener;
    }

    public boolean backButtonEnabled() {
        return true;
    }

    public ViewGroup getLayout() {
        return this.f20729;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m26643();

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public BaseVideoViewControllerListener m26644() {
        return this.f20731;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public Context m26645() {
        return this.f20732;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public abstract void m26646();

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract VideoView m26647();

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26648(boolean z) {
        if (z) {
            this.f20731.onFinish();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public abstract void m26649();

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract void m26650();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26651() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        this.f20729.addView(m26647(), 0, layoutParams);
        this.f20731.onSetContentView(this.f20729);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26652(int i, int i2, Intent intent) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26653(Configuration configuration);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26654(Bundle bundle);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26655(String str) {
        if (this.f20730 != null) {
            BaseBroadcastReceiver.broadcastAction(this.f20732, this.f20730.longValue(), str);
        } else {
            MoPubLog.w("Tried to broadcast a video event without a broadcast identifier to send to.");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26656(boolean z) {
        MoPubLog.e("Video cannot be played.");
        m26655(IntentActions.ACTION_INTERSTITIAL_FAIL);
        if (z) {
            this.f20731.onFinish();
        }
    }
}
