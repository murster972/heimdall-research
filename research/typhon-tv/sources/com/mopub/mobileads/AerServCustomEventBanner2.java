package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.Pinkamena;
import com.aerserv.sdk.AerServBanner;
import com.aerserv.sdk.AerServConfig;
import com.aerserv.sdk.AerServEvent;
import com.aerserv.sdk.AerServEventListener;
import com.aerserv.sdk.AerServSdk;
import com.mopub.mobileads.CustomEventBanner;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import java.util.List;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

public class AerServCustomEventBanner2 extends CustomEventBanner {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f20702 = AerServCustomEventBanner2.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public AerServBanner f20703 = null;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public CustomEventBanner.CustomEventBannerListener f20704 = null;

    /* renamed from: com.mopub.mobileads.AerServCustomEventBanner2$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f20709 = new int[AerServEvent.values().length];

        static {
            try {
                f20709[AerServEvent.AD_LOADED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f20709[AerServEvent.AD_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f20709[AerServEvent.AD_CLICKED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26626() {
        if (this.f20703 != null) {
            this.f20703.kill();
            this.f20703 = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26627(final Context context, CustomEventBanner.CustomEventBannerListener customEventBannerListener, Map<String, Object> map, Map<String, String> map2) {
        if (customEventBannerListener != null) {
            if (TyphoonApp.f5874 != 0) {
                customEventBannerListener.onBannerFailed(MoPubErrorCode.NO_FILL);
            } else if (context == null) {
                customEventBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            } else if (!(context instanceof Activity)) {
                customEventBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            } else {
                if (!TyphoonApp.f5864) {
                    try {
                        AerServSdk.init((Activity) context, "1004083");
                    } catch (Throwable th) {
                        Logger.m6281(th, new boolean[0]);
                    }
                    TyphoonApp.f5864 = true;
                }
                this.f20704 = customEventBannerListener;
                String string = AerServPluginUtil.getString("placement", map, map2);
                if (string == null) {
                    Log.w(f20702, "Cannot load AerServ ad because placement is missing");
                    customEventBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                    return;
                }
                this.f20703 = new AerServBanner(context);
                AerServConfig aerServConfig = new AerServConfig(context, string);
                aerServConfig.enableBackButton(true);
                aerServConfig.setDebug(false);
                Integer integer = AerServPluginUtil.getInteger("timeoutMillis", map, map2);
                if (integer != null) {
                    aerServConfig.setTimeout(integer.intValue());
                }
                List<String> stringList = AerServPluginUtil.getStringList(PubnativeRequest.Parameters.KEYWORDS, map, map2);
                if (stringList != null) {
                    aerServConfig.setKeywords(stringList);
                }
                aerServConfig.setEventListener(new AerServEventListener() {
                    public void onAerServEvent(final AerServEvent aerServEvent, List<Object> list) {
                        new Handler(context.getMainLooper()).post(new Runnable() {
                            public void run() {
                                switch (AnonymousClass2.f20709[aerServEvent.ordinal()]) {
                                    case 1:
                                        AerServCustomEventBanner2.this.f20704.onBannerLoaded(AerServCustomEventBanner2.this.f20703);
                                        return;
                                    case 2:
                                        AerServCustomEventBanner2.this.f20704.onBannerFailed(MoPubErrorCode.NETWORK_NO_FILL);
                                        return;
                                    case 3:
                                        AerServCustomEventBanner2.this.f20704.onBannerClicked();
                                        return;
                                    default:
                                        return;
                                }
                            }
                        });
                    }
                });
                this.f20703.configure(aerServConfig);
                AerServBanner aerServBanner = this.f20703;
                Pinkamena.DianePie();
            }
        }
    }
}
