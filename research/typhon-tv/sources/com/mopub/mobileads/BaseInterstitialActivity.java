package com.mopub.mobileads;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.mopub.common.AdReport;
import com.mopub.common.CloseableLayout;
import com.mopub.common.DataKeys;

abstract class BaseInterstitialActivity extends Activity {

    /* renamed from: 靐  reason: contains not printable characters */
    private CloseableLayout f20724;

    /* renamed from: 齉  reason: contains not printable characters */
    private Long f20725;

    /* renamed from: 龘  reason: contains not printable characters */
    protected AdReport f20726;

    enum JavaScriptWebViewCallbacks {
        WEB_VIEW_DID_APPEAR("webviewDidAppear();"),
        WEB_VIEW_DID_CLOSE("webviewDidClose();");
        
        private String mJavascript;

        private JavaScriptWebViewCallbacks(String str) {
            this.mJavascript = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public String m26638() {
            return "javascript:" + this.mJavascript;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m26639() {
            return this.mJavascript;
        }
    }

    BaseInterstitialActivity() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    protected static AdReport m26633(Intent intent) {
        try {
            return (AdReport) intent.getSerializableExtra(DataKeys.AD_REPORT_KEY);
        } catch (ClassCastException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static Long m26634(Intent intent) {
        if (intent.hasExtra(DataKeys.BROADCAST_IDENTIFIER_KEY)) {
            return Long.valueOf(intent.getLongExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, -1));
        }
        return null;
    }

    public abstract View getAdView();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.f20725 = m26634(intent);
        this.f20726 = m26633(intent);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        View adView = getAdView();
        this.f20724 = new CloseableLayout(this);
        this.f20724.setOnCloseListener(new CloseableLayout.OnCloseListener() {
            public void onClose() {
                BaseInterstitialActivity.this.finish();
            }
        });
        this.f20724.addView(adView, new FrameLayout.LayoutParams(-1, -1));
        setContentView(this.f20724);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f20724 != null) {
            this.f20724.removeAllViews();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26635() {
        if (this.f20724 != null) {
            this.f20724.setCloseVisible(true);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m26636() {
        if (this.f20724 != null) {
            this.f20724.setCloseVisible(false);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Long m26637() {
        return this.f20725;
    }
}
