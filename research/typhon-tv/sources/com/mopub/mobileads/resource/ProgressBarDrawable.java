package com.mopub.mobileads.resource;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Dips;
import com.mopub.mobileads.resource.DrawableTyphoonApp;

public class ProgressBarDrawable extends BaseWidgetDrawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f5775;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f5776;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f5777;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f5778;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Paint f5779;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f5780;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f5781;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f5782 = new Paint();

    public ProgressBarDrawable(Context context) {
        this.f5782.setColor(-1);
        this.f5782.setAlpha(128);
        this.f5782.setStyle(DrawableTyphoonApp.ProgressBar.BACKGROUND_STYLE);
        this.f5782.setAntiAlias(true);
        this.f5779 = new Paint();
        this.f5779.setColor(DrawableTyphoonApp.ProgressBar.PROGRESS_COLOR);
        this.f5779.setAlpha(255);
        this.f5779.setStyle(DrawableTyphoonApp.ProgressBar.PROGRESS_STYLE);
        this.f5779.setAntiAlias(true);
        this.f5781 = Dips.dipsToIntPixels(4.0f, context);
    }

    public void draw(Canvas canvas) {
        canvas.drawRect(getBounds(), this.f5782);
        Canvas canvas2 = canvas;
        canvas2.drawRect((float) getBounds().left, (float) getBounds().top, ((float) getBounds().right) * (((float) this.f5775) / ((float) this.f5780)), (float) getBounds().bottom, this.f5779);
        if (this.f5778 > 0 && this.f5778 < this.f5780) {
            float f = ((float) getBounds().right) * this.f5777;
            canvas.drawRect(f, (float) getBounds().top, f + ((float) this.f5781), (float) getBounds().bottom, this.f5779);
        }
    }

    @VisibleForTesting
    public void forceCompletion() {
        this.f5775 = this.f5780;
    }

    @Deprecated
    @VisibleForTesting
    public int getCurrentProgress() {
        return this.f5775;
    }

    @Deprecated
    @VisibleForTesting
    public float getSkipRatio() {
        return this.f5777;
    }

    public void reset() {
        this.f5776 = 0;
    }

    public void setDurationAndSkipOffset(int i, int i2) {
        this.f5780 = i;
        this.f5778 = i2;
        this.f5777 = ((float) this.f5778) / ((float) this.f5780);
    }

    @SuppressLint({"DefaultLocale"})
    public void setProgress(int i) {
        if (i >= this.f5776) {
            this.f5775 = i;
            this.f5776 = i;
        } else if (i != 0) {
            MoPubLog.d(String.format("Progress not monotonically increasing: last = %d, current = %d", new Object[]{Integer.valueOf(this.f5776), Integer.valueOf(i)}));
            forceCompletion();
        }
        invalidateSelf();
    }
}
