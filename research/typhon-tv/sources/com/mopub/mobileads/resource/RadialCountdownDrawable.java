package com.mopub.mobileads.resource;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Numbers;
import com.mopub.mobileads.resource.DrawableTyphoonApp;

public class RadialCountdownDrawable extends BaseWidgetDrawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f5783;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f5784;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f5785;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Paint f5786;

    /* renamed from: 麤  reason: contains not printable characters */
    private Rect f5787;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Paint f5788;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f5789 = new Paint();

    public RadialCountdownDrawable(Context context) {
        int dipsToIntPixels = Dips.dipsToIntPixels(3.0f, context);
        float dipsToFloatPixels = Dips.dipsToFloatPixels(18.0f, context);
        this.f5789.setColor(-1);
        this.f5789.setAlpha(128);
        this.f5789.setStyle(DrawableTyphoonApp.RadialCountdown.BACKGROUND_STYLE);
        this.f5789.setStrokeWidth((float) dipsToIntPixels);
        this.f5789.setAntiAlias(true);
        this.f5786 = new Paint();
        this.f5786.setColor(-1);
        this.f5786.setAlpha(255);
        this.f5786.setStyle(DrawableTyphoonApp.RadialCountdown.PROGRESS_STYLE);
        this.f5786.setStrokeWidth((float) dipsToIntPixels);
        this.f5786.setAntiAlias(true);
        this.f5788 = new Paint();
        this.f5788.setColor(-1);
        this.f5788.setTextAlign(DrawableTyphoonApp.RadialCountdown.TEXT_ALIGN);
        this.f5788.setTextSize(dipsToFloatPixels);
        this.f5788.setAntiAlias(true);
        this.f5787 = new Rect();
    }

    public void draw(Canvas canvas) {
        int centerX = getBounds().centerX();
        int centerY = getBounds().centerY();
        canvas.drawCircle((float) centerX, (float) centerY, (float) Math.min(centerX, centerY), this.f5789);
        m26902(canvas, this.f5788, this.f5787, String.valueOf(this.f5783));
        canvas.drawArc(new RectF(getBounds()), -90.0f, this.f5784, false, this.f5786);
    }

    @Deprecated
    @VisibleForTesting
    public int getInitialCountdownMilliseconds() {
        return this.f5785;
    }

    public void setInitialCountdown(int i) {
        this.f5785 = i;
    }

    public void updateCountdownProgress(int i) {
        this.f5783 = (int) Numbers.convertMillisecondsToSecondsRoundedUp((long) (this.f5785 - i));
        this.f5784 = (360.0f * ((float) i)) / ((float) this.f5785);
        invalidateSelf();
    }
}
