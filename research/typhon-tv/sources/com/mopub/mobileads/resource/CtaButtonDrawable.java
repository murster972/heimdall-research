package com.mopub.mobileads.resource;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.mobileads.resource.DrawableTyphoonApp;

public class CtaButtonDrawable extends BaseWidgetDrawable {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f5768;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f5769;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Rect f5770;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Paint f5771;

    /* renamed from: 麤  reason: contains not printable characters */
    private final RectF f5772;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Paint f5773;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f5774 = new Paint();

    public CtaButtonDrawable(Context context) {
        int dipsToIntPixels = Dips.dipsToIntPixels(2.0f, context);
        float dipsToFloatPixels = Dips.dipsToFloatPixels(15.0f, context);
        this.f5774.setColor(-16777216);
        this.f5774.setAlpha(51);
        this.f5774.setStyle(DrawableTyphoonApp.CtaButton.BACKGROUND_STYLE);
        this.f5774.setAntiAlias(true);
        this.f5771 = new Paint();
        this.f5771.setColor(-1);
        this.f5771.setAlpha(51);
        this.f5771.setStyle(DrawableTyphoonApp.CtaButton.OUTLINE_STYLE);
        this.f5771.setStrokeWidth((float) dipsToIntPixels);
        this.f5771.setAntiAlias(true);
        this.f5773 = new Paint();
        this.f5773.setColor(-1);
        this.f5773.setTextAlign(DrawableTyphoonApp.CtaButton.TEXT_ALIGN);
        this.f5773.setTypeface(DrawableTyphoonApp.CtaButton.TEXT_TYPEFACE);
        this.f5773.setTextSize(dipsToFloatPixels);
        this.f5773.setAntiAlias(true);
        this.f5770 = new Rect();
        this.f5769 = DrawableTyphoonApp.CtaButton.DEFAULT_CTA_TEXT;
        this.f5772 = new RectF();
        this.f5768 = Dips.dipsToIntPixels(6.0f, context);
    }

    public void draw(Canvas canvas) {
        this.f5772.set(getBounds());
        canvas.drawRoundRect(this.f5772, (float) this.f5768, (float) this.f5768, this.f5774);
        canvas.drawRoundRect(this.f5772, (float) this.f5768, (float) this.f5768, this.f5771);
        m26902(canvas, this.f5773, this.f5770, this.f5769);
    }

    @Deprecated
    @VisibleForTesting
    public String getCtaText() {
        return this.f5769;
    }

    public void setCtaText(String str) {
        this.f5769 = str;
        invalidateSelf();
    }
}
