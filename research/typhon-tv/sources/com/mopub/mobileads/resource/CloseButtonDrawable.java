package com.mopub.mobileads.resource;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.mopub.mobileads.resource.DrawableTyphoonApp;

public class CloseButtonDrawable extends BaseWidgetDrawable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final float f20970;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f20971;

    public CloseButtonDrawable() {
        this(8.0f);
    }

    public CloseButtonDrawable(float f) {
        this.f20970 = f / 2.0f;
        this.f20971 = new Paint();
        this.f20971.setColor(-1);
        this.f20971.setStrokeWidth(f);
        this.f20971.setStrokeCap(DrawableTyphoonApp.CloseButton.STROKE_CAP);
    }

    public void draw(Canvas canvas) {
        int width = getBounds().width();
        int height = getBounds().height();
        canvas.drawLine(0.0f + this.f20970, ((float) height) - this.f20970, ((float) width) - this.f20970, 0.0f + this.f20970, this.f20971);
        canvas.drawLine(0.0f + this.f20970, 0.0f + this.f20970, ((float) width) - this.f20970, ((float) height) - this.f20970, this.f20971);
    }
}
