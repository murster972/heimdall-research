package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.mopub.common.IntentActions;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.BaseVideoViewController;
import com.mopub.mobileads.VastWebView;
import com.mopub.network.TrackingRequest;
import java.io.Serializable;
import java.util.Map;

public class VastVideoViewController extends BaseVideoViewController {
    public static final int WEBVIEW_PADDING = 16;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<String, VastCompanionAdConfig> f20893;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final View f20894;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final View f20895;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public VastVideoGradientStripWidget f20896;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public ImageView f20897;
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public View f20898;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public VastVideoGradientStripWidget f20899;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public int f20900 = 5000;
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public VastVideoCtaButtonWidget f20901;

    /* renamed from: ˋ  reason: contains not printable characters */
    private VastVideoCloseButtonWidget f20902;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public VastCompanionAdConfig f20903;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f20904;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final VastVideoViewProgressRunnable f20905;

    /* renamed from: י  reason: contains not printable characters */
    private int f20906 = -1;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public boolean f20907;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final VastVideoViewCountdownRunnable f20908;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final View.OnTouchListener f20909;
    /* access modifiers changed from: private */

    /* renamed from: ᴵ  reason: contains not printable characters */
    public boolean f20910;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f20911 = false;
    /* access modifiers changed from: private */

    /* renamed from: ᵔ  reason: contains not printable characters */
    public boolean f20912 = false;
    /* access modifiers changed from: private */

    /* renamed from: ᵢ  reason: contains not printable characters */
    public boolean f20913 = false;
    /* access modifiers changed from: private */

    /* renamed from: ⁱ  reason: contains not printable characters */
    public int f20914;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final View f20915;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final VastVideoView f20916;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final View f20917;

    /* renamed from: 齉  reason: contains not printable characters */
    private final VastIconConfig f20918;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final VastVideoConfig f20919;
    /* access modifiers changed from: private */

    /* renamed from: ﹳ  reason: contains not printable characters */
    public boolean f20920 = false;
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public VastVideoProgressBarWidget f20921;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public VastVideoRadialCountdownWidget f20922;

    VastVideoViewController(final Activity activity, Bundle bundle, Bundle bundle2, long j, BaseVideoViewController.BaseVideoViewControllerListener baseVideoViewControllerListener) throws IllegalStateException {
        super(activity, Long.valueOf(j), baseVideoViewControllerListener);
        Serializable serializable = bundle2 != null ? bundle2.getSerializable("resumed_vast_config") : null;
        Serializable serializable2 = bundle.getSerializable("vast_video_config");
        if (serializable != null && (serializable instanceof VastVideoConfig)) {
            this.f20919 = (VastVideoConfig) serializable;
            this.f20906 = bundle2.getInt("current_position", -1);
        } else if (serializable2 == null || !(serializable2 instanceof VastVideoConfig)) {
            throw new IllegalStateException("VastVideoConfig is invalid");
        } else {
            this.f20919 = (VastVideoConfig) serializable2;
        }
        if (this.f20919.getDiskMediaFileUrl() == null) {
            throw new IllegalStateException("VastVideoConfig does not have a video disk path");
        }
        this.f20903 = this.f20919.getVastCompanionAd(activity.getResources().getConfiguration().orientation);
        this.f20893 = this.f20919.getSocialActionsCompanionAds();
        this.f20918 = this.f20919.getVastIconConfig();
        this.f20909 = new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 1 && VastVideoViewController.this.m26812()) {
                    boolean unused = VastVideoViewController.this.f20920 = true;
                    VastVideoViewController.this.m26655(IntentActions.ACTION_INTERSTITIAL_CLICK);
                    VastVideoViewController.this.f20919.handleClickForResult(activity, VastVideoViewController.this.f20907 ? VastVideoViewController.this.f20914 : VastVideoViewController.this.m26848(), 1);
                }
                return true;
            }
        };
        getLayout().setBackgroundColor(-16777216);
        m26821(activity, 4);
        this.f20916 = m26835((Context) activity, 0);
        this.f20916.requestFocus();
        this.f20917 = m26855((Context) activity, this.f20919.getVastCompanionAd(2), 4);
        this.f20915 = m26855((Context) activity, this.f20919.getVastCompanionAd(1), 4);
        m26837((Context) activity);
        m26823((Context) activity, 4);
        m26822((Context) activity);
        m26831((Context) activity, 4);
        this.f20895 = m26857((Context) activity, this.f20918, 4);
        this.f20895.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                View unused = VastVideoViewController.this.f20898 = VastVideoViewController.this.m26854(activity);
                VastVideoViewController.this.f20895.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
        m26830((Context) activity);
        this.f20894 = m26856(activity, this.f20893.get(VastXmlManagerAggregator.SOCIAL_ACTIONS_AD_SLOT_ID), Dips.dipsToIntPixels(38.0f, activity), 6, this.f20901, 4, 16);
        m26827((Context) activity, 8);
        Handler handler = new Handler(Looper.getMainLooper());
        this.f20905 = new VastVideoViewProgressRunnable(this, this.f20919, handler);
        this.f20908 = new VastVideoViewCountdownRunnable(this, handler);
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m26812() {
        return this.f20904;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m26814() {
        this.f20905.startRepeating(50);
        this.f20908.startRepeating(250);
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m26816() {
        this.f20905.stop();
        this.f20908.stop();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m26821(Context context, int i) {
        this.f20897 = new ImageView(context);
        this.f20897.setVisibility(i);
        getLayout().addView(this.f20897, new RelativeLayout.LayoutParams(-1, -1));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26822(Context context) {
        this.f20896 = new VastVideoGradientStripWidget(context, GradientDrawable.Orientation.BOTTOM_TOP, this.f20919.getCustomForceOrientation(), this.f20903 != null, 8, 2, this.f20921.getId());
        getLayout().addView(this.f20896);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26823(Context context, int i) {
        this.f20921 = new VastVideoProgressBarWidget(context);
        this.f20921.setAnchorId(this.f20916.getId());
        this.f20921.setVisibility(i);
        getLayout().addView(this.f20921);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m26827(Context context, int i) {
        this.f20902 = new VastVideoCloseButtonWidget(context);
        this.f20902.setVisibility(i);
        getLayout().addView(this.f20902);
        this.f20902.setOnTouchListenerToContent(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int r0 = VastVideoViewController.this.f20907 ? VastVideoViewController.this.f20914 : VastVideoViewController.this.m26848();
                if (motionEvent.getAction() == 1) {
                    boolean unused = VastVideoViewController.this.f20920 = true;
                    VastVideoViewController.this.f20919.handleClose(VastVideoViewController.this.m26645(), r0);
                    VastVideoViewController.this.m26644().onFinish();
                }
                return true;
            }
        });
        String customSkipText = this.f20919.getCustomSkipText();
        if (customSkipText != null) {
            this.f20902.m6231(customSkipText);
        }
        String customCloseIconUrl = this.f20919.getCustomCloseIconUrl();
        if (customCloseIconUrl != null) {
            this.f20902.m6230(customCloseIconUrl);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26830(Context context) {
        this.f20901 = new VastVideoCtaButtonWidget(context, this.f20916.getId(), this.f20903 != null, !TextUtils.isEmpty(this.f20919.getClickThroughUrl()));
        getLayout().addView(this.f20901);
        this.f20901.setOnTouchListener(this.f20909);
        String customCtaText = this.f20919.getCustomCtaText();
        if (customCtaText != null) {
            this.f20901.m6235(customCtaText);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26831(Context context, int i) {
        this.f20922 = new VastVideoRadialCountdownWidget(context);
        this.f20922.setVisibility(i);
        getLayout().addView(this.f20922);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private VastVideoView m26835(final Context context, int i) {
        if (this.f20919.getDiskMediaFileUrl() == null) {
            throw new IllegalStateException("VastVideoConfig does not have a video disk path");
        }
        final VastVideoView vastVideoView = new VastVideoView(context);
        vastVideoView.setId((int) Utils.generateUniqueId());
        vastVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                int unused = VastVideoViewController.this.f20914 = VastVideoViewController.this.f20916.getDuration();
                VastVideoViewController.this.m26842();
                if (VastVideoViewController.this.f20903 == null || VastVideoViewController.this.f20913) {
                    vastVideoView.prepareBlurredLastVideoFrame(VastVideoViewController.this.f20897, VastVideoViewController.this.f20919.getDiskMediaFileUrl());
                }
                VastVideoViewController.this.f20921.calibrateAndMakeVisible(VastVideoViewController.this.m26847(), VastVideoViewController.this.f20900);
                VastVideoViewController.this.f20922.calibrateAndMakeVisible(VastVideoViewController.this.f20900);
                boolean unused2 = VastVideoViewController.this.f20912 = true;
            }
        });
        vastVideoView.setOnTouchListener(this.f20909);
        vastVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                VastVideoViewController.this.m26816();
                VastVideoViewController.this.m26849();
                VastVideoViewController.this.m26648(false);
                boolean unused = VastVideoViewController.this.f20907 = true;
                if (VastVideoViewController.this.f20919.isRewardedVideo()) {
                    VastVideoViewController.this.m26655(IntentActions.ACTION_REWARDED_VIDEO_COMPLETE);
                }
                if (!VastVideoViewController.this.f20910 && VastVideoViewController.this.f20919.getRemainingProgressTrackerCount() == 0) {
                    VastVideoViewController.this.f20919.handleComplete(VastVideoViewController.this.m26645(), VastVideoViewController.this.m26848());
                }
                vastVideoView.setVisibility(8);
                VastVideoViewController.this.f20921.setVisibility(8);
                if (!VastVideoViewController.this.f20913) {
                    VastVideoViewController.this.f20895.setVisibility(8);
                } else if (VastVideoViewController.this.f20897.getDrawable() != null) {
                    VastVideoViewController.this.f20897.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    VastVideoViewController.this.f20897.setVisibility(0);
                }
                VastVideoViewController.this.f20899.m26801();
                VastVideoViewController.this.f20896.m26801();
                VastVideoViewController.this.f20901.m6233();
                if (VastVideoViewController.this.f20903 != null) {
                    if (context.getResources().getConfiguration().orientation == 1) {
                        VastVideoViewController.this.f20915.setVisibility(0);
                    } else {
                        VastVideoViewController.this.f20917.setVisibility(0);
                    }
                    VastVideoViewController.this.f20903.m26729(context, VastVideoViewController.this.f20914);
                } else if (VastVideoViewController.this.f20897.getDrawable() != null) {
                    VastVideoViewController.this.f20897.setVisibility(0);
                }
            }
        });
        vastVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                if (vastVideoView.m6237(mediaPlayer, i, i2, VastVideoViewController.this.f20919.getDiskMediaFileUrl())) {
                    return true;
                }
                VastVideoViewController.this.m26816();
                VastVideoViewController.this.m26849();
                VastVideoViewController.this.m26656(false);
                boolean unused = VastVideoViewController.this.f20910 = true;
                VastVideoViewController.this.f20919.handleError(VastVideoViewController.this.m26645(), VastErrorCode.GENERAL_LINEAR_AD_ERROR, VastVideoViewController.this.m26848());
                return false;
            }
        });
        vastVideoView.setVideoPath(this.f20919.getDiskMediaFileUrl());
        vastVideoView.setVisibility(i);
        return vastVideoView;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private VastWebView m26836(final Context context, final VastCompanionAdConfig vastCompanionAdConfig) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(vastCompanionAdConfig);
        VastWebView r0 = VastWebView.m26865(context, vastCompanionAdConfig.getVastResource());
        r0.m26866((VastWebView.VastWebViewClickListener) new VastWebView.VastWebViewClickListener() {
            public void onVastWebViewClick() {
                VastVideoViewController.this.m26655(IntentActions.ACTION_INTERSTITIAL_CLICK);
                TrackingRequest.makeVastTrackingHttpRequest(vastCompanionAdConfig.getClickTrackers(), (VastErrorCode) null, Integer.valueOf(VastVideoViewController.this.f20914), (String) null, context);
                vastCompanionAdConfig.m26730(context, 1, (String) null, VastVideoViewController.this.f20919.getDspCreativeId());
            }
        });
        r0.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                vastCompanionAdConfig.m26730(context, 1, str, VastVideoViewController.this.f20919.getDspCreativeId());
                return true;
            }
        });
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26837(Context context) {
        this.f20899 = new VastVideoGradientStripWidget(context, GradientDrawable.Orientation.TOP_BOTTOM, this.f20919.getCustomForceOrientation(), this.f20903 != null, 0, 6, getLayout().getId());
        getLayout().addView(this.f20899);
    }

    /* access modifiers changed from: private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public void m26842() {
        int r1 = m26847();
        if (this.f20919.isRewardedVideo()) {
            this.f20900 = r1;
            return;
        }
        if (r1 < 16000) {
            this.f20900 = r1;
        }
        Integer skipOffsetMillis = this.f20919.getSkipOffsetMillis(r1);
        if (skipOffsetMillis != null) {
            this.f20900 = skipOffsetMillis.intValue();
            this.f20911 = true;
        }
    }

    public boolean backButtonEnabled() {
        return this.f20904;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m26843() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m26844() {
        if (this.f20912) {
            this.f20922.updateCountdownProgress(this.f20900, m26848());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m26845() {
        this.f20921.updateProgress(m26848());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m26846() {
        return !this.f20904 && m26848() >= this.f20900;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public int m26847() {
        return this.f20916.getDuration();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public int m26848() {
        return this.f20916.getCurrentPosition();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m26849() {
        this.f20904 = true;
        this.f20922.setVisibility(8);
        this.f20902.setVisibility(0);
        this.f20901.m6234();
        this.f20894.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m26850() {
        m26816();
        m26655(IntentActions.ACTION_INTERSTITIAL_DISMISS);
        this.f20916.onDestroy();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public VideoView m26851() {
        return this.f20916;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m26852() {
        m26814();
        if (this.f20906 > 0) {
            this.f20916.seekTo(this.f20906);
        }
        if (!this.f20907) {
            this.f20916.start();
        }
        if (this.f20906 != -1) {
            this.f20919.handleResume(m26645(), this.f20906);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m26853() {
        m26816();
        this.f20906 = m26848();
        this.f20916.pause();
        if (!this.f20907 && !this.f20920) {
            this.f20919.handlePause(m26645(), this.f20906);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public View m26854(Activity activity) {
        return m26856(activity, this.f20893.get(VastXmlManagerAggregator.ADS_BY_AD_SLOT_ID), this.f20895.getHeight(), 1, this.f20895, 0, 6);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public View m26855(Context context, VastCompanionAdConfig vastCompanionAdConfig, int i) {
        Preconditions.checkNotNull(context);
        if (vastCompanionAdConfig == null) {
            View view = new View(context);
            view.setVisibility(4);
            return view;
        }
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setGravity(17);
        getLayout().addView(relativeLayout, new RelativeLayout.LayoutParams(-1, -1));
        VastWebView r1 = m26836(context, vastCompanionAdConfig);
        r1.setVisibility(i);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(Dips.dipsToIntPixels((float) (vastCompanionAdConfig.getWidth() + 16), context), Dips.dipsToIntPixels((float) (vastCompanionAdConfig.getHeight() + 16), context));
        layoutParams.addRule(13, -1);
        relativeLayout.addView(r1, layoutParams);
        return r1;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public View m26856(Context context, VastCompanionAdConfig vastCompanionAdConfig, int i, int i2, View view, int i3, int i4) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(view);
        if (vastCompanionAdConfig == null) {
            View view2 = new View(context);
            view2.setVisibility(4);
            return view2;
        }
        this.f20913 = true;
        this.f20901.setHasSocialActions(this.f20913);
        VastWebView r2 = m26836(context, vastCompanionAdConfig);
        int dipsToIntPixels = Dips.dipsToIntPixels((float) vastCompanionAdConfig.getWidth(), context);
        int dipsToIntPixels2 = Dips.dipsToIntPixels((float) vastCompanionAdConfig.getHeight(), context);
        int dipsToIntPixels3 = Dips.dipsToIntPixels((float) i4, context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dipsToIntPixels, dipsToIntPixels2);
        layoutParams.addRule(i2, view.getId());
        layoutParams.addRule(6, view.getId());
        layoutParams.setMargins(dipsToIntPixels3, (i - dipsToIntPixels2) / 2, 0, 0);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setGravity(16);
        relativeLayout.addView(r2, new RelativeLayout.LayoutParams(-2, -2));
        getLayout().addView(relativeLayout, layoutParams);
        r2.setVisibility(i3);
        return r2;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public View m26857(final Context context, final VastIconConfig vastIconConfig, int i) {
        Preconditions.checkNotNull(context);
        if (vastIconConfig == null) {
            return new View(context);
        }
        VastWebView r0 = VastWebView.m26865(context, vastIconConfig.m26744());
        r0.m26866((VastWebView.VastWebViewClickListener) new VastWebView.VastWebViewClickListener() {
            public void onVastWebViewClick() {
                TrackingRequest.makeVastTrackingHttpRequest(vastIconConfig.m26743(), (VastErrorCode) null, Integer.valueOf(VastVideoViewController.this.m26848()), VastVideoViewController.this.m26863(), context);
                vastIconConfig.m26750(VastVideoViewController.this.m26645(), (String) null, VastVideoViewController.this.f20919.getDspCreativeId());
            }
        });
        r0.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                vastIconConfig.m26750(VastVideoViewController.this.m26645(), str, VastVideoViewController.this.f20919.getDspCreativeId());
                return true;
            }
        });
        r0.setVisibility(i);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(Dips.asIntPixels((float) vastIconConfig.m26748(), context), Dips.asIntPixels((float) vastIconConfig.m26745(), context));
        layoutParams.setMargins(Dips.dipsToIntPixels(12.0f, context), Dips.dipsToIntPixels(12.0f, context), 0, 0);
        getLayout().addView(r0, layoutParams);
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26858() {
        super.m26651();
        switch (this.f20919.getCustomForceOrientation()) {
            case FORCE_PORTRAIT:
                m26644().onSetRequestedOrientation(1);
                break;
            case FORCE_LANDSCAPE:
                m26644().onSetRequestedOrientation(6);
                break;
        }
        this.f20919.handleImpression(m26645(), m26848());
        m26655(IntentActions.ACTION_INTERSTITIAL_SHOW);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26859(int i) {
        if (this.f20918 != null && i >= this.f20918.m26747()) {
            this.f20895.setVisibility(0);
            this.f20918.m26749(m26645(), i, m26863());
            if (this.f20918.m26746() != null && i >= this.f20918.m26747() + this.f20918.m26746().intValue()) {
                this.f20895.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26860(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            m26644().onFinish();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26861(Configuration configuration) {
        int i = m26645().getResources().getConfiguration().orientation;
        this.f20903 = this.f20919.getVastCompanionAd(i);
        if (this.f20917.getVisibility() == 0 || this.f20915.getVisibility() == 0) {
            if (i == 1) {
                this.f20917.setVisibility(4);
                this.f20915.setVisibility(0);
            } else {
                this.f20915.setVisibility(4);
                this.f20917.setVisibility(0);
            }
            if (this.f20903 != null) {
                this.f20903.m26729(m26645(), this.f20914);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26862(Bundle bundle) {
        bundle.putInt("current_position", this.f20906);
        bundle.putSerializable("resumed_vast_config", this.f20919);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public String m26863() {
        if (this.f20919 == null) {
            return null;
        }
        return this.f20919.getNetworkMediaFileUrl();
    }
}
