package com.mopub.mobileads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import com.Pinkamena;
import com.mopub.common.AdReport;
import com.mopub.common.DataKeys;
import com.mopub.common.IntentActions;
import com.mopub.common.TyphoonApp;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.BaseInterstitialActivity;
import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mraid.MraidController;
import com.mopub.mraid.MraidWebViewClient;
import com.mopub.mraid.MraidWebViewDebugListener;
import com.mopub.mraid.PlacementType;
import com.mopub.network.Networking;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class MraidActivity extends BaseInterstitialActivity {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public MraidController f20827;

    /* renamed from: 齉  reason: contains not printable characters */
    private MraidWebViewDebugListener f20828;

    public static void preRenderHtml(Context context, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, String str) {
        m26715(customEventInterstitialListener, str, new BaseWebView(context));
    }

    public static void start(Context context, AdReport adReport, String str, long j) {
        try {
            context.startActivity(m26713(context, adReport, str, j));
        } catch (ActivityNotFoundException e) {
        }
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    protected static Intent m26713(Context context, AdReport adReport, String str, long j) {
        Intent intent = new Intent(context, MraidActivity.class);
        intent.putExtra(DataKeys.HTML_RESPONSE_BODY_KEY, str);
        intent.putExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, j);
        intent.putExtra(DataKeys.AD_REPORT_KEY, adReport);
        intent.addFlags(268435456);
        return intent;
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static void m26715(final CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, String str, final BaseWebView baseWebView) {
        baseWebView.m6210(false);
        baseWebView.m6211();
        baseWebView.setWebViewClient(new MraidWebViewClient() {
            public void onPageFinished(WebView webView, String str) {
                customEventInterstitialListener.onInterstitialLoaded();
                BaseWebView baseWebView = baseWebView;
                Pinkamena.DianePie();
                BaseWebView baseWebView2 = baseWebView;
                Pinkamena.DianePie();
            }

            public void onReceivedError(WebView webView, int i, String str, String str2) {
                super.onReceivedError(webView, i, str, str2);
                customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.MRAID_LOAD_ERROR);
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                return true;
            }
        });
        String str2 = Networking.getBaseUrlScheme() + "://" + TyphoonApp.HOST + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
        BaseWebView baseWebView2 = baseWebView;
        String str3 = str;
        Pinkamena.DianePie();
    }

    public View getAdView() {
        String stringExtra = getIntent().getStringExtra(DataKeys.HTML_RESPONSE_BODY_KEY);
        if (stringExtra == null) {
            MoPubLog.w("MraidActivity received a null HTML body. Finishing the activity.");
            finish();
            return new View(this);
        }
        this.f20827 = new MraidController(this, this.f20726, PlacementType.INTERSTITIAL);
        this.f20827.setDebugListener(this.f20828);
        this.f20827.setMraidListener(new MraidController.MraidListener() {
            public void onClose() {
                MraidActivity.this.f20827.loadJavascript(BaseInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_CLOSE.m26639());
                MraidActivity.this.finish();
            }

            public void onExpand() {
            }

            public void onFailedToLoad() {
                if (MraidActivity.this.m26637() != null) {
                    EventForwardingBroadcastReceiver.broadcastAction(MraidActivity.this, MraidActivity.this.m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_FAIL);
                }
                MraidActivity.this.finish();
            }

            public void onLoaded(View view) {
                MraidActivity.this.f20827.loadJavascript(BaseInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_APPEAR.m26639());
            }

            public void onOpen() {
                if (MraidActivity.this.m26637() != null) {
                    EventForwardingBroadcastReceiver.broadcastAction(MraidActivity.this, MraidActivity.this.m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_CLICK);
                }
            }
        });
        this.f20827.setUseCustomCloseListener(new MraidController.UseCustomCloseListener() {
            public void useCustomCloseChanged(boolean z) {
                if (z) {
                    MraidActivity.this.m26636();
                } else {
                    MraidActivity.this.m26635();
                }
            }
        });
        this.f20827.loadContent(stringExtra);
        return this.f20827.getAdContainer();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (m26637() != null) {
            EventForwardingBroadcastReceiver.broadcastAction(this, m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_SHOW);
        }
        if (Build.VERSION.SDK_INT >= 14) {
            getWindow().setFlags(16777216, 16777216);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f20827 != null) {
            this.f20827.destroy();
        }
        if (m26637() != null) {
            EventForwardingBroadcastReceiver.broadcastAction(this, m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_DISMISS);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.f20827 != null) {
            this.f20827.pause(isFinishing());
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f20827 != null) {
            this.f20827.resume();
        }
    }

    @VisibleForTesting
    public void setDebugListener(MraidWebViewDebugListener mraidWebViewDebugListener) {
        this.f20828 = mraidWebViewDebugListener;
        if (this.f20827 != null) {
            this.f20827.setDebugListener(mraidWebViewDebugListener);
        }
    }
}
