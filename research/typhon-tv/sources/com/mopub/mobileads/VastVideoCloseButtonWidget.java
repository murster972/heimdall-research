package com.mopub.mobileads;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.CloseButtonDrawable;
import com.mopub.mobileads.resource.DrawableTyphoonApp;
import com.mopub.network.Networking;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.ImageLoader;

public class VastVideoCloseButtonWidget extends RelativeLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private TextView f5735;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public ImageView f5736;

    /* renamed from: ʽ  reason: contains not printable characters */
    private CloseButtonDrawable f5737 = new CloseButtonDrawable();

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f5738;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f5739;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f5740;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f5741;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ImageLoader f5742;

    public VastVideoCloseButtonWidget(Context context) {
        super(context);
        setId((int) Utils.generateUniqueId());
        this.f5739 = Dips.dipsToIntPixels(6.0f, context);
        this.f5740 = Dips.dipsToIntPixels(15.0f, context);
        this.f5738 = Dips.dipsToIntPixels(56.0f, context);
        this.f5741 = Dips.dipsToIntPixels(0.0f, context);
        this.f5742 = Networking.getImageLoader(context);
        m6229();
        m6227();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, this.f5738);
        layoutParams.addRule(11);
        setLayoutParams(layoutParams);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m6227() {
        this.f5735 = new TextView(getContext());
        this.f5735.setSingleLine();
        this.f5735.setEllipsize(TextUtils.TruncateAt.END);
        this.f5735.setTextColor(-1);
        this.f5735.setTextSize(20.0f);
        this.f5735.setTypeface(DrawableTyphoonApp.CloseButton.TEXT_TYPEFACE);
        this.f5735.setText("");
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(15);
        layoutParams.addRule(0, this.f5736.getId());
        this.f5735.setPadding(0, this.f5739, 0, 0);
        layoutParams.setMargins(0, 0, this.f5741, 0);
        addView(this.f5735, layoutParams);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m6229() {
        this.f5736 = new ImageView(getContext());
        this.f5736.setId((int) Utils.generateUniqueId());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.f5738, this.f5738);
        layoutParams.addRule(11);
        this.f5736.setImageDrawable(this.f5737);
        this.f5736.setPadding(this.f5740, this.f5740 + this.f5739, this.f5740 + this.f5739, this.f5740);
        addView(this.f5736, layoutParams);
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public ImageView getImageView() {
        return this.f5736;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public TextView getTextView() {
        return this.f5735;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setImageView(ImageView imageView) {
        this.f5736 = imageView;
    }

    /* access modifiers changed from: package-private */
    public void setOnTouchListenerToContent(View.OnTouchListener onTouchListener) {
        this.f5736.setOnTouchListener(onTouchListener);
        this.f5735.setOnTouchListener(onTouchListener);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6230(String str) {
        this.f5742.get(str, new ImageLoader.ImageListener() {
            public void onErrorResponse(VolleyError volleyError) {
            }

            public void onResponse(ImageLoader.ImageContainer imageContainer, boolean z) {
                Bitmap bitmap = imageContainer.getBitmap();
                if (bitmap != null) {
                    VastVideoCloseButtonWidget.this.f5736.setImageBitmap(bitmap);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6231(String str) {
        if (this.f5735 != null) {
            this.f5735.setText(str);
        }
    }
}
