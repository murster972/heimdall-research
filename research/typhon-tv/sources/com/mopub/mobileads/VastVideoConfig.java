package com.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.mopub.common.MoPubBrowser;
import com.mopub.common.Preconditions;
import com.mopub.common.TyphoonApp;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Intents;
import com.mopub.common.util.Strings;
import com.mopub.exceptions.IntentNotResolvableException;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.network.TrackingRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class VastVideoConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private final ArrayList<VastAbsoluteProgressTracker> mAbsoluteTrackers = new ArrayList<>();
    private String mClickThroughUrl;
    private final ArrayList<VastTracker> mClickTrackers = new ArrayList<>();
    private final ArrayList<VastTracker> mCloseTrackers = new ArrayList<>();
    private final ArrayList<VastTracker> mCompleteTrackers = new ArrayList<>();
    private String mCustomCloseIconUrl;
    private String mCustomCtaText;
    private DeviceUtils.ForceOrientation mCustomForceOrientation = DeviceUtils.ForceOrientation.FORCE_LANDSCAPE;
    private String mCustomSkipText;
    private String mDiskMediaFileUrl;
    /* access modifiers changed from: private */
    public String mDspCreativeId;
    private final ArrayList<VastTracker> mErrorTrackers = new ArrayList<>();
    private final ArrayList<VastFractionalProgressTracker> mFractionalTrackers = new ArrayList<>();
    private final ArrayList<VastTracker> mImpressionTrackers = new ArrayList<>();
    private boolean mIsForceOrientationSet;
    private boolean mIsRewardedVideo = false;
    private VastCompanionAdConfig mLandscapeVastCompanionAdConfig;
    private String mNetworkMediaFileUrl;
    private final ArrayList<VastTracker> mPauseTrackers = new ArrayList<>();
    private VastCompanionAdConfig mPortraitVastCompanionAdConfig;
    private final ArrayList<VastTracker> mResumeTrackers = new ArrayList<>();
    private String mSkipOffset;
    private final ArrayList<VastTracker> mSkipTrackers = new ArrayList<>();
    private Map<String, VastCompanionAdConfig> mSocialActionsCompanionAds = new HashMap();
    private VastIconConfig mVastIconConfig;
    private VideoViewabilityTracker mVideoViewabilityTracker;

    /* renamed from: 连任  reason: contains not printable characters */
    private void m26791(List<String> list) {
        Preconditions.checkNotNull(list);
        if (hasCompanionAd()) {
            List<VastTracker> r0 = m26797(list);
            this.mLandscapeVastCompanionAdConfig.addClickTrackers(r0);
            this.mPortraitVastCompanionAdConfig.addClickTrackers(r0);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26792(List<String> list) {
        Preconditions.checkNotNull(list);
        addCompleteTrackers(m26797(list));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m26793(List<String> list) {
        Preconditions.checkNotNull(list);
        if (hasCompanionAd()) {
            List<VastTracker> r0 = m26797(list);
            this.mLandscapeVastCompanionAdConfig.addCreativeViewTrackers(r0);
            this.mPortraitVastCompanionAdConfig.addCreativeViewTrackers(r0);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26794(List<String> list) {
        Preconditions.checkNotNull(list);
        ArrayList arrayList = new ArrayList();
        for (String vastAbsoluteProgressTracker : list) {
            arrayList.add(new VastAbsoluteProgressTracker(vastAbsoluteProgressTracker, 0));
        }
        addAbsoluteTrackers(arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<String> m26796(String str, JSONArray jSONArray) {
        Preconditions.checkNotNull(jSONArray);
        if (str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            String optString = jSONArray.optString(i);
            if (optString != null) {
                arrayList.add(optString.replace(TyphoonApp.VIDEO_TRACKING_URL_MACRO, str));
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<VastTracker> m26797(List<String> list) {
        Preconditions.checkNotNull(list);
        ArrayList arrayList = new ArrayList();
        for (String vastTracker : list) {
            arrayList.add(new VastTracker(vastTracker));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26798(final Context context, int i, final Integer num) {
        Preconditions.checkNotNull(context, "context cannot be null");
        TrackingRequest.makeVastTrackingHttpRequest(this.mClickTrackers, (VastErrorCode) null, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
        if (!TextUtils.isEmpty(this.mClickThroughUrl)) {
            new UrlHandler.Builder().withDspCreativeId(this.mDspCreativeId).withSupportedUrlActions(UrlAction.IGNORE_ABOUT_SCHEME, UrlAction.OPEN_APP_MARKET, UrlAction.OPEN_NATIVE_BROWSER, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK).withResultActions(new UrlHandler.ResultActions() {
                public void urlHandlingFailed(String str, UrlAction urlAction) {
                }

                public void urlHandlingSucceeded(String str, UrlAction urlAction) {
                    if (urlAction == UrlAction.OPEN_IN_APP_BROWSER) {
                        Bundle bundle = new Bundle();
                        bundle.putString(MoPubBrowser.DESTINATION_URL_KEY, str);
                        bundle.putString(MoPubBrowser.DSP_CREATIVE_ID, VastVideoConfig.this.mDspCreativeId);
                        Class<MoPubBrowser> cls = MoPubBrowser.class;
                        Intent startActivityIntent = Intents.getStartActivityIntent(context, cls, bundle);
                        try {
                            if (context instanceof Activity) {
                                Preconditions.checkNotNull(num);
                                ((Activity) context).startActivityForResult(startActivityIntent, num.intValue());
                                return;
                            }
                            Intents.startActivity(context, startActivityIntent);
                        } catch (ActivityNotFoundException e) {
                            MoPubLog.d("Activity " + cls.getName() + " not found. Did you declare it in your AndroidManifest.xml?");
                        } catch (IntentNotResolvableException e2) {
                            MoPubLog.d("Activity " + cls.getName() + " not found. Did you declare it in your AndroidManifest.xml?");
                        }
                    }
                }
            }).withoutMoPubBrowser().build().handleUrl(context, this.mClickThroughUrl);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26799(List<String> list, float f) {
        Preconditions.checkNotNull(list);
        ArrayList arrayList = new ArrayList();
        for (String vastFractionalProgressTracker : list) {
            arrayList.add(new VastFractionalProgressTracker(vastFractionalProgressTracker, f));
        }
        addFractionalTrackers(arrayList);
    }

    public void addAbsoluteTrackers(List<VastAbsoluteProgressTracker> list) {
        Preconditions.checkNotNull(list, "absoluteTrackers cannot be null");
        this.mAbsoluteTrackers.addAll(list);
        Collections.sort(this.mAbsoluteTrackers);
    }

    public void addClickTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "clickTrackers cannot be null");
        this.mClickTrackers.addAll(list);
    }

    public void addCloseTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "closeTrackers cannot be null");
        this.mCloseTrackers.addAll(list);
    }

    public void addCompleteTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "completeTrackers cannot be null");
        this.mCompleteTrackers.addAll(list);
    }

    public void addErrorTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "errorTrackers cannot be null");
        this.mErrorTrackers.addAll(list);
    }

    public void addFractionalTrackers(List<VastFractionalProgressTracker> list) {
        Preconditions.checkNotNull(list, "fractionalTrackers cannot be null");
        this.mFractionalTrackers.addAll(list);
        Collections.sort(this.mFractionalTrackers);
    }

    public void addImpressionTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "impressionTrackers cannot be null");
        this.mImpressionTrackers.addAll(list);
    }

    public void addPauseTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "pauseTrackers cannot be null");
        this.mPauseTrackers.addAll(list);
    }

    public void addResumeTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "resumeTrackers cannot be null");
        this.mResumeTrackers.addAll(list);
    }

    public void addSkipTrackers(List<VastTracker> list) {
        Preconditions.checkNotNull(list, "skipTrackers cannot be null");
        this.mSkipTrackers.addAll(list);
    }

    public void addVideoTrackers(JSONObject jSONObject) {
        if (jSONObject != null) {
            JSONArray optJSONArray = jSONObject.optJSONArray(TyphoonApp.VIDEO_TRACKING_URLS_KEY);
            JSONArray optJSONArray2 = jSONObject.optJSONArray(TyphoonApp.VIDEO_TRACKING_EVENTS_KEY);
            if (optJSONArray != null && optJSONArray2 != null) {
                for (int i = 0; i < optJSONArray2.length(); i++) {
                    String optString = optJSONArray2.optString(i);
                    List<String> r5 = m26796(optString, optJSONArray);
                    VideoTrackingEvent fromString = VideoTrackingEvent.fromString(optString);
                    if (!(optString == null || r5 == null)) {
                        switch (fromString) {
                            case START:
                                m26794(r5);
                                break;
                            case FIRST_QUARTILE:
                                m26799(r5, 0.25f);
                                break;
                            case MIDPOINT:
                                m26799(r5, 0.5f);
                                break;
                            case THIRD_QUARTILE:
                                m26799(r5, 0.75f);
                                break;
                            case COMPLETE:
                                m26792(r5);
                                break;
                            case COMPANION_AD_VIEW:
                                m26793(r5);
                                break;
                            case COMPANION_AD_CLICK:
                                m26791(r5);
                                break;
                        }
                    }
                }
            }
        }
    }

    public ArrayList<VastAbsoluteProgressTracker> getAbsoluteTrackers() {
        return this.mAbsoluteTrackers;
    }

    public String getClickThroughUrl() {
        return this.mClickThroughUrl;
    }

    public List<VastTracker> getClickTrackers() {
        return this.mClickTrackers;
    }

    public List<VastTracker> getCloseTrackers() {
        return this.mCloseTrackers;
    }

    public List<VastTracker> getCompleteTrackers() {
        return this.mCompleteTrackers;
    }

    public String getCustomCloseIconUrl() {
        return this.mCustomCloseIconUrl;
    }

    public String getCustomCtaText() {
        return this.mCustomCtaText;
    }

    public DeviceUtils.ForceOrientation getCustomForceOrientation() {
        return this.mCustomForceOrientation;
    }

    public String getCustomSkipText() {
        return this.mCustomSkipText;
    }

    public String getDiskMediaFileUrl() {
        return this.mDiskMediaFileUrl;
    }

    public String getDspCreativeId() {
        return this.mDspCreativeId;
    }

    public List<VastTracker> getErrorTrackers() {
        return this.mErrorTrackers;
    }

    public ArrayList<VastFractionalProgressTracker> getFractionalTrackers() {
        return this.mFractionalTrackers;
    }

    public List<VastTracker> getImpressionTrackers() {
        return this.mImpressionTrackers;
    }

    public String getNetworkMediaFileUrl() {
        return this.mNetworkMediaFileUrl;
    }

    public List<VastTracker> getPauseTrackers() {
        return this.mPauseTrackers;
    }

    public int getRemainingProgressTrackerCount() {
        return getUntriggeredTrackersBefore(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT).size();
    }

    public List<VastTracker> getResumeTrackers() {
        return this.mResumeTrackers;
    }

    public Integer getSkipOffsetMillis(int i) {
        Integer valueOf;
        if (this.mSkipOffset != null) {
            try {
                if (Strings.isAbsoluteTracker(this.mSkipOffset)) {
                    valueOf = Strings.parseAbsoluteOffset(this.mSkipOffset);
                } else if (Strings.isPercentageTracker(this.mSkipOffset)) {
                    valueOf = Integer.valueOf(Math.round(((float) i) * (Float.parseFloat(this.mSkipOffset.replace("%", "")) / 100.0f)));
                } else {
                    MoPubLog.d(String.format("Invalid VAST skipoffset format: %s", new Object[]{this.mSkipOffset}));
                    return null;
                }
                if (valueOf != null) {
                    return valueOf.intValue() < i ? valueOf : Integer.valueOf(i);
                }
            } catch (NumberFormatException e) {
            }
        }
        return null;
    }

    public String getSkipOffsetString() {
        return this.mSkipOffset;
    }

    public List<VastTracker> getSkipTrackers() {
        return this.mSkipTrackers;
    }

    public Map<String, VastCompanionAdConfig> getSocialActionsCompanionAds() {
        return this.mSocialActionsCompanionAds;
    }

    public List<VastTracker> getUntriggeredTrackersBefore(int i, int i2) {
        if (!Preconditions.NoThrow.checkArgument(i2 > 0) || i < 0) {
            return Collections.emptyList();
        }
        float f = ((float) i) / ((float) i2);
        ArrayList arrayList = new ArrayList();
        VastAbsoluteProgressTracker vastAbsoluteProgressTracker = new VastAbsoluteProgressTracker("", i);
        int size = this.mAbsoluteTrackers.size();
        for (int i3 = 0; i3 < size; i3++) {
            VastAbsoluteProgressTracker vastAbsoluteProgressTracker2 = this.mAbsoluteTrackers.get(i3);
            if (vastAbsoluteProgressTracker2.compareTo(vastAbsoluteProgressTracker) > 0) {
                break;
            }
            if (!vastAbsoluteProgressTracker2.isTracked()) {
                arrayList.add(vastAbsoluteProgressTracker2);
            }
        }
        VastFractionalProgressTracker vastFractionalProgressTracker = new VastFractionalProgressTracker("", f);
        int size2 = this.mFractionalTrackers.size();
        for (int i4 = 0; i4 < size2; i4++) {
            VastFractionalProgressTracker vastFractionalProgressTracker2 = this.mFractionalTrackers.get(i4);
            if (vastFractionalProgressTracker2.compareTo(vastFractionalProgressTracker) > 0) {
                return arrayList;
            }
            if (!vastFractionalProgressTracker2.isTracked()) {
                arrayList.add(vastFractionalProgressTracker2);
            }
        }
        return arrayList;
    }

    public VastCompanionAdConfig getVastCompanionAd(int i) {
        switch (i) {
            case 1:
                return this.mPortraitVastCompanionAdConfig;
            case 2:
                return this.mLandscapeVastCompanionAdConfig;
            default:
                return this.mLandscapeVastCompanionAdConfig;
        }
    }

    public VastIconConfig getVastIconConfig() {
        return this.mVastIconConfig;
    }

    public VideoViewabilityTracker getVideoViewabilityTracker() {
        return this.mVideoViewabilityTracker;
    }

    public void handleClickForResult(Activity activity, int i, int i2) {
        m26798(activity, i, Integer.valueOf(i2));
    }

    public void handleClickWithoutResult(Context context, int i) {
        m26798(context.getApplicationContext(), i, (Integer) null);
    }

    public void handleClose(Context context, int i) {
        Preconditions.checkNotNull(context, "context cannot be null");
        TrackingRequest.makeVastTrackingHttpRequest(this.mCloseTrackers, (VastErrorCode) null, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
        TrackingRequest.makeVastTrackingHttpRequest(this.mSkipTrackers, (VastErrorCode) null, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
    }

    public void handleComplete(Context context, int i) {
        Preconditions.checkNotNull(context, "context cannot be null");
        TrackingRequest.makeVastTrackingHttpRequest(this.mCompleteTrackers, (VastErrorCode) null, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
    }

    public void handleError(Context context, VastErrorCode vastErrorCode, int i) {
        Preconditions.checkNotNull(context, "context cannot be null");
        TrackingRequest.makeVastTrackingHttpRequest(this.mErrorTrackers, vastErrorCode, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
    }

    public void handleImpression(Context context, int i) {
        Preconditions.checkNotNull(context, "context cannot be null");
        TrackingRequest.makeVastTrackingHttpRequest(this.mImpressionTrackers, (VastErrorCode) null, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
    }

    public void handlePause(Context context, int i) {
        Preconditions.checkNotNull(context, "context cannot be null");
        TrackingRequest.makeVastTrackingHttpRequest(this.mPauseTrackers, (VastErrorCode) null, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
    }

    public void handleResume(Context context, int i) {
        Preconditions.checkNotNull(context, "context cannot be null");
        TrackingRequest.makeVastTrackingHttpRequest(this.mResumeTrackers, (VastErrorCode) null, Integer.valueOf(i), this.mNetworkMediaFileUrl, context);
    }

    public boolean hasCompanionAd() {
        return (this.mLandscapeVastCompanionAdConfig == null || this.mPortraitVastCompanionAdConfig == null) ? false : true;
    }

    public boolean isCustomForceOrientationSet() {
        return this.mIsForceOrientationSet;
    }

    public boolean isRewardedVideo() {
        return this.mIsRewardedVideo;
    }

    public void setClickThroughUrl(String str) {
        this.mClickThroughUrl = str;
    }

    public void setCustomCloseIconUrl(String str) {
        if (str != null) {
            this.mCustomCloseIconUrl = str;
        }
    }

    public void setCustomCtaText(String str) {
        if (str != null) {
            this.mCustomCtaText = str;
        }
    }

    public void setCustomForceOrientation(DeviceUtils.ForceOrientation forceOrientation) {
        if (forceOrientation != null && forceOrientation != DeviceUtils.ForceOrientation.UNDEFINED) {
            this.mCustomForceOrientation = forceOrientation;
            this.mIsForceOrientationSet = true;
        }
    }

    public void setCustomSkipText(String str) {
        if (str != null) {
            this.mCustomSkipText = str;
        }
    }

    public void setDiskMediaFileUrl(String str) {
        this.mDiskMediaFileUrl = str;
    }

    public void setDspCreativeId(String str) {
        this.mDspCreativeId = str;
    }

    public void setIsRewardedVideo(boolean z) {
        this.mIsRewardedVideo = z;
    }

    public void setNetworkMediaFileUrl(String str) {
        this.mNetworkMediaFileUrl = str;
    }

    public void setSkipOffset(String str) {
        if (str != null) {
            this.mSkipOffset = str;
        }
    }

    public void setSocialActionsCompanionAds(Map<String, VastCompanionAdConfig> map) {
        this.mSocialActionsCompanionAds = map;
    }

    public void setVastCompanionAd(VastCompanionAdConfig vastCompanionAdConfig, VastCompanionAdConfig vastCompanionAdConfig2) {
        this.mLandscapeVastCompanionAdConfig = vastCompanionAdConfig;
        this.mPortraitVastCompanionAdConfig = vastCompanionAdConfig2;
    }

    public void setVastIconConfig(VastIconConfig vastIconConfig) {
        this.mVastIconConfig = vastIconConfig;
    }

    public void setVideoViewabilityTracker(VideoViewabilityTracker videoViewabilityTracker) {
        if (videoViewabilityTracker != null) {
            this.mVideoViewabilityTracker = videoViewabilityTracker;
        }
    }
}
