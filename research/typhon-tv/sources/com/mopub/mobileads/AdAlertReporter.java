package com.mopub.mobileads;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;
import com.mopub.common.AdReport;
import com.mopub.common.util.DateAndTime;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class AdAlertReporter {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f20689;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f20690;

    /* renamed from: 靐  reason: contains not printable characters */
    private final View f20691;

    /* renamed from: 麤  reason: contains not printable characters */
    private Intent f20692;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Context f20693;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f20694 = new SimpleDateFormat("M/d/yy hh:mm:ss a z", Locale.US).format(DateAndTime.now());

    public AdAlertReporter(Context context, View view, AdReport adReport) {
        this.f20691 = view;
        this.f20693 = context;
        m26619();
        String r2 = m26618(m26616());
        this.f20690 = "";
        this.f20689 = "";
        if (adReport != null) {
            this.f20690 = adReport.toString();
            this.f20689 = adReport.getResponseString();
        }
        m26617();
        m26620(this.f20690, this.f20689, r2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Bitmap m26616() {
        if (this.f20691 == null || this.f20691.getRootView() == null) {
            return null;
        }
        View rootView = this.f20691.getRootView();
        boolean isDrawingCacheEnabled = rootView.isDrawingCacheEnabled();
        rootView.setDrawingCacheEnabled(true);
        Bitmap drawingCache = rootView.getDrawingCache();
        if (drawingCache == null) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(drawingCache);
        rootView.setDrawingCacheEnabled(isDrawingCacheEnabled);
        return createBitmap;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26617() {
        this.f20692.putExtra("android.intent.extra.SUBJECT", "New creative violation report - " + this.f20694);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m26618(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 25, byteArrayOutputStream);
            return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        } catch (Exception e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26619() {
        this.f20692 = new Intent("android.intent.action.SENDTO");
        this.f20692.setData(Uri.parse("mailto:creative-review@mopub.com"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26620(String... strArr) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strArr.length; i++) {
            sb.append(strArr[i]);
            if (i != strArr.length - 1) {
                sb.append("\n=================\n");
            }
        }
        this.f20692.putExtra("android.intent.extra.TEXT", sb.toString());
    }

    public void send() {
        try {
            Intents.startActivity(this.f20693, this.f20692);
        } catch (IntentNotResolvableException e) {
            Toast makeText = Toast.makeText(this.f20693, "No email client available", 0);
        }
    }
}
