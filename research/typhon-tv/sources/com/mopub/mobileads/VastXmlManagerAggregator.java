package com.mopub.mobileads;

import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.mobileads.VastResource;
import com.mopub.network.Networking;
import com.mopub.network.TrackingRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class VastXmlManagerAggregator extends AsyncTask<String, Void, VastVideoConfig> {
    public static final String ADS_BY_AD_SLOT_ID = "adsBy";
    public static final String SOCIAL_ACTIONS_AD_SLOT_ID = "socialActions";

    /* renamed from: 龘  reason: contains not printable characters */
    private static final List<String> f20954 = Arrays.asList(new String[]{MimeTypes.VIDEO_MP4, MimeTypes.VIDEO_H263});

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f20955;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Context f20956;

    /* renamed from: 靐  reason: contains not printable characters */
    private final WeakReference<VastXmlManagerAggregatorListener> f20957;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f20958;

    /* renamed from: 齉  reason: contains not printable characters */
    private final double f20959;

    enum CompanionOrientation {
        LANDSCAPE,
        PORTRAIT
    }

    interface VastXmlManagerAggregatorListener {
        void onAggregationComplete(VastVideoConfig vastVideoConfig);
    }

    VastXmlManagerAggregator(VastXmlManagerAggregatorListener vastXmlManagerAggregatorListener, double d, int i, Context context) {
        Preconditions.checkNotNull(vastXmlManagerAggregatorListener);
        Preconditions.checkNotNull(context);
        this.f20957 = new WeakReference<>(vastXmlManagerAggregatorListener);
        this.f20959 = d;
        this.f20958 = i;
        this.f20956 = context.getApplicationContext();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0031  */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m26877(java.lang.String r6) throws java.io.IOException {
        /*
            r5 = this;
            com.mopub.common.Preconditions.checkNotNull(r6)
            int r3 = r5.f20955
            r4 = 10
            if (r3 >= r4) goto L_0x0035
            int r3 = r5.f20955
            int r3 = r3 + 1
            r5.f20955 = r3
            r2 = 0
            r0 = 0
            java.net.HttpURLConnection r2 = com.mopub.common.MoPubHttpUrlConnection.getHttpUrlConnection(r6)     // Catch:{ all -> 0x002b }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x002b }
            java.io.InputStream r3 = r2.getInputStream()     // Catch:{ all -> 0x002b }
            r1.<init>(r3)     // Catch:{ all -> 0x002b }
            java.lang.String r3 = com.mopub.common.util.Strings.fromStream(r1)     // Catch:{ all -> 0x0037 }
            com.mopub.common.util.Streams.closeStream(r1)
            if (r2 == 0) goto L_0x002a
            r2.disconnect()
        L_0x002a:
            return r3
        L_0x002b:
            r3 = move-exception
        L_0x002c:
            com.mopub.common.util.Streams.closeStream(r0)
            if (r2 == 0) goto L_0x0034
            r2.disconnect()
        L_0x0034:
            throw r3
        L_0x0035:
            r3 = 0
            goto L_0x002a
        L_0x0037:
            r3 = move-exception
            r0 = r1
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.VastXmlManagerAggregator.m26877(java.lang.String):java.lang.String");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private double m26878(int i, int i2) {
        return (70.0d * Math.abs(Math.log((((double) i) / ((double) i2)) / this.f20959))) + (30.0d * Math.abs(Math.log(((double) (i * i2)) / ((double) this.f20958))));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private VastVideoConfig m26879(VastInLineXmlManager vastInLineXmlManager, List<VastTracker> list) {
        Preconditions.checkNotNull(vastInLineXmlManager);
        Preconditions.checkNotNull(list);
        for (VastLinearXmlManager next : vastInLineXmlManager.m26727()) {
            String r0 = m26892(next.m26768());
            if (r0 != null) {
                VastVideoConfig vastVideoConfig = new VastVideoConfig();
                vastVideoConfig.addImpressionTrackers(vastInLineXmlManager.m26728());
                m26882(next, vastVideoConfig);
                vastVideoConfig.setClickThroughUrl(next.m26764());
                vastVideoConfig.setNetworkMediaFileUrl(r0);
                List<VastCompanionAdXmlManager> r1 = vastInLineXmlManager.m26726();
                vastVideoConfig.setVastCompanionAd(m26889(r1, CompanionOrientation.LANDSCAPE), m26889(r1, CompanionOrientation.PORTRAIT));
                vastVideoConfig.setSocialActionsCompanionAds(m26886(r1));
                list.addAll(vastInLineXmlManager.m26725());
                vastVideoConfig.addErrorTrackers(list);
                m26881((VastBaseInLineWrapperXmlManager) vastInLineXmlManager, vastVideoConfig);
                return vastVideoConfig;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m26880(VastWrapperXmlManager vastWrapperXmlManager, List<VastTracker> list) {
        String r1 = vastWrapperXmlManager.m26868();
        if (r1 == null) {
            return null;
        }
        try {
            return m26877(r1);
        } catch (Exception e) {
            if (list.isEmpty()) {
                return null;
            }
            TrackingRequest.makeVastTrackingHttpRequest(list, VastErrorCode.WRAPPER_TIMEOUT, (Integer) null, (String) null, this.f20956);
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26881(VastBaseInLineWrapperXmlManager vastBaseInLineWrapperXmlManager, VastVideoConfig vastVideoConfig) {
        VastExtensionParentXmlManager r0;
        Preconditions.checkNotNull(vastBaseInLineWrapperXmlManager);
        Preconditions.checkNotNull(vastVideoConfig);
        if (vastVideoConfig.getVideoViewabilityTracker() == null && (r0 = vastBaseInLineWrapperXmlManager.m26724()) != null) {
            for (VastExtensionXmlManager next : r0.m26740()) {
                if ("MoPub".equals(next.m26741())) {
                    vastVideoConfig.setVideoViewabilityTracker(next.m26742());
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26882(VastLinearXmlManager vastLinearXmlManager, VastVideoConfig vastVideoConfig) {
        Preconditions.checkNotNull(vastLinearXmlManager, "linearXmlManager cannot be null");
        Preconditions.checkNotNull(vastVideoConfig, "vastVideoConfig cannot be null");
        vastVideoConfig.addAbsoluteTrackers(vastLinearXmlManager.m26770());
        vastVideoConfig.addFractionalTrackers(vastLinearXmlManager.m26773());
        vastVideoConfig.addPauseTrackers(vastLinearXmlManager.m26771());
        vastVideoConfig.addResumeTrackers(vastLinearXmlManager.m26769());
        vastVideoConfig.addCompleteTrackers(vastLinearXmlManager.m26772());
        vastVideoConfig.addCloseTrackers(vastLinearXmlManager.m26762());
        vastVideoConfig.addSkipTrackers(vastLinearXmlManager.m26763());
        vastVideoConfig.addClickTrackers(vastLinearXmlManager.m26766());
        if (vastVideoConfig.getSkipOffsetString() == null) {
            vastVideoConfig.setSkipOffset(vastLinearXmlManager.m26767());
        }
        if (vastVideoConfig.getVastIconConfig() == null) {
            vastVideoConfig.setVastIconConfig(m26887(vastLinearXmlManager.m26765()));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26883(VastXmlManager vastXmlManager, VastVideoConfig vastVideoConfig) {
        Preconditions.checkNotNull(vastXmlManager, "xmlManager cannot be null");
        Preconditions.checkNotNull(vastVideoConfig, "vastVideoConfig cannot be null");
        vastVideoConfig.addImpressionTrackers(vastXmlManager.m26874());
        if (vastVideoConfig.getCustomCtaText() == null) {
            vastVideoConfig.setCustomCtaText(vastXmlManager.m26873());
        }
        if (vastVideoConfig.getCustomSkipText() == null) {
            vastVideoConfig.setCustomSkipText(vastXmlManager.m26871());
        }
        if (vastVideoConfig.getCustomCloseIconUrl() == null) {
            vastVideoConfig.setCustomCloseIconUrl(vastXmlManager.m26869());
        }
        if (!vastVideoConfig.isCustomForceOrientationSet()) {
            vastVideoConfig.setCustomForceOrientation(vastXmlManager.m26870());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m26884(String str) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        try {
            return Integer.parseInt(str) < 2;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26885(List<VastAdXmlManager> list, VastXmlManager vastXmlManager, Context context) {
        if (!list.isEmpty() || vastXmlManager.m26872() == null) {
            return false;
        }
        TrackingRequest.makeVastTrackingHttpRequest(Collections.singletonList(vastXmlManager.m26872()), this.f20955 > 0 ? VastErrorCode.NO_ADS_VAST_RESPONSE : VastErrorCode.UNDEFINED_ERROR, (Integer) null, (String) null, context);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        VastXmlManagerAggregatorListener vastXmlManagerAggregatorListener = (VastXmlManagerAggregatorListener) this.f20957.get();
        if (vastXmlManagerAggregatorListener != null) {
            vastXmlManagerAggregatorListener.onAggregationComplete((VastVideoConfig) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        Networking.getUserAgent(this.f20956);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public Map<String, VastCompanionAdConfig> m26886(List<VastCompanionAdXmlManager> list) {
        Preconditions.checkNotNull(list, "managers cannot be null");
        HashMap hashMap = new HashMap();
        for (VastCompanionAdXmlManager next : list) {
            Integer r11 = next.m26738();
            Integer r9 = next.m26735();
            if (!(r11 == null || r9 == null)) {
                String r7 = next.m26737();
                if (!ADS_BY_AD_SLOT_ID.equals(r7) ? !(!SOCIAL_ACTIONS_AD_SLOT_ID.equals(r7) || r11.intValue() < 50 || r11.intValue() > 150 || r9.intValue() < 10 || r9.intValue() > 50) : !(r11.intValue() < 25 || r11.intValue() > 75 || r9.intValue() < 10 || r9.intValue() > 50)) {
                    VastResource r3 = VastResource.m26784(next.m26736(), VastResource.Type.HTML_RESOURCE, r11.intValue(), r9.intValue());
                    if (r3 != null) {
                        hashMap.put(r7, new VastCompanionAdConfig(r11.intValue(), r9.intValue(), r3, next.m26734(), next.m26731(), next.m26732()));
                    }
                }
            }
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 齉  reason: contains not printable characters */
    public VastIconConfig m26887(List<VastIconXmlManager> list) {
        VastResource r6;
        Preconditions.checkNotNull(list, "managers cannot be null");
        ArrayList<VastIconXmlManager> arrayList = new ArrayList<>(list);
        for (VastResource.Type type : VastResource.Type.values()) {
            for (VastIconXmlManager vastIconXmlManager : arrayList) {
                Integer r14 = vastIconXmlManager.m26758();
                Integer r10 = vastIconXmlManager.m26755();
                if (r14 != null && r14.intValue() > 0 && r14.intValue() <= 300 && r10 != null && r10.intValue() > 0 && r10.intValue() <= 300 && (r6 = VastResource.m26784(vastIconXmlManager.m26754(), type, r14.intValue(), r10.intValue())) != null) {
                    return new VastIconConfig(vastIconXmlManager.m26758().intValue(), vastIconXmlManager.m26755().intValue(), vastIconXmlManager.m26757(), vastIconXmlManager.m26756(), r6, vastIconXmlManager.m26751(), vastIconXmlManager.m26752(), vastIconXmlManager.m26753());
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public Point m26888(int i, int i2, VastResource.Type type, CompanionOrientation companionOrientation) {
        int min;
        int max;
        Point point = new Point(i, i2);
        Display defaultDisplay = ((WindowManager) this.f20956.getSystemService("window")).getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int dipsToIntPixels = Dips.dipsToIntPixels((float) i, this.f20956);
        int dipsToIntPixels2 = Dips.dipsToIntPixels((float) i2, this.f20956);
        if (CompanionOrientation.LANDSCAPE == companionOrientation) {
            min = Math.max(width, height);
            max = Math.min(width, height);
        } else {
            min = Math.min(width, height);
            max = Math.max(width, height);
        }
        if (dipsToIntPixels <= min - 16 && dipsToIntPixels2 <= max - 16) {
            return point;
        }
        Point point2 = new Point();
        if (VastResource.Type.HTML_RESOURCE == type) {
            point2.x = Math.min(min, dipsToIntPixels);
            point2.y = Math.min(max, dipsToIntPixels2);
        } else {
            float f = ((float) dipsToIntPixels) / ((float) min);
            float f2 = ((float) dipsToIntPixels2) / ((float) max);
            if (f >= f2) {
                point2.x = min;
                point2.y = (int) (((float) dipsToIntPixels2) / f);
            } else {
                point2.x = (int) (((float) dipsToIntPixels) / f2);
                point2.y = max;
            }
        }
        point2.x -= 16;
        point2.y -= 16;
        if (point2.x < 0 || point2.y < 0) {
            return point;
        }
        point2.x = Dips.pixelsToIntDips((float) point2.x, this.f20956);
        point2.y = Dips.pixelsToIntDips((float) point2.y, this.f20956);
        return point2;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public VastCompanionAdConfig m26889(List<VastCompanionAdXmlManager> list, CompanionOrientation companionOrientation) {
        double r16;
        Preconditions.checkNotNull(list, "managers cannot be null");
        Preconditions.checkNotNull(companionOrientation, "orientation cannot be null");
        ArrayList<VastCompanionAdXmlManager> arrayList = new ArrayList<>(list);
        double d = Double.POSITIVE_INFINITY;
        VastCompanionAdXmlManager vastCompanionAdXmlManager = null;
        VastResource vastResource = null;
        Point point = null;
        for (VastResource.Type type : VastResource.Type.values()) {
            for (VastCompanionAdXmlManager vastCompanionAdXmlManager2 : arrayList) {
                Integer r23 = vastCompanionAdXmlManager2.m26738();
                Integer r19 = vastCompanionAdXmlManager2.m26735();
                if (r23 != null && r23.intValue() >= 300 && r19 != null && r19.intValue() >= 250) {
                    Point r22 = m26888(r23.intValue(), r19.intValue(), type, companionOrientation);
                    VastResource r21 = VastResource.m26784(vastCompanionAdXmlManager2.m26736(), type, r22.x, r22.y);
                    if (r21 != null) {
                        if (CompanionOrientation.PORTRAIT == companionOrientation) {
                            r16 = m26878(r19.intValue(), r23.intValue());
                        } else {
                            r16 = m26878(r23.intValue(), r19.intValue());
                        }
                        if (r16 < d) {
                            d = r16;
                            vastCompanionAdXmlManager = vastCompanionAdXmlManager2;
                            vastResource = r21;
                            point = r22;
                        }
                    }
                }
            }
            if (vastCompanionAdXmlManager != null) {
                break;
            }
        }
        if (vastCompanionAdXmlManager != null) {
            return new VastCompanionAdConfig(point.x, point.y, vastResource, vastCompanionAdXmlManager.m26734(), vastCompanionAdXmlManager.m26731(), vastCompanionAdXmlManager.m26732());
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public VastVideoConfig m26890(String str, List<VastTracker> list) {
        VastVideoConfig r16;
        VastVideoConfig r162;
        Preconditions.checkNotNull(str, "vastXml cannot be null");
        Preconditions.checkNotNull(list, "errorTrackers cannot be null");
        VastXmlManager vastXmlManager = new VastXmlManager();
        try {
            vastXmlManager.m26876(str);
            List<VastAdXmlManager> r13 = vastXmlManager.m26875();
            if (m26885(r13, vastXmlManager, this.f20956)) {
                return null;
            }
            for (VastAdXmlManager next : r13) {
                if (m26884(next.m26722())) {
                    VastInLineXmlManager r14 = next.m26723();
                    if (r14 == null || (r162 = m26879(r14, list)) == null) {
                        VastWrapperXmlManager r17 = next.m26721();
                        if (r17 != null) {
                            ArrayList arrayList = new ArrayList(list);
                            arrayList.addAll(r17.m26725());
                            String r15 = m26880(r17, (List<VastTracker>) arrayList);
                            if (!(r15 == null || (r16 = m26890(r15, (List<VastTracker>) arrayList)) == null)) {
                                r16.addImpressionTrackers(r17.m26728());
                                for (VastLinearXmlManager r9 : r17.m26727()) {
                                    m26882(r9, r16);
                                }
                                m26881((VastBaseInLineWrapperXmlManager) r17, r16);
                                List<VastCompanionAdXmlManager> r6 = r17.m26726();
                                if (!r16.hasCompanionAd()) {
                                    r16.setVastCompanionAd(m26889(r6, CompanionOrientation.LANDSCAPE), m26889(r6, CompanionOrientation.PORTRAIT));
                                } else {
                                    VastCompanionAdConfig vastCompanionAd = r16.getVastCompanionAd(2);
                                    VastCompanionAdConfig vastCompanionAd2 = r16.getVastCompanionAd(1);
                                    if (!(vastCompanionAd == null || vastCompanionAd2 == null)) {
                                        for (VastCompanionAdXmlManager next2 : r6) {
                                            if (!next2.m26733()) {
                                                vastCompanionAd.addClickTrackers(next2.m26731());
                                                vastCompanionAd.addCreativeViewTrackers(next2.m26732());
                                                vastCompanionAd2.addClickTrackers(next2.m26731());
                                                vastCompanionAd2.addCreativeViewTrackers(next2.m26732());
                                            }
                                        }
                                    }
                                }
                                if (r16.getSocialActionsCompanionAds().isEmpty()) {
                                    r16.setSocialActionsCompanionAds(m26886(r6));
                                }
                                m26883(vastXmlManager, r16);
                                return r16;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        m26883(vastXmlManager, r162);
                        return r162;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            TrackingRequest.makeVastTrackingHttpRequest(list, VastErrorCode.XML_PARSING_ERROR, (Integer) null, (String) null, this.f20956);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public VastVideoConfig doInBackground(String... strArr) {
        if (strArr == null || strArr.length == 0 || strArr[0] == null) {
            return null;
        }
        try {
            return m26890(strArr[0], (List<VastTracker>) new ArrayList());
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public String m26892(List<VastMediaXmlManager> list) {
        Preconditions.checkNotNull(list, "managers cannot be null");
        double d = Double.POSITIVE_INFINITY;
        String str = null;
        Iterator it2 = new ArrayList(list).iterator();
        while (it2.hasNext()) {
            VastMediaXmlManager vastMediaXmlManager = (VastMediaXmlManager) it2.next();
            String r6 = vastMediaXmlManager.m26782();
            String r7 = vastMediaXmlManager.m26781();
            if (!f20954.contains(r6) || r7 == null) {
                it2.remove();
            } else {
                Integer r8 = vastMediaXmlManager.m26783();
                Integer r1 = vastMediaXmlManager.m26780();
                if (r8 != null && r8.intValue() > 0 && r1 != null && r1.intValue() > 0) {
                    double r4 = m26878(r8.intValue(), r1.intValue());
                    if (r4 < d) {
                        d = r4;
                        str = r7;
                    }
                }
            }
        }
        return str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void onPostExecute(VastVideoConfig vastVideoConfig) {
        VastXmlManagerAggregatorListener vastXmlManagerAggregatorListener = (VastXmlManagerAggregatorListener) this.f20957.get();
        if (vastXmlManagerAggregatorListener != null) {
            vastXmlManagerAggregatorListener.onAggregationComplete(vastVideoConfig);
        }
    }
}
