package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.FrameLayout;
import com.Pinkamena;
import com.flurry.android.FlurryAgentListener;
import com.flurry.android.ads.FlurryAdBanner;
import com.flurry.android.ads.FlurryAdBannerListener;
import com.flurry.android.ads.FlurryAdErrorType;
import com.mopub.mobileads.CustomEventBanner;
import java.util.Map;

public class FlurryCustomEventBanner extends CustomEventBanner {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f20757 = FlurryCustomEventBanner.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public FlurryAdBanner f20758;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f20759;

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f20760;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public FrameLayout f20761;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public CustomEventBanner.CustomEventBannerListener f20762;

    /* renamed from: com.mopub.mobileads.FlurryCustomEventBanner$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f20763 = new int[FlurryAdErrorType.values().length];

        static {
            try {
                f20763[FlurryAdErrorType.FETCH.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f20763[FlurryAdErrorType.RENDER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f20763[FlurryAdErrorType.CLICK.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private class FlurryMopubBannerListener implements FlurryAdBannerListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f20764;

        private FlurryMopubBannerListener() {
            this.f20764 = getClass().getSimpleName();
        }

        /* synthetic */ FlurryMopubBannerListener(FlurryCustomEventBanner flurryCustomEventBanner, AnonymousClass1 r2) {
            this();
        }

        public void onAppExit(FlurryAdBanner flurryAdBanner) {
        }

        public void onClicked(FlurryAdBanner flurryAdBanner) {
            if (FlurryCustomEventBanner.this.f20762 != null) {
                FlurryCustomEventBanner.this.f20762.onBannerClicked();
            }
        }

        public void onCloseFullscreen(FlurryAdBanner flurryAdBanner) {
            if (FlurryCustomEventBanner.this.f20762 != null) {
                FlurryCustomEventBanner.this.f20762.onBannerCollapsed();
            }
        }

        public void onError(FlurryAdBanner flurryAdBanner, FlurryAdErrorType flurryAdErrorType, int i) {
            Log.d(this.f20764, String.format("onError: Flurry banner ad not available. Error type: %s. Error code: %s", new Object[]{flurryAdErrorType.toString(), Integer.valueOf(i)}));
            if (FlurryCustomEventBanner.this.f20762 != null) {
                switch (AnonymousClass1.f20763[flurryAdErrorType.ordinal()]) {
                    case 1:
                        FlurryCustomEventBanner.this.f20762.onBannerFailed(MoPubErrorCode.NETWORK_NO_FILL);
                        return;
                    case 2:
                        FlurryCustomEventBanner.this.f20762.onBannerFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
                        return;
                    case 3:
                        return;
                    default:
                        FlurryCustomEventBanner.this.f20762.onBannerFailed(MoPubErrorCode.UNSPECIFIED);
                        return;
                }
            }
        }

        public void onFetched(FlurryAdBanner flurryAdBanner) {
            if (FlurryCustomEventBanner.this.f20758 != null) {
                FlurryCustomEventBanner.this.f20758.displayAd();
            }
        }

        public void onRendered(FlurryAdBanner flurryAdBanner) {
            if (FlurryCustomEventBanner.this.f20762 != null) {
                FlurryCustomEventBanner.this.f20762.onBannerLoaded(FlurryCustomEventBanner.this.f20761);
            }
        }

        public void onShowFullscreen(FlurryAdBanner flurryAdBanner) {
            if (FlurryCustomEventBanner.this.f20762 != null) {
                FlurryCustomEventBanner.this.f20762.onBannerExpanded();
            }
        }

        public void onVideoCompleted(FlurryAdBanner flurryAdBanner) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26673(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        String str = map.get(FlurryAgentWrapper.PARAM_API_KEY);
        String str2 = map.get(FlurryAgentWrapper.PARAM_AD_SPACE_NAME);
        Log.i(f20757, "ServerInfo fetched from Mopub apiKey : " + str + " and " + FlurryAgentWrapper.PARAM_AD_SPACE_NAME + " :" + str2);
        return !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26674() {
        if (this.f20760 != null) {
            if (this.f20758 != null) {
                this.f20758.destroy();
                this.f20758 = null;
            }
            FlurryAgentWrapper.getInstance().endSession(this.f20760);
            this.f20760 = null;
            this.f20762 = null;
            this.f20761 = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26675(Context context, CustomEventBanner.CustomEventBannerListener customEventBannerListener, Map<String, Object> map, Map<String, String> map2) {
        if (customEventBannerListener == null) {
            Log.e(f20757, "CustomEventBannerListener cannot be null.");
        } else if (context == null) {
            Log.e(f20757, "Context cannot be null.");
            customEventBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        } else if (!(context instanceof Activity)) {
            Log.e(f20757, "Ad can be rendered only in Activity context.");
            customEventBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        } else if (!m26673(map2)) {
            Log.e(f20757, "Failed banner ad fetch: Missing required server extras [FLURRY_APIKEY and/or FLURRY_ADSPACE].");
            customEventBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        } else {
            this.f20760 = context;
            this.f20762 = customEventBannerListener;
            this.f20761 = new FrameLayout(context);
            this.f20759 = map2.get(FlurryAgentWrapper.PARAM_AD_SPACE_NAME);
            FlurryAgentWrapper.getInstance().startSession(context, map2.get(FlurryAgentWrapper.PARAM_API_KEY), (FlurryAgentListener) null);
            this.f20758 = new FlurryAdBanner(this.f20760, this.f20761, this.f20759);
            this.f20758.setListener(new FlurryMopubBannerListener(this, (AnonymousClass1) null));
            FlurryAdBanner flurryAdBanner = this.f20758;
            Pinkamena.DianePie();
        }
    }
}
