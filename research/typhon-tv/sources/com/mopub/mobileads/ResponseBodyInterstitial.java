package com.mopub.mobileads;

import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.common.DataKeys;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.CustomEventInterstitial;
import java.util.Map;

public abstract class ResponseBodyInterstitial extends CustomEventInterstitial {

    /* renamed from: 靐  reason: contains not printable characters */
    protected AdReport f20835;

    /* renamed from: 麤  reason: contains not printable characters */
    private EventForwardingBroadcastReceiver f20836;

    /* renamed from: 齉  reason: contains not printable characters */
    protected long f20837;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Context f20838;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m26718(Map<String, String> map) {
        return map.containsKey(DataKeys.HTML_RESPONSE_BODY_KEY);
    }

    public void loadInterstitial(Context context, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> map, Map<String, String> map2) {
        this.f20838 = context;
        if (m26718(map2)) {
            m26720(map2);
            try {
                this.f20835 = (AdReport) map.get(DataKeys.AD_REPORT_KEY);
                Long l = (Long) map.get(DataKeys.BROADCAST_IDENTIFIER_KEY);
                if (l == null) {
                    MoPubLog.e("Broadcast Identifier was not set in localExtras");
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                    return;
                }
                this.f20837 = l.longValue();
                this.f20836 = new EventForwardingBroadcastReceiver(customEventInterstitialListener, this.f20837);
                this.f20836.register(this.f20836, context);
                m26719(customEventInterstitialListener);
            } catch (ClassCastException e) {
                MoPubLog.e("LocalExtras contained an incorrect type.");
                customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        } else {
            customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
        }
    }

    public void onInvalidate() {
        if (this.f20836 != null) {
            this.f20836.unregister(this.f20836);
        }
    }

    public abstract void showInterstitial();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26719(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m26720(Map<String, String> map);
}
