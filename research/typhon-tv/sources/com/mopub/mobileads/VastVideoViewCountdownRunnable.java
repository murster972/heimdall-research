package com.mopub.mobileads;

import android.os.Handler;
import com.mopub.common.Preconditions;

public class VastVideoViewCountdownRunnable extends RepeatingHandlerRunnable {

    /* renamed from: 齉  reason: contains not printable characters */
    private final VastVideoViewController f20947;

    public VastVideoViewCountdownRunnable(VastVideoViewController vastVideoViewController, Handler handler) {
        super(handler);
        Preconditions.checkNotNull(handler);
        Preconditions.checkNotNull(vastVideoViewController);
        this.f20947 = vastVideoViewController;
    }

    public void doWork() {
        this.f20947.m26844();
        if (this.f20947.m26846()) {
            this.f20947.m26849();
        }
    }
}
