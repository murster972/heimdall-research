package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import java.io.Serializable;
import java.util.Locale;

public class VastFractionalProgressTracker extends VastTracker implements Serializable, Comparable<VastFractionalProgressTracker> {
    private static final long serialVersionUID = 0;
    private final float mFraction;

    public VastFractionalProgressTracker(String str, float f) {
        super(str);
        Preconditions.checkArgument(f >= 0.0f);
        this.mFraction = f;
    }

    public int compareTo(VastFractionalProgressTracker vastFractionalProgressTracker) {
        return Double.compare((double) trackingFraction(), (double) vastFractionalProgressTracker.trackingFraction());
    }

    public String toString() {
        return String.format(Locale.US, "%2f: %s", new Object[]{Float.valueOf(this.mFraction), this.mTrackingUrl});
    }

    public float trackingFraction() {
        return this.mFraction;
    }
}
