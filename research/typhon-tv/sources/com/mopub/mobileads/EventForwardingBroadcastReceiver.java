package com.mopub.mobileads;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.mopub.common.IntentActions;
import com.mopub.mobileads.CustomEventInterstitial;

public class EventForwardingBroadcastReceiver extends BaseBroadcastReceiver {

    /* renamed from: 龘  reason: contains not printable characters */
    private static IntentFilter f20753;

    /* renamed from: 靐  reason: contains not printable characters */
    private final CustomEventInterstitial.CustomEventInterstitialListener f20754;

    public EventForwardingBroadcastReceiver(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, long j) {
        super(j);
        this.f20754 = customEventInterstitialListener;
        getIntentFilter();
    }

    public IntentFilter getIntentFilter() {
        if (f20753 == null) {
            f20753 = new IntentFilter();
            f20753.addAction(IntentActions.ACTION_INTERSTITIAL_FAIL);
            f20753.addAction(IntentActions.ACTION_INTERSTITIAL_SHOW);
            f20753.addAction(IntentActions.ACTION_INTERSTITIAL_DISMISS);
            f20753.addAction(IntentActions.ACTION_INTERSTITIAL_CLICK);
        }
        return f20753;
    }

    public void onReceive(Context context, Intent intent) {
        if (this.f20754 != null && shouldConsumeBroadcast(intent)) {
            String action = intent.getAction();
            if (IntentActions.ACTION_INTERSTITIAL_FAIL.equals(action)) {
                this.f20754.onInterstitialFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
            } else if (IntentActions.ACTION_INTERSTITIAL_SHOW.equals(action)) {
                this.f20754.onInterstitialShown();
            } else if (IntentActions.ACTION_INTERSTITIAL_DISMISS.equals(action)) {
                this.f20754.onInterstitialDismissed();
                unregister(this);
            } else if (IntentActions.ACTION_INTERSTITIAL_CLICK.equals(action)) {
                this.f20754.onInterstitialClicked();
            }
        }
    }
}
