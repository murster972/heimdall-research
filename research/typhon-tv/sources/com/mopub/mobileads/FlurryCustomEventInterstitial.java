package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.Pinkamena;
import com.flurry.android.FlurryAgentListener;
import com.flurry.android.ads.FlurryAdErrorType;
import com.flurry.android.ads.FlurryAdInterstitial;
import com.flurry.android.ads.FlurryAdInterstitialListener;
import com.mopub.mobileads.CustomEventInterstitial;
import java.util.Map;

public class FlurryCustomEventInterstitial extends CustomEventInterstitial {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f20766 = FlurryCustomEventInterstitial.class.getSimpleName();

    /* renamed from: 连任  reason: contains not printable characters */
    private FlurryAdInterstitial f20767;

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f20768;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f20769;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public CustomEventInterstitial.CustomEventInterstitialListener f20770;

    /* renamed from: com.mopub.mobileads.FlurryCustomEventInterstitial$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f20771 = new int[FlurryAdErrorType.values().length];

        static {
            try {
                f20771[FlurryAdErrorType.FETCH.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f20771[FlurryAdErrorType.RENDER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f20771[FlurryAdErrorType.CLICK.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private class FlurryMopubInterstitialListener implements FlurryAdInterstitialListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f20772;

        private FlurryMopubInterstitialListener() {
            this.f20772 = getClass().getSimpleName();
        }

        /* synthetic */ FlurryMopubInterstitialListener(FlurryCustomEventInterstitial flurryCustomEventInterstitial, AnonymousClass1 r2) {
            this();
        }

        public void onAppExit(FlurryAdInterstitial flurryAdInterstitial) {
        }

        public void onClicked(FlurryAdInterstitial flurryAdInterstitial) {
            if (FlurryCustomEventInterstitial.this.f20770 != null) {
                FlurryCustomEventInterstitial.this.f20770.onInterstitialClicked();
            }
        }

        public void onClose(FlurryAdInterstitial flurryAdInterstitial) {
            if (FlurryCustomEventInterstitial.this.f20770 != null) {
                FlurryCustomEventInterstitial.this.f20770.onInterstitialDismissed();
            }
        }

        public void onDisplay(FlurryAdInterstitial flurryAdInterstitial) {
        }

        public void onError(FlurryAdInterstitial flurryAdInterstitial, FlurryAdErrorType flurryAdErrorType, int i) {
            Log.d(this.f20772, String.format("onError: Flurry interstitial ad not available. Error type: %s. Error code: %s", new Object[]{flurryAdErrorType.toString(), Integer.valueOf(i)}));
            if (FlurryCustomEventInterstitial.this.f20770 != null) {
                switch (AnonymousClass1.f20771[flurryAdErrorType.ordinal()]) {
                    case 1:
                        FlurryCustomEventInterstitial.this.f20770.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
                        return;
                    case 2:
                        FlurryCustomEventInterstitial.this.f20770.onInterstitialFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
                        return;
                    case 3:
                        return;
                    default:
                        FlurryCustomEventInterstitial.this.f20770.onInterstitialFailed(MoPubErrorCode.UNSPECIFIED);
                        return;
                }
            }
        }

        public void onFetched(FlurryAdInterstitial flurryAdInterstitial) {
            if (FlurryCustomEventInterstitial.this.f20770 != null) {
                FlurryCustomEventInterstitial.this.f20770.onInterstitialLoaded();
            }
        }

        public void onRendered(FlurryAdInterstitial flurryAdInterstitial) {
            if (FlurryCustomEventInterstitial.this.f20770 != null) {
                FlurryCustomEventInterstitial.this.f20770.onInterstitialShown();
            }
        }

        public void onVideoCompleted(FlurryAdInterstitial flurryAdInterstitial) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26677(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        String str = map.get(FlurryAgentWrapper.PARAM_API_KEY);
        String str2 = map.get(FlurryAgentWrapper.PARAM_AD_SPACE_NAME);
        Log.i(f20766, "ServerInfo fetched from Mopub apiKey : " + str + " and " + FlurryAgentWrapper.PARAM_AD_SPACE_NAME + " :" + str2);
        return !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2);
    }

    /* access modifiers changed from: protected */
    public void loadInterstitial(Context context, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> map, Map<String, String> map2) {
        if (customEventInterstitialListener == null) {
            Log.e(f20766, "CustomEventInterstitialListener cannot be null.");
        } else if (context == null) {
            Log.e(f20766, "Context cannot be null.");
            customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        } else if (!(context instanceof Activity)) {
            Log.e(f20766, "Ad can be rendered only in Activity context.");
            customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        } else if (!m26677(map2)) {
            Log.e(f20766, "Failed interstitial ad fetch: Missing required server extras [FLURRY_APIKEY and/or FLURRY_ADSPACE].");
            customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        } else {
            this.f20768 = context;
            this.f20770 = customEventInterstitialListener;
            this.f20769 = map2.get(FlurryAgentWrapper.PARAM_AD_SPACE_NAME);
            FlurryAgentWrapper.getInstance().startSession(context, map2.get(FlurryAgentWrapper.PARAM_API_KEY), (FlurryAgentListener) null);
            this.f20767 = new FlurryAdInterstitial(this.f20768, this.f20769);
            this.f20767.setListener(new FlurryMopubInterstitialListener(this, (AnonymousClass1) null));
            FlurryAdInterstitial flurryAdInterstitial = this.f20767;
            Pinkamena.DianePie();
        }
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        if (this.f20768 != null) {
            if (this.f20767 != null) {
                this.f20767.destroy();
                this.f20767 = null;
            }
            FlurryAgentWrapper.getInstance().endSession(this.f20768);
            this.f20768 = null;
            this.f20770 = null;
        }
    }

    /* access modifiers changed from: protected */
    public void showInterstitial() {
        if (this.f20767 != null) {
            this.f20767.displayAd();
        }
    }
}
