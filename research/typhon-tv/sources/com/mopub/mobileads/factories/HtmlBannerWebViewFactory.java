package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.HtmlBannerWebView;

public class HtmlBannerWebViewFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static HtmlBannerWebViewFactory f5764 = new HtmlBannerWebViewFactory();

    public static HtmlBannerWebView create(Context context, AdReport adReport, CustomEventBanner.CustomEventBannerListener customEventBannerListener, boolean z, String str, String str2) {
        return f5764.internalCreate(context, adReport, customEventBannerListener, z, str, str2);
    }

    @Deprecated
    public static void setInstance(HtmlBannerWebViewFactory htmlBannerWebViewFactory) {
        f5764 = htmlBannerWebViewFactory;
    }

    public HtmlBannerWebView internalCreate(Context context, AdReport adReport, CustomEventBanner.CustomEventBannerListener customEventBannerListener, boolean z, String str, String str2) {
        HtmlBannerWebView htmlBannerWebView = new HtmlBannerWebView(context, adReport);
        htmlBannerWebView.init(customEventBannerListener, z, str, str2, adReport.getDspCreativeId());
        return htmlBannerWebView;
    }
}
