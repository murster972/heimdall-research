package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.common.VisibleForTesting;
import com.mopub.mraid.MraidController;
import com.mopub.mraid.PlacementType;

public class MraidControllerFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static MraidControllerFactory f20969 = new MraidControllerFactory();

    public static MraidController create(Context context, AdReport adReport, PlacementType placementType) {
        return f20969.m26901(context, adReport, placementType);
    }

    @VisibleForTesting
    public static void setInstance(MraidControllerFactory mraidControllerFactory) {
        f20969 = mraidControllerFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public MraidController m26901(Context context, AdReport adReport, PlacementType placementType) {
        return new MraidController(context, adReport, placementType);
    }
}
