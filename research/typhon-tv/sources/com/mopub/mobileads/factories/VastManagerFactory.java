package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.mobileads.VastManager;

public class VastManagerFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static VastManagerFactory f5767 = new VastManagerFactory();

    public static VastManager create(Context context) {
        return f5767.internalCreate(context, true);
    }

    public static VastManager create(Context context, boolean z) {
        return f5767.internalCreate(context, z);
    }

    @Deprecated
    public static void setInstance(VastManagerFactory vastManagerFactory) {
        f5767 = vastManagerFactory;
    }

    public VastManager internalCreate(Context context, boolean z) {
        return new VastManager(context, z);
    }
}
