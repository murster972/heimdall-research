package com.mopub.mobileads.factories;

import com.mopub.common.AdReport;
import com.mopub.mobileads.CustomEventBannerAdapter;
import com.mopub.mobileads.MoPubView;
import java.util.Map;

public class CustomEventBannerAdapterFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static CustomEventBannerAdapterFactory f5760 = new CustomEventBannerAdapterFactory();

    public static CustomEventBannerAdapter create(MoPubView moPubView, String str, Map<String, String> map, long j, AdReport adReport) {
        return f5760.m6241(moPubView, str, map, j, adReport);
    }

    @Deprecated
    public static void setInstance(CustomEventBannerAdapterFactory customEventBannerAdapterFactory) {
        f5760 = customEventBannerAdapterFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public CustomEventBannerAdapter m6241(MoPubView moPubView, String str, Map<String, String> map, long j, AdReport adReport) {
        return new CustomEventBannerAdapter(moPubView, str, map, j, adReport);
    }
}
