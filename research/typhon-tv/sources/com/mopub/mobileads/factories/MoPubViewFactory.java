package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.common.VisibleForTesting;
import com.mopub.mobileads.MoPubView;

public class MoPubViewFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static MoPubViewFactory f5766 = new MoPubViewFactory();

    public static MoPubView create(Context context) {
        return f5766.m6245(context);
    }

    @Deprecated
    @VisibleForTesting
    public static void setInstance(MoPubViewFactory moPubViewFactory) {
        f5766 = moPubViewFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public MoPubView m6245(Context context) {
        return new MoPubView(context);
    }
}
