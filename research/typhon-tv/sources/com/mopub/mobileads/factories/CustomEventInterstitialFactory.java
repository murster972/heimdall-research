package com.mopub.mobileads.factories;

import com.mopub.mobileads.CustomEventInterstitial;
import java.lang.reflect.Constructor;

public class CustomEventInterstitialFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    private static CustomEventInterstitialFactory f5763 = new CustomEventInterstitialFactory();

    public static CustomEventInterstitial create(String str) throws Exception {
        return f5763.m6244(str);
    }

    @Deprecated
    public static void setInstance(CustomEventInterstitialFactory customEventInterstitialFactory) {
        f5763 = customEventInterstitialFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public CustomEventInterstitial m6244(String str) throws Exception {
        Constructor<? extends U> declaredConstructor = Class.forName(str).asSubclass(CustomEventInterstitial.class).getDeclaredConstructor((Class[]) null);
        declaredConstructor.setAccessible(true);
        return (CustomEventInterstitial) declaredConstructor.newInstance(new Object[0]);
    }
}
