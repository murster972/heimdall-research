package com.mopub.mobileads.factories;

import com.mopub.mobileads.CustomEventBanner;
import java.lang.reflect.Constructor;

public class CustomEventBannerFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    private static CustomEventBannerFactory f5761 = new CustomEventBannerFactory();

    public static CustomEventBanner create(String str) throws Exception {
        return f5761.m6242(str);
    }

    @Deprecated
    public static void setInstance(CustomEventBannerFactory customEventBannerFactory) {
        f5761 = customEventBannerFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public CustomEventBanner m6242(String str) throws Exception {
        Constructor<? extends U> declaredConstructor = Class.forName(str).asSubclass(CustomEventBanner.class).getDeclaredConstructor((Class[]) null);
        declaredConstructor.setAccessible(true);
        return (CustomEventBanner) declaredConstructor.newInstance(new Object[0]);
    }
}
