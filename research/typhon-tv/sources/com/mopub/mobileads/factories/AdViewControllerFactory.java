package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.mobileads.AdViewController;
import com.mopub.mobileads.MoPubView;

public class AdViewControllerFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static AdViewControllerFactory f5759 = new AdViewControllerFactory();

    public static AdViewController create(Context context, MoPubView moPubView) {
        return f5759.m6240(context, moPubView);
    }

    @Deprecated
    public static void setInstance(AdViewControllerFactory adViewControllerFactory) {
        f5759 = adViewControllerFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public AdViewController m6240(Context context, MoPubView moPubView) {
        return new AdViewController(context, moPubView);
    }
}
