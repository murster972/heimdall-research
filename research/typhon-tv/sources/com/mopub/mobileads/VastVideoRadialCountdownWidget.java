package com.mopub.mobileads;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.widget.RelativeLayout;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.RadialCountdownDrawable;

public class VastVideoRadialCountdownWidget extends AppCompatImageView {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f5753;

    /* renamed from: 龘  reason: contains not printable characters */
    private RadialCountdownDrawable f5754;

    public VastVideoRadialCountdownWidget(Context context) {
        super(context);
        setId((int) Utils.generateUniqueId());
        int dipsToIntPixels = Dips.dipsToIntPixels(45.0f, context);
        int dipsToIntPixels2 = Dips.dipsToIntPixels(16.0f, context);
        int dipsToIntPixels3 = Dips.dipsToIntPixels(16.0f, context);
        int dipsToIntPixels4 = Dips.dipsToIntPixels(5.0f, context);
        this.f5754 = new RadialCountdownDrawable(context);
        setImageDrawable(this.f5754);
        setPadding(dipsToIntPixels4, dipsToIntPixels4, dipsToIntPixels4, dipsToIntPixels4);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dipsToIntPixels, dipsToIntPixels);
        layoutParams.setMargins(0, dipsToIntPixels2, dipsToIntPixels3, 0);
        layoutParams.addRule(11);
        setLayoutParams(layoutParams);
    }

    public void calibrateAndMakeVisible(int i) {
        this.f5754.setInitialCountdown(i);
        setVisibility(8);
    }

    @Deprecated
    @VisibleForTesting
    public RadialCountdownDrawable getImageViewDrawable() {
        return this.f5754;
    }

    @Deprecated
    @VisibleForTesting
    public void setImageViewDrawable(RadialCountdownDrawable radialCountdownDrawable) {
        this.f5754 = radialCountdownDrawable;
    }

    public void updateCountdownProgress(int i, int i2) {
        if (i2 < this.f5753) {
            return;
        }
        if (i - i2 < 0) {
            setVisibility(8);
            return;
        }
        this.f5754.updateCountdownProgress(i2);
        this.f5753 = i2;
    }
}
