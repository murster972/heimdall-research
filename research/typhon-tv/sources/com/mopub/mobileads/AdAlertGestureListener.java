package com.mopub.mobileads;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.mopub.common.AdReport;

public class AdAlertGestureListener extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private AdAlertReporter f20677;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f20678;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f20679;

    /* renamed from: ˑ  reason: contains not printable characters */
    private ZigZagState f20680 = ZigZagState.UNSET;

    /* renamed from: ٴ  reason: contains not printable characters */
    private View f20681;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f20682;

    /* renamed from: 靐  reason: contains not printable characters */
    private float f20683 = 100.0f;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f20684;

    /* renamed from: 齉  reason: contains not printable characters */
    private float f20685;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AdReport f20686;

    enum ZigZagState {
        UNSET,
        GOING_RIGHT,
        GOING_LEFT,
        FINISHED,
        FAILED
    }

    AdAlertGestureListener(View view, AdReport adReport) {
        if (view != null && view.getWidth() > 0) {
            this.f20683 = Math.min(100.0f, ((float) view.getWidth()) / 3.0f);
        }
        this.f20681 = view;
        this.f20686 = adReport;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m26605(float f) {
        return f > this.f20685;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m26606(float f) {
        return f < this.f20685;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m26607(float f) {
        if (this.f20684) {
            return true;
        }
        if (f > this.f20679 - this.f20683) {
            return false;
        }
        this.f20682 = false;
        this.f20684 = true;
        m26610();
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26608(float f) {
        if (m26609(f) && m26606(f)) {
            this.f20680 = ZigZagState.GOING_LEFT;
            this.f20679 = f;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m26609(float f) {
        if (this.f20682) {
            return true;
        }
        if (f < this.f20679 + this.f20683) {
            return false;
        }
        this.f20684 = false;
        this.f20682 = true;
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26610() {
        this.f20678++;
        if (this.f20678 >= 4) {
            this.f20680 = ZigZagState.FINISHED;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26611(float f) {
        if (m26607(f) && m26605(f)) {
            this.f20680 = ZigZagState.GOING_RIGHT;
            this.f20679 = f;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26612(float f) {
        if (f > this.f20679) {
            this.f20680 = ZigZagState.GOING_RIGHT;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26613(float f, float f2) {
        return Math.abs(f2 - f) > 100.0f;
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (this.f20680 == ZigZagState.FINISHED) {
            return super.onScroll(motionEvent, motionEvent2, f, f2);
        }
        if (m26613(motionEvent.getY(), motionEvent2.getY())) {
            this.f20680 = ZigZagState.FAILED;
            return super.onScroll(motionEvent, motionEvent2, f, f2);
        }
        switch (this.f20680) {
            case UNSET:
                this.f20679 = motionEvent.getX();
                m26612(motionEvent2.getX());
                break;
            case GOING_RIGHT:
                m26608(motionEvent2.getX());
                break;
            case GOING_LEFT:
                m26611(motionEvent2.getX());
                break;
        }
        this.f20685 = motionEvent2.getX();
        return super.onScroll(motionEvent, motionEvent2, f, f2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26614() {
        this.f20678 = 0;
        this.f20680 = ZigZagState.UNSET;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26615() {
        ZigZagState zigZagState = this.f20680;
        ZigZagState zigZagState2 = this.f20680;
        if (zigZagState == ZigZagState.FINISHED) {
            this.f20677 = new AdAlertReporter(this.f20681.getContext(), this.f20681, this.f20686);
            this.f20677.send();
        }
        m26614();
    }
}
