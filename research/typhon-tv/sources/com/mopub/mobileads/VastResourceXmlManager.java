package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

public class VastResourceXmlManager {
    public static final String CREATIVE_TYPE = "creativeType";
    public static final String HTML_RESOURCE = "HTMLResource";
    public static final String IFRAME_RESOURCE = "IFrameResource";
    public static final String STATIC_RESOURCE = "StaticResource";

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20873;

    VastResourceXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20873 = node;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m26785() {
        String attributeValue = XmlUtils.getAttributeValue(XmlUtils.getFirstMatchingChildNode(this.f20873, STATIC_RESOURCE), CREATIVE_TYPE);
        if (attributeValue != null) {
            return attributeValue.toLowerCase();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m26786() {
        return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(this.f20873, HTML_RESOURCE));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m26787() {
        return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(this.f20873, IFRAME_RESOURCE));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m26788() {
        return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(this.f20873, STATIC_RESOURCE));
    }
}
