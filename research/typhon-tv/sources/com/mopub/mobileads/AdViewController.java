package com.mopub.mobileads;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.Pinkamena;
import com.mopub.common.AdReport;
import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;
import com.mopub.common.TyphoonApp;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.event.BaseEvent;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mraid.MraidNativeCommandHandler;
import com.mopub.network.AdRequest;
import com.mopub.network.AdResponse;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.Networking;
import com.mopub.network.TrackingRequest;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.VolleyError;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

public class AdViewController {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final FrameLayout.LayoutParams f5698 = new FrameLayout.LayoutParams(-2, -2, 17);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final WeakHashMap<View, Boolean> f5699 = new WeakHashMap<>();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final AdRequest.Listener f5700;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Context f5701;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MoPubView f5702;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Handler f5703;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f5704;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Location f5705;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f5706;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f5707;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f5708 = true;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f5709 = true;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f5710;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f5711;

    /* renamed from: ˑ  reason: contains not printable characters */
    private WebViewAdUrlGenerator f5712;

    /* renamed from: י  reason: contains not printable characters */
    private String f5713;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f5714;

    /* renamed from: ٴ  reason: contains not printable characters */
    private AdResponse f5715;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f5716;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private AdRequest f5717;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private Integer f5718;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Runnable f5719;

    /* renamed from: 麤  reason: contains not printable characters */
    private final long f5720;
    @VisibleForTesting

    /* renamed from: 龘  reason: contains not printable characters */
    int f5721 = 1;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String f5722;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private Map<String, Object> f5723 = new HashMap();

    public AdViewController(Context context, MoPubView moPubView) {
        this.f5701 = context;
        this.f5702 = moPubView;
        this.f5714 = -1;
        this.f5720 = Utils.generateUniqueId();
        this.f5712 = new WebViewAdUrlGenerator(this.f5701.getApplicationContext(), MraidNativeCommandHandler.isStorePictureSupported(this.f5701));
        this.f5700 = new AdRequest.Listener() {
            public void onErrorResponse(VolleyError volleyError) {
                AdViewController.this.m6203(volleyError);
            }

            public void onSuccess(AdResponse adResponse) {
                AdViewController.this.m6202(adResponse);
            }
        };
        this.f5719 = new Runnable() {
            public void run() {
                AdViewController.this.m6180();
            }
        };
        this.f5718 = 60000;
        this.f5703 = new Handler();
    }

    public static void setShouldHonorServerDimensions(View view) {
        f5699.put(view, true);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m6178() {
        this.f5703.removeCallbacks(this.f5719);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean m6179() {
        if (this.f5701 == null) {
            return false;
        }
        if (DeviceUtils.isPermissionGranted(this.f5701, "android.permission.ACCESS_NETWORK_STATE")) {
            return (((ConnectivityManager) this.f5701.getSystemService("connectivity")).getActiveNetworkInfo() == null || 0 == 0) ? false : true;
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m6180() {
        this.f5711 = true;
        if (TextUtils.isEmpty(this.f5713)) {
            MoPubLog.d("Can't load an ad in this ad view because the ad unit ID is not set. Did you forget to call setAdUnitId()?");
        } else if (!m6179()) {
            m6191();
        } else {
            m6204(m6190());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m6181(boolean z) {
        if (this.f5711 && this.f5708 != z) {
            if (z) {
            }
        }
        this.f5708 = z;
        if (this.f5711 && this.f5708) {
            m6191();
        } else if (!this.f5708) {
            m6178();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m6182(View view) {
        return f5699.get(view) != null;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public FrameLayout.LayoutParams m6183(View view) {
        Integer num = null;
        Integer num2 = null;
        if (this.f5715 != null) {
            num = this.f5715.getWidth();
            num2 = this.f5715.getHeight();
        }
        return (num == null || num2 == null || !m6182(view) || num.intValue() <= 0 || num2.intValue() <= 0) ? f5698 : new FrameLayout.LayoutParams(Dips.asIntPixels((float) num.intValue(), this.f5701), Dips.asIntPixels((float) num2.intValue(), this.f5701), 17);
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static MoPubErrorCode m6185(VolleyError volleyError, Context context) {
        NetworkResponse networkResponse = volleyError.networkResponse;
        if (!(volleyError instanceof MoPubNetworkError)) {
            return networkResponse == null ? !DeviceUtils.isNetworkAvailable(context) ? MoPubErrorCode.NO_CONNECTION : MoPubErrorCode.UNSPECIFIED : volleyError.networkResponse.statusCode >= 400 ? MoPubErrorCode.SERVER_ERROR : MoPubErrorCode.UNSPECIFIED;
        }
        switch (((MoPubNetworkError) volleyError).getReason()) {
            case WARMING_UP:
                return MoPubErrorCode.WARMUP;
            case NO_FILL:
                return MoPubErrorCode.NO_FILL;
            default:
                return MoPubErrorCode.UNSPECIFIED;
        }
    }

    public int getAdHeight() {
        if (this.f5715 == null || this.f5715.getHeight() == null) {
            return 0;
        }
        return this.f5715.getHeight().intValue();
    }

    public AdReport getAdReport() {
        if (this.f5713 == null || this.f5715 == null) {
            return null;
        }
        return new AdReport(this.f5713, ClientMetadata.getInstance(this.f5701), this.f5715);
    }

    public String getAdUnitId() {
        return this.f5713;
    }

    public int getAdWidth() {
        if (this.f5715 == null || this.f5715.getWidth() == null) {
            return 0;
        }
        return this.f5715.getWidth().intValue();
    }

    @Deprecated
    public boolean getAutorefreshEnabled() {
        return getCurrentAutoRefreshStatus();
    }

    public long getBroadcastIdentifier() {
        return this.f5720;
    }

    public boolean getCurrentAutoRefreshStatus() {
        return this.f5708;
    }

    public String getCustomEventClassName() {
        return this.f5716;
    }

    public String getKeywords() {
        return this.f5710;
    }

    public Location getLocation() {
        return this.f5705;
    }

    public MoPubView getMoPubView() {
        return this.f5702;
    }

    public boolean getTesting() {
        return this.f5707;
    }

    public void loadAd() {
        this.f5721 = 1;
        m6180();
    }

    public void reload() {
        m6204(this.f5722);
    }

    public void setAdUnitId(String str) {
        this.f5713 = str;
    }

    public void setKeywords(String str) {
        this.f5710 = str;
    }

    public void setLocation(Location location) {
        this.f5705 = location;
    }

    public void setTesting(boolean z) {
        this.f5707 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6187() {
        if (this.f5715 != null) {
            TrackingRequest.makeTrackingHttpRequest(this.f5715.getImpressionTrackingUrl(), this.f5701, BaseEvent.Name.IMPRESSION_REQUEST);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m6188() {
        if (this.f5715 != null) {
            TrackingRequest.makeTrackingHttpRequest(this.f5715.getClickTrackingUrl(), this.f5701, BaseEvent.Name.CLICK_REQUEST);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m6189() {
        m6199();
        Pinkamena.DianePie();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public String m6190() {
        if (this.f5712 == null) {
            return null;
        }
        return this.f5712.withAdUnitId(this.f5713).withKeywords(this.f5710).withLocation(this.f5705).generateUrlString(TyphoonApp.HOST);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m6191() {
        m6178();
        if (this.f5708 && this.f5718 != null && this.f5718.intValue() > 0) {
            this.f5703.postDelayed(this.f5719, Math.min(600000, ((long) this.f5718.intValue()) * ((long) Math.pow(1.5d, (double) this.f5721))));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public Map<String, Object> m6192() {
        return this.f5723 != null ? new TreeMap(this.f5723) : new TreeMap();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public Integer m6193() {
        return Integer.valueOf(this.f5714);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6194() {
        m6181(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6195(MoPubErrorCode moPubErrorCode) {
        MoPubLog.i("Ad failed to load.");
        m6199();
        MoPubView moPubView = getMoPubView();
        if (moPubView != null) {
            m6191();
            moPubView.m6225(moPubErrorCode);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6196(String str) {
        MoPubView moPubView = getMoPubView();
        if (moPubView == null || this.f5701 == null) {
            m6199();
            return;
        }
        AdRequest adRequest = new AdRequest(str, moPubView.getAdFormat(), this.f5713, this.f5701, this.f5700);
        Networking.getRequestQueue(this.f5701).add(adRequest);
        this.f5717 = adRequest;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m6197() {
        if (!this.f5706) {
            if (this.f5717 != null) {
                this.f5717.cancel();
                this.f5717 = null;
            }
            m6181(false);
            m6178();
            this.f5702 = null;
            this.f5701 = null;
            this.f5712 = null;
            this.f5706 = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m6198() {
        if (this.f5709) {
            m6181(true);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6199() {
        this.f5704 = false;
        if (this.f5717 != null) {
            if (!this.f5717.isCanceled()) {
                this.f5717.cancel();
            }
            this.f5717 = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6200(final View view) {
        this.f5703.post(new Runnable() {
            public void run() {
                MoPubView moPubView = AdViewController.this.getMoPubView();
                if (moPubView != null) {
                    moPubView.removeAllViews();
                    moPubView.addView(view, AdViewController.this.m6183(view));
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6201(MoPubView moPubView, String str, Map<String, String> map) {
        Preconditions.checkNotNull(map);
        if (moPubView != null) {
            moPubView.m6226(str, map);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6202(AdResponse adResponse) {
        this.f5721 = 1;
        this.f5715 = adResponse;
        this.f5716 = adResponse.getCustomEventClassName();
        this.f5714 = this.f5715.getAdTimeoutMillis() == null ? this.f5714 : this.f5715.getAdTimeoutMillis().intValue();
        this.f5718 = this.f5715.getRefreshTimeMillis();
        m6199();
        m6201(this.f5702, adResponse.getCustomEventClassName(), adResponse.getServerExtras());
        m6191();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6203(VolleyError volleyError) {
        if (volleyError instanceof MoPubNetworkError) {
            MoPubNetworkError moPubNetworkError = (MoPubNetworkError) volleyError;
            if (moPubNetworkError.getRefreshTimeMillis() != null) {
                this.f5718 = moPubNetworkError.getRefreshTimeMillis();
            }
        }
        MoPubErrorCode r0 = m6185(volleyError, this.f5701);
        if (r0 == MoPubErrorCode.SERVER_ERROR) {
            this.f5721++;
        }
        m6199();
        m6195(r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6204(String str) {
        if (str != null) {
            if (!this.f5704) {
                this.f5722 = str;
                this.f5704 = true;
                m6196(this.f5722);
            } else if (!TextUtils.isEmpty(this.f5713)) {
                MoPubLog.i("Already loading an ad for " + this.f5713 + ", wait to finish.");
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6205(Map<String, Object> map) {
        this.f5723 = map != null ? new TreeMap(map) : new TreeMap();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6206(boolean z) {
        this.f5709 = z;
        m6181(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6207(MoPubErrorCode moPubErrorCode) {
        this.f5704 = false;
        Log.v("MoPub", "MoPubErrorCode: " + (moPubErrorCode == null ? "" : moPubErrorCode.toString()));
        String failoverUrl = this.f5715 == null ? "" : this.f5715.getFailoverUrl();
        if (!TextUtils.isEmpty(failoverUrl)) {
            m6204(failoverUrl);
            return true;
        }
        m6195(MoPubErrorCode.NO_FILL);
        return false;
    }
}
