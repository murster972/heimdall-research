package com.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import com.mopub.common.AdType;
import com.mopub.common.DataKeys;
import com.mopub.common.FullAdType;
import com.mopub.common.IntentActions;
import com.mopub.common.util.Intents;
import com.mopub.common.util.Reflection;
import com.mopub.mobileads.BaseVideoViewController;
import com.mopub.mraid.MraidVideoViewController;

public class MraidVideoPlayerActivity extends BaseVideoPlayerActivity implements BaseVideoViewController.BaseVideoViewControllerListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f20833;

    /* renamed from: 龘  reason: contains not printable characters */
    private BaseVideoViewController f20834;

    /* renamed from: 龘  reason: contains not printable characters */
    protected static long m26716(Intent intent) {
        return intent.getLongExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, -1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private BaseVideoViewController m26717(Bundle bundle) throws IllegalStateException {
        String stringExtra = getIntent().getStringExtra(BaseVideoPlayerActivity.VIDEO_CLASS_EXTRAS_KEY);
        if (FullAdType.VAST.equals(stringExtra)) {
            return new VastVideoViewController(this, getIntent().getExtras(), bundle, this.f20833, this);
        } else if (AdType.MRAID.equals(stringExtra)) {
            return new MraidVideoViewController(this, getIntent().getExtras(), bundle, this);
        } else {
            if ("native".equals(stringExtra)) {
                Class[] clsArr = {Context.class, Bundle.class, Bundle.class, BaseVideoViewController.BaseVideoViewControllerListener.class};
                Object[] objArr = {this, getIntent().getExtras(), bundle, this};
                if (!Reflection.classFound("com.mopub.nativeads.NativeVideoViewController")) {
                    throw new IllegalStateException("Missing native video module");
                }
                try {
                    return (BaseVideoViewController) Reflection.instantiateClassWithConstructor("com.mopub.nativeads.NativeVideoViewController", BaseVideoViewController.class, clsArr, objArr);
                } catch (Exception e) {
                    throw new IllegalStateException("Missing native video module");
                }
            } else {
                throw new IllegalStateException("Unsupported video type: " + stringExtra);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (this.f20834 != null) {
            this.f20834.m26652(i, i2, intent);
        }
    }

    public void onBackPressed() {
        if (this.f20834 != null && this.f20834.backButtonEnabled()) {
            super.onBackPressed();
            this.f20834.m26643();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f20834 != null) {
            this.f20834.m26653(configuration);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        this.f20833 = m26716(getIntent());
        try {
            this.f20834 = m26717(bundle);
            this.f20834.m26651();
        } catch (IllegalStateException e) {
            BaseBroadcastReceiver.broadcastAction(this, this.f20833, IntentActions.ACTION_INTERSTITIAL_FAIL);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f20834 != null) {
            this.f20834.m26646();
        }
        super.onDestroy();
    }

    public void onFinish() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.f20834 != null) {
            this.f20834.m26650();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f20834 != null) {
            this.f20834.m26649();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.f20834 != null) {
            this.f20834.m26654(bundle);
        }
    }

    public void onSetContentView(View view) {
        setContentView(view);
    }

    public void onSetRequestedOrientation(int i) {
        setRequestedOrientation(i);
    }

    public void onStartActivityForResult(Class<? extends Activity> cls, int i, Bundle bundle) {
        if (cls != null) {
            try {
                startActivityForResult(Intents.getStartActivityIntent(this, cls, bundle), i);
            } catch (ActivityNotFoundException e) {
            }
        }
    }
}
