package com.mopub.mobileads;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.mopub.common.AdReport;

public class ViewGestureDetector extends GestureDetector {

    /* renamed from: 靐  reason: contains not printable characters */
    private AdAlertGestureListener f20965;

    /* renamed from: 齉  reason: contains not printable characters */
    private UserClickListener f20966;

    /* renamed from: 龘  reason: contains not printable characters */
    private final View f20967;

    public interface UserClickListener {
        void onResetUserClick();

        void onUserClick();

        boolean wasClicked();
    }

    public ViewGestureDetector(Context context, View view, AdReport adReport) {
        this(context, view, new AdAlertGestureListener(view, adReport));
    }

    private ViewGestureDetector(Context context, View view, AdAlertGestureListener adAlertGestureListener) {
        super(context, adAlertGestureListener);
        this.f20965 = adAlertGestureListener;
        this.f20967 = view;
        setIsLongpressEnabled(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26899(MotionEvent motionEvent, View view) {
        if (motionEvent == null || view == null) {
            return false;
        }
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        return x >= 0.0f && x <= ((float) view.getWidth()) && y >= 0.0f && y <= ((float) view.getHeight());
    }

    public void sendTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                onTouchEvent(motionEvent);
                return;
            case 1:
                if (this.f20966 != null) {
                    this.f20966.onUserClick();
                }
                this.f20965.m26615();
                return;
            case 2:
                if (m26899(motionEvent, this.f20967)) {
                    onTouchEvent(motionEvent);
                    return;
                } else {
                    m26900();
                    return;
                }
            default:
                return;
        }
    }

    public void setUserClickListener(UserClickListener userClickListener) {
        this.f20966 = userClickListener;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26900() {
        this.f20965.m26614();
    }
}
