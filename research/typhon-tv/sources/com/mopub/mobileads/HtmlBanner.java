package com.mopub.mobileads;

import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.common.DataKeys;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.factories.HtmlBannerWebViewFactory;
import java.util.Map;

public class HtmlBanner extends CustomEventBanner {

    /* renamed from: 龘  reason: contains not printable characters */
    private HtmlBannerWebView f20774;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26678(Map<String, String> map) {
        return map.containsKey(DataKeys.HTML_RESPONSE_BODY_KEY);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26679() {
        if (this.f20774 != null) {
            this.f20774.destroy();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26680(Context context, CustomEventBanner.CustomEventBannerListener customEventBannerListener, Map<String, Object> map, Map<String, String> map2) {
        if (m26678(map2)) {
            String str = map2.get(DataKeys.HTML_RESPONSE_BODY_KEY);
            String str2 = map2.get(DataKeys.REDIRECT_URL_KEY);
            String str3 = map2.get(DataKeys.CLICKTHROUGH_URL_KEY);
            Boolean valueOf = Boolean.valueOf(map2.get(DataKeys.SCROLLABLE_KEY));
            try {
                this.f20774 = HtmlBannerWebViewFactory.create(context, (AdReport) map.get(DataKeys.AD_REPORT_KEY), customEventBannerListener, valueOf.booleanValue(), str2, str3);
                AdViewController.setShouldHonorServerDimensions(this.f20774);
                this.f20774.m26631(str);
            } catch (ClassCastException e) {
                MoPubLog.e("LocalExtras contained an incorrect type.");
                customEventBannerListener.onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        } else {
            customEventBannerListener.onBannerFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
        }
    }
}
