package com.mopub.mobileads;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.Pinkamena;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.exceptions.IntentNotResolvableException;
import java.util.EnumSet;

class HtmlWebViewClient extends WebViewClient {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final HtmlWebViewListener f20783;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final BaseHtmlWebView f20784;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f20785;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f20786;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f20787;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f20788;

    /* renamed from: 龘  reason: contains not printable characters */
    private final EnumSet<UrlAction> f20789 = EnumSet.of(UrlAction.HANDLE_MOPUB_SCHEME, new UrlAction[]{UrlAction.IGNORE_ABOUT_SCHEME, UrlAction.HANDLE_PHONE_SCHEME, UrlAction.OPEN_APP_MARKET, UrlAction.OPEN_NATIVE_BROWSER, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK});

    HtmlWebViewClient(HtmlWebViewListener htmlWebViewListener, BaseHtmlWebView baseHtmlWebView, String str, String str2, String str3) {
        this.f20783 = htmlWebViewListener;
        this.f20784 = baseHtmlWebView;
        this.f20787 = str;
        this.f20785 = str2;
        this.f20788 = str3;
        this.f20786 = baseHtmlWebView.getContext();
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.f20785 != null && str.startsWith(this.f20785)) {
            webView.stopLoading();
            if (this.f20784.wasClicked()) {
                try {
                    Context context = this.f20786;
                    Uri parse = Uri.parse(str);
                    String str2 = this.f20788;
                    Pinkamena.DianePie();
                } catch (IntentNotResolvableException e) {
                }
            }
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        new UrlHandler.Builder().withDspCreativeId(this.f20788).withSupportedUrlActions(this.f20789).withResultActions(new UrlHandler.ResultActions() {
            public void urlHandlingFailed(String str, UrlAction urlAction) {
            }

            public void urlHandlingSucceeded(String str, UrlAction urlAction) {
                if (HtmlWebViewClient.this.f20784.wasClicked()) {
                    HtmlWebViewClient.this.f20783.onClicked();
                    HtmlWebViewClient.this.f20784.onResetUserClick();
                }
            }
        }).withMoPubSchemeListener(new UrlHandler.MoPubSchemeListener() {
            public void onClose() {
                HtmlWebViewClient.this.f20783.onCollapsed();
            }

            public void onFailLoad() {
                HtmlWebViewClient.this.f20784.stopLoading();
                HtmlWebViewClient.this.f20783.onFailed(MoPubErrorCode.UNSPECIFIED);
            }

            public void onFinishLoad() {
                HtmlWebViewClient.this.f20783.onLoaded(HtmlWebViewClient.this.f20784);
            }
        }).build().handleUrl(this.f20786, str, this.f20784.wasClicked());
        return true;
    }
}
