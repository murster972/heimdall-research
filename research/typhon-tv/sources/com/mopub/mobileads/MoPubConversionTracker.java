package com.mopub.mobileads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.SharedPreferencesHelper;
import com.mopub.common.TyphoonApp;
import com.mopub.network.TrackingRequest;
import com.mopub.volley.VolleyError;

public class MoPubConversionTracker {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public String f20805;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public String f20806;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public SharedPreferences f20807;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public Context f20808;

    private class ConversionUrlGenerator extends BaseUrlGenerator {
        private ConversionUrlGenerator() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m26695(String str) {
            m26450("id", str);
        }

        public String generateUrlString(String str) {
            m26454(str, TyphoonApp.CONVERSION_TRACKING_HANDLER);
            m26448("6");
            m26695(MoPubConversionTracker.this.f20806);
            m26447(ClientMetadata.getInstance(MoPubConversionTracker.this.f20808).getAppVersion());
            m26449();
            return m26452();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26694() {
        return this.f20807.getBoolean(this.f20805, false);
    }

    public void reportAppOpen(Context context) {
        if (context != null) {
            this.f20808 = context;
            this.f20806 = this.f20808.getPackageName();
            this.f20805 = this.f20806 + " tracked";
            this.f20807 = SharedPreferencesHelper.getSharedPreferences(this.f20808);
            if (!m26694()) {
                TrackingRequest.makeTrackingHttpRequest(new ConversionUrlGenerator().generateUrlString(TyphoonApp.HOST), this.f20808, (TrackingRequest.Listener) new TrackingRequest.Listener() {
                    public void onErrorResponse(VolleyError volleyError) {
                    }

                    @SuppressLint({"ApplySharedPref"})
                    public void onResponse(String str) {
                        MoPubConversionTracker.this.f20807.edit().putBoolean(MoPubConversionTracker.this.f20805, true).commit();
                    }
                });
            }
        }
    }
}
