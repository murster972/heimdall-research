package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;

public class VastExtensionParentXmlManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20848;

    VastExtensionParentXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20848 = node;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<VastExtensionXmlManager> m26740() {
        ArrayList arrayList = new ArrayList();
        List<Node> matchingChildNodes = XmlUtils.getMatchingChildNodes(this.f20848, "Extension");
        if (matchingChildNodes != null) {
            for (Node vastExtensionXmlManager : matchingChildNodes) {
                arrayList.add(new VastExtensionXmlManager(vastExtensionXmlManager));
            }
        }
        return arrayList;
    }
}
