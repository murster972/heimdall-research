package com.mopub.mobileads.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtils {

    public interface NodeProcessor<T> {
        T process(Node node);
    }

    private XmlUtils() {
    }

    public static String getAttributeValue(Node node, String str) {
        Node namedItem;
        if (node == null || str == null || (namedItem = node.getAttributes().getNamedItem(str)) == null) {
            return null;
        }
        return namedItem.getNodeValue();
    }

    public static Integer getAttributeValueAsInt(Node node, String str) {
        if (node == null || str == null) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(getAttributeValue(node, str)));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static <T> T getFirstMatchFromDocument(Document document, String str, String str2, String str3, NodeProcessor<T> nodeProcessor) {
        NodeList elementsByTagName;
        List asList;
        T process;
        if (document == null || (elementsByTagName = document.getElementsByTagName(str)) == null) {
            return null;
        }
        if (str3 == null) {
            asList = null;
        } else {
            asList = Arrays.asList(new String[]{str3});
        }
        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            Node item = elementsByTagName.item(i);
            if (item != null && nodeMatchesAttributeFilter(item, str2, asList) && (process = nodeProcessor.process(item)) != null) {
                return process;
            }
        }
        return null;
    }

    public static Node getFirstMatchingChildNode(Node node, String str) {
        return getFirstMatchingChildNode(node, str, (String) null, (List<String>) null);
    }

    public static Node getFirstMatchingChildNode(Node node, String str, String str2, List<String> list) {
        List<Node> matchingChildNodes;
        if (node == null || str == null || (matchingChildNodes = getMatchingChildNodes(node, str, str2, list)) == null || matchingChildNodes.isEmpty()) {
            return null;
        }
        return matchingChildNodes.get(0);
    }

    public static String getFirstMatchingStringData(Document document, String str) {
        return getFirstMatchingStringData(document, str, (String) null, (String) null);
    }

    public static String getFirstMatchingStringData(Document document, String str, String str2, String str3) {
        return (String) getFirstMatchFromDocument(document, str, str2, str3, new NodeProcessor<String>() {
            public String process(Node node) {
                return XmlUtils.getNodeValue(node);
            }
        });
    }

    public static <T> List<T> getListFromDocument(Document document, String str, String str2, String str3, NodeProcessor<T> nodeProcessor) {
        NodeList elementsByTagName;
        List asList;
        T process;
        ArrayList arrayList = new ArrayList();
        if (!(document == null || (elementsByTagName = document.getElementsByTagName(str)) == null)) {
            if (str3 == null) {
                asList = null;
            } else {
                asList = Arrays.asList(new String[]{str3});
            }
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                Node item = elementsByTagName.item(i);
                if (!(item == null || !nodeMatchesAttributeFilter(item, str2, asList) || (process = nodeProcessor.process(item)) == null)) {
                    arrayList.add(process);
                }
            }
        }
        return arrayList;
    }

    public static List<Node> getMatchingChildNodes(Node node, String str) {
        return getMatchingChildNodes(node, str, (String) null, (List<String>) null);
    }

    public static List<Node> getMatchingChildNodes(Node node, String str, String str2, List<String> list) {
        if (node == null || str == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            if (item.getNodeName().equals(str) && nodeMatchesAttributeFilter(item, str2, list)) {
                arrayList.add(item);
            }
        }
        return arrayList;
    }

    public static String getNodeValue(Node node) {
        if (node == null || node.getFirstChild() == null || node.getFirstChild().getNodeValue() == null) {
            return null;
        }
        return node.getFirstChild().getNodeValue().trim();
    }

    public static List<Node> getNodesWithElementAndAttribute(Document document, String str, String str2, String str3) {
        return getListFromDocument(document, str, str2, str3, new NodeProcessor<Node>() {
            public Node process(Node node) {
                return node;
            }
        });
    }

    public static List<String> getStringDataAsList(Document document, String str) {
        return getStringDataAsList(document, str, (String) null, (String) null);
    }

    public static List<String> getStringDataAsList(Document document, String str, String str2, String str3) {
        return getListFromDocument(document, str, str2, str3, new NodeProcessor<String>() {
            public String process(Node node) {
                return XmlUtils.getNodeValue(node);
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        r1 = r0.getNamedItem(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean nodeMatchesAttributeFilter(org.w3c.dom.Node r4, java.lang.String r5, java.util.List<java.lang.String> r6) {
        /*
            r2 = 1
            if (r5 == 0) goto L_0x0005
            if (r6 != 0) goto L_0x0006
        L_0x0005:
            return r2
        L_0x0006:
            org.w3c.dom.NamedNodeMap r0 = r4.getAttributes()
            if (r0 == 0) goto L_0x001c
            org.w3c.dom.Node r1 = r0.getNamedItem(r5)
            if (r1 == 0) goto L_0x001c
            java.lang.String r3 = r1.getNodeValue()
            boolean r3 = r6.contains(r3)
            if (r3 != 0) goto L_0x0005
        L_0x001c:
            r2 = 0
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.util.XmlUtils.nodeMatchesAttributeFilter(org.w3c.dom.Node, java.lang.String, java.util.List):boolean");
    }
}
