package com.mopub.mobileads;

import android.content.Context;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.mopub.common.CacheService;
import com.mopub.common.Preconditions;
import com.mopub.common.util.AsyncTasks;
import com.mopub.mobileads.VastXmlManagerAggregator;
import com.mopub.mobileads.VideoDownloader;

public class VastManager implements VastXmlManagerAggregator.VastXmlManagerAggregatorListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f20859;

    /* renamed from: 连任  reason: contains not printable characters */
    private double f20860;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public VastManagerListener f20861;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f20862;

    /* renamed from: 齉  reason: contains not printable characters */
    private VastXmlManagerAggregator f20863;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f20864;

    public interface VastManagerListener {
        void onVastVideoConfigurationPrepared(VastVideoConfig vastVideoConfig);
    }

    public VastManager(Context context, boolean z) {
        m26777(context);
        this.f20864 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26777(Context context) {
        Preconditions.checkNotNull(context, "context cannot be null");
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        float f = context.getResources().getDisplayMetrics().density;
        if (f <= 0.0f) {
            f = 1.0f;
        }
        int max = Math.max(width, height);
        int min = Math.min(width, height);
        this.f20860 = ((double) max) / ((double) min);
        this.f20859 = (int) ((((float) max) / f) * (((float) min) / f));
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26779(VastVideoConfig vastVideoConfig) {
        Preconditions.checkNotNull(vastVideoConfig, "vastVideoConfig cannot be null");
        String networkMediaFileUrl = vastVideoConfig.getNetworkMediaFileUrl();
        if (!CacheService.containsKeyDiskCache(networkMediaFileUrl)) {
            return false;
        }
        vastVideoConfig.setDiskMediaFileUrl(CacheService.getFilePathDiskCache(networkMediaFileUrl));
        return true;
    }

    public void cancel() {
        if (this.f20863 != null) {
            this.f20863.cancel(true);
            this.f20863 = null;
        }
    }

    public void onAggregationComplete(final VastVideoConfig vastVideoConfig) {
        if (this.f20861 == null) {
            throw new IllegalStateException("mVastManagerListener cannot be null here. Did you call prepareVastVideoConfiguration()?");
        } else if (vastVideoConfig == null) {
            this.f20861.onVastVideoConfigurationPrepared((VastVideoConfig) null);
        } else {
            if (!TextUtils.isEmpty(this.f20862)) {
                vastVideoConfig.setDspCreativeId(this.f20862);
            }
            if (!this.f20864 || m26779(vastVideoConfig)) {
                this.f20861.onVastVideoConfigurationPrepared(vastVideoConfig);
                return;
            }
            VideoDownloader.cache(vastVideoConfig.getNetworkMediaFileUrl(), new VideoDownloader.VideoDownloaderListener() {
                public void onComplete(boolean z) {
                    if (!z || !VastManager.this.m26779(vastVideoConfig)) {
                        VastManager.this.f20861.onVastVideoConfigurationPrepared((VastVideoConfig) null);
                    } else {
                        VastManager.this.f20861.onVastVideoConfigurationPrepared(vastVideoConfig);
                    }
                }
            });
        }
    }

    public void prepareVastVideoConfiguration(String str, VastManagerListener vastManagerListener, String str2, Context context) {
        Preconditions.checkNotNull(vastManagerListener, "vastManagerListener cannot be null");
        Preconditions.checkNotNull(context, "context cannot be null");
        if (this.f20863 == null) {
            this.f20861 = vastManagerListener;
            this.f20863 = new VastXmlManagerAggregator(this, this.f20860, this.f20859, context.getApplicationContext());
            this.f20862 = str2;
            try {
                AsyncTasks.safeExecuteOnExecutor(this.f20863, str);
            } catch (Exception e) {
                this.f20861.onVastVideoConfigurationPrepared((VastVideoConfig) null);
            }
        }
    }
}
