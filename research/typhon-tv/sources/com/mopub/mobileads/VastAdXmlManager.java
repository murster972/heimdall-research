package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

class VastAdXmlManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20839;

    VastAdXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20839 = node;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public VastWrapperXmlManager m26721() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20839, "Wrapper");
        if (firstMatchingChildNode != null) {
            return new VastWrapperXmlManager(firstMatchingChildNode);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m26722() {
        return XmlUtils.getAttributeValue(this.f20839, "sequence");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public VastInLineXmlManager m26723() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20839, "InLine");
        if (firstMatchingChildNode != null) {
            return new VastInLineXmlManager(firstMatchingChildNode);
        }
        return null;
    }
}
