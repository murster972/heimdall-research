package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.CacheService;
import com.mopub.common.DataKeys;
import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.VastManager;
import com.mopub.mobileads.factories.VastManagerFactory;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class VastVideoInterstitial extends ResponseBodyInterstitial implements VastManager.VastManagerListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private VastManager f20888;

    /* renamed from: ʼ  reason: contains not printable characters */
    private VastVideoConfig f20889;

    /* renamed from: ʽ  reason: contains not printable characters */
    private JSONObject f20890;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f20891;

    /* renamed from: 麤  reason: contains not printable characters */
    private CustomEventInterstitial.CustomEventInterstitialListener f20892;

    VastVideoInterstitial() {
    }

    public void onInvalidate() {
        if (this.f20888 != null) {
            this.f20888.cancel();
        }
        super.onInvalidate();
    }

    public void onVastVideoConfigurationPrepared(VastVideoConfig vastVideoConfig) {
        if (vastVideoConfig == null) {
            this.f20892.onInterstitialFailed(MoPubErrorCode.VIDEO_DOWNLOAD_ERROR);
            return;
        }
        this.f20889 = vastVideoConfig;
        this.f20889.addVideoTrackers(this.f20890);
        this.f20892.onInterstitialLoaded();
    }

    public void showInterstitial() {
        MraidVideoPlayerActivity.m26642(this.f20838, this.f20889, this.f20837);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26802(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener) {
        this.f20892 = customEventInterstitialListener;
        if (!CacheService.initializeDiskCache(this.f20838)) {
            this.f20892.onInterstitialFailed(MoPubErrorCode.VIDEO_CACHE_ERROR);
            return;
        }
        this.f20888 = VastManagerFactory.create(this.f20838);
        this.f20888.prepareVastVideoConfiguration(this.f20891, this, this.f20835.getDspCreativeId(), this.f20838);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26803(Map<String, String> map) {
        this.f20891 = map.get(DataKeys.HTML_RESPONSE_BODY_KEY);
        String str = map.get(DataKeys.VIDEO_TRACKERS_KEY);
        if (!TextUtils.isEmpty(str)) {
            try {
                this.f20890 = new JSONObject(str);
            } catch (JSONException e) {
                this.f20890 = null;
            }
        }
    }
}
