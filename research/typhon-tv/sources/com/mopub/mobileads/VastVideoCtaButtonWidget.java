package com.mopub.mobileads;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.widget.AppCompatImageView;
import android.widget.RelativeLayout;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.CtaButtonDrawable;

public class VastVideoCtaButtonWidget extends AppCompatImageView {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f5743;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f5744;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f5745 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f5746;

    /* renamed from: 靐  reason: contains not printable characters */
    private final RelativeLayout.LayoutParams f5747;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f5748;

    /* renamed from: 齉  reason: contains not printable characters */
    private CtaButtonDrawable f5749;

    /* renamed from: 龘  reason: contains not printable characters */
    private final RelativeLayout.LayoutParams f5750;

    public VastVideoCtaButtonWidget(Context context, int i, boolean z, boolean z2) {
        super(context);
        this.f5743 = z;
        this.f5744 = z2;
        setId((int) Utils.generateUniqueId());
        int dipsToIntPixels = Dips.dipsToIntPixels(150.0f, context);
        int dipsToIntPixels2 = Dips.dipsToIntPixels(38.0f, context);
        int dipsToIntPixels3 = Dips.dipsToIntPixels(16.0f, context);
        this.f5749 = new CtaButtonDrawable(context);
        setImageDrawable(this.f5749);
        this.f5750 = new RelativeLayout.LayoutParams(dipsToIntPixels, dipsToIntPixels2);
        this.f5750.setMargins(dipsToIntPixels3, dipsToIntPixels3, dipsToIntPixels3, dipsToIntPixels3);
        this.f5750.addRule(8, i);
        this.f5750.addRule(7, i);
        this.f5747 = new RelativeLayout.LayoutParams(dipsToIntPixels, dipsToIntPixels2);
        this.f5747.setMargins(dipsToIntPixels3, dipsToIntPixels3, dipsToIntPixels3, dipsToIntPixels3);
        this.f5747.addRule(12);
        this.f5747.addRule(11);
        m6232();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m6232() {
        if (!this.f5744) {
            setVisibility(8);
        } else if (!this.f5748) {
            setVisibility(8);
        } else if (!this.f5746 || !this.f5743 || this.f5745) {
            switch (getResources().getConfiguration().orientation) {
                case 0:
                    setLayoutParams(this.f5747);
                    break;
                case 1:
                    setLayoutParams(this.f5747);
                    break;
                case 2:
                    setLayoutParams(this.f5750);
                    break;
                case 3:
                    setLayoutParams(this.f5747);
                    break;
                default:
                    setLayoutParams(this.f5747);
                    break;
            }
            setVisibility(8);
        } else {
            setVisibility(8);
        }
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public String getCtaText() {
        return this.f5749.getCtaText();
    }

    /* access modifiers changed from: package-private */
    public boolean getHasSocialActions() {
        return this.f5745;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        m6232();
    }

    /* access modifiers changed from: package-private */
    public void setHasSocialActions(boolean z) {
        this.f5745 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6233() {
        this.f5748 = true;
        this.f5746 = true;
        m6232();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6234() {
        this.f5748 = true;
        m6232();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6235(String str) {
        this.f5749.setCtaText(str);
    }
}
