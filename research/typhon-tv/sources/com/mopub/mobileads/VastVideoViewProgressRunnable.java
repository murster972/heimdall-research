package com.mopub.mobileads;

import android.os.Handler;
import com.mopub.common.Preconditions;
import com.mopub.network.TrackingRequest;
import java.util.ArrayList;
import java.util.List;

public class VastVideoViewProgressRunnable extends RepeatingHandlerRunnable {

    /* renamed from: 麤  reason: contains not printable characters */
    private final VastVideoConfig f20948;

    /* renamed from: 齉  reason: contains not printable characters */
    private final VastVideoViewController f20949;

    public VastVideoViewProgressRunnable(VastVideoViewController vastVideoViewController, VastVideoConfig vastVideoConfig, Handler handler) {
        super(handler);
        Preconditions.checkNotNull(vastVideoViewController);
        Preconditions.checkNotNull(vastVideoConfig);
        this.f20949 = vastVideoViewController;
        this.f20948 = vastVideoConfig;
    }

    public void doWork() {
        int r4 = this.f20949.m26847();
        int r0 = this.f20949.m26848();
        this.f20949.m26845();
        if (r4 > 0) {
            List<VastTracker> untriggeredTrackersBefore = this.f20948.getUntriggeredTrackersBefore(r0, r4);
            if (!untriggeredTrackersBefore.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (VastTracker next : untriggeredTrackersBefore) {
                    arrayList.add(next.getTrackingUrl());
                    next.setTracked();
                }
                TrackingRequest.makeTrackingHttpRequest((Iterable<String>) new VastMacroHelper(arrayList).withAssetUri(this.f20949.m26863()).withContentPlayHead(Integer.valueOf(r0)).getUris(), this.f20949.m26645());
            }
            this.f20949.m26859(r0);
        }
    }
}
