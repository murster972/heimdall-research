package com.mopub.mobileads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.Pinkamena;
import com.mopub.common.AdReport;
import com.mopub.common.CreativeOrientation;
import com.mopub.common.DataKeys;
import com.mopub.common.IntentActions;
import com.mopub.common.util.DeviceUtils;
import com.mopub.mobileads.BaseInterstitialActivity;
import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.factories.HtmlInterstitialWebViewFactory;
import java.io.Serializable;

public class MoPubActivity extends BaseInterstitialActivity {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public HtmlInterstitialWebView f20802;

    class BroadcastingInterstitialListener implements CustomEventInterstitial.CustomEventInterstitialListener {
        BroadcastingInterstitialListener() {
        }

        public void onInterstitialClicked() {
            EventForwardingBroadcastReceiver.broadcastAction(MoPubActivity.this, MoPubActivity.this.m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_CLICK);
        }

        public void onInterstitialDismissed() {
        }

        public void onInterstitialFailed(MoPubErrorCode moPubErrorCode) {
            EventForwardingBroadcastReceiver.broadcastAction(MoPubActivity.this, MoPubActivity.this.m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_FAIL);
            MoPubActivity.this.finish();
        }

        public void onInterstitialLoaded() {
            HtmlInterstitialWebView r0 = MoPubActivity.this.f20802;
            String r1 = BaseInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_APPEAR.m26638();
            Pinkamena.DianePie();
        }

        public void onInterstitialShown() {
        }

        public void onLeaveApplication() {
        }
    }

    public static void start(Context context, String str, AdReport adReport, boolean z, String str2, String str3, CreativeOrientation creativeOrientation, long j) {
        try {
            context.startActivity(m26687(context, str, adReport, z, str2, str3, creativeOrientation, j));
        } catch (ActivityNotFoundException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Intent m26687(Context context, String str, AdReport adReport, boolean z, String str2, String str3, CreativeOrientation creativeOrientation, long j) {
        Intent intent = new Intent(context, MoPubActivity.class);
        intent.putExtra(DataKeys.HTML_RESPONSE_BODY_KEY, str);
        intent.putExtra(DataKeys.SCROLLABLE_KEY, z);
        intent.putExtra(DataKeys.CLICKTHROUGH_URL_KEY, str3);
        intent.putExtra(DataKeys.REDIRECT_URL_KEY, str2);
        intent.putExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, j);
        intent.putExtra(DataKeys.AD_REPORT_KEY, adReport);
        intent.putExtra(DataKeys.CREATIVE_ORIENTATION_KEY, creativeOrientation);
        intent.addFlags(268435456);
        return intent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m26689(Context context, AdReport adReport, final CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, String str) {
        HtmlInterstitialWebView create = HtmlInterstitialWebViewFactory.create(context, adReport, customEventInterstitialListener, false, (String) null, (String) null);
        create.m6210(false);
        create.m6211();
        create.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if ("mopub://finishLoad".equals(str)) {
                    customEventInterstitialListener.onInterstitialLoaded();
                    return true;
                } else if (!"mopub://failLoad".equals(str)) {
                    return true;
                } else {
                    customEventInterstitialListener.onInterstitialFailed((MoPubErrorCode) null);
                    return true;
                }
            }
        });
        create.m26631(str);
    }

    public View getAdView() {
        Intent intent = getIntent();
        boolean booleanExtra = intent.getBooleanExtra(DataKeys.SCROLLABLE_KEY, false);
        String stringExtra = intent.getStringExtra(DataKeys.REDIRECT_URL_KEY);
        String stringExtra2 = intent.getStringExtra(DataKeys.CLICKTHROUGH_URL_KEY);
        String stringExtra3 = intent.getStringExtra(DataKeys.HTML_RESPONSE_BODY_KEY);
        this.f20802 = HtmlInterstitialWebViewFactory.create(getApplicationContext(), this.f20726, new BroadcastingInterstitialListener(), booleanExtra, stringExtra, stringExtra2);
        this.f20802.m26631(stringExtra3);
        return this.f20802;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Serializable serializableExtra = getIntent().getSerializableExtra(DataKeys.CREATIVE_ORIENTATION_KEY);
        DeviceUtils.lockOrientation(this, (serializableExtra == null || !(serializableExtra instanceof CreativeOrientation)) ? CreativeOrientation.UNDEFINED : (CreativeOrientation) serializableExtra);
        EventForwardingBroadcastReceiver.broadcastAction(this, m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_SHOW);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        HtmlInterstitialWebView htmlInterstitialWebView = this.f20802;
        String r1 = BaseInterstitialActivity.JavaScriptWebViewCallbacks.WEB_VIEW_DID_CLOSE.m26638();
        Pinkamena.DianePie();
        this.f20802.destroy();
        EventForwardingBroadcastReceiver.broadcastAction(this, m26637().longValue(), IntentActions.ACTION_INTERSTITIAL_DISMISS);
        super.onDestroy();
    }
}
