package com.mopub.mobileads;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.AppCompatImageView;
import android.widget.RelativeLayout;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import com.mopub.mobileads.resource.DrawableTyphoonApp;

public class VastVideoGradientStripWidget extends AppCompatImageView {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f20884;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f20885;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f20886;

    /* renamed from: 龘  reason: contains not printable characters */
    DeviceUtils.ForceOrientation f20887;

    public VastVideoGradientStripWidget(Context context, GradientDrawable.Orientation orientation, DeviceUtils.ForceOrientation forceOrientation, boolean z, int i, int i2, int i3) {
        super(context);
        this.f20887 = forceOrientation;
        this.f20884 = i;
        this.f20886 = z;
        setImageDrawable(new GradientDrawable(orientation, new int[]{DrawableTyphoonApp.GradientStrip.START_COLOR, DrawableTyphoonApp.GradientStrip.END_COLOR}));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, Dips.dipsToIntPixels(72.0f, context));
        layoutParams.addRule(i2, i3);
        setLayoutParams(layoutParams);
        m26800();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26800() {
        if (this.f20885) {
            if (this.f20886) {
                setVisibility(this.f20884);
            } else {
                setVisibility(8);
            }
        } else if (this.f20887 == DeviceUtils.ForceOrientation.FORCE_PORTRAIT) {
            setVisibility(4);
        } else if (this.f20887 == DeviceUtils.ForceOrientation.FORCE_LANDSCAPE) {
            setVisibility(0);
        } else {
            switch (getResources().getConfiguration().orientation) {
                case 0:
                    setVisibility(4);
                    return;
                case 1:
                    setVisibility(4);
                    return;
                case 2:
                    setVisibility(0);
                    return;
                case 3:
                    setVisibility(4);
                    return;
                default:
                    setVisibility(4);
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        m26800();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26801() {
        this.f20885 = true;
        m26800();
    }
}
