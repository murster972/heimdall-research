package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.Pinkamena;
import com.aerserv.sdk.AerServConfig;
import com.aerserv.sdk.AerServEvent;
import com.aerserv.sdk.AerServEventListener;
import com.aerserv.sdk.AerServInterstitial;
import com.aerserv.sdk.AerServSdk;
import com.mopub.mobileads.CustomEventInterstitial;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import java.util.List;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

public class AerServCustomEventInterstitial2 extends CustomEventInterstitial {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f20710 = AerServCustomEventInterstitial2.class.getSimpleName();

    /* renamed from: 靐  reason: contains not printable characters */
    private AerServInterstitial f20711 = null;

    /* renamed from: com.mopub.mobileads.AerServCustomEventInterstitial2$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f20717 = new int[AerServEvent.values().length];

        static {
            try {
                f20717[AerServEvent.PRELOAD_READY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f20717[AerServEvent.AD_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f20717[AerServEvent.AD_IMPRESSION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f20717[AerServEvent.AD_CLICKED.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f20717[AerServEvent.AD_DISMISSED.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void loadInterstitial(final Context context, final CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> map, Map<String, String> map2) {
        if (customEventInterstitialListener != null) {
            try {
                if (TyphoonApp.f5874 != 0) {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.NO_FILL);
                } else if (context == null) {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                } else if (!(context instanceof Activity)) {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                } else {
                    if (!TyphoonApp.f5864) {
                        try {
                            AerServSdk.init((Activity) context, "1004083");
                        } catch (Throwable th) {
                            Logger.m6281(th, new boolean[0]);
                        }
                        TyphoonApp.f5864 = true;
                    }
                    String string = AerServPluginUtil.getString("placement", map, map2);
                    if (string == null) {
                        Log.w(f20710, "Cannot load AerServ ad because placement is missing");
                        customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                        return;
                    }
                    AerServConfig aerServConfig = new AerServConfig(context, string);
                    aerServConfig.setPreload(true);
                    aerServConfig.enableBackButton(true);
                    aerServConfig.setDebug(false);
                    Integer integer = AerServPluginUtil.getInteger("timeoutMillis", map, map2);
                    if (integer != null) {
                        aerServConfig.setTimeout(integer.intValue());
                    }
                    List<String> stringList = AerServPluginUtil.getStringList(PubnativeRequest.Parameters.KEYWORDS, map, map2);
                    if (stringList != null) {
                        aerServConfig.setKeywords(stringList);
                    }
                    aerServConfig.setEventListener(new AerServEventListener() {
                        public void onAerServEvent(final AerServEvent aerServEvent, List<Object> list) {
                            new Handler(context.getMainLooper()).post(new Runnable() {
                                public void run() {
                                    switch (AnonymousClass2.f20717[aerServEvent.ordinal()]) {
                                        case 1:
                                            customEventInterstitialListener.onInterstitialLoaded();
                                            return;
                                        case 2:
                                            customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.NETWORK_NO_FILL);
                                            return;
                                        case 3:
                                            customEventInterstitialListener.onInterstitialShown();
                                            return;
                                        case 4:
                                            customEventInterstitialListener.onInterstitialClicked();
                                            return;
                                        case 5:
                                            customEventInterstitialListener.onInterstitialDismissed();
                                            return;
                                        default:
                                            Log.d(AerServCustomEventInterstitial2.f20710, "The following AerServ interstitial ad event cannot be mapped and will be ignored: " + aerServEvent.name());
                                            return;
                                    }
                                }
                            });
                        }
                    });
                    this.f20711 = new AerServInterstitial(aerServConfig);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
                customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        if (this.f20711 != null) {
            this.f20711.kill();
            this.f20711 = null;
        }
    }

    /* access modifiers changed from: protected */
    public void showInterstitial() {
        if (this.f20711 != null) {
            AerServInterstitial aerServInterstitial = this.f20711;
            Pinkamena.DianePie();
        }
    }
}
