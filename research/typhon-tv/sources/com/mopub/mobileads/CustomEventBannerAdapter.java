package com.mopub.mobileads;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import com.mopub.common.AdReport;
import com.mopub.common.DataKeys;
import com.mopub.common.Preconditions;
import com.mopub.common.util.ReflectionTarget;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.factories.CustomEventBannerFactory;
import java.util.Map;
import java.util.TreeMap;

public class CustomEventBannerAdapter implements CustomEventBanner.CustomEventBannerListener {
    public static final int DEFAULT_BANNER_TIMEOUT_DELAY = 10000;

    /* renamed from: ʻ  reason: contains not printable characters */
    private CustomEventBanner f20733;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Map<String, Object> f20734;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Map<String, String> f20735;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f20736;

    /* renamed from: 连任  reason: contains not printable characters */
    private Context f20737;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Runnable f20738;

    /* renamed from: 麤  reason: contains not printable characters */
    private MoPubView f20739;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f20740;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Handler f20741 = new Handler();

    public CustomEventBannerAdapter(MoPubView moPubView, String str, Map<String, String> map, long j, AdReport adReport) {
        Preconditions.checkNotNull(map);
        this.f20739 = moPubView;
        this.f20737 = moPubView.getContext();
        this.f20738 = new Runnable() {
            public void run() {
                CustomEventBannerAdapter.this.onBannerFailed(MoPubErrorCode.NETWORK_TIMEOUT);
                CustomEventBannerAdapter.this.invalidate();
            }
        };
        try {
            this.f20733 = CustomEventBannerFactory.create(str);
            this.f20735 = new TreeMap(map);
            this.f20734 = this.f20739.getLocalExtras();
            if (this.f20739.getLocation() != null) {
                this.f20734.put("location", this.f20739.getLocation());
            }
            this.f20734.put(DataKeys.BROADCAST_IDENTIFIER_KEY, Long.valueOf(j));
            this.f20734.put(DataKeys.AD_REPORT_KEY, adReport);
            this.f20734.put(DataKeys.AD_WIDTH, Integer.valueOf(this.f20739.getAdWidth()));
            this.f20734.put(DataKeys.AD_HEIGHT, Integer.valueOf(this.f20739.getAdHeight()));
        } catch (Exception e) {
            this.f20739.m6222(MoPubErrorCode.ADAPTER_NOT_FOUND);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26659() {
        this.f20741.removeCallbacks(this.f20738);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private int m26660() {
        if (this.f20739 == null || this.f20739.getAdTimeoutDelay() == null || this.f20739.getAdTimeoutDelay().intValue() < 0) {
            return 10000;
        }
        return this.f20739.getAdTimeoutDelay().intValue() * 1000;
    }

    /* access modifiers changed from: package-private */
    @ReflectionTarget
    public void invalidate() {
        if (this.f20733 != null) {
            try {
                this.f20733.m26657();
            } catch (Exception e) {
            }
        }
        this.f20737 = null;
        this.f20733 = null;
        this.f20734 = null;
        this.f20735 = null;
        this.f20740 = true;
    }

    /* access modifiers changed from: package-private */
    @ReflectionTarget
    public void loadAd() {
        if (!m26661() && this.f20733 != null) {
            this.f20741.postDelayed(this.f20738, (long) m26660());
            try {
                this.f20733.m26658(this.f20737, this, this.f20734, this.f20735);
            } catch (Exception e) {
                onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        }
    }

    public void onBannerClicked() {
        if (!m26661() && this.f20739 != null) {
            this.f20739.m6221();
        }
    }

    public void onBannerCollapsed() {
        if (!m26661()) {
            this.f20739.setAutorefreshEnabled(this.f20736);
            this.f20739.m6217();
        }
    }

    public void onBannerExpanded() {
        if (!m26661()) {
            this.f20736 = this.f20739.getAutorefreshEnabled();
            this.f20739.setAutorefreshEnabled(false);
            this.f20739.m6220();
        }
    }

    public void onBannerFailed(MoPubErrorCode moPubErrorCode) {
        if (!m26661() && this.f20739 != null) {
            if (moPubErrorCode == null) {
                moPubErrorCode = MoPubErrorCode.UNSPECIFIED;
            }
            m26659();
            this.f20739.m6222(moPubErrorCode);
        }
    }

    public void onBannerLoaded(View view) {
        if (!m26661()) {
            m26659();
            if (this.f20739 != null) {
                this.f20739.m6219();
                this.f20739.setAdContentView(view);
                if (!(view instanceof HtmlBannerWebView)) {
                    this.f20739.m6224();
                }
            }
        }
    }

    public void onLeaveApplication() {
        onBannerClicked();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26661() {
        return this.f20740;
    }
}
