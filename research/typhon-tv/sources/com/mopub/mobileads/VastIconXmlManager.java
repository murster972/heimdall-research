package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.common.util.Strings;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;

public class VastIconXmlManager {
    public static final String DURATION = "duration";
    public static final String HEIGHT = "height";
    public static final String ICON_CLICKS = "IconClicks";
    public static final String ICON_CLICK_THROUGH = "IconClickThrough";
    public static final String ICON_CLICK_TRACKING = "IconClickTracking";
    public static final String ICON_VIEW_TRACKING = "IconViewTracking";
    public static final String OFFSET = "offset";
    public static final String WIDTH = "width";

    /* renamed from: 靐  reason: contains not printable characters */
    private final VastResourceXmlManager f20853;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20854;

    VastIconXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20854 = node;
        this.f20853 = new VastResourceXmlManager(node);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public List<VastTracker> m26751() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20854, ICON_CLICKS);
        ArrayList arrayList = new ArrayList();
        if (firstMatchingChildNode != null) {
            for (Node nodeValue : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, ICON_CLICK_TRACKING)) {
                String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
                if (nodeValue2 != null) {
                    arrayList.add(new VastTracker(nodeValue2));
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public String m26752() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20854, ICON_CLICKS);
        if (firstMatchingChildNode == null) {
            return null;
        }
        return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(firstMatchingChildNode, ICON_CLICK_THROUGH));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public List<VastTracker> m26753() {
        List<Node> matchingChildNodes = XmlUtils.getMatchingChildNodes(this.f20854, ICON_VIEW_TRACKING);
        ArrayList arrayList = new ArrayList();
        for (Node nodeValue : matchingChildNodes) {
            String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
            if (nodeValue2 != null) {
                arrayList.add(new VastTracker(nodeValue2));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public VastResourceXmlManager m26754() {
        return this.f20853;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Integer m26755() {
        return XmlUtils.getAttributeValueAsInt(this.f20854, HEIGHT);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public Integer m26756() {
        try {
            return Strings.parseAbsoluteOffset(XmlUtils.getAttributeValue(this.f20854, DURATION));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public Integer m26757() {
        try {
            return Strings.parseAbsoluteOffset(XmlUtils.getAttributeValue(this.f20854, OFFSET));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Integer m26758() {
        return XmlUtils.getAttributeValueAsInt(this.f20854, WIDTH);
    }
}
