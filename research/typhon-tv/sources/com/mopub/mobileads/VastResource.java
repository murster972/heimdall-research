package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

class VastResource implements Serializable {
    private static final long serialVersionUID = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final List<String> f20868 = Arrays.asList(new String[]{"application/x-javascript"});

    /* renamed from: 龘  reason: contains not printable characters */
    private static final List<String> f20869 = Arrays.asList(new String[]{"image/jpeg", "image/png", "image/bmp", "image/gif"});
    private CreativeType mCreativeType;
    private int mHeight;
    private String mResource;
    private Type mType;
    private int mWidth;

    enum CreativeType {
        NONE,
        IMAGE,
        JAVASCRIPT
    }

    enum Type {
        STATIC_RESOURCE,
        HTML_RESOURCE,
        IFRAME_RESOURCE
    }

    VastResource(String str, Type type, CreativeType creativeType, int i, int i2) {
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(type);
        Preconditions.checkNotNull(creativeType);
        this.mResource = str;
        this.mType = type;
        this.mCreativeType = creativeType;
        this.mWidth = i;
        this.mHeight = i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static VastResource m26784(VastResourceXmlManager vastResourceXmlManager, Type type, int i, int i2) {
        String str;
        CreativeType creativeType;
        Preconditions.checkNotNull(vastResourceXmlManager);
        Preconditions.checkNotNull(type);
        String r7 = vastResourceXmlManager.m26787();
        String r6 = vastResourceXmlManager.m26786();
        String r8 = vastResourceXmlManager.m26788();
        String r9 = vastResourceXmlManager.m26785();
        if (type == Type.STATIC_RESOURCE && r8 != null && r9 != null && (f20869.contains(r9) || f20868.contains(r9))) {
            str = r8;
            creativeType = f20869.contains(r9) ? CreativeType.IMAGE : CreativeType.JAVASCRIPT;
        } else if (type == Type.HTML_RESOURCE && r6 != null) {
            str = r6;
            creativeType = CreativeType.NONE;
        } else if (type != Type.IFRAME_RESOURCE || r7 == null) {
            return null;
        } else {
            str = r7;
            creativeType = CreativeType.NONE;
        }
        return new VastResource(str, type, creativeType, i, i2);
    }

    public String getCorrectClickThroughUrl(String str, String str2) {
        switch (this.mType) {
            case STATIC_RESOURCE:
                if (CreativeType.IMAGE == this.mCreativeType) {
                    return str;
                }
                if (CreativeType.JAVASCRIPT != this.mCreativeType) {
                    return null;
                }
                return str2;
            case HTML_RESOURCE:
            case IFRAME_RESOURCE:
                return str2;
            default:
                return null;
        }
    }

    public CreativeType getCreativeType() {
        return this.mCreativeType;
    }

    public String getResource() {
        return this.mResource;
    }

    public Type getType() {
        return this.mType;
    }

    public void initializeWebView(VastWebView vastWebView) {
        Preconditions.checkNotNull(vastWebView);
        if (this.mType == Type.IFRAME_RESOURCE) {
            vastWebView.m26867("<iframe frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" style=\"border: 0px; margin: 0px;\" width=\"" + this.mWidth + "\" height=\"" + this.mHeight + "\" src=\"" + this.mResource + "\"></iframe>");
        } else if (this.mType == Type.HTML_RESOURCE) {
            vastWebView.m26867(this.mResource);
        } else if (this.mType != Type.STATIC_RESOURCE) {
        } else {
            if (this.mCreativeType == CreativeType.IMAGE) {
                vastWebView.m26867("<html><head></head><body style=\"margin:0;padding:0\"><img src=\"" + this.mResource + "\" width=\"100%\" style=\"max-width:100%;max-height:100%;\" /></body></html>");
            } else if (this.mCreativeType == CreativeType.JAVASCRIPT) {
                vastWebView.m26867("<script src=\"" + this.mResource + "\"></script>");
            }
        }
    }
}
