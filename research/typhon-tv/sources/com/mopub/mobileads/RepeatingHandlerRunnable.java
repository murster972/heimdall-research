package com.mopub.mobileads;

import android.os.Handler;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;

public abstract class RepeatingHandlerRunnable implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    protected volatile long f5732;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile boolean f5733;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final Handler f5734;

    public RepeatingHandlerRunnable(Handler handler) {
        Preconditions.checkNotNull(handler);
        this.f5734 = handler;
    }

    public abstract void doWork();

    @Deprecated
    @VisibleForTesting
    public boolean isRunning() {
        return this.f5733;
    }

    public void run() {
        if (this.f5733) {
            doWork();
            this.f5734.postDelayed(this, this.f5732);
        }
    }

    public void startRepeating(long j) {
        Preconditions.checkArgument(j > 0, "intervalMillis must be greater than 0. Saw: %d", Long.valueOf(j));
        this.f5732 = j;
        if (!this.f5733) {
            this.f5733 = true;
            this.f5734.post(this);
        }
    }

    public void stop() {
        this.f5733 = false;
    }
}
