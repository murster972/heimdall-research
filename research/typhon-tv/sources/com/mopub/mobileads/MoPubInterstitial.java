package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.text.TextUtils;
import com.Pinkamena;
import com.mopub.common.AdFormat;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.mobileads.AdTypeTranslator;
import com.mopub.mobileads.CustomEventInterstitialAdapter;
import com.mopub.mobileads.MoPubView;
import com.mopub.mobileads.factories.CustomEventInterstitialAdapterFactory;
import java.util.Map;

public class MoPubInterstitial implements CustomEventInterstitialAdapter.CustomEventInterstitialAdapterListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Handler f20812;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public volatile InterstitialState f20813;

    /* renamed from: 连任  reason: contains not printable characters */
    private Activity f20814;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public MoPubInterstitialView f20815 = new MoPubInterstitialView(this.f20814);
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public InterstitialAdListener f20816;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public CustomEventInterstitialAdapter f20817;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Runnable f20818;

    public interface InterstitialAdListener {
        void onInterstitialClicked(MoPubInterstitial moPubInterstitial);

        void onInterstitialDismissed(MoPubInterstitial moPubInterstitial);

        void onInterstitialFailed(MoPubInterstitial moPubInterstitial, MoPubErrorCode moPubErrorCode);

        void onInterstitialLoaded(MoPubInterstitial moPubInterstitial);

        void onInterstitialShown(MoPubInterstitial moPubInterstitial);
    }

    @VisibleForTesting
    enum InterstitialState {
        IDLE,
        LOADING,
        READY,
        SHOWING,
        DESTROYED
    }

    public class MoPubInterstitialView extends MoPubView {
        public MoPubInterstitialView(Context context) {
            super(context);
            setAutorefreshEnabled(false);
        }

        public AdFormat getAdFormat() {
            return AdFormat.INTERSTITIAL;
        }

        public String getCustomEventClassName() {
            return this.f5728.getCustomEventClassName();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m26709() {
            if (this.f5728 != null) {
                this.f5728.m6187();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m26710(MoPubErrorCode moPubErrorCode) {
            boolean unused = MoPubInterstitial.this.m26704(InterstitialState.IDLE);
            if (MoPubInterstitial.this.f20816 != null) {
                MoPubInterstitial.this.f20816.onInterstitialFailed(MoPubInterstitial.this, moPubErrorCode);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m26711(String str, Map<String, String> map) {
            if (this.f5728 != null) {
                if (TextUtils.isEmpty(str)) {
                    m6222(MoPubErrorCode.ADAPTER_NOT_FOUND);
                    return;
                }
                if (MoPubInterstitial.this.f20817 != null) {
                    MoPubInterstitial.this.f20817.m26666();
                }
                CustomEventInterstitialAdapter unused = MoPubInterstitial.this.f20817 = CustomEventInterstitialAdapterFactory.create(MoPubInterstitial.this, str, map, this.f5728.getBroadcastIdentifier(), this.f5728.getAdReport());
                MoPubInterstitial.this.f20817.m26668(MoPubInterstitial.this);
                MoPubInterstitial.this.f20817.m26667();
            }
        }
    }

    public MoPubInterstitial(Activity activity, String str) {
        this.f20814 = activity;
        this.f20815.setAdUnitId(str);
        this.f20813 = InterstitialState.IDLE;
        this.f20812 = new Handler();
        this.f20818 = new Runnable() {
            public void run() {
                MoPubInterstitial.this.m26708(InterstitialState.IDLE, true);
                if (!InterstitialState.SHOWING.equals(MoPubInterstitial.this.f20813) && !InterstitialState.DESTROYED.equals(MoPubInterstitial.this.f20813)) {
                    MoPubInterstitial.this.f20815.m26710(MoPubErrorCode.EXPIRED);
                }
            }
        };
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m26696() {
        if (this.f20817 != null) {
            this.f20817.m26666();
            this.f20817 = null;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m26699() {
        if (this.f20817 != null) {
            this.f20817.m26664();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26701() {
        m26696();
        this.f20815.setBannerAdListener((MoPubView.BannerAdListener) null);
        this.f20815.destroy();
        this.f20812.removeCallbacks(this.f20818);
        this.f20813 = InterstitialState.DESTROYED;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26704(InterstitialState interstitialState) {
        return m26708(interstitialState, false);
    }

    public void destroy() {
        m26704(InterstitialState.DESTROYED);
    }

    public void forceRefresh() {
        m26708(InterstitialState.IDLE, true);
        m26708(InterstitialState.LOADING, true);
    }

    public Activity getActivity() {
        return this.f20814;
    }

    public InterstitialAdListener getInterstitialAdListener() {
        return this.f20816;
    }

    public String getKeywords() {
        return this.f20815.getKeywords();
    }

    public Map<String, Object> getLocalExtras() {
        return this.f20815.getLocalExtras();
    }

    public Location getLocation() {
        return this.f20815.getLocation();
    }

    public MoPubInterstitialView getMoPubInterstitialView() {
        return this.f20815;
    }

    public boolean getTesting() {
        return this.f20815.getTesting();
    }

    public boolean isReady() {
        return this.f20813 == InterstitialState.READY;
    }

    public void load() {
        m26704(InterstitialState.LOADING);
    }

    public void onCustomEventInterstitialClicked() {
        if (!m26707()) {
            this.f20815.m6221();
            if (this.f20816 != null) {
                this.f20816.onInterstitialClicked(this);
            }
        }
    }

    public void onCustomEventInterstitialDismissed() {
        if (!m26707()) {
            m26704(InterstitialState.IDLE);
            if (this.f20816 != null) {
                this.f20816.onInterstitialDismissed(this);
            }
        }
    }

    public void onCustomEventInterstitialFailed(MoPubErrorCode moPubErrorCode) {
        if (!m26707() && !this.f20815.m6222(moPubErrorCode)) {
            m26704(InterstitialState.IDLE);
        }
    }

    public void onCustomEventInterstitialLoaded() {
        if (!m26707()) {
            m26704(InterstitialState.READY);
            if (this.f20816 != null) {
                this.f20816.onInterstitialLoaded(this);
            }
        }
    }

    public void onCustomEventInterstitialShown() {
        if (!m26707()) {
            this.f20815.m26709();
            if (this.f20816 != null) {
                this.f20816.onInterstitialShown(this);
            }
        }
    }

    public void setInterstitialAdListener(InterstitialAdListener interstitialAdListener) {
        this.f20816 = interstitialAdListener;
    }

    public void setKeywords(String str) {
        this.f20815.setKeywords(str);
    }

    public void setLocalExtras(Map<String, Object> map) {
        this.f20815.setLocalExtras(map);
    }

    public void setTesting(boolean z) {
        this.f20815.setTesting(z);
    }

    public boolean show() {
        return m26704(InterstitialState.SHOWING);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Integer m26706() {
        return this.f20815.getAdTimeoutDelay();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26707() {
        return this.f20813 == InterstitialState.DESTROYED;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized boolean m26708(InterstitialState interstitialState, boolean z) {
        boolean z2 = true;
        synchronized (this) {
            Preconditions.checkNotNull(interstitialState);
            switch (this.f20813) {
                case LOADING:
                    switch (interstitialState) {
                        case LOADING:
                            if (!z) {
                            }
                            z2 = false;
                            break;
                        case SHOWING:
                            z2 = false;
                            break;
                        case DESTROYED:
                            m26701();
                            break;
                        case IDLE:
                            m26696();
                            this.f20813 = InterstitialState.IDLE;
                            break;
                        case READY:
                            this.f20813 = InterstitialState.READY;
                            if (AdTypeTranslator.CustomEventType.isMoPubSpecific(this.f20815.getCustomEventClassName())) {
                                this.f20812.postDelayed(this.f20818, 14400000);
                                break;
                            }
                            break;
                        default:
                            z2 = false;
                            break;
                    }
                case SHOWING:
                    switch (interstitialState) {
                        case LOADING:
                            if (!z) {
                            }
                            z2 = false;
                            break;
                        case SHOWING:
                            z2 = false;
                            break;
                        case DESTROYED:
                            m26701();
                            break;
                        case IDLE:
                            if (!z) {
                                m26696();
                                this.f20813 = InterstitialState.IDLE;
                                break;
                            } else {
                                z2 = false;
                                break;
                            }
                        default:
                            z2 = false;
                            break;
                    }
                case DESTROYED:
                    z2 = false;
                    break;
                case IDLE:
                    switch (interstitialState) {
                        case LOADING:
                            m26696();
                            this.f20813 = InterstitialState.LOADING;
                            if (!z) {
                                MoPubInterstitialView moPubInterstitialView = this.f20815;
                                Pinkamena.DianePie();
                                break;
                            } else {
                                this.f20815.forceRefresh();
                                break;
                            }
                        case SHOWING:
                            z2 = false;
                            break;
                        case DESTROYED:
                            m26701();
                            break;
                        default:
                            z2 = false;
                            break;
                    }
                case READY:
                    switch (interstitialState) {
                        case LOADING:
                            if (this.f20816 != null) {
                                this.f20816.onInterstitialLoaded(this);
                            }
                            z2 = false;
                            break;
                        case SHOWING:
                            m26699();
                            this.f20813 = InterstitialState.SHOWING;
                            this.f20812.removeCallbacks(this.f20818);
                            break;
                        case DESTROYED:
                            m26701();
                            break;
                        case IDLE:
                            if (!z) {
                                z2 = false;
                                break;
                            } else {
                                m26696();
                                this.f20813 = InterstitialState.IDLE;
                                break;
                            }
                        default:
                            z2 = false;
                            break;
                    }
                default:
                    z2 = false;
                    break;
            }
        }
        return z2;
    }
}
