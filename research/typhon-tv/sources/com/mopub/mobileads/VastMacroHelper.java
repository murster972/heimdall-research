package com.mopub.mobileads;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class VastMacroHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Map<VastMacro, String> f20857 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<String> f20858;

    public VastMacroHelper(List<String> list) {
        Preconditions.checkNotNull(list, "uris cannot be null");
        this.f20858 = list;
        this.f20857.put(VastMacro.CACHEBUSTING, m26774());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m26774() {
        return String.format(Locale.US, "%08d", new Object[]{Long.valueOf(Math.round(Math.random() * 1.0E8d))});
    }

    @SuppressLint({"DefaultLocale"})
    /* renamed from: 龘  reason: contains not printable characters */
    private String m26775(int i) {
        return String.format("%02d:%02d:%02d.%03d", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toHours((long) i)), Long.valueOf(TimeUnit.MILLISECONDS.toMinutes((long) i) % TimeUnit.HOURS.toMinutes(1)), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds((long) i) % TimeUnit.MINUTES.toSeconds(1)), Integer.valueOf(i % 1000)});
    }

    public List<String> getUris() {
        ArrayList arrayList = new ArrayList();
        for (String next : this.f20858) {
            if (!TextUtils.isEmpty(next)) {
                for (VastMacro vastMacro : VastMacro.values()) {
                    String str = this.f20857.get(vastMacro);
                    if (str == null) {
                        str = "";
                    }
                    next = next.replaceAll("\\[" + vastMacro.name() + "\\]", str);
                }
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public VastMacroHelper withAssetUri(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                str = URLEncoder.encode(str, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                MoPubLog.w("Failed to encode url", e);
            }
            this.f20857.put(VastMacro.ASSETURI, str);
        }
        return this;
    }

    public VastMacroHelper withContentPlayHead(Integer num) {
        if (num != null) {
            String r0 = m26775(num.intValue());
            if (!TextUtils.isEmpty(r0)) {
                this.f20857.put(VastMacro.CONTENTPLAYHEAD, r0);
            }
        }
        return this;
    }

    public VastMacroHelper withErrorCode(VastErrorCode vastErrorCode) {
        if (vastErrorCode != null) {
            this.f20857.put(VastMacro.ERRORCODE, vastErrorCode.m26739());
        }
        return this;
    }
}
