package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import com.Pinkamena;
import com.mobvista.msdk.out.InterstitialListener;
import com.mobvista.msdk.out.MVInterstitialHandler;
import com.mobvista.msdk.out.MobVistaSDKFactory;
import com.mobvista.msdk.system.MobVistaSDKImpl;
import com.mopub.mobileads.CustomEventInterstitial;
import com.typhoon.tv.BuildConfig;
import com.typhoon.tv.Logger;
import java.util.HashMap;
import java.util.Map;

public class MobvistaCustomEventInterstitial extends CustomEventInterstitial {

    /* renamed from: 靐  reason: contains not printable characters */
    private MVInterstitialHandler f20824;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public CustomEventInterstitial.CustomEventInterstitialListener f20825;

    /* access modifiers changed from: protected */
    public void loadInterstitial(Context context, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> map, Map<String, String> map2) {
        if (customEventInterstitialListener != null) {
            this.f20825 = customEventInterstitialListener;
            if (context == null) {
                try {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                }
            } else if (!(context instanceof Activity)) {
                customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
            } else {
                String str = map2.get("appKey");
                String str2 = map2.get("appId");
                String str3 = map2.get("unitId");
                if (str == null || str.isEmpty() || str2 == null || str2.isEmpty() || str3 == null || str3.isEmpty()) {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                    return;
                }
                MobVistaSDKImpl mobVistaSDK = MobVistaSDKFactory.getMobVistaSDK();
                Map mVConfigurationMap = mobVistaSDK.getMVConfigurationMap(str2, str);
                mVConfigurationMap.put("applicationID", BuildConfig.APPLICATION_ID);
                mobVistaSDK.init(mVConfigurationMap, ((Activity) context).getApplication());
                HashMap hashMap = new HashMap();
                hashMap.put("unit_id", str3);
                this.f20824 = new MVInterstitialHandler(context, hashMap);
                this.f20824.setInterstitialListener(new InterstitialListener() {
                    public void onInterstitialAdClick() {
                        MobvistaCustomEventInterstitial.this.f20825.onInterstitialClicked();
                    }

                    public void onInterstitialClosed() {
                        MobvistaCustomEventInterstitial.this.f20825.onInterstitialDismissed();
                    }

                    public void onInterstitialLoadFail(String str) {
                        MobvistaCustomEventInterstitial.this.f20825.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                    }

                    public void onInterstitialLoadSuccess() {
                        MobvistaCustomEventInterstitial.this.f20825.onInterstitialLoaded();
                    }

                    public void onInterstitialShowFail(String str) {
                        MobvistaCustomEventInterstitial.this.f20825.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                    }

                    public void onInterstitialShowSuccess() {
                        MobvistaCustomEventInterstitial.this.f20825.onInterstitialShown();
                    }
                });
                MVInterstitialHandler mVInterstitialHandler = this.f20824;
                Pinkamena.DianePie();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
    }

    /* access modifiers changed from: protected */
    public void showInterstitial() {
        if (this.f20824 != null) {
            try {
                MVInterstitialHandler mVInterstitialHandler = this.f20824;
                Pinkamena.DianePie();
            } catch (Exception e) {
                this.f20825.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        } else if (this.f20825 != null) {
            this.f20825.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
        }
    }
}
