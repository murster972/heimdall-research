package com.mopub.mobileads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.VersionCode;
import com.mopub.common.util.Views;
import com.mopub.mobileads.util.WebViews;

public class BaseWebView extends WebView {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f5724 = false;

    /* renamed from: 龘  reason: contains not printable characters */
    protected boolean f5725;

    public BaseWebView(Context context) {
        super(context.getApplicationContext());
        m6210(false);
        m6208();
        WebViews.setDisableJSChromeClient(this);
        if (!f5724) {
            m6209(getContext());
            f5724 = true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m6208() {
        getSettings().setAllowFileAccess(false);
        getSettings().setAllowContentAccess(false);
        if (Build.VERSION.SDK_INT >= 16) {
            getSettings().setAllowFileAccessFromFileURLs(false);
            getSettings().setAllowUniversalAccessFromFileURLs(false);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m6209(Context context) {
        if (Build.VERSION.SDK_INT == 19) {
            WebView webView = new WebView(context.getApplicationContext());
            webView.setBackgroundColor(0);
            webView.loadDataWithBaseURL((String) null, "", "text/html", "UTF-8", (String) null);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.width = 1;
            layoutParams.height = 1;
            layoutParams.type = 2005;
            layoutParams.flags = 16777240;
            layoutParams.format = -2;
            layoutParams.gravity = 8388659;
            ((WindowManager) context.getSystemService("window")).addView(webView, layoutParams);
        }
    }

    public void destroy() {
        this.f5725 = true;
        Views.removeFromParent(this);
        removeAllViews();
        super.destroy();
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setIsDestroyed(boolean z) {
        this.f5725 = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6210(boolean z) {
        if (!VersionCode.currentApiLevel().isAtLeast(VersionCode.JELLY_BEAN_MR2)) {
            if (z) {
                getSettings().setPluginState(WebSettings.PluginState.ON);
            } else {
                getSettings().setPluginState(WebSettings.PluginState.OFF);
            }
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6211() {
        getSettings().setJavaScriptEnabled(true);
        getSettings().setDomStorageEnabled(true);
        getSettings().setAppCacheEnabled(true);
        getSettings().setAppCachePath(getContext().getCacheDir().getAbsolutePath());
    }
}
