package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import com.apptracker.android.listener.AppModuleListener;
import com.apptracker.android.track.AppTracker;
import com.mopub.mobileads.CustomEventInterstitial;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TyphoonApp;
import java.util.Map;

public class LeadBoltCustomEventInterstitial extends CustomEventInterstitial {

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean f20792 = false;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static CustomEventInterstitial.CustomEventInterstitialListener f20793;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Handler f20794 = new Handler();

    /* renamed from: 齉  reason: contains not printable characters */
    private Context f20795;

    /* access modifiers changed from: protected */
    public void loadInterstitial(Context context, CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, Map<String, Object> map, Map<String, String> map2) {
        if (customEventInterstitialListener != null) {
            try {
                if (TyphoonApp.f5874 != 0) {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.NO_FILL);
                } else if (context == null) {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                } else if (!(context instanceof Activity)) {
                    customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                } else {
                    this.f20795 = context;
                    f20793 = customEventInterstitialListener;
                    String str = map2.get("api_key");
                    if (str == null || str.isEmpty() || str.contains("debug")) {
                        f20793.onInterstitialFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                        return;
                    }
                    AppTracker.setModuleListener(new AppModuleListener() {
                        public void onModuleCached(String str) {
                            LeadBoltCustomEventInterstitial.this.f20794.post(new Runnable() {
                                public void run() {
                                    LeadBoltCustomEventInterstitial.f20793.onInterstitialLoaded();
                                }
                            });
                        }

                        public void onModuleClicked(String str) {
                            LeadBoltCustomEventInterstitial.this.f20794.post(new Runnable() {
                                public void run() {
                                    LeadBoltCustomEventInterstitial.f20793.onInterstitialClicked();
                                }
                            });
                        }

                        public void onModuleClosed(String str, boolean z) {
                            LeadBoltCustomEventInterstitial.this.f20794.post(new Runnable() {
                                public void run() {
                                    LeadBoltCustomEventInterstitial.f20793.onInterstitialDismissed();
                                }
                            });
                        }

                        public void onModuleFailed(String str, String str2, boolean z) {
                            LeadBoltCustomEventInterstitial.this.f20794.post(new Runnable() {
                                public void run() {
                                    LeadBoltCustomEventInterstitial.f20793.onInterstitialFailed(MoPubErrorCode.NO_FILL);
                                }
                            });
                        }

                        public void onModuleLoaded(String str) {
                            LeadBoltCustomEventInterstitial.this.f20794.post(new Runnable() {
                                public void run() {
                                    LeadBoltCustomEventInterstitial.f20793.onInterstitialShown();
                                }
                            });
                        }
                    });
                    if (!f20792) {
                        AppTracker.setFramework("mopub");
                        AppTracker.startSession(this.f20795, str, AppTracker.ENABLE_AUTO_CACHE);
                        f20792 = true;
                    }
                    AppTracker.loadModuleToCache(context, "inapp");
                }
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
                customEventInterstitialListener.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onInvalidate() {
        try {
            AppTracker.setModuleListener((AppModuleListener) null);
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
        this.f20795 = null;
    }

    /* access modifiers changed from: protected */
    public void showInterstitial() {
        try {
            if (AppTracker.isAdReady("inapp")) {
                AppTracker.loadModule(this.f20795, "inapp");
            } else if (f20793 != null) {
                f20793.onInterstitialFailed(MoPubErrorCode.NO_FILL);
            }
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
            f20793.onInterstitialFailed(MoPubErrorCode.INTERNAL_ERROR);
        }
    }
}
