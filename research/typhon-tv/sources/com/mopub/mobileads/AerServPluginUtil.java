package com.mopub.mobileads;

import android.text.TextUtils;
import android.util.Log;
import com.mopub.common.util.Json;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class AerServPluginUtil {
    public static final String LOG_TAG = AerServPluginUtil.class.getSimpleName();

    public static Integer getInteger(String str, Map<String, Object> map, Map<String, String> map2) {
        if (!(map2 == null || map2.get(str) == null)) {
            try {
                return Integer.valueOf(Integer.parseInt(map2.get(str)));
            } catch (NumberFormatException e) {
                Log.d(LOG_TAG, "Cannot parse '" + map2.get(str) + "' in serverExtras to Integer.  Trying from localExtras instead.");
            }
        }
        if (map == null || map.get(str) == null || !(map.get(str) instanceof Integer)) {
            return null;
        }
        return (Integer) map.get(str);
    }

    public static String getString(String str, Map<String, Object> map, Map<String, String> map2) {
        if (map2 != null && map2.get(str) != null && !TextUtils.isEmpty(map2.get(str))) {
            return map2.get(str);
        }
        if (map == null || map.get(str) == null || !(map.get(str) instanceof String) || TextUtils.isEmpty((String) map.get(str))) {
            return null;
        }
        return (String) map.get(str);
    }

    public static List<String> getStringList(String str, Map<String, Object> map, Map<String, String> map2) {
        if (!(map2 == null || map2.get(str) == null)) {
            try {
                return new ArrayList(Arrays.asList(Json.jsonArrayToStringArray(map2.get(str))));
            } catch (Exception e) {
                Log.d(LOG_TAG, "Cannot parse '" + map2.get(str) + "' in serverExtras to List<String>.  Trying from localExtras instead.");
            }
        }
        if (map == null || map.get(str) == null || !(map.get(str) instanceof List)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Object next : (List) map.get(str)) {
            if (next != null && (next instanceof String)) {
                arrayList.add((String) next);
            }
        }
        return arrayList;
    }

    public static boolean isEmpty(String str, Map<String, Object> map, Map<String, String> map2) {
        return (map2 == null || map2.get(str) == null) && (map == null || map.get(str) == null);
    }
}
