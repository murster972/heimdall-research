package com.mopub.mobileads;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;
import android.widget.VideoView;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.AsyncTasks;
import com.mopub.common.util.Streams;
import java.io.File;
import java.io.FileInputStream;

public class VastVideoView extends VideoView {

    /* renamed from: 靐  reason: contains not printable characters */
    private MediaMetadataRetriever f5755 = m6236();

    /* renamed from: 齉  reason: contains not printable characters */
    private int f5756;

    /* renamed from: 龘  reason: contains not printable characters */
    private VastVideoBlurLastVideoFrameTask f5757;

    public VastVideoView(Context context) {
        super(context);
        Preconditions.checkNotNull(context, "context cannot be null");
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public VastVideoBlurLastVideoFrameTask getBlurLastVideoFrameTask() {
        return this.f5757;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public int getVideoRetries() {
        return this.f5756;
    }

    public void onDestroy() {
        if (this.f5757 != null && this.f5757.getStatus() != AsyncTask.Status.FINISHED) {
            this.f5757.cancel(true);
        }
    }

    public void onResume() {
        this.f5756 = 0;
    }

    public void prepareBlurredLastVideoFrame(ImageView imageView, String str) {
        if (this.f5755 != null) {
            this.f5757 = new VastVideoBlurLastVideoFrameTask(this.f5755, imageView, getDuration());
            try {
                AsyncTasks.safeExecuteOnExecutor(this.f5757, str);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setBlurLastVideoFrameTask(VastVideoBlurLastVideoFrameTask vastVideoBlurLastVideoFrameTask) {
        this.f5757 = vastVideoBlurLastVideoFrameTask;
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setMediaMetadataRetriever(MediaMetadataRetriever mediaMetadataRetriever) {
        this.f5755 = mediaMetadataRetriever;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public MediaMetadataRetriever m6236() {
        return new MediaMetadataRetriever();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6237(MediaPlayer mediaPlayer, int i, int i2, String str) {
        if (Build.VERSION.SDK_INT >= 16 || i != 1 || i2 != Integer.MIN_VALUE || this.f5756 >= 1) {
            return false;
        }
        FileInputStream fileInputStream = null;
        try {
            mediaPlayer.reset();
            FileInputStream fileInputStream2 = new FileInputStream(new File(str));
            try {
                mediaPlayer.setDataSource(fileInputStream2.getFD());
                mediaPlayer.prepareAsync();
                start();
                Streams.closeStream(fileInputStream2);
                this.f5756++;
                return true;
            } catch (Exception e) {
                fileInputStream = fileInputStream2;
                Streams.closeStream(fileInputStream);
                this.f5756++;
                return false;
            } catch (Throwable th) {
                th = th;
                fileInputStream = fileInputStream2;
                Streams.closeStream(fileInputStream);
                this.f5756++;
                throw th;
            }
        } catch (Exception e2) {
            Streams.closeStream(fileInputStream);
            this.f5756++;
            return false;
        } catch (Throwable th2) {
            th = th2;
            Streams.closeStream(fileInputStream);
            this.f5756++;
            throw th;
        }
    }
}
