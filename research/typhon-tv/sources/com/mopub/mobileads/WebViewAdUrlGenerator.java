package com.mopub.mobileads;

import android.content.Context;
import com.mopub.common.AdUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.TyphoonApp;

public class WebViewAdUrlGenerator extends AdUrlGenerator {

    /* renamed from: 连任  reason: contains not printable characters */
    private final boolean f20968;

    public WebViewAdUrlGenerator(Context context, boolean z) {
        super(context);
        this.f20968 = z;
    }

    public String generateUrlString(String str) {
        m26454(str, TyphoonApp.AD_HANDLER);
        m26448("6");
        m6156(ClientMetadata.getInstance(this.f5657));
        m6158(true);
        m26451(this.f20968);
        return m26452();
    }
}
