package com.mopub.mobileads;

import android.support.v4.app.NotificationCompat;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Strings;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.w3c.dom.Node;

class VastLinearXmlManager {
    public static final String ICON = "Icon";
    public static final String ICONS = "Icons";

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20855;

    VastLinearXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20855 = node;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private List<String> m26759(String str) {
        Preconditions.checkNotNull(str);
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20855, "TrackingEvents");
        if (firstMatchingChildNode != null) {
            for (Node nodeValue : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "Tracking", NotificationCompat.CATEGORY_EVENT, Collections.singletonList(str))) {
                String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
                if (nodeValue2 != null) {
                    arrayList.add(nodeValue2);
                }
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<VastTracker> m26760(String str) {
        List<String> r1 = m26759(str);
        ArrayList arrayList = new ArrayList(r1.size());
        for (String vastTracker : r1) {
            arrayList.add(new VastTracker(vastTracker));
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26761(List<VastFractionalProgressTracker> list, List<String> list2, float f) {
        Preconditions.checkNotNull(list, "trackers cannot be null");
        Preconditions.checkNotNull(list2, "urls cannot be null");
        for (String vastFractionalProgressTracker : list2) {
            list.add(new VastFractionalProgressTracker(vastFractionalProgressTracker, f));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public List<VastTracker> m26762() {
        List<VastTracker> r0 = m26760("close");
        r0.addAll(m26760("closeLinear"));
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public List<VastTracker> m26763() {
        return m26760("skip");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public String m26764() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20855, "VideoClicks");
        if (firstMatchingChildNode == null) {
            return null;
        }
        return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(firstMatchingChildNode, "ClickThrough"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public List<VastIconXmlManager> m26765() {
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20855, ICONS);
        if (firstMatchingChildNode != null) {
            for (Node vastIconXmlManager : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, ICON)) {
                arrayList.add(new VastIconXmlManager(vastIconXmlManager));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public List<VastTracker> m26766() {
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20855, "VideoClicks");
        if (firstMatchingChildNode != null) {
            for (Node nodeValue : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "ClickTracking")) {
                String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
                if (nodeValue2 != null) {
                    arrayList.add(new VastTracker(nodeValue2));
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public String m26767() {
        String attributeValue = XmlUtils.getAttributeValue(this.f20855, "skipoffset");
        if (attributeValue != null && !attributeValue.trim().isEmpty()) {
            return attributeValue.trim();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public List<VastMediaXmlManager> m26768() {
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20855, "MediaFiles");
        if (firstMatchingChildNode != null) {
            for (Node vastMediaXmlManager : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "MediaFile")) {
                arrayList.add(new VastMediaXmlManager(vastMediaXmlManager));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public List<VastTracker> m26769() {
        List<String> r1 = m26759("resume");
        ArrayList arrayList = new ArrayList();
        for (String vastTracker : r1) {
            arrayList.add(new VastTracker(vastTracker, true));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public List<VastAbsoluteProgressTracker> m26770() {
        ArrayList arrayList = new ArrayList();
        for (String vastAbsoluteProgressTracker : m26759(TtmlNode.START)) {
            arrayList.add(new VastAbsoluteProgressTracker(vastAbsoluteProgressTracker, 0));
        }
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20855, "TrackingEvents");
        if (firstMatchingChildNode != null) {
            for (Node next : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "Tracking", NotificationCompat.CATEGORY_EVENT, Collections.singletonList(NotificationCompat.CATEGORY_PROGRESS))) {
                String attributeValue = XmlUtils.getAttributeValue(next, VastIconXmlManager.OFFSET);
                if (attributeValue != null) {
                    String trim = attributeValue.trim();
                    if (Strings.isAbsoluteTracker(trim)) {
                        String nodeValue = XmlUtils.getNodeValue(next);
                        try {
                            Integer parseAbsoluteOffset = Strings.parseAbsoluteOffset(trim);
                            if (parseAbsoluteOffset != null && parseAbsoluteOffset.intValue() >= 0) {
                                arrayList.add(new VastAbsoluteProgressTracker(nodeValue, parseAbsoluteOffset.intValue()));
                            }
                        } catch (NumberFormatException e) {
                            MoPubLog.d(String.format("Failed to parse VAST progress tracker %s", new Object[]{trim}));
                        }
                    }
                }
            }
            for (Node nodeValue2 : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "Tracking", NotificationCompat.CATEGORY_EVENT, Collections.singletonList("creativeView"))) {
                String nodeValue3 = XmlUtils.getNodeValue(nodeValue2);
                if (nodeValue3 != null) {
                    arrayList.add(new VastAbsoluteProgressTracker(nodeValue3, 0));
                }
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public List<VastTracker> m26771() {
        List<String> r1 = m26759("pause");
        ArrayList arrayList = new ArrayList();
        for (String vastTracker : r1) {
            arrayList.add(new VastTracker(vastTracker, true));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public List<VastTracker> m26772() {
        return m26760("complete");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<VastFractionalProgressTracker> m26773() {
        ArrayList arrayList = new ArrayList();
        m26761(arrayList, m26759("firstQuartile"), 0.25f);
        m26761(arrayList, m26759("midpoint"), 0.5f);
        m26761(arrayList, m26759("thirdQuartile"), 0.75f);
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20855, "TrackingEvents");
        if (firstMatchingChildNode != null) {
            for (Node next : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "Tracking", NotificationCompat.CATEGORY_EVENT, Collections.singletonList(NotificationCompat.CATEGORY_PROGRESS))) {
                String attributeValue = XmlUtils.getAttributeValue(next, VastIconXmlManager.OFFSET);
                if (attributeValue != null) {
                    String trim = attributeValue.trim();
                    if (Strings.isPercentageTracker(trim)) {
                        String nodeValue = XmlUtils.getNodeValue(next);
                        try {
                            float parseFloat = Float.parseFloat(trim.replace("%", "")) / 100.0f;
                            if (parseFloat >= 0.0f) {
                                arrayList.add(new VastFractionalProgressTracker(nodeValue, parseFloat));
                            }
                        } catch (NumberFormatException e) {
                            MoPubLog.d(String.format("Failed to parse VAST progress tracker %s", new Object[]{trim}));
                        }
                    }
                }
            }
        }
        Collections.sort(arrayList);
        return arrayList;
    }
}
