package com.mopub.mobileads;

import android.content.Context;
import android.os.Handler;
import com.mopub.common.AdReport;
import com.mopub.mobileads.CustomEventInterstitial;

public class HtmlInterstitialWebView extends BaseHtmlWebView {

    /* renamed from: 靐  reason: contains not printable characters */
    private Handler f20781 = new Handler();

    static class HtmlInterstitialWebViewListener implements HtmlWebViewListener {

        /* renamed from: 龘  reason: contains not printable characters */
        private final CustomEventInterstitial.CustomEventInterstitialListener f20782;

        public HtmlInterstitialWebViewListener(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener) {
            this.f20782 = customEventInterstitialListener;
        }

        public void onClicked() {
            this.f20782.onInterstitialClicked();
        }

        public void onCollapsed() {
        }

        public void onFailed(MoPubErrorCode moPubErrorCode) {
            this.f20782.onInterstitialFailed(moPubErrorCode);
        }

        public void onLoaded(BaseHtmlWebView baseHtmlWebView) {
            this.f20782.onInterstitialLoaded();
        }
    }

    public HtmlInterstitialWebView(Context context, AdReport adReport) {
        super(context, adReport);
    }

    public void init(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener, boolean z, String str, String str2, String str3) {
        super.init(z);
        setWebViewClient(new HtmlWebViewClient(new HtmlInterstitialWebViewListener(customEventInterstitialListener), this, str2, str, str3));
    }
}
