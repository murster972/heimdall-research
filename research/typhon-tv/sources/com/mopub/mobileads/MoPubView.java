package com.mopub.mobileads;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebViewDatabase;
import android.widget.FrameLayout;
import com.Pinkamena;
import com.mopub.common.AdFormat;
import com.mopub.common.AdReport;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.ManifestUtils;
import com.mopub.common.util.Reflection;
import com.mopub.common.util.Visibility;
import com.mopub.mobileads.factories.AdViewControllerFactory;
import java.util.Map;
import java.util.TreeMap;

public class MoPubView extends FrameLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private BannerAdListener f5726;

    /* renamed from: 连任  reason: contains not printable characters */
    private BroadcastReceiver f5727;

    /* renamed from: 靐  reason: contains not printable characters */
    protected AdViewController f5728;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public int f5729;

    /* renamed from: 齉  reason: contains not printable characters */
    protected Object f5730;

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f5731;

    public interface BannerAdListener {
        void onBannerClicked(MoPubView moPubView);

        void onBannerCollapsed(MoPubView moPubView);

        void onBannerExpanded(MoPubView moPubView);

        void onBannerFailed(MoPubView moPubView, MoPubErrorCode moPubErrorCode);

        void onBannerLoaded(MoPubView moPubView);
    }

    public MoPubView(Context context) {
        this(context, (AttributeSet) null);
    }

    public MoPubView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ManifestUtils.checkWebViewActivitiesDeclared(context);
        this.f5731 = context;
        this.f5729 = getVisibility();
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        try {
            if (WebViewDatabase.getInstance(context) == null) {
                MoPubLog.e("Disabling MoPub. Local cache file is inaccessible so MoPub will fail if we try to create a WebView. Details of this Android bug found at:https://code.google.com/p/android/issues/detail?id=10789");
                return;
            }
            this.f5728 = AdViewControllerFactory.create(context, this);
            m6215();
        } catch (Exception e) {
            MoPubLog.e("Disabling MoPub due to no WebView, or it's being updated", e);
        }
    }

    /* access modifiers changed from: private */
    public void setAdVisibility(int i) {
        if (this.f5728 != null) {
            if (Visibility.isScreenVisible(i)) {
                this.f5728.m6198();
            } else {
                this.f5728.m6194();
            }
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m6212() {
        try {
            this.f5731.unregisterReceiver(this.f5727);
        } catch (Exception e) {
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m6213() {
        if (this.f5730 != null) {
            try {
                new Reflection.MethodBuilder(this.f5730, "invalidate").setAccessible().execute();
            } catch (Exception e) {
                MoPubLog.e("Error invalidating adapter", e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m6215() {
        this.f5727 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (Visibility.isScreenVisible(MoPubView.this.f5729) && intent != null) {
                    String action = intent.getAction();
                    if ("android.intent.action.USER_PRESENT".equals(action)) {
                        MoPubView.this.setAdVisibility(0);
                    } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                        MoPubView.this.setAdVisibility(8);
                    }
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        this.f5731.registerReceiver(this.f5727, intentFilter);
    }

    public void destroy() {
        m6212();
        removeAllViews();
        if (this.f5728 != null) {
            this.f5728.m6197();
            this.f5728 = null;
        }
        if (this.f5730 != null) {
            m6213();
            this.f5730 = null;
        }
    }

    public void forceRefresh() {
        if (this.f5730 != null) {
            m6213();
            this.f5730 = null;
        }
        if (this.f5728 != null) {
            this.f5728.m6189();
        }
    }

    public Activity getActivity() {
        return (Activity) this.f5731;
    }

    public AdFormat getAdFormat() {
        return AdFormat.BANNER;
    }

    public int getAdHeight() {
        if (this.f5728 != null) {
            return this.f5728.getAdHeight();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public Integer getAdTimeoutDelay() {
        if (this.f5728 != null) {
            return this.f5728.m6193();
        }
        return null;
    }

    public String getAdUnitId() {
        if (this.f5728 != null) {
            return this.f5728.getAdUnitId();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public AdViewController getAdViewController() {
        return this.f5728;
    }

    public int getAdWidth() {
        if (this.f5728 != null) {
            return this.f5728.getAdWidth();
        }
        return 0;
    }

    public boolean getAutorefreshEnabled() {
        if (this.f5728 != null) {
            return this.f5728.getCurrentAutoRefreshStatus();
        }
        MoPubLog.d("Can't get autorefresh status for destroyed MoPubView. Returning false.");
        return false;
    }

    public BannerAdListener getBannerAdListener() {
        return this.f5726;
    }

    @Deprecated
    public String getClickTrackingUrl() {
        return null;
    }

    public String getKeywords() {
        if (this.f5728 != null) {
            return this.f5728.getKeywords();
        }
        return null;
    }

    public Map<String, Object> getLocalExtras() {
        return this.f5728 != null ? this.f5728.m6192() : new TreeMap();
    }

    public Location getLocation() {
        if (this.f5728 != null) {
            return this.f5728.getLocation();
        }
        return null;
    }

    @Deprecated
    public String getResponseString() {
        return null;
    }

    public boolean getTesting() {
        if (this.f5728 != null) {
            return this.f5728.getTesting();
        }
        MoPubLog.d("Can't get testing status for destroyed MoPubView. Returning false.");
        return false;
    }

    public void loadAd() {
        if (this.f5728 != null) {
            AdViewController adViewController = this.f5728;
            Pinkamena.DianePie();
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        if (Visibility.hasScreenVisibilityChanged(this.f5729, i)) {
            this.f5729 = i;
            setAdVisibility(this.f5729);
        }
    }

    public void setAdContentView(View view) {
        if (this.f5728 != null) {
            this.f5728.m6200(view);
        }
    }

    public void setAdUnitId(String str) {
        if (this.f5728 != null) {
            this.f5728.setAdUnitId(str);
        }
    }

    public void setAutorefreshEnabled(boolean z) {
        if (this.f5728 != null) {
            this.f5728.m6206(z);
        }
    }

    public void setBannerAdListener(BannerAdListener bannerAdListener) {
        this.f5726 = bannerAdListener;
    }

    public void setKeywords(String str) {
        if (this.f5728 != null) {
            this.f5728.setKeywords(str);
        }
    }

    public void setLocalExtras(Map<String, Object> map) {
        if (this.f5728 != null) {
            this.f5728.m6205(map);
        }
    }

    public void setLocation(Location location) {
        if (this.f5728 != null) {
            this.f5728.setLocation(location);
        }
    }

    public void setTesting(boolean z) {
        if (this.f5728 != null) {
            this.f5728.setTesting(z);
        }
    }

    @Deprecated
    public void setTimeout(int i) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6217() {
        if (this.f5726 != null) {
            this.f5726.onBannerCollapsed(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m6218() {
        if (this.f5726 != null) {
            this.f5726.onBannerClicked(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m6219() {
        if (this.f5728 != null) {
            this.f5728.m6191();
        }
        m6223();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m6220() {
        if (this.f5726 != null) {
            this.f5726.onBannerExpanded(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6221() {
        if (this.f5728 != null) {
            this.f5728.m6188();
            m6218();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m6222(MoPubErrorCode moPubErrorCode) {
        return this.f5728 != null && this.f5728.m6207(moPubErrorCode);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m6223() {
        if (this.f5726 != null) {
            this.f5726.onBannerLoaded(this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m6224() {
        if (this.f5728 != null) {
            this.f5728.m6187();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6225(MoPubErrorCode moPubErrorCode) {
        if (this.f5726 != null) {
            this.f5726.onBannerFailed(this, moPubErrorCode);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6226(String str, Map<String, String> map) {
        if (this.f5728 != null) {
            if (TextUtils.isEmpty(str)) {
                m6222(MoPubErrorCode.ADAPTER_NOT_FOUND);
                return;
            }
            if (this.f5730 != null) {
                m6213();
            }
            if (Reflection.classFound("com.mopub.mobileads.factories.CustomEventBannerAdapterFactory")) {
                try {
                    this.f5730 = new Reflection.MethodBuilder((Object) null, "create").setStatic(Class.forName("com.mopub.mobileads.factories.CustomEventBannerAdapterFactory")).addParam(MoPubView.class, this).addParam(String.class, str).addParam(Map.class, map).addParam(Long.TYPE, Long.valueOf(this.f5728.getBroadcastIdentifier())).addParam(AdReport.class, this.f5728.getAdReport()).execute();
                    new Reflection.MethodBuilder(this.f5730, "loadAd").setAccessible().execute();
                } catch (Exception e) {
                    MoPubLog.e("Error loading custom event", e);
                }
            } else {
                MoPubLog.e("Could not load custom event -- missing banner module");
            }
        }
    }
}
