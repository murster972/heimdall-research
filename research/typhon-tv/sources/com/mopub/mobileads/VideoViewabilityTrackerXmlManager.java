package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Strings;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

public class VideoViewabilityTrackerXmlManager {
    public static final String PERCENT_VIEWABLE = "percentViewable";
    public static final String VIEWABLE_PLAYTIME = "viewablePlaytime";

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20964;

    VideoViewabilityTrackerXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20964 = node;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Integer m26896() {
        String attributeValue = XmlUtils.getAttributeValue(this.f20964, PERCENT_VIEWABLE);
        if (attributeValue == null) {
            return null;
        }
        Integer num = null;
        try {
            num = Integer.valueOf((int) Float.parseFloat(attributeValue.replace("%", "")));
        } catch (NumberFormatException e) {
            MoPubLog.d(String.format("Invalid VAST percentViewable format for \"d{1,3}%%\": %s:", new Object[]{attributeValue}));
        }
        if (num == null || num.intValue() < 0 || num.intValue() > 100) {
            return null;
        }
        return num;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m26897() {
        return XmlUtils.getNodeValue(this.f20964);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Integer m26898() {
        String attributeValue = XmlUtils.getAttributeValue(this.f20964, VIEWABLE_PLAYTIME);
        if (attributeValue == null) {
            return null;
        }
        Integer num = null;
        if (Strings.isAbsoluteTracker(attributeValue)) {
            try {
                num = Strings.parseAbsoluteOffset(attributeValue);
            } catch (NumberFormatException e) {
                MoPubLog.d(String.format("Invalid VAST viewablePlaytime format for \"HH:MM:SS[.mmm]\": %s:", new Object[]{attributeValue}));
            }
        } else {
            try {
                num = Integer.valueOf((int) (Float.parseFloat(attributeValue) * 1000.0f));
            } catch (NumberFormatException e2) {
                MoPubLog.d(String.format("Invalid VAST viewablePlaytime format for \"SS[.mmm]\": %s:", new Object[]{attributeValue}));
            }
        }
        if (num == null || num.intValue() < 0) {
            return null;
        }
        return num;
    }
}
