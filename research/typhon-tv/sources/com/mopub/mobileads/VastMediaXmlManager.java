package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

class VastMediaXmlManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20867;

    VastMediaXmlManager(Node node) {
        Preconditions.checkNotNull(node, "mediaNode cannot be null");
        this.f20867 = node;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Integer m26780() {
        return XmlUtils.getAttributeValueAsInt(this.f20867, VastIconXmlManager.HEIGHT);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public String m26781() {
        return XmlUtils.getNodeValue(this.f20867);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m26782() {
        return XmlUtils.getAttributeValue(this.f20867, VastExtensionXmlManager.TYPE);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Integer m26783() {
        return XmlUtils.getAttributeValueAsInt(this.f20867, VastIconXmlManager.WIDTH);
    }
}
