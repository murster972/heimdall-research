package com.mopub.mobileads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import com.mopub.common.DataKeys;
import com.mopub.common.Preconditions;

public abstract class BaseBroadcastReceiver extends BroadcastReceiver {

    /* renamed from: 靐  reason: contains not printable characters */
    private Context f20718;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f20719;

    public BaseBroadcastReceiver(long j) {
        this.f20719 = j;
    }

    public static void broadcastAction(Context context, long j, String str) {
        Preconditions.checkNotNull(context, "context cannot be null");
        Preconditions.checkNotNull(str, "action cannot be null");
        Intent intent = new Intent(str);
        intent.putExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, j);
        LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
    }

    public abstract IntentFilter getIntentFilter();

    public void register(BroadcastReceiver broadcastReceiver, Context context) {
        this.f20718 = context;
        LocalBroadcastManager.getInstance(this.f20718).registerReceiver(broadcastReceiver, getIntentFilter());
    }

    public boolean shouldConsumeBroadcast(Intent intent) {
        Preconditions.checkNotNull(intent, "intent cannot be null");
        return this.f20719 == intent.getLongExtra(DataKeys.BROADCAST_IDENTIFIER_KEY, -1);
    }

    public void unregister(BroadcastReceiver broadcastReceiver) {
        if (this.f20718 != null && broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this.f20718).unregisterReceiver(broadcastReceiver);
            this.f20718 = null;
        }
    }
}
