package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;

abstract class VastBaseInLineWrapperXmlManager {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final Node f20840;

    VastBaseInLineWrapperXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20840 = node;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public VastExtensionParentXmlManager m26724() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20840, "Extensions");
        if (firstMatchingChildNode == null) {
            return null;
        }
        return new VastExtensionParentXmlManager(firstMatchingChildNode);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public List<VastTracker> m26725() {
        ArrayList arrayList = new ArrayList();
        List<Node> matchingChildNodes = XmlUtils.getMatchingChildNodes(this.f20840, "Error");
        if (matchingChildNodes != null) {
            for (Node nodeValue : matchingChildNodes) {
                String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
                if (!TextUtils.isEmpty(nodeValue2)) {
                    arrayList.add(new VastTracker(nodeValue2, true));
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public List<VastCompanionAdXmlManager> m26726() {
        List<Node> matchingChildNodes;
        List<Node> matchingChildNodes2;
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20840, "Creatives");
        if (!(firstMatchingChildNode == null || (matchingChildNodes = XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "Creative")) == null)) {
            for (Node firstMatchingChildNode2 : matchingChildNodes) {
                Node firstMatchingChildNode3 = XmlUtils.getFirstMatchingChildNode(firstMatchingChildNode2, "CompanionAds");
                if (!(firstMatchingChildNode3 == null || (matchingChildNodes2 = XmlUtils.getMatchingChildNodes(firstMatchingChildNode3, "Companion")) == null)) {
                    for (Node vastCompanionAdXmlManager : matchingChildNodes2) {
                        arrayList.add(new VastCompanionAdXmlManager(vastCompanionAdXmlManager));
                    }
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public List<VastLinearXmlManager> m26727() {
        List<Node> matchingChildNodes;
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20840, "Creatives");
        if (!(firstMatchingChildNode == null || (matchingChildNodes = XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "Creative")) == null)) {
            for (Node firstMatchingChildNode2 : matchingChildNodes) {
                Node firstMatchingChildNode3 = XmlUtils.getFirstMatchingChildNode(firstMatchingChildNode2, "Linear");
                if (firstMatchingChildNode3 != null) {
                    arrayList.add(new VastLinearXmlManager(firstMatchingChildNode3));
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<VastTracker> m26728() {
        List<Node> matchingChildNodes = XmlUtils.getMatchingChildNodes(this.f20840, "Impression");
        ArrayList arrayList = new ArrayList();
        for (Node nodeValue : matchingChildNodes) {
            String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
            if (!TextUtils.isEmpty(nodeValue2)) {
                arrayList.add(new VastTracker(nodeValue2));
            }
        }
        return arrayList;
    }
}
