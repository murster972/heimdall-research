package com.mopub.mobileads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import com.Pinkamena;
import com.mopub.common.AdReport;
import com.mopub.common.TyphoonApp;
import com.mopub.common.util.VersionCode;
import com.mopub.mobileads.ViewGestureDetector;
import com.mopub.network.Networking;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class BaseHtmlWebView extends BaseWebView implements ViewGestureDetector.UserClickListener {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final ViewGestureDetector f20720;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f20721;

    @SuppressLint({"SetJavaScriptEnabled"})
    public BaseHtmlWebView(Context context, AdReport adReport) {
        super(context);
        m26629();
        getSettings().setJavaScriptEnabled(true);
        this.f20720 = new ViewGestureDetector(context, (View) this, adReport);
        this.f20720.setUserClickListener(this);
        if (VersionCode.currentApiLevel().isAtLeast(VersionCode.ICE_CREAM_SANDWICH)) {
            m6210(true);
        }
        setBackgroundColor(0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26629() {
        setHorizontalScrollBarEnabled(false);
        setHorizontalScrollbarOverlay(false);
        setVerticalScrollBarEnabled(false);
        setVerticalScrollbarOverlay(false);
        getSettings().setSupportZoom(false);
    }

    public void init(boolean z) {
        m26632(z);
    }

    public void loadUrl(String str) {
        if (str != null && str.startsWith("javascript:")) {
            Pinkamena.DianePie();
        }
    }

    public void onResetUserClick() {
        this.f20721 = false;
    }

    public void onUserClick() {
        this.f20721 = true;
    }

    public void stopLoading() {
        WebSettings settings;
        if (!this.f5725 && (settings = getSettings()) != null) {
            settings.setJavaScriptEnabled(false);
            super.stopLoading();
            settings.setJavaScriptEnabled(true);
        }
    }

    public boolean wasClicked() {
        return this.f20721;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26631(String str) {
        String str2 = Networking.getBaseUrlScheme() + "://" + TyphoonApp.HOST + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
        String str3 = str;
        Pinkamena.DianePie();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26632(final boolean z) {
        setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                BaseHtmlWebView.this.f20720.sendTouchEvent(motionEvent);
                return motionEvent.getAction() == 2 && !z;
            }
        });
    }
}
