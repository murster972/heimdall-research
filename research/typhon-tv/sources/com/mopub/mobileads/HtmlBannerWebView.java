package com.mopub.mobileads;

import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.mobileads.CustomEventBanner;

public class HtmlBannerWebView extends BaseHtmlWebView {
    public static final String EXTRA_AD_CLICK_DATA = "com.mopub.intent.extra.AD_CLICK_DATA";

    static class HtmlBannerWebViewListener implements HtmlWebViewListener {

        /* renamed from: 龘  reason: contains not printable characters */
        private final CustomEventBanner.CustomEventBannerListener f20775;

        public HtmlBannerWebViewListener(CustomEventBanner.CustomEventBannerListener customEventBannerListener) {
            this.f20775 = customEventBannerListener;
        }

        public void onClicked() {
            this.f20775.onBannerClicked();
        }

        public void onCollapsed() {
            this.f20775.onBannerCollapsed();
        }

        public void onFailed(MoPubErrorCode moPubErrorCode) {
            this.f20775.onBannerFailed(moPubErrorCode);
        }

        public void onLoaded(BaseHtmlWebView baseHtmlWebView) {
            this.f20775.onBannerLoaded(baseHtmlWebView);
        }
    }

    public HtmlBannerWebView(Context context, AdReport adReport) {
        super(context, adReport);
    }

    public void init(CustomEventBanner.CustomEventBannerListener customEventBannerListener, boolean z, String str, String str2, String str3) {
        super.init(z);
        setWebViewClient(new HtmlWebViewClient(new HtmlBannerWebViewListener(customEventBannerListener), this, str2, str, str3));
    }
}
