package com.mopub.mobileads;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.widget.RelativeLayout;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.ProgressBarDrawable;

public class VastVideoProgressBarWidget extends AppCompatImageView {

    /* renamed from: 靐  reason: contains not printable characters */
    private ProgressBarDrawable f5751;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f5752;

    public VastVideoProgressBarWidget(Context context) {
        super(context);
        setId((int) Utils.generateUniqueId());
        this.f5751 = new ProgressBarDrawable(context);
        setImageDrawable(this.f5751);
        this.f5752 = Dips.dipsToIntPixels(4.0f, context);
    }

    public void calibrateAndMakeVisible(int i, int i2) {
        this.f5751.setDurationAndSkipOffset(i, i2);
        setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public ProgressBarDrawable getImageViewDrawable() {
        return this.f5751;
    }

    public void reset() {
        this.f5751.reset();
        this.f5751.setProgress(0);
    }

    public void setAnchorId(int i) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, this.f5752);
        layoutParams.addRule(8, i);
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    @VisibleForTesting
    public void setImageViewDrawable(ProgressBarDrawable progressBarDrawable) {
        this.f5751 = progressBarDrawable;
    }

    public void updateProgress(int i) {
        this.f5751.setProgress(i);
    }
}
