package com.mopub.mobileads;

public class VideoViewabilityTracker extends VastTracker {
    private final int mPercentViewable;
    private final int mViewablePlaytimeMS;

    public VideoViewabilityTracker(int i, int i2, String str) {
        super(str);
        this.mViewablePlaytimeMS = i;
        this.mPercentViewable = i2;
    }

    public int getPercentViewable() {
        return this.mPercentViewable;
    }

    public int getViewablePlaytimeMS() {
        return this.mViewablePlaytimeMS;
    }
}
