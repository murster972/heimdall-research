package com.mopub.mobileads;

import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.w3c.dom.Node;

class VastCompanionAdXmlManager {

    /* renamed from: 靐  reason: contains not printable characters */
    private final VastResourceXmlManager f20845;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20846;

    VastCompanionAdXmlManager(Node node) {
        Preconditions.checkNotNull(node, "companionNode cannot be null");
        this.f20846 = node;
        this.f20845 = new VastResourceXmlManager(node);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public List<VastTracker> m26731() {
        ArrayList arrayList = new ArrayList();
        List<Node> matchingChildNodes = XmlUtils.getMatchingChildNodes(this.f20846, "CompanionClickTracking");
        if (matchingChildNodes != null) {
            for (Node nodeValue : matchingChildNodes) {
                String nodeValue2 = XmlUtils.getNodeValue(nodeValue);
                if (!TextUtils.isEmpty(nodeValue2)) {
                    arrayList.add(new VastTracker(nodeValue2));
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public List<VastTracker> m26732() {
        ArrayList arrayList = new ArrayList();
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20846, "TrackingEvents");
        if (firstMatchingChildNode != null) {
            for (Node nodeValue : XmlUtils.getMatchingChildNodes(firstMatchingChildNode, "Tracking", NotificationCompat.CATEGORY_EVENT, Collections.singletonList("creativeView"))) {
                arrayList.add(new VastTracker(XmlUtils.getNodeValue(nodeValue)));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m26733() {
        return !TextUtils.isEmpty(this.f20845.m26788()) || !TextUtils.isEmpty(this.f20845.m26786()) || !TextUtils.isEmpty(this.f20845.m26787());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public String m26734() {
        return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(this.f20846, "CompanionClickThrough"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Integer m26735() {
        return XmlUtils.getAttributeValueAsInt(this.f20846, VastIconXmlManager.HEIGHT);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public VastResourceXmlManager m26736() {
        return this.f20845;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public String m26737() {
        return XmlUtils.getAttributeValue(this.f20846, "adSlotID");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Integer m26738() {
        return XmlUtils.getAttributeValueAsInt(this.f20846, VastIconXmlManager.WIDTH);
    }
}
