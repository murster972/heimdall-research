package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

public class VastExtensionXmlManager {
    public static final String TYPE = "type";
    public static final String VIDEO_VIEWABILITY_TRACKER = "MoPubViewabilityTracker";

    /* renamed from: 龘  reason: contains not printable characters */
    private final Node f20849;

    public VastExtensionXmlManager(Node node) {
        Preconditions.checkNotNull(node);
        this.f20849 = node;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m26741() {
        return XmlUtils.getAttributeValue(this.f20849, TYPE);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public VideoViewabilityTracker m26742() {
        Node firstMatchingChildNode = XmlUtils.getFirstMatchingChildNode(this.f20849, VIDEO_VIEWABILITY_TRACKER);
        if (firstMatchingChildNode == null) {
            return null;
        }
        VideoViewabilityTrackerXmlManager videoViewabilityTrackerXmlManager = new VideoViewabilityTrackerXmlManager(firstMatchingChildNode);
        Integer r4 = videoViewabilityTrackerXmlManager.m26898();
        Integer r0 = videoViewabilityTrackerXmlManager.m26896();
        String r2 = videoViewabilityTrackerXmlManager.m26897();
        if (r4 == null || r0 == null || TextUtils.isEmpty(r2)) {
            return null;
        }
        return new VideoViewabilityTracker(r4.intValue(), r0.intValue(), r2);
    }
}
