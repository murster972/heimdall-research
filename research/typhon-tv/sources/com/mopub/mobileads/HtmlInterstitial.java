package com.mopub.mobileads;

import com.mopub.common.CreativeOrientation;
import com.mopub.common.DataKeys;
import com.mopub.mobileads.CustomEventInterstitial;
import java.util.Map;

public class HtmlInterstitial extends ResponseBodyInterstitial {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f20776;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f20777;

    /* renamed from: ʽ  reason: contains not printable characters */
    private CreativeOrientation f20778;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f20779;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f20780;

    public void showInterstitial() {
        MoPubActivity.start(this.f20838, this.f20780, this.f20835, this.f20779, this.f20776, this.f20777, this.f20778, this.f20837);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26681(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener) {
        MoPubActivity.m26689(this.f20838, this.f20835, customEventInterstitialListener, this.f20780);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26682(Map<String, String> map) {
        this.f20780 = map.get(DataKeys.HTML_RESPONSE_BODY_KEY);
        this.f20779 = Boolean.valueOf(map.get(DataKeys.SCROLLABLE_KEY)).booleanValue();
        this.f20776 = map.get(DataKeys.REDIRECT_URL_KEY);
        this.f20777 = map.get(DataKeys.CLICKTHROUGH_URL_KEY);
        this.f20778 = CreativeOrientation.fromHeader(map.get(DataKeys.CREATIVE_ORIENTATION_KEY));
    }
}
