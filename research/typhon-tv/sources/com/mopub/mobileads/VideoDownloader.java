package com.mopub.mobileads;

import android.os.AsyncTask;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.AsyncTasks;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Deque;

public class VideoDownloader {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Deque<WeakReference<VideoDownloaderTask>> f5758 = new ArrayDeque();

    interface VideoDownloaderListener {
        void onComplete(boolean z);
    }

    @VisibleForTesting
    static class VideoDownloaderTask extends AsyncTask<String, Void, Boolean> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final WeakReference<VideoDownloaderTask> f20961 = new WeakReference<>(this);

        /* renamed from: 龘  reason: contains not printable characters */
        private final VideoDownloaderListener f20962;

        @VisibleForTesting
        VideoDownloaderTask(VideoDownloaderListener videoDownloaderListener) {
            this.f20962 = videoDownloaderListener;
            VideoDownloader.f5758.add(this.f20961);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            VideoDownloader.f5758.remove(this.f20961);
            this.f20962.onComplete(false);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00a1  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x00ac  */
        /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
        @android.annotation.SuppressLint({"DefaultLocale"})
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(java.lang.String... r12) {
            /*
                r11 = this;
                r9 = 26214400(0x1900000, float:5.2897246E-38)
                r8 = 0
                if (r12 == 0) goto L_0x000c
                int r7 = r12.length
                if (r7 == 0) goto L_0x000c
                r7 = r12[r8]
                if (r7 != 0) goto L_0x0011
            L_0x000c:
                java.lang.Boolean r7 = java.lang.Boolean.valueOf(r8)
            L_0x0010:
                return r7
            L_0x0011:
                r6 = r12[r8]
                r5 = 0
                r2 = 0
                java.net.HttpURLConnection r5 = com.mopub.common.MoPubHttpUrlConnection.getHttpUrlConnection(r6)     // Catch:{ Exception -> 0x0096 }
                java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0096 }
                java.io.InputStream r7 = r5.getInputStream()     // Catch:{ Exception -> 0x0096 }
                r3.<init>(r7)     // Catch:{ Exception -> 0x0096 }
                int r4 = r5.getResponseCode()     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r7 = 200(0xc8, float:2.8E-43)
                if (r4 < r7) goto L_0x002e
                r7 = 300(0x12c, float:4.2E-43)
                if (r4 < r7) goto L_0x0053
            L_0x002e:
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r7.<init>()     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                java.lang.String r8 = "VideoDownloader encountered unexpected statusCode: "
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                com.mopub.common.logging.MoPubLog.d(r7)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r7 = 0
                java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                com.mopub.common.util.Streams.closeStream(r3)
                if (r5 == 0) goto L_0x0010
                r5.disconnect()
                goto L_0x0010
            L_0x0053:
                int r0 = r5.getContentLength()     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                if (r0 <= r9) goto L_0x0084
                java.lang.String r7 = "VideoDownloader encountered video larger than disk cap. (%d bytes / %d maximum)."
                r8 = 2
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r9 = 0
                java.lang.Integer r10 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r8[r9] = r10     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r9 = 1
                r10 = 26214400(0x1900000, float:5.2897246E-38)
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r8[r9] = r10     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                java.lang.String r7 = java.lang.String.format(r7, r8)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                com.mopub.common.logging.MoPubLog.d(r7)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                r7 = 0
                java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                com.mopub.common.util.Streams.closeStream(r3)
                if (r5 == 0) goto L_0x0010
                r5.disconnect()
                goto L_0x0010
            L_0x0084:
                boolean r7 = com.mopub.common.CacheService.putToDiskCache((java.lang.String) r6, (java.io.InputStream) r3)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Exception -> 0x00b3, all -> 0x00b0 }
                com.mopub.common.util.Streams.closeStream(r3)
                if (r5 == 0) goto L_0x0010
                r5.disconnect()
                goto L_0x0010
            L_0x0096:
                r1 = move-exception
            L_0x0097:
                r7 = 0
                java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ all -> 0x00a6 }
                com.mopub.common.util.Streams.closeStream(r2)
                if (r5 == 0) goto L_0x0010
                r5.disconnect()
                goto L_0x0010
            L_0x00a6:
                r7 = move-exception
            L_0x00a7:
                com.mopub.common.util.Streams.closeStream(r2)
                if (r5 == 0) goto L_0x00af
                r5.disconnect()
            L_0x00af:
                throw r7
            L_0x00b0:
                r7 = move-exception
                r2 = r3
                goto L_0x00a7
            L_0x00b3:
                r1 = move-exception
                r2 = r3
                goto L_0x0097
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.VideoDownloader.VideoDownloaderTask.doInBackground(java.lang.String[]):java.lang.Boolean");
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void onPostExecute(Boolean bool) {
            if (isCancelled()) {
                onCancelled();
                return;
            }
            VideoDownloader.f5758.remove(this.f20961);
            if (bool == null) {
                this.f20962.onComplete(false);
            } else {
                this.f20962.onComplete(bool.booleanValue());
            }
        }
    }

    private VideoDownloader() {
    }

    public static void cache(String str, VideoDownloaderListener videoDownloaderListener) {
        Preconditions.checkNotNull(videoDownloaderListener);
        if (str == null) {
            videoDownloaderListener.onComplete(false);
            return;
        }
        try {
            AsyncTasks.safeExecuteOnExecutor(new VideoDownloaderTask(videoDownloaderListener), str);
        } catch (Exception e) {
            videoDownloaderListener.onComplete(false);
        }
    }

    public static void cancelAllDownloaderTasks() {
        for (WeakReference<VideoDownloaderTask> r0 : f5758) {
            m6239(r0);
        }
        f5758.clear();
    }

    public static void cancelLastDownloadTask() {
        if (!f5758.isEmpty()) {
            m6239(f5758.peekLast());
            f5758.removeLast();
        }
    }

    @Deprecated
    @VisibleForTesting
    public static void clearDownloaderTasks() {
        f5758.clear();
    }

    @Deprecated
    @VisibleForTesting
    public static Deque<WeakReference<VideoDownloaderTask>> getDownloaderTasks() {
        return f5758;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m6239(WeakReference<VideoDownloaderTask> weakReference) {
        boolean z = true;
        if (weakReference == null) {
            return false;
        }
        VideoDownloaderTask videoDownloaderTask = (VideoDownloaderTask) weakReference.get();
        if (videoDownloaderTask == null || !videoDownloaderTask.cancel(true)) {
            z = false;
        }
        return z;
    }
}
