package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.VisibleForTesting;
import java.util.WeakHashMap;

@Deprecated
class NativeAdViewHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final WeakHashMap<View, NativeAd> f5794 = new WeakHashMap<>();

    @VisibleForTesting
    enum ViewType {
        EMPTY,
        AD
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    static View m6248(View view, ViewGroup viewGroup, Context context, NativeAd nativeAd) {
        if (view != null) {
            m6249(view);
        }
        if (nativeAd != null && !nativeAd.isDestroyed()) {
            if (view == null || !ViewType.AD.equals(view.getTag())) {
                view = nativeAd.createAdView(context, viewGroup);
                view.setTag(ViewType.AD);
            }
            m6250(view, nativeAd);
            nativeAd.renderAdView(view);
            return view;
        } else if (view != null && ViewType.EMPTY.equals(view.getTag())) {
            return view;
        } else {
            View view2 = new View(context);
            view2.setTag(ViewType.EMPTY);
            view2.setVisibility(8);
            return view2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m6249(View view) {
        NativeAd nativeAd = f5794.get(view);
        if (nativeAd != null) {
            nativeAd.clear(view);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m6250(View view, NativeAd nativeAd) {
        f5794.put(view, nativeAd);
        nativeAd.prepare(view);
    }
}
