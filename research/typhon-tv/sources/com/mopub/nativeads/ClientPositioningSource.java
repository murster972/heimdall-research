package com.mopub.nativeads;

import android.os.Handler;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.PositioningSource;

class ClientPositioningSource implements PositioningSource {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final MoPubNativeAdPositioning.MoPubClientPositioning f21091;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Handler f21092 = new Handler();

    ClientPositioningSource(MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
        this.f21091 = MoPubNativeAdPositioning.m27093(moPubClientPositioning);
    }

    public void loadPositions(String str, final PositioningSource.PositioningListener positioningListener) {
        this.f21092.post(new Runnable() {
            public void run() {
                positioningListener.onLoad(ClientPositioningSource.this.f21091);
            }
        });
    }
}
