package com.mopub.nativeads;

import android.app.Activity;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.Pinkamena;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.VisibilityTracker;
import java.util.List;
import java.util.WeakHashMap;

public class MoPubAdAdapter extends BaseAdapter {

    /* renamed from: 连任  reason: contains not printable characters */
    private MoPubNativeAdLoadedListener f21122;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Adapter f21123;

    /* renamed from: 麤  reason: contains not printable characters */
    private final VisibilityTracker f21124;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final MoPubStreamAdPlacer f21125;

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakHashMap<View, Integer> f21126;

    public MoPubAdAdapter(Activity activity, Adapter adapter) {
        this(activity, adapter, MoPubNativeAdPositioning.serverPositioning());
    }

    public MoPubAdAdapter(Activity activity, Adapter adapter, MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
        this(new MoPubStreamAdPlacer(activity, moPubClientPositioning), adapter, new VisibilityTracker(activity));
    }

    public MoPubAdAdapter(Activity activity, Adapter adapter, MoPubNativeAdPositioning.MoPubServerPositioning moPubServerPositioning) {
        this(new MoPubStreamAdPlacer(activity, moPubServerPositioning), adapter, new VisibilityTracker(activity));
    }

    @VisibleForTesting
    MoPubAdAdapter(MoPubStreamAdPlacer moPubStreamAdPlacer, Adapter adapter, VisibilityTracker visibilityTracker) {
        this.f21123 = adapter;
        this.f21125 = moPubStreamAdPlacer;
        this.f21126 = new WeakHashMap<>();
        this.f21124 = visibilityTracker;
        this.f21124.m27212((VisibilityTracker.VisibilityTrackerListener) new VisibilityTracker.VisibilityTrackerListener() {
            public void onVisibilityChanged(List<View> list, List<View> list2) {
                MoPubAdAdapter.this.m27072(list);
            }
        });
        this.f21123.registerDataSetObserver(new DataSetObserver() {
            public void onChanged() {
                MoPubAdAdapter.this.f21125.setItemCount(MoPubAdAdapter.this.f21123.getCount());
                MoPubAdAdapter.this.notifyDataSetChanged();
            }

            public void onInvalidated() {
                MoPubAdAdapter.this.notifyDataSetInvalidated();
            }
        });
        this.f21125.setAdLoadedListener(new MoPubNativeAdLoadedListener() {
            public void onAdLoaded(int i) {
                MoPubAdAdapter.this.m27074(i);
            }

            public void onAdRemoved(int i) {
                MoPubAdAdapter.this.m27073(i);
            }
        });
        this.f21125.setItemCount(this.f21123.getCount());
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27072(List<View> list) {
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        int i2 = 0;
        for (View view : list) {
            Integer num = this.f21126.get(view);
            if (num != null) {
                i = Math.min(num.intValue(), i);
                i2 = Math.max(num.intValue(), i2);
            }
        }
        this.f21125.placeAdsInRange(i, i2 + 1);
    }

    public boolean areAllItemsEnabled() {
        return (this.f21123 instanceof ListAdapter) && ((ListAdapter) this.f21123).areAllItemsEnabled();
    }

    public void clearAds() {
        this.f21125.clearAds();
    }

    public void destroy() {
        this.f21125.destroy();
        this.f21124.m27205();
    }

    public int getAdjustedPosition(int i) {
        return this.f21125.getAdjustedPosition(i);
    }

    public int getCount() {
        return this.f21125.getAdjustedCount(this.f21123.getCount());
    }

    public Object getItem(int i) {
        Object adData = this.f21125.getAdData(i);
        return adData != null ? adData : this.f21123.getItem(this.f21125.getOriginalPosition(i));
    }

    public long getItemId(int i) {
        Object adData = this.f21125.getAdData(i);
        return adData != null ? (long) (-System.identityHashCode(adData)) : this.f21123.getItemId(this.f21125.getOriginalPosition(i));
    }

    public int getItemViewType(int i) {
        int adViewType = this.f21125.getAdViewType(i);
        return adViewType != 0 ? (this.f21123.getViewTypeCount() + adViewType) - 1 : this.f21123.getItemViewType(this.f21125.getOriginalPosition(i));
    }

    public int getOriginalPosition(int i) {
        return this.f21125.getOriginalPosition(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View adView = this.f21125.getAdView(i, view, viewGroup);
        View view2 = adView != null ? adView : this.f21123.getView(this.f21125.getOriginalPosition(i), view, viewGroup);
        this.f21126.put(view2, Integer.valueOf(i));
        this.f21124.m27209(view2, 0);
        return view2;
    }

    public int getViewTypeCount() {
        return this.f21123.getViewTypeCount() + this.f21125.getAdViewTypeCount();
    }

    public boolean hasStableIds() {
        return this.f21123.hasStableIds();
    }

    public void insertItem(int i) {
        this.f21125.insertItem(i);
    }

    public boolean isAd(int i) {
        return this.f21125.isAd(i);
    }

    public boolean isEmpty() {
        return this.f21123.isEmpty() && this.f21125.getAdjustedCount(0) == 0;
    }

    public boolean isEnabled(int i) {
        return isAd(i) || ((this.f21123 instanceof ListAdapter) && ((ListAdapter) this.f21123).isEnabled(this.f21125.getOriginalPosition(i)));
    }

    public void loadAds(String str) {
        MoPubStreamAdPlacer moPubStreamAdPlacer = this.f21125;
        Pinkamena.DianePie();
    }

    public void loadAds(String str, RequestParameters requestParameters) {
        MoPubStreamAdPlacer moPubStreamAdPlacer = this.f21125;
        Pinkamena.DianePie();
    }

    public void refreshAds(ListView listView, String str) {
        Pinkamena.DianePie();
    }

    public void refreshAds(ListView listView, String str, RequestParameters requestParameters) {
        if (Preconditions.NoThrow.checkNotNull(listView, "You called MoPubAdAdapter.refreshAds with a null ListView")) {
            View childAt = listView.getChildAt(0);
            int top = childAt == null ? 0 : childAt.getTop();
            int firstVisiblePosition = listView.getFirstVisiblePosition();
            int max = Math.max(firstVisiblePosition - 1, 0);
            while (this.f21125.isAd(max) && max > 0) {
                max--;
            }
            int lastVisiblePosition = listView.getLastVisiblePosition();
            while (this.f21125.isAd(lastVisiblePosition) && lastVisiblePosition < getCount() - 1) {
                lastVisiblePosition++;
            }
            int originalPosition = this.f21125.getOriginalPosition(max);
            this.f21125.removeAdsInRange(this.f21125.getOriginalCount(lastVisiblePosition + 1), this.f21125.getOriginalCount(getCount()));
            int removeAdsInRange = this.f21125.removeAdsInRange(0, originalPosition);
            if (removeAdsInRange > 0) {
                listView.setSelectionFromTop(firstVisiblePosition - removeAdsInRange, top);
            }
            Pinkamena.DianePie();
        }
    }

    public final void registerAdRenderer(MoPubAdRenderer moPubAdRenderer) {
        if (Preconditions.NoThrow.checkNotNull(moPubAdRenderer, "Tried to set a null ad renderer on the placer.")) {
            this.f21125.registerAdRenderer(moPubAdRenderer);
        }
    }

    public void removeItem(int i) {
        this.f21125.removeItem(i);
    }

    public final void setAdLoadedListener(MoPubNativeAdLoadedListener moPubNativeAdLoadedListener) {
        this.f21122 = moPubNativeAdLoadedListener;
    }

    public void setOnClickListener(ListView listView, final AdapterView.OnItemClickListener onItemClickListener) {
        if (Preconditions.NoThrow.checkNotNull(listView, "You called MoPubAdAdapter.setOnClickListener with a null ListView")) {
            if (onItemClickListener == null) {
                listView.setOnItemClickListener((AdapterView.OnItemClickListener) null);
            } else {
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        if (!MoPubAdAdapter.this.f21125.isAd(i)) {
                            onItemClickListener.onItemClick(adapterView, view, MoPubAdAdapter.this.f21125.getOriginalPosition(i), j);
                        }
                    }
                });
            }
        }
    }

    public void setOnItemLongClickListener(ListView listView, final AdapterView.OnItemLongClickListener onItemLongClickListener) {
        if (Preconditions.NoThrow.checkNotNull(listView, "You called MoPubAdAdapter.setOnItemLongClickListener with a null ListView")) {
            if (onItemLongClickListener == null) {
                listView.setOnItemLongClickListener((AdapterView.OnItemLongClickListener) null);
            } else {
                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                        if (!MoPubAdAdapter.this.isAd(i)) {
                            return onItemLongClickListener.onItemLongClick(adapterView, view, MoPubAdAdapter.this.f21125.getOriginalPosition(i), j);
                        }
                    }
                });
            }
        }
    }

    public void setOnItemSelectedListener(ListView listView, final AdapterView.OnItemSelectedListener onItemSelectedListener) {
        if (Preconditions.NoThrow.checkNotNull(listView, "You called MoPubAdAdapter.setOnItemSelectedListener with a null ListView")) {
            if (onItemSelectedListener == null) {
                listView.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) null);
            } else {
                listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                        if (!MoPubAdAdapter.this.isAd(i)) {
                            onItemSelectedListener.onItemSelected(adapterView, view, MoPubAdAdapter.this.f21125.getOriginalPosition(i), j);
                        }
                    }

                    public void onNothingSelected(AdapterView<?> adapterView) {
                        onItemSelectedListener.onNothingSelected(adapterView);
                    }
                });
            }
        }
    }

    public void setSelection(ListView listView, int i) {
        if (Preconditions.NoThrow.checkNotNull(listView, "You called MoPubAdAdapter.setSelection with a null ListView")) {
            listView.setSelection(this.f21125.getAdjustedPosition(i));
        }
    }

    public void smoothScrollToPosition(ListView listView, int i) {
        if (Preconditions.NoThrow.checkNotNull(listView, "You called MoPubAdAdapter.smoothScrollToPosition with a null ListView")) {
            listView.smoothScrollToPosition(this.f21125.getAdjustedPosition(i));
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27073(int i) {
        if (this.f21122 != null) {
            this.f21122.onAdRemoved(i);
        }
        notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27074(int i) {
        if (this.f21122 != null) {
            this.f21122.onAdLoaded(i);
        }
        notifyDataSetChanged();
    }
}
