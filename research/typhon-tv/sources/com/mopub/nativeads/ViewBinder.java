package com.mopub.nativeads;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ViewBinder {

    /* renamed from: ʻ  reason: contains not printable characters */
    final int f21302;

    /* renamed from: ʼ  reason: contains not printable characters */
    final int f21303;

    /* renamed from: ʽ  reason: contains not printable characters */
    final Map<String, Integer> f21304;

    /* renamed from: 连任  reason: contains not printable characters */
    final int f21305;

    /* renamed from: 靐  reason: contains not printable characters */
    final int f21306;

    /* renamed from: 麤  reason: contains not printable characters */
    final int f21307;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f21308;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f21309;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f21310;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f21311;
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public Map<String, Integer> f21312 = Collections.emptyMap();
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public int f21313;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public int f21314;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public int f21315;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public int f21316;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final int f21317;

        public Builder(int i) {
            this.f21317 = i;
            this.f21312 = new HashMap();
        }

        public final Builder addExtra(String str, int i) {
            this.f21312.put(str, Integer.valueOf(i));
            return this;
        }

        public final Builder addExtras(Map<String, Integer> map) {
            this.f21312 = new HashMap(map);
            return this;
        }

        public final ViewBinder build() {
            return new ViewBinder(this);
        }

        public final Builder callToActionId(int i) {
            this.f21315 = i;
            return this;
        }

        public final Builder iconImageId(int i) {
            this.f21310 = i;
            return this;
        }

        public final Builder mainImageId(int i) {
            this.f21313 = i;
            return this;
        }

        public final Builder privacyInformationIconImageId(int i) {
            this.f21311 = i;
            return this;
        }

        public final Builder textId(int i) {
            this.f21316 = i;
            return this;
        }

        public final Builder titleId(int i) {
            this.f21314 = i;
            return this;
        }
    }

    private ViewBinder(Builder builder) {
        this.f21309 = builder.f21317;
        this.f21306 = builder.f21314;
        this.f21308 = builder.f21316;
        this.f21307 = builder.f21315;
        this.f21305 = builder.f21313;
        this.f21302 = builder.f21310;
        this.f21303 = builder.f21311;
        this.f21304 = builder.f21312;
    }
}
