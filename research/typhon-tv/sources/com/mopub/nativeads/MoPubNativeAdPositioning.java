package com.mopub.nativeads;

import com.mopub.common.Preconditions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class MoPubNativeAdPositioning {

    public static class MoPubClientPositioning {
        public static final int NO_REPEAT = Integer.MAX_VALUE;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public int f21157 = NO_REPEAT;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final ArrayList<Integer> f21158 = new ArrayList<>();

        public MoPubClientPositioning addFixedPosition(int i) {
            int binarySearch;
            if (Preconditions.NoThrow.checkArgument(i >= 0) && (binarySearch = Collections.binarySearch(this.f21158, Integer.valueOf(i))) < 0) {
                this.f21158.add(binarySearch ^ -1, Integer.valueOf(i));
            }
            return this;
        }

        public MoPubClientPositioning enableRepeatingPositions(int i) {
            boolean z = true;
            if (i <= 1) {
                z = false;
            }
            if (!Preconditions.NoThrow.checkArgument(z, "Repeating interval must be greater than 1")) {
                this.f21157 = NO_REPEAT;
            } else {
                this.f21157 = i;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m27097() {
            return this.f21157;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public List<Integer> m27098() {
            return this.f21158;
        }
    }

    public static class MoPubServerPositioning {
    }

    public static MoPubClientPositioning clientPositioning() {
        return new MoPubClientPositioning();
    }

    public static MoPubServerPositioning serverPositioning() {
        return new MoPubServerPositioning();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static MoPubClientPositioning m27093(MoPubClientPositioning moPubClientPositioning) {
        Preconditions.checkNotNull(moPubClientPositioning);
        MoPubClientPositioning moPubClientPositioning2 = new MoPubClientPositioning();
        moPubClientPositioning2.f21158.addAll(moPubClientPositioning.f21158);
        int unused = moPubClientPositioning2.f21157 = moPubClientPositioning.f21157;
        return moPubClientPositioning2;
    }
}
