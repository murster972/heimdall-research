package com.mopub.nativeads;

import java.util.List;
import java.util.Map;

public interface FlurryBaseNativeAd {
    void addExtra(String str, Object obj);

    void fetchAd();

    String getCallToAction();

    Map<String, Object> getExtras();

    String getIconImageUrl();

    List<String> getImageUrls();

    String getMainImageUrl();

    Double getStarRating();

    String getText();

    String getTitle();

    boolean isAppInstallAd();

    void onNativeAdLoaded();

    void precacheImages();

    void setCallToAction(String str);

    void setIconImageUrl(String str);

    void setMainImageUrl(String str);

    void setStarRating(Double d);

    void setText(String str);

    void setTitle(String str);
}
