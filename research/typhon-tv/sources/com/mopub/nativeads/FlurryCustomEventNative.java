package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.Pinkamena;
import com.flurry.android.FlurryAgentListener;
import com.flurry.android.ads.FlurryAdErrorType;
import com.flurry.android.ads.FlurryAdNative;
import com.flurry.android.ads.FlurryAdNativeAsset;
import com.flurry.android.ads.FlurryAdNativeListener;
import com.flurry.android.ads.FlurryAdTargeting;
import com.mopub.mobileads.FlurryAgentWrapper;
import com.mopub.nativeads.CustomEventNative;
import com.mopub.nativeads.NativeImageHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public final class FlurryCustomEventNative extends CustomEventNative {
    public static final String EXTRA_APP_CATEGORY = "flurry_appcategorytext";
    public static final String EXTRA_SEC_BRANDING_LOGO = "flurry_brandingimage";
    public static final String EXTRA_STAR_RATING_IMG = "flurry_starratingimage";
    public static final String LOCAL_EXTRA_TEST_MODE = "enableTestMode";
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final List<FlurryAdNative> f21095 = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final String f21096 = FlurryCustomEventNative.class.getSimpleName();

    /* renamed from: 齉  reason: contains not printable characters */
    private FlurryAgentListener f21097;

    static abstract class FlurryBaseAdListener implements FlurryAdNativeListener {

        /* renamed from: 龘  reason: contains not printable characters */
        private final FlurryBaseNativeAd f21103;

        FlurryBaseAdListener(FlurryBaseNativeAd flurryBaseNativeAd) {
            this.f21103 = flurryBaseNativeAd;
        }

        public void onAppExit(FlurryAdNative flurryAdNative) {
        }

        public void onClicked(FlurryAdNative flurryAdNative) {
        }

        public void onCloseFullscreen(FlurryAdNative flurryAdNative) {
        }

        public void onCollapsed(FlurryAdNative flurryAdNative) {
        }

        public void onError(FlurryAdNative flurryAdNative, FlurryAdErrorType flurryAdErrorType, int i) {
            Log.d(FlurryCustomEventNative.f21096, String.format("onError: Flurry native ad not available. Error type: %s. Error code: %s", new Object[]{flurryAdErrorType.toString(), Integer.valueOf(i)}));
            FlurryCustomEventNative.f21095.remove(flurryAdNative);
        }

        public void onExpanded(FlurryAdNative flurryAdNative) {
        }

        public void onFetched(FlurryAdNative flurryAdNative) {
            FlurryCustomEventNative.m27055(this.f21103, flurryAdNative);
            FlurryCustomEventNative.f21095.remove(flurryAdNative);
        }

        public void onImpressionLogged(FlurryAdNative flurryAdNative) {
        }

        public void onShowFullscreen(FlurryAdNative flurryAdNative) {
        }
    }

    private static class FlurryStaticNativeAd extends StaticNativeAd implements FlurryBaseNativeAd {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final CustomEventNative.CustomEventNativeListener f21104;

        /* renamed from: 麤  reason: contains not printable characters */
        private final FlurryAdNativeListener f21105 = new FlurryBaseAdListener(this) {
            public void onClicked(FlurryAdNative flurryAdNative) {
                super.onClicked(flurryAdNative);
                FlurryStaticNativeAd.this.m27046();
            }

            public void onError(FlurryAdNative flurryAdNative, FlurryAdErrorType flurryAdErrorType, int i) {
                super.onError(flurryAdNative, flurryAdErrorType, i);
                FlurryStaticNativeAd.this.f21104.onNativeAdFailed(NativeErrorCode.NETWORK_NO_FILL);
            }

            public void onImpressionLogged(FlurryAdNative flurryAdNative) {
                super.onImpressionLogged(flurryAdNative);
                FlurryStaticNativeAd.this.m27050();
            }
        };

        /* renamed from: 齉  reason: contains not printable characters */
        private final FlurryAdNative f21106;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Context f21107;

        FlurryStaticNativeAd(Context context, FlurryAdNative flurryAdNative, CustomEventNative.CustomEventNativeListener customEventNativeListener) {
            this.f21107 = context;
            this.f21106 = flurryAdNative;
            this.f21104 = customEventNativeListener;
        }

        public void clear(View view) {
            this.f21106.removeTrackingView();
        }

        public void destroy() {
            this.f21106.destroy();
            FlurryAgentWrapper.getInstance().endSession(this.f21107);
        }

        public synchronized void fetchAd() {
            this.f21106.setListener(this.f21105);
            FlurryAdNative flurryAdNative = this.f21106;
            Pinkamena.DianePie();
        }

        public List<String> getImageUrls() {
            ArrayList arrayList = new ArrayList(2);
            if (getMainImageUrl() != null) {
                arrayList.add(getMainImageUrl());
            }
            if (getIconImageUrl() != null) {
                arrayList.add(getIconImageUrl());
            }
            return arrayList;
        }

        public boolean isAppInstallAd() {
            return (this.f21106.getAsset("secRatingImg") == null && this.f21106.getAsset("secHqRatingImg") == null && this.f21106.getAsset("appCategory") == null) ? false : true;
        }

        public void onNativeAdLoaded() {
            this.f21104.onNativeAdLoaded(this);
        }

        public void precacheImages() {
            NativeImageHelper.preCacheImages(this.f21107, getImageUrls(), new NativeImageHelper.ImageListener() {
                public void onImagesCached() {
                    FlurryStaticNativeAd.this.f21104.onNativeAdLoaded(FlurryStaticNativeAd.this);
                }

                public void onImagesFailedToCache(NativeErrorCode nativeErrorCode) {
                    FlurryStaticNativeAd.this.f21104.onNativeAdFailed(nativeErrorCode);
                    Log.d(FlurryCustomEventNative.f21096, "preCacheImages: Unable to cache Ad image. Error[" + nativeErrorCode.toString() + "]");
                }
            });
        }

        public void prepare(View view) {
            this.f21106.setTrackingView(view);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static synchronized void m27055(FlurryBaseNativeAd flurryBaseNativeAd, FlurryAdNative flurryAdNative) {
        synchronized (FlurryCustomEventNative.class) {
            FlurryAdNativeAsset asset = flurryAdNative.getAsset("secHqImage");
            FlurryAdNativeAsset asset2 = flurryAdNative.getAsset("secImage");
            if (asset != null && !TextUtils.isEmpty(asset.getValue())) {
                flurryBaseNativeAd.setMainImageUrl(asset.getValue());
            }
            if (asset2 != null && !TextUtils.isEmpty(asset2.getValue())) {
                flurryBaseNativeAd.setIconImageUrl(asset2.getValue());
            }
            flurryBaseNativeAd.setTitle(flurryAdNative.getAsset("headline").getValue());
            flurryBaseNativeAd.setText(flurryAdNative.getAsset("summary").getValue());
            flurryBaseNativeAd.addExtra(EXTRA_SEC_BRANDING_LOGO, flurryAdNative.getAsset("secHqBrandingLogo").getValue());
            if (flurryBaseNativeAd.isAppInstallAd()) {
                FlurryAdNativeAsset asset3 = flurryAdNative.getAsset("secHqRatingImg");
                if (asset3 == null || TextUtils.isEmpty(asset3.getValue())) {
                    FlurryAdNativeAsset asset4 = flurryAdNative.getAsset("secRatingImg");
                    if (asset4 != null && !TextUtils.isEmpty(asset4.getValue())) {
                        flurryBaseNativeAd.addExtra(EXTRA_STAR_RATING_IMG, asset4.getValue());
                    }
                } else {
                    flurryBaseNativeAd.addExtra(EXTRA_STAR_RATING_IMG, asset3.getValue());
                }
                FlurryAdNativeAsset asset5 = flurryAdNative.getAsset("appCategory");
                if (asset5 != null) {
                    flurryBaseNativeAd.addExtra(EXTRA_APP_CATEGORY, asset5.getValue());
                }
                FlurryAdNativeAsset asset6 = flurryAdNative.getAsset("appRating");
                if (asset6 != null) {
                    flurryBaseNativeAd.setStarRating(m27056(asset6.getValue()));
                }
            }
            FlurryAdNativeAsset asset7 = flurryAdNative.getAsset("callToAction");
            if (asset7 != null) {
                flurryBaseNativeAd.setCallToAction(asset7.getValue());
            }
            if (flurryBaseNativeAd.getImageUrls().isEmpty()) {
                Log.d(f21096, "preCacheImages: No images to cache for Flurry Native Ad: " + flurryAdNative.toString());
                flurryBaseNativeAd.onNativeAdLoaded();
            } else {
                flurryBaseNativeAd.precacheImages();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Double m27056(String str) {
        if (str == null) {
            return null;
        }
        String[] split = str.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR);
        if (split.length != 2) {
            return null;
        }
        try {
            return Double.valueOf(((double) (((float) Integer.valueOf(split[0]).intValue()) / ((float) Integer.valueOf(split[1]).intValue()))) * 5.0d);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27058(Context context, String str, Map<String, Object> map, CustomEventNative.CustomEventNativeListener customEventNativeListener) {
        FlurryAdNative flurryAdNative = new FlurryAdNative(context, str);
        if (map.containsKey(LOCAL_EXTRA_TEST_MODE) && (map.get(LOCAL_EXTRA_TEST_MODE) instanceof Boolean)) {
            new FlurryAdTargeting().setEnableTestAds(((Boolean) map.get(LOCAL_EXTRA_TEST_MODE)).booleanValue());
        }
        new FlurryStaticNativeAd(context, flurryAdNative, customEventNativeListener);
        f21095.add(flurryAdNative);
        Pinkamena.DianePie();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m27061(Map<String, String> map) {
        String str = map.get(FlurryAgentWrapper.PARAM_API_KEY);
        String str2 = map.get(FlurryAgentWrapper.PARAM_AD_SPACE_NAME);
        Log.i(f21096, "ServerInfo fetched from Mopub apiKey : " + str + " and " + FlurryAgentWrapper.PARAM_AD_SPACE_NAME + " :" + str2);
        return !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27062(Context context, CustomEventNative.CustomEventNativeListener customEventNativeListener, Map<String, Object> map, Map<String, String> map2) {
        String str;
        Object obj;
        if (m27061(map2)) {
            String str2 = map2.get(FlurryAgentWrapper.PARAM_API_KEY);
            final String str3 = map2.get(FlurryAgentWrapper.PARAM_AD_SPACE_NAME);
            boolean z = true;
            boolean z2 = false;
            if (!map.isEmpty() && map.containsKey("isBanner")) {
                Object obj2 = map.get("isBanner");
                if (obj2 != null && (obj2 instanceof Boolean)) {
                    z2 = ((Boolean) obj2).booleanValue();
                }
                if (map.containsKey("loadFlurryNativeBanner") && (obj = map.get("loadFlurryNativeBanner")) != null && (obj instanceof Boolean)) {
                    z = ((Boolean) obj).booleanValue();
                }
                if (!map2.isEmpty() && map2.containsKey("loadFlurryNativeBanner") && (str = map2.get("loadFlurryNativeBanner")) != null) {
                    if (str.equalsIgnoreCase("true")) {
                        z = true;
                    } else if (str.equalsIgnoreCase("false")) {
                        z = false;
                    }
                }
                if (z2 && !z) {
                    customEventNativeListener.onNativeAdFailed(NativeErrorCode.UNSPECIFIED);
                    return;
                }
            }
            if (FlurryAgentWrapper.getInstance().isSessionActive() || this.f21097 != null) {
                m27058(context, str3, map, customEventNativeListener);
                return;
            }
            final Context context2 = context;
            final Map<String, Object> map3 = map;
            final CustomEventNative.CustomEventNativeListener customEventNativeListener2 = customEventNativeListener;
            this.f21097 = new FlurryAgentListener() {
                public void onSessionStarted() {
                    FlurryCustomEventNative.this.m27058(context2, str3, (Map<String, Object>) map3, customEventNativeListener2);
                }
            };
            FlurryAgentWrapper.getInstance().startSession(context, str2, this.f21097);
            return;
        }
        customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
        Log.i(f21096, "Failed Native AdFetch: Missing required server extras [FLURRY_APIKEY and/or FLURRY_ADSPACE].");
    }
}
