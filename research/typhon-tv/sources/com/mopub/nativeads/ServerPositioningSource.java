package com.mopub.nativeads;

import android.content.Context;
import android.os.Handler;
import com.mopub.common.TyphoonApp;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.PositioningSource;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.Networking;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyError;

class ServerPositioningSource implements PositioningSource {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f21266 = 300000;

    /* renamed from: ʼ  reason: contains not printable characters */
    private PositioningSource.PositioningListener f21267;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f21268;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f21269;

    /* renamed from: ٴ  reason: contains not printable characters */
    private PositioningRequest f21270;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Response.ErrorListener f21271;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Handler f21272;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Response.Listener<MoPubNativeAdPositioning.MoPubClientPositioning> f21273;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Runnable f21274;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context f21275;

    ServerPositioningSource(Context context) {
        this.f21275 = context.getApplicationContext();
        this.f21272 = new Handler();
        this.f21274 = new Runnable() {
            public void run() {
                ServerPositioningSource.this.m27184();
            }
        };
        this.f21273 = new Response.Listener<MoPubNativeAdPositioning.MoPubClientPositioning>() {
            public void onResponse(MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
                ServerPositioningSource.this.m27185(moPubClientPositioning);
            }
        };
        this.f21271 = new Response.ErrorListener() {
            public void onErrorResponse(VolleyError volleyError) {
                if (!(volleyError instanceof MoPubNetworkError) || ((MoPubNetworkError) volleyError).getReason().equals(MoPubNetworkError.Reason.WARMING_UP)) {
                    MoPubLog.e("Failed to load positioning data", volleyError);
                    if (volleyError.networkResponse == null && !DeviceUtils.isNetworkAvailable(ServerPositioningSource.this.f21275)) {
                        MoPubLog.c(String.valueOf(MoPubErrorCode.NO_CONNECTION.toString()));
                    }
                }
                ServerPositioningSource.this.m27182();
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27182() {
        int pow = (int) (1000.0d * Math.pow(2.0d, (double) (this.f21268 + 1)));
        if (pow >= this.f21266) {
            if (this.f21267 != null) {
                this.f21267.onFailed();
            }
            this.f21267 = null;
            return;
        }
        this.f21268++;
        this.f21272.postDelayed(this.f21274, (long) pow);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27184() {
        this.f21270 = new PositioningRequest(this.f21269, this.f21273, this.f21271);
        Networking.getRequestQueue(this.f21275).add(this.f21270);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27185(MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
        if (this.f21267 != null) {
            this.f21267.onLoad(moPubClientPositioning);
        }
        this.f21267 = null;
        this.f21268 = 0;
    }

    public void loadPositions(String str, PositioningSource.PositioningListener positioningListener) {
        if (this.f21270 != null) {
            this.f21270.cancel();
            this.f21270 = null;
        }
        if (this.f21268 > 0) {
            this.f21272.removeCallbacks(this.f21274);
            this.f21268 = 0;
        }
        this.f21267 = positioningListener;
        this.f21269 = new PositioningUrlGenerator(this.f21275).withAdUnitId(str).generateUrlString(TyphoonApp.HOST);
        m27184();
    }
}
