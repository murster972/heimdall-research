package com.mopub.nativeads;

import android.content.Context;
import com.Pinkamena;
import com.mopub.common.AdFormat;
import com.mopub.common.GpsHelper;
import com.mopub.common.Preconditions;
import com.mopub.common.TyphoonApp;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.ManifestUtils;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.nativeads.CustomEventNative;
import com.mopub.network.AdRequest;
import com.mopub.network.AdResponse;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.Networking;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.VolleyError;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.TreeMap;

public class MoPubNative {

    /* renamed from: 龘  reason: contains not printable characters */
    static final MoPubNativeNetworkListener f21145 = new MoPubNativeNetworkListener() {
        public void onNativeFail(NativeErrorCode nativeErrorCode) {
        }

        public void onNativeLoad(NativeAd nativeAd) {
            nativeAd.destroy();
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public MoPubNativeNetworkListener f21146;

    /* renamed from: ʼ  reason: contains not printable characters */
    private Map<String, Object> f21147;

    /* renamed from: ʽ  reason: contains not printable characters */
    private AdRequest f21148;

    /* renamed from: 连任  reason: contains not printable characters */
    private final AdRequest.Listener f21149;

    /* renamed from: 靐  reason: contains not printable characters */
    AdRendererRegistry f21150;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final String f21151;

    /* renamed from: 齉  reason: contains not printable characters */
    private final WeakReference<Context> f21152;

    public interface MoPubNativeNetworkListener {
        void onNativeFail(NativeErrorCode nativeErrorCode);

        void onNativeLoad(NativeAd nativeAd);
    }

    @VisibleForTesting
    public MoPubNative(Context context, String str, AdRendererRegistry adRendererRegistry, MoPubNativeNetworkListener moPubNativeNetworkListener) {
        this.f21147 = new TreeMap();
        Preconditions.checkNotNull(context, "context may not be null.");
        Preconditions.checkNotNull(str, "AdUnitId may not be null.");
        Preconditions.checkNotNull(adRendererRegistry, "AdRendererRegistry may not be null.");
        Preconditions.checkNotNull(moPubNativeNetworkListener, "MoPubNativeNetworkListener may not be null.");
        ManifestUtils.checkNativeActivitiesDeclared(context);
        this.f21152 = new WeakReference<>(context);
        this.f21151 = str;
        this.f21146 = moPubNativeNetworkListener;
        this.f21150 = adRendererRegistry;
        this.f21149 = new AdRequest.Listener() {
            public void onErrorResponse(VolleyError volleyError) {
                MoPubNative.this.m27091(volleyError);
            }

            public void onSuccess(AdResponse adResponse) {
                MoPubNative.this.m27089(adResponse);
            }
        };
        GpsHelper.fetchAdvertisingInfoAsync(context, (GpsHelper.GpsHelperListener) null);
    }

    public MoPubNative(Context context, String str, MoPubNativeNetworkListener moPubNativeNetworkListener) {
        this(context, str, new AdRendererRegistry(), moPubNativeNetworkListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27088(RequestParameters requestParameters, Integer num) {
        Context r0 = m27090();
        if (r0 != null) {
            NativeUrlGenerator r2 = new NativeUrlGenerator(r0).withAdUnitId(this.f21151).m27150(requestParameters);
            if (num != null) {
                r2.m27149(num.intValue());
            }
            String generateUrlString = r2.generateUrlString(TyphoonApp.HOST);
            if (generateUrlString != null) {
            }
            m27092(generateUrlString);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27089(final AdResponse adResponse) {
        if (m27090() != null) {
            new CustomEventNative.CustomEventNativeListener() {
                public void onNativeAdFailed(NativeErrorCode nativeErrorCode) {
                    MoPubLog.v(String.format("Native Ad failed to load with error: %s.", new Object[]{nativeErrorCode}));
                    MoPubNative.this.m27092(adResponse.getFailoverUrl());
                }

                public void onNativeAdLoaded(BaseNativeAd baseNativeAd) {
                    Context r1 = MoPubNative.this.m27090();
                    if (r1 != null) {
                        MoPubAdRenderer rendererForAd = MoPubNative.this.f21150.getRendererForAd(baseNativeAd);
                        if (rendererForAd == null) {
                            onNativeAdFailed(NativeErrorCode.NATIVE_RENDERER_CONFIGURATION_ERROR);
                        } else {
                            MoPubNative.this.f21146.onNativeLoad(new NativeAd(r1, adResponse.getImpressionTrackingUrl(), adResponse.getClickTrackingUrl(), MoPubNative.this.f21151, baseNativeAd, rendererForAd));
                        }
                    }
                }
            };
            Map<String, Object> map = this.f21147;
            Pinkamena.DianePie();
        }
    }

    public void destroy() {
        this.f21152.clear();
        if (this.f21148 != null) {
            this.f21148.cancel();
            this.f21148 = null;
        }
        this.f21146 = f21145;
    }

    public void makeRequest() {
        makeRequest((RequestParameters) null);
    }

    public void makeRequest(RequestParameters requestParameters) {
        makeRequest(requestParameters, (Integer) null);
    }

    public void makeRequest(RequestParameters requestParameters, Integer num) {
        Context r0 = m27090();
        if (r0 != null) {
            if (!DeviceUtils.isNetworkAvailable(r0)) {
                this.f21146.onNativeFail(NativeErrorCode.CONNECTION_ERROR);
            } else {
                m27088(requestParameters, num);
            }
        }
    }

    public void registerAdRenderer(MoPubAdRenderer moPubAdRenderer) {
        this.f21150.registerAdRenderer(moPubAdRenderer);
    }

    public void setLocalExtras(Map<String, Object> map) {
        if (map == null) {
            this.f21147 = new TreeMap();
        } else {
            this.f21147 = new TreeMap(map);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public Context m27090() {
        Context context = (Context) this.f21152.get();
        if (context == null) {
            destroy();
            MoPubLog.d("Weak reference to Context in MoPubNative became null. This instance of MoPubNative is destroyed and No more requests will be processed.");
        }
        return context;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27091(VolleyError volleyError) {
        if (volleyError instanceof MoPubNetworkError) {
            switch (((MoPubNetworkError) volleyError).getReason()) {
                case BAD_BODY:
                    this.f21146.onNativeFail(NativeErrorCode.INVALID_RESPONSE);
                    return;
                case BAD_HEADER_DATA:
                    this.f21146.onNativeFail(NativeErrorCode.INVALID_RESPONSE);
                    return;
                case WARMING_UP:
                    MoPubLog.c(MoPubErrorCode.WARMUP.toString());
                    this.f21146.onNativeFail(NativeErrorCode.EMPTY_AD_RESPONSE);
                    return;
                case NO_FILL:
                    this.f21146.onNativeFail(NativeErrorCode.EMPTY_AD_RESPONSE);
                    return;
                default:
                    this.f21146.onNativeFail(NativeErrorCode.UNSPECIFIED);
                    return;
            }
        } else {
            NetworkResponse networkResponse = volleyError.networkResponse;
            if (networkResponse != null && networkResponse.statusCode >= 500 && networkResponse.statusCode < 600) {
                this.f21146.onNativeFail(NativeErrorCode.SERVER_ERROR_RESPONSE_CODE);
            } else if (networkResponse != null || DeviceUtils.isNetworkAvailable((Context) this.f21152.get())) {
                this.f21146.onNativeFail(NativeErrorCode.UNSPECIFIED);
            } else {
                MoPubLog.c(String.valueOf(MoPubErrorCode.NO_CONNECTION.toString()));
                this.f21146.onNativeFail(NativeErrorCode.CONNECTION_ERROR);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27092(String str) {
        Context r4 = m27090();
        if (r4 != null) {
            if (str == null) {
                this.f21146.onNativeFail(NativeErrorCode.INVALID_REQUEST_URL);
                return;
            }
            this.f21148 = new AdRequest(str, AdFormat.NATIVE, this.f21151, r4, this.f21149);
            Networking.getRequestQueue(r4).add(this.f21148);
        }
    }
}
