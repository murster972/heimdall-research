package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.lang.ref.WeakReference;

@Deprecated
public final class AdapterHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Context f5790;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f5791;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f5792;

    /* renamed from: 龘  reason: contains not printable characters */
    private final WeakReference<Context> f5793;

    @Deprecated
    public AdapterHelper(Context context, int i, int i2) {
        boolean z = true;
        Preconditions.checkNotNull(context, "Context cannot be null.");
        Preconditions.checkArgument(i >= 0, "start position must be non-negative");
        Preconditions.checkArgument(i2 < 2 ? false : z, "interval must be at least 2");
        this.f5793 = new WeakReference<>(context);
        this.f5790 = context.getApplicationContext();
        this.f5792 = i;
        this.f5791 = i2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m6246(int i) {
        if (i <= this.f5792) {
            return 0;
        }
        int i2 = this.f5791 - 1;
        return (i - this.f5792) % i2 == 0 ? (i - this.f5792) / i2 : ((int) Math.floor(((double) (i - this.f5792)) / ((double) i2))) + 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m6247(int i) {
        if (i <= this.f5792) {
            return 0;
        }
        return ((int) Math.floor(((double) (i - this.f5792)) / ((double) this.f5791))) + 1;
    }

    @Deprecated
    public View getAdView(View view, ViewGroup viewGroup, NativeAd nativeAd) {
        return getAdView(view, viewGroup, nativeAd, (ViewBinder) null);
    }

    @Deprecated
    public View getAdView(View view, ViewGroup viewGroup, NativeAd nativeAd, ViewBinder viewBinder) {
        Context context = (Context) this.f5793.get();
        if (context != null) {
            return NativeAdViewHelper.m6248(view, viewGroup, context, nativeAd);
        }
        MoPubLog.w("Weak reference to Context in AdapterHelper became null. Returning empty view.");
        return new View(this.f5790);
    }

    @Deprecated
    public boolean isAdPosition(int i) {
        return i >= this.f5792 && (i - this.f5792) % this.f5791 == 0;
    }

    @Deprecated
    public int shiftedCount(int i) {
        return m6246(i) + i;
    }

    @Deprecated
    public int shiftedPosition(int i) {
        return i - m6247(i);
    }
}
