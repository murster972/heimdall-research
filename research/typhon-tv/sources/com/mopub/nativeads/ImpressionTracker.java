package com.mopub.nativeads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.mopub.common.VisibleForTesting;
import com.mopub.nativeads.VisibilityTracker;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class ImpressionTracker {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final VisibilityTracker.VisibilityChecker f21110;

    /* renamed from: ʼ  reason: contains not printable characters */
    private VisibilityTracker.VisibilityTrackerListener f21111;

    /* renamed from: 连任  reason: contains not printable characters */
    private final PollingRunnable f21112;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Map<View, ImpressionInterface> f21113;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Handler f21114;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Map<View, TimestampWrapper<ImpressionInterface>> f21115;

    /* renamed from: 龘  reason: contains not printable characters */
    private final VisibilityTracker f21116;

    @VisibleForTesting
    class PollingRunnable implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ArrayList<View> f21118 = new ArrayList<>();

        PollingRunnable() {
        }

        public void run() {
            for (Map.Entry entry : ImpressionTracker.this.f21115.entrySet()) {
                View view = (View) entry.getKey();
                TimestampWrapper timestampWrapper = (TimestampWrapper) entry.getValue();
                if (ImpressionTracker.this.f21110.m27213(timestampWrapper.f21300, ((ImpressionInterface) timestampWrapper.f21301).getImpressionMinTimeViewed())) {
                    ((ImpressionInterface) timestampWrapper.f21301).recordImpression(view);
                    ((ImpressionInterface) timestampWrapper.f21301).setImpressionRecorded();
                    this.f21118.add(view);
                }
            }
            Iterator<View> it2 = this.f21118.iterator();
            while (it2.hasNext()) {
                ImpressionTracker.this.removeView(it2.next());
            }
            this.f21118.clear();
            if (!ImpressionTracker.this.f21115.isEmpty()) {
                ImpressionTracker.this.m27068();
            }
        }
    }

    public ImpressionTracker(Context context) {
        this(new WeakHashMap(), new WeakHashMap(), new VisibilityTracker.VisibilityChecker(), new VisibilityTracker(context), new Handler(Looper.getMainLooper()));
    }

    @VisibleForTesting
    ImpressionTracker(Map<View, ImpressionInterface> map, Map<View, TimestampWrapper<ImpressionInterface>> map2, VisibilityTracker.VisibilityChecker visibilityChecker, VisibilityTracker visibilityTracker, Handler handler) {
        this.f21113 = map;
        this.f21115 = map2;
        this.f21110 = visibilityChecker;
        this.f21116 = visibilityTracker;
        this.f21111 = new VisibilityTracker.VisibilityTrackerListener() {
            public void onVisibilityChanged(List<View> list, List<View> list2) {
                for (View next : list) {
                    ImpressionInterface impressionInterface = (ImpressionInterface) ImpressionTracker.this.f21113.get(next);
                    if (impressionInterface == null) {
                        ImpressionTracker.this.removeView(next);
                    } else {
                        TimestampWrapper timestampWrapper = (TimestampWrapper) ImpressionTracker.this.f21115.get(next);
                        if (timestampWrapper == null || !impressionInterface.equals(timestampWrapper.f21301)) {
                            ImpressionTracker.this.f21115.put(next, new TimestampWrapper(impressionInterface));
                        }
                    }
                }
                for (View remove : list2) {
                    ImpressionTracker.this.f21115.remove(remove);
                }
                ImpressionTracker.this.m27068();
            }
        };
        this.f21116.m27212(this.f21111);
        this.f21114 = handler;
        this.f21112 = new PollingRunnable();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27067(View view) {
        this.f21115.remove(view);
    }

    public void addView(View view, ImpressionInterface impressionInterface) {
        if (this.f21113.get(view) != impressionInterface) {
            removeView(view);
            if (!impressionInterface.isImpressionRecorded()) {
                this.f21113.put(view, impressionInterface);
                this.f21116.m27209(view, impressionInterface.getImpressionMinPercentageViewed());
            }
        }
    }

    public void clear() {
        this.f21113.clear();
        this.f21115.clear();
        this.f21116.m27207();
        this.f21114.removeMessages(0);
    }

    public void destroy() {
        clear();
        this.f21116.m27205();
        this.f21111 = null;
    }

    public void removeView(View view) {
        this.f21113.remove(view);
        m27067(view);
        this.f21116.m27208(view);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27068() {
        if (!this.f21114.hasMessages(0)) {
            this.f21114.postDelayed(this.f21112, 250);
        }
    }
}
