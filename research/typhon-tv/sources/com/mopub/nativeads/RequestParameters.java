package com.mopub.nativeads;

import android.location.Location;
import android.text.TextUtils;
import com.google.android.exoplayer2.util.MimeTypes;
import java.util.EnumSet;
import net.pubnative.library.request.PubnativeAsset;

public class RequestParameters {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Location f21259;

    /* renamed from: 齉  reason: contains not printable characters */
    private final EnumSet<NativeAdAsset> f21260;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f21261;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public Location f21262;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public EnumSet<NativeAdAsset> f21263;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public String f21264;

        public final RequestParameters build() {
            return new RequestParameters(this);
        }

        public final Builder desiredAssets(EnumSet<NativeAdAsset> enumSet) {
            this.f21263 = EnumSet.copyOf(enumSet);
            return this;
        }

        public final Builder keywords(String str) {
            this.f21264 = str;
            return this;
        }

        public final Builder location(Location location) {
            this.f21262 = location;
            return this;
        }
    }

    public enum NativeAdAsset {
        TITLE(PubnativeAsset.TITLE),
        TEXT(MimeTypes.BASE_TYPE_TEXT),
        ICON_IMAGE("iconimage"),
        MAIN_IMAGE("mainimage"),
        CALL_TO_ACTION_TEXT("ctatext"),
        STAR_RATING("starrating");
        
        private final String mAssetName;

        private NativeAdAsset(String str) {
            this.mAssetName = str;
        }

        public String toString() {
            return this.mAssetName;
        }
    }

    private RequestParameters(Builder builder) {
        this.f21261 = builder.f21264;
        this.f21259 = builder.f21262;
        this.f21260 = builder.f21263;
    }

    public final String getDesiredAssets() {
        return this.f21260 != null ? TextUtils.join(",", this.f21260.toArray()) : "";
    }

    public final String getKeywords() {
        return this.f21261;
    }

    public final Location getLocation() {
        return this.f21259;
    }
}
