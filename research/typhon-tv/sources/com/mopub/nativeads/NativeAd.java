package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.VisibleForTesting;
import com.mopub.nativeads.BaseNativeAd;
import com.mopub.network.TrackingRequest;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

public class NativeAd {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f21205;

    /* renamed from: ʼ  reason: contains not printable characters */
    private MoPubNativeEventListener f21206;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f21207;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f21208;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f21209;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Set<String> f21210;

    /* renamed from: 靐  reason: contains not printable characters */
    private final BaseNativeAd f21211;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Set<String> f21212 = new HashSet();

    /* renamed from: 齉  reason: contains not printable characters */
    private final MoPubAdRenderer f21213;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f21214;

    public interface MoPubNativeEventListener {
        void onClick(View view);

        void onImpression(View view);
    }

    public NativeAd(Context context, String str, String str2, String str3, BaseNativeAd baseNativeAd, MoPubAdRenderer moPubAdRenderer) {
        this.f21214 = context.getApplicationContext();
        this.f21205 = str3;
        this.f21212.add(str);
        this.f21212.addAll(baseNativeAd.m27049());
        this.f21210 = new HashSet();
        this.f21210.add(str2);
        this.f21210.addAll(baseNativeAd.m27048());
        this.f21211 = baseNativeAd;
        this.f21211.setNativeEventListener(new BaseNativeAd.NativeEventListener() {
            public void onAdClicked() {
                NativeAd.this.m27124((View) null);
            }

            public void onAdImpressed() {
                NativeAd.this.m27125((View) null);
            }
        });
        this.f21213 = moPubAdRenderer;
    }

    public void clear(View view) {
        if (!this.f21209) {
            this.f21211.clear(view);
        }
    }

    public View createAdView(Context context, ViewGroup viewGroup) {
        return this.f21213.createAdView(context, viewGroup);
    }

    public void destroy() {
        if (!this.f21209) {
            this.f21211.destroy();
            this.f21209 = true;
        }
    }

    public String getAdUnitId() {
        return this.f21205;
    }

    public BaseNativeAd getBaseNativeAd() {
        return this.f21211;
    }

    public MoPubAdRenderer getMoPubAdRenderer() {
        return this.f21213;
    }

    public boolean isDestroyed() {
        return this.f21209;
    }

    public void prepare(View view) {
        if (!this.f21209) {
            this.f21211.prepare(view);
        }
    }

    public void renderAdView(View view) {
        this.f21213.renderAdView(view, this.f21211);
    }

    public void setMoPubNativeEventListener(MoPubNativeEventListener moPubNativeEventListener) {
        this.f21206 = moPubNativeEventListener;
    }

    public String toString() {
        return "\nimpressionTrackers:" + this.f21212 + "\nclickTrackers:" + this.f21210 + "\nrecordedImpression:" + this.f21207 + "\nisClicked:" + this.f21208 + "\nisDestroyed:" + this.f21209 + StringUtils.LF;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27124(View view) {
        if (!this.f21208 && !this.f21209) {
            TrackingRequest.makeTrackingHttpRequest((Iterable<String>) this.f21210, this.f21214);
            if (this.f21206 != null) {
                this.f21206.onClick(view);
            }
            this.f21208 = true;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27125(View view) {
        if (!this.f21207 && !this.f21209) {
            TrackingRequest.makeTrackingHttpRequest((Iterable<String>) this.f21212, this.f21214);
            if (this.f21206 != null) {
                this.f21206.onImpression(view);
            }
            this.f21207 = true;
        }
    }
}
