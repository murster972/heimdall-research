package com.mopub.nativeads;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Views;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

class VisibilityTracker {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final VisibilityRunnable f21318;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Handler f21319;

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f21320;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public VisibilityTrackerListener f21321;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean f21322;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final VisibilityChecker f21323;
    @VisibleForTesting

    /* renamed from: 靐  reason: contains not printable characters */
    WeakReference<ViewTreeObserver> f21324;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final Map<View, TrackingInfo> f21325;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ArrayList<View> f21326;
    @VisibleForTesting

    /* renamed from: 龘  reason: contains not printable characters */
    final ViewTreeObserver.OnPreDrawListener f21327;

    static class TrackingInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        int f21329;

        /* renamed from: 麤  reason: contains not printable characters */
        View f21330;

        /* renamed from: 齉  reason: contains not printable characters */
        long f21331;

        /* renamed from: 龘  reason: contains not printable characters */
        int f21332;

        TrackingInfo() {
        }
    }

    static class VisibilityChecker {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Rect f21333 = new Rect();

        VisibilityChecker() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m27213(long j, int i) {
            return SystemClock.uptimeMillis() - j >= ((long) i);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m27214(View view, View view2, int i) {
            if (view2 == null || view2.getVisibility() != 0 || view.getParent() == null || !view2.getGlobalVisibleRect(this.f21333)) {
                return false;
            }
            long height = ((long) this.f21333.height()) * ((long) this.f21333.width());
            long height2 = ((long) view2.getHeight()) * ((long) view2.getWidth());
            return height2 > 0 && 100 * height >= ((long) i) * height2;
        }
    }

    class VisibilityRunnable implements Runnable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final ArrayList<View> f21334 = new ArrayList<>();

        /* renamed from: 齉  reason: contains not printable characters */
        private final ArrayList<View> f21335 = new ArrayList<>();

        VisibilityRunnable() {
        }

        public void run() {
            boolean unused = VisibilityTracker.this.f21322 = false;
            for (Map.Entry entry : VisibilityTracker.this.f21325.entrySet()) {
                View view = (View) entry.getKey();
                int i = ((TrackingInfo) entry.getValue()).f21332;
                int i2 = ((TrackingInfo) entry.getValue()).f21329;
                View view2 = ((TrackingInfo) entry.getValue()).f21330;
                if (VisibilityTracker.this.f21323.m27214(view2, view, i)) {
                    this.f21334.add(view);
                } else if (!VisibilityTracker.this.f21323.m27214(view2, view, i2)) {
                    this.f21335.add(view);
                }
            }
            if (VisibilityTracker.this.f21321 != null) {
                VisibilityTracker.this.f21321.onVisibilityChanged(this.f21334, this.f21335);
            }
            this.f21334.clear();
            this.f21335.clear();
        }
    }

    interface VisibilityTrackerListener {
        void onVisibilityChanged(List<View> list, List<View> list2);
    }

    public VisibilityTracker(Context context) {
        this(context, new WeakHashMap(10), new VisibilityChecker(), new Handler());
    }

    @VisibleForTesting
    VisibilityTracker(Context context, Map<View, TrackingInfo> map, VisibilityChecker visibilityChecker, Handler handler) {
        this.f21320 = 0;
        this.f21325 = map;
        this.f21323 = visibilityChecker;
        this.f21319 = handler;
        this.f21318 = new VisibilityRunnable();
        this.f21326 = new ArrayList<>(50);
        this.f21327 = new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                VisibilityTracker.this.m27206();
                return true;
            }
        };
        this.f21324 = new WeakReference<>((Object) null);
        m27203(context, (View) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27202(long j) {
        for (Map.Entry next : this.f21325.entrySet()) {
            if (((TrackingInfo) next.getValue()).f21331 < j) {
                this.f21326.add(next.getKey());
            }
        }
        Iterator<View> it2 = this.f21326.iterator();
        while (it2.hasNext()) {
            m27208(it2.next());
        }
        this.f21326.clear();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27203(Context context, View view) {
        View topmostView;
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f21324.get();
        if ((viewTreeObserver == null || !viewTreeObserver.isAlive()) && (topmostView = Views.getTopmostView(context, view)) != null) {
            ViewTreeObserver viewTreeObserver2 = topmostView.getViewTreeObserver();
            if (!viewTreeObserver2.isAlive()) {
                MoPubLog.w("Visibility Tracker was unable to track views because the root view tree observer was not alive");
                return;
            }
            this.f21324 = new WeakReference<>(viewTreeObserver2);
            viewTreeObserver2.addOnPreDrawListener(this.f21327);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27205() {
        m27207();
        ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f21324.get();
        if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
            viewTreeObserver.removeOnPreDrawListener(this.f21327);
        }
        this.f21324.clear();
        this.f21321 = null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m27206() {
        if (!this.f21322) {
            this.f21322 = true;
            this.f21319.postDelayed(this.f21318, 100);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27207() {
        this.f21325.clear();
        this.f21319.removeMessages(0);
        this.f21322 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27208(View view) {
        this.f21325.remove(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27209(View view, int i) {
        m27210(view, view, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27210(View view, View view2, int i) {
        m27211(view, view2, i, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27211(View view, View view2, int i, int i2) {
        m27203(view2.getContext(), view2);
        TrackingInfo trackingInfo = this.f21325.get(view2);
        if (trackingInfo == null) {
            trackingInfo = new TrackingInfo();
            this.f21325.put(view2, trackingInfo);
            m27206();
        }
        int min = Math.min(i2, i);
        trackingInfo.f21330 = view;
        trackingInfo.f21332 = i;
        trackingInfo.f21329 = min;
        trackingInfo.f21331 = this.f21320;
        this.f21320++;
        if (this.f21320 % 50 == 0) {
            m27202(this.f21320 - 50);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27212(VisibilityTrackerListener visibilityTrackerListener) {
        this.f21321 = visibilityTrackerListener;
    }
}
