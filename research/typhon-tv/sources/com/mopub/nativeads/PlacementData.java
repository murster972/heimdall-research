package com.mopub.nativeads;

import com.mopub.common.logging.MoPubLog;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.List;

class PlacementData {
    public static final int NOT_FOUND = -1;

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f21250 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f21251 = 0;

    /* renamed from: 连任  reason: contains not printable characters */
    private final NativeAd[] f21252 = new NativeAd[200];

    /* renamed from: 靐  reason: contains not printable characters */
    private final int[] f21253 = new int[200];

    /* renamed from: 麤  reason: contains not printable characters */
    private final int[] f21254 = new int[200];

    /* renamed from: 齉  reason: contains not printable characters */
    private final int[] f21255 = new int[200];

    /* renamed from: 龘  reason: contains not printable characters */
    private final int[] f21256 = new int[200];

    private PlacementData(int[] iArr) {
        this.f21250 = Math.min(iArr.length, 200);
        System.arraycopy(iArr, 0, this.f21253, 0, this.f21250);
        System.arraycopy(iArr, 0, this.f21256, 0, this.f21250);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m27151(int[] iArr, int i, int i2) {
        int r1 = m27153(iArr, 0, i, i2);
        if (r1 < 0) {
            return r1 ^ -1;
        }
        int i3 = iArr[r1];
        int i4 = r1;
        while (i4 < i && iArr[i4] == i3) {
            i4++;
        }
        int i5 = i4;
        return i4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m27152(int[] iArr, int i, int i2) {
        int r1 = m27153(iArr, 0, i, i2);
        if (r1 < 0) {
            return r1 ^ -1;
        }
        int i3 = iArr[r1];
        while (r1 >= 0 && iArr[r1] == i3) {
            r1--;
        }
        return r1 + 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m27153(int[] iArr, int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2 - 1;
        while (i4 <= i5) {
            int i6 = (i4 + i5) >>> 1;
            int i7 = iArr[i6];
            if (i7 < i3) {
                i4 = i6 + 1;
            } else if (i7 <= i3) {
                return i6;
            } else {
                i5 = i6 - 1;
            }
        }
        return i4 ^ -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static PlacementData m27154() {
        return new PlacementData(new int[0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static PlacementData m27155(MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
        List<Integer> r1 = moPubClientPositioning.m27098();
        int r2 = moPubClientPositioning.m27097();
        int size = r2 == Integer.MAX_VALUE ? r1.size() : 200;
        int[] iArr = new int[size];
        int i = 0;
        int i2 = 0;
        for (Integer intValue : r1) {
            i2 = intValue.intValue() - i;
            iArr[i] = i2;
            i++;
        }
        for (int i3 = i; i3 < size; i3++) {
            i2 = (i2 + r2) - 1;
            iArr[i3] = i2;
        }
        return new PlacementData(iArr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m27156(int i) {
        return i + m27151(this.f21255, this.f21251, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m27157(int i) {
        if (i == 0) {
            return 0;
        }
        int r0 = m27161(i - 1);
        if (r0 != -1) {
            return r0 + 1;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m27158(int i) {
        if (i == 0) {
            return 0;
        }
        return m27156(i - 1) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m27159(int i) {
        for (int r0 = m27152(this.f21256, this.f21250, i); r0 < this.f21250; r0++) {
            int[] iArr = this.f21256;
            iArr[r0] = iArr[r0] + 1;
            int[] iArr2 = this.f21253;
            iArr2[r0] = iArr2[r0] + 1;
        }
        for (int r02 = m27152(this.f21255, this.f21251, i); r02 < this.f21251; r02++) {
            int[] iArr3 = this.f21255;
            iArr3[r02] = iArr3[r02] + 1;
            int[] iArr4 = this.f21254;
            iArr4[r02] = iArr4[r02] + 1;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m27160(int i) {
        for (int r0 = m27151(this.f21256, this.f21250, i); r0 < this.f21250; r0++) {
            int[] iArr = this.f21256;
            iArr[r0] = iArr[r0] - 1;
            int[] iArr2 = this.f21253;
            iArr2[r0] = iArr2[r0] - 1;
        }
        for (int r02 = m27151(this.f21255, this.f21251, i); r02 < this.f21251; r02++) {
            int[] iArr3 = this.f21255;
            iArr3[r02] = iArr3[r02] - 1;
            int[] iArr4 = this.f21254;
            iArr4[r02] = iArr4[r02] - 1;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m27161(int i) {
        int r0 = m27153(this.f21254, 0, this.f21251, i);
        if (r0 < 0) {
            return i - (r0 ^ -1);
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m27162(int i) {
        int r0 = m27151(this.f21253, this.f21250, i);
        if (r0 == this.f21250) {
            return -1;
        }
        return this.f21253[r0];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27163(int i, int i2) {
        m27160(i);
        m27159(i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int[] m27164() {
        int[] iArr = new int[this.f21251];
        System.arraycopy(this.f21254, 0, iArr, 0, this.f21251);
        return iArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public NativeAd m27165(int i) {
        int r0 = m27153(this.f21254, 0, this.f21251, i);
        if (r0 < 0) {
            return null;
        }
        return this.f21252[r0];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m27166() {
        if (this.f21251 != 0) {
            m27168(0, this.f21254[this.f21251 - 1] + 1);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m27167(int i) {
        return m27153(this.f21254, 0, this.f21251, i) >= 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m27168(int i, int i2) {
        int[] iArr = new int[this.f21251];
        int[] iArr2 = new int[this.f21251];
        int i3 = 0;
        for (int i4 = 0; i4 < this.f21251; i4++) {
            int i5 = this.f21255[i4];
            int i6 = this.f21254[i4];
            if (i <= i6 && i6 < i2) {
                iArr[i3] = i5;
                iArr2[i3] = i6 - i3;
                this.f21252[i4].destroy();
                this.f21252[i4] = null;
                i3++;
            } else if (i3 > 0) {
                int i7 = i4 - i3;
                this.f21255[i7] = i5;
                this.f21254[i7] = i6 - i3;
                this.f21252[i7] = this.f21252[i4];
            }
        }
        if (i3 == 0) {
            return 0;
        }
        int r4 = m27152(this.f21253, this.f21250, iArr2[0]);
        for (int i8 = this.f21250 - 1; i8 >= r4; i8--) {
            this.f21256[i8 + i3] = this.f21256[i8];
            this.f21253[i8 + i3] = this.f21253[i8] - i3;
        }
        for (int i9 = 0; i9 < i3; i9++) {
            this.f21256[r4 + i9] = iArr[i9];
            this.f21253[r4 + i9] = iArr2[i9];
        }
        this.f21250 += i3;
        this.f21251 -= i3;
        return i3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27169(int i, NativeAd nativeAd) {
        int r0 = m27152(this.f21253, this.f21250, i);
        if (r0 == this.f21250 || this.f21253[r0] != i) {
            MoPubLog.w("Attempted to insert an ad at an invalid position");
            return;
        }
        int i2 = this.f21256[r0];
        int r4 = m27151(this.f21255, this.f21251, i2);
        if (r4 < this.f21251) {
            int i3 = this.f21251 - r4;
            System.arraycopy(this.f21255, r4, this.f21255, r4 + 1, i3);
            System.arraycopy(this.f21254, r4, this.f21254, r4 + 1, i3);
            System.arraycopy(this.f21252, r4, this.f21252, r4 + 1, i3);
        }
        this.f21255[r4] = i2;
        this.f21254[r4] = i;
        this.f21252[r4] = nativeAd;
        this.f21251++;
        int i4 = (this.f21250 - r0) - 1;
        System.arraycopy(this.f21253, r0 + 1, this.f21253, r0, i4);
        System.arraycopy(this.f21256, r0 + 1, this.f21256, r0, i4);
        this.f21250--;
        for (int i5 = r0; i5 < this.f21250; i5++) {
            int[] iArr = this.f21253;
            iArr[i5] = iArr[i5] + 1;
        }
        for (int i6 = r4 + 1; i6 < this.f21251; i6++) {
            int[] iArr2 = this.f21254;
            iArr2[i6] = iArr2[i6] + 1;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m27170(int i) {
        return m27153(this.f21253, 0, this.f21250, i) >= 0;
    }
}
