package com.mopub.nativeads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.Pinkamena;
import com.mobvista.msdk.out.Campaign;
import com.mobvista.msdk.out.Frame;
import com.mobvista.msdk.out.MobVistaSDKFactory;
import com.mobvista.msdk.out.MvNativeHandler;
import com.mobvista.msdk.out.NativeListener;
import com.mobvista.msdk.system.MobVistaSDKImpl;
import com.mopub.nativeads.CustomEventNative;
import com.mopub.nativeads.NativeImageHelper;
import com.typhoon.tv.BuildConfig;
import com.typhoon.tv.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MobvistaCustomEventNative extends CustomEventNative {

    public static class MobvistaStaticNativeAd extends StaticNativeAd {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public CustomEventNative.CustomEventNativeListener f21200;

        /* renamed from: 麤  reason: contains not printable characters */
        private Campaign f21201;

        /* renamed from: 齉  reason: contains not printable characters */
        private MvNativeHandler f21202;

        /* renamed from: 龘  reason: contains not printable characters */
        private Context f21203;

        /* renamed from: 龘  reason: contains not printable characters */
        private List<View> m27123(View view) {
            ArrayList arrayList = new ArrayList();
            if (view != null) {
                try {
                    if (view instanceof ViewGroup) {
                        ViewGroup viewGroup = (ViewGroup) view;
                        for (int i = 0; i < viewGroup.getChildCount(); i++) {
                            if (viewGroup.getChildAt(i) instanceof ViewGroup) {
                                arrayList.addAll(m27123(viewGroup.getChildAt(i)));
                            } else {
                                arrayList.add(viewGroup.getChildAt(i));
                            }
                        }
                    } else {
                        arrayList.add(view);
                    }
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                } catch (Throwable th) {
                    Logger.m6281(th, new boolean[0]);
                }
            }
            return arrayList;
        }

        public void clear(View view) {
            new ImpressionTracker(this.f21203).removeView(view);
        }

        public void destroy() {
            super.destroy();
            if (this.f21202 != null) {
                try {
                    this.f21202.release();
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
        }

        public void loadAd(Context context, List<Campaign> list, MvNativeHandler mvNativeHandler, CustomEventNative.CustomEventNativeListener customEventNativeListener) {
            this.f21203 = context;
            this.f21200 = customEventNativeListener;
            this.f21202 = mvNativeHandler;
            ArrayList arrayList = new ArrayList();
            if (list != null && list.size() > 0) {
                this.f21201 = list.get(0);
            }
            if (this.f21201 == null) {
                customEventNativeListener.onNativeAdFailed(NativeErrorCode.EMPTY_AD_RESPONSE);
                return;
            }
            try {
                setMainImageUrl(this.f21201.getImageUrl());
                arrayList.add(this.f21201.getImageUrl());
                setIconImageUrl(this.f21201.getIconUrl());
                arrayList.add(this.f21201.getIconUrl());
                setCallToAction(this.f21201.getAdCall());
                setTitle(this.f21201.getAppName());
                setText(this.f21201.getAppDesc());
                if (this.f21201.getAppDesc() != null) {
                    String appDesc = this.f21201.getAppDesc();
                    if (this.f21201.getRating() >= 4.5d) {
                        appDesc = "★ " + String.valueOf(this.f21201.getRating()) + " - " + appDesc;
                    }
                    setStarRating(Double.valueOf(this.f21201.getRating()));
                    setText(appDesc);
                }
                NativeImageHelper.preCacheImages(this.f21203, arrayList, new NativeImageHelper.ImageListener() {
                    public void onImagesCached() {
                        MobvistaStaticNativeAd.this.f21200.onNativeAdLoaded(MobvistaStaticNativeAd.this);
                    }

                    public void onImagesFailedToCache(NativeErrorCode nativeErrorCode) {
                        MobvistaStaticNativeAd.this.f21200.onNativeAdFailed(nativeErrorCode);
                    }
                });
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
                this.f21200.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
            }
        }

        public void prepare(View view) {
            try {
                List<View> r1 = m27123(view);
                if (this.f21202 != null && r1 != null && r1.size() != 0) {
                    this.f21202.registerView(view, r1, this.f21201);
                    m27050();
                }
            } catch (Throwable th) {
                Logger.m6281(th, new boolean[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27121(Context context, CustomEventNative.CustomEventNativeListener customEventNativeListener, Map<String, Object> map, Map<String, String> map2) {
        String str = map2.get("appKey");
        String str2 = map2.get("appId");
        String str3 = map2.get("unitId");
        if (str == null || str.isEmpty() || str2 == null || str2.isEmpty() || str3 == null || str3.isEmpty()) {
            customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        try {
            MobVistaSDKImpl mobVistaSDK = MobVistaSDKFactory.getMobVistaSDK();
            Map mVConfigurationMap = mobVistaSDK.getMVConfigurationMap(str2, str);
            mVConfigurationMap.put("applicationID", BuildConfig.APPLICATION_ID);
            if (context instanceof Activity) {
                mobVistaSDK.init(mVConfigurationMap, ((Activity) context).getApplication());
            } else if (context instanceof Application) {
                mobVistaSDK.init(mVConfigurationMap, context);
            } else {
                customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
                return;
            }
            Map nativeProperties = MvNativeHandler.getNativeProperties(str3);
            nativeProperties.put("ad_num", 1);
            final MobvistaStaticNativeAd mobvistaStaticNativeAd = new MobvistaStaticNativeAd();
            final MvNativeHandler mvNativeHandler = new MvNativeHandler(nativeProperties, context);
            final CustomEventNative.CustomEventNativeListener customEventNativeListener2 = customEventNativeListener;
            final Context context2 = context;
            mvNativeHandler.setAdListener(new NativeListener.NativeAdListener() {
                public void onAdClick(Campaign campaign) {
                    mobvistaStaticNativeAd.m27046();
                }

                public void onAdFramesLoaded(List<Frame> list) {
                }

                public void onAdLoadError(String str) {
                    customEventNativeListener2.onNativeAdFailed(NativeErrorCode.UNSPECIFIED);
                }

                public void onAdLoaded(List<Campaign> list, int i) {
                    if (list == null || list.isEmpty()) {
                        customEventNativeListener2.onNativeAdFailed(NativeErrorCode.NETWORK_NO_FILL);
                        return;
                    }
                    MobvistaStaticNativeAd mobvistaStaticNativeAd = mobvistaStaticNativeAd;
                    Context context = context2;
                    MvNativeHandler mvNativeHandler = mvNativeHandler;
                    CustomEventNative.CustomEventNativeListener customEventNativeListener = customEventNativeListener2;
                    Pinkamena.DianePie();
                }
            });
            Pinkamena.DianePieNull();
        } catch (Throwable th) {
            Logger.m6281(th, new boolean[0]);
        }
    }
}
