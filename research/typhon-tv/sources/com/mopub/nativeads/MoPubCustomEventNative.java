package com.mopub.nativeads;

import android.content.Context;
import android.view.View;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mopub.common.DataKeys;
import com.mopub.common.VisibleForTesting;
import com.mopub.nativeads.CustomEventNative;
import com.mopub.nativeads.NativeImageHelper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import net.pubnative.library.request.PubnativeAsset;
import org.json.JSONArray;
import org.json.JSONObject;

public class MoPubCustomEventNative extends CustomEventNative {

    static class MoPubStaticNativeAd extends StaticNativeAd {

        /* renamed from: 连任  reason: contains not printable characters */
        private final NativeClickHandler f21137;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final CustomEventNative.CustomEventNativeListener f21138;

        /* renamed from: 麤  reason: contains not printable characters */
        private final ImpressionTracker f21139;

        /* renamed from: 齉  reason: contains not printable characters */
        private final JSONObject f21140;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Context f21141;

        enum Parameter {
            IMPRESSION_TRACKER("imptracker", true),
            CLICK_TRACKER("clktracker", true),
            TITLE(PubnativeAsset.TITLE, false),
            TEXT(MimeTypes.BASE_TYPE_TEXT, false),
            MAIN_IMAGE("mainimage", false),
            ICON_IMAGE("iconimage", false),
            CLICK_DESTINATION("clk", false),
            FALLBACK("fallback", false),
            CALL_TO_ACTION("ctatext", false),
            STAR_RATING("starrating", false);
            
            @VisibleForTesting

            /* renamed from: 龘  reason: contains not printable characters */
            static final Set<String> f21144 = null;
            final String name;
            final boolean required;

            static {
                int i;
                f21144 = new HashSet();
                for (Parameter parameter : values()) {
                    if (parameter.required) {
                        f21144.add(parameter.name);
                    }
                }
            }

            private Parameter(String str, boolean z) {
                this.name = str;
                this.required = z;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            static Parameter m27084(String str) {
                for (Parameter parameter : values()) {
                    if (parameter.name.equals(str)) {
                        return parameter;
                    }
                }
                return null;
            }
        }

        MoPubStaticNativeAd(Context context, JSONObject jSONObject, ImpressionTracker impressionTracker, NativeClickHandler nativeClickHandler, CustomEventNative.CustomEventNativeListener customEventNativeListener) {
            this.f21140 = jSONObject;
            this.f21141 = context.getApplicationContext();
            this.f21139 = impressionTracker;
            this.f21137 = nativeClickHandler;
            this.f21138 = customEventNativeListener;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m27076(Object obj) {
            if (obj instanceof JSONArray) {
                m27047(obj);
            } else {
                addClickTracker((String) obj);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:18:? A[Catch:{ ClassCastException -> 0x0012 }, RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0017  */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void m27078(com.mopub.nativeads.MoPubCustomEventNative.MoPubStaticNativeAd.Parameter r4, java.lang.Object r5) throws java.lang.ClassCastException {
            /*
                r3 = this;
                int[] r1 = com.mopub.nativeads.MoPubCustomEventNative.AnonymousClass1.f21136     // Catch:{ ClassCastException -> 0x0012 }
                int r2 = r4.ordinal()     // Catch:{ ClassCastException -> 0x0012 }
                r1 = r1[r2]     // Catch:{ ClassCastException -> 0x0012 }
                switch(r1) {
                    case 1: goto L_0x000c;
                    case 2: goto L_0x0018;
                    case 3: goto L_0x001e;
                    case 4: goto L_0x0022;
                    case 5: goto L_0x0028;
                    case 6: goto L_0x002c;
                    case 7: goto L_0x0032;
                    case 8: goto L_0x0038;
                    case 9: goto L_0x003e;
                    default: goto L_0x000b;
                }     // Catch:{ ClassCastException -> 0x0012 }
            L_0x000b:
                return
            L_0x000c:
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ ClassCastException -> 0x0012 }
                r3.setMainImageUrl(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x0012:
                r0 = move-exception
                boolean r1 = r4.required
                if (r1 == 0) goto L_0x000b
                throw r0
            L_0x0018:
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ ClassCastException -> 0x0012 }
                r3.setIconImageUrl(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x001e:
                r3.m27051(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x0022:
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ ClassCastException -> 0x0012 }
                r3.setClickDestinationUrl(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x0028:
                r3.m27076(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x002c:
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ ClassCastException -> 0x0012 }
                r3.setCallToAction(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x0032:
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ ClassCastException -> 0x0012 }
                r3.setTitle(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x0038:
                java.lang.String r5 = (java.lang.String) r5     // Catch:{ ClassCastException -> 0x0012 }
                r3.setText(r5)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            L_0x003e:
                java.lang.Double r1 = com.mopub.common.util.Numbers.parseDouble(r5)     // Catch:{ ClassCastException -> 0x0012 }
                r3.setStarRating(r1)     // Catch:{ ClassCastException -> 0x0012 }
                goto L_0x000b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mopub.nativeads.MoPubCustomEventNative.MoPubStaticNativeAd.m27078(com.mopub.nativeads.MoPubCustomEventNative$MoPubStaticNativeAd$Parameter, java.lang.Object):void");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m27079(String str) {
            return str != null && str.toLowerCase(Locale.US).endsWith("image");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m27080(JSONObject jSONObject) {
            HashSet hashSet = new HashSet();
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                hashSet.add(keys.next());
            }
            return hashSet.containsAll(Parameter.f21144);
        }

        public void clear(View view) {
            this.f21139.removeView(view);
            this.f21137.clearOnClickListener(view);
        }

        public void destroy() {
            this.f21139.destroy();
        }

        public void handleClick(View view) {
            m27046();
            this.f21137.openClickDestinationUrl(getClickDestinationUrl(), view);
        }

        public void prepare(View view) {
            this.f21139.addView(view, this);
            this.f21137.setOnClickListener(view, this);
        }

        public void recordImpression(View view) {
            m27050();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʻ  reason: contains not printable characters */
        public List<String> m27081() {
            ArrayList arrayList = new ArrayList(getExtras().size());
            for (Map.Entry next : getExtras().entrySet()) {
                if (m27079((String) next.getKey()) && (next.getValue() instanceof String)) {
                    arrayList.add((String) next.getValue());
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public List<String> m27082() {
            ArrayList arrayList = new ArrayList();
            if (getMainImageUrl() != null) {
                arrayList.add(getMainImageUrl());
            }
            if (getIconImageUrl() != null) {
                arrayList.add(getIconImageUrl());
            }
            arrayList.addAll(m27081());
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public void m27083() throws IllegalArgumentException {
            if (!m27080(this.f21140)) {
                throw new IllegalArgumentException("JSONObject did not contain required keys.");
            }
            Iterator keys = this.f21140.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                Parameter r3 = Parameter.m27084(str);
                if (r3 != null) {
                    try {
                        m27078(r3, this.f21140.opt(str));
                    } catch (ClassCastException e) {
                        throw new IllegalArgumentException("JSONObject key (" + str + ") contained unexpected value.");
                    }
                } else {
                    addExtra(str, this.f21140.opt(str));
                }
            }
            setPrivacyInformationIconClickThroughUrl("=");
            NativeImageHelper.preCacheImages(this.f21141, m27082(), new NativeImageHelper.ImageListener() {
                public void onImagesCached() {
                    MoPubStaticNativeAd.this.f21138.onNativeAdLoaded(MoPubStaticNativeAd.this);
                }

                public void onImagesFailedToCache(NativeErrorCode nativeErrorCode) {
                    MoPubStaticNativeAd.this.f21138.onNativeAdFailed(nativeErrorCode);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27075(Context context, CustomEventNative.CustomEventNativeListener customEventNativeListener, Map<String, Object> map, Map<String, String> map2) {
        Object obj = map.get(DataKeys.JSON_BODY_KEY);
        if (!(obj instanceof JSONObject)) {
            customEventNativeListener.onNativeAdFailed(NativeErrorCode.INVALID_RESPONSE);
            return;
        }
        try {
            new MoPubStaticNativeAd(context, (JSONObject) obj, new ImpressionTracker(context), new NativeClickHandler(context), customEventNativeListener).m27083();
        } catch (IllegalArgumentException e) {
            customEventNativeListener.onNativeAdFailed(NativeErrorCode.UNSPECIFIED);
        }
    }
}
