package com.mopub.nativeads;

import android.view.View;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.util.HashMap;
import java.util.Map;

public abstract class StaticNativeAd extends BaseNativeAd implements ClickInterface, ImpressionInterface {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f21281;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f21282;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Double f21283;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f21284 = 1000;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f21285;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f21286;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f21287;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f21288;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f21289;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f21290;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f21291;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, Object> f21292 = new HashMap();

    public final void addExtra(String str, Object obj) {
        if (Preconditions.NoThrow.checkNotNull(str, "addExtra key is not allowed to be null")) {
            this.f21292.put(str, obj);
        }
    }

    public void clear(View view) {
    }

    public void destroy() {
    }

    public final String getCallToAction() {
        return this.f21288;
    }

    public final String getClickDestinationUrl() {
        return this.f21290;
    }

    public final Object getExtra(String str) {
        if (!Preconditions.NoThrow.checkNotNull(str, "getExtra key is not allowed to be null")) {
            return null;
        }
        return this.f21292.get(str);
    }

    public final Map<String, Object> getExtras() {
        return new HashMap(this.f21292);
    }

    public final String getIconImageUrl() {
        return this.f21291;
    }

    public final int getImpressionMinPercentageViewed() {
        return 50;
    }

    public final int getImpressionMinTimeViewed() {
        return this.f21284;
    }

    public final String getMainImageUrl() {
        return this.f21289;
    }

    public final String getPrivacyInformationIconClickThroughUrl() {
        return this.f21285;
    }

    public String getPrivacyInformationIconImageUrl() {
        return this.f21286;
    }

    public final Double getStarRating() {
        return this.f21283;
    }

    public final String getText() {
        return this.f21282;
    }

    public final String getTitle() {
        return this.f21281;
    }

    public void handleClick(View view) {
    }

    public final boolean isImpressionRecorded() {
        return this.f21287;
    }

    public void prepare(View view) {
    }

    public void recordImpression(View view) {
    }

    public final void setCallToAction(String str) {
        this.f21288 = str;
    }

    public final void setClickDestinationUrl(String str) {
        this.f21290 = str;
    }

    public final void setIconImageUrl(String str) {
        this.f21291 = str;
    }

    public final void setImpressionMinTimeViewed(int i) {
        if (i >= 0) {
            this.f21284 = i;
        }
    }

    public final void setImpressionRecorded() {
        this.f21287 = true;
    }

    public final void setMainImageUrl(String str) {
        this.f21289 = str;
    }

    public final void setPrivacyInformationIconClickThroughUrl(String str) {
        this.f21285 = str;
    }

    public final void setPrivacyInformationIconImageUrl(String str) {
        this.f21286 = str;
    }

    public final void setStarRating(Double d) {
        if (d == null) {
            this.f21283 = null;
        } else if (d.doubleValue() < 0.0d || d.doubleValue() > 5.0d) {
            MoPubLog.d("Ignoring attempt to set invalid star rating (" + d + "). Must be between " + 0.0d + " and " + 5.0d + ".");
        } else {
            this.f21283 = d;
        }
    }

    public final void setText(String str) {
        this.f21282 = str;
    }

    public final void setTitle(String str) {
        this.f21281 = str;
    }
}
