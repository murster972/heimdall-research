package com.mopub.nativeads;

import android.content.Context;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.TyphoonApp;
import net.pubnative.library.request.PubnativeRequest;

class PositioningUrlGenerator extends BaseUrlGenerator {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f21257;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f21258;

    public PositioningUrlGenerator(Context context) {
        this.f21258 = context;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m27176(String str) {
        m26450("nsv", str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27177(String str) {
        m26450("id", str);
    }

    public String generateUrlString(String str) {
        m26454(str, TyphoonApp.POSITIONING_HANDLER);
        m27177(this.f21257);
        m26448(PubnativeRequest.LEGACY_ZONE_ID);
        ClientMetadata instance = ClientMetadata.getInstance(this.f21258);
        m27176(instance.getSdkVersion());
        m26455(instance.getDeviceManufacturer(), instance.getDeviceModel(), instance.getDeviceProduct());
        m26447(instance.getAppVersion());
        m26449();
        return m26452();
    }

    public PositioningUrlGenerator withAdUnitId(String str) {
        this.f21257 = str;
        return this;
    }
}
