package com.mopub.nativeads;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.Pinkamena;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.NativeAdSource;
import com.mopub.nativeads.PositioningSource;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.WeakHashMap;

public class MoPubStreamAdPlacer {
    public static final int CONTENT_VIEW_TYPE = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final MoPubNativeAdLoadedListener f21173 = new MoPubNativeAdLoadedListener() {
        public void onAdLoaded(int i) {
        }

        public void onAdRemoved(int i) {
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final NativeAdSource f21174;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final HashMap<NativeAd, WeakReference<View>> f21175;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final WeakHashMap<View, NativeAd> f21176;

    /* renamed from: ʾ  reason: contains not printable characters */
    private PlacementData f21177;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f21178;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f21179;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f21180;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f21181;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f21182;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f21183;

    /* renamed from: ٴ  reason: contains not printable characters */
    private PlacementData f21184;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f21185;

    /* renamed from: 连任  reason: contains not printable characters */
    private final PositioningSource f21186;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Activity f21187;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Runnable f21188;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Handler f21189;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private MoPubNativeAdLoadedListener f21190;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f21191;

    public MoPubStreamAdPlacer(Activity activity) {
        this(activity, MoPubNativeAdPositioning.serverPositioning());
    }

    public MoPubStreamAdPlacer(Activity activity, MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
        this(activity, new NativeAdSource(), new ClientPositioningSource(moPubClientPositioning));
    }

    public MoPubStreamAdPlacer(Activity activity, MoPubNativeAdPositioning.MoPubServerPositioning moPubServerPositioning) {
        this(activity, new NativeAdSource(), new ServerPositioningSource(activity));
    }

    @VisibleForTesting
    MoPubStreamAdPlacer(Activity activity, NativeAdSource nativeAdSource, PositioningSource positioningSource) {
        this.f21190 = f21173;
        Preconditions.checkNotNull(activity, "activity is not allowed to be null");
        Preconditions.checkNotNull(nativeAdSource, "adSource is not allowed to be null");
        Preconditions.checkNotNull(positioningSource, "positioningSource is not allowed to be null");
        this.f21187 = activity;
        this.f21186 = positioningSource;
        this.f21174 = nativeAdSource;
        this.f21177 = PlacementData.m27154();
        this.f21176 = new WeakHashMap<>();
        this.f21175 = new HashMap<>();
        this.f21189 = new Handler();
        this.f21188 = new Runnable() {
            public void run() {
                if (MoPubStreamAdPlacer.this.f21182) {
                    MoPubStreamAdPlacer.this.m27111();
                    boolean unused = MoPubStreamAdPlacer.this.f21182 = false;
                }
            }
        };
        this.f21191 = 0;
        this.f21180 = 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m27109() {
        if (!this.f21182) {
            this.f21182 = true;
            this.f21189.post(this.f21188);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m27111() {
        if (m27116(this.f21191, this.f21180)) {
            m27116(this.f21180, this.f21180 + 6);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27112(View view) {
        NativeAd nativeAd;
        if (view != null && (nativeAd = this.f21176.get(view)) != null) {
            nativeAd.clear(view);
            this.f21176.remove(view);
            this.f21175.remove(nativeAd);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27113(NativeAd nativeAd, View view) {
        this.f21175.put(nativeAd, new WeakReference(view));
        this.f21176.put(view, nativeAd);
        nativeAd.prepare(view);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27114(PlacementData placementData) {
        removeAdsInRange(0, this.f21181);
        this.f21177 = placementData;
        m27111();
        this.f21179 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m27115(int i) {
        NativeAd r0 = this.f21174.m27136();
        if (r0 == null) {
            return false;
        }
        this.f21177.m27169(i, r0);
        this.f21181++;
        this.f21190.onAdLoaded(i);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m27116(int i, int i2) {
        int i3 = i;
        int i4 = i2 - 1;
        while (i3 <= i4 && i3 != -1 && i3 < this.f21181) {
            if (this.f21177.m27170(i3)) {
                if (!m27115(i3)) {
                    return false;
                }
                i4++;
            }
            i3 = this.f21177.m27162(i3);
        }
        return true;
    }

    public void bindAdView(NativeAd nativeAd, View view) {
        WeakReference weakReference = this.f21175.get(nativeAd);
        View view2 = null;
        if (weakReference != null) {
            view2 = (View) weakReference.get();
        }
        if (!view.equals(view2)) {
            m27112(view2);
            m27112(view);
            m27113(nativeAd, view);
            nativeAd.renderAdView(view);
        }
    }

    public void clearAds() {
        removeAdsInRange(0, this.f21181);
        this.f21174.m27134();
    }

    public void destroy() {
        this.f21189.removeMessages(0);
        this.f21174.m27134();
        this.f21177.m27166();
    }

    public Object getAdData(int i) {
        return this.f21177.m27165(i);
    }

    public MoPubAdRenderer getAdRendererForViewType(int i) {
        return this.f21174.getAdRendererForViewType(i);
    }

    public View getAdView(int i, View view, ViewGroup viewGroup) {
        NativeAd r0 = this.f21177.m27165(i);
        if (r0 == null) {
            return null;
        }
        View createAdView = view != null ? view : r0.createAdView(this.f21187, viewGroup);
        bindAdView(r0, createAdView);
        return createAdView;
    }

    public int getAdViewType(int i) {
        NativeAd r0 = this.f21177.m27165(i);
        if (r0 == null) {
            return 0;
        }
        return this.f21174.getViewTypeForAd(r0);
    }

    public int getAdViewTypeCount() {
        return this.f21174.m27137();
    }

    public int getAdjustedCount(int i) {
        return this.f21177.m27158(i);
    }

    public int getAdjustedPosition(int i) {
        return this.f21177.m27156(i);
    }

    public int getOriginalCount(int i) {
        return this.f21177.m27157(i);
    }

    public int getOriginalPosition(int i) {
        return this.f21177.m27161(i);
    }

    public void insertItem(int i) {
        this.f21177.m27159(i);
    }

    public boolean isAd(int i) {
        return this.f21177.m27167(i);
    }

    public void loadAds(String str) {
        Pinkamena.DianePie();
    }

    public void loadAds(String str, RequestParameters requestParameters) {
        if (Preconditions.NoThrow.checkNotNull(str, "Cannot load ads with a null ad unit ID")) {
            if (this.f21174.m27137() == 0) {
                MoPubLog.w("You must register at least 1 ad renderer by calling registerAdRenderer before loading ads");
                return;
            }
            this.f21178 = str;
            this.f21179 = false;
            this.f21183 = false;
            this.f21185 = false;
            this.f21186.loadPositions(str, new PositioningSource.PositioningListener() {
                public void onFailed() {
                    MoPubLog.d("Unable to show ads because ad positions could not be loaded from the MoPub ad server.");
                }

                public void onLoad(MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
                    MoPubStreamAdPlacer.this.m27120(moPubClientPositioning);
                }
            });
            this.f21174.m27140((NativeAdSource.AdSourceListener) new NativeAdSource.AdSourceListener() {
                public void onAdsAvailable() {
                    MoPubStreamAdPlacer.this.m27119();
                }
            });
            this.f21174.m27138(this.f21187, str, requestParameters);
        }
    }

    public void moveItem(int i, int i2) {
        this.f21177.m27163(i, i2);
    }

    public void placeAdsInRange(int i, int i2) {
        this.f21191 = i;
        this.f21180 = Math.min(i2, i + 100);
        m27109();
    }

    public void registerAdRenderer(MoPubAdRenderer moPubAdRenderer) {
        if (Preconditions.NoThrow.checkNotNull(moPubAdRenderer, "Cannot register a null adRenderer")) {
            this.f21174.m27139(moPubAdRenderer);
        }
    }

    public int removeAdsInRange(int i, int i2) {
        int[] r5 = this.f21177.m27164();
        int r1 = this.f21177.m27156(i);
        int r0 = this.f21177.m27156(i2);
        ArrayList arrayList = new ArrayList();
        for (int length = r5.length - 1; length >= 0; length--) {
            int i3 = r5[length];
            if (i3 >= r1 && i3 < r0) {
                arrayList.add(Integer.valueOf(i3));
                if (i3 < this.f21191) {
                    this.f21191--;
                }
                this.f21181--;
            }
        }
        int r2 = this.f21177.m27168(r1, r0);
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            this.f21190.onAdRemoved(((Integer) it2.next()).intValue());
        }
        return r2;
    }

    public void removeItem(int i) {
        this.f21177.m27160(i);
    }

    public void setAdLoadedListener(MoPubNativeAdLoadedListener moPubNativeAdLoadedListener) {
        if (moPubNativeAdLoadedListener == null) {
            moPubNativeAdLoadedListener = f21173;
        }
        this.f21190 = moPubNativeAdLoadedListener;
    }

    public void setItemCount(int i) {
        this.f21181 = this.f21177.m27158(i);
        if (this.f21179) {
            m27109();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27119() {
        if (this.f21179) {
            m27109();
            return;
        }
        if (this.f21183) {
            m27114(this.f21184);
        }
        this.f21185 = true;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27120(MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
        PlacementData r0 = PlacementData.m27155(moPubClientPositioning);
        if (this.f21185) {
            m27114(r0);
        } else {
            this.f21184 = r0;
        }
        this.f21183 = true;
    }
}
