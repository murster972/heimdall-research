package com.mopub.nativeads;

import android.view.View;
import com.mopub.common.Preconditions;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

public abstract class BaseNativeAd {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Set<String> f21088 = new HashSet();

    /* renamed from: 齉  reason: contains not printable characters */
    private NativeEventListener f21089;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Set<String> f21090 = new HashSet();

    public interface NativeEventListener {
        void onAdClicked();

        void onAdImpressed();
    }

    protected BaseNativeAd() {
    }

    public final void addClickTracker(String str) {
        if (Preconditions.NoThrow.checkNotNull(str, "clickTracker url is not allowed to be null")) {
            this.f21088.add(str);
        }
    }

    public final void addImpressionTracker(String str) {
        if (Preconditions.NoThrow.checkNotNull(str, "impressionTracker url is not allowed to be null")) {
            this.f21090.add(str);
        }
    }

    public abstract void clear(View view);

    public abstract void destroy();

    public abstract void prepare(View view);

    public void setNativeEventListener(NativeEventListener nativeEventListener) {
        this.f21089 = nativeEventListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m27046() {
        if (this.f21089 != null) {
            this.f21089.onAdClicked();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m27047(Object obj) throws ClassCastException {
        if (!(obj instanceof JSONArray)) {
            throw new ClassCastException("Expected click trackers of type JSONArray.");
        }
        JSONArray jSONArray = (JSONArray) obj;
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                addClickTracker(jSONArray.getString(i));
            } catch (JSONException e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public Set<String> m27048() {
        return new HashSet(this.f21088);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public Set<String> m27049() {
        return new HashSet(this.f21090);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m27050() {
        if (this.f21089 != null) {
            this.f21089.onAdImpressed();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m27051(Object obj) throws ClassCastException {
        if (!(obj instanceof JSONArray)) {
            throw new ClassCastException("Expected impression trackers of type JSONArray.");
        }
        JSONArray jSONArray = (JSONArray) obj;
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                addImpressionTracker(jSONArray.getString(i));
            } catch (JSONException e) {
            }
        }
    }
}
