package com.mopub.nativeads;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.mopub.common.logging.MoPubLog;

class StaticNativeViewHolder {

    /* renamed from: ʻ  reason: contains not printable characters */
    ImageView f21293;

    /* renamed from: ʼ  reason: contains not printable characters */
    ImageView f21294;

    /* renamed from: 连任  reason: contains not printable characters */
    ImageView f21295;

    /* renamed from: 靐  reason: contains not printable characters */
    TextView f21296;

    /* renamed from: 麤  reason: contains not printable characters */
    TextView f21297;

    /* renamed from: 齉  reason: contains not printable characters */
    TextView f21298;

    /* renamed from: 龘  reason: contains not printable characters */
    View f21299;

    private StaticNativeViewHolder() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static StaticNativeViewHolder m27190(View view, ViewBinder viewBinder) {
        StaticNativeViewHolder staticNativeViewHolder = new StaticNativeViewHolder();
        staticNativeViewHolder.f21299 = view;
        try {
            staticNativeViewHolder.f21296 = (TextView) view.findViewById(viewBinder.f21306);
            staticNativeViewHolder.f21298 = (TextView) view.findViewById(viewBinder.f21308);
            staticNativeViewHolder.f21297 = (TextView) view.findViewById(viewBinder.f21307);
            staticNativeViewHolder.f21295 = (ImageView) view.findViewById(viewBinder.f21305);
            staticNativeViewHolder.f21293 = (ImageView) view.findViewById(viewBinder.f21302);
            staticNativeViewHolder.f21294 = (ImageView) view.findViewById(viewBinder.f21303);
            return staticNativeViewHolder;
        } catch (ClassCastException e) {
            MoPubLog.w("Could not cast from id in ViewBinder to expected View type", e);
            return new StaticNativeViewHolder();
        }
    }
}
