package com.mopub.nativeads;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.adincube.sdk.AdinCube;
import com.adincube.sdk.AdinCubeNativeEventListener;
import com.adincube.sdk.NativeAd;
import com.mopub.nativeads.CustomEventNative;
import com.mopub.nativeads.NativeImageHelper;
import com.typhoon.tv.Logger;
import com.typhoon.tv.TVApplication;
import com.typhoon.tv.utils.NativeAdsUtils;
import com.typhoon.tv.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AdinCubeCustomEventNative extends CustomEventNative {

    public static class AdinCubeNativeAd extends StaticNativeAd {

        /* renamed from: 靐  reason: contains not printable characters */
        private List<NativeAd> f21083;

        /* renamed from: 齉  reason: contains not printable characters */
        private NativeAd f21084;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final CustomEventNative.CustomEventNativeListener f21085;

        public AdinCubeNativeAd(Context context, List<NativeAd> list, boolean z, CustomEventNative.CustomEventNativeListener customEventNativeListener) {
            this.f21085 = customEventNativeListener;
            this.f21083 = list;
            if (z) {
                try {
                    this.f21083 = NativeAdsUtils.m17796(this.f21083);
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            try {
                this.f21084 = this.f21083.get(0);
                ArrayList arrayList = new ArrayList();
                if (this.f21084.m2360() != null) {
                    String r3 = this.f21084.m2360().m2366();
                    setIconImageUrl(r3);
                    arrayList.add(r3);
                }
                if (this.f21084.m2358() != null) {
                    String r0 = this.f21084.m2358().m2366();
                    setMainImageUrl(r0);
                    arrayList.add(r0);
                }
                setTitle(this.f21084.m2362());
                if (this.f21084.m2361() != null) {
                    String r1 = this.f21084.m2361();
                    if (this.f21084.m2356() != null) {
                        r1 = ((double) this.f21084.m2356().floatValue()) >= 4.5d ? "★ " + String.valueOf(this.f21084.m2356()) + " - " + r1 : r1;
                        setStarRating(Double.valueOf(this.f21084.m2356().doubleValue()));
                    }
                    setText(r1);
                }
                setCallToAction(this.f21084.m2359());
                NativeImageHelper.preCacheImages(context, arrayList, new NativeImageHelper.ImageListener() {
                    public void onImagesCached() {
                        AdinCubeNativeAd.this.f21085.onNativeAdLoaded(AdinCubeNativeAd.this);
                    }

                    public void onImagesFailedToCache(NativeErrorCode nativeErrorCode) {
                        AdinCubeNativeAd.this.f21085.onNativeAdFailed(nativeErrorCode);
                    }
                });
            } catch (Exception e2) {
                Logger.m6281((Throwable) e2, new boolean[0]);
                this.f21085.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
            }
        }

        public void destroy() {
            super.destroy();
            if (this.f21084 != null) {
                try {
                    AdinCube.Native.m2313(this.f21084);
                } catch (Exception e) {
                    Logger.m6281((Throwable) e, new boolean[0]);
                }
            }
            this.f21084 = null;
            if (this.f21083 != null) {
                try {
                    AdinCube.Native.m2314(this.f21083);
                } catch (Exception e2) {
                    Logger.m6281((Throwable) e2, new boolean[0]);
                }
            }
            this.f21083 = null;
        }

        public void prepare(View view) {
            super.prepare(view);
            if (!(view instanceof ViewGroup) && this.f21085 != null) {
                this.f21085.onNativeAdFailed(NativeErrorCode.NATIVE_RENDERER_CONFIGURATION_ERROR);
            } else if ((view instanceof ViewGroup) && this.f21084 != null) {
                AdinCube.Native.m2311((ViewGroup) view, this.f21084);
                m27050();
                view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        AdinCubeNativeAd.this.m27046();
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27044(final Context context, CustomEventNative.CustomEventNativeListener customEventNativeListener, Map<String, Object> map, Map<String, String> map2) {
        String str;
        Object obj;
        String str2;
        if (!(context instanceof Activity)) {
            customEventNativeListener.onNativeAdFailed(NativeErrorCode.NATIVE_ADAPTER_CONFIGURATION_ERROR);
            return;
        }
        boolean z = true;
        boolean z2 = false;
        final boolean z3 = TVApplication.m6285().getBoolean("is_nad_mech_enabled", false);
        int i = 1;
        if (z3) {
            i = 3;
            try {
                if (!map2.isEmpty() && map2.containsKey("loadAdCount") && (str2 = map2.get("loadAdCount")) != null && Utils.m6426(str2)) {
                    i = Integer.parseInt(str2);
                }
            } catch (Exception e) {
                Logger.m6281((Throwable) e, new boolean[0]);
            }
        }
        if (!map.isEmpty() && map.containsKey("isBanner")) {
            Object obj2 = map.get("isBanner");
            if (obj2 != null && (obj2 instanceof Boolean)) {
                z2 = ((Boolean) obj2).booleanValue();
            }
            if (map.containsKey("loadAdinCubeNativeBanner") && (obj = map.get("loadAdinCubeNativeBanner")) != null && (obj instanceof Boolean)) {
                z = ((Boolean) obj).booleanValue();
            }
            if (!map2.isEmpty() && map2.containsKey("loadAdinCubeNativeBanner") && (str = map2.get("loadAdinCubeNativeBanner")) != null) {
                if (str.equalsIgnoreCase("true")) {
                    z = true;
                } else if (str.equalsIgnoreCase("false")) {
                    z = false;
                }
            }
            if (z2 && !z) {
                customEventNativeListener.onNativeAdFailed(NativeErrorCode.UNSPECIFIED);
                return;
            }
        }
        final CustomEventNative.CustomEventNativeListener customEventNativeListener2 = customEventNativeListener;
        AnonymousClass1 r9 = new AdinCubeNativeEventListener() {
            public void onAdLoaded(List<NativeAd> list) {
                if (list == null || list.isEmpty() || list.get(0) == null) {
                    customEventNativeListener2.onNativeAdFailed(NativeErrorCode.NETWORK_NO_FILL);
                } else {
                    new AdinCubeNativeAd(context, list, z3, customEventNativeListener2);
                }
            }

            public void onLoadError(String str) {
                super.onLoadError(str);
                customEventNativeListener2.onNativeAdFailed(NativeErrorCode.UNSPECIFIED);
            }
        };
        if (z3) {
            AdinCube.Native.m2309(context, i, r9);
        } else {
            AdinCube.Native.m2310(context, (AdinCubeNativeEventListener) r9);
        }
    }
}
