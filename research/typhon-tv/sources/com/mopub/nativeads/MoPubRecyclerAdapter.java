package com.mopub.nativeads;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import com.Pinkamena;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.VisibilityTracker;
import java.util.List;
import java.util.WeakHashMap;

public final class MoPubRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private RecyclerView f21159;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public ContentChangeStrategy f21160;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MoPubNativeAdLoadedListener f21161;

    /* renamed from: 连任  reason: contains not printable characters */
    private final WeakHashMap<View, Integer> f21162;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final MoPubStreamAdPlacer f21163;

    /* renamed from: 麤  reason: contains not printable characters */
    private final VisibilityTracker f21164;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final RecyclerView.Adapter f21165;

    /* renamed from: 龘  reason: contains not printable characters */
    private final RecyclerView.AdapterDataObserver f21166;

    public enum ContentChangeStrategy {
        INSERT_AT_END,
        MOVE_ALL_ADS_WITH_CONTENT,
        KEEP_ADS_FIXED
    }

    public MoPubRecyclerAdapter(Activity activity, RecyclerView.Adapter adapter) {
        this(activity, adapter, MoPubNativeAdPositioning.serverPositioning());
    }

    public MoPubRecyclerAdapter(Activity activity, RecyclerView.Adapter adapter, MoPubNativeAdPositioning.MoPubClientPositioning moPubClientPositioning) {
        this(new MoPubStreamAdPlacer(activity, moPubClientPositioning), adapter, new VisibilityTracker(activity));
    }

    public MoPubRecyclerAdapter(Activity activity, RecyclerView.Adapter adapter, MoPubNativeAdPositioning.MoPubServerPositioning moPubServerPositioning) {
        this(new MoPubStreamAdPlacer(activity, moPubServerPositioning), adapter, new VisibilityTracker(activity));
    }

    @VisibleForTesting
    MoPubRecyclerAdapter(MoPubStreamAdPlacer moPubStreamAdPlacer, RecyclerView.Adapter adapter, VisibilityTracker visibilityTracker) {
        this.f21160 = ContentChangeStrategy.INSERT_AT_END;
        this.f21162 = new WeakHashMap<>();
        this.f21165 = adapter;
        this.f21164 = visibilityTracker;
        this.f21164.m27212((VisibilityTracker.VisibilityTrackerListener) new VisibilityTracker.VisibilityTrackerListener() {
            public void onVisibilityChanged(List<View> list, List<View> list2) {
                MoPubRecyclerAdapter.this.m27103(list, list2);
            }
        });
        m27104(this.f21165.hasStableIds());
        this.f21163 = moPubStreamAdPlacer;
        this.f21163.setAdLoadedListener(new MoPubNativeAdLoadedListener() {
            public void onAdLoaded(int i) {
                MoPubRecyclerAdapter.this.m27106(i);
            }

            public void onAdRemoved(int i) {
                MoPubRecyclerAdapter.this.m27105(i);
            }
        });
        this.f21163.setItemCount(this.f21165.getItemCount());
        this.f21166 = new RecyclerView.AdapterDataObserver() {
            public void onChanged() {
                MoPubRecyclerAdapter.this.f21163.setItemCount(MoPubRecyclerAdapter.this.f21165.getItemCount());
                MoPubRecyclerAdapter.this.notifyDataSetChanged();
            }

            public void onItemRangeChanged(int i, int i2) {
                int adjustedPosition = MoPubRecyclerAdapter.this.f21163.getAdjustedPosition((i + i2) - 1);
                int adjustedPosition2 = MoPubRecyclerAdapter.this.f21163.getAdjustedPosition(i);
                MoPubRecyclerAdapter.this.notifyItemRangeChanged(adjustedPosition2, (adjustedPosition - adjustedPosition2) + 1);
            }

            public void onItemRangeInserted(int i, int i2) {
                int adjustedPosition = MoPubRecyclerAdapter.this.f21163.getAdjustedPosition(i);
                int itemCount = MoPubRecyclerAdapter.this.f21165.getItemCount();
                MoPubRecyclerAdapter.this.f21163.setItemCount(itemCount);
                boolean z = i + i2 >= itemCount;
                if (ContentChangeStrategy.KEEP_ADS_FIXED == MoPubRecyclerAdapter.this.f21160 || (ContentChangeStrategy.INSERT_AT_END == MoPubRecyclerAdapter.this.f21160 && z)) {
                    MoPubRecyclerAdapter.this.notifyDataSetChanged();
                    return;
                }
                for (int i3 = 0; i3 < i2; i3++) {
                    MoPubRecyclerAdapter.this.f21163.insertItem(i);
                }
                MoPubRecyclerAdapter.this.notifyItemRangeInserted(adjustedPosition, i2);
            }

            public void onItemRangeMoved(int i, int i2, int i3) {
                MoPubRecyclerAdapter.this.notifyDataSetChanged();
            }

            public void onItemRangeRemoved(int i, int i2) {
                int adjustedPosition = MoPubRecyclerAdapter.this.f21163.getAdjustedPosition(i);
                int itemCount = MoPubRecyclerAdapter.this.f21165.getItemCount();
                MoPubRecyclerAdapter.this.f21163.setItemCount(itemCount);
                boolean z = i + i2 >= itemCount;
                if (ContentChangeStrategy.KEEP_ADS_FIXED == MoPubRecyclerAdapter.this.f21160 || (ContentChangeStrategy.INSERT_AT_END == MoPubRecyclerAdapter.this.f21160 && z)) {
                    MoPubRecyclerAdapter.this.notifyDataSetChanged();
                    return;
                }
                int adjustedCount = MoPubRecyclerAdapter.this.f21163.getAdjustedCount(itemCount + i2);
                for (int i3 = 0; i3 < i2; i3++) {
                    MoPubRecyclerAdapter.this.f21163.removeItem(i);
                }
                int adjustedCount2 = adjustedCount - MoPubRecyclerAdapter.this.f21163.getAdjustedCount(itemCount);
                MoPubRecyclerAdapter.this.notifyItemRangeRemoved(adjustedPosition - (adjustedCount2 - i2), adjustedCount2);
            }
        };
        this.f21165.registerAdapterDataObserver(this.f21166);
    }

    public static int computeScrollOffset(LinearLayoutManager linearLayoutManager, RecyclerView.ViewHolder viewHolder) {
        if (viewHolder == null) {
            return 0;
        }
        View view = viewHolder.itemView;
        if (linearLayoutManager.canScrollVertically()) {
            return linearLayoutManager.getStackFromEnd() ? view.getBottom() : view.getTop();
        }
        if (linearLayoutManager.canScrollHorizontally()) {
            return linearLayoutManager.getStackFromEnd() ? view.getRight() : view.getLeft();
        }
        return 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27103(List<View> list, List<View> list2) {
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        int i2 = 0;
        for (View view : list) {
            Integer num = this.f21162.get(view);
            if (num != null) {
                i = Math.min(num.intValue(), i);
                i2 = Math.max(num.intValue(), i2);
            }
        }
        this.f21163.placeAdsInRange(i, i2 + 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27104(boolean z) {
        super.setHasStableIds(z);
    }

    public void clearAds() {
        this.f21163.clearAds();
    }

    public void destroy() {
        this.f21165.unregisterAdapterDataObserver(this.f21166);
        this.f21163.destroy();
        this.f21164.m27205();
    }

    public int getAdjustedPosition(int i) {
        return this.f21163.getAdjustedPosition(i);
    }

    public int getItemCount() {
        return this.f21163.getAdjustedCount(this.f21165.getItemCount());
    }

    public long getItemId(int i) {
        if (!this.f21165.hasStableIds()) {
            return -1;
        }
        Object adData = this.f21163.getAdData(i);
        return adData != null ? (long) (-System.identityHashCode(adData)) : this.f21165.getItemId(this.f21163.getOriginalPosition(i));
    }

    public int getItemViewType(int i) {
        int adViewType = this.f21163.getAdViewType(i);
        return adViewType != 0 ? adViewType - 56 : this.f21165.getItemViewType(this.f21163.getOriginalPosition(i));
    }

    public int getOriginalPosition(int i) {
        return this.f21163.getOriginalPosition(i);
    }

    public boolean isAd(int i) {
        return this.f21163.isAd(i);
    }

    public void loadAds(String str) {
        MoPubStreamAdPlacer moPubStreamAdPlacer = this.f21163;
        Pinkamena.DianePie();
    }

    public void loadAds(String str, RequestParameters requestParameters) {
        MoPubStreamAdPlacer moPubStreamAdPlacer = this.f21163;
        Pinkamena.DianePie();
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.f21159 = recyclerView;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Object adData = this.f21163.getAdData(i);
        if (adData != null) {
            this.f21163.bindAdView((NativeAd) adData, viewHolder.itemView);
            return;
        }
        this.f21162.put(viewHolder.itemView, Integer.valueOf(i));
        this.f21164.m27209(viewHolder.itemView, 0);
        this.f21165.onBindViewHolder(viewHolder, this.f21163.getOriginalPosition(i));
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i < -56 || i > this.f21163.getAdViewTypeCount() - 56) {
            return this.f21165.onCreateViewHolder(viewGroup, i);
        }
        MoPubAdRenderer adRendererForViewType = this.f21163.getAdRendererForViewType(i + 56);
        if (adRendererForViewType != null) {
            return new MoPubRecyclerViewHolder(adRendererForViewType.createAdView((Activity) viewGroup.getContext(), viewGroup));
        }
        MoPubLog.w("No view binder was registered for ads in MoPubRecyclerAdapter.");
        return null;
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.f21159 = null;
    }

    public boolean onFailedToRecycleView(RecyclerView.ViewHolder viewHolder) {
        return viewHolder instanceof MoPubRecyclerViewHolder ? super.onFailedToRecycleView(viewHolder) : this.f21165.onFailedToRecycleView(viewHolder);
    }

    public void onViewAttachedToWindow(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof MoPubRecyclerViewHolder) {
            super.onViewAttachedToWindow(viewHolder);
        } else {
            this.f21165.onViewAttachedToWindow(viewHolder);
        }
    }

    public void onViewDetachedFromWindow(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof MoPubRecyclerViewHolder) {
            super.onViewDetachedFromWindow(viewHolder);
        } else {
            this.f21165.onViewDetachedFromWindow(viewHolder);
        }
    }

    public void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof MoPubRecyclerViewHolder) {
            super.onViewRecycled(viewHolder);
        } else {
            this.f21165.onViewRecycled(viewHolder);
        }
    }

    public void refreshAds(String str) {
        Pinkamena.DianePie();
    }

    public void refreshAds(String str, RequestParameters requestParameters) {
        if (this.f21159 == null) {
            MoPubLog.w("This adapter is not attached to a RecyclerView and cannot be refreshed.");
            return;
        }
        RecyclerView.LayoutManager layoutManager = this.f21159.getLayoutManager();
        if (layoutManager == null) {
            MoPubLog.w("Can't refresh ads when there is no layout manager on a RecyclerView.");
        } else if (layoutManager instanceof LinearLayoutManager) {
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
            int findFirstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
            int computeScrollOffset = computeScrollOffset(linearLayoutManager, this.f21159.findViewHolderForLayoutPosition(findFirstVisibleItemPosition));
            int max = Math.max(0, findFirstVisibleItemPosition - 1);
            while (this.f21163.isAd(max) && max > 0) {
                max--;
            }
            int itemCount = getItemCount();
            int findLastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition();
            while (this.f21163.isAd(findLastVisibleItemPosition) && findLastVisibleItemPosition < itemCount - 1) {
                findLastVisibleItemPosition++;
            }
            int originalPosition = this.f21163.getOriginalPosition(max);
            this.f21163.removeAdsInRange(this.f21163.getOriginalPosition(findLastVisibleItemPosition), this.f21165.getItemCount());
            int removeAdsInRange = this.f21163.removeAdsInRange(0, originalPosition);
            if (removeAdsInRange > 0) {
                linearLayoutManager.scrollToPositionWithOffset(findFirstVisibleItemPosition - removeAdsInRange, computeScrollOffset);
            }
            Pinkamena.DianePie();
        } else {
            MoPubLog.w("This LayoutManager can't be refreshed.");
        }
    }

    public void registerAdRenderer(MoPubAdRenderer moPubAdRenderer) {
        if (Preconditions.NoThrow.checkNotNull(moPubAdRenderer, "Cannot register a null adRenderer")) {
            this.f21163.registerAdRenderer(moPubAdRenderer);
        }
    }

    public void setAdLoadedListener(MoPubNativeAdLoadedListener moPubNativeAdLoadedListener) {
        this.f21161 = moPubNativeAdLoadedListener;
    }

    public void setContentChangeStrategy(ContentChangeStrategy contentChangeStrategy) {
        if (Preconditions.NoThrow.checkNotNull(contentChangeStrategy)) {
            this.f21160 = contentChangeStrategy;
        }
    }

    public void setHasStableIds(boolean z) {
        m27104(z);
        this.f21165.unregisterAdapterDataObserver(this.f21166);
        this.f21165.setHasStableIds(z);
        this.f21165.registerAdapterDataObserver(this.f21166);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27105(int i) {
        if (this.f21161 != null) {
            this.f21161.onAdRemoved(i);
        }
        notifyItemRemoved(i);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27106(int i) {
        if (this.f21161 != null) {
            this.f21161.onAdLoaded(i);
        }
        notifyItemInserted(i);
    }
}
