package com.mopub.nativeads;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.Pinkamena;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import java.util.WeakHashMap;

public class MoPubStaticNativeAdRenderer implements MoPubAdRenderer<StaticNativeAd> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ViewBinder f21171;
    @VisibleForTesting

    /* renamed from: 龘  reason: contains not printable characters */
    final WeakHashMap<View, StaticNativeViewHolder> f21172 = new WeakHashMap<>();

    public MoPubStaticNativeAdRenderer(ViewBinder viewBinder) {
        this.f21171 = viewBinder;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27107(StaticNativeViewHolder staticNativeViewHolder, int i) {
        if (staticNativeViewHolder.f21299 != null) {
            staticNativeViewHolder.f21299.setVisibility(i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27108(StaticNativeViewHolder staticNativeViewHolder, StaticNativeAd staticNativeAd) {
        NativeRendererHelper.addTextView(staticNativeViewHolder.f21296, staticNativeAd.getTitle());
        NativeRendererHelper.addTextView(staticNativeViewHolder.f21298, staticNativeAd.getText());
        NativeRendererHelper.addTextView(staticNativeViewHolder.f21297, staticNativeAd.getCallToAction());
        String mainImageUrl = staticNativeAd.getMainImageUrl();
        ImageView imageView = staticNativeViewHolder.f21295;
        Pinkamena.DianePie();
        String iconImageUrl = staticNativeAd.getIconImageUrl();
        ImageView imageView2 = staticNativeViewHolder.f21293;
        Pinkamena.DianePie();
        NativeRendererHelper.addPrivacyInformationIcon(staticNativeViewHolder.f21294, staticNativeAd.getPrivacyInformationIconImageUrl(), staticNativeAd.getPrivacyInformationIconClickThroughUrl());
    }

    public View createAdView(Context context, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(this.f21171.f21309, viewGroup, false);
    }

    public void renderAdView(View view, StaticNativeAd staticNativeAd) {
        StaticNativeViewHolder staticNativeViewHolder = this.f21172.get(view);
        if (staticNativeViewHolder == null) {
            staticNativeViewHolder = StaticNativeViewHolder.m27190(view, this.f21171);
            this.f21172.put(view, staticNativeViewHolder);
        }
        m27108(staticNativeViewHolder, staticNativeAd);
        NativeRendererHelper.updateExtras(staticNativeViewHolder.f21299, this.f21171.f21304, staticNativeAd.getExtras());
        m27107(staticNativeViewHolder, 0);
    }

    public boolean supports(BaseNativeAd baseNativeAd) {
        Preconditions.checkNotNull(baseNativeAd);
        return baseNativeAd instanceof StaticNativeAd;
    }
}
