package com.mopub.nativeads;

public class IntInterval implements Comparable<IntInterval> {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f21120;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f21121;

    public IntInterval(int i, int i2) {
        this.f21121 = i;
        this.f21120 = i2;
    }

    public int compareTo(IntInterval intInterval) {
        return this.f21121 == intInterval.f21121 ? this.f21120 - intInterval.f21120 : this.f21121 - intInterval.f21121;
    }

    public boolean equals(int i, int i2) {
        return this.f21121 == i && this.f21120 == i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IntInterval)) {
            return false;
        }
        IntInterval intInterval = (IntInterval) obj;
        return this.f21121 == intInterval.f21121 && this.f21120 == intInterval.f21120;
    }

    public int getLength() {
        return this.f21120;
    }

    public int getStart() {
        return this.f21121;
    }

    public int hashCode() {
        return ((this.f21121 + 899) * 31) + this.f21120;
    }

    public void setLength(int i) {
        this.f21120 = i;
    }

    public void setStart(int i) {
        this.f21121 = i;
    }

    public String toString() {
        return "{start : " + this.f21121 + ", length : " + this.f21120 + "}";
    }
}
