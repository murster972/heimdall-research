package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.mopub.common.Preconditions;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.VisibleForTesting;

public class NativeClickHandler {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f21232;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean f21233;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f21234;

    public NativeClickHandler(Context context) {
        this(context, (String) null);
    }

    public NativeClickHandler(Context context, String str) {
        Preconditions.checkNotNull(context);
        this.f21234 = context.getApplicationContext();
        this.f21232 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27142(View view, View.OnClickListener onClickListener) {
        view.setOnClickListener(onClickListener);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                m27142(viewGroup.getChildAt(i), onClickListener);
            }
        }
    }

    public void clearOnClickListener(View view) {
        if (Preconditions.NoThrow.checkNotNull(view, "Cannot clear click listener from a null view")) {
            m27142(view, (View.OnClickListener) null);
        }
    }

    public void openClickDestinationUrl(String str, View view) {
        m27144(str, view, new SpinningProgressView(this.f21234));
    }

    public void setOnClickListener(View view, final ClickInterface clickInterface) {
        if (Preconditions.NoThrow.checkNotNull(view, "Cannot set click listener on a null view") && Preconditions.NoThrow.checkNotNull(clickInterface, "Cannot set click listener with a null ClickInterface")) {
            m27142(view, (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    clickInterface.handleClick(view);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27144(String str, final View view, final SpinningProgressView spinningProgressView) {
        if (Preconditions.NoThrow.checkNotNull(str, "Cannot open a null click destination url")) {
            Preconditions.checkNotNull(spinningProgressView);
            if (!this.f21233) {
                this.f21233 = true;
                if (view != null) {
                    spinningProgressView.m27189(view);
                }
                UrlHandler.Builder builder = new UrlHandler.Builder();
                if (!TextUtils.isEmpty(this.f21232)) {
                    builder.withDspCreativeId(this.f21232);
                }
                builder.withSupportedUrlActions(UrlAction.IGNORE_ABOUT_SCHEME, UrlAction.OPEN_NATIVE_BROWSER, UrlAction.OPEN_APP_MARKET, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK).withResultActions(new UrlHandler.ResultActions() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    private void m27145() {
                        if (view != null) {
                            spinningProgressView.m27188();
                        }
                    }

                    public void urlHandlingFailed(String str, UrlAction urlAction) {
                        m27145();
                        boolean unused = NativeClickHandler.this.f21233 = false;
                    }

                    public void urlHandlingSucceeded(String str, UrlAction urlAction) {
                        m27145();
                        boolean unused = NativeClickHandler.this.f21233 = false;
                    }
                }).build().handleUrl(this.f21234, str);
            }
        }
    }
}
