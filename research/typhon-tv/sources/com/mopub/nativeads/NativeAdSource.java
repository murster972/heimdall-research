package com.mopub.nativeads;

import android.app.Activity;
import android.os.Handler;
import android.os.SystemClock;
import com.mopub.common.VisibleForTesting;
import com.mopub.nativeads.MoPubNative;
import java.util.ArrayList;
import java.util.List;

class NativeAdSource {
    @VisibleForTesting

    /* renamed from: 龘  reason: contains not printable characters */
    static final int[] f21216 = {1000, 3000, 5000, 25000, 60000, 300000};
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final List<TimestampWrapper<NativeAd>> f21217;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public final Handler f21218;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Runnable f21219;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public MoPubNative f21220;

    /* renamed from: ˈ  reason: contains not printable characters */
    private RequestParameters f21221;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final MoPubNative.MoPubNativeNetworkListener f21222;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final AdRendererRegistry f21223;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public AdSourceListener f21224;
    @VisibleForTesting

    /* renamed from: 连任  reason: contains not printable characters */
    int f21225;
    @VisibleForTesting

    /* renamed from: 靐  reason: contains not printable characters */
    boolean f21226;
    @VisibleForTesting

    /* renamed from: 麤  reason: contains not printable characters */
    int f21227;
    @VisibleForTesting

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f21228;

    interface AdSourceListener {
        void onAdsAvailable();
    }

    NativeAdSource() {
        this(new ArrayList(1), new Handler(), new AdRendererRegistry());
    }

    @VisibleForTesting
    NativeAdSource(List<TimestampWrapper<NativeAd>> list, Handler handler, AdRendererRegistry adRendererRegistry) {
        this.f21217 = list;
        this.f21218 = handler;
        this.f21219 = new Runnable() {
            public void run() {
                NativeAdSource.this.f21228 = false;
                NativeAdSource.this.m27132();
            }
        };
        this.f21223 = adRendererRegistry;
        this.f21222 = new MoPubNative.MoPubNativeNetworkListener() {
            public void onNativeFail(NativeErrorCode nativeErrorCode) {
                NativeAdSource.this.f21226 = false;
                if (NativeAdSource.this.f21225 >= NativeAdSource.f21216.length - 1) {
                    NativeAdSource.this.m27133();
                    return;
                }
                NativeAdSource.this.m27135();
                NativeAdSource.this.f21228 = true;
                NativeAdSource.this.f21218.postDelayed(NativeAdSource.this.f21219, (long) NativeAdSource.this.m27131());
            }

            public void onNativeLoad(NativeAd nativeAd) {
                if (NativeAdSource.this.f21220 != null) {
                    NativeAdSource.this.f21226 = false;
                    NativeAdSource.this.f21227++;
                    NativeAdSource.this.m27133();
                    NativeAdSource.this.f21217.add(new TimestampWrapper(nativeAd));
                    if (NativeAdSource.this.f21217.size() == 1 && NativeAdSource.this.f21224 != null) {
                        NativeAdSource.this.f21224.onAdsAvailable();
                    }
                    NativeAdSource.this.m27132();
                }
            }
        };
        this.f21227 = 0;
        m27133();
    }

    public MoPubAdRenderer getAdRendererForViewType(int i) {
        return this.f21223.getRendererForViewType(i);
    }

    public int getViewTypeForAd(NativeAd nativeAd) {
        return this.f21223.getViewTypeForAd(nativeAd);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m27131() {
        if (this.f21225 >= f21216.length) {
            this.f21225 = f21216.length - 1;
        }
        return f21216[this.f21225];
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m27132() {
        if (!this.f21226 && this.f21220 != null && this.f21217.size() < 1) {
            this.f21226 = true;
            this.f21220.makeRequest(this.f21221, Integer.valueOf(this.f21227));
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 连任  reason: contains not printable characters */
    public void m27133() {
        this.f21225 = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27134() {
        if (this.f21220 != null) {
            this.f21220.destroy();
            this.f21220 = null;
        }
        this.f21221 = null;
        for (TimestampWrapper<NativeAd> timestampWrapper : this.f21217) {
            ((NativeAd) timestampWrapper.f21301).destroy();
        }
        this.f21217.clear();
        this.f21218.removeMessages(0);
        this.f21226 = false;
        this.f21227 = 0;
        m27133();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 麤  reason: contains not printable characters */
    public void m27135() {
        if (this.f21225 < f21216.length - 1) {
            this.f21225++;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public NativeAd m27136() {
        long uptimeMillis = SystemClock.uptimeMillis();
        if (!this.f21226 && !this.f21228) {
            this.f21218.post(this.f21219);
        }
        while (!this.f21217.isEmpty()) {
            TimestampWrapper remove = this.f21217.remove(0);
            if (uptimeMillis - remove.f21300 < 14400000) {
                return (NativeAd) remove.f21301;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m27137() {
        return this.f21223.getAdRendererCount();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27138(Activity activity, String str, RequestParameters requestParameters) {
        m27141(requestParameters, new MoPubNative(activity, str, this.f21222));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27139(MoPubAdRenderer moPubAdRenderer) {
        this.f21223.registerAdRenderer(moPubAdRenderer);
        if (this.f21220 != null) {
            this.f21220.registerAdRenderer(moPubAdRenderer);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27140(AdSourceListener adSourceListener) {
        this.f21224 = adSourceListener;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27141(RequestParameters requestParameters, MoPubNative moPubNative) {
        m27134();
        for (MoPubAdRenderer registerAdRenderer : this.f21223.getRendererIterable()) {
            moPubNative.registerAdRenderer(registerAdRenderer);
        }
        this.f21221 = requestParameters;
        this.f21220 = moPubNative;
        m27132();
    }
}
