package com.mopub.nativeads;

import android.content.Context;
import android.text.TextUtils;
import com.mopub.common.AdUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.TyphoonApp;

class NativeUrlGenerator extends AdUrlGenerator {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f21248;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f21249;

    NativeUrlGenerator(Context context) {
        super(context);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m27146() {
        if (!TextUtils.isEmpty(this.f21249)) {
            m26450("assets", this.f21249);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m27147() {
        if (!TextUtils.isEmpty(this.f21248)) {
            m26450("MAGIC_NO", this.f21248);
        }
    }

    public String generateUrlString(String str) {
        m26454(str, TyphoonApp.AD_HANDLER);
        m6156(ClientMetadata.getInstance(this.f5657));
        m27146();
        m27147();
        return m26452();
    }

    public NativeUrlGenerator withAdUnitId(String str) {
        this.f5654 = str;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27148(String str) {
        m26450("nsv", str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public NativeUrlGenerator m27149(int i) {
        this.f21248 = String.valueOf(i);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public NativeUrlGenerator m27150(RequestParameters requestParameters) {
        if (requestParameters != null) {
            this.f5656 = requestParameters.getKeywords();
            this.f5655 = requestParameters.getLocation();
            this.f21249 = requestParameters.getDesiredAssets();
        }
        return this;
    }
}
