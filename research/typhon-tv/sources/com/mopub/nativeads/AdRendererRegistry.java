package com.mopub.nativeads;

import com.mopub.common.Preconditions;
import java.util.ArrayList;
import java.util.Iterator;

public class AdRendererRegistry {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ArrayList<MoPubAdRenderer> f21078 = new ArrayList<>();

    public int getAdRendererCount() {
        return this.f21078.size();
    }

    public MoPubAdRenderer getRendererForAd(BaseNativeAd baseNativeAd) {
        Preconditions.checkNotNull(baseNativeAd);
        Iterator<MoPubAdRenderer> it2 = this.f21078.iterator();
        while (it2.hasNext()) {
            MoPubAdRenderer next = it2.next();
            if (next.supports(baseNativeAd)) {
                return next;
            }
        }
        return null;
    }

    public MoPubAdRenderer getRendererForViewType(int i) {
        try {
            return this.f21078.get(i - 1);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Iterable<MoPubAdRenderer> getRendererIterable() {
        return this.f21078;
    }

    public int getViewTypeForAd(NativeAd nativeAd) {
        Preconditions.checkNotNull(nativeAd);
        for (int i = 0; i < this.f21078.size(); i++) {
            if (nativeAd.getMoPubAdRenderer() == this.f21078.get(i)) {
                return i + 1;
            }
        }
        return 0;
    }

    public void registerAdRenderer(MoPubAdRenderer moPubAdRenderer) {
        this.f21078.add(moPubAdRenderer);
    }
}
