package com.mopub.nativeads;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.Pinkamena;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Drawables;
import java.util.Map;

public class NativeRendererHelper {
    public static void addCtaButton(TextView textView, final View view, String str) {
        addTextView(textView, str);
        if (textView != null && view != null) {
            textView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    view.performClick();
                }
            });
        }
    }

    public static void addPrivacyInformationIcon(ImageView imageView, String str, final String str2) {
        if (imageView != null) {
            if (str2 == null) {
                imageView.setImageDrawable((Drawable) null);
                imageView.setOnClickListener((View.OnClickListener) null);
                imageView.setVisibility(4);
                return;
            }
            final Context context = imageView.getContext();
            if (context != null) {
                if (str == null) {
                    imageView.setImageDrawable(Drawables.NATIVE_PRIVACY_INFORMATION_ICON.createDrawable(context));
                } else {
                    Pinkamena.DianePie();
                }
                imageView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        new UrlHandler.Builder().withSupportedUrlActions(UrlAction.IGNORE_ABOUT_SCHEME, UrlAction.OPEN_NATIVE_BROWSER, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK).build().handleUrl(context, str2);
                    }
                });
                imageView.setVisibility(0);
            }
        }
    }

    public static void addTextView(TextView textView, String str) {
        if (textView != null) {
            textView.setText((CharSequence) null);
            if (str != null) {
                textView.setText(str);
                if (textView.getEllipsize() != null && textView.getEllipsize().equals(TextUtils.TruncateAt.MARQUEE)) {
                    textView.setSelected(true);
                    textView.setSingleLine(true);
                }
            }
        }
    }

    public static void updateExtras(View view, Map<String, Integer> map, Map<String, Object> map2) {
        if (view == null) {
            MoPubLog.w("Attempted to bind extras on a null main view.");
            return;
        }
        for (String next : map.keySet()) {
            View findViewById = view.findViewById(map.get(next).intValue());
            Object obj = map2.get(next);
            if (findViewById instanceof ImageView) {
                ((ImageView) findViewById).setImageDrawable((Drawable) null);
                Object obj2 = map2.get(next);
                if (obj2 != null && (obj2 instanceof String)) {
                    String str = (String) obj2;
                    ImageView imageView = (ImageView) findViewById;
                    Pinkamena.DianePie();
                }
            } else if (findViewById instanceof TextView) {
                ((TextView) findViewById).setText((CharSequence) null);
                if (obj instanceof String) {
                    addTextView((TextView) findViewById, (String) obj);
                }
            }
        }
    }
}
