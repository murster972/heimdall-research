package com.mopub.common;

import android.net.Uri;
import android.os.AsyncTask;
import com.mopub.common.util.AsyncTasks;
import com.mopub.common.util.Network;
import java.io.IOException;
import java.net.URISyntaxException;

@VisibleForTesting
public class UrlResolutionTask extends AsyncTask<String, Void, String> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final UrlResolutionListener f20563;

    interface UrlResolutionListener {
        void onFailure(String str, Throwable th);

        void onSuccess(String str);
    }

    UrlResolutionTask(UrlResolutionListener urlResolutionListener) {
        this.f20563 = urlResolutionListener;
    }

    public static void getResolvedUrl(String str, UrlResolutionListener urlResolutionListener) {
        try {
            AsyncTasks.safeExecuteOnExecutor(new UrlResolutionTask(urlResolutionListener), str);
        } catch (Exception e) {
            urlResolutionListener.onFailure("Failed to resolve url", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        this.f20563.onFailure("Task for resolving url was cancelled", (Throwable) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String doInBackground(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        String str = null;
        try {
            String str2 = strArr[0];
            int i = 0;
            while (str2 != null && i < 10) {
                if (!UrlAction.OPEN_IN_APP_BROWSER.shouldTryHandlingUrl(Uri.parse(str2))) {
                    return str2;
                }
                str = str2;
                str2 = Network.getRedirectLocation(str2);
                i++;
            }
            return str;
        } catch (IOException e) {
            return null;
        } catch (URISyntaxException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void onPostExecute(String str) {
        super.onPostExecute(str);
        if (isCancelled() || str == null) {
            onCancelled();
        } else {
            this.f20563.onSuccess(str);
        }
    }
}
