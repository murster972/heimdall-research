package com.mopub.common.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.widget.Toast;
import com.mopub.common.MoPubBrowser;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.MraidVideoPlayerActivity;
import java.util.ArrayList;
import java.util.List;

public class ManifestUtils {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final List<Class<? extends Activity>> f20659 = new ArrayList(1);

    /* renamed from: 齉  reason: contains not printable characters */
    private static FlagCheckUtil f20660 = new FlagCheckUtil();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final List<Class<? extends Activity>> f20661 = new ArrayList(4);

    private static class ActivityConfigChanges {
        public boolean hasKeyboardHidden;
        public boolean hasOrientation;
        public boolean hasScreenSize;

        private ActivityConfigChanges() {
        }
    }

    static class FlagCheckUtil {
        FlagCheckUtil() {
        }

        public boolean hasFlag(Class cls, int i, int i2) {
            return Utils.bitMaskContainsFlag(i, i2);
        }
    }

    static {
        try {
            Class<?> cls = Class.forName("com.mopub.mobileads.MoPubActivity");
            Class<?> cls2 = Class.forName("com.mopub.mobileads.MraidActivity");
            Class<?> cls3 = Class.forName("com.mopub.mobileads.RewardedMraidActivity");
            f20661.add(cls);
            f20661.add(cls2);
            f20661.add(cls3);
        } catch (ClassNotFoundException e) {
            MoPubLog.i("ManifestUtils running without interstitial module");
        }
        f20661.add(MraidVideoPlayerActivity.class);
        f20661.add(MoPubBrowser.class);
        f20659.add(MoPubBrowser.class);
    }

    private ManifestUtils() {
    }

    public static void checkNativeActivitiesDeclared(Context context) {
        if (Preconditions.NoThrow.checkNotNull(context, "context is not allowed to be null")) {
            m26600(context, f20659);
            m26594(context, f20659);
        }
    }

    public static void checkWebViewActivitiesDeclared(Context context) {
        if (Preconditions.NoThrow.checkNotNull(context, "context is not allowed to be null")) {
            m26600(context, f20661);
            m26594(context, f20661);
        }
    }

    public static boolean isDebuggable(Context context) {
        return Utils.bitMaskContainsFlag(context.getApplicationInfo().flags, 2);
    }

    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    static void m26594(Context context, List<Class<? extends Activity>> list) {
        List<Class<? extends Activity>> r1 = m26596(context, m26598(context, list, true));
        if (!r1.isEmpty()) {
            m26599(context);
            m26595(context, r1);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static void m26595(Context context, List<Class<? extends Activity>> list) {
        StringBuilder sb = new StringBuilder("In AndroidManifest, the android:configChanges param is missing values for the following MoPub activities:\n");
        for (Class next : list) {
            try {
                ActivityConfigChanges r1 = m26597(context, (Class<? extends Activity>) next);
                if (!r1.hasKeyboardHidden) {
                    sb.append("\n\tThe android:configChanges param for activity ").append(next.getName()).append(" must include keyboardHidden.");
                }
                if (!r1.hasOrientation) {
                    sb.append("\n\tThe android:configChanges param for activity ").append(next.getName()).append(" must include orientation.");
                }
                if (!r1.hasScreenSize) {
                    sb.append("\n\tThe android:configChanges param for activity ").append(next.getName()).append(" must include screenSize.");
                }
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        sb.append("\n\nPlease update your manifest to include them.");
        MoPubLog.w(sb.toString());
    }

    @TargetApi(13)
    /* renamed from: 齉  reason: contains not printable characters */
    private static List<Class<? extends Activity>> m26596(Context context, List<Class<? extends Activity>> list) {
        ArrayList arrayList = new ArrayList();
        for (Class next : list) {
            try {
                ActivityConfigChanges r1 = m26597(context, (Class<? extends Activity>) next);
                if (!r1.hasKeyboardHidden || !r1.hasOrientation || !r1.hasScreenSize) {
                    arrayList.add(next);
                }
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static ActivityConfigChanges m26597(Context context, Class<? extends Activity> cls) throws PackageManager.NameNotFoundException {
        ActivityInfo activityInfo = context.getPackageManager().getActivityInfo(new ComponentName(context, cls.getName()), 0);
        ActivityConfigChanges activityConfigChanges = new ActivityConfigChanges();
        activityConfigChanges.hasKeyboardHidden = f20660.hasFlag(cls, activityInfo.configChanges, 32);
        activityConfigChanges.hasOrientation = f20660.hasFlag(cls, activityInfo.configChanges, 128);
        activityConfigChanges.hasScreenSize = true;
        if (VersionCode.currentApiLevel().isAtLeast(VersionCode.HONEYCOMB_MR2) && context.getApplicationInfo().targetSdkVersion >= VersionCode.HONEYCOMB_MR2.getApiLevel()) {
            activityConfigChanges.hasScreenSize = f20660.hasFlag(cls, activityInfo.configChanges, 1024);
        }
        return activityConfigChanges;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<Class<? extends Activity>> m26598(Context context, List<Class<? extends Activity>> list, boolean z) {
        ArrayList arrayList = new ArrayList();
        for (Class next : list) {
            if (Intents.deviceCanHandleIntent(context, new Intent(context, next)) == z) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26599(Context context) {
        if (isDebuggable(context)) {
            Toast.makeText(context, "ERROR: YOUR MOPUB INTEGRATION IS INCOMPLETE.\nCheck logcat and update your AndroidManifest.xml with the correct activities and configuration.", 1).setGravity(7, 0, 0);
        }
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static void m26600(Context context, List<Class<? extends Activity>> list) {
        List<Class<? extends Activity>> r0 = m26598(context, list, false);
        if (!r0.isEmpty()) {
            m26599(context);
            m26601(r0);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26601(List<Class<? extends Activity>> list) {
        StringBuilder sb = new StringBuilder("AndroidManifest permissions for the following required MoPub activities are missing:\n");
        for (Class<? extends Activity> name : list) {
            sb.append("\n\t").append(name.getName());
        }
        sb.append("\n\nPlease update your manifest to include them.");
        MoPubLog.w(sb.toString());
    }
}
