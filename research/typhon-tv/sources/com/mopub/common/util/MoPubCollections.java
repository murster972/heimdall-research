package com.mopub.common.util;

import java.util.Collection;
import java.util.Collections;

public class MoPubCollections {
    @SafeVarargs
    public static <T> void addAllNonNull(Collection<? super T> collection, T... tArr) {
        Collections.addAll(collection, tArr);
        collection.removeAll(Collections.singleton((Object) null));
    }
}
