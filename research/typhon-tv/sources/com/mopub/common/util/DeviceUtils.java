package com.mopub.common.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.os.StatFs;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.WindowManager;
import com.mopub.common.CreativeOrientation;
import com.mopub.common.Preconditions;
import com.mopub.common.util.Reflection;
import com.typhoon.tv.Logger;
import java.io.File;
import java.net.SocketException;

public class DeviceUtils {

    @Deprecated
    public enum IP {
        IPv4,
        IPv6
    }

    public enum ForceOrientation {
        FORCE_PORTRAIT("portrait"),
        FORCE_LANDSCAPE("landscape"),
        DEVICE_ORIENTATION("device"),
        UNDEFINED("");
        
        private final String mKey;

        private ForceOrientation(String str) {
            this.mKey = str;
        }

        public static ForceOrientation getForceOrientation(String str) {
            for (ForceOrientation forceOrientation : values()) {
                if (forceOrientation.mKey.equalsIgnoreCase(str)) {
                    return forceOrientation;
                }
            }
            return UNDEFINED;
        }
    }

    private DeviceUtils() {
    }

    public static long diskCacheSizeBytes(File file) {
        return diskCacheSizeBytes(file, 31457280);
    }

    public static long diskCacheSizeBytes(File file, long j) {
        long j2 = j;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            j2 = (((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 50;
        } catch (IllegalArgumentException e) {
        }
        return Math.max(Math.min(j2, 104857600), 31457280);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.Integer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.Integer} */
    /* JADX WARNING: Multi-variable type inference failed */
    @android.annotation.TargetApi(17)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Point getDeviceDimensions(android.content.Context r11) {
        /*
            r2 = 0
            r1 = 0
            java.lang.String r8 = "window"
            java.lang.Object r7 = r11.getSystemService(r8)
            android.view.WindowManager r7 = (android.view.WindowManager) r7
            android.view.Display r3 = r7.getDefaultDisplay()
            int r8 = android.os.Build.VERSION.SDK_INT
            r9 = 17
            if (r8 < r9) goto L_0x004f
            android.graphics.Point r6 = new android.graphics.Point
            r6.<init>()
            r3.getRealSize(r6)
            int r8 = r6.x
            java.lang.Integer r2 = java.lang.Integer.valueOf(r8)
            int r8 = r6.y
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)
        L_0x0029:
            if (r2 == 0) goto L_0x002d
            if (r1 != 0) goto L_0x0041
        L_0x002d:
            android.content.res.Resources r8 = r11.getResources()
            android.util.DisplayMetrics r4 = r8.getDisplayMetrics()
            int r8 = r4.widthPixels
            java.lang.Integer r2 = java.lang.Integer.valueOf(r8)
            int r8 = r4.heightPixels
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)
        L_0x0041:
            android.graphics.Point r8 = new android.graphics.Point
            int r9 = r2.intValue()
            int r10 = r1.intValue()
            r8.<init>(r9, r10)
            return r8
        L_0x004f:
            com.mopub.common.util.Reflection$MethodBuilder r8 = new com.mopub.common.util.Reflection$MethodBuilder     // Catch:{ Exception -> 0x0070 }
            java.lang.String r9 = "getRawWidth"
            r8.<init>(r3, r9)     // Catch:{ Exception -> 0x0070 }
            java.lang.Object r8 = r8.execute()     // Catch:{ Exception -> 0x0070 }
            r0 = r8
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0070 }
            r2 = r0
            com.mopub.common.util.Reflection$MethodBuilder r8 = new com.mopub.common.util.Reflection$MethodBuilder     // Catch:{ Exception -> 0x0070 }
            java.lang.String r9 = "getRawHeight"
            r8.<init>(r3, r9)     // Catch:{ Exception -> 0x0070 }
            java.lang.Object r8 = r8.execute()     // Catch:{ Exception -> 0x0070 }
            r0 = r8
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0070 }
            r1 = r0
            goto L_0x0029
        L_0x0070:
            r5 = move-exception
            java.lang.String r8 = "Display#getRawWidth/Height failed."
            com.mopub.common.logging.MoPubLog.v(r8, r5)
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.common.util.DeviceUtils.getDeviceDimensions(android.content.Context):android.graphics.Point");
    }

    @Deprecated
    public static String getHashedUdid(Context context) {
        return null;
    }

    @Deprecated
    public static String getIpAddress(IP ip) throws SocketException {
        return null;
    }

    public static int getScreenOrientation(Activity activity) {
        return m6176(activity.getWindowManager().getDefaultDisplay().getRotation(), activity.getResources().getConfiguration().orientation);
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null || !isPermissionGranted(context, "android.permission.INTERNET")) {
            return false;
        }
        if (!isPermissionGranted(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return true;
        }
        try {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo().isConnected();
        } catch (NullPointerException e) {
            return false;
        }
    }

    public static boolean isPermissionGranted(Context context, String str) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(str);
        try {
            return ContextCompat.checkSelfPermission(context, str) == 0;
        } catch (Exception e) {
            Logger.m6281((Throwable) e, new boolean[0]);
            return false;
        }
    }

    public static void lockOrientation(Activity activity, CreativeOrientation creativeOrientation) {
        int i;
        if (Preconditions.NoThrow.checkNotNull(creativeOrientation) && Preconditions.NoThrow.checkNotNull(activity)) {
            int r0 = m6176(((WindowManager) activity.getSystemService("window")).getDefaultDisplay().getRotation(), activity.getResources().getConfiguration().orientation);
            if (CreativeOrientation.PORTRAIT == creativeOrientation) {
                i = 9 == r0 ? 9 : 1;
            } else if (CreativeOrientation.LANDSCAPE == creativeOrientation) {
                i = 8 == r0 ? 8 : 0;
            } else {
                return;
            }
            activity.setRequestedOrientation(i);
        }
    }

    public static int memoryCacheSizeBytes(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        long memoryClass = (long) activityManager.getMemoryClass();
        if (VersionCode.currentApiLevel().isAtLeast(VersionCode.HONEYCOMB)) {
            try {
                if (Utils.bitMaskContainsFlag(context.getApplicationInfo().flags, ApplicationInfo.class.getDeclaredField("FLAG_LARGE_HEAP").getInt((Object) null))) {
                    memoryClass = (long) ((Integer) new Reflection.MethodBuilder(activityManager, "getLargeMemoryClass").execute()).intValue();
                }
            } catch (Exception e) {
            }
        }
        return (int) Math.min(31457280, (memoryClass / 8) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m6176(int i, int i2) {
        if (1 == i2) {
            switch (i) {
                case 1:
                case 2:
                    return 9;
                default:
                    return 1;
            }
        } else if (2 != i2) {
            return 9;
        } else {
            switch (i) {
                case 2:
                case 3:
                    return 8;
                default:
                    return 0;
            }
        }
    }
}
