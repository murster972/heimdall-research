package com.mopub.common.util;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import java.util.concurrent.Executor;

public class AsyncTasks {

    /* renamed from: 龘  reason: contains not printable characters */
    private static Executor f5694;

    static {
        m6175();
    }

    @TargetApi(11)
    @SafeVarargs
    public static <P> void safeExecuteOnExecutor(AsyncTask<P, ?, ?> asyncTask, P... pArr) {
        Preconditions.checkNotNull(asyncTask, "Unable to execute null AsyncTask.");
        Preconditions.checkUiThread("AsyncTask must be executed on the main thread");
        asyncTask.executeOnExecutor(f5694, pArr);
    }

    @VisibleForTesting
    public static void setExecutor(Executor executor) {
        f5694 = executor;
    }

    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    private static void m6175() {
        f5694 = AsyncTask.THREAD_POOL_EXECUTOR;
    }
}
