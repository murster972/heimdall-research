package com.mopub.common.util;

import com.mopub.common.Preconditions;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Reflection {

    public static class MethodBuilder {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f20662;

        /* renamed from: ʼ  reason: contains not printable characters */
        private boolean f20663;

        /* renamed from: 连任  reason: contains not printable characters */
        private List<Object> f20664 = new ArrayList();

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f20665;

        /* renamed from: 麤  reason: contains not printable characters */
        private List<Class<?>> f20666 = new ArrayList();

        /* renamed from: 齉  reason: contains not printable characters */
        private Class<?> f20667;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f20668;

        public MethodBuilder(Object obj, String str) {
            this.f20668 = obj;
            this.f20665 = str;
            this.f20667 = obj != null ? obj.getClass() : null;
        }

        public <T> MethodBuilder addParam(Class<T> cls, T t) {
            this.f20666.add(cls);
            this.f20664.add(t);
            return this;
        }

        public Object execute() throws Exception {
            Method declaredMethodWithTraversal = Reflection.getDeclaredMethodWithTraversal(this.f20667, this.f20665, (Class[]) this.f20666.toArray(new Class[this.f20666.size()]));
            if (this.f20662) {
                declaredMethodWithTraversal.setAccessible(true);
            }
            Object[] array = this.f20664.toArray();
            return this.f20663 ? declaredMethodWithTraversal.invoke((Object) null, array) : declaredMethodWithTraversal.invoke(this.f20668, array);
        }

        public MethodBuilder setAccessible() {
            this.f20662 = true;
            return this;
        }

        public MethodBuilder setStatic(Class<?> cls) {
            this.f20663 = true;
            this.f20667 = cls;
            return this;
        }
    }

    public static boolean classFound(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public static Method getDeclaredMethodWithTraversal(Class<?> cls, String str, Class<?>... clsArr) throws NoSuchMethodException {
        Class<?> cls2 = cls;
        while (cls2 != null) {
            try {
                return cls2.getDeclaredMethod(str, clsArr);
            } catch (NoSuchMethodException e) {
                cls2 = cls2.getSuperclass();
            }
        }
        throw new NoSuchMethodException();
    }

    public static <T> T instantiateClassWithConstructor(String str, Class<? extends T> cls, Class[] clsArr, Object[] objArr) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Preconditions.checkNotNull(str);
        Preconditions.checkNotNull(cls);
        Preconditions.checkNotNull(clsArr);
        Preconditions.checkNotNull(objArr);
        Constructor<? extends U> declaredConstructor = Class.forName(str).asSubclass(cls).getDeclaredConstructor(clsArr);
        declaredConstructor.setAccessible(true);
        return declaredConstructor.newInstance(objArr);
    }

    public static <T> T instantiateClassWithEmptyConstructor(String str, Class<? extends T> cls) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NullPointerException {
        Preconditions.checkNotNull(str);
        Constructor<? extends U> declaredConstructor = Class.forName(str).asSubclass(cls).getDeclaredConstructor((Class[]) null);
        declaredConstructor.setAccessible(true);
        return declaredConstructor.newInstance(new Object[0]);
    }
}
