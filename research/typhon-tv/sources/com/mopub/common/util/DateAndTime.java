package com.mopub.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateAndTime {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static DateAndTime f5695 = new DateAndTime();

    public static String getTimeZoneOffsetString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("Z", Locale.US);
        simpleDateFormat.setTimeZone(localTimeZone());
        return simpleDateFormat.format(now());
    }

    public static TimeZone localTimeZone() {
        return f5695.internalLocalTimeZone();
    }

    public static Date now() {
        return f5695.internalNow();
    }

    @Deprecated
    public static void setInstance(DateAndTime dateAndTime) {
        f5695 = dateAndTime;
    }

    public TimeZone internalLocalTimeZone() {
        return TimeZone.getDefault();
    }

    public Date internalNow() {
        return new Date();
    }
}
