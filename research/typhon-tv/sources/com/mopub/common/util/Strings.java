package com.mopub.common.util;

import android.text.TextUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

public class Strings {

    /* renamed from: 靐  reason: contains not printable characters */
    private static Pattern f20669 = Pattern.compile("\\d{2}:\\d{2}:\\d{2}(.\\d{3})?");

    /* renamed from: 龘  reason: contains not printable characters */
    private static Pattern f20670 = Pattern.compile("((\\d{1,2})|(100))%");

    public static String fromStream(InputStream inputStream) throws IOException {
        int i = 0;
        StringBuilder sb = new StringBuilder();
        byte[] bArr = new byte[4096];
        while (i != -1) {
            sb.append(new String(bArr, 0, i));
            i = inputStream.read(bArr);
        }
        inputStream.close();
        return sb.toString();
    }

    public static boolean isAbsoluteTracker(String str) {
        return !TextUtils.isEmpty(str) && f20669.matcher(str).matches();
    }

    public static boolean isPercentageTracker(String str) {
        return !TextUtils.isEmpty(str) && f20670.matcher(str).matches();
    }

    public static Integer parseAbsoluteOffset(String str) {
        if (str == null) {
            return null;
        }
        String[] split = str.split(":");
        if (split.length == 3) {
            return Integer.valueOf((Integer.parseInt(split[0]) * 60 * 60 * 1000) + (Integer.parseInt(split[1]) * 60 * 1000) + ((int) (Float.parseFloat(split[2]) * 1000.0f)));
        }
        return null;
    }
}
