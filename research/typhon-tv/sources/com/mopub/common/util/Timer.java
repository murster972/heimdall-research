package com.mopub.common.util;

import java.util.concurrent.TimeUnit;

public class Timer {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f20671;

    /* renamed from: 齉  reason: contains not printable characters */
    private State f20672 = State.STOPPED;

    /* renamed from: 龘  reason: contains not printable characters */
    private long f20673;

    private enum State {
        STARTED,
        STOPPED
    }

    public long getTime() {
        return TimeUnit.MILLISECONDS.convert((this.f20672 == State.STARTED ? System.nanoTime() : this.f20673) - this.f20671, TimeUnit.NANOSECONDS);
    }

    public void start() {
        this.f20671 = System.nanoTime();
        this.f20672 = State.STARTED;
    }

    public void stop() {
        if (this.f20672 != State.STARTED) {
            throw new IllegalStateException("EventTimer was not started.");
        }
        this.f20672 = State.STOPPED;
        this.f20673 = System.nanoTime();
    }
}
