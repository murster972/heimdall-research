package com.mopub.common.util;

import com.mopub.common.VisibleForTesting;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class Network {
    public static String getRedirectLocation(String str) throws IOException, URISyntaxException {
        return getRedirectLocation(str, false, (Map<String, String>) null);
    }

    /* JADX WARNING: type inference failed for: r6v1, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getRedirectLocation(java.lang.String r9, boolean r10, java.util.Map<java.lang.String, java.lang.String> r11) throws java.io.IOException, java.net.URISyntaxException {
        /*
            java.net.URL r5 = new java.net.URL
            r5.<init>(r9)
            r3 = 0
            java.net.URLConnection r6 = r5.openConnection()     // Catch:{ all -> 0x004c }
            r0 = r6
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x004c }
            r3 = r0
            r6 = 45000(0xafc8, float:6.3058E-41)
            r3.setConnectTimeout(r6)     // Catch:{ all -> 0x004c }
            r6 = 45000(0xafc8, float:6.3058E-41)
            r3.setReadTimeout(r6)     // Catch:{ all -> 0x004c }
            r6 = 0
            r3.setInstanceFollowRedirects(r6)     // Catch:{ all -> 0x004c }
            if (r10 == 0) goto L_0x0026
            java.lang.String r6 = "HEAD"
            r3.setRequestMethod(r6)     // Catch:{ all -> 0x004c }
        L_0x0026:
            if (r11 == 0) goto L_0x005d
            java.util.Set r6 = r11.entrySet()     // Catch:{ all -> 0x004c }
            java.util.Iterator r8 = r6.iterator()     // Catch:{ all -> 0x004c }
        L_0x0030:
            boolean r6 = r8.hasNext()     // Catch:{ all -> 0x004c }
            if (r6 == 0) goto L_0x005d
            java.lang.Object r2 = r8.next()     // Catch:{ all -> 0x004c }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x004c }
            java.lang.Object r6 = r2.getKey()     // Catch:{ all -> 0x004c }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x004c }
            java.lang.Object r7 = r2.getValue()     // Catch:{ all -> 0x004c }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x004c }
            r3.addRequestProperty(r6, r7)     // Catch:{ all -> 0x004c }
            goto L_0x0030
        L_0x004c:
            r6 = move-exception
            if (r3 == 0) goto L_0x005c
            r4 = 0
            java.io.InputStream r4 = r3.getInputStream()     // Catch:{ Exception -> 0x0079 }
        L_0x0054:
            if (r4 == 0) goto L_0x0059
            r4.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0059:
            r3.disconnect()
        L_0x005c:
            throw r6
        L_0x005d:
            java.lang.String r6 = resolveRedirectLocation(r9, r3)     // Catch:{ all -> 0x004c }
            if (r3 == 0) goto L_0x0070
            r4 = 0
            java.io.InputStream r4 = r3.getInputStream()     // Catch:{ Exception -> 0x0071 }
        L_0x0068:
            if (r4 == 0) goto L_0x006d
            r4.close()     // Catch:{ IOException -> 0x0081 }
        L_0x006d:
            r3.disconnect()
        L_0x0070:
            return r6
        L_0x0071:
            r1 = move-exception
            java.lang.String r7 = "Exception when getting input stream from httpUrlConnection."
            com.mopub.common.logging.MoPubLog.e(r7)
            goto L_0x0068
        L_0x0079:
            r1 = move-exception
            java.lang.String r7 = "Exception when getting input stream from httpUrlConnection."
            com.mopub.common.logging.MoPubLog.e(r7)
            goto L_0x0054
        L_0x0081:
            r7 = move-exception
            goto L_0x006d
        L_0x0083:
            r7 = move-exception
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.common.util.Network.getRedirectLocation(java.lang.String, boolean, java.util.Map):java.lang.String");
    }

    @VisibleForTesting
    public static String resolveRedirectLocation(String str, HttpURLConnection httpURLConnection) throws IOException, URISyntaxException {
        URI uri = new URI(str.replace(StringUtils.LF, "").replace(StringUtils.CR, ""));
        int responseCode = httpURLConnection.getResponseCode();
        String headerField = httpURLConnection.getHeaderField("Location");
        if (responseCode < 300 || responseCode >= 400) {
            return null;
        }
        try {
            return uri.resolve(headerField).toString();
        } catch (IllegalArgumentException e) {
            throw new URISyntaxException(headerField, "Unable to parse invalid URL");
        }
    }
}
