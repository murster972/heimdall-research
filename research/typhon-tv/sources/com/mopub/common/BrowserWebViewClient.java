package com.mopub.common;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.common.UrlHandler;
import com.mopub.common.util.Drawables;
import java.util.EnumSet;

class BrowserWebViewClient extends WebViewClient {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final EnumSet<UrlAction> f20445 = EnumSet.of(UrlAction.HANDLE_PHONE_SCHEME, new UrlAction[]{UrlAction.OPEN_APP_MARKET, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK});
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public MoPubBrowser f20446;

    public BrowserWebViewClient(MoPubBrowser moPubBrowser) {
        this.f20446 = moPubBrowser;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.f20446.getBackButton().setImageDrawable(webView.canGoBack() ? Drawables.LEFT_ARROW.createDrawable(this.f20446) : Drawables.UNLEFT_ARROW.createDrawable(this.f20446));
        this.f20446.getForwardButton().setImageDrawable(webView.canGoForward() ? Drawables.RIGHT_ARROW.createDrawable(this.f20446) : Drawables.UNRIGHT_ARROW.createDrawable(this.f20446));
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.f20446.getForwardButton().setImageDrawable(Drawables.UNRIGHT_ARROW.createDrawable(this.f20446));
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return new UrlHandler.Builder().withSupportedUrlActions(f20445).withoutMoPubBrowser().withResultActions(new UrlHandler.ResultActions() {
            public void urlHandlingFailed(String str, UrlAction urlAction) {
            }

            public void urlHandlingSucceeded(String str, UrlAction urlAction) {
                if (urlAction.equals(UrlAction.OPEN_IN_APP_BROWSER)) {
                    BrowserWebViewClient.this.f20446.getWebView().loadUrl(str);
                } else {
                    BrowserWebViewClient.this.f20446.finish();
                }
            }
        }).build().handleResolvedUrl(this.f20446.getApplicationContext(), str, true, (Iterable<String>) null);
    }
}
