package com.mopub.common;

public enum CreativeOrientation {
    PORTRAIT,
    LANDSCAPE,
    UNDEFINED;

    public static CreativeOrientation fromHeader(String str) {
        return "l".equalsIgnoreCase(str) ? LANDSCAPE : TtmlNode.TAG_P.equalsIgnoreCase(str) ? PORTRAIT : UNDEFINED;
    }
}
