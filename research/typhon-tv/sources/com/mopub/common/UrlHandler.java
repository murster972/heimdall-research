package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.mopub.common.UrlResolutionTask;
import com.mopub.common.event.BaseEvent;
import com.mopub.exceptions.IntentNotResolvableException;
import com.mopub.network.TrackingRequest;
import java.util.EnumSet;
import java.util.Iterator;

public class UrlHandler {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final MoPubSchemeListener f20544 = new MoPubSchemeListener() {
        public void onClose() {
        }

        public void onFailLoad() {
        }

        public void onFinishLoad() {
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ResultActions f20545 = new ResultActions() {
        public void urlHandlingFailed(String str, UrlAction urlAction) {
        }

        public void urlHandlingSucceeded(String str, UrlAction urlAction) {
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f20546;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f20547;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f20548;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean f20549;

    /* renamed from: 连任  reason: contains not printable characters */
    private MoPubSchemeListener f20550;

    /* renamed from: 麤  reason: contains not printable characters */
    private ResultActions f20551;

    /* renamed from: 齉  reason: contains not printable characters */
    private EnumSet<UrlAction> f20552;

    public static class Builder {

        /* renamed from: 连任  reason: contains not printable characters */
        private String f20558;

        /* renamed from: 靐  reason: contains not printable characters */
        private ResultActions f20559 = UrlHandler.f20545;

        /* renamed from: 麤  reason: contains not printable characters */
        private boolean f20560 = false;

        /* renamed from: 齉  reason: contains not printable characters */
        private MoPubSchemeListener f20561 = UrlHandler.f20544;

        /* renamed from: 龘  reason: contains not printable characters */
        private EnumSet<UrlAction> f20562 = EnumSet.of(UrlAction.NOOP);

        public UrlHandler build() {
            return new UrlHandler(this.f20562, this.f20559, this.f20561, this.f20560, this.f20558);
        }

        public Builder withDspCreativeId(String str) {
            this.f20558 = str;
            return this;
        }

        public Builder withMoPubSchemeListener(MoPubSchemeListener moPubSchemeListener) {
            this.f20561 = moPubSchemeListener;
            return this;
        }

        public Builder withResultActions(ResultActions resultActions) {
            this.f20559 = resultActions;
            return this;
        }

        public Builder withSupportedUrlActions(UrlAction urlAction, UrlAction... urlActionArr) {
            this.f20562 = EnumSet.of(urlAction, urlActionArr);
            return this;
        }

        public Builder withSupportedUrlActions(EnumSet<UrlAction> enumSet) {
            this.f20562 = EnumSet.copyOf(enumSet);
            return this;
        }

        public Builder withoutMoPubBrowser() {
            this.f20560 = true;
            return this;
        }
    }

    public interface MoPubSchemeListener {
        void onClose();

        void onFailLoad();

        void onFinishLoad();
    }

    public interface ResultActions {
        void urlHandlingFailed(String str, UrlAction urlAction);

        void urlHandlingSucceeded(String str, UrlAction urlAction);
    }

    private UrlHandler(EnumSet<UrlAction> enumSet, ResultActions resultActions, MoPubSchemeListener moPubSchemeListener, boolean z, String str) {
        this.f20552 = EnumSet.copyOf(enumSet);
        this.f20551 = resultActions;
        this.f20550 = moPubSchemeListener;
        this.f20547 = z;
        this.f20546 = str;
        this.f20548 = false;
        this.f20549 = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26550(String str, UrlAction urlAction, String str2, Throwable th) {
        Preconditions.checkNotNull(str2);
        if (urlAction == null) {
            urlAction = UrlAction.NOOP;
        }
        this.f20551.urlHandlingFailed(str, urlAction);
    }

    public boolean handleResolvedUrl(Context context, String str, boolean z, Iterable<String> iterable) {
        if (TextUtils.isEmpty(str)) {
            m26550(str, (UrlAction) null, "Attempted to handle empty url.", (Throwable) null);
            return false;
        }
        UrlAction urlAction = UrlAction.NOOP;
        Uri parse = Uri.parse(str);
        Iterator it2 = this.f20552.iterator();
        while (it2.hasNext()) {
            UrlAction urlAction2 = (UrlAction) it2.next();
            if (urlAction2.shouldTryHandlingUrl(parse)) {
                try {
                    urlAction2.handleUrl(this, context, parse, z, this.f20546);
                    if (!this.f20548 && !this.f20549 && !UrlAction.IGNORE_ABOUT_SCHEME.equals(urlAction2) && !UrlAction.HANDLE_MOPUB_SCHEME.equals(urlAction2)) {
                        TrackingRequest.makeTrackingHttpRequest(iterable, context, BaseEvent.Name.CLICK_REQUEST);
                        this.f20551.urlHandlingSucceeded(parse.toString(), urlAction2);
                        this.f20548 = true;
                    }
                    return true;
                } catch (IntentNotResolvableException e) {
                    urlAction = urlAction2;
                }
            }
        }
        m26550(str, urlAction, "Link ignored. Unable to handle url: " + str, (Throwable) null);
        return false;
    }

    public void handleUrl(Context context, String str) {
        Preconditions.checkNotNull(context);
        handleUrl(context, str, true);
    }

    public void handleUrl(Context context, String str, boolean z) {
        Preconditions.checkNotNull(context);
        handleUrl(context, str, z, (Iterable<String>) null);
    }

    public void handleUrl(Context context, String str, boolean z, Iterable<String> iterable) {
        Preconditions.checkNotNull(context);
        if (TextUtils.isEmpty(str)) {
            m26550(str, (UrlAction) null, "Attempted to handle empty url.", (Throwable) null);
            return;
        }
        final Context context2 = context;
        final boolean z2 = z;
        final Iterable<String> iterable2 = iterable;
        final String str2 = str;
        UrlResolutionTask.getResolvedUrl(str, new UrlResolutionTask.UrlResolutionListener() {
            public void onFailure(String str, Throwable th) {
                boolean unused = UrlHandler.this.f20549 = false;
                UrlHandler.this.m26550(str2, (UrlAction) null, str, th);
            }

            public void onSuccess(String str) {
                boolean unused = UrlHandler.this.f20549 = false;
                UrlHandler.this.handleResolvedUrl(context2, str, z2, iterable2);
            }
        });
        this.f20549 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m26552() {
        return this.f20547;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public MoPubSchemeListener m26553() {
        return this.f20550;
    }
}
