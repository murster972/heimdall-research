package com.mopub.common;

import android.app.Activity;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

public class MoPubLifecycleManager implements LifecycleListener {

    /* renamed from: 龘  reason: contains not printable characters */
    private static MoPubLifecycleManager f20539;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Set<LifecycleListener> f20540 = new HashSet();

    /* renamed from: 齉  reason: contains not printable characters */
    private final WeakReference<Activity> f20541;

    private MoPubLifecycleManager(Activity activity) {
        this.f20541 = new WeakReference<>(activity);
    }

    public static synchronized MoPubLifecycleManager getInstance(Activity activity) {
        MoPubLifecycleManager moPubLifecycleManager;
        synchronized (MoPubLifecycleManager.class) {
            if (f20539 == null) {
                f20539 = new MoPubLifecycleManager(activity);
            }
            moPubLifecycleManager = f20539;
        }
        return moPubLifecycleManager;
    }

    public void addLifecycleListener(LifecycleListener lifecycleListener) {
        Activity activity;
        if (lifecycleListener != null && this.f20540.add(lifecycleListener) && (activity = (Activity) this.f20541.get()) != null) {
            lifecycleListener.onCreate(activity);
            lifecycleListener.onStart(activity);
        }
    }

    public void onBackPressed(Activity activity) {
        for (LifecycleListener onBackPressed : this.f20540) {
            onBackPressed.onBackPressed(activity);
        }
    }

    public void onCreate(Activity activity) {
        for (LifecycleListener onCreate : this.f20540) {
            onCreate.onCreate(activity);
        }
    }

    public void onDestroy(Activity activity) {
        for (LifecycleListener onDestroy : this.f20540) {
            onDestroy.onDestroy(activity);
        }
    }

    public void onPause(Activity activity) {
        for (LifecycleListener onPause : this.f20540) {
            onPause.onPause(activity);
        }
    }

    public void onRestart(Activity activity) {
        for (LifecycleListener onRestart : this.f20540) {
            onRestart.onRestart(activity);
        }
    }

    public void onResume(Activity activity) {
        for (LifecycleListener onResume : this.f20540) {
            onResume.onResume(activity);
        }
    }

    public void onStart(Activity activity) {
        for (LifecycleListener onStart : this.f20540) {
            onStart.onStart(activity);
        }
    }

    public void onStop(Activity activity) {
        for (LifecycleListener onStop : this.f20540) {
            onStop.onStop(activity);
        }
    }
}
