package com.mopub.common.event;

import com.mopub.common.Preconditions;
import com.mopub.common.util.Json;
import java.util.HashMap;
import java.util.Map;

public class EventDetails {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<String, String> f20635;

    public static class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, String> f20636 = new HashMap();

        public Builder adHeightPx(Integer num) {
            if (num != null) {
                this.f20636.put("ad_height_px_key", String.valueOf(num));
            }
            return this;
        }

        public Builder adNetworkType(String str) {
            if (str != null) {
                this.f20636.put("ad_network_type", str);
            }
            return this;
        }

        public Builder adType(String str) {
            if (str != null) {
                this.f20636.put("ad_type", str);
            }
            return this;
        }

        public Builder adUnitId(String str) {
            if (str != null) {
                this.f20636.put("ad_unit_id", str);
            }
            return this;
        }

        public Builder adWidthPx(Integer num) {
            if (num != null) {
                this.f20636.put("ad_width_px", String.valueOf(num));
            }
            return this;
        }

        public EventDetails build() {
            return new EventDetails(this.f20636);
        }

        public Builder dspCreativeId(String str) {
            if (str != null) {
                this.f20636.put("dsp_creative_id", str);
            }
            return this;
        }

        public Builder geoAccuracy(Float f) {
            if (f != null) {
                this.f20636.put("geo_accuracy_key", String.valueOf((double) f.floatValue()));
            }
            return this;
        }

        public Builder geoLatitude(Double d) {
            if (d != null) {
                this.f20636.put("geo_latitude", String.valueOf(d));
            }
            return this;
        }

        public Builder geoLongitude(Double d) {
            if (d != null) {
                this.f20636.put("geo_longitude", String.valueOf(d));
            }
            return this;
        }

        public Builder performanceDurationMs(Long l) {
            if (l != null) {
                this.f20636.put("performance_duration_ms", String.valueOf((double) l.longValue()));
            }
            return this;
        }

        public Builder requestId(String str) {
            if (str != null) {
                this.f20636.put("request_id_key", str);
            }
            return this;
        }

        public Builder requestStatusCode(Integer num) {
            if (num != null) {
                this.f20636.put("request_status_code", String.valueOf(num));
            }
            return this;
        }

        public Builder requestUri(String str) {
            if (str != null) {
                this.f20636.put("request_uri_key", str);
            }
            return this;
        }
    }

    private EventDetails(Map<String, String> map) {
        Preconditions.checkNotNull(map);
        this.f20635 = map;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static Integer m26583(Map<String, String> map, String str) {
        String str2 = map.get(str);
        if (str2 == null) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(str2));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Double m26584(Map<String, String> map, String str) {
        String str2 = map.get(str);
        if (str2 == null) {
            return null;
        }
        try {
            return Double.valueOf(Double.parseDouble(str2));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public Double getAdHeightPx() {
        return m26584(this.f20635, "ad_height_px_key");
    }

    public String getAdNetworkType() {
        return this.f20635.get("ad_network_type");
    }

    public String getAdType() {
        return this.f20635.get("ad_type");
    }

    public String getAdUnitId() {
        return this.f20635.get("ad_unit_id");
    }

    public Double getAdWidthPx() {
        return m26584(this.f20635, "ad_width_px");
    }

    public String getDspCreativeId() {
        return this.f20635.get("dsp_creative_id");
    }

    public Double getGeoAccuracy() {
        return m26584(this.f20635, "geo_accuracy_key");
    }

    public Double getGeoLatitude() {
        return m26584(this.f20635, "geo_latitude");
    }

    public Double getGeoLongitude() {
        return m26584(this.f20635, "geo_longitude");
    }

    public Double getPerformanceDurationMs() {
        return m26584(this.f20635, "performance_duration_ms");
    }

    public String getRequestId() {
        return this.f20635.get("request_id_key");
    }

    public Integer getRequestStatusCode() {
        return m26583(this.f20635, "request_status_code");
    }

    public String getRequestUri() {
        return this.f20635.get("request_uri_key");
    }

    public String toJsonString() {
        return Json.mapToJsonString(this.f20635);
    }

    public String toString() {
        return toJsonString();
    }
}
