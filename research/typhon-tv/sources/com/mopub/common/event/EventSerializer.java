package com.mopub.common.event;

import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;
import com.mopub.common.event.BaseEvent;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EventSerializer {
    public JSONArray serializeAsJson(List<BaseEvent> list) {
        Preconditions.checkNotNull(list);
        JSONArray jSONArray = new JSONArray();
        for (BaseEvent serializeAsJson : list) {
            try {
                jSONArray.put((Object) serializeAsJson(serializeAsJson));
            } catch (JSONException e) {
            }
        }
        return jSONArray;
    }

    public JSONObject serializeAsJson(BaseEvent baseEvent) throws JSONException {
        Integer num = null;
        Preconditions.checkNotNull(baseEvent);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("_category_", (Object) baseEvent.getScribeCategory().getCategory());
        jSONObject.put("ts", (Object) baseEvent.getTimestampUtcMs());
        jSONObject.put("name", (Object) baseEvent.getName().getName());
        jSONObject.put("name_category", (Object) baseEvent.getCategory().getCategory());
        BaseEvent.SdkProduct sdkProduct = baseEvent.getSdkProduct();
        jSONObject.put("sdk_product", (Object) sdkProduct == null ? null : Integer.valueOf(sdkProduct.getType()));
        jSONObject.put("sdk_version", (Object) baseEvent.getSdkVersion());
        jSONObject.put("ad_unit_id", (Object) baseEvent.getAdUnitId());
        jSONObject.put("ad_creative_id", (Object) baseEvent.getAdCreativeId());
        jSONObject.put("ad_type", (Object) baseEvent.getAdType());
        jSONObject.put("ad_network_type", (Object) baseEvent.getAdNetworkType());
        jSONObject.put("ad_width_px", (Object) baseEvent.getAdWidthPx());
        jSONObject.put("ad_height_px", (Object) baseEvent.getAdHeightPx());
        jSONObject.put("dsp_creative_id", (Object) baseEvent.getDspCreativeId());
        BaseEvent.AppPlatform appPlatform = baseEvent.getAppPlatform();
        jSONObject.put("app_platform", (Object) appPlatform == null ? null : Integer.valueOf(appPlatform.getType()));
        jSONObject.put("app_name", (Object) baseEvent.getAppName());
        jSONObject.put("app_package_name", (Object) baseEvent.getAppPackageName());
        jSONObject.put("app_version", (Object) baseEvent.getAppVersion());
        jSONObject.put("client_advertising_id", (Object) baseEvent.getObfuscatedClientAdvertisingId());
        jSONObject.put("client_do_not_track", (Object) baseEvent.getClientDoNotTrack());
        jSONObject.put("device_manufacturer", (Object) baseEvent.getDeviceManufacturer());
        jSONObject.put("device_model", (Object) baseEvent.getDeviceModel());
        jSONObject.put("device_product", (Object) baseEvent.getDeviceProduct());
        jSONObject.put("device_os_version", (Object) baseEvent.getDeviceOsVersion());
        jSONObject.put("device_screen_width_px", (Object) baseEvent.getDeviceScreenWidthDip());
        jSONObject.put("device_screen_height_px", (Object) baseEvent.getDeviceScreenHeightDip());
        jSONObject.put("geo_lat", (Object) baseEvent.getGeoLat());
        jSONObject.put("geo_lon", (Object) baseEvent.getGeoLon());
        jSONObject.put("geo_accuracy_radius_meters", (Object) baseEvent.getGeoAccuracy());
        jSONObject.put("perf_duration_ms", (Object) baseEvent.getPerformanceDurationMs());
        ClientMetadata.MoPubNetworkType networkType = baseEvent.getNetworkType();
        if (networkType != null) {
            num = Integer.valueOf(networkType.getId());
        }
        jSONObject.put("network_type", (Object) num);
        jSONObject.put("network_operator_code", (Object) baseEvent.getNetworkOperatorCode());
        jSONObject.put("network_operator_name", (Object) baseEvent.getNetworkOperatorName());
        jSONObject.put("network_iso_country_code", (Object) baseEvent.getNetworkIsoCountryCode());
        jSONObject.put("network_sim_code", (Object) baseEvent.getNetworkSimCode());
        jSONObject.put("network_sim_operator_name", (Object) baseEvent.getNetworkSimOperatorName());
        jSONObject.put("network_sim_iso_country_code", (Object) baseEvent.getNetworkSimIsoCountryCode());
        jSONObject.put("req_id", (Object) baseEvent.getRequestId());
        jSONObject.put("req_status_code", (Object) baseEvent.getRequestStatusCode());
        jSONObject.put("req_uri", (Object) baseEvent.getRequestUri());
        jSONObject.put("req_retries", (Object) baseEvent.getRequestRetries());
        jSONObject.put("timestamp_client", (Object) baseEvent.getTimestampUtcMs());
        if (baseEvent instanceof ErrorEvent) {
            ErrorEvent errorEvent = (ErrorEvent) baseEvent;
            jSONObject.put("error_exception_class_name", (Object) errorEvent.getErrorExceptionClassName());
            jSONObject.put("error_message", (Object) errorEvent.getErrorMessage());
            jSONObject.put("error_stack_trace", (Object) errorEvent.getErrorStackTrace());
            jSONObject.put("error_file_name", (Object) errorEvent.getErrorFileName());
            jSONObject.put("error_class_name", (Object) errorEvent.getErrorClassName());
            jSONObject.put("error_method_name", (Object) errorEvent.getErrorMethodName());
            jSONObject.put("error_line_number", (Object) errorEvent.getErrorLineNumber());
        }
        return jSONObject;
    }
}
