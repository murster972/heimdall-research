package com.mopub.common.event;

import com.mopub.common.ClientMetadata;
import com.mopub.common.Preconditions;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

public abstract class BaseEvent {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f20564;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f20565;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f20566;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final Integer f20567;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final Double f20568;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final String f20569;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Integer f20570;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final String f20571;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final ClientMetadata.MoPubNetworkType f20572;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final String f20573;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final String f20574;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final String f20575;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Double f20576;

    /* renamed from: י  reason: contains not printable characters */
    private final String f20577;

    /* renamed from: ـ  reason: contains not printable characters */
    private final Double f20578;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final Double f20579;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final String f20580;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final String f20581;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final Integer f20582;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final String f20583;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final Integer f20584;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private final long f20585 = System.currentTimeMillis();

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f20586;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Name f20587;

    /* renamed from: 麤  reason: contains not printable characters */
    private final SdkProduct f20588;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Category f20589;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ScribeCategory f20590;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final double f20591;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final Double f20592;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final Double f20593;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private ClientMetadata f20594 = ClientMetadata.getInstance();

    public enum AppPlatform {
        NONE(0),
        IOS(1),
        ANDROID(2),
        MOBILE_WEB(3);
        
        private final int mType;

        private AppPlatform(int i) {
            this.mType = i;
        }

        public int getType() {
            return this.mType;
        }
    }

    public static abstract class Builder {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public String f20596;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public String f20597;
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public String f20598;
        /* access modifiers changed from: private */

        /* renamed from: ʾ  reason: contains not printable characters */
        public Double f20599;
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public Double f20600;
        /* access modifiers changed from: private */

        /* renamed from: ˆ  reason: contains not printable characters */
        public double f20601;
        /* access modifiers changed from: private */

        /* renamed from: ˈ  reason: contains not printable characters */
        public Double f20602;
        /* access modifiers changed from: private */

        /* renamed from: ˊ  reason: contains not printable characters */
        public Integer f20603;
        /* access modifiers changed from: private */

        /* renamed from: ˋ  reason: contains not printable characters */
        public String f20604;
        /* access modifiers changed from: private */

        /* renamed from: ˎ  reason: contains not printable characters */
        public Integer f20605;
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public Double f20606;
        /* access modifiers changed from: private */

        /* renamed from: ٴ  reason: contains not printable characters */
        public Double f20607;
        /* access modifiers changed from: private */

        /* renamed from: ᐧ  reason: contains not printable characters */
        public String f20608;
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public String f20609;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public Name f20610;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public SdkProduct f20611;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public Category f20612;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public ScribeCategory f20613;
        /* access modifiers changed from: private */

        /* renamed from: ﹶ  reason: contains not printable characters */
        public Double f20614;
        /* access modifiers changed from: private */

        /* renamed from: ﾞ  reason: contains not printable characters */
        public String f20615;

        public Builder(ScribeCategory scribeCategory, Name name, Category category, double d) {
            Preconditions.checkNotNull(scribeCategory);
            Preconditions.checkNotNull(name);
            Preconditions.checkNotNull(category);
            Preconditions.checkArgument(d >= 0.0d && d <= 1.0d);
            this.f20613 = scribeCategory;
            this.f20610 = name;
            this.f20612 = category;
            this.f20601 = d;
        }

        public abstract BaseEvent build();

        public Builder withAdCreativeId(String str) {
            this.f20596 = str;
            return this;
        }

        public Builder withAdHeightPx(Double d) {
            this.f20607 = d;
            return this;
        }

        public Builder withAdNetworkType(String str) {
            this.f20598 = str;
            return this;
        }

        public Builder withAdType(String str) {
            this.f20597 = str;
            return this;
        }

        public Builder withAdUnitId(String str) {
            this.f20609 = str;
            return this;
        }

        public Builder withAdWidthPx(Double d) {
            this.f20606 = d;
            return this;
        }

        public Builder withDspCreativeId(String str) {
            this.f20608 = str;
            return this;
        }

        public Builder withGeoAccuracy(Double d) {
            this.f20600 = d;
            return this;
        }

        public Builder withGeoLat(Double d) {
            this.f20602 = d;
            return this;
        }

        public Builder withGeoLon(Double d) {
            this.f20599 = d;
            return this;
        }

        public Builder withPerformanceDurationMs(Double d) {
            this.f20614 = d;
            return this;
        }

        public Builder withRequestId(String str) {
            this.f20615 = str;
            return this;
        }

        public Builder withRequestRetries(Integer num) {
            this.f20605 = num;
            return this;
        }

        public Builder withRequestStatusCode(Integer num) {
            this.f20603 = num;
            return this;
        }

        public Builder withRequestUri(String str) {
            this.f20604 = str;
            return this;
        }

        public Builder withSdkProduct(SdkProduct sdkProduct) {
            this.f20611 = sdkProduct;
            return this;
        }
    }

    public enum Category {
        REQUESTS("requests"),
        NATIVE_VIDEO("native_video"),
        AD_INTERACTIONS("ad_interactions");
        
        private final String mCategory;

        private Category(String str) {
            this.mCategory = str;
        }

        public String getCategory() {
            return this.mCategory;
        }
    }

    public enum Name {
        AD_REQUEST("ad_request"),
        IMPRESSION_REQUEST("impression_request"),
        CLICK_REQUEST("click_request"),
        DOWNLOAD_START("download_start"),
        DOWNLOAD_VIDEO_READY("download_video_ready"),
        DOWNLOAD_BUFFERING("download_video_buffering"),
        DOWNLOAD_FINISHED("download_finished"),
        ERROR_DURING_PLAYBACK("error_during_playback"),
        ERROR_FAILED_TO_PLAY("error_failed_to_play"),
        AD_DWELL_TIME("clickthrough_dwell_time");
        
        private final String mName;

        private Name(String str) {
            this.mName = str;
        }

        public String getName() {
            return this.mName;
        }
    }

    public enum SamplingRate {
        AD_REQUEST(0.1d),
        NATIVE_VIDEO(0.1d),
        AD_INTERACTIONS(0.1d);
        
        private final double mSamplingRate;

        private SamplingRate(double d) {
            this.mSamplingRate = d;
        }

        public double getSamplingRate() {
            return this.mSamplingRate;
        }
    }

    public enum ScribeCategory {
        EXCHANGE_CLIENT_EVENT("exchange_client_event"),
        EXCHANGE_CLIENT_ERROR("exchange_client_error");
        
        private final String mScribeCategory;

        private ScribeCategory(String str) {
            this.mScribeCategory = str;
        }

        public String getCategory() {
            return this.mScribeCategory;
        }
    }

    public enum SdkProduct {
        NONE(0),
        WEB_VIEW(1),
        NATIVE(2);
        
        private final int mType;

        private SdkProduct(int i) {
            this.mType = i;
        }

        public int getType() {
            return this.mType;
        }
    }

    public BaseEvent(Builder builder) {
        Preconditions.checkNotNull(builder);
        this.f20590 = builder.f20613;
        this.f20587 = builder.f20610;
        this.f20589 = builder.f20612;
        this.f20588 = builder.f20611;
        this.f20586 = builder.f20609;
        this.f20564 = builder.f20596;
        this.f20565 = builder.f20597;
        this.f20566 = builder.f20598;
        this.f20576 = builder.f20606;
        this.f20579 = builder.f20607;
        this.f20580 = builder.f20608;
        this.f20568 = builder.f20602;
        this.f20592 = builder.f20599;
        this.f20593 = builder.f20600;
        this.f20578 = builder.f20614;
        this.f20581 = builder.f20615;
        this.f20582 = builder.f20603;
        this.f20583 = builder.f20604;
        this.f20584 = builder.f20605;
        this.f20591 = builder.f20601;
        if (this.f20594 != null) {
            this.f20570 = Integer.valueOf(this.f20594.getDeviceScreenWidthDip());
            this.f20567 = Integer.valueOf(this.f20594.getDeviceScreenHeightDip());
            this.f20572 = this.f20594.getActiveNetworkType();
            this.f20573 = this.f20594.getNetworkOperator();
            this.f20574 = this.f20594.getNetworkOperatorName();
            this.f20569 = this.f20594.getIsoCountryCode();
            this.f20571 = this.f20594.getSimOperator();
            this.f20575 = this.f20594.getSimOperatorName();
            this.f20577 = this.f20594.getSimIsoCountryCode();
            return;
        }
        this.f20570 = null;
        this.f20567 = null;
        this.f20572 = null;
        this.f20573 = null;
        this.f20574 = null;
        this.f20569 = null;
        this.f20571 = null;
        this.f20575 = null;
        this.f20577 = null;
    }

    public String getAdCreativeId() {
        return this.f20564;
    }

    public Double getAdHeightPx() {
        return this.f20579;
    }

    public String getAdNetworkType() {
        return this.f20566;
    }

    public String getAdType() {
        return this.f20565;
    }

    public String getAdUnitId() {
        return this.f20586;
    }

    public Double getAdWidthPx() {
        return this.f20576;
    }

    public String getAppName() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getAppName();
    }

    public String getAppPackageName() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getAppPackageName();
    }

    public AppPlatform getAppPlatform() {
        return AppPlatform.ANDROID;
    }

    public String getAppVersion() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getAppVersion();
    }

    public Category getCategory() {
        return this.f20589;
    }

    public String getClientAdvertisingId() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getDeviceId();
    }

    public Boolean getClientDoNotTrack() {
        return Boolean.valueOf(this.f20594 == null || this.f20594.isDoNotTrackSet());
    }

    public String getDeviceManufacturer() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getDeviceManufacturer();
    }

    public String getDeviceModel() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getDeviceModel();
    }

    public String getDeviceOsVersion() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getDeviceOsVersion();
    }

    public String getDeviceProduct() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getDeviceProduct();
    }

    public Integer getDeviceScreenHeightDip() {
        return this.f20567;
    }

    public Integer getDeviceScreenWidthDip() {
        return this.f20570;
    }

    public String getDspCreativeId() {
        return this.f20580;
    }

    public Double getGeoAccuracy() {
        return this.f20593;
    }

    public Double getGeoLat() {
        return this.f20568;
    }

    public Double getGeoLon() {
        return this.f20592;
    }

    public Name getName() {
        return this.f20587;
    }

    public String getNetworkIsoCountryCode() {
        return this.f20569;
    }

    public String getNetworkOperatorCode() {
        return this.f20573;
    }

    public String getNetworkOperatorName() {
        return this.f20574;
    }

    public String getNetworkSimCode() {
        return this.f20571;
    }

    public String getNetworkSimIsoCountryCode() {
        return this.f20577;
    }

    public String getNetworkSimOperatorName() {
        return this.f20575;
    }

    public ClientMetadata.MoPubNetworkType getNetworkType() {
        return this.f20572;
    }

    public String getObfuscatedClientAdvertisingId() {
        return "ifa:XXXX";
    }

    public Double getPerformanceDurationMs() {
        return this.f20578;
    }

    public String getRequestId() {
        return this.f20581;
    }

    public Integer getRequestRetries() {
        return this.f20584;
    }

    public Integer getRequestStatusCode() {
        return this.f20582;
    }

    public String getRequestUri() {
        return this.f20583;
    }

    public double getSamplingRate() {
        return this.f20591;
    }

    public ScribeCategory getScribeCategory() {
        return this.f20590;
    }

    public SdkProduct getSdkProduct() {
        return this.f20588;
    }

    public String getSdkVersion() {
        if (this.f20594 == null) {
            return null;
        }
        return this.f20594.getSdkVersion();
    }

    public Long getTimestampUtcMs() {
        return Long.valueOf(this.f20585);
    }

    public String toString() {
        return "BaseEvent\nScribeCategory: " + getScribeCategory() + "\nName: " + getName() + "\nCategory: " + getCategory() + "\nSdkProduct: " + getSdkProduct() + "\nSdkVersion: " + getSdkVersion() + "\nAdUnitId: " + getAdUnitId() + "\nAdCreativeId: " + getAdCreativeId() + "\nAdType: " + getAdType() + "\nAdNetworkType: " + getAdNetworkType() + "\nAdWidthPx: " + getAdWidthPx() + "\nAdHeightPx: " + getAdHeightPx() + "\nDspCreativeId: " + getDspCreativeId() + "\nAppPlatform: " + getAppPlatform() + "\nAppName: " + getAppName() + "\nAppPackageName: " + getAppPackageName() + "\nAppVersion: " + getAppVersion() + "\nDeviceManufacturer: " + getDeviceManufacturer() + "\nDeviceModel: " + getDeviceModel() + "\nDeviceProduct: " + getDeviceProduct() + "\nDeviceOsVersion: " + getDeviceOsVersion() + "\nDeviceScreenWidth: " + getDeviceScreenWidthDip() + "\nDeviceScreenHeight: " + getDeviceScreenHeightDip() + "\nGeoLat: " + getGeoLat() + "\nGeoLon: " + getGeoLon() + "\nGeoAccuracy: " + getGeoAccuracy() + "\nPerformanceDurationMs: " + getPerformanceDurationMs() + "\nNetworkType: " + getNetworkType() + "\nNetworkOperatorCode: " + getNetworkOperatorCode() + "\nNetworkOperatorName: " + getNetworkOperatorName() + "\nNetworkIsoCountryCode: " + getNetworkIsoCountryCode() + "\nNetworkSimCode: " + getNetworkSimCode() + "\nNetworkSimOperatorName: " + getNetworkSimOperatorName() + "\nNetworkSimIsoCountryCode: " + getNetworkSimIsoCountryCode() + "\nRequestId: " + getRequestId() + "\nRequestStatusCode: " + getRequestStatusCode() + "\nRequestUri: " + getRequestUri() + "\nRequestRetries: " + getRequestRetries() + "\nSamplingRate: " + getSamplingRate() + "\nTimestampUtcMs: " + new SimpleDateFormat().format(new Date(getTimestampUtcMs().longValue())) + StringUtils.LF;
    }
}
