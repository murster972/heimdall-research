package com.mopub.common.event;

import com.mopub.common.event.BaseEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.commons.lang3.StringUtils;

public class ErrorEvent extends BaseEvent {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f20621;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Integer f20622;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f20623;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f20624;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f20625;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f20626;

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f20627;

    public static class Builder extends BaseEvent.Builder {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public String f20628;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public Integer f20629;
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public String f20630;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public String f20631;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public String f20632;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public String f20633;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public String f20634;

        public Builder(BaseEvent.Name name, BaseEvent.Category category, double d) {
            super(BaseEvent.ScribeCategory.EXCHANGE_CLIENT_ERROR, name, category, d);
        }

        public ErrorEvent build() {
            return new ErrorEvent(this);
        }

        public Builder withErrorClassName(String str) {
            this.f20630 = str;
            return this;
        }

        public Builder withErrorExceptionClassName(String str) {
            this.f20634 = str;
            return this;
        }

        public Builder withErrorFileName(String str) {
            this.f20632 = str;
            return this;
        }

        public Builder withErrorLineNumber(Integer num) {
            this.f20629 = num;
            return this;
        }

        public Builder withErrorMessage(String str) {
            this.f20631 = str;
            return this;
        }

        public Builder withErrorMethodName(String str) {
            this.f20628 = str;
            return this;
        }

        public Builder withErrorStackTrace(String str) {
            this.f20633 = str;
            return this;
        }

        public Builder withException(Exception exc) {
            this.f20634 = exc.getClass().getName();
            this.f20631 = exc.getMessage();
            StringWriter stringWriter = new StringWriter();
            exc.printStackTrace(new PrintWriter(stringWriter));
            this.f20633 = stringWriter.toString();
            if (exc.getStackTrace().length > 0) {
                this.f20632 = exc.getStackTrace()[0].getFileName();
                this.f20630 = exc.getStackTrace()[0].getClassName();
                this.f20628 = exc.getStackTrace()[0].getMethodName();
                this.f20629 = Integer.valueOf(exc.getStackTrace()[0].getLineNumber());
            }
            return this;
        }
    }

    private ErrorEvent(Builder builder) {
        super(builder);
        this.f20627 = builder.f20634;
        this.f20624 = builder.f20631;
        this.f20626 = builder.f20633;
        this.f20625 = builder.f20632;
        this.f20623 = builder.f20630;
        this.f20621 = builder.f20628;
        this.f20622 = builder.f20629;
    }

    public String getErrorClassName() {
        return this.f20623;
    }

    public String getErrorExceptionClassName() {
        return this.f20627;
    }

    public String getErrorFileName() {
        return this.f20625;
    }

    public Integer getErrorLineNumber() {
        return this.f20622;
    }

    public String getErrorMessage() {
        return this.f20624;
    }

    public String getErrorMethodName() {
        return this.f20621;
    }

    public String getErrorStackTrace() {
        return this.f20626;
    }

    public String toString() {
        return super.toString() + "ErrorEvent\nErrorExceptionClassName: " + getErrorExceptionClassName() + "\nErrorMessage: " + getErrorMessage() + "\nErrorStackTrace: " + getErrorStackTrace() + "\nErrorFileName: " + getErrorFileName() + "\nErrorClassName: " + getErrorClassName() + "\nErrorMethodName: " + getErrorMethodName() + "\nErrorLineNumber: " + getErrorLineNumber() + StringUtils.LF;
    }
}
