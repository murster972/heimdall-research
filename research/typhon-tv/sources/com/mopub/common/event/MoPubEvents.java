package com.mopub.common.event;

import android.os.HandlerThread;
import com.mopub.common.VisibleForTesting;
import java.util.ArrayList;

public class MoPubEvents {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile EventDispatcher f20644;

    public static void log(BaseEvent baseEvent) {
        m26587().dispatch(baseEvent);
    }

    @VisibleForTesting
    public static void setEventDispatcher(EventDispatcher eventDispatcher) {
        f20644 = eventDispatcher;
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static EventDispatcher m26587() {
        EventDispatcher eventDispatcher = f20644;
        if (eventDispatcher == null) {
            synchronized (MoPubEvents.class) {
                eventDispatcher = f20644;
                if (eventDispatcher == null) {
                    ArrayList arrayList = new ArrayList();
                    HandlerThread handlerThread = new HandlerThread("mopub_event_logging");
                    handlerThread.start();
                    arrayList.add(new ScribeEventRecorder(handlerThread.getLooper()));
                    EventDispatcher eventDispatcher2 = new EventDispatcher(arrayList, handlerThread.getLooper());
                    f20644 = eventDispatcher2;
                    eventDispatcher = eventDispatcher2;
                }
            }
        }
        return eventDispatcher;
    }
}
