package com.mopub.common.event;

import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class EventSampler {

    /* renamed from: 靐  reason: contains not printable characters */
    private LinkedHashMap<String, Boolean> f20642;

    /* renamed from: 龘  reason: contains not printable characters */
    private Random f20643;

    public EventSampler() {
        this(new Random());
    }

    @VisibleForTesting
    public EventSampler(Random random) {
        this.f20643 = random;
        this.f20642 = new LinkedHashMap<String, Boolean>(TsExtractor.TS_STREAM_TYPE_E_AC3, 0.75f, true) {
            /* access modifiers changed from: protected */
            public boolean removeEldestEntry(Map.Entry<String, Boolean> entry) {
                return size() > 100;
            }
        };
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26586(BaseEvent baseEvent) {
        Preconditions.checkNotNull(baseEvent);
        String requestId = baseEvent.getRequestId();
        if (requestId == null) {
            return this.f20643.nextDouble() < baseEvent.getSamplingRate();
        }
        Boolean bool = this.f20642.get(requestId);
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean z = this.f20643.nextDouble() < baseEvent.getSamplingRate();
        this.f20642.put(requestId, Boolean.valueOf(z));
        return z;
    }
}
