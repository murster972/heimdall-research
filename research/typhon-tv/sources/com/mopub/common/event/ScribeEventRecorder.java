package com.mopub.common.event;

import android.os.Handler;
import android.os.Looper;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.network.ScribeBackoffPolicy;
import com.mopub.network.ScribeRequest;
import com.mopub.network.ScribeRequestManager;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ScribeEventRecorder implements EventRecorder {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final PollingRunnable f20645;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Handler f20646;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Queue<BaseEvent> f20647;

    /* renamed from: 麤  reason: contains not printable characters */
    private final ScribeRequestManager f20648;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final EventSerializer f20649;

    /* renamed from: 龘  reason: contains not printable characters */
    private final EventSampler f20650;

    class PollingRunnable implements Runnable {
        PollingRunnable() {
        }

        public void run() {
            ScribeEventRecorder.this.m26591();
            ScribeEventRecorder.this.m26590();
        }
    }

    ScribeEventRecorder(Looper looper) {
        this(new EventSampler(), new LinkedList(), new EventSerializer(), new ScribeRequestManager(looper), new Handler(looper));
    }

    @VisibleForTesting
    ScribeEventRecorder(EventSampler eventSampler, Queue<BaseEvent> queue, EventSerializer eventSerializer, ScribeRequestManager scribeRequestManager, Handler handler) {
        this.f20650 = eventSampler;
        this.f20647 = queue;
        this.f20649 = eventSerializer;
        this.f20648 = scribeRequestManager;
        this.f20646 = handler;
        this.f20645 = new PollingRunnable();
    }

    public void record(BaseEvent baseEvent) {
        if (this.f20650.m26586(baseEvent)) {
            if (this.f20647.size() >= 500) {
                MoPubLog.d("EventQueue is at max capacity. Event \"" + baseEvent.getName() + "\" is being dropped.");
                return;
            }
            this.f20647.add(baseEvent);
            if (this.f20647.size() >= 100) {
                m26591();
            }
            m26590();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public List<BaseEvent> m26589() {
        ArrayList arrayList = new ArrayList();
        while (this.f20647.peek() != null && arrayList.size() < 100) {
            arrayList.add(this.f20647.poll());
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 齉  reason: contains not printable characters */
    public void m26590() {
        if (!this.f20646.hasMessages(0) && !this.f20647.isEmpty()) {
            this.f20646.postDelayed(this.f20645, 120000);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26591() {
        if (!this.f20648.isAtCapacity()) {
            final List<BaseEvent> r0 = m26589();
            if (!r0.isEmpty()) {
                this.f20648.makeRequest(new ScribeRequest.ScribeRequestFactory() {
                    public ScribeRequest createRequest(ScribeRequest.Listener listener) {
                        return new ScribeRequest("=", r0, ScribeEventRecorder.this.f20649, listener);
                    }
                }, new ScribeBackoffPolicy());
            }
        }
    }
}
