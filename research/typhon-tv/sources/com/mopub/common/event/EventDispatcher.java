package com.mopub.common.event;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.mopub.common.VisibleForTesting;

public class EventDispatcher {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Looper f20637;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Handler.Callback f20638 = new Handler.Callback() {
        public boolean handleMessage(Message message) {
            if (!(message.obj instanceof BaseEvent)) {
                return true;
            }
            for (EventRecorder record : EventDispatcher.this.f20640) {
                record.record((BaseEvent) message.obj);
            }
            return true;
        }
    };

    /* renamed from: 齉  reason: contains not printable characters */
    private final Handler f20639 = new Handler(this.f20637, this.f20638);
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Iterable<EventRecorder> f20640;

    @VisibleForTesting
    EventDispatcher(Iterable<EventRecorder> iterable, Looper looper) {
        this.f20640 = iterable;
        this.f20637 = looper;
    }

    public void dispatch(BaseEvent baseEvent) {
        Message.obtain(this.f20639, 0, baseEvent).sendToTarget();
    }
}
