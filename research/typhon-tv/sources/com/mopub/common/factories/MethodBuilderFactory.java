package com.mopub.common.factories;

import com.mopub.common.util.Reflection;

public class MethodBuilderFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static MethodBuilderFactory f5693 = new MethodBuilderFactory();

    public static Reflection.MethodBuilder create(Object obj, String str) {
        return f5693.m6174(obj, str);
    }

    @Deprecated
    public static void setInstance(MethodBuilderFactory methodBuilderFactory) {
        f5693 = methodBuilderFactory;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Reflection.MethodBuilder m6174(Object obj, String str) {
        return new Reflection.MethodBuilder(obj, str);
    }
}
