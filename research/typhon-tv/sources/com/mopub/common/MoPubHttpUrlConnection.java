package com.mopub.common;

import com.mopub.common.logging.MoPubLog;
import com.mopub.network.Networking;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;

public abstract class MoPubHttpUrlConnection extends HttpURLConnection {
    public static HttpURLConnection getHttpUrlConnection(String str) throws IOException {
        String str2;
        Preconditions.checkNotNull(str);
        if (m26526(str)) {
            throw new IllegalArgumentException("URL is improperly encoded: " + str);
        }
        try {
            str2 = urlEncode(str);
        } catch (Exception e) {
            str2 = str;
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
        httpURLConnection.setRequestProperty(AbstractSpiCall.HEADER_USER_AGENT, Networking.getCachedUserAgent());
        httpURLConnection.setConnectTimeout(10000);
        httpURLConnection.setReadTimeout(10000);
        return httpURLConnection;
    }

    public static String urlEncode(String str) throws Exception {
        Preconditions.checkNotNull(str);
        if (m26526(str)) {
            throw new UnsupportedEncodingException("URL is improperly encoded: " + str);
        }
        return (m26524(str) ? m26525(str) : new URI(str)).toURL().toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m26524(String str) {
        try {
            new URI(str);
            return false;
        } catch (URISyntaxException e) {
            return true;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static URI m26525(String str) throws Exception {
        try {
            URL url = new URL(str);
            return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
        } catch (Exception e) {
            MoPubLog.w("Failed to encode url: " + str);
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m26526(String str) {
        try {
            URLDecoder.decode(str, "UTF-8");
            return false;
        } catch (UnsupportedEncodingException e) {
            MoPubLog.w("Url is improperly encoded: " + str);
            return true;
        }
    }
}
