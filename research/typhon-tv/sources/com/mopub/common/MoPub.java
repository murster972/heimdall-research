package com.mopub.common;

import android.app.Activity;
import com.Pinkamena;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MoPub {
    public static final String SDK_VERSION = "4.15.0";

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean f5686 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Method f5687;

    /* renamed from: 连任  reason: contains not printable characters */
    private static volatile boolean f5688 = false;

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile int f5689 = 6;

    /* renamed from: 麤  reason: contains not printable characters */
    private static volatile BrowserAgent f5690 = BrowserAgent.IN_APP;

    /* renamed from: 齉  reason: contains not printable characters */
    private static volatile long f5691 = 60000;

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile LocationAwareness f5692 = LocationAwareness.NORMAL;

    public enum BrowserAgent {
        IN_APP,
        NATIVE;

        public static BrowserAgent fromHeader(Integer num) {
            return num == null ? IN_APP : num.intValue() == 1 ? NATIVE : IN_APP;
        }
    }

    public enum LocationAwareness {
        NORMAL,
        TRUNCATED,
        DISABLED
    }

    public static BrowserAgent getBrowserAgent() {
        Preconditions.checkNotNull(f5690);
        return f5690;
    }

    public static LocationAwareness getLocationAwareness() {
        Preconditions.checkNotNull(f5692);
        return f5692;
    }

    public static int getLocationPrecision() {
        return f5689;
    }

    public static long getMinimumLocationRefreshTimeMillis() {
        return f5691;
    }

    @Deprecated
    public static boolean hasRewardedVideo(String str) {
        try {
            return ((Boolean) new Reflection.MethodBuilder((Object) null, "hasRewardedVideo").setStatic(Class.forName("com.mopub.mobileads.MoPubRewardedVideos")).addParam(String.class, str).execute()).booleanValue();
        } catch (ClassNotFoundException e) {
            MoPubLog.w("hasRewardedVideo was called without the rewarded video module");
        } catch (NoSuchMethodException e2) {
            MoPubLog.w("hasRewardedVideo was called without the rewarded video module");
        } catch (Exception e3) {
            MoPubLog.e("Error while checking rewarded video", e3);
        }
        return false;
    }

    @Deprecated
    public static void initializeRewardedVideo(Activity activity, MediationSettings... mediationSettingsArr) {
        try {
            new Reflection.MethodBuilder((Object) null, "initializeRewardedVideo").setStatic(Class.forName("com.mopub.mobileads.MoPubRewardedVideos")).addParam(Activity.class, activity).addParam(MediationSettings[].class, mediationSettingsArr).execute();
        } catch (ClassNotFoundException e) {
            MoPubLog.w("initializeRewardedVideo was called without the rewarded video module");
        } catch (NoSuchMethodException e2) {
            MoPubLog.w("initializeRewardedVideo was called without the rewarded video module");
        } catch (Exception e3) {
            MoPubLog.e("Error while initializing rewarded video", e3);
        }
    }

    @Deprecated
    public static void loadRewardedVideo(String str, Object obj, MediationSettings... mediationSettingsArr) {
        try {
            new Reflection.MethodBuilder((Object) null, "loadRewardedVideo").setStatic(Class.forName("com.mopub.mobileads.MoPubRewardedVideos")).addParam(String.class, str).addParam(Class.forName("com.mopub.mobileads.MoPubRewardedVideoManager$RequestParameters"), obj).addParam(MediationSettings[].class, mediationSettingsArr).execute();
        } catch (ClassNotFoundException e) {
            MoPubLog.w("loadRewardedVideo was called without the rewarded video module");
        } catch (NoSuchMethodException e2) {
            MoPubLog.w("loadRewardedVideo was called without the rewarded video module");
        } catch (Exception e3) {
            MoPubLog.e("Error while loading rewarded video", e3);
        }
    }

    @Deprecated
    public static void loadRewardedVideo(String str, MediationSettings... mediationSettingsArr) {
        Pinkamena.DianePie();
    }

    public static void onBackPressed(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onBackPressed(activity);
    }

    public static void onCreate(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onCreate(activity);
        m6173(activity);
    }

    public static void onDestroy(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onDestroy(activity);
    }

    public static void onPause(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onPause(activity);
    }

    public static void onRestart(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onRestart(activity);
        m6173(activity);
    }

    public static void onResume(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onResume(activity);
        m6173(activity);
    }

    public static void onStart(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onStart(activity);
        m6173(activity);
    }

    public static void onStop(Activity activity) {
        MoPubLifecycleManager.getInstance(activity).onStop(activity);
    }

    @Deprecated
    @VisibleForTesting
    public static void resetBrowserAgent() {
        f5690 = BrowserAgent.IN_APP;
        f5688 = false;
    }

    public static void setBrowserAgent(BrowserAgent browserAgent) {
        Preconditions.checkNotNull(browserAgent);
        f5690 = browserAgent;
        f5688 = true;
    }

    public static void setBrowserAgentFromAdServer(BrowserAgent browserAgent) {
        Preconditions.checkNotNull(browserAgent);
        if (f5688) {
            MoPubLog.w("Browser agent already overridden by client with value " + f5690);
        } else {
            f5690 = browserAgent;
        }
    }

    public static void setLocationAwareness(LocationAwareness locationAwareness) {
        Preconditions.checkNotNull(locationAwareness);
        f5692 = locationAwareness;
    }

    public static void setLocationPrecision(int i) {
        f5689 = Math.min(Math.max(0, i), 6);
    }

    public static void setMinimumLocationRefreshTimeMillis(long j) {
        f5691 = j;
    }

    @Deprecated
    public static void setRewardedVideoListener(Object obj) {
        try {
            new Reflection.MethodBuilder((Object) null, "setRewardedVideoListener").setStatic(Class.forName("com.mopub.mobileads.MoPubRewardedVideos")).addParam(Class.forName("com.mopub.mobileads.MoPubRewardedVideoListener"), obj).execute();
        } catch (ClassNotFoundException e) {
            MoPubLog.w("setRewardedVideoListener was called without the rewarded video module");
        } catch (NoSuchMethodException e2) {
            MoPubLog.w("setRewardedVideoListener was called without the rewarded video module");
        } catch (Exception e3) {
            MoPubLog.e("Error while setting rewarded video listener", e3);
        }
    }

    @Deprecated
    public static void showRewardedVideo(String str) {
        try {
            new Reflection.MethodBuilder((Object) null, "showRewardedVideo").setStatic(Class.forName("com.mopub.mobileads.MoPubRewardedVideos")).addParam(String.class, str).execute();
        } catch (ClassNotFoundException e) {
            MoPubLog.w("showRewardedVideo was called without the rewarded video module");
        } catch (NoSuchMethodException e2) {
            MoPubLog.w("showRewardedVideo was called without the rewarded video module");
        } catch (Exception e3) {
            MoPubLog.e("Error while showing rewarded video", e3);
        }
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static void m6173(Activity activity) {
        if (!f5686) {
            f5686 = true;
            try {
                f5687 = Reflection.getDeclaredMethodWithTraversal(Class.forName("com.mopub.mobileads.MoPubRewardedVideoManager"), "updateActivity", Activity.class);
            } catch (ClassNotFoundException | NoSuchMethodException e) {
            }
        }
        if (f5687 != null) {
            try {
                f5687.invoke((Object) null, new Object[]{activity});
            } catch (IllegalAccessException e2) {
                MoPubLog.e("Error while attempting to access the update activity method - this should not have happened", e2);
            } catch (InvocationTargetException e3) {
                MoPubLog.e("Error while attempting to access the update activity method - this should not have happened", e3);
            }
        }
    }
}
