package com.mopub.common;

import android.graphics.Point;
import android.net.Uri;
import android.text.TextUtils;
import com.mopub.network.Networking;
import com.mopub.network.PlayServicesUrlRewriter;
import net.pubnative.library.request.PubnativeRequest;

public abstract class BaseUrlGenerator {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f20443;

    /* renamed from: 龘  reason: contains not printable characters */
    private StringBuilder f20444;

    /* renamed from: 齉  reason: contains not printable characters */
    private String m26446() {
        if (!this.f20443) {
            return "&";
        }
        this.f20443 = false;
        return "?";
    }

    public abstract String generateUrlString(String str);

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m26447(String str) {
        m26450("av", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m26448(String str) {
        m26450("v", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26449() {
        m26450("udid", PlayServicesUrlRewriter.UDID_TEMPLATE);
        m26450(PubnativeRequest.Parameters.NO_USER_ID, PlayServicesUrlRewriter.DO_NOT_TRACK_TEMPLATE);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26450(String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.f20444.append(m26446());
            this.f20444.append(str);
            this.f20444.append("=");
            this.f20444.append(Uri.encode(str2));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26451(boolean z) {
        m26450("android_perms_ext_storage", z ? PubnativeRequest.LEGACY_ZONE_ID : "0");
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m26452() {
        return this.f20444.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26453(Point point) {
        m26450("w", "" + point.x);
        m26450("h", "" + point.y);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26454(String str, String str2) {
        this.f20444 = new StringBuilder(Networking.getScheme()).append("://").append(str).append(str2);
        this.f20443 = true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26455(String... strArr) {
        StringBuilder sb = new StringBuilder();
        if (strArr != null && strArr.length >= 1) {
            for (int i = 0; i < strArr.length - 1; i++) {
                sb.append(strArr[i]).append(",");
            }
            sb.append(strArr[strArr.length - 1]);
            m26450("dn", sb.toString());
        }
    }
}
