package com.mopub.common;

import android.os.Looper;
import com.mopub.common.logging.MoPubLog;
import java.util.IllegalFormatException;

public final class Preconditions {
    public static final String EMPTY_ARGUMENTS = "";

    public static final class NoThrow {

        /* renamed from: 龘  reason: contains not printable characters */
        private static volatile boolean f20542 = false;

        public static boolean checkArgument(boolean z) {
            return Preconditions.m26531(z, f20542, "Illegal argument", "");
        }

        public static boolean checkArgument(boolean z, String str) {
            return Preconditions.m26531(z, f20542, str, "");
        }

        public static boolean checkArgument(boolean z, String str, Object... objArr) {
            return Preconditions.m26531(z, f20542, str, objArr);
        }

        public static boolean checkNotNull(Object obj) {
            return Preconditions.m26527(obj, f20542, "Object can not be null.", "");
        }

        public static boolean checkNotNull(Object obj, String str) {
            return Preconditions.m26527(obj, f20542, str, "");
        }

        public static boolean checkNotNull(Object obj, String str, Object... objArr) {
            return Preconditions.m26527(obj, f20542, str, objArr);
        }

        public static boolean checkState(boolean z) {
            return Preconditions.m26530(z, f20542, "Illegal state.", "");
        }

        public static boolean checkState(boolean z, String str) {
            return Preconditions.m26530(z, f20542, str, "");
        }

        public static boolean checkState(boolean z, String str, Object... objArr) {
            return Preconditions.m26530(z, f20542, str, objArr);
        }

        public static boolean checkUiThread() {
            return Preconditions.m26528(f20542, "This method must be called from the UI thread.", "");
        }

        public static boolean checkUiThread(String str) {
            return Preconditions.m26528(f20542, str, "");
        }

        public static boolean checkUiThread(String str, Object... objArr) {
            return Preconditions.m26528(false, str, objArr);
        }

        public static void setStrictMode(boolean z) {
            f20542 = z;
        }
    }

    private Preconditions() {
    }

    public static void checkArgument(boolean z) {
        m26531(z, true, "Illegal argument.", "");
    }

    public static void checkArgument(boolean z, String str) {
        m26531(z, true, str, "");
    }

    public static void checkArgument(boolean z, String str, Object... objArr) {
        m26531(z, true, str, objArr);
    }

    public static void checkNotNull(Object obj) {
        m26527(obj, true, "Object can not be null.", "");
    }

    public static void checkNotNull(Object obj, String str) {
        m26527(obj, true, str, "");
    }

    public static void checkNotNull(Object obj, String str, Object... objArr) {
        m26527(obj, true, str, objArr);
    }

    public static void checkState(boolean z) {
        m26530(z, true, "Illegal state.", "");
    }

    public static void checkState(boolean z, String str) {
        m26530(z, true, str, "");
    }

    public static void checkState(boolean z, String str, Object... objArr) {
        m26530(z, true, str, objArr);
    }

    public static void checkUiThread() {
        m26528(true, "This method must be called from the UI thread.", "");
    }

    public static void checkUiThread(String str) {
        m26528(true, str, "");
    }

    public static void checkUiThread(String str, Object... objArr) {
        m26528(true, str, objArr);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m26527(Object obj, boolean z, String str, Object... objArr) {
        if (obj != null) {
            return true;
        }
        String r0 = m26532(str, objArr);
        if (z) {
            throw new NullPointerException(r0);
        }
        MoPubLog.e(r0);
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m26528(boolean z, String str, Object... objArr) {
        if (Looper.getMainLooper().equals(Looper.myLooper())) {
            return true;
        }
        String r0 = m26532(str, objArr);
        if (z) {
            throw new IllegalStateException(r0);
        }
        MoPubLog.e(r0);
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m26530(boolean z, boolean z2, String str, Object... objArr) {
        if (z) {
            return true;
        }
        String r0 = m26532(str, objArr);
        if (z2) {
            throw new IllegalStateException(r0);
        }
        MoPubLog.e(r0);
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m26531(boolean z, boolean z2, String str, Object... objArr) {
        if (z) {
            return true;
        }
        String r0 = m26532(str, objArr);
        if (z2) {
            throw new IllegalArgumentException(r0);
        }
        MoPubLog.e(r0);
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m26532(String str, Object... objArr) {
        String valueOf = String.valueOf(str);
        try {
            return String.format(valueOf, objArr);
        } catch (IllegalFormatException e) {
            MoPubLog.e("MoPub preconditions had a format exception: " + e.getMessage());
            return valueOf;
        }
    }
}
