package com.mopub.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.StateListDrawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Drawables;

public class CloseableLayout extends FrameLayout {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Rect f20453 = new Rect();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Rect f20454 = new Rect();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Rect f20455 = new Rect();

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f20456;

    /* renamed from: ʿ  reason: contains not printable characters */
    private UnsetPressedState f20457;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f20458;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Rect f20459 = new Rect();

    /* renamed from: ٴ  reason: contains not printable characters */
    private OnCloseListener f20460;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private ClosePosition f20461 = ClosePosition.TOP_RIGHT;

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f20462;

    /* renamed from: 靐  reason: contains not printable characters */
    private final StateListDrawable f20463 = new StateListDrawable();

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f20464;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f20465;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f20466;

    public enum ClosePosition {
        TOP_LEFT(51),
        TOP_CENTER(49),
        TOP_RIGHT(53),
        CENTER(17),
        BOTTOM_LEFT(83),
        BOTTOM_CENTER(81),
        BOTTOM_RIGHT(85);
        
        private final int mGravity;

        private ClosePosition(int i) {
            this.mGravity = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m26469() {
            return this.mGravity;
        }
    }

    public interface OnCloseListener {
        void onClose();
    }

    private final class UnsetPressedState implements Runnable {
        private UnsetPressedState() {
        }

        public void run() {
            CloseableLayout.this.setClosePressed(false);
        }
    }

    public CloseableLayout(Context context) {
        super(context);
        this.f20463.addState(SELECTED_STATE_SET, Drawables.INTERSTITIAL_CLOSE_BUTTON_PRESSED.createDrawable(context));
        this.f20463.addState(EMPTY_STATE_SET, Drawables.INTERSTITIAL_CLOSE_BUTTON_NORMAL.createDrawable(context));
        this.f20463.setState(EMPTY_STATE_SET);
        this.f20463.setCallback(this);
        this.f20466 = ViewConfiguration.get(context).getScaledTouchSlop();
        this.f20465 = Dips.asIntPixels(50.0f, context);
        this.f20464 = Dips.asIntPixels(30.0f, context);
        this.f20462 = Dips.asIntPixels(8.0f, context);
        setWillNotDraw(false);
        this.f20456 = true;
    }

    /* access modifiers changed from: private */
    public void setClosePressed(boolean z) {
        if (z != m26466()) {
            this.f20463.setState(z ? SELECTED_STATE_SET : EMPTY_STATE_SET);
            invalidate(this.f20454);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26462() {
        playSoundEffect(0);
        if (this.f20460 != null) {
            this.f20460.onClose();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26463(ClosePosition closePosition, int i, Rect rect, Rect rect2) {
        Gravity.apply(closePosition.m26469(), i, i, rect, rect2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26464(ClosePosition closePosition, Rect rect, Rect rect2) {
        m26463(closePosition, this.f20464, rect, rect2);
    }

    public void applyCloseRegionBounds(ClosePosition closePosition, Rect rect, Rect rect2) {
        m26463(closePosition, this.f20465, rect, rect2);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f20458) {
            this.f20458 = false;
            this.f20453.set(0, 0, getWidth(), getHeight());
            applyCloseRegionBounds(this.f20461, this.f20453, this.f20454);
            this.f20459.set(this.f20454);
            this.f20459.inset(this.f20462, this.f20462);
            m26464(this.f20461, this.f20459, this.f20455);
            this.f20463.setBounds(this.f20455);
        }
        if (this.f20463.isVisible()) {
            this.f20463.draw(canvas);
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public Rect getCloseBounds() {
        return this.f20454;
    }

    @VisibleForTesting
    public boolean isCloseVisible() {
        return this.f20463.isVisible();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        return m26468((int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f20458 = true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!m26468((int) motionEvent.getX(), (int) motionEvent.getY(), this.f20466) || !m26467()) {
            setClosePressed(false);
            super.onTouchEvent(motionEvent);
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                setClosePressed(true);
                return true;
            case 1:
                if (!m26466()) {
                    return true;
                }
                if (this.f20457 == null) {
                    this.f20457 = new UnsetPressedState();
                }
                postDelayed(this.f20457, (long) ViewConfiguration.getPressedStateDuration());
                m26462();
                return true;
            case 3:
                setClosePressed(false);
                return true;
            default:
                return true;
        }
    }

    public void setCloseAlwaysInteractable(boolean z) {
        this.f20456 = z;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void setCloseBoundChanged(boolean z) {
        this.f20458 = z;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public void setCloseBounds(Rect rect) {
        this.f20454.set(rect);
    }

    public void setClosePosition(ClosePosition closePosition) {
        Preconditions.checkNotNull(closePosition);
        this.f20461 = closePosition;
        this.f20458 = true;
        invalidate();
    }

    public void setCloseVisible(boolean z) {
        if (this.f20463.setVisible(z, false)) {
            invalidate(this.f20454);
        }
    }

    public void setOnCloseListener(OnCloseListener onCloseListener) {
        this.f20460 = onCloseListener;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m26466() {
        return this.f20463.getState() == SELECTED_STATE_SET;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26467() {
        return this.f20456 || this.f20463.isVisible();
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26468(int i, int i2, int i3) {
        return i >= this.f20454.left - i3 && i2 >= this.f20454.top - i3 && i < this.f20454.right + i3 && i2 < this.f20454.bottom + i3;
    }
}
