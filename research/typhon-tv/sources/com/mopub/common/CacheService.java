package com.mopub.common;

import android.content.Context;
import android.os.AsyncTask;
import com.mopub.common.DiskLruCache;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Streams;
import com.mopub.common.util.Utils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class CacheService {

    /* renamed from: 龘  reason: contains not printable characters */
    private static DiskLruCache f5658;

    public interface DiskLruCacheGetListener {
        void onComplete(String str, byte[] bArr);
    }

    private static class DiskLruCacheGetTask extends AsyncTask<Void, Void, byte[]> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f20448;

        /* renamed from: 龘  reason: contains not printable characters */
        private final DiskLruCacheGetListener f20449;

        DiskLruCacheGetTask(String str, DiskLruCacheGetListener diskLruCacheGetListener) {
            this.f20449 = diskLruCacheGetListener;
            this.f20448 = str;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            if (this.f20449 != null) {
                this.f20449.onComplete(this.f20448, (byte[]) null);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void onPostExecute(byte[] bArr) {
            if (isCancelled()) {
                onCancelled();
            } else if (this.f20449 != null) {
                this.f20449.onComplete(this.f20448, bArr);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public byte[] doInBackground(Void... voidArr) {
            return CacheService.getFromDiskCache(this.f20448);
        }
    }

    private static class DiskLruCachePutTask extends AsyncTask<Void, Void, Void> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final byte[] f20450;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f20451;

        DiskLruCachePutTask(String str, byte[] bArr) {
            this.f20451 = str;
            this.f20450 = bArr;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Void doInBackground(Void... voidArr) {
            CacheService.putToDiskCache(this.f20451, this.f20450);
            return null;
        }
    }

    @Deprecated
    @VisibleForTesting
    public static void clearAndNullCaches() {
        if (f5658 != null) {
            try {
                f5658.delete();
                f5658 = null;
            } catch (IOException e) {
                f5658 = null;
            }
        }
    }

    public static boolean containsKeyDiskCache(String str) {
        if (f5658 == null) {
            return false;
        }
        try {
            return f5658.get(createValidDiskCacheKey(str)) != null;
        } catch (Exception e) {
            return false;
        }
    }

    public static String createValidDiskCacheKey(String str) {
        return Utils.sha1(str);
    }

    public static File getDiskCacheDirectory(Context context) {
        File cacheDir = context.getCacheDir();
        if (cacheDir == null) {
            return null;
        }
        return new File(cacheDir.getPath() + File.separator + "mopub-cache");
    }

    @Deprecated
    @VisibleForTesting
    public static DiskLruCache getDiskLruCache() {
        return f5658;
    }

    public static String getFilePathDiskCache(String str) {
        if (f5658 == null) {
            return null;
        }
        return f5658.getDirectory() + File.separator + createValidDiskCacheKey(str) + "." + 0;
    }

    public static byte[] getFromDiskCache(String str) {
        BufferedInputStream bufferedInputStream;
        if (f5658 == null) {
            return null;
        }
        byte[] bArr = null;
        DiskLruCache.Snapshot snapshot = null;
        try {
            DiskLruCache.Snapshot snapshot2 = f5658.get(createValidDiskCacheKey(str));
            if (snapshot2 == null) {
                if (snapshot2 != null) {
                    snapshot2.close();
                }
                return null;
            }
            InputStream inputStream = snapshot2.getInputStream(0);
            if (inputStream != null) {
                bArr = new byte[((int) snapshot2.getLength(0))];
                bufferedInputStream = new BufferedInputStream(inputStream);
                Streams.readStream(bufferedInputStream, bArr);
                Streams.closeStream(bufferedInputStream);
            }
            if (snapshot2 == null) {
                return bArr;
            }
            snapshot2.close();
            return bArr;
        } catch (Exception e) {
            if (snapshot == null) {
                return bArr;
            }
            snapshot.close();
            return bArr;
        } catch (Throwable th) {
            if (snapshot != null) {
                snapshot.close();
            }
            throw th;
        }
    }

    public static void getFromDiskCacheAsync(String str, DiskLruCacheGetListener diskLruCacheGetListener) {
        new DiskLruCacheGetTask(str, diskLruCacheGetListener).execute(new Void[0]);
    }

    public static void initialize(Context context) {
        initializeDiskCache(context);
    }

    public static boolean initializeDiskCache(Context context) {
        if (context == null) {
            return false;
        }
        if (f5658 == null) {
            File diskCacheDirectory = getDiskCacheDirectory(context);
            if (diskCacheDirectory == null) {
                return false;
            }
            try {
                f5658 = DiskLruCache.open(diskCacheDirectory, 1, 1, DeviceUtils.diskCacheSizeBytes(diskCacheDirectory));
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    public static boolean putToDiskCache(String str, InputStream inputStream) {
        if (f5658 == null) {
            return false;
        }
        DiskLruCache.Editor editor = null;
        try {
            DiskLruCache.Editor edit = f5658.edit(createValidDiskCacheKey(str));
            if (edit == null) {
                return false;
            }
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(edit.newOutputStream(0));
            Streams.copyContent(inputStream, bufferedOutputStream);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            f5658.flush();
            edit.commit();
            return true;
        } catch (Exception e) {
            if (editor == null) {
                return false;
            }
            try {
                editor.abort();
                return false;
            } catch (IOException e2) {
                return false;
            }
        }
    }

    public static boolean putToDiskCache(String str, byte[] bArr) {
        return putToDiskCache(str, (InputStream) new ByteArrayInputStream(bArr));
    }

    public static void putToDiskCacheAsync(String str, byte[] bArr) {
        new DiskLruCachePutTask(str, bArr).execute(new Void[0]);
    }
}
