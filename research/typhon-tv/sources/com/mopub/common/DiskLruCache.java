package com.mopub.common;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.lang3.StringUtils;

public final class DiskLruCache implements Closeable {
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final OutputStream f20470 = new OutputStream() {
        public void write(int i) throws IOException {
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    static final Pattern f20471 = Pattern.compile("[a-z0-9_-]{1,64}");

    /* renamed from: ʻ  reason: contains not printable characters */
    private final File f20472;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final File f20473;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f20474;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public Writer f20475;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f20476;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f20477 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final int f20478;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final LinkedHashMap<String, Entry> f20479 = new LinkedHashMap<>(0, 0.75f, true);

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f20480;

    /* renamed from: 连任  reason: contains not printable characters */
    private final File f20481;

    /* renamed from: 靐  reason: contains not printable characters */
    final ThreadPoolExecutor f20482 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final File f20483;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final Callable<Void> f20484 = new Callable<Void>() {
        public Void call() throws Exception {
            synchronized (DiskLruCache.this) {
                if (DiskLruCache.this.f20475 != null) {
                    DiskLruCache.this.m26472();
                    if (DiskLruCache.this.m26474()) {
                        DiskLruCache.this.m26479();
                        int unused = DiskLruCache.this.f20476 = 0;
                    }
                }
            }
            return null;
        }
    };

    /* renamed from: ﾞ  reason: contains not printable characters */
    private long f20485 = 0;

    public final class Editor {

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f20487;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final Entry f20488;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean f20489;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final boolean[] f20490;

        private class FaultHidingOutputStream extends FilterOutputStream {
            private FaultHidingOutputStream(OutputStream outputStream) {
                super(outputStream);
            }

            public void close() {
                try {
                    this.out.close();
                } catch (IOException e) {
                    boolean unused = Editor.this.f20489 = true;
                }
            }

            public void flush() {
                try {
                    this.out.flush();
                } catch (IOException e) {
                    boolean unused = Editor.this.f20489 = true;
                }
            }

            public void write(int i) {
                try {
                    this.out.write(i);
                } catch (IOException e) {
                    boolean unused = Editor.this.f20489 = true;
                }
            }

            public void write(byte[] bArr, int i, int i2) {
                try {
                    this.out.write(bArr, i, i2);
                } catch (IOException e) {
                    boolean unused = Editor.this.f20489 = true;
                }
            }
        }

        private Editor(Entry entry) {
            this.f20488 = entry;
            this.f20490 = entry.f20496 ? null : new boolean[DiskLruCache.this.f20478];
        }

        public void abort() throws IOException {
            DiskLruCache.this.m26489(this, false);
        }

        public void abortUnlessCommitted() {
            if (!this.f20487) {
                try {
                    abort();
                } catch (IOException e) {
                }
            }
        }

        public void commit() throws IOException {
            if (this.f20489) {
                DiskLruCache.this.m26489(this, false);
                DiskLruCache.this.remove(this.f20488.f20495);
            } else {
                DiskLruCache.this.m26489(this, true);
            }
            this.f20487 = true;
        }

        public String getString(int i) throws IOException {
            InputStream newInputStream = newInputStream(i);
            if (newInputStream != null) {
                return DiskLruCache.m26475(newInputStream);
            }
            return null;
        }

        public InputStream newInputStream(int i) throws IOException {
            synchronized (DiskLruCache.this) {
                if (this.f20488.f20494 != this) {
                    throw new IllegalStateException();
                } else if (!this.f20488.f20496) {
                    return null;
                } else {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(this.f20488.getCleanFile(i));
                        return fileInputStream;
                    } catch (FileNotFoundException e) {
                        return null;
                    }
                }
            }
        }

        public OutputStream newOutputStream(int i) throws IOException {
            OutputStream r4;
            FileOutputStream fileOutputStream;
            synchronized (DiskLruCache.this) {
                if (this.f20488.f20494 != this) {
                    throw new IllegalStateException();
                }
                if (!this.f20488.f20496) {
                    this.f20490[i] = true;
                }
                File dirtyFile = this.f20488.getDirtyFile(i);
                try {
                    fileOutputStream = new FileOutputStream(dirtyFile);
                } catch (FileNotFoundException e) {
                    DiskLruCache.this.f20483.mkdirs();
                    try {
                        fileOutputStream = new FileOutputStream(dirtyFile);
                    } catch (FileNotFoundException e2) {
                        r4 = DiskLruCache.f20470;
                    }
                }
                r4 = new FaultHidingOutputStream(fileOutputStream);
            }
            return r4;
        }

        public void set(int i, String str) throws IOException {
            OutputStreamWriter outputStreamWriter = null;
            try {
                OutputStreamWriter outputStreamWriter2 = new OutputStreamWriter(newOutputStream(i), DiskLruCacheUtil.f20510);
                try {
                    outputStreamWriter2.write(str);
                    DiskLruCacheUtil.m26511((Closeable) outputStreamWriter2);
                } catch (Throwable th) {
                    th = th;
                    outputStreamWriter = outputStreamWriter2;
                    DiskLruCacheUtil.m26511((Closeable) outputStreamWriter);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                DiskLruCacheUtil.m26511((Closeable) outputStreamWriter);
                throw th;
            }
        }
    }

    private final class Entry {
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public long f20493;
        /* access modifiers changed from: private */

        /* renamed from: 连任  reason: contains not printable characters */
        public Editor f20494;
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final String f20495;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean f20496;
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public final long[] f20497;

        private Entry(String str) {
            this.f20495 = str;
            this.f20497 = new long[DiskLruCache.this.f20478];
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private IOException m26498(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        /* access modifiers changed from: private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m26506(String[] strArr) throws IOException {
            if (strArr.length != DiskLruCache.this.f20478) {
                throw m26498(strArr);
            }
            int i = 0;
            while (i < strArr.length) {
                try {
                    this.f20497[i] = Long.parseLong(strArr[i]);
                    i++;
                } catch (NumberFormatException e) {
                    throw m26498(strArr);
                }
            }
        }

        public File getCleanFile(int i) {
            return new File(DiskLruCache.this.f20483, this.f20495 + "." + i);
        }

        public File getDirtyFile(int i) {
            return new File(DiskLruCache.this.f20483, this.f20495 + "." + i + ".tmp");
        }

        public String getLengths() throws IOException {
            StringBuilder sb = new StringBuilder();
            for (long append : this.f20497) {
                sb.append(' ').append(append);
            }
            return sb.toString();
        }
    }

    public final class Snapshot implements Closeable {

        /* renamed from: 连任  reason: contains not printable characters */
        private final long[] f20499;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f20500;

        /* renamed from: 麤  reason: contains not printable characters */
        private final InputStream[] f20501;

        /* renamed from: 齉  reason: contains not printable characters */
        private final long f20502;

        private Snapshot(String str, long j, InputStream[] inputStreamArr, long[] jArr) {
            this.f20500 = str;
            this.f20502 = j;
            this.f20501 = inputStreamArr;
            this.f20499 = jArr;
        }

        public void close() {
            for (InputStream r0 : this.f20501) {
                DiskLruCacheUtil.m26511((Closeable) r0);
            }
        }

        public Editor edit() throws IOException {
            return DiskLruCache.this.m26485(this.f20500, this.f20502);
        }

        public InputStream getInputStream(int i) {
            return this.f20501[i];
        }

        public long getLength(int i) {
            return this.f20499[i];
        }

        public String getString(int i) throws IOException {
            return DiskLruCache.m26475(getInputStream(i));
        }
    }

    private DiskLruCache(File file, int i, int i2, long j) {
        this.f20483 = file;
        this.f20474 = i;
        this.f20481 = new File(file, "journal");
        this.f20472 = new File(file, "journal.tmp");
        this.f20473 = new File(file, "journal.bkp");
        this.f20478 = i2;
        this.f20480 = j;
    }

    public static DiskLruCache open(File file, int i, int i2, long j) throws IOException {
        if (j <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("valueCount <= 0");
        } else {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    m26492(file2, file3, false);
                }
            }
            DiskLruCache diskLruCache = new DiskLruCache(file, i, i2, j);
            if (diskLruCache.f20481.exists()) {
                try {
                    diskLruCache.m26476();
                    diskLruCache.m26481();
                    diskLruCache.f20475 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(diskLruCache.f20481, true), DiskLruCacheUtil.f20511));
                    DiskLruCache diskLruCache2 = diskLruCache;
                    return diskLruCache;
                } catch (IOException e) {
                    System.out.println("DiskLruCache " + file + " is corrupt: " + e.getMessage() + ", removing");
                    diskLruCache.delete();
                }
            }
            file.mkdirs();
            DiskLruCache diskLruCache3 = new DiskLruCache(file, i, i2, j);
            diskLruCache3.m26479();
            DiskLruCache diskLruCache4 = diskLruCache3;
            return diskLruCache3;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m26471() {
        if (this.f20475 == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m26472() throws IOException {
        while (this.f20477 > this.f20480) {
            remove((String) this.f20479.entrySet().iterator().next().getKey());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m26474() {
        return this.f20476 >= 2000 && this.f20476 >= this.f20479.size();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m26475(InputStream inputStream) throws IOException {
        return DiskLruCacheUtil.m26510((Reader) new InputStreamReader(inputStream, DiskLruCacheUtil.f20510));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26476() throws IOException {
        int i;
        DiskLruCacheStrictLineReader diskLruCacheStrictLineReader = new DiskLruCacheStrictLineReader(new FileInputStream(this.f20481), DiskLruCacheUtil.f20511);
        try {
            String readLine = diskLruCacheStrictLineReader.readLine();
            String readLine2 = diskLruCacheStrictLineReader.readLine();
            String readLine3 = diskLruCacheStrictLineReader.readLine();
            String readLine4 = diskLruCacheStrictLineReader.readLine();
            String readLine5 = diskLruCacheStrictLineReader.readLine();
            if (!"libcore.io.DiskLruCache".equals(readLine) || !PubnativeRequest.LEGACY_ZONE_ID.equals(readLine2) || !Integer.toString(this.f20474).equals(readLine3) || !Integer.toString(this.f20478).equals(readLine4) || !"".equals(readLine5)) {
                throw new IOException("unexpected journal header: [" + readLine + ", " + readLine2 + ", " + readLine4 + ", " + readLine5 + "]");
            }
            i = 0;
            while (true) {
                m26493(diskLruCacheStrictLineReader.readLine());
                i++;
            }
        } catch (EOFException e) {
            this.f20476 = i - this.f20479.size();
            DiskLruCacheUtil.m26511((Closeable) diskLruCacheStrictLineReader);
        } catch (Throwable th) {
            DiskLruCacheUtil.m26511((Closeable) diskLruCacheStrictLineReader);
            throw th;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26478(String str) {
        if (!f20471.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,64}: \"" + str + "\"");
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public synchronized void m26479() throws IOException {
        if (this.f20475 != null) {
            this.f20475.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f20472), DiskLruCacheUtil.f20511));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(PubnativeRequest.LEGACY_ZONE_ID);
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(Integer.toString(this.f20474));
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(Integer.toString(this.f20478));
            bufferedWriter.write(StringUtils.LF);
            bufferedWriter.write(StringUtils.LF);
            for (Entry next : this.f20479.values()) {
                if (next.f20494 != null) {
                    bufferedWriter.write("DIRTY " + next.f20495 + 10);
                } else {
                    bufferedWriter.write("CLEAN " + next.f20495 + next.getLengths() + 10);
                }
            }
            bufferedWriter.close();
            if (this.f20481.exists()) {
                m26492(this.f20481, this.f20473, true);
            }
            m26492(this.f20472, this.f20481, false);
            this.f20473.delete();
            this.f20475 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f20481, true), DiskLruCacheUtil.f20511));
        } catch (Throwable th) {
            bufferedWriter.close();
            throw th;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26481() throws IOException {
        m26491(this.f20472);
        Iterator<Entry> it2 = this.f20479.values().iterator();
        while (it2.hasNext()) {
            Entry next = it2.next();
            if (next.f20494 == null) {
                for (int i = 0; i < this.f20478; i++) {
                    this.f20477 += next.f20497[i];
                }
            } else {
                Editor unused = next.f20494 = null;
                for (int i2 = 0; i2 < this.f20478; i2++) {
                    m26491(next.getCleanFile(i2));
                    m26491(next.getDirtyFile(i2));
                }
                it2.remove();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0064, code lost:
        if (com.mopub.common.DiskLruCache.Entry.m26503(r1) != null) goto L_0x0020;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.mopub.common.DiskLruCache.Editor m26485(java.lang.String r7, long r8) throws java.io.IOException {
        /*
            r6 = this;
            r0 = 0
            monitor-enter(r6)
            r6.m26471()     // Catch:{ all -> 0x005d }
            r6.m26478((java.lang.String) r7)     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.mopub.common.DiskLruCache$Entry> r2 = r6.f20479     // Catch:{ all -> 0x005d }
            java.lang.Object r1 = r2.get(r7)     // Catch:{ all -> 0x005d }
            com.mopub.common.DiskLruCache$Entry r1 = (com.mopub.common.DiskLruCache.Entry) r1     // Catch:{ all -> 0x005d }
            r2 = -1
            int r2 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x0022
            if (r1 == 0) goto L_0x0020
            long r2 = r1.f20493     // Catch:{ all -> 0x005d }
            int r2 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r2 == 0) goto L_0x0022
        L_0x0020:
            monitor-exit(r6)
            return r0
        L_0x0022:
            if (r1 != 0) goto L_0x0060
            com.mopub.common.DiskLruCache$Entry r1 = new com.mopub.common.DiskLruCache$Entry     // Catch:{ all -> 0x005d }
            r2 = 0
            r1.<init>(r7)     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.mopub.common.DiskLruCache$Entry> r2 = r6.f20479     // Catch:{ all -> 0x005d }
            r2.put(r7, r1)     // Catch:{ all -> 0x005d }
        L_0x002f:
            com.mopub.common.DiskLruCache$Editor r0 = new com.mopub.common.DiskLruCache$Editor     // Catch:{ all -> 0x005d }
            r2 = 0
            r0.<init>(r1)     // Catch:{ all -> 0x005d }
            com.mopub.common.DiskLruCache.Editor unused = r1.f20494 = r0     // Catch:{ all -> 0x005d }
            java.io.Writer r2 = r6.f20475     // Catch:{ all -> 0x005d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005d }
            r3.<init>()     // Catch:{ all -> 0x005d }
            java.lang.String r4 = "DIRTY "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x005d }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ all -> 0x005d }
            r4 = 10
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x005d }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x005d }
            r2.write(r3)     // Catch:{ all -> 0x005d }
            java.io.Writer r2 = r6.f20475     // Catch:{ all -> 0x005d }
            r2.flush()     // Catch:{ all -> 0x005d }
            goto L_0x0020
        L_0x005d:
            r2 = move-exception
            monitor-exit(r6)
            throw r2
        L_0x0060:
            com.mopub.common.DiskLruCache$Editor r2 = r1.f20494     // Catch:{ all -> 0x005d }
            if (r2 == 0) goto L_0x002f
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mopub.common.DiskLruCache.m26485(java.lang.String, long):com.mopub.common.DiskLruCache$Editor");
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m26489(Editor editor, boolean z) throws IOException {
        Entry r2 = editor.f20488;
        if (r2.f20494 != editor) {
            throw new IllegalStateException();
        }
        if (z) {
            if (!r2.f20496) {
                int i = 0;
                while (true) {
                    if (i >= this.f20478) {
                        break;
                    } else if (!editor.f20490[i]) {
                        editor.abort();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i);
                    } else if (!r2.getDirtyFile(i).exists()) {
                        editor.abort();
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        for (int i2 = 0; i2 < this.f20478; i2++) {
            File dirtyFile = r2.getDirtyFile(i2);
            if (!z) {
                m26491(dirtyFile);
            } else if (dirtyFile.exists()) {
                File cleanFile = r2.getCleanFile(i2);
                dirtyFile.renameTo(cleanFile);
                long j = r2.f20497[i2];
                long length = cleanFile.length();
                r2.f20497[i2] = length;
                this.f20477 = (this.f20477 - j) + length;
            }
        }
        this.f20476++;
        Editor unused = r2.f20494 = null;
        if (r2.f20496 || z) {
            boolean unused2 = r2.f20496 = true;
            this.f20475.write("CLEAN " + r2.f20495 + r2.getLengths() + 10);
            if (z) {
                long j2 = this.f20485;
                this.f20485 = 1 + j2;
                long unused3 = r2.f20493 = j2;
            }
        } else {
            this.f20479.remove(r2.f20495);
            this.f20475.write("REMOVE " + r2.f20495 + 10);
        }
        this.f20475.flush();
        if (this.f20477 > this.f20480 || m26474()) {
            this.f20482.submit(this.f20484);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26491(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26492(File file, File file2, boolean z) throws IOException {
        if (z) {
            m26491(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26493(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf == -1) {
            throw new IOException("unexpected journal line: " + str);
        }
        int i = indexOf + 1;
        int indexOf2 = str.indexOf(32, i);
        if (indexOf2 == -1) {
            str2 = str.substring(i);
            if (indexOf == "REMOVE".length() && str.startsWith("REMOVE")) {
                this.f20479.remove(str2);
                return;
            }
        } else {
            str2 = str.substring(i, indexOf2);
        }
        Entry entry = this.f20479.get(str2);
        if (entry == null) {
            entry = new Entry(str2);
            this.f20479.put(str2, entry);
        }
        if (indexOf2 != -1 && indexOf == "CLEAN".length() && str.startsWith("CLEAN")) {
            String[] split = str.substring(indexOf2 + 1).split(StringUtils.SPACE);
            boolean unused = entry.f20496 = true;
            Editor unused2 = entry.f20494 = null;
            entry.m26506(split);
        } else if (indexOf2 == -1 && indexOf == "DIRTY".length() && str.startsWith("DIRTY")) {
            Editor unused3 = entry.f20494 = new Editor(entry);
        } else if (indexOf2 != -1 || indexOf != "READ".length() || !str.startsWith("READ")) {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    public synchronized void close() throws IOException {
        if (this.f20475 != null) {
            Iterator it2 = new ArrayList(this.f20479.values()).iterator();
            while (it2.hasNext()) {
                Entry entry = (Entry) it2.next();
                if (entry.f20494 != null) {
                    entry.f20494.abort();
                }
            }
            m26472();
            this.f20475.close();
            this.f20475 = null;
        }
    }

    public void delete() throws IOException {
        close();
        DiskLruCacheUtil.m26512(this.f20483);
    }

    public Editor edit(String str) throws IOException {
        return m26485(str, -1);
    }

    public synchronized void flush() throws IOException {
        m26471();
        m26472();
        this.f20475.flush();
    }

    public synchronized Snapshot get(String str) throws IOException {
        Snapshot snapshot = null;
        synchronized (this) {
            m26471();
            m26478(str);
            Entry entry = this.f20479.get(str);
            if (entry != null) {
                if (entry.f20496) {
                    InputStream[] inputStreamArr = new InputStream[this.f20478];
                    int i = 0;
                    while (i < this.f20478) {
                        try {
                            inputStreamArr[i] = new FileInputStream(entry.getCleanFile(i));
                            i++;
                        } catch (FileNotFoundException e) {
                            int i2 = 0;
                            while (i2 < this.f20478 && inputStreamArr[i2] != null) {
                                DiskLruCacheUtil.m26511((Closeable) inputStreamArr[i2]);
                                i2++;
                            }
                        }
                    }
                    this.f20476++;
                    this.f20475.append("READ ").append(str).append(10);
                    if (m26474()) {
                        this.f20482.submit(this.f20484);
                    }
                    snapshot = new Snapshot(str, entry.f20493, inputStreamArr, entry.f20497);
                }
            }
        }
        return snapshot;
    }

    public File getDirectory() {
        return this.f20483;
    }

    public synchronized long getMaxSize() {
        return this.f20480;
    }

    public synchronized boolean isClosed() {
        return this.f20475 == null;
    }

    public synchronized boolean remove(String str) throws IOException {
        boolean z;
        m26471();
        m26478(str);
        Entry entry = this.f20479.get(str);
        if (entry == null || entry.f20494 != null) {
            z = false;
        } else {
            int i = 0;
            while (i < this.f20478) {
                File cleanFile = entry.getCleanFile(i);
                if (!cleanFile.exists() || cleanFile.delete()) {
                    this.f20477 -= entry.f20497[i];
                    entry.f20497[i] = 0;
                    i++;
                } else {
                    throw new IOException("failed to delete " + cleanFile);
                }
            }
            this.f20476++;
            this.f20475.append("REMOVE ").append(str).append(10);
            this.f20479.remove(str);
            if (m26474()) {
                this.f20482.submit(this.f20484);
            }
            z = true;
        }
        return z;
    }

    public synchronized void setMaxSize(long j) {
        this.f20480 = j;
        this.f20482.submit(this.f20484);
    }

    public synchronized long size() {
        return this.f20477;
    }
}
