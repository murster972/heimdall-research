package com.mopub.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.mopub.common.event.BaseEvent;
import com.mopub.common.event.Event;
import com.mopub.common.event.MoPubEvents;
import com.mopub.common.util.Drawables;
import com.mopub.mobileads.BaseWebView;
import com.mopub.mobileads.util.WebViews;

public class MoPubBrowser extends Activity {
    public static final String DESTINATION_URL_KEY = "URL";
    public static final String DSP_CREATIVE_ID = "mopub-dsp-creative-id";
    public static final int MOPUB_BROWSER_REQUEST_CODE = 1;

    /* renamed from: ʻ  reason: contains not printable characters */
    private DoubleTimeTracker f20527;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f20528;

    /* renamed from: 连任  reason: contains not printable characters */
    private ImageButton f20529;

    /* renamed from: 靐  reason: contains not printable characters */
    private ImageButton f20530;

    /* renamed from: 麤  reason: contains not printable characters */
    private ImageButton f20531;

    /* renamed from: 齉  reason: contains not printable characters */
    private ImageButton f20532;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public WebView f20533;

    /* renamed from: 靐  reason: contains not printable characters */
    private void m26518() {
        this.f20530.setBackgroundColor(0);
        this.f20530.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (MoPubBrowser.this.f20533.canGoBack()) {
                    MoPubBrowser.this.f20533.goBack();
                }
            }
        });
        this.f20532.setBackgroundColor(0);
        this.f20532.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (MoPubBrowser.this.f20533.canGoForward()) {
                    MoPubBrowser.this.f20533.goForward();
                }
            }
        });
        this.f20531.setBackgroundColor(0);
        this.f20531.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MoPubBrowser.this.f20533.reload();
            }
        });
        this.f20529.setBackgroundColor(0);
        this.f20529.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MoPubBrowser.this.finish();
            }
        });
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private View m26519() {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setOrientation(1);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.addView(relativeLayout);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setId(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        linearLayout2.setLayoutParams(layoutParams);
        linearLayout2.setBackgroundDrawable(Drawables.BACKGROUND.createDrawable(this));
        relativeLayout.addView(linearLayout2);
        this.f20530 = m26522(Drawables.UNLEFT_ARROW.createDrawable(this));
        this.f20532 = m26522(Drawables.UNRIGHT_ARROW.createDrawable(this));
        this.f20531 = m26522(Drawables.REFRESH.createDrawable(this));
        this.f20529 = m26522(Drawables.CLOSE.createDrawable(this));
        linearLayout2.addView(this.f20530);
        linearLayout2.addView(this.f20532);
        linearLayout2.addView(this.f20531);
        linearLayout2.addView(this.f20529);
        this.f20533 = new BaseWebView(this);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams2.addRule(2, 1);
        this.f20533.setLayoutParams(layoutParams2);
        relativeLayout.addView(this.f20533);
        return linearLayout;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26520() {
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().startSync();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ImageButton m26522(Drawable drawable) {
        ImageButton imageButton = new ImageButton(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2, 1.0f);
        layoutParams.gravity = 16;
        imageButton.setLayoutParams(layoutParams);
        imageButton.setImageDrawable(drawable);
        return imageButton;
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    /* renamed from: 龘  reason: contains not printable characters */
    private void m26523() {
        WebSettings settings = this.f20533.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        this.f20528 = getIntent().getStringExtra(DSP_CREATIVE_ID);
        this.f20533.loadUrl(getIntent().getStringExtra(DESTINATION_URL_KEY));
        this.f20533.setWebViewClient(new BrowserWebViewClient(this));
        this.f20533.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView webView, int i) {
                MoPubBrowser.this.setTitle("Loading...");
                MoPubBrowser.this.setProgress(i * 100);
                if (i == 100) {
                    MoPubBrowser.this.setTitle(webView.getUrl());
                }
            }
        });
    }

    public void finish() {
        ((ViewGroup) getWindow().getDecorView()).removeAllViews();
        super.finish();
    }

    public ImageButton getBackButton() {
        return this.f20530;
    }

    public ImageButton getCloseButton() {
        return this.f20529;
    }

    public ImageButton getForwardButton() {
        return this.f20532;
    }

    public ImageButton getRefreshButton() {
        return this.f20531;
    }

    public WebView getWebView() {
        return this.f20533;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setResult(-1);
        getWindow().requestFeature(2);
        getWindow().setFeatureInt(2, -1);
        setContentView(m26519());
        this.f20527 = new DoubleTimeTracker();
        m26523();
        m26518();
        m26520();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f20533.destroy();
        this.f20533 = null;
        MoPubEvents.log(new Event.Builder(BaseEvent.Name.AD_DWELL_TIME, BaseEvent.Category.AD_INTERACTIONS, BaseEvent.SamplingRate.AD_INTERACTIONS.getSamplingRate()).withDspCreativeId(this.f20528).withPerformanceDurationMs(Double.valueOf(this.f20527.getInterval())).build());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        CookieSyncManager.getInstance().stopSync();
        WebViews.onPause(this.f20533, isFinishing());
        this.f20527.pause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        CookieSyncManager.getInstance().startSync();
        WebViews.onResume(this.f20533);
        this.f20527.start();
    }
}
