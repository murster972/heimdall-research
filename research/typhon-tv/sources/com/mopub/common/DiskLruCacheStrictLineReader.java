package com.mopub.common;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

class DiskLruCacheStrictLineReader implements Closeable {

    /* renamed from: 连任  reason: contains not printable characters */
    private int f20504;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final Charset f20505;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f20506;

    /* renamed from: 齉  reason: contains not printable characters */
    private byte[] f20507;

    /* renamed from: 龘  reason: contains not printable characters */
    private final InputStream f20508;

    public DiskLruCacheStrictLineReader(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw new NullPointerException();
        } else if (i < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        } else if (!charset.equals(DiskLruCacheUtil.f20511)) {
            throw new IllegalArgumentException("Unsupported encoding");
        } else {
            this.f20508 = inputStream;
            this.f20505 = charset;
            this.f20507 = new byte[i];
        }
    }

    public DiskLruCacheStrictLineReader(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26509() throws IOException {
        int read = this.f20508.read(this.f20507, 0, this.f20507.length);
        if (read == -1) {
            throw new EOFException();
        }
        this.f20506 = 0;
        this.f20504 = read;
    }

    public void close() throws IOException {
        synchronized (this.f20508) {
            if (this.f20507 != null) {
                this.f20507 = null;
                this.f20508.close();
            }
        }
    }

    public String readLine() throws IOException {
        int i;
        String byteArrayOutputStream;
        synchronized (this.f20508) {
            if (this.f20507 == null) {
                throw new IOException("LineReader is closed");
            }
            if (this.f20506 >= this.f20504) {
                m26509();
            }
            int i2 = this.f20506;
            while (true) {
                if (i2 == this.f20504) {
                    AnonymousClass1 r2 = new ByteArrayOutputStream((this.f20504 - this.f20506) + 80) {
                        public String toString() {
                            try {
                                return new String(this.buf, 0, (this.count <= 0 || this.buf[this.count + -1] != 13) ? this.count : this.count - 1, DiskLruCacheStrictLineReader.this.f20505.name());
                            } catch (UnsupportedEncodingException e) {
                                throw new AssertionError(e);
                            }
                        }
                    };
                    loop1:
                    while (true) {
                        r2.write(this.f20507, this.f20506, this.f20504 - this.f20506);
                        this.f20504 = -1;
                        m26509();
                        i = this.f20506;
                        while (true) {
                            if (i != this.f20504) {
                                if (this.f20507[i] == 10) {
                                    break loop1;
                                }
                                i++;
                            }
                        }
                    }
                    if (i != this.f20506) {
                        r2.write(this.f20507, this.f20506, i - this.f20506);
                    }
                    this.f20506 = i + 1;
                    byteArrayOutputStream = r2.toString();
                } else if (this.f20507[i2] == 10) {
                    byteArrayOutputStream = new String(this.f20507, this.f20506, ((i2 == this.f20506 || this.f20507[i2 + -1] != 13) ? i2 : i2 - 1) - this.f20506, this.f20505.name());
                    this.f20506 = i2 + 1;
                } else {
                    i2++;
                }
            }
        }
        return byteArrayOutputStream;
    }
}
