package com.mopub.common;

import android.os.Build;
import com.mopub.network.AdResponse;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

public class AdReport implements Serializable {
    private static final long serialVersionUID = 1;
    private final AdResponse mAdResponse;
    private final String mAdUnitId;
    private final Locale mDeviceLocale;
    private final String mDeviceModel;
    private final String mSdkVersion;
    private final String mUdid;

    public AdReport(String str, ClientMetadata clientMetadata, AdResponse adResponse) {
        this.mAdUnitId = str;
        this.mSdkVersion = clientMetadata.getSdkVersion();
        this.mDeviceModel = clientMetadata.getDeviceModel();
        this.mDeviceLocale = clientMetadata.getDeviceLocale();
        this.mUdid = clientMetadata.getDeviceId();
        this.mAdResponse = adResponse;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m26444(long j) {
        if (j != -1) {
            return new SimpleDateFormat("M/d/yy hh:mm:ss a z", Locale.US).format(new Date(j));
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26445(StringBuilder sb, String str, String str2) {
        sb.append(str);
        sb.append(" : ");
        sb.append(str2);
        sb.append(StringUtils.LF);
    }

    public String getDspCreativeId() {
        return this.mAdResponse.getDspCreativeId();
    }

    public String getResponseString() {
        return this.mAdResponse.getStringBody();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        m26445(sb, "sdk_version", this.mSdkVersion);
        m26445(sb, "creative_id", this.mAdResponse.getDspCreativeId());
        m26445(sb, "platform_version", Integer.toString(Build.VERSION.SDK_INT));
        m26445(sb, "device_model", this.mDeviceModel);
        m26445(sb, "ad_unit_id", this.mAdUnitId);
        m26445(sb, "device_locale", this.mDeviceLocale == null ? null : this.mDeviceLocale.toString());
        m26445(sb, "device_id", this.mUdid);
        m26445(sb, "network_type", this.mAdResponse.getNetworkType());
        m26445(sb, "platform", AbstractSpiCall.ANDROID_CLIENT_TYPE);
        m26445(sb, "timestamp", m26444(this.mAdResponse.getTimestamp()));
        m26445(sb, "ad_type", this.mAdResponse.getAdType());
        Object width = this.mAdResponse.getWidth();
        Object height = this.mAdResponse.getHeight();
        StringBuilder append = new StringBuilder().append("{");
        if (width == null) {
            width = "0";
        }
        StringBuilder append2 = append.append(width).append(", ");
        if (height == null) {
            height = "0";
        }
        m26445(sb, "ad_size", append2.append(height).append("}").toString());
        return sb.toString();
    }
}
