package com.mopub.common;

import com.mopub.common.util.ResponseHeader;
import com.mopub.common.util.Streams;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

public class DownloadResponse {

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f20517;

    /* renamed from: 麤  reason: contains not printable characters */
    private byte[] f20518 = new byte[0];

    /* renamed from: 齉  reason: contains not printable characters */
    private final Header[] f20519;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f20520;

    public DownloadResponse(HttpResponse httpResponse) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedInputStream bufferedInputStream = null;
        try {
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                BufferedInputStream bufferedInputStream2 = new BufferedInputStream(entity.getContent());
                try {
                    Streams.copyContent(bufferedInputStream2, byteArrayOutputStream);
                    this.f20518 = byteArrayOutputStream.toByteArray();
                    bufferedInputStream = bufferedInputStream2;
                } catch (Throwable th) {
                    th = th;
                    bufferedInputStream = bufferedInputStream2;
                    Streams.closeStream(bufferedInputStream);
                    Streams.closeStream(byteArrayOutputStream);
                    throw th;
                }
            }
            Streams.closeStream(bufferedInputStream);
            Streams.closeStream(byteArrayOutputStream);
            this.f20520 = httpResponse.getStatusLine().getStatusCode();
            this.f20517 = (long) this.f20518.length;
            this.f20519 = httpResponse.getAllHeaders();
        } catch (Throwable th2) {
            th = th2;
            Streams.closeStream(bufferedInputStream);
            Streams.closeStream(byteArrayOutputStream);
            throw th;
        }
    }

    public byte[] getByteArray() {
        return this.f20518;
    }

    public long getContentLength() {
        return this.f20517;
    }

    public String getFirstHeader(ResponseHeader responseHeader) {
        for (Header header : this.f20519) {
            if (header.getName().equalsIgnoreCase(responseHeader.getKey())) {
                return header.getValue();
            }
        }
        return null;
    }

    public int getStatusCode() {
        return this.f20520;
    }
}
