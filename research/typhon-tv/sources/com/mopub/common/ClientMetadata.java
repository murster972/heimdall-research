package com.mopub.common;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import java.util.Locale;

public class ClientMetadata {

    /* renamed from: 龘  reason: contains not printable characters */
    private static volatile ClientMetadata f5659;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f5660;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f5661;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f5662;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final ConnectivityManager f5663;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f5664;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f5665 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final Context f5666;

    /* renamed from: ˉ  reason: contains not printable characters */
    private String f5667;

    /* renamed from: ˊ  reason: contains not printable characters */
    private String f5668;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f5669;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f5670 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final String f5671;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final String f5672;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final String f5673;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String f5674;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f5675;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f5676;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f5677;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String f5678;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private String f5679;

    public enum MoPubNetworkType {
        UNKNOWN(0),
        ETHERNET(1),
        WIFI(2),
        MOBILE(3);
        
        private final int mId;

        private MoPubNetworkType(int i) {
            this.mId = i;
        }

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public static MoPubNetworkType m26460(int i) {
            switch (i) {
                case 0:
                case 2:
                case 3:
                case 4:
                case 5:
                    return MOBILE;
                case 1:
                    return WIFI;
                case 9:
                    return ETHERNET;
                default:
                    return UNKNOWN;
            }
        }

        public int getId() {
            return this.mId;
        }

        public String toString() {
            return Integer.toString(this.mId);
        }
    }

    public ClientMetadata(Context context) {
        this.f5666 = context.getApplicationContext();
        this.f5663 = (ConnectivityManager) this.f5666.getSystemService("connectivity");
        this.f5674 = Build.MANUFACTURER;
        this.f5660 = Build.MODEL;
        this.f5661 = Build.PRODUCT;
        this.f5662 = Build.VERSION.RELEASE;
        this.f5671 = MoPub.SDK_VERSION;
        this.f5672 = m6160(this.f5666);
        PackageManager packageManager = this.f5666.getPackageManager();
        ApplicationInfo applicationInfo = null;
        this.f5673 = context.getPackageName();
        try {
            applicationInfo = packageManager.getApplicationInfo(this.f5673, 0);
        } catch (PackageManager.NameNotFoundException e) {
        }
        if (applicationInfo != null) {
            this.f5667 = (String) packageManager.getApplicationLabel(applicationInfo);
        }
        TelephonyManager telephonyManager = (TelephonyManager) this.f5666.getSystemService("phone");
        this.f5664 = telephonyManager.getNetworkOperator();
        this.f5675 = telephonyManager.getNetworkOperator();
        if (telephonyManager.getPhoneType() == 2 && telephonyManager.getSimState() == 5) {
            this.f5664 = telephonyManager.getSimOperator();
            this.f5678 = telephonyManager.getSimOperator();
        }
        this.f5677 = telephonyManager.getNetworkCountryIso();
        this.f5676 = telephonyManager.getSimCountryIso();
        try {
            this.f5679 = telephonyManager.getNetworkOperatorName();
            if (telephonyManager.getSimState() == 5) {
                this.f5668 = telephonyManager.getSimOperatorName();
            }
        } catch (SecurityException e2) {
            this.f5679 = null;
            this.f5668 = null;
        }
        m6161();
        if (!this.f5665) {
            this.f5669 = m6159(this.f5666);
        }
    }

    @VisibleForTesting
    public static void clearForTesting() {
        f5659 = null;
    }

    public static ClientMetadata getInstance() {
        ClientMetadata clientMetadata = f5659;
        if (clientMetadata == null) {
            synchronized (ClientMetadata.class) {
                clientMetadata = f5659;
            }
        }
        return clientMetadata;
    }

    public static ClientMetadata getInstance(Context context) {
        ClientMetadata clientMetadata = f5659;
        if (clientMetadata == null) {
            synchronized (ClientMetadata.class) {
                try {
                    clientMetadata = f5659;
                    if (clientMetadata == null) {
                        ClientMetadata clientMetadata2 = new ClientMetadata(context);
                        try {
                            f5659 = clientMetadata2;
                            clientMetadata = clientMetadata2;
                        } catch (Throwable th) {
                            th = th;
                            ClientMetadata clientMetadata3 = clientMetadata2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return clientMetadata;
    }

    @Deprecated
    @VisibleForTesting
    public static void setInstance(ClientMetadata clientMetadata) {
        synchronized (ClientMetadata.class) {
            f5659 = clientMetadata;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m6159(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        return "sha:" + (string == null ? "" : Utils.sha1(string));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m6160(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            return null;
        }
    }

    public MoPubNetworkType getActiveNetworkType() {
        int i = -1;
        if (DeviceUtils.isPermissionGranted(this.f5666, "android.permission.ACCESS_NETWORK_STATE")) {
            NetworkInfo activeNetworkInfo = this.f5663.getActiveNetworkInfo();
            i = activeNetworkInfo != null ? activeNetworkInfo.getType() : -1;
        }
        return MoPubNetworkType.m26460(i);
    }

    public String getAppName() {
        return this.f5667;
    }

    public String getAppPackageName() {
        return this.f5673;
    }

    public String getAppVersion() {
        return this.f5672;
    }

    public float getDensity() {
        return this.f5666.getResources().getDisplayMetrics().density;
    }

    public Point getDeviceDimensions() {
        return Preconditions.NoThrow.checkNotNull(this.f5666) ? DeviceUtils.getDeviceDimensions(this.f5666) : new Point(0, 0);
    }

    public synchronized String getDeviceId() {
        return this.f5669;
    }

    public Locale getDeviceLocale() {
        return this.f5666.getResources().getConfiguration().locale;
    }

    public String getDeviceManufacturer() {
        return this.f5674;
    }

    public String getDeviceModel() {
        return this.f5660;
    }

    public String getDeviceOsVersion() {
        return this.f5662;
    }

    public String getDeviceProduct() {
        return this.f5661;
    }

    public int getDeviceScreenHeightDip() {
        return Dips.screenHeightAsIntDips(this.f5666);
    }

    public int getDeviceScreenWidthDip() {
        return Dips.screenWidthAsIntDips(this.f5666);
    }

    public String getIsoCountryCode() {
        return this.f5677;
    }

    public String getNetworkOperator() {
        return this.f5675;
    }

    public String getNetworkOperatorForUrl() {
        return this.f5664;
    }

    public String getNetworkOperatorName() {
        return this.f5679;
    }

    public String getOrientationString() {
        int i = this.f5666.getResources().getConfiguration().orientation;
        return i == 1 ? TtmlNode.TAG_P : i == 2 ? "l" : i == 3 ? "s" : "u";
    }

    public String getSdkVersion() {
        return this.f5671;
    }

    public String getSimIsoCountryCode() {
        return this.f5676;
    }

    public String getSimOperator() {
        return this.f5678;
    }

    public String getSimOperatorName() {
        return this.f5668;
    }

    public synchronized boolean isAdvertisingInfoSet() {
        return this.f5665;
    }

    public synchronized boolean isDoNotTrackSet() {
        return this.f5670;
    }

    public synchronized void setAdvertisingInfo(String str, boolean z) {
        this.f5669 = "ifa:" + str;
        this.f5670 = z;
        this.f5665 = true;
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6161() {
        try {
            ContentResolver contentResolver = this.f5666.getContentResolver();
            int i = Settings.Secure.getInt(contentResolver, "limit_ad_tracking", -1);
            String string = Settings.Secure.getString(contentResolver, "advertising_id");
            if (i != -1 && !TextUtils.isEmpty(string)) {
                setAdvertisingInfo(string, i != 0);
            }
        } catch (Exception e) {
            MoPubLog.e("Failed to retrieve Amazon advertising info", e);
        }
    }
}
