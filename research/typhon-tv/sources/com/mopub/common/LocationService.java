package com.mopub.common;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import com.mopub.common.MoPub;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import java.math.BigDecimal;

public class LocationService {

    /* renamed from: 齉  reason: contains not printable characters */
    private static volatile LocationService f5682;
    @VisibleForTesting

    /* renamed from: 靐  reason: contains not printable characters */
    long f5683;
    @VisibleForTesting

    /* renamed from: 龘  reason: contains not printable characters */
    Location f5684;

    public enum LocationAwareness {
        NORMAL,
        TRUNCATED,
        DISABLED;

        @Deprecated
        public static LocationAwareness fromMoPubLocationAwareness(MoPub.LocationAwareness locationAwareness) {
            return locationAwareness == MoPub.LocationAwareness.DISABLED ? DISABLED : locationAwareness == MoPub.LocationAwareness.TRUNCATED ? TRUNCATED : NORMAL;
        }

        @Deprecated
        public MoPub.LocationAwareness getNewLocationAwareness() {
            return this == TRUNCATED ? MoPub.LocationAwareness.TRUNCATED : this == DISABLED ? MoPub.LocationAwareness.DISABLED : MoPub.LocationAwareness.NORMAL;
        }
    }

    public enum ValidLocationProvider {
        NETWORK("network"),
        GPS("gps");
        
        final String name;

        private ValidLocationProvider(String str) {
            this.name = str;
        }

        /* access modifiers changed from: private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m26516(Context context) {
            switch (this) {
                case NETWORK:
                    return DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_FINE_LOCATION") || DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_COARSE_LOCATION");
                case GPS:
                    return DeviceUtils.isPermissionGranted(context, "android.permission.ACCESS_FINE_LOCATION");
                default:
                    return false;
            }
        }

        public String toString() {
            return this.name;
        }
    }

    private LocationService() {
    }

    @Deprecated
    @VisibleForTesting
    public static void clearLastKnownLocation() {
        m6171().f5684 = null;
    }

    public static Location getLastKnownLocation(Context context, int i, MoPub.LocationAwareness locationAwareness) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(locationAwareness);
        if (locationAwareness == MoPub.LocationAwareness.DISABLED) {
            return null;
        }
        LocationService r1 = m6171();
        if (m6168()) {
            return r1.f5684;
        }
        Location r3 = m6170(m6169(context, ValidLocationProvider.GPS), m6169(context, ValidLocationProvider.NETWORK));
        if (locationAwareness == MoPub.LocationAwareness.TRUNCATED) {
            m6172(r3, i);
        }
        r1.f5684 = r3;
        r1.f5683 = SystemClock.elapsedRealtime();
        return r3;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m6168() {
        LocationService r0 = m6171();
        return r0.f5684 != null && SystemClock.elapsedRealtime() - r0.f5683 <= MoPub.getMinimumLocationRefreshTimeMillis();
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static Location m6169(Context context, ValidLocationProvider validLocationProvider) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(validLocationProvider);
        if (!validLocationProvider.m26516(context)) {
            return null;
        }
        try {
            return ((LocationManager) context.getSystemService("location")).getLastKnownLocation(validLocationProvider.toString());
        } catch (SecurityException e) {
            MoPubLog.d("Failed to retrieve location from " + validLocationProvider.toString() + " provider: access appears to be disabled.");
            return null;
        } catch (IllegalArgumentException e2) {
            MoPubLog.d("Failed to retrieve location: device has no " + validLocationProvider.toString() + " location provider.");
            return null;
        } catch (NullPointerException e3) {
            MoPubLog.d("Failed to retrieve location: device has no " + validLocationProvider.toString() + " location provider.");
            return null;
        }
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static Location m6170(Location location, Location location2) {
        return location == null ? location2 : (location2 == null || location.getTime() > location2.getTime()) ? location : location2;
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static LocationService m6171() {
        LocationService locationService = f5682;
        if (locationService == null) {
            synchronized (LocationService.class) {
                try {
                    locationService = f5682;
                    if (locationService == null) {
                        LocationService locationService2 = new LocationService();
                        try {
                            f5682 = locationService2;
                            locationService = locationService2;
                        } catch (Throwable th) {
                            th = th;
                            LocationService locationService3 = locationService2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
        return locationService;
    }

    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    static void m6172(Location location, int i) {
        if (location != null && i >= 0) {
            location.setLatitude(BigDecimal.valueOf(location.getLatitude()).setScale(i, 5).doubleValue());
            location.setLongitude(BigDecimal.valueOf(location.getLongitude()).setScale(i, 5).doubleValue());
        }
    }
}
