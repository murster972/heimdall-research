package com.mopub.common;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.mopub.common.ClientMetadata;
import com.mopub.common.util.DateAndTime;
import net.pubnative.library.request.PubnativeRequest;

public abstract class AdUrlGenerator extends BaseUrlGenerator {

    /* renamed from: 靐  reason: contains not printable characters */
    protected String f5654;

    /* renamed from: 麤  reason: contains not printable characters */
    protected Location f5655;

    /* renamed from: 齉  reason: contains not printable characters */
    protected String f5656;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Context f5657;

    public AdUrlGenerator(Context context) {
        this.f5657 = context;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private int m6141(String str) {
        return Math.min(3, str.length());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m6142(Location location) {
        Preconditions.checkNotNull(location);
        return (int) (System.currentTimeMillis() - location.getTime());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m6143(String str, ClientMetadata.MoPubNetworkType moPubNetworkType) {
        m26450(str, moPubNetworkType.toString());
    }

    public AdUrlGenerator withAdUnitId(String str) {
        this.f5654 = str;
        return this;
    }

    @Deprecated
    public AdUrlGenerator withFacebookSupported(boolean z) {
        return this;
    }

    public AdUrlGenerator withKeywords(String str) {
        this.f5656 = str;
        return this;
    }

    public AdUrlGenerator withLocation(Location location) {
        this.f5655 = location;
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m6144(String str) {
        m26450("mcc", str == null ? "" : str.substring(0, m6141(str)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m6145(String str) {
        m26450("mnc", str == null ? "" : str.substring(m6141(str)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m6146(String str) {
        m26450("iso", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m6147(String str) {
        m26450("cn", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m6148(String str) {
        if (!TextUtils.isEmpty(str)) {
            m26450("bundle", str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m6149(String str) {
        m26450("o", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m6150(String str) {
        m26450("nv", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m6151(String str) {
        m26450("z", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m6152(String str) {
        m26450("q", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6153(float f) {
        m26450("sc_a", "" + f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6154(Location location) {
        Location location2 = location;
        Location lastKnownLocation = LocationService.getLastKnownLocation(this.f5657, MoPub.getLocationPrecision(), MoPub.getLocationAwareness());
        if (lastKnownLocation != null && (location == null || lastKnownLocation.getTime() >= location.getTime())) {
            location2 = lastKnownLocation;
        }
        if (location2 != null) {
            m26450("ll", location2.getLatitude() + "," + location2.getLongitude());
            m26450("lla", String.valueOf((int) location2.getAccuracy()));
            m26450("llf", String.valueOf(m6142(location2)));
            if (location2 == lastKnownLocation) {
                m26450("llsdk", PubnativeRequest.LEGACY_ZONE_ID);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6155(ClientMetadata.MoPubNetworkType moPubNetworkType) {
        m6143("ct", moPubNetworkType);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6156(ClientMetadata clientMetadata) {
        m6157(this.f5654);
        m6150(clientMetadata.getSdkVersion());
        m26455(clientMetadata.getDeviceManufacturer(), clientMetadata.getDeviceModel(), clientMetadata.getDeviceProduct());
        m6148(clientMetadata.getAppPackageName());
        m6152(this.f5656);
        m6154(this.f5655);
        m6151(DateAndTime.getTimeZoneOffsetString());
        m6149(clientMetadata.getOrientationString());
        m26453(clientMetadata.getDeviceDimensions());
        m6153(clientMetadata.getDensity());
        String networkOperatorForUrl = clientMetadata.getNetworkOperatorForUrl();
        m6144(networkOperatorForUrl);
        m6145(networkOperatorForUrl);
        m6146(clientMetadata.getIsoCountryCode());
        m6147(clientMetadata.getNetworkOperatorName());
        m6155(clientMetadata.getActiveNetworkType());
        m26447(clientMetadata.getAppVersion());
        m26449();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6157(String str) {
        m26450("id", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6158(boolean z) {
        if (z) {
            m26450("mr", PubnativeRequest.LEGACY_ZONE_ID);
        }
    }
}
