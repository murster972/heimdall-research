package com.mopub.common;

import android.content.Context;
import android.os.AsyncTask;
import com.mopub.common.factories.MethodBuilderFactory;
import com.mopub.common.util.AsyncTasks;
import com.mopub.common.util.Reflection;
import java.lang.ref.WeakReference;

public class GpsHelper {
    public static final String ADVERTISING_ID_KEY = "advertisingId";
    public static final int GOOGLE_PLAY_SUCCESS_CODE = 0;
    public static final String IS_LIMIT_AD_TRACKING_ENABLED_KEY = "isLimitAdTrackingEnabled";
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static String f5680 = "com.google.android.gms.ads.identifier.AdvertisingIdClient";

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f5681 = "com.google.android.gms.common.GooglePlayServicesUtil";

    public static class AdvertisingInfo {
        public final String advertisingId;
        public final boolean limitAdTracking;

        public AdvertisingInfo(String str, boolean z) {
            this.advertisingId = str;
            this.limitAdTracking = z;
        }
    }

    private static class FetchAdvertisingInfoTask extends AsyncTask<Void, Void, Void> {

        /* renamed from: 靐  reason: contains not printable characters */
        private WeakReference<GpsHelperListener> f20521;

        /* renamed from: 龘  reason: contains not printable characters */
        private WeakReference<Context> f20522;

        public FetchAdvertisingInfoTask(Context context, GpsHelperListener gpsHelperListener) {
            this.f20522 = new WeakReference<>(context);
            this.f20521 = new WeakReference<>(gpsHelperListener);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Void doInBackground(Void... voidArr) {
            Object execute;
            try {
                Context context = (Context) this.f20522.get();
                if (!(context == null || (execute = MethodBuilderFactory.create((Object) null, "getAdvertisingIdInfo").setStatic(Class.forName(GpsHelper.f5680)).addParam(Context.class, context).execute()) == null)) {
                    GpsHelper.m6165(context, execute);
                }
            } catch (Exception e) {
            }
            return null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void onPostExecute(Void voidR) {
            GpsHelperListener gpsHelperListener = (GpsHelperListener) this.f20521.get();
            if (gpsHelperListener != null) {
                gpsHelperListener.onFetchAdInfoCompleted();
            }
        }
    }

    public interface GpsHelperListener {
        void onFetchAdInfoCompleted();
    }

    public static void fetchAdvertisingInfoAsync(Context context, GpsHelperListener gpsHelperListener) {
        boolean isPlayServicesAvailable = isPlayServicesAvailable(context);
        if (!isPlayServicesAvailable || m6166(context)) {
            if (gpsHelperListener != null) {
                gpsHelperListener.onFetchAdInfoCompleted();
            }
            if (isPlayServicesAvailable) {
                m6164(context, (GpsHelperListener) null);
                return;
            }
            return;
        }
        m6164(context, gpsHelperListener);
    }

    public static AdvertisingInfo fetchAdvertisingInfoSync(Context context) {
        if (context == null) {
            return null;
        }
        try {
            Object execute = MethodBuilderFactory.create((Object) null, "getAdvertisingIdInfo").setStatic(Class.forName(f5680)).addParam(Context.class, context).execute();
            return new AdvertisingInfo(m6163(execute, (String) null), m6167(execute, false));
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isLimitAdTrackingEnabled(Context context) {
        if (isPlayServicesAvailable(context)) {
            return SharedPreferencesHelper.getSharedPreferences(context).getBoolean(IS_LIMIT_AD_TRACKING_ENABLED_KEY, false);
        }
        return false;
    }

    public static boolean isPlayServicesAvailable(Context context) {
        try {
            Object execute = MethodBuilderFactory.create((Object) null, "isGooglePlayServicesAvailable").setStatic(Class.forName(f5681)).addParam(Context.class, context).execute();
            return execute != null && ((Integer) execute).intValue() == 0;
        } catch (Exception e) {
            return false;
        }
    }

    @Deprecated
    public static void setClassNamesForTesting() {
        f5681 = "java.lang.Class";
        f5680 = "java.lang.Class";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m6163(Object obj, String str) {
        try {
            return (String) MethodBuilderFactory.create(obj, "getId").execute();
        } catch (Exception e) {
            return str;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m6164(Context context, GpsHelperListener gpsHelperListener) {
        if (Reflection.classFound(f5680)) {
            try {
                AsyncTasks.safeExecuteOnExecutor(new FetchAdvertisingInfoTask(context, gpsHelperListener), new Void[0]);
            } catch (Exception e) {
                if (gpsHelperListener != null) {
                    gpsHelperListener.onFetchAdInfoCompleted();
                }
            }
        } else if (gpsHelperListener != null) {
            gpsHelperListener.onFetchAdInfoCompleted();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6165(Context context, Object obj) {
        ClientMetadata.getInstance(context).setAdvertisingInfo(m6163(obj, (String) null), m6167(obj, false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m6166(Context context) {
        return ClientMetadata.getInstance(context).isAdvertisingInfoSet();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m6167(Object obj, boolean z) {
        try {
            Boolean bool = (Boolean) MethodBuilderFactory.create(obj, IS_LIMIT_AD_TRACKING_ENABLED_KEY).execute();
            return bool != null ? bool.booleanValue() : z;
        } catch (Exception e) {
            return z;
        }
    }
}
