package com.mopub.common;

import android.os.SystemClock;
import com.mopub.common.logging.MoPubLog;

public class DoubleTimeTracker {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile State f20512;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f20513;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f20514;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Clock f20515;

    public interface Clock {
        long elapsedRealTime();
    }

    private enum State {
        STARTED,
        PAUSED
    }

    private static class SystemClockClock implements Clock {
        private SystemClockClock() {
        }

        public long elapsedRealTime() {
            return SystemClock.elapsedRealtime();
        }
    }

    public DoubleTimeTracker() {
        this(new SystemClockClock());
    }

    @VisibleForTesting
    public DoubleTimeTracker(Clock clock) {
        this.f20515 = clock;
        this.f20512 = State.PAUSED;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized long m26513() {
        return this.f20512 == State.PAUSED ? 0 : this.f20515.elapsedRealTime() - this.f20514;
    }

    public synchronized double getInterval() {
        return (double) (this.f20513 + m26513());
    }

    public synchronized void pause() {
        if (this.f20512 == State.PAUSED) {
            MoPubLog.v("DoubleTimeTracker already paused.");
        } else {
            this.f20513 += m26513();
            this.f20514 = 0;
            this.f20512 = State.PAUSED;
        }
    }

    public synchronized void start() {
        if (this.f20512 == State.STARTED) {
            MoPubLog.v("DoubleTimeTracker already started.");
        } else {
            this.f20512 = State.STARTED;
            this.f20514 = this.f20515.elapsedRealTime();
        }
    }
}
