package com.mopub.common.logging;

import android.util.Log;
import com.mopub.common.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;

public class MoPubLog {
    public static final String LOGGER_NAMESPACE = "com.mopub";

    /* renamed from: 靐  reason: contains not printable characters */
    private static final MoPubLogHandler f20654 = new MoPubLogHandler();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Logger f20655 = Logger.getLogger(LOGGER_NAMESPACE);

    private static final class MoPubLogHandler extends Handler {

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Map<Level, Integer> f20656 = new HashMap(7);

        static {
            f20656.put(Level.FINEST, 2);
            f20656.put(Level.FINER, 2);
            f20656.put(Level.FINE, 2);
            f20656.put(Level.CONFIG, 3);
            f20656.put(Level.INFO, 4);
            f20656.put(Level.WARNING, 5);
            f20656.put(Level.SEVERE, 6);
        }

        private MoPubLogHandler() {
        }

        public void close() {
        }

        public void flush() {
        }

        public void publish(LogRecord logRecord) {
            if (isLoggable(logRecord)) {
                int intValue = f20656.containsKey(logRecord.getLevel()) ? f20656.get(logRecord.getLevel()).intValue() : 2;
                String str = logRecord.getMessage() + StringUtils.LF;
                Throwable thrown = logRecord.getThrown();
                if (thrown != null) {
                    str = str + Log.getStackTraceString(thrown);
                }
                Log.println(intValue, "MoPub", str);
            }
        }
    }

    static {
        f20655.setUseParentHandlers(false);
        f20655.setLevel(Level.ALL);
        f20654.setLevel(Level.FINE);
        LogManager.getLogManager().addLogger(f20655);
        m26592(f20655, f20654);
    }

    private MoPubLog() {
    }

    public static void c(String str) {
        c(str, (Throwable) null);
    }

    public static void c(String str, Throwable th) {
        f20655.log(Level.FINEST, str, th);
    }

    public static void d(String str) {
    }

    public static void d(String str, Throwable th) {
        f20655.log(Level.CONFIG, str, th);
    }

    public static void e(String str) {
        e(str, (Throwable) null);
    }

    public static void e(String str, Throwable th) {
        f20655.log(Level.SEVERE, str, th);
    }

    public static void i(String str) {
        i(str, (Throwable) null);
    }

    public static void i(String str, Throwable th) {
        f20655.log(Level.INFO, str, th);
    }

    @VisibleForTesting
    public static void setSdkHandlerLevel(Level level) {
        f20654.setLevel(level);
    }

    public static void v(String str) {
        v(str, (Throwable) null);
    }

    public static void v(String str, Throwable th) {
        f20655.log(Level.FINE, str, th);
    }

    public static void w(String str) {
        w(str, (Throwable) null);
    }

    public static void w(String str, Throwable th) {
        f20655.log(Level.WARNING, str, th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m26592(Logger logger, Handler handler) {
        Handler[] handlers = logger.getHandlers();
        int length = handlers.length;
        int i = 0;
        while (i < length) {
            if (!handlers[i].equals(handler)) {
                i++;
            } else {
                return;
            }
        }
        logger.addHandler(handler);
    }
}
