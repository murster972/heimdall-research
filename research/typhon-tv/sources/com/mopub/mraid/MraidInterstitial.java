package com.mopub.mraid;

import com.mopub.common.DataKeys;
import com.mopub.mobileads.CustomEventInterstitial;
import com.mopub.mobileads.MraidActivity;
import com.mopub.mobileads.ResponseBodyInterstitial;
import java.util.Map;

class MraidInterstitial extends ResponseBodyInterstitial {

    /* renamed from: 麤  reason: contains not printable characters */
    protected String f21042;

    MraidInterstitial() {
    }

    public void showInterstitial() {
        MraidActivity.start(this.f20838, this.f20835, this.f21042, this.f20837);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26983(CustomEventInterstitial.CustomEventInterstitialListener customEventInterstitialListener) {
        MraidActivity.preRenderHtml(this.f20838, customEventInterstitialListener, this.f21042);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26984(Map<String, String> map) {
        this.f21042 = map.get(DataKeys.HTML_RESPONSE_BODY_KEY);
    }
}
