package com.mopub.mraid;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.Pinkamena;
import com.mopub.common.AdReport;
import com.mopub.common.AdType;
import com.mopub.common.CloseableLayout;
import com.mopub.common.TyphoonApp;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.BaseWebView;
import com.mopub.mobileads.VastIconXmlManager;
import com.mopub.mobileads.ViewGestureDetector;
import com.mopub.mraid.MraidNativeCommandHandler;
import com.mopub.network.Networking;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONObject;

public class MraidBridge {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f20976;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f20977;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final WebViewClient f20978;

    /* renamed from: 连任  reason: contains not printable characters */
    private MraidWebView f20979;

    /* renamed from: 靐  reason: contains not printable characters */
    private final PlacementType f20980;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public MraidBridgeListener f20981;

    /* renamed from: 齉  reason: contains not printable characters */
    private final MraidNativeCommandHandler f20982;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AdReport f20983;

    public interface MraidBridgeListener {
        void onClose();

        boolean onConsoleMessage(ConsoleMessage consoleMessage);

        void onExpand(URI uri, boolean z) throws MraidCommandException;

        boolean onJsAlert(String str, JsResult jsResult);

        void onOpen(URI uri);

        void onPageFailedToLoad();

        void onPageLoaded();

        void onPlayVideo(URI uri);

        void onResize(int i, int i2, int i3, int i4, CloseableLayout.ClosePosition closePosition, boolean z) throws MraidCommandException;

        void onSetOrientationProperties(boolean z, MraidOrientation mraidOrientation) throws MraidCommandException;

        void onUseCustomClose(boolean z);

        void onVisibilityChanged(boolean z);
    }

    public static class MraidWebView extends BaseWebView {

        /* renamed from: 靐  reason: contains not printable characters */
        private OnVisibilityChangedListener f20993;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f20994;

        public interface OnVisibilityChangedListener {
            void onVisibilityChanged(boolean z);
        }

        public MraidWebView(Context context) {
            super(context);
            this.f20994 = getVisibility() == 0;
        }

        public boolean isVisible() {
            return this.f20994;
        }

        /* access modifiers changed from: protected */
        public void onVisibilityChanged(View view, int i) {
            super.onVisibilityChanged(view, i);
            boolean z = i == 0;
            if (z != this.f20994) {
                this.f20994 = z;
                if (this.f20993 != null) {
                    this.f20993.onVisibilityChanged(this.f20994);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void setVisibilityChangedListener(OnVisibilityChangedListener onVisibilityChangedListener) {
            this.f20993 = onVisibilityChangedListener;
        }
    }

    MraidBridge(AdReport adReport, PlacementType placementType) {
        this(adReport, placementType, new MraidNativeCommandHandler());
    }

    @VisibleForTesting
    MraidBridge(AdReport adReport, PlacementType placementType, MraidNativeCommandHandler mraidNativeCommandHandler) {
        this.f20978 = new WebViewClient() {
            public void onPageFinished(WebView webView, String str) {
                MraidBridge.this.m26908();
            }

            public void onReceivedError(WebView webView, int i, String str, String str2) {
                super.onReceivedError(webView, i, str, str2);
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                return MraidBridge.this.m26927(str);
            }
        };
        this.f20983 = adReport;
        this.f20980 = placementType;
        this.f20982 = mraidNativeCommandHandler;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private URI m26907(String str) throws MraidCommandException {
        if (str == null) {
            throw new MraidCommandException("Parameter cannot be null");
        }
        try {
            return new URI(str);
        } catch (URISyntaxException e) {
            throw new MraidCommandException("Invalid URL parameter: " + str);
        }
    }

    /* access modifiers changed from: private */
    @VisibleForTesting
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m26908() {
        if (!this.f20977) {
            this.f20977 = true;
            if (this.f20981 != null) {
                this.f20981.onPageLoaded();
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m26909(String str) throws MraidCommandException {
        if ("true".equals(str)) {
            return true;
        }
        if ("false".equals(str)) {
            return false;
        }
        throw new MraidCommandException("Invalid boolean parameter: " + str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m26911(Rect rect) {
        return rect.width() + "," + rect.height();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private MraidOrientation m26912(String str) throws MraidCommandException {
        if ("portrait".equals(str)) {
            return MraidOrientation.PORTRAIT;
        }
        if ("landscape".equals(str)) {
            return MraidOrientation.LANDSCAPE;
        }
        if ("none".equals(str)) {
            return MraidOrientation.NONE;
        }
        throw new MraidCommandException("Invalid orientation: " + str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private int m26913(String str) throws MraidCommandException {
        try {
            return Integer.parseInt(str, 10);
        } catch (NumberFormatException e) {
            throw new MraidCommandException("Invalid numeric parameter: " + str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m26915(int i, int i2, int i3) throws MraidCommandException {
        if (i >= i2 && i <= i3) {
            return i;
        }
        throw new MraidCommandException("Integer parameter out of range: " + i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private CloseableLayout.ClosePosition m26916(String str, CloseableLayout.ClosePosition closePosition) throws MraidCommandException {
        if (TextUtils.isEmpty(str)) {
            return closePosition;
        }
        if (str.equals("top-left")) {
            return CloseableLayout.ClosePosition.TOP_LEFT;
        }
        if (str.equals("top-right")) {
            return CloseableLayout.ClosePosition.TOP_RIGHT;
        }
        if (str.equals(TtmlNode.CENTER)) {
            return CloseableLayout.ClosePosition.CENTER;
        }
        if (str.equals("bottom-left")) {
            return CloseableLayout.ClosePosition.BOTTOM_LEFT;
        }
        if (str.equals("bottom-right")) {
            return CloseableLayout.ClosePosition.BOTTOM_RIGHT;
        }
        if (str.equals("top-center")) {
            return CloseableLayout.ClosePosition.TOP_CENTER;
        }
        if (str.equals("bottom-center")) {
            return CloseableLayout.ClosePosition.BOTTOM_CENTER;
        }
        throw new MraidCommandException("Invalid close position: " + str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m26917(Rect rect) {
        return rect.left + "," + rect.top + "," + rect.width() + "," + rect.height();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private URI m26918(String str, URI uri) throws MraidCommandException {
        return str == null ? uri : m26907(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26921(MraidJavascriptCommand mraidJavascriptCommand) {
        m26936("window.mraidbridge.nativeCallComplete(" + JSONObject.quote(mraidJavascriptCommand.m26986()) + ")");
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26922(MraidJavascriptCommand mraidJavascriptCommand, String str) {
        m26936("window.mraidbridge.notifyErrorEvent(" + JSONObject.quote(mraidJavascriptCommand.m26986()) + ", " + JSONObject.quote(str) + ")");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26924(String str, boolean z) throws MraidCommandException {
        return str == null ? z : m26909(str);
    }

    public void notifyScreenMetrics(MraidScreenMetrics mraidScreenMetrics) {
        m26936("mraidbridge.setScreenSize(" + m26911(mraidScreenMetrics.m27025()) + ");mraidbridge.setMaxSize(" + m26911(mraidScreenMetrics.m27023()) + ");mraidbridge.setCurrentPosition(" + m26917(mraidScreenMetrics.m27022()) + ");mraidbridge.setDefaultPosition(" + m26917(mraidScreenMetrics.m27018()) + ")");
        m26936("mraidbridge.notifySizeChangeEvent(" + m26911(mraidScreenMetrics.m27022()) + ")");
    }

    public void setContentHtml(String str) {
        if (this.f20979 != null) {
            this.f20977 = false;
            MraidWebView mraidWebView = this.f20979;
            String str2 = Networking.getBaseUrlScheme() + "://" + TyphoonApp.HOST + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR;
            String str3 = str;
            Pinkamena.DianePie();
        }
    }

    public void setContentUrl(String str) {
        if (this.f20979 != null) {
            this.f20977 = false;
            MraidWebView mraidWebView = this.f20979;
            Pinkamena.DianePie();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m26925() {
        return this.f20977;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26926() {
        m26936("mraidbridge.notifyReadyEvent();");
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m26927(String str) {
        try {
            URI uri = new URI(str);
            String scheme = uri.getScheme();
            String host = uri.getHost();
            if ("mopub".equals(scheme)) {
                if (!"failLoad".equals(host) || this.f20980 != PlacementType.INLINE || this.f20981 == null) {
                    return true;
                }
                this.f20981.onPageFailedToLoad();
                return true;
            } else if (AdType.MRAID.equals(scheme)) {
                HashMap hashMap = new HashMap();
                for (NameValuePair nameValuePair : URLEncodedUtils.parse(uri, "UTF-8")) {
                    hashMap.put(nameValuePair.getName(), nameValuePair.getValue());
                }
                MraidJavascriptCommand r0 = MraidJavascriptCommand.m26985(host);
                try {
                    m26933(r0, (Map<String, String>) hashMap);
                } catch (MraidCommandException e) {
                    m26922(r0, e.getMessage());
                }
                m26921(r0);
                return true;
            } else if (!this.f20976) {
                return false;
            } else {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setData(Uri.parse(str));
                intent.addFlags(268435456);
                try {
                    if (this.f20979 == null) {
                        return true;
                    }
                    this.f20979.getContext().startActivity(intent);
                    return true;
                } catch (ActivityNotFoundException e2) {
                    return false;
                }
            }
        } catch (URISyntaxException e3) {
            MoPubLog.w("Invalid MRAID URL: " + str);
            m26922(MraidJavascriptCommand.UNSPECIFIED, "Mraid command sent an invalid URL");
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m26928() {
        return this.f20979 != null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m26929() {
        return this.f20979 != null && this.f20979.isVisible();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26930() {
        this.f20979 = null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26931(MraidBridgeListener mraidBridgeListener) {
        this.f20981 = mraidBridgeListener;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"SetJavaScriptEnabled"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26932(MraidWebView mraidWebView) {
        this.f20979 = mraidWebView;
        this.f20979.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= 17 && this.f20980 == PlacementType.INTERSTITIAL) {
            mraidWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        this.f20979.setScrollContainer(false);
        this.f20979.setVerticalScrollBarEnabled(false);
        this.f20979.setHorizontalScrollBarEnabled(false);
        this.f20979.setBackgroundColor(-16777216);
        this.f20979.setWebViewClient(this.f20978);
        this.f20979.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                return MraidBridge.this.f20981 != null ? MraidBridge.this.f20981.onConsoleMessage(consoleMessage) : super.onConsoleMessage(consoleMessage);
            }

            public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                return MraidBridge.this.f20981 != null ? MraidBridge.this.f20981.onJsAlert(str2, jsResult) : super.onJsAlert(webView, str, str2, jsResult);
            }

            public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
                super.onShowCustomView(view, customViewCallback);
            }
        });
        final ViewGestureDetector viewGestureDetector = new ViewGestureDetector(this.f20979.getContext(), (View) this.f20979, this.f20983);
        viewGestureDetector.setUserClickListener(new ViewGestureDetector.UserClickListener() {
            public void onResetUserClick() {
                boolean unused = MraidBridge.this.f20976 = false;
            }

            public void onUserClick() {
                boolean unused = MraidBridge.this.f20976 = true;
            }

            public boolean wasClicked() {
                return MraidBridge.this.f20976;
            }
        });
        this.f20979.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                viewGestureDetector.sendTouchEvent(motionEvent);
                switch (motionEvent.getAction()) {
                    case 0:
                    case 1:
                        if (view.hasFocus()) {
                            return false;
                        }
                        view.requestFocus();
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.f20979.setVisibilityChangedListener(new MraidWebView.OnVisibilityChangedListener() {
            public void onVisibilityChanged(boolean z) {
                if (MraidBridge.this.f20981 != null) {
                    MraidBridge.this.f20981.onVisibilityChanged(z);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26933(final MraidJavascriptCommand mraidJavascriptCommand, Map<String, String> map) throws MraidCommandException {
        if (mraidJavascriptCommand.m26987(this.f20980) && !this.f20976) {
            throw new MraidCommandException("Cannot execute this command unless the user clicks");
        } else if (this.f20981 == null) {
            throw new MraidCommandException("Invalid state to execute this command");
        } else if (this.f20979 == null) {
            throw new MraidCommandException("The current WebView is being destroyed");
        } else {
            switch (mraidJavascriptCommand) {
                case CLOSE:
                    this.f20981.onClose();
                    return;
                case RESIZE:
                    this.f20981.onResize(m26915(m26913(map.get(VastIconXmlManager.WIDTH)), 0, (int) DefaultOggSeeker.MATCH_BYTE_RANGE), m26915(m26913(map.get(VastIconXmlManager.HEIGHT)), 0, (int) DefaultOggSeeker.MATCH_BYTE_RANGE), m26915(m26913(map.get("offsetX")), -100000, (int) DefaultOggSeeker.MATCH_BYTE_RANGE), m26915(m26913(map.get("offsetY")), -100000, (int) DefaultOggSeeker.MATCH_BYTE_RANGE), m26916(map.get("customClosePosition"), CloseableLayout.ClosePosition.TOP_RIGHT), m26924(map.get("allowOffscreen"), true));
                    return;
                case EXPAND:
                    this.f20981.onExpand(m26918(map.get("url"), (URI) null), m26924(map.get("shouldUseCustomClose"), false));
                    return;
                case USE_CUSTOM_CLOSE:
                    this.f20981.onUseCustomClose(m26924(map.get("shouldUseCustomClose"), false));
                    return;
                case OPEN:
                    this.f20981.onOpen(m26907(map.get("url")));
                    return;
                case SET_ORIENTATION_PROPERTIES:
                    this.f20981.onSetOrientationProperties(m26909(map.get("allowOrientationChange")), m26912(map.get("forceOrientation")));
                    return;
                case PLAY_VIDEO:
                    this.f20981.onPlayVideo(m26907(map.get("uri")));
                    return;
                case STORE_PICTURE:
                    this.f20982.m27006(this.f20979.getContext(), m26907(map.get("uri")).toString(), new MraidNativeCommandHandler.MraidCommandFailureListener() {
                        public void onFailure(MraidCommandException mraidCommandException) {
                            MraidBridge.this.m26922(mraidJavascriptCommand, mraidCommandException.getMessage());
                        }
                    });
                    return;
                case CREATE_CALENDAR_EVENT:
                    this.f20982.m27007(this.f20979.getContext(), map);
                    return;
                case UNSPECIFIED:
                    throw new MraidCommandException("Unspecified MRAID Javascript command");
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26934(PlacementType placementType) {
        m26936("mraidbridge.setPlacementType(" + JSONObject.quote(placementType.m27043()) + ")");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26935(ViewState viewState) {
        m26936("mraidbridge.setState(" + JSONObject.quote(viewState.toJavascriptString()) + ")");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26936(String str) {
        if (this.f20979 == null) {
            MoPubLog.d("Attempted to inject Javascript into MRAID WebView while was not attached:\n\t" + str);
            return;
        }
        MoPubLog.v("Injecting Javascript into MRAID WebView:\n\t" + str);
        MraidWebView mraidWebView = this.f20979;
        String str2 = "javascript:" + str;
        Pinkamena.DianePie();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26937(boolean z) {
        m26936("mraidbridge.setIsViewable(" + z + ")");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26938(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        m26936("mraidbridge.setSupports(" + z + "," + z2 + "," + z3 + "," + z4 + "," + z5 + ")");
    }
}
