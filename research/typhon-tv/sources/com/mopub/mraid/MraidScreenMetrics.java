package com.mopub.mraid;

import android.content.Context;
import android.graphics.Rect;
import com.mopub.common.util.Dips;

class MraidScreenMetrics {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Rect f21058 = new Rect();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Rect f21059 = new Rect();

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Rect f21060 = new Rect();

    /* renamed from: ˑ  reason: contains not printable characters */
    private final Rect f21061 = new Rect();

    /* renamed from: ٴ  reason: contains not printable characters */
    private final float f21062;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Rect f21063 = new Rect();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Rect f21064 = new Rect();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Rect f21065 = new Rect();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Rect f21066 = new Rect();

    /* renamed from: 龘  reason: contains not printable characters */
    private final Context f21067;

    MraidScreenMetrics(Context context, float f) {
        this.f21067 = context.getApplicationContext();
        this.f21062 = f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m27017(Rect rect, Rect rect2) {
        rect2.set(Dips.pixelsToIntDips((float) rect.left, this.f21067), Dips.pixelsToIntDips((float) rect.top, this.f21067), Dips.pixelsToIntDips((float) rect.right, this.f21067), Dips.pixelsToIntDips((float) rect.bottom, this.f21067));
    }

    public float getDensity() {
        return this.f21062;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Rect m27018() {
        return this.f21061;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public Rect m27019() {
        return this.f21060;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Rect m27020() {
        return this.f21065;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27021(int i, int i2, int i3, int i4) {
        this.f21058.set(i, i2, i + i3, i2 + i4);
        m27017(this.f21058, this.f21059);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public Rect m27022() {
        return this.f21059;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public Rect m27023() {
        return this.f21063;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m27024(int i, int i2, int i3, int i4) {
        this.f21060.set(i, i2, i + i3, i2 + i4);
        m27017(this.f21060, this.f21061);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Rect m27025() {
        return this.f21066;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27026(int i, int i2) {
        this.f21064.set(0, 0, i, i2);
        m27017(this.f21064, this.f21066);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27027(int i, int i2, int i3, int i4) {
        this.f21065.set(i, i2, i + i3, i2 + i4);
        m27017(this.f21065, this.f21063);
    }
}
