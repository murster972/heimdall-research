package com.mopub.mraid;

import android.net.Uri;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.mobileads.resource.MraidJavascript;
import java.io.ByteArrayInputStream;
import java.util.Locale;

public class MraidWebViewClient extends WebViewClient {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f21075 = ("javascript:" + MraidJavascript.JAVASCRIPT_SOURCE);

    /* renamed from: 龘  reason: contains not printable characters */
    private WebResourceResponse m27041() {
        return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(f21075.getBytes()));
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        return m27042(str) ? m27041() : super.shouldInterceptRequest(webView, str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m27042(String str) {
        return "mraid.js".equals(Uri.parse(str.toLowerCase(Locale.US)).getLastPathSegment());
    }
}
