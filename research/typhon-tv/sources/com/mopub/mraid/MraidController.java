package com.mopub.mraid;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout;
import com.mopub.common.AdReport;
import com.mopub.common.CloseableLayout;
import com.mopub.common.Preconditions;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.common.util.Views;
import com.mopub.mobileads.MraidVideoPlayerActivity;
import com.mopub.mobileads.util.WebViews;
import com.mopub.mraid.MraidBridge;
import java.lang.ref.WeakReference;
import java.net.URI;

public class MraidController {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final CloseableLayout f20995;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ScreenMetricsWaiter f20996;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public final MraidScreenMetrics f20997;
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public ViewState f20998;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public MraidListener f20999;

    /* renamed from: ˆ  reason: contains not printable characters */
    private Integer f21000;

    /* renamed from: ˈ  reason: contains not printable characters */
    private ViewGroup f21001;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f21002;

    /* renamed from: ˊ  reason: contains not printable characters */
    private MraidBridge.MraidWebView f21003;

    /* renamed from: ˋ  reason: contains not printable characters */
    private MraidBridge.MraidWebView f21004;

    /* renamed from: ˎ  reason: contains not printable characters */
    private OrientationBroadcastReceiver f21005;

    /* renamed from: ˏ  reason: contains not printable characters */
    private MraidOrientation f21006;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public final MraidBridge f21007;

    /* renamed from: י  reason: contains not printable characters */
    private final MraidBridge.MraidBridgeListener f21008;

    /* renamed from: ـ  reason: contains not printable characters */
    private final MraidBridge.MraidBridgeListener f21009;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public final MraidBridge f21010;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public final MraidNativeCommandHandler f21011;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f21012;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final FrameLayout f21013;

    /* renamed from: 靐  reason: contains not printable characters */
    private final WeakReference<Activity> f21014;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public final PlacementType f21015;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final Context f21016;

    /* renamed from: 龘  reason: contains not printable characters */
    private final AdReport f21017;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private UseCustomCloseListener f21018;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private MraidWebViewDebugListener f21019;

    public interface MraidListener {
        void onClose();

        void onExpand();

        void onFailedToLoad();

        void onLoaded(View view);

        void onOpen();
    }

    @VisibleForTesting
    class OrientationBroadcastReceiver extends BroadcastReceiver {

        /* renamed from: 靐  reason: contains not printable characters */
        private Context f21029;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f21030 = -1;

        OrientationBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            int r0;
            if (this.f21029 != null && "android.intent.action.CONFIGURATION_CHANGED".equals(intent.getAction()) && (r0 = MraidController.this.m26939()) != this.f21030) {
                this.f21030 = r0;
                MraidController.this.m26967(this.f21030);
            }
        }

        public void register(Context context) {
            Preconditions.checkNotNull(context);
            this.f21029 = context.getApplicationContext();
            if (this.f21029 != null) {
                this.f21029.registerReceiver(this, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
            }
        }

        public void unregister() {
            if (this.f21029 != null) {
                this.f21029.unregisterReceiver(this);
                this.f21029 = null;
            }
        }
    }

    @VisibleForTesting
    static class ScreenMetricsWaiter {

        /* renamed from: 靐  reason: contains not printable characters */
        private WaitRequest f21032;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Handler f21033 = new Handler();

        static class WaitRequest {

            /* renamed from: 连任  reason: contains not printable characters */
            private final Runnable f21034;
            /* access modifiers changed from: private */

            /* renamed from: 靐  reason: contains not printable characters */
            public final View[] f21035;

            /* renamed from: 麤  reason: contains not printable characters */
            private Runnable f21036;

            /* renamed from: 齉  reason: contains not printable characters */
            private final Handler f21037;

            /* renamed from: 龘  reason: contains not printable characters */
            int f21038;

            private WaitRequest(Handler handler, View[] viewArr) {
                this.f21034 = new Runnable() {
                    public void run() {
                        for (final View view : WaitRequest.this.f21035) {
                            if (view.getHeight() > 0 || view.getWidth() > 0) {
                                WaitRequest.this.m26978();
                            } else {
                                view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                    public boolean onPreDraw() {
                                        view.getViewTreeObserver().removeOnPreDrawListener(this);
                                        WaitRequest.this.m26978();
                                        return true;
                                    }
                                });
                            }
                        }
                    }
                };
                this.f21037 = handler;
                this.f21035 = viewArr;
            }

            /* access modifiers changed from: private */
            /* renamed from: 靐  reason: contains not printable characters */
            public void m26978() {
                this.f21038--;
                if (this.f21038 == 0 && this.f21036 != null) {
                    this.f21036.run();
                    this.f21036 = null;
                }
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m26981() {
                this.f21037.removeCallbacks(this.f21034);
                this.f21036 = null;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m26982(Runnable runnable) {
                this.f21036 = runnable;
                this.f21038 = this.f21035.length;
                this.f21037.post(this.f21034);
            }
        }

        ScreenMetricsWaiter() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public WaitRequest m26976(View... viewArr) {
            this.f21032 = new WaitRequest(this.f21033, viewArr);
            return this.f21032;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m26977() {
            if (this.f21032 != null) {
                this.f21032.m26981();
                this.f21032 = null;
            }
        }
    }

    public interface UseCustomCloseListener {
        void useCustomCloseChanged(boolean z);
    }

    public MraidController(Context context, AdReport adReport, PlacementType placementType) {
        this(context, adReport, placementType, new MraidBridge(adReport, placementType), new MraidBridge(adReport, PlacementType.INTERSTITIAL), new ScreenMetricsWaiter());
    }

    @VisibleForTesting
    MraidController(Context context, AdReport adReport, PlacementType placementType, MraidBridge mraidBridge, MraidBridge mraidBridge2, ScreenMetricsWaiter screenMetricsWaiter) {
        this.f20998 = ViewState.LOADING;
        this.f21005 = new OrientationBroadcastReceiver();
        this.f21002 = true;
        this.f21006 = MraidOrientation.NONE;
        this.f21008 = new MraidBridge.MraidBridgeListener() {
            public void onClose() {
                MraidController.this.m26964();
            }

            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                return MraidController.this.m26973(consoleMessage);
            }

            public void onExpand(URI uri, boolean z) throws MraidCommandException {
                MraidController.this.m26970(uri, z);
            }

            public boolean onJsAlert(String str, JsResult jsResult) {
                return MraidController.this.m26975(str, jsResult);
            }

            public void onOpen(URI uri) {
                MraidController.this.m26962(uri.toString());
            }

            public void onPageFailedToLoad() {
                if (MraidController.this.f20999 != null) {
                    MraidController.this.f20999.onFailedToLoad();
                }
            }

            public void onPageLoaded() {
                MraidController.this.m26966();
            }

            public void onPlayVideo(URI uri) {
                MraidController.this.m26969(uri.toString());
            }

            public void onResize(int i, int i2, int i3, int i4, CloseableLayout.ClosePosition closePosition, boolean z) throws MraidCommandException {
                MraidController.this.m26968(i, i2, i3, i4, closePosition, z);
            }

            public void onSetOrientationProperties(boolean z, MraidOrientation mraidOrientation) throws MraidCommandException {
                MraidController.this.m26972(z, mraidOrientation);
            }

            public void onUseCustomClose(boolean z) {
                MraidController.this.m26971(z);
            }

            public void onVisibilityChanged(boolean z) {
                if (!MraidController.this.f21010.m26928()) {
                    MraidController.this.f21007.m26937(z);
                }
            }
        };
        this.f21009 = new MraidBridge.MraidBridgeListener() {
            public void onClose() {
                MraidController.this.m26964();
            }

            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                return MraidController.this.m26973(consoleMessage);
            }

            public void onExpand(URI uri, boolean z) {
            }

            public boolean onJsAlert(String str, JsResult jsResult) {
                return MraidController.this.m26975(str, jsResult);
            }

            public void onOpen(URI uri) {
                MraidController.this.m26962(uri.toString());
            }

            public void onPageFailedToLoad() {
            }

            public void onPageLoaded() {
                MraidController.this.m26960();
            }

            public void onPlayVideo(URI uri) {
                MraidController.this.m26969(uri.toString());
            }

            public void onResize(int i, int i2, int i3, int i4, CloseableLayout.ClosePosition closePosition, boolean z) throws MraidCommandException {
                throw new MraidCommandException("Not allowed to resize from an expanded state");
            }

            public void onSetOrientationProperties(boolean z, MraidOrientation mraidOrientation) throws MraidCommandException {
                MraidController.this.m26972(z, mraidOrientation);
            }

            public void onUseCustomClose(boolean z) {
                MraidController.this.m26971(z);
            }

            public void onVisibilityChanged(boolean z) {
                MraidController.this.f21007.m26937(z);
                MraidController.this.f21010.m26937(z);
            }
        };
        this.f21016 = context.getApplicationContext();
        Preconditions.checkNotNull(this.f21016);
        this.f21017 = adReport;
        if (context instanceof Activity) {
            this.f21014 = new WeakReference<>((Activity) context);
        } else {
            this.f21014 = new WeakReference<>((Object) null);
        }
        this.f21015 = placementType;
        this.f21007 = mraidBridge;
        this.f21010 = mraidBridge2;
        this.f20996 = screenMetricsWaiter;
        this.f20998 = ViewState.LOADING;
        this.f20997 = new MraidScreenMetrics(this.f21016, this.f21016.getResources().getDisplayMetrics().density);
        this.f21013 = new FrameLayout(this.f21016);
        this.f20995 = new CloseableLayout(this.f21016);
        this.f20995.setOnCloseListener(new CloseableLayout.OnCloseListener() {
            public void onClose() {
                MraidController.this.m26964();
            }
        });
        View view = new View(this.f21016);
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.f20995.addView(view, new FrameLayout.LayoutParams(-1, -1));
        this.f21005.register(this.f21016);
        this.f21007.m26931(this.f21008);
        this.f21010.m26931(this.f21009);
        this.f21011 = new MraidNativeCommandHandler();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m26939() {
        return ((WindowManager) this.f21016.getSystemService("window")).getDefaultDisplay().getRotation();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private View m26941() {
        return this.f21010.m26928() ? this.f21004 : this.f21003;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m26944() {
        Activity activity = (Activity) this.f21014.get();
        if (activity == null || m26941() == null) {
            return false;
        }
        return this.f21011.m27008(activity, m26941());
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public ViewGroup m26946() {
        if (this.f21001 != null) {
            return this.f21001;
        }
        View topmostView = Views.getTopmostView((Context) this.f21014.get(), this.f21013);
        return topmostView instanceof ViewGroup ? (ViewGroup) topmostView : this.f21013;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private ViewGroup m26948() {
        if (this.f21001 == null) {
            this.f21001 = m26946();
        }
        return this.f21001;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26956(ViewState viewState) {
        m26957(viewState, (Runnable) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26957(ViewState viewState, Runnable runnable) {
        ViewState viewState2 = this.f20998;
        this.f20998 = viewState;
        this.f21007.m26935(viewState);
        if (this.f21010.m26925()) {
            this.f21010.m26935(viewState);
        }
        if (this.f20999 != null) {
            if (viewState == ViewState.EXPANDED) {
                this.f20999.onExpand();
            } else if (viewState2 == ViewState.EXPANDED && viewState == ViewState.DEFAULT) {
                this.f20999.onClose();
            } else if (viewState == ViewState.HIDDEN) {
                this.f20999.onClose();
            }
        }
        m26958(runnable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m26958(final Runnable runnable) {
        this.f20996.m26977();
        final View r0 = m26941();
        if (r0 != null) {
            this.f20996.m26976(this.f21013, r0).m26982((Runnable) new Runnable() {
                public void run() {
                    DisplayMetrics displayMetrics = MraidController.this.f21016.getResources().getDisplayMetrics();
                    MraidController.this.f20997.m27026(displayMetrics.widthPixels, displayMetrics.heightPixels);
                    int[] iArr = new int[2];
                    ViewGroup r2 = MraidController.this.m26946();
                    r2.getLocationOnScreen(iArr);
                    MraidController.this.f20997.m27027(iArr[0], iArr[1], r2.getWidth(), r2.getHeight());
                    MraidController.this.f21013.getLocationOnScreen(iArr);
                    MraidController.this.f20997.m27024(iArr[0], iArr[1], MraidController.this.f21013.getWidth(), MraidController.this.f21013.getHeight());
                    r0.getLocationOnScreen(iArr);
                    MraidController.this.f20997.m27021(iArr[0], iArr[1], r0.getWidth(), r0.getHeight());
                    MraidController.this.f21007.notifyScreenMetrics(MraidController.this.f20997);
                    if (MraidController.this.f21010.m26928()) {
                        MraidController.this.f21010.notifyScreenMetrics(MraidController.this.f20997);
                    }
                    if (runnable != null) {
                        runnable.run();
                    }
                }
            });
        }
    }

    public void destroy() {
        this.f20996.m26977();
        try {
            this.f21005.unregister();
        } catch (IllegalArgumentException e) {
            if (!e.getMessage().contains("Receiver not registered")) {
                throw e;
            }
        }
        if (!this.f21012) {
            pause(true);
        }
        Views.removeFromParent(this.f20995);
        this.f21007.m26930();
        if (this.f21003 != null) {
            this.f21003.destroy();
            this.f21003 = null;
        }
        this.f21010.m26930();
        if (this.f21004 != null) {
            this.f21004.destroy();
            this.f21004 = null;
        }
    }

    public FrameLayout getAdContainer() {
        return this.f21013;
    }

    public Context getContext() {
        return this.f21016;
    }

    public void loadContent(String str) {
        Preconditions.checkState(this.f21003 == null, "loadContent should only be called once");
        this.f21003 = new MraidBridge.MraidWebView(this.f21016);
        this.f21007.m26932(this.f21003);
        this.f21013.addView(this.f21003, new FrameLayout.LayoutParams(-1, -1));
        this.f21007.setContentHtml(str);
    }

    public void loadJavascript(String str) {
        this.f21007.m26936(str);
    }

    public void pause(boolean z) {
        this.f21012 = true;
        if (this.f21003 != null) {
            WebViews.onPause(this.f21003, z);
        }
        if (this.f21004 != null) {
            WebViews.onPause(this.f21004, z);
        }
    }

    public void resume() {
        this.f21012 = false;
        if (this.f21003 != null) {
            WebViews.onResume(this.f21003);
        }
        if (this.f21004 != null) {
            WebViews.onResume(this.f21004);
        }
    }

    public void setDebugListener(MraidWebViewDebugListener mraidWebViewDebugListener) {
        this.f21019 = mraidWebViewDebugListener;
    }

    public void setMraidListener(MraidListener mraidListener) {
        this.f20999 = mraidListener;
    }

    public void setUseCustomCloseListener(UseCustomCloseListener useCustomCloseListener) {
        this.f21018 = useCustomCloseListener;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 连任  reason: contains not printable characters */
    public void m26959() {
        Activity activity = (Activity) this.f21014.get();
        if (!(activity == null || this.f21000 == null)) {
            activity.setRequestedOrientation(this.f21000.intValue());
        }
        this.f21000 = null;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26960() {
        m26958((Runnable) new Runnable() {
            public void run() {
                MraidBridge r0 = MraidController.this.f21010;
                boolean r1 = MraidController.this.f21011.m27005(MraidController.this.f21016);
                boolean r2 = MraidController.this.f21011.m27004(MraidController.this.f21016);
                MraidNativeCommandHandler unused = MraidController.this.f21011;
                boolean r3 = MraidNativeCommandHandler.m27002(MraidController.this.f21016);
                MraidNativeCommandHandler unused2 = MraidController.this.f21011;
                r0.m26938(r1, r2, r3, MraidNativeCommandHandler.isStorePictureSupported(MraidController.this.f21016), MraidController.this.m26944());
                MraidController.this.f21010.m26935(MraidController.this.f20998);
                MraidController.this.f21010.m26934(MraidController.this.f21015);
                MraidController.this.f21010.m26937(MraidController.this.f21010.m26929());
                MraidController.this.f21010.m26926();
            }
        });
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26961(int i) throws MraidCommandException {
        Activity activity = (Activity) this.f21014.get();
        if (activity == null || !m26974(this.f21006)) {
            throw new MraidCommandException("Attempted to lock orientation to unsupported value: " + this.f21006.name());
        }
        if (this.f21000 == null) {
            this.f21000 = Integer.valueOf(activity.getRequestedOrientation());
        }
        activity.setRequestedOrientation(i);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 靐  reason: contains not printable characters */
    public void m26962(String str) {
        if (this.f20999 != null) {
            this.f20999.onOpen();
        }
        UrlHandler.Builder builder = new UrlHandler.Builder();
        if (this.f21017 != null) {
            builder.withDspCreativeId(this.f21017.getDspCreativeId());
        }
        builder.withSupportedUrlActions(UrlAction.IGNORE_ABOUT_SCHEME, UrlAction.OPEN_NATIVE_BROWSER, UrlAction.OPEN_IN_APP_BROWSER, UrlAction.HANDLE_SHARE_TWEET, UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK, UrlAction.FOLLOW_DEEP_LINK).build().handleUrl(this.f21016, str);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 麤  reason: contains not printable characters */
    public void m26963() throws MraidCommandException {
        if (this.f21006 != MraidOrientation.NONE) {
            m26961(this.f21006.m27016());
        } else if (this.f21002) {
            m26959();
        } else {
            Activity activity = (Activity) this.f21014.get();
            if (activity == null) {
                throw new MraidCommandException("Unable to set MRAID expand orientation to 'none'; expected passed in Activity Context.");
            }
            m26961(DeviceUtils.getScreenOrientation(activity));
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    /* renamed from: 齉  reason: contains not printable characters */
    public void m26964() {
        if (this.f21003 != null && this.f20998 != ViewState.LOADING && this.f20998 != ViewState.HIDDEN) {
            if (this.f20998 == ViewState.EXPANDED || this.f21015 == PlacementType.INTERSTITIAL) {
                m26959();
            }
            if (this.f20998 == ViewState.RESIZED || this.f20998 == ViewState.EXPANDED) {
                if (!this.f21010.m26928() || this.f21004 == null) {
                    this.f20995.removeView(this.f21003);
                    this.f21013.addView(this.f21003, new FrameLayout.LayoutParams(-1, -1));
                    this.f21013.setVisibility(0);
                } else {
                    this.f20995.removeView(this.f21004);
                    this.f21010.m26930();
                }
                Views.removeFromParent(this.f20995);
                m26956(ViewState.DEFAULT);
            } else if (this.f20998 == ViewState.DEFAULT) {
                this.f21013.setVisibility(4);
                m26956(ViewState.HIDDEN);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m26965(int i, int i2, int i3) {
        return Math.max(i, Math.min(i2, i3));
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26966() {
        m26957(ViewState.DEFAULT, (Runnable) new Runnable() {
            public void run() {
                MraidController.this.f21007.m26938(MraidController.this.f21011.m27005(MraidController.this.f21016), MraidController.this.f21011.m27004(MraidController.this.f21016), MraidNativeCommandHandler.m27002(MraidController.this.f21016), MraidNativeCommandHandler.isStorePictureSupported(MraidController.this.f21016), MraidController.this.m26944());
                MraidController.this.f21007.m26934(MraidController.this.f21015);
                MraidController.this.f21007.m26937(MraidController.this.f21007.m26929());
                MraidController.this.f21007.m26926();
            }
        });
        if (this.f20999 != null) {
            this.f20999.onLoaded(this.f21013);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26967(int i) {
        m26958((Runnable) null);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26968(int i, int i2, int i3, int i4, CloseableLayout.ClosePosition closePosition, boolean z) throws MraidCommandException {
        if (this.f21003 == null) {
            throw new MraidCommandException("Unable to resize after the WebView is destroyed");
        } else if (this.f20998 != ViewState.LOADING && this.f20998 != ViewState.HIDDEN) {
            if (this.f20998 == ViewState.EXPANDED) {
                throw new MraidCommandException("Not allowed to resize from an already expanded ad");
            } else if (this.f21015 == PlacementType.INTERSTITIAL) {
                throw new MraidCommandException("Not allowed to resize from an interstitial ad");
            } else {
                int dipsToIntPixels = Dips.dipsToIntPixels((float) i, this.f21016);
                int dipsToIntPixels2 = Dips.dipsToIntPixels((float) i2, this.f21016);
                int dipsToIntPixels3 = Dips.dipsToIntPixels((float) i3, this.f21016);
                int dipsToIntPixels4 = Dips.dipsToIntPixels((float) i4, this.f21016);
                int i5 = this.f20997.m27019().left + dipsToIntPixels3;
                int i6 = this.f20997.m27019().top + dipsToIntPixels4;
                Rect rect = new Rect(i5, i6, i5 + dipsToIntPixels, i6 + dipsToIntPixels2);
                if (!z) {
                    Rect r1 = this.f20997.m27020();
                    if (rect.width() > r1.width() || rect.height() > r1.height()) {
                        throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + i2 + ") and offset (" + i3 + ", " + i4 + ") that doesn't allow the ad to appear within the max allowed size (" + this.f20997.m27023().width() + ", " + this.f20997.m27023().height() + ")");
                    }
                    rect.offsetTo(m26965(r1.left, rect.left, r1.right - rect.width()), m26965(r1.top, rect.top, r1.bottom - rect.height()));
                }
                Rect rect2 = new Rect();
                this.f20995.applyCloseRegionBounds(closePosition, rect, rect2);
                if (!this.f20997.m27020().contains(rect2)) {
                    throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + i2 + ") and offset (" + i3 + ", " + i4 + ") that doesn't allow the close region to appear within the max allowed size (" + this.f20997.m27023().width() + ", " + this.f20997.m27023().height() + ")");
                } else if (!rect.contains(rect2)) {
                    throw new MraidCommandException("resizeProperties specified a size (" + i + ", " + dipsToIntPixels2 + ") and offset (" + i3 + ", " + i4 + ") that don't allow the close region to appear within the resized ad.");
                } else {
                    this.f20995.setCloseVisible(false);
                    this.f20995.setClosePosition(closePosition);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(rect.width(), rect.height());
                    layoutParams.leftMargin = rect.left - this.f20997.m27020().left;
                    layoutParams.topMargin = rect.top - this.f20997.m27020().top;
                    if (this.f20998 == ViewState.DEFAULT) {
                        this.f21013.removeView(this.f21003);
                        this.f21013.setVisibility(4);
                        this.f20995.addView(this.f21003, new FrameLayout.LayoutParams(-1, -1));
                        m26948().addView(this.f20995, layoutParams);
                    } else if (this.f20998 == ViewState.RESIZED) {
                        this.f20995.setLayoutParams(layoutParams);
                    }
                    this.f20995.setClosePosition(closePosition);
                    m26956(ViewState.RESIZED);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26969(String str) {
        MraidVideoPlayerActivity.startMraid(this.f21016, str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26970(URI uri, boolean z) throws MraidCommandException {
        if (this.f21003 == null) {
            throw new MraidCommandException("Unable to expand after the WebView is destroyed");
        } else if (this.f21015 != PlacementType.INTERSTITIAL) {
            if (this.f20998 == ViewState.DEFAULT || this.f20998 == ViewState.RESIZED) {
                m26963();
                boolean z2 = uri != null;
                if (z2) {
                    this.f21004 = new MraidBridge.MraidWebView(this.f21016);
                    this.f21010.m26932(this.f21004);
                    this.f21010.setContentUrl(uri.toString());
                }
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
                if (this.f20998 == ViewState.DEFAULT) {
                    if (z2) {
                        this.f20995.addView(this.f21004, layoutParams);
                    } else {
                        this.f21013.removeView(this.f21003);
                        this.f21013.setVisibility(4);
                        this.f20995.addView(this.f21003, layoutParams);
                    }
                    m26948().addView(this.f20995, new FrameLayout.LayoutParams(-1, -1));
                } else if (this.f20998 == ViewState.RESIZED && z2) {
                    this.f20995.removeView(this.f21003);
                    this.f21013.addView(this.f21003, layoutParams);
                    this.f21013.setVisibility(4);
                    this.f20995.addView(this.f21004, layoutParams);
                }
                this.f20995.setLayoutParams(layoutParams);
                m26971(z);
                m26956(ViewState.EXPANDED);
            }
        }
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26971(boolean z) {
        boolean z2 = true;
        if (z != (!this.f20995.isCloseVisible())) {
            CloseableLayout closeableLayout = this.f20995;
            if (z) {
                z2 = false;
            }
            closeableLayout.setCloseVisible(z2);
            if (this.f21018 != null) {
                this.f21018.useCustomCloseChanged(z);
            }
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26972(boolean z, MraidOrientation mraidOrientation) throws MraidCommandException {
        if (!m26974(mraidOrientation)) {
            throw new MraidCommandException("Unable to force orientation to " + mraidOrientation);
        }
        this.f21002 = z;
        this.f21006 = mraidOrientation;
        if (this.f20998 == ViewState.EXPANDED || this.f21015 == PlacementType.INTERSTITIAL) {
            m26963();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26973(ConsoleMessage consoleMessage) {
        if (this.f21019 != null) {
            return this.f21019.onConsoleMessage(consoleMessage);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26974(MraidOrientation mraidOrientation) {
        if (mraidOrientation == MraidOrientation.NONE) {
            return true;
        }
        Activity activity = (Activity) this.f21014.get();
        if (activity == null) {
            return false;
        }
        try {
            ActivityInfo activityInfo = activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0);
            int i = activityInfo.screenOrientation;
            if (i != -1) {
                return i == mraidOrientation.m27016();
            }
            return Utils.bitMaskContainsFlag(activityInfo.configChanges, 128) && Utils.bitMaskContainsFlag(activityInfo.configChanges, 1024);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m26975(String str, JsResult jsResult) {
        if (this.f21019 != null) {
            return this.f21019.onJsAlert(str, jsResult);
        }
        jsResult.confirm();
        return true;
    }
}
