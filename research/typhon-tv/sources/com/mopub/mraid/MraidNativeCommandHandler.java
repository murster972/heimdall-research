package com.mopub.mraid;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.mopub.common.MoPubHttpUrlConnection;
import com.mopub.common.Preconditions;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Intents;
import com.mopub.common.util.ResponseHeader;
import com.mopub.common.util.Streams;
import com.mopub.common.util.Utils;
import com.mopub.common.util.VersionCode;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.oltu.oauth2.common.OAuth;

public class MraidNativeCommandHandler {
    public static final String ANDROID_CALENDAR_CONTENT_TYPE = "vnd.android.cursor.item/event";

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f21044 = {"yyyy-MM-dd'T'HH:mm:ssZZZZZ", "yyyy-MM-dd'T'HH:mmZZZZZ"};

    @VisibleForTesting
    static class DownloadImageAsyncTask extends AsyncTask<String, Void, Boolean> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final DownloadImageAsyncTaskListener f21052;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Context f21053;

        interface DownloadImageAsyncTaskListener {
            void onFailure();

            void onSuccess();
        }

        public DownloadImageAsyncTask(Context context, DownloadImageAsyncTaskListener downloadImageAsyncTaskListener) {
            this.f21053 = context.getApplicationContext();
            this.f21052 = downloadImageAsyncTaskListener;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private File m27009() {
            return new File(Environment.getExternalStorageDirectory(), "Pictures");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m27010(URI uri, Map<String, List<String>> map) {
            Preconditions.checkNotNull(uri);
            String path = uri.getPath();
            if (path == null || map == null) {
                return null;
            }
            String name = new File(path).getName();
            List list = map.get(OAuth.HeaderType.CONTENT_TYPE);
            if (list == null || list.isEmpty() || list.get(0) == null) {
                return name;
            }
            for (String str : ((String) list.get(0)).split(";")) {
                if (str.contains("image/")) {
                    String str2 = "." + str.split(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR)[1];
                    return !name.endsWith(str2) ? name + str2 : name;
                }
            }
            return name;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m27011(String str) {
            MoPubMediaScannerConnectionClient moPubMediaScannerConnectionClient = new MoPubMediaScannerConnectionClient(str, (String) null);
            MediaScannerConnection mediaScannerConnection = new MediaScannerConnection(this.f21053, moPubMediaScannerConnectionClient);
            moPubMediaScannerConnectionClient.m27014(mediaScannerConnection);
            mediaScannerConnection.connect();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Boolean doInBackground(String... strArr) {
            File file;
            FileOutputStream fileOutputStream;
            if (strArr == null || strArr.length == 0 || strArr[0] == null) {
                return false;
            }
            File r8 = m27009();
            r8.mkdirs();
            String str = strArr[0];
            URI create = URI.create(str);
            BufferedInputStream bufferedInputStream = null;
            FileOutputStream fileOutputStream2 = null;
            try {
                HttpURLConnection httpUrlConnection = MoPubHttpUrlConnection.getHttpUrlConnection(str);
                BufferedInputStream bufferedInputStream2 = new BufferedInputStream(httpUrlConnection.getInputStream());
                try {
                    String headerField = httpUrlConnection.getHeaderField(ResponseHeader.LOCATION.getKey());
                    if (!TextUtils.isEmpty(headerField)) {
                        create = URI.create(headerField);
                    }
                    file = new File(r8, m27010(create, httpUrlConnection.getHeaderFields()));
                    fileOutputStream = new FileOutputStream(file);
                } catch (Exception e) {
                    bufferedInputStream = bufferedInputStream2;
                    try {
                        Streams.closeStream(bufferedInputStream);
                        Streams.closeStream(fileOutputStream2);
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        Streams.closeStream(bufferedInputStream);
                        Streams.closeStream(fileOutputStream2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedInputStream = bufferedInputStream2;
                    Streams.closeStream(bufferedInputStream);
                    Streams.closeStream(fileOutputStream2);
                    throw th;
                }
                try {
                    Streams.copyContent(bufferedInputStream2, fileOutputStream);
                    m27011(file.toString());
                    Streams.closeStream(bufferedInputStream2);
                    Streams.closeStream(fileOutputStream);
                    return true;
                } catch (Exception e2) {
                    fileOutputStream2 = fileOutputStream;
                    bufferedInputStream = bufferedInputStream2;
                    Streams.closeStream(bufferedInputStream);
                    Streams.closeStream(fileOutputStream2);
                    return false;
                } catch (Throwable th3) {
                    th = th3;
                    fileOutputStream2 = fileOutputStream;
                    bufferedInputStream = bufferedInputStream2;
                    Streams.closeStream(bufferedInputStream);
                    Streams.closeStream(fileOutputStream2);
                    throw th;
                }
            } catch (Exception e3) {
                Streams.closeStream(bufferedInputStream);
                Streams.closeStream(fileOutputStream2);
                return false;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void onPostExecute(Boolean bool) {
            if (bool == null || !bool.booleanValue()) {
                this.f21052.onFailure();
            } else {
                this.f21052.onSuccess();
            }
        }
    }

    private static class MoPubMediaScannerConnectionClient implements MediaScannerConnection.MediaScannerConnectionClient {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f21054;

        /* renamed from: 齉  reason: contains not printable characters */
        private MediaScannerConnection f21055;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f21056;

        private MoPubMediaScannerConnectionClient(String str, String str2) {
            this.f21056 = str;
            this.f21054 = str2;
        }

        /* access modifiers changed from: private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m27014(MediaScannerConnection mediaScannerConnection) {
            this.f21055 = mediaScannerConnection;
        }

        public void onMediaScannerConnected() {
            if (this.f21055 != null) {
                this.f21055.scanFile(this.f21056, this.f21054);
            }
        }

        public void onScanCompleted(String str, Uri uri) {
            if (this.f21055 != null) {
                this.f21055.disconnect();
            }
        }
    }

    interface MraidCommandFailureListener {
        void onFailure(MraidCommandException mraidCommandException);
    }

    public static boolean isStorePictureSupported(Context context) {
        return "mounted".equals(Environment.getExternalStorageState()) && DeviceUtils.isPermissionGranted(context, "android.permission.WRITE_EXTERNAL_STORAGE");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m26994(int i) throws IllegalArgumentException {
        if (i != 0 && i >= -31 && i <= 31) {
            return "" + i;
        }
        throw new IllegalArgumentException("invalid day of month " + i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m26995(String str) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder();
        boolean[] zArr = new boolean[7];
        String[] split = str.split(",");
        for (String parseInt : split) {
            int parseInt2 = Integer.parseInt(parseInt);
            if (parseInt2 == 7) {
                parseInt2 = 0;
            }
            if (!zArr[parseInt2]) {
                sb.append(m26999(parseInt2)).append(",");
                zArr[parseInt2] = true;
            }
        }
        if (split.length == 0) {
            throw new IllegalArgumentException("must have at least 1 day of the week if specifying repeating weekly");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m26996(Map<String, String> map) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder();
        if (map.containsKey("frequency")) {
            String str = map.get("frequency");
            int i = -1;
            if (map.containsKey("interval")) {
                i = Integer.parseInt(map.get("interval"));
            }
            if ("daily".equals(str)) {
                sb.append("FREQ=DAILY;");
                if (i != -1) {
                    sb.append("INTERVAL=").append(i).append(";");
                }
            } else if ("weekly".equals(str)) {
                sb.append("FREQ=WEEKLY;");
                if (i != -1) {
                    sb.append("INTERVAL=").append(i).append(";");
                }
                if (map.containsKey("daysInWeek")) {
                    String r4 = m26995(map.get("daysInWeek"));
                    if (r4 == null) {
                        throw new IllegalArgumentException("invalid ");
                    }
                    sb.append("BYDAY=").append(r4).append(";");
                }
            } else if ("monthly".equals(str)) {
                sb.append("FREQ=MONTHLY;");
                if (i != -1) {
                    sb.append("INTERVAL=").append(i).append(";");
                }
                if (map.containsKey("daysInMonth")) {
                    String r2 = m26997(map.get("daysInMonth"));
                    if (r2 == null) {
                        throw new IllegalArgumentException();
                    }
                    sb.append("BYMONTHDAY=").append(r2).append(";");
                }
            } else {
                throw new IllegalArgumentException("frequency is only supported for daily, weekly, and monthly.");
            }
        }
        return sb.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private String m26997(String str) throws IllegalArgumentException {
        StringBuilder sb = new StringBuilder();
        boolean[] zArr = new boolean[63];
        String[] split = str.split(",");
        for (String parseInt : split) {
            int parseInt2 = Integer.parseInt(parseInt);
            if (!zArr[parseInt2 + 31]) {
                sb.append(m26994(parseInt2)).append(",");
                zArr[parseInt2 + 31] = true;
            }
        }
        if (split.length == 0) {
            throw new IllegalArgumentException("must have at least 1 day of the month if specifying repeating weekly");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m26998(final Context context, final String str, final MraidCommandFailureListener mraidCommandFailureListener) {
        new AlertDialog.Builder(context).setTitle("Save Image").setMessage("Download image to Picture gallery?").setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MraidNativeCommandHandler.this.m27003(context, str, mraidCommandFailureListener);
            }
        }).setCancelable(true).show();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m26999(int i) throws IllegalArgumentException {
        switch (i) {
            case 0:
                return "SU";
            case 1:
                return "MO";
            case 2:
                return "TU";
            case 3:
                return "WE";
            case 4:
                return "TH";
            case 5:
                return "FR";
            case 6:
                return "SA";
            default:
                throw new IllegalArgumentException("invalid day of week " + i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Date m27000(String str) {
        Date date = null;
        String[] strArr = f21044;
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            try {
                date = new SimpleDateFormat(strArr[i], Locale.US).parse(str);
                if (date != null) {
                    break;
                }
                i++;
            } catch (ParseException e) {
            }
        }
        return date;
    }

    @TargetApi(14)
    /* renamed from: 龘  reason: contains not printable characters */
    private Map<String, Object> m27001(Map<String, String> map) {
        HashMap hashMap = new HashMap();
        if (!map.containsKey(PubnativeAsset.DESCRIPTION) || !map.containsKey(TtmlNode.START)) {
            throw new IllegalArgumentException("Missing start and description fields");
        }
        hashMap.put(PubnativeAsset.TITLE, map.get(PubnativeAsset.DESCRIPTION));
        if (!map.containsKey(TtmlNode.START) || map.get(TtmlNode.START) == null) {
            throw new IllegalArgumentException("Invalid calendar event: start is null.");
        }
        Date r1 = m27000(map.get(TtmlNode.START));
        if (r1 != null) {
            hashMap.put("beginTime", Long.valueOf(r1.getTime()));
            if (map.containsKey(TtmlNode.END) && map.get(TtmlNode.END) != null) {
                Date r0 = m27000(map.get(TtmlNode.END));
                if (r0 != null) {
                    hashMap.put("endTime", Long.valueOf(r0.getTime()));
                } else {
                    throw new IllegalArgumentException("Invalid calendar event: end time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00");
                }
            }
            if (map.containsKey("location")) {
                hashMap.put("eventLocation", map.get("location"));
            }
            if (map.containsKey("summary")) {
                hashMap.put(PubnativeAsset.DESCRIPTION, map.get("summary"));
            }
            if (map.containsKey("transparency")) {
                hashMap.put("availability", Integer.valueOf(map.get("transparency").equals("transparent") ? 1 : 0));
            }
            hashMap.put("rrule", m26996(map));
            return hashMap;
        }
        throw new IllegalArgumentException("Invalid calendar event: start time is malformed. Date format expecting (yyyy-MM-DDTHH:MM:SS-xx:xx) or (yyyy-MM-DDTHH:MM-xx:xx) i.e. 2013-08-14T09:00:01-08:00");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m27002(Context context) {
        return VersionCode.currentApiLevel().isAtLeast(VersionCode.ICE_CREAM_SANDWICH) && Intents.deviceCanHandleIntent(context, new Intent("android.intent.action.INSERT").setType(ANDROID_CALENDAR_CONTENT_TYPE));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m27003(final Context context, String str, final MraidCommandFailureListener mraidCommandFailureListener) {
        AsyncTasks.safeExecuteOnExecutor(new DownloadImageAsyncTask(context, new DownloadImageAsyncTask.DownloadImageAsyncTaskListener() {
            public void onFailure() {
                Toast makeText = Toast.makeText(context, "Image failed to download.", 0);
                mraidCommandFailureListener.onFailure(new MraidCommandException("Error downloading and saving image file."));
            }

            public void onSuccess() {
            }
        }), str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m27004(Context context) {
        Intent intent = new Intent("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:"));
        return Intents.deviceCanHandleIntent(context, intent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m27005(Context context) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("sms:"));
        return Intents.deviceCanHandleIntent(context, intent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27006(Context context, String str, MraidCommandFailureListener mraidCommandFailureListener) throws MraidCommandException {
        if (!isStorePictureSupported(context)) {
            MoPubLog.d("Error downloading file - the device does not have an SD card mounted, or the Android permission is not granted.");
            throw new MraidCommandException("Error downloading file  - the device does not have an SD card mounted, or the Android permission is not granted.");
        } else if (context instanceof Activity) {
            m26998(context, str, mraidCommandFailureListener);
        } else {
            Toast makeText = Toast.makeText(context, "Downloading image to Picture gallery...", 0);
            m27003(context, str, mraidCommandFailureListener);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27007(Context context, Map<String, String> map) throws MraidCommandException {
        if (m27002(context)) {
            try {
                Map<String, Object> r0 = m27001(map);
                Intent type = new Intent("android.intent.action.INSERT").setType(ANDROID_CALENDAR_CONTENT_TYPE);
                for (String next : r0.keySet()) {
                    Object obj = r0.get(next);
                    if (obj instanceof Long) {
                        type.putExtra(next, ((Long) obj).longValue());
                    } else if (obj instanceof Integer) {
                        type.putExtra(next, ((Integer) obj).intValue());
                    } else {
                        type.putExtra(next, (String) obj);
                    }
                }
                type.setFlags(268435456);
                context.startActivity(type);
            } catch (ActivityNotFoundException e) {
                throw new MraidCommandException("Action is unsupported on this device - no calendar app installed");
            } catch (IllegalArgumentException e2) {
                throw new MraidCommandException((Throwable) e2);
            } catch (Exception e3) {
                throw new MraidCommandException((Throwable) e3);
            }
        } else {
            throw new MraidCommandException("Action is unsupported on this device (need Android version Ice Cream Sandwich or above)");
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(11)
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m27008(Activity activity, View view) {
        if (VersionCode.currentApiLevel().isBelow(VersionCode.HONEYCOMB_MR1)) {
            return false;
        }
        View view2 = view;
        while (view2.isHardwareAccelerated() && !Utils.bitMaskContainsFlag(view2.getLayerType(), 1)) {
            if (!(view2.getParent() instanceof View)) {
                Window window = activity.getWindow();
                return window != null && Utils.bitMaskContainsFlag(window.getAttributes().flags, 16777216);
            }
            view2 = (View) view2.getParent();
        }
        return false;
    }
}
