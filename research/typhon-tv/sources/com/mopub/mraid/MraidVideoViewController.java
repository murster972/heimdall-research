package com.mopub.mraid;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Drawables;
import com.mopub.mobileads.BaseVideoPlayerActivity;
import com.mopub.mobileads.BaseVideoViewController;

public class MraidVideoViewController extends BaseVideoViewController {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public ImageButton f21068;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f21069;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f21070;

    /* renamed from: 龘  reason: contains not printable characters */
    private final VideoView f21071;

    public MraidVideoViewController(Context context, Bundle bundle, Bundle bundle2, BaseVideoViewController.BaseVideoViewControllerListener baseVideoViewControllerListener) {
        super(context, (Long) null, baseVideoViewControllerListener);
        this.f21071 = new VideoView(context);
        this.f21071.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                MraidVideoViewController.this.f21068.setVisibility(0);
                MraidVideoViewController.this.m26648(true);
            }
        });
        this.f21071.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                MraidVideoViewController.this.f21068.setVisibility(0);
                MraidVideoViewController.this.m26656(false);
                return false;
            }
        });
        this.f21071.setVideoPath(bundle.getString(BaseVideoPlayerActivity.VIDEO_URL));
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m27028() {
        this.f21068 = new ImageButton(m26645());
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{-16842919}, Drawables.INTERSTITIAL_CLOSE_BUTTON_NORMAL.createDrawable(m26645()));
        stateListDrawable.addState(new int[]{16842919}, Drawables.INTERSTITIAL_CLOSE_BUTTON_PRESSED.createDrawable(m26645()));
        this.f21068.setImageDrawable(stateListDrawable);
        this.f21068.setBackgroundDrawable((Drawable) null);
        this.f21068.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MraidVideoViewController.this.m26644().onFinish();
            }
        });
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.f21069, this.f21069);
        layoutParams.addRule(11);
        layoutParams.setMargins(this.f21070, 0, this.f21070, 0);
        getLayout().addView(this.f21068, layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m27033() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m27034() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public VideoView m27035() {
        return this.f21071;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m27036() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m27037() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27038() {
        super.m26651();
        this.f21069 = Dips.asIntPixels(50.0f, m26645());
        this.f21070 = Dips.asIntPixels(8.0f, m26645());
        m27028();
        this.f21068.setVisibility(8);
        this.f21071.start();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27039(Configuration configuration) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m27040(Bundle bundle) {
    }
}
