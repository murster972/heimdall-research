package com.mopub.mraid;

import android.content.Context;
import android.view.View;
import com.mopub.common.AdReport;
import com.mopub.common.DataKeys;
import com.mopub.common.VisibleForTesting;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.AdViewController;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.factories.MraidControllerFactory;
import com.mopub.mraid.MraidController;
import java.util.Map;

class MraidBanner extends CustomEventBanner {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public CustomEventBanner.CustomEventBannerListener f20972;

    /* renamed from: 齉  reason: contains not printable characters */
    private MraidWebViewDebugListener f20973;

    /* renamed from: 龘  reason: contains not printable characters */
    private MraidController f20974;

    MraidBanner() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m26904(Map<String, String> map) {
        return map.containsKey(DataKeys.HTML_RESPONSE_BODY_KEY);
    }

    @VisibleForTesting
    public void setDebugListener(MraidWebViewDebugListener mraidWebViewDebugListener) {
        this.f20973 = mraidWebViewDebugListener;
        if (this.f20974 != null) {
            this.f20974.setDebugListener(mraidWebViewDebugListener);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26905() {
        if (this.f20974 != null) {
            this.f20974.setMraidListener((MraidController.MraidListener) null);
            this.f20974.destroy();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m26906(Context context, CustomEventBanner.CustomEventBannerListener customEventBannerListener, Map<String, Object> map, Map<String, String> map2) {
        this.f20972 = customEventBannerListener;
        if (m26904(map2)) {
            String str = map2.get(DataKeys.HTML_RESPONSE_BODY_KEY);
            try {
                this.f20974 = MraidControllerFactory.create(context, (AdReport) map.get(DataKeys.AD_REPORT_KEY), PlacementType.INLINE);
                this.f20974.setDebugListener(this.f20973);
                this.f20974.setMraidListener(new MraidController.MraidListener() {
                    public void onClose() {
                        MraidBanner.this.f20972.onBannerCollapsed();
                    }

                    public void onExpand() {
                        MraidBanner.this.f20972.onBannerExpanded();
                        MraidBanner.this.f20972.onBannerClicked();
                    }

                    public void onFailedToLoad() {
                        MraidBanner.this.f20972.onBannerFailed(MoPubErrorCode.MRAID_LOAD_ERROR);
                    }

                    public void onLoaded(View view) {
                        AdViewController.setShouldHonorServerDimensions(view);
                        MraidBanner.this.f20972.onBannerLoaded(view);
                    }

                    public void onOpen() {
                        MraidBanner.this.f20972.onBannerClicked();
                    }
                });
                this.f20974.loadContent(str);
            } catch (ClassCastException e) {
                MoPubLog.w("MRAID banner creating failed:", e);
                this.f20972.onBannerFailed(MoPubErrorCode.MRAID_LOAD_ERROR);
            }
        } else {
            this.f20972.onBannerFailed(MoPubErrorCode.MRAID_LOAD_ERROR);
        }
    }
}
