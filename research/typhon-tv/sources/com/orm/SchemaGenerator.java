package com.orm;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.orm.dsl.Column;
import com.orm.dsl.MultiUnique;
import com.orm.dsl.NotNull;
import com.orm.dsl.Unique;
import com.orm.util.MigrationFileParser;
import com.orm.util.NamingHelper;
import com.orm.util.NumberComparator;
import com.orm.util.QueryBuilder;
import com.orm.util.ReflectionUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class SchemaGenerator {

    /* renamed from: 龘  reason: contains not printable characters */
    private Context f11773;

    public SchemaGenerator(Context context) {
        this.f11773 = context;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m14822(SQLiteDatabase sQLiteDatabase, String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.f11773.getAssets().open("sugar_upgrades/" + str)));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            for (String str2 : new MigrationFileParser(sb.toString()).m14838()) {
                Log.i("Sugar script", str2);
                if (!str2.isEmpty()) {
                    sQLiteDatabase.execSQL(str2);
                }
            }
        } catch (IOException e) {
            Log.e(SugarRecord.SUGAR, e.getMessage());
        }
        Log.i(SugarRecord.SUGAR, "Script executed");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m14823(Class<?> cls, SQLiteDatabase sQLiteDatabase) {
        String r0 = m14827(cls);
        if (!r0.isEmpty()) {
            try {
                sQLiteDatabase.execSQL(r0);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m14824(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        boolean z = false;
        try {
            List<String> asList = Arrays.asList(this.f11773.getAssets().list("sugar_upgrades"));
            Collections.sort(asList, new NumberComparator());
            for (String str : asList) {
                Log.i(SugarRecord.SUGAR, "filename : " + str);
                try {
                    int intValue = Integer.valueOf(str.replace(".sql", "")).intValue();
                    if (intValue > i && intValue <= i2) {
                        m14822(sQLiteDatabase, str);
                        z = true;
                    }
                } catch (NumberFormatException e) {
                    Log.i(SugarRecord.SUGAR, "not a sugar script. ignored." + str);
                }
            }
        } catch (IOException e2) {
            Log.e(SugarRecord.SUGAR, e2.getMessage());
        }
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ArrayList<String> m14825(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor query = sQLiteDatabase.query(str, (String[]) null, (String) null, (String[]) null, (String) null, (String) null, (String) null);
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < query.getColumnCount(); i++) {
            arrayList.add(query.getColumnName(i));
        }
        query.close();
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14826(Class<?> cls, SQLiteDatabase sQLiteDatabase) {
        List<Field> r6 = ReflectionUtil.m14852((Class) cls);
        String r9 = NamingHelper.m14842(cls);
        ArrayList<String> r7 = m14825(sQLiteDatabase, r9);
        ArrayList arrayList = new ArrayList();
        for (Field next : r6) {
            String r3 = NamingHelper.m14844(next);
            String r4 = QueryBuilder.m14848(next.getType());
            if (next.isAnnotationPresent(Column.class)) {
                r3 = ((Column) next.getAnnotation(Column.class)).m6273();
            }
            if (!r7.contains(r3)) {
                StringBuilder sb = new StringBuilder("ALTER TABLE ");
                sb.append(r9).append(" ADD COLUMN ").append(r3).append(StringUtils.SPACE).append(r4);
                if (next.isAnnotationPresent(NotNull.class)) {
                    if (r4.endsWith(" NULL")) {
                        sb.delete(sb.length() - 5, sb.length());
                    }
                    sb.append(" NOT NULL");
                }
                arrayList.add(sb.toString());
            }
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            String str = (String) it2.next();
            Log.i(SugarRecord.SUGAR, str);
            sQLiteDatabase.execSQL(str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m14827(Class<?> cls) {
        Log.i(SugarRecord.SUGAR, "Create table if not exists");
        List<Field> r6 = ReflectionUtil.m14852((Class) cls);
        String r9 = NamingHelper.m14842(cls);
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(r9).append(" ( ID INTEGER PRIMARY KEY AUTOINCREMENT ");
        for (Field next : r6) {
            String r2 = NamingHelper.m14844(next);
            String r3 = QueryBuilder.m14848(next.getType());
            if (r3 != null && !r2.equalsIgnoreCase("Id")) {
                if (next.isAnnotationPresent(Column.class)) {
                    Column column = (Column) next.getAnnotation(Column.class);
                    sb.append(", ").append(column.m6273()).append(StringUtils.SPACE).append(r3);
                    if (column.m6272()) {
                        if (r3.endsWith(" NULL")) {
                            sb.delete(sb.length() - 5, sb.length());
                        }
                        sb.append(" NOT NULL");
                    }
                    if (column.m6271()) {
                        sb.append(" UNIQUE");
                    }
                } else {
                    sb.append(", ").append(r2).append(StringUtils.SPACE).append(r3);
                    if (next.isAnnotationPresent(NotNull.class)) {
                        if (r3.endsWith(" NULL")) {
                            sb.delete(sb.length() - 5, sb.length());
                        }
                        sb.append(" NOT NULL");
                    }
                    if (next.isAnnotationPresent(Unique.class)) {
                        sb.append(" UNIQUE");
                    }
                }
            }
        }
        if (cls.isAnnotationPresent(MultiUnique.class)) {
            String r4 = ((MultiUnique) cls.getAnnotation(MultiUnique.class)).m6274();
            sb.append(", UNIQUE(");
            String[] split = r4.split(",");
            for (int i = 0; i < split.length; i++) {
                sb.append(NamingHelper.m14843(split[i]));
                if (i < split.length - 1) {
                    sb.append(",");
                }
            }
            sb.append(") ON CONFLICT REPLACE");
        }
        sb.append(" ) ");
        Log.i(SugarRecord.SUGAR, "Creating table " + r9);
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14828(SQLiteDatabase sQLiteDatabase) {
        for (Class r0 : ReflectionUtil.m14851(this.f11773)) {
            m14823((Class<?>) r0, sQLiteDatabase);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14829(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        for (Class next : ReflectionUtil.m14851(this.f11773)) {
            Cursor rawQuery = sQLiteDatabase.rawQuery(String.format("select count(*) from sqlite_master where type='table' and name='%s';", new Object[]{NamingHelper.m14842((Class<?>) next)}), (String[]) null);
            if (!rawQuery.moveToFirst() || rawQuery.getInt(0) != 0) {
                m14826((Class<?>) next, sQLiteDatabase);
            } else {
                m14823((Class<?>) next, sQLiteDatabase);
            }
        }
        m14824(sQLiteDatabase, i, i2);
    }
}
