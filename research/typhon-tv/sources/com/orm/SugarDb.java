package com.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.orm.util.ManifestHelper;
import com.orm.util.SugarCursorFactory;

public class SugarDb extends SQLiteOpenHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private SQLiteDatabase f11774;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f11775 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SchemaGenerator f11776;

    public SugarDb(Context context) {
        super(context, ManifestHelper.m14835(context), new SugarCursorFactory(ManifestHelper.m14833(context)), ManifestHelper.m14836(context));
        this.f11776 = new SchemaGenerator(context);
    }

    public synchronized void close() {
        Log.d("SUGAR", "getReadableDatabase");
        this.f11775--;
        if (this.f11775 == 0) {
            Log.d("SUGAR", "closing");
            super.close();
        }
    }

    public synchronized SQLiteDatabase getReadableDatabase() {
        Log.d("SUGAR", "getReadableDatabase");
        this.f11775++;
        return super.getReadableDatabase();
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        this.f11776.m14828(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        this.f11776.m14829(sQLiteDatabase, i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized SQLiteDatabase m14830() {
        if (this.f11774 == null) {
            this.f11774 = getWritableDatabase();
        }
        return this.f11774;
    }
}
