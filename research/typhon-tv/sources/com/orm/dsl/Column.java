package com.orm.dsl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
    /* renamed from: 靐  reason: contains not printable characters */
    boolean m6271() default false;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m6272() default false;

    /* renamed from: 龘  reason: contains not printable characters */
    String m6273();
}
