package com.orm;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;
import com.orm.util.NamingHelper;
import com.orm.util.QueryBuilder;
import com.orm.util.ReflectionUtil;
import com.orm.util.SugarCursor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import net.pubnative.library.request.PubnativeRequest;

public class SugarRecord {
    public static final String SUGAR = "Sugar";
    private Long id = null;

    static class CursorIterator<E> implements Iterator<E> {

        /* renamed from: 靐  reason: contains not printable characters */
        Cursor f11777;

        /* renamed from: 龘  reason: contains not printable characters */
        Class<E> f11778;

        public CursorIterator(Class<E> cls, Cursor cursor) {
            this.f11778 = cls;
            this.f11777 = cursor;
        }

        public boolean hasNext() {
            return this.f11777 != null && !this.f11777.isClosed() && !this.f11777.isAfterLast();
        }

        public E next() {
            E e = null;
            if (this.f11777 == null || this.f11777.isAfterLast()) {
                throw new NoSuchElementException();
            }
            if (this.f11777.isBeforeFirst()) {
                this.f11777.moveToFirst();
            }
            try {
                e = this.f11778.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                SugarRecord.inflate(this.f11777, e, SugarContext.m6267().m6269());
                this.f11777.moveToNext();
                if (this.f11777.isAfterLast()) {
                    this.f11777.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                this.f11777.moveToNext();
                if (this.f11777.isAfterLast()) {
                    this.f11777.close();
                }
            } catch (Throwable th) {
                this.f11777.moveToNext();
                if (this.f11777.isAfterLast()) {
                    this.f11777.close();
                }
                throw th;
            }
            return e;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public static <T> long count(Class<?> cls) {
        return count(cls, (String) null, (String[]) null, (String) null, (String) null, (String) null);
    }

    public static <T> long count(Class<?> cls, String str, String[] strArr) {
        return count(cls, str, strArr, (String) null, (String) null, (String) null);
    }

    /* JADX INFO: finally extract failed */
    public static <T> long count(Class<?> cls, String str, String[] strArr, String str2, String str3, String str4) {
        try {
            SQLiteStatement compileStatement = getSugarDataBase().compileStatement("SELECT count(*) FROM " + NamingHelper.m14842(cls) + (!TextUtils.isEmpty(str) ? " where " + str : ""));
            if (strArr != null) {
                for (int length = strArr.length; length != 0; length--) {
                    compileStatement.bindString(length, strArr[length - 1]);
                }
            }
            try {
                long simpleQueryForLong = compileStatement.simpleQueryForLong();
                compileStatement.close();
                long j = simpleQueryForLong;
                return simpleQueryForLong;
            } catch (Throwable th) {
                compileStatement.close();
                throw th;
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static boolean delete(Object obj) {
        boolean z = true;
        Class<?> cls = obj.getClass();
        if (cls.isAnnotationPresent(Table.class)) {
            try {
                Field declaredField = cls.getDeclaredField("id");
                declaredField.setAccessible(true);
                Long l = (Long) declaredField.get(obj);
                if (l == null || l.longValue() <= 0) {
                    Log.i(SUGAR, "Cannot delete object: " + obj.getClass().getSimpleName() + " - object has not been saved");
                    return false;
                }
                if (getSugarDataBase().delete(NamingHelper.m14842(cls), "Id=?", new String[]{l.toString()}) != 1) {
                    z = false;
                }
                Log.i(SUGAR, cls.getSimpleName() + " deleted : " + l);
                return z;
            } catch (NoSuchFieldException e) {
                Log.i(SUGAR, "Cannot delete object: " + obj.getClass().getSimpleName() + " - annotated object has no id");
                return false;
            } catch (IllegalAccessException e2) {
                Log.i(SUGAR, "Cannot delete object: " + obj.getClass().getSimpleName() + " - can't access id");
                return false;
            }
        } else if (SugarRecord.class.isAssignableFrom(cls)) {
            return ((SugarRecord) obj).delete();
        } else {
            Log.i(SUGAR, "Cannot delete object: " + obj.getClass().getSimpleName() + " - not persisted");
            return false;
        }
    }

    public static <T> int deleteAll(Class<T> cls) {
        return deleteAll(cls, (String) null, new String[0]);
    }

    public static <T> int deleteAll(Class<T> cls, String str, String... strArr) {
        return getSugarDataBase().delete(NamingHelper.m14842((Class<?>) cls), str, strArr);
    }

    public static <T> int deleteInTx(Collection<T> collection) {
        SQLiteDatabase sugarDataBase = getSugarDataBase();
        int i = 0;
        try {
            sugarDataBase.beginTransaction();
            sugarDataBase.setLockingEnabled(false);
            for (T delete : collection) {
                if (delete(delete)) {
                    i++;
                }
            }
            sugarDataBase.setTransactionSuccessful();
        } catch (Exception e) {
            i = 0;
            Log.i(SUGAR, "Error in deleting in transaction " + e.getMessage());
        } finally {
            sugarDataBase.endTransaction();
            sugarDataBase.setLockingEnabled(true);
        }
        return i;
    }

    public static <T> int deleteInTx(T... tArr) {
        return deleteInTx(Arrays.asList(tArr));
    }

    public static void executeQuery(String str, String... strArr) {
        getSugarDataBase().execSQL(str, strArr);
    }

    public static <T> List<T> find(Class<T> cls, String str, String... strArr) {
        return find(cls, str, strArr, (String) null, (String) null, (String) null);
    }

    public static <T> List<T> find(Class<T> cls, String str, String[] strArr, String str2, String str3, String str4) {
        return getEntitiesFromCursor(getSugarDataBase().query(NamingHelper.m14842((Class<?>) cls), (String[]) null, str, strArr, str2, (String) null, str3, str4), cls);
    }

    public static <T> Iterator<T> findAll(Class<T> cls) {
        return findAsIterator(cls, (String) null, (String[]) null, (String) null, (String) null, (String) null);
    }

    public static <T> Iterator<T> findAsIterator(Class<T> cls, String str, String... strArr) {
        return findAsIterator(cls, str, strArr, (String) null, (String) null, (String) null);
    }

    public static <T> Iterator<T> findAsIterator(Class<T> cls, String str, String[] strArr, String str2, String str3, String str4) {
        return new CursorIterator(cls, getSugarDataBase().query(NamingHelper.m14842((Class<?>) cls), (String[]) null, str, strArr, str2, (String) null, str3, str4));
    }

    public static <T> T findById(Class<T> cls, Integer num) {
        return findById(cls, Long.valueOf((long) num.intValue()));
    }

    public static <T> T findById(Class<T> cls, Long l) {
        List<T> find = find(cls, "id=?", new String[]{String.valueOf(l)}, (String) null, (String) null, PubnativeRequest.LEGACY_ZONE_ID);
        if (find.isEmpty()) {
            return null;
        }
        return find.get(0);
    }

    public static <T> List<T> findById(Class<T> cls, String[] strArr) {
        return find(cls, "id IN (" + QueryBuilder.m14847(strArr.length) + ")", strArr);
    }

    public static <T> List<T> findWithQuery(Class<T> cls, String str, String... strArr) {
        return getEntitiesFromCursor(getSugarDataBase().rawQuery(str, strArr), cls);
    }

    public static <T> Iterator<T> findWithQueryAsIterator(Class<T> cls, String str, String... strArr) {
        return new CursorIterator(cls, getSugarDataBase().rawQuery(str, strArr));
    }

    public static <T> T first(Class<T> cls) {
        List<T> findWithQuery = findWithQuery(cls, "SELECT * FROM " + NamingHelper.m14842((Class<?>) cls) + " ORDER BY ID ASC LIMIT 1", new String[0]);
        if (findWithQuery.isEmpty()) {
            return null;
        }
        return findWithQuery.get(0);
    }

    public static <T> Cursor getCursor(Class<T> cls, String str, String[] strArr, String str2, String str3, String str4) {
        return new SugarCursor(getSugarDataBase().query(NamingHelper.m14842((Class<?>) cls), (String[]) null, str, strArr, str2, (String) null, str3, str4));
    }

    public static <T> List<T> getEntitiesFromCursor(Cursor cursor, Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            try {
                T newInstance = cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                inflate(cursor, newInstance, SugarContext.m6267().m6269());
                arrayList.add(newInstance);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }
        return arrayList;
    }

    private static SQLiteDatabase getSugarDataBase() {
        return SugarContext.m6267().m6270().m14830();
    }

    /* access modifiers changed from: private */
    public static void inflate(Cursor cursor, Object obj, Map<Object, Long> map) {
        List<Field> r0 = ReflectionUtil.m14852((Class) obj.getClass());
        if (!map.containsKey(obj)) {
            map.put(obj, Long.valueOf(cursor.getLong(cursor.getColumnIndex("ID"))));
        }
        for (Field next : r0) {
            next.setAccessible(true);
            Class<?> type = next.getType();
            if (isSugarEntity(type)) {
                try {
                    long j = cursor.getLong(cursor.getColumnIndex(NamingHelper.m14844(next)));
                    next.set(obj, j > 0 ? findById(type, Long.valueOf(j)) : null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                ReflectionUtil.m14855(cursor, next, obj);
            }
        }
    }

    public static boolean isSugarEntity(Class<?> cls) {
        return cls.isAnnotationPresent(Table.class) || SugarRecord.class.isAssignableFrom(cls);
    }

    public static <T> T last(Class<T> cls) {
        List<T> findWithQuery = findWithQuery(cls, "SELECT * FROM " + NamingHelper.m14842((Class<?>) cls) + " ORDER BY ID DESC LIMIT 1", new String[0]);
        if (findWithQuery.isEmpty()) {
            return null;
        }
        return findWithQuery.get(0);
    }

    public static <T> List<T> listAll(Class<T> cls) {
        return find(cls, (String) null, (String[]) null, (String) null, (String) null, (String) null);
    }

    public static <T> List<T> listAll(Class<T> cls, String str) {
        return find(cls, (String) null, (String[]) null, (String) null, str, (String) null);
    }

    static long save(SQLiteDatabase sQLiteDatabase, Object obj) {
        Map<Object, Long> r3 = SugarContext.m6267().m6269();
        List<Field> r1 = ReflectionUtil.m14852((Class) obj.getClass());
        ContentValues contentValues = new ContentValues(r1.size());
        Field field = null;
        for (Field next : r1) {
            ReflectionUtil.m14854(contentValues, next, obj, r3);
            if (next.getName().equals("id")) {
                field = next;
            }
        }
        if (isSugarEntity(obj.getClass()) && r3.containsKey(obj)) {
            contentValues.put("id", r3.get(obj));
        }
        long insertWithOnConflict = sQLiteDatabase.insertWithOnConflict(NamingHelper.m14842(obj.getClass()), (String) null, contentValues, 5);
        if (obj.getClass().isAnnotationPresent(Table.class)) {
            if (field != null) {
                field.setAccessible(true);
                try {
                    field.set(obj, new Long(insertWithOnConflict));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                r3.put(obj, Long.valueOf(insertWithOnConflict));
            }
        } else if (SugarRecord.class.isAssignableFrom(obj.getClass())) {
            ((SugarRecord) obj).setId(Long.valueOf(insertWithOnConflict));
        }
        Log.i(SUGAR, obj.getClass().getSimpleName() + " saved : " + insertWithOnConflict);
        return insertWithOnConflict;
    }

    public static long save(Object obj) {
        return save(getSugarDataBase(), obj);
    }

    public static <T> void saveInTx(Collection<T> collection) {
        SQLiteDatabase sugarDataBase = getSugarDataBase();
        try {
            sugarDataBase.beginTransaction();
            sugarDataBase.setLockingEnabled(false);
            for (T save : collection) {
                save(save);
            }
            sugarDataBase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.i(SUGAR, "Error in saving in transaction " + e.getMessage());
        } finally {
            sugarDataBase.endTransaction();
            sugarDataBase.setLockingEnabled(true);
        }
    }

    public static <T> void saveInTx(T... tArr) {
        saveInTx(Arrays.asList(tArr));
    }

    static long update(SQLiteDatabase sQLiteDatabase, Object obj) {
        Map<Object, Long> r7 = SugarContext.m6267().m6269();
        List<Field> r5 = ReflectionUtil.m14852((Class) obj.getClass());
        ContentValues contentValues = new ContentValues(r5.size());
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList();
        for (Field next : r5) {
            if (next.isAnnotationPresent(Unique.class)) {
                try {
                    next.setAccessible(true);
                    String r3 = NamingHelper.m14844(next);
                    Object obj2 = next.get(obj);
                    sb.append(r3).append(" = ?");
                    arrayList.add(String.valueOf(obj2));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else if (!next.getName().equals("id")) {
                ReflectionUtil.m14854(contentValues, next, obj, r7);
            }
        }
        long update = (long) sQLiteDatabase.update(NamingHelper.m14842(obj.getClass()), contentValues, sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
        return update == 0 ? save(sQLiteDatabase, obj) : update;
    }

    public static long update(Object obj) {
        return update(getSugarDataBase(), obj);
    }

    public static <T> void updateInTx(Collection<T> collection) {
        SQLiteDatabase sugarDataBase = getSugarDataBase();
        try {
            sugarDataBase.beginTransaction();
            sugarDataBase.setLockingEnabled(false);
            for (T update : collection) {
                update(update);
            }
            sugarDataBase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.i(SUGAR, "Error in saving in transaction " + e.getMessage());
        } finally {
            sugarDataBase.endTransaction();
            sugarDataBase.setLockingEnabled(true);
        }
    }

    public static <T> void updateInTx(T... tArr) {
        updateInTx(Arrays.asList(tArr));
    }

    public boolean delete() {
        Long id2 = getId();
        Class<?> cls = getClass();
        if (id2 == null || id2.longValue() <= 0) {
            Log.i(SUGAR, "Cannot delete object: " + cls.getSimpleName() + " - object has not been saved");
            return false;
        }
        Log.i(SUGAR, cls.getSimpleName() + " deleted : " + id2);
        return getSugarDataBase().delete(NamingHelper.m14842(cls), "Id=?", new String[]{id2.toString()}) == 1;
    }

    public Long getId() {
        return this.id;
    }

    /* access modifiers changed from: package-private */
    public void inflate(Cursor cursor) {
        inflate(cursor, this, SugarContext.m6267().m6269());
    }

    public long save() {
        return save(getSugarDataBase(), this);
    }

    public void setId(Long l) {
        this.id = l;
    }

    public long update() {
        return update(getSugarDataBase(), this);
    }
}
