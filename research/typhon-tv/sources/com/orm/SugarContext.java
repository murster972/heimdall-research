package com.orm;

import android.content.Context;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

public class SugarContext {

    /* renamed from: 龘  reason: contains not printable characters */
    private static SugarContext f5813 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private SugarDb f5814;

    /* renamed from: 齉  reason: contains not printable characters */
    private Map<Object, Long> f5815 = Collections.synchronizedMap(new WeakHashMap());

    private SugarContext(Context context) {
        this.f5814 = new SugarDb(context);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m6265() {
        if (this.f5814 != null) {
            this.f5814.m14830().close();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m6266() {
        if (f5813 != null) {
            f5813.m6265();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SugarContext m6267() {
        if (f5813 != null) {
            return f5813;
        }
        throw new NullPointerException("SugarContext has not been initialized properly. Call SugarContext.init(Context) in your Application.onCreate() method and SugarContext.terminate() in your Application.onTerminate() method.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m6268(Context context) {
        f5813 = new SugarContext(context);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public Map<Object, Long> m6269() {
        return this.f5815;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public SugarDb m6270() {
        return this.f5814;
    }
}
