package com.orm.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import dalvik.system.DexFile;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MultiDexHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f11780 = ("code_cache" + File.separator + "secondary-dexes");

    /* renamed from: 靐  reason: contains not printable characters */
    public static List<String> m14839(Context context) throws PackageManager.NameNotFoundException, IOException {
        ArrayList arrayList = new ArrayList();
        for (String next : m14841(context)) {
            try {
                Enumeration<String> entries = (next.endsWith(".zip") ? DexFile.loadDex(next, next + ".tmp", 0) : new DexFile(next)).entries();
                while (entries.hasMoreElements()) {
                    arrayList.add(entries.nextElement());
                }
            } catch (IOException e) {
                throw new IOException("Error at loading dex file '" + next + "'");
            }
        }
        return arrayList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static SharedPreferences m14840(Context context) {
        return context.getSharedPreferences("multidex.version", Build.VERSION.SDK_INT < 11 ? 0 : 4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<String> m14841(Context context) throws PackageManager.NameNotFoundException, IOException {
        ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
        File file = new File(applicationInfo.sourceDir);
        File file2 = new File(applicationInfo.dataDir, f11780);
        ArrayList arrayList = new ArrayList();
        arrayList.add(applicationInfo.sourceDir);
        String str = file.getName() + ".classes";
        int i = m14840(context).getInt("dex.number", 1);
        for (int i2 = 2; i2 <= i; i2++) {
            File file3 = new File(file2, str + i2 + ".zip");
            if (file3.isFile()) {
                arrayList.add(file3.getAbsolutePath());
            }
        }
        return arrayList;
    }
}
