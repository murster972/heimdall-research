package com.orm.util;

import android.content.Context;
import android.util.Log;

public class ManifestHelper {
    /* renamed from: 靐  reason: contains not printable characters */
    private static Integer m14831(Context context, String str) {
        try {
            return Integer.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getInt(str));
        } catch (Exception e) {
            Log.d("sugar", "Couldn't find config value: " + str);
            return null;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m14832(Context context) {
        String r0 = m14837(context, "DOMAIN_PACKAGE_NAME");
        return r0 == null ? "" : r0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m14833(Context context) {
        return m14834(context, "QUERY_LOG").booleanValue();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static Boolean m14834(Context context, String str) {
        boolean z = false;
        try {
            return Boolean.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getBoolean(str));
        } catch (Exception e) {
            Log.d("sugar", "Couldn't find config value: " + str);
            return z;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m14835(Context context) {
        String r0 = m14837(context, "DATABASE");
        return r0 == null ? "Sugar.db" : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m14836(Context context) {
        Integer r0 = m14831(context, "VERSION");
        if (r0 == null || r0.intValue() == 0) {
            r0 = 1;
        }
        return r0.intValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m14837(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(str);
        } catch (Exception e) {
            Log.d("sugar", "Couldn't find config value: " + str);
            return null;
        }
    }
}
