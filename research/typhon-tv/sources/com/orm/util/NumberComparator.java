package com.orm.util;

import java.util.Comparator;

public class NumberComparator implements Comparator<Object> {
    /* renamed from: 龘  reason: contains not printable characters */
    private static char m14845(String str, int i) {
        if (i >= str.length()) {
            return 0;
        }
        return str.charAt(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m14846(String str, String str2) {
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            char r1 = m14845(str, i2);
            char r2 = m14845(str2, i3);
            if (!Character.isDigit(r1) && !Character.isDigit(r2)) {
                return i;
            }
            if (!Character.isDigit(r1)) {
                return -1;
            }
            if (!Character.isDigit(r2)) {
                return 1;
            }
            if (r1 < r2) {
                if (i == 0) {
                    i = -1;
                }
            } else if (r1 > r2) {
                if (i == 0) {
                    i = 1;
                }
            } else if (r1 == 0 && r2 == 0) {
                return i;
            }
            i2++;
            i3++;
        }
    }

    public int compare(Object obj, Object obj2) {
        int r8;
        String obj3 = obj.toString();
        String obj4 = obj2.toString();
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = 0;
            int i4 = 0;
            char r2 = m14845(obj3, i);
            char r3 = m14845(obj4, i2);
            while (true) {
                if (!Character.isSpaceChar(r2) && r2 != '0') {
                    break;
                }
                i4 = r2 == '0' ? i4 + 1 : 0;
                i++;
                r2 = m14845(obj3, i);
            }
            while (true) {
                if (!Character.isSpaceChar(r3) && r3 != '0') {
                    break;
                }
                i3 = r3 == '0' ? i3 + 1 : 0;
                i2++;
                r3 = m14845(obj4, i2);
            }
            if (Character.isDigit(r2) && Character.isDigit(r3) && (r8 = m14846(obj3.substring(i), obj4.substring(i2))) != 0) {
                return r8;
            }
            if (r2 == 0 && r3 == 0) {
                return i4 - i3;
            }
            if (r2 < r3) {
                return -1;
            }
            if (r2 > r3) {
                return 1;
            }
            i++;
            i2++;
        }
    }
}
