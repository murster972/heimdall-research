package com.orm.util;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.util.Log;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

public class ReflectionUtil {
    /* renamed from: 靐  reason: contains not printable characters */
    private static List<String> m14849(Context context) throws PackageManager.NameNotFoundException, IOException {
        String r10 = ManifestHelper.m14832(context);
        ArrayList arrayList = new ArrayList();
        try {
            for (String next : MultiDexHelper.m14839(context)) {
                if (next.startsWith(r10)) {
                    arrayList.add(next);
                }
            }
        } catch (NullPointerException e) {
            Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources("");
            while (resources.hasMoreElements()) {
                ArrayList<String> arrayList2 = new ArrayList<>();
                String file = resources.nextElement().getFile();
                if (file.contains("bin") || file.contains("classes") || file.contains("retrolambda")) {
                    for (File r9 : new File(file).listFiles()) {
                        m14856(r9, (List<String>) arrayList2, "");
                    }
                    for (String str : arrayList2) {
                        if (str.startsWith(r10)) {
                            arrayList.add(str);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Class m14850(String str, Context context) {
        Class<?> cls = null;
        try {
            cls = Class.forName(str, true, context.getClass().getClassLoader());
        } catch (Throwable th) {
            Log.e(SugarRecord.SUGAR, th.getMessage() == null ? "getDomainClass " + str + " error" : th.getMessage());
        }
        if (cls == null || (((!SugarRecord.class.isAssignableFrom(cls) || SugarRecord.class.equals(cls)) && !cls.isAnnotationPresent(Table.class)) || Modifier.isAbstract(cls.getModifiers()))) {
            return null;
        }
        Log.i(SugarRecord.SUGAR, "domain class : " + cls.getSimpleName());
        return cls;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<Class> m14851(Context context) {
        ArrayList arrayList = new ArrayList();
        try {
            for (String r0 : m14849(context)) {
                Class r1 = m14850(r0, context);
                if (r1 != null) {
                    arrayList.add(r1);
                }
            }
        } catch (IOException e) {
            Log.e(SugarRecord.SUGAR, e.getMessage());
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e(SugarRecord.SUGAR, e2.getMessage());
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<Field> m14852(Class cls) {
        List<Field> r1 = SugarConfig.m14857(cls);
        if (r1 != null) {
            return r1;
        }
        Log.d(SugarRecord.SUGAR, "Fetching properties");
        ArrayList<Field> arrayList = new ArrayList<>();
        m14853((List<Field>) arrayList, (Class<?>) cls);
        ArrayList arrayList2 = new ArrayList();
        for (Field field : arrayList) {
            if (!field.isAnnotationPresent(Ignore.class) && !Modifier.isStatic(field.getModifiers()) && !Modifier.isTransient(field.getModifiers())) {
                arrayList2.add(field);
            }
        }
        SugarConfig.m14858(cls, arrayList2);
        return arrayList2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static List<Field> m14853(List<Field> list, Class<?> cls) {
        Collections.addAll(list, cls.getDeclaredFields());
        return cls.getSuperclass() != null ? m14853(list, (Class<?>) cls.getSuperclass()) : list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m14854(ContentValues contentValues, Field field, Object obj, Map<Object, Long> map) {
        field.setAccessible(true);
        Class<?> type = field.getType();
        try {
            String r0 = NamingHelper.m14844(field);
            Object obj2 = field.get(obj);
            if (type.isAnnotationPresent(Table.class)) {
                try {
                    Field declaredField = type.getDeclaredField("id");
                    declaredField.setAccessible(true);
                    contentValues.put(r0, declaredField != null ? String.valueOf(declaredField.get(obj2)) : "0");
                } catch (NoSuchFieldException e) {
                    if (map.containsKey(obj2)) {
                        contentValues.put(r0, map.get(obj2));
                    }
                }
            } else if (SugarRecord.class.isAssignableFrom(type)) {
                contentValues.put(r0, obj2 != null ? String.valueOf(((SugarRecord) obj2).getId()) : "0");
            } else if (type.equals(Short.class) || type.equals(Short.TYPE)) {
                contentValues.put(r0, (Short) obj2);
            } else if (type.equals(Integer.class) || type.equals(Integer.TYPE)) {
                contentValues.put(r0, (Integer) obj2);
            } else if (type.equals(Long.class) || type.equals(Long.TYPE)) {
                contentValues.put(r0, (Long) obj2);
            } else if (type.equals(Float.class) || type.equals(Float.TYPE)) {
                contentValues.put(r0, (Float) obj2);
            } else if (type.equals(Double.class) || type.equals(Double.TYPE)) {
                contentValues.put(r0, (Double) obj2);
            } else if (type.equals(Boolean.class) || type.equals(Boolean.TYPE)) {
                contentValues.put(r0, (Boolean) obj2);
            } else if (type.equals(BigDecimal.class)) {
                try {
                    contentValues.put(r0, field.get(obj).toString());
                } catch (NullPointerException e2) {
                    contentValues.putNull(r0);
                }
            } else if (Timestamp.class.equals(type)) {
                try {
                    contentValues.put(r0, Long.valueOf(((Timestamp) field.get(obj)).getTime()));
                } catch (NullPointerException e3) {
                    contentValues.put(r0, (Long) null);
                }
            } else if (Date.class.equals(type)) {
                try {
                    contentValues.put(r0, Long.valueOf(((Date) field.get(obj)).getTime()));
                } catch (NullPointerException e4) {
                    contentValues.put(r0, (Long) null);
                }
            } else if (Calendar.class.equals(type)) {
                try {
                    contentValues.put(r0, Long.valueOf(((Calendar) field.get(obj)).getTimeInMillis()));
                } catch (NullPointerException e5) {
                    contentValues.put(r0, (Long) null);
                }
            } else if (type.equals(byte[].class)) {
                if (obj2 == null) {
                    contentValues.put(r0, "".getBytes());
                } else {
                    contentValues.put(r0, (byte[]) obj2);
                }
            } else if (obj2 == null) {
                contentValues.putNull(r0);
            } else if (type.isEnum()) {
                contentValues.put(r0, ((Enum) obj2).name());
            } else {
                contentValues.put(r0, String.valueOf(obj2));
            }
        } catch (IllegalAccessException e6) {
            Log.e(SugarRecord.SUGAR, e6.getMessage());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m14855(Cursor cursor, Field field, Object obj) {
        field.setAccessible(true);
        try {
            Class<?> type = field.getType();
            String r8 = NamingHelper.m14844(field);
            int columnIndex = cursor.getColumnIndex(r8);
            if (columnIndex < 0) {
                Log.e("SUGAR", "Invalid colName, you should upgrade database");
            } else if (cursor.isNull(columnIndex)) {
            } else {
                if (r8.equalsIgnoreCase("id")) {
                    field.set(obj, Long.valueOf(cursor.getLong(columnIndex)));
                } else if (type.equals(Long.TYPE) || type.equals(Long.class)) {
                    field.set(obj, Long.valueOf(cursor.getLong(columnIndex)));
                } else if (type.equals(String.class)) {
                    String string = cursor.getString(columnIndex);
                    if (string != null && string.equals("null")) {
                        string = null;
                    }
                    field.set(obj, string);
                } else if (type.equals(Double.TYPE) || type.equals(Double.class)) {
                    field.set(obj, Double.valueOf(cursor.getDouble(columnIndex)));
                } else if (type.equals(Boolean.TYPE) || type.equals(Boolean.class)) {
                    field.set(obj, Boolean.valueOf(cursor.getString(columnIndex).equals(PubnativeRequest.LEGACY_ZONE_ID)));
                } else if (type.equals(Integer.TYPE) || type.equals(Integer.class)) {
                    field.set(obj, Integer.valueOf(cursor.getInt(columnIndex)));
                } else if (type.equals(Float.TYPE) || type.equals(Float.class)) {
                    field.set(obj, Float.valueOf(cursor.getFloat(columnIndex)));
                } else if (type.equals(Short.TYPE) || type.equals(Short.class)) {
                    field.set(obj, Short.valueOf(cursor.getShort(columnIndex)));
                } else if (type.equals(BigDecimal.class)) {
                    String string2 = cursor.getString(columnIndex);
                    field.set(obj, (string2 == null || !string2.equals("null")) ? new BigDecimal(string2) : null);
                } else if (type.equals(Timestamp.class)) {
                    field.set(obj, new Timestamp(cursor.getLong(columnIndex)));
                } else if (type.equals(Date.class)) {
                    field.set(obj, new Date(cursor.getLong(columnIndex)));
                } else if (type.equals(Calendar.class)) {
                    long j = cursor.getLong(columnIndex);
                    Calendar instance = Calendar.getInstance();
                    instance.setTimeInMillis(j);
                    field.set(obj, instance);
                } else if (type.equals(byte[].class)) {
                    if (cursor.getBlob(columnIndex) == null) {
                        field.set(obj, "".getBytes());
                    } else {
                        field.set(obj, cursor.getBlob(columnIndex));
                    }
                } else if (Enum.class.isAssignableFrom(type)) {
                    try {
                        Field field2 = field;
                        Object obj2 = obj;
                        field2.set(obj2, field.getType().getMethod("valueOf", new Class[]{String.class}).invoke(field.getType(), new Object[]{cursor.getString(columnIndex)}));
                    } catch (Exception e) {
                        Log.e(SugarRecord.SUGAR, "Enum cannot be read from Sqlite3 database. Please check the type of field " + field.getName());
                    }
                } else {
                    Log.e(SugarRecord.SUGAR, "Class cannot be read from Sqlite3 database. Please check the type of field " + field.getName() + "(" + field.getType().getName() + ")");
                }
            }
        } catch (IllegalArgumentException e2) {
            Log.e("field set error", e2.getMessage());
        } catch (IllegalAccessException e3) {
            Log.e("field set error", e3.getMessage());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m14856(File file, List<String> list, String str) {
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                if ("".equals(str)) {
                    m14856(file2, list, file.getName());
                } else {
                    m14856(file2, list, str + "." + file.getName());
                }
            }
            return;
        }
        String name = file.getName();
        if (name.endsWith(".class")) {
            name = name.substring(0, name.length() - ".class".length());
        }
        if ("".equals(str)) {
            list.add(name);
        } else {
            list.add(str + "." + name);
        }
    }
}
