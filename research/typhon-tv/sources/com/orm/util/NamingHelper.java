package com.orm.util;

import com.orm.dsl.Column;
import com.orm.dsl.Table;
import java.lang.reflect.Field;

public class NamingHelper {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m14842(Class<?> cls) {
        if (!cls.isAnnotationPresent(Table.class)) {
            return m14843(cls.getSimpleName());
        }
        Table table = (Table) cls.getAnnotation(Table.class);
        return "".equals(table.m6275()) ? m14843(cls.getSimpleName()) : table.m6275();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m14843(String str) {
        if (str.equalsIgnoreCase("_id")) {
            return "_id";
        }
        StringBuilder sb = new StringBuilder();
        char[] charArray = str.toCharArray();
        int i = 0;
        while (i < charArray.length) {
            char c = i > 0 ? charArray[i - 1] : 0;
            char c2 = charArray[i];
            char c3 = i < charArray.length + -1 ? charArray[i + 1] : 0;
            if ((i == 0) || Character.isLowerCase(c2) || Character.isDigit(c2)) {
                sb.append(Character.toUpperCase(c2));
            } else if (Character.isUpperCase(c2)) {
                if (!Character.isLetterOrDigit(c)) {
                    sb.append(c2);
                } else if (Character.isLowerCase(c)) {
                    sb.append('_').append(c2);
                } else if (c3 <= 0 || !Character.isLowerCase(c3)) {
                    sb.append(c2);
                } else {
                    sb.append('_').append(c2);
                }
            }
            i++;
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m14844(Field field) {
        return field.isAnnotationPresent(Column.class) ? ((Column) field.getAnnotation(Column.class)).m6273() : m14843(field.getName());
    }
}
