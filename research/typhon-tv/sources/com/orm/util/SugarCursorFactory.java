package com.orm.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.util.Log;

public class SugarCursorFactory implements SQLiteDatabase.CursorFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f11782;

    public SugarCursorFactory() {
        this.f11782 = false;
    }

    public SugarCursorFactory(boolean z) {
        this.f11782 = z;
    }

    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        if (this.f11782) {
            Log.d("SQL Log", sQLiteQuery.toString());
        }
        return new SQLiteCursor(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
    }
}
