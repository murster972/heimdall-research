package com.p003if.p004do;

import com.p003if.p004do.p014if.ChoiBounge;
import com.p003if.p004do.p014if.KyoKusanagi;
import java.math.BigInteger;

/* renamed from: com.if.do.ShingoYabuki  reason: invalid package */
public final class ShingoYabuki extends SaishuKusanagi {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Class<?>[] f11372 = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};

    /* renamed from: 靐  reason: contains not printable characters */
    private Object f11373;

    public ShingoYabuki(Boolean bool) {
        m14160((Object) bool);
    }

    public ShingoYabuki(Number number) {
        m14160((Object) number);
    }

    ShingoYabuki(Object obj) {
        m14160(obj);
    }

    public ShingoYabuki(String str) {
        m14160((Object) str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m14150(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> isAssignableFrom : f11372) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m14151(ShingoYabuki shingoYabuki) {
        if (!(shingoYabuki.f11373 instanceof Number)) {
            return false;
        }
        Number number = (Number) shingoYabuki.f11373;
        return (number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte);
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ShingoYabuki shingoYabuki = (ShingoYabuki) obj;
        if (this.f11373 == null) {
            return shingoYabuki.f11373 == null;
        }
        if (m14151(this) && m14151(shingoYabuki)) {
            return m14157().longValue() == shingoYabuki.m14157().longValue();
        }
        if (!(this.f11373 instanceof Number) || !(shingoYabuki.f11373 instanceof Number)) {
            return this.f11373.equals(shingoYabuki.f11373);
        }
        double doubleValue = m14157().doubleValue();
        double doubleValue2 = shingoYabuki.m14157().doubleValue();
        if (doubleValue == doubleValue2 || (Double.isNaN(doubleValue) && Double.isNaN(doubleValue2))) {
            z = true;
        }
        return z;
    }

    public int hashCode() {
        if (this.f11373 == null) {
            return 31;
        }
        if (m14151(this)) {
            long longValue = m14157().longValue();
            return (int) (longValue ^ (longValue >>> 32));
        } else if (!(this.f11373 instanceof Number)) {
            return this.f11373.hashCode();
        } else {
            long doubleToLongBits = Double.doubleToLongBits(m14157().doubleValue());
            return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m14152() {
        return m14154() ? m14157().intValue() : Integer.parseInt(m14159());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m14153() {
        return m14161() ? m14162().booleanValue() : Boolean.parseBoolean(m14159());
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m14154() {
        return this.f11373 instanceof Number;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m14155() {
        return this.f11373 instanceof String;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m14156() {
        return m14154() ? m14157().longValue() : Long.parseLong(m14159());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Number m14157() {
        return this.f11373 instanceof String ? new ChoiBounge((String) this.f11373) : (Number) this.f11373;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public double m14158() {
        return m14154() ? m14157().doubleValue() : Double.parseDouble(m14159());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m14159() {
        return m14154() ? m14157().toString() : m14161() ? m14162().toString() : (String) this.f11373;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14160(Object obj) {
        if (obj instanceof Character) {
            this.f11373 = String.valueOf(((Character) obj).charValue());
            return;
        }
        KyoKusanagi.m14250((obj instanceof Number) || m14150(obj));
        this.f11373 = obj;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14161() {
        return this.f11373 instanceof Boolean;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public Boolean m14162() {
        return (Boolean) this.f11373;
    }
}
