package com.p003if.p004do;

import com.p003if.p004do.p014if.RugalBernstein;
import com.p003if.p004do.p016int.BenimaruNikaido;
import com.p003if.p004do.p016int.ChinGentsai;
import com.p003if.p004do.p016int.KyoKusanagi;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: com.if.do.Goenitz  reason: invalid package */
public final class Goenitz {
    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14113(KyoKusanagi kyoKusanagi) throws LeonaHeidern, Chris {
        boolean r1 = kyoKusanagi.m14527();
        kyoKusanagi.m14526(true);
        try {
            SaishuKusanagi r0 = RugalBernstein.m14252(kyoKusanagi);
            kyoKusanagi.m14526(r1);
            return r0;
        } catch (StackOverflowError e) {
            throw new ChizuruKagura("Failed parsing JSON source: " + kyoKusanagi + " to Json", e);
        } catch (OutOfMemoryError e2) {
            throw new ChizuruKagura("Failed parsing JSON source: " + kyoKusanagi + " to Json", e2);
        } catch (Throwable th) {
            kyoKusanagi.m14526(r1);
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14114(Reader reader) throws LeonaHeidern, Chris {
        try {
            KyoKusanagi kyoKusanagi = new KyoKusanagi(reader);
            SaishuKusanagi r1 = m14113(kyoKusanagi);
            if (r1.m14142() || kyoKusanagi.m14511() == BenimaruNikaido.END_DOCUMENT) {
                return r1;
            }
            throw new Chris("Did not consume the entire document.");
        } catch (ChinGentsai e) {
            throw new Chris((Throwable) e);
        } catch (IOException e2) {
            throw new LeonaHeidern((Throwable) e2);
        } catch (NumberFormatException e3) {
            throw new Chris((Throwable) e3);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14115(String str) throws Chris {
        return m14114((Reader) new StringReader(str));
    }
}
