package com.p003if.p004do;

import com.p003if.p004do.p014if.RugalBernstein;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;

/* renamed from: com.if.do.KDash  reason: invalid package */
final class KDash<T> extends Maxima<T> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Maxima<T> f11357;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Whip f11358;

    /* renamed from: 靐  reason: contains not printable characters */
    private final IoriYagami<T> f11359;

    /* renamed from: 麤  reason: contains not printable characters */
    private final com.p003if.p004do.p013for.KyoKusanagi<T> f11360;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ChoiBounge f11361;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Shermie<T> f11362;

    /* renamed from: com.if.do.KDash$KyoKusanagi */
    private static class KyoKusanagi implements Whip {

        /* renamed from: 连任  reason: contains not printable characters */
        private final IoriYagami<?> f11363;

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f11364;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Shermie<?> f11365;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Class<?> f11366;

        /* renamed from: 龘  reason: contains not printable characters */
        private final com.p003if.p004do.p013for.KyoKusanagi<?> f11367;

        private KyoKusanagi(Object obj, com.p003if.p004do.p013for.KyoKusanagi<?> kyoKusanagi, boolean z, Class<?> cls) {
            this.f11365 = obj instanceof Shermie ? (Shermie) obj : null;
            this.f11363 = obj instanceof IoriYagami ? (IoriYagami) obj : null;
            com.p003if.p004do.p014if.KyoKusanagi.m14250((this.f11365 == null && this.f11363 == null) ? false : true);
            this.f11367 = kyoKusanagi;
            this.f11364 = z;
            this.f11366 = cls;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public <T> Maxima<T> m14124(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
            if (this.f11367 != null ? this.f11367.equals(kyoKusanagi) || (this.f11364 && this.f11367.b() == kyoKusanagi.a()) : this.f11366.isAssignableFrom(kyoKusanagi.a())) {
                return new KDash(this.f11365, this.f11363, choiBounge, kyoKusanagi, this);
            }
            return null;
        }
    }

    private KDash(Shermie<T> shermie, IoriYagami<T> ioriYagami, ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi, Whip whip) {
        this.f11362 = shermie;
        this.f11359 = ioriYagami;
        this.f11361 = choiBounge;
        this.f11360 = kyoKusanagi;
        this.f11358 = whip;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Whip m14119(com.p003if.p004do.p013for.KyoKusanagi<?> kyoKusanagi, Object obj) {
        return new KyoKusanagi(obj, kyoKusanagi, kyoKusanagi.b() == kyoKusanagi.a(), (Class) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Maxima<T> m14120() {
        Maxima<T> maxima = this.f11357;
        if (maxima != null) {
            return maxima;
        }
        Maxima<T> r0 = this.f11361.m14080(this.f11358, this.f11360);
        this.f11357 = r0;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Whip m14121(com.p003if.p004do.p013for.KyoKusanagi<?> kyoKusanagi, Object obj) {
        return new KyoKusanagi(obj, kyoKusanagi, false, (Class) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public T m14122(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
        if (this.f11359 == null) {
            return m14120().m14130(kyoKusanagi);
        }
        SaishuKusanagi r0 = RugalBernstein.m14252(kyoKusanagi);
        if (r0.m14142()) {
            return null;
        }
        return this.f11359.b(r0, this.f11360.b(), this.f11361.f11334);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14123(GoroDaimon goroDaimon, T t) throws IOException {
        if (this.f11362 == null) {
            m14120().m14132(goroDaimon, t);
        } else if (t == null) {
            goroDaimon.m14470();
        } else {
            RugalBernstein.m14254(this.f11362.m14149(t, this.f11360.b(), this.f11361.f11331), goroDaimon);
        }
    }
}
