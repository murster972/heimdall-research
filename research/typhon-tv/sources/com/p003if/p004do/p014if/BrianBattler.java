package com.p003if.p004do.p014if;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.if.do.if.BrianBattler  reason: invalid package */
public final class BrianBattler {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Map<Class<?>, Class<?>> f11376;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Map<Class<?>, Class<?>> f11377;

    static {
        HashMap hashMap = new HashMap(16);
        HashMap hashMap2 = new HashMap(16);
        m14196(hashMap, hashMap2, Boolean.TYPE, Boolean.class);
        m14196(hashMap, hashMap2, Byte.TYPE, Byte.class);
        m14196(hashMap, hashMap2, Character.TYPE, Character.class);
        m14196(hashMap, hashMap2, Double.TYPE, Double.class);
        m14196(hashMap, hashMap2, Float.TYPE, Float.class);
        m14196(hashMap, hashMap2, Integer.TYPE, Integer.class);
        m14196(hashMap, hashMap2, Long.TYPE, Long.class);
        m14196(hashMap, hashMap2, Short.TYPE, Short.class);
        m14196(hashMap, hashMap2, Void.TYPE, Void.class);
        f11377 = Collections.unmodifiableMap(hashMap);
        f11376 = Collections.unmodifiableMap(hashMap2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Class<T> m14195(Class<T> cls) {
        Class<T> cls2 = f11377.get(KyoKusanagi.m14249(cls));
        return cls2 == null ? cls : cls2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m14196(Map<Class<?>, Class<?>> map, Map<Class<?>, Class<?>> map2, Class<?> cls, Class<?> cls2) {
        map.put(cls, cls2);
        map2.put(cls2, cls);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m14197(Type type) {
        return f11377.containsKey(type);
    }
}
