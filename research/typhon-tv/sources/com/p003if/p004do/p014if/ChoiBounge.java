package com.p003if.p004do.p014if;

import java.io.ObjectStreamException;
import java.math.BigDecimal;

/* renamed from: com.if.do.if.ChoiBounge  reason: invalid package */
public final class ChoiBounge extends Number {
    private final String a;

    public ChoiBounge(String str) {
        this.a = str;
    }

    private Object writeReplace() throws ObjectStreamException {
        return new BigDecimal(this.a);
    }

    public double doubleValue() {
        return Double.parseDouble(this.a);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChoiBounge)) {
            return false;
        }
        ChoiBounge choiBounge = (ChoiBounge) obj;
        return this.a == choiBounge.a || this.a.equals(choiBounge.a);
    }

    public float floatValue() {
        return Float.parseFloat(this.a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.a);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.a);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.a).longValue();
        }
    }

    public String toString() {
        return this.a;
    }
}
