package com.p003if.p004do.p014if;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

/* renamed from: com.if.do.if.BenimaruNikaido  reason: invalid package */
public final class BenimaruNikaido {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Type[] f11375 = new Type[0];

    /* renamed from: com.if.do.if.BenimaruNikaido$BenimaruNikaido  reason: collision with other inner class name */
    private static final class C0025BenimaruNikaido implements Serializable, ParameterizedType {
        private static final long serialVersionUID = 0;
        private final Type a;
        private final Type b;
        private final Type[] c;

        public C0025BenimaruNikaido(Type type, Type type2, Type... typeArr) {
            if (type2 instanceof Class) {
                Class cls = (Class) type2;
                KyoKusanagi.m14250(type != null || (Modifier.isStatic(cls.getModifiers()) || cls.getEnclosingClass() == null));
            }
            this.a = type == null ? null : BenimaruNikaido.m14182(type);
            this.b = BenimaruNikaido.m14182(type2);
            this.c = (Type[]) typeArr.clone();
            for (int i = 0; i < this.c.length; i++) {
                KyoKusanagi.m14249(this.c[i]);
                BenimaruNikaido.m14176(this.c[i]);
                this.c[i] = BenimaruNikaido.m14182(this.c[i]);
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && BenimaruNikaido.m14194((Type) this, (Type) (ParameterizedType) obj);
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.c.clone();
        }

        public Type getOwnerType() {
            return this.a;
        }

        public Type getRawType() {
            return this.b;
        }

        public int hashCode() {
            return (Arrays.hashCode(this.c) ^ this.b.hashCode()) ^ BenimaruNikaido.m14178((Object) this.a);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder((this.c.length + 1) * 30);
            sb.append(BenimaruNikaido.m14173(this.b));
            if (this.c.length == 0) {
                return sb.toString();
            }
            sb.append("<").append(BenimaruNikaido.m14173(this.c[0]));
            for (int i = 1; i < this.c.length; i++) {
                sb.append(", ").append(BenimaruNikaido.m14173(this.c[i]));
            }
            return sb.append(">").toString();
        }
    }

    /* renamed from: com.if.do.if.BenimaruNikaido$GoroDaimon */
    private static final class GoroDaimon implements Serializable, WildcardType {
        private static final long serialVersionUID = 0;
        private final Type a;
        private final Type b;

        public GoroDaimon(Type[] typeArr, Type[] typeArr2) {
            boolean z = true;
            KyoKusanagi.m14250(typeArr2.length <= 1);
            KyoKusanagi.m14250(typeArr.length == 1);
            if (typeArr2.length == 1) {
                KyoKusanagi.m14249(typeArr2[0]);
                BenimaruNikaido.m14176(typeArr2[0]);
                KyoKusanagi.m14250(typeArr[0] != Object.class ? false : z);
                this.b = BenimaruNikaido.m14182(typeArr2[0]);
                this.a = Object.class;
                return;
            }
            KyoKusanagi.m14249(typeArr[0]);
            BenimaruNikaido.m14176(typeArr[0]);
            this.b = null;
            this.a = BenimaruNikaido.m14182(typeArr[0]);
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && BenimaruNikaido.m14194((Type) this, (Type) (WildcardType) obj);
        }

        public Type[] getLowerBounds() {
            if (this.b == null) {
                return BenimaruNikaido.f11375;
            }
            return new Type[]{this.b};
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.a};
        }

        public int hashCode() {
            return (this.b != null ? this.b.hashCode() + 31 : 1) ^ (this.a.hashCode() + 31);
        }

        public String toString() {
            return this.b != null ? "? super " + BenimaruNikaido.m14173(this.b) : this.a == Object.class ? "?" : "? extends " + BenimaruNikaido.m14173(this.a);
        }
    }

    /* renamed from: com.if.do.if.BenimaruNikaido$KyoKusanagi */
    private static final class KyoKusanagi implements Serializable, GenericArrayType {
        private static final long serialVersionUID = 0;
        private final Type a;

        public KyoKusanagi(Type type) {
            this.a = BenimaruNikaido.m14182(type);
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && BenimaruNikaido.m14194((Type) this, (Type) (GenericArrayType) obj);
        }

        public Type getGenericComponentType() {
            return this.a;
        }

        public int hashCode() {
            return this.a.hashCode();
        }

        public String toString() {
            return BenimaruNikaido.m14173(this.a) + "[]";
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m14173(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static Type m14174(Type type) {
        return type instanceof GenericArrayType ? ((GenericArrayType) type).getGenericComponentType() : ((Class) type).getComponentType();
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public static void m14176(Type type) {
        KyoKusanagi.m14250(!(type instanceof Class) || !((Class) type).isPrimitive());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static Class<?> m14177(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            KyoKusanagi.m14250(rawType instanceof Class);
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(m14177(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return m14177(((WildcardType) type).getUpperBounds()[0]);
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + (type == null ? "null" : type.getClass().getName()));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static int m14178(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static Type m14179(Type type, Class<?> cls, Class<?> cls2) {
        KyoKusanagi.m14250(cls2.isAssignableFrom(cls));
        return m14191(type, cls, m14190(type, cls, cls2));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static WildcardType m14180(Type type) {
        return new GoroDaimon(new Type[]{type}, f11375);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Type[] m14181(Type type, Class<?> cls) {
        if (type == Properties.class) {
            return new Type[]{String.class, String.class};
        }
        Type r0 = m14179(type, cls, Map.class);
        if (r0 instanceof ParameterizedType) {
            return ((ParameterizedType) r0).getActualTypeArguments();
        }
        return new Type[]{Object.class, Object.class};
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static Type m14182(Type type) {
        if (type instanceof Class) {
            Class cls = (Class) type;
            return cls.isArray() ? new KyoKusanagi(m14182(cls.getComponentType())) : cls;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return new C0025BenimaruNikaido(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new KyoKusanagi(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType wildcardType = (WildcardType) type;
            return new GoroDaimon(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static WildcardType m14183(Type type) {
        return new GoroDaimon(new Type[]{Object.class}, new Type[]{type});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m14185(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Class<?> m14186(TypeVariable<?> typeVariable) {
        Object genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static GenericArrayType m14187(Type type) {
        return new KyoKusanagi(type);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ParameterizedType m14188(Type type, Type type2, Type... typeArr) {
        return new C0025BenimaruNikaido(type, type2, typeArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Type m14189(Type type, Class<?> cls) {
        Type r0 = m14179(type, cls, Collection.class);
        if (r0 instanceof WildcardType) {
            r0 = ((WildcardType) r0).getUpperBounds()[0];
        }
        return r0 instanceof ParameterizedType ? ((ParameterizedType) r0).getActualTypeArguments()[0] : Object.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Type m14190(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return m14190(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return m14190(cls.getGenericSuperclass(), (Class<?>) superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Type m14191(Type type, Class<?> cls, Type type2) {
        Type r1;
        Type[] typeArr;
        boolean z;
        while (true) {
            Type type3 = type2;
            if (type3 instanceof TypeVariable) {
                TypeVariable typeVariable = (TypeVariable) type3;
                type2 = m14192(type, cls, (TypeVariable<?>) typeVariable);
                if (type2 == typeVariable) {
                    return type2;
                }
            } else if ((type3 instanceof Class) && ((Class) type3).isArray()) {
                Class cls2 = (Class) type3;
                Class<?> componentType = cls2.getComponentType();
                Type r2 = m14191(type, cls, (Type) componentType);
                return componentType != r2 ? m14187(r2) : cls2;
            } else if (type3 instanceof GenericArrayType) {
                GenericArrayType genericArrayType = (GenericArrayType) type3;
                Type genericComponentType = genericArrayType.getGenericComponentType();
                Type r22 = m14191(type, cls, genericComponentType);
                return genericComponentType != r22 ? m14187(r22) : genericArrayType;
            } else if (type3 instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type3;
                Type ownerType = parameterizedType.getOwnerType();
                Type r7 = m14191(type, cls, ownerType);
                boolean z2 = r7 != ownerType;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                int length = actualTypeArguments.length;
                int i = 0;
                boolean z3 = z2;
                while (i < length) {
                    Type r9 = m14191(type, cls, actualTypeArguments[i]);
                    if (r9 != actualTypeArguments[i]) {
                        if (!z3) {
                            typeArr = (Type[]) actualTypeArguments.clone();
                            z = true;
                        } else {
                            typeArr = actualTypeArguments;
                            z = z3;
                        }
                        typeArr[i] = r9;
                    } else {
                        typeArr = actualTypeArguments;
                        z = z3;
                    }
                    i++;
                    actualTypeArguments = typeArr;
                    z3 = z;
                }
                return z3 ? m14188(r7, parameterizedType.getRawType(), actualTypeArguments) : parameterizedType;
            } else if (!(type3 instanceof WildcardType)) {
                return type3;
            } else {
                WildcardType wildcardType = (WildcardType) type3;
                Type[] lowerBounds = wildcardType.getLowerBounds();
                Type[] upperBounds = wildcardType.getUpperBounds();
                if (lowerBounds.length != 1) {
                    return (upperBounds.length != 1 || (r1 = m14191(type, cls, upperBounds[0])) == upperBounds[0]) ? wildcardType : m14180(r1);
                }
                Type r23 = m14191(type, cls, lowerBounds[0]);
                return r23 != lowerBounds[0] ? m14183(r23) : wildcardType;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Type m14192(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> r1 = m14186(typeVariable);
        if (r1 == null) {
            return typeVariable;
        }
        Type r0 = m14190(type, cls, r1);
        if (!(r0 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) r0).getActualTypeArguments()[m14185((Object[]) r1.getTypeParameters(), (Object) typeVariable)];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m14193(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m14194(Type type, Type type2) {
        boolean z = true;
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            if (!m14193((Object) parameterizedType.getOwnerType(), (Object) parameterizedType2.getOwnerType()) || !parameterizedType.getRawType().equals(parameterizedType2.getRawType()) || !Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                z = false;
            }
            return z;
        } else if (type instanceof GenericArrayType) {
            if (type2 instanceof GenericArrayType) {
                return m14194(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
            }
            return false;
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                z = false;
            }
            return z;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                z = false;
            }
            return z;
        }
    }
}
