package com.p003if.p004do.p014if;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: com.if.do.if.HeavyD  reason: invalid package */
public final class HeavyD<K, V> extends AbstractMap<K, V> implements Serializable {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Comparator<Comparable> f11414 = new Comparator<Comparable>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ boolean f11415 = (!HeavyD.class.desiredAssertionStatus());
    Comparator<? super K> a;
    ChinGentsai<K, V> b;
    int c;
    int d;
    final ChinGentsai<K, V> e;
    private HeavyD<K, V>.KyoKusanagi h;
    private HeavyD<K, V>.BenimaruNikaido i;

    /* renamed from: com.if.do.if.HeavyD$BenimaruNikaido */
    final class BenimaruNikaido extends AbstractSet<K> {
        BenimaruNikaido() {
        }

        public void clear() {
            HeavyD.this.clear();
        }

        public boolean contains(Object obj) {
            return HeavyD.this.containsKey(obj);
        }

        public Iterator<K> iterator() {
            return new HeavyD<K, V>.GoroDaimon<K>() {
                {
                    HeavyD heavyD = HeavyD.this;
                }

                public K next() {
                    return m14241().f11418;
                }
            };
        }

        public boolean remove(Object obj) {
            return HeavyD.this.m14233(obj) != null;
        }

        public int size() {
            return HeavyD.this.c;
        }
    }

    /* renamed from: com.if.do.if.HeavyD$ChinGentsai */
    static final class ChinGentsai<K, V> implements Map.Entry<K, V> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final K f11418;

        /* renamed from: ʼ  reason: contains not printable characters */
        V f11419;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f11420;

        /* renamed from: 连任  reason: contains not printable characters */
        ChinGentsai<K, V> f11421;

        /* renamed from: 靐  reason: contains not printable characters */
        ChinGentsai<K, V> f11422;

        /* renamed from: 麤  reason: contains not printable characters */
        ChinGentsai<K, V> f11423;

        /* renamed from: 齉  reason: contains not printable characters */
        ChinGentsai<K, V> f11424;

        /* renamed from: 龘  reason: contains not printable characters */
        ChinGentsai<K, V> f11425;

        ChinGentsai() {
            this.f11418 = null;
            this.f11421 = this;
            this.f11423 = this;
        }

        ChinGentsai(ChinGentsai<K, V> chinGentsai, K k, ChinGentsai<K, V> chinGentsai2, ChinGentsai<K, V> chinGentsai3) {
            this.f11425 = chinGentsai;
            this.f11418 = k;
            this.f11420 = 1;
            this.f11423 = chinGentsai2;
            this.f11421 = chinGentsai3;
            chinGentsai3.f11423 = this;
            chinGentsai2.f11421 = this;
        }

        /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                r0 = 0
                boolean r1 = r4 instanceof java.util.Map.Entry
                if (r1 == 0) goto L_0x001c
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r1 = r3.f11418
                if (r1 != 0) goto L_0x001d
                java.lang.Object r1 = r4.getKey()
                if (r1 != 0) goto L_0x001c
            L_0x0011:
                V r1 = r3.f11419
                if (r1 != 0) goto L_0x002a
                java.lang.Object r1 = r4.getValue()
                if (r1 != 0) goto L_0x001c
            L_0x001b:
                r0 = 1
            L_0x001c:
                return r0
            L_0x001d:
                K r1 = r3.f11418
                java.lang.Object r2 = r4.getKey()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x0011
            L_0x002a:
                V r1 = r3.f11419
                java.lang.Object r2 = r4.getValue()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x001b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.p003if.p004do.p014if.HeavyD.ChinGentsai.equals(java.lang.Object):boolean");
        }

        public K getKey() {
            return this.f11418;
        }

        public V getValue() {
            return this.f11419;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.f11418 == null ? 0 : this.f11418.hashCode();
            if (this.f11419 != null) {
                i = this.f11419.hashCode();
            }
            return hashCode ^ i;
        }

        public V setValue(V v) {
            V v2 = this.f11419;
            this.f11419 = v;
            return v2;
        }

        public String toString() {
            return this.f11418 + "=" + this.f11419;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public ChinGentsai<K, V> m14239() {
            for (ChinGentsai<K, V> chinGentsai = this.f11424; chinGentsai != null; chinGentsai = chinGentsai.f11424) {
                this = chinGentsai;
            }
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ChinGentsai<K, V> m14240() {
            for (ChinGentsai<K, V> chinGentsai = this.f11422; chinGentsai != null; chinGentsai = chinGentsai.f11422) {
                this = chinGentsai;
            }
            return this;
        }
    }

    /* renamed from: com.if.do.if.HeavyD$GoroDaimon */
    private abstract class GoroDaimon<T> implements Iterator<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        ChinGentsai<K, V> f11427;

        /* renamed from: 麤  reason: contains not printable characters */
        int f11428;

        /* renamed from: 齉  reason: contains not printable characters */
        ChinGentsai<K, V> f11429;

        private GoroDaimon() {
            this.f11427 = HeavyD.this.e.f11423;
            this.f11429 = null;
            this.f11428 = HeavyD.this.d;
        }

        public final boolean hasNext() {
            return this.f11427 != HeavyD.this.e;
        }

        public final void remove() {
            if (this.f11429 == null) {
                throw new IllegalStateException();
            }
            HeavyD.this.m14237(this.f11429, true);
            this.f11429 = null;
            this.f11428 = HeavyD.this.d;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final ChinGentsai<K, V> m14241() {
            ChinGentsai<K, V> chinGentsai = this.f11427;
            if (chinGentsai == HeavyD.this.e) {
                throw new NoSuchElementException();
            } else if (HeavyD.this.d != this.f11428) {
                throw new ConcurrentModificationException();
            } else {
                this.f11427 = chinGentsai.f11423;
                this.f11429 = chinGentsai;
                return chinGentsai;
            }
        }
    }

    /* renamed from: com.if.do.if.HeavyD$KyoKusanagi */
    class KyoKusanagi extends AbstractSet<Map.Entry<K, V>> {
        KyoKusanagi() {
        }

        public void clear() {
            HeavyD.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && HeavyD.this.m14236((Map.Entry<?, ?>) (Map.Entry) obj) != null;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new HeavyD<K, V>.GoroDaimon<Map.Entry<K, V>>() {
                {
                    HeavyD heavyD = HeavyD.this;
                }

                /* renamed from: 靐  reason: contains not printable characters */
                public Map.Entry<K, V> next() {
                    return m14241();
                }
            };
        }

        public boolean remove(Object obj) {
            ChinGentsai r2;
            if (!(obj instanceof Map.Entry) || (r2 = HeavyD.this.m14236((Map.Entry<?, ?>) (Map.Entry) obj)) == null) {
                return false;
            }
            HeavyD.this.m14237(r2, true);
            return true;
        }

        public int size() {
            return HeavyD.this.c;
        }
    }

    public HeavyD() {
        this(f11414);
    }

    public HeavyD(Comparator<? super K> comparator) {
        this.c = 0;
        this.d = 0;
        this.e = new ChinGentsai<>();
        this.a = comparator == null ? f11414 : comparator;
    }

    private Object writeReplace() throws ObjectStreamException {
        return new LinkedHashMap(this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m14228(ChinGentsai<K, V> chinGentsai) {
        int i2 = 0;
        ChinGentsai<K, V> chinGentsai2 = chinGentsai.f11422;
        ChinGentsai<K, V> chinGentsai3 = chinGentsai.f11424;
        ChinGentsai<K, V> chinGentsai4 = chinGentsai2.f11422;
        ChinGentsai<K, V> chinGentsai5 = chinGentsai2.f11424;
        chinGentsai.f11422 = chinGentsai5;
        if (chinGentsai5 != null) {
            chinGentsai5.f11425 = chinGentsai;
        }
        m14231(chinGentsai, chinGentsai2);
        chinGentsai2.f11424 = chinGentsai;
        chinGentsai.f11425 = chinGentsai2;
        chinGentsai.f11420 = Math.max(chinGentsai3 != null ? chinGentsai3.f11420 : 0, chinGentsai5 != null ? chinGentsai5.f11420 : 0) + 1;
        int i3 = chinGentsai.f11420;
        if (chinGentsai4 != null) {
            i2 = chinGentsai4.f11420;
        }
        chinGentsai2.f11420 = Math.max(i3, i2) + 1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m14229(ChinGentsai<K, V> chinGentsai, boolean z) {
        while (chinGentsai != null) {
            ChinGentsai<K, V> chinGentsai2 = chinGentsai.f11422;
            ChinGentsai<K, V> chinGentsai3 = chinGentsai.f11424;
            int i2 = chinGentsai2 != null ? chinGentsai2.f11420 : 0;
            int i3 = chinGentsai3 != null ? chinGentsai3.f11420 : 0;
            int i4 = i2 - i3;
            if (i4 == -2) {
                ChinGentsai<K, V> chinGentsai4 = chinGentsai3.f11422;
                ChinGentsai<K, V> chinGentsai5 = chinGentsai3.f11424;
                int i5 = (chinGentsai4 != null ? chinGentsai4.f11420 : 0) - (chinGentsai5 != null ? chinGentsai5.f11420 : 0);
                if (i5 == -1 || (i5 == 0 && !z)) {
                    m14230(chinGentsai);
                } else if (f11415 || i5 == 1) {
                    m14228(chinGentsai3);
                    m14230(chinGentsai);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i4 == 2) {
                ChinGentsai<K, V> chinGentsai6 = chinGentsai2.f11422;
                ChinGentsai<K, V> chinGentsai7 = chinGentsai2.f11424;
                int i6 = (chinGentsai6 != null ? chinGentsai6.f11420 : 0) - (chinGentsai7 != null ? chinGentsai7.f11420 : 0);
                if (i6 == 1 || (i6 == 0 && !z)) {
                    m14228(chinGentsai);
                } else if (f11415 || i6 == -1) {
                    m14230(chinGentsai2);
                    m14228(chinGentsai);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i4 == 0) {
                chinGentsai.f11420 = i2 + 1;
                if (z) {
                    return;
                }
            } else if (f11415 || i4 == -1 || i4 == 1) {
                chinGentsai.f11420 = Math.max(i2, i3) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
            chinGentsai = chinGentsai.f11425;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14230(ChinGentsai<K, V> chinGentsai) {
        int i2 = 0;
        ChinGentsai<K, V> chinGentsai2 = chinGentsai.f11422;
        ChinGentsai<K, V> chinGentsai3 = chinGentsai.f11424;
        ChinGentsai<K, V> chinGentsai4 = chinGentsai3.f11422;
        ChinGentsai<K, V> chinGentsai5 = chinGentsai3.f11424;
        chinGentsai.f11424 = chinGentsai4;
        if (chinGentsai4 != null) {
            chinGentsai4.f11425 = chinGentsai;
        }
        m14231(chinGentsai, chinGentsai3);
        chinGentsai3.f11422 = chinGentsai;
        chinGentsai.f11425 = chinGentsai3;
        chinGentsai.f11420 = Math.max(chinGentsai2 != null ? chinGentsai2.f11420 : 0, chinGentsai4 != null ? chinGentsai4.f11420 : 0) + 1;
        int i3 = chinGentsai.f11420;
        if (chinGentsai5 != null) {
            i2 = chinGentsai5.f11420;
        }
        chinGentsai3.f11420 = Math.max(i3, i2) + 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14231(ChinGentsai<K, V> chinGentsai, ChinGentsai<K, V> chinGentsai2) {
        ChinGentsai<K, V> chinGentsai3 = chinGentsai.f11425;
        chinGentsai.f11425 = null;
        if (chinGentsai2 != null) {
            chinGentsai2.f11425 = chinGentsai3;
        }
        if (chinGentsai3 == null) {
            this.b = chinGentsai2;
        } else if (chinGentsai3.f11422 == chinGentsai) {
            chinGentsai3.f11422 = chinGentsai2;
        } else if (f11415 || chinGentsai3.f11424 == chinGentsai) {
            chinGentsai3.f11424 = chinGentsai2;
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m14232(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public void clear() {
        this.b = null;
        this.c = 0;
        this.d++;
        ChinGentsai<K, V> chinGentsai = this.e;
        chinGentsai.f11421 = chinGentsai;
        chinGentsai.f11423 = chinGentsai;
    }

    public boolean containsKey(Object obj) {
        return m14234(obj) != null;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        HeavyD<K, V>.KyoKusanagi kyoKusanagi = this.h;
        if (kyoKusanagi != null) {
            return kyoKusanagi;
        }
        HeavyD<K, V>.KyoKusanagi kyoKusanagi2 = new KyoKusanagi();
        this.h = kyoKusanagi2;
        return kyoKusanagi2;
    }

    public V get(Object obj) {
        ChinGentsai r0 = m14234(obj);
        if (r0 != null) {
            return r0.f11419;
        }
        return null;
    }

    public Set<K> keySet() {
        HeavyD<K, V>.BenimaruNikaido benimaruNikaido = this.i;
        if (benimaruNikaido != null) {
            return benimaruNikaido;
        }
        HeavyD<K, V>.BenimaruNikaido benimaruNikaido2 = new BenimaruNikaido();
        this.i = benimaruNikaido2;
        return benimaruNikaido2;
    }

    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        ChinGentsai r0 = m14235(k, true);
        V v2 = r0.f11419;
        r0.f11419 = v;
        return v2;
    }

    public V remove(Object obj) {
        ChinGentsai r0 = m14233(obj);
        if (r0 != null) {
            return r0.f11419;
        }
        return null;
    }

    public int size() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public ChinGentsai<K, V> m14233(Object obj) {
        ChinGentsai<K, V> r0 = m14234(obj);
        if (r0 != null) {
            m14237(r0, true);
        }
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ChinGentsai<K, V> m14234(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return m14235(obj, false);
        } catch (ClassCastException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ChinGentsai<K, V> m14235(K k, boolean z) {
        int i2;
        ChinGentsai<K, V> chinGentsai;
        Comparator<? super K> comparator = this.a;
        ChinGentsai<K, V> chinGentsai2 = this.b;
        if (chinGentsai2 != null) {
            Comparable comparable = comparator == f11414 ? (Comparable) k : null;
            while (true) {
                i2 = comparable != null ? comparable.compareTo(chinGentsai2.f11418) : comparator.compare(k, chinGentsai2.f11418);
                if (i2 == 0) {
                    return chinGentsai2;
                }
                ChinGentsai<K, V> chinGentsai3 = i2 < 0 ? chinGentsai2.f11422 : chinGentsai2.f11424;
                if (chinGentsai3 == null) {
                    break;
                }
                chinGentsai2 = chinGentsai3;
            }
        } else {
            i2 = 0;
        }
        if (!z) {
            return null;
        }
        ChinGentsai<K, V> chinGentsai4 = this.e;
        if (chinGentsai2 != null) {
            chinGentsai = new ChinGentsai<>(chinGentsai2, k, chinGentsai4, chinGentsai4.f11421);
            if (i2 < 0) {
                chinGentsai2.f11422 = chinGentsai;
            } else {
                chinGentsai2.f11424 = chinGentsai;
            }
            m14229(chinGentsai2, true);
        } else if (comparator != f11414 || (k instanceof Comparable)) {
            chinGentsai = new ChinGentsai<>(chinGentsai2, k, chinGentsai4, chinGentsai4.f11421);
            this.b = chinGentsai;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.c++;
        this.d++;
        return chinGentsai;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ChinGentsai<K, V> m14236(Map.Entry<?, ?> entry) {
        ChinGentsai<K, V> r0 = m14234((Object) entry.getKey());
        if (r0 != null && m14232((Object) r0.f11419, (Object) entry.getValue())) {
            return r0;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14237(ChinGentsai<K, V> chinGentsai, boolean z) {
        int i2;
        int i3 = 0;
        if (z) {
            chinGentsai.f11421.f11423 = chinGentsai.f11423;
            chinGentsai.f11423.f11421 = chinGentsai.f11421;
        }
        ChinGentsai<K, V> chinGentsai2 = chinGentsai.f11422;
        ChinGentsai<K, V> chinGentsai3 = chinGentsai.f11424;
        ChinGentsai<K, V> chinGentsai4 = chinGentsai.f11425;
        if (chinGentsai2 == null || chinGentsai3 == null) {
            if (chinGentsai2 != null) {
                m14231(chinGentsai, chinGentsai2);
                chinGentsai.f11422 = null;
            } else if (chinGentsai3 != null) {
                m14231(chinGentsai, chinGentsai3);
                chinGentsai.f11424 = null;
            } else {
                m14231(chinGentsai, (ChinGentsai<K, V>) null);
            }
            m14229(chinGentsai4, false);
            this.c--;
            this.d++;
            return;
        }
        ChinGentsai<K, V> r0 = chinGentsai2.f11420 > chinGentsai3.f11420 ? chinGentsai2.m14239() : chinGentsai3.m14240();
        m14237(r0, false);
        ChinGentsai<K, V> chinGentsai5 = chinGentsai.f11422;
        if (chinGentsai5 != null) {
            i2 = chinGentsai5.f11420;
            r0.f11422 = chinGentsai5;
            chinGentsai5.f11425 = r0;
            chinGentsai.f11422 = null;
        } else {
            i2 = 0;
        }
        ChinGentsai<K, V> chinGentsai6 = chinGentsai.f11424;
        if (chinGentsai6 != null) {
            i3 = chinGentsai6.f11420;
            r0.f11424 = chinGentsai6;
            chinGentsai6.f11425 = r0;
            chinGentsai.f11424 = null;
        }
        r0.f11420 = Math.max(i2, i3) + 1;
        m14231(chinGentsai, r0);
    }
}
