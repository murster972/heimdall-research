package com.p003if.p004do.p014if;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: com.if.do.if.IoriYagami  reason: invalid package */
public abstract class IoriYagami {
    /* renamed from: 龘  reason: contains not printable characters */
    public static IoriYagami m14243() {
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            final Object obj = declaredField.get((Object) null);
            final Method method = cls.getMethod("allocateInstance", new Class[]{Class.class});
            return new IoriYagami() {
                /* renamed from: 龘  reason: contains not printable characters */
                public <T> T m14245(Class<T> cls) throws Exception {
                    return method.invoke(obj, new Object[]{cls});
                }
            };
        } catch (Exception e) {
            try {
                Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", new Class[]{Class.class});
                declaredMethod.setAccessible(true);
                final int intValue = ((Integer) declaredMethod.invoke((Object) null, new Object[]{Object.class})).intValue();
                final Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", new Class[]{Class.class, Integer.TYPE});
                declaredMethod2.setAccessible(true);
                return new IoriYagami() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public <T> T m14246(Class<T> cls) throws Exception {
                        return declaredMethod2.invoke((Object) null, new Object[]{cls, Integer.valueOf(intValue)});
                    }
                };
            } catch (Exception e2) {
                try {
                    final Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", new Class[]{Class.class, Class.class});
                    declaredMethod3.setAccessible(true);
                    return new IoriYagami() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public <T> T m14247(Class<T> cls) throws Exception {
                            return declaredMethod3.invoke((Object) null, new Object[]{cls, Object.class});
                        }
                    };
                } catch (Exception e3) {
                    return new IoriYagami() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public <T> T m14248(Class<T> cls) {
                            throw new UnsupportedOperationException("Cannot allocate " + cls);
                        }
                    };
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract <T> T m14244(Class<T> cls) throws Exception;
}
