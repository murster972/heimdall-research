package com.p003if.p004do.p014if;

import com.p003if.p004do.LeonaHeidern;
import com.p003if.p004do.LuckyGlauber;
import com.p003if.p004do.p013for.KyoKusanagi;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/* renamed from: com.if.do.if.GoroDaimon  reason: invalid package */
public final class GoroDaimon {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Map<Type, LuckyGlauber<?>> f11392;

    public GoroDaimon(Map<Type, LuckyGlauber<?>> map) {
        this.f11392 = map;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private <T> LuckyGlauber<T> m14212(final Type type, final Class<? super T> cls) {
        return new LuckyGlauber<T>() {

            /* renamed from: 麤  reason: contains not printable characters */
            private final IoriYagami f11402 = IoriYagami.m14243();

            /* renamed from: 龘  reason: contains not printable characters */
            public T m14222() {
                try {
                    return this.f11402.m14244(cls);
                } catch (Exception e) {
                    throw new RuntimeException("Unable to invoke no-args constructor for " + type + ". " + "Register an InstanceCreator with Gson for this type may fix this problem.", e);
                }
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private <T> LuckyGlauber<T> m14213(Class<? super T> cls) {
        try {
            final Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14224() {
                    try {
                        return declaredConstructor.newInstance((Object[]) null);
                    } catch (InstantiationException e) {
                        throw new RuntimeException("Failed to invoke " + declaredConstructor + " with no args", e);
                    } catch (InvocationTargetException e2) {
                        throw new RuntimeException("Failed to invoke " + declaredConstructor + " with no args", e2.getTargetException());
                    } catch (IllegalAccessException e3) {
                        throw new AssertionError(e3);
                    }
                }
            };
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private <T> LuckyGlauber<T> m14214(final Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14225() {
                    return new TreeSet();
                }
            } : EnumSet.class.isAssignableFrom(cls) ? new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14226() {
                    if (type instanceof ParameterizedType) {
                        Type type = ((ParameterizedType) type).getActualTypeArguments()[0];
                        if (type instanceof Class) {
                            return EnumSet.noneOf((Class) type);
                        }
                        throw new LeonaHeidern("Invalid EnumSet type: " + type.toString());
                    }
                    throw new LeonaHeidern("Invalid EnumSet type: " + type.toString());
                }
            } : Set.class.isAssignableFrom(cls) ? new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14227() {
                    return new LinkedHashSet();
                }
            } : Queue.class.isAssignableFrom(cls) ? new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14217() {
                    return new LinkedList();
                }
            } : new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14218() {
                    return new ArrayList();
                }
            };
        }
        if (Map.class.isAssignableFrom(cls)) {
            return SortedMap.class.isAssignableFrom(cls) ? new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14219() {
                    return new TreeMap();
                }
            } : (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(KyoKusanagi.a(((ParameterizedType) type).getActualTypeArguments()[0]).a())) ? new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14221() {
                    return new HeavyD();
                }
            } : new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14220() {
                    return new LinkedHashMap();
                }
            };
        }
        return null;
    }

    public String toString() {
        return this.f11392.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> LuckyGlauber<T> m14215(KyoKusanagi<T> kyoKusanagi) {
        final Type b = kyoKusanagi.b();
        Class<? super T> a = kyoKusanagi.a();
        final LuckyGlauber luckyGlauber = this.f11392.get(b);
        if (luckyGlauber != null) {
            return new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14216() {
                    return luckyGlauber.m14129(b);
                }
            };
        }
        final LuckyGlauber luckyGlauber2 = this.f11392.get(a);
        if (luckyGlauber2 != null) {
            return new LuckyGlauber<T>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public T m14223() {
                    return luckyGlauber2.m14129(b);
                }
            };
        }
        LuckyGlauber<T> r0 = m14213(a);
        if (r0 != null) {
            return r0;
        }
        LuckyGlauber<T> r02 = m14214(b, a);
        return r02 == null ? m14212(b, a) : r02;
    }
}
