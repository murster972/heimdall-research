package com.p003if.p004do.p014if;

import com.p003if.p004do.ChizuruKagura;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Mature;
import com.p003if.p004do.SaishuKusanagi;
import com.p003if.p004do.p014if.p015do.LeonaHeidern;
import com.p003if.p004do.p016int.ChinGentsai;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

/* renamed from: com.if.do.if.RugalBernstein  reason: invalid package */
public final class RugalBernstein {

    /* renamed from: com.if.do.if.RugalBernstein$KyoKusanagi */
    private static final class KyoKusanagi extends Writer {

        /* renamed from: 靐  reason: contains not printable characters */
        private final C0026KyoKusanagi f11437;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Appendable f11438;

        /* renamed from: com.if.do.if.RugalBernstein$KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
        static class C0026KyoKusanagi implements CharSequence {

            /* renamed from: 龘  reason: contains not printable characters */
            char[] f11439;

            C0026KyoKusanagi() {
            }

            public char charAt(int i) {
                return this.f11439[i];
            }

            public int length() {
                return this.f11439.length;
            }

            public CharSequence subSequence(int i, int i2) {
                return new String(this.f11439, i, i2 - i);
            }
        }

        private KyoKusanagi(Appendable appendable) {
            this.f11437 = new C0026KyoKusanagi();
            this.f11438 = appendable;
        }

        public void close() {
        }

        public void flush() {
        }

        public void write(int i) throws IOException {
            this.f11438.append((char) i);
        }

        public void write(char[] cArr, int i, int i2) throws IOException {
            this.f11437.f11439 = cArr;
            this.f11438.append(this.f11437, i, i + i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static SaishuKusanagi m14252(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws ChizuruKagura {
        boolean z = true;
        try {
            kyoKusanagi.m14511();
            z = false;
            return LeonaHeidern.f11494.m14130(kyoKusanagi);
        } catch (EOFException e) {
            if (z) {
                return Mature.f5653;
            }
            throw new Chris((Throwable) e);
        } catch (ChinGentsai e2) {
            throw new Chris((Throwable) e2);
        } catch (IOException e3) {
            throw new com.p003if.p004do.LeonaHeidern((Throwable) e3);
        } catch (NumberFormatException e4) {
            throw new Chris((Throwable) e4);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Writer m14253(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new KyoKusanagi(appendable);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m14254(SaishuKusanagi saishuKusanagi, GoroDaimon goroDaimon) throws IOException {
        LeonaHeidern.f11494.m14132(goroDaimon, saishuKusanagi);
    }
}
