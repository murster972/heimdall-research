package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p013for.KyoKusanagi;
import com.p003if.p004do.p016int.BenimaruNikaido;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/* renamed from: com.if.do.if.do.GoroDaimon  reason: invalid package */
public final class GoroDaimon extends Maxima<Date> {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Whip f11466 = new Whip() {
        /* renamed from: 龘  reason: contains not printable characters */
        public <T> Maxima<T> m14316(ChoiBounge choiBounge, KyoKusanagi<T> kyoKusanagi) {
            if (kyoKusanagi.a() == Date.class) {
                return new GoroDaimon();
            }
            return null;
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private final DateFormat f11467 = DateFormat.getDateTimeInstance(2, 2, Locale.US);

    /* renamed from: 麤  reason: contains not printable characters */
    private final DateFormat f11468 = m14310();

    /* renamed from: 齉  reason: contains not printable characters */
    private final DateFormat f11469 = DateFormat.getDateTimeInstance(2, 2);

    /* renamed from: 龘  reason: contains not printable characters */
    private static DateFormat m14310() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized Date m14311(String str) {
        Date parse;
        try {
            parse = this.f11469.parse(str);
        } catch (ParseException e) {
            try {
                parse = this.f11467.parse(str);
            } catch (ParseException e2) {
                try {
                    parse = this.f11468.parse(str);
                } catch (ParseException e3) {
                    throw new Chris(str, e3);
                }
            }
        }
        return parse;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Date m14312(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
        if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
            return m14311(kyoKusanagi.m14513());
        }
        kyoKusanagi.m14519();
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m14314(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Date date) throws IOException {
        if (date == null) {
            goroDaimon.m14470();
        } else {
            goroDaimon.m14476(this.f11467.format(date));
        }
    }
}
