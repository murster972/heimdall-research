package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p013for.KyoKusanagi;
import com.p003if.p004do.p014if.HeavyD;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;
import java.util.ArrayList;

/* renamed from: com.if.do.if.do.LuckyGlauber  reason: invalid package */
public final class LuckyGlauber extends Maxima<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Whip f11542 = new Whip() {
        /* renamed from: 龘  reason: contains not printable characters */
        public <T> Maxima<T> m14451(ChoiBounge choiBounge, KyoKusanagi<T> kyoKusanagi) {
            if (kyoKusanagi.a() == Object.class) {
                return new LuckyGlauber(choiBounge);
            }
            return null;
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private final ChoiBounge f11543;

    private LuckyGlauber(ChoiBounge choiBounge) {
        this.f11543 = choiBounge;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Object m14449(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
        switch (kyoKusanagi.m14511()) {
            case BEGIN_ARRAY:
                ArrayList arrayList = new ArrayList();
                kyoKusanagi.m14525();
                while (kyoKusanagi.m14521()) {
                    arrayList.add(m14449(kyoKusanagi));
                }
                kyoKusanagi.m14522();
                return arrayList;
            case BEGIN_OBJECT:
                HeavyD heavyD = new HeavyD();
                kyoKusanagi.m14524();
                while (kyoKusanagi.m14521()) {
                    heavyD.put(kyoKusanagi.m14512(), m14449(kyoKusanagi));
                }
                kyoKusanagi.m14523();
                return heavyD;
            case STRING:
                return kyoKusanagi.m14513();
            case NUMBER:
                return Double.valueOf(kyoKusanagi.m14520());
            case BOOLEAN:
                return Boolean.valueOf(kyoKusanagi.m14518());
            case NULL:
                kyoKusanagi.m14519();
                return null;
            default:
                throw new IllegalStateException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14450(GoroDaimon goroDaimon, Object obj) throws IOException {
        if (obj == null) {
            goroDaimon.m14470();
            return;
        }
        Maxima<?> r0 = this.f11543.m14082(obj.getClass());
        if (r0 instanceof LuckyGlauber) {
            goroDaimon.m14478();
            goroDaimon.m14474();
            return;
        }
        r0.m14132(goroDaimon, obj);
    }
}
