package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.BrianBattler;
import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Mature;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.SaishuKusanagi;
import com.p003if.p004do.ShingoYabuki;
import com.p003if.p004do.Vice;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p016int.BenimaruNikaido;
import com.p003if.p004do.p016int.GoroDaimon;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

/* renamed from: com.if.do.if.do.LeonaHeidern  reason: invalid package */
public final class LeonaHeidern {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final Maxima<Boolean> f11481 = new Maxima<Boolean>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Boolean m14393(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return Boolean.valueOf(kyoKusanagi.m14513());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14396(GoroDaimon goroDaimon, Boolean bool) throws IOException {
            goroDaimon.m14476(bool == null ? "null" : bool.toString());
        }
    };

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public static final Whip f11482 = m14333(InetAddress.class, f11511);

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final Whip f11483 = m14337(Boolean.TYPE, Boolean.class, f11516);

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public static final Whip f11484 = m14336(UUID.class, f11486);

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final Maxima<Number> f11485 = new Maxima<Number>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Number m14397(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            try {
                return Byte.valueOf((byte) kyoKusanagi.m14514());
            } catch (NumberFormatException e) {
                throw new Chris((Throwable) e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14400(GoroDaimon goroDaimon, Number number) throws IOException {
            goroDaimon.m14484(number);
        }
    };

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public static final Maxima<UUID> f11486 = new Maxima<UUID>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public UUID m14358(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return UUID.fromString(kyoKusanagi.m14513());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14360(GoroDaimon goroDaimon, UUID uuid) throws IOException {
            goroDaimon.m14476(uuid == null ? null : uuid.toString());
        }
    };

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final Whip f11487 = m14337(Integer.TYPE, Integer.class, f11493);

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public static final Maxima<Calendar> f11488 = new Maxima<Calendar>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Calendar m14367(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            kyoKusanagi.m14524();
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (kyoKusanagi.m14511() != BenimaruNikaido.END_OBJECT) {
                String r7 = kyoKusanagi.m14512();
                int r0 = kyoKusanagi.m14514();
                if ("year".equals(r7)) {
                    i6 = r0;
                } else if ("month".equals(r7)) {
                    i5 = r0;
                } else if ("dayOfMonth".equals(r7)) {
                    i4 = r0;
                } else if ("hourOfDay".equals(r7)) {
                    i3 = r0;
                } else if ("minute".equals(r7)) {
                    i2 = r0;
                } else if ("second".equals(r7)) {
                    i = r0;
                }
            }
            kyoKusanagi.m14523();
            return new GregorianCalendar(i6, i5, i4, i3, i2, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14369(GoroDaimon goroDaimon, Calendar calendar) throws IOException {
            if (calendar == null) {
                goroDaimon.m14470();
                return;
            }
            goroDaimon.m14478();
            goroDaimon.m14485("year");
            goroDaimon.m14483((long) calendar.get(1));
            goroDaimon.m14485("month");
            goroDaimon.m14483((long) calendar.get(2));
            goroDaimon.m14485("dayOfMonth");
            goroDaimon.m14483((long) calendar.get(5));
            goroDaimon.m14485("hourOfDay");
            goroDaimon.m14483((long) calendar.get(11));
            goroDaimon.m14485("minute");
            goroDaimon.m14483((long) calendar.get(12));
            goroDaimon.m14485("second");
            goroDaimon.m14483((long) calendar.get(13));
            goroDaimon.m14474();
        }
    };

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final Maxima<Number> f11489 = new Maxima<Number>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Number m14413(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            try {
                return Long.valueOf(kyoKusanagi.m14516());
            } catch (NumberFormatException e) {
                throw new Chris((Throwable) e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14416(GoroDaimon goroDaimon, Number number) throws IOException {
            goroDaimon.m14484(number);
        }
    };

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public static final Whip f11490 = new Whip() {
        /* renamed from: 龘  reason: contains not printable characters */
        public <T> Maxima<T> m14362(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
            if (kyoKusanagi.a() != Timestamp.class) {
                return null;
            }
            final Maxima<Date> r1 = choiBounge.m14082(Date.class);
            return new Maxima<Timestamp>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Timestamp m14363(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
                    Date date = (Date) r1.m14130(kyoKusanagi);
                    if (date != null) {
                        return new Timestamp(date.getTime());
                    }
                    return null;
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public void m14365(GoroDaimon goroDaimon, Timestamp timestamp) throws IOException {
                    r1.m14132(goroDaimon, timestamp);
                }
            };
        }
    };

    /* renamed from: ˆ  reason: contains not printable characters */
    public static final Whip f11491 = m14337(Character.TYPE, Character.class, f11501);

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public static final Maxima<Locale> f11492 = new Maxima<Locale>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Locale m14371(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(kyoKusanagi.m14513(), EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken3 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            return (nextToken2 == null && nextToken3 == null) ? new Locale(nextToken) : nextToken3 == null ? new Locale(nextToken, nextToken2) : new Locale(nextToken, nextToken2, nextToken3);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14373(GoroDaimon goroDaimon, Locale locale) throws IOException {
            goroDaimon.m14476(locale == null ? null : locale.toString());
        }
    };

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final Maxima<Number> f11493 = new Maxima<Number>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Number m14409(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            try {
                return Integer.valueOf(kyoKusanagi.m14514());
            } catch (NumberFormatException e) {
                throw new Chris((Throwable) e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14412(GoroDaimon goroDaimon, Number number) throws IOException {
            goroDaimon.m14484(number);
        }
    };

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public static final Maxima<SaishuKusanagi> f11494 = new Maxima<SaishuKusanagi>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public SaishuKusanagi m14375(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            switch (AnonymousClass26.f11539[kyoKusanagi.m14511().ordinal()]) {
                case 1:
                    return new ShingoYabuki((Number) new com.p003if.p004do.p014if.ChoiBounge(kyoKusanagi.m14513()));
                case 2:
                    return new ShingoYabuki(Boolean.valueOf(kyoKusanagi.m14518()));
                case 3:
                    return new ShingoYabuki(kyoKusanagi.m14513());
                case 4:
                    kyoKusanagi.m14519();
                    return Mature.f5653;
                case 5:
                    BrianBattler brianBattler = new BrianBattler();
                    kyoKusanagi.m14525();
                    while (kyoKusanagi.m14521()) {
                        brianBattler.m14060(m14375(kyoKusanagi));
                    }
                    kyoKusanagi.m14522();
                    return brianBattler;
                case 6:
                    Vice vice = new Vice();
                    kyoKusanagi.m14524();
                    while (kyoKusanagi.m14521()) {
                        vice.m14170(kyoKusanagi.m14512(), m14375(kyoKusanagi));
                    }
                    kyoKusanagi.m14523();
                    return vice;
                default:
                    throw new IllegalArgumentException();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14378(GoroDaimon goroDaimon, SaishuKusanagi saishuKusanagi) throws IOException {
            if (saishuKusanagi == null || saishuKusanagi.m14142()) {
                goroDaimon.m14470();
            } else if (saishuKusanagi.m14141()) {
                ShingoYabuki r0 = saishuKusanagi.m14138();
                if (r0.m14154()) {
                    goroDaimon.m14484(r0.m14157());
                } else if (r0.m14161()) {
                    goroDaimon.m14486(r0.m14153());
                } else {
                    goroDaimon.m14476(r0.m14159());
                }
            } else if (saishuKusanagi.m14136()) {
                goroDaimon.m14475();
                Iterator<SaishuKusanagi> it2 = saishuKusanagi.m14137().iterator();
                while (it2.hasNext()) {
                    m14378(goroDaimon, it2.next());
                }
                goroDaimon.m14480();
            } else if (saishuKusanagi.m14140()) {
                goroDaimon.m14478();
                for (Map.Entry next : saishuKusanagi.m14139().m14169()) {
                    goroDaimon.m14485((String) next.getKey());
                    m14378(goroDaimon, (SaishuKusanagi) next.getValue());
                }
                goroDaimon.m14474();
            } else {
                throw new IllegalArgumentException("Couldn't write " + saishuKusanagi.getClass());
            }
        }
    };

    /* renamed from: ˉ  reason: contains not printable characters */
    public static final Maxima<String> f11495 = new Maxima<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public String m14425(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            BenimaruNikaido r0 = kyoKusanagi.m14511();
            if (r0 != BenimaruNikaido.NULL) {
                return r0 == BenimaruNikaido.BOOLEAN ? Boolean.toString(kyoKusanagi.m14518()) : kyoKusanagi.m14513();
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14427(GoroDaimon goroDaimon, String str) throws IOException {
            goroDaimon.m14476(str);
        }
    };

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public static final Whip f11496 = m14336(Locale.class, f11492);

    /* renamed from: ˊ  reason: contains not printable characters */
    public static final Maxima<Number> f11497 = new Maxima<Number>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Number m14405(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            BenimaruNikaido r0 = kyoKusanagi.m14511();
            switch (r0) {
                case NUMBER:
                    return new com.p003if.p004do.p014if.ChoiBounge(kyoKusanagi.m14513());
                case NULL:
                    kyoKusanagi.m14519();
                    return null;
                default:
                    throw new Chris("Expecting number, got: " + r0);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14408(GoroDaimon goroDaimon, Number number) throws IOException {
            goroDaimon.m14484(number);
        }
    };

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public static final Whip f11498 = new Whip() {
        /* JADX WARNING: type inference failed for: r4v0, types: [com.if.do.for.KyoKusanagi<T>, com.if.do.for.KyoKusanagi] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public <T> com.p003if.p004do.Maxima<T> m14379(com.p003if.p004do.ChoiBounge r3, com.p003if.p004do.p013for.KyoKusanagi<T> r4) {
            /*
                r2 = this;
                java.lang.Class r0 = r4.a()
                java.lang.Class<java.lang.Enum> r1 = java.lang.Enum.class
                boolean r1 = r1.isAssignableFrom(r0)
                if (r1 == 0) goto L_0x0010
                java.lang.Class<java.lang.Enum> r1 = java.lang.Enum.class
                if (r0 != r1) goto L_0x0012
            L_0x0010:
                r0 = 0
            L_0x0011:
                return r0
            L_0x0012:
                boolean r1 = r0.isEnum()
                if (r1 != 0) goto L_0x001c
                java.lang.Class r0 = r0.getSuperclass()
            L_0x001c:
                com.if.do.if.do.LeonaHeidern$KyoKusanagi r1 = new com.if.do.if.do.LeonaHeidern$KyoKusanagi
                r1.<init>(r0)
                r0 = r1
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: com.p003if.p004do.p014if.p015do.LeonaHeidern.AnonymousClass19.m14379(com.if.do.ChoiBounge, com.if.do.for.KyoKusanagi):com.if.do.Maxima");
        }
    };

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final Whip f11499 = m14336(Number.class, f11497);

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public static final Whip f11500 = m14333(SaishuKusanagi.class, f11494);

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final Maxima<Character> f11501 = new Maxima<Character>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Character m14421(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            String r0 = kyoKusanagi.m14513();
            if (r0.length() == 1) {
                return Character.valueOf(r0.charAt(0));
            }
            throw new Chris("Expecting character, got: " + r0);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14424(GoroDaimon goroDaimon, Character ch) throws IOException {
            goroDaimon.m14476(ch == null ? null : String.valueOf(ch));
        }
    };

    /* renamed from: ˏ  reason: contains not printable characters */
    public static final Maxima<BigDecimal> f11502 = new Maxima<BigDecimal>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public BigDecimal m14429(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            try {
                return new BigDecimal(kyoKusanagi.m14513());
            } catch (NumberFormatException e) {
                throw new Chris((Throwable) e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14431(GoroDaimon goroDaimon, BigDecimal bigDecimal) throws IOException {
            goroDaimon.m14484((Number) bigDecimal);
        }
    };

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final Whip f11503 = m14337(Byte.TYPE, Byte.class, f11485);

    /* renamed from: י  reason: contains not printable characters */
    public static final Maxima<BigInteger> f11504 = new Maxima<BigInteger>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public BigInteger m14433(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            try {
                return new BigInteger(kyoKusanagi.m14513());
            } catch (NumberFormatException e) {
                throw new Chris((Throwable) e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14435(GoroDaimon goroDaimon, BigInteger bigInteger) throws IOException {
            goroDaimon.m14484((Number) bigInteger);
        }
    };

    /* renamed from: ـ  reason: contains not printable characters */
    public static final Whip f11505 = m14336(String.class, f11495);

    /* renamed from: ــ  reason: contains not printable characters */
    public static final Whip f11506 = m14334(Calendar.class, GregorianCalendar.class, f11488);

    /* renamed from: ٴ  reason: contains not printable characters */
    public static final Maxima<Number> f11507 = new Maxima<Number>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Number m14401(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            try {
                return Short.valueOf((short) kyoKusanagi.m14514());
            } catch (NumberFormatException e) {
                throw new Chris((Throwable) e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14404(GoroDaimon goroDaimon, Number number) throws IOException {
            goroDaimon.m14484(number);
        }
    };

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final Whip f11508 = m14337(Short.TYPE, Short.class, f11507);

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public static final Whip f11509 = m14336(URI.class, f11524);

    /* renamed from: ᴵ  reason: contains not printable characters */
    public static final Maxima<StringBuilder> f11510 = new Maxima<StringBuilder>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public StringBuilder m14437(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return new StringBuilder(kyoKusanagi.m14513());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14439(GoroDaimon goroDaimon, StringBuilder sb) throws IOException {
            goroDaimon.m14476(sb == null ? null : sb.toString());
        }
    };

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public static final Maxima<InetAddress> f11511 = new Maxima<InetAddress>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public InetAddress m14354(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return InetAddress.getByName(kyoKusanagi.m14513());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14356(GoroDaimon goroDaimon, InetAddress inetAddress) throws IOException {
            goroDaimon.m14476(inetAddress == null ? null : inetAddress.getHostAddress());
        }
    };

    /* renamed from: ᵎ  reason: contains not printable characters */
    public static final Whip f11512 = m14336(StringBuilder.class, f11510);

    /* renamed from: ᵔ  reason: contains not printable characters */
    public static final Maxima<StringBuffer> f11513 = new Maxima<StringBuffer>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public StringBuffer m14441(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return new StringBuffer(kyoKusanagi.m14513());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14443(GoroDaimon goroDaimon, StringBuffer stringBuffer) throws IOException {
            goroDaimon.m14476(stringBuffer == null ? null : stringBuffer.toString());
        }
    };

    /* renamed from: ᵢ  reason: contains not printable characters */
    public static final Whip f11514 = m14336(StringBuffer.class, f11513);

    /* renamed from: ⁱ  reason: contains not printable characters */
    public static final Maxima<URL> f11515 = new Maxima<URL>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public URL m14342(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            String r1 = kyoKusanagi.m14513();
            if (!"null".equals(r1)) {
                return new URL(r1);
            }
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14344(GoroDaimon goroDaimon, URL url) throws IOException {
            goroDaimon.m14476(url == null ? null : url.toExternalForm());
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    public static final Maxima<Boolean> f11516 = new Maxima<Boolean>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Boolean m14387(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return kyoKusanagi.m14511() == BenimaruNikaido.STRING ? Boolean.valueOf(Boolean.parseBoolean(kyoKusanagi.m14513())) : Boolean.valueOf(kyoKusanagi.m14518());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14390(GoroDaimon goroDaimon, Boolean bool) throws IOException {
            if (bool == null) {
                goroDaimon.m14470();
            } else {
                goroDaimon.m14486(bool.booleanValue());
            }
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Whip f11517 = m14336(Class.class, f11520);

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Whip f11518 = m14336(BitSet.class, f11519);

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Maxima<BitSet> f11519 = new Maxima<BitSet>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public BitSet m14350(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            boolean z;
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            BitSet bitSet = new BitSet();
            kyoKusanagi.m14525();
            BenimaruNikaido r0 = kyoKusanagi.m14511();
            int i = 0;
            while (r0 != BenimaruNikaido.END_ARRAY) {
                switch (AnonymousClass26.f11539[r0.ordinal()]) {
                    case 1:
                        if (kyoKusanagi.m14514() == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    case 2:
                        z = kyoKusanagi.m14518();
                        break;
                    case 3:
                        String r02 = kyoKusanagi.m14513();
                        try {
                            if (Integer.parseInt(r02) == 0) {
                                z = false;
                                break;
                            } else {
                                z = true;
                                break;
                            }
                        } catch (NumberFormatException e) {
                            throw new Chris("Error: Expecting: bitset number value (1, 0), Found: " + r02);
                        }
                    default:
                        throw new Chris("Invalid bitset value type: " + r0);
                }
                if (z) {
                    bitSet.set(i);
                }
                i++;
                r0 = kyoKusanagi.m14511();
            }
            kyoKusanagi.m14522();
            return bitSet;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14352(GoroDaimon goroDaimon, BitSet bitSet) throws IOException {
            if (bitSet == null) {
                goroDaimon.m14470();
                return;
            }
            goroDaimon.m14475();
            for (int i = 0; i < bitSet.length(); i++) {
                goroDaimon.m14483((long) (bitSet.get(i) ? 1 : 0));
            }
            goroDaimon.m14480();
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Maxima<Class> f11520 = new Maxima<Class>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Class m14338(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14341(GoroDaimon goroDaimon, Class cls) throws IOException {
            if (cls == null) {
                goroDaimon.m14470();
                return;
            }
            throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
        }
    };

    /* renamed from: ﹳ  reason: contains not printable characters */
    public static final Whip f11521 = m14336(URL.class, f11515);

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static final Maxima<Number> f11522 = new Maxima<Number>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Number m14417(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return Float.valueOf((float) kyoKusanagi.m14520());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14420(GoroDaimon goroDaimon, Number number) throws IOException {
            goroDaimon.m14484(number);
        }
    };

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static final Maxima<Number> f11523 = new Maxima<Number>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Number m14380(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return Double.valueOf(kyoKusanagi.m14520());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14383(GoroDaimon goroDaimon, Number number) throws IOException {
            goroDaimon.m14484(number);
        }
    };

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public static final Maxima<URI> f11524 = new Maxima<URI>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public URI m14346(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            try {
                String r1 = kyoKusanagi.m14513();
                if (!"null".equals(r1)) {
                    return new URI(r1);
                }
                return null;
            } catch (URISyntaxException e) {
                throw new com.p003if.p004do.LeonaHeidern((Throwable) e);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14348(GoroDaimon goroDaimon, URI uri) throws IOException {
            goroDaimon.m14476(uri == null ? null : uri.toASCIIString());
        }
    };

    /* renamed from: com.if.do.if.do.LeonaHeidern$KyoKusanagi */
    private static final class KyoKusanagi<T extends Enum<T>> extends Maxima<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Map<T, String> f11540 = new HashMap();

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, T> f11541 = new HashMap();

        public KyoKusanagi(Class<T> cls) {
            try {
                for (Enum enumR : (Enum[]) cls.getEnumTyphoonApp()) {
                    String name = enumR.name();
                    com.p003if.p004do.p005do.GoroDaimon goroDaimon = (com.p003if.p004do.p005do.GoroDaimon) cls.getField(name).getAnnotation(com.p003if.p004do.p005do.GoroDaimon.class);
                    if (goroDaimon != null) {
                        name = goroDaimon.m6138();
                        for (String put : goroDaimon.m6137()) {
                            this.f11541.put(put, enumR);
                        }
                    }
                    String str = name;
                    this.f11541.put(str, enumR);
                    this.f11540.put(enumR, str);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public T m14445(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() != BenimaruNikaido.NULL) {
                return (Enum) this.f11541.get(kyoKusanagi.m14513());
            }
            kyoKusanagi.m14519();
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14448(GoroDaimon goroDaimon, T t) throws IOException {
            goroDaimon.m14476(t == null ? null : this.f11540.get(t));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <TT> Whip m14333(final Class<TT> cls, final Maxima<TT> maxima) {
        return new Whip() {
            public String toString() {
                return "Factory[typeHierarchy=" + cls.getName() + ",adapter=" + maxima + "]";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public <T> Maxima<T> m14392(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
                if (cls.isAssignableFrom(kyoKusanagi.a())) {
                    return maxima;
                }
                return null;
            }
        };
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <TT> Whip m14334(final Class<TT> cls, final Class<? extends TT> cls2, final Maxima<? super TT> maxima) {
        return new Whip() {
            public String toString() {
                return "Factory[type=" + cls.getName() + "+" + cls2.getName() + ",adapter=" + maxima + "]";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public <T> Maxima<T> m14391(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
                Class<? super T> a = kyoKusanagi.a();
                if (a == cls || a == cls2) {
                    return maxima;
                }
                return null;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TT> Whip m14335(final com.p003if.p004do.p013for.KyoKusanagi<TT> kyoKusanagi, final Maxima<TT> maxima) {
        return new Whip() {
            /* renamed from: 龘  reason: contains not printable characters */
            public <T> Maxima<T> m14384(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
                if (kyoKusanagi.equals(kyoKusanagi)) {
                    return maxima;
                }
                return null;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TT> Whip m14336(final Class<TT> cls, final Maxima<TT> maxima) {
        return new Whip() {
            public String toString() {
                return "Factory[type=" + cls.getName() + ",adapter=" + maxima + "]";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public <T> Maxima<T> m14385(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
                if (kyoKusanagi.a() == cls) {
                    return maxima;
                }
                return null;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <TT> Whip m14337(final Class<TT> cls, final Class<TT> cls2, final Maxima<? super TT> maxima) {
        return new Whip() {
            public String toString() {
                return "Factory[type=" + cls2.getName() + "+" + cls.getName() + ",adapter=" + maxima + "]";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public <T> Maxima<T> m14386(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
                Class<? super T> a = kyoKusanagi.a();
                if (a == cls || a == cls2) {
                    return maxima;
                }
                return null;
            }
        };
    }
}
