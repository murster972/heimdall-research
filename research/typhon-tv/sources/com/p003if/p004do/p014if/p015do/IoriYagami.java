package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p013for.KyoKusanagi;
import com.p003if.p004do.p016int.BenimaruNikaido;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* renamed from: com.if.do.if.do.IoriYagami  reason: invalid package */
public final class IoriYagami extends Maxima<Time> {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Whip f11476 = new Whip() {
        /* renamed from: 龘  reason: contains not printable characters */
        public <T> Maxima<T> m14329(ChoiBounge choiBounge, KyoKusanagi<T> kyoKusanagi) {
            if (kyoKusanagi.a() == Time.class) {
                return new IoriYagami();
            }
            return null;
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private final DateFormat f11477 = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Time m14325(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
        Time time;
        if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
            kyoKusanagi.m14519();
            time = null;
        } else {
            try {
                time = new Time(this.f11477.parse(kyoKusanagi.m14513()).getTime());
            } catch (ParseException e) {
                throw new Chris((Throwable) e);
            }
        }
        return time;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m14327(GoroDaimon goroDaimon, Time time) throws IOException {
        goroDaimon.m14476(time == null ? null : this.f11477.format(time));
    }
}
