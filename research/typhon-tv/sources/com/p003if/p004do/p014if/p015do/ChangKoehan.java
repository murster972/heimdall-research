package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.BrianBattler;
import com.p003if.p004do.Mature;
import com.p003if.p004do.SaishuKusanagi;
import com.p003if.p004do.ShingoYabuki;
import com.p003if.p004do.Vice;
import com.p003if.p004do.p016int.BenimaruNikaido;
import com.p003if.p004do.p016int.KyoKusanagi;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.if.do.if.do.ChangKoehan  reason: invalid package */
public final class ChangKoehan extends KyoKusanagi {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f11457 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Reader f11458 = new Reader() {
        public void close() throws IOException {
            throw new AssertionError();
        }

        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }
    };

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<Object> f11459 = new ArrayList();

    public ChangKoehan(SaishuKusanagi saishuKusanagi) {
        super(f11458);
        this.f11459.add(saishuKusanagi);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private Object m14277() {
        return this.f11459.get(this.f11459.size() - 1);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private Object m14278() {
        return this.f11459.remove(this.f11459.size() - 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14279(BenimaruNikaido benimaruNikaido) throws IOException {
        if (m14280() != benimaruNikaido) {
            throw new IllegalStateException("Expected " + benimaruNikaido + " but was " + m14280());
        }
    }

    public void close() throws IOException {
        this.f11459.clear();
        this.f11459.add(f11457);
    }

    public String toString() {
        return getClass().getSimpleName();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public BenimaruNikaido m14280() throws IOException {
        if (this.f11459.isEmpty()) {
            return BenimaruNikaido.END_DOCUMENT;
        }
        Object r0 = m14277();
        if (r0 instanceof Iterator) {
            boolean z = this.f11459.get(this.f11459.size() - 2) instanceof Vice;
            Iterator it2 = (Iterator) r0;
            if (!it2.hasNext()) {
                return z ? BenimaruNikaido.END_OBJECT : BenimaruNikaido.END_ARRAY;
            }
            if (z) {
                return BenimaruNikaido.NAME;
            }
            this.f11459.add(it2.next());
            return m14280();
        } else if (r0 instanceof Vice) {
            return BenimaruNikaido.BEGIN_OBJECT;
        } else {
            if (r0 instanceof BrianBattler) {
                return BenimaruNikaido.BEGIN_ARRAY;
            }
            if (r0 instanceof ShingoYabuki) {
                ShingoYabuki shingoYabuki = (ShingoYabuki) r0;
                if (shingoYabuki.m14155()) {
                    return BenimaruNikaido.STRING;
                }
                if (shingoYabuki.m14161()) {
                    return BenimaruNikaido.BOOLEAN;
                }
                if (shingoYabuki.m14154()) {
                    return BenimaruNikaido.NUMBER;
                }
                throw new AssertionError();
            } else if (r0 instanceof Mature) {
                return BenimaruNikaido.NULL;
            } else {
                if (r0 == f11457) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m14281() throws IOException {
        m14279(BenimaruNikaido.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) m14277()).next();
        this.f11459.add(entry.getValue());
        return (String) entry.getKey();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m14282() throws IOException {
        BenimaruNikaido r0 = m14280();
        if (r0 == BenimaruNikaido.STRING || r0 == BenimaruNikaido.NUMBER) {
            return ((ShingoYabuki) m14278()).m14159();
        }
        throw new IllegalStateException("Expected " + BenimaruNikaido.STRING + " but was " + r0);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m14283() throws IOException {
        BenimaruNikaido r0 = m14280();
        if (r0 == BenimaruNikaido.NUMBER || r0 == BenimaruNikaido.STRING) {
            int r02 = ((ShingoYabuki) m14277()).m14152();
            m14278();
            return r02;
        }
        throw new IllegalStateException("Expected " + BenimaruNikaido.NUMBER + " but was " + r0);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m14284() throws IOException {
        if (m14280() == BenimaruNikaido.NAME) {
            m14281();
        } else {
            m14278();
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public long m14285() throws IOException {
        BenimaruNikaido r0 = m14280();
        if (r0 == BenimaruNikaido.NUMBER || r0 == BenimaruNikaido.STRING) {
            long r02 = ((ShingoYabuki) m14277()).m14156();
            m14278();
            return r02;
        }
        throw new IllegalStateException("Expected " + BenimaruNikaido.NUMBER + " but was " + r0);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m14286() throws IOException {
        m14279(BenimaruNikaido.BOOLEAN);
        return ((ShingoYabuki) m14278()).m14153();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m14287() throws IOException {
        m14279(BenimaruNikaido.NULL);
        m14278();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public double m14288() throws IOException {
        BenimaruNikaido r0 = m14280();
        if (r0 == BenimaruNikaido.NUMBER || r0 == BenimaruNikaido.STRING) {
            double r02 = ((ShingoYabuki) m14277()).m14158();
            if (m14527() || (!Double.isNaN(r02) && !Double.isInfinite(r02))) {
                m14278();
                return r02;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + r02);
        }
        throw new IllegalStateException("Expected " + BenimaruNikaido.NUMBER + " but was " + r0);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m14289() throws IOException {
        BenimaruNikaido r0 = m14280();
        return (r0 == BenimaruNikaido.END_OBJECT || r0 == BenimaruNikaido.END_ARRAY) ? false : true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14290() throws IOException {
        m14279(BenimaruNikaido.END_ARRAY);
        m14278();
        m14278();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m14291() throws IOException {
        m14279(BenimaruNikaido.END_OBJECT);
        m14278();
        m14278();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m14292() throws IOException {
        m14279(BenimaruNikaido.BEGIN_OBJECT);
        this.f11459.add(((Vice) m14277()).m14169().iterator());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14293() throws IOException {
        m14279(BenimaruNikaido.BEGIN_ARRAY);
        this.f11459.add(((BrianBattler) m14277()).iterator());
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public void m14294() throws IOException {
        m14279(BenimaruNikaido.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) m14277()).next();
        this.f11459.add(entry.getValue());
        this.f11459.add(new ShingoYabuki((String) entry.getKey()));
    }
}
