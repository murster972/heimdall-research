package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p005do.BenimaruNikaido;
import com.p003if.p004do.p013for.KyoKusanagi;
import com.p003if.p004do.p014if.GoroDaimon;

/* renamed from: com.if.do.if.do.ChinGentsai  reason: invalid package */
public final class ChinGentsai implements Whip {

    /* renamed from: 龘  reason: contains not printable characters */
    private final GoroDaimon f11460;

    public ChinGentsai(GoroDaimon goroDaimon) {
        this.f11460 = goroDaimon;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Maxima<?> m14295(GoroDaimon goroDaimon, ChoiBounge choiBounge, KyoKusanagi<?> kyoKusanagi, BenimaruNikaido benimaruNikaido) {
        Class<?> r0 = benimaruNikaido.m6134();
        if (Maxima.class.isAssignableFrom(r0)) {
            return (Maxima) goroDaimon.m14215(KyoKusanagi.b(r0)).m14251();
        }
        if (Whip.class.isAssignableFrom(r0)) {
            return ((Whip) goroDaimon.m14215(KyoKusanagi.b(r0)).m14251()).m14172(choiBounge, kyoKusanagi);
        }
        throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14296(ChoiBounge choiBounge, KyoKusanagi<T> kyoKusanagi) {
        BenimaruNikaido benimaruNikaido = (BenimaruNikaido) kyoKusanagi.a().getAnnotation(BenimaruNikaido.class);
        if (benimaruNikaido == null) {
            return null;
        }
        return m14295(this.f11460, choiBounge, kyoKusanagi, benimaruNikaido);
    }
}
