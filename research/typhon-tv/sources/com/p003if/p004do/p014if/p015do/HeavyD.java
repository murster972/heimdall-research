package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.SaishuKusanagi;
import com.p003if.p004do.ShingoYabuki;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p014if.ChangKoehan;
import com.p003if.p004do.p014if.GoroDaimon;
import com.p003if.p004do.p014if.LuckyGlauber;
import com.p003if.p004do.p014if.RugalBernstein;
import com.p003if.p004do.p016int.BenimaruNikaido;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: com.if.do.if.do.HeavyD  reason: invalid package */
public final class HeavyD implements Whip {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean f11470;

    /* renamed from: 龘  reason: contains not printable characters */
    private final GoroDaimon f11471;

    /* renamed from: com.if.do.if.do.HeavyD$KyoKusanagi */
    private final class KyoKusanagi<K, V> extends Maxima<Map<K, V>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Maxima<K> f11472;

        /* renamed from: 麤  reason: contains not printable characters */
        private final LuckyGlauber<? extends Map<K, V>> f11473;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Maxima<V> f11474;

        public KyoKusanagi(ChoiBounge choiBounge, Type type, Maxima<K> maxima, Type type2, Maxima<V> maxima2, LuckyGlauber<? extends Map<K, V>> luckyGlauber) {
            this.f11472 = new SaishuKusanagi(choiBounge, maxima, type);
            this.f11474 = new SaishuKusanagi(choiBounge, maxima2, type2);
            this.f11473 = luckyGlauber;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m14320(SaishuKusanagi saishuKusanagi) {
            if (saishuKusanagi.m14141()) {
                ShingoYabuki r0 = saishuKusanagi.m14138();
                if (r0.m14154()) {
                    return String.valueOf(r0.m14157());
                }
                if (r0.m14161()) {
                    return Boolean.toString(r0.m14153());
                }
                if (r0.m14155()) {
                    return r0.m14159();
                }
                throw new AssertionError();
            } else if (saishuKusanagi.m14142()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Map<K, V> m14321(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            BenimaruNikaido r1 = kyoKusanagi.m14511();
            if (r1 == BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            Map<K, V> map = (Map) this.f11473.m14251();
            if (r1 == BenimaruNikaido.BEGIN_ARRAY) {
                kyoKusanagi.m14525();
                while (kyoKusanagi.m14521()) {
                    kyoKusanagi.m14525();
                    K r12 = this.f11472.m14130(kyoKusanagi);
                    if (map.put(r12, this.f11474.m14130(kyoKusanagi)) != null) {
                        throw new Chris("duplicate key: " + r12);
                    }
                    kyoKusanagi.m14522();
                }
                kyoKusanagi.m14522();
                return map;
            }
            kyoKusanagi.m14524();
            while (kyoKusanagi.m14521()) {
                ChangKoehan.f11378.m14198(kyoKusanagi);
                K r13 = this.f11472.m14130(kyoKusanagi);
                if (map.put(r13, this.f11474.m14130(kyoKusanagi)) != null) {
                    throw new Chris("duplicate key: " + r13);
                }
            }
            kyoKusanagi.m14523();
            return map;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14323(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Map<K, V> map) throws IOException {
            int i = 0;
            if (map == null) {
                goroDaimon.m14470();
            } else if (!HeavyD.this.f11470) {
                goroDaimon.m14478();
                for (Map.Entry next : map.entrySet()) {
                    goroDaimon.m14485(String.valueOf(next.getKey()));
                    this.f11474.m14132(goroDaimon, next.getValue());
                }
                goroDaimon.m14474();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                boolean z = false;
                for (Map.Entry next2 : map.entrySet()) {
                    SaishuKusanagi r6 = this.f11472.m14131(next2.getKey());
                    arrayList.add(r6);
                    arrayList2.add(next2.getValue());
                    z = (r6.m14136() || r6.m14140()) | z;
                }
                if (z) {
                    goroDaimon.m14475();
                    while (i < arrayList.size()) {
                        goroDaimon.m14475();
                        RugalBernstein.m14254((SaishuKusanagi) arrayList.get(i), goroDaimon);
                        this.f11474.m14132(goroDaimon, arrayList2.get(i));
                        goroDaimon.m14480();
                        i++;
                    }
                    goroDaimon.m14480();
                    return;
                }
                goroDaimon.m14478();
                while (i < arrayList.size()) {
                    goroDaimon.m14485(m14320((SaishuKusanagi) arrayList.get(i)));
                    this.f11474.m14132(goroDaimon, arrayList2.get(i));
                    i++;
                }
                goroDaimon.m14474();
            }
        }
    }

    public HeavyD(GoroDaimon goroDaimon, boolean z) {
        this.f11471 = goroDaimon;
        this.f11470 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Maxima<?> m14317(ChoiBounge choiBounge, Type type) {
        return (type == Boolean.TYPE || type == Boolean.class) ? LeonaHeidern.f11481 : choiBounge.m14081(com.p003if.p004do.p013for.KyoKusanagi.a(type));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14319(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
        Type b = kyoKusanagi.b();
        if (!Map.class.isAssignableFrom(kyoKusanagi.a())) {
            return null;
        }
        Type[] r1 = com.p003if.p004do.p014if.BenimaruNikaido.m14181(b, com.p003if.p004do.p014if.BenimaruNikaido.m14177(b));
        Maxima<?> r4 = m14317(choiBounge, r1[0]);
        Maxima<?> r6 = choiBounge.m14081(com.p003if.p004do.p013for.KyoKusanagi.a(r1[1]));
        LuckyGlauber<T> r7 = this.f11471.m14215(kyoKusanagi);
        return new KyoKusanagi(choiBounge, r1[0], r4, r1[1], r6, r7);
    }
}
