package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.BrianBattler;
import com.p003if.p004do.Mature;
import com.p003if.p004do.SaishuKusanagi;
import com.p003if.p004do.ShingoYabuki;
import com.p003if.p004do.Vice;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.if.do.if.do.ChoiBounge  reason: invalid package */
public final class ChoiBounge extends GoroDaimon {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ShingoYabuki f11461 = new ShingoYabuki("closed");

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Writer f11462 = new Writer() {
        public void close() throws IOException {
            throw new AssertionError();
        }

        public void flush() throws IOException {
            throw new AssertionError();
        }

        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private SaishuKusanagi f11463 = Mature.f5653;

    /* renamed from: 麤  reason: contains not printable characters */
    private String f11464;

    /* renamed from: 齉  reason: contains not printable characters */
    private final List<SaishuKusanagi> f11465 = new ArrayList();

    public ChoiBounge() {
        super(f11462);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private SaishuKusanagi m14297() {
        return this.f11465.get(this.f11465.size() - 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14298(SaishuKusanagi saishuKusanagi) {
        if (this.f11464 != null) {
            if (!saishuKusanagi.m14142() || m14473()) {
                ((Vice) m14297()).m14170(this.f11464, saishuKusanagi);
            }
            this.f11464 = null;
        } else if (this.f11465.isEmpty()) {
            this.f11463 = saishuKusanagi;
        } else {
            SaishuKusanagi r0 = m14297();
            if (r0 instanceof BrianBattler) {
                ((BrianBattler) r0).m14060(saishuKusanagi);
                return;
            }
            throw new IllegalStateException();
        }
    }

    public void close() throws IOException {
        if (!this.f11465.isEmpty()) {
            throw new IOException("Incomplete document");
        }
        this.f11465.add(f11461);
    }

    public void flush() throws IOException {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public GoroDaimon m14299() throws IOException {
        m14298((SaishuKusanagi) Mature.f5653);
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public GoroDaimon m14300() throws IOException {
        if (this.f11465.isEmpty() || this.f11464 != null) {
            throw new IllegalStateException();
        } else if (m14297() instanceof Vice) {
            this.f11465.remove(this.f11465.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m14301() throws IOException {
        BrianBattler brianBattler = new BrianBattler();
        m14298((SaishuKusanagi) brianBattler);
        this.f11465.add(brianBattler);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m14302(String str) throws IOException {
        if (str == null) {
            return m14299();
        }
        m14298((SaishuKusanagi) new ShingoYabuki(str));
        return this;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public GoroDaimon m14303() throws IOException {
        Vice vice = new Vice();
        m14298((SaishuKusanagi) vice);
        this.f11465.add(vice);
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public GoroDaimon m14304() throws IOException {
        if (this.f11465.isEmpty() || this.f11464 != null) {
            throw new IllegalStateException();
        } else if (m14297() instanceof BrianBattler) {
            this.f11465.remove(this.f11465.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14305() {
        if (this.f11465.isEmpty()) {
            return this.f11463;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.f11465);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14306(long j) throws IOException {
        m14298((SaishuKusanagi) new ShingoYabuki((Number) Long.valueOf(j)));
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14307(Number number) throws IOException {
        if (number == null) {
            return m14299();
        }
        if (!m14471()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        m14298((SaishuKusanagi) new ShingoYabuki(number));
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14308(String str) throws IOException {
        if (this.f11465.isEmpty() || this.f11464 != null) {
            throw new IllegalStateException();
        } else if (m14297() instanceof Vice) {
            this.f11464 = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14309(boolean z) throws IOException {
        m14298((SaishuKusanagi) new ShingoYabuki(Boolean.valueOf(z)));
        return this;
    }
}
