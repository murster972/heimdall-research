package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.p014if.p015do.BrianBattler;
import com.p003if.p004do.p016int.GoroDaimon;
import com.p003if.p004do.p016int.KyoKusanagi;
import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/* renamed from: com.if.do.if.do.SaishuKusanagi  reason: invalid package */
final class SaishuKusanagi<T> extends Maxima<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Maxima<T> f11547;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Type f11548;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ChoiBounge f11549;

    SaishuKusanagi(ChoiBounge choiBounge, Maxima<T> maxima, Type type) {
        this.f11549 = choiBounge;
        this.f11547 = maxima;
        this.f11548 = type;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Type m14457(Type type, Object obj) {
        return obj != null ? (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type : type;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public T m14458(KyoKusanagi kyoKusanagi) throws IOException {
        return this.f11547.m14130(kyoKusanagi);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14459(GoroDaimon goroDaimon, T t) throws IOException {
        Maxima<T> maxima = this.f11547;
        Type r1 = m14457(this.f11548, (Object) t);
        if (r1 != this.f11548) {
            maxima = this.f11549.m14081(com.p003if.p004do.p013for.KyoKusanagi.a(r1));
            if ((maxima instanceof BrianBattler.KyoKusanagi) && !(this.f11547 instanceof BrianBattler.KyoKusanagi)) {
                maxima = this.f11547;
            }
        }
        maxima.m14132(goroDaimon, t);
    }
}
