package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChangKoehan;
import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p014if.ChinGentsai;
import com.p003if.p004do.p014if.GoroDaimon;
import com.p003if.p004do.p014if.LuckyGlauber;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/* renamed from: com.if.do.if.do.BrianBattler  reason: invalid package */
public final class BrianBattler implements Whip {

    /* renamed from: 靐  reason: contains not printable characters */
    private final ChangKoehan f11443;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ChinGentsai f11444;

    /* renamed from: 龘  reason: contains not printable characters */
    private final GoroDaimon f11445;

    /* renamed from: com.if.do.if.do.BrianBattler$BenimaruNikaido */
    static abstract class BenimaruNikaido {

        /* renamed from: ʼ  reason: contains not printable characters */
        final String f11452;

        /* renamed from: ʽ  reason: contains not printable characters */
        final boolean f11453;

        /* renamed from: ˑ  reason: contains not printable characters */
        final boolean f11454;

        protected BenimaruNikaido(String str, boolean z, boolean z2) {
            this.f11452 = str;
            this.f11453 = z;
            this.f11454 = z2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m14272(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m14273(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract boolean m14274(Object obj) throws IOException, IllegalAccessException;
    }

    /* renamed from: com.if.do.if.do.BrianBattler$KyoKusanagi */
    public static final class KyoKusanagi<T> extends Maxima<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Map<String, BenimaruNikaido> f11455;

        /* renamed from: 龘  reason: contains not printable characters */
        private final LuckyGlauber<T> f11456;

        private KyoKusanagi(LuckyGlauber<T> luckyGlauber, Map<String, BenimaruNikaido> map) {
            this.f11456 = luckyGlauber;
            this.f11455 = map;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public T m14275(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == com.p003if.p004do.p016int.BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            T r1 = this.f11456.m14251();
            try {
                kyoKusanagi.m14524();
                while (kyoKusanagi.m14521()) {
                    BenimaruNikaido benimaruNikaido = this.f11455.get(kyoKusanagi.m14512());
                    if (benimaruNikaido == null || !benimaruNikaido.f11454) {
                        kyoKusanagi.m14515();
                    } else {
                        benimaruNikaido.m14273(kyoKusanagi, (Object) r1);
                    }
                }
                kyoKusanagi.m14523();
                return r1;
            } catch (IllegalStateException e) {
                throw new Chris((Throwable) e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14276(com.p003if.p004do.p016int.GoroDaimon goroDaimon, T t) throws IOException {
            if (t == null) {
                goroDaimon.m14470();
                return;
            }
            goroDaimon.m14478();
            try {
                for (BenimaruNikaido next : this.f11455.values()) {
                    if (next.m14274(t)) {
                        goroDaimon.m14485(next.f11452);
                        next.m14272(goroDaimon, (Object) t);
                    }
                }
                goroDaimon.m14474();
            } catch (IllegalAccessException e) {
                throw new AssertionError();
            }
        }
    }

    public BrianBattler(GoroDaimon goroDaimon, ChangKoehan changKoehan, ChinGentsai chinGentsai) {
        this.f11445 = goroDaimon;
        this.f11443 = changKoehan;
        this.f11444 = chinGentsai;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000a, code lost:
        r0 = com.p003if.p004do.p014if.p015do.ChinGentsai.m14295(r2.f11445, r3, r5, r0);
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.p003if.p004do.Maxima<?> m14260(com.p003if.p004do.ChoiBounge r3, java.lang.reflect.Field r4, com.p003if.p004do.p013for.KyoKusanagi<?> r5) {
        /*
            r2 = this;
            java.lang.Class<com.if.do.do.BenimaruNikaido> r0 = com.p003if.p004do.p005do.BenimaruNikaido.class
            java.lang.annotation.Annotation r0 = r4.getAnnotation(r0)
            com.if.do.do.BenimaruNikaido r0 = (com.p003if.p004do.p005do.BenimaruNikaido) r0
            if (r0 == 0) goto L_0x0013
            com.if.do.if.GoroDaimon r1 = r2.f11445
            com.if.do.Maxima r0 = com.p003if.p004do.p014if.p015do.ChinGentsai.m14295(r1, r3, r5, r0)
            if (r0 == 0) goto L_0x0013
        L_0x0012:
            return r0
        L_0x0013:
            com.if.do.Maxima r0 = r3.m14081(r5)
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.p003if.p004do.p014if.p015do.BrianBattler.m14260(com.if.do.ChoiBounge, java.lang.reflect.Field, com.if.do.for.KyoKusanagi):com.if.do.Maxima");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private BenimaruNikaido m14262(ChoiBounge choiBounge, Field field, String str, com.p003if.p004do.p013for.KyoKusanagi<?> kyoKusanagi, boolean z, boolean z2) {
        final boolean r8 = com.p003if.p004do.p014if.BrianBattler.m14197((Type) kyoKusanagi.a());
        final ChoiBounge choiBounge2 = choiBounge;
        final Field field2 = field;
        final com.p003if.p004do.p013for.KyoKusanagi<?> kyoKusanagi2 = kyoKusanagi;
        return new BenimaruNikaido(str, z, z2) {

            /* renamed from: 龘  reason: contains not printable characters */
            final Maxima<?> f11451 = BrianBattler.this.m14260(choiBounge2, field2, (com.p003if.p004do.p013for.KyoKusanagi<?>) kyoKusanagi2);

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m14269(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Object obj) throws IOException, IllegalAccessException {
                new SaishuKusanagi(choiBounge2, this.f11451, kyoKusanagi2.b()).m14132(goroDaimon, field2.get(obj));
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m14270(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi, Object obj) throws IOException, IllegalAccessException {
                Object r0 = this.f11451.m14130(kyoKusanagi);
                if (r0 != null || !r8) {
                    field2.set(obj, r0);
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m14271(Object obj) throws IOException, IllegalAccessException {
                return this.f11453 && field2.get(obj) != obj;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static List<String> m14263(ChangKoehan changKoehan, Field field) {
        com.p003if.p004do.p005do.GoroDaimon goroDaimon = (com.p003if.p004do.p005do.GoroDaimon) field.getAnnotation(com.p003if.p004do.p005do.GoroDaimon.class);
        LinkedList linkedList = new LinkedList();
        if (goroDaimon == null) {
            linkedList.add(changKoehan.m14061(field));
        } else {
            linkedList.add(goroDaimon.m6138());
            for (String add : goroDaimon.m6137()) {
                linkedList.add(add);
            }
        }
        return linkedList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<String> m14264(Field field) {
        return m14263(this.f11443, field);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.Class<java.lang.Object>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v2, resolved type: com.if.do.for.KyoKusanagi<?>} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.Class<?>, code=java.lang.Class, for r20v0, types: [java.lang.Class<?>, java.lang.Class] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, com.p003if.p004do.p014if.p015do.BrianBattler.BenimaruNikaido> m14265(com.p003if.p004do.ChoiBounge r18, com.p003if.p004do.p013for.KyoKusanagi<?> r19, java.lang.Class r20) {
        /*
            r17 = this;
            java.util.LinkedHashMap r10 = new java.util.LinkedHashMap
            r10.<init>()
            boolean r1 = r20.isInterface()
            if (r1 == 0) goto L_0x000d
            r1 = r10
        L_0x000c:
            return r1
        L_0x000d:
            java.lang.reflect.Type r12 = r19.b()
        L_0x0011:
            java.lang.Class<java.lang.Object> r1 = java.lang.Object.class
            r0 = r20
            if (r0 == r1) goto L_0x00b7
            java.lang.reflect.Field[] r13 = r20.getDeclaredFields()
            int r14 = r13.length
            r1 = 0
            r11 = r1
        L_0x001e:
            if (r11 >= r14) goto L_0x009f
            r3 = r13[r11]
            r1 = 1
            r0 = r17
            boolean r6 = r0.m14268((java.lang.reflect.Field) r3, (boolean) r1)
            r1 = 0
            r0 = r17
            boolean r7 = r0.m14268((java.lang.reflect.Field) r3, (boolean) r1)
            if (r6 != 0) goto L_0x0038
            if (r7 != 0) goto L_0x0038
        L_0x0034:
            int r1 = r11 + 1
            r11 = r1
            goto L_0x001e
        L_0x0038:
            r1 = 1
            r3.setAccessible(r1)
            java.lang.reflect.Type r1 = r19.b()
            java.lang.reflect.Type r2 = r3.getGenericType()
            r0 = r20
            java.lang.reflect.Type r15 = com.p003if.p004do.p014if.BenimaruNikaido.m14191((java.lang.reflect.Type) r1, (java.lang.Class<?>) r0, (java.lang.reflect.Type) r2)
            r0 = r17
            java.util.List r16 = r0.m14264(r3)
            r8 = 0
            r1 = 0
            r9 = r1
        L_0x0053:
            int r1 = r16.size()
            if (r9 >= r1) goto L_0x007d
            r0 = r16
            java.lang.Object r4 = r0.get(r9)
            java.lang.String r4 = (java.lang.String) r4
            if (r9 == 0) goto L_0x0064
            r6 = 0
        L_0x0064:
            com.if.do.for.KyoKusanagi r5 = com.p003if.p004do.p013for.KyoKusanagi.a((java.lang.reflect.Type) r15)
            r1 = r17
            r2 = r18
            com.if.do.if.do.BrianBattler$BenimaruNikaido r1 = r1.m14262(r2, r3, r4, r5, r6, r7)
            java.lang.Object r1 = r10.put(r4, r1)
            com.if.do.if.do.BrianBattler$BenimaruNikaido r1 = (com.p003if.p004do.p014if.p015do.BrianBattler.BenimaruNikaido) r1
            if (r8 != 0) goto L_0x00ba
        L_0x0078:
            int r2 = r9 + 1
            r9 = r2
            r8 = r1
            goto L_0x0053
        L_0x007d:
            if (r8 == 0) goto L_0x0034
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r12)
            java.lang.String r3 = " declares multiple JSON fields named "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r8.f11452
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x009f:
            java.lang.reflect.Type r1 = r19.b()
            java.lang.reflect.Type r2 = r20.getGenericSuperclass()
            r0 = r20
            java.lang.reflect.Type r1 = com.p003if.p004do.p014if.BenimaruNikaido.m14191((java.lang.reflect.Type) r1, (java.lang.Class<?>) r0, (java.lang.reflect.Type) r2)
            com.if.do.for.KyoKusanagi r19 = com.p003if.p004do.p013for.KyoKusanagi.a((java.lang.reflect.Type) r1)
            java.lang.Class r20 = r19.a()
            goto L_0x0011
        L_0x00b7:
            r1 = r10
            goto L_0x000c
        L_0x00ba:
            r1 = r8
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.p003if.p004do.p014if.p015do.BrianBattler.m14265(com.if.do.ChoiBounge, com.if.do.for.KyoKusanagi, java.lang.Class):java.util.Map");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m14266(Field field, boolean z, ChinGentsai chinGentsai) {
        return !chinGentsai.m14207(field.getType(), z) && !chinGentsai.m14208(field, z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14267(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
        Class<? super T> a = kyoKusanagi.a();
        if (!Object.class.isAssignableFrom(a)) {
            return null;
        }
        return new KyoKusanagi(this.f11445.m14215(kyoKusanagi), m14265(choiBounge, (com.p003if.p004do.p013for.KyoKusanagi<?>) kyoKusanagi, (Class<?>) a));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14268(Field field, boolean z) {
        return m14266(field, z, this.f11444);
    }
}
