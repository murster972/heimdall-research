package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Chris;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p013for.KyoKusanagi;
import com.p003if.p004do.p016int.BenimaruNikaido;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* renamed from: com.if.do.if.do.RugalBernstein  reason: invalid package */
public final class RugalBernstein extends Maxima<Date> {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Whip f11545 = new Whip() {
        /* renamed from: 龘  reason: contains not printable characters */
        public <T> Maxima<T> m14456(ChoiBounge choiBounge, KyoKusanagi<T> kyoKusanagi) {
            if (kyoKusanagi.a() == Date.class) {
                return new RugalBernstein();
            }
            return null;
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private final DateFormat f11546 = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Date m14452(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
        Date date;
        if (kyoKusanagi.m14511() == BenimaruNikaido.NULL) {
            kyoKusanagi.m14519();
            date = null;
        } else {
            try {
                date = new Date(this.f11546.parse(kyoKusanagi.m14513()).getTime());
            } catch (ParseException e) {
                throw new Chris((Throwable) e);
            }
        }
        return date;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m14454(GoroDaimon goroDaimon, Date date) throws IOException {
        goroDaimon.m14476(date == null ? null : this.f11546.format(date));
    }
}
