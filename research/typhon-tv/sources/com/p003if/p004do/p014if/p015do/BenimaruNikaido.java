package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p014if.GoroDaimon;
import com.p003if.p004do.p014if.LuckyGlauber;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

/* renamed from: com.if.do.if.do.BenimaruNikaido  reason: invalid package */
public final class BenimaruNikaido implements Whip {

    /* renamed from: 龘  reason: contains not printable characters */
    private final GoroDaimon f11440;

    /* renamed from: com.if.do.if.do.BenimaruNikaido$KyoKusanagi */
    private static final class KyoKusanagi<E> extends Maxima<Collection<E>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final LuckyGlauber<? extends Collection<E>> f11441;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Maxima<E> f11442;

        public KyoKusanagi(ChoiBounge choiBounge, Type type, Maxima<E> maxima, LuckyGlauber<? extends Collection<E>> luckyGlauber) {
            this.f11442 = new SaishuKusanagi(choiBounge, maxima, type);
            this.f11441 = luckyGlauber;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Collection<E> m14256(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (kyoKusanagi.m14511() == com.p003if.p004do.p016int.BenimaruNikaido.NULL) {
                kyoKusanagi.m14519();
                return null;
            }
            Collection<E> collection = (Collection) this.f11441.m14251();
            kyoKusanagi.m14525();
            while (kyoKusanagi.m14521()) {
                collection.add(this.f11442.m14130(kyoKusanagi));
            }
            kyoKusanagi.m14522();
            return collection;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14258(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Collection<E> collection) throws IOException {
            if (collection == null) {
                goroDaimon.m14470();
                return;
            }
            goroDaimon.m14475();
            for (E r1 : collection) {
                this.f11442.m14132(goroDaimon, r1);
            }
            goroDaimon.m14480();
        }
    }

    public BenimaruNikaido(GoroDaimon goroDaimon) {
        this.f11440 = goroDaimon;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14255(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
        Type b = kyoKusanagi.b();
        Class<? super T> a = kyoKusanagi.a();
        if (!Collection.class.isAssignableFrom(a)) {
            return null;
        }
        Type r1 = com.p003if.p004do.p014if.BenimaruNikaido.m14189(b, (Class<?>) a);
        return new KyoKusanagi(choiBounge, r1, choiBounge.m14081(com.p003if.p004do.p013for.KyoKusanagi.a(r1)), this.f11440.m14215(kyoKusanagi));
    }
}
