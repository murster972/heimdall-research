package com.p003if.p004do.p014if.p015do;

import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p014if.BenimaruNikaido;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;

/* renamed from: com.if.do.if.do.KyoKusanagi  reason: invalid package */
public final class KyoKusanagi<E> extends Maxima<Object> {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Whip f11478 = new Whip() {
        /* renamed from: 龘  reason: contains not printable characters */
        public <T> Maxima<T> m14332(ChoiBounge choiBounge, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
            Type b = kyoKusanagi.b();
            if (!(b instanceof GenericArrayType) && (!(b instanceof Class) || !((Class) b).isArray())) {
                return null;
            }
            Type r1 = BenimaruNikaido.m14174(b);
            return new KyoKusanagi(choiBounge, choiBounge.m14081(com.p003if.p004do.p013for.KyoKusanagi.a(r1)), BenimaruNikaido.m14177(r1));
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private final Class<E> f11479;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Maxima<E> f11480;

    public KyoKusanagi(ChoiBounge choiBounge, Maxima<E> maxima, Class<E> cls) {
        this.f11480 = new SaishuKusanagi(choiBounge, maxima, cls);
        this.f11479 = cls;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Object m14330(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
        if (kyoKusanagi.m14511() == com.p003if.p004do.p016int.BenimaruNikaido.NULL) {
            kyoKusanagi.m14519();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        kyoKusanagi.m14525();
        while (kyoKusanagi.m14521()) {
            arrayList.add(this.f11480.m14130(kyoKusanagi));
        }
        kyoKusanagi.m14522();
        Object newInstance = Array.newInstance(this.f11479, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14331(GoroDaimon goroDaimon, Object obj) throws IOException {
        if (obj == null) {
            goroDaimon.m14470();
            return;
        }
        goroDaimon.m14475();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.f11480.m14132(goroDaimon, Array.get(obj, i));
        }
        goroDaimon.m14480();
    }
}
