package com.p003if.p004do.p014if;

import com.p003if.p004do.BenimaruNikaido;
import com.p003if.p004do.ChoiBounge;
import com.p003if.p004do.Maxima;
import com.p003if.p004do.Whip;
import com.p003if.p004do.p005do.ChangKoehan;
import com.p003if.p004do.p013for.KyoKusanagi;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

/* renamed from: com.if.do.if.ChinGentsai  reason: invalid package */
public final class ChinGentsai implements Whip, Cloneable {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ChinGentsai f11379 = new ChinGentsai();

    /* renamed from: ʻ  reason: contains not printable characters */
    private List<BenimaruNikaido> f11380 = Collections.emptyList();

    /* renamed from: ʼ  reason: contains not printable characters */
    private List<BenimaruNikaido> f11381 = Collections.emptyList();

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f11382;

    /* renamed from: 靐  reason: contains not printable characters */
    private double f11383 = -1.0d;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f11384 = true;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f11385 = 136;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m14199(Class<?> cls) {
        return cls.isMemberClass() && !m14200(cls);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m14200(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m14201(ChangKoehan changKoehan) {
        return changKoehan == null || changKoehan.m6135() > this.f11383;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m14202(com.p003if.p004do.p005do.ChinGentsai chinGentsai) {
        return chinGentsai == null || chinGentsai.m6136() <= this.f11383;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m14203(com.p003if.p004do.p005do.ChinGentsai chinGentsai, ChangKoehan changKoehan) {
        return m14202(chinGentsai) && m14201(changKoehan);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m14204(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14205(ChoiBounge choiBounge, KyoKusanagi<T> kyoKusanagi) {
        Class<? super T> a = kyoKusanagi.a();
        final boolean r3 = m14207((Class<?>) a, true);
        final boolean r2 = m14207((Class<?>) a, false);
        if (!r3 && !r2) {
            return null;
        }
        final ChoiBounge choiBounge2 = choiBounge;
        final KyoKusanagi<T> kyoKusanagi2 = kyoKusanagi;
        return new Maxima<T>() {

            /* renamed from: ʻ  reason: contains not printable characters */
            private Maxima<T> f11386;

            /* renamed from: 龘  reason: contains not printable characters */
            private Maxima<T> m14209() {
                Maxima<T> maxima = this.f11386;
                if (maxima != null) {
                    return maxima;
                }
                Maxima<T> r0 = choiBounge2.m14080((Whip) ChinGentsai.this, kyoKusanagi2);
                this.f11386 = r0;
                return r0;
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public T m14210(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
                if (!r2) {
                    return m14209().m14130(kyoKusanagi);
                }
                kyoKusanagi.m14515();
                return null;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m14211(GoroDaimon goroDaimon, T t) throws IOException {
                if (r3) {
                    goroDaimon.m14470();
                } else {
                    m14209().m14132(goroDaimon, t);
                }
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public ChinGentsai clone() {
        try {
            return (ChinGentsai) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14207(Class<?> cls, boolean z) {
        if (this.f11383 != -1.0d && !m14203((com.p003if.p004do.p005do.ChinGentsai) cls.getAnnotation(com.p003if.p004do.p005do.ChinGentsai.class), (ChangKoehan) cls.getAnnotation(ChangKoehan.class))) {
            return true;
        }
        if (!this.f11384 && m14199(cls)) {
            return true;
        }
        if (m14204(cls)) {
            return true;
        }
        for (BenimaruNikaido r0 : z ? this.f11380 : this.f11381) {
            if (r0.m14051(cls)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m14208(Field field, boolean z) {
        com.p003if.p004do.p005do.KyoKusanagi kyoKusanagi;
        if ((this.f11385 & field.getModifiers()) != 0) {
            return true;
        }
        if (this.f11383 != -1.0d && !m14203((com.p003if.p004do.p005do.ChinGentsai) field.getAnnotation(com.p003if.p004do.p005do.ChinGentsai.class), (ChangKoehan) field.getAnnotation(ChangKoehan.class))) {
            return true;
        }
        if (field.isSynthetic()) {
            return true;
        }
        if (this.f11382 && ((kyoKusanagi = (com.p003if.p004do.p005do.KyoKusanagi) field.getAnnotation(com.p003if.p004do.p005do.KyoKusanagi.class)) == null || (!z ? !kyoKusanagi.m6139() : !kyoKusanagi.m6140()))) {
            return true;
        }
        if (!this.f11384 && m14199(field.getType())) {
            return true;
        }
        if (m14204(field.getType())) {
            return true;
        }
        List<BenimaruNikaido> list = z ? this.f11380 : this.f11381;
        if (!list.isEmpty()) {
            com.p003if.p004do.GoroDaimon goroDaimon = new com.p003if.p004do.GoroDaimon(field);
            for (BenimaruNikaido r0 : list) {
                if (r0.m14050(goroDaimon)) {
                    return true;
                }
            }
        }
        return false;
    }
}
