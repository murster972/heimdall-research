package com.p003if.p004do;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/* renamed from: com.if.do.KyoKusanagi  reason: invalid package */
final class KyoKusanagi implements IoriYagami<Date>, Shermie<Date> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final DateFormat f11368;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DateFormat f11369;

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateFormat f11370;

    KyoKusanagi() {
        this(DateFormat.getDateTimeInstance(2, 2, Locale.US), DateFormat.getDateTimeInstance(2, 2));
    }

    public KyoKusanagi(int i, int i2) {
        this(DateFormat.getDateTimeInstance(i, i2, Locale.US), DateFormat.getDateTimeInstance(i, i2));
    }

    KyoKusanagi(String str) {
        this((DateFormat) new SimpleDateFormat(str, Locale.US), (DateFormat) new SimpleDateFormat(str));
    }

    KyoKusanagi(DateFormat dateFormat, DateFormat dateFormat2) {
        this.f11370 = dateFormat;
        this.f11368 = dateFormat2;
        this.f11369 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        this.f11369.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Date m14125(SaishuKusanagi saishuKusanagi) {
        Date parse;
        synchronized (this.f11368) {
            try {
                parse = this.f11368.parse(saishuKusanagi.m14146());
            } catch (ParseException e) {
                throw new Chris(saishuKusanagi.m14146(), e);
            } catch (ParseException e2) {
                try {
                    parse = this.f11370.parse(saishuKusanagi.m14146());
                } catch (ParseException e3) {
                    parse = this.f11369.parse(saishuKusanagi.m14146());
                }
            }
        }
        return parse;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(KyoKusanagi.class.getSimpleName());
        sb.append('(').append(this.f11368.getClass().getSimpleName()).append(')');
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14126(Date date, Type type, YashiroNanakase yashiroNanakase) {
        ShingoYabuki shingoYabuki;
        synchronized (this.f11368) {
            shingoYabuki = new ShingoYabuki(this.f11370.format(date));
        }
        return shingoYabuki;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Date b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura {
        if (!(saishuKusanagi instanceof ShingoYabuki)) {
            throw new ChizuruKagura("The date should be a string value");
        }
        Date r0 = m14125(saishuKusanagi);
        if (type == Date.class) {
            return r0;
        }
        if (type == Timestamp.class) {
            return new Timestamp(r0.getTime());
        }
        if (type == java.sql.Date.class) {
            return new java.sql.Date(r0.getTime());
        }
        throw new IllegalArgumentException(getClass() + " cannot deserialize to " + type);
    }
}
