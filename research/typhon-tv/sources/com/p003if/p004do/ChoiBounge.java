package com.p003if.p004do;

import com.p003if.p004do.p014if.ChinGentsai;
import com.p003if.p004do.p014if.GoroDaimon;
import com.p003if.p004do.p014if.p015do.BenimaruNikaido;
import com.p003if.p004do.p014if.p015do.BrianBattler;
import com.p003if.p004do.p014if.p015do.ChangKoehan;
import com.p003if.p004do.p014if.p015do.HeavyD;
import com.p003if.p004do.p014if.p015do.IoriYagami;
import com.p003if.p004do.p014if.p015do.LeonaHeidern;
import com.p003if.p004do.p014if.p015do.LuckyGlauber;
import com.p003if.p004do.p014if.p015do.RugalBernstein;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.if.do.ChoiBounge  reason: invalid package */
public final class ChoiBounge {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final GoroDaimon f11325;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f11326;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final boolean f11327;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final boolean f11328;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final boolean f11329;

    /* renamed from: 连任  reason: contains not printable characters */
    private final List<Whip> f11330;

    /* renamed from: 靐  reason: contains not printable characters */
    final YashiroNanakase f11331;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<com.p003if.p004do.p013for.KyoKusanagi<?>, Maxima<?>> f11332;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ThreadLocal<Map<com.p003if.p004do.p013for.KyoKusanagi<?>, KyoKusanagi<?>>> f11333;

    /* renamed from: 龘  reason: contains not printable characters */
    final RugalBernstein f11334;

    /* renamed from: com.if.do.ChoiBounge$KyoKusanagi */
    static class KyoKusanagi<T> extends Maxima<T> {

        /* renamed from: 龘  reason: contains not printable characters */
        private Maxima<T> f11340;

        KyoKusanagi() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public T m14110(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
            if (this.f11340 != null) {
                return this.f11340.m14130(kyoKusanagi);
            }
            throw new IllegalStateException();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14111(Maxima<T> maxima) {
            if (this.f11340 != null) {
                throw new AssertionError();
            }
            this.f11340 = maxima;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m14112(com.p003if.p004do.p016int.GoroDaimon goroDaimon, T t) throws IOException {
            if (this.f11340 == null) {
                throw new IllegalStateException();
            }
            this.f11340.m14132(goroDaimon, t);
        }
    }

    public ChoiBounge() {
        this(ChinGentsai.f11379, ChinGentsai.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, Orochi.DEFAULT, Collections.emptyList());
    }

    ChoiBounge(ChinGentsai chinGentsai, ChangKoehan changKoehan, Map<Type, LuckyGlauber<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Orochi orochi, List<Whip> list) {
        this.f11333 = new ThreadLocal<>();
        this.f11332 = Collections.synchronizedMap(new HashMap());
        this.f11334 = new RugalBernstein() {
            /* renamed from: 龘  reason: contains not printable characters */
            public <T> T m14097(SaishuKusanagi saishuKusanagi, Type type) throws ChizuruKagura {
                return ChoiBounge.this.m14087(saishuKusanagi, type);
            }
        };
        this.f11331 = new YashiroNanakase() {
        };
        this.f11325 = new GoroDaimon(map);
        this.f11326 = z;
        this.f11328 = z3;
        this.f11327 = z4;
        this.f11329 = z5;
        ArrayList arrayList = new ArrayList();
        arrayList.add(LeonaHeidern.f11500);
        arrayList.add(LuckyGlauber.f11542);
        arrayList.add(chinGentsai);
        arrayList.addAll(list);
        arrayList.add(LeonaHeidern.f11505);
        arrayList.add(LeonaHeidern.f11487);
        arrayList.add(LeonaHeidern.f11483);
        arrayList.add(LeonaHeidern.f11503);
        arrayList.add(LeonaHeidern.f11508);
        arrayList.add(LeonaHeidern.m14337(Long.TYPE, Long.class, m14073(orochi)));
        arrayList.add(LeonaHeidern.m14337(Double.TYPE, Double.class, m14074(z6)));
        arrayList.add(LeonaHeidern.m14337(Float.TYPE, Float.class, m14072(z6)));
        arrayList.add(LeonaHeidern.f11499);
        arrayList.add(LeonaHeidern.f11491);
        arrayList.add(LeonaHeidern.f11512);
        arrayList.add(LeonaHeidern.f11514);
        arrayList.add(LeonaHeidern.m14336(BigDecimal.class, LeonaHeidern.f11502));
        arrayList.add(LeonaHeidern.m14336(BigInteger.class, LeonaHeidern.f11504));
        arrayList.add(LeonaHeidern.f11521);
        arrayList.add(LeonaHeidern.f11509);
        arrayList.add(LeonaHeidern.f11484);
        arrayList.add(LeonaHeidern.f11496);
        arrayList.add(LeonaHeidern.f11482);
        arrayList.add(LeonaHeidern.f11518);
        arrayList.add(com.p003if.p004do.p014if.p015do.GoroDaimon.f11466);
        arrayList.add(LeonaHeidern.f11506);
        arrayList.add(IoriYagami.f11476);
        arrayList.add(RugalBernstein.f11545);
        arrayList.add(LeonaHeidern.f11490);
        arrayList.add(com.p003if.p004do.p014if.p015do.KyoKusanagi.f11478);
        arrayList.add(LeonaHeidern.f11517);
        arrayList.add(new BenimaruNikaido(this.f11325));
        arrayList.add(new HeavyD(this.f11325, z2));
        arrayList.add(new com.p003if.p004do.p014if.p015do.ChinGentsai(this.f11325));
        arrayList.add(LeonaHeidern.f11498);
        arrayList.add(new BrianBattler(this.f11325, changKoehan, chinGentsai));
        this.f11330 = Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Maxima<Number> m14072(boolean z) {
        return z ? LeonaHeidern.f11522 : new Maxima<Number>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Float m14102(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
                if (kyoKusanagi.m14511() != com.p003if.p004do.p016int.BenimaruNikaido.NULL) {
                    return Float.valueOf((float) kyoKusanagi.m14520());
                }
                kyoKusanagi.m14519();
                return null;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m14105(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Number number) throws IOException {
                if (number == null) {
                    goroDaimon.m14470();
                    return;
                }
                ChoiBounge.this.m14075((double) number.floatValue());
                goroDaimon.m14484(number);
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Maxima<Number> m14073(Orochi orochi) {
        return orochi == Orochi.DEFAULT ? LeonaHeidern.f11489 : new Maxima<Number>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Number m14106(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
                if (kyoKusanagi.m14511() != com.p003if.p004do.p016int.BenimaruNikaido.NULL) {
                    return Long.valueOf(kyoKusanagi.m14516());
                }
                kyoKusanagi.m14519();
                return null;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m14109(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Number number) throws IOException {
                if (number == null) {
                    goroDaimon.m14470();
                } else {
                    goroDaimon.m14476(number.toString());
                }
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Maxima<Number> m14074(boolean z) {
        return z ? LeonaHeidern.f11523 : new Maxima<Number>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Double m14098(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) throws IOException {
                if (kyoKusanagi.m14511() != com.p003if.p004do.p016int.BenimaruNikaido.NULL) {
                    return Double.valueOf(kyoKusanagi.m14520());
                }
                kyoKusanagi.m14519();
                return null;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m14101(com.p003if.p004do.p016int.GoroDaimon goroDaimon, Number number) throws IOException {
                if (number == null) {
                    goroDaimon.m14470();
                    return;
                }
                ChoiBounge.this.m14075(number.doubleValue());
                goroDaimon.m14484(number);
            }
        };
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m14075(double d) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            throw new IllegalArgumentException(d + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m14077(Object obj, com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi) {
        if (obj != null) {
            try {
                if (kyoKusanagi.m14511() != com.p003if.p004do.p016int.BenimaruNikaido.END_DOCUMENT) {
                    throw new LeonaHeidern("JSON document was not fully consumed.");
                }
            } catch (com.p003if.p004do.p016int.ChinGentsai e) {
                throw new Chris((Throwable) e);
            } catch (IOException e2) {
                throw new LeonaHeidern((Throwable) e2);
            }
        }
    }

    public String toString() {
        return "{serializeNulls:" + this.f11326 + "factories:" + this.f11330 + ",instanceCreators:" + this.f11325 + "}";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m14078(Object obj) {
        return obj == null ? m14092((SaishuKusanagi) Mature.f5653) : m14079(obj, obj.getClass());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m14079(Object obj, Type type) {
        StringWriter stringWriter = new StringWriter();
        m14096(obj, type, (Appendable) stringWriter);
        return stringWriter.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14080(Whip whip, com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
        boolean z = false;
        if (!this.f11330.contains(whip)) {
            z = true;
        }
        boolean z2 = z;
        for (Whip next : this.f11330) {
            if (z2) {
                Maxima<T> r0 = next.m14172(this, kyoKusanagi);
                if (r0 != null) {
                    return r0;
                }
            } else if (next == whip) {
                z2 = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + kyoKusanagi);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14081(com.p003if.p004do.p013for.KyoKusanagi<T> kyoKusanagi) {
        Map map;
        Maxima<T> maxima = this.f11332.get(kyoKusanagi);
        if (maxima == null) {
            Map map2 = this.f11333.get();
            boolean z = false;
            if (map2 == null) {
                map = new HashMap();
                this.f11333.set(map);
                z = true;
            } else {
                map = map2;
            }
            maxima = (KyoKusanagi) map.get(kyoKusanagi);
            if (maxima == null) {
                try {
                    KyoKusanagi kyoKusanagi2 = new KyoKusanagi();
                    map.put(kyoKusanagi, kyoKusanagi2);
                    for (Whip r0 : this.f11330) {
                        maxima = r0.m14172(this, kyoKusanagi);
                        if (maxima != null) {
                            kyoKusanagi2.m14111(maxima);
                            this.f11332.put(kyoKusanagi, maxima);
                            map.remove(kyoKusanagi);
                            if (z) {
                                this.f11333.remove();
                            }
                        }
                    }
                    throw new IllegalArgumentException("GSON cannot handle " + kyoKusanagi);
                } catch (Throwable th) {
                    map.remove(kyoKusanagi);
                    if (z) {
                        this.f11333.remove();
                    }
                    throw th;
                }
            }
        }
        return maxima;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Maxima<T> m14082(Class<T> cls) {
        return m14081(com.p003if.p004do.p013for.KyoKusanagi.b(cls));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14083(Object obj) {
        return obj == null ? Mature.f5653 : m14084(obj, (Type) obj.getClass());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14084(Object obj, Type type) {
        com.p003if.p004do.p014if.p015do.ChoiBounge choiBounge = new com.p003if.p004do.p014if.p015do.ChoiBounge();
        m14095(obj, type, (com.p003if.p004do.p016int.GoroDaimon) choiBounge);
        return choiBounge.m14305();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public com.p003if.p004do.p016int.GoroDaimon m14085(Writer writer) throws IOException {
        if (this.f11328) {
            writer.write(")]}'\n");
        }
        com.p003if.p004do.p016int.GoroDaimon goroDaimon = new com.p003if.p004do.p016int.GoroDaimon(writer);
        if (this.f11329) {
            goroDaimon.m14481("  ");
        }
        goroDaimon.m14479(this.f11326);
        return goroDaimon;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m14086(SaishuKusanagi saishuKusanagi, Class<T> cls) throws Chris {
        return com.p003if.p004do.p014if.BrianBattler.m14195(cls).cast(m14087(saishuKusanagi, (Type) cls));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m14087(SaishuKusanagi saishuKusanagi, Type type) throws Chris {
        if (saishuKusanagi == null) {
            return null;
        }
        return m14088((com.p003if.p004do.p016int.KyoKusanagi) new ChangKoehan(saishuKusanagi), type);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m14088(com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi, Type type) throws LeonaHeidern, Chris {
        boolean z = true;
        boolean r2 = kyoKusanagi.m14527();
        kyoKusanagi.m14526(true);
        try {
            kyoKusanagi.m14511();
            z = false;
            T r0 = m14081(com.p003if.p004do.p013for.KyoKusanagi.a(type)).m14130(kyoKusanagi);
            kyoKusanagi.m14526(r2);
            return r0;
        } catch (EOFException e) {
            if (z) {
                kyoKusanagi.m14526(r2);
                return null;
            }
            throw new Chris((Throwable) e);
        } catch (IllegalStateException e2) {
            throw new Chris((Throwable) e2);
        } catch (IOException e3) {
            throw new Chris((Throwable) e3);
        } catch (Throwable th) {
            kyoKusanagi.m14526(r2);
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m14089(Reader reader, Type type) throws LeonaHeidern, Chris {
        com.p003if.p004do.p016int.KyoKusanagi kyoKusanagi = new com.p003if.p004do.p016int.KyoKusanagi(reader);
        T r1 = m14088(kyoKusanagi, type);
        m14077((Object) r1, kyoKusanagi);
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m14090(String str, Class<T> cls) throws Chris {
        return com.p003if.p004do.p014if.BrianBattler.m14195(cls).cast(m14091(str, (Type) cls));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m14091(String str, Type type) throws Chris {
        if (str == null) {
            return null;
        }
        return m14089((Reader) new StringReader(str), type);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m14092(SaishuKusanagi saishuKusanagi) {
        StringWriter stringWriter = new StringWriter();
        m14094(saishuKusanagi, (Appendable) stringWriter);
        return stringWriter.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14093(SaishuKusanagi saishuKusanagi, com.p003if.p004do.p016int.GoroDaimon goroDaimon) throws LeonaHeidern {
        boolean r1 = goroDaimon.m14471();
        goroDaimon.m14477(true);
        boolean r2 = goroDaimon.m14472();
        goroDaimon.m14482(this.f11327);
        boolean r3 = goroDaimon.m14473();
        goroDaimon.m14479(this.f11326);
        try {
            com.p003if.p004do.p014if.RugalBernstein.m14254(saishuKusanagi, goroDaimon);
            goroDaimon.m14477(r1);
            goroDaimon.m14482(r2);
            goroDaimon.m14479(r3);
        } catch (IOException e) {
            throw new LeonaHeidern((Throwable) e);
        } catch (Throwable th) {
            goroDaimon.m14477(r1);
            goroDaimon.m14482(r2);
            goroDaimon.m14479(r3);
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14094(SaishuKusanagi saishuKusanagi, Appendable appendable) throws LeonaHeidern {
        try {
            m14093(saishuKusanagi, m14085(com.p003if.p004do.p014if.RugalBernstein.m14253(appendable)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14095(Object obj, Type type, com.p003if.p004do.p016int.GoroDaimon goroDaimon) throws LeonaHeidern {
        Maxima<?> r0 = m14081(com.p003if.p004do.p013for.KyoKusanagi.a(type));
        boolean r1 = goroDaimon.m14471();
        goroDaimon.m14477(true);
        boolean r2 = goroDaimon.m14472();
        goroDaimon.m14482(this.f11327);
        boolean r3 = goroDaimon.m14473();
        goroDaimon.m14479(this.f11326);
        try {
            r0.m14132(goroDaimon, obj);
            goroDaimon.m14477(r1);
            goroDaimon.m14482(r2);
            goroDaimon.m14479(r3);
        } catch (IOException e) {
            throw new LeonaHeidern((Throwable) e);
        } catch (Throwable th) {
            goroDaimon.m14477(r1);
            goroDaimon.m14482(r2);
            goroDaimon.m14479(r3);
            throw th;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14096(Object obj, Type type, Appendable appendable) throws LeonaHeidern {
        try {
            m14095(obj, type, m14085(com.p003if.p004do.p014if.RugalBernstein.m14253(appendable)));
        } catch (IOException e) {
            throw new LeonaHeidern((Throwable) e);
        }
    }
}
