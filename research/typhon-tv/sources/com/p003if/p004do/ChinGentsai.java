package com.p003if.p004do;

import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.reflect.Field;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

/* renamed from: com.if.do.ChinGentsai  reason: invalid package */
public enum ChinGentsai implements ChangKoehan {
    IDENTITY {
        /* renamed from: 龘  reason: contains not printable characters */
        public String m14067(Field field) {
            return field.getName();
        }
    },
    UPPER_CAMEL_CASE {
        /* renamed from: 龘  reason: contains not printable characters */
        public String m14068(Field field) {
            return ChinGentsai.m14062(field.getName());
        }
    },
    UPPER_CAMEL_CASE_WITH_SPACES {
        /* renamed from: 龘  reason: contains not printable characters */
        public String m14069(Field field) {
            return ChinGentsai.m14062(ChinGentsai.m14063(field.getName(), StringUtils.SPACE));
        }
    },
    LOWER_CASE_WITH_UNDERSCORES {
        /* renamed from: 龘  reason: contains not printable characters */
        public String m14070(Field field) {
            return ChinGentsai.m14063(field.getName(), EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR).toLowerCase(Locale.ENGLISH);
        }
    },
    LOWER_CASE_WITH_DASHES {
        /* renamed from: 龘  reason: contains not printable characters */
        public String m14071(Field field) {
            return ChinGentsai.m14063(field.getName(), "-").toLowerCase(Locale.ENGLISH);
        }
    };

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m14062(String str) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        char charAt = str.charAt(0);
        while (i < str.length() - 1 && !Character.isLetter(charAt)) {
            sb.append(charAt);
            i++;
            charAt = str.charAt(i);
        }
        return i == str.length() ? sb.toString() : !Character.isUpperCase(charAt) ? sb.append(m14064(Character.toUpperCase(charAt), str, i + 1)).toString() : str;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m14063(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt) && sb.length() != 0) {
                sb.append(str2);
            }
            sb.append(charAt);
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m14064(char c, String str, int i) {
        return i < str.length() ? c + str.substring(i) : String.valueOf(c);
    }
}
