package com.p003if.p004do.p005do;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: com.if.do.do.ChinGentsai  reason: invalid package */
public @interface ChinGentsai {
    /* renamed from: 龘  reason: contains not printable characters */
    double m6136();
}
