package com.p003if.p004do.p005do;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: com.if.do.do.GoroDaimon  reason: invalid package */
public @interface GoroDaimon {
    /* renamed from: 靐  reason: contains not printable characters */
    String[] m6137() default {};

    /* renamed from: 龘  reason: contains not printable characters */
    String m6138();
}
