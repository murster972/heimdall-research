package com.p003if.p004do.p005do;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: com.if.do.do.KyoKusanagi  reason: invalid package */
public @interface KyoKusanagi {
    /* renamed from: 靐  reason: contains not printable characters */
    boolean m6139() default true;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m6140() default true;
}
