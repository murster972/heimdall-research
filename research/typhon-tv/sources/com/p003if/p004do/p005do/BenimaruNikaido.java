package com.p003if.p004do.p005do;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: com.if.do.do.BenimaruNikaido  reason: invalid package */
public @interface BenimaruNikaido {
    /* renamed from: 龘  reason: contains not printable characters */
    Class<?> m6134();
}
