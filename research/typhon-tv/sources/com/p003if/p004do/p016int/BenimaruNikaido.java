package com.p003if.p004do.p016int;

/* renamed from: com.if.do.int.BenimaruNikaido  reason: invalid package */
public enum BenimaruNikaido {
    BEGIN_ARRAY,
    END_ARRAY,
    BEGIN_OBJECT,
    END_OBJECT,
    NAME,
    STRING,
    NUMBER,
    BOOLEAN,
    NULL,
    END_DOCUMENT
}
