package com.p003if.p004do.p016int;

import com.p003if.p004do.p014if.ChangKoehan;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

/* renamed from: com.if.do.int.KyoKusanagi  reason: invalid package */
public class KyoKusanagi implements Closeable {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final char[] f11562 = ")]}'\n".toCharArray();

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f11563 = 0;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f11564 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f11565 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int[] f11566 = new int[32];

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f11567 = 0;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f11568;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public int f11569 = 0;

    /* renamed from: ٴ  reason: contains not printable characters */
    private long f11570;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f11571;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f11572 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Reader f11573;

    /* renamed from: 麤  reason: contains not printable characters */
    private final char[] f11574 = new char[1024];

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f11575 = false;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String[] f11576;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int[] f11577;

    static {
        ChangKoehan.f11378 = new ChangKoehan() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m14528(KyoKusanagi kyoKusanagi) throws IOException {
                if (kyoKusanagi instanceof com.p003if.p004do.p014if.p015do.ChangKoehan) {
                    ((com.p003if.p004do.p014if.p015do.ChangKoehan) kyoKusanagi).m14294();
                    return;
                }
                int r0 = kyoKusanagi.f11569;
                if (r0 == 0) {
                    r0 = kyoKusanagi.m14510();
                }
                if (r0 == 13) {
                    int unused = kyoKusanagi.f11569 = 9;
                } else if (r0 == 12) {
                    int unused2 = kyoKusanagi.f11569 = 8;
                } else if (r0 == 14) {
                    int unused3 = kyoKusanagi.f11569 = 10;
                } else {
                    throw new IllegalStateException("Expected a name but was " + kyoKusanagi.m14511() + StringUtils.SPACE + " at line " + kyoKusanagi.m14491() + " column " + kyoKusanagi.m14492() + " path " + kyoKusanagi.m14517());
                }
            }
        };
    }

    public KyoKusanagi(Reader reader) {
        int[] iArr = this.f11566;
        int i = this.f11567;
        this.f11567 = i + 1;
        iArr[i] = 6;
        this.f11576 = new String[32];
        this.f11577 = new int[32];
        if (reader == null) {
            throw new NullPointerException("in == null");
        }
        this.f11573 = reader;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private String m14487() throws IOException {
        String sb;
        StringBuilder sb2 = null;
        int i = 0;
        while (true) {
            if (this.f11572 + i < this.f11563) {
                switch (this.f11574[this.f11572 + i]) {
                    case 9:
                    case 10:
                    case 12:
                    case 13:
                    case ' ':
                    case ',':
                    case ':':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                        break;
                    case '#':
                    case '/':
                    case ';':
                    case '=':
                    case '\\':
                        m14493();
                        break;
                    default:
                        i++;
                        continue;
                }
            } else if (i >= this.f11574.length) {
                if (sb2 == null) {
                    sb2 = new StringBuilder();
                }
                sb2.append(this.f11574, this.f11572, i);
                this.f11572 = i + this.f11572;
                if (!m14501(1)) {
                    i = 0;
                } else {
                    i = 0;
                }
            } else if (m14501(i + 1)) {
            }
        }
        if (sb2 == null) {
            sb = new String(this.f11574, this.f11572, i);
        } else {
            sb2.append(this.f11574, this.f11572, i);
            sb = sb2.toString();
        }
        this.f11572 = i + this.f11572;
        return sb;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private void m14488() throws IOException {
        do {
            int i = 0;
            while (this.f11572 + i < this.f11563) {
                switch (this.f11574[this.f11572 + i]) {
                    case 9:
                    case 10:
                    case 12:
                    case 13:
                    case ' ':
                    case ',':
                    case ':':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                        break;
                    case '#':
                    case '/':
                    case ';':
                    case '=':
                    case '\\':
                        m14493();
                        break;
                    default:
                        i++;
                }
                this.f11572 = i + this.f11572;
                return;
            }
            this.f11572 = i + this.f11572;
        } while (m14501(1));
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private int m14489() throws IOException {
        String str;
        String str2;
        int i;
        char c = this.f11574[this.f11572];
        if (c == 't' || c == 'T') {
            str = "true";
            str2 = "TRUE";
            i = 5;
        } else if (c == 'f' || c == 'F') {
            str = "false";
            str2 = "FALSE";
            i = 6;
        } else if (c != 'n' && c != 'N') {
            return 0;
        } else {
            str = "null";
            str2 = "NULL";
            i = 7;
        }
        int length = str.length();
        for (int i2 = 1; i2 < length; i2++) {
            if (this.f11572 + i2 >= this.f11563 && !m14501(i2 + 1)) {
                return 0;
            }
            char c2 = this.f11574[this.f11572 + i2];
            if (c2 != str.charAt(i2) && c2 != str2.charAt(i2)) {
                return 0;
            }
        }
        if ((this.f11572 + length < this.f11563 || m14501(length + 1)) && m14508(this.f11574[this.f11572 + length])) {
            return 0;
        }
        this.f11572 += length;
        this.f11569 = i;
        return i;
    }

    /* JADX WARNING: type inference failed for: r2v5 */
    /* JADX WARNING: type inference failed for: r2v7 */
    /* JADX WARNING: type inference failed for: r2v10 */
    /* JADX WARNING: type inference failed for: r2v11 */
    /* JADX WARNING: type inference failed for: r2v13 */
    /* JADX WARNING: type inference failed for: r2v14 */
    /* JADX WARNING: type inference failed for: r2v17 */
    /* JADX WARNING: type inference failed for: r2v20 */
    /* JADX WARNING: type inference failed for: r2v22 */
    /* JADX WARNING: type inference failed for: r2v23 */
    /* JADX WARNING: type inference failed for: r2v27 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: ˎ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m14490() throws java.io.IOException {
        /*
            r14 = this;
            char[] r11 = r14.f11574
            int r1 = r14.f11572
            int r0 = r14.f11563
            r6 = 0
            r5 = 0
            r4 = 1
            r3 = 0
            r2 = 0
            r10 = r2
        L_0x000d:
            int r2 = r1 + r10
            if (r2 != r0) goto L_0x003d
            int r0 = r11.length
            if (r10 != r0) goto L_0x0016
            r0 = 0
        L_0x0015:
            return r0
        L_0x0016:
            int r0 = r10 + 1
            boolean r0 = r14.m14501((int) r0)
            if (r0 != 0) goto L_0x0039
        L_0x001e:
            r0 = 2
            if (r3 != r0) goto L_0x00c7
            if (r4 == 0) goto L_0x00c7
            r0 = -9223372036854775808
            int r0 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x002b
            if (r5 == 0) goto L_0x00c7
        L_0x002b:
            if (r5 == 0) goto L_0x00c4
        L_0x002d:
            r14.f11570 = r6
            int r0 = r14.f11572
            int r0 = r0 + r10
            r14.f11572 = r0
            r0 = 15
            r14.f11569 = r0
            goto L_0x0015
        L_0x0039:
            int r1 = r14.f11572
            int r0 = r14.f11563
        L_0x003d:
            int r2 = r1 + r10
            char r2 = r11[r2]
            switch(r2) {
                case 43: goto L_0x0065;
                case 45: goto L_0x0054;
                case 46: goto L_0x0076;
                case 69: goto L_0x006c;
                case 101: goto L_0x006c;
                default: goto L_0x0044;
            }
        L_0x0044:
            r8 = 48
            if (r2 < r8) goto L_0x004c
            r8 = 57
            if (r2 <= r8) goto L_0x007d
        L_0x004c:
            boolean r0 = r14.m14508((char) r2)
            if (r0 == 0) goto L_0x001e
            r0 = 0
            goto L_0x0015
        L_0x0054:
            if (r3 != 0) goto L_0x005e
            r3 = 1
            r2 = 1
            r5 = r3
        L_0x0059:
            int r8 = r10 + 1
            r10 = r8
            r3 = r2
            goto L_0x000d
        L_0x005e:
            r2 = 5
            if (r3 != r2) goto L_0x0063
            r2 = 6
            goto L_0x0059
        L_0x0063:
            r0 = 0
            goto L_0x0015
        L_0x0065:
            r2 = 5
            if (r3 != r2) goto L_0x006a
            r2 = 6
            goto L_0x0059
        L_0x006a:
            r0 = 0
            goto L_0x0015
        L_0x006c:
            r2 = 2
            if (r3 == r2) goto L_0x0072
            r2 = 4
            if (r3 != r2) goto L_0x0074
        L_0x0072:
            r2 = 5
            goto L_0x0059
        L_0x0074:
            r0 = 0
            goto L_0x0015
        L_0x0076:
            r2 = 2
            if (r3 != r2) goto L_0x007b
            r2 = 3
            goto L_0x0059
        L_0x007b:
            r0 = 0
            goto L_0x0015
        L_0x007d:
            r8 = 1
            if (r3 == r8) goto L_0x0082
            if (r3 != 0) goto L_0x0088
        L_0x0082:
            int r2 = r2 + -48
            int r2 = -r2
            long r6 = (long) r2
            r2 = 2
            goto L_0x0059
        L_0x0088:
            r8 = 2
            if (r3 != r8) goto L_0x00b7
            r8 = 0
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 != 0) goto L_0x0093
            r0 = 0
            goto L_0x0015
        L_0x0093:
            r8 = 10
            long r8 = r8 * r6
            int r2 = r2 + -48
            long r12 = (long) r2
            long r8 = r8 - r12
            r12 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r2 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r2 > 0) goto L_0x00b0
            r12 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r2 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r2 != 0) goto L_0x00b5
            int r2 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r2 >= 0) goto L_0x00b5
        L_0x00b0:
            r2 = 1
        L_0x00b1:
            r4 = r4 & r2
            r2 = r3
            r6 = r8
            goto L_0x0059
        L_0x00b5:
            r2 = 0
            goto L_0x00b1
        L_0x00b7:
            r2 = 3
            if (r3 != r2) goto L_0x00bc
            r2 = 4
            goto L_0x0059
        L_0x00bc:
            r2 = 5
            if (r3 == r2) goto L_0x00c2
            r2 = 6
            if (r3 != r2) goto L_0x00db
        L_0x00c2:
            r2 = 7
            goto L_0x0059
        L_0x00c4:
            long r6 = -r6
            goto L_0x002d
        L_0x00c7:
            r0 = 2
            if (r3 == r0) goto L_0x00d0
            r0 = 4
            if (r3 == r0) goto L_0x00d0
            r0 = 7
            if (r3 != r0) goto L_0x00d8
        L_0x00d0:
            r14.f11571 = r10
            r0 = 16
            r14.f11569 = r0
            goto L_0x0015
        L_0x00d8:
            r0 = 0
            goto L_0x0015
        L_0x00db:
            r2 = r3
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.p003if.p004do.p016int.KyoKusanagi.m14490():int");
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public int m14491() {
        return this.f11564 + 1;
    }

    /* access modifiers changed from: private */
    /* renamed from: י  reason: contains not printable characters */
    public int m14492() {
        return (this.f11572 - this.f11565) + 1;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    private void m14493() throws IOException {
        if (!this.f11575) {
            throw m14499("Use JsonReader.setLenient(true) to accept malformed JSON");
        }
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    private void m14494() throws IOException {
        char c;
        do {
            if (this.f11572 < this.f11563 || m14501(1)) {
                char[] cArr = this.f11574;
                int i = this.f11572;
                this.f11572 = i + 1;
                c = cArr[i];
                if (c == 10) {
                    this.f11564++;
                    this.f11565 = this.f11572;
                    return;
                }
            } else {
                return;
            }
        } while (c != 13);
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    private char m14495() throws IOException {
        int i;
        if (this.f11572 != this.f11563 || m14501(1)) {
            char[] cArr = this.f11574;
            int i2 = this.f11572;
            this.f11572 = i2 + 1;
            char c = cArr[i2];
            switch (c) {
                case 10:
                    this.f11564++;
                    this.f11565 = this.f11572;
                    return c;
                case 'b':
                    return 8;
                case 'f':
                    return 12;
                case 'n':
                    return 10;
                case 'r':
                    return CharUtils.CR;
                case 't':
                    return 9;
                case 'u':
                    if (this.f11572 + 4 <= this.f11563 || m14501(4)) {
                        char c2 = 0;
                        int i3 = this.f11572;
                        int i4 = i3 + 4;
                        while (i3 < i4) {
                            char c3 = this.f11574[i3];
                            char c4 = (char) (c2 << 4);
                            if (c3 >= '0' && c3 <= '9') {
                                i = c3 - '0';
                            } else if (c3 >= 'a' && c3 <= 'f') {
                                i = (c3 - 'a') + 10;
                            } else if (c3 < 'A' || c3 > 'F') {
                                throw new NumberFormatException("\\u" + new String(this.f11574, this.f11572, 4));
                            } else {
                                i = (c3 - 'A') + 10;
                            }
                            c2 = (char) (c4 + i);
                            i3++;
                        }
                        this.f11572 += 4;
                        return c2;
                    }
                    throw m14499("Unterminated escape sequence");
                default:
                    return c;
            }
        } else {
            throw m14499("Unterminated escape sequence");
        }
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    private void m14496() throws IOException {
        m14498(true);
        this.f11572--;
        if (this.f11572 + f11562.length <= this.f11563 || m14501(f11562.length)) {
            int i = 0;
            while (i < f11562.length) {
                if (this.f11574[this.f11572 + i] == f11562[i]) {
                    i++;
                } else {
                    return;
                }
            }
            this.f11572 += f11562.length;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m14498(boolean z) throws IOException {
        char[] cArr = this.f11574;
        int i = this.f11572;
        int i2 = this.f11563;
        while (true) {
            if (i == i2) {
                this.f11572 = i;
                if (m14501(1)) {
                    i = this.f11572;
                    i2 = this.f11563;
                } else if (!z) {
                    return -1;
                } else {
                    throw new EOFException("End of input at line " + m14491() + " column " + m14492());
                }
            }
            int i3 = i + 1;
            char c = cArr[i];
            if (c == 10) {
                this.f11564++;
                this.f11565 = i3;
                i = i3;
            } else if (c == ' ' || c == 13) {
                i = i3;
            } else if (c == 9) {
                i = i3;
            } else if (c == '/') {
                this.f11572 = i3;
                if (i3 == i2) {
                    this.f11572--;
                    boolean r0 = m14501(2);
                    this.f11572++;
                    if (!r0) {
                        return c;
                    }
                }
                m14493();
                switch (cArr[this.f11572]) {
                    case '*':
                        this.f11572++;
                        if (m14509("*/")) {
                            i = this.f11572 + 2;
                            i2 = this.f11563;
                            break;
                        } else {
                            throw m14499("Unterminated comment");
                        }
                    case '/':
                        this.f11572++;
                        m14494();
                        i = this.f11572;
                        i2 = this.f11563;
                        break;
                    default:
                        return c;
                }
            } else if (c == '#') {
                this.f11572 = i3;
                m14493();
                m14494();
                i = this.f11572;
                i2 = this.f11563;
            } else {
                this.f11572 = i3;
                return c;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private IOException m14499(String str) throws IOException {
        throw new ChinGentsai(str + " at line " + m14491() + " column " + m14492() + " path " + m14517());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m14500(char c) throws IOException {
        int i;
        char[] cArr = this.f11574;
        StringBuilder sb = new StringBuilder();
        do {
            int i2 = this.f11572;
            int i3 = this.f11563;
            int i4 = i2;
            int i5 = i2;
            while (i5 < i3) {
                int i6 = i5 + 1;
                char c2 = cArr[i5];
                if (c2 == c) {
                    this.f11572 = i6;
                    sb.append(cArr, i4, (i6 - i4) - 1);
                    return sb.toString();
                }
                if (c2 == '\\') {
                    this.f11572 = i6;
                    sb.append(cArr, i4, (i6 - i4) - 1);
                    sb.append(m14495());
                    int i7 = this.f11572;
                    i = this.f11563;
                    i4 = i7;
                    i6 = i7;
                } else {
                    if (c2 == 10) {
                        this.f11564++;
                        this.f11565 = i6;
                    }
                    i = i3;
                }
                i3 = i;
                i5 = i6;
            }
            sb.append(cArr, i4, i5 - i4);
            this.f11572 = i5;
        } while (m14501(1));
        throw m14499("Unterminated string");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m14501(int i) throws IOException {
        char[] cArr = this.f11574;
        this.f11565 -= this.f11572;
        if (this.f11563 != this.f11572) {
            this.f11563 -= this.f11572;
            System.arraycopy(cArr, this.f11572, cArr, 0, this.f11563);
        } else {
            this.f11563 = 0;
        }
        this.f11572 = 0;
        do {
            int read = this.f11573.read(cArr, this.f11563, cArr.length - this.f11563);
            if (read == -1) {
                return false;
            }
            this.f11563 = read + this.f11563;
            if (this.f11564 == 0 && this.f11565 == 0 && this.f11563 > 0 && cArr[0] == 65279) {
                this.f11572++;
                this.f11565++;
                i++;
            }
        } while (this.f11563 < i);
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m14504(char c) throws IOException {
        char[] cArr = this.f11574;
        do {
            int i = this.f11572;
            int i2 = this.f11563;
            int i3 = i;
            while (i3 < i2) {
                int i4 = i3 + 1;
                char c2 = cArr[i3];
                if (c2 == c) {
                    this.f11572 = i4;
                    return;
                }
                if (c2 == '\\') {
                    this.f11572 = i4;
                    m14495();
                    i4 = this.f11572;
                    i2 = this.f11563;
                } else if (c2 == 10) {
                    this.f11564++;
                    this.f11565 = i4;
                }
                i3 = i4;
            }
            this.f11572 = i3;
        } while (m14501(1));
        throw m14499("Unterminated string");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14507(int i) {
        if (this.f11567 == this.f11566.length) {
            int[] iArr = new int[(this.f11567 * 2)];
            int[] iArr2 = new int[(this.f11567 * 2)];
            String[] strArr = new String[(this.f11567 * 2)];
            System.arraycopy(this.f11566, 0, iArr, 0, this.f11567);
            System.arraycopy(this.f11577, 0, iArr2, 0, this.f11567);
            System.arraycopy(this.f11576, 0, strArr, 0, this.f11567);
            this.f11566 = iArr;
            this.f11577 = iArr2;
            this.f11576 = strArr;
        }
        int[] iArr3 = this.f11566;
        int i2 = this.f11567;
        this.f11567 = i2 + 1;
        iArr3[i2] = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m14508(char c) throws IOException {
        switch (c) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
            case ',':
            case ':':
            case '[':
            case ']':
            case '{':
            case '}':
                break;
            case '#':
            case '/':
            case ';':
            case '=':
            case '\\':
                m14493();
                break;
            default:
                return true;
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m14509(String str) throws IOException {
        while (true) {
            if (this.f11572 + str.length() > this.f11563 && !m14501(str.length())) {
                return false;
            }
            if (this.f11574[this.f11572] == 10) {
                this.f11564++;
                this.f11565 = this.f11572 + 1;
            } else {
                int i = 0;
                while (i < str.length()) {
                    if (this.f11574[this.f11572 + i] == str.charAt(i)) {
                        i++;
                    }
                }
                return true;
            }
            this.f11572++;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public int m14510() throws IOException {
        int i = this.f11566[this.f11567 - 1];
        if (i == 1) {
            this.f11566[this.f11567 - 1] = 2;
        } else if (i == 2) {
            switch (m14498(true)) {
                case 44:
                    break;
                case 59:
                    m14493();
                    break;
                case 93:
                    this.f11569 = 4;
                    return 4;
                default:
                    throw m14499("Unterminated array");
            }
        } else if (i == 3 || i == 5) {
            this.f11566[this.f11567 - 1] = 4;
            if (i == 5) {
                switch (m14498(true)) {
                    case 44:
                        break;
                    case 59:
                        m14493();
                        break;
                    case 125:
                        this.f11569 = 2;
                        return 2;
                    default:
                        throw m14499("Unterminated object");
                }
            }
            int r0 = m14498(true);
            switch (r0) {
                case 34:
                    this.f11569 = 13;
                    return 13;
                case 39:
                    m14493();
                    this.f11569 = 12;
                    return 12;
                case 125:
                    if (i != 5) {
                        this.f11569 = 2;
                        return 2;
                    }
                    throw m14499("Expected name");
                default:
                    m14493();
                    this.f11572--;
                    if (m14508((char) r0)) {
                        this.f11569 = 14;
                        return 14;
                    }
                    throw m14499("Expected name");
            }
        } else if (i == 4) {
            this.f11566[this.f11567 - 1] = 5;
            switch (m14498(true)) {
                case 58:
                    break;
                case 61:
                    m14493();
                    if ((this.f11572 < this.f11563 || m14501(1)) && this.f11574[this.f11572] == '>') {
                        this.f11572++;
                        break;
                    }
                default:
                    throw m14499("Expected ':'");
            }
        } else if (i == 6) {
            if (this.f11575) {
                m14496();
            }
            this.f11566[this.f11567 - 1] = 7;
        } else if (i == 7) {
            if (m14498(false) == -1) {
                this.f11569 = 17;
                return 17;
            }
            m14493();
            this.f11572--;
        } else if (i == 8) {
            throw new IllegalStateException("JsonReader is closed");
        }
        switch (m14498(true)) {
            case 34:
                if (this.f11567 == 1) {
                    m14493();
                }
                this.f11569 = 9;
                return 9;
            case 39:
                m14493();
                this.f11569 = 8;
                return 8;
            case 44:
            case 59:
                break;
            case 91:
                this.f11569 = 3;
                return 3;
            case 93:
                if (i == 1) {
                    this.f11569 = 4;
                    return 4;
                }
                break;
            case 123:
                this.f11569 = 1;
                return 1;
            default:
                this.f11572--;
                if (this.f11567 == 1) {
                    m14493();
                }
                int r02 = m14489();
                if (r02 != 0) {
                    return r02;
                }
                int r03 = m14490();
                if (r03 != 0) {
                    return r03;
                }
                if (!m14508(this.f11574[this.f11572])) {
                    throw m14499("Expected value");
                }
                m14493();
                this.f11569 = 10;
                return 10;
        }
        if (i == 1 || i == 2) {
            m14493();
            this.f11572--;
            this.f11569 = 7;
            return 7;
        }
        throw m14499("Unexpected value");
    }

    public void close() throws IOException {
        this.f11569 = 0;
        this.f11566[0] = 8;
        this.f11567 = 1;
        this.f11573.close();
    }

    public String toString() {
        return getClass().getSimpleName() + " at line " + m14491() + " column " + m14492();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public BenimaruNikaido m14511() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        switch (i) {
            case 1:
                return BenimaruNikaido.BEGIN_OBJECT;
            case 2:
                return BenimaruNikaido.END_OBJECT;
            case 3:
                return BenimaruNikaido.BEGIN_ARRAY;
            case 4:
                return BenimaruNikaido.END_ARRAY;
            case 5:
            case 6:
                return BenimaruNikaido.BOOLEAN;
            case 7:
                return BenimaruNikaido.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return BenimaruNikaido.STRING;
            case 12:
            case 13:
            case 14:
                return BenimaruNikaido.NAME;
            case 15:
            case 16:
                return BenimaruNikaido.NUMBER;
            case 17:
                return BenimaruNikaido.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m14512() throws IOException {
        String r0;
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 14) {
            r0 = m14487();
        } else if (i == 12) {
            r0 = m14500('\'');
        } else if (i == 13) {
            r0 = m14500('\"');
        } else {
            throw new IllegalStateException("Expected a name but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
        this.f11569 = 0;
        this.f11576[this.f11567 - 1] = r0;
        return r0;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m14513() throws IOException {
        String str;
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 10) {
            str = m14487();
        } else if (i == 8) {
            str = m14500('\'');
        } else if (i == 9) {
            str = m14500('\"');
        } else if (i == 11) {
            str = this.f11568;
            this.f11568 = null;
        } else if (i == 15) {
            str = Long.toString(this.f11570);
        } else if (i == 16) {
            str = new String(this.f11574, this.f11572, this.f11571);
            this.f11572 += this.f11571;
        } else {
            throw new IllegalStateException("Expected a string but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
        this.f11569 = 0;
        int[] iArr = this.f11577;
        int i2 = this.f11567 - 1;
        iArr[i2] = iArr[i2] + 1;
        return str;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m14514() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 15) {
            int i2 = (int) this.f11570;
            if (this.f11570 != ((long) i2)) {
                throw new NumberFormatException("Expected an int but was " + this.f11570 + " at line " + m14491() + " column " + m14492() + " path " + m14517());
            }
            this.f11569 = 0;
            int[] iArr = this.f11577;
            int i3 = this.f11567 - 1;
            iArr[i3] = iArr[i3] + 1;
            return i2;
        }
        if (i == 16) {
            this.f11568 = new String(this.f11574, this.f11572, this.f11571);
            this.f11572 += this.f11571;
        } else if (i == 8 || i == 9) {
            this.f11568 = m14500(i == 8 ? '\'' : '\"');
            try {
                int parseInt = Integer.parseInt(this.f11568);
                this.f11569 = 0;
                int[] iArr2 = this.f11577;
                int i4 = this.f11567 - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseInt;
            } catch (NumberFormatException e) {
            }
        } else {
            throw new IllegalStateException("Expected an int but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
        this.f11569 = 11;
        double parseDouble = Double.parseDouble(this.f11568);
        int i5 = (int) parseDouble;
        if (((double) i5) != parseDouble) {
            throw new NumberFormatException("Expected an int but was " + this.f11568 + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
        this.f11568 = null;
        this.f11569 = 0;
        int[] iArr3 = this.f11577;
        int i6 = this.f11567 - 1;
        iArr3[i6] = iArr3[i6] + 1;
        return i5;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m14515() throws IOException {
        int i = 0;
        do {
            int i2 = this.f11569;
            if (i2 == 0) {
                i2 = m14510();
            }
            if (i2 == 3) {
                m14507(1);
                i++;
            } else if (i2 == 1) {
                m14507(3);
                i++;
            } else if (i2 == 4) {
                this.f11567--;
                i--;
            } else if (i2 == 2) {
                this.f11567--;
                i--;
            } else if (i2 == 14 || i2 == 10) {
                m14488();
            } else if (i2 == 8 || i2 == 12) {
                m14504('\'');
            } else if (i2 == 9 || i2 == 13) {
                m14504('\"');
            } else if (i2 == 16) {
                this.f11572 += this.f11571;
            }
            this.f11569 = 0;
        } while (i != 0);
        int[] iArr = this.f11577;
        int i3 = this.f11567 - 1;
        iArr[i3] = iArr[i3] + 1;
        this.f11576[this.f11567 - 1] = "null";
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public long m14516() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 15) {
            this.f11569 = 0;
            int[] iArr = this.f11577;
            int i2 = this.f11567 - 1;
            iArr[i2] = iArr[i2] + 1;
            return this.f11570;
        }
        if (i == 16) {
            this.f11568 = new String(this.f11574, this.f11572, this.f11571);
            this.f11572 += this.f11571;
        } else if (i == 8 || i == 9) {
            this.f11568 = m14500(i == 8 ? '\'' : '\"');
            try {
                long parseLong = Long.parseLong(this.f11568);
                this.f11569 = 0;
                int[] iArr2 = this.f11577;
                int i3 = this.f11567 - 1;
                iArr2[i3] = iArr2[i3] + 1;
                return parseLong;
            } catch (NumberFormatException e) {
            }
        } else {
            throw new IllegalStateException("Expected a long but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
        this.f11569 = 11;
        double parseDouble = Double.parseDouble(this.f11568);
        long j = (long) parseDouble;
        if (((double) j) != parseDouble) {
            throw new NumberFormatException("Expected a long but was " + this.f11568 + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
        this.f11568 = null;
        this.f11569 = 0;
        int[] iArr3 = this.f11577;
        int i4 = this.f11567 - 1;
        iArr3[i4] = iArr3[i4] + 1;
        return j;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public String m14517() {
        StringBuilder append = new StringBuilder().append('$');
        int i = this.f11567;
        for (int i2 = 0; i2 < i; i2++) {
            switch (this.f11566[i2]) {
                case 1:
                case 2:
                    append.append('[').append(this.f11577[i2]).append(']');
                    break;
                case 3:
                case 4:
                case 5:
                    append.append('.');
                    if (this.f11576[i2] == null) {
                        break;
                    } else {
                        append.append(this.f11576[i2]);
                        break;
                    }
            }
        }
        return append.toString();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m14518() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 5) {
            this.f11569 = 0;
            int[] iArr = this.f11577;
            int i2 = this.f11567 - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.f11569 = 0;
            int[] iArr2 = this.f11577;
            int i3 = this.f11567 - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            throw new IllegalStateException("Expected a boolean but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m14519() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 7) {
            this.f11569 = 0;
            int[] iArr = this.f11577;
            int i2 = this.f11567 - 1;
            iArr[i2] = iArr[i2] + 1;
            return;
        }
        throw new IllegalStateException("Expected null but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public double m14520() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 15) {
            this.f11569 = 0;
            int[] iArr = this.f11577;
            int i2 = this.f11567 - 1;
            iArr[i2] = iArr[i2] + 1;
            return (double) this.f11570;
        }
        if (i == 16) {
            this.f11568 = new String(this.f11574, this.f11572, this.f11571);
            this.f11572 += this.f11571;
        } else if (i == 8 || i == 9) {
            this.f11568 = m14500(i == 8 ? '\'' : '\"');
        } else if (i == 10) {
            this.f11568 = m14487();
        } else if (i != 11) {
            throw new IllegalStateException("Expected a double but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
        }
        this.f11569 = 11;
        double parseDouble = Double.parseDouble(this.f11568);
        if (this.f11575 || (!Double.isNaN(parseDouble) && !Double.isInfinite(parseDouble))) {
            this.f11568 = null;
            this.f11569 = 0;
            int[] iArr2 = this.f11577;
            int i3 = this.f11567 - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return parseDouble;
        }
        throw new ChinGentsai("JSON forbids NaN and infinities: " + parseDouble + " at line " + m14491() + " column " + m14492() + " path " + m14517());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m14521() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        return (i == 2 || i == 4) ? false : true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m14522() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 4) {
            this.f11567--;
            int[] iArr = this.f11577;
            int i2 = this.f11567 - 1;
            iArr[i2] = iArr[i2] + 1;
            this.f11569 = 0;
            return;
        }
        throw new IllegalStateException("Expected END_ARRAY but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m14523() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 2) {
            this.f11567--;
            this.f11576[this.f11567] = null;
            int[] iArr = this.f11577;
            int i2 = this.f11567 - 1;
            iArr[i2] = iArr[i2] + 1;
            this.f11569 = 0;
            return;
        }
        throw new IllegalStateException("Expected END_OBJECT but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m14524() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 1) {
            m14507(3);
            this.f11569 = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_OBJECT but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14525() throws IOException {
        int i = this.f11569;
        if (i == 0) {
            i = m14510();
        }
        if (i == 3) {
            m14507(1);
            this.f11577[this.f11567 - 1] = 0;
            this.f11569 = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_ARRAY but was " + m14511() + " at line " + m14491() + " column " + m14492() + " path " + m14517());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m14526(boolean z) {
        this.f11575 = z;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public final boolean m14527() {
        return this.f11575;
    }
}
