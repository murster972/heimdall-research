package com.p003if.p004do.p016int;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import org.apache.commons.lang3.StringUtils;

/* renamed from: com.if.do.int.GoroDaimon  reason: invalid package */
public class GoroDaimon implements Closeable, Flushable {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f11551 = ((String[]) f11552.clone());

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f11552 = new String[128];

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f11553;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f11554;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f11555;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f11556;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f11557;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f11558;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f11559 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    private int[] f11560 = new int[32];

    /* renamed from: 齉  reason: contains not printable characters */
    private final Writer f11561;

    static {
        for (int i = 0; i <= 31; i++) {
            f11552[i] = String.format("\\u%04x", new Object[]{Integer.valueOf(i)});
        }
        f11552[34] = "\\\"";
        f11552[92] = "\\\\";
        f11552[9] = "\\t";
        f11552[8] = "\\b";
        f11552[10] = "\\n";
        f11552[13] = "\\r";
        f11552[12] = "\\f";
        f11551[60] = "\\u003c";
        f11551[62] = "\\u003e";
        f11551[38] = "\\u0026";
        f11551[61] = "\\u003d";
        f11551[39] = "\\u0027";
    }

    public GoroDaimon(Writer writer) {
        m14469(6);
        this.f11554 = ":";
        this.f11558 = true;
        if (writer == null) {
            throw new NullPointerException("out == null");
        }
        this.f11561 = writer;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private void m14460() throws IOException {
        int r0 = m14466();
        if (r0 == 5) {
            this.f11561.write(44);
        } else if (r0 != 3) {
            throw new IllegalStateException("Nesting problem.");
        }
        m14462();
        m14464(4);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m14461() throws IOException {
        if (this.f11557 != null) {
            m14460();
            m14465(this.f11557);
            this.f11557 = null;
        }
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m14462() throws IOException {
        if (this.f11553 != null) {
            this.f11561.write(StringUtils.LF);
            int i = this.f11559;
            for (int i2 = 1; i2 < i; i2++) {
                this.f11561.write(this.f11553);
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m14463(boolean z) throws IOException {
        switch (m14466()) {
            case 1:
                m14464(2);
                m14462();
                return;
            case 2:
                this.f11561.append(',');
                m14462();
                return;
            case 4:
                this.f11561.append(this.f11554);
                m14464(5);
                return;
            case 6:
                break;
            case 7:
                if (!this.f11555) {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
                break;
            default:
                throw new IllegalStateException("Nesting problem.");
        }
        if (this.f11555 || z) {
            m14464(7);
            return;
        }
        throw new IllegalStateException("JSON must start with an array or an object.");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m14464(int i) {
        this.f11560[this.f11559 - 1] = i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0033  */
    /* renamed from: 麤  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m14465(java.lang.String r8) throws java.io.IOException {
        /*
            r7 = this;
            r2 = 0
            boolean r0 = r7.f11556
            if (r0 == 0) goto L_0x0027
            java.lang.String[] r0 = f11551
        L_0x0007:
            java.io.Writer r1 = r7.f11561
            java.lang.String r3 = "\""
            r1.write(r3)
            int r4 = r8.length()
            r3 = r2
            r1 = r2
        L_0x0015:
            if (r3 >= r4) goto L_0x004a
            char r2 = r8.charAt(r3)
            r5 = 128(0x80, float:1.794E-43)
            if (r2 >= r5) goto L_0x002a
            r2 = r0[r2]
            if (r2 != 0) goto L_0x0031
        L_0x0023:
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0015
        L_0x0027:
            java.lang.String[] r0 = f11552
            goto L_0x0007
        L_0x002a:
            r5 = 8232(0x2028, float:1.1535E-41)
            if (r2 != r5) goto L_0x0042
            java.lang.String r2 = "\\u2028"
        L_0x0031:
            if (r1 >= r3) goto L_0x003a
            java.io.Writer r5 = r7.f11561
            int r6 = r3 - r1
            r5.write(r8, r1, r6)
        L_0x003a:
            java.io.Writer r1 = r7.f11561
            r1.write(r2)
            int r1 = r3 + 1
            goto L_0x0023
        L_0x0042:
            r5 = 8233(0x2029, float:1.1537E-41)
            if (r2 != r5) goto L_0x0023
            java.lang.String r2 = "\\u2029"
            goto L_0x0031
        L_0x004a:
            if (r1 >= r4) goto L_0x0053
            java.io.Writer r0 = r7.f11561
            int r2 = r4 - r1
            r0.write(r8, r1, r2)
        L_0x0053:
            java.io.Writer r0 = r7.f11561
            java.lang.String r1 = "\""
            r0.write(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.p003if.p004do.p016int.GoroDaimon.m14465(java.lang.String):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m14466() {
        if (this.f11559 != 0) {
            return this.f11560[this.f11559 - 1];
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GoroDaimon m14467(int i, int i2, String str) throws IOException {
        int r0 = m14466();
        if (r0 != i2 && r0 != i) {
            throw new IllegalStateException("Nesting problem.");
        } else if (this.f11557 != null) {
            throw new IllegalStateException("Dangling name: " + this.f11557);
        } else {
            this.f11559--;
            if (r0 == i2) {
                m14462();
            }
            this.f11561.write(str);
            return this;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private GoroDaimon m14468(int i, String str) throws IOException {
        m14463(true);
        m14469(i);
        this.f11561.write(str);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14469(int i) {
        if (this.f11559 == this.f11560.length) {
            int[] iArr = new int[(this.f11559 * 2)];
            System.arraycopy(this.f11560, 0, iArr, 0, this.f11559);
            this.f11560 = iArr;
        }
        int[] iArr2 = this.f11560;
        int i2 = this.f11559;
        this.f11559 = i2 + 1;
        iArr2[i2] = i;
    }

    public void close() throws IOException {
        this.f11561.close();
        int i = this.f11559;
        if (i > 1 || (i == 1 && this.f11560[i - 1] != 7)) {
            throw new IOException("Incomplete document");
        }
        this.f11559 = 0;
    }

    public void flush() throws IOException {
        if (this.f11559 == 0) {
            throw new IllegalStateException("JsonWriter is closed.");
        }
        this.f11561.flush();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public GoroDaimon m14470() throws IOException {
        if (this.f11557 != null) {
            if (this.f11558) {
                m14461();
            } else {
                this.f11557 = null;
                return this;
            }
        }
        m14463(false);
        this.f11561.write("null");
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m14471() {
        return this.f11555;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final boolean m14472() {
        return this.f11556;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m14473() {
        return this.f11558;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public GoroDaimon m14474() throws IOException {
        return m14467(3, 5, "}");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m14475() throws IOException {
        m14461();
        return m14468(1, "[");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m14476(String str) throws IOException {
        if (str == null) {
            return m14470();
        }
        m14461();
        m14463(false);
        m14465(str);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final void m14477(boolean z) {
        this.f11555 = z;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public GoroDaimon m14478() throws IOException {
        m14461();
        return m14468(3, "{");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final void m14479(boolean z) {
        this.f11558 = z;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public GoroDaimon m14480() throws IOException {
        return m14467(1, 2, "]");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m14481(String str) {
        if (str.length() == 0) {
            this.f11553 = null;
            this.f11554 = ":";
            return;
        }
        this.f11553 = str;
        this.f11554 = ": ";
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m14482(boolean z) {
        this.f11556 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14483(long j) throws IOException {
        m14461();
        m14463(false);
        this.f11561.write(Long.toString(j));
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14484(Number number) throws IOException {
        if (number == null) {
            return m14470();
        }
        m14461();
        String obj = number.toString();
        if (this.f11555 || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            m14463(false);
            this.f11561.append(obj);
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + number);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14485(String str) throws IOException {
        if (str == null) {
            throw new NullPointerException("name == null");
        } else if (this.f11557 != null) {
            throw new IllegalStateException();
        } else if (this.f11559 == 0) {
            throw new IllegalStateException("JsonWriter is closed.");
        } else {
            this.f11557 = str;
            return this;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m14486(boolean z) throws IOException {
        m14461();
        m14463(false);
        this.f11561.write(z ? "true" : "false");
        return this;
    }
}
