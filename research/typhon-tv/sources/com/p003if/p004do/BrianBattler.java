package com.p003if.p004do;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.if.do.BrianBattler  reason: invalid package */
public final class BrianBattler extends SaishuKusanagi implements Iterable<SaishuKusanagi> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final List<SaishuKusanagi> f11323 = new ArrayList();

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof BrianBattler) && ((BrianBattler) obj).f11323.equals(this.f11323));
    }

    public int hashCode() {
        return this.f11323.hashCode();
    }

    public Iterator<SaishuKusanagi> iterator() {
        return this.f11323.iterator();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m14052() {
        if (this.f11323.size() == 1) {
            return this.f11323.get(0).m14134();
        }
        throw new IllegalStateException();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m14053() {
        if (this.f11323.size() == 1) {
            return this.f11323.get(0).m14135();
        }
        throw new IllegalStateException();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m14054() {
        if (this.f11323.size() == 1) {
            return this.f11323.get(0).m14143();
        }
        throw new IllegalStateException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Number m14055() {
        if (this.f11323.size() == 1) {
            return this.f11323.get(0).m14144();
        }
        throw new IllegalStateException();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public double m14056() {
        if (this.f11323.size() == 1) {
            return this.f11323.get(0).m14145();
        }
        throw new IllegalStateException();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m14057() {
        if (this.f11323.size() == 1) {
            return this.f11323.get(0).m14146();
        }
        throw new IllegalStateException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m14058() {
        return this.f11323.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14059(int i) {
        return this.f11323.get(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14060(SaishuKusanagi saishuKusanagi) {
        if (saishuKusanagi == null) {
            saishuKusanagi = Mature.f5653;
        }
        this.f11323.add(saishuKusanagi);
    }
}
