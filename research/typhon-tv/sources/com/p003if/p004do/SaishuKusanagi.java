package com.p003if.p004do;

import com.p003if.p004do.p014if.RugalBernstein;
import com.p003if.p004do.p016int.GoroDaimon;
import java.io.IOException;
import java.io.StringWriter;

/* renamed from: com.if.do.SaishuKusanagi  reason: invalid package */
public abstract class SaishuKusanagi {
    public String toString() {
        try {
            StringWriter stringWriter = new StringWriter();
            GoroDaimon goroDaimon = new GoroDaimon(stringWriter);
            goroDaimon.m14477(true);
            RugalBernstein.m14254(this, goroDaimon);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m14134() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m14135() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m14136() {
        return this instanceof BrianBattler;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public BrianBattler m14137() {
        if (m14136()) {
            return (BrianBattler) this;
        }
        throw new IllegalStateException("This is not a JSON Array.");
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public ShingoYabuki m14138() {
        if (m14141()) {
            return (ShingoYabuki) this;
        }
        throw new IllegalStateException("This is not a JSON Primitive.");
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Vice m14139() {
        if (m14140()) {
            return (Vice) this;
        }
        throw new IllegalStateException("Not a JSON Object: " + this);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m14140() {
        return this instanceof Vice;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m14141() {
        return this instanceof ShingoYabuki;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m14142() {
        return this instanceof Mature;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m14143() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Number m14144() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public double m14145() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m14146() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Mature m14147() {
        if (m14142()) {
            return (Mature) this;
        }
        throw new IllegalStateException("This is not a JSON Null.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public Boolean m14148() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }
}
