package com.p003if.p004do;

/* renamed from: com.if.do.ChizuruKagura  reason: invalid package */
public class ChizuruKagura extends RuntimeException {
    static final long serialVersionUID = -4086729973971783390L;

    public ChizuruKagura(String str) {
        super(str);
    }

    public ChizuruKagura(String str, Throwable th) {
        super(str, th);
    }

    public ChizuruKagura(Throwable th) {
        super(th);
    }
}
