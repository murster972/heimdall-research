package com.p003if.p004do;

import java.lang.reflect.Type;

/* renamed from: com.if.do.IoriYagami  reason: invalid package */
public interface IoriYagami<T> {
    T b(SaishuKusanagi saishuKusanagi, Type type, RugalBernstein rugalBernstein) throws ChizuruKagura;
}
