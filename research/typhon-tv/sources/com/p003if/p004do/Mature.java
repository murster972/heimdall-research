package com.p003if.p004do;

/* renamed from: com.if.do.Mature  reason: invalid package */
public final class Mature extends SaishuKusanagi {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Mature f5653 = new Mature();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof Mature);
    }

    public int hashCode() {
        return Mature.class.hashCode();
    }
}
