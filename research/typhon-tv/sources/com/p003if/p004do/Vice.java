package com.p003if.p004do;

import com.p003if.p004do.p014if.HeavyD;
import java.util.Map;
import java.util.Set;

/* renamed from: com.if.do.Vice  reason: invalid package */
public final class Vice extends SaishuKusanagi {

    /* renamed from: 龘  reason: contains not printable characters */
    private final HeavyD<String, SaishuKusanagi> f11374 = new HeavyD<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private SaishuKusanagi m14163(Object obj) {
        return obj == null ? Mature.f5653 : new ShingoYabuki(obj);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof Vice) && ((Vice) obj).f11374.equals(this.f11374));
    }

    public int hashCode() {
        return this.f11374.hashCode();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Vice m14164(String str) {
        return (Vice) this.f11374.get(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m14165(String str) {
        return this.f11374.containsKey(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public BrianBattler m14166(String str) {
        return (BrianBattler) this.f11374.get(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public SaishuKusanagi m14167(String str) {
        return this.f11374.get(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SaishuKusanagi m14168(String str) {
        return this.f11374.remove(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<Map.Entry<String, SaishuKusanagi>> m14169() {
        return this.f11374.entrySet();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14170(String str, SaishuKusanagi saishuKusanagi) {
        if (saishuKusanagi == null) {
            saishuKusanagi = Mature.f5653;
        }
        this.f11374.put(str, saishuKusanagi);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m14171(String str, String str2) {
        m14170(str, m14163((Object) str2));
    }
}
