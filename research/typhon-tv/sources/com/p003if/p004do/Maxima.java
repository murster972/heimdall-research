package com.p003if.p004do;

import com.p003if.p004do.p014if.p015do.ChoiBounge;
import com.p003if.p004do.p016int.GoroDaimon;
import com.p003if.p004do.p016int.KyoKusanagi;
import java.io.IOException;

/* renamed from: com.if.do.Maxima  reason: invalid package */
public abstract class Maxima<T> {
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract T m14130(KyoKusanagi kyoKusanagi) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    public final SaishuKusanagi m14131(T t) {
        try {
            ChoiBounge choiBounge = new ChoiBounge();
            m14132(choiBounge, t);
            return choiBounge.m14305();
        } catch (IOException e) {
            throw new LeonaHeidern((Throwable) e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m14132(GoroDaimon goroDaimon, T t) throws IOException;
}
