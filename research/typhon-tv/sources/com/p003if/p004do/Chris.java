package com.p003if.p004do;

/* renamed from: com.if.do.Chris  reason: invalid package */
public final class Chris extends ChizuruKagura {
    private static final long serialVersionUID = 1;

    public Chris(String str) {
        super(str);
    }

    public Chris(String str, Throwable th) {
        super(str, th);
    }

    public Chris(Throwable th) {
        super(th);
    }
}
