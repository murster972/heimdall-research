package com.p003if.p004do;

import com.p003if.p004do.p013for.KyoKusanagi;
import com.p003if.p004do.p014if.ChinGentsai;
import com.p003if.p004do.p014if.p015do.LeonaHeidern;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.if.do.HeavyD  reason: invalid package */
public final class HeavyD {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<Whip> f11342 = new ArrayList();

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f11343;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f11344;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f11345 = true;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f11346;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f11347;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f11348 = 2;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f11349 = 2;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f11350;

    /* renamed from: 连任  reason: contains not printable characters */
    private final List<Whip> f11351 = new ArrayList();

    /* renamed from: 靐  reason: contains not printable characters */
    private Orochi f11352 = Orochi.DEFAULT;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<Type, LuckyGlauber<?>> f11353 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private ChangKoehan f11354 = ChinGentsai.IDENTITY;

    /* renamed from: 龘  reason: contains not printable characters */
    private ChinGentsai f11355 = ChinGentsai.f11379;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f11356;

    /* renamed from: 龘  reason: contains not printable characters */
    private void m14116(String str, int i, int i2, List<Whip> list) {
        KyoKusanagi kyoKusanagi;
        if (str != null && !"".equals(str.trim())) {
            kyoKusanagi = new KyoKusanagi(str);
        } else if (i != 2 && i2 != 2) {
            kyoKusanagi = new KyoKusanagi(i, i2);
        } else {
            return;
        }
        list.add(KDash.m14121((KyoKusanagi<?>) KyoKusanagi.b(Date.class), (Object) kyoKusanagi));
        list.add(KDash.m14121((KyoKusanagi<?>) KyoKusanagi.b(Timestamp.class), (Object) kyoKusanagi));
        list.add(KDash.m14121((KyoKusanagi<?>) KyoKusanagi.b(java.sql.Date.class), (Object) kyoKusanagi));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ChoiBounge m14117() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f11351);
        Collections.reverse(arrayList);
        arrayList.addAll(this.f11342);
        m14116(this.f11344, this.f11348, this.f11349, arrayList);
        return new ChoiBounge(this.f11355, this.f11354, this.f11353, this.f11343, this.f11350, this.f11356, this.f11345, this.f11346, this.f11347, this.f11352, arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public HeavyD m14118(Type type, Object obj) {
        com.p003if.p004do.p014if.KyoKusanagi.m14250((obj instanceof Shermie) || (obj instanceof IoriYagami) || (obj instanceof LuckyGlauber) || (obj instanceof Maxima));
        if (obj instanceof LuckyGlauber) {
            this.f11353.put(type, (LuckyGlauber) obj);
        }
        if ((obj instanceof Shermie) || (obj instanceof IoriYagami)) {
            this.f11351.add(KDash.m14119(KyoKusanagi.a(type), obj));
        }
        if (obj instanceof Maxima) {
            this.f11351.add(LeonaHeidern.m14335(KyoKusanagi.a(type), (Maxima) obj));
        }
        return this;
    }
}
