package com.p003if.p004do.p013for;

import com.p003if.p004do.p014if.BenimaruNikaido;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* renamed from: com.if.do.for.KyoKusanagi  reason: invalid package */
public class KyoKusanagi<T> {
    final Class<? super T> a;
    final Type b;
    final int c;

    protected KyoKusanagi() {
        this.b = a(getClass());
        this.a = BenimaruNikaido.m14177(this.b);
        this.c = this.b.hashCode();
    }

    KyoKusanagi(Type type) {
        this.b = BenimaruNikaido.m14182((Type) com.p003if.p004do.p014if.KyoKusanagi.m14249(type));
        this.a = BenimaruNikaido.m14177(this.b);
        this.c = this.b.hashCode();
    }

    public static KyoKusanagi<?> a(Type type) {
        return new KyoKusanagi<>(type);
    }

    static Type a(Class<?> cls) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            return BenimaruNikaido.m14182(((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]);
        }
        throw new RuntimeException("Missing type parameter.");
    }

    public static <T> KyoKusanagi<T> b(Class<T> cls) {
        return new KyoKusanagi<>(cls);
    }

    public final Class<? super T> a() {
        return this.a;
    }

    public final Type b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof KyoKusanagi) && BenimaruNikaido.m14194(this.b, ((KyoKusanagi) obj).b);
    }

    public final int hashCode() {
        return this.c;
    }

    public final String toString() {
        return BenimaruNikaido.m14173(this.b);
    }
}
