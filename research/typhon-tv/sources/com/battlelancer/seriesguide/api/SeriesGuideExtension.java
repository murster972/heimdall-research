package com.battlelancer.seriesguide.api;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.JobIntentService;
import android.text.TextUtils;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public abstract class SeriesGuideExtension extends JobIntentService {
    public static final String EXTRA_FROM_SERIESGUIDE_SETTINGS = "com.battlelancer.seriesguide.api.extra.FROM_SERIESGUIDE_SETTINGS";
    private static final String PREF_LAST_ACTION = "action";
    private static final String PREF_PREFIX = "seriesguideextension_";
    private static final String PREF_SUBSCRIPTIONS = "subscriptions";
    private static final String TAG = "SeriesGuideExtension";
    private Action currentAction;
    private int currentActionType;
    private int currentVersion;
    private Handler handler = new Handler();
    private final String name;
    private SharedPreferences sharedPrefs;
    private Map<ComponentName, String> subscribers;

    public SeriesGuideExtension(String str) {
        this.name = str;
    }

    static void enqueue(Context context, Class cls, int i, Intent intent) {
        enqueueWork(context, cls, i, intent);
    }

    protected static SharedPreferences getSharedPreferences(Context context, String str) {
        return context.getSharedPreferences(PREF_PREFIX + str, 0);
    }

    private void handleEpisodeRequest(int i, Bundle bundle, int i2) {
        if (i > 0 && bundle != null) {
            this.currentActionType = 0;
            this.currentVersion = i2;
            onRequest(i, Episode.m25111(bundle));
        }
    }

    private void handleMovieRequest(int i, Bundle bundle, int i2) {
        if (i > 0 && bundle != null) {
            this.currentActionType = 1;
            this.currentVersion = i2;
            onRequest(i, Movie.m25133(bundle));
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"LogNotTimber"})
    public synchronized void handleSubscribe(ComponentName componentName, String str) {
        if (componentName == null) {
            Log.w(TAG, "No subscriber given.");
        } else {
            String str2 = this.subscribers.get(componentName);
            if (!TextUtils.isEmpty(str)) {
                if (!TextUtils.isEmpty(str2)) {
                    this.subscribers.remove(componentName);
                    handleSubscriberRemoved(componentName);
                }
                if (onAllowSubscription(componentName)) {
                    this.subscribers.put(componentName, str);
                    handleSubscriberAdded(componentName);
                }
            } else if (str2 != null) {
                this.subscribers.remove(componentName);
                handleSubscriberRemoved(componentName);
            }
            saveSubscriptions();
        }
    }

    private synchronized void handleSubscriberAdded(ComponentName componentName) {
        if (this.subscribers.size() == 1) {
            onEnabled();
        }
        onSubscriberAdded(componentName);
    }

    private synchronized void handleSubscriberRemoved(ComponentName componentName) {
        onSubscriberRemoved(componentName);
        if (this.subscribers.size() == 0) {
            onDisabled();
        }
    }

    @SuppressLint({"LogNotTimber"})
    private void loadLastAction() {
        String string = this.sharedPrefs.getString(PREF_LAST_ACTION, (String) null);
        if (string != null) {
            try {
                this.currentAction = Action.m25098((JSONObject) new JSONTokener(string).nextValue());
            } catch (JSONException e) {
                Log.e(TAG, "Couldn't deserialize current state, id=" + this.name, e);
            }
        } else {
            this.currentAction = null;
        }
    }

    private synchronized void loadSubscriptions() {
        this.subscribers = new HashMap();
        Set<String> stringSet = this.sharedPrefs.getStringSet(PREF_SUBSCRIPTIONS, (Set) null);
        if (stringSet != null) {
            for (String split : stringSet) {
                String[] split2 = split.split("\\|", 2);
                this.subscribers.put(ComponentName.unflattenFromString(split2[0]), split2[1]);
            }
        }
    }

    private synchronized void publishCurrentAction() {
        for (ComponentName publishCurrentAction : this.subscribers.keySet()) {
            publishCurrentAction(publishCurrentAction);
        }
    }

    @SuppressLint({"LogNotTimber"})
    private synchronized void publishCurrentAction(ComponentName componentName) {
        String str = this.subscribers.get(componentName);
        if (TextUtils.isEmpty(str)) {
            Log.w(TAG, "Not active, canceling update, id=" + this.name);
        } else {
            Intent putExtra = new Intent("com.battlelancer.seriesguide.api.action.PUBLISH_ACTION").setComponent(componentName).putExtra("com.battlelancer.seriesguide.api.extra.TOKEN", str).putExtra("com.battlelancer.seriesguide.api.extra.ACTION", this.currentAction != null ? this.currentAction.m25100() : null).putExtra("com.battlelancer.seriesguide.api.extra.ACTION_TYPE", this.currentActionType);
            if (this.currentVersion == 2) {
                try {
                    getPackageManager().getReceiverInfo(componentName, 0);
                    sendBroadcast(putExtra);
                } catch (PackageManager.NameNotFoundException e) {
                    unsubscribeAsync(componentName);
                }
            } else if (this.currentVersion == 1 && Build.VERSION.SDK_INT < 26) {
                try {
                    if (startService(putExtra) == null) {
                        unsubscribeAsync(componentName);
                    }
                } catch (SecurityException e2) {
                    Log.e(TAG, "Couldn't publish update, id=" + this.name, e2);
                }
            }
        }
        return;
    }

    @SuppressLint({"LogNotTimber"})
    private void saveLastAction() {
        try {
            this.sharedPrefs.edit().putString(PREF_LAST_ACTION, this.currentAction.m25099().toString()).apply();
        } catch (JSONException e) {
            Log.e(TAG, "Couldn't serialize current state, id=" + this.name, e);
        }
    }

    private synchronized void saveSubscriptions() {
        HashSet hashSet = new HashSet();
        for (ComponentName next : this.subscribers.keySet()) {
            hashSet.add(next.flattenToShortString() + "|" + this.subscribers.get(next));
        }
        this.sharedPrefs.edit().putStringSet(PREF_SUBSCRIPTIONS, hashSet).apply();
    }

    @SuppressLint({"LogNotTimber"})
    private void unsubscribeAsync(final ComponentName componentName) {
        Log.e(TAG, "Update not published because subscriber no longer exists, id=" + this.name);
        this.handler.post(new Runnable() {
            public void run() {
                SeriesGuideExtension.this.handleSubscribe(componentName, (String) null);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final Action getCurrentAction() {
        return this.currentAction;
    }

    /* access modifiers changed from: protected */
    public final SharedPreferences getSharedPreferences() {
        return getSharedPreferences(this, this.name);
    }

    /* access modifiers changed from: protected */
    public boolean onAllowSubscription(ComponentName componentName) {
        return true;
    }

    public void onCreate() {
        super.onCreate();
        this.sharedPrefs = getSharedPreferences();
        loadSubscriptions();
        loadLastAction();
    }

    /* access modifiers changed from: protected */
    public void onDisabled() {
    }

    /* access modifiers changed from: protected */
    public void onEnabled() {
    }

    /* access modifiers changed from: protected */
    public void onHandleWork(Intent intent) {
        String action = intent.getAction();
        if ("com.battlelancer.seriesguide.api.action.SUBSCRIBE".equals(action)) {
            handleSubscribe((ComponentName) intent.getParcelableExtra("com.battlelancer.seriesguide.api.extra.SUBSCRIBER_COMPONENT"), intent.getStringExtra("com.battlelancer.seriesguide.api.extra.TOKEN"));
        } else if ("com.battlelancer.seriesguide.api.action.UPDATE".equals(action) && intent.hasExtra("com.battlelancer.seriesguide.api.extra.ENTITY_IDENTIFIER")) {
            int intExtra = intent.getIntExtra("com.battlelancer.seriesguide.api.extra.VERSION", 1);
            if (intent.hasExtra("com.battlelancer.seriesguide.api.extra.EPISODE")) {
                handleEpisodeRequest(intent.getIntExtra("com.battlelancer.seriesguide.api.extra.ENTITY_IDENTIFIER", 0), intent.getBundleExtra("com.battlelancer.seriesguide.api.extra.EPISODE"), intExtra);
            } else if (intent.hasExtra("com.battlelancer.seriesguide.api.extra.MOVIE")) {
                handleMovieRequest(intent.getIntExtra("com.battlelancer.seriesguide.api.extra.ENTITY_IDENTIFIER", 0), intent.getBundleExtra("com.battlelancer.seriesguide.api.extra.MOVIE"), intExtra);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequest(int i, Episode episode) {
    }

    /* access modifiers changed from: protected */
    public void onRequest(int i, Movie movie) {
    }

    /* access modifiers changed from: protected */
    public void onSubscriberAdded(ComponentName componentName) {
    }

    /* access modifiers changed from: protected */
    public void onSubscriberRemoved(ComponentName componentName) {
    }

    /* access modifiers changed from: protected */
    public final void publishAction(Action action) {
        this.currentAction = action;
        publishCurrentAction();
        saveLastAction();
    }
}
