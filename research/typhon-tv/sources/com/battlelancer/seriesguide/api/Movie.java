package com.battlelancer.seriesguide.api;

import android.os.Bundle;
import java.util.Date;
import net.pubnative.library.request.PubnativeAsset;

public class Movie {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public Integer f19493;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Date f19494;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public String f19495;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public String f19496;

    public static class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Movie f19497 = new Movie();

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m25142(String str) {
            String unused = this.f19497.f19495 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m25143(Integer num) {
            Integer unused = this.f19497.f19493 = num;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m25144(String str) {
            String unused = this.f19497.f19496 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m25145(Date date) {
            Date unused = this.f19497.f19494 = date;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Movie m25146() {
            return this.f19497;
        }
    }

    private Movie() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Movie m25133(Bundle bundle) {
        long j = bundle.getLong("releaseDate", Long.MAX_VALUE);
        return new Builder().m25144(bundle.getString(PubnativeAsset.TITLE)).m25143(Integer.valueOf(bundle.getInt("tmdbid"))).m25142(bundle.getString("imdbid")).m25145(j == Long.MAX_VALUE ? null : new Date(j)).m25146();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Bundle m25137() {
        Bundle bundle = new Bundle();
        bundle.putString(PubnativeAsset.TITLE, this.f19496);
        bundle.putInt("tmdbid", this.f19493.intValue());
        bundle.putString("imdbid", this.f19495);
        if (this.f19494 != null) {
            bundle.putLong("releaseDate", this.f19494.getTime());
        }
        return bundle;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Integer m25138() {
        return this.f19493;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Date m25139() {
        return this.f19494;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m25140() {
        return this.f19495;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m25141() {
        return this.f19496;
    }
}
