package com.battlelancer.seriesguide.api;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import java.net.URISyntaxException;
import net.pubnative.library.request.PubnativeAsset;
import org.json.JSONException;
import org.json.JSONObject;

public class Action {

    /* renamed from: 靐  reason: contains not printable characters */
    private Intent f19476;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f19477;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f19478;

    public static class Builder {

        /* renamed from: 靐  reason: contains not printable characters */
        private Intent f19479;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f19480;

        /* renamed from: 龘  reason: contains not printable characters */
        private String f19481;

        public Builder(String str, int i) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("Title may not be null or empty.");
            } else if (i <= 0) {
                throw new IllegalArgumentException("Entity identifier may not be negative or zero.");
            } else {
                this.f19481 = str;
                this.f19480 = i;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m25101(Intent intent) {
            this.f19479 = intent;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Action m25102() {
            return new Action(this.f19481, this.f19479, this.f19480);
        }
    }

    private Action(String str, Intent intent, int i) {
        this.f19478 = str;
        this.f19476 = intent;
        this.f19477 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Action m25098(JSONObject jSONObject) throws JSONException {
        int optInt;
        String optString = jSONObject.optString(PubnativeAsset.TITLE);
        if (TextUtils.isEmpty(optString) || (optInt = jSONObject.optInt("entityIdentifier")) <= 0) {
            return null;
        }
        Builder builder = new Builder(optString, optInt);
        try {
            String optString2 = jSONObject.optString("viewIntent");
            if (!TextUtils.isEmpty(optString2)) {
                builder.m25101(Intent.parseUri(optString2, 1));
            }
        } catch (URISyntaxException e) {
        }
        return builder.m25102();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public JSONObject m25099() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(PubnativeAsset.TITLE, (Object) this.f19478);
        jSONObject.put("viewIntent", (Object) this.f19476 != null ? this.f19476.toUri(1) : null);
        jSONObject.put("entityIdentifier", this.f19477);
        return jSONObject;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Bundle m25100() {
        Bundle bundle = new Bundle();
        bundle.putString(PubnativeAsset.TITLE, this.f19478);
        bundle.putString("viewIntent", this.f19476 != null ? this.f19476.toUri(1) : null);
        bundle.putInt("entityIdentifier", this.f19477);
        return bundle;
    }
}
