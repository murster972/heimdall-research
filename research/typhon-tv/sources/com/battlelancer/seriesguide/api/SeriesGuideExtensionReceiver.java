package com.battlelancer.seriesguide.api;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public abstract class SeriesGuideExtensionReceiver extends BroadcastReceiver {
    public static final String ACTION_SERIESGUIDE_EXTENSION = "com.battlelancer.seriesguide.api.SeriesGuideExtension";

    /* access modifiers changed from: protected */
    public abstract Class<? extends SeriesGuideExtension> getExtensionClass();

    /* access modifiers changed from: protected */
    public abstract int getJobId();

    public void onReceive(Context context, Intent intent) {
        SeriesGuideExtension.enqueue(context, getExtensionClass(), getJobId(), intent);
    }
}
