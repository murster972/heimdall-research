package com.battlelancer.seriesguide.api;

import android.os.Bundle;
import net.pubnative.library.request.PubnativeAsset;

public class Episode {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f19482;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f19483;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public Integer f19484;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public String f19485;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public String f19486;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public Integer f19487;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public Integer f19488;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Integer f19489;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public Integer f19490;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public String f19491;

    public static class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Episode f19492 = new Episode();

        /* renamed from: 连任  reason: contains not printable characters */
        public Builder m25121(Integer num) {
            Integer unused = this.f19492.f19484 = num;
            return this;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public Builder m25122(String str) {
            String unused = this.f19492.f19486 = str;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m25123(Integer num) {
            Integer unused = this.f19492.f19490 = num;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m25124(String str) {
            String unused = this.f19492.f19482 = str;
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m25125(Integer num) {
            Integer unused = this.f19492.f19487 = num;
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Builder m25126(String str) {
            String unused = this.f19492.f19485 = str;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m25127(Integer num) {
            Integer unused = this.f19492.f19489 = num;
            return this;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m25128(String str) {
            String unused = this.f19492.f19483 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m25129(Integer num) {
            Integer unused = this.f19492.f19488 = num;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m25130(String str) {
            String unused = this.f19492.f19491 = str;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Episode m25131() {
            return this.f19492;
        }
    }

    private Episode() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Episode m25111(Bundle bundle) {
        return new Builder().m25130(bundle.getString(PubnativeAsset.TITLE)).m25129(Integer.valueOf(bundle.getInt("number"))).m25123(Integer.valueOf(bundle.getInt("numberAbsolute"))).m25127(Integer.valueOf(bundle.getInt("season"))).m25125(Integer.valueOf(bundle.getInt("tvdbid"))).m25124(bundle.getString("imdbid")).m25128(bundle.getString("showTitle")).m25121(Integer.valueOf(bundle.getInt("showTvdbId"))).m25126(bundle.getString("showImdbId")).m25122(bundle.getString("showFirstReleaseDate")).m25131();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m25114() {
        return this.f19486;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Bundle m25115() {
        Bundle bundle = new Bundle();
        bundle.putString(PubnativeAsset.TITLE, this.f19491);
        bundle.putInt("number", this.f19488.intValue());
        bundle.putInt("numberAbsolute", this.f19490.intValue());
        bundle.putInt("season", this.f19489.intValue());
        bundle.putInt("tvdbid", this.f19487.intValue());
        bundle.putString("imdbid", this.f19482);
        bundle.putString("showTitle", this.f19483);
        bundle.putInt("showTvdbId", this.f19484.intValue());
        bundle.putString("showImdbId", this.f19485);
        bundle.putString("showFirstReleaseDate", this.f19486);
        return bundle;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m25116() {
        return this.f19485;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Integer m25117() {
        return this.f19489;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Integer m25118() {
        return this.f19484;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m25119() {
        return this.f19483;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Integer m25120() {
        return this.f19488;
    }
}
