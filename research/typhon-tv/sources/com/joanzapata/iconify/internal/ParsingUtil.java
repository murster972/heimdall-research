package com.joanzapata.iconify.internal;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.ViewCompat;
import android.text.SpannableStringBuilder;
import android.util.TypedValue;
import android.widget.TextView;
import com.joanzapata.iconify.internal.HasOnViewAttachListener;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.List;

public final class ParsingUtil {
    private static final String ANDROID_PACKAGE_NAME = "android";

    private ParsingUtil() {
    }

    public static float dpToPx(Context context, float f) {
        return TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
    }

    public static int getColorFromResource(Context context, String str, String str2) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier(str2, TtmlNode.ATTR_TTS_COLOR, str);
        return identifier <= 0 ? MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT : resources.getColor(identifier);
    }

    public static float getPxFromDimen(Context context, String str, String str2) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier(str2, "dimen", str);
        if (identifier <= 0) {
            return -1.0f;
        }
        return resources.getDimension(identifier);
    }

    private static boolean hasAnimatedSpans(SpannableStringBuilder spannableStringBuilder) {
        for (CustomTypefaceSpan isAnimated : (CustomTypefaceSpan[]) spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), CustomTypefaceSpan.class)) {
            if (isAnimated.isAnimated()) {
                return true;
            }
        }
        return false;
    }

    public static CharSequence parse(Context context, List<IconFontDescriptorWrapper> list, CharSequence charSequence, final TextView textView) {
        Context applicationContext = context.getApplicationContext();
        if (charSequence == null) {
            return charSequence;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        recursivePrepareSpannableIndexes(applicationContext, charSequence.toString(), spannableStringBuilder, list, 0);
        if (hasAnimatedSpans(spannableStringBuilder)) {
            if (textView == null) {
                throw new IllegalArgumentException("You can't use \"spin\" without providing the target TextView.");
            } else if (!(textView instanceof HasOnViewAttachListener)) {
                throw new IllegalArgumentException(textView.getClass().getSimpleName() + " does not implement " + "HasOnViewAttachListener. Please use IconTextView, IconButton or IconToggleButton.");
            } else {
                ((HasOnViewAttachListener) textView).setOnViewAttachListener(new HasOnViewAttachListener.OnViewAttachListener() {
                    boolean isAttached = false;

                    public void onAttach() {
                        this.isAttached = true;
                        ViewCompat.postOnAnimation(textView, new Runnable() {
                            public void run() {
                                if (AnonymousClass1.this.isAttached) {
                                    textView.invalidate();
                                    ViewCompat.postOnAnimation(textView, this);
                                }
                            }
                        });
                    }

                    public void onDetach() {
                        this.isAttached = false;
                    }
                });
            }
        } else if (textView instanceof HasOnViewAttachListener) {
            ((HasOnViewAttachListener) textView).setOnViewAttachListener((HasOnViewAttachListener.OnViewAttachListener) null);
        }
        return spannableStringBuilder;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0043, code lost:
        r16 = r26.get(r15);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void recursivePrepareSpannableIndexes(android.content.Context r23, java.lang.String r24, android.text.SpannableStringBuilder r25, java.util.List<com.joanzapata.iconify.internal.IconFontDescriptorWrapper> r26, int r27) {
        /*
            java.lang.String r19 = r25.toString()
            java.lang.String r5 = "{"
            r0 = r19
            r1 = r27
            int r18 = r0.indexOf(r5, r1)
            r5 = -1
            r0 = r18
            if (r0 != r5) goto L_0x0015
        L_0x0014:
            return
        L_0x0015:
            java.lang.String r5 = "}"
            r0 = r19
            r1 = r18
            int r5 = r0.indexOf(r5, r1)
            int r13 = r5 + 1
            r5 = -1
            if (r13 == r5) goto L_0x0014
            int r5 = r18 + 1
            int r7 = r13 + -1
            r0 = r19
            java.lang.String r14 = r0.substring(r5, r7)
            java.lang.String r5 = " "
            java.lang.String[] r21 = r14.split(r5)
            r5 = 0
            r17 = r21[r5]
            r16 = 0
            r6 = 0
            r15 = 0
        L_0x003d:
            int r5 = r26.size()
            if (r15 >= r5) goto L_0x0051
            r0 = r26
            java.lang.Object r16 = r0.get(r15)
            com.joanzapata.iconify.internal.IconFontDescriptorWrapper r16 = (com.joanzapata.iconify.internal.IconFontDescriptorWrapper) r16
            com.joanzapata.iconify.Icon r6 = r16.getIcon(r17)
            if (r6 == 0) goto L_0x005f
        L_0x0051:
            if (r6 != 0) goto L_0x0062
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r26
            recursivePrepareSpannableIndexes(r0, r1, r2, r3, r13)
            goto L_0x0014
        L_0x005f:
            int r15 = r15 + 1
            goto L_0x003d
        L_0x0062:
            r8 = -1082130432(0xffffffffbf800000, float:-1.0)
            r10 = 2147483647(0x7fffffff, float:NaN)
            r9 = -1082130432(0xffffffffbf800000, float:-1.0)
            r11 = 0
            r12 = 0
            r15 = 1
        L_0x006c:
            r0 = r21
            int r5 = r0.length
            if (r15 >= r5) goto L_0x02c8
            r20 = r21[r15]
            java.lang.String r5 = "spin"
            r0 = r20
            boolean r5 = r0.equalsIgnoreCase(r5)
            if (r5 == 0) goto L_0x0082
            r11 = 1
        L_0x007f:
            int r15 = r15 + 1
            goto L_0x006c
        L_0x0082:
            java.lang.String r5 = "baseline"
            r0 = r20
            boolean r5 = r0.equalsIgnoreCase(r5)
            if (r5 == 0) goto L_0x008f
            r12 = 1
            goto L_0x007f
        L_0x008f:
            java.lang.String r5 = "([0-9]*(\\.[0-9]*)?)dp"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x00b6
            r5 = 0
            int r7 = r20.length()
            int r7 = r7 + -2
            r0 = r20
            java.lang.String r5 = r0.substring(r5, r7)
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            float r5 = r5.floatValue()
            r0 = r23
            float r8 = dpToPx(r0, r5)
            goto L_0x007f
        L_0x00b6:
            java.lang.String r5 = "([0-9]*(\\.[0-9]*)?)sp"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x00dd
            r5 = 0
            int r7 = r20.length()
            int r7 = r7 + -2
            r0 = r20
            java.lang.String r5 = r0.substring(r5, r7)
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            float r5 = r5.floatValue()
            r0 = r23
            float r8 = spToPx(r0, r5)
            goto L_0x007f
        L_0x00dd:
            java.lang.String r5 = "([0-9]*)px"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x00ff
            r5 = 0
            int r7 = r20.length()
            int r7 = r7 + -2
            r0 = r20
            java.lang.String r5 = r0.substring(r5, r7)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            int r5 = r5.intValue()
            float r8 = (float) r5
            goto L_0x007f
        L_0x00ff:
            java.lang.String r5 = "@dimen/(.*)"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x0156
            java.lang.String r5 = r23.getPackageName()
            r7 = 7
            r0 = r20
            java.lang.String r7 = r0.substring(r7)
            r0 = r23
            float r8 = getPxFromDimen(r0, r5, r7)
            r5 = 0
            int r5 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x007f
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r22 = "Unknown resource "
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r20
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = " in \""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r24
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = "\""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r7 = r7.toString()
            r5.<init>(r7)
            throw r5
        L_0x0156:
            java.lang.String r5 = "@android:dimen/(.*)"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x01ad
            java.lang.String r5 = "android"
            r7 = 15
            r0 = r20
            java.lang.String r7 = r0.substring(r7)
            r0 = r23
            float r8 = getPxFromDimen(r0, r5, r7)
            r5 = 0
            int r5 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x007f
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r22 = "Unknown resource "
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r20
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = " in \""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r24
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = "\""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r7 = r7.toString()
            r5.<init>(r7)
            throw r5
        L_0x01ad:
            java.lang.String r5 = "([0-9]*(\\.[0-9]*)?)%"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x01d3
            r5 = 0
            int r7 = r20.length()
            int r7 = r7 + -1
            r0 = r20
            java.lang.String r5 = r0.substring(r5, r7)
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            float r5 = r5.floatValue()
            r7 = 1120403456(0x42c80000, float:100.0)
            float r9 = r5 / r7
            goto L_0x007f
        L_0x01d3:
            java.lang.String r5 = "#([0-9A-Fa-f]{6}|[0-9A-Fa-f]{8})"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x01e4
            int r10 = android.graphics.Color.parseColor(r20)
            goto L_0x007f
        L_0x01e4:
            java.lang.String r5 = "@color/(.*)"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x023b
            java.lang.String r5 = r23.getPackageName()
            r7 = 7
            r0 = r20
            java.lang.String r7 = r0.substring(r7)
            r0 = r23
            int r10 = getColorFromResource(r0, r5, r7)
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r10 != r5) goto L_0x007f
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r22 = "Unknown resource "
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r20
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = " in \""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r24
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = "\""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r7 = r7.toString()
            r5.<init>(r7)
            throw r5
        L_0x023b:
            java.lang.String r5 = "@android:color/(.*)"
            r0 = r20
            boolean r5 = r0.matches(r5)
            if (r5 == 0) goto L_0x0292
            java.lang.String r5 = "android"
            r7 = 15
            r0 = r20
            java.lang.String r7 = r0.substring(r7)
            r0 = r23
            int r10 = getColorFromResource(r0, r5, r7)
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r10 != r5) goto L_0x007f
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r22 = "Unknown resource "
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r20
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = " in \""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r24
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = "\""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r7 = r7.toString()
            r5.<init>(r7)
            throw r5
        L_0x0292:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r22 = "Unknown expression "
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r20
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = " in \""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            r0 = r24
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r22 = "\""
            r0 = r22
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r7 = r7.toString()
            r5.<init>(r7)
            throw r5
        L_0x02c8:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = ""
            java.lang.StringBuilder r5 = r5.append(r7)
            char r7 = r6.character()
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r5 = r5.toString()
            r0 = r25
            r1 = r18
            android.text.SpannableStringBuilder r25 = r0.replace(r1, r13, r5)
            com.joanzapata.iconify.internal.CustomTypefaceSpan r5 = new com.joanzapata.iconify.internal.CustomTypefaceSpan
            r0 = r16
            r1 = r23
            android.graphics.Typeface r7 = r0.getTypeface(r1)
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            int r7 = r18 + 1
            r22 = 17
            r0 = r25
            r1 = r18
            r2 = r22
            r0.setSpan(r5, r1, r7, r2)
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r26
            r4 = r18
            recursivePrepareSpannableIndexes(r0, r1, r2, r3, r4)
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.joanzapata.iconify.internal.ParsingUtil.recursivePrepareSpannableIndexes(android.content.Context, java.lang.String, android.text.SpannableStringBuilder, java.util.List, int):void");
    }

    public static float spToPx(Context context, float f) {
        return TypedValue.applyDimension(2, f, context.getResources().getDisplayMetrics());
    }
}
