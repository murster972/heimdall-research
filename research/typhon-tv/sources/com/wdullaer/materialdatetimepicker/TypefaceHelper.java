package com.wdullaer.materialdatetimepicker;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.util.SimpleArrayMap;

public class TypefaceHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final SimpleArrayMap<String, Typeface> f14085 = new SimpleArrayMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static Typeface m17950(Context context, String str) {
        synchronized (f14085) {
            if (!f14085.containsKey(str)) {
                Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), String.format("fonts/%s.ttf", new Object[]{str}));
                f14085.put(str, createFromAsset);
                return createFromAsset;
            }
            Typeface typeface = f14085.get(str);
            return typeface;
        }
    }
}
