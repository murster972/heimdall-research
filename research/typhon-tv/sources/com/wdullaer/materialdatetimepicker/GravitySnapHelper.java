package com.wdullaer.materialdatetimepicker;

import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class GravitySnapHelper extends LinearSnapHelper {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean f14071;

    /* renamed from: ʼ  reason: contains not printable characters */
    private RecyclerView.OnScrollListener f14072 = new RecyclerView.OnScrollListener() {
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
            if (i == 2) {
                boolean unused = GravitySnapHelper.this.f14071 = false;
            }
            if (i == 0 && GravitySnapHelper.this.f14073 != null) {
                int r0 = GravitySnapHelper.this.m17934(recyclerView);
                if (r0 != -1) {
                    GravitySnapHelper.this.f14073.m17941(r0);
                }
                boolean unused2 = GravitySnapHelper.this.f14071 = false;
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public SnapListener f14073;

    /* renamed from: 靐  reason: contains not printable characters */
    private OrientationHelper f14074;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f14075;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f14076;

    /* renamed from: 龘  reason: contains not printable characters */
    private OrientationHelper f14077;

    public interface SnapListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m17941(int i);
    }

    public GravitySnapHelper(int i, SnapListener snapListener) {
        if (i == 8388611 || i == 8388613 || i == 80 || i == 48) {
            this.f14076 = i;
            this.f14073 = snapListener;
            return;
        }
        throw new IllegalArgumentException("Invalid gravity value. Use START | END | BOTTOM | TOP constants");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m17931(View view, OrientationHelper orientationHelper, boolean z) {
        return (!this.f14075 || z) ? orientationHelper.getDecoratedEnd(view) - orientationHelper.getEndAfterPadding() : m17935(view, orientationHelper, true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private OrientationHelper m17932(RecyclerView.LayoutManager layoutManager) {
        if (this.f14074 == null) {
            this.f14074 = OrientationHelper.createHorizontalHelper(layoutManager);
        }
        return this.f14074;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private View m17933(RecyclerView.LayoutManager layoutManager, OrientationHelper orientationHelper) {
        if (!(layoutManager instanceof LinearLayoutManager)) {
            return null;
        }
        int findLastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        if (findLastVisibleItemPosition == -1) {
            return null;
        }
        View findViewByPosition = layoutManager.findViewByPosition(findLastVisibleItemPosition);
        float decoratedEnd = this.f14075 ? ((float) orientationHelper.getDecoratedEnd(findViewByPosition)) / ((float) orientationHelper.getDecoratedMeasurement(findViewByPosition)) : ((float) (orientationHelper.getTotalSpace() - orientationHelper.getDecoratedStart(findViewByPosition))) / ((float) orientationHelper.getDecoratedMeasurement(findViewByPosition));
        boolean z = ((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition() == 0;
        if (decoratedEnd > 0.5f && !z) {
            return findViewByPosition;
        }
        if (z) {
            return null;
        }
        return layoutManager.findViewByPosition(findLastVisibleItemPosition - 1);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m17934(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            if (this.f14076 == 8388611 || this.f14076 == 48) {
                return ((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition();
            }
            if (this.f14076 == 8388613 || this.f14076 == 80) {
                return ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition();
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m17935(View view, OrientationHelper orientationHelper, boolean z) {
        return (!this.f14075 || z) ? orientationHelper.getDecoratedStart(view) - orientationHelper.getStartAfterPadding() : m17931(view, orientationHelper, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private OrientationHelper m17937(RecyclerView.LayoutManager layoutManager) {
        if (this.f14077 == null) {
            this.f14077 = OrientationHelper.createVerticalHelper(layoutManager);
        }
        return this.f14077;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private View m17938(RecyclerView.LayoutManager layoutManager, OrientationHelper orientationHelper) {
        if (!(layoutManager instanceof LinearLayoutManager)) {
            return null;
        }
        int findFirstVisibleItemPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        if (findFirstVisibleItemPosition == -1) {
            return null;
        }
        View findViewByPosition = layoutManager.findViewByPosition(findFirstVisibleItemPosition);
        float totalSpace = this.f14075 ? ((float) (orientationHelper.getTotalSpace() - orientationHelper.getDecoratedStart(findViewByPosition))) / ((float) orientationHelper.getDecoratedMeasurement(findViewByPosition)) : ((float) orientationHelper.getDecoratedEnd(findViewByPosition)) / ((float) orientationHelper.getDecoratedMeasurement(findViewByPosition));
        boolean z = ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition() == layoutManager.getItemCount() + -1;
        if (totalSpace > 0.5f && !z) {
            return findViewByPosition;
        }
        if (z) {
            return null;
        }
        return layoutManager.findViewByPosition(findFirstVisibleItemPosition + 1);
    }

    public void attachToRecyclerView(RecyclerView recyclerView) throws IllegalStateException {
        boolean z = true;
        if (recyclerView != null) {
            if ((this.f14076 == 8388611 || this.f14076 == 8388613) && Build.VERSION.SDK_INT >= 17) {
                if (recyclerView.getContext().getResources().getConfiguration().getLayoutDirection() != 1) {
                    z = false;
                }
                this.f14075 = z;
            }
            if (this.f14073 != null) {
                recyclerView.addOnScrollListener(this.f14072);
            }
        }
        super.attachToRecyclerView(recyclerView);
    }

    public int[] calculateDistanceToFinalSnap(RecyclerView.LayoutManager layoutManager, View view) {
        int[] iArr = new int[2];
        if (!layoutManager.canScrollHorizontally()) {
            iArr[0] = 0;
        } else if (this.f14076 == 8388611) {
            iArr[0] = m17935(view, m17932(layoutManager), false);
        } else {
            iArr[0] = m17931(view, m17932(layoutManager), false);
        }
        if (!layoutManager.canScrollVertically()) {
            iArr[1] = 0;
        } else if (this.f14076 == 48) {
            iArr[1] = m17935(view, m17937(layoutManager), false);
        } else {
            iArr[1] = m17931(view, m17937(layoutManager), false);
        }
        return iArr;
    }

    public View findSnapView(RecyclerView.LayoutManager layoutManager) {
        View view = null;
        if (layoutManager instanceof LinearLayoutManager) {
            switch (this.f14076) {
                case 48:
                    view = m17938(layoutManager, m17937(layoutManager));
                    break;
                case 80:
                    view = m17933(layoutManager, m17937(layoutManager));
                    break;
                case GravityCompat.START:
                    view = m17938(layoutManager, m17932(layoutManager));
                    break;
                case GravityCompat.END:
                    view = m17933(layoutManager, m17932(layoutManager));
                    break;
            }
        }
        this.f14071 = view != null;
        return view;
    }
}
