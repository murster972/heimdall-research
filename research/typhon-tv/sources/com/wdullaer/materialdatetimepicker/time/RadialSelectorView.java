package com.wdullaer.materialdatetimepicker.time;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

public class RadialSelectorView extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f14304;

    /* renamed from: ʼ  reason: contains not printable characters */
    private float f14305;

    /* renamed from: ʽ  reason: contains not printable characters */
    private float f14306;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f14307;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f14308;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f14309;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f14310;

    /* renamed from: ˉ  reason: contains not printable characters */
    private InvalidateUpdateListener f14311;

    /* renamed from: ˊ  reason: contains not printable characters */
    private float f14312;

    /* renamed from: ˋ  reason: contains not printable characters */
    private float f14313;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f14314;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f14315;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f14316;

    /* renamed from: י  reason: contains not printable characters */
    private double f14317;

    /* renamed from: ـ  reason: contains not printable characters */
    private boolean f14318;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f14319;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean f14320;

    /* renamed from: 连任  reason: contains not printable characters */
    private float f14321;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f14322 = false;

    /* renamed from: 麤  reason: contains not printable characters */
    private float f14323;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f14324;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f14325 = new Paint();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f14326;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f14327;

    private class InvalidateUpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private InvalidateUpdateListener() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            RadialSelectorView.this.invalidate();
        }
    }

    public RadialSelectorView(Context context) {
        super(context);
    }

    public ObjectAnimator getDisappearAnimator() {
        if (!this.f14322 || !this.f14324) {
            Log.e("RadialSelectorView", "RadialSelectorView was not ready for animation.");
            return null;
        }
        ObjectAnimator duration = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofKeyframe("animationRadiusMultiplier", new Keyframe[]{Keyframe.ofFloat(0.0f, 1.0f), Keyframe.ofFloat(0.2f, this.f14312), Keyframe.ofFloat(1.0f, this.f14313)}), PropertyValuesHolder.ofKeyframe("alpha", new Keyframe[]{Keyframe.ofFloat(0.0f, 1.0f), Keyframe.ofFloat(1.0f, 0.0f)})}).setDuration((long) 500);
        duration.addUpdateListener(this.f14311);
        return duration;
    }

    public ObjectAnimator getReappearAnimator() {
        if (!this.f14322 || !this.f14324) {
            Log.e("RadialSelectorView", "RadialSelectorView was not ready for animation.");
            return null;
        }
        int i = (int) (((float) 500) * (1.0f + 0.25f));
        float f = (((float) 500) * 0.25f) / ((float) i);
        ObjectAnimator duration = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofKeyframe("animationRadiusMultiplier", new Keyframe[]{Keyframe.ofFloat(0.0f, this.f14313), Keyframe.ofFloat(f, this.f14313), Keyframe.ofFloat(1.0f - ((1.0f - f) * 0.2f), this.f14312), Keyframe.ofFloat(1.0f, 1.0f)}), PropertyValuesHolder.ofKeyframe("alpha", new Keyframe[]{Keyframe.ofFloat(0.0f, 0.0f), Keyframe.ofFloat(f, 0.0f), Keyframe.ofFloat(1.0f, 1.0f)})}).setDuration((long) i);
        duration.addUpdateListener(this.f14311);
        return duration;
    }

    public boolean hasOverlappingRendering() {
        return false;
    }

    public void onDraw(Canvas canvas) {
        boolean z = true;
        if (getWidth() != 0 && this.f14322) {
            if (!this.f14324) {
                this.f14308 = getWidth() / 2;
                this.f14326 = getHeight() / 2;
                this.f14327 = (int) (((float) Math.min(this.f14308, this.f14326)) * this.f14323);
                if (!this.f14320) {
                    this.f14326 = (int) (((double) this.f14326) - (((double) ((int) (((float) this.f14327) * this.f14321))) * 0.75d));
                }
                this.f14309 = (int) (((float) this.f14327) * this.f14316);
                this.f14324 = true;
            }
            this.f14314 = (int) (((float) this.f14327) * this.f14306 * this.f14319);
            int sin = this.f14308 + ((int) (((double) this.f14314) * Math.sin(this.f14317)));
            int cos = this.f14326 - ((int) (((double) this.f14314) * Math.cos(this.f14317)));
            this.f14325.setAlpha(this.f14307);
            canvas.drawCircle((float) sin, (float) cos, (float) this.f14309, this.f14325);
            boolean z2 = this.f14318;
            if (this.f14315 % 30 == 0) {
                z = false;
            }
            if (z || z2) {
                this.f14325.setAlpha(255);
                canvas.drawCircle((float) sin, (float) cos, (float) ((this.f14309 * 2) / 7), this.f14325);
            } else {
                int i = this.f14314 - this.f14309;
                sin = this.f14308 + ((int) (((double) i) * Math.sin(this.f14317)));
                cos = this.f14326 - ((int) (((double) i) * Math.cos(this.f14317)));
            }
            this.f14325.setAlpha(255);
            this.f14325.setStrokeWidth(3.0f);
            canvas.drawLine((float) this.f14308, (float) this.f14326, (float) sin, (float) cos, this.f14325);
        }
    }

    public void setAnimationRadiusMultiplier(float f) {
        this.f14319 = f;
    }

    public void setSelection(int i, boolean z, boolean z2) {
        this.f14315 = i;
        this.f14317 = (((double) i) * 3.141592653589793d) / 180.0d;
        this.f14318 = z2;
        if (!this.f14310) {
            return;
        }
        if (z) {
            this.f14306 = this.f14304;
        } else {
            this.f14306 = this.f14305;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m18131(float f, float f2, boolean z, Boolean[] boolArr) {
        if (!this.f14324) {
            return -1;
        }
        double sqrt = Math.sqrt((double) (((f2 - ((float) this.f14326)) * (f2 - ((float) this.f14326))) + ((f - ((float) this.f14308)) * (f - ((float) this.f14308)))));
        if (this.f14310) {
            if (z) {
                boolArr[0] = Boolean.valueOf(((int) Math.abs(sqrt - ((double) ((int) (((float) this.f14327) * this.f14304))))) <= ((int) Math.abs(sqrt - ((double) ((int) (((float) this.f14327) * this.f14305))))));
            } else {
                int i = ((int) (((float) this.f14327) * this.f14304)) - this.f14309;
                int i2 = ((int) (((float) this.f14327) * this.f14305)) + this.f14309;
                int i3 = (int) (((float) this.f14327) * ((this.f14305 + this.f14304) / 2.0f));
                if (sqrt >= ((double) i) && sqrt <= ((double) i3)) {
                    boolArr[0] = true;
                } else if (sqrt > ((double) i2) || sqrt < ((double) i3)) {
                    return -1;
                } else {
                    boolArr[0] = false;
                }
            }
        } else if (!z && ((int) Math.abs(sqrt - ((double) this.f14314))) > ((int) (((float) this.f14327) * (1.0f - this.f14306)))) {
            return -1;
        }
        int asin = (int) ((180.0d * Math.asin(((double) Math.abs(f2 - ((float) this.f14326))) / sqrt)) / 3.141592653589793d);
        boolean z2 = f > ((float) this.f14308);
        boolean z3 = f2 < ((float) this.f14326);
        return (!z2 || !z3) ? (!z2 || z3) ? (z2 || z3) ? (z2 || !z3) ? asin : asin + 270 : 270 - asin : asin + 90 : 90 - asin;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18132(Context context, TimePickerController timePickerController, boolean z, boolean z2, int i, boolean z3) {
        int i2 = -1;
        if (this.f14322) {
            Log.e("RadialSelectorView", "This RadialSelectorView may only be initialized once.");
            return;
        }
        Resources resources = context.getResources();
        this.f14325.setColor(timePickerController.m18144());
        this.f14325.setAntiAlias(true);
        if (timePickerController.m18143()) {
        }
        this.f14307 = 255;
        this.f14320 = timePickerController.m18145();
        if (this.f14320 || timePickerController.m18142() != TimePickerDialog.Version.VERSION_1) {
            this.f14323 = Float.parseFloat(resources.getString(R.string.mdtp_circle_radius_multiplier_24HourMode));
        } else {
            this.f14323 = Float.parseFloat(resources.getString(R.string.mdtp_circle_radius_multiplier));
            this.f14321 = Float.parseFloat(resources.getString(R.string.mdtp_ampm_circle_radius_multiplier));
        }
        this.f14310 = z;
        if (z) {
            this.f14304 = Float.parseFloat(resources.getString(R.string.mdtp_numbers_radius_multiplier_inner));
            this.f14305 = Float.parseFloat(resources.getString(R.string.mdtp_numbers_radius_multiplier_outer));
        } else {
            this.f14306 = Float.parseFloat(resources.getString(R.string.mdtp_numbers_radius_multiplier_normal));
        }
        this.f14316 = Float.parseFloat(resources.getString(R.string.mdtp_selection_radius_multiplier));
        this.f14319 = 1.0f;
        this.f14312 = (((float) (z2 ? -1 : 1)) * 0.05f) + 1.0f;
        if (z2) {
            i2 = 1;
        }
        this.f14313 = (0.3f * ((float) i2)) + 1.0f;
        this.f14311 = new InvalidateUpdateListener();
        setSelection(i, z3, false);
        this.f14322 = true;
    }
}
