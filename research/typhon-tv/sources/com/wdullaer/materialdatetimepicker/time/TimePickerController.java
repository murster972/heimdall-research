package com.wdullaer.materialdatetimepicker.time;

import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

interface TimePickerController {
    /* renamed from: ʻ  reason: contains not printable characters */
    void m18139();

    /* renamed from: ʼ  reason: contains not printable characters */
    boolean m18140();

    /* renamed from: ʽ  reason: contains not printable characters */
    boolean m18141();

    /* renamed from: 连任  reason: contains not printable characters */
    TimePickerDialog.Version m18142();

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m18143();

    /* renamed from: 麤  reason: contains not printable characters */
    int m18144();

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m18145();

    /* renamed from: 龘  reason: contains not printable characters */
    Timepoint m18146(Timepoint timepoint, Timepoint.TYPE type);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m18147(Timepoint timepoint, int i);
}
