package com.wdullaer.materialdatetimepicker.time;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

public class CircleView extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    private float f14253;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f14254 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f14255;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f14256;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f14257;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f14258;

    /* renamed from: 连任  reason: contains not printable characters */
    private float f14259;

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f14260;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f14261;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f14262;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f14263 = new Paint();

    public CircleView(Context context) {
        super(context);
    }

    public void onDraw(Canvas canvas) {
        if (getWidth() != 0 && this.f14254) {
            if (!this.f14255) {
                this.f14256 = getWidth() / 2;
                this.f14257 = getHeight() / 2;
                this.f14258 = (int) (((float) Math.min(this.f14256, this.f14257)) * this.f14259);
                if (!this.f14260) {
                    this.f14257 = (int) (((double) this.f14257) - (((double) ((int) (((float) this.f14258) * this.f14253))) * 0.75d));
                }
                this.f14255 = true;
            }
            this.f14263.setColor(this.f14262);
            canvas.drawCircle((float) this.f14256, (float) this.f14257, (float) this.f14258, this.f14263);
            this.f14263.setColor(this.f14261);
            canvas.drawCircle((float) this.f14256, (float) this.f14257, 8.0f, this.f14263);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18090(Context context, TimePickerController timePickerController) {
        if (this.f14254) {
            Log.e("CircleView", "CircleView may only be initialized once.");
            return;
        }
        Resources resources = context.getResources();
        this.f14262 = ContextCompat.getColor(context, timePickerController.m18143() ? R.color.mdtp_circle_background_dark_theme : R.color.mdtp_circle_color);
        this.f14261 = timePickerController.m18144();
        this.f14263.setAntiAlias(true);
        this.f14260 = timePickerController.m18145();
        if (this.f14260 || timePickerController.m18142() != TimePickerDialog.Version.VERSION_1) {
            this.f14259 = Float.parseFloat(resources.getString(R.string.mdtp_circle_radius_multiplier_24HourMode));
        } else {
            this.f14259 = Float.parseFloat(resources.getString(R.string.mdtp_circle_radius_multiplier));
            this.f14253 = Float.parseFloat(resources.getString(R.string.mdtp_ampm_circle_radius_multiplier));
        }
        this.f14254 = true;
    }
}
