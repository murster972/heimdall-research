package com.wdullaer.materialdatetimepicker.time;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.Utils;
import java.text.DateFormatSymbols;
import java.util.Locale;

public class AmPmCirclesView extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f14231;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f14232;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f14233;

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f14234;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f14235;

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f14236;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String f14237;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f14238;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f14239;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f14240;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f14241;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f14242;

    /* renamed from: ˑ  reason: contains not printable characters */
    private float f14243;

    /* renamed from: ٴ  reason: contains not printable characters */
    private float f14244;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f14245;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f14246;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f14247;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f14248;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f14249;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Paint f14250 = new Paint();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f14251 = false;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private boolean f14252;

    public AmPmCirclesView(Context context) {
        super(context);
    }

    public void onDraw(Canvas canvas) {
        if (getWidth() != 0 && this.f14251) {
            if (!this.f14252) {
                int width = getWidth() / 2;
                int height = getHeight() / 2;
                int min = (int) (((float) Math.min(width, height)) * this.f14243);
                this.f14239 = (int) (((float) min) * this.f14244);
                int i = (int) (((double) height) + (((double) this.f14239) * 0.75d));
                this.f14250.setTextSize((float) ((this.f14239 * 3) / 4));
                this.f14236 = (i - (this.f14239 / 2)) + min;
                this.f14240 = (width - min) + this.f14239;
                this.f14241 = (width + min) - this.f14239;
                this.f14252 = true;
            }
            int i2 = this.f14248;
            int i3 = 255;
            int i4 = this.f14246;
            int i5 = this.f14248;
            int i6 = 255;
            int i7 = this.f14246;
            if (this.f14238 == 0) {
                i2 = this.f14233;
                i3 = this.f14247;
                i4 = this.f14231;
            } else if (this.f14238 == 1) {
                i5 = this.f14233;
                i6 = this.f14247;
                i7 = this.f14231;
            }
            if (this.f14242 == 0) {
                i2 = this.f14249;
                i3 = this.f14247;
            } else if (this.f14242 == 1) {
                i5 = this.f14249;
                i6 = this.f14247;
            }
            if (this.f14234) {
                i2 = this.f14248;
                i4 = this.f14232;
            }
            if (this.f14235) {
                i5 = this.f14248;
                i7 = this.f14232;
            }
            this.f14250.setColor(i2);
            this.f14250.setAlpha(i3);
            canvas.drawCircle((float) this.f14240, (float) this.f14236, (float) this.f14239, this.f14250);
            this.f14250.setColor(i5);
            this.f14250.setAlpha(i6);
            canvas.drawCircle((float) this.f14241, (float) this.f14236, (float) this.f14239, this.f14250);
            this.f14250.setColor(i4);
            int descent = this.f14236 - (((int) (this.f14250.descent() + this.f14250.ascent())) / 2);
            canvas.drawText(this.f14245, (float) this.f14240, (float) descent, this.f14250);
            this.f14250.setColor(i7);
            canvas.drawText(this.f14237, (float) this.f14241, (float) descent, this.f14250);
        }
    }

    public void setAmOrPm(int i) {
        this.f14238 = i;
    }

    public void setAmOrPmPressed(int i) {
        this.f14242 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m18088(float f, float f2) {
        if (!this.f14252) {
            return -1;
        }
        int i = (int) ((f2 - ((float) this.f14236)) * (f2 - ((float) this.f14236)));
        if (((int) Math.sqrt((double) (((f - ((float) this.f14240)) * (f - ((float) this.f14240))) + ((float) i)))) > this.f14239 || this.f14234) {
            return (((int) Math.sqrt((double) (((f - ((float) this.f14241)) * (f - ((float) this.f14241))) + ((float) i)))) > this.f14239 || this.f14235) ? -1 : 1;
        }
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18089(Context context, Locale locale, TimePickerController timePickerController, int i) {
        if (this.f14251) {
            Log.e("AmPmCirclesView", "AmPmCirclesView may only be initialized once.");
            return;
        }
        Resources resources = context.getResources();
        if (timePickerController.m18143()) {
            this.f14248 = ContextCompat.getColor(context, R.color.mdtp_circle_background_dark_theme);
            this.f14246 = ContextCompat.getColor(context, R.color.mdtp_white);
            this.f14232 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_disabled_dark_theme);
            this.f14247 = 255;
        } else {
            this.f14248 = ContextCompat.getColor(context, R.color.mdtp_white);
            this.f14246 = ContextCompat.getColor(context, R.color.mdtp_ampm_text_color);
            this.f14232 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_disabled);
            this.f14247 = 255;
        }
        this.f14233 = timePickerController.m18144();
        this.f14249 = Utils.m17952(this.f14233);
        this.f14231 = ContextCompat.getColor(context, R.color.mdtp_white);
        this.f14250.setTypeface(Typeface.create(resources.getString(R.string.mdtp_sans_serif), 0));
        this.f14250.setAntiAlias(true);
        this.f14250.setTextAlign(Paint.Align.CENTER);
        this.f14243 = Float.parseFloat(resources.getString(R.string.mdtp_circle_radius_multiplier));
        this.f14244 = Float.parseFloat(resources.getString(R.string.mdtp_ampm_circle_radius_multiplier));
        String[] amPmStrings = new DateFormatSymbols(locale).getAmPmStrings();
        this.f14245 = amPmStrings[0];
        this.f14237 = amPmStrings[1];
        this.f14234 = timePickerController.m18140();
        this.f14235 = timePickerController.m18141();
        setAmOrPm(i);
        this.f14242 = -1;
        this.f14251 = true;
    }
}
