package com.wdullaer.materialdatetimepicker.time;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.time.RadialTextsView;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;
import java.util.Calendar;
import java.util.Locale;

public class RadialPickerLayout extends FrameLayout implements View.OnTouchListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f14269;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public Timepoint f14270;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean f14271;

    /* renamed from: ʾ  reason: contains not printable characters */
    private RadialTextsView f14272;

    /* renamed from: ʿ  reason: contains not printable characters */
    private RadialTextsView f14273;

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean f14274;

    /* renamed from: ˈ  reason: contains not printable characters */
    private RadialTextsView f14275;
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public int f14276 = -1;

    /* renamed from: ˊ  reason: contains not printable characters */
    private RadialSelectorView f14277;

    /* renamed from: ˋ  reason: contains not printable characters */
    private View f14278;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int[] f14279;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean f14280;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f14281;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f14282;
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public int f14283;

    /* renamed from: ٴ  reason: contains not printable characters */
    private CircleView f14284;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public AmPmCirclesView f14285;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private float f14286;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private float f14287;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private AccessibilityManager f14288;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private AnimatorSet f14289;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private Handler f14290 = new Handler();
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public OnValueSelectedListener f14291;

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f14292;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public TimePickerController f14293;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public Timepoint f14294;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f14295;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private RadialSelectorView f14296;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private RadialSelectorView f14297;

    public interface OnValueSelectedListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m18128();

        /* renamed from: 龘  reason: contains not printable characters */
        void m18129(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m18130(Timepoint timepoint);
    }

    public RadialPickerLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOnTouchListener(this);
        this.f14295 = ViewConfiguration.get(context).getScaledTouchSlop();
        this.f14292 = ViewConfiguration.getTapTimeout();
        this.f14280 = false;
        this.f14284 = new CircleView(context);
        addView(this.f14284);
        this.f14285 = new AmPmCirclesView(context);
        addView(this.f14285);
        this.f14296 = new RadialSelectorView(context);
        addView(this.f14296);
        this.f14297 = new RadialSelectorView(context);
        addView(this.f14297);
        this.f14277 = new RadialSelectorView(context);
        addView(this.f14277);
        this.f14275 = new RadialTextsView(context);
        addView(this.f14275);
        this.f14272 = new RadialTextsView(context);
        addView(this.f14272);
        this.f14273 = new RadialTextsView(context);
        addView(this.f14273);
        m18117();
        this.f14294 = null;
        this.f14274 = true;
        this.f14278 = new View(context);
        this.f14278.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.f14278.setBackgroundColor(ContextCompat.getColor(context, R.color.mdtp_transparent_black));
        this.f14278.setVisibility(4);
        addView(this.f14278);
        this.f14288 = (AccessibilityManager) context.getSystemService("accessibility");
        this.f14269 = false;
    }

    private int getCurrentlyShowingValue() {
        switch (getCurrentItemShowing()) {
            case 0:
                return this.f14270.m18197();
            case 1:
                return this.f14270.m18194();
            case 2:
                return this.f14270.m18196();
            default:
                return -1;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private int m18104(int i) {
        if (this.f14279 == null) {
            return -1;
        }
        return this.f14279[i];
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m18107(int i) {
        int i2 = 1;
        int i3 = i == 0 ? 1 : 0;
        int i4 = i == 1 ? 1 : 0;
        if (i != 2) {
            i2 = 0;
        }
        this.f14275.setAlpha((float) i3);
        this.f14296.setAlpha((float) i3);
        this.f14272.setAlpha((float) i4);
        this.f14297.setAlpha((float) i4);
        this.f14273.setAlpha((float) i2);
        this.f14277.setAlpha((float) i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m18109(float f, float f2, boolean z, Boolean[] boolArr) {
        switch (getCurrentItemShowing()) {
            case 0:
                return this.f14296.m18131(f, f2, z, boolArr);
            case 1:
                return this.f14297.m18131(f, f2, z, boolArr);
            case 2:
                return this.f14277.m18131(f, f2, z, boolArr);
            default:
                return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m18110(int i, int i2) {
        int i3 = (i / 30) * 30;
        int i4 = i3 + 30;
        if (i2 == 1) {
            return i4;
        }
        if (i2 != -1) {
            return i - i3 < i4 - i ? i3 : i4;
        }
        if (i == i3) {
            i3 -= 30;
        }
        return i3;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Timepoint m18111(int i, boolean z, boolean z2) {
        int i2;
        if (i == -1) {
            return null;
        }
        int currentItemShowing = getCurrentItemShowing();
        int r12 = !z2 && (currentItemShowing == 1 || currentItemShowing == 2) ? m18104(i) : m18110(i, 0);
        switch (currentItemShowing) {
            case 0:
                i2 = 30;
                break;
            case 1:
                i2 = 6;
                break;
            default:
                i2 = 6;
                break;
        }
        if (currentItemShowing == 0) {
            if (this.f14271) {
                if (r12 == 0 && z) {
                    r12 = 360;
                } else if (r12 == 360 && !z) {
                    r12 = 0;
                }
            } else if (r12 == 0) {
                r12 = 360;
            }
        } else if (r12 == 360 && (currentItemShowing == 1 || currentItemShowing == 2)) {
            r12 = 0;
        }
        int i3 = r12 / i2;
        if (currentItemShowing == 0 && this.f14271 && !z && r12 != 0) {
            i3 += 12;
        }
        if (currentItemShowing == 0 && this.f14293.m18142() != TimePickerDialog.Version.VERSION_1 && this.f14271) {
            i3 = (i3 + 12) % 24;
        }
        switch (currentItemShowing) {
            case 0:
                int i4 = i3;
                if (!this.f14271 && getIsCurrentlyAmOrPm() == 1 && r12 != 360) {
                    i4 += 12;
                }
                if (!this.f14271 && getIsCurrentlyAmOrPm() == 0 && r12 == 360) {
                    i4 = 0;
                }
                return new Timepoint(i4, this.f14270.m18194(), this.f14270.m18196());
            case 1:
                return new Timepoint(this.f14270.m18197(), i3, this.f14270.m18196());
            case 2:
                return new Timepoint(this.f14270.m18197(), this.f14270.m18194(), i3);
            default:
                return this.f14270;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Timepoint m18116(Timepoint timepoint, int i) {
        switch (i) {
            case 0:
                return this.f14293.m18146(timepoint, (Timepoint.TYPE) null);
            case 1:
                return this.f14293.m18146(timepoint, Timepoint.TYPE.HOUR);
            default:
                return this.f14293.m18146(timepoint, Timepoint.TYPE.MINUTE);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18117() {
        this.f14279 = new int[361];
        int i = 0;
        int i2 = 1;
        int i3 = 8;
        for (int i4 = 0; i4 < 361; i4++) {
            this.f14279[i4] = i;
            if (i2 == i3) {
                i += 6;
                i3 = i == 360 ? 7 : i % 30 == 0 ? 14 : 4;
                i2 = 1;
            } else {
                i2++;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18118(int i, Timepoint timepoint) {
        Timepoint r3 = m18116(timepoint, i);
        this.f14270 = r3;
        m18120(r3, false, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18120(Timepoint timepoint, boolean z, int i) {
        switch (i) {
            case 0:
                int r1 = timepoint.m18197();
                boolean r2 = m18121(r1);
                int i2 = ((r1 % 12) * 360) / 12;
                if (!this.f14271) {
                    r1 %= 12;
                }
                if (!this.f14271 && r1 == 0) {
                    r1 += 12;
                }
                this.f14296.setSelection(i2, r2, z);
                this.f14275.setSelection(r1);
                if (timepoint.m18194() != this.f14270.m18194()) {
                    this.f14297.setSelection(timepoint.m18194() * 6, r2, z);
                    this.f14272.setSelection(timepoint.m18194());
                }
                if (timepoint.m18196() != this.f14270.m18196()) {
                    this.f14277.setSelection(timepoint.m18196() * 6, r2, z);
                    this.f14273.setSelection(timepoint.m18196());
                    break;
                }
                break;
            case 1:
                this.f14297.setSelection(timepoint.m18194() * 6, false, z);
                this.f14272.setSelection(timepoint.m18194());
                if (timepoint.m18196() != this.f14270.m18196()) {
                    this.f14277.setSelection(timepoint.m18196() * 6, false, z);
                    this.f14273.setSelection(timepoint.m18196());
                    break;
                }
                break;
            case 2:
                this.f14277.setSelection(timepoint.m18196() * 6, false, z);
                this.f14273.setSelection(timepoint.m18196());
                break;
        }
        switch (getCurrentItemShowing()) {
            case 0:
                this.f14296.invalidate();
                this.f14275.invalidate();
                return;
            case 1:
                this.f14297.invalidate();
                this.f14272.invalidate();
                return;
            case 2:
                this.f14277.invalidate();
                this.f14273.invalidate();
                return;
            default:
                return;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m18121(int i) {
        boolean z = i <= 12 && i != 0;
        if (this.f14293.m18142() != TimePickerDialog.Version.VERSION_1) {
            z = !z;
        }
        return this.f14271 && z;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() != 32) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        accessibilityEvent.getText().clear();
        Calendar instance = Calendar.getInstance();
        instance.set(10, getHours());
        instance.set(12, getMinutes());
        instance.set(13, getSeconds());
        long timeInMillis = instance.getTimeInMillis();
        int i = 1;
        if (this.f14271) {
            i = 1 | 128;
        }
        accessibilityEvent.getText().add(DateUtils.formatDateTime(getContext(), timeInMillis, i));
        return true;
    }

    public int getCurrentItemShowing() {
        if (this.f14281 == 0 || this.f14281 == 1 || this.f14281 == 2) {
            return this.f14281;
        }
        Log.e("RadialPickerLayout", "Current item showing was unfortunately set to " + this.f14281);
        return -1;
    }

    public int getHours() {
        return this.f14270.m18197();
    }

    public int getIsCurrentlyAmOrPm() {
        if (this.f14270.m18195()) {
            return 0;
        }
        return this.f14270.m18193() ? 1 : -1;
    }

    public int getMinutes() {
        return this.f14270.m18194();
    }

    public int getSeconds() {
        return this.f14270.m18196();
    }

    public Timepoint getTime() {
        return this.f14270;
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        if (Build.VERSION.SDK_INT >= 21) {
            accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
            accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
        } else if (Build.VERSION.SDK_INT >= 16) {
            accessibilityNodeInfo.addAction(4096);
            accessibilityNodeInfo.addAction(8192);
        } else {
            accessibilityNodeInfo.addAction(4096);
            accessibilityNodeInfo.addAction(8192);
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int r4;
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        final Boolean[] boolArr = {false};
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.f14274) {
                    return true;
                }
                this.f14286 = x;
                this.f14287 = y;
                this.f14294 = null;
                this.f14280 = false;
                this.f14282 = true;
                if (this.f14271 || this.f14293.m18142() != TimePickerDialog.Version.VERSION_1) {
                    this.f14276 = -1;
                } else {
                    this.f14276 = this.f14285.m18088(x, y);
                }
                if (this.f14276 == 0 || this.f14276 == 1) {
                    this.f14293.m18139();
                    this.f14283 = -1;
                    this.f14290.postDelayed(new Runnable() {
                        public void run() {
                            RadialPickerLayout.this.f14285.setAmOrPmPressed(RadialPickerLayout.this.f14276);
                            RadialPickerLayout.this.f14285.invalidate();
                        }
                    }, (long) this.f14292);
                } else {
                    this.f14283 = m18109(x, y, this.f14288.isTouchExplorationEnabled(), boolArr);
                    if (this.f14293.m18147(m18111(this.f14283, boolArr[0].booleanValue(), false), getCurrentItemShowing())) {
                        this.f14283 = -1;
                    }
                    if (this.f14283 != -1) {
                        this.f14293.m18139();
                        this.f14290.postDelayed(new Runnable() {
                            public void run() {
                                boolean unused = RadialPickerLayout.this.f14280 = true;
                                Timepoint unused2 = RadialPickerLayout.this.f14294 = RadialPickerLayout.this.m18111(RadialPickerLayout.this.f14283, boolArr[0].booleanValue(), false);
                                Timepoint unused3 = RadialPickerLayout.this.f14294 = RadialPickerLayout.this.m18116(RadialPickerLayout.this.f14294, RadialPickerLayout.this.getCurrentItemShowing());
                                RadialPickerLayout.this.m18120(RadialPickerLayout.this.f14294, true, RadialPickerLayout.this.getCurrentItemShowing());
                                RadialPickerLayout.this.f14291.m18130(RadialPickerLayout.this.f14294);
                            }
                        }, (long) this.f14292);
                    }
                }
                return true;
            case 1:
                if (!this.f14274) {
                    Log.d("RadialPickerLayout", "Input was disabled, but received ACTION_UP.");
                    this.f14291.m18128();
                    return true;
                }
                this.f14290.removeCallbacksAndMessages((Object) null);
                this.f14282 = false;
                if (this.f14276 == 0 || this.f14276 == 1) {
                    int r9 = this.f14285.m18088(x, y);
                    this.f14285.setAmOrPmPressed(-1);
                    this.f14285.invalidate();
                    if (r9 == this.f14276) {
                        this.f14285.setAmOrPm(r9);
                        if (getIsCurrentlyAmOrPm() != r9) {
                            Timepoint timepoint = new Timepoint(this.f14270);
                            if (this.f14276 == 0) {
                                timepoint.m18190();
                            } else if (this.f14276 == 1) {
                                timepoint.m18191();
                            }
                            Timepoint r10 = m18116(timepoint, 0);
                            m18120(r10, false, 0);
                            this.f14270 = r10;
                            this.f14291.m18130(r10);
                        }
                    }
                    this.f14276 = -1;
                    break;
                } else {
                    if (!(this.f14283 == -1 || (r4 = m18109(x, y, this.f14280, boolArr)) == -1)) {
                        Timepoint r12 = m18116(m18111(r4, boolArr[0].booleanValue(), !this.f14280), getCurrentItemShowing());
                        m18120(r12, false, getCurrentItemShowing());
                        this.f14270 = r12;
                        this.f14291.m18130(r12);
                        this.f14291.m18129(getCurrentItemShowing());
                    }
                    this.f14280 = false;
                    return true;
                }
            case 2:
                if (!this.f14274) {
                    Log.e("RadialPickerLayout", "Input was disabled, but received ACTION_MOVE.");
                    return true;
                }
                float abs = Math.abs(y - this.f14287);
                float abs2 = Math.abs(x - this.f14286);
                if (this.f14280 || abs2 > ((float) this.f14295) || abs > ((float) this.f14295)) {
                    if (this.f14276 == 0 || this.f14276 == 1) {
                        this.f14290.removeCallbacksAndMessages((Object) null);
                        if (this.f14285.m18088(x, y) != this.f14276) {
                            this.f14285.setAmOrPmPressed(-1);
                            this.f14285.invalidate();
                            this.f14276 = -1;
                            break;
                        }
                    } else if (this.f14283 != -1) {
                        this.f14280 = true;
                        this.f14290.removeCallbacksAndMessages((Object) null);
                        int r42 = m18109(x, y, true, boolArr);
                        if (r42 != -1) {
                            Timepoint r122 = m18116(m18111(r42, boolArr[0].booleanValue(), false), getCurrentItemShowing());
                            m18120(r122, true, getCurrentItemShowing());
                            if (r122 != null && (this.f14294 == null || !this.f14294.equals(r122))) {
                                this.f14293.m18139();
                                this.f14294 = r122;
                                this.f14291.m18130(r122);
                            }
                        }
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    public boolean performAccessibilityAction(int i, Bundle bundle) {
        int i2;
        int i3;
        int i4;
        Timepoint timepoint;
        if (super.performAccessibilityAction(i, bundle)) {
            return true;
        }
        int i5 = 0;
        if (Build.VERSION.SDK_INT >= 16) {
            i2 = 4096;
            i3 = 8192;
        } else {
            i2 = 4096;
            i3 = 8192;
        }
        if (i == i2) {
            i5 = 1;
        } else if (i == i3) {
            i5 = -1;
        }
        if (i5 == 0) {
            return false;
        }
        int currentlyShowingValue = getCurrentlyShowingValue();
        int i6 = 0;
        int currentItemShowing = getCurrentItemShowing();
        if (currentItemShowing == 0) {
            i6 = 30;
            currentlyShowingValue %= 12;
        } else if (currentItemShowing == 1) {
            i6 = 6;
        } else if (currentItemShowing == 2) {
            i6 = 6;
        }
        int r9 = m18110(currentlyShowingValue * i6, i5) / i6;
        int i7 = 0;
        if (currentItemShowing != 0) {
            i4 = 55;
        } else if (this.f14271) {
            i4 = 23;
        } else {
            i4 = 12;
            i7 = 1;
        }
        if (r9 > i4) {
            r9 = i7;
        } else if (r9 < i7) {
            r9 = i4;
        }
        switch (currentItemShowing) {
            case 0:
                timepoint = new Timepoint(r9, this.f14270.m18194(), this.f14270.m18196());
                break;
            case 1:
                timepoint = new Timepoint(this.f14270.m18197(), r9, this.f14270.m18196());
                break;
            case 2:
                timepoint = new Timepoint(this.f14270.m18197(), this.f14270.m18194(), r9);
                break;
            default:
                timepoint = this.f14270;
                break;
        }
        m18118(currentItemShowing, timepoint);
        this.f14291.m18130(timepoint);
        return true;
    }

    public void setAmOrPm(int i) {
        this.f14285.setAmOrPm(i);
        this.f14285.invalidate();
        Timepoint timepoint = new Timepoint(this.f14270);
        if (i == 0) {
            timepoint.m18190();
        } else if (i == 1) {
            timepoint.m18191();
        }
        Timepoint r0 = m18116(timepoint, 0);
        m18120(r0, false, 0);
        this.f14270 = r0;
        this.f14291.m18130(r0);
    }

    public void setCurrentItemShowing(int i, boolean z) {
        if (i == 0 || i == 1 || i == 2) {
            int currentItemShowing = getCurrentItemShowing();
            this.f14281 = i;
            m18120(getTime(), true, i);
            if (!z || i == currentItemShowing) {
                m18107(i);
                return;
            }
            ObjectAnimator[] objectAnimatorArr = new ObjectAnimator[4];
            if (i == 1 && currentItemShowing == 0) {
                objectAnimatorArr[0] = this.f14275.getDisappearAnimator();
                objectAnimatorArr[1] = this.f14296.getDisappearAnimator();
                objectAnimatorArr[2] = this.f14272.getReappearAnimator();
                objectAnimatorArr[3] = this.f14297.getReappearAnimator();
            } else if (i == 0 && currentItemShowing == 1) {
                objectAnimatorArr[0] = this.f14275.getReappearAnimator();
                objectAnimatorArr[1] = this.f14296.getReappearAnimator();
                objectAnimatorArr[2] = this.f14272.getDisappearAnimator();
                objectAnimatorArr[3] = this.f14297.getDisappearAnimator();
            } else if (i == 1 && currentItemShowing == 2) {
                objectAnimatorArr[0] = this.f14273.getDisappearAnimator();
                objectAnimatorArr[1] = this.f14277.getDisappearAnimator();
                objectAnimatorArr[2] = this.f14272.getReappearAnimator();
                objectAnimatorArr[3] = this.f14297.getReappearAnimator();
            } else if (i == 0 && currentItemShowing == 2) {
                objectAnimatorArr[0] = this.f14273.getDisappearAnimator();
                objectAnimatorArr[1] = this.f14277.getDisappearAnimator();
                objectAnimatorArr[2] = this.f14275.getReappearAnimator();
                objectAnimatorArr[3] = this.f14296.getReappearAnimator();
            } else if (i == 2 && currentItemShowing == 1) {
                objectAnimatorArr[0] = this.f14273.getReappearAnimator();
                objectAnimatorArr[1] = this.f14277.getReappearAnimator();
                objectAnimatorArr[2] = this.f14272.getDisappearAnimator();
                objectAnimatorArr[3] = this.f14297.getDisappearAnimator();
            } else if (i == 2 && currentItemShowing == 0) {
                objectAnimatorArr[0] = this.f14273.getReappearAnimator();
                objectAnimatorArr[1] = this.f14277.getReappearAnimator();
                objectAnimatorArr[2] = this.f14275.getDisappearAnimator();
                objectAnimatorArr[3] = this.f14296.getDisappearAnimator();
            }
            if (objectAnimatorArr[0] == null || objectAnimatorArr[1] == null || objectAnimatorArr[2] == null || objectAnimatorArr[3] == null) {
                m18107(i);
                return;
            }
            if (this.f14289 != null && this.f14289.isRunning()) {
                this.f14289.end();
            }
            this.f14289 = new AnimatorSet();
            this.f14289.playTogether(objectAnimatorArr);
            this.f14289.start();
            return;
        }
        Log.e("RadialPickerLayout", "TimePicker does not support view at index " + i);
    }

    public void setOnValueSelectedListener(OnValueSelectedListener onValueSelectedListener) {
        this.f14291 = onValueSelectedListener;
    }

    public void setTime(Timepoint timepoint) {
        m18118(0, timepoint);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18123(Context context, Locale locale, TimePickerController timePickerController, Timepoint timepoint, boolean z) {
        if (this.f14269) {
            Log.e("RadialPickerLayout", "Time has already been initialized.");
            return;
        }
        this.f14293 = timePickerController;
        this.f14271 = this.f14288.isTouchExplorationEnabled() || z;
        this.f14284.m18090(context, this.f14293);
        this.f14284.invalidate();
        if (!this.f14271 && this.f14293.m18142() == TimePickerDialog.Version.VERSION_1) {
            this.f14285.m18089(context, locale, this.f14293, timepoint.m18195() ? 0 : 1);
            this.f14285.invalidate();
        }
        AnonymousClass1 r0 = new RadialTextsView.SelectionValidator() {
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m18125(int i) {
                return !RadialPickerLayout.this.f14293.m18147(new Timepoint(RadialPickerLayout.this.f14270.m18197(), RadialPickerLayout.this.f14270.m18194(), i), 2);
            }
        };
        AnonymousClass2 r13 = new RadialTextsView.SelectionValidator() {
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m18126(int i) {
                return !RadialPickerLayout.this.f14293.m18147(new Timepoint(RadialPickerLayout.this.f14270.m18197(), i, RadialPickerLayout.this.f14270.m18196()), 1);
            }
        };
        AnonymousClass3 r7 = new RadialTextsView.SelectionValidator() {
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m18127(int i) {
                Timepoint timepoint = new Timepoint(i, RadialPickerLayout.this.f14270.m18194(), RadialPickerLayout.this.f14270.m18196());
                if (!RadialPickerLayout.this.f14271 && RadialPickerLayout.this.getIsCurrentlyAmOrPm() == 1) {
                    timepoint.m18191();
                }
                if (!RadialPickerLayout.this.f14271 && RadialPickerLayout.this.getIsCurrentlyAmOrPm() == 0) {
                    timepoint.m18190();
                }
                return !RadialPickerLayout.this.f14293.m18147(timepoint, 0);
            }
        };
        int[] iArr = {12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        int[] iArr2 = {0, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};
        int[] iArr3 = {0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55};
        int[] iArr4 = {0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55};
        String[] strArr = new String[12];
        String[] strArr2 = new String[12];
        String[] strArr3 = new String[12];
        String[] strArr4 = new String[12];
        for (int i = 0; i < 12; i++) {
            strArr[i] = z ? String.format(locale, "%02d", new Object[]{Integer.valueOf(iArr2[i])}) : String.format(locale, "%d", new Object[]{Integer.valueOf(iArr[i])});
            strArr2[i] = String.format(locale, "%d", new Object[]{Integer.valueOf(iArr[i])});
            strArr3[i] = String.format(locale, "%02d", new Object[]{Integer.valueOf(iArr3[i])});
            strArr4[i] = String.format(locale, "%02d", new Object[]{Integer.valueOf(iArr4[i])});
        }
        if (this.f14293.m18142() == TimePickerDialog.Version.VERSION_2) {
            String[] strArr5 = strArr;
            strArr = strArr2;
            strArr2 = strArr5;
        }
        this.f14275.m18137(context, strArr, z ? strArr2 : null, this.f14293, (RadialTextsView.SelectionValidator) r7, true);
        this.f14275.setSelection(z ? timepoint.m18197() : iArr[timepoint.m18197() % 12]);
        this.f14275.invalidate();
        this.f14272.m18137(context, strArr3, (String[]) null, this.f14293, (RadialTextsView.SelectionValidator) r13, false);
        this.f14272.setSelection(timepoint.m18194());
        this.f14272.invalidate();
        this.f14273.m18137(context, strArr4, (String[]) null, this.f14293, (RadialTextsView.SelectionValidator) r0, false);
        this.f14273.setSelection(timepoint.m18196());
        this.f14273.invalidate();
        this.f14270 = timepoint;
        this.f14296.m18132(context, this.f14293, z, true, (timepoint.m18197() % 12) * 30, m18121(timepoint.m18197()));
        this.f14297.m18132(context, this.f14293, false, false, timepoint.m18194() * 6, false);
        this.f14277.m18132(context, this.f14293, false, false, timepoint.m18196() * 6, false);
        this.f14269 = true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18124(boolean z) {
        int i = 0;
        if (this.f14282 && !z) {
            return false;
        }
        this.f14274 = z;
        View view = this.f14278;
        if (z) {
            i = 4;
        }
        view.setVisibility(i);
        return true;
    }
}
