package com.wdullaer.materialdatetimepicker.time;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

public class RadialTextsView extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f14329;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private float f14330;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f14331 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f14332 = -1;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private InvalidateUpdateListener f14333;

    /* renamed from: ʾ  reason: contains not printable characters */
    private String[] f14334;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f14335;

    /* renamed from: ˆ  reason: contains not printable characters */
    private float f14336;

    /* renamed from: ˈ  reason: contains not printable characters */
    private String[] f14337;

    /* renamed from: ˉ  reason: contains not printable characters */
    private float f14338;

    /* renamed from: ˊ  reason: contains not printable characters */
    private float f14339;

    /* renamed from: ˋ  reason: contains not printable characters */
    private float f14340;

    /* renamed from: ˎ  reason: contains not printable characters */
    private float f14341;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f14342;

    /* renamed from: ˑ  reason: contains not printable characters */
    private SelectionValidator f14343;

    /* renamed from: י  reason: contains not printable characters */
    private int f14344;

    /* renamed from: ـ  reason: contains not printable characters */
    private float f14345;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Typeface f14346;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private Typeface f14347;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private float f14348;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f14349;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private float f14350;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private float f14351;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private float f14352;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private float[] f14353;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private float[] f14354;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Paint f14355 = new Paint();

    /* renamed from: 靐  reason: contains not printable characters */
    ObjectAnimator f14356;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Paint f14357 = new Paint();

    /* renamed from: 齉  reason: contains not printable characters */
    private final Paint f14358 = new Paint();

    /* renamed from: 龘  reason: contains not printable characters */
    ObjectAnimator f14359;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private float[] f14360;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private boolean f14361;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private float f14362;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private float[] f14363;

    private class InvalidateUpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private InvalidateUpdateListener() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            RadialTextsView.this.invalidate();
        }
    }

    interface SelectionValidator {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m18138(int i);
    }

    public RadialTextsView(Context context) {
        super(context);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18133() {
        this.f14359 = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofKeyframe("animationRadiusMultiplier", new Keyframe[]{Keyframe.ofFloat(0.0f, 1.0f), Keyframe.ofFloat(0.2f, this.f14350), Keyframe.ofFloat(1.0f, this.f14330)}), PropertyValuesHolder.ofKeyframe("alpha", new Keyframe[]{Keyframe.ofFloat(0.0f, 1.0f), Keyframe.ofFloat(1.0f, 0.0f)})}).setDuration((long) 500);
        this.f14359.addUpdateListener(this.f14333);
        int i = (int) (((float) 500) * (1.0f + 0.25f));
        float f = (((float) 500) * 0.25f) / ((float) i);
        this.f14356 = ObjectAnimator.ofPropertyValuesHolder(this, new PropertyValuesHolder[]{PropertyValuesHolder.ofKeyframe("animationRadiusMultiplier", new Keyframe[]{Keyframe.ofFloat(0.0f, this.f14330), Keyframe.ofFloat(f, this.f14330), Keyframe.ofFloat(1.0f - ((1.0f - f) * 0.2f), this.f14350), Keyframe.ofFloat(1.0f, 1.0f)}), PropertyValuesHolder.ofKeyframe("alpha", new Keyframe[]{Keyframe.ofFloat(0.0f, 0.0f), Keyframe.ofFloat(f, 0.0f), Keyframe.ofFloat(1.0f, 1.0f)})}).setDuration((long) i);
        this.f14356.addUpdateListener(this.f14333);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18134(float f, float f2, float f3, float f4, float[] fArr, float[] fArr2) {
        float f5 = f;
        float sqrt = (((float) Math.sqrt(3.0d)) * f) / 2.0f;
        float f6 = f / 2.0f;
        this.f14358.setTextSize(f4);
        this.f14357.setTextSize(f4);
        this.f14355.setTextSize(f4);
        float descent = f3 - ((this.f14358.descent() + this.f14358.ascent()) / 2.0f);
        fArr[0] = descent - f5;
        fArr2[0] = f2 - f5;
        fArr[1] = descent - sqrt;
        fArr2[1] = f2 - sqrt;
        fArr[2] = descent - f6;
        fArr2[2] = f2 - f6;
        fArr[3] = descent;
        fArr2[3] = f2;
        fArr[4] = descent + f6;
        fArr2[4] = f2 + f6;
        fArr[5] = descent + sqrt;
        fArr2[5] = f2 + sqrt;
        fArr[6] = descent + f5;
        fArr2[6] = f2 + f5;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18135(Canvas canvas, float f, Typeface typeface, String[] strArr, float[] fArr, float[] fArr2) {
        this.f14358.setTextSize(f);
        this.f14358.setTypeface(typeface);
        Paint[] r0 = m18136(strArr);
        canvas.drawText(strArr[0], fArr[3], fArr2[0], r0[0]);
        canvas.drawText(strArr[1], fArr[4], fArr2[1], r0[1]);
        canvas.drawText(strArr[2], fArr[5], fArr2[2], r0[2]);
        canvas.drawText(strArr[3], fArr[6], fArr2[3], r0[3]);
        canvas.drawText(strArr[4], fArr[5], fArr2[4], r0[4]);
        canvas.drawText(strArr[5], fArr[4], fArr2[5], r0[5]);
        canvas.drawText(strArr[6], fArr[3], fArr2[6], r0[6]);
        canvas.drawText(strArr[7], fArr[2], fArr2[5], r0[7]);
        canvas.drawText(strArr[8], fArr[1], fArr2[4], r0[8]);
        canvas.drawText(strArr[9], fArr[0], fArr2[3], r0[9]);
        canvas.drawText(strArr[10], fArr[1], fArr2[2], r0[10]);
        canvas.drawText(strArr[11], fArr[2], fArr2[1], r0[11]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Paint[] m18136(String[] strArr) {
        Paint[] paintArr = new Paint[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            int parseInt = Integer.parseInt(strArr[i]);
            if (parseInt == this.f14332) {
                paintArr[i] = this.f14357;
            } else if (this.f14343.m18138(parseInt)) {
                paintArr[i] = this.f14358;
            } else {
                paintArr[i] = this.f14355;
            }
        }
        return paintArr;
    }

    public ObjectAnimator getDisappearAnimator() {
        if (this.f14331 && this.f14329 && this.f14359 != null) {
            return this.f14359;
        }
        Log.e("RadialTextsView", "RadialTextView was not ready for animation.");
        return null;
    }

    public ObjectAnimator getReappearAnimator() {
        if (this.f14331 && this.f14329 && this.f14356 != null) {
            return this.f14356;
        }
        Log.e("RadialTextsView", "RadialTextView was not ready for animation.");
        return null;
    }

    public boolean hasOverlappingRendering() {
        return false;
    }

    public void onDraw(Canvas canvas) {
        if (getWidth() != 0 && this.f14331) {
            if (!this.f14329) {
                this.f14342 = getWidth() / 2;
                this.f14344 = getHeight() / 2;
                this.f14345 = ((float) Math.min(this.f14342, this.f14344)) * this.f14362;
                if (!this.f14335) {
                    this.f14344 = (int) (((double) this.f14344) - (((double) (this.f14345 * this.f14339)) * 0.75d));
                }
                this.f14351 = this.f14345 * this.f14336;
                if (this.f14361) {
                    this.f14352 = this.f14345 * this.f14338;
                }
                m18133();
                this.f14349 = true;
                this.f14329 = true;
            }
            if (this.f14349) {
                m18134(this.f14345 * this.f14340 * this.f14348, (float) this.f14342, (float) this.f14344, this.f14351, this.f14353, this.f14354);
                if (this.f14361) {
                    m18134(this.f14345 * this.f14341 * this.f14348, (float) this.f14342, (float) this.f14344, this.f14352, this.f14360, this.f14363);
                }
                this.f14349 = false;
            }
            m18135(canvas, this.f14351, this.f14346, this.f14337, this.f14354, this.f14353);
            if (this.f14361) {
                m18135(canvas, this.f14352, this.f14347, this.f14334, this.f14363, this.f14360);
            }
        }
    }

    public void setAnimationRadiusMultiplier(float f) {
        this.f14348 = f;
        this.f14349 = true;
    }

    /* access modifiers changed from: protected */
    public void setSelection(int i) {
        this.f14332 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18137(Context context, String[] strArr, String[] strArr2, TimePickerController timePickerController, SelectionValidator selectionValidator, boolean z) {
        if (this.f14331) {
            Log.e("RadialTextsView", "This RadialTextsView may only be initialized once.");
            return;
        }
        Resources resources = context.getResources();
        this.f14358.setColor(ContextCompat.getColor(context, timePickerController.m18143() ? R.color.mdtp_white : R.color.mdtp_numbers_text_color));
        this.f14346 = Typeface.create(resources.getString(R.string.mdtp_radial_numbers_typeface), 0);
        this.f14347 = Typeface.create(resources.getString(R.string.mdtp_sans_serif), 0);
        this.f14358.setAntiAlias(true);
        this.f14358.setTextAlign(Paint.Align.CENTER);
        this.f14357.setColor(ContextCompat.getColor(context, R.color.mdtp_white));
        this.f14357.setAntiAlias(true);
        this.f14357.setTextAlign(Paint.Align.CENTER);
        this.f14355.setColor(ContextCompat.getColor(context, timePickerController.m18143() ? R.color.mdtp_date_picker_text_disabled_dark_theme : R.color.mdtp_date_picker_text_disabled));
        this.f14355.setAntiAlias(true);
        this.f14355.setTextAlign(Paint.Align.CENTER);
        this.f14337 = strArr;
        this.f14334 = strArr2;
        this.f14335 = timePickerController.m18145();
        this.f14361 = strArr2 != null;
        if (this.f14335 || timePickerController.m18142() != TimePickerDialog.Version.VERSION_1) {
            this.f14362 = Float.parseFloat(resources.getString(R.string.mdtp_circle_radius_multiplier_24HourMode));
        } else {
            this.f14362 = Float.parseFloat(resources.getString(R.string.mdtp_circle_radius_multiplier));
            this.f14339 = Float.parseFloat(resources.getString(R.string.mdtp_ampm_circle_radius_multiplier));
        }
        this.f14353 = new float[7];
        this.f14354 = new float[7];
        if (this.f14361) {
            this.f14340 = Float.parseFloat(resources.getString(R.string.mdtp_numbers_radius_multiplier_outer));
            this.f14341 = Float.parseFloat(resources.getString(R.string.mdtp_numbers_radius_multiplier_inner));
            if (timePickerController.m18142() == TimePickerDialog.Version.VERSION_1) {
                this.f14336 = Float.parseFloat(resources.getString(R.string.mdtp_text_size_multiplier_outer));
                this.f14338 = Float.parseFloat(resources.getString(R.string.mdtp_text_size_multiplier_inner));
            } else {
                this.f14336 = Float.parseFloat(resources.getString(R.string.mdtp_text_size_multiplier_outer_v2));
                this.f14338 = Float.parseFloat(resources.getString(R.string.mdtp_text_size_multiplier_inner_v2));
            }
            this.f14360 = new float[7];
            this.f14363 = new float[7];
        } else {
            this.f14340 = Float.parseFloat(resources.getString(R.string.mdtp_numbers_radius_multiplier_normal));
            this.f14336 = Float.parseFloat(resources.getString(R.string.mdtp_text_size_multiplier_normal));
        }
        this.f14348 = 1.0f;
        this.f14350 = (((float) (z ? -1 : 1)) * 0.05f) + 1.0f;
        this.f14330 = (((float) (z ? 1 : -1)) * 0.3f) + 1.0f;
        this.f14333 = new InvalidateUpdateListener();
        this.f14343 = selectionValidator;
        this.f14349 = true;
        this.f14331 = true;
    }
}
