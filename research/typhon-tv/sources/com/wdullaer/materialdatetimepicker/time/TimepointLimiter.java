package com.wdullaer.materialdatetimepicker.time;

import android.os.Parcelable;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

public interface TimepointLimiter extends Parcelable {
    /* renamed from: 靐  reason: contains not printable characters */
    boolean m18204();

    /* renamed from: 龘  reason: contains not printable characters */
    Timepoint m18205(Timepoint timepoint, Timepoint.TYPE type, Timepoint.TYPE type2);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m18206();

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m18207(Timepoint timepoint, int i, Timepoint.TYPE type);
}
