package com.wdullaer.materialdatetimepicker.time;

import android.os.Parcel;
import android.os.Parcelable;

public class Timepoint implements Parcelable, Comparable<Timepoint> {
    public static final Parcelable.Creator<Timepoint> CREATOR = new Parcelable.Creator<Timepoint>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Timepoint createFromParcel(Parcel parcel) {
            return new Timepoint(parcel);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timepoint[] newArray(int i) {
            return new Timepoint[i];
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private int f14430;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f14431;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f14432;

    public enum TYPE {
        HOUR,
        MINUTE,
        SECOND
    }

    public Timepoint(int i) {
        this(i, 0);
    }

    public Timepoint(int i, int i2) {
        this(i, i2, 0);
    }

    public Timepoint(int i, int i2, int i3) {
        this.f14432 = i % 24;
        this.f14430 = i2 % 60;
        this.f14431 = i3 % 60;
    }

    public Timepoint(Parcel parcel) {
        this.f14432 = parcel.readInt();
        this.f14430 = parcel.readInt();
        this.f14431 = parcel.readInt();
    }

    public Timepoint(Timepoint timepoint) {
        this(timepoint.f14432, timepoint.f14430, timepoint.f14431);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return hashCode() == ((Timepoint) obj).hashCode();
    }

    public int hashCode() {
        return m18192();
    }

    public String toString() {
        return "" + this.f14432 + "h " + this.f14430 + "m " + this.f14431 + "s";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f14432);
        parcel.writeInt(this.f14430);
        parcel.writeInt(this.f14431);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m18190() {
        if (this.f14432 >= 12) {
            this.f14432 %= 12;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m18191() {
        if (this.f14432 < 12) {
            this.f14432 = (this.f14432 + 12) % 24;
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m18192() {
        return (this.f14432 * 3600) + (this.f14430 * 60) + this.f14431;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m18193() {
        return !m18195();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m18194() {
        return this.f14430;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m18195() {
        return this.f14432 < 12;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m18196() {
        return this.f14431;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m18197() {
        return this.f14432;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m18198(TYPE type) {
        switch (type) {
            case SECOND:
                return m18196();
            case MINUTE:
                return m18194();
            default:
                return m18197();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int compareTo(Timepoint timepoint) {
        return hashCode() - timepoint.hashCode();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18200(TYPE type, int i) {
        if (type == TYPE.MINUTE) {
            i *= 60;
        }
        if (type == TYPE.HOUR) {
            i *= 3600;
        }
        int r4 = i + m18192();
        switch (type) {
            case SECOND:
                this.f14431 = (r4 % 3600) % 60;
                break;
            case MINUTE:
                break;
            case HOUR:
                break;
            default:
                return;
        }
        this.f14430 = (r4 % 3600) / 60;
        this.f14432 = (r4 / 3600) % 24;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        if (r6.m18194() != m18194()) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        if (r0 == false) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        if (r6.m18197() != m18197()) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003d, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003f, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        if (r0 == false) goto L_0x003d;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m18201(com.wdullaer.materialdatetimepicker.time.Timepoint r6, com.wdullaer.materialdatetimepicker.time.Timepoint.TYPE r7) {
        /*
            r5 = this;
            r1 = 1
            r2 = 0
            if (r6 != 0) goto L_0x0005
        L_0x0004:
            return r2
        L_0x0005:
            r0 = 1
            int[] r3 = com.wdullaer.materialdatetimepicker.time.Timepoint.AnonymousClass2.f14433
            int r4 = r7.ordinal()
            r3 = r3[r4]
            switch(r3) {
                case 1: goto L_0x0013;
                case 2: goto L_0x0020;
                case 3: goto L_0x002d;
                default: goto L_0x0011;
            }
        L_0x0011:
            r2 = r0
            goto L_0x0004
        L_0x0013:
            if (r0 == 0) goto L_0x003b
            int r3 = r6.m18196()
            int r4 = r5.m18196()
            if (r3 != r4) goto L_0x003b
            r0 = r1
        L_0x0020:
            if (r0 == 0) goto L_0x003d
            int r3 = r6.m18194()
            int r4 = r5.m18194()
            if (r3 != r4) goto L_0x003d
            r0 = r1
        L_0x002d:
            if (r0 == 0) goto L_0x003f
            int r3 = r6.m18197()
            int r4 = r5.m18197()
            if (r3 != r4) goto L_0x003f
            r0 = r1
        L_0x003a:
            goto L_0x0011
        L_0x003b:
            r0 = r2
            goto L_0x0020
        L_0x003d:
            r0 = r2
            goto L_0x002d
        L_0x003f:
            r0 = r2
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wdullaer.materialdatetimepicker.time.Timepoint.m18201(com.wdullaer.materialdatetimepicker.time.Timepoint, com.wdullaer.materialdatetimepicker.time.Timepoint$TYPE):boolean");
    }
}
