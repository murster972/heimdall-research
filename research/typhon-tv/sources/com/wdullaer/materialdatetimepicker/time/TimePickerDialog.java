package com.wdullaer.materialdatetimepicker.time;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wdullaer.materialdatetimepicker.HapticFeedbackController;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.TypefaceHelper;
import com.wdullaer.materialdatetimepicker.Utils;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.Timepoint;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import net.pubnative.library.request.PubnativeRequest;

public class TimePickerDialog extends DialogFragment implements RadialPickerLayout.OnValueSelectedListener, TimePickerController {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Button f14365;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private int f14366;

    /* renamed from: ʼ  reason: contains not printable characters */
    private TextView f14367;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private String f14368;

    /* renamed from: ʽ  reason: contains not printable characters */
    private TextView f14369;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private int f14370;

    /* renamed from: ʾ  reason: contains not printable characters */
    private TextView f14371;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private Version f14372;

    /* renamed from: ʿ  reason: contains not printable characters */
    private TextView f14373;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private int f14374;

    /* renamed from: ˆ  reason: contains not printable characters */
    private String f14375;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private TimepointLimiter f14376;

    /* renamed from: ˈ  reason: contains not printable characters */
    private TextView f14377;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private char f14378;

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean f14379;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private Locale f14380;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f14381;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private String f14382;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f14383;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private String f14384;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f14385;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private ArrayList<Integer> f14386;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Timepoint f14387;
    /* access modifiers changed from: private */

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public boolean f14388;

    /* renamed from: ˑ  reason: contains not printable characters */
    private TextView f14389;

    /* renamed from: ˑˑ  reason: contains not printable characters */
    private Node f14390;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f14391;

    /* renamed from: יי  reason: contains not printable characters */
    private int f14392;

    /* renamed from: ـ  reason: contains not printable characters */
    private String f14393;

    /* renamed from: ــ  reason: contains not printable characters */
    private DefaultTimepointLimiter f14394;

    /* renamed from: ٴ  reason: contains not printable characters */
    private TextView f14395;

    /* renamed from: ٴٴ  reason: contains not printable characters */
    private String f14396;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private TextView f14397;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private int f14398;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f14399;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private String f14400;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f14401;

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    private String f14402;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f14403;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    private int f14404;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f14405;

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    private String f14406;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private boolean f14407;

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    private String f14408;

    /* renamed from: 连任  reason: contains not printable characters */
    private Button f14409;

    /* renamed from: 靐  reason: contains not printable characters */
    private DialogInterface.OnCancelListener f14410;

    /* renamed from: 麤  reason: contains not printable characters */
    private HapticFeedbackController f14411;

    /* renamed from: 齉  reason: contains not printable characters */
    private DialogInterface.OnDismissListener f14412;

    /* renamed from: 龘  reason: contains not printable characters */
    private OnTimeSetListener f14413;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private boolean f14414;

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    private String f14415;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private View f14416;

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    private String f14417;
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public RadialPickerLayout f14418;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private boolean f14419;

    private class KeyboardListener implements View.OnKeyListener {
        private KeyboardListener() {
        }

        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() == 1) {
                return TimePickerDialog.this.m18156(i);
            }
            return false;
        }
    }

    private static class Node {

        /* renamed from: 靐  reason: contains not printable characters */
        private ArrayList<Node> f14427 = new ArrayList<>();

        /* renamed from: 龘  reason: contains not printable characters */
        private int[] f14428;

        public Node(int... iArr) {
            this.f14428 = iArr;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Node m18186(int i) {
            if (this.f14427 == null) {
                return null;
            }
            Iterator<Node> it2 = this.f14427.iterator();
            while (it2.hasNext()) {
                Node next = it2.next();
                if (next.m18188(i)) {
                    return next;
                }
            }
            return null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18187(Node node) {
            this.f14427.add(node);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m18188(int i) {
            for (int i2 : this.f14428) {
                if (i2 == i) {
                    return true;
                }
            }
            return false;
        }
    }

    public interface OnTimeSetListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m18189(TimePickerDialog timePickerDialog, int i, int i2, int i3);
    }

    public enum Version {
        VERSION_1,
        VERSION_2
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m18148(int i) {
        if (!this.f14418.m18124(false)) {
            return;
        }
        if (i == -1 || m18149(i)) {
            this.f14388 = true;
            this.f14365.setEnabled(false);
            m18159(false);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m18149(int i) {
        int i2 = 6;
        if (this.f14419 && !this.f14414) {
            i2 = 4;
        }
        if (!this.f14419 && !this.f14414) {
            i2 = 2;
        }
        if (this.f14391 && this.f14386.size() == i2) {
            return false;
        }
        if (!this.f14391 && m18153()) {
            return false;
        }
        this.f14386.add(Integer.valueOf(i));
        if (!m18155()) {
            m18151();
            return false;
        }
        int r1 = m18150(i);
        Utils.m17956((View) this.f14418, (CharSequence) String.format(this.f14380, "%d", new Object[]{Integer.valueOf(r1)}));
        if (m18153()) {
            if (!this.f14391 && this.f14386.size() <= i2 - 1) {
                this.f14386.add(this.f14386.size() - 1, 7);
                this.f14386.add(this.f14386.size() - 1, 7);
            }
            this.f14365.setEnabled(true);
        }
        return true;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static int m18150(int i) {
        switch (i) {
            case 7:
                return 0;
            case 8:
                return 1;
            case 9:
                return 2;
            case 10:
                return 3;
            case 11:
                return 4;
            case 12:
                return 5;
            case 13:
                return 6;
            case 14:
                return 7;
            case 15:
                return 8;
            case 16:
                return 9;
            default:
                return -1;
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private int m18151() {
        int intValue = this.f14386.remove(this.f14386.size() - 1).intValue();
        if (!m18153()) {
            this.f14365.setEnabled(false);
        }
        return intValue;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m18152() {
        this.f14390 = new Node(new int[0]);
        if (!this.f14419 && this.f14391) {
            Node node = new Node(7, 8);
            this.f14390.m18187(node);
            node.m18187(new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16));
            Node node2 = new Node(9);
            this.f14390.m18187(node2);
            node2.m18187(new Node(7, 8, 9, 10));
        } else if (!this.f14419 && !this.f14391) {
            Node node3 = new Node(m18154(0), m18154(1));
            Node node4 = new Node(8);
            this.f14390.m18187(node4);
            node4.m18187(node3);
            Node node5 = new Node(7, 8, 9);
            node4.m18187(node5);
            node5.m18187(node3);
            Node node6 = new Node(9, 10, 11, 12, 13, 14, 15, 16);
            this.f14390.m18187(node6);
            node6.m18187(node3);
        } else if (this.f14391) {
            Node node7 = new Node(7, 8, 9, 10, 11, 12);
            Node node8 = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            node7.m18187(node8);
            if (this.f14414) {
                Node node9 = new Node(7, 8, 9, 10, 11, 12);
                node9.m18187(new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16));
                node8.m18187(node9);
            }
            Node node10 = new Node(7, 8);
            this.f14390.m18187(node10);
            Node node11 = new Node(7, 8, 9, 10, 11, 12);
            node10.m18187(node11);
            node11.m18187(node7);
            node11.m18187(new Node(13, 14, 15, 16));
            Node node12 = new Node(13, 14, 15, 16);
            node10.m18187(node12);
            node12.m18187(node7);
            Node node13 = new Node(9);
            this.f14390.m18187(node13);
            Node node14 = new Node(7, 8, 9, 10);
            node13.m18187(node14);
            node14.m18187(node7);
            Node node15 = new Node(11, 12);
            node13.m18187(node15);
            node15.m18187(node8);
            Node node16 = new Node(10, 11, 12, 13, 14, 15, 16);
            this.f14390.m18187(node16);
            node16.m18187(node7);
        } else {
            Node node17 = new Node(m18154(0), m18154(1));
            Node node18 = new Node(7, 8, 9, 10, 11, 12);
            Node node19 = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            node19.m18187(node17);
            node18.m18187(node19);
            Node node20 = new Node(8);
            this.f14390.m18187(node20);
            node20.m18187(node17);
            Node node21 = new Node(7, 8, 9);
            node20.m18187(node21);
            node21.m18187(node17);
            Node node22 = new Node(7, 8, 9, 10, 11, 12);
            node21.m18187(node22);
            node22.m18187(node17);
            Node node23 = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            node22.m18187(node23);
            node23.m18187(node17);
            if (this.f14414) {
                node23.m18187(node18);
            }
            Node node24 = new Node(13, 14, 15, 16);
            node21.m18187(node24);
            node24.m18187(node17);
            if (this.f14414) {
                node24.m18187(node18);
            }
            Node node25 = new Node(10, 11, 12);
            node20.m18187(node25);
            Node node26 = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            node25.m18187(node26);
            node26.m18187(node17);
            if (this.f14414) {
                node26.m18187(node18);
            }
            Node node27 = new Node(9, 10, 11, 12, 13, 14, 15, 16);
            this.f14390.m18187(node27);
            node27.m18187(node17);
            Node node28 = new Node(7, 8, 9, 10, 11, 12);
            node27.m18187(node28);
            Node node29 = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            node28.m18187(node29);
            node29.m18187(node17);
            if (this.f14414) {
                node29.m18187(node18);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m18153() {
        boolean z = false;
        if (this.f14391) {
            int[] r1 = m18171(new Boolean[]{false, false, false});
            return r1[0] >= 0 && r1[1] >= 0 && r1[1] < 60 && r1[2] >= 0 && r1[2] < 60;
        }
        if (this.f14386.contains(Integer.valueOf(m18154(0))) || this.f14386.contains(Integer.valueOf(m18154(1)))) {
            z = true;
        }
        return z;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m18154(int i) {
        if (this.f14404 == -1 || this.f14392 == -1) {
            KeyCharacterMap load = KeyCharacterMap.load(-1);
            int i2 = 0;
            while (true) {
                if (i2 >= Math.max(this.f14385.length(), this.f14375.length())) {
                    break;
                }
                char charAt = this.f14385.toLowerCase(this.f14380).charAt(i2);
                char charAt2 = this.f14375.toLowerCase(this.f14380).charAt(i2);
                if (charAt != charAt2) {
                    KeyEvent[] events = load.getEvents(new char[]{charAt, charAt2});
                    if (events == null || events.length != 4) {
                        Log.e("TimePickerDialog", "Unable to find keycodes for AM and PM.");
                    } else {
                        this.f14404 = events[0].getKeyCode();
                        this.f14392 = events[2].getKeyCode();
                    }
                } else {
                    i2++;
                }
            }
        }
        if (i == 0) {
            return this.f14404;
        }
        if (i == 1) {
            return this.f14392;
        }
        return -1;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private boolean m18155() {
        Node node = this.f14390;
        Iterator<Integer> it2 = this.f14386.iterator();
        while (it2.hasNext()) {
            node = node.m18186(it2.next().intValue());
            if (node == null) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m18156(int i) {
        String format;
        if (i != 111 && i != 4) {
            if (i == 61) {
                if (this.f14388) {
                    if (!m18153()) {
                        return true;
                    }
                    m18168(true);
                    return true;
                }
            } else if (i == 66) {
                if (this.f14388) {
                    if (!m18153()) {
                        return true;
                    }
                    m18168(false);
                }
                if (this.f14413 != null) {
                    this.f14413.m18189(this, this.f14418.getHours(), this.f14418.getMinutes(), this.f14418.getSeconds());
                }
                dismiss();
                return true;
            } else if (i == 67) {
                if (this.f14388 && !this.f14386.isEmpty()) {
                    int r0 = m18151();
                    if (r0 == m18154(0)) {
                        format = this.f14385;
                    } else if (r0 == m18154(1)) {
                        format = this.f14375;
                    } else {
                        format = String.format(this.f14380, "%d", new Object[]{Integer.valueOf(m18150(r0))});
                    }
                    Utils.m17956((View) this.f14418, (CharSequence) String.format(this.f14382, new Object[]{format}));
                    m18159(true);
                }
            } else if (i == 7 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15 || i == 16 || (!this.f14391 && (i == m18154(0) || i == m18154(1)))) {
                if (!this.f14388) {
                    if (this.f14418 == null) {
                        Log.e("TimePickerDialog", "Unable to initiate keyboard mode, TimePicker was null.");
                        return true;
                    }
                    this.f14386.clear();
                    m18148(i);
                    return true;
                } else if (!m18149(i)) {
                    return true;
                } else {
                    m18159(false);
                    return true;
                }
            }
            return false;
        } else if (!isCancelable()) {
            return true;
        } else {
            dismiss();
            return true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Timepoint m18157(Timepoint timepoint) {
        return m18181(timepoint, (Timepoint.TYPE) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m18158(int i) {
        if (this.f14372 == Version.VERSION_2) {
            if (i == 0) {
                this.f14371.setTextColor(this.f14381);
                this.f14373.setTextColor(this.f14383);
                Utils.m17956((View) this.f14418, (CharSequence) this.f14385);
                return;
            }
            this.f14371.setTextColor(this.f14383);
            this.f14373.setTextColor(this.f14381);
            Utils.m17956((View) this.f14418, (CharSequence) this.f14375);
        } else if (i == 0) {
            this.f14373.setText(this.f14385);
            Utils.m17956((View) this.f14418, (CharSequence) this.f14385);
            this.f14373.setContentDescription(this.f14385);
        } else if (i == 1) {
            this.f14373.setText(this.f14375);
            Utils.m17956((View) this.f14418, (CharSequence) this.f14375);
            this.f14373.setContentDescription(this.f14375);
        } else {
            this.f14373.setText(this.f14384);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m18159(boolean z) {
        if (z || !this.f14386.isEmpty()) {
            Boolean[] boolArr = {false, false, false};
            int[] r10 = m18171(boolArr);
            String str = boolArr[0].booleanValue() ? "%02d" : "%2d";
            String str2 = boolArr[1].booleanValue() ? "%02d" : "%2d";
            String str3 = boolArr[1].booleanValue() ? "%02d" : "%2d";
            String replace = r10[0] == -1 ? this.f14384 : String.format(str, new Object[]{Integer.valueOf(r10[0])}).replace(' ', this.f14378);
            String replace2 = r10[1] == -1 ? this.f14384 : String.format(str2, new Object[]{Integer.valueOf(r10[1])}).replace(' ', this.f14378);
            String replace3 = r10[2] == -1 ? this.f14384 : String.format(str3, new Object[]{Integer.valueOf(r10[1])}).replace(' ', this.f14378);
            this.f14367.setText(replace);
            this.f14369.setText(replace);
            this.f14367.setTextColor(this.f14383);
            this.f14389.setText(replace2);
            this.f14395.setText(replace2);
            this.f14389.setTextColor(this.f14383);
            this.f14397.setText(replace3);
            this.f14377.setText(replace3);
            this.f14397.setTextColor(this.f14383);
            if (!this.f14391) {
                m18158(r10[3]);
                return;
            }
            return;
        }
        int hours = this.f14418.getHours();
        int minutes = this.f14418.getMinutes();
        int seconds = this.f14418.getSeconds();
        m18164(hours, true);
        m18163(minutes);
        m18161(seconds);
        if (!this.f14391) {
            m18158(hours < 12 ? 0 : 1);
        }
        m18165(this.f14418.getCurrentItemShowing(), true, true, true);
        this.f14365.setEnabled(true);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m18161(int i) {
        if (i == 60) {
            i = 0;
        }
        String format = String.format(this.f14380, "%02d", new Object[]{Integer.valueOf(i)});
        Utils.m17956((View) this.f14418, (CharSequence) format);
        this.f14397.setText(format);
        this.f14377.setText(format);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m18163(int i) {
        if (i == 60) {
            i = 0;
        }
        String format = String.format(this.f14380, "%02d", new Object[]{Integer.valueOf(i)});
        Utils.m17956((View) this.f14418, (CharSequence) format);
        this.f14389.setText(format);
        this.f14395.setText(format);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18164(int i, boolean z) {
        String str;
        if (this.f14391) {
            str = "%02d";
        } else {
            str = "%d";
            i %= 12;
            if (i == 0) {
                i = 12;
            }
        }
        String format = String.format(this.f14380, str, new Object[]{Integer.valueOf(i)});
        this.f14367.setText(format);
        this.f14369.setText(format);
        if (z) {
            Utils.m17956((View) this.f14418, (CharSequence) format);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18165(int i, boolean z, boolean z2, boolean z3) {
        TextView textView;
        this.f14418.setCurrentItemShowing(i, z);
        switch (i) {
            case 0:
                int hours = this.f14418.getHours();
                if (!this.f14391) {
                    hours %= 12;
                }
                this.f14418.setContentDescription(this.f14402 + ": " + hours);
                if (z3) {
                    Utils.m17956((View) this.f14418, (CharSequence) this.f14406);
                }
                textView = this.f14367;
                break;
            case 1:
                this.f14418.setContentDescription(this.f14408 + ": " + this.f14418.getMinutes());
                if (z3) {
                    Utils.m17956((View) this.f14418, (CharSequence) this.f14415);
                }
                textView = this.f14389;
                break;
            default:
                this.f14418.setContentDescription(this.f14396 + ": " + this.f14418.getSeconds());
                if (z3) {
                    Utils.m17956((View) this.f14418, (CharSequence) this.f14417);
                }
                textView = this.f14397;
                break;
        }
        int i2 = i == 0 ? this.f14381 : this.f14383;
        int i3 = i == 1 ? this.f14381 : this.f14383;
        int i4 = i == 2 ? this.f14381 : this.f14383;
        this.f14367.setTextColor(i2);
        this.f14389.setTextColor(i3);
        this.f14397.setTextColor(i4);
        ObjectAnimator r5 = Utils.m17954((View) textView, 0.85f, 1.1f);
        if (z2) {
            r5.setStartDelay(300);
        }
        r5.start();
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18168(boolean z) {
        this.f14388 = false;
        if (!this.f14386.isEmpty()) {
            int[] r1 = m18171(new Boolean[]{false, false, false});
            this.f14418.setTime(new Timepoint(r1[0], r1[1], r1[2]));
            if (!this.f14391) {
                this.f14418.setAmOrPm(r1[3]);
            }
            this.f14386.clear();
        }
        if (z) {
            m18159(false);
            this.f14418.m18124(true);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int[] m18171(Boolean[] boolArr) {
        int i = -1;
        int i2 = 1;
        if (!this.f14391 && m18153()) {
            int intValue = this.f14386.get(this.f14386.size() - 1).intValue();
            if (intValue == m18154(0)) {
                i = 0;
            } else if (intValue == m18154(1)) {
                i = 1;
            }
            i2 = 2;
        }
        int i3 = -1;
        int i4 = -1;
        int i5 = 0;
        int i6 = this.f14414 ? 2 : 0;
        for (int i7 = i2; i7 <= this.f14386.size(); i7++) {
            int r8 = m18150(this.f14386.get(this.f14386.size() - i7).intValue());
            if (this.f14414) {
                if (i7 == i2) {
                    i5 = r8;
                } else if (i7 == i2 + 1) {
                    i5 += r8 * 10;
                    if (boolArr != null && r8 == 0) {
                        boolArr[2] = true;
                    }
                }
            }
            if (this.f14419) {
                if (i7 == i2 + i6) {
                    i3 = r8;
                } else if (i7 == i2 + i6 + 1) {
                    i3 += r8 * 10;
                    if (boolArr != null && r8 == 0) {
                        boolArr[1] = true;
                    }
                } else if (i7 == i2 + i6 + 2) {
                    i4 = r8;
                } else if (i7 == i2 + i6 + 3) {
                    i4 += r8 * 10;
                    if (boolArr != null && r8 == 0) {
                        boolArr[0] = true;
                    }
                }
            } else if (i7 == i2 + i6) {
                i4 = r8;
            } else if (i7 == i2 + i6 + 1) {
                i4 += r8 * 10;
                if (boolArr != null && r8 == 0) {
                    boolArr[0] = true;
                }
            }
        }
        return new int[]{i4, i3, i5, i};
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        if (this.f14410 != null) {
            this.f14410.onCancel(dialogInterface);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        ViewGroup viewGroup = (ViewGroup) getView();
        if (viewGroup != null) {
            viewGroup.removeAllViewsInLayout();
            viewGroup.addView(onCreateView(getActivity().getLayoutInflater(), viewGroup, (Bundle) null));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null && bundle.containsKey("initial_time") && bundle.containsKey("is_24_hour_view")) {
            this.f14387 = (Timepoint) bundle.getParcelable("initial_time");
            this.f14391 = bundle.getBoolean("is_24_hour_view");
            this.f14388 = bundle.getBoolean("in_kb_mode");
            this.f14393 = bundle.getString("dialog_title");
            this.f14399 = bundle.getBoolean("theme_dark");
            this.f14401 = bundle.getBoolean("theme_dark_changed");
            this.f14405 = bundle.getInt("accent");
            this.f14403 = bundle.getBoolean("vibrate");
            this.f14407 = bundle.getBoolean("dismiss");
            this.f14414 = bundle.getBoolean("enable_seconds");
            this.f14419 = bundle.getBoolean("enable_minutes");
            this.f14398 = bundle.getInt("ok_resid");
            this.f14400 = bundle.getString("ok_string");
            this.f14366 = bundle.getInt("ok_color");
            this.f14370 = bundle.getInt("cancel_resid");
            this.f14368 = bundle.getString("cancel_string");
            this.f14374 = bundle.getInt("cancel_color");
            this.f14372 = (Version) bundle.getSerializable("version");
            this.f14376 = (TimepointLimiter) bundle.getParcelable("timepoint_limiter");
            this.f14380 = (Locale) bundle.getSerializable(PubnativeRequest.Parameters.LOCALE);
            this.f14394 = this.f14376 instanceof DefaultTimepointLimiter ? (DefaultTimepointLimiter) this.f14376 : new DefaultTimepointLimiter();
        }
    }

    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        onCreateDialog.requestWindowFeature(1);
        return onCreateDialog;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(this.f14372 == Version.VERSION_1 ? R.layout.mdtp_time_picker_dialog : R.layout.mdtp_time_picker_dialog_v2, viewGroup, false);
        KeyboardListener keyboardListener = new KeyboardListener();
        inflate.findViewById(R.id.mdtp_time_picker_dialog).setOnKeyListener(keyboardListener);
        if (this.f14405 == -1) {
            this.f14405 = Utils.m17953((Context) getActivity());
        }
        if (!this.f14401) {
            this.f14399 = Utils.m17959((Context) getActivity(), this.f14399);
        }
        Resources resources = getResources();
        Activity activity = getActivity();
        this.f14402 = resources.getString(R.string.mdtp_hour_picker_description);
        this.f14406 = resources.getString(R.string.mdtp_select_hours);
        this.f14408 = resources.getString(R.string.mdtp_minute_picker_description);
        this.f14415 = resources.getString(R.string.mdtp_select_minutes);
        this.f14396 = resources.getString(R.string.mdtp_second_picker_description);
        this.f14417 = resources.getString(R.string.mdtp_select_seconds);
        this.f14381 = ContextCompat.getColor(activity, R.color.mdtp_white);
        this.f14383 = ContextCompat.getColor(activity, R.color.mdtp_accent_color_focused);
        this.f14367 = (TextView) inflate.findViewById(R.id.mdtp_hours);
        this.f14367.setOnKeyListener(keyboardListener);
        this.f14369 = (TextView) inflate.findViewById(R.id.mdtp_hour_space);
        this.f14395 = (TextView) inflate.findViewById(R.id.mdtp_minutes_space);
        this.f14389 = (TextView) inflate.findViewById(R.id.mdtp_minutes);
        this.f14389.setOnKeyListener(keyboardListener);
        this.f14377 = (TextView) inflate.findViewById(R.id.mdtp_seconds_space);
        this.f14397 = (TextView) inflate.findViewById(R.id.mdtp_seconds);
        this.f14397.setOnKeyListener(keyboardListener);
        this.f14371 = (TextView) inflate.findViewById(R.id.mdtp_am_label);
        this.f14371.setOnKeyListener(keyboardListener);
        this.f14373 = (TextView) inflate.findViewById(R.id.mdtp_pm_label);
        this.f14373.setOnKeyListener(keyboardListener);
        this.f14416 = inflate.findViewById(R.id.mdtp_ampm_layout);
        String[] amPmStrings = new DateFormatSymbols(this.f14380).getAmPmStrings();
        this.f14385 = amPmStrings[0];
        this.f14375 = amPmStrings[1];
        this.f14411 = new HapticFeedbackController(getActivity());
        if (this.f14418 != null) {
            this.f14387 = new Timepoint(this.f14418.getHours(), this.f14418.getMinutes(), this.f14418.getSeconds());
        }
        this.f14387 = m18157(this.f14387);
        this.f14418 = (RadialPickerLayout) inflate.findViewById(R.id.mdtp_time_picker);
        this.f14418.setOnValueSelectedListener(this);
        this.f14418.setOnKeyListener(keyboardListener);
        this.f14418.m18123(getActivity(), this.f14380, this, this.f14387, this.f14391);
        int i = 0;
        if (bundle != null && bundle.containsKey("current_item_showing")) {
            i = bundle.getInt("current_item_showing");
        }
        m18165(i, false, true, true);
        this.f14418.invalidate();
        this.f14367.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TimePickerDialog.this.m18165(0, true, false, true);
                TimePickerDialog.this.m18172();
            }
        });
        this.f14389.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TimePickerDialog.this.m18165(1, true, false, true);
                TimePickerDialog.this.m18172();
            }
        });
        this.f14397.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TimePickerDialog.this.m18165(2, true, false, true);
                TimePickerDialog.this.m18172();
            }
        });
        String string = activity.getResources().getString(R.string.mdtp_button_typeface);
        this.f14365 = (Button) inflate.findViewById(R.id.mdtp_ok);
        this.f14365.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!TimePickerDialog.this.f14388 || !TimePickerDialog.this.m18153()) {
                    TimePickerDialog.this.m18172();
                } else {
                    TimePickerDialog.this.m18168(false);
                }
                TimePickerDialog.this.m18176();
                TimePickerDialog.this.dismiss();
            }
        });
        this.f14365.setOnKeyListener(keyboardListener);
        this.f14365.setTypeface(TypefaceHelper.m17950(activity, string));
        if (this.f14400 != null) {
            this.f14365.setText(this.f14400);
        } else {
            this.f14365.setText(this.f14398);
        }
        this.f14409 = (Button) inflate.findViewById(R.id.mdtp_cancel);
        this.f14409.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TimePickerDialog.this.m18172();
                if (TimePickerDialog.this.getDialog() != null) {
                    TimePickerDialog.this.getDialog().cancel();
                }
            }
        });
        this.f14409.setTypeface(TypefaceHelper.m17950(activity, string));
        if (this.f14368 != null) {
            this.f14409.setText(this.f14368);
        } else {
            this.f14409.setText(this.f14370);
        }
        this.f14409.setVisibility(isCancelable() ? 0 : 8);
        if (this.f14391) {
            this.f14416.setVisibility(8);
        } else {
            AnonymousClass6 r0 = new View.OnClickListener() {
                public void onClick(View view) {
                    if (!TimePickerDialog.this.m18173() && !TimePickerDialog.this.m18174()) {
                        TimePickerDialog.this.m18172();
                        int isCurrentlyAmOrPm = TimePickerDialog.this.f14418.getIsCurrentlyAmOrPm();
                        if (isCurrentlyAmOrPm == 0) {
                            isCurrentlyAmOrPm = 1;
                        } else if (isCurrentlyAmOrPm == 1) {
                            isCurrentlyAmOrPm = 0;
                        }
                        TimePickerDialog.this.f14418.setAmOrPm(isCurrentlyAmOrPm);
                    }
                }
            };
            this.f14371.setVisibility(8);
            this.f14373.setVisibility(0);
            this.f14416.setOnClickListener(r0);
            if (this.f14372 == Version.VERSION_2) {
                this.f14371.setText(this.f14385);
                this.f14373.setText(this.f14375);
                this.f14371.setVisibility(0);
            }
            m18158(this.f14387.m18195() ? 0 : 1);
        }
        if (!this.f14414) {
            this.f14397.setVisibility(8);
            inflate.findViewById(R.id.mdtp_separator_seconds).setVisibility(8);
        }
        if (!this.f14419) {
            this.f14395.setVisibility(8);
            inflate.findViewById(R.id.mdtp_separator).setVisibility(8);
        }
        if (getResources().getConfiguration().orientation == 2) {
            if (!this.f14419 && !this.f14414) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(2, R.id.mdtp_center_view);
                layoutParams.addRule(14);
                this.f14369.setLayoutParams(layoutParams);
                if (this.f14391) {
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams2.addRule(1, R.id.mdtp_hour_space);
                    this.f14416.setLayoutParams(layoutParams2);
                }
            } else if (!this.f14414 && this.f14391) {
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(14);
                layoutParams3.addRule(2, R.id.mdtp_center_view);
                ((TextView) inflate.findViewById(R.id.mdtp_separator)).setLayoutParams(layoutParams3);
            } else if (!this.f14414) {
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.addRule(14);
                layoutParams4.addRule(2, R.id.mdtp_center_view);
                ((TextView) inflate.findViewById(R.id.mdtp_separator)).setLayoutParams(layoutParams4);
                RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams5.addRule(13);
                layoutParams5.addRule(3, R.id.mdtp_center_view);
                this.f14416.setLayoutParams(layoutParams5);
            } else if (this.f14391) {
                RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams6.addRule(14);
                layoutParams6.addRule(2, R.id.mdtp_seconds_space);
                ((TextView) inflate.findViewById(R.id.mdtp_separator)).setLayoutParams(layoutParams6);
                RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams7.addRule(13);
                this.f14377.setLayoutParams(layoutParams7);
            } else {
                RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams8.addRule(13);
                this.f14377.setLayoutParams(layoutParams8);
                RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams9.addRule(14);
                layoutParams9.addRule(2, R.id.mdtp_seconds_space);
                ((TextView) inflate.findViewById(R.id.mdtp_separator)).setLayoutParams(layoutParams9);
                RelativeLayout.LayoutParams layoutParams10 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams10.addRule(14);
                layoutParams10.addRule(3, R.id.mdtp_seconds_space);
                this.f14416.setLayoutParams(layoutParams10);
            }
        } else if (this.f14391 && !this.f14414 && this.f14419) {
            RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams11.addRule(13);
            ((TextView) inflate.findViewById(R.id.mdtp_separator)).setLayoutParams(layoutParams11);
        } else if (!this.f14419 && !this.f14414) {
            RelativeLayout.LayoutParams layoutParams12 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams12.addRule(13);
            this.f14369.setLayoutParams(layoutParams12);
            if (!this.f14391) {
                RelativeLayout.LayoutParams layoutParams13 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams13.addRule(1, R.id.mdtp_hour_space);
                layoutParams13.addRule(4, R.id.mdtp_hour_space);
                this.f14416.setLayoutParams(layoutParams13);
            }
        } else if (this.f14414) {
            View findViewById = inflate.findViewById(R.id.mdtp_separator);
            RelativeLayout.LayoutParams layoutParams14 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams14.addRule(0, R.id.mdtp_minutes_space);
            layoutParams14.addRule(15, -1);
            findViewById.setLayoutParams(layoutParams14);
            if (!this.f14391) {
                RelativeLayout.LayoutParams layoutParams15 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams15.addRule(13);
                this.f14395.setLayoutParams(layoutParams15);
            } else {
                RelativeLayout.LayoutParams layoutParams16 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams16.addRule(1, R.id.mdtp_center_view);
                this.f14395.setLayoutParams(layoutParams16);
            }
        }
        this.f14379 = true;
        m18164(this.f14387.m18197(), true);
        m18163(this.f14387.m18194());
        m18161(this.f14387.m18196());
        this.f14384 = resources.getString(R.string.mdtp_time_placeholder);
        this.f14382 = resources.getString(R.string.mdtp_deleted_key);
        this.f14378 = this.f14384.charAt(0);
        this.f14392 = -1;
        this.f14404 = -1;
        m18152();
        if (this.f14388 && bundle != null) {
            this.f14386 = bundle.getIntegerArrayList("typed_times");
            m18148(-1);
            this.f14367.invalidate();
        } else if (this.f14386 == null) {
            this.f14386 = new ArrayList<>();
        }
        TextView textView = (TextView) inflate.findViewById(R.id.mdtp_time_picker_header);
        if (!this.f14393.isEmpty()) {
            textView.setVisibility(0);
            textView.setText(this.f14393.toUpperCase(this.f14380));
        }
        textView.setBackgroundColor(Utils.m17952(this.f14405));
        inflate.findViewById(R.id.mdtp_time_display_background).setBackgroundColor(this.f14405);
        inflate.findViewById(R.id.mdtp_time_display).setBackgroundColor(this.f14405);
        if (this.f14366 != -1) {
            this.f14365.setTextColor(this.f14366);
        } else {
            this.f14365.setTextColor(this.f14405);
        }
        if (this.f14374 != -1) {
            this.f14409.setTextColor(this.f14374);
        } else {
            this.f14409.setTextColor(this.f14405);
        }
        if (getDialog() == null) {
            inflate.findViewById(R.id.mdtp_done_background).setVisibility(8);
        }
        int color = ContextCompat.getColor(activity, R.color.mdtp_circle_background);
        int color2 = ContextCompat.getColor(activity, R.color.mdtp_background_color);
        int color3 = ContextCompat.getColor(activity, R.color.mdtp_light_gray);
        int color4 = ContextCompat.getColor(activity, R.color.mdtp_light_gray);
        RadialPickerLayout radialPickerLayout = this.f14418;
        if (!this.f14399) {
            color4 = color;
        }
        radialPickerLayout.setBackgroundColor(color4);
        View findViewById2 = inflate.findViewById(R.id.mdtp_time_picker_dialog);
        if (!this.f14399) {
            color3 = color2;
        }
        findViewById2.setBackgroundColor(color3);
        return inflate;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (this.f14412 != null) {
            this.f14412.onDismiss(dialogInterface);
        }
    }

    public void onPause() {
        super.onPause();
        this.f14411.m17947();
        if (this.f14407) {
            dismiss();
        }
    }

    public void onResume() {
        super.onResume();
        this.f14411.m17949();
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (this.f14418 != null) {
            bundle.putParcelable("initial_time", this.f14418.getTime());
            bundle.putBoolean("is_24_hour_view", this.f14391);
            bundle.putInt("current_item_showing", this.f14418.getCurrentItemShowing());
            bundle.putBoolean("in_kb_mode", this.f14388);
            if (this.f14388) {
                bundle.putIntegerArrayList("typed_times", this.f14386);
            }
            bundle.putString("dialog_title", this.f14393);
            bundle.putBoolean("theme_dark", this.f14399);
            bundle.putBoolean("theme_dark_changed", this.f14401);
            bundle.putInt("accent", this.f14405);
            bundle.putBoolean("vibrate", this.f14403);
            bundle.putBoolean("dismiss", this.f14407);
            bundle.putBoolean("enable_seconds", this.f14414);
            bundle.putBoolean("enable_minutes", this.f14419);
            bundle.putInt("ok_resid", this.f14398);
            bundle.putString("ok_string", this.f14400);
            bundle.putInt("ok_color", this.f14366);
            bundle.putInt("cancel_resid", this.f14370);
            bundle.putString("cancel_string", this.f14368);
            bundle.putInt("cancel_color", this.f14374);
            bundle.putSerializable("version", this.f14372);
            bundle.putParcelable("timepoint_limiter", this.f14376);
            bundle.putSerializable(PubnativeRequest.Parameters.LOCALE, this.f14380);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m18172() {
        if (this.f14403) {
            this.f14411.m17948();
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m18173() {
        return this.f14376.m18206();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m18174() {
        return this.f14376.m18204();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public Timepoint.TYPE m18175() {
        return this.f14414 ? Timepoint.TYPE.SECOND : this.f14419 ? Timepoint.TYPE.MINUTE : Timepoint.TYPE.HOUR;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m18176() {
        if (this.f14413 != null) {
            this.f14413.m18189(this, this.f14418.getHours(), this.f14418.getMinutes(), this.f14418.getSeconds());
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Version m18177() {
        return this.f14372;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m18178() {
        return this.f14399;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m18179() {
        return this.f14405;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m18180() {
        return this.f14391;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timepoint m18181(Timepoint timepoint, Timepoint.TYPE type) {
        return this.f14376.m18205(timepoint, type, m18175());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18182() {
        if (!m18153()) {
            this.f14386.clear();
        }
        m18168(true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18183(int i) {
        if (this.f14379) {
            if (i == 0 && this.f14419) {
                m18165(1, true, true, false);
                Utils.m17956((View) this.f14418, (CharSequence) this.f14406 + ". " + this.f14418.getMinutes());
            } else if (i == 1 && this.f14414) {
                m18165(2, true, true, false);
                Utils.m17956((View) this.f14418, (CharSequence) this.f14415 + ". " + this.f14418.getSeconds());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18184(Timepoint timepoint) {
        int i = 0;
        m18164(timepoint.m18197(), false);
        this.f14418.setContentDescription(this.f14402 + ": " + timepoint.m18197());
        m18163(timepoint.m18194());
        this.f14418.setContentDescription(this.f14408 + ": " + timepoint.m18194());
        m18161(timepoint.m18196());
        this.f14418.setContentDescription(this.f14396 + ": " + timepoint.m18196());
        if (!this.f14391) {
            if (!timepoint.m18195()) {
                i = 1;
            }
            m18158(i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18185(Timepoint timepoint, int i) {
        return this.f14376.m18207(timepoint, i, m18175());
    }
}
