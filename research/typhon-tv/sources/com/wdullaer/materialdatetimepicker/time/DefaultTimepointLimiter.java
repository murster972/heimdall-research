package com.wdullaer.materialdatetimepicker.time;

import android.os.Parcel;
import android.os.Parcelable;
import com.wdullaer.materialdatetimepicker.time.Timepoint;
import java.util.Arrays;
import java.util.TreeSet;

class DefaultTimepointLimiter implements TimepointLimiter {
    public static final Parcelable.Creator<DefaultTimepointLimiter> CREATOR = new Parcelable.Creator<DefaultTimepointLimiter>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public DefaultTimepointLimiter createFromParcel(Parcel parcel) {
            return new DefaultTimepointLimiter(parcel);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public DefaultTimepointLimiter[] newArray(int i) {
            return new DefaultTimepointLimiter[i];
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private Timepoint f14264;

    /* renamed from: 靐  reason: contains not printable characters */
    private TreeSet<Timepoint> f14265 = new TreeSet<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private Timepoint f14266;

    /* renamed from: 齉  reason: contains not printable characters */
    private TreeSet<Timepoint> f14267 = new TreeSet<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private TreeSet<Timepoint> f14268 = new TreeSet<>();

    DefaultTimepointLimiter() {
    }

    public DefaultTimepointLimiter(Parcel parcel) {
        this.f14266 = (Timepoint) parcel.readParcelable(Timepoint.class.getClassLoader());
        this.f14264 = (Timepoint) parcel.readParcelable(Timepoint.class.getClassLoader());
        this.f14268.addAll(Arrays.asList(parcel.createTypedArray(Timepoint.CREATOR)));
        this.f14265.addAll(Arrays.asList(parcel.createTypedArray(Timepoint.CREATOR)));
        this.f14267 = m18092(this.f14268, this.f14265);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Timepoint m18091(Timepoint timepoint, Timepoint.TYPE type, Timepoint.TYPE type2) {
        Timepoint timepoint2 = new Timepoint(timepoint);
        Timepoint timepoint3 = new Timepoint(timepoint);
        int i = 0;
        int i2 = 1;
        if (type2 == Timepoint.TYPE.MINUTE) {
            i2 = 60;
        }
        if (type2 == Timepoint.TYPE.SECOND) {
            i2 = 3600;
        }
        while (i < i2 * 24) {
            i++;
            timepoint2.m18200(type2, 1);
            timepoint3.m18200(type2, -1);
            if (type == null || timepoint2.m18198(type) == timepoint.m18198(type)) {
                Timepoint floor = this.f14265.floor(timepoint2);
                if (!timepoint2.m18201(this.f14265.ceiling(timepoint2), type2) && !timepoint2.m18201(floor, type2)) {
                    return timepoint2;
                }
            }
            if (type == null || timepoint3.m18198(type) == timepoint.m18198(type)) {
                Timepoint floor2 = this.f14265.floor(timepoint3);
                if (!timepoint3.m18201(this.f14265.ceiling(timepoint3), type2) && !timepoint3.m18201(floor2, type2)) {
                    return timepoint3;
                }
            }
            if (type != null && timepoint3.m18198(type) != timepoint.m18198(type) && timepoint2.m18198(type) != timepoint.m18198(type)) {
                break;
            }
        }
        return timepoint;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private TreeSet<Timepoint> m18092(TreeSet<Timepoint> treeSet, TreeSet<Timepoint> treeSet2) {
        TreeSet<Timepoint> treeSet3 = new TreeSet<>(treeSet);
        treeSet3.removeAll(treeSet2);
        return treeSet3;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f14266, i);
        parcel.writeParcelable(this.f14264, i);
        parcel.writeTypedArray((Parcelable[]) this.f14268.toArray(new Timepoint[this.f14268.size()]), i);
        parcel.writeTypedArray((Parcelable[]) this.f14265.toArray(new Timepoint[this.f14265.size()]), i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m18093() {
        Timepoint timepoint = new Timepoint(12);
        if (this.f14264 != null && this.f14264.compareTo(timepoint) < 0) {
            return true;
        }
        if (this.f14267.isEmpty()) {
            return false;
        }
        return this.f14267.last().compareTo(timepoint) < 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timepoint m18094(Timepoint timepoint, Timepoint.TYPE type, Timepoint.TYPE type2) {
        if (this.f14266 != null && this.f14266.compareTo(timepoint) > 0) {
            return this.f14266;
        }
        if (this.f14264 != null && this.f14264.compareTo(timepoint) < 0) {
            return this.f14264;
        }
        if (type == Timepoint.TYPE.SECOND) {
            return timepoint;
        }
        if (!this.f14267.isEmpty()) {
            Timepoint floor = this.f14267.floor(timepoint);
            Timepoint ceiling = this.f14267.ceiling(timepoint);
            if (floor == null || ceiling == null) {
                Timepoint timepoint2 = floor == null ? ceiling : floor;
                return type == null ? timepoint2 : timepoint2.m18197() == timepoint.m18197() ? (type != Timepoint.TYPE.MINUTE || timepoint2.m18194() == timepoint.m18194()) ? timepoint2 : timepoint : timepoint;
            }
            if (type == Timepoint.TYPE.HOUR) {
                if (floor.m18197() != timepoint.m18197() && ceiling.m18197() == timepoint.m18197()) {
                    return ceiling;
                }
                if (floor.m18197() == timepoint.m18197() && ceiling.m18197() != timepoint.m18197()) {
                    return floor;
                }
                if (!(floor.m18197() == timepoint.m18197() || ceiling.m18197() == timepoint.m18197())) {
                    return timepoint;
                }
            }
            if (type == Timepoint.TYPE.MINUTE) {
                if (floor.m18197() != timepoint.m18197() && ceiling.m18197() != timepoint.m18197()) {
                    return timepoint;
                }
                if (floor.m18197() != timepoint.m18197() && ceiling.m18197() == timepoint.m18197()) {
                    if (ceiling.m18194() != timepoint.m18194()) {
                        ceiling = timepoint;
                    }
                    return ceiling;
                } else if (floor.m18197() == timepoint.m18197() && ceiling.m18197() != timepoint.m18197()) {
                    if (floor.m18194() != timepoint.m18194()) {
                        floor = timepoint;
                    }
                    return floor;
                } else if (floor.m18194() != timepoint.m18194() && ceiling.m18194() == timepoint.m18194()) {
                    return ceiling;
                } else {
                    if (floor.m18194() == timepoint.m18194() && ceiling.m18194() != timepoint.m18194()) {
                        return floor;
                    }
                    if (!(floor.m18194() == timepoint.m18194() || ceiling.m18194() == timepoint.m18194())) {
                        return timepoint;
                    }
                }
            }
            if (Math.abs(timepoint.compareTo(floor)) >= Math.abs(timepoint.compareTo(ceiling))) {
                floor = ceiling;
            }
            return floor;
        } else if (this.f14265.isEmpty()) {
            return timepoint;
        } else {
            if (type != null && type == type2) {
                return timepoint;
            }
            if (type2 == Timepoint.TYPE.SECOND) {
                return this.f14265.contains(timepoint) ? m18091(timepoint, type, type2) : timepoint;
            }
            if (type2 == Timepoint.TYPE.MINUTE) {
                return (timepoint.m18201(this.f14265.ceiling(timepoint), Timepoint.TYPE.MINUTE) || timepoint.m18201(this.f14265.floor(timepoint), Timepoint.TYPE.MINUTE)) ? m18091(timepoint, type, type2) : timepoint;
            } else if (type2 != Timepoint.TYPE.HOUR) {
                return timepoint;
            } else {
                return (timepoint.m18201(this.f14265.ceiling(timepoint), Timepoint.TYPE.HOUR) || timepoint.m18201(this.f14265.floor(timepoint), Timepoint.TYPE.HOUR)) ? m18091(timepoint, type, type2) : timepoint;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18095() {
        Timepoint timepoint = new Timepoint(12);
        if (this.f14266 != null && this.f14266.compareTo(timepoint) >= 0) {
            return true;
        }
        if (this.f14267.isEmpty()) {
            return false;
        }
        return this.f14267.first().compareTo(timepoint) >= 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18096(Timepoint timepoint) {
        if (this.f14266 != null && this.f14266.compareTo(timepoint) > 0) {
            return true;
        }
        if (this.f14264 == null || this.f14264.compareTo(timepoint) >= 0) {
            return !this.f14267.isEmpty() ? !this.f14267.contains(timepoint) : this.f14265.contains(timepoint);
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18097(Timepoint timepoint, int i, Timepoint.TYPE type) {
        boolean z = false;
        if (timepoint == null) {
            return false;
        }
        if (i == 0) {
            if (this.f14266 != null && this.f14266.m18197() > timepoint.m18197()) {
                return true;
            }
            if (this.f14264 != null && this.f14264.m18197() + 1 <= timepoint.m18197()) {
                return true;
            }
            if (!this.f14267.isEmpty()) {
                return !timepoint.m18201(this.f14267.ceiling(timepoint), Timepoint.TYPE.HOUR) && !timepoint.m18201(this.f14267.floor(timepoint), Timepoint.TYPE.HOUR);
            } else if (this.f14265.isEmpty() || type != Timepoint.TYPE.HOUR) {
                return false;
            } else {
                Timepoint floor = this.f14265.floor(timepoint);
                if (timepoint.m18201(this.f14265.ceiling(timepoint), Timepoint.TYPE.HOUR) || timepoint.m18201(floor, Timepoint.TYPE.HOUR)) {
                    z = true;
                }
                return z;
            }
        } else if (i != 1) {
            return m18096(timepoint);
        } else {
            if (this.f14266 != null && new Timepoint(this.f14266.m18197(), this.f14266.m18194()).compareTo(timepoint) > 0) {
                return true;
            }
            if (this.f14264 != null && new Timepoint(this.f14264.m18197(), this.f14264.m18194(), 59).compareTo(timepoint) < 0) {
                return true;
            }
            if (!this.f14267.isEmpty()) {
                return !timepoint.m18201(this.f14267.ceiling(timepoint), Timepoint.TYPE.MINUTE) && !timepoint.m18201(this.f14267.floor(timepoint), Timepoint.TYPE.MINUTE);
            } else if (this.f14265.isEmpty() || type != Timepoint.TYPE.MINUTE) {
                return false;
            } else {
                boolean r1 = timepoint.m18201(this.f14265.ceiling(timepoint), Timepoint.TYPE.MINUTE);
                boolean r3 = timepoint.m18201(this.f14265.floor(timepoint), Timepoint.TYPE.MINUTE);
                if (r1 || r3) {
                    z = true;
                }
                return z;
            }
        }
    }
}
