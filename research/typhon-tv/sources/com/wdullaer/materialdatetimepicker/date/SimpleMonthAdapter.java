package com.wdullaer.materialdatetimepicker.date;

import android.content.Context;
import android.util.AttributeSet;

public class SimpleMonthAdapter extends MonthAdapter {
    public SimpleMonthAdapter(DatePickerController datePickerController) {
        super(datePickerController);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MonthView m18076(Context context) {
        return new SimpleMonthView(context, (AttributeSet) null, this.f14163);
    }
}
