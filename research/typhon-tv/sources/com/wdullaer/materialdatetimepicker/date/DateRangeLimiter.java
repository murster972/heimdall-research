package com.wdullaer.materialdatetimepicker.date;

import android.os.Parcelable;
import java.util.Calendar;

public interface DateRangeLimiter extends Parcelable {
    /* renamed from: 靐  reason: contains not printable characters */
    int m18004();

    /* renamed from: 麤  reason: contains not printable characters */
    Calendar m18005();

    /* renamed from: 齉  reason: contains not printable characters */
    Calendar m18006();

    /* renamed from: 龘  reason: contains not printable characters */
    int m18007();

    /* renamed from: 龘  reason: contains not printable characters */
    Calendar m18008(Calendar calendar);

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m18009(int i, int i2, int i3);
}
