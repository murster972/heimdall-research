package com.wdullaer.materialdatetimepicker.date;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.date.DayPickerView;

public class DayPickerGroup extends ViewGroup implements View.OnClickListener, DayPickerView.OnPageListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private ImageButton f14138;

    /* renamed from: 麤  reason: contains not printable characters */
    private DatePickerController f14139;

    /* renamed from: 齉  reason: contains not printable characters */
    private DayPickerView f14140;

    /* renamed from: 龘  reason: contains not printable characters */
    private ImageButton f14141;

    public DayPickerGroup(Context context) {
        super(context);
        m18010();
    }

    public DayPickerGroup(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m18010();
    }

    public DayPickerGroup(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m18010();
    }

    public DayPickerGroup(Context context, DatePickerController datePickerController) {
        super(context);
        this.f14139 = datePickerController;
        m18010();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m18010() {
        this.f14140 = new SimpleDayPickerView(getContext(), this.f14139);
        addView(this.f14140);
        ViewGroup viewGroup = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.mdtp_daypicker_group, this, false);
        while (viewGroup.getChildCount() > 0) {
            View childAt = viewGroup.getChildAt(0);
            viewGroup.removeViewAt(0);
            addView(childAt);
        }
        this.f14141 = (ImageButton) findViewById(R.id.mdtp_previous_month_arrow);
        this.f14138 = (ImageButton) findViewById(R.id.mdtp_next_month_arrow);
        if (this.f14139.m17964() == DatePickerDialog.Version.VERSION_1) {
            int r2 = Utils.m17951(16.0f, getResources());
            this.f14141.setMinimumHeight(r2);
            this.f14141.setMinimumWidth(r2);
            this.f14138.setMinimumHeight(r2);
            this.f14138.setMinimumWidth(r2);
        }
        this.f14141.setOnClickListener(this);
        this.f14138.setOnClickListener(this);
        this.f14140.setOnPageListener(this);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m18011(int i) {
        int i2 = 0;
        boolean z = this.f14139.m17963() == DatePickerDialog.ScrollOrientation.HORIZONTAL;
        boolean z2 = i > 0;
        boolean z3 = i < this.f14140.getCount() + -1;
        this.f14141.setVisibility((!z || !z2) ? 4 : 0);
        ImageButton imageButton = this.f14138;
        if (!z || !z3) {
            i2 = 4;
        }
        imageButton.setVisibility(i2);
    }

    public int getMostVisiblePosition() {
        return this.f14140.getMostVisiblePosition();
    }

    public void onClick(View view) {
        int i;
        if (this.f14138 == view) {
            i = 1;
        } else if (this.f14141 == view) {
            i = -1;
        } else {
            return;
        }
        int mostVisiblePosition = this.f14140.getMostVisiblePosition() + i;
        this.f14140.smoothScrollToPosition(mostVisiblePosition);
        m18011(mostVisiblePosition);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        ImageButton imageButton;
        ImageButton imageButton2;
        if (ViewCompat.getLayoutDirection(this) == 1) {
            imageButton = this.f14138;
            imageButton2 = this.f14141;
        } else {
            imageButton = this.f14141;
            imageButton2 = this.f14138;
        }
        int i5 = i3 - i;
        this.f14140.layout(0, 0, i5, i4 - i2);
        SimpleMonthView simpleMonthView = (SimpleMonthView) this.f14140.getChildAt(0);
        int monthHeight = simpleMonthView.getMonthHeight();
        int cellWidth = simpleMonthView.getCellWidth();
        int edgePadding = simpleMonthView.getEdgePadding();
        int measuredWidth = imageButton.getMeasuredWidth();
        int measuredHeight = imageButton.getMeasuredHeight();
        int paddingTop = simpleMonthView.getPaddingTop() + ((monthHeight - measuredHeight) / 2);
        int i6 = edgePadding + ((cellWidth - measuredWidth) / 2);
        imageButton.layout(i6, paddingTop, i6 + measuredWidth, paddingTop + measuredHeight);
        int measuredWidth2 = imageButton2.getMeasuredWidth();
        int measuredHeight2 = imageButton2.getMeasuredHeight();
        int paddingTop2 = simpleMonthView.getPaddingTop() + ((monthHeight - measuredHeight2) / 2);
        int i7 = ((i5 - edgePadding) - ((cellWidth - measuredWidth2) / 2)) - 2;
        imageButton2.layout(i7 - measuredWidth2, paddingTop2, i7, paddingTop2 + measuredHeight2);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        measureChild(this.f14140, i, i2);
        setMeasuredDimension(this.f14140.getMeasuredWidthAndState(), this.f14140.getMeasuredHeightAndState());
        int measuredWidth = this.f14140.getMeasuredWidth();
        int measuredHeight = this.f14140.getMeasuredHeight();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(measuredHeight, Integer.MIN_VALUE);
        this.f14141.measure(makeMeasureSpec, makeMeasureSpec2);
        this.f14138.measure(makeMeasureSpec, makeMeasureSpec2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18012(int i) {
        m18011(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18013() {
        this.f14140.m18022();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18014(int i) {
        this.f14140.m18023(i);
    }
}
