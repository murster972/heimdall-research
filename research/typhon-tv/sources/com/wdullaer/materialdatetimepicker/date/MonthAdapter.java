package com.wdullaer.materialdatetimepicker.date;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.AbsListView;
import com.wdullaer.materialdatetimepicker.date.MonthView;
import java.util.Calendar;
import java.util.TimeZone;

public abstract class MonthAdapter extends RecyclerView.Adapter<MonthViewHolder> implements MonthView.OnDayClickListener {

    /* renamed from: 靐  reason: contains not printable characters */
    private CalendarDay f14162;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final DatePickerController f14163;

    public static class CalendarDay {

        /* renamed from: 连任  reason: contains not printable characters */
        private Calendar f14164;

        /* renamed from: 靐  reason: contains not printable characters */
        int f14165;

        /* renamed from: 麤  reason: contains not printable characters */
        TimeZone f14166;

        /* renamed from: 齉  reason: contains not printable characters */
        int f14167;

        /* renamed from: 龘  reason: contains not printable characters */
        int f14168;

        public CalendarDay(int i, int i2, int i3, TimeZone timeZone) {
            this.f14166 = timeZone;
            m18050(i, i2, i3);
        }

        public CalendarDay(long j, TimeZone timeZone) {
            this.f14166 = timeZone;
            m18049(j);
        }

        public CalendarDay(Calendar calendar, TimeZone timeZone) {
            this.f14166 = timeZone;
            this.f14168 = calendar.get(1);
            this.f14165 = calendar.get(2);
            this.f14167 = calendar.get(5);
        }

        public CalendarDay(TimeZone timeZone) {
            this.f14166 = timeZone;
            m18049(System.currentTimeMillis());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m18049(long j) {
            if (this.f14164 == null) {
                this.f14164 = Calendar.getInstance(this.f14166);
            }
            this.f14164.setTimeInMillis(j);
            this.f14165 = this.f14164.get(2);
            this.f14168 = this.f14164.get(1);
            this.f14167 = this.f14164.get(5);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18050(int i, int i2, int i3) {
            this.f14168 = i;
            this.f14165 = i2;
            this.f14167 = i3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m18051(CalendarDay calendarDay) {
            this.f14168 = calendarDay.f14168;
            this.f14165 = calendarDay.f14165;
            this.f14167 = calendarDay.f14167;
        }
    }

    static class MonthViewHolder extends RecyclerView.ViewHolder {
        public MonthViewHolder(MonthView monthView) {
            super(monthView);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m18052(CalendarDay calendarDay, int i, int i2) {
            return calendarDay.f14168 == i && calendarDay.f14165 == i2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18053(int i, DatePickerController datePickerController, CalendarDay calendarDay) {
            int i2 = (datePickerController.m17961().get(2) + i) % 12;
            int r2 = ((datePickerController.m17961().get(2) + i) / 12) + datePickerController.m17968();
            int i3 = -1;
            if (m18052(calendarDay, r2, i2)) {
                i3 = calendarDay.f14167;
            }
            ((MonthView) this.itemView).setMonthParams(i3, r2, i2, datePickerController.m17971());
            this.itemView.invalidate();
        }
    }

    public MonthAdapter(DatePickerController datePickerController) {
        this.f14163 = datePickerController;
        m18045();
        m18046(this.f14163.m17974());
        setHasStableIds(true);
    }

    public int getItemCount() {
        Calendar r0 = this.f14163.m17962();
        Calendar r2 = this.f14163.m17961();
        return (((r0.get(1) * 12) + r0.get(2)) - ((r2.get(1) * 12) + r2.get(2))) + 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m18042(CalendarDay calendarDay) {
        this.f14163.m17965();
        this.f14163.m17976(calendarDay.f14168, calendarDay.f14165, calendarDay.f14167);
        m18046(calendarDay);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MonthViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        MonthView r1 = m18044(viewGroup.getContext());
        r1.setLayoutParams(new AbsListView.LayoutParams(-1, -1));
        r1.setClickable(true);
        r1.setOnDayClickListener(this);
        return new MonthViewHolder(r1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract MonthView m18044(Context context);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18045() {
        this.f14162 = new CalendarDay(System.currentTimeMillis(), this.f14163.m17966());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18046(CalendarDay calendarDay) {
        this.f14162 = calendarDay;
        notifyDataSetChanged();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void onBindViewHolder(MonthViewHolder monthViewHolder, int i) {
        monthViewHolder.m18053(i, this.f14163, this.f14162);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18048(MonthView monthView, CalendarDay calendarDay) {
        if (calendarDay != null) {
            m18042(calendarDay);
        }
    }
}
