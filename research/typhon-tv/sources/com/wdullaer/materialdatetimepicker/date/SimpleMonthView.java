package com.wdullaer.materialdatetimepicker.date;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class SimpleMonthView extends MonthView {
    public SimpleMonthView(Context context, AttributeSet attributeSet, DatePickerController datePickerController) {
        super(context, attributeSet, datePickerController);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18077(Canvas canvas, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        if (this.f14197 == i3) {
            canvas.drawCircle((float) i4, (float) (i5 - (f14177 / 3)), (float) f14171, this.f14210);
        }
        if (!m18068(i, i2, i3) || this.f14197 == i3) {
            this.f14182.setTypeface(Typeface.create(Typeface.DEFAULT, 0));
        } else {
            canvas.drawCircle((float) i4, (float) ((f14177 + i5) - f14173), (float) f14172, this.f14210);
            this.f14182.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        }
        if (this.f14201.m17973(i, i2, i3)) {
            this.f14182.setColor(this.f14181);
        } else if (this.f14197 == i3) {
            this.f14182.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
            this.f14182.setColor(this.f14212);
        } else if (!this.f14190 || this.f14198 != i3) {
            this.f14182.setColor(m18068(i, i2, i3) ? this.f14179 : this.f14209);
        } else {
            this.f14182.setColor(this.f14204);
        }
        canvas.drawText(String.format(this.f14201.m17967(), "%d", new Object[]{Integer.valueOf(i3)}), (float) i4, (float) i5, this.f14182);
    }
}
