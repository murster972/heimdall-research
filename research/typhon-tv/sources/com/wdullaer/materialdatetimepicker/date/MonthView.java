package com.wdullaer.materialdatetimepicker.date;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.date.MonthAdapter;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public abstract class MonthView extends View {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected static int f14169;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected static int f14170;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected static int f14171;

    /* renamed from: ˑ  reason: contains not printable characters */
    protected static int f14172;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected static int f14173;

    /* renamed from: 连任  reason: contains not printable characters */
    protected static int f14174;

    /* renamed from: 靐  reason: contains not printable characters */
    protected static int f14175 = 1;

    /* renamed from: 麤  reason: contains not printable characters */
    protected static int f14176;

    /* renamed from: 齉  reason: contains not printable characters */
    protected static int f14177;

    /* renamed from: 龘  reason: contains not printable characters */
    protected static int f14178 = 32;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    protected int f14179;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    protected int f14180;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    protected int f14181;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected Paint f14182;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private String f14183;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected Paint f14184;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private String f14185;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected int f14186;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private final Calendar f14187;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected int f14188;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private boolean f14189;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected boolean f14190;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private final MonthViewTouchHelper f14191;

    /* renamed from: ˊ  reason: contains not printable characters */
    protected int f14192;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private int f14193;

    /* renamed from: ˋ  reason: contains not printable characters */
    protected int f14194;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private SimpleDateFormat f14195;

    /* renamed from: ˎ  reason: contains not printable characters */
    protected int f14196;

    /* renamed from: ˏ  reason: contains not printable characters */
    protected int f14197;

    /* renamed from: י  reason: contains not printable characters */
    protected int f14198;

    /* renamed from: ـ  reason: contains not printable characters */
    protected int f14199;

    /* renamed from: ــ  reason: contains not printable characters */
    private final StringBuilder f14200;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected DatePickerController f14201;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    protected int f14202;

    /* renamed from: ᴵ  reason: contains not printable characters */
    protected int f14203;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    protected int f14204;

    /* renamed from: ᵎ  reason: contains not printable characters */
    protected int f14205;

    /* renamed from: ᵔ  reason: contains not printable characters */
    protected final Calendar f14206;

    /* renamed from: ᵢ  reason: contains not printable characters */
    protected int f14207;

    /* renamed from: ⁱ  reason: contains not printable characters */
    protected OnDayClickListener f14208;

    /* renamed from: ﹳ  reason: contains not printable characters */
    protected int f14209;

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected Paint f14210;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected Paint f14211;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    protected int f14212;

    protected class MonthViewTouchHelper extends ExploreByTouchHelper {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Rect f14213 = new Rect();

        /* renamed from: 齉  reason: contains not printable characters */
        private final Calendar f14214 = Calendar.getInstance(MonthView.this.f14201.m17966());

        MonthViewTouchHelper(View view) {
            super(view);
        }

        /* access modifiers changed from: protected */
        public int getVirtualViewAt(float f, float f2) {
            int r0 = MonthView.this.m18064(f, f2);
            if (r0 >= 0) {
                return r0;
            }
            return Integer.MIN_VALUE;
        }

        /* access modifiers changed from: protected */
        public void getVisibleVirtualViews(List<Integer> list) {
            for (int i = 1; i <= MonthView.this.f14205; i++) {
                list.add(Integer.valueOf(i));
            }
        }

        /* access modifiers changed from: protected */
        public boolean onPerformActionForVirtualView(int i, int i2, Bundle bundle) {
            switch (i2) {
                case 16:
                    MonthView.this.m18056(i);
                    return true;
                default:
                    return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPopulateEventForVirtualView(int i, AccessibilityEvent accessibilityEvent) {
            accessibilityEvent.setContentDescription(m18070(i));
        }

        /* access modifiers changed from: protected */
        public void onPopulateNodeForVirtualView(int i, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            m18073(i, this.f14213);
            accessibilityNodeInfoCompat.setContentDescription(m18070(i));
            accessibilityNodeInfoCompat.setBoundsInParent(this.f14213);
            accessibilityNodeInfoCompat.addAction(16);
            if (i == MonthView.this.f14197) {
                accessibilityNodeInfoCompat.setSelected(true);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public CharSequence m18070(int i) {
            this.f14214.set(MonthView.this.f14194, MonthView.this.f14192, i);
            CharSequence format = DateFormat.format("dd MMMM yyyy", this.f14214.getTimeInMillis());
            if (i != MonthView.this.f14197) {
                return format;
            }
            return MonthView.this.getContext().getString(R.string.mdtp_item_is_selected, new Object[]{format});
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18071() {
            int accessibilityFocusedVirtualViewId = getAccessibilityFocusedVirtualViewId();
            if (accessibilityFocusedVirtualViewId != Integer.MIN_VALUE) {
                getAccessibilityNodeProvider(MonthView.this).performAction(accessibilityFocusedVirtualViewId, 128, (Bundle) null);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18072(int i) {
            getAccessibilityNodeProvider(MonthView.this).performAction(i, 64, (Bundle) null);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m18073(int i, Rect rect) {
            int i2 = MonthView.this.f14188;
            int monthHeaderSize = MonthView.this.getMonthHeaderSize();
            int i3 = MonthView.this.f14186;
            int i4 = (MonthView.this.f14196 - (MonthView.this.f14188 * 2)) / MonthView.this.f14203;
            int r3 = (i - 1) + MonthView.this.m18059();
            int i5 = r3 / MonthView.this.f14203;
            int i6 = i2 + ((r3 % MonthView.this.f14203) * i4);
            int i7 = monthHeaderSize + (i5 * i3);
            rect.set(i6, i7, i6 + i4, i7 + i3);
        }
    }

    public interface OnDayClickListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m18074(MonthView monthView, MonthAdapter.CalendarDay calendarDay);
    }

    public MonthView(Context context) {
        this(context, (AttributeSet) null, (DatePickerController) null);
    }

    public MonthView(Context context, AttributeSet attributeSet, DatePickerController datePickerController) {
        super(context, attributeSet);
        this.f14188 = 0;
        this.f14186 = f14178;
        this.f14190 = false;
        this.f14197 = -1;
        this.f14198 = -1;
        this.f14199 = 1;
        this.f14203 = 7;
        this.f14205 = this.f14203;
        this.f14207 = 6;
        this.f14193 = 0;
        this.f14201 = datePickerController;
        Resources resources = context.getResources();
        this.f14206 = Calendar.getInstance(this.f14201.m17966(), this.f14201.m17967());
        this.f14187 = Calendar.getInstance(this.f14201.m17966(), this.f14201.m17967());
        this.f14185 = resources.getString(R.string.mdtp_day_of_week_label_typeface);
        this.f14183 = resources.getString(R.string.mdtp_sans_serif);
        if (this.f14201 != null && this.f14201.m17969()) {
            this.f14209 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_normal_dark_theme);
            this.f14202 = ContextCompat.getColor(context, R.color.mdtp_date_picker_month_day_dark_theme);
            this.f14181 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_disabled_dark_theme);
            this.f14179 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_highlighted_dark_theme);
        } else {
            this.f14209 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_normal);
            this.f14202 = ContextCompat.getColor(context, R.color.mdtp_date_picker_month_day);
            this.f14181 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_disabled);
            this.f14179 = ContextCompat.getColor(context, R.color.mdtp_date_picker_text_highlighted);
        }
        this.f14212 = ContextCompat.getColor(context, R.color.mdtp_white);
        this.f14204 = this.f14201.m17972();
        this.f14180 = ContextCompat.getColor(context, R.color.mdtp_white);
        this.f14200 = new StringBuilder(50);
        f14177 = resources.getDimensionPixelSize(R.dimen.mdtp_day_number_size);
        f14176 = resources.getDimensionPixelSize(R.dimen.mdtp_month_label_size);
        f14174 = resources.getDimensionPixelSize(R.dimen.mdtp_month_day_label_text_size);
        f14169 = resources.getDimensionPixelOffset(R.dimen.mdtp_month_list_item_header_height);
        f14170 = resources.getDimensionPixelOffset(R.dimen.mdtp_month_list_item_header_height_v2);
        f14171 = this.f14201.m17964() == DatePickerDialog.Version.VERSION_1 ? resources.getDimensionPixelSize(R.dimen.mdtp_day_number_select_circle_radius) : resources.getDimensionPixelSize(R.dimen.mdtp_day_number_select_circle_radius_v2);
        f14172 = resources.getDimensionPixelSize(R.dimen.mdtp_day_highlight_circle_radius);
        f14173 = resources.getDimensionPixelSize(R.dimen.mdtp_day_highlight_circle_margin);
        if (this.f14201.m17964() == DatePickerDialog.Version.VERSION_1) {
            this.f14186 = (resources.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height) - getMonthHeaderSize()) / 6;
        } else {
            this.f14186 = ((resources.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height_v2) - getMonthHeaderSize()) - (f14174 * 2)) / 6;
        }
        this.f14188 = this.f14201.m17964() == DatePickerDialog.Version.VERSION_1 ? 0 : context.getResources().getDimensionPixelSize(R.dimen.mdtp_date_picker_view_animator_padding_v2);
        this.f14191 = getMonthViewTouchHelper();
        ViewCompat.setAccessibilityDelegate(this, this.f14191);
        ViewCompat.setImportantForAccessibility(this, 1);
        this.f14189 = true;
        m18065();
    }

    private String getMonthAndYearString() {
        Locale r1 = this.f14201.m17967();
        String string = Build.VERSION.SDK_INT < 18 ? getContext().getResources().getString(R.string.mdtp_date_v1_monthyear) : DateFormat.getBestDateTimePattern(r1, "MMMM yyyy");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(string, r1);
        simpleDateFormat.setTimeZone(this.f14201.m17966());
        simpleDateFormat.applyLocalizedPattern(string);
        this.f14200.setLength(0);
        return simpleDateFormat.format(this.f14187.getTime());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m18054() {
        int r1 = m18059();
        return ((this.f14205 + r1) % this.f14203 > 0 ? 1 : 0) + ((this.f14205 + r1) / this.f14203);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private String m18055(Calendar calendar) {
        Locale r3 = this.f14201.m17967();
        if (Build.VERSION.SDK_INT < 18) {
            String format = new SimpleDateFormat("E", r3).format(calendar.getTime());
            String substring = format.toUpperCase(r3).substring(0, 1);
            if (r3.equals(Locale.CHINA) || r3.equals(Locale.CHINESE) || r3.equals(Locale.SIMPLIFIED_CHINESE) || r3.equals(Locale.TRADITIONAL_CHINESE)) {
                int length = format.length();
                substring = format.substring(length - 1, length);
            }
            if (r3.getLanguage().equals("he") || r3.getLanguage().equals("iw")) {
                if (this.f14206.get(7) != 7) {
                    int length2 = format.length();
                    substring = format.substring(length2 - 2, length2 - 1);
                } else {
                    substring = format.toUpperCase(r3).substring(0, 1);
                }
            }
            if (r3.getLanguage().equals("ca")) {
                substring = format.toLowerCase().substring(0, 2);
            }
            return (!r3.getLanguage().equals("es") || calendar.get(7) != 4) ? substring : "X";
        }
        if (this.f14195 == null) {
            this.f14195 = new SimpleDateFormat("EEEEE", r3);
        }
        return this.f14195.format(calendar.getTime());
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18056(int i) {
        if (!this.f14201.m17973(this.f14194, this.f14192, i)) {
            if (this.f14208 != null) {
                this.f14208.m18074(this, new MonthAdapter.CalendarDay(this.f14194, this.f14192, i, this.f14201.m17966()));
            }
            this.f14191.sendEventForVirtualView(i, 1);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m18058(int i, Calendar calendar) {
        return this.f14194 == calendar.get(1) && this.f14192 == calendar.get(2) && i == calendar.get(5);
    }

    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        return this.f14191.dispatchHoverEvent(motionEvent) || super.dispatchHoverEvent(motionEvent);
    }

    public MonthAdapter.CalendarDay getAccessibilityFocus() {
        int accessibilityFocusedVirtualViewId = this.f14191.getAccessibilityFocusedVirtualViewId();
        if (accessibilityFocusedVirtualViewId >= 0) {
            return new MonthAdapter.CalendarDay(this.f14194, this.f14192, accessibilityFocusedVirtualViewId, this.f14201.m17966());
        }
        return null;
    }

    public int getCellWidth() {
        return (this.f14196 - (this.f14188 * 2)) / this.f14203;
    }

    public int getEdgePadding() {
        return this.f14188;
    }

    public int getMonth() {
        return this.f14192;
    }

    /* access modifiers changed from: protected */
    public int getMonthHeaderSize() {
        return this.f14201.m17964() == DatePickerDialog.Version.VERSION_1 ? f14169 : f14170;
    }

    public int getMonthHeight() {
        return getMonthHeaderSize() - (f14174 * (this.f14201.m17964() == DatePickerDialog.Version.VERSION_1 ? 2 : 3));
    }

    /* access modifiers changed from: protected */
    public MonthViewTouchHelper getMonthViewTouchHelper() {
        return new MonthViewTouchHelper(this);
    }

    public int getYear() {
        return this.f14194;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        m18066(canvas);
        m18061(canvas);
        m18063(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(View.MeasureSpec.getSize(i), (this.f14186 * this.f14207) + getMonthHeaderSize());
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.f14196 = i;
        this.f14191.invalidateRoot();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
                int r0 = m18064(motionEvent.getX(), motionEvent.getY());
                if (r0 < 0) {
                    return true;
                }
                m18056(r0);
                return true;
            default:
                return true;
        }
    }

    public void setAccessibilityDelegate(View.AccessibilityDelegate accessibilityDelegate) {
        if (!this.f14189) {
            super.setAccessibilityDelegate(accessibilityDelegate);
        }
    }

    public void setMonthParams(int i, int i2, int i3, int i4) {
        if (i3 == -1 && i2 == -1) {
            throw new InvalidParameterException("You must specify month and year for this view");
        }
        this.f14197 = i;
        this.f14192 = i3;
        this.f14194 = i2;
        Calendar instance = Calendar.getInstance(this.f14201.m17966(), this.f14201.m17967());
        this.f14190 = false;
        this.f14198 = -1;
        this.f14187.set(2, this.f14192);
        this.f14187.set(1, this.f14194);
        this.f14187.set(5, 1);
        this.f14193 = this.f14187.get(7);
        if (i4 != -1) {
            this.f14199 = i4;
        } else {
            this.f14199 = this.f14187.getFirstDayOfWeek();
        }
        this.f14205 = this.f14187.getActualMaximum(5);
        for (int i5 = 0; i5 < this.f14205; i5++) {
            int i6 = i5 + 1;
            if (m18058(i6, instance)) {
                this.f14190 = true;
                this.f14198 = i6;
            }
        }
        this.f14207 = m18054();
        this.f14191.invalidateRoot();
    }

    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        this.f14208 = onDayClickListener;
    }

    public void setSelectedDay(int i) {
        this.f14197 = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m18059() {
        return (this.f14193 < this.f14199 ? this.f14193 + this.f14203 : this.f14193) - this.f14199;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m18060(float f, float f2) {
        int i = this.f14188;
        if (f < ((float) i) || f > ((float) (this.f14196 - this.f14188))) {
            return -1;
        }
        return (((int) (((f - ((float) i)) * ((float) this.f14203)) / ((float) ((this.f14196 - i) - this.f14188)))) - m18059()) + 1 + (this.f14203 * (((int) (f2 - ((float) getMonthHeaderSize()))) / this.f14186));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m18061(Canvas canvas) {
        int monthHeaderSize = getMonthHeaderSize() - (f14174 / 2);
        int i = (this.f14196 - (this.f14188 * 2)) / (this.f14203 * 2);
        for (int i2 = 0; i2 < this.f14203; i2++) {
            int i3 = (((i2 * 2) + 1) * i) + this.f14188;
            this.f14206.set(7, (this.f14199 + i2) % this.f14203);
            canvas.drawText(m18055(this.f14206), (float) i3, (float) monthHeaderSize, this.f14211);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m18062() {
        this.f14191.m18071();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m18063(Canvas canvas) {
        int monthHeaderSize = (((this.f14186 + f14177) / 2) - f14175) + getMonthHeaderSize();
        int i = (this.f14196 - (this.f14188 * 2)) / (this.f14203 * 2);
        int r12 = m18059();
        for (int i2 = 1; i2 <= this.f14205; i2++) {
            int i3 = (((r12 * 2) + 1) * i) + this.f14188;
            int i4 = monthHeaderSize - (((this.f14186 + f14177) / 2) - f14175);
            int i5 = i4 + this.f14186;
            Canvas canvas2 = canvas;
            m18067(canvas2, this.f14194, this.f14192, i2, i3, monthHeaderSize, i3 - i, i3 + i, i4, i5);
            r12++;
            if (r12 == this.f14203) {
                r12 = 0;
                monthHeaderSize += this.f14186;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m18064(float f, float f2) {
        int r0 = m18060(f, f2);
        if (r0 < 1 || r0 > this.f14205) {
            return -1;
        }
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18065() {
        this.f14184 = new Paint();
        if (this.f14201.m17964() == DatePickerDialog.Version.VERSION_1) {
            this.f14184.setFakeBoldText(true);
        }
        this.f14184.setAntiAlias(true);
        this.f14184.setTextSize((float) f14176);
        this.f14184.setTypeface(Typeface.create(this.f14183, 1));
        this.f14184.setColor(this.f14209);
        this.f14184.setTextAlign(Paint.Align.CENTER);
        this.f14184.setStyle(Paint.Style.FILL);
        this.f14210 = new Paint();
        this.f14210.setFakeBoldText(true);
        this.f14210.setAntiAlias(true);
        this.f14210.setColor(this.f14204);
        this.f14210.setTextAlign(Paint.Align.CENTER);
        this.f14210.setStyle(Paint.Style.FILL);
        this.f14210.setAlpha(255);
        this.f14211 = new Paint();
        this.f14211.setAntiAlias(true);
        this.f14211.setTextSize((float) f14174);
        this.f14211.setColor(this.f14202);
        this.f14184.setTypeface(Typeface.create(this.f14185, 1));
        this.f14211.setStyle(Paint.Style.FILL);
        this.f14211.setTextAlign(Paint.Align.CENTER);
        this.f14211.setFakeBoldText(true);
        this.f14182 = new Paint();
        this.f14182.setAntiAlias(true);
        this.f14182.setTextSize((float) f14177);
        this.f14182.setStyle(Paint.Style.FILL);
        this.f14182.setTextAlign(Paint.Align.CENTER);
        this.f14182.setFakeBoldText(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18066(Canvas canvas) {
        canvas.drawText(getMonthAndYearString(), (float) (this.f14196 / 2), (float) (this.f14201.m17964() == DatePickerDialog.Version.VERSION_1 ? (getMonthHeaderSize() - f14174) / 2 : (getMonthHeaderSize() / 2) - f14174), this.f14184);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m18067(Canvas canvas, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18068(int i, int i2, int i3) {
        return this.f14201.m17970(i, i2, i3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18069(MonthAdapter.CalendarDay calendarDay) {
        if (calendarDay.f14168 != this.f14194 || calendarDay.f14165 != this.f14192 || calendarDay.f14167 > this.f14205) {
            return false;
        }
        this.f14191.m18072(calendarDay.f14167);
        return true;
    }
}
