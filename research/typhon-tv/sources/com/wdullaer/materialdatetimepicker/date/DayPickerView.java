package com.wdullaer.materialdatetimepicker.date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import com.wdullaer.materialdatetimepicker.GravitySnapHelper;
import com.wdullaer.materialdatetimepicker.Utils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.date.MonthAdapter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

public abstract class DayPickerView extends RecyclerView implements DatePickerDialog.OnDateChangedListener {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static SimpleDateFormat f14142 = new SimpleDateFormat("yyyy", Locale.getDefault());

    /* renamed from: ʻ  reason: contains not printable characters */
    protected int f14143;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected int f14144 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public OnPageListener f14145;

    /* renamed from: ٴ  reason: contains not printable characters */
    private DatePickerController f14146;

    /* renamed from: 连任  reason: contains not printable characters */
    protected MonthAdapter.CalendarDay f14147;

    /* renamed from: 靐  reason: contains not printable characters */
    protected Handler f14148;

    /* renamed from: 麤  reason: contains not printable characters */
    protected MonthAdapter f14149;

    /* renamed from: 齉  reason: contains not printable characters */
    protected MonthAdapter.CalendarDay f14150;

    /* renamed from: 龘  reason: contains not printable characters */
    protected Context f14151;

    public interface OnPageListener {
        /* renamed from: 靐  reason: contains not printable characters */
        void m18027(int i);
    }

    public DayPickerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m18024(context);
    }

    public DayPickerView(Context context, DatePickerController datePickerController) {
        super(context);
        setController(datePickerController);
        m18024(context);
    }

    private int getFirstVisiblePosition() {
        return getChildAdapterPosition(getChildAt(0));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private MonthAdapter.CalendarDay m18015() {
        MonthAdapter.CalendarDay accessibilityFocus;
        int childCount = getChildCount();
        int i = 0;
        while (i < childCount) {
            View childAt = getChildAt(i);
            if (!(childAt instanceof MonthView) || (accessibilityFocus = ((MonthView) childAt).getAccessibilityFocus()) == null) {
                i++;
            } else if (Build.VERSION.SDK_INT != 17) {
                return accessibilityFocus;
            } else {
                ((MonthView) childAt).m18062();
                return accessibilityFocus;
            }
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m18017(MonthAdapter.CalendarDay calendarDay, Locale locale) {
        Calendar instance = Calendar.getInstance();
        instance.set(calendarDay.f14168, calendarDay.f14165, calendarDay.f14167);
        return (("" + instance.getDisplayName(2, 2, locale)) + StringUtils.SPACE) + f14142.format(instance.getTime());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m18018(MonthAdapter.CalendarDay calendarDay) {
        if (calendarDay == null) {
            return false;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((childAt instanceof MonthView) && ((MonthView) childAt).m18069(calendarDay)) {
                return true;
            }
        }
        return false;
    }

    public int getCount() {
        return this.f14149.getItemCount();
    }

    public MonthView getMostVisibleMonth() {
        boolean z = this.f14146.m17963() == DatePickerDialog.ScrollOrientation.VERTICAL;
        int height = z ? getHeight() : getWidth();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        MonthView monthView = null;
        while (i3 < height) {
            View childAt = getChildAt(i2);
            if (childAt == null) {
                break;
            }
            i3 = z ? childAt.getBottom() : childAt.getRight();
            int min = Math.min(i3, height) - Math.max(0, z ? childAt.getTop() : childAt.getLeft());
            if (min > i) {
                monthView = (MonthView) childAt;
                i = min;
            }
            i2++;
        }
        return monthView;
    }

    public int getMostVisiblePosition() {
        return getChildAdapterPosition(getMostVisibleMonth());
    }

    public OnPageListener getOnPageListener() {
        return this.f14145;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setItemCount(-1);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        if (Build.VERSION.SDK_INT >= 21) {
            accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD);
            accessibilityNodeInfo.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD);
            return;
        }
        accessibilityNodeInfo.addAction(4096);
        accessibilityNodeInfo.addAction(8192);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        m18018(m18015());
    }

    @SuppressLint({"NewApi"})
    public boolean performAccessibilityAction(int i, Bundle bundle) {
        View childAt;
        if (i != 4096 && i != 8192) {
            return super.performAccessibilityAction(i, bundle);
        }
        int firstVisiblePosition = getFirstVisiblePosition();
        int i2 = this.f14146.m17961().get(2);
        MonthAdapter.CalendarDay calendarDay = new MonthAdapter.CalendarDay(((firstVisiblePosition + i2) / 12) + this.f14146.m17968(), (firstVisiblePosition + i2) % 12, 1, this.f14146.m17966());
        if (i == 4096) {
            calendarDay.f14165++;
            if (calendarDay.f14165 == 12) {
                calendarDay.f14165 = 0;
                calendarDay.f14168++;
            }
        } else if (i == 8192 && (childAt = getChildAt(0)) != null && childAt.getTop() >= -1) {
            calendarDay.f14165--;
            if (calendarDay.f14165 == -1) {
                calendarDay.f14165 = 11;
                calendarDay.f14168--;
            }
        }
        Utils.m17956((View) this, (CharSequence) m18017(calendarDay, this.f14146.m17967()));
        m18025(calendarDay, true, false, true);
        return true;
    }

    public void setController(DatePickerController datePickerController) {
        this.f14146 = datePickerController;
        this.f14146.m17977((DatePickerDialog.OnDateChangedListener) this);
        this.f14150 = new MonthAdapter.CalendarDay(this.f14146.m17966());
        this.f14147 = new MonthAdapter.CalendarDay(this.f14146.m17966());
        f14142 = new SimpleDateFormat("yyyy", datePickerController.m17967());
        m18020();
        m18022();
    }

    /* access modifiers changed from: protected */
    public void setMonthDisplayed(MonthAdapter.CalendarDay calendarDay) {
        this.f14143 = calendarDay.f14165;
    }

    public void setOnPageListener(OnPageListener onPageListener) {
        this.f14145 = onPageListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m18019() {
        setVerticalScrollBarEnabled(false);
        setFadingEdgeLength(0);
        new GravitySnapHelper(this.f14146.m17963() == DatePickerDialog.ScrollOrientation.VERTICAL ? 48 : GravityCompat.START, new GravitySnapHelper.SnapListener() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m18026(int i) {
                if (DayPickerView.this.f14145 != null) {
                    DayPickerView.this.f14145.m18027(i);
                }
            }
        }).attachToRecyclerView(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m18020() {
        if (this.f14149 == null) {
            this.f14149 = m18021(this.f14146);
        } else {
            this.f14149.m18046(this.f14150);
            if (this.f14145 != null) {
                this.f14145.m18027(getMostVisiblePosition());
            }
        }
        setAdapter(this.f14149);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract MonthAdapter m18021(DatePickerController datePickerController);

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18022() {
        m18025(this.f14146.m17974(), false, true, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18023(final int i) {
        clearFocus();
        post(new Runnable() {
            public void run() {
                ((LinearLayoutManager) DayPickerView.this.getLayoutManager()).scrollToPositionWithOffset(i, 0);
                if (DayPickerView.this.f14145 != null) {
                    DayPickerView.this.f14145.m18027(i);
                }
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18024(Context context) {
        setLayoutManager(new LinearLayoutManager(context, this.f14146.m17963() == DatePickerDialog.ScrollOrientation.VERTICAL ? 1 : 0, false));
        this.f14148 = new Handler();
        setLayoutParams(new RecyclerView.LayoutParams(-1, -1));
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setClipChildren(false);
        this.f14151 = context;
        m18019();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18025(MonthAdapter.CalendarDay calendarDay, boolean z, boolean z2, boolean z3) {
        View childAt;
        if (z2) {
            this.f14150.m18051(calendarDay);
        }
        this.f14147.m18051(calendarDay);
        int r4 = (((calendarDay.f14168 - this.f14146.m17968()) * 12) + calendarDay.f14165) - this.f14146.m17961().get(2);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            childAt = getChildAt(i);
            if (childAt != null) {
                int top = childAt.getTop();
                if (Log.isLoggable("MonthFragment", 3)) {
                    Log.d("MonthFragment", "child at " + (i2 - 1) + " has top " + top);
                }
                if (top >= 0) {
                    break;
                }
                i = i2;
            } else {
                break;
            }
        }
        int childAdapterPosition = childAt != null ? getChildAdapterPosition(childAt) : 0;
        if (z2) {
            this.f14149.m18046(this.f14150);
        }
        if (Log.isLoggable("MonthFragment", 3)) {
            Log.d("MonthFragment", "GoTo position " + r4);
        }
        if (r4 != childAdapterPosition || z3) {
            setMonthDisplayed(this.f14147);
            this.f14144 = 1;
            if (z) {
                smoothScrollToPosition(r4);
                if (this.f14145 != null) {
                    this.f14145.m18027(r4);
                }
                return true;
            }
            m18023(r4);
        } else if (z2) {
            setMonthDisplayed(this.f14150);
        }
        return false;
    }
}
