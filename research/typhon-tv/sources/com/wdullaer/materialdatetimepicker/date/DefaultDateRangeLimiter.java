package com.wdullaer.materialdatetimepicker.date;

import android.os.Parcel;
import android.os.Parcelable;
import com.wdullaer.materialdatetimepicker.Utils;
import java.util.Calendar;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.TreeSet;

class DefaultDateRangeLimiter implements DateRangeLimiter {
    public static final Parcelable.Creator<DefaultDateRangeLimiter> CREATOR = new Parcelable.Creator<DefaultDateRangeLimiter>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public DefaultDateRangeLimiter createFromParcel(Parcel parcel) {
            return new DefaultDateRangeLimiter(parcel);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public DefaultDateRangeLimiter[] newArray(int i) {
            return new DefaultDateRangeLimiter[i];
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private TreeSet<Calendar> f14155 = new TreeSet<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private HashSet<Calendar> f14156 = new HashSet<>();

    /* renamed from: 连任  reason: contains not printable characters */
    private Calendar f14157;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f14158 = 1900;

    /* renamed from: 麤  reason: contains not printable characters */
    private Calendar f14159;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f14160 = 2100;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient DatePickerController f14161;

    DefaultDateRangeLimiter() {
    }

    public DefaultDateRangeLimiter(Parcel parcel) {
        this.f14158 = parcel.readInt();
        this.f14160 = parcel.readInt();
        this.f14159 = (Calendar) parcel.readSerializable();
        this.f14157 = (Calendar) parcel.readSerializable();
        this.f14155 = (TreeSet) parcel.readSerializable();
        this.f14156 = (HashSet) parcel.readSerializable();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean m18028(Calendar calendar) {
        return (this.f14157 != null && calendar.after(this.f14157)) || calendar.get(1) > this.f14160;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean m18029(Calendar calendar) {
        return (this.f14159 != null && calendar.before(this.f14159)) || calendar.get(1) < this.f14158;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m18030(Calendar calendar) {
        Utils.m17955(calendar);
        return m18032(calendar) || !m18031(calendar);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m18031(Calendar calendar) {
        return this.f14155.isEmpty() || this.f14155.contains(Utils.m17955(calendar));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m18032(Calendar calendar) {
        return this.f14156.contains(Utils.m17955(calendar)) || m18029(calendar) || m18028(calendar);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f14158);
        parcel.writeInt(this.f14160);
        parcel.writeSerializable(this.f14159);
        parcel.writeSerializable(this.f14157);
        parcel.writeSerializable(this.f14155);
        parcel.writeSerializable(this.f14156);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m18033() {
        return !this.f14155.isEmpty() ? this.f14155.last().get(1) : (this.f14157 == null || this.f14157.get(1) >= this.f14160) ? this.f14160 : this.f14157.get(1);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Calendar m18034() {
        if (!this.f14155.isEmpty()) {
            return (Calendar) this.f14155.last().clone();
        }
        if (this.f14157 != null) {
            return (Calendar) this.f14157.clone();
        }
        Calendar instance = Calendar.getInstance(this.f14161 == null ? TimeZone.getDefault() : this.f14161.m17966());
        instance.set(1, this.f14160);
        instance.set(5, 31);
        instance.set(2, 11);
        return instance;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Calendar m18035() {
        if (!this.f14155.isEmpty()) {
            return (Calendar) this.f14155.first().clone();
        }
        if (this.f14159 != null) {
            return (Calendar) this.f14159.clone();
        }
        Calendar instance = Calendar.getInstance(this.f14161 == null ? TimeZone.getDefault() : this.f14161.m17966());
        instance.set(1, this.f14158);
        instance.set(5, 1);
        instance.set(2, 0);
        return instance;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m18036() {
        return !this.f14155.isEmpty() ? this.f14155.first().get(1) : (this.f14159 == null || this.f14159.get(1) <= this.f14158) ? this.f14158 : this.f14159.get(1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Calendar m18037(Calendar calendar) {
        if (!this.f14155.isEmpty()) {
            Calendar calendar2 = null;
            Calendar ceiling = this.f14155.ceiling(calendar);
            Calendar lower = this.f14155.lower(calendar);
            if (ceiling == null && lower != null) {
                calendar2 = lower;
            } else if (lower == null && ceiling != null) {
                calendar2 = ceiling;
            }
            if (calendar2 != null || ceiling == null) {
                if (calendar2 == null) {
                    calendar2 = calendar;
                }
                calendar2.setTimeZone(this.f14161 == null ? TimeZone.getDefault() : this.f14161.m17966());
                return (Calendar) calendar2.clone();
            }
            return Math.abs(calendar.getTimeInMillis() - lower.getTimeInMillis()) < Math.abs(ceiling.getTimeInMillis() - calendar.getTimeInMillis()) ? (Calendar) lower.clone() : (Calendar) ceiling.clone();
        }
        if (!this.f14156.isEmpty()) {
            Calendar r3 = m18029(calendar) ? m18035() : (Calendar) calendar.clone();
            Calendar r2 = m18028(calendar) ? m18034() : (Calendar) calendar.clone();
            while (m18032(r3) && m18032(r2)) {
                r3.add(5, 1);
                r2.add(5, -1);
            }
            if (!m18032(r2)) {
                return r2;
            }
            if (!m18032(r3)) {
                return r3;
            }
        }
        TimeZone timeZone = this.f14161 == null ? TimeZone.getDefault() : this.f14161.m17966();
        if (m18029(calendar)) {
            if (this.f14159 != null) {
                return (Calendar) this.f14159.clone();
            }
            Calendar instance = Calendar.getInstance(timeZone);
            instance.set(1, this.f14158);
            instance.set(2, 0);
            instance.set(5, 1);
            return Utils.m17955(instance);
        } else if (!m18028(calendar)) {
            return calendar;
        } else {
            if (this.f14157 != null) {
                return (Calendar) this.f14157.clone();
            }
            Calendar instance2 = Calendar.getInstance(timeZone);
            instance2.set(1, this.f14160);
            instance2.set(2, 11);
            instance2.set(5, 31);
            return Utils.m17955(instance2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18038(DatePickerController datePickerController) {
        this.f14161 = datePickerController;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m18039(int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance(this.f14161 == null ? TimeZone.getDefault() : this.f14161.m17966());
        instance.set(1, i);
        instance.set(2, i2);
        instance.set(5, i3);
        return m18030(instance);
    }
}
