package com.wdullaer.materialdatetimepicker.date;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.TextView;
import com.wdullaer.materialdatetimepicker.R;

public class TextViewWithCircularIndicator extends TextView {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f14216;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f14217;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f14218;

    /* renamed from: 龘  reason: contains not printable characters */
    Paint f14219 = new Paint();

    public TextViewWithCircularIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f14216 = ContextCompat.getColor(context, R.color.mdtp_accent_color);
        this.f14218 = context.getResources().getString(R.string.mdtp_item_is_selected);
        m18079();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ColorStateList m18078(int i, boolean z) {
        int i2 = -1;
        int[][] iArr = {new int[]{16842919}, new int[]{16842913}, new int[0]};
        int[] iArr2 = new int[3];
        iArr2[0] = i;
        iArr2[1] = -1;
        if (!z) {
            i2 = -16777216;
        }
        iArr2[2] = i2;
        return new ColorStateList(iArr, iArr2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18079() {
        this.f14219.setFakeBoldText(true);
        this.f14219.setAntiAlias(true);
        this.f14219.setColor(this.f14216);
        this.f14219.setTextAlign(Paint.Align.CENTER);
        this.f14219.setStyle(Paint.Style.FILL);
        this.f14219.setAlpha(255);
    }

    public CharSequence getContentDescription() {
        CharSequence text = getText();
        if (!this.f14217) {
            return text;
        }
        return String.format(this.f14218, new Object[]{text});
    }

    public void onDraw(Canvas canvas) {
        if (this.f14217) {
            int width = getWidth();
            int height = getHeight();
            canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (Math.min(width, height) / 2), this.f14219);
        }
        setSelected(this.f14217);
        super.onDraw(canvas);
    }

    public void setAccentColor(int i, boolean z) {
        this.f14216 = i;
        this.f14219.setColor(this.f14216);
        setTextColor(m18078(i, z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18080(boolean z) {
        this.f14217 = z;
    }
}
