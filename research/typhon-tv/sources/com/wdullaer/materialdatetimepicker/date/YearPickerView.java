package com.wdullaer.materialdatetimepicker.date;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

public class YearPickerView extends ListView implements AdapterView.OnItemClickListener, DatePickerDialog.OnDateChangedListener {
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public TextViewWithCircularIndicator f14220;

    /* renamed from: 靐  reason: contains not printable characters */
    private YearAdapter f14221;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f14222;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f14223;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final DatePickerController f14224;

    private final class YearAdapter extends BaseAdapter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f14228;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f14229;

        YearAdapter(int i, int i2) {
            if (i > i2) {
                throw new IllegalArgumentException("minYear > maxYear");
            }
            this.f14228 = i;
            this.f14229 = i2;
        }

        public int getCount() {
            return (this.f14229 - this.f14228) + 1;
        }

        public Object getItem(int i) {
            return Integer.valueOf(this.f14228 + i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            TextViewWithCircularIndicator textViewWithCircularIndicator;
            if (view != null) {
                textViewWithCircularIndicator = (TextViewWithCircularIndicator) view;
            } else {
                textViewWithCircularIndicator = (TextViewWithCircularIndicator) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mdtp_year_label_text_view, viewGroup, false);
                textViewWithCircularIndicator.setAccentColor(YearPickerView.this.f14224.m17972(), YearPickerView.this.f14224.m17969());
            }
            int i2 = this.f14228 + i;
            boolean z = YearPickerView.this.f14224.m17974().f14168 == i2;
            textViewWithCircularIndicator.setText(String.format(YearPickerView.this.f14224.m17967(), "%d", new Object[]{Integer.valueOf(i2)}));
            textViewWithCircularIndicator.m18080(z);
            textViewWithCircularIndicator.requestLayout();
            if (z) {
                TextViewWithCircularIndicator unused = YearPickerView.this.f14220 = textViewWithCircularIndicator;
            }
            return textViewWithCircularIndicator;
        }
    }

    public YearPickerView(Context context, DatePickerController datePickerController) {
        super(context);
        this.f14224 = datePickerController;
        this.f14224.m17977((DatePickerDialog.OnDateChangedListener) this);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        Resources resources = context.getResources();
        this.f14223 = this.f14224.m17964() == DatePickerDialog.Version.VERSION_1 ? resources.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height) : resources.getDimensionPixelOffset(R.dimen.mdtp_date_picker_view_animator_height_v2);
        this.f14222 = resources.getDimensionPixelOffset(R.dimen.mdtp_year_label_height);
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(this.f14222 / 3);
        m18081();
        setOnItemClickListener(this);
        setSelector(new StateListDrawable());
        setDividerHeight(0);
        m18085();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m18081() {
        this.f14221 = new YearAdapter(this.f14224.m17968(), this.f14224.m17960());
        setAdapter(this.f14221);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m18082(TextView textView) {
        return Integer.valueOf(textView.getText().toString()).intValue();
    }

    public int getFirstPositionOffset() {
        View childAt = getChildAt(0);
        if (childAt == null) {
            return 0;
        }
        return childAt.getTop();
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        if (accessibilityEvent.getEventType() == 4096) {
            accessibilityEvent.setFromIndex(0);
            accessibilityEvent.setToIndex(0);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f14224.m17965();
        TextViewWithCircularIndicator textViewWithCircularIndicator = (TextViewWithCircularIndicator) view;
        if (textViewWithCircularIndicator != null) {
            if (textViewWithCircularIndicator != this.f14220) {
                if (this.f14220 != null) {
                    this.f14220.m18080(false);
                    this.f14220.requestLayout();
                }
                textViewWithCircularIndicator.m18080(true);
                textViewWithCircularIndicator.requestLayout();
                this.f14220 = textViewWithCircularIndicator;
            }
            this.f14224.m17975(m18082((TextView) textViewWithCircularIndicator));
            this.f14221.notifyDataSetChanged();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18085() {
        this.f14221.notifyDataSetChanged();
        m18086(this.f14224.m17974().f14168 - this.f14224.m17968());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18086(int i) {
        m18087(i, (this.f14223 / 2) - (this.f14222 / 2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18087(final int i, final int i2) {
        post(new Runnable() {
            public void run() {
                YearPickerView.this.setSelectionFromTop(i, i2);
                YearPickerView.this.requestLayout();
            }
        });
    }
}
