package com.wdullaer.materialdatetimepicker.date;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.date.MonthAdapter;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public interface DatePickerController {
    /* renamed from: ʻ  reason: contains not printable characters */
    int m17960();

    /* renamed from: ʼ  reason: contains not printable characters */
    Calendar m17961();

    /* renamed from: ʽ  reason: contains not printable characters */
    Calendar m17962();

    /* renamed from: ʾ  reason: contains not printable characters */
    DatePickerDialog.ScrollOrientation m17963();

    /* renamed from: ˈ  reason: contains not printable characters */
    DatePickerDialog.Version m17964();

    /* renamed from: ˑ  reason: contains not printable characters */
    void m17965();

    /* renamed from: ٴ  reason: contains not printable characters */
    TimeZone m17966();

    /* renamed from: ᐧ  reason: contains not printable characters */
    Locale m17967();

    /* renamed from: 连任  reason: contains not printable characters */
    int m17968();

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m17969();

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m17970(int i, int i2, int i3);

    /* renamed from: 麤  reason: contains not printable characters */
    int m17971();

    /* renamed from: 齉  reason: contains not printable characters */
    int m17972();

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m17973(int i, int i2, int i3);

    /* renamed from: 龘  reason: contains not printable characters */
    MonthAdapter.CalendarDay m17974();

    /* renamed from: 龘  reason: contains not printable characters */
    void m17975(int i);

    /* renamed from: 龘  reason: contains not printable characters */
    void m17976(int i, int i2, int i3);

    /* renamed from: 龘  reason: contains not printable characters */
    void m17977(DatePickerDialog.OnDateChangedListener onDateChangedListener);
}
