package com.wdullaer.materialdatetimepicker.date;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wdullaer.materialdatetimepicker.HapticFeedbackController;
import com.wdullaer.materialdatetimepicker.R;
import com.wdullaer.materialdatetimepicker.TypefaceHelper;
import com.wdullaer.materialdatetimepicker.Utils;
import com.wdullaer.materialdatetimepicker.date.MonthAdapter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import net.pubnative.library.request.PubnativeAsset;
import net.pubnative.library.request.PubnativeRequest;

public class DatePickerDialog extends DialogFragment implements View.OnClickListener, DatePickerController {

    /* renamed from: 靐  reason: contains not printable characters */
    private static SimpleDateFormat f14088 = new SimpleDateFormat("MMM", Locale.getDefault());

    /* renamed from: 麤  reason: contains not printable characters */
    private static SimpleDateFormat f14089;

    /* renamed from: 齉  reason: contains not printable characters */
    private static SimpleDateFormat f14090 = new SimpleDateFormat("dd", Locale.getDefault());

    /* renamed from: 龘  reason: contains not printable characters */
    private static SimpleDateFormat f14091 = new SimpleDateFormat("yyyy", Locale.getDefault());

    /* renamed from: ʻ  reason: contains not printable characters */
    private OnDateSetListener f14092;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private int f14093;

    /* renamed from: ʼ  reason: contains not printable characters */
    private HashSet<OnDateChangedListener> f14094;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private ScrollOrientation f14095;

    /* renamed from: ʽ  reason: contains not printable characters */
    private DialogInterface.OnCancelListener f14096;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private Version f14097;

    /* renamed from: ʾ  reason: contains not printable characters */
    private TextView f14098;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    private Locale f14099;

    /* renamed from: ʿ  reason: contains not printable characters */
    private TextView f14100;

    /* renamed from: ʿʿ  reason: contains not printable characters */
    private TimeZone f14101;

    /* renamed from: ˆ  reason: contains not printable characters */
    private String f14102;

    /* renamed from: ˆˆ  reason: contains not printable characters */
    private DateRangeLimiter f14103;

    /* renamed from: ˈ  reason: contains not printable characters */
    private LinearLayout f14104;

    /* renamed from: ˈˈ  reason: contains not printable characters */
    private boolean f14105;

    /* renamed from: ˉ  reason: contains not printable characters */
    private HashSet<Calendar> f14106;

    /* renamed from: ˉˉ  reason: contains not printable characters */
    private HapticFeedbackController f14107;

    /* renamed from: ˊ  reason: contains not printable characters */
    private YearPickerView f14108;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private String f14109;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f14110;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private String f14111;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int f14112;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private String f14113;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f14114;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private String f14115;

    /* renamed from: ˑ  reason: contains not printable characters */
    private DialogInterface.OnDismissListener f14116;

    /* renamed from: י  reason: contains not printable characters */
    private boolean f14117;

    /* renamed from: ـ  reason: contains not printable characters */
    private int f14118;

    /* renamed from: ــ  reason: contains not printable characters */
    private DefaultDateRangeLimiter f14119;

    /* renamed from: ٴ  reason: contains not printable characters */
    private AccessibleDateAnimator f14120;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private TextView f14121;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private int f14122;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f14123;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private String f14124;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f14125;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f14126;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int f14127;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f14128;

    /* renamed from: 连任  reason: contains not printable characters */
    private Calendar f14129;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private String f14130;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private TextView f14131;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private DayPickerGroup f14132;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private int f14133;

    protected interface OnDateChangedListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m18002();
    }

    public interface OnDateSetListener {
        /* renamed from: 龘  reason: contains not printable characters */
        void m18003(DatePickerDialog datePickerDialog, int i, int i2, int i3);
    }

    public enum ScrollOrientation {
        HORIZONTAL,
        VERTICAL
    }

    public enum Version {
        VERSION_1,
        VERSION_2
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m17978(int i) {
        long timeInMillis = this.f14129.getTimeInMillis();
        switch (i) {
            case 0:
                if (this.f14097 == Version.VERSION_1) {
                    ObjectAnimator r4 = Utils.m17954((View) this.f14104, 0.9f, 1.05f);
                    if (this.f14105) {
                        r4.setStartDelay(500);
                        this.f14105 = false;
                    }
                    this.f14132.m18013();
                    if (this.f14110 != i) {
                        this.f14104.setSelected(true);
                        this.f14131.setSelected(false);
                        this.f14120.setDisplayedChild(0);
                        this.f14110 = i;
                    }
                    r4.start();
                } else {
                    this.f14132.m18013();
                    if (this.f14110 != i) {
                        this.f14104.setSelected(true);
                        this.f14131.setSelected(false);
                        this.f14120.setDisplayedChild(0);
                        this.f14110 = i;
                    }
                }
                this.f14120.setContentDescription(this.f14111 + ": " + DateUtils.formatDateTime(getActivity(), timeInMillis, 16));
                Utils.m17956((View) this.f14120, (CharSequence) this.f14109);
                return;
            case 1:
                if (this.f14097 == Version.VERSION_1) {
                    ObjectAnimator r42 = Utils.m17954((View) this.f14131, 0.85f, 1.1f);
                    if (this.f14105) {
                        r42.setStartDelay(500);
                        this.f14105 = false;
                    }
                    this.f14108.m18085();
                    if (this.f14110 != i) {
                        this.f14104.setSelected(false);
                        this.f14131.setSelected(true);
                        this.f14120.setDisplayedChild(1);
                        this.f14110 = i;
                    }
                    r42.start();
                } else {
                    this.f14108.m18085();
                    if (this.f14110 != i) {
                        this.f14104.setSelected(false);
                        this.f14131.setSelected(true);
                        this.f14120.setDisplayedChild(1);
                        this.f14110 = i;
                    }
                }
                this.f14120.setContentDescription(this.f14115 + ": " + f14091.format(Long.valueOf(timeInMillis)));
                Utils.m17956((View) this.f14120, (CharSequence) this.f14113);
                return;
            default:
                return;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Calendar m17979(Calendar calendar) {
        int i = calendar.get(5);
        int actualMaximum = calendar.getActualMaximum(5);
        if (i > actualMaximum) {
            calendar.set(5, actualMaximum);
        }
        return this.f14103.m18008(calendar);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m17980(boolean z) {
        this.f14131.setText(f14091.format(this.f14129.getTime()));
        if (this.f14097 == Version.VERSION_1) {
            if (this.f14121 != null) {
                if (this.f14102 != null) {
                    this.f14121.setText(this.f14102.toUpperCase(this.f14099));
                } else {
                    this.f14121.setText(this.f14129.getDisplayName(7, 2, this.f14099).toUpperCase(this.f14099));
                }
            }
            this.f14098.setText(f14088.format(this.f14129.getTime()));
            this.f14100.setText(f14090.format(this.f14129.getTime()));
        }
        if (this.f14097 == Version.VERSION_2) {
            this.f14100.setText(f14089.format(this.f14129.getTime()));
            if (this.f14102 != null) {
                this.f14121.setText(this.f14102.toUpperCase(this.f14099));
            } else {
                this.f14121.setVisibility(8);
            }
        }
        long timeInMillis = this.f14129.getTimeInMillis();
        this.f14120.setDateMillis(timeInMillis);
        this.f14104.setContentDescription(DateUtils.formatDateTime(getActivity(), timeInMillis, 24));
        if (z) {
            Utils.m17956((View) this.f14120, (CharSequence) DateUtils.formatDateTime(getActivity(), timeInMillis, 20));
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private void m17981() {
        Iterator<OnDateChangedListener> it2 = this.f14094.iterator();
        while (it2.hasNext()) {
            it2.next().m18002();
        }
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        if (this.f14096 != null) {
            this.f14096.onCancel(dialogInterface);
        }
    }

    public void onClick(View view) {
        m17988();
        if (view.getId() == R.id.mdtp_date_picker_year) {
            m17978(1);
        } else if (view.getId() == R.id.mdtp_date_picker_month_and_day) {
            m17978(0);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        ViewGroup viewGroup = (ViewGroup) getView();
        if (viewGroup != null) {
            viewGroup.removeAllViewsInLayout();
            viewGroup.addView(onCreateView(getActivity().getLayoutInflater(), viewGroup, (Bundle) null));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Activity activity = getActivity();
        activity.getWindow().setSoftInputMode(3);
        this.f14110 = -1;
        if (bundle != null) {
            this.f14129.set(1, bundle.getInt("year"));
            this.f14129.set(2, bundle.getInt("month"));
            this.f14129.set(5, bundle.getInt("day"));
            this.f14127 = bundle.getInt("default_view");
        }
        if (Build.VERSION.SDK_INT < 18) {
            f14089 = new SimpleDateFormat(activity.getResources().getString(R.string.mdtp_date_v2_daymonthyear), this.f14099);
        } else {
            f14089 = new SimpleDateFormat(DateFormat.getBestDateTimePattern(this.f14099, "EEEMMMdd"), this.f14099);
        }
        f14089.setTimeZone(m17989());
    }

    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        onCreateDialog.requestWindowFeature(1);
        return onCreateDialog;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i = -1;
        int i2 = 0;
        int i3 = this.f14127;
        if (this.f14095 == null) {
            this.f14095 = this.f14097 == Version.VERSION_1 ? ScrollOrientation.VERTICAL : ScrollOrientation.HORIZONTAL;
        }
        if (bundle != null) {
            this.f14112 = bundle.getInt("week_start");
            i3 = bundle.getInt("current_view");
            i = bundle.getInt("list_position");
            i2 = bundle.getInt("list_position_offset");
            this.f14106 = (HashSet) bundle.getSerializable("highlighted_days");
            this.f14114 = bundle.getBoolean("theme_dark");
            this.f14117 = bundle.getBoolean("theme_dark_changed");
            this.f14118 = bundle.getInt("accent");
            this.f14123 = bundle.getBoolean("vibrate");
            this.f14125 = bundle.getBoolean("dismiss");
            this.f14126 = bundle.getBoolean("auto_dismiss");
            this.f14102 = bundle.getString(PubnativeAsset.TITLE);
            this.f14128 = bundle.getInt("ok_resid");
            this.f14130 = bundle.getString("ok_string");
            this.f14133 = bundle.getInt("ok_color");
            this.f14122 = bundle.getInt("cancel_resid");
            this.f14124 = bundle.getString("cancel_string");
            this.f14093 = bundle.getInt("cancel_color");
            this.f14097 = (Version) bundle.getSerializable("version");
            this.f14095 = (ScrollOrientation) bundle.getSerializable("scrollorientation");
            this.f14101 = (TimeZone) bundle.getSerializable("timezone");
            this.f14103 = (DateRangeLimiter) bundle.getParcelable("daterangelimiter");
            m18001((Locale) bundle.getSerializable(PubnativeRequest.Parameters.LOCALE));
            if (this.f14103 instanceof DefaultDateRangeLimiter) {
                this.f14119 = (DefaultDateRangeLimiter) this.f14103;
            } else {
                this.f14119 = new DefaultDateRangeLimiter();
            }
        }
        this.f14119.m18038((DatePickerController) this);
        View inflate = layoutInflater.inflate(this.f14097 == Version.VERSION_1 ? R.layout.mdtp_date_picker_dialog : R.layout.mdtp_date_picker_dialog_v2, viewGroup, false);
        this.f14129 = this.f14103.m18008(this.f14129);
        this.f14121 = (TextView) inflate.findViewById(R.id.mdtp_date_picker_header);
        this.f14104 = (LinearLayout) inflate.findViewById(R.id.mdtp_date_picker_month_and_day);
        this.f14104.setOnClickListener(this);
        this.f14098 = (TextView) inflate.findViewById(R.id.mdtp_date_picker_month);
        this.f14100 = (TextView) inflate.findViewById(R.id.mdtp_date_picker_day);
        this.f14131 = (TextView) inflate.findViewById(R.id.mdtp_date_picker_year);
        this.f14131.setOnClickListener(this);
        Activity activity = getActivity();
        this.f14132 = new DayPickerGroup((Context) activity, (DatePickerController) this);
        this.f14108 = new YearPickerView(activity, this);
        if (!this.f14117) {
            this.f14114 = Utils.m17959((Context) activity, this.f14114);
        }
        Resources resources = getResources();
        this.f14111 = resources.getString(R.string.mdtp_day_picker_description);
        this.f14109 = resources.getString(R.string.mdtp_select_day);
        this.f14115 = resources.getString(R.string.mdtp_year_picker_description);
        this.f14113 = resources.getString(R.string.mdtp_select_year);
        inflate.setBackgroundColor(ContextCompat.getColor(activity, this.f14114 ? R.color.mdtp_date_picker_view_animator_dark_theme : R.color.mdtp_date_picker_view_animator));
        this.f14120 = (AccessibleDateAnimator) inflate.findViewById(R.id.mdtp_animator);
        this.f14120.addView(this.f14132);
        this.f14120.addView(this.f14108);
        this.f14120.setDateMillis(this.f14129.getTimeInMillis());
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(300);
        this.f14120.setInAnimation(alphaAnimation);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation2.setDuration(300);
        this.f14120.setOutAnimation(alphaAnimation2);
        String string = activity.getResources().getString(R.string.mdtp_button_typeface);
        Button button = (Button) inflate.findViewById(R.id.mdtp_ok);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DatePickerDialog.this.m17988();
                DatePickerDialog.this.m17986();
                DatePickerDialog.this.dismiss();
            }
        });
        button.setTypeface(TypefaceHelper.m17950(activity, string));
        if (this.f14130 != null) {
            button.setText(this.f14130);
        } else {
            button.setText(this.f14128);
        }
        Button button2 = (Button) inflate.findViewById(R.id.mdtp_cancel);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DatePickerDialog.this.m17988();
                if (DatePickerDialog.this.getDialog() != null) {
                    DatePickerDialog.this.getDialog().cancel();
                }
            }
        });
        button2.setTypeface(TypefaceHelper.m17950(activity, string));
        if (this.f14124 != null) {
            button2.setText(this.f14124);
        } else {
            button2.setText(this.f14122);
        }
        button2.setVisibility(isCancelable() ? 0 : 8);
        if (this.f14118 == -1) {
            this.f14118 = Utils.m17953((Context) getActivity());
        }
        if (this.f14121 != null) {
            this.f14121.setBackgroundColor(Utils.m17952(this.f14118));
        }
        inflate.findViewById(R.id.mdtp_day_picker_selected_date_layout).setBackgroundColor(this.f14118);
        if (this.f14133 != -1) {
            button.setTextColor(this.f14133);
        } else {
            button.setTextColor(this.f14118);
        }
        if (this.f14093 != -1) {
            button2.setTextColor(this.f14093);
        } else {
            button2.setTextColor(this.f14118);
        }
        if (getDialog() == null) {
            inflate.findViewById(R.id.mdtp_done_background).setVisibility(8);
        }
        m17980(false);
        m17978(i3);
        if (i != -1) {
            if (i3 == 0) {
                this.f14132.m18014(i);
            } else if (i3 == 1) {
                this.f14108.m18087(i, i2);
            }
        }
        this.f14107 = new HapticFeedbackController(activity);
        return inflate;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (this.f14116 != null) {
            this.f14116.onDismiss(dialogInterface);
        }
    }

    public void onPause() {
        super.onPause();
        this.f14107.m17947();
        if (this.f14125) {
            dismiss();
        }
    }

    public void onResume() {
        super.onResume();
        this.f14107.m17949();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("year", this.f14129.get(1));
        bundle.putInt("month", this.f14129.get(2));
        bundle.putInt("day", this.f14129.get(5));
        bundle.putInt("week_start", this.f14112);
        bundle.putInt("current_view", this.f14110);
        int i = -1;
        if (this.f14110 == 0) {
            i = this.f14132.getMostVisiblePosition();
        } else if (this.f14110 == 1) {
            i = this.f14108.getFirstVisiblePosition();
            bundle.putInt("list_position_offset", this.f14108.getFirstPositionOffset());
        }
        bundle.putInt("list_position", i);
        bundle.putSerializable("highlighted_days", this.f14106);
        bundle.putBoolean("theme_dark", this.f14114);
        bundle.putBoolean("theme_dark_changed", this.f14117);
        bundle.putInt("accent", this.f14118);
        bundle.putBoolean("vibrate", this.f14123);
        bundle.putBoolean("dismiss", this.f14125);
        bundle.putBoolean("auto_dismiss", this.f14126);
        bundle.putInt("default_view", this.f14127);
        bundle.putString(PubnativeAsset.TITLE, this.f14102);
        bundle.putInt("ok_resid", this.f14128);
        bundle.putString("ok_string", this.f14130);
        bundle.putInt("ok_color", this.f14133);
        bundle.putInt("cancel_resid", this.f14122);
        bundle.putString("cancel_string", this.f14124);
        bundle.putInt("cancel_color", this.f14093);
        bundle.putSerializable("version", this.f14097);
        bundle.putSerializable("scrollorientation", this.f14095);
        bundle.putSerializable("timezone", this.f14101);
        bundle.putParcelable("daterangelimiter", this.f14103);
        bundle.putSerializable(PubnativeRequest.Parameters.LOCALE, this.f14099);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m17982() {
        return this.f14103.m18004();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Calendar m17983() {
        return this.f14103.m18006();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Calendar m17984() {
        return this.f14103.m18005();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public ScrollOrientation m17985() {
        return this.f14095;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public void m17986() {
        if (this.f14092 != null) {
            this.f14092.m18003(this, this.f14129.get(1), this.f14129.get(2), this.f14129.get(5));
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Version m17987() {
        return this.f14097;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m17988() {
        if (this.f14123) {
            this.f14107.m17948();
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public TimeZone m17989() {
        return this.f14101 == null ? TimeZone.getDefault() : this.f14101;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Locale m17990() {
        return this.f14099;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m17991() {
        return this.f14103.m18007();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m17992() {
        return this.f14114;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m17993(int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance(m17989());
        instance.set(1, i);
        instance.set(2, i2);
        instance.set(5, i3);
        Utils.m17955(instance);
        return this.f14106.contains(instance);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m17994() {
        return this.f14112;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m17995() {
        return this.f14118;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m17996(int i, int i2, int i3) {
        return this.f14103.m18009(i, i2, i3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public MonthAdapter.CalendarDay m17997() {
        return new MonthAdapter.CalendarDay(this.f14129, m17989());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17998(int i) {
        this.f14129.set(1, i);
        this.f14129 = m17979(this.f14129);
        m17981();
        m17978(0);
        m17980(true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17999(int i, int i2, int i3) {
        this.f14129.set(1, i);
        this.f14129.set(2, i2);
        this.f14129.set(5, i3);
        m17981();
        m17980(true);
        if (this.f14126) {
            m17986();
            dismiss();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18000(OnDateChangedListener onDateChangedListener) {
        this.f14094.add(onDateChangedListener);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18001(Locale locale) {
        this.f14099 = locale;
        this.f14112 = Calendar.getInstance(this.f14101, this.f14099).getFirstDayOfWeek();
        f14091 = new SimpleDateFormat("yyyy", locale);
        f14088 = new SimpleDateFormat("MMM", locale);
        f14090 = new SimpleDateFormat("dd", locale);
    }
}
