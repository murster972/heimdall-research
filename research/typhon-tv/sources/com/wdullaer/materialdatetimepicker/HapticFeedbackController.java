package com.wdullaer.materialdatetimepicker;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;

public class HapticFeedbackController {

    /* renamed from: 连任  reason: contains not printable characters */
    private long f14079;

    /* renamed from: 靐  reason: contains not printable characters */
    private final ContentObserver f14080 = new ContentObserver((Handler) null) {
        public void onChange(boolean z) {
            boolean unused = HapticFeedbackController.this.f14081 = HapticFeedbackController.m17942(HapticFeedbackController.this.f14083);
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean f14081;

    /* renamed from: 齉  reason: contains not printable characters */
    private Vibrator f14082;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public final Context f14083;

    public HapticFeedbackController(Context context) {
        this.f14083 = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m17942(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "haptic_feedback_enabled", 0) == 1;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m17943(Context context) {
        return context.getPackageManager().checkPermission("android.permission.VIBRATE", context.getPackageName()) == 0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m17947() {
        this.f14082 = null;
        this.f14083.getContentResolver().unregisterContentObserver(this.f14080);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m17948() {
        if (this.f14082 != null && this.f14081) {
            long uptimeMillis = SystemClock.uptimeMillis();
            if (uptimeMillis - this.f14079 >= 125) {
                this.f14082.vibrate(50);
                this.f14079 = uptimeMillis;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m17949() {
        if (m17943(this.f14083)) {
            this.f14082 = (Vibrator) this.f14083.getSystemService("vibrator");
        }
        this.f14081 = m17942(this.f14083);
        this.f14083.getContentResolver().registerContentObserver(Settings.System.getUriFor("haptic_feedback_enabled"), false, this.f14080);
    }
}
