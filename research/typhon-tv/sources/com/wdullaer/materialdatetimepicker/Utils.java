package com.wdullaer.materialdatetimepicker;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import java.util.Calendar;

public class Utils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m17951(float f, Resources resources) {
        return (int) TypedValue.applyDimension(1, f, resources.getDisplayMetrics());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m17952(int i) {
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        fArr[2] = fArr[2] * 0.8f;
        return Color.HSVToColor(fArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m17953(Context context) {
        TypedValue typedValue = new TypedValue();
        if (Build.VERSION.SDK_INT >= 21) {
            context.getTheme().resolveAttribute(16843829, typedValue, true);
            return typedValue.data;
        }
        int identifier = context.getResources().getIdentifier("colorAccent", "attr", context.getPackageName());
        return (identifier == 0 || !context.getTheme().resolveAttribute(identifier, typedValue, true)) ? ContextCompat.getColor(context, R.color.mdtp_accent_color) : typedValue.data;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ObjectAnimator m17954(View view, float f, float f2) {
        Keyframe ofFloat = Keyframe.ofFloat(0.0f, 1.0f);
        Keyframe ofFloat2 = Keyframe.ofFloat(0.275f, f);
        Keyframe ofFloat3 = Keyframe.ofFloat(0.69f, f2);
        Keyframe ofFloat4 = Keyframe.ofFloat(1.0f, 1.0f);
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(view, new PropertyValuesHolder[]{PropertyValuesHolder.ofKeyframe("scaleX", new Keyframe[]{ofFloat, ofFloat2, ofFloat3, ofFloat4}), PropertyValuesHolder.ofKeyframe("scaleY", new Keyframe[]{ofFloat, ofFloat2, ofFloat3, ofFloat4})});
        ofPropertyValuesHolder.setDuration(544);
        return ofPropertyValuesHolder;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Calendar m17955(Calendar calendar) {
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar;
    }

    @SuppressLint({"NewApi"})
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m17956(View view, CharSequence charSequence) {
        if (m17957() && view != null && charSequence != null) {
            view.announceForAccessibility(charSequence);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m17957() {
        return Build.VERSION.SDK_INT >= 16;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m17958(Context context, int i, boolean z) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{i});
        try {
            return obtainStyledAttributes.getBoolean(0, z);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m17959(Context context, boolean z) {
        return m17958(context, R.attr.mdtp_theme_dark, z);
    }
}
