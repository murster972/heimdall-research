package com.evernote.android.job;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.LruCache;
import android.util.SparseArray;
import com.evernote.android.job.Job;
import com.evernote.android.job.util.JobCat;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class JobExecutor {
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final long f6560 = TimeUnit.MINUTES.toMillis(3);
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final JobCat f6561 = new JobCat("JobExecutor");

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Set<JobRequest> f6562 = new HashSet();

    /* renamed from: 连任  reason: contains not printable characters */
    private final SparseArray<Job.Result> f6563 = new SparseArray<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private final LruCache<Integer, WeakReference<Job>> f6564 = new LruCache<>(20);

    /* renamed from: 齉  reason: contains not printable characters */
    private final SparseArray<Job> f6565 = new SparseArray<>();

    private final class JobCallable implements Callable<Job.Result> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Job f6566;

        /* renamed from: 齉  reason: contains not printable characters */
        private final PowerManager.WakeLock f6567;

        private JobCallable(Job job) {
            this.f6566 = job;
            this.f6567 = WakeLockUtil.m7553(this.f6566.getContext(), "JobExecutor", JobExecutor.f6560);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private Job.Result m7518() {
            try {
                Job.Result runJob = this.f6566.runJob();
                JobExecutor.f6561.i("Finished %s", this.f6566);
                m7519(this.f6566, runJob);
                return runJob;
            } catch (Throwable th) {
                JobExecutor.f6561.e(th, "Crashed %s", this.f6566);
                return this.f6566.getResult();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m7519(Job job, Job.Result result) {
            JobRequest request = this.f6566.getParams().getRequest();
            boolean z = false;
            boolean z2 = false;
            if (!request.isPeriodic() && Job.Result.RESCHEDULE.equals(result) && !job.isDeleted()) {
                request = request.reschedule(true, true);
                this.f6566.onReschedule(request.getJobId());
                z2 = true;
            } else if (request.isPeriodic()) {
                z2 = true;
                if (!Job.Result.SUCCESS.equals(result)) {
                    z = true;
                }
            }
            if (job.isDeleted()) {
                return;
            }
            if (z || z2) {
                request.updateStats(z, z2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Job.Result call() throws Exception {
            String str;
            try {
                WakeLockUtil.m7555(this.f6566.getContext(), this.f6567, JobExecutor.f6560);
                return m7518();
            } finally {
                JobExecutor.this.m7516(this.f6566);
                if (this.f6567 == null || !this.f6567.isHeld()) {
                    str = "Wake lock was not held after job %s was done. The job took too long to complete. This could have unintended side effects on your app.";
                    JobExecutor.f6561.w(str, this.f6566);
                }
                WakeLockUtil.m7554(this.f6567);
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public SparseArray<Job.Result> m7509() {
        return this.f6563.clone();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized boolean m7510(JobRequest jobRequest) {
        boolean z;
        if (jobRequest != null) {
            if (this.f6562.contains(jobRequest)) {
                z = true;
            }
        }
        z = false;
        return z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Job m7511(int i) {
        Job job;
        job = this.f6565.get(i);
        if (job == null) {
            WeakReference weakReference = this.f6564.get(Integer.valueOf(i));
            job = weakReference != null ? (Job) weakReference.get() : null;
        }
        return job;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Set<Job> m7512() {
        return m7513((String) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Set<Job> m7513(String str) {
        HashSet hashSet;
        hashSet = new HashSet();
        for (int i = 0; i < this.f6565.size(); i++) {
            Job valueAt = this.f6565.valueAt(i);
            if (str == null || str.equals(valueAt.getParams().getTag())) {
                hashSet.add(valueAt);
            }
        }
        for (WeakReference<Job> weakReference : this.f6564.snapshot().values()) {
            Job job = (Job) weakReference.get();
            if (job != null && (str == null || str.equals(job.getParams().getTag()))) {
                hashSet.add(job);
            }
        }
        return hashSet;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized Future<Job.Result> m7514(Context context, JobRequest jobRequest, Job job, Bundle bundle) {
        Future<Job.Result> future = null;
        synchronized (this) {
            this.f6562.remove(jobRequest);
            if (job == null) {
                f6561.w("JobCreator returned null for tag %s", jobRequest.getTag());
            } else if (job.isFinished()) {
                throw new IllegalStateException(String.format(Locale.ENGLISH, "Job for tag %s was already run, a creator should always create a new Job instance", new Object[]{jobRequest.getTag()}));
            } else {
                job.setContext(context).setRequest(jobRequest, bundle);
                f6561.i("Executing %s, context %s", jobRequest, context.getClass().getSimpleName());
                this.f6565.put(jobRequest.getJobId(), job);
                future = JobConfig.getExecutorService().submit(new JobCallable(job));
            }
        }
        return future;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"UseSparseArrays"})
    /* renamed from: 龘  reason: contains not printable characters */
    public void m7515(LruCache<Integer, WeakReference<Job>> lruCache) {
        HashMap hashMap = new HashMap(lruCache.snapshot());
        for (Integer num : hashMap.keySet()) {
            if (hashMap.get(num) == null || ((WeakReference) hashMap.get(num)).get() == null) {
                lruCache.remove(num);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m7516(Job job) {
        int id = job.getParams().getId();
        this.f6565.remove(id);
        m7515(this.f6564);
        this.f6563.put(id, job.getResult());
        this.f6564.put(Integer.valueOf(id), new WeakReference(job));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m7517(JobRequest jobRequest) {
        this.f6562.add(jobRequest);
    }
}
