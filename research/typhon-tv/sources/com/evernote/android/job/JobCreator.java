package com.evernote.android.job;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public interface JobCreator {
    public static final String ACTION_ADD_JOB_CREATOR = "com.evernote.android.job.ADD_JOB_CREATOR";

    public static abstract class AddJobCreatorReceiver extends BroadcastReceiver {
        /* access modifiers changed from: protected */
        public abstract void addJobCreator(Context context, JobManager jobManager);

        public final void onReceive(Context context, Intent intent) {
            if (context != null && intent != null && JobCreator.ACTION_ADD_JOB_CREATOR.equals(intent.getAction())) {
                try {
                    addJobCreator(context, JobManager.create(context));
                } catch (JobManagerCreateException e) {
                }
            }
        }
    }

    Job create(String str);
}
