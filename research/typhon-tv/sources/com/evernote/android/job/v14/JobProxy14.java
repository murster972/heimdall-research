package com.evernote.android.job.v14;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.evernote.android.job.JobConfig;
import com.evernote.android.job.JobProxy;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobUtil;

public class JobProxy14 implements JobProxy {
    private static final String TAG = "JobProxy14";
    private AlarmManager mAlarmManager;
    protected final JobCat mCat;
    protected final Context mContext;

    public JobProxy14(Context context) {
        this(context, TAG);
    }

    protected JobProxy14(Context context, String str) {
        this.mContext = context;
        this.mCat = new JobCat(str);
    }

    private void logScheduled(JobRequest jobRequest) {
        this.mCat.d("Scheduled alarm, %s, delay %s (from now), exact %b, reschedule count %d", jobRequest, JobUtil.timeToString(JobProxy.Common.getAverageDelayMs(jobRequest)), Boolean.valueOf(jobRequest.isExact()), Integer.valueOf(JobProxy.Common.getRescheduleCount(jobRequest)));
    }

    public void cancel(int i) {
        AlarmManager alarmManager = getAlarmManager();
        if (alarmManager != null) {
            try {
                alarmManager.cancel(getPendingIntent(i, false, (Bundle) null, createPendingIntentFlags(true)));
                alarmManager.cancel(getPendingIntent(i, false, (Bundle) null, createPendingIntentFlags(false)));
            } catch (Exception e) {
                this.mCat.e((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int createPendingIntentFlags(boolean z) {
        if (!z) {
            return 134217728 | 1073741824;
        }
        return 134217728;
    }

    /* access modifiers changed from: protected */
    public AlarmManager getAlarmManager() {
        if (this.mAlarmManager == null) {
            this.mAlarmManager = (AlarmManager) this.mContext.getSystemService(NotificationCompat.CATEGORY_ALARM);
        }
        if (this.mAlarmManager == null) {
            this.mCat.e("AlarmManager is null");
        }
        return this.mAlarmManager;
    }

    /* access modifiers changed from: protected */
    public PendingIntent getPendingIntent(int i, boolean z, Bundle bundle, int i2) {
        try {
            return PendingIntent.getBroadcast(this.mContext, i, PlatformAlarmReceiver.createIntent(this.mContext, i, z, bundle), i2);
        } catch (Exception e) {
            this.mCat.e((Throwable) e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public PendingIntent getPendingIntent(JobRequest jobRequest, int i) {
        return getPendingIntent(jobRequest.getJobId(), jobRequest.isExact(), jobRequest.getTransientExtras(), i);
    }

    /* access modifiers changed from: protected */
    public PendingIntent getPendingIntent(JobRequest jobRequest, boolean z) {
        return getPendingIntent(jobRequest, createPendingIntentFlags(z));
    }

    /* access modifiers changed from: protected */
    public long getTriggerAtMillis(JobRequest jobRequest) {
        return JobConfig.isForceRtc() ? JobConfig.getClock().currentTimeMillis() + JobProxy.Common.getAverageDelayMs(jobRequest) : JobConfig.getClock().elapsedRealtime() + JobProxy.Common.getAverageDelayMs(jobRequest);
    }

    /* access modifiers changed from: protected */
    public int getType(boolean z) {
        return z ? JobConfig.isForceRtc() ? 0 : 2 : JobConfig.isForceRtc() ? 1 : 3;
    }

    public boolean isPlatformJobScheduled(JobRequest jobRequest) {
        return getPendingIntent(jobRequest, 536870912) != null;
    }

    public void plantOneOff(JobRequest jobRequest) {
        PendingIntent pendingIntent = getPendingIntent(jobRequest, false);
        AlarmManager alarmManager = getAlarmManager();
        if (alarmManager != null) {
            try {
                if (!jobRequest.isExact()) {
                    plantOneOffInexact(jobRequest, alarmManager, pendingIntent);
                } else if (jobRequest.getStartMs() != 1 || jobRequest.getFailureCount() > 0) {
                    plantOneOffExact(jobRequest, alarmManager, pendingIntent);
                } else {
                    PlatformAlarmService.start(this.mContext, jobRequest.getJobId(), jobRequest.getTransientExtras());
                }
            } catch (Exception e) {
                this.mCat.e((Throwable) e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void plantOneOffExact(JobRequest jobRequest, AlarmManager alarmManager, PendingIntent pendingIntent) {
        long triggerAtMillis = getTriggerAtMillis(jobRequest);
        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(getType(true), triggerAtMillis, pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(getType(true), triggerAtMillis, pendingIntent);
        } else {
            alarmManager.set(getType(true), triggerAtMillis, pendingIntent);
        }
        logScheduled(jobRequest);
    }

    /* access modifiers changed from: protected */
    public void plantOneOffFlexSupport(JobRequest jobRequest, AlarmManager alarmManager, PendingIntent pendingIntent) {
        alarmManager.set(1, JobConfig.getClock().currentTimeMillis() + JobProxy.Common.getAverageDelayMsSupportFlex(jobRequest), pendingIntent);
        this.mCat.d("Scheduled repeating alarm (flex support), %s, interval %s, flex %s", jobRequest, JobUtil.timeToString(jobRequest.getIntervalMs()), JobUtil.timeToString(jobRequest.getFlexMs()));
    }

    /* access modifiers changed from: protected */
    public void plantOneOffInexact(JobRequest jobRequest, AlarmManager alarmManager, PendingIntent pendingIntent) {
        alarmManager.set(getType(false), getTriggerAtMillis(jobRequest), pendingIntent);
        logScheduled(jobRequest);
    }

    public void plantPeriodic(JobRequest jobRequest) {
        PendingIntent pendingIntent = getPendingIntent(jobRequest, true);
        AlarmManager alarmManager = getAlarmManager();
        if (alarmManager != null) {
            alarmManager.setRepeating(getType(true), getTriggerAtMillis(jobRequest), jobRequest.getIntervalMs(), pendingIntent);
        }
        this.mCat.d("Scheduled repeating alarm, %s, interval %s", jobRequest, JobUtil.timeToString(jobRequest.getIntervalMs()));
    }

    public void plantPeriodicFlexSupport(JobRequest jobRequest) {
        PendingIntent pendingIntent = getPendingIntent(jobRequest, false);
        AlarmManager alarmManager = getAlarmManager();
        if (alarmManager != null) {
            try {
                plantOneOffFlexSupport(jobRequest, alarmManager, pendingIntent);
            } catch (Exception e) {
                this.mCat.e((Throwable) e);
            }
        }
    }
}
