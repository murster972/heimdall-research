package com.evernote.android.job;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import com.evernote.android.job.gcm.JobProxyGcm;
import com.evernote.android.job.v14.JobProxy14;
import com.evernote.android.job.v14.PlatformAlarmReceiver;
import com.evernote.android.job.v14.PlatformAlarmService;
import com.evernote.android.job.v14.PlatformAlarmServiceExact;
import com.evernote.android.job.v19.JobProxy19;
import com.evernote.android.job.v21.JobProxy21;
import com.evernote.android.job.v21.PlatformJobService;
import com.evernote.android.job.v24.JobProxy24;
import com.evernote.android.job.v26.JobProxy26;
import java.util.List;

public enum JobApi {
    V_26(true, false, true),
    V_24(true, false, false),
    V_21(true, true, false),
    V_19(true, true, true),
    V_14(false, true, true),
    GCM(true, false, true);
    
    private static final String JOB_SCHEDULER_PERMISSION = "android.permission.BIND_JOB_SERVICE";
    private volatile JobProxy mCachedProxy;
    private final boolean mFlexSupport;
    private final boolean mSupportsExecutionWindow;
    private final boolean mSupportsTransientJobs;

    private JobApi(boolean z, boolean z2, boolean z3) {
        this.mSupportsExecutionWindow = z;
        this.mFlexSupport = z2;
        this.mSupportsTransientJobs = z3;
    }

    private JobProxy createProxy(Context context) {
        switch (this) {
            case V_26:
                return new JobProxy26(context);
            case V_24:
                return new JobProxy24(context);
            case V_21:
                return new JobProxy21(context);
            case V_19:
                return new JobProxy19(context);
            case V_14:
                return new JobProxy14(context);
            case GCM:
                return new JobProxyGcm(context);
            default:
                throw new IllegalStateException("not implemented");
        }
    }

    public static JobApi getDefault(Context context) {
        if (V_26.isSupported(context) && JobConfig.isApiEnabled(V_26)) {
            return V_26;
        }
        if (V_24.isSupported(context) && JobConfig.isApiEnabled(V_24)) {
            return V_24;
        }
        if (V_21.isSupported(context) && JobConfig.isApiEnabled(V_21)) {
            return V_21;
        }
        if (GCM.isSupported(context) && JobConfig.isApiEnabled(GCM)) {
            return GCM;
        }
        if (V_19.isSupported(context) && JobConfig.isApiEnabled(V_19)) {
            return V_19;
        }
        if (JobConfig.isApiEnabled(V_14)) {
            return V_14;
        }
        throw new IllegalStateException("All supported APIs are disabled");
    }

    private boolean isBroadcastEnabled(Context context, Class<? extends BroadcastReceiver> cls) {
        try {
            List<ResolveInfo> queryBroadcastReceivers = context.getPackageManager().queryBroadcastReceivers(new Intent(context, cls), 0);
            return queryBroadcastReceivers != null && !queryBroadcastReceivers.isEmpty();
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isServiceEnabled(Context context, Class<? extends Service> cls) {
        try {
            List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(new Intent(context, cls), 0);
            return queryIntentServices != null && !queryIntentServices.isEmpty();
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isServiceEnabledAndHasPermission(Context context, Class<? extends Service> cls, String str) {
        try {
            List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(new Intent(context, cls), 0);
            if (queryIntentServices == null || queryIntentServices.isEmpty()) {
                return false;
            }
            for (ResolveInfo next : queryIntentServices) {
                if (next.serviceInfo != null && str.equals(next.serviceInfo.permission)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized JobProxy getProxy(Context context) {
        if (this.mCachedProxy == null) {
            this.mCachedProxy = createProxy(context);
        }
        return this.mCachedProxy;
    }

    public synchronized void invalidateCachedProxy() {
        this.mCachedProxy = null;
    }

    /* access modifiers changed from: package-private */
    public boolean isFlexSupport() {
        return this.mFlexSupport;
    }

    public boolean isSupported(Context context) {
        boolean z = false;
        switch (this) {
            case V_26:
                return Build.VERSION.SDK_INT >= 26 && isServiceEnabled(context, PlatformJobService.class);
            case V_24:
                return Build.VERSION.SDK_INT >= 24 && isServiceEnabledAndHasPermission(context, PlatformJobService.class, JOB_SCHEDULER_PERMISSION);
            case V_21:
                return Build.VERSION.SDK_INT >= 21 && isServiceEnabledAndHasPermission(context, PlatformJobService.class, JOB_SCHEDULER_PERMISSION);
            case V_19:
                return Build.VERSION.SDK_INT >= 19 && isServiceEnabled(context, PlatformAlarmService.class) && isBroadcastEnabled(context, PlatformAlarmReceiver.class);
            case V_14:
                if (JobConfig.isForceAllowApi14() || (isServiceEnabled(context, PlatformAlarmService.class) && isServiceEnabled(context, PlatformAlarmServiceExact.class) && isBroadcastEnabled(context, PlatformAlarmReceiver.class))) {
                    z = true;
                }
                return z;
            case GCM:
                return GcmAvailableHelper.m7501(context);
            default:
                throw new IllegalStateException("not implemented");
        }
    }

    /* access modifiers changed from: package-private */
    public boolean supportsExecutionWindow() {
        return this.mSupportsExecutionWindow;
    }

    /* access modifiers changed from: package-private */
    public boolean supportsTransientJobs() {
        return this.mSupportsTransientJobs;
    }
}
