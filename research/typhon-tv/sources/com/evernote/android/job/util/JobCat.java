package com.evernote.android.job.util;

import android.util.Log;
import java.util.Arrays;

public class JobCat implements JobLogger {
    private static volatile boolean logcatEnabled = true;
    private static volatile JobLogger[] loggers = new JobLogger[0];
    protected final boolean mEnabled;
    protected final String mTag;

    public JobCat(Class<?> cls) {
        this(cls.getSimpleName());
    }

    public JobCat(String str) {
        this(str, true);
    }

    public JobCat(String str, boolean z) {
        this.mTag = str;
        this.mEnabled = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (r0 >= loggers.length) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0022, code lost:
        if (loggers[r0] != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        loggers[r0] = r10;
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        r1 = loggers.length;
        loggers = (com.evernote.android.job.util.JobLogger[]) java.util.Arrays.copyOf(loggers, loggers.length + 2);
        loggers[r1] = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0043, code lost:
        r3 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean addLogger(com.evernote.android.job.util.JobLogger r10) {
        /*
            r4 = 1
            r3 = 0
            java.lang.Class<com.evernote.android.job.util.JobCat> r6 = com.evernote.android.job.util.JobCat.class
            monitor-enter(r6)
            com.evernote.android.job.util.JobLogger[] r7 = loggers     // Catch:{ all -> 0x0045 }
            int r8 = r7.length     // Catch:{ all -> 0x0045 }
            r5 = r3
        L_0x0009:
            if (r5 >= r8) goto L_0x0018
            r2 = r7[r5]     // Catch:{ all -> 0x0045 }
            boolean r9 = r10.equals(r2)     // Catch:{ all -> 0x0045 }
            if (r9 == 0) goto L_0x0015
        L_0x0013:
            monitor-exit(r6)
            return r3
        L_0x0015:
            int r5 = r5 + 1
            goto L_0x0009
        L_0x0018:
            r0 = 0
        L_0x0019:
            com.evernote.android.job.util.JobLogger[] r3 = loggers     // Catch:{ all -> 0x0045 }
            int r3 = r3.length     // Catch:{ all -> 0x0045 }
            if (r0 >= r3) goto L_0x002d
            com.evernote.android.job.util.JobLogger[] r3 = loggers     // Catch:{ all -> 0x0045 }
            r3 = r3[r0]     // Catch:{ all -> 0x0045 }
            if (r3 != 0) goto L_0x002a
            com.evernote.android.job.util.JobLogger[] r3 = loggers     // Catch:{ all -> 0x0045 }
            r3[r0] = r10     // Catch:{ all -> 0x0045 }
            r3 = r4
            goto L_0x0013
        L_0x002a:
            int r0 = r0 + 1
            goto L_0x0019
        L_0x002d:
            com.evernote.android.job.util.JobLogger[] r3 = loggers     // Catch:{ all -> 0x0045 }
            int r1 = r3.length     // Catch:{ all -> 0x0045 }
            com.evernote.android.job.util.JobLogger[] r3 = loggers     // Catch:{ all -> 0x0045 }
            com.evernote.android.job.util.JobLogger[] r5 = loggers     // Catch:{ all -> 0x0045 }
            int r5 = r5.length     // Catch:{ all -> 0x0045 }
            int r5 = r5 + 2
            java.lang.Object[] r3 = java.util.Arrays.copyOf(r3, r5)     // Catch:{ all -> 0x0045 }
            com.evernote.android.job.util.JobLogger[] r3 = (com.evernote.android.job.util.JobLogger[]) r3     // Catch:{ all -> 0x0045 }
            loggers = r3     // Catch:{ all -> 0x0045 }
            com.evernote.android.job.util.JobLogger[] r3 = loggers     // Catch:{ all -> 0x0045 }
            r3[r1] = r10     // Catch:{ all -> 0x0045 }
            r3 = r4
            goto L_0x0013
        L_0x0045:
            r3 = move-exception
            monitor-exit(r6)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.evernote.android.job.util.JobCat.addLogger(com.evernote.android.job.util.JobLogger):boolean");
    }

    public static synchronized void clearLogger() {
        synchronized (JobCat.class) {
            Arrays.fill(loggers, (Object) null);
        }
    }

    public static boolean isLogcatEnabled() {
        return logcatEnabled;
    }

    public static synchronized void removeLogger(JobLogger jobLogger) {
        synchronized (JobCat.class) {
            for (int i = 0; i < loggers.length; i++) {
                if (jobLogger.equals(loggers[i])) {
                    loggers[i] = null;
                }
            }
        }
    }

    public static void setLogcatEnabled(boolean z) {
        logcatEnabled = z;
    }

    public void d(String str) {
        log(3, this.mTag, str, (Throwable) null);
    }

    public void d(String str, Object... objArr) {
        log(3, this.mTag, String.format(str, objArr), (Throwable) null);
    }

    public void d(Throwable th, String str, Object... objArr) {
        log(3, this.mTag, String.format(str, objArr), th);
    }

    public void e(String str) {
        log(6, this.mTag, str, (Throwable) null);
    }

    public void e(String str, Object... objArr) {
        log(6, this.mTag, String.format(str, objArr), (Throwable) null);
    }

    public void e(Throwable th) {
        String message = th.getMessage();
        String str = this.mTag;
        if (message == null) {
            message = "empty message";
        }
        log(6, str, message, th);
    }

    public void e(Throwable th, String str, Object... objArr) {
        log(6, this.mTag, String.format(str, objArr), th);
    }

    public void i(String str) {
        log(4, this.mTag, str, (Throwable) null);
    }

    public void i(String str, Object... objArr) {
        log(4, this.mTag, String.format(str, objArr), (Throwable) null);
    }

    public void log(int i, String str, String str2, Throwable th) {
        if (this.mEnabled) {
            if (logcatEnabled) {
                Log.println(i, str, str2 + (th == null ? "" : 10 + Log.getStackTraceString(th)));
            }
            JobLogger[] jobLoggerArr = loggers;
            if (jobLoggerArr.length > 0) {
                for (JobLogger jobLogger : jobLoggerArr) {
                    if (jobLogger != null) {
                        jobLogger.log(i, str, str2, th);
                    }
                }
            }
        }
    }

    public void w(String str) {
        log(5, this.mTag, str, (Throwable) null);
    }

    public void w(String str, Object... objArr) {
        log(5, this.mTag, String.format(str, objArr), (Throwable) null);
    }

    public void w(Throwable th, String str, Object... objArr) {
        log(5, this.mTag, String.format(str, objArr), th);
    }
}
