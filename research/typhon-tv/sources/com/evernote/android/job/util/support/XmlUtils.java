package com.evernote.android.job.util.support;

import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.pubnative.library.request.PubnativeRequest;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

final class XmlUtils {

    public interface ReadMapCallback {
        Object readThisUnknownObjectXml(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException;
    }

    public interface WriteMapCallback {
        void writeUnknownObject(Object obj, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final double[] m7565(XmlPullParser xmlPullParser, String str, String[] strArr) throws XmlPullParserException, IOException {
        try {
            int parseInt = Integer.parseInt(xmlPullParser.getAttributeValue((String) null, "num"));
            xmlPullParser.next();
            double[] dArr = new double[parseInt];
            int i = 0;
            int eventType = xmlPullParser.getEventType();
            do {
                if (eventType == 2) {
                    if (xmlPullParser.getName().equals("item")) {
                        try {
                            dArr[i] = Double.parseDouble(xmlPullParser.getAttributeValue((String) null, "value"));
                        } catch (NullPointerException e) {
                            throw new XmlPullParserException("Need value attribute in item");
                        } catch (NumberFormatException e2) {
                            throw new XmlPullParserException("Not a number in value attribute in item");
                        }
                    } else {
                        throw new XmlPullParserException("Expected item tag at: " + xmlPullParser.getName());
                    }
                } else if (eventType == 3) {
                    if (xmlPullParser.getName().equals(str)) {
                        return dArr;
                    }
                    if (xmlPullParser.getName().equals("item")) {
                        i++;
                    } else {
                        throw new XmlPullParserException("Expected " + str + " end tag at: " + xmlPullParser.getName());
                    }
                }
                eventType = xmlPullParser.next();
            } while (eventType != 1);
            throw new XmlPullParserException("Document ended before " + str + " end tag");
        } catch (NullPointerException e3) {
            throw new XmlPullParserException("Need num attribute in double-array");
        } catch (NumberFormatException e4) {
            throw new XmlPullParserException("Not a number in num attribute in double-array");
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final String[] m7566(XmlPullParser xmlPullParser, String str, String[] strArr) throws XmlPullParserException, IOException {
        try {
            int parseInt = Integer.parseInt(xmlPullParser.getAttributeValue((String) null, "num"));
            xmlPullParser.next();
            String[] strArr2 = new String[parseInt];
            int i = 0;
            int eventType = xmlPullParser.getEventType();
            do {
                if (eventType == 2) {
                    if (xmlPullParser.getName().equals("item")) {
                        try {
                            strArr2[i] = xmlPullParser.getAttributeValue((String) null, "value");
                        } catch (NullPointerException e) {
                            throw new XmlPullParserException("Need value attribute in item");
                        } catch (NumberFormatException e2) {
                            throw new XmlPullParserException("Not a number in value attribute in item");
                        }
                    } else if (xmlPullParser.getName().equals("null")) {
                        strArr2[i] = null;
                    } else {
                        throw new XmlPullParserException("Expected item tag at: " + xmlPullParser.getName());
                    }
                } else if (eventType == 3) {
                    if (xmlPullParser.getName().equals(str)) {
                        return strArr2;
                    }
                    if (xmlPullParser.getName().equals("item") || xmlPullParser.getName().equals("null")) {
                        i++;
                    } else {
                        throw new XmlPullParserException("Expected " + str + " end tag at: " + xmlPullParser.getName());
                    }
                }
                eventType = xmlPullParser.next();
            } while (eventType != 1);
            throw new XmlPullParserException("Document ended before " + str + " end tag");
        } catch (NullPointerException e3) {
            throw new XmlPullParserException("Need num attribute in string-array");
        } catch (NumberFormatException e4) {
            throw new XmlPullParserException("Not a number in num attribute in string-array");
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static final long[] m7567(XmlPullParser xmlPullParser, String str, String[] strArr) throws XmlPullParserException, IOException {
        try {
            int parseInt = Integer.parseInt(xmlPullParser.getAttributeValue((String) null, "num"));
            xmlPullParser.next();
            long[] jArr = new long[parseInt];
            int i = 0;
            int eventType = xmlPullParser.getEventType();
            do {
                if (eventType == 2) {
                    if (xmlPullParser.getName().equals("item")) {
                        try {
                            jArr[i] = Long.parseLong(xmlPullParser.getAttributeValue((String) null, "value"));
                        } catch (NullPointerException e) {
                            throw new XmlPullParserException("Need value attribute in item");
                        } catch (NumberFormatException e2) {
                            throw new XmlPullParserException("Not a number in value attribute in item");
                        }
                    } else {
                        throw new XmlPullParserException("Expected item tag at: " + xmlPullParser.getName());
                    }
                } else if (eventType == 3) {
                    if (xmlPullParser.getName().equals(str)) {
                        return jArr;
                    }
                    if (xmlPullParser.getName().equals("item")) {
                        i++;
                    } else {
                        throw new XmlPullParserException("Expected " + str + " end tag at: " + xmlPullParser.getName());
                    }
                }
                eventType = xmlPullParser.next();
            } while (eventType != 1);
            throw new XmlPullParserException("Document ended before " + str + " end tag");
        } catch (NullPointerException e3) {
            throw new XmlPullParserException("Need num attribute in long-array");
        } catch (NumberFormatException e4) {
            throw new XmlPullParserException("Not a number in num attribute in long-array");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ArrayList m7568(XmlPullParser xmlPullParser, String str, String[] strArr) throws XmlPullParserException, IOException {
        return m7569(xmlPullParser, str, strArr, (ReadMapCallback) null);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ArrayList m7569(XmlPullParser xmlPullParser, String str, String[] strArr, ReadMapCallback readMapCallback) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        int eventType = xmlPullParser.getEventType();
        do {
            if (eventType == 2) {
                arrayList.add(m7575(xmlPullParser, strArr, readMapCallback));
            } else if (eventType == 3) {
                if (xmlPullParser.getName().equals(str)) {
                    return arrayList;
                }
                throw new XmlPullParserException("Expected " + str + " end tag at: " + xmlPullParser.getName());
            }
            eventType = xmlPullParser.next();
        } while (eventType != 1);
        throw new XmlPullParserException("Document ended before " + str + " end tag");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static final int[] m7570(XmlPullParser xmlPullParser, String str, String[] strArr) throws XmlPullParserException, IOException {
        try {
            int parseInt = Integer.parseInt(xmlPullParser.getAttributeValue((String) null, "num"));
            xmlPullParser.next();
            int[] iArr = new int[parseInt];
            int i = 0;
            int eventType = xmlPullParser.getEventType();
            do {
                if (eventType == 2) {
                    if (xmlPullParser.getName().equals("item")) {
                        try {
                            iArr[i] = Integer.parseInt(xmlPullParser.getAttributeValue((String) null, "value"));
                        } catch (NullPointerException e) {
                            throw new XmlPullParserException("Need value attribute in item");
                        } catch (NumberFormatException e2) {
                            throw new XmlPullParserException("Not a number in value attribute in item");
                        }
                    } else {
                        throw new XmlPullParserException("Expected item tag at: " + xmlPullParser.getName());
                    }
                } else if (eventType == 3) {
                    if (xmlPullParser.getName().equals(str)) {
                        return iArr;
                    }
                    if (xmlPullParser.getName().equals("item")) {
                        i++;
                    } else {
                        throw new XmlPullParserException("Expected " + str + " end tag at: " + xmlPullParser.getName());
                    }
                }
                eventType = xmlPullParser.next();
            } while (eventType != 1);
            throw new XmlPullParserException("Document ended before " + str + " end tag");
        } catch (NullPointerException e3) {
            throw new XmlPullParserException("Need num attribute in byte-array");
        } catch (NumberFormatException e4) {
            throw new XmlPullParserException("Not a number in num attribute in byte-array");
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static final HashSet m7571(XmlPullParser xmlPullParser, String str, String[] strArr) throws XmlPullParserException, IOException {
        return m7572(xmlPullParser, str, strArr, (ReadMapCallback) null);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static final HashSet m7572(XmlPullParser xmlPullParser, String str, String[] strArr, ReadMapCallback readMapCallback) throws XmlPullParserException, IOException {
        HashSet hashSet = new HashSet();
        int eventType = xmlPullParser.getEventType();
        do {
            if (eventType == 2) {
                hashSet.add(m7575(xmlPullParser, strArr, readMapCallback));
            } else if (eventType == 3) {
                if (xmlPullParser.getName().equals(str)) {
                    return hashSet;
                }
                throw new XmlPullParserException("Expected " + str + " end tag at: " + xmlPullParser.getName());
            }
            eventType = xmlPullParser.next();
        } while (eventType != 1);
        throw new XmlPullParserException("Document ended before " + str + " end tag");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object m7573(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        try {
            if (str.equals("int")) {
                return Integer.valueOf(Integer.parseInt(xmlPullParser.getAttributeValue((String) null, "value")));
            }
            if (str.equals(PubnativeRequest.Parameters.LONG)) {
                return Long.valueOf(xmlPullParser.getAttributeValue((String) null, "value"));
            }
            if (str.equals("float")) {
                return Float.valueOf(xmlPullParser.getAttributeValue((String) null, "value"));
            }
            if (str.equals("double")) {
                return Double.valueOf(xmlPullParser.getAttributeValue((String) null, "value"));
            }
            if (str.equals("boolean")) {
                return Boolean.valueOf(xmlPullParser.getAttributeValue((String) null, "value"));
            }
            return null;
        } catch (NullPointerException e) {
            throw new XmlPullParserException("Need value attribute in <" + str + ">");
        } catch (NumberFormatException e2) {
            throw new XmlPullParserException("Not a number in value attribute in <" + str + ">");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Object m7574(XmlPullParser xmlPullParser, String[] strArr) throws XmlPullParserException, IOException {
        int eventType = xmlPullParser.getEventType();
        while (eventType != 2) {
            if (eventType == 3) {
                throw new XmlPullParserException("Unexpected end tag at: " + xmlPullParser.getName());
            } else if (eventType == 4) {
                throw new XmlPullParserException("Unexpected text: " + xmlPullParser.getText());
            } else {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    throw new XmlPullParserException("Unexpected end of document");
                }
            }
        }
        return m7575(xmlPullParser, strArr, (ReadMapCallback) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object m7575(XmlPullParser xmlPullParser, String[] strArr, ReadMapCallback readMapCallback) throws XmlPullParserException, IOException {
        Object obj;
        int next;
        String attributeValue = xmlPullParser.getAttributeValue((String) null, "name");
        String name = xmlPullParser.getName();
        if (name.equals("null")) {
            obj = null;
        } else if (name.equals("string")) {
            String str = "";
            while (true) {
                int next2 = xmlPullParser.next();
                if (next2 == 1) {
                    throw new XmlPullParserException("Unexpected end of document in <string>");
                } else if (next2 == 3) {
                    if (xmlPullParser.getName().equals("string")) {
                        strArr[0] = attributeValue;
                        return str;
                    }
                    throw new XmlPullParserException("Unexpected end tag in <string>: " + xmlPullParser.getName());
                } else if (next2 == 4) {
                    str = str + xmlPullParser.getText();
                } else if (next2 == 2) {
                    throw new XmlPullParserException("Unexpected start tag in <string>: " + xmlPullParser.getName());
                }
            }
        } else {
            obj = m7573(xmlPullParser, name);
            if (obj == null) {
                if (name.equals("int-array")) {
                    int[] r1 = m7570(xmlPullParser, "int-array", strArr);
                    strArr[0] = attributeValue;
                    return r1;
                } else if (name.equals("long-array")) {
                    long[] r12 = m7567(xmlPullParser, "long-array", strArr);
                    strArr[0] = attributeValue;
                    return r12;
                } else if (name.equals("double-array")) {
                    double[] r13 = m7565(xmlPullParser, "double-array", strArr);
                    strArr[0] = attributeValue;
                    return r13;
                } else if (name.equals("string-array")) {
                    String[] r14 = m7566(xmlPullParser, "string-array", strArr);
                    strArr[0] = attributeValue;
                    return r14;
                } else if (name.equals("map")) {
                    xmlPullParser.next();
                    HashMap<String, ?> r15 = m7577(xmlPullParser, "map", strArr);
                    strArr[0] = attributeValue;
                    return r15;
                } else if (name.equals("list")) {
                    xmlPullParser.next();
                    ArrayList r16 = m7568(xmlPullParser, "list", strArr);
                    strArr[0] = attributeValue;
                    return r16;
                } else if (name.equals("set")) {
                    xmlPullParser.next();
                    HashSet r17 = m7571(xmlPullParser, "set", strArr);
                    strArr[0] = attributeValue;
                    return r17;
                } else if (readMapCallback != null) {
                    Object readThisUnknownObjectXml = readMapCallback.readThisUnknownObjectXml(xmlPullParser, name);
                    strArr[0] = attributeValue;
                    return readThisUnknownObjectXml;
                } else {
                    throw new XmlPullParserException("Unknown tag: " + name);
                }
            }
        }
        do {
            next = xmlPullParser.next();
            if (next == 1) {
                throw new XmlPullParserException("Unexpected end of document in <" + name + ">");
            } else if (next == 3) {
                if (xmlPullParser.getName().equals(name)) {
                    strArr[0] = attributeValue;
                    return obj;
                }
                throw new XmlPullParserException("Unexpected end tag in <" + name + ">: " + xmlPullParser.getName());
            } else if (next == 4) {
                throw new XmlPullParserException("Unexpected text in <" + name + ">: " + xmlPullParser.getName());
            }
        } while (next != 2);
        throw new XmlPullParserException("Unexpected start tag in <" + name + ">: " + xmlPullParser.getName());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final HashMap<String, ?> m7576(InputStream inputStream) throws XmlPullParserException, IOException {
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setInput(inputStream, (String) null);
        return (HashMap) m7574(newPullParser, new String[1]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final HashMap<String, ?> m7577(XmlPullParser xmlPullParser, String str, String[] strArr) throws XmlPullParserException, IOException {
        return m7578(xmlPullParser, str, strArr, (ReadMapCallback) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final HashMap<String, ?> m7578(XmlPullParser xmlPullParser, String str, String[] strArr, ReadMapCallback readMapCallback) throws XmlPullParserException, IOException {
        HashMap<String, ?> hashMap = new HashMap<>();
        int eventType = xmlPullParser.getEventType();
        do {
            if (eventType == 2) {
                hashMap.put(strArr[0], m7575(xmlPullParser, strArr, readMapCallback));
            } else if (eventType == 3) {
                if (xmlPullParser.getName().equals(str)) {
                    return hashMap;
                }
                throw new XmlPullParserException("Expected " + str + " end tag at: " + xmlPullParser.getName());
            }
            eventType = xmlPullParser.next();
        } while (eventType != 1);
        throw new XmlPullParserException("Document ended before " + str + " end tag");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7579(Object obj, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        m7580(obj, str, xmlSerializer, (WriteMapCallback) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static final void m7580(Object obj, String str, XmlSerializer xmlSerializer, WriteMapCallback writeMapCallback) throws XmlPullParserException, IOException {
        String str2;
        if (obj == null) {
            xmlSerializer.startTag((String) null, "null");
            if (str != null) {
                xmlSerializer.attribute((String) null, "name", str);
            }
            xmlSerializer.endTag((String) null, "null");
        } else if (obj instanceof String) {
            xmlSerializer.startTag((String) null, "string");
            if (str != null) {
                xmlSerializer.attribute((String) null, "name", str);
            }
            xmlSerializer.text(obj.toString());
            xmlSerializer.endTag((String) null, "string");
        } else {
            if (obj instanceof Integer) {
                str2 = "int";
            } else if (obj instanceof Long) {
                str2 = PubnativeRequest.Parameters.LONG;
            } else if (obj instanceof Float) {
                str2 = "float";
            } else if (obj instanceof Double) {
                str2 = "double";
            } else if (obj instanceof Boolean) {
                str2 = "boolean";
            } else if (obj instanceof byte[]) {
                m7587((byte[]) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof int[]) {
                m7589((int[]) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof long[]) {
                m7590((long[]) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof double[]) {
                m7588((double[]) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof String[]) {
                m7591((String[]) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof Map) {
                m7583((Map) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof List) {
                m7581((List) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof Set) {
                m7586((Set) obj, str, xmlSerializer);
                return;
            } else if (obj instanceof CharSequence) {
                xmlSerializer.startTag((String) null, "string");
                if (str != null) {
                    xmlSerializer.attribute((String) null, "name", str);
                }
                xmlSerializer.text(obj.toString());
                xmlSerializer.endTag((String) null, "string");
                return;
            } else if (writeMapCallback != null) {
                writeMapCallback.writeUnknownObject(obj, str, xmlSerializer);
                return;
            } else {
                throw new RuntimeException("writeValueXml: unable to write value " + obj);
            }
            xmlSerializer.startTag((String) null, str2);
            if (str != null) {
                xmlSerializer.attribute((String) null, "name", str);
            }
            xmlSerializer.attribute((String) null, "value", obj.toString());
            xmlSerializer.endTag((String) null, str2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7581(List list, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        if (list == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "list");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        int size = list.size();
        for (int i = 0; i < size; i++) {
            m7579(list.get(i), (String) null, xmlSerializer);
        }
        xmlSerializer.endTag((String) null, "list");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7582(Map map, OutputStream outputStream) throws XmlPullParserException, IOException {
        FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
        fastXmlSerializer.setOutput(outputStream, "utf-8");
        fastXmlSerializer.startDocument((String) null, true);
        fastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        m7583(map, (String) null, (XmlSerializer) fastXmlSerializer);
        fastXmlSerializer.endDocument();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7583(Map map, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        m7584(map, str, xmlSerializer, (WriteMapCallback) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7584(Map map, String str, XmlSerializer xmlSerializer, WriteMapCallback writeMapCallback) throws XmlPullParserException, IOException {
        if (map == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "map");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        m7585(map, xmlSerializer, writeMapCallback);
        xmlSerializer.endTag((String) null, "map");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7585(Map map, XmlSerializer xmlSerializer, WriteMapCallback writeMapCallback) throws XmlPullParserException, IOException {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                m7580(entry.getValue(), (String) entry.getKey(), xmlSerializer, writeMapCallback);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7586(Set set, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        if (set == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "set");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        for (Object r0 : set) {
            m7579(r0, (String) null, xmlSerializer);
        }
        xmlSerializer.endTag((String) null, "set");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7587(byte[] bArr, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        if (bArr == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "byte-array");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        xmlSerializer.attribute((String) null, "num", Integer.toString(r3));
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            int i = b >> 4;
            sb.append(i >= 10 ? (i + 97) - 10 : i + 48);
            byte b2 = b & 255;
            sb.append(b2 >= 10 ? (b2 + 97) - 10 : b2 + 48);
        }
        xmlSerializer.text(sb.toString());
        xmlSerializer.endTag((String) null, "byte-array");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7588(double[] dArr, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        if (dArr == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "double-array");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        xmlSerializer.attribute((String) null, "num", Integer.toString(r1));
        for (double d : dArr) {
            xmlSerializer.startTag((String) null, "item");
            xmlSerializer.attribute((String) null, "value", Double.toString(d));
            xmlSerializer.endTag((String) null, "item");
        }
        xmlSerializer.endTag((String) null, "double-array");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7589(int[] iArr, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        if (iArr == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "int-array");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        xmlSerializer.attribute((String) null, "num", Integer.toString(r1));
        for (int num : iArr) {
            xmlSerializer.startTag((String) null, "item");
            xmlSerializer.attribute((String) null, "value", Integer.toString(num));
            xmlSerializer.endTag((String) null, "item");
        }
        xmlSerializer.endTag((String) null, "int-array");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7590(long[] jArr, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        if (jArr == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "long-array");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        xmlSerializer.attribute((String) null, "num", Integer.toString(r1));
        for (long l : jArr) {
            xmlSerializer.startTag((String) null, "item");
            xmlSerializer.attribute((String) null, "value", Long.toString(l));
            xmlSerializer.endTag((String) null, "item");
        }
        xmlSerializer.endTag((String) null, "long-array");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m7591(String[] strArr, String str, XmlSerializer xmlSerializer) throws XmlPullParserException, IOException {
        if (strArr == null) {
            xmlSerializer.startTag((String) null, "null");
            xmlSerializer.endTag((String) null, "null");
            return;
        }
        xmlSerializer.startTag((String) null, "string-array");
        if (str != null) {
            xmlSerializer.attribute((String) null, "name", str);
        }
        int length = strArr.length;
        xmlSerializer.attribute((String) null, "num", Integer.toString(length));
        for (int i = 0; i < length; i++) {
            if (strArr[i] == null) {
                xmlSerializer.startTag((String) null, "null");
                xmlSerializer.endTag((String) null, "null");
            } else {
                xmlSerializer.startTag((String) null, "item");
                xmlSerializer.attribute((String) null, "value", strArr[i]);
                xmlSerializer.endTag((String) null, "item");
            }
        }
        xmlSerializer.endTag((String) null, "string-array");
    }
}
