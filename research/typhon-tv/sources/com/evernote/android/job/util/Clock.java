package com.evernote.android.job.util;

import android.os.SystemClock;

public interface Clock {
    public static final Clock DEFAULT = new Clock() {
        public long currentTimeMillis() {
            return System.currentTimeMillis();
        }

        public long elapsedRealtime() {
            return SystemClock.elapsedRealtime();
        }
    };

    long currentTimeMillis();

    long elapsedRealtime();
}
