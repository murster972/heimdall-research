package com.evernote.android.job.util.support;

import com.evernote.android.job.util.JobCat;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.xmlpull.v1.XmlPullParserException;

public final class PersistableBundleCompat {
    private static final JobCat CAT = new JobCat("PersistableBundleCompat");
    private static final String UTF_8 = "UTF-8";
    private final Map<String, Object> mValues;

    public PersistableBundleCompat() {
        this((Map<String, Object>) new HashMap());
    }

    public PersistableBundleCompat(PersistableBundleCompat persistableBundleCompat) {
        this((Map<String, Object>) new HashMap(persistableBundleCompat.mValues));
    }

    private PersistableBundleCompat(Map<String, Object> map) {
        this.mValues = map;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002b A[SYNTHETIC, Splitter:B:15:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003e A[SYNTHETIC, Splitter:B:22:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047 A[SYNTHETIC, Splitter:B:27:0x0047] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0032=Splitter:B:19:0x0032, B:12:0x001f=Splitter:B:12:0x001f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.evernote.android.job.util.support.PersistableBundleCompat fromXml(java.lang.String r6) {
        /*
            r1 = 0
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ XmlPullParserException -> 0x001d, IOException -> 0x004b, VerifyError -> 0x0031 }
            java.lang.String r4 = "UTF-8"
            byte[] r4 = r6.getBytes(r4)     // Catch:{ XmlPullParserException -> 0x001d, IOException -> 0x004b, VerifyError -> 0x0031 }
            r2.<init>(r4)     // Catch:{ XmlPullParserException -> 0x001d, IOException -> 0x004b, VerifyError -> 0x0031 }
            java.util.HashMap r3 = com.evernote.android.job.util.support.XmlUtils.m7576(r2)     // Catch:{ XmlPullParserException -> 0x0058, IOException -> 0x005b, VerifyError -> 0x0055, all -> 0x0052 }
            com.evernote.android.job.util.support.PersistableBundleCompat r4 = new com.evernote.android.job.util.support.PersistableBundleCompat     // Catch:{ XmlPullParserException -> 0x0058, IOException -> 0x005b, VerifyError -> 0x0055, all -> 0x0052 }
            r4.<init>((java.util.Map<java.lang.String, java.lang.Object>) r3)     // Catch:{ XmlPullParserException -> 0x0058, IOException -> 0x005b, VerifyError -> 0x0055, all -> 0x0052 }
            if (r2 == 0) goto L_0x001b
            r2.close()     // Catch:{ IOException -> 0x004e }
        L_0x001b:
            r1 = r2
        L_0x001c:
            return r4
        L_0x001d:
            r4 = move-exception
        L_0x001e:
            r0 = r4
        L_0x001f:
            com.evernote.android.job.util.JobCat r4 = CAT     // Catch:{ all -> 0x0044 }
            r4.e((java.lang.Throwable) r0)     // Catch:{ all -> 0x0044 }
            com.evernote.android.job.util.support.PersistableBundleCompat r4 = new com.evernote.android.job.util.support.PersistableBundleCompat     // Catch:{ all -> 0x0044 }
            r4.<init>()     // Catch:{ all -> 0x0044 }
            if (r1 == 0) goto L_0x001c
            r1.close()     // Catch:{ IOException -> 0x002f }
            goto L_0x001c
        L_0x002f:
            r5 = move-exception
            goto L_0x001c
        L_0x0031:
            r0 = move-exception
        L_0x0032:
            com.evernote.android.job.util.JobCat r4 = CAT     // Catch:{ all -> 0x0044 }
            r4.e((java.lang.Throwable) r0)     // Catch:{ all -> 0x0044 }
            com.evernote.android.job.util.support.PersistableBundleCompat r4 = new com.evernote.android.job.util.support.PersistableBundleCompat     // Catch:{ all -> 0x0044 }
            r4.<init>()     // Catch:{ all -> 0x0044 }
            if (r1 == 0) goto L_0x001c
            r1.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x001c
        L_0x0042:
            r5 = move-exception
            goto L_0x001c
        L_0x0044:
            r4 = move-exception
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x0050 }
        L_0x004a:
            throw r4
        L_0x004b:
            r4 = move-exception
        L_0x004c:
            r0 = r4
            goto L_0x001f
        L_0x004e:
            r5 = move-exception
            goto L_0x001b
        L_0x0050:
            r5 = move-exception
            goto L_0x004a
        L_0x0052:
            r4 = move-exception
            r1 = r2
            goto L_0x0045
        L_0x0055:
            r0 = move-exception
            r1 = r2
            goto L_0x0032
        L_0x0058:
            r4 = move-exception
            r1 = r2
            goto L_0x001e
        L_0x005b:
            r4 = move-exception
            r1 = r2
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.evernote.android.job.util.support.PersistableBundleCompat.fromXml(java.lang.String):com.evernote.android.job.util.support.PersistableBundleCompat");
    }

    public void clear() {
        this.mValues.clear();
    }

    public boolean containsKey(String str) {
        return this.mValues.containsKey(str);
    }

    public Object get(String str) {
        return this.mValues.get(str);
    }

    public boolean getBoolean(String str, boolean z) {
        Object obj = this.mValues.get(str);
        return obj instanceof Boolean ? ((Boolean) obj).booleanValue() : z;
    }

    public double getDouble(String str, double d) {
        Object obj = this.mValues.get(str);
        return obj instanceof Double ? ((Double) obj).doubleValue() : d;
    }

    public double[] getDoubleArray(String str) {
        Object obj = this.mValues.get(str);
        if (obj instanceof double[]) {
            return (double[]) obj;
        }
        return null;
    }

    public int getInt(String str, int i) {
        Object obj = this.mValues.get(str);
        return obj instanceof Integer ? ((Integer) obj).intValue() : i;
    }

    public int[] getIntArray(String str) {
        Object obj = this.mValues.get(str);
        if (obj instanceof int[]) {
            return (int[]) obj;
        }
        return null;
    }

    public long getLong(String str, long j) {
        Object obj = this.mValues.get(str);
        return obj instanceof Long ? ((Long) obj).longValue() : j;
    }

    public long[] getLongArray(String str) {
        Object obj = this.mValues.get(str);
        if (obj instanceof long[]) {
            return (long[]) obj;
        }
        return null;
    }

    public PersistableBundleCompat getPersistableBundleCompat(String str) {
        Object obj = this.mValues.get(str);
        if (obj instanceof Map) {
            return new PersistableBundleCompat((Map<String, Object>) (Map) obj);
        }
        return null;
    }

    public String getString(String str, String str2) {
        Object obj = this.mValues.get(str);
        return obj instanceof String ? (String) obj : str2;
    }

    public String[] getStringArray(String str) {
        Object obj = this.mValues.get(str);
        if (obj instanceof String[]) {
            return (String[]) obj;
        }
        return null;
    }

    public boolean isEmpty() {
        return this.mValues.isEmpty();
    }

    public Set<String> keySet() {
        return this.mValues.keySet();
    }

    public void putAll(PersistableBundleCompat persistableBundleCompat) {
        this.mValues.putAll(persistableBundleCompat.mValues);
    }

    public void putBoolean(String str, boolean z) {
        this.mValues.put(str, Boolean.valueOf(z));
    }

    public void putDouble(String str, double d) {
        this.mValues.put(str, Double.valueOf(d));
    }

    public void putDoubleArray(String str, double[] dArr) {
        this.mValues.put(str, dArr);
    }

    public void putInt(String str, int i) {
        this.mValues.put(str, Integer.valueOf(i));
    }

    public void putIntArray(String str, int[] iArr) {
        this.mValues.put(str, iArr);
    }

    public void putLong(String str, long j) {
        this.mValues.put(str, Long.valueOf(j));
    }

    public void putLongArray(String str, long[] jArr) {
        this.mValues.put(str, jArr);
    }

    public void putPersistableBundleCompat(String str, PersistableBundleCompat persistableBundleCompat) {
        this.mValues.put(str, persistableBundleCompat == null ? null : persistableBundleCompat.mValues);
    }

    public void putString(String str, String str2) {
        this.mValues.put(str, str2);
    }

    public void putStringArray(String str, String[] strArr) {
        this.mValues.put(str, strArr);
    }

    public void remove(String str) {
        this.mValues.remove(str);
    }

    public String saveToXml() {
        Throwable th;
        String str;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            XmlUtils.m7582((Map) this.mValues, (OutputStream) byteArrayOutputStream);
            str = byteArrayOutputStream.toString("UTF-8");
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
            }
        } catch (XmlPullParserException e2) {
            th = e2;
        } catch (IOException e3) {
            th = e3;
        } catch (Error e4) {
            CAT.e((Throwable) e4);
            str = "";
            try {
                byteArrayOutputStream.close();
            } catch (IOException e5) {
            }
        } catch (Throwable th2) {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e6) {
            }
            throw th2;
        }
        return str;
        CAT.e(th);
        str = "";
        try {
            byteArrayOutputStream.close();
        } catch (IOException e7) {
        }
        return str;
    }

    public int size() {
        return this.mValues.size();
    }
}
