package com.evernote.android.job.util.support;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import org.xmlpull.v1.XmlSerializer;

class FastXmlSerializer implements XmlSerializer {

    /* renamed from: 靐  reason: contains not printable characters */
    private static String f6587 = "                                                              ";

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f6588 = {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "&quot;", null, null, null, "&amp;", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "&lt;", null, "&gt;", null};

    /* renamed from: ʻ  reason: contains not printable characters */
    private OutputStream f6589;

    /* renamed from: ʼ  reason: contains not printable characters */
    private CharsetEncoder f6590;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ByteBuffer f6591 = ByteBuffer.allocate(8192);

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f6592 = true;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f6593 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f6594;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f6595 = 0;

    /* renamed from: 连任  reason: contains not printable characters */
    private Writer f6596;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f6597;

    /* renamed from: 齉  reason: contains not printable characters */
    private final char[] f6598 = new char[8192];

    FastXmlSerializer() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m7557(String str) throws IOException {
        String str2;
        int length = str.length();
        char length2 = (char) f6588.length;
        String[] strArr = f6588;
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (charAt < length2 && (str2 = strArr[charAt]) != null) {
                if (i < i2) {
                    m7563(str, i, i2 - i);
                }
                i = i2 + 1;
                m7562(str2);
            }
            i2++;
        }
        if (i < i2) {
            m7563(str, i, i2 - i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m7558(char[] cArr, int i, int i2) throws IOException {
        String str;
        char length = (char) f6588.length;
        String[] strArr = f6588;
        int i3 = i + i2;
        int i4 = i;
        int i5 = i;
        while (i5 < i3) {
            char c = cArr[i5];
            if (c < length && (str = strArr[c]) != null) {
                if (i4 < i5) {
                    m7564(cArr, i4, i5 - i4);
                }
                i4 = i5 + 1;
                m7562(str);
            }
            i5++;
        }
        if (i4 < i5) {
            m7564(cArr, i4, i5 - i4);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7559() throws IOException {
        int position = this.f6591.position();
        if (position > 0) {
            this.f6591.flip();
            this.f6589.write(this.f6591.array(), 0, position);
            this.f6591.clear();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7560(char c) throws IOException {
        int i = this.f6597;
        if (i >= 8191) {
            flush();
            i = this.f6597;
        }
        this.f6598[i] = c;
        this.f6597 = i + 1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7561(int i) throws IOException {
        int i2 = i * 4;
        if (i2 > f6587.length()) {
            i2 = f6587.length();
        }
        m7563(f6587, 0, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7562(String str) throws IOException {
        m7563(str, 0, str.length());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7563(String str, int i, int i2) throws IOException {
        if (i2 > 8192) {
            int i3 = i + i2;
            while (i < i3) {
                int i4 = i + 8192;
                m7563(str, i, i4 < i3 ? 8192 : i3 - i);
                i = i4;
            }
            return;
        }
        int i5 = this.f6597;
        if (i5 + i2 > 8192) {
            flush();
            i5 = this.f6597;
        }
        str.getChars(i, i + i2, this.f6598, i5);
        this.f6597 = i5 + i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7564(char[] cArr, int i, int i2) throws IOException {
        if (i2 > 8192) {
            int i3 = i + i2;
            while (i < i3) {
                int i4 = i + 8192;
                m7564(cArr, i, i4 < i3 ? 8192 : i3 - i);
                i = i4;
            }
            return;
        }
        int i5 = this.f6597;
        if (i5 + i2 > 8192) {
            flush();
            i5 = this.f6597;
        }
        System.arraycopy(cArr, i, this.f6598, i5, i2);
        this.f6597 = i5 + i2;
    }

    public XmlSerializer attribute(String str, String str2, String str3) throws IOException, IllegalArgumentException, IllegalStateException {
        m7560(' ');
        if (str != null) {
            m7562(str);
            m7560(':');
        }
        m7562(str2);
        m7562("=\"");
        m7557(str3);
        m7560('\"');
        this.f6592 = false;
        return this;
    }

    public void cdsect(String str) throws IOException, IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void comment(String str) throws IOException, IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void docdecl(String str) throws IOException, IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void endDocument() throws IOException, IllegalArgumentException, IllegalStateException {
        flush();
    }

    public XmlSerializer endTag(String str, String str2) throws IOException, IllegalArgumentException, IllegalStateException {
        this.f6595--;
        if (this.f6594) {
            m7562(" />\n");
        } else {
            if (this.f6593 && this.f6592) {
                m7561(this.f6595);
            }
            m7562("</");
            if (str != null) {
                m7562(str);
                m7560(':');
            }
            m7562(str2);
            m7562(">\n");
        }
        this.f6592 = true;
        this.f6594 = false;
        return this;
    }

    public void entityRef(String str) throws IOException, IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void flush() throws IOException {
        if (this.f6597 > 0) {
            if (this.f6589 != null) {
                CharBuffer wrap = CharBuffer.wrap(this.f6598, 0, this.f6597);
                CoderResult encode = this.f6590.encode(wrap, this.f6591, true);
                while (!encode.isError()) {
                    if (encode.isOverflow()) {
                        m7559();
                        encode = this.f6590.encode(wrap, this.f6591, true);
                    } else {
                        m7559();
                        this.f6589.flush();
                    }
                }
                throw new IOException(encode.toString());
            }
            this.f6596.write(this.f6598, 0, this.f6597);
            this.f6596.flush();
            this.f6597 = 0;
        }
    }

    public int getDepth() {
        throw new UnsupportedOperationException();
    }

    public boolean getFeature(String str) {
        throw new UnsupportedOperationException();
    }

    public String getName() {
        throw new UnsupportedOperationException();
    }

    public String getNamespace() {
        throw new UnsupportedOperationException();
    }

    public String getPrefix(String str, boolean z) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }

    public Object getProperty(String str) {
        throw new UnsupportedOperationException();
    }

    public void ignorableWhitespace(String str) throws IOException, IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void processingInstruction(String str) throws IOException, IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void setFeature(String str, boolean z) throws IllegalArgumentException, IllegalStateException {
        if (str.equals("http://xmlpull.org/v1/doc/features.html#indent-output")) {
            this.f6593 = true;
            return;
        }
        throw new UnsupportedOperationException();
    }

    public void setOutput(OutputStream outputStream, String str) throws IOException, IllegalArgumentException, IllegalStateException {
        if (outputStream == null) {
            throw new IllegalArgumentException();
        }
        try {
            this.f6590 = Charset.forName(str).newEncoder();
            this.f6589 = outputStream;
        } catch (IllegalCharsetNameException e) {
            throw ((UnsupportedEncodingException) new UnsupportedEncodingException(str).initCause(e));
        } catch (UnsupportedCharsetException e2) {
            throw ((UnsupportedEncodingException) new UnsupportedEncodingException(str).initCause(e2));
        }
    }

    public void setOutput(Writer writer) throws IOException, IllegalArgumentException, IllegalStateException {
        this.f6596 = writer;
    }

    public void setPrefix(String str, String str2) throws IOException, IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void setProperty(String str, Object obj) throws IllegalArgumentException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public void startDocument(String str, Boolean bool) throws IOException, IllegalArgumentException, IllegalStateException {
        m7562("<?xml version='1.0' encoding='utf-8' standalone='" + (bool.booleanValue() ? "yes" : "no") + "' ?>\n");
        this.f6592 = true;
    }

    public XmlSerializer startTag(String str, String str2) throws IOException, IllegalArgumentException, IllegalStateException {
        if (this.f6594) {
            m7562(">\n");
        }
        if (this.f6593) {
            m7561(this.f6595);
        }
        this.f6595++;
        m7560('<');
        if (str != null) {
            m7562(str);
            m7560(':');
        }
        m7562(str2);
        this.f6594 = true;
        this.f6592 = false;
        return this;
    }

    public XmlSerializer text(String str) throws IOException, IllegalArgumentException, IllegalStateException {
        boolean z = false;
        if (this.f6594) {
            m7562(">");
            this.f6594 = false;
        }
        m7557(str);
        if (this.f6593) {
            if (str.length() > 0 && str.charAt(str.length() - 1) == 10) {
                z = true;
            }
            this.f6592 = z;
        }
        return this;
    }

    public XmlSerializer text(char[] cArr, int i, int i2) throws IOException, IllegalArgumentException, IllegalStateException {
        boolean z = false;
        if (this.f6594) {
            m7562(">");
            this.f6594 = false;
        }
        m7558(cArr, i, i2);
        if (this.f6593) {
            if (cArr[(i + i2) - 1] == 10) {
                z = true;
            }
            this.f6592 = z;
        }
        return this;
    }
}
