package com.evernote.android.job.util;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.net.ConnectivityManagerCompat;
import com.evernote.android.job.JobRequest;

public final class Device {
    private Device() {
    }

    @TargetApi(17)
    public static BatteryStatus getBatteryStatus(Context context) {
        boolean z = false;
        Intent registerReceiver = context.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return BatteryStatus.DEFAULT;
        }
        float intExtra = ((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
        int intExtra2 = registerReceiver.getIntExtra("plugged", 0);
        if (intExtra2 == 1 || intExtra2 == 2 || (Build.VERSION.SDK_INT >= 17 && intExtra2 == 4)) {
            z = true;
        }
        return new BatteryStatus(z, intExtra);
    }

    public static JobRequest.NetworkType getNetworkType(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return (activeNetworkInfo == null || !activeNetworkInfo.isConnectedOrConnecting()) ? JobRequest.NetworkType.ANY : !ConnectivityManagerCompat.isActiveNetworkMetered(connectivityManager) ? JobRequest.NetworkType.UNMETERED : activeNetworkInfo.isRoaming() ? JobRequest.NetworkType.CONNECTED : JobRequest.NetworkType.NOT_ROAMING;
        } catch (Throwable th) {
            return JobRequest.NetworkType.ANY;
        }
    }

    public static boolean isIdle(Context context) {
        boolean z = false;
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (Build.VERSION.SDK_INT < 23) {
            return Build.VERSION.SDK_INT >= 20 ? !powerManager.isInteractive() : !powerManager.isScreenOn();
        }
        if (powerManager.isDeviceIdleMode() || !powerManager.isInteractive()) {
            z = true;
        }
        return z;
    }

    public static boolean isStorageLow() {
        return false;
    }
}
