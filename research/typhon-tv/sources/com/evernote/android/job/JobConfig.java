package com.evernote.android.job;

import android.os.Build;
import com.evernote.android.job.util.Clock;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobLogger;
import com.evernote.android.job.util.JobPreconditions;
import java.util.EnumMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class JobConfig {
    private static final JobCat CAT = new JobCat("JobConfig");
    private static final ExecutorService DEFAULT_EXECUTOR_SERVICE = Executors.newCachedThreadPool(new ThreadFactory() {

        /* renamed from: 龘  reason: contains not printable characters */
        private final AtomicInteger f6557 = new AtomicInteger();

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "AndroidJob-" + this.f6557.incrementAndGet());
            if (thread.isDaemon()) {
                thread.setDaemon(false);
            }
            if (thread.getPriority() != 5) {
                thread.setPriority(5);
            }
            return thread;
        }
    });
    private static final long DEFAULT_JOB_RESCHEDULE_PAUSE = 3000;
    private static final EnumMap<JobApi, Boolean> ENABLED_APIS = new EnumMap<>(JobApi.class);
    private static volatile boolean allowSmallerIntervals;
    private static volatile Clock clock = Clock.DEFAULT;
    private static volatile boolean closeDatabase = false;
    private static volatile ExecutorService executorService = DEFAULT_EXECUTOR_SERVICE;
    private static volatile boolean forceAllowApi14 = false;
    private static volatile boolean forceRtc = false;
    private static volatile int jobIdOffset = 0;
    private static volatile long jobReschedulePause = DEFAULT_JOB_RESCHEDULE_PAUSE;
    private static volatile boolean skipJobReschedule = false;

    static {
        for (JobApi api : JobApi.values()) {
            ENABLED_APIS.put(api, Boolean.TRUE);
        }
    }

    private JobConfig() {
        throw new UnsupportedOperationException();
    }

    public static synchronized boolean addLogger(JobLogger logger) {
        boolean addLogger;
        synchronized (JobConfig.class) {
            addLogger = JobCat.addLogger(logger);
        }
        return addLogger;
    }

    public static void forceApi(JobApi api) {
        JobApi[] values = JobApi.values();
        int length = values.length;
        for (int i = 0; i < length; i++) {
            JobApi jobApi = values[i];
            ENABLED_APIS.put(jobApi, Boolean.valueOf(jobApi == api));
        }
        CAT.w("forceApi - %s", api);
    }

    public static Clock getClock() {
        return clock;
    }

    public static ExecutorService getExecutorService() {
        return executorService;
    }

    public static int getJobIdOffset() {
        return jobIdOffset;
    }

    public static long getJobReschedulePause() {
        return jobReschedulePause;
    }

    public static boolean isAllowSmallerIntervalsForMarshmallow() {
        return allowSmallerIntervals && Build.VERSION.SDK_INT < 24;
    }

    public static boolean isApiEnabled(JobApi api) {
        return ENABLED_APIS.get(api).booleanValue();
    }

    public static boolean isCloseDatabase() {
        return closeDatabase;
    }

    public static boolean isForceAllowApi14() {
        return forceAllowApi14;
    }

    public static boolean isForceRtc() {
        return forceRtc;
    }

    public static boolean isLogcatEnabled() {
        return JobCat.isLogcatEnabled();
    }

    static boolean isSkipJobReschedule() {
        return skipJobReschedule;
    }

    public static synchronized void removeLogger(JobLogger logger) {
        synchronized (JobConfig.class) {
            JobCat.removeLogger(logger);
        }
    }

    public static void reset() {
        for (JobApi api : JobApi.values()) {
            ENABLED_APIS.put(api, Boolean.TRUE);
        }
        allowSmallerIntervals = false;
        forceAllowApi14 = false;
        jobReschedulePause = DEFAULT_JOB_RESCHEDULE_PAUSE;
        skipJobReschedule = false;
        jobIdOffset = 0;
        forceRtc = false;
        clock = Clock.DEFAULT;
        executorService = DEFAULT_EXECUTOR_SERVICE;
        closeDatabase = false;
        JobCat.setLogcatEnabled(true);
        JobCat.clearLogger();
    }

    public static void setAllowSmallerIntervalsForMarshmallow(boolean allowSmallerIntervals2) {
        if (!allowSmallerIntervals2 || Build.VERSION.SDK_INT < 24) {
            allowSmallerIntervals = allowSmallerIntervals2;
            return;
        }
        throw new IllegalStateException("This method is only allowed to call on Android M or earlier");
    }

    public static void setApiEnabled(JobApi api, boolean enabled) {
        ENABLED_APIS.put(api, Boolean.valueOf(enabled));
        CAT.w("setApiEnabled - %s, %b", api, Boolean.valueOf(enabled));
    }

    static void setClock(Clock clock2) {
        clock = clock2;
    }

    public static void setCloseDatabase(boolean closeDatabase2) {
        closeDatabase = closeDatabase2;
    }

    public static void setExecutorService(ExecutorService executorService2) {
        executorService = (ExecutorService) JobPreconditions.checkNotNull(executorService2);
    }

    public static void setForceAllowApi14(boolean forceAllowApi142) {
        forceAllowApi14 = forceAllowApi142;
    }

    public static void setForceRtc(boolean forceRtc2) {
        forceRtc = forceRtc2;
    }

    public static void setJobIdOffset(int jobIdOffset2) {
        JobPreconditions.checkArgumentNonnegative(jobIdOffset2, "offset can't be negative");
        if (jobIdOffset2 > 2147479500) {
            throw new IllegalArgumentException("offset is too close to Integer.MAX_VALUE");
        }
        jobIdOffset = jobIdOffset2;
    }

    public static void setJobReschedulePause(long pause, TimeUnit timeUnit) {
        jobReschedulePause = timeUnit.toMillis(pause);
    }

    public static void setLogcatEnabled(boolean enabled) {
        JobCat.setLogcatEnabled(enabled);
    }

    static void setSkipJobReschedule(boolean skipJobReschedule2) {
        skipJobReschedule = skipJobReschedule2;
    }
}
