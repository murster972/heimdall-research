package com.evernote.android.job.gcm;

import android.app.Service;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobManagerCreateException;
import com.evernote.android.job.JobProxy;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.JobCat;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

public class PlatformGcmService extends GcmTaskService {
    private static final JobCat CAT = new JobCat("PlatformGcmService");

    public void onInitializeTasks() {
        super.onInitializeTasks();
        try {
            JobManager.create(getApplicationContext());
        } catch (JobManagerCreateException e) {
        }
    }

    public int onRunTask(TaskParams taskParams) {
        JobProxy.Common common = new JobProxy.Common((Service) this, CAT, Integer.parseInt(taskParams.m9413()));
        JobRequest pendingRequest = common.getPendingRequest(true, true);
        if (pendingRequest == null) {
            return 2;
        }
        return Job.Result.SUCCESS.equals(common.executeJobRequest(pendingRequest, taskParams.m9412())) ? 0 : 2;
    }
}
