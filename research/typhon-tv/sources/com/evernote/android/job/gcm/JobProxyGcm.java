package com.evernote.android.job.gcm;

import android.content.Context;
import com.evernote.android.job.JobProxy;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobUtil;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;

public class JobProxyGcm implements JobProxy {
    private static final JobCat CAT = new JobCat("JobProxyGcm");
    private final Context mContext;
    private final GcmNetworkManager mGcmNetworkManager;

    public JobProxyGcm(Context context) {
        this.mContext = context;
        this.mGcmNetworkManager = GcmNetworkManager.m9352(context);
    }

    public void cancel(int i) {
        this.mGcmNetworkManager.m9359(createTag(i), (Class<? extends GcmTaskService>) PlatformGcmService.class);
    }

    /* access modifiers changed from: protected */
    public int convertNetworkType(JobRequest.NetworkType networkType) {
        switch (networkType) {
            case ANY:
                return 2;
            case CONNECTED:
                return 0;
            case UNMETERED:
            case NOT_ROAMING:
                return 1;
            default:
                throw new IllegalStateException("not implemented");
        }
    }

    /* access modifiers changed from: protected */
    public String createTag(int i) {
        return String.valueOf(i);
    }

    /* access modifiers changed from: protected */
    public String createTag(JobRequest jobRequest) {
        return createTag(jobRequest.getJobId());
    }

    public boolean isPlatformJobScheduled(JobRequest jobRequest) {
        return true;
    }

    public void plantOneOff(JobRequest jobRequest) {
        long startMs = JobProxy.Common.getStartMs(jobRequest);
        long j = startMs / 1000;
        long endMs = JobProxy.Common.getEndMs(jobRequest);
        this.mGcmNetworkManager.m9358((Task) ((OneoffTask.Builder) prepareBuilder(new OneoffTask.Builder(), jobRequest)).m9378(j, Math.max(endMs / 1000, 1 + j)).m9370());
        CAT.d("Scheduled OneoffTask, %s, start %s, end %s (from now), reschedule count %d", jobRequest, JobUtil.timeToString(startMs), JobUtil.timeToString(endMs), Integer.valueOf(JobProxy.Common.getRescheduleCount(jobRequest)));
    }

    public void plantPeriodic(JobRequest jobRequest) {
        this.mGcmNetworkManager.m9358((Task) ((PeriodicTask.Builder) prepareBuilder(new PeriodicTask.Builder(), jobRequest)).m9398(jobRequest.getIntervalMs() / 1000).m9388(jobRequest.getFlexMs() / 1000).m9390());
        CAT.d("Scheduled PeriodicTask, %s, interval %s, flex %s", jobRequest, JobUtil.timeToString(jobRequest.getIntervalMs()), JobUtil.timeToString(jobRequest.getFlexMs()));
    }

    public void plantPeriodicFlexSupport(JobRequest jobRequest) {
        CAT.w("plantPeriodicFlexSupport called although flex is supported");
        long startMsSupportFlex = JobProxy.Common.getStartMsSupportFlex(jobRequest);
        long endMsSupportFlex = JobProxy.Common.getEndMsSupportFlex(jobRequest);
        this.mGcmNetworkManager.m9358((Task) ((OneoffTask.Builder) prepareBuilder(new OneoffTask.Builder(), jobRequest)).m9378(startMsSupportFlex / 1000, endMsSupportFlex / 1000).m9370());
        CAT.d("Scheduled periodic (flex support), %s, start %s, end %s, flex %s", jobRequest, JobUtil.timeToString(startMsSupportFlex), JobUtil.timeToString(endMsSupportFlex), JobUtil.timeToString(jobRequest.getFlexMs()));
    }

    /* access modifiers changed from: protected */
    public <T extends Task.Builder> T prepareBuilder(T t, JobRequest jobRequest) {
        t.m9409(createTag(jobRequest)).m9408((Class<? extends GcmTaskService>) PlatformGcmService.class).m9405(true).m9406(convertNetworkType(jobRequest.requiredNetworkType())).m9410(JobUtil.hasBootPermission(this.mContext)).m9404(jobRequest.requiresCharging()).m9407(jobRequest.getTransientExtras());
        return t;
    }
}
