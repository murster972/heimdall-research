package com.evernote.android.job;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import android.util.SparseArray;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;
import com.evernote.android.job.JobProxy;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobPreconditions;
import com.evernote.android.job.util.JobUtil;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class JobManager {
    private static final JobCat CAT = new JobCat("JobManager");
    @SuppressLint({"StaticFieldLeak"})
    private static volatile JobManager instance;
    private final Context mContext;
    private final JobCreatorHolder mJobCreatorHolder = new JobCreatorHolder();
    private final JobExecutor mJobExecutor;
    private final JobStorage mJobStorage;

    private JobManager(Context context) {
        this.mContext = context;
        this.mJobStorage = new JobStorage(context);
        this.mJobExecutor = new JobExecutor();
        if (!JobConfig.isSkipJobReschedule()) {
            JobRescheduleService.startService(this.mContext);
        }
    }

    private synchronized int cancelAllInner(String tag) {
        int canceled;
        canceled = 0;
        for (JobRequest request : getAllJobRequests(tag, true, false)) {
            if (cancelInner(request)) {
                canceled++;
            }
        }
        for (Job job : TextUtils.isEmpty(tag) ? getAllJobs() : getAllJobsForTag(tag)) {
            if (cancelInner(job)) {
                canceled++;
            }
        }
        return canceled;
    }

    private boolean cancelInner(Job job) {
        if (job == null || !job.cancel(true)) {
            return false;
        }
        CAT.i("Cancel running %s", job);
        return true;
    }

    private boolean cancelInner(JobRequest request) {
        if (request == null) {
            return false;
        }
        CAT.i("Found pending job %s, canceling", request);
        getJobProxy(request.getJobApi()).cancel(request.getJobId());
        getJobStorage().m7535(request);
        request.setScheduledAt(0);
        return true;
    }

    public static JobManager create(Context context) throws JobManagerCreateException {
        if (instance == null) {
            synchronized (JobManager.class) {
                if (instance == null) {
                    JobPreconditions.checkNotNull(context, "Context cannot be null");
                    if (context.getApplicationContext() != null) {
                        context = context.getApplicationContext();
                    }
                    JobApi api = JobApi.getDefault(context);
                    if (api != JobApi.V_14 || api.isSupported(context)) {
                        instance = new JobManager(context);
                        if (!JobUtil.hasWakeLockPermission(context)) {
                            CAT.w("No wake lock permission");
                        }
                        if (!JobUtil.hasBootPermission(context)) {
                            CAT.w("No boot permission");
                        }
                        sendAddJobCreatorIntent(context);
                    } else {
                        throw new JobManagerCreateException("All APIs are disabled, cannot schedule any job");
                    }
                }
            }
        }
        return instance;
    }

    public static JobManager instance() {
        if (instance == null) {
            synchronized (JobManager.class) {
                if (instance == null) {
                    throw new IllegalStateException("You need to call create() at least once to create the singleton");
                }
            }
        }
        return instance;
    }

    private void scheduleWithApi(JobRequest request, JobApi jobApi, boolean periodic, boolean flexSupport) {
        JobProxy proxy = getJobProxy(jobApi);
        if (!periodic) {
            proxy.plantOneOff(request);
        } else if (flexSupport) {
            proxy.plantPeriodicFlexSupport(request);
        } else {
            proxy.plantPeriodic(request);
        }
    }

    private static void sendAddJobCreatorIntent(Context context) {
        List<ResolveInfo> resolveInfos;
        String myPackage = context.getPackageName();
        Intent intent = new Intent(JobCreator.ACTION_ADD_JOB_CREATOR);
        intent.setPackage(myPackage);
        try {
            resolveInfos = context.getPackageManager().queryBroadcastReceivers(intent, 0);
        } catch (Exception e) {
            resolveInfos = Collections.emptyList();
        }
        for (ResolveInfo resolveInfo : resolveInfos) {
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            if (activityInfo != null && !activityInfo.exported && myPackage.equals(activityInfo.packageName) && !TextUtils.isEmpty(activityInfo.name)) {
                try {
                    ((JobCreator.AddJobCreatorReceiver) Class.forName(activityInfo.name).newInstance()).addJobCreator(context, instance);
                } catch (Exception e2) {
                }
            }
        }
    }

    public void addJobCreator(JobCreator jobCreator) {
        this.mJobCreatorHolder.m7505(jobCreator);
    }

    public int cancelAll() {
        return cancelAllInner((String) null);
    }

    public int cancelAllForTag(String tag) {
        return cancelAllInner(tag);
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        synchronized (JobManager.class) {
            instance = null;
            for (JobApi api : JobApi.values()) {
                api.invalidateCachedProxy();
            }
        }
    }

    public Set<JobRequest> getAllJobRequests() {
        return getAllJobRequests((String) null, false, true);
    }

    /* access modifiers changed from: package-private */
    public Set<JobRequest> getAllJobRequests(String tag, boolean includeStarted, boolean cleanUpTransient) {
        Set<JobRequest> requests = this.mJobStorage.m7539(tag, includeStarted);
        if (cleanUpTransient) {
            Iterator<JobRequest> iterator = requests.iterator();
            while (iterator.hasNext()) {
                JobRequest request = iterator.next();
                if (request.isTransient() && !request.getJobApi().getProxy(this.mContext).isPlatformJobScheduled(request)) {
                    this.mJobStorage.m7535(request);
                    iterator.remove();
                }
            }
        }
        return requests;
    }

    public Set<JobRequest> getAllJobRequestsForTag(String tag) {
        return getAllJobRequests(tag, false, true);
    }

    public SparseArray<Job.Result> getAllJobResults() {
        return this.mJobExecutor.m7509();
    }

    public Set<Job> getAllJobs() {
        return this.mJobExecutor.m7512();
    }

    public Set<Job> getAllJobsForTag(String tag) {
        return this.mJobExecutor.m7513(tag);
    }

    /* access modifiers changed from: package-private */
    public Context getContext() {
        return this.mContext;
    }

    public Job getJob(int jobId) {
        return this.mJobExecutor.m7511(jobId);
    }

    /* access modifiers changed from: package-private */
    public JobCreatorHolder getJobCreatorHolder() {
        return this.mJobCreatorHolder;
    }

    /* access modifiers changed from: package-private */
    public JobExecutor getJobExecutor() {
        return this.mJobExecutor;
    }

    /* access modifiers changed from: package-private */
    public JobProxy getJobProxy(JobApi api) {
        return api.getProxy(this.mContext);
    }

    public JobRequest getJobRequest(int jobId) {
        JobRequest request = getJobRequest(jobId, false);
        if (request == null || !request.isTransient() || request.getJobApi().getProxy(this.mContext).isPlatformJobScheduled(request)) {
            return request;
        }
        getJobStorage().m7535(request);
        return null;
    }

    /* access modifiers changed from: package-private */
    public JobRequest getJobRequest(int jobId, boolean includeStarted) {
        JobRequest jobRequest = this.mJobStorage.m7538(jobId);
        if (includeStarted || jobRequest == null || !jobRequest.isStarted()) {
            return jobRequest;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public JobStorage getJobStorage() {
        return this.mJobStorage;
    }

    public void removeJobCreator(JobCreator jobCreator) {
        this.mJobCreatorHolder.m7503(jobCreator);
    }

    public synchronized void schedule(JobRequest request) {
        if (this.mJobCreatorHolder.m7506()) {
            CAT.w("you haven't registered a JobCreator with addJobCreator(), it's likely that your job never will be executed");
        }
        if (request.getScheduledAt() <= 0) {
            if (request.isUpdateCurrent()) {
                cancelAllForTag(request.getTag());
            }
            JobProxy.Common.cleanUpOrphanedJob(this.mContext, request.getJobId());
            JobApi jobApi = request.getJobApi();
            boolean periodic = request.isPeriodic();
            boolean flexSupport = periodic && jobApi.isFlexSupport() && request.getFlexMs() < request.getIntervalMs();
            request.setScheduledAt(JobConfig.getClock().currentTimeMillis());
            request.setFlexSupport(flexSupport);
            this.mJobStorage.m7540(request);
            try {
                scheduleWithApi(request, jobApi, periodic, flexSupport);
            } catch (Exception e) {
                this.mJobStorage.m7535(request);
                throw e;
            } catch (JobProxyIllegalStateException e2) {
                jobApi.invalidateCachedProxy();
                scheduleWithApi(request, jobApi, periodic, flexSupport);
            } catch (Exception e3) {
                this.mJobStorage.m7535(request);
                throw e3;
            } catch (Exception e4) {
                if (jobApi == JobApi.V_14 || jobApi == JobApi.V_19) {
                    this.mJobStorage.m7535(request);
                    throw e4;
                }
                scheduleWithApi(request, JobApi.V_19.isSupported(this.mContext) ? JobApi.V_19 : JobApi.V_14, periodic, flexSupport);
            }
        }
    }

    public boolean cancel(int jobId) {
        boolean result = cancelInner(getJobRequest(jobId, true)) | cancelInner(getJob(jobId));
        JobProxy.Common.cleanUpOrphanedJob(this.mContext, jobId);
        return result;
    }
}
