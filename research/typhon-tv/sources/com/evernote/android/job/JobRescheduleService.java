package com.evernote.android.job;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.JobIntentService;
import com.evernote.android.job.util.JobCat;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

public final class JobRescheduleService extends JobIntentService {
    private static final JobCat CAT = new JobCat("JobRescheduleService", false);
    static CountDownLatch latch;

    static void startService(Context context) {
        try {
            enqueueWork(context, JobRescheduleService.class, 2147480000, new Intent());
            latch = new CountDownLatch(1);
        } catch (Exception e) {
            CAT.e((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    public void onHandleWork(Intent intent) {
        try {
            CAT.d("Reschedule service started");
            SystemClock.sleep(JobConfig.getJobReschedulePause());
            try {
                JobManager create = JobManager.create(this);
                Set<JobRequest> allJobRequests = create.getAllJobRequests((String) null, true, true);
                int rescheduleJobs = rescheduleJobs(create, allJobRequests);
                CAT.d("Reschedule %d jobs of %d jobs", Integer.valueOf(rescheduleJobs), Integer.valueOf(allJobRequests.size()));
            } catch (JobManagerCreateException e) {
                if (latch != null) {
                    latch.countDown();
                }
            }
        } finally {
            if (latch != null) {
                latch.countDown();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int rescheduleJobs(JobManager jobManager) {
        return rescheduleJobs(jobManager, jobManager.getAllJobRequests((String) null, true, true));
    }

    /* access modifiers changed from: package-private */
    public int rescheduleJobs(JobManager jobManager, Collection<JobRequest> collection) {
        int i = 0;
        boolean z = false;
        for (JobRequest next : collection) {
            if (next.isStarted() ? jobManager.getJob(next.getJobId()) == null : !jobManager.getJobProxy(next.getJobApi()).isPlatformJobScheduled(next)) {
                try {
                    next.cancelAndEdit().build().schedule();
                } catch (Exception e) {
                    if (!z) {
                        CAT.e((Throwable) e);
                        z = true;
                    }
                }
                i++;
            }
        }
        return i;
    }
}
