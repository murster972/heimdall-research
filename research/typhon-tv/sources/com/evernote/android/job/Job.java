package com.evernote.android.job;

import android.content.Context;
import android.os.Bundle;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.Device;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import java.lang.ref.WeakReference;

public abstract class Job {
    private static final JobCat CAT = new JobCat("Job");
    private Context mApplicationContext;
    private volatile boolean mCanceled;
    private WeakReference<Context> mContextReference;
    private volatile boolean mDeleted;
    private volatile long mFinishedTimeStamp = -1;
    private Params mParams;
    private Result mResult = Result.FAILURE;

    public static final class Params {
        private PersistableBundleCompat mExtras;
        private final JobRequest mRequest;
        private Bundle mTransientExtras;

        private Params(JobRequest jobRequest, Bundle bundle) {
            this.mRequest = jobRequest;
            this.mTransientExtras = bundle;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.mRequest.equals(((Params) obj).mRequest);
        }

        public long getBackoffMs() {
            return this.mRequest.getBackoffMs();
        }

        public JobRequest.BackoffPolicy getBackoffPolicy() {
            return this.mRequest.getBackoffPolicy();
        }

        public long getEndMs() {
            return this.mRequest.getEndMs();
        }

        public PersistableBundleCompat getExtras() {
            if (this.mExtras == null) {
                this.mExtras = this.mRequest.getExtras();
                if (this.mExtras == null) {
                    this.mExtras = new PersistableBundleCompat();
                }
            }
            return this.mExtras;
        }

        public int getFailureCount() {
            return this.mRequest.getFailureCount();
        }

        public long getFlexMs() {
            return this.mRequest.getFlexMs();
        }

        public int getId() {
            return this.mRequest.getJobId();
        }

        public long getIntervalMs() {
            return this.mRequest.getIntervalMs();
        }

        public long getLastRun() {
            return this.mRequest.getLastRun();
        }

        /* access modifiers changed from: package-private */
        public JobRequest getRequest() {
            return this.mRequest;
        }

        public long getScheduledAt() {
            return this.mRequest.getScheduledAt();
        }

        public long getStartMs() {
            return this.mRequest.getStartMs();
        }

        public String getTag() {
            return this.mRequest.getTag();
        }

        public Bundle getTransientExtras() {
            return this.mTransientExtras;
        }

        public int hashCode() {
            return this.mRequest.hashCode();
        }

        public boolean isExact() {
            return this.mRequest.isExact();
        }

        public boolean isPeriodic() {
            return this.mRequest.isPeriodic();
        }

        public boolean isTransient() {
            return this.mRequest.isTransient();
        }

        public JobRequest.NetworkType requiredNetworkType() {
            return this.mRequest.requiredNetworkType();
        }

        public boolean requirementsEnforced() {
            return this.mRequest.requirementsEnforced();
        }

        public boolean requiresBatteryNotLow() {
            return this.mRequest.requiresBatteryNotLow();
        }

        public boolean requiresCharging() {
            return this.mRequest.requiresCharging();
        }

        public boolean requiresDeviceIdle() {
            return this.mRequest.requiresDeviceIdle();
        }

        public boolean requiresStorageNotLow() {
            return this.mRequest.requiresStorageNotLow();
        }
    }

    public enum Result {
        SUCCESS,
        FAILURE,
        RESCHEDULE
    }

    public final void cancel() {
        cancel(false);
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean cancel(boolean z) {
        boolean z2 = true;
        synchronized (this) {
            if (!isFinished()) {
                if (!this.mCanceled) {
                    this.mCanceled = true;
                    onCancel();
                }
                this.mDeleted |= z;
            } else {
                z2 = false;
            }
        }
        return z2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.mParams.equals(((Job) obj).mParams);
    }

    /* access modifiers changed from: protected */
    public final Context getContext() {
        Context context = (Context) this.mContextReference.get();
        return context == null ? this.mApplicationContext : context;
    }

    /* access modifiers changed from: package-private */
    public final synchronized long getFinishedTimeStamp() {
        return this.mFinishedTimeStamp;
    }

    /* access modifiers changed from: protected */
    public final Params getParams() {
        return this.mParams;
    }

    /* access modifiers changed from: package-private */
    public final Result getResult() {
        return this.mResult;
    }

    public int hashCode() {
        return this.mParams.hashCode();
    }

    /* access modifiers changed from: protected */
    public final synchronized boolean isCanceled() {
        return this.mCanceled;
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean isDeleted() {
        return this.mDeleted;
    }

    public final synchronized boolean isFinished() {
        return this.mFinishedTimeStamp > 0;
    }

    /* access modifiers changed from: protected */
    public boolean isRequirementBatteryNotLowMet() {
        return !getParams().getRequest().requiresBatteryNotLow() || !Device.getBatteryStatus(getContext()).isBatteryLow();
    }

    /* access modifiers changed from: protected */
    public boolean isRequirementChargingMet() {
        return !getParams().getRequest().requiresCharging() || Device.getBatteryStatus(getContext()).isCharging();
    }

    /* access modifiers changed from: protected */
    public boolean isRequirementDeviceIdleMet() {
        return !getParams().getRequest().requiresDeviceIdle() || Device.isIdle(getContext());
    }

    /* access modifiers changed from: protected */
    public boolean isRequirementNetworkTypeMet() {
        boolean z = false;
        JobRequest.NetworkType requiredNetworkType = getParams().getRequest().requiredNetworkType();
        if (requiredNetworkType == JobRequest.NetworkType.ANY) {
            return true;
        }
        JobRequest.NetworkType networkType = Device.getNetworkType(getContext());
        switch (requiredNetworkType) {
            case CONNECTED:
                return networkType != JobRequest.NetworkType.ANY;
            case NOT_ROAMING:
                if (networkType == JobRequest.NetworkType.NOT_ROAMING || networkType == JobRequest.NetworkType.UNMETERED || networkType == JobRequest.NetworkType.METERED) {
                    z = true;
                }
                return z;
            case UNMETERED:
                return networkType == JobRequest.NetworkType.UNMETERED;
            case METERED:
                if (networkType == JobRequest.NetworkType.CONNECTED || networkType == JobRequest.NetworkType.NOT_ROAMING) {
                    z = true;
                }
                return z;
            default:
                throw new IllegalStateException("not implemented");
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRequirementStorageNotLowMet() {
        return !getParams().getRequest().requiresStorageNotLow() || !Device.isStorageLow();
    }

    /* access modifiers changed from: protected */
    public boolean meetsRequirements() {
        return meetsRequirements(false);
    }

    /* access modifiers changed from: package-private */
    public boolean meetsRequirements(boolean z) {
        if (z && !getParams().getRequest().requirementsEnforced()) {
            return true;
        }
        if (!isRequirementChargingMet()) {
            CAT.w("Job requires charging, reschedule");
            return false;
        } else if (!isRequirementDeviceIdleMet()) {
            CAT.w("Job requires device to be idle, reschedule");
            return false;
        } else if (!isRequirementNetworkTypeMet()) {
            CAT.w("Job requires network to be %s, but was %s", getParams().getRequest().requiredNetworkType(), Device.getNetworkType(getContext()));
            return false;
        } else if (!isRequirementBatteryNotLowMet()) {
            CAT.w("Job requires battery not be low, reschedule");
            return false;
        } else if (isRequirementStorageNotLowMet()) {
            return true;
        } else {
            CAT.w("Job requires storage not be low, reschedule");
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancel() {
    }

    /* access modifiers changed from: protected */
    public void onReschedule(int i) {
    }

    /* access modifiers changed from: protected */
    public abstract Result onRunJob(Params params);

    /* access modifiers changed from: package-private */
    public final Result runJob() {
        try {
            if ((this instanceof DailyJob) || meetsRequirements(true)) {
                this.mResult = onRunJob(getParams());
            } else {
                this.mResult = getParams().isPeriodic() ? Result.FAILURE : Result.RESCHEDULE;
            }
            return this.mResult;
        } finally {
            this.mFinishedTimeStamp = System.currentTimeMillis();
        }
    }

    /* access modifiers changed from: package-private */
    public final Job setContext(Context context) {
        this.mContextReference = new WeakReference<>(context);
        this.mApplicationContext = context.getApplicationContext();
        return this;
    }

    /* access modifiers changed from: package-private */
    public final Job setRequest(JobRequest jobRequest, Bundle bundle) {
        this.mParams = new Params(jobRequest, bundle);
        return this;
    }

    public String toString() {
        return "job{id=" + this.mParams.getId() + ", finished=" + isFinished() + ", result=" + this.mResult + ", canceled=" + this.mCanceled + ", periodic=" + this.mParams.isPeriodic() + ", class=" + getClass().getSimpleName() + ", tag=" + this.mParams.getTag() + '}';
    }
}
