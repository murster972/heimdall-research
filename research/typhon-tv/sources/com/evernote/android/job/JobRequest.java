package com.evernote.android.job;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobPreconditions;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import java.util.concurrent.TimeUnit;

public final class JobRequest {
    /* access modifiers changed from: private */
    public static final JobCat CAT = new JobCat("JobRequest");
    public static final long DEFAULT_BACKOFF_MS = 30000;
    public static final BackoffPolicy DEFAULT_BACKOFF_POLICY = BackoffPolicy.EXPONENTIAL;
    public static final JobScheduledCallback DEFAULT_JOB_SCHEDULED_CALLBACK = new JobScheduledCallback() {
        public void onJobScheduled(int i, String str, Exception exc) {
            if (exc != null) {
                JobRequest.CAT.e(exc, "The job with tag %s couldn't be scheduled", str);
            }
        }
    };
    public static final NetworkType DEFAULT_NETWORK_TYPE = NetworkType.ANY;
    public static final long MIN_FLEX = TimeUnit.MINUTES.toMillis(5);
    public static final long MIN_INTERVAL = TimeUnit.MINUTES.toMillis(15);
    static final long START_NOW = 1;
    private static final long WINDOW_THRESHOLD_MAX = 6148914691236517204L;
    private static final long WINDOW_THRESHOLD_WARNING = 3074457345618258602L;
    private final Builder mBuilder;
    private int mFailureCount;
    private boolean mFlexSupport;
    private long mLastRun;
    private long mScheduledAt;
    private boolean mStarted;

    public enum BackoffPolicy {
        LINEAR,
        EXPONENTIAL
    }

    public static final class Builder {
        private static final int CREATE_ID = -8765;
        /* access modifiers changed from: private */
        public long mBackoffMs;
        /* access modifiers changed from: private */
        public BackoffPolicy mBackoffPolicy;
        /* access modifiers changed from: private */
        public long mEndMs;
        /* access modifiers changed from: private */
        public boolean mExact;
        /* access modifiers changed from: private */
        public PersistableBundleCompat mExtras;
        /* access modifiers changed from: private */
        public String mExtrasXml;
        /* access modifiers changed from: private */
        public long mFlexMs;
        /* access modifiers changed from: private */
        public int mId;
        /* access modifiers changed from: private */
        public long mIntervalMs;
        /* access modifiers changed from: private */
        public NetworkType mNetworkType;
        /* access modifiers changed from: private */
        public boolean mRequirementsEnforced;
        /* access modifiers changed from: private */
        public boolean mRequiresBatteryNotLow;
        /* access modifiers changed from: private */
        public boolean mRequiresCharging;
        /* access modifiers changed from: private */
        public boolean mRequiresDeviceIdle;
        /* access modifiers changed from: private */
        public boolean mRequiresStorageNotLow;
        /* access modifiers changed from: private */
        public long mStartMs;
        final String mTag;
        /* access modifiers changed from: private */
        public boolean mTransient;
        /* access modifiers changed from: private */
        public Bundle mTransientExtras;
        /* access modifiers changed from: private */
        public boolean mUpdateCurrent;

        private Builder(Cursor cursor) {
            boolean z = true;
            this.mTransientExtras = Bundle.EMPTY;
            this.mId = cursor.getInt(cursor.getColumnIndex("_id"));
            this.mTag = cursor.getString(cursor.getColumnIndex("tag"));
            this.mStartMs = cursor.getLong(cursor.getColumnIndex("startMs"));
            this.mEndMs = cursor.getLong(cursor.getColumnIndex("endMs"));
            this.mBackoffMs = cursor.getLong(cursor.getColumnIndex("backoffMs"));
            try {
                this.mBackoffPolicy = BackoffPolicy.valueOf(cursor.getString(cursor.getColumnIndex("backoffPolicy")));
            } catch (Throwable th) {
                JobRequest.CAT.e(th);
                this.mBackoffPolicy = JobRequest.DEFAULT_BACKOFF_POLICY;
            }
            this.mIntervalMs = cursor.getLong(cursor.getColumnIndex("intervalMs"));
            this.mFlexMs = cursor.getLong(cursor.getColumnIndex("flexMs"));
            this.mRequirementsEnforced = cursor.getInt(cursor.getColumnIndex("requirementsEnforced")) > 0;
            this.mRequiresCharging = cursor.getInt(cursor.getColumnIndex("requiresCharging")) > 0;
            this.mRequiresDeviceIdle = cursor.getInt(cursor.getColumnIndex("requiresDeviceIdle")) > 0;
            this.mRequiresBatteryNotLow = cursor.getInt(cursor.getColumnIndex("requiresBatteryNotLow")) > 0;
            this.mRequiresStorageNotLow = cursor.getInt(cursor.getColumnIndex("requiresStorageNotLow")) > 0;
            this.mExact = cursor.getInt(cursor.getColumnIndex("exact")) > 0;
            try {
                this.mNetworkType = NetworkType.valueOf(cursor.getString(cursor.getColumnIndex("networkType")));
            } catch (Throwable th2) {
                JobRequest.CAT.e(th2);
                this.mNetworkType = JobRequest.DEFAULT_NETWORK_TYPE;
            }
            this.mExtrasXml = cursor.getString(cursor.getColumnIndex("extras"));
            this.mTransient = cursor.getInt(cursor.getColumnIndex("transient")) <= 0 ? false : z;
        }

        private Builder(Builder builder) {
            this(builder, false);
        }

        private Builder(Builder builder, boolean z) {
            this.mTransientExtras = Bundle.EMPTY;
            this.mId = z ? CREATE_ID : builder.mId;
            this.mTag = builder.mTag;
            this.mStartMs = builder.mStartMs;
            this.mEndMs = builder.mEndMs;
            this.mBackoffMs = builder.mBackoffMs;
            this.mBackoffPolicy = builder.mBackoffPolicy;
            this.mIntervalMs = builder.mIntervalMs;
            this.mFlexMs = builder.mFlexMs;
            this.mRequirementsEnforced = builder.mRequirementsEnforced;
            this.mRequiresCharging = builder.mRequiresCharging;
            this.mRequiresDeviceIdle = builder.mRequiresDeviceIdle;
            this.mRequiresBatteryNotLow = builder.mRequiresBatteryNotLow;
            this.mRequiresStorageNotLow = builder.mRequiresStorageNotLow;
            this.mExact = builder.mExact;
            this.mNetworkType = builder.mNetworkType;
            this.mExtras = builder.mExtras;
            this.mExtrasXml = builder.mExtrasXml;
            this.mUpdateCurrent = builder.mUpdateCurrent;
            this.mTransient = builder.mTransient;
            this.mTransientExtras = builder.mTransientExtras;
        }

        public Builder(String str) {
            this.mTransientExtras = Bundle.EMPTY;
            this.mTag = (String) JobPreconditions.checkNotEmpty(str);
            this.mId = CREATE_ID;
            this.mStartMs = -1;
            this.mEndMs = -1;
            this.mBackoffMs = 30000;
            this.mBackoffPolicy = JobRequest.DEFAULT_BACKOFF_POLICY;
            this.mNetworkType = JobRequest.DEFAULT_NETWORK_TYPE;
        }

        /* access modifiers changed from: private */
        public void fillContentValues(ContentValues contentValues) {
            contentValues.put("_id", Integer.valueOf(this.mId));
            contentValues.put("tag", this.mTag);
            contentValues.put("startMs", Long.valueOf(this.mStartMs));
            contentValues.put("endMs", Long.valueOf(this.mEndMs));
            contentValues.put("backoffMs", Long.valueOf(this.mBackoffMs));
            contentValues.put("backoffPolicy", this.mBackoffPolicy.toString());
            contentValues.put("intervalMs", Long.valueOf(this.mIntervalMs));
            contentValues.put("flexMs", Long.valueOf(this.mFlexMs));
            contentValues.put("requirementsEnforced", Boolean.valueOf(this.mRequirementsEnforced));
            contentValues.put("requiresCharging", Boolean.valueOf(this.mRequiresCharging));
            contentValues.put("requiresDeviceIdle", Boolean.valueOf(this.mRequiresDeviceIdle));
            contentValues.put("requiresBatteryNotLow", Boolean.valueOf(this.mRequiresBatteryNotLow));
            contentValues.put("requiresStorageNotLow", Boolean.valueOf(this.mRequiresStorageNotLow));
            contentValues.put("exact", Boolean.valueOf(this.mExact));
            contentValues.put("networkType", this.mNetworkType.toString());
            if (this.mExtras != null) {
                contentValues.put("extras", this.mExtras.saveToXml());
            } else if (!TextUtils.isEmpty(this.mExtrasXml)) {
                contentValues.put("extras", this.mExtrasXml);
            }
            contentValues.put("transient", Boolean.valueOf(this.mTransient));
        }

        public Builder addExtras(PersistableBundleCompat persistableBundleCompat) {
            if (this.mExtras == null) {
                this.mExtras = persistableBundleCompat;
            } else {
                this.mExtras.putAll(persistableBundleCompat);
            }
            this.mExtrasXml = null;
            return this;
        }

        public JobRequest build() {
            JobPreconditions.checkNotEmpty(this.mTag);
            JobPreconditions.checkArgumentPositive(this.mBackoffMs, "backoffMs must be > 0");
            JobPreconditions.checkNotNull(this.mBackoffPolicy);
            JobPreconditions.checkNotNull(this.mNetworkType);
            if (this.mIntervalMs > 0) {
                JobPreconditions.checkArgumentInRange(this.mIntervalMs, JobRequest.getMinInterval(), Long.MAX_VALUE, "intervalMs");
                JobPreconditions.checkArgumentInRange(this.mFlexMs, JobRequest.getMinFlex(), this.mIntervalMs, "flexMs");
                if (this.mIntervalMs < JobRequest.MIN_INTERVAL || this.mFlexMs < JobRequest.MIN_FLEX) {
                    JobRequest.CAT.w("AllowSmallerIntervals enabled, this will crash on Android N and later, interval %d (minimum is %d), flex %d (minimum is %d)", Long.valueOf(this.mIntervalMs), Long.valueOf(JobRequest.MIN_INTERVAL), Long.valueOf(this.mFlexMs), Long.valueOf(JobRequest.MIN_FLEX));
                }
            }
            if (this.mExact && this.mIntervalMs > 0) {
                throw new IllegalArgumentException("Can't call setExact() on a periodic job.");
            } else if (this.mExact && this.mStartMs != this.mEndMs) {
                throw new IllegalArgumentException("Can't call setExecutionWindow() for an exact job.");
            } else if (this.mExact && (this.mRequirementsEnforced || this.mRequiresDeviceIdle || this.mRequiresCharging || !JobRequest.DEFAULT_NETWORK_TYPE.equals(this.mNetworkType) || this.mRequiresBatteryNotLow || this.mRequiresStorageNotLow)) {
                throw new IllegalArgumentException("Can't require any condition for an exact job.");
            } else if (this.mIntervalMs <= 0 && (this.mStartMs == -1 || this.mEndMs == -1)) {
                throw new IllegalArgumentException("You're trying to build a job with no constraints, this is not allowed.");
            } else if (this.mIntervalMs > 0 && (this.mStartMs != -1 || this.mEndMs != -1)) {
                throw new IllegalArgumentException("Can't call setExecutionWindow() on a periodic job.");
            } else if (this.mIntervalMs <= 0 || (this.mBackoffMs == 30000 && JobRequest.DEFAULT_BACKOFF_POLICY.equals(this.mBackoffPolicy))) {
                if (this.mIntervalMs <= 0 && (this.mStartMs > JobRequest.WINDOW_THRESHOLD_WARNING || this.mEndMs > JobRequest.WINDOW_THRESHOLD_WARNING)) {
                    JobRequest.CAT.w("Attention: your execution window is too large. This could result in undesired behavior, see https://github.com/evernote/android-job/wiki/FAQ");
                }
                if (this.mIntervalMs <= 0 && this.mStartMs > TimeUnit.DAYS.toMillis(365)) {
                    JobRequest.CAT.w("Warning: job with tag %s scheduled over a year in the future", this.mTag);
                }
                if (this.mId != CREATE_ID) {
                    JobPreconditions.checkArgumentNonnegative(this.mId, "id can't be negative");
                }
                Builder builder = new Builder(this);
                if (this.mId == CREATE_ID) {
                    builder.mId = JobManager.instance().getJobStorage().m7537();
                    JobPreconditions.checkArgumentNonnegative(builder.mId, "id can't be negative");
                }
                return new JobRequest(builder);
            } else {
                throw new IllegalArgumentException("A periodic job will not respect any back-off policy, so calling setBackoffCriteria() with setPeriodic() is an error.");
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.mId == ((Builder) obj).mId;
        }

        public int hashCode() {
            return this.mId;
        }

        public Builder setBackoffCriteria(long j, BackoffPolicy backoffPolicy) {
            this.mBackoffMs = JobPreconditions.checkArgumentPositive(j, "backoffMs must be > 0");
            this.mBackoffPolicy = (BackoffPolicy) JobPreconditions.checkNotNull(backoffPolicy);
            return this;
        }

        public Builder setExact(long j) {
            this.mExact = true;
            if (j > JobRequest.WINDOW_THRESHOLD_MAX) {
                JobRequest.CAT.i("exactInMs clamped from %d days to %d days", Long.valueOf(TimeUnit.MILLISECONDS.toDays(j)), Long.valueOf(TimeUnit.MILLISECONDS.toDays(JobRequest.WINDOW_THRESHOLD_MAX)));
                j = JobRequest.WINDOW_THRESHOLD_MAX;
            }
            return setExecutionWindow(j, j);
        }

        public Builder setExecutionWindow(long j, long j2) {
            this.mStartMs = JobPreconditions.checkArgumentPositive(j, "startInMs must be greater than 0");
            this.mEndMs = JobPreconditions.checkArgumentInRange(j2, j, Long.MAX_VALUE, "endInMs");
            if (this.mStartMs > JobRequest.WINDOW_THRESHOLD_MAX) {
                JobRequest.CAT.i("startInMs reduced from %d days to %d days", Long.valueOf(TimeUnit.MILLISECONDS.toDays(this.mStartMs)), Long.valueOf(TimeUnit.MILLISECONDS.toDays(JobRequest.WINDOW_THRESHOLD_MAX)));
                this.mStartMs = JobRequest.WINDOW_THRESHOLD_MAX;
            }
            if (this.mEndMs > JobRequest.WINDOW_THRESHOLD_MAX) {
                JobRequest.CAT.i("endInMs reduced from %d days to %d days", Long.valueOf(TimeUnit.MILLISECONDS.toDays(this.mEndMs)), Long.valueOf(TimeUnit.MILLISECONDS.toDays(JobRequest.WINDOW_THRESHOLD_MAX)));
                this.mEndMs = JobRequest.WINDOW_THRESHOLD_MAX;
            }
            return this;
        }

        public Builder setExtras(PersistableBundleCompat persistableBundleCompat) {
            if (persistableBundleCompat == null) {
                this.mExtras = null;
                this.mExtrasXml = null;
            } else {
                this.mExtras = new PersistableBundleCompat(persistableBundleCompat);
            }
            return this;
        }

        public Builder setPeriodic(long j) {
            return setPeriodic(j, j);
        }

        public Builder setPeriodic(long j, long j2) {
            this.mIntervalMs = JobPreconditions.checkArgumentInRange(j, JobRequest.getMinInterval(), Long.MAX_VALUE, "intervalMs");
            this.mFlexMs = JobPreconditions.checkArgumentInRange(j2, JobRequest.getMinFlex(), this.mIntervalMs, "flexMs");
            return this;
        }

        public Builder setRequiredNetworkType(NetworkType networkType) {
            this.mNetworkType = networkType;
            return this;
        }

        public Builder setRequirementsEnforced(boolean z) {
            this.mRequirementsEnforced = z;
            return this;
        }

        public Builder setRequiresBatteryNotLow(boolean z) {
            this.mRequiresBatteryNotLow = z;
            return this;
        }

        public Builder setRequiresCharging(boolean z) {
            this.mRequiresCharging = z;
            return this;
        }

        public Builder setRequiresDeviceIdle(boolean z) {
            this.mRequiresDeviceIdle = z;
            return this;
        }

        public Builder setRequiresStorageNotLow(boolean z) {
            this.mRequiresStorageNotLow = z;
            return this;
        }

        public Builder setTransientExtras(Bundle bundle) {
            this.mTransient = bundle != null && !bundle.isEmpty();
            this.mTransientExtras = this.mTransient ? new Bundle(bundle) : Bundle.EMPTY;
            return this;
        }

        public Builder setUpdateCurrent(boolean z) {
            this.mUpdateCurrent = z;
            return this;
        }

        public Builder startNow() {
            return setExact(1);
        }
    }

    public interface JobScheduledCallback {
        public static final int JOB_ID_ERROR = -1;

        void onJobScheduled(int i, String str, Exception exc);
    }

    public enum NetworkType {
        ANY,
        CONNECTED,
        UNMETERED,
        NOT_ROAMING,
        METERED
    }

    private JobRequest(Builder builder) {
        this.mBuilder = builder;
    }

    private static Context context() {
        return JobManager.instance().getContext();
    }

    static JobRequest fromCursor(Cursor cursor) {
        boolean z = true;
        JobRequest build = new Builder(cursor).build();
        build.mFailureCount = cursor.getInt(cursor.getColumnIndex("numFailures"));
        build.mScheduledAt = cursor.getLong(cursor.getColumnIndex("scheduledAt"));
        build.mStarted = cursor.getInt(cursor.getColumnIndex("started")) > 0;
        if (cursor.getInt(cursor.getColumnIndex("flexSupport")) <= 0) {
            z = false;
        }
        build.mFlexSupport = z;
        build.mLastRun = cursor.getLong(cursor.getColumnIndex("lastRun"));
        JobPreconditions.checkArgumentNonnegative(build.mFailureCount, "failure count can't be negative");
        JobPreconditions.checkArgumentNonnegative(build.mScheduledAt, "scheduled at can't be negative");
        return build;
    }

    static long getMinFlex() {
        return JobConfig.isAllowSmallerIntervalsForMarshmallow() ? TimeUnit.SECONDS.toMillis(30) : MIN_FLEX;
    }

    static long getMinInterval() {
        return JobConfig.isAllowSmallerIntervalsForMarshmallow() ? TimeUnit.MINUTES.toMillis(1) : MIN_INTERVAL;
    }

    public Builder cancelAndEdit() {
        long j = this.mScheduledAt;
        JobManager.instance().cancel(getJobId());
        Builder builder = new Builder(this.mBuilder);
        this.mStarted = false;
        if (!isPeriodic()) {
            long currentTimeMillis = JobConfig.getClock().currentTimeMillis() - j;
            builder.setExecutionWindow(Math.max(1, getStartMs() - currentTimeMillis), Math.max(1, getEndMs() - currentTimeMillis));
        }
        return builder;
    }

    /* access modifiers changed from: package-private */
    public Builder createBuilder() {
        return new Builder(this.mBuilder, true);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.mBuilder.equals(((JobRequest) obj).mBuilder);
    }

    public long getBackoffMs() {
        return this.mBuilder.mBackoffMs;
    }

    /* access modifiers changed from: package-private */
    public long getBackoffOffset() {
        long backoffMs;
        if (isPeriodic()) {
            return 0;
        }
        switch (getBackoffPolicy()) {
            case LINEAR:
                backoffMs = ((long) this.mFailureCount) * getBackoffMs();
                break;
            case EXPONENTIAL:
                if (this.mFailureCount != 0) {
                    backoffMs = (long) (((double) getBackoffMs()) * Math.pow(2.0d, (double) (this.mFailureCount - 1)));
                    break;
                } else {
                    backoffMs = 0;
                    break;
                }
            default:
                throw new IllegalStateException("not implemented");
        }
        return Math.min(backoffMs, TimeUnit.HOURS.toMillis(5));
    }

    public BackoffPolicy getBackoffPolicy() {
        return this.mBuilder.mBackoffPolicy;
    }

    public long getEndMs() {
        return this.mBuilder.mEndMs;
    }

    public PersistableBundleCompat getExtras() {
        if (this.mBuilder.mExtras == null && !TextUtils.isEmpty(this.mBuilder.mExtrasXml)) {
            PersistableBundleCompat unused = this.mBuilder.mExtras = PersistableBundleCompat.fromXml(this.mBuilder.mExtrasXml);
        }
        return this.mBuilder.mExtras;
    }

    public int getFailureCount() {
        return this.mFailureCount;
    }

    public long getFlexMs() {
        return this.mBuilder.mFlexMs;
    }

    public long getIntervalMs() {
        return this.mBuilder.mIntervalMs;
    }

    /* access modifiers changed from: package-private */
    public JobApi getJobApi() {
        return this.mBuilder.mExact ? JobApi.V_14 : JobApi.getDefault(context());
    }

    public int getJobId() {
        return this.mBuilder.mId;
    }

    public long getLastRun() {
        return this.mLastRun;
    }

    public long getScheduledAt() {
        return this.mScheduledAt;
    }

    public long getStartMs() {
        return this.mBuilder.mStartMs;
    }

    public String getTag() {
        return this.mBuilder.mTag;
    }

    public Bundle getTransientExtras() {
        return this.mBuilder.mTransientExtras;
    }

    public boolean hasRequirements() {
        return requiresCharging() || requiresDeviceIdle() || requiresBatteryNotLow() || requiresStorageNotLow() || requiredNetworkType() != DEFAULT_NETWORK_TYPE;
    }

    public int hashCode() {
        return this.mBuilder.hashCode();
    }

    public boolean isExact() {
        return this.mBuilder.mExact;
    }

    /* access modifiers changed from: package-private */
    public boolean isFlexSupport() {
        return this.mFlexSupport;
    }

    public boolean isPeriodic() {
        return getIntervalMs() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isStarted() {
        return this.mStarted;
    }

    public boolean isTransient() {
        return this.mBuilder.mTransient;
    }

    public boolean isUpdateCurrent() {
        return this.mBuilder.mUpdateCurrent;
    }

    public NetworkType requiredNetworkType() {
        return this.mBuilder.mNetworkType;
    }

    public boolean requirementsEnforced() {
        return this.mBuilder.mRequirementsEnforced;
    }

    public boolean requiresBatteryNotLow() {
        return this.mBuilder.mRequiresBatteryNotLow;
    }

    public boolean requiresCharging() {
        return this.mBuilder.mRequiresCharging;
    }

    public boolean requiresDeviceIdle() {
        return this.mBuilder.mRequiresDeviceIdle;
    }

    public boolean requiresStorageNotLow() {
        return this.mBuilder.mRequiresStorageNotLow;
    }

    /* access modifiers changed from: package-private */
    public JobRequest reschedule(boolean z, boolean z2) {
        JobRequest build = new Builder(this.mBuilder, z2).build();
        if (z) {
            build.mFailureCount = this.mFailureCount + 1;
        }
        try {
            build.schedule();
        } catch (Exception e) {
            CAT.e((Throwable) e);
        }
        return build;
    }

    public int schedule() {
        JobManager.instance().schedule(this);
        return getJobId();
    }

    public void scheduleAsync() {
        scheduleAsync(DEFAULT_JOB_SCHEDULED_CALLBACK);
    }

    public void scheduleAsync(final JobScheduledCallback jobScheduledCallback) {
        JobPreconditions.checkNotNull(jobScheduledCallback);
        JobConfig.getExecutorService().execute(new Runnable() {
            public void run() {
                try {
                    jobScheduledCallback.onJobScheduled(JobRequest.this.schedule(), JobRequest.this.getTag(), (Exception) null);
                } catch (Exception e) {
                    jobScheduledCallback.onJobScheduled(-1, JobRequest.this.getTag(), e);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setFlexSupport(boolean z) {
        this.mFlexSupport = z;
    }

    /* access modifiers changed from: package-private */
    public void setScheduledAt(long j) {
        this.mScheduledAt = j;
    }

    /* access modifiers changed from: package-private */
    public void setStarted(boolean z) {
        this.mStarted = z;
        ContentValues contentValues = new ContentValues();
        contentValues.put("started", Boolean.valueOf(this.mStarted));
        JobManager.instance().getJobStorage().m7541(this, contentValues);
    }

    /* access modifiers changed from: package-private */
    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        this.mBuilder.fillContentValues(contentValues);
        contentValues.put("numFailures", Integer.valueOf(this.mFailureCount));
        contentValues.put("scheduledAt", Long.valueOf(this.mScheduledAt));
        contentValues.put("started", Boolean.valueOf(this.mStarted));
        contentValues.put("flexSupport", Boolean.valueOf(this.mFlexSupport));
        contentValues.put("lastRun", Long.valueOf(this.mLastRun));
        return contentValues;
    }

    public String toString() {
        return "request{id=" + getJobId() + ", tag=" + getTag() + ", transient=" + isTransient() + '}';
    }

    /* access modifiers changed from: package-private */
    public void updateStats(boolean z, boolean z2) {
        ContentValues contentValues = new ContentValues();
        if (z) {
            this.mFailureCount++;
            contentValues.put("numFailures", Integer.valueOf(this.mFailureCount));
        }
        if (z2) {
            this.mLastRun = JobConfig.getClock().currentTimeMillis();
            contentValues.put("lastRun", Long.valueOf(this.mLastRun));
        }
        JobManager.instance().getJobStorage().m7541(this, contentValues);
    }
}
