package com.evernote.android.job;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.LruCache;
import com.evernote.android.job.util.JobCat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class JobStorage {
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final JobCat f6572 = new JobCat("JobStorage");

    /* renamed from: ʻ  reason: contains not printable characters */
    private final JobOpenHelper f6573;

    /* renamed from: ʼ  reason: contains not printable characters */
    private SQLiteDatabase f6574;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ReadWriteLock f6575;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public final Set<String> f6576;

    /* renamed from: 靐  reason: contains not printable characters */
    private final SharedPreferences f6577;

    /* renamed from: 麤  reason: contains not printable characters */
    private AtomicInteger f6578;

    /* renamed from: 齉  reason: contains not printable characters */
    private final JobCacheId f6579;

    private class JobCacheId extends LruCache<Integer, JobRequest> {
        public JobCacheId() {
            super(30);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public JobRequest create(Integer num) {
            return JobStorage.this.m7527(num.intValue(), true);
        }
    }

    private static final class JobOpenHelper extends SQLiteOpenHelper {
        private JobOpenHelper(Context context, String str) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, 6, new JobStorageDatabaseErrorHandler());
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        private void m7543(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("alter table jobs add column requiresBatteryNotLow integer;");
            sQLiteDatabase.execSQL("alter table jobs add column requiresStorageNotLow integer;");
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private void m7544(SQLiteDatabase sQLiteDatabase) {
            try {
                sQLiteDatabase.beginTransaction();
                sQLiteDatabase.execSQL("create table " + "jobs_new" + " (" + "_id" + " integer primary key, " + "tag" + " text not null, " + "startMs" + " integer, " + "endMs" + " integer, " + "backoffMs" + " integer, " + "backoffPolicy" + " text not null, " + "intervalMs" + " integer, " + "requirementsEnforced" + " integer, " + "requiresCharging" + " integer, " + "requiresDeviceIdle" + " integer, " + "exact" + " integer, " + "networkType" + " text not null, " + "extras" + " text, " + "numFailures" + " integer, " + "scheduledAt" + " integer, " + "started" + " integer, " + "flexMs" + " integer, " + "flexSupport" + " integer, " + "lastRun" + " integer);");
                sQLiteDatabase.execSQL("INSERT INTO " + "jobs_new" + " SELECT " + "_id" + "," + "tag" + "," + "startMs" + "," + "endMs" + "," + "backoffMs" + "," + "backoffPolicy" + "," + "intervalMs" + "," + "requirementsEnforced" + "," + "requiresCharging" + "," + "requiresDeviceIdle" + "," + "exact" + "," + "networkType" + "," + "extras" + "," + "numFailures" + "," + "scheduledAt" + "," + "isTransient" + "," + "flexMs" + "," + "flexSupport" + "," + "lastRun" + " FROM " + "jobs");
                sQLiteDatabase.execSQL("DROP TABLE jobs");
                sQLiteDatabase.execSQL("ALTER TABLE " + "jobs_new" + " RENAME TO " + "jobs");
                sQLiteDatabase.execSQL("alter table jobs add column transient integer;");
                sQLiteDatabase.setTransactionSuccessful();
            } finally {
                sQLiteDatabase.endTransaction();
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m7545(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("alter table jobs add column isTransient integer;");
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private void m7546(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("alter table jobs add column lastRun integer;");
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m7547(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("alter table jobs add column flexMs integer;");
            sQLiteDatabase.execSQL("alter table jobs add column flexSupport integer;");
            ContentValues contentValues = new ContentValues();
            contentValues.put("intervalMs", Long.valueOf(JobRequest.MIN_INTERVAL));
            sQLiteDatabase.update("jobs", contentValues, "intervalMs>0 AND intervalMs<" + JobRequest.MIN_INTERVAL, new String[0]);
            sQLiteDatabase.execSQL("update jobs set flexMs = intervalMs;");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m7548(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("create table jobs (_id integer primary key, tag text not null, startMs integer, endMs integer, backoffMs integer, backoffPolicy text not null, intervalMs integer, requirementsEnforced integer, requiresCharging integer, requiresDeviceIdle integer, exact integer, networkType text not null, extras text, numFailures integer, scheduledAt integer, started integer, flexMs integer, flexSupport integer, lastRun integer, transient integer, requiresBatteryNotLow integer, requiresStorageNotLow integer);");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            m7548(sQLiteDatabase);
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            while (i < i2) {
                switch (i) {
                    case 1:
                        m7545(sQLiteDatabase);
                        i++;
                        break;
                    case 2:
                        m7547(sQLiteDatabase);
                        i++;
                        break;
                    case 3:
                        m7546(sQLiteDatabase);
                        i++;
                        break;
                    case 4:
                        m7544(sQLiteDatabase);
                        i++;
                        break;
                    case 5:
                        m7543(sQLiteDatabase);
                        i++;
                        break;
                    default:
                        throw new IllegalStateException("not implemented");
                }
            }
        }
    }

    public JobStorage(Context context) {
        this(context, "evernote_jobs.db");
    }

    public JobStorage(Context context, String str) {
        this.f6577 = context.getSharedPreferences("evernote_jobs", 0);
        this.f6575 = new ReentrantReadWriteLock();
        this.f6579 = new JobCacheId();
        this.f6573 = new JobOpenHelper(context, str);
        this.f6576 = this.f6577.getStringSet("FAILED_DELETE_IDS", new HashSet());
        if (!this.f6576.isEmpty()) {
            m7521();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m7521() {
        new Thread("CleanupFinishedJobsThread") {
            public void run() {
                HashSet hashSet;
                synchronized (JobStorage.this.f6576) {
                    hashSet = new HashSet(JobStorage.this.f6576);
                }
                Iterator it2 = hashSet.iterator();
                while (it2.hasNext()) {
                    try {
                        int parseInt = Integer.parseInt((String) it2.next());
                        if (JobStorage.this.m7532((JobRequest) null, parseInt)) {
                            it2.remove();
                            JobStorage.f6572.i("Deleted job %d which failed to delete earlier", Integer.valueOf(parseInt));
                        } else {
                            JobStorage.f6572.e("Couldn't delete job %d which failed to delete earlier", Integer.valueOf(parseInt));
                        }
                    } catch (NumberFormatException e) {
                        it2.remove();
                    }
                }
                synchronized (JobStorage.this.f6576) {
                    JobStorage.this.f6576.clear();
                    if (hashSet.size() > 50) {
                        Iterator it3 = hashSet.iterator();
                        int i = 0;
                        while (true) {
                            if (!it3.hasNext()) {
                                int i2 = i;
                                break;
                            }
                            String str = (String) it3.next();
                            int i3 = i + 1;
                            if (i > 50) {
                                break;
                            }
                            JobStorage.this.f6576.add(str);
                            i = i3;
                        }
                    } else {
                        JobStorage.this.f6576.addAll(hashSet);
                    }
                }
            }
        }.start();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m7522(int i) {
        synchronized (this.f6576) {
            this.f6576.add(String.valueOf(i));
            this.f6577.edit().putStringSet("FAILED_DELETE_IDS", this.f6576).apply();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m7524(JobRequest jobRequest) {
        ContentValues contentValues = jobRequest.toContentValues();
        SQLiteDatabase sQLiteDatabase = null;
        try {
            sQLiteDatabase = m7534();
            if (sQLiteDatabase.insertWithOnConflict("jobs", (String) null, contentValues, 5) < 0) {
                throw new SQLException("Couldn't insert job request into database");
            }
        } finally {
            m7531(sQLiteDatabase);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m7525(JobRequest jobRequest) {
        this.f6579.put(Integer.valueOf(jobRequest.getJobId()), jobRequest);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m7526(int i) {
        boolean z;
        synchronized (this.f6576) {
            z = !this.f6576.isEmpty() && this.f6576.contains(String.valueOf(i));
        }
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public JobRequest m7527(int i, boolean z) {
        if (m7526(i)) {
            return null;
        }
        String str = "_id=?";
        if (!z) {
            try {
                str = str + " AND started<=0";
            } catch (Exception e) {
                f6572.e(e, "could not load id %d", Integer.valueOf(i));
                m7530((Cursor) null);
                m7531((SQLiteDatabase) null);
            } catch (Throwable th) {
                m7530((Cursor) null);
                m7531((SQLiteDatabase) null);
                throw th;
            }
        }
        SQLiteDatabase r0 = m7534();
        Cursor query = r0.query("jobs", (String[]) null, str, new String[]{String.valueOf(i)}, (String) null, (String) null, (String) null);
        if (query == null || !query.moveToFirst()) {
            m7530(query);
            m7531(r0);
            return null;
        }
        JobRequest fromCursor = JobRequest.fromCursor(query);
        m7530(query);
        m7531(r0);
        return fromCursor;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m7530(Cursor cursor) {
        if (cursor != null) {
            try {
                cursor.close();
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m7531(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase != null && JobConfig.isCloseDatabase()) {
            try {
                sQLiteDatabase.close();
            } catch (Exception e) {
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7532(JobRequest jobRequest, int i) {
        SQLiteDatabase sQLiteDatabase = null;
        this.f6575.writeLock().lock();
        try {
            this.f6579.remove(Integer.valueOf(i));
            sQLiteDatabase = m7534();
            sQLiteDatabase.delete("jobs", "_id=?", new String[]{String.valueOf(i)});
            m7531(sQLiteDatabase);
            this.f6575.writeLock().unlock();
            return true;
        } catch (Exception e) {
            f6572.e(e, "could not delete %d %s", Integer.valueOf(i), jobRequest);
            m7522(i);
            m7531(sQLiteDatabase);
            this.f6575.writeLock().unlock();
            return false;
        } catch (Throwable th) {
            m7531(sQLiteDatabase);
            this.f6575.writeLock().unlock();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public SQLiteDatabase m7534() {
        if (this.f6574 != null) {
            return this.f6574;
        }
        try {
            return this.f6573.getWritableDatabase();
        } catch (SQLiteCantOpenDatabaseException e) {
            f6572.e((Throwable) e);
            new JobStorageDatabaseErrorHandler().m7551("evernote_jobs.db");
            return this.f6573.getWritableDatabase();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m7535(JobRequest jobRequest) {
        m7532(jobRequest, jobRequest.getJobId());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m7536() {
        SQLiteDatabase sQLiteDatabase = null;
        Cursor cursor = null;
        int i = 0;
        try {
            sQLiteDatabase = m7534();
            cursor = sQLiteDatabase.rawQuery("SELECT MAX(_id) FROM jobs", (String[]) null);
            if (cursor != null && cursor.moveToFirst()) {
                i = cursor.getInt(0);
            }
        } catch (Exception e) {
            f6572.e((Throwable) e);
        } finally {
            m7530(cursor);
            m7531(sQLiteDatabase);
        }
        return Math.max(JobConfig.getJobIdOffset(), Math.max(i, this.f6577.getInt("JOB_ID_COUNTER_v2", 0)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized int m7537() {
        int incrementAndGet;
        if (this.f6578 == null) {
            this.f6578 = new AtomicInteger(m7536());
        }
        incrementAndGet = this.f6578.incrementAndGet();
        int jobIdOffset = JobConfig.getJobIdOffset();
        if (incrementAndGet < jobIdOffset || incrementAndGet >= 2147480000) {
            this.f6578.set(jobIdOffset);
            incrementAndGet = this.f6578.incrementAndGet();
        }
        this.f6577.edit().putInt("JOB_ID_COUNTER_v2", incrementAndGet).apply();
        return incrementAndGet;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public JobRequest m7538(int i) {
        this.f6575.readLock().lock();
        try {
            return (JobRequest) this.f6579.get(Integer.valueOf(i));
        } finally {
            this.f6575.readLock().unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<JobRequest> m7539(String str, boolean z) {
        String[] strArr;
        String str2 = null;
        HashSet hashSet = new HashSet();
        SQLiteDatabase sQLiteDatabase = null;
        Cursor cursor = null;
        this.f6575.readLock().lock();
        try {
            if (TextUtils.isEmpty(str)) {
                if (!z) {
                    str2 = "ifnull(started, 0)<=0";
                }
                strArr = null;
            } else {
                str2 = (z ? "" : "ifnull(started, 0)<=0 AND ") + "tag=?";
                strArr = new String[]{str};
            }
            sQLiteDatabase = m7534();
            cursor = sQLiteDatabase.query("jobs", (String[]) null, str2, strArr, (String) null, (String) null, (String) null);
            HashMap hashMap = new HashMap(this.f6579.snapshot());
            while (cursor != null && cursor.moveToNext()) {
                Integer valueOf = Integer.valueOf(cursor.getInt(cursor.getColumnIndex("_id")));
                if (!m7526(valueOf.intValue())) {
                    if (hashMap.containsKey(valueOf)) {
                        hashSet.add(hashMap.get(valueOf));
                    } else {
                        hashSet.add(JobRequest.fromCursor(cursor));
                    }
                }
            }
        } catch (Exception e) {
            f6572.e(e, "could not load all jobs", new Object[0]);
        } finally {
            m7530(cursor);
            m7531(sQLiteDatabase);
            this.f6575.readLock().unlock();
        }
        return hashSet;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7540(JobRequest jobRequest) {
        this.f6575.writeLock().lock();
        try {
            m7524(jobRequest);
            m7525(jobRequest);
        } finally {
            this.f6575.writeLock().unlock();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7541(JobRequest jobRequest, ContentValues contentValues) {
        SQLiteDatabase sQLiteDatabase = null;
        this.f6575.writeLock().lock();
        try {
            m7525(jobRequest);
            sQLiteDatabase = m7534();
            sQLiteDatabase.update("jobs", contentValues, "_id=?", new String[]{String.valueOf(jobRequest.getJobId())});
        } catch (Exception e) {
            f6572.e(e, "could not update %s", jobRequest);
        } finally {
            m7531(sQLiteDatabase);
            this.f6575.writeLock().unlock();
        }
    }
}
