package com.evernote.android.job.v21;

import android.annotation.TargetApi;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.os.Bundle;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobConfig;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobProxy;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.JobCat;

@TargetApi(21)
public class PlatformJobService extends JobService {
    /* access modifiers changed from: private */
    public static final JobCat CAT = new JobCat("PlatformJobService");

    /* access modifiers changed from: private */
    @TargetApi(26)
    public Bundle getTransientBundle(JobParameters jobParameters) {
        return Build.VERSION.SDK_INT >= 26 ? jobParameters.getTransientExtras() : Bundle.EMPTY;
    }

    public boolean onStartJob(final JobParameters jobParameters) {
        JobConfig.getExecutorService().execute(new Runnable() {
            public void run() {
                try {
                    JobProxy.Common common = new JobProxy.Common((Service) PlatformJobService.this, PlatformJobService.CAT, jobParameters.getJobId());
                    JobRequest pendingRequest = common.getPendingRequest(true, false);
                    if (pendingRequest != null) {
                        if (pendingRequest.isTransient()) {
                            if (TransientBundleCompat.m7592(PlatformJobService.this, pendingRequest)) {
                                if (Build.VERSION.SDK_INT >= 26) {
                                    PlatformJobService.CAT.d("PendingIntent for transient bundle is not null although running on O, using compat mode, request %s", pendingRequest);
                                }
                                PlatformJobService.this.jobFinished(jobParameters, false);
                                return;
                            } else if (Build.VERSION.SDK_INT < 26) {
                                PlatformJobService.CAT.d("PendingIntent for transient job %s expired", pendingRequest);
                                PlatformJobService.this.jobFinished(jobParameters, false);
                                return;
                            }
                        }
                        common.markStarting(pendingRequest);
                        common.executeJobRequest(pendingRequest, PlatformJobService.this.getTransientBundle(jobParameters));
                        PlatformJobService.this.jobFinished(jobParameters, false);
                    }
                } finally {
                    PlatformJobService.this.jobFinished(jobParameters, false);
                }
            }
        });
        return true;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        Job job = JobManager.instance().getJob(jobParameters.getJobId());
        if (job != null) {
            job.cancel();
            CAT.d("Called onStopJob for %s", job);
        } else {
            CAT.d("Called onStopJob, job %d not found", Integer.valueOf(jobParameters.getJobId()));
        }
        return false;
    }
}
