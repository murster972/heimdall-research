package com.evernote.android.job.v21;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import com.evernote.android.job.JobProxy;
import com.evernote.android.job.JobProxyIllegalStateException;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobUtil;
import java.util.List;

@TargetApi(21)
public class JobProxy21 implements JobProxy {
    private static final int ERROR_BOOT_PERMISSION = -123;
    private static final String TAG = "JobProxy21";
    protected final JobCat mCat;
    protected final Context mContext;

    public JobProxy21(Context context) {
        this(context, TAG);
    }

    protected JobProxy21(Context context, String str) {
        this.mContext = context;
        this.mCat = new JobCat(str);
    }

    protected static String scheduleResultToString(int i) {
        return i == 1 ? "success" : "failure";
    }

    public void cancel(int i) {
        try {
            getJobScheduler().cancel(i);
        } catch (Exception e) {
            this.mCat.e((Throwable) e);
        }
        TransientBundleCompat.m7593(this.mContext, i, (PendingIntent) null);
    }

    /* access modifiers changed from: protected */
    public int convertNetworkType(JobRequest.NetworkType networkType) {
        switch (networkType) {
            case ANY:
                return 0;
            case CONNECTED:
            case METERED:
                return 1;
            case UNMETERED:
                return 2;
            case NOT_ROAMING:
                return 2;
            default:
                throw new IllegalStateException("not implemented");
        }
    }

    /* access modifiers changed from: protected */
    public JobInfo.Builder createBaseBuilder(JobRequest jobRequest, boolean z) {
        return setTransientBundle(jobRequest, new JobInfo.Builder(jobRequest.getJobId(), new ComponentName(this.mContext, PlatformJobService.class)).setRequiresCharging(jobRequest.requiresCharging()).setRequiresDeviceIdle(jobRequest.requiresDeviceIdle()).setRequiredNetworkType(convertNetworkType(jobRequest.requiredNetworkType())).setPersisted(z && !jobRequest.isTransient() && JobUtil.hasBootPermission(this.mContext)));
    }

    /* access modifiers changed from: protected */
    public JobInfo.Builder createBuilderOneOff(JobInfo.Builder builder, long j, long j2) {
        return builder.setMinimumLatency(j).setOverrideDeadline(j2);
    }

    /* access modifiers changed from: protected */
    public JobInfo.Builder createBuilderPeriodic(JobInfo.Builder builder, long j, long j2) {
        return builder.setPeriodic(j);
    }

    /* access modifiers changed from: protected */
    public final JobScheduler getJobScheduler() {
        return (JobScheduler) this.mContext.getSystemService("jobscheduler");
    }

    /* access modifiers changed from: protected */
    public boolean isJobInfoScheduled(JobInfo jobInfo, JobRequest jobRequest) {
        if (!(jobInfo != null && jobInfo.getId() == jobRequest.getJobId())) {
            return false;
        }
        return !jobRequest.isTransient() || TransientBundleCompat.m7595(this.mContext, jobRequest.getJobId());
    }

    public boolean isPlatformJobScheduled(JobRequest jobRequest) {
        try {
            List<JobInfo> allPendingJobs = getJobScheduler().getAllPendingJobs();
            if (allPendingJobs == null || allPendingJobs.isEmpty()) {
                return false;
            }
            for (JobInfo isJobInfoScheduled : allPendingJobs) {
                if (isJobInfoScheduled(isJobInfoScheduled, jobRequest)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            this.mCat.e((Throwable) e);
            return false;
        }
    }

    public void plantOneOff(JobRequest jobRequest) {
        long startMs = JobProxy.Common.getStartMs(jobRequest);
        long endMs = JobProxy.Common.getEndMs(jobRequest, true);
        int schedule = schedule(createBuilderOneOff(createBaseBuilder(jobRequest, true), startMs, endMs).build());
        if (schedule == ERROR_BOOT_PERMISSION) {
            schedule = schedule(createBuilderOneOff(createBaseBuilder(jobRequest, false), startMs, endMs).build());
        }
        this.mCat.d("Schedule one-off jobInfo %s, %s, start %s, end %s (from now), reschedule count %d", scheduleResultToString(schedule), jobRequest, JobUtil.timeToString(startMs), JobUtil.timeToString(JobProxy.Common.getEndMs(jobRequest, false)), Integer.valueOf(JobProxy.Common.getRescheduleCount(jobRequest)));
    }

    public void plantPeriodic(JobRequest jobRequest) {
        long intervalMs = jobRequest.getIntervalMs();
        long flexMs = jobRequest.getFlexMs();
        int schedule = schedule(createBuilderPeriodic(createBaseBuilder(jobRequest, true), intervalMs, flexMs).build());
        if (schedule == ERROR_BOOT_PERMISSION) {
            schedule = schedule(createBuilderPeriodic(createBaseBuilder(jobRequest, false), intervalMs, flexMs).build());
        }
        this.mCat.d("Schedule periodic jobInfo %s, %s, interval %s, flex %s", scheduleResultToString(schedule), jobRequest, JobUtil.timeToString(intervalMs), JobUtil.timeToString(flexMs));
    }

    public void plantPeriodicFlexSupport(JobRequest jobRequest) {
        long startMsSupportFlex = JobProxy.Common.getStartMsSupportFlex(jobRequest);
        long endMsSupportFlex = JobProxy.Common.getEndMsSupportFlex(jobRequest);
        int schedule = schedule(createBuilderOneOff(createBaseBuilder(jobRequest, true), startMsSupportFlex, endMsSupportFlex).build());
        if (schedule == ERROR_BOOT_PERMISSION) {
            schedule = schedule(createBuilderOneOff(createBaseBuilder(jobRequest, false), startMsSupportFlex, endMsSupportFlex).build());
        }
        this.mCat.d("Schedule periodic (flex support) jobInfo %s, %s, start %s, end %s, flex %s", scheduleResultToString(schedule), jobRequest, JobUtil.timeToString(startMsSupportFlex), JobUtil.timeToString(endMsSupportFlex), JobUtil.timeToString(jobRequest.getFlexMs()));
    }

    /* access modifiers changed from: protected */
    public final int schedule(JobInfo jobInfo) {
        JobScheduler jobScheduler = getJobScheduler();
        if (jobScheduler == null) {
            throw new JobProxyIllegalStateException("JobScheduler is null");
        }
        try {
            return jobScheduler.schedule(jobInfo);
        } catch (IllegalArgumentException e) {
            this.mCat.e((Throwable) e);
            String message = e.getMessage();
            if (message != null && message.contains("RECEIVE_BOOT_COMPLETED")) {
                return ERROR_BOOT_PERMISSION;
            }
            if (message == null || !message.contains("No such service ComponentInfo")) {
                throw e;
            }
            throw new JobProxyIllegalStateException((Throwable) e);
        } catch (NullPointerException e2) {
            this.mCat.e((Throwable) e2);
            throw new JobProxyIllegalStateException((Throwable) e2);
        }
    }

    /* access modifiers changed from: protected */
    public JobInfo.Builder setTransientBundle(JobRequest jobRequest, JobInfo.Builder builder) {
        if (jobRequest.isTransient()) {
            TransientBundleCompat.m7594(this.mContext, jobRequest);
        }
        return builder;
    }
}
