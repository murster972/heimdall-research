package com.evernote.android.job.v21;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.v14.PlatformAlarmServiceExact;
import java.util.concurrent.TimeUnit;

@TargetApi(21)
final class TransientBundleCompat {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final JobCat f6605 = new JobCat("TransientBundleCompat");

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m7592(Context context, JobRequest jobRequest) {
        PendingIntent service = PendingIntent.getService(context, jobRequest.getJobId(), PlatformAlarmServiceExact.createIntent(context, jobRequest.getJobId(), (Bundle) null), 536870912);
        if (service == null) {
            return false;
        }
        try {
            f6605.i("Delegating transient job %s to API 14", jobRequest);
            service.send();
            if (!jobRequest.isPeriodic()) {
                m7593(context, jobRequest.getJobId(), service);
            }
            return true;
        } catch (PendingIntent.CanceledException e) {
            f6605.e((Throwable) e);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m7593(Context context, int i, PendingIntent pendingIntent) {
        if (pendingIntent == null) {
            try {
                pendingIntent = PendingIntent.getService(context, i, PlatformAlarmServiceExact.createIntent(context, i, (Bundle) null), 536870912);
                if (pendingIntent == null) {
                    return;
                }
            } catch (Exception e) {
                f6605.e((Throwable) e);
                return;
            }
        }
        ((AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM)).cancel(pendingIntent);
        pendingIntent.cancel();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m7594(Context context, JobRequest jobRequest) {
        PendingIntent service = PendingIntent.getService(context, jobRequest.getJobId(), PlatformAlarmServiceExact.createIntent(context, jobRequest.getJobId(), jobRequest.getTransientExtras()), 134217728);
        ((AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM)).setExact(1, System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1000), service);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m7595(Context context, int i) {
        return PendingIntent.getService(context, i, PlatformAlarmServiceExact.createIntent(context, i, (Bundle) null), 536870912) != null;
    }
}
