package com.evernote.android.job;

public final class JobIdsInternal {
    public static final int JOB_ID_JOB_RESCHEDULE_SERVICE = 2147480000;
    public static final int JOB_ID_PLATFORM_ALARM_SERVICE = 2147480001;
    public static final int RESERVED_JOB_ID_RANGE_START = 2147480000;

    private JobIdsInternal() {
    }
}
