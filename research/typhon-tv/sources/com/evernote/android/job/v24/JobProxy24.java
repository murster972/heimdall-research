package com.evernote.android.job.v24;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.content.Context;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.v21.JobProxy21;

@TargetApi(24)
public class JobProxy24 extends JobProxy21 {
    private static final String TAG = "JobProxy24";

    public JobProxy24(Context context) {
        this(context, TAG);
    }

    public JobProxy24(Context context, String str) {
        super(context, str);
    }

    /* access modifiers changed from: protected */
    public int convertNetworkType(JobRequest.NetworkType networkType) {
        switch (networkType) {
            case NOT_ROAMING:
                return 3;
            default:
                return super.convertNetworkType(networkType);
        }
    }

    /* access modifiers changed from: protected */
    public JobInfo.Builder createBuilderPeriodic(JobInfo.Builder builder, long j, long j2) {
        return builder.setPeriodic(j, j2);
    }

    public boolean isPlatformJobScheduled(JobRequest jobRequest) {
        try {
            return isJobInfoScheduled(getJobScheduler().getPendingJob(jobRequest.getJobId()), jobRequest);
        } catch (Exception e) {
            this.mCat.e((Throwable) e);
            return false;
        }
    }

    public void plantPeriodicFlexSupport(JobRequest jobRequest) {
        this.mCat.w("plantPeriodicFlexSupport called although flex is supported");
        super.plantPeriodicFlexSupport(jobRequest);
    }
}
