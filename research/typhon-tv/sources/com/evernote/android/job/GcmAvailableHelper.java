package com.evernote.android.job;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import com.evernote.android.job.gcm.JobProxyGcm;
import com.evernote.android.job.gcm.PlatformGcmService;
import com.evernote.android.job.util.JobCat;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GcmTaskService;
import java.util.List;

final class GcmAvailableHelper {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final boolean f6551;

    /* renamed from: 麤  reason: contains not printable characters */
    private static boolean f6552;

    /* renamed from: 齉  reason: contains not printable characters */
    private static int f6553 = -1;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final JobCat f6554 = new JobCat("GcmAvailableHelper");

    static {
        boolean z;
        try {
            Class.forName("com.google.android.gms.gcm.GcmNetworkManager");
            z = true;
        } catch (Throwable th) {
            z = false;
        }
        f6551 = z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m7499(Context context) {
        if (f6553 < 0) {
            synchronized (JobApi.class) {
                if (f6553 < 0) {
                    if (!m7502(context.getPackageManager().queryIntentServices(new Intent(context, PlatformGcmService.class), 0))) {
                        f6553 = 1;
                        int i = f6553;
                        return i;
                    }
                    Intent intent = new Intent(GcmTaskService.SERVICE_ACTION_EXECUTE_TASK);
                    intent.setPackage(context.getPackageName());
                    if (!m7502(context.getPackageManager().queryIntentServices(intent, 0))) {
                        f6553 = 1;
                        int i2 = f6553;
                        return i2;
                    }
                    f6553 = 0;
                }
            }
        }
        return f6553;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m7500(Context context, boolean z) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ComponentName componentName = new ComponentName(context, JobProxyGcm.class.getPackage().getName() + ".PlatformGcmService");
            switch (packageManager.getComponentEnabledSetting(componentName)) {
                case 0:
                case 2:
                    if (z) {
                        packageManager.setComponentEnabledSetting(componentName, 1, 1);
                        f6554.i("GCM service enabled");
                        return;
                    }
                    return;
                case 1:
                    if (!z) {
                        packageManager.setComponentEnabledSetting(componentName, 2, 1);
                        f6554.i("GCM service disabled");
                        return;
                    }
                    return;
                default:
                    return;
            }
        } catch (Throwable th) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m7501(Context context) {
        try {
            if (!f6552) {
                f6552 = true;
                m7500(context, f6551);
            }
            return f6551 && GoogleApiAvailability.m8486().m8494(context) == 0 && m7499(context) == 0;
        } catch (Throwable th) {
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m7502(List<ResolveInfo> list) {
        if (list == null || list.isEmpty()) {
            return false;
        }
        for (ResolveInfo next : list) {
            if (next.serviceInfo != null && GcmTaskService.SERVICE_PERMISSION.equals(next.serviceInfo.permission) && next.serviceInfo.exported) {
                return true;
            }
        }
        return false;
    }
}
