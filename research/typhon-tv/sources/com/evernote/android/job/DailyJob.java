package com.evernote.android.job;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobPreconditions;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public abstract class DailyJob extends Job {
    private static final JobCat CAT = new JobCat("DailyJob");
    private static final long DAY = TimeUnit.DAYS.toMillis(1);
    static final String EXTRA_END_MS = "EXTRA_END_MS";
    private static final String EXTRA_ONCE = "EXTRA_ONCE";
    static final String EXTRA_START_MS = "EXTRA_START_MS";

    public enum DailyJobResult {
        SUCCESS,
        CANCEL
    }

    public static int schedule(JobRequest.Builder builder, long j, long j2) {
        return schedule(builder, true, j, j2, false);
    }

    private static int schedule(JobRequest.Builder builder, boolean z, long j, long j2, boolean z2) {
        if (j >= DAY || j2 >= DAY || j < 0 || j2 < 0) {
            throw new IllegalArgumentException("startMs or endMs should be less than one day (in milliseconds)");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(JobConfig.getClock().currentTimeMillis());
        int i = instance.get(11);
        int i2 = instance.get(12);
        long millis = ((((((TimeUnit.SECONDS.toMillis((long) (60 - instance.get(13))) + TimeUnit.MINUTES.toMillis((long) (60 - i2))) + TimeUnit.HOURS.toMillis((long) ((24 - i) % 24))) - TimeUnit.HOURS.toMillis(1)) - TimeUnit.MINUTES.toMillis(1)) + TimeUnit.DAYS.toMillis(1)) + j) % TimeUnit.DAYS.toMillis(1);
        if (z2 && millis < TimeUnit.HOURS.toMillis(12)) {
            millis += TimeUnit.DAYS.toMillis(1);
        }
        if (j > j2) {
            j2 += TimeUnit.DAYS.toMillis(1);
        }
        long j3 = millis + (j2 - j);
        PersistableBundleCompat persistableBundleCompat = new PersistableBundleCompat();
        persistableBundleCompat.putLong(EXTRA_START_MS, j);
        persistableBundleCompat.putLong(EXTRA_END_MS, j2);
        builder.addExtras(persistableBundleCompat);
        if (z) {
            JobManager instance2 = JobManager.instance();
            for (JobRequest jobRequest : new HashSet(instance2.getAllJobRequestsForTag(builder.mTag))) {
                if (!jobRequest.isExact() || jobRequest.getStartMs() != 1) {
                    instance2.cancel(jobRequest.getJobId());
                }
            }
        }
        JobRequest build = builder.setExecutionWindow(Math.max(1, millis), Math.max(1, j3)).build();
        if (!z || (!build.isExact() && !build.isPeriodic() && !build.isTransient())) {
            return build.schedule();
        }
        throw new IllegalArgumentException("Daily jobs cannot be exact, periodic or transient");
    }

    public static void scheduleAsync(JobRequest.Builder builder, long j, long j2) {
        scheduleAsync(builder, j, j2, JobRequest.DEFAULT_JOB_SCHEDULED_CALLBACK);
    }

    public static void scheduleAsync(JobRequest.Builder builder, long j, long j2, JobRequest.JobScheduledCallback jobScheduledCallback) {
        JobPreconditions.checkNotNull(jobScheduledCallback);
        final JobRequest.Builder builder2 = builder;
        final long j3 = j;
        final long j4 = j2;
        final JobRequest.JobScheduledCallback jobScheduledCallback2 = jobScheduledCallback;
        JobConfig.getExecutorService().execute(new Runnable() {
            public void run() {
                try {
                    jobScheduledCallback2.onJobScheduled(DailyJob.schedule(builder2, j3, j4), builder2.mTag, (Exception) null);
                } catch (Exception e) {
                    jobScheduledCallback2.onJobScheduled(-1, builder2.mTag, e);
                }
            }
        });
    }

    public static int startNowOnce(JobRequest.Builder builder) {
        PersistableBundleCompat persistableBundleCompat = new PersistableBundleCompat();
        persistableBundleCompat.putBoolean(EXTRA_ONCE, true);
        return builder.startNow().addExtras(persistableBundleCompat).build().schedule();
    }

    /* access modifiers changed from: protected */
    public abstract DailyJobResult onRunDailyJob(Job.Params params);

    /* access modifiers changed from: protected */
    public final Job.Result onRunJob(Job.Params params) {
        PersistableBundleCompat extras = params.getExtras();
        boolean z = extras.getBoolean(EXTRA_ONCE, false);
        if (z || (extras.containsKey(EXTRA_START_MS) && extras.containsKey(EXTRA_END_MS))) {
            DailyJobResult dailyJobResult = null;
            try {
                if (meetsRequirements(true)) {
                    dailyJobResult = onRunDailyJob(params);
                } else {
                    dailyJobResult = DailyJobResult.SUCCESS;
                    CAT.i("Daily job requirements not met, reschedule for the next day");
                }
                if (dailyJobResult == null) {
                    dailyJobResult = DailyJobResult.SUCCESS;
                    CAT.e("Daily job result was null");
                }
                if (!z) {
                    JobRequest request = params.getRequest();
                    if (dailyJobResult == DailyJobResult.SUCCESS) {
                        CAT.i("Rescheduling daily job %s", request);
                        JobRequest jobRequest = JobManager.instance().getJobRequest(schedule(request.createBuilder(), false, extras.getLong(EXTRA_START_MS, 0) % DAY, extras.getLong(EXTRA_END_MS, 0) % DAY, true));
                        if (jobRequest != null) {
                            jobRequest.updateStats(false, true);
                        }
                    } else {
                        CAT.i("Cancel daily job %s", request);
                    }
                }
                return Job.Result.SUCCESS;
            } catch (Throwable th) {
                Throwable th2 = th;
                if (dailyJobResult == null) {
                    dailyJobResult = DailyJobResult.SUCCESS;
                    CAT.e("Daily job result was null");
                }
                if (!z) {
                    JobRequest request2 = params.getRequest();
                    if (dailyJobResult == DailyJobResult.SUCCESS) {
                        CAT.i("Rescheduling daily job %s", request2);
                        JobRequest jobRequest2 = JobManager.instance().getJobRequest(schedule(request2.createBuilder(), false, extras.getLong(EXTRA_START_MS, 0) % DAY, extras.getLong(EXTRA_END_MS, 0) % DAY, true));
                        if (jobRequest2 != null) {
                            jobRequest2.updateStats(false, true);
                        }
                    } else {
                        CAT.i("Cancel daily job %s", request2);
                    }
                }
                throw th2;
            }
        } else {
            CAT.e("Daily job doesn't contain start and end time");
            return Job.Result.FAILURE;
        }
    }
}
