package com.evernote.android.job;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.SparseArray;
import com.evernote.android.job.util.JobCat;
import com.evernote.android.job.util.JobUtil;
import java.util.concurrent.TimeUnit;

final class WakeLockUtil {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final SparseArray<PowerManager.WakeLock> f6583 = new SparseArray<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static int f6584 = 1;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final JobCat f6585 = new JobCat("WakeLockUtil");

    /* renamed from: 龘  reason: contains not printable characters */
    public static ComponentName m7552(Context context, Intent intent) {
        ComponentName startService;
        synchronized (f6583) {
            int i = f6584;
            f6584++;
            if (f6584 <= 0) {
                f6584 = 1;
            }
            intent.putExtra("com.evernote.android.job.wakelockid", i);
            startService = context.startService(intent);
            if (startService == null) {
                startService = null;
            } else {
                PowerManager.WakeLock r3 = m7553(context, "wake:" + startService.flattenToShortString(), TimeUnit.MINUTES.toMillis(3));
                if (r3 != null) {
                    f6583.put(i, r3);
                }
            }
        }
        return startService;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static PowerManager.WakeLock m7553(Context context, String str, long j) {
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, str);
        newWakeLock.setReferenceCounted(false);
        if (m7555(context, newWakeLock, j)) {
            return newWakeLock;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m7554(PowerManager.WakeLock wakeLock) {
        if (wakeLock != null) {
            try {
                if (wakeLock.isHeld()) {
                    wakeLock.release();
                }
            } catch (Exception e) {
                f6585.e((Throwable) e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m7555(Context context, PowerManager.WakeLock wakeLock, long j) {
        if (wakeLock != null && !wakeLock.isHeld() && JobUtil.hasWakeLockPermission(context)) {
            try {
                wakeLock.acquire(j);
                return true;
            } catch (Exception e) {
                f6585.e((Throwable) e);
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m7556(Intent intent) {
        int intExtra;
        boolean z = false;
        if (!(intent == null || (intExtra = intent.getIntExtra("com.evernote.android.job.wakelockid", 0)) == 0)) {
            synchronized (f6583) {
                m7554(f6583.get(intExtra));
                f6583.remove(intExtra);
                z = true;
            }
        }
        return z;
    }
}
