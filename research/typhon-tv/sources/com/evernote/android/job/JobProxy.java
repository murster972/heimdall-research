package com.evernote.android.job;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.evernote.android.job.util.JobCat;

public interface JobProxy {

    public static final class Common {
        private static final Object COMMON_MONITOR = new Object();
        private final JobCat mCat;
        private final Context mContext;
        private final int mJobId;
        private final JobManager mJobManager;

        public Common(Service service, JobCat jobCat, int i) {
            this((Context) service, jobCat, i);
        }

        Common(Context context, JobCat jobCat, int i) {
            JobManager jobManager;
            this.mContext = context;
            this.mJobId = i;
            this.mCat = jobCat;
            try {
                jobManager = JobManager.create(context);
            } catch (JobManagerCreateException e) {
                this.mCat.e((Throwable) e);
                jobManager = null;
            }
            this.mJobManager = jobManager;
        }

        private static long checkNoOverflow(long j, boolean z) {
            if (z) {
                return j;
            }
            return Long.MAX_VALUE;
        }

        private static long checkedAdd(long j, long j2) {
            boolean z = true;
            long j3 = j + j2;
            boolean z2 = (j ^ j2) < 0;
            if ((j ^ j3) < 0) {
                z = false;
            }
            return checkNoOverflow(j3, z | z2);
        }

        private static long checkedMultiply(long j, long j2) {
            boolean z = false;
            int numberOfLeadingZeros = Long.numberOfLeadingZeros(j) + Long.numberOfLeadingZeros(j ^ -1) + Long.numberOfLeadingZeros(j2) + Long.numberOfLeadingZeros(j2 ^ -1);
            if (numberOfLeadingZeros > 65) {
                return j * j2;
            }
            long checkNoOverflow = checkNoOverflow(checkNoOverflow(j * j2, numberOfLeadingZeros >= 64), (j2 != Long.MIN_VALUE) | (j >= 0));
            if (j == 0 || checkNoOverflow / j == j2) {
                z = true;
            }
            return checkNoOverflow(checkNoOverflow, z);
        }

        static void cleanUpOrphanedJob(Context context, int i) {
            for (JobApi jobApi : JobApi.values()) {
                if (jobApi.isSupported(context)) {
                    try {
                        jobApi.getProxy(context).cancel(i);
                    } catch (Exception e) {
                    }
                }
            }
        }

        private void cleanUpOrphanedJob(boolean z) {
            if (z) {
                cleanUpOrphanedJob(this.mContext, this.mJobId);
            }
        }

        public static boolean completeWakefulIntent(Intent intent) {
            return WakeLockUtil.m7556(intent);
        }

        public static long getAverageDelayMs(JobRequest jobRequest) {
            return checkedAdd(getStartMs(jobRequest), (getEndMs(jobRequest) - getStartMs(jobRequest)) / 2);
        }

        public static long getAverageDelayMsSupportFlex(JobRequest jobRequest) {
            return checkedAdd(getStartMsSupportFlex(jobRequest), (getEndMsSupportFlex(jobRequest) - getStartMsSupportFlex(jobRequest)) / 2);
        }

        public static long getEndMs(JobRequest jobRequest) {
            return getEndMs(jobRequest, false);
        }

        public static long getEndMs(JobRequest jobRequest, boolean z) {
            long backoffOffset = jobRequest.getFailureCount() > 0 ? jobRequest.getBackoffOffset() : jobRequest.getEndMs();
            return (!z || !jobRequest.requirementsEnforced() || !jobRequest.hasRequirements()) ? backoffOffset : checkedMultiply(backoffOffset, 100);
        }

        public static long getEndMsSupportFlex(JobRequest jobRequest) {
            return jobRequest.getIntervalMs();
        }

        public static int getRescheduleCount(JobRequest jobRequest) {
            return jobRequest.getFailureCount();
        }

        public static long getStartMs(JobRequest jobRequest) {
            return jobRequest.getFailureCount() > 0 ? jobRequest.getBackoffOffset() : jobRequest.getStartMs();
        }

        public static long getStartMsSupportFlex(JobRequest jobRequest) {
            return Math.max(1, jobRequest.getIntervalMs() - jobRequest.getFlexMs());
        }

        public static ComponentName startWakefulService(Context context, Intent intent) {
            return WakeLockUtil.m7552(context, intent);
        }

        /* JADX WARNING: Removed duplicated region for block: B:47:0x0179 A[Catch:{ all -> 0x01c7 }] */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x0194 A[DONT_GENERATE] */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x01a3  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.evernote.android.job.Job.Result executeJobRequest(com.evernote.android.job.JobRequest r17, android.os.Bundle r18) {
            /*
                r16 = this;
                long r10 = java.lang.System.currentTimeMillis()
                long r12 = r17.getScheduledAt()
                long r8 = r10 - r12
                boolean r10 = r17.isPeriodic()
                if (r10 == 0) goto L_0x00ad
                java.util.Locale r10 = java.util.Locale.US
                java.lang.String r11 = "interval %s, flex %s"
                r12 = 2
                java.lang.Object[] r12 = new java.lang.Object[r12]
                r13 = 0
                long r14 = r17.getIntervalMs()
                java.lang.String r14 = com.evernote.android.job.util.JobUtil.timeToString(r14)
                r12[r13] = r14
                r13 = 1
                long r14 = r17.getFlexMs()
                java.lang.String r14 = com.evernote.android.job.util.JobUtil.timeToString(r14)
                r12[r13] = r14
                java.lang.String r7 = java.lang.String.format(r10, r11, r12)
            L_0x0032:
                android.os.Looper r10 = android.os.Looper.myLooper()
                android.os.Looper r11 = android.os.Looper.getMainLooper()
                if (r10 != r11) goto L_0x0046
                r0 = r16
                com.evernote.android.job.util.JobCat r10 = r0.mCat
                java.lang.String r11 = "Running JobRequest on a main thread, this could cause stutter or ANR in your app."
                r10.w(r11)
            L_0x0046:
                r0 = r16
                com.evernote.android.job.util.JobCat r10 = r0.mCat
                java.lang.String r11 = "Run job, %s, waited %s, %s"
                r12 = 3
                java.lang.Object[] r12 = new java.lang.Object[r12]
                r13 = 0
                r12[r13] = r17
                r13 = 1
                java.lang.String r14 = com.evernote.android.job.util.JobUtil.timeToString(r8)
                r12[r13] = r14
                r13 = 2
                r12[r13] = r7
                r10.d(r11, r12)
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager
                com.evernote.android.job.JobExecutor r5 = r10.getJobExecutor()
                r4 = 0
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                com.evernote.android.job.JobCreatorHolder r10 = r10.getJobCreatorHolder()     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                java.lang.String r11 = r17.getTag()     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                com.evernote.android.job.Job r4 = r10.m7504((java.lang.String) r11)     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                boolean r10 = r17.isPeriodic()     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                if (r10 != 0) goto L_0x0085
                r10 = 1
                r0 = r17
                r0.setStarted(r10)     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
            L_0x0085:
                if (r18 != 0) goto L_0x0089
                android.os.Bundle r18 = android.os.Bundle.EMPTY     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
            L_0x0089:
                r0 = r16
                android.content.Context r10 = r0.mContext     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                r0 = r17
                r1 = r18
                java.util.concurrent.Future r3 = r5.m7514(r10, r0, r4, r1)     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                if (r3 != 0) goto L_0x011c
                com.evernote.android.job.Job$Result r6 = com.evernote.android.job.Job.Result.FAILURE     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                boolean r10 = r17.isPeriodic()
                if (r10 != 0) goto L_0x00f9
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager
                com.evernote.android.job.JobStorage r10 = r10.getJobStorage()
                r0 = r17
                r10.m7535((com.evernote.android.job.JobRequest) r0)
            L_0x00ac:
                return r6
            L_0x00ad:
                com.evernote.android.job.JobApi r10 = r17.getJobApi()
                boolean r10 = r10.supportsExecutionWindow()
                if (r10 == 0) goto L_0x00db
                java.util.Locale r10 = java.util.Locale.US
                java.lang.String r11 = "start %s, end %s"
                r12 = 2
                java.lang.Object[] r12 = new java.lang.Object[r12]
                r13 = 0
                long r14 = getStartMs(r17)
                java.lang.String r14 = com.evernote.android.job.util.JobUtil.timeToString(r14)
                r12[r13] = r14
                r13 = 1
                long r14 = getEndMs(r17)
                java.lang.String r14 = com.evernote.android.job.util.JobUtil.timeToString(r14)
                r12[r13] = r14
                java.lang.String r7 = java.lang.String.format(r10, r11, r12)
                goto L_0x0032
            L_0x00db:
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "delay "
                java.lang.StringBuilder r10 = r10.append(r11)
                long r12 = getAverageDelayMs(r17)
                java.lang.String r11 = com.evernote.android.job.util.JobUtil.timeToString(r12)
                java.lang.StringBuilder r10 = r10.append(r11)
                java.lang.String r7 = r10.toString()
                goto L_0x0032
            L_0x00f9:
                boolean r10 = r17.isFlexSupport()
                if (r10 == 0) goto L_0x00ac
                if (r4 == 0) goto L_0x0107
                boolean r10 = r4.isDeleted()
                if (r10 != 0) goto L_0x00ac
            L_0x0107:
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager
                com.evernote.android.job.JobStorage r10 = r10.getJobStorage()
                r0 = r17
                r10.m7535((com.evernote.android.job.JobRequest) r0)
                r10 = 0
                r11 = 0
                r0 = r17
                r0.reschedule(r10, r11)
                goto L_0x00ac
            L_0x011c:
                java.lang.Object r6 = r3.get()     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                com.evernote.android.job.Job$Result r6 = (com.evernote.android.job.Job.Result) r6     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                r0 = r16
                com.evernote.android.job.util.JobCat r10 = r0.mCat     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                java.lang.String r11 = "Finished job, %s %s"
                r12 = 2
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                r13 = 0
                r12[r13] = r17     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                r13 = 1
                r12[r13] = r6     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                r10.d(r11, r12)     // Catch:{ InterruptedException -> 0x016e, ExecutionException -> 0x01ff }
                boolean r10 = r17.isPeriodic()
                if (r10 != 0) goto L_0x014a
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager
                com.evernote.android.job.JobStorage r10 = r10.getJobStorage()
                r0 = r17
                r10.m7535((com.evernote.android.job.JobRequest) r0)
                goto L_0x00ac
            L_0x014a:
                boolean r10 = r17.isFlexSupport()
                if (r10 == 0) goto L_0x00ac
                if (r4 == 0) goto L_0x0158
                boolean r10 = r4.isDeleted()
                if (r10 != 0) goto L_0x00ac
            L_0x0158:
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager
                com.evernote.android.job.JobStorage r10 = r10.getJobStorage()
                r0 = r17
                r10.m7535((com.evernote.android.job.JobRequest) r0)
                r10 = 0
                r11 = 0
                r0 = r17
                r0.reschedule(r10, r11)
                goto L_0x00ac
            L_0x016e:
                r10 = move-exception
                r2 = r10
            L_0x0170:
                r0 = r16
                com.evernote.android.job.util.JobCat r10 = r0.mCat     // Catch:{ all -> 0x01c7 }
                r10.e((java.lang.Throwable) r2)     // Catch:{ all -> 0x01c7 }
                if (r4 == 0) goto L_0x018c
                r4.cancel()     // Catch:{ all -> 0x01c7 }
                r0 = r16
                com.evernote.android.job.util.JobCat r10 = r0.mCat     // Catch:{ all -> 0x01c7 }
                java.lang.String r11 = "Canceled %s"
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ all -> 0x01c7 }
                r13 = 0
                r12[r13] = r17     // Catch:{ all -> 0x01c7 }
                r10.e(r11, r12)     // Catch:{ all -> 0x01c7 }
            L_0x018c:
                com.evernote.android.job.Job$Result r6 = com.evernote.android.job.Job.Result.FAILURE     // Catch:{ all -> 0x01c7 }
                boolean r10 = r17.isPeriodic()
                if (r10 != 0) goto L_0x01a3
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager
                com.evernote.android.job.JobStorage r10 = r10.getJobStorage()
                r0 = r17
                r10.m7535((com.evernote.android.job.JobRequest) r0)
                goto L_0x00ac
            L_0x01a3:
                boolean r10 = r17.isFlexSupport()
                if (r10 == 0) goto L_0x00ac
                if (r4 == 0) goto L_0x01b1
                boolean r10 = r4.isDeleted()
                if (r10 != 0) goto L_0x00ac
            L_0x01b1:
                r0 = r16
                com.evernote.android.job.JobManager r10 = r0.mJobManager
                com.evernote.android.job.JobStorage r10 = r10.getJobStorage()
                r0 = r17
                r10.m7535((com.evernote.android.job.JobRequest) r0)
                r10 = 0
                r11 = 0
                r0 = r17
                r0.reschedule(r10, r11)
                goto L_0x00ac
            L_0x01c7:
                r10 = move-exception
                boolean r11 = r17.isPeriodic()
                if (r11 != 0) goto L_0x01dc
                r0 = r16
                com.evernote.android.job.JobManager r11 = r0.mJobManager
                com.evernote.android.job.JobStorage r11 = r11.getJobStorage()
                r0 = r17
                r11.m7535((com.evernote.android.job.JobRequest) r0)
            L_0x01db:
                throw r10
            L_0x01dc:
                boolean r11 = r17.isFlexSupport()
                if (r11 == 0) goto L_0x01db
                if (r4 == 0) goto L_0x01ea
                boolean r11 = r4.isDeleted()
                if (r11 != 0) goto L_0x01db
            L_0x01ea:
                r0 = r16
                com.evernote.android.job.JobManager r11 = r0.mJobManager
                com.evernote.android.job.JobStorage r11 = r11.getJobStorage()
                r0 = r17
                r11.m7535((com.evernote.android.job.JobRequest) r0)
                r11 = 0
                r12 = 0
                r0 = r17
                r0.reschedule(r11, r12)
                goto L_0x01db
            L_0x01ff:
                r10 = move-exception
                r2 = r10
                goto L_0x0170
            */
            throw new UnsupportedOperationException("Method not decompiled: com.evernote.android.job.JobProxy.Common.executeJobRequest(com.evernote.android.job.JobRequest, android.os.Bundle):com.evernote.android.job.Job$Result");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
            return r2;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.evernote.android.job.JobRequest getPendingRequest(boolean r11, boolean r12) {
            /*
                r10 = this;
                r3 = 0
                r4 = 0
                r1 = 1
                java.lang.Object r5 = COMMON_MONITOR
                monitor-enter(r5)
                com.evernote.android.job.JobManager r6 = r10.mJobManager     // Catch:{ all -> 0x0108 }
                if (r6 != 0) goto L_0x000d
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                r2 = r3
            L_0x000c:
                return r2
            L_0x000d:
                com.evernote.android.job.JobManager r6 = r10.mJobManager     // Catch:{ all -> 0x0108 }
                int r7 = r10.mJobId     // Catch:{ all -> 0x0108 }
                r8 = 1
                com.evernote.android.job.JobRequest r2 = r6.getJobRequest(r7, r8)     // Catch:{ all -> 0x0108 }
                com.evernote.android.job.JobManager r6 = r10.mJobManager     // Catch:{ all -> 0x0108 }
                int r7 = r10.mJobId     // Catch:{ all -> 0x0108 }
                com.evernote.android.job.Job r0 = r6.getJob(r7)     // Catch:{ all -> 0x0108 }
                if (r2 == 0) goto L_0x0048
                boolean r6 = r2.isPeriodic()     // Catch:{ all -> 0x0108 }
                if (r6 == 0) goto L_0x0048
            L_0x0026:
                if (r0 == 0) goto L_0x004a
                boolean r4 = r0.isFinished()     // Catch:{ all -> 0x0108 }
                if (r4 != 0) goto L_0x004a
                com.evernote.android.job.util.JobCat r4 = r10.mCat     // Catch:{ all -> 0x0108 }
                java.lang.String r6 = "Job %d is already running, %s"
                r7 = 2
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0108 }
                r8 = 0
                int r9 = r10.mJobId     // Catch:{ all -> 0x0108 }
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0108 }
                r7[r8] = r9     // Catch:{ all -> 0x0108 }
                r8 = 1
                r7[r8] = r2     // Catch:{ all -> 0x0108 }
                r4.d(r6, r7)     // Catch:{ all -> 0x0108 }
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                r2 = r3
                goto L_0x000c
            L_0x0048:
                r1 = r4
                goto L_0x0026
            L_0x004a:
                if (r0 == 0) goto L_0x006b
                if (r1 != 0) goto L_0x006b
                com.evernote.android.job.util.JobCat r4 = r10.mCat     // Catch:{ all -> 0x0108 }
                java.lang.String r6 = "Job %d already finished, %s"
                r7 = 2
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0108 }
                r8 = 0
                int r9 = r10.mJobId     // Catch:{ all -> 0x0108 }
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0108 }
                r7[r8] = r9     // Catch:{ all -> 0x0108 }
                r8 = 1
                r7[r8] = r2     // Catch:{ all -> 0x0108 }
                r4.d(r6, r7)     // Catch:{ all -> 0x0108 }
                r10.cleanUpOrphanedJob(r11)     // Catch:{ all -> 0x0108 }
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                r2 = r3
                goto L_0x000c
            L_0x006b:
                if (r0 == 0) goto L_0x0097
                long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0108 }
                long r8 = r0.getFinishedTimeStamp()     // Catch:{ all -> 0x0108 }
                long r6 = r6 - r8
                r8 = 2000(0x7d0, double:9.88E-321)
                int r4 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
                if (r4 >= 0) goto L_0x0097
                com.evernote.android.job.util.JobCat r4 = r10.mCat     // Catch:{ all -> 0x0108 }
                java.lang.String r6 = "Job %d is periodic and just finished, %s"
                r7 = 2
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0108 }
                r8 = 0
                int r9 = r10.mJobId     // Catch:{ all -> 0x0108 }
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0108 }
                r7[r8] = r9     // Catch:{ all -> 0x0108 }
                r8 = 1
                r7[r8] = r2     // Catch:{ all -> 0x0108 }
                r4.d(r6, r7)     // Catch:{ all -> 0x0108 }
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                r2 = r3
                goto L_0x000c
            L_0x0097:
                if (r2 == 0) goto L_0x00ba
                boolean r4 = r2.isStarted()     // Catch:{ all -> 0x0108 }
                if (r4 == 0) goto L_0x00ba
                com.evernote.android.job.util.JobCat r4 = r10.mCat     // Catch:{ all -> 0x0108 }
                java.lang.String r6 = "Request %d already started, %s"
                r7 = 2
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0108 }
                r8 = 0
                int r9 = r10.mJobId     // Catch:{ all -> 0x0108 }
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0108 }
                r7[r8] = r9     // Catch:{ all -> 0x0108 }
                r8 = 1
                r7[r8] = r2     // Catch:{ all -> 0x0108 }
                r4.d(r6, r7)     // Catch:{ all -> 0x0108 }
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                r2 = r3
                goto L_0x000c
            L_0x00ba:
                if (r2 == 0) goto L_0x00e3
                com.evernote.android.job.JobManager r4 = r10.mJobManager     // Catch:{ all -> 0x0108 }
                com.evernote.android.job.JobExecutor r4 = r4.getJobExecutor()     // Catch:{ all -> 0x0108 }
                boolean r4 = r4.m7510(r2)     // Catch:{ all -> 0x0108 }
                if (r4 == 0) goto L_0x00e3
                com.evernote.android.job.util.JobCat r4 = r10.mCat     // Catch:{ all -> 0x0108 }
                java.lang.String r6 = "Request %d is in the queue to start, %s"
                r7 = 2
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0108 }
                r8 = 0
                int r9 = r10.mJobId     // Catch:{ all -> 0x0108 }
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0108 }
                r7[r8] = r9     // Catch:{ all -> 0x0108 }
                r8 = 1
                r7[r8] = r2     // Catch:{ all -> 0x0108 }
                r4.d(r6, r7)     // Catch:{ all -> 0x0108 }
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                r2 = r3
                goto L_0x000c
            L_0x00e3:
                if (r2 != 0) goto L_0x0100
                com.evernote.android.job.util.JobCat r4 = r10.mCat     // Catch:{ all -> 0x0108 }
                java.lang.String r6 = "Request for ID %d was null"
                r7 = 1
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0108 }
                r8 = 0
                int r9 = r10.mJobId     // Catch:{ all -> 0x0108 }
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0108 }
                r7[r8] = r9     // Catch:{ all -> 0x0108 }
                r4.d(r6, r7)     // Catch:{ all -> 0x0108 }
                r10.cleanUpOrphanedJob(r11)     // Catch:{ all -> 0x0108 }
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                r2 = r3
                goto L_0x000c
            L_0x0100:
                if (r12 == 0) goto L_0x0105
                r10.markStarting(r2)     // Catch:{ all -> 0x0108 }
            L_0x0105:
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                goto L_0x000c
            L_0x0108:
                r3 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0108 }
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.evernote.android.job.JobProxy.Common.getPendingRequest(boolean, boolean):com.evernote.android.job.JobRequest");
        }

        public void markStarting(JobRequest jobRequest) {
            this.mJobManager.getJobExecutor().m7517(jobRequest);
        }
    }

    void cancel(int i);

    boolean isPlatformJobScheduled(JobRequest jobRequest);

    void plantOneOff(JobRequest jobRequest);

    void plantPeriodic(JobRequest jobRequest);

    void plantPeriodicFlexSupport(JobRequest jobRequest);
}
