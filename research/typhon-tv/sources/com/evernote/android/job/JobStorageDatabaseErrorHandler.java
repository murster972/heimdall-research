package com.evernote.android.job;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import com.evernote.android.job.util.JobCat;
import java.io.File;

final class JobStorageDatabaseErrorHandler implements DatabaseErrorHandler {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final JobCat f6582 = new JobCat("DatabaseErrorHandler");

    JobStorageDatabaseErrorHandler() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0055, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0056, code lost:
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        if (r0 != null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0059, code lost:
        r4 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0061, code lost:
        if (r4.hasNext() != false) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0063, code lost:
        m7551((java.lang.String) r4.next().second);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0071, code lost:
        m7551(r6.getPath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0078, code lost:
        throw r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0055 A[ExcHandler: all (r2v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r0 
      PHI: (r0v2 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) = (r0v0 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v3 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v3 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) binds: [B:4:0x002c, B:6:0x0030, B:7:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:4:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCorruption(android.database.sqlite.SQLiteDatabase r6) {
        /*
            r5 = this;
            com.evernote.android.job.util.JobCat r2 = f6582
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Corruption reported by sqlite on database: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r6.getPath()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.e((java.lang.String) r3)
            boolean r2 = r6.isOpen()
            if (r2 != 0) goto L_0x002b
            java.lang.String r2 = r6.getPath()
            r5.m7551((java.lang.String) r2)
        L_0x002a:
            return
        L_0x002b:
            r0 = 0
            java.util.List r0 = r6.getAttachedDbs()     // Catch:{ SQLiteException -> 0x0079, all -> 0x0055 }
        L_0x0030:
            r6.close()     // Catch:{ SQLiteException -> 0x007b, all -> 0x0055 }
        L_0x0033:
            if (r0 == 0) goto L_0x004d
            java.util.Iterator r3 = r0.iterator()
        L_0x0039:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x002a
            java.lang.Object r1 = r3.next()
            android.util.Pair r1 = (android.util.Pair) r1
            java.lang.Object r2 = r1.second
            java.lang.String r2 = (java.lang.String) r2
            r5.m7551((java.lang.String) r2)
            goto L_0x0039
        L_0x004d:
            java.lang.String r2 = r6.getPath()
            r5.m7551((java.lang.String) r2)
            goto L_0x002a
        L_0x0055:
            r2 = move-exception
            r3 = r2
            if (r0 == 0) goto L_0x0071
            java.util.Iterator r4 = r0.iterator()
        L_0x005d:
            boolean r2 = r4.hasNext()
            if (r2 == 0) goto L_0x0078
            java.lang.Object r1 = r4.next()
            android.util.Pair r1 = (android.util.Pair) r1
            java.lang.Object r2 = r1.second
            java.lang.String r2 = (java.lang.String) r2
            r5.m7551((java.lang.String) r2)
            goto L_0x005d
        L_0x0071:
            java.lang.String r2 = r6.getPath()
            r5.m7551((java.lang.String) r2)
        L_0x0078:
            throw r3
        L_0x0079:
            r2 = move-exception
            goto L_0x0030
        L_0x007b:
            r2 = move-exception
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.evernote.android.job.JobStorageDatabaseErrorHandler.onCorruption(android.database.sqlite.SQLiteDatabase):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m7549(Context context, File file) {
        context.deleteDatabase(file.getName());
    }

    /* access modifiers changed from: package-private */
    @TargetApi(16)
    /* renamed from: 龘  reason: contains not printable characters */
    public void m7550(File file) {
        SQLiteDatabase.deleteDatabase(file);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m7551(String str) {
        if (!str.equalsIgnoreCase(":memory:") && str.trim().length() != 0) {
            f6582.e("deleting the database file: " + str);
            try {
                File file = new File(str);
                if (Build.VERSION.SDK_INT >= 16) {
                    m7550(file);
                } else {
                    m7549(JobManager.instance().getContext(), file);
                }
            } catch (Exception e) {
                f6582.w(e, "delete failed: " + e.getMessage(), new Object[0]);
            }
        }
    }
}
