package com.evernote.android.job.v26;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.content.Context;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.v24.JobProxy24;

@TargetApi(26)
public class JobProxy26 extends JobProxy24 {
    private static final String TAG = "JobProxy26";

    public JobProxy26(Context context) {
        super(context, TAG);
    }

    public JobProxy26(Context context, String str) {
        super(context, str);
    }

    /* access modifiers changed from: protected */
    public int convertNetworkType(JobRequest.NetworkType networkType) {
        switch (networkType) {
            case METERED:
                return 4;
            default:
                return super.convertNetworkType(networkType);
        }
    }

    /* access modifiers changed from: protected */
    public JobInfo.Builder createBaseBuilder(JobRequest jobRequest, boolean z) {
        return super.createBaseBuilder(jobRequest, z).setRequiresBatteryNotLow(jobRequest.requiresBatteryNotLow()).setRequiresStorageNotLow(jobRequest.requiresStorageNotLow());
    }

    /* access modifiers changed from: protected */
    public boolean isJobInfoScheduled(JobInfo jobInfo, JobRequest jobRequest) {
        return jobInfo != null && jobInfo.getId() == jobRequest.getJobId();
    }

    /* access modifiers changed from: protected */
    public JobInfo.Builder setTransientBundle(JobRequest jobRequest, JobInfo.Builder builder) {
        return builder.setTransientExtras(jobRequest.getTransientExtras());
    }
}
