package com.evernote.android.job;

import com.evernote.android.job.util.JobCat;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

class JobCreatorHolder {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final JobCat f6558 = new JobCat("JobCreatorHolder");

    /* renamed from: 靐  reason: contains not printable characters */
    private final List<JobCreator> f6559 = new CopyOnWriteArrayList();

    /* renamed from: 靐  reason: contains not printable characters */
    public void m7503(JobCreator jobCreator) {
        this.f6559.remove(jobCreator);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Job m7504(String str) {
        Job job = null;
        boolean z = false;
        for (JobCreator create : this.f6559) {
            z = true;
            job = create.create(str);
            if (job != null) {
                break;
            }
        }
        if (!z) {
            f6558.w("no JobCreator added");
        }
        return job;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7505(JobCreator jobCreator) {
        this.f6559.add(jobCreator);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7506() {
        return this.f6559.isEmpty();
    }
}
