package com.getkeepsafe.relinker;

import com.getkeepsafe.relinker.ReLinker;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ApkLibraryInstaller implements ReLinker.LibraryInstaller {
    private static final int COPY_BUFFER_SIZE = 4096;
    private static final int MAX_TRIES = 5;

    private void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    private long copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        long j = 0;
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009d, code lost:
        if (r11 == null) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a6, code lost:
        throw new com.getkeepsafe.relinker.MissingLibraryException(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ba, code lost:
        throw new com.getkeepsafe.relinker.MissingLibraryException(r26);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void installLibrary(android.content.Context r24, java.lang.String[] r25, java.lang.String r26, java.io.File r27, com.getkeepsafe.relinker.ReLinkerInstance r28) {
        /*
            r23 = this;
            r15 = 0
            android.content.pm.ApplicationInfo r5 = r24.getApplicationInfo()     // Catch:{ all -> 0x00a7 }
            r13 = 0
            r14 = r13
        L_0x0007:
            int r13 = r14 + 1
            r19 = 5
            r0 = r19
            if (r14 >= r0) goto L_0x0021
            java.util.zip.ZipFile r18 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x0033 }
            java.io.File r19 = new java.io.File     // Catch:{ IOException -> 0x0033 }
            java.lang.String r0 = r5.sourceDir     // Catch:{ IOException -> 0x0033 }
            r20 = r0
            r19.<init>(r20)     // Catch:{ IOException -> 0x0033 }
            r20 = 1
            r18.<init>(r19, r20)     // Catch:{ IOException -> 0x0033 }
            r15 = r18
        L_0x0021:
            if (r15 != 0) goto L_0x0036
            java.lang.String r19 = "FATAL! Couldn't find application APK!"
            r0 = r28
            r1 = r19
            r0.log((java.lang.String) r1)     // Catch:{ all -> 0x00a7 }
            if (r15 == 0) goto L_0x0032
            r15.close()     // Catch:{ IOException -> 0x0188 }
        L_0x0032:
            return
        L_0x0033:
            r19 = move-exception
            r14 = r13
            goto L_0x0007
        L_0x0036:
            r13 = 0
            r14 = r13
        L_0x0038:
            int r13 = r14 + 1
            r19 = 5
            r0 = r19
            if (r14 >= r0) goto L_0x0174
            r11 = 0
            r12 = 0
            r0 = r25
            int r0 = r0.length     // Catch:{ all -> 0x00a7 }
            r20 = r0
            r19 = 0
        L_0x0049:
            r0 = r19
            r1 = r20
            if (r0 >= r1) goto L_0x0081
            r4 = r25[r19]     // Catch:{ all -> 0x00a7 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a7 }
            r21.<init>()     // Catch:{ all -> 0x00a7 }
            java.lang.String r22 = "lib"
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ all -> 0x00a7 }
            char r22 = java.io.File.separatorChar     // Catch:{ all -> 0x00a7 }
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ all -> 0x00a7 }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r4)     // Catch:{ all -> 0x00a7 }
            char r22 = java.io.File.separatorChar     // Catch:{ all -> 0x00a7 }
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ all -> 0x00a7 }
            r0 = r21
            r1 = r26
            java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ all -> 0x00a7 }
            java.lang.String r11 = r21.toString()     // Catch:{ all -> 0x00a7 }
            java.util.zip.ZipEntry r12 = r15.getEntry(r11)     // Catch:{ all -> 0x00a7 }
            if (r12 == 0) goto L_0x00ae
        L_0x0081:
            if (r11 == 0) goto L_0x009b
            java.lang.String r19 = "Looking for %s in APK..."
            r20 = 1
            r0 = r20
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x00a7 }
            r20 = r0
            r21 = 0
            r20[r21] = r11     // Catch:{ all -> 0x00a7 }
            r0 = r28
            r1 = r19
            r2 = r20
            r0.log(r1, r2)     // Catch:{ all -> 0x00a7 }
        L_0x009b:
            if (r12 != 0) goto L_0x00bb
            if (r11 == 0) goto L_0x00b1
            com.getkeepsafe.relinker.MissingLibraryException r19 = new com.getkeepsafe.relinker.MissingLibraryException     // Catch:{ all -> 0x00a7 }
            r0 = r19
            r0.<init>(r11)     // Catch:{ all -> 0x00a7 }
            throw r19     // Catch:{ all -> 0x00a7 }
        L_0x00a7:
            r19 = move-exception
            if (r15 == 0) goto L_0x00ad
            r15.close()     // Catch:{ IOException -> 0x018b }
        L_0x00ad:
            throw r19
        L_0x00ae:
            int r19 = r19 + 1
            goto L_0x0049
        L_0x00b1:
            com.getkeepsafe.relinker.MissingLibraryException r19 = new com.getkeepsafe.relinker.MissingLibraryException     // Catch:{ all -> 0x00a7 }
            r0 = r19
            r1 = r26
            r0.<init>(r1)     // Catch:{ all -> 0x00a7 }
            throw r19     // Catch:{ all -> 0x00a7 }
        L_0x00bb:
            java.lang.String r19 = "Found %s! Extracting..."
            r20 = 1
            r0 = r20
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x00a7 }
            r20 = r0
            r21 = 0
            r20[r21] = r11     // Catch:{ all -> 0x00a7 }
            r0 = r28
            r1 = r19
            r2 = r20
            r0.log(r1, r2)     // Catch:{ all -> 0x00a7 }
            boolean r19 = r27.exists()     // Catch:{ IOException -> 0x00e2 }
            if (r19 != 0) goto L_0x00e6
            boolean r19 = r27.createNewFile()     // Catch:{ IOException -> 0x00e2 }
            if (r19 != 0) goto L_0x00e6
            r14 = r13
            goto L_0x0038
        L_0x00e2:
            r9 = move-exception
            r14 = r13
            goto L_0x0038
        L_0x00e6:
            r10 = 0
            r7 = 0
            java.io.InputStream r10 = r15.getInputStream(r12)     // Catch:{ FileNotFoundException -> 0x014c, IOException -> 0x015a, all -> 0x0168 }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x014c, IOException -> 0x015a, all -> 0x0168 }
            r0 = r27
            r8.<init>(r0)     // Catch:{ FileNotFoundException -> 0x014c, IOException -> 0x015a, all -> 0x0168 }
            r0 = r23
            long r16 = r0.copy(r10, r8)     // Catch:{ FileNotFoundException -> 0x0194, IOException -> 0x0191, all -> 0x018e }
            java.io.FileDescriptor r19 = r8.getFD()     // Catch:{ FileNotFoundException -> 0x0194, IOException -> 0x0191, all -> 0x018e }
            r19.sync()     // Catch:{ FileNotFoundException -> 0x0194, IOException -> 0x0191, all -> 0x018e }
            long r20 = r27.length()     // Catch:{ FileNotFoundException -> 0x0194, IOException -> 0x0191, all -> 0x018e }
            int r19 = (r16 > r20 ? 1 : (r16 == r20 ? 0 : -1))
            if (r19 == 0) goto L_0x0115
            r0 = r23
            r0.closeSilently(r10)     // Catch:{ all -> 0x00a7 }
            r0 = r23
            r0.closeSilently(r8)     // Catch:{ all -> 0x00a7 }
            r14 = r13
            goto L_0x0038
        L_0x0115:
            r0 = r23
            r0.closeSilently(r10)     // Catch:{ all -> 0x00a7 }
            r0 = r23
            r0.closeSilently(r8)     // Catch:{ all -> 0x00a7 }
            r19 = 1
            r20 = 0
            r0 = r27
            r1 = r19
            r2 = r20
            r0.setReadable(r1, r2)     // Catch:{ all -> 0x00a7 }
            r19 = 1
            r20 = 0
            r0 = r27
            r1 = r19
            r2 = r20
            r0.setExecutable(r1, r2)     // Catch:{ all -> 0x00a7 }
            r19 = 1
            r0 = r27
            r1 = r19
            r0.setWritable(r1)     // Catch:{ all -> 0x00a7 }
            if (r15 == 0) goto L_0x0032
            r15.close()     // Catch:{ IOException -> 0x0149 }
            goto L_0x0032
        L_0x0149:
            r19 = move-exception
            goto L_0x0032
        L_0x014c:
            r6 = move-exception
        L_0x014d:
            r0 = r23
            r0.closeSilently(r10)     // Catch:{ all -> 0x00a7 }
            r0 = r23
            r0.closeSilently(r7)     // Catch:{ all -> 0x00a7 }
            r14 = r13
            goto L_0x0038
        L_0x015a:
            r6 = move-exception
        L_0x015b:
            r0 = r23
            r0.closeSilently(r10)     // Catch:{ all -> 0x00a7 }
            r0 = r23
            r0.closeSilently(r7)     // Catch:{ all -> 0x00a7 }
            r14 = r13
            goto L_0x0038
        L_0x0168:
            r19 = move-exception
        L_0x0169:
            r0 = r23
            r0.closeSilently(r10)     // Catch:{ all -> 0x00a7 }
            r0 = r23
            r0.closeSilently(r7)     // Catch:{ all -> 0x00a7 }
            throw r19     // Catch:{ all -> 0x00a7 }
        L_0x0174:
            java.lang.String r19 = "FATAL! Couldn't extract the library from the APK!"
            r0 = r28
            r1 = r19
            r0.log((java.lang.String) r1)     // Catch:{ all -> 0x00a7 }
            if (r15 == 0) goto L_0x0032
            r15.close()     // Catch:{ IOException -> 0x0185 }
            goto L_0x0032
        L_0x0185:
            r19 = move-exception
            goto L_0x0032
        L_0x0188:
            r19 = move-exception
            goto L_0x0032
        L_0x018b:
            r20 = move-exception
            goto L_0x00ad
        L_0x018e:
            r19 = move-exception
            r7 = r8
            goto L_0x0169
        L_0x0191:
            r6 = move-exception
            r7 = r8
            goto L_0x015b
        L_0x0194:
            r6 = move-exception
            r7 = r8
            goto L_0x014d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.getkeepsafe.relinker.ApkLibraryInstaller.installLibrary(android.content.Context, java.lang.String[], java.lang.String, java.io.File, com.getkeepsafe.relinker.ReLinkerInstance):void");
    }
}
