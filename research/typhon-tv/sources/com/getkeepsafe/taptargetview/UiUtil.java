package com.getkeepsafe.taptargetview;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

class UiUtil {
    UiUtil() {
    }

    static int dp(Context context, int i) {
        return (int) TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }

    static int setAlpha(int i, float f) {
        if (f > 1.0f) {
            f = 1.0f;
        } else if (f <= 0.0f) {
            f = 0.0f;
        }
        return (((int) (((float) (i >>> 24)) * f)) << 24) | (16777215 & i);
    }

    static int sp(Context context, int i) {
        return (int) TypedValue.applyDimension(2, (float) i, context.getResources().getDisplayMetrics());
    }

    static int themeIntAttr(Context context, String str) {
        Resources.Theme theme = context.getTheme();
        if (theme == null) {
            return -1;
        }
        TypedValue typedValue = new TypedValue();
        int identifier = context.getResources().getIdentifier(str, "attr", context.getPackageName());
        if (identifier == 0) {
            return -1;
        }
        theme.resolveAttribute(identifier, typedValue, true);
        return typedValue.data;
    }
}
