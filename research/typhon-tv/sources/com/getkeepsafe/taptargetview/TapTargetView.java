package com.getkeepsafe.taptargetview;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.internal.view.SupportMenu;
import android.text.DynamicLayout;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.ViewOutlineProvider;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.getkeepsafe.taptargetview.FloatValueAnimatorBuilder;
import org.apache.commons.lang3.StringUtils;

@SuppressLint({"ViewConstructor"})
public class TapTargetView extends View {
    final int CIRCLE_PADDING;
    final int GUTTER_DIM;
    final int SHADOW_DIM;
    final int SHADOW_JITTER_DIM;
    final int TARGET_PADDING;
    final int TARGET_PULSE_RADIUS;
    final int TARGET_RADIUS;
    final int TEXT_MAX_WIDTH;
    final int TEXT_PADDING;
    final int TEXT_POSITIONING_BIAS;
    final int TEXT_SPACING;
    private ValueAnimator[] animators = {this.expandAnimation, this.pulseAnimation, this.dismissConfirmAnimation, this.dismissAnimation};
    int bottomBoundary;
    final ViewGroup boundingParent;
    int calculatedOuterCircleRadius;
    boolean cancelable;
    boolean debug;
    DynamicLayout debugLayout;
    Paint debugPaint;
    SpannableStringBuilder debugStringBuilder;
    TextPaint debugTextPaint;
    CharSequence description;
    StaticLayout descriptionLayout;
    final TextPaint descriptionPaint;
    int dimColor;
    final ValueAnimator dismissAnimation = new FloatValueAnimatorBuilder(true).duration(250).interpolator(new AccelerateDecelerateInterpolator()).onUpdate(new FloatValueAnimatorBuilder.UpdateListener() {
        public void onUpdate(float f) {
            TapTargetView.this.expandContractUpdateListener.onUpdate(f);
        }
    }).onEnd(new FloatValueAnimatorBuilder.EndListener() {
        public void onEnd() {
            TapTargetView.this.onDismiss(true);
            ViewUtil.removeView(TapTargetView.this.parent, TapTargetView.this);
        }
    }).build();
    private final ValueAnimator dismissConfirmAnimation = new FloatValueAnimatorBuilder().duration(250).interpolator(new AccelerateDecelerateInterpolator()).onUpdate(new FloatValueAnimatorBuilder.UpdateListener() {
        public void onUpdate(float f) {
            float min = Math.min(1.0f, 2.0f * f);
            TapTargetView.this.outerCircleRadius = ((float) TapTargetView.this.calculatedOuterCircleRadius) * ((0.2f * min) + 1.0f);
            TapTargetView.this.outerCircleAlpha = (int) ((1.0f - min) * TapTargetView.this.target.outerCircleAlpha * 255.0f);
            TapTargetView.this.outerCirclePath.reset();
            TapTargetView.this.outerCirclePath.addCircle((float) TapTargetView.this.outerCircleCenter[0], (float) TapTargetView.this.outerCircleCenter[1], TapTargetView.this.outerCircleRadius, Path.Direction.CW);
            TapTargetView.this.targetCircleRadius = (1.0f - f) * ((float) TapTargetView.this.TARGET_RADIUS);
            TapTargetView.this.targetCircleAlpha = (int) ((1.0f - f) * 255.0f);
            TapTargetView.this.targetCirclePulseRadius = (1.0f + f) * ((float) TapTargetView.this.TARGET_RADIUS);
            TapTargetView.this.targetCirclePulseAlpha = (int) ((1.0f - f) * ((float) TapTargetView.this.targetCirclePulseAlpha));
            TapTargetView.this.textAlpha = (int) ((1.0f - min) * 255.0f);
            TapTargetView.this.calculateDrawingBounds();
            TapTargetView.this.invalidateViewAndOutline(TapTargetView.this.drawingBounds);
        }
    }).onEnd(new FloatValueAnimatorBuilder.EndListener() {
        public void onEnd() {
            TapTargetView.this.onDismiss(true);
            ViewUtil.removeView(TapTargetView.this.parent, TapTargetView.this);
        }
    }).build();
    Rect drawingBounds;
    final ValueAnimator expandAnimation = new FloatValueAnimatorBuilder().duration(250).delayBy(250).interpolator(new AccelerateDecelerateInterpolator()).onUpdate(new FloatValueAnimatorBuilder.UpdateListener() {
        public void onUpdate(float f) {
            TapTargetView.this.expandContractUpdateListener.onUpdate(f);
        }
    }).onEnd(new FloatValueAnimatorBuilder.EndListener() {
        public void onEnd() {
            TapTargetView.this.pulseAnimation.start();
        }
    }).build();
    final FloatValueAnimatorBuilder.UpdateListener expandContractUpdateListener = new FloatValueAnimatorBuilder.UpdateListener() {
        public void onUpdate(float f) {
            float f2 = ((float) TapTargetView.this.calculatedOuterCircleRadius) * f;
            boolean z = f2 > TapTargetView.this.outerCircleRadius;
            if (!z) {
                TapTargetView.this.calculateDrawingBounds();
            }
            float f3 = TapTargetView.this.target.outerCircleAlpha * 255.0f;
            TapTargetView.this.outerCircleRadius = f2;
            TapTargetView.this.outerCircleAlpha = (int) Math.min(f3, f * 1.5f * f3);
            TapTargetView.this.outerCirclePath.reset();
            TapTargetView.this.outerCirclePath.addCircle((float) TapTargetView.this.outerCircleCenter[0], (float) TapTargetView.this.outerCircleCenter[1], TapTargetView.this.outerCircleRadius, Path.Direction.CW);
            TapTargetView.this.targetCircleAlpha = (int) Math.min(255.0f, f * 1.5f * 255.0f);
            if (z) {
                TapTargetView.this.targetCircleRadius = ((float) TapTargetView.this.TARGET_RADIUS) * Math.min(1.0f, f * 1.5f);
            } else {
                TapTargetView.this.targetCircleRadius = ((float) TapTargetView.this.TARGET_RADIUS) * f;
                TapTargetView.this.targetCirclePulseRadius *= f;
            }
            TapTargetView.this.textAlpha = (int) (TapTargetView.this.delayedLerp(f, 0.7f) * 255.0f);
            if (z) {
                TapTargetView.this.calculateDrawingBounds();
            }
            TapTargetView.this.invalidateViewAndOutline(TapTargetView.this.drawingBounds);
        }
    };
    private final ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener;
    boolean isDark;
    private boolean isDismissed = false;
    /* access modifiers changed from: private */
    public boolean isDismissing = false;
    /* access modifiers changed from: private */
    public boolean isInteractable = true;
    float lastTouchX;
    float lastTouchY;
    Listener listener;
    int outerCircleAlpha;
    int[] outerCircleCenter;
    final Paint outerCirclePaint;
    Path outerCirclePath;
    float outerCircleRadius;
    final Paint outerCircleShadowPaint;
    ViewOutlineProvider outlineProvider;
    final ViewManager parent;
    final ValueAnimator pulseAnimation = new FloatValueAnimatorBuilder().duration(1000).repeat(-1).interpolator(new AccelerateDecelerateInterpolator()).onUpdate(new FloatValueAnimatorBuilder.UpdateListener() {
        public void onUpdate(float f) {
            float delayedLerp = TapTargetView.this.delayedLerp(f, 0.5f);
            TapTargetView.this.targetCirclePulseRadius = (1.0f + delayedLerp) * ((float) TapTargetView.this.TARGET_RADIUS);
            TapTargetView.this.targetCirclePulseAlpha = (int) ((1.0f - delayedLerp) * 255.0f);
            TapTargetView.this.targetCircleRadius = ((float) TapTargetView.this.TARGET_RADIUS) + (TapTargetView.this.halfwayLerp(f) * ((float) TapTargetView.this.TARGET_PULSE_RADIUS));
            if (TapTargetView.this.outerCircleRadius != ((float) TapTargetView.this.calculatedOuterCircleRadius)) {
                TapTargetView.this.outerCircleRadius = (float) TapTargetView.this.calculatedOuterCircleRadius;
            }
            TapTargetView.this.calculateDrawingBounds();
            TapTargetView.this.invalidateViewAndOutline(TapTargetView.this.drawingBounds);
        }
    }).build();
    boolean shouldDrawShadow;
    boolean shouldTintTarget;
    final TapTarget target;
    final Rect targetBounds;
    int targetCircleAlpha;
    final Paint targetCirclePaint;
    int targetCirclePulseAlpha;
    final Paint targetCirclePulsePaint;
    float targetCirclePulseRadius;
    float targetCircleRadius;
    int textAlpha;
    Rect textBounds;
    Bitmap tintedTarget;
    CharSequence title;
    StaticLayout titleLayout;
    final TextPaint titlePaint;
    int topBoundary;
    boolean visible;

    public static class Listener {
        public void onOuterCircleClick(TapTargetView tapTargetView) {
        }

        public void onTargetCancel(TapTargetView tapTargetView) {
            tapTargetView.dismiss(false);
        }

        public void onTargetClick(TapTargetView tapTargetView) {
            tapTargetView.dismiss(true);
        }

        public void onTargetDismissed(TapTargetView tapTargetView, boolean z) {
        }

        public void onTargetLongClick(TapTargetView tapTargetView) {
            onTargetClick(tapTargetView);
        }
    }

    public TapTargetView(final Context context, ViewManager viewManager, final ViewGroup viewGroup, final TapTarget tapTarget, Listener listener2) {
        super(context);
        if (tapTarget == null) {
            throw new IllegalArgumentException("Target cannot be null");
        }
        this.target = tapTarget;
        this.parent = viewManager;
        this.boundingParent = viewGroup;
        this.listener = listener2 == null ? new Listener() : listener2;
        this.title = tapTarget.title;
        this.description = tapTarget.description;
        this.TARGET_PADDING = UiUtil.dp(context, 20);
        this.CIRCLE_PADDING = UiUtil.dp(context, 40);
        this.TARGET_RADIUS = UiUtil.dp(context, tapTarget.targetRadius);
        this.TEXT_PADDING = UiUtil.dp(context, 40);
        this.TEXT_SPACING = UiUtil.dp(context, 8);
        this.TEXT_MAX_WIDTH = UiUtil.dp(context, 360);
        this.TEXT_POSITIONING_BIAS = UiUtil.dp(context, 20);
        this.GUTTER_DIM = UiUtil.dp(context, 88);
        this.SHADOW_DIM = UiUtil.dp(context, 8);
        this.SHADOW_JITTER_DIM = UiUtil.dp(context, 1);
        this.TARGET_PULSE_RADIUS = (int) (0.1f * ((float) this.TARGET_RADIUS));
        this.outerCirclePath = new Path();
        this.targetBounds = new Rect();
        this.drawingBounds = new Rect();
        this.titlePaint = new TextPaint();
        this.titlePaint.setTextSize((float) tapTarget.titleTextSizePx(context));
        this.titlePaint.setTypeface(Typeface.create("sans-serif-medium", 0));
        this.titlePaint.setAntiAlias(true);
        this.descriptionPaint = new TextPaint();
        this.descriptionPaint.setTextSize((float) tapTarget.descriptionTextSizePx(context));
        this.descriptionPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 0));
        this.descriptionPaint.setAntiAlias(true);
        this.descriptionPaint.setAlpha(137);
        this.outerCirclePaint = new Paint();
        this.outerCirclePaint.setAntiAlias(true);
        this.outerCirclePaint.setAlpha((int) (tapTarget.outerCircleAlpha * 255.0f));
        this.outerCircleShadowPaint = new Paint();
        this.outerCircleShadowPaint.setAntiAlias(true);
        this.outerCircleShadowPaint.setAlpha(50);
        this.outerCircleShadowPaint.setStyle(Paint.Style.STROKE);
        this.outerCircleShadowPaint.setStrokeWidth((float) this.SHADOW_JITTER_DIM);
        this.outerCircleShadowPaint.setColor(-16777216);
        this.targetCirclePaint = new Paint();
        this.targetCirclePaint.setAntiAlias(true);
        this.targetCirclePulsePaint = new Paint();
        this.targetCirclePulsePaint.setAntiAlias(true);
        applyTargetOptions(context);
        this.globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (!TapTargetView.this.isDismissing) {
                    TapTargetView.this.updateTextLayouts();
                    tapTarget.onReady(new Runnable() {
                        public void run() {
                            int[] iArr = new int[2];
                            TapTargetView.this.targetBounds.set(tapTarget.bounds());
                            TapTargetView.this.getLocationOnScreen(iArr);
                            TapTargetView.this.targetBounds.offset(-iArr[0], -iArr[1]);
                            if (viewGroup != null) {
                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                                Rect rect = new Rect();
                                viewGroup.getWindowVisibleDisplayFrame(rect);
                                TapTargetView.this.topBoundary = Math.max(0, rect.top);
                                TapTargetView.this.bottomBoundary = Math.min(rect.bottom, displayMetrics.heightPixels);
                            }
                            TapTargetView.this.drawTintedTarget();
                            TapTargetView.this.requestFocus();
                            TapTargetView.this.calculateDimensions();
                            if (!TapTargetView.this.visible) {
                                TapTargetView.this.expandAnimation.start();
                                TapTargetView.this.visible = true;
                            }
                        }
                    });
                }
            }
        };
        getViewTreeObserver().addOnGlobalLayoutListener(this.globalLayoutListener);
        setFocusableInTouchMode(true);
        setClickable(true);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (TapTargetView.this.listener != null && TapTargetView.this.outerCircleCenter != null && TapTargetView.this.isInteractable) {
                    boolean z = TapTargetView.this.distance(TapTargetView.this.targetBounds.centerX(), TapTargetView.this.targetBounds.centerY(), (int) TapTargetView.this.lastTouchX, (int) TapTargetView.this.lastTouchY) <= ((double) TapTargetView.this.targetCircleRadius);
                    boolean z2 = TapTargetView.this.distance(TapTargetView.this.outerCircleCenter[0], TapTargetView.this.outerCircleCenter[1], (int) TapTargetView.this.lastTouchX, (int) TapTargetView.this.lastTouchY) <= ((double) TapTargetView.this.outerCircleRadius);
                    if (z) {
                        boolean unused = TapTargetView.this.isInteractable = false;
                        TapTargetView.this.listener.onTargetClick(TapTargetView.this);
                    } else if (z2) {
                        TapTargetView.this.listener.onOuterCircleClick(TapTargetView.this);
                    } else if (TapTargetView.this.cancelable) {
                        boolean unused2 = TapTargetView.this.isInteractable = false;
                        TapTargetView.this.listener.onTargetCancel(TapTargetView.this);
                    }
                }
            }
        });
        setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                if (TapTargetView.this.listener == null || !TapTargetView.this.targetBounds.contains((int) TapTargetView.this.lastTouchX, (int) TapTargetView.this.lastTouchY)) {
                    return false;
                }
                TapTargetView.this.listener.onTargetLongClick(TapTargetView.this);
                return true;
            }
        });
    }

    public static TapTargetView showFor(Activity activity, TapTarget tapTarget) {
        return showFor(activity, tapTarget, (Listener) null);
    }

    public static TapTargetView showFor(Activity activity, TapTarget tapTarget, Listener listener2) {
        if (activity == null) {
            throw new IllegalArgumentException("Activity is null");
        }
        ViewGroup viewGroup = (ViewGroup) activity.getWindow().getDecorView();
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        TapTargetView tapTargetView = new TapTargetView(activity, viewGroup, (ViewGroup) viewGroup.findViewById(16908290), tapTarget, listener2);
        viewGroup.addView(tapTargetView, layoutParams);
        return tapTargetView;
    }

    public static TapTargetView showFor(Dialog dialog, TapTarget tapTarget) {
        return showFor(dialog, tapTarget, (Listener) null);
    }

    public static TapTargetView showFor(Dialog dialog, TapTarget tapTarget, Listener listener2) {
        if (dialog == null) {
            throw new IllegalArgumentException("Dialog is null");
        }
        Context context = dialog.getContext();
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.type = 2;
        layoutParams.format = 1;
        layoutParams.flags = 0;
        layoutParams.gravity = 8388659;
        layoutParams.x = 0;
        layoutParams.y = 0;
        layoutParams.width = -1;
        layoutParams.height = -1;
        TapTargetView tapTargetView = new TapTargetView(context, windowManager, (ViewGroup) null, tapTarget, listener2);
        windowManager.addView(tapTargetView, layoutParams);
        return tapTargetView;
    }

    /* access modifiers changed from: protected */
    public void applyTargetOptions(Context context) {
        boolean z = true;
        int i = -16777216;
        this.shouldTintTarget = this.target.tintTarget;
        this.shouldDrawShadow = this.target.drawShadow;
        this.cancelable = this.target.cancelable;
        if (this.shouldDrawShadow && Build.VERSION.SDK_INT >= 21 && !this.target.transparentTarget) {
            this.outlineProvider = new ViewOutlineProvider() {
                @TargetApi(21)
                public void getOutline(View view, Outline outline) {
                    if (TapTargetView.this.outerCircleCenter != null) {
                        outline.setOval((int) (((float) TapTargetView.this.outerCircleCenter[0]) - TapTargetView.this.outerCircleRadius), (int) (((float) TapTargetView.this.outerCircleCenter[1]) - TapTargetView.this.outerCircleRadius), (int) (((float) TapTargetView.this.outerCircleCenter[0]) + TapTargetView.this.outerCircleRadius), (int) (((float) TapTargetView.this.outerCircleCenter[1]) + TapTargetView.this.outerCircleRadius));
                        outline.setAlpha(((float) TapTargetView.this.outerCircleAlpha) / 255.0f);
                        if (Build.VERSION.SDK_INT >= 22) {
                            outline.offset(0, TapTargetView.this.SHADOW_DIM);
                        }
                    }
                }
            };
            setOutlineProvider(this.outlineProvider);
            setElevation((float) this.SHADOW_DIM);
        }
        if (!this.shouldDrawShadow || this.outlineProvider != null || Build.VERSION.SDK_INT >= 18) {
            setLayerType(2, (Paint) null);
        } else {
            setLayerType(1, (Paint) null);
        }
        Resources.Theme theme = context.getTheme();
        if (UiUtil.themeIntAttr(context, "isLightTheme") != 0) {
            z = false;
        }
        this.isDark = z;
        Integer outerCircleColorInt = this.target.outerCircleColorInt(context);
        if (outerCircleColorInt != null) {
            this.outerCirclePaint.setColor(outerCircleColorInt.intValue());
        } else if (theme != null) {
            this.outerCirclePaint.setColor(UiUtil.themeIntAttr(context, "colorPrimary"));
        } else {
            this.outerCirclePaint.setColor(-1);
        }
        Integer targetCircleColorInt = this.target.targetCircleColorInt(context);
        if (targetCircleColorInt != null) {
            this.targetCirclePaint.setColor(targetCircleColorInt.intValue());
        } else {
            this.targetCirclePaint.setColor(this.isDark ? -16777216 : -1);
        }
        if (this.target.transparentTarget) {
            this.targetCirclePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        }
        this.targetCirclePulsePaint.setColor(this.targetCirclePaint.getColor());
        Integer dimColorInt = this.target.dimColorInt(context);
        if (dimColorInt != null) {
            this.dimColor = UiUtil.setAlpha(dimColorInt.intValue(), 0.3f);
        } else {
            this.dimColor = -1;
        }
        Integer titleTextColorInt = this.target.titleTextColorInt(context);
        if (titleTextColorInt != null) {
            this.titlePaint.setColor(titleTextColorInt.intValue());
        } else {
            TextPaint textPaint = this.titlePaint;
            if (!this.isDark) {
                i = -1;
            }
            textPaint.setColor(i);
        }
        Integer descriptionTextColorInt = this.target.descriptionTextColorInt(context);
        if (descriptionTextColorInt != null) {
            this.descriptionPaint.setColor(descriptionTextColorInt.intValue());
        } else {
            this.descriptionPaint.setColor(this.titlePaint.getColor());
        }
        if (this.target.titleTypeface != null) {
            this.titlePaint.setTypeface(this.target.titleTypeface);
        }
        if (this.target.descriptionTypeface != null) {
            this.descriptionPaint.setTypeface(this.target.descriptionTypeface);
        }
    }

    /* access modifiers changed from: package-private */
    public void calculateDimensions() {
        this.textBounds = getTextBounds();
        this.outerCircleCenter = getOuterCircleCenterPoint();
        this.calculatedOuterCircleRadius = getOuterCircleRadius(this.outerCircleCenter[0], this.outerCircleCenter[1], this.textBounds, this.targetBounds);
    }

    /* access modifiers changed from: package-private */
    public void calculateDrawingBounds() {
        if (this.outerCircleCenter != null) {
            this.drawingBounds.left = (int) Math.max(0.0f, ((float) this.outerCircleCenter[0]) - this.outerCircleRadius);
            this.drawingBounds.top = (int) Math.min(0.0f, ((float) this.outerCircleCenter[1]) - this.outerCircleRadius);
            this.drawingBounds.right = (int) Math.min((float) getWidth(), ((float) this.outerCircleCenter[0]) + this.outerCircleRadius + ((float) this.CIRCLE_PADDING));
            this.drawingBounds.bottom = (int) Math.min((float) getHeight(), ((float) this.outerCircleCenter[1]) + this.outerCircleRadius + ((float) this.CIRCLE_PADDING));
        }
    }

    /* access modifiers changed from: package-private */
    public float delayedLerp(float f, float f2) {
        if (f < f2) {
            return 0.0f;
        }
        return (f - f2) / (1.0f - f2);
    }

    public void dismiss(boolean z) {
        this.isDismissing = true;
        this.pulseAnimation.cancel();
        this.expandAnimation.cancel();
        if (z) {
            this.dismissConfirmAnimation.start();
        } else {
            this.dismissAnimation.start();
        }
    }

    /* access modifiers changed from: package-private */
    public double distance(int i, int i2, int i3, int i4) {
        return Math.sqrt(Math.pow((double) (i3 - i), 2.0d) + Math.pow((double) (i4 - i2), 2.0d));
    }

    /* access modifiers changed from: package-private */
    public void drawDebugInformation(Canvas canvas) {
        if (this.debugPaint == null) {
            this.debugPaint = new Paint();
            this.debugPaint.setARGB(255, 255, 0, 0);
            this.debugPaint.setStyle(Paint.Style.STROKE);
            this.debugPaint.setStrokeWidth((float) UiUtil.dp(getContext(), 1));
        }
        if (this.debugTextPaint == null) {
            this.debugTextPaint = new TextPaint();
            this.debugTextPaint.setColor(SupportMenu.CATEGORY_MASK);
            this.debugTextPaint.setTextSize((float) UiUtil.sp(getContext(), 16));
        }
        this.debugPaint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(this.textBounds, this.debugPaint);
        canvas.drawRect(this.targetBounds, this.debugPaint);
        canvas.drawCircle((float) this.outerCircleCenter[0], (float) this.outerCircleCenter[1], 10.0f, this.debugPaint);
        canvas.drawCircle((float) this.outerCircleCenter[0], (float) this.outerCircleCenter[1], (float) (this.calculatedOuterCircleRadius - this.CIRCLE_PADDING), this.debugPaint);
        canvas.drawCircle((float) this.targetBounds.centerX(), (float) this.targetBounds.centerY(), (float) (this.TARGET_RADIUS + this.TARGET_PADDING), this.debugPaint);
        this.debugPaint.setStyle(Paint.Style.FILL);
        String str = "Text bounds: " + this.textBounds.toShortString() + "\nTarget bounds: " + this.targetBounds.toShortString() + "\nCenter: " + this.outerCircleCenter[0] + StringUtils.SPACE + this.outerCircleCenter[1] + "\nView size: " + getWidth() + StringUtils.SPACE + getHeight() + "\nTarget bounds: " + this.targetBounds.toShortString();
        if (this.debugStringBuilder == null) {
            this.debugStringBuilder = new SpannableStringBuilder(str);
        } else {
            this.debugStringBuilder.clear();
            this.debugStringBuilder.append(str);
        }
        if (this.debugLayout == null) {
            this.debugLayout = new DynamicLayout(str, this.debugTextPaint, getWidth(), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        }
        int save = canvas.save();
        this.debugPaint.setARGB(220, 0, 0, 0);
        canvas.translate(0.0f, (float) this.topBoundary);
        canvas.drawRect(0.0f, 0.0f, (float) this.debugLayout.getWidth(), (float) this.debugLayout.getHeight(), this.debugPaint);
        this.debugPaint.setARGB(255, 255, 0, 0);
        this.debugLayout.draw(canvas);
        canvas.restoreToCount(save);
    }

    /* access modifiers changed from: package-private */
    public void drawJitteredShadow(Canvas canvas) {
        float f = 0.2f * ((float) this.outerCircleAlpha);
        this.outerCircleShadowPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.outerCircleShadowPaint.setAlpha((int) f);
        canvas.drawCircle((float) this.outerCircleCenter[0], (float) (this.outerCircleCenter[1] + this.SHADOW_DIM), this.outerCircleRadius, this.outerCircleShadowPaint);
        this.outerCircleShadowPaint.setStyle(Paint.Style.STROKE);
        for (int i = 6; i > 0; i--) {
            this.outerCircleShadowPaint.setAlpha((int) ((((float) i) / 7.0f) * f));
            canvas.drawCircle((float) this.outerCircleCenter[0], (float) (this.outerCircleCenter[1] + this.SHADOW_DIM), this.outerCircleRadius + ((float) ((7 - i) * this.SHADOW_JITTER_DIM)), this.outerCircleShadowPaint);
        }
    }

    /* access modifiers changed from: package-private */
    public void drawTintedTarget() {
        Drawable drawable = this.target.icon;
        if (!this.shouldTintTarget || drawable == null) {
            this.tintedTarget = null;
        } else if (this.tintedTarget == null) {
            this.tintedTarget = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(this.tintedTarget);
            drawable.setColorFilter(new PorterDuffColorFilter(this.outerCirclePaint.getColor(), PorterDuff.Mode.SRC_ATOP));
            drawable.draw(canvas);
            drawable.setColorFilter((ColorFilter) null);
        }
    }

    /* access modifiers changed from: package-private */
    public int[] getOuterCircleCenterPoint() {
        if (inGutter(this.targetBounds.centerY())) {
            return new int[]{this.targetBounds.centerX(), this.targetBounds.centerY()};
        }
        int max = (Math.max(this.targetBounds.width(), this.targetBounds.height()) / 2) + this.TARGET_PADDING;
        int totalTextHeight = getTotalTextHeight();
        boolean z = ((this.targetBounds.centerY() - this.TARGET_RADIUS) - this.TARGET_PADDING) - totalTextHeight > 0;
        int min = Math.min(this.textBounds.left, this.targetBounds.left - max);
        int max2 = Math.max(this.textBounds.right, this.targetBounds.right + max);
        int height = this.titleLayout == null ? 0 : this.titleLayout.getHeight();
        return new int[]{(min + max2) / 2, z ? (((this.targetBounds.centerY() - this.TARGET_RADIUS) - this.TARGET_PADDING) - totalTextHeight) + height : this.targetBounds.centerY() + this.TARGET_RADIUS + this.TARGET_PADDING + height};
    }

    /* access modifiers changed from: package-private */
    public int getOuterCircleRadius(int i, int i2, Rect rect, Rect rect2) {
        int centerX = rect2.centerX();
        int centerY = rect2.centerY();
        int i3 = (int) (1.1f * ((float) this.TARGET_RADIUS));
        Rect rect3 = new Rect(centerX, centerY, centerX, centerY);
        rect3.inset(-i3, -i3);
        return Math.max(maxDistanceToPoints(i, i2, rect), maxDistanceToPoints(i, i2, rect3)) + this.CIRCLE_PADDING;
    }

    /* access modifiers changed from: package-private */
    public Rect getTextBounds() {
        int totalTextHeight = getTotalTextHeight();
        int totalTextWidth = getTotalTextWidth();
        int centerY = ((this.targetBounds.centerY() - this.TARGET_RADIUS) - this.TARGET_PADDING) - totalTextHeight;
        int centerY2 = centerY > this.topBoundary ? centerY : this.targetBounds.centerY() + this.TARGET_RADIUS + this.TARGET_PADDING;
        int max = Math.max(this.TEXT_PADDING, (this.targetBounds.centerX() - ((getWidth() / 2) - this.targetBounds.centerX() < 0 ? -this.TEXT_POSITIONING_BIAS : this.TEXT_POSITIONING_BIAS)) - totalTextWidth);
        return new Rect(max, centerY2, Math.min(getWidth() - this.TEXT_PADDING, max + totalTextWidth), centerY2 + totalTextHeight);
    }

    /* access modifiers changed from: package-private */
    public int getTotalTextHeight() {
        if (this.titleLayout == null) {
            return 0;
        }
        return this.descriptionLayout == null ? this.titleLayout.getHeight() + this.TEXT_SPACING : this.titleLayout.getHeight() + this.descriptionLayout.getHeight() + this.TEXT_SPACING;
    }

    /* access modifiers changed from: package-private */
    public int getTotalTextWidth() {
        if (this.titleLayout == null) {
            return 0;
        }
        return this.descriptionLayout == null ? this.titleLayout.getWidth() : Math.max(this.titleLayout.getWidth(), this.descriptionLayout.getWidth());
    }

    /* access modifiers changed from: package-private */
    public float halfwayLerp(float f) {
        return f < 0.5f ? f / 0.5f : (1.0f - f) / 0.5f;
    }

    /* access modifiers changed from: package-private */
    public boolean inGutter(int i) {
        return this.bottomBoundary > 0 ? i < this.GUTTER_DIM || i > this.bottomBoundary - this.GUTTER_DIM : i < this.GUTTER_DIM || i > getHeight() - this.GUTTER_DIM;
    }

    /* access modifiers changed from: package-private */
    public void invalidateViewAndOutline(Rect rect) {
        invalidate(rect);
        if (this.outlineProvider != null && Build.VERSION.SDK_INT >= 21) {
            invalidateOutline();
        }
    }

    public boolean isVisible() {
        return !this.isDismissed && this.visible;
    }

    /* access modifiers changed from: package-private */
    public int maxDistanceToPoints(int i, int i2, Rect rect) {
        return (int) Math.max(distance(i, i2, rect.left, rect.top), Math.max(distance(i, i2, rect.right, rect.top), Math.max(distance(i, i2, rect.left, rect.bottom), distance(i, i2, rect.right, rect.bottom))));
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        onDismiss(false);
    }

    /* access modifiers changed from: package-private */
    public void onDismiss(boolean z) {
        if (!this.isDismissed) {
            this.isDismissing = false;
            this.isDismissed = true;
            for (ValueAnimator valueAnimator : this.animators) {
                valueAnimator.cancel();
                valueAnimator.removeAllUpdateListeners();
            }
            ViewUtil.removeOnGlobalLayoutListener(getViewTreeObserver(), this.globalLayoutListener);
            this.visible = false;
            if (this.listener != null) {
                this.listener.onTargetDismissed(this, z);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!this.isDismissed && this.outerCircleCenter != null) {
            if (this.topBoundary > 0 && this.bottomBoundary > 0) {
                canvas.clipRect(0, this.topBoundary, getWidth(), this.bottomBoundary);
            }
            if (this.dimColor != -1) {
                canvas.drawColor(this.dimColor);
            }
            this.outerCirclePaint.setAlpha(this.outerCircleAlpha);
            if (this.shouldDrawShadow && this.outlineProvider == null) {
                int save = canvas.save();
                canvas.clipPath(this.outerCirclePath, Region.Op.DIFFERENCE);
                drawJitteredShadow(canvas);
                canvas.restoreToCount(save);
            }
            canvas.drawCircle((float) this.outerCircleCenter[0], (float) this.outerCircleCenter[1], this.outerCircleRadius, this.outerCirclePaint);
            this.targetCirclePaint.setAlpha(this.targetCircleAlpha);
            if (this.targetCirclePulseAlpha > 0) {
                this.targetCirclePulsePaint.setAlpha(this.targetCirclePulseAlpha);
                canvas.drawCircle((float) this.targetBounds.centerX(), (float) this.targetBounds.centerY(), this.targetCirclePulseRadius, this.targetCirclePulsePaint);
            }
            canvas.drawCircle((float) this.targetBounds.centerX(), (float) this.targetBounds.centerY(), this.targetCircleRadius, this.targetCirclePaint);
            int save2 = canvas.save();
            canvas.translate((float) this.textBounds.left, (float) this.textBounds.top);
            this.titlePaint.setAlpha(this.textAlpha);
            if (this.titleLayout != null) {
                this.titleLayout.draw(canvas);
            }
            if (!(this.descriptionLayout == null || this.titleLayout == null)) {
                canvas.translate(0.0f, (float) (this.titleLayout.getHeight() + this.TEXT_SPACING));
                this.descriptionPaint.setAlpha((int) (this.target.descriptionTextAlpha * ((float) this.textAlpha)));
                this.descriptionLayout.draw(canvas);
            }
            canvas.restoreToCount(save2);
            int save3 = canvas.save();
            if (this.tintedTarget != null) {
                canvas.translate((float) (this.targetBounds.centerX() - (this.tintedTarget.getWidth() / 2)), (float) (this.targetBounds.centerY() - (this.tintedTarget.getHeight() / 2)));
                canvas.drawBitmap(this.tintedTarget, 0.0f, 0.0f, this.targetCirclePaint);
            } else if (this.target.icon != null) {
                canvas.translate((float) (this.targetBounds.centerX() - (this.target.icon.getBounds().width() / 2)), (float) (this.targetBounds.centerY() - (this.target.icon.getBounds().height() / 2)));
                this.target.icon.setAlpha(this.targetCirclePaint.getAlpha());
                this.target.icon.draw(canvas);
            }
            canvas.restoreToCount(save3);
            if (this.debug) {
                drawDebugInformation(canvas);
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (!isVisible() || !this.cancelable || i != 4) {
            return false;
        }
        keyEvent.startTracking();
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (!isVisible() || !this.isInteractable || !this.cancelable || i != 4 || !keyEvent.isTracking() || keyEvent.isCanceled()) {
            return false;
        }
        this.isInteractable = false;
        if (this.listener != null) {
            this.listener.onTargetCancel(this);
        } else {
            new Listener().onTargetCancel(this);
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.lastTouchX = motionEvent.getX();
        this.lastTouchY = motionEvent.getY();
        return super.onTouchEvent(motionEvent);
    }

    public void setDrawDebug(boolean z) {
        if (this.debug != z) {
            this.debug = z;
            postInvalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateTextLayouts() {
        int min = Math.min(getWidth(), this.TEXT_MAX_WIDTH) - (this.TEXT_PADDING * 2);
        if (min > 0) {
            this.titleLayout = new StaticLayout(this.title, this.titlePaint, min, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            if (this.description != null) {
                this.descriptionLayout = new StaticLayout(this.description, this.descriptionPaint, min, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            } else {
                this.descriptionLayout = null;
            }
        }
    }
}
