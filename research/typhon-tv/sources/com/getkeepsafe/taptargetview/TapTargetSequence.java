package com.getkeepsafe.taptargetview;

import android.app.Activity;
import android.app.Dialog;
import com.getkeepsafe.taptargetview.TapTargetView;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

public class TapTargetSequence {
    private boolean active;
    private final Activity activity;
    boolean considerOuterCircleCanceled;
    boolean continueOnCancel;
    private TapTargetView currentView;
    private final Dialog dialog;
    Listener listener;
    private final TapTargetView.Listener tapTargetListener = new TapTargetView.Listener() {
        public void onOuterCircleClick(TapTargetView tapTargetView) {
            if (TapTargetSequence.this.considerOuterCircleCanceled) {
                onTargetCancel(tapTargetView);
            }
        }

        public void onTargetCancel(TapTargetView tapTargetView) {
            super.onTargetCancel(tapTargetView);
            if (TapTargetSequence.this.continueOnCancel) {
                if (TapTargetSequence.this.listener != null) {
                    TapTargetSequence.this.listener.onSequenceStep(tapTargetView.target, false);
                }
                TapTargetSequence.this.showNext();
            } else if (TapTargetSequence.this.listener != null) {
                TapTargetSequence.this.listener.onSequenceCanceled(tapTargetView.target);
            }
        }

        public void onTargetClick(TapTargetView tapTargetView) {
            super.onTargetClick(tapTargetView);
            if (TapTargetSequence.this.listener != null) {
                TapTargetSequence.this.listener.onSequenceStep(tapTargetView.target, true);
            }
            TapTargetSequence.this.showNext();
        }
    };
    private final Queue<TapTarget> targets;

    public interface Listener {
        void onSequenceCanceled(TapTarget tapTarget);

        void onSequenceFinish();

        void onSequenceStep(TapTarget tapTarget, boolean z);
    }

    public TapTargetSequence(Activity activity2) {
        if (activity2 == null) {
            throw new IllegalArgumentException("Activity is null");
        }
        this.activity = activity2;
        this.dialog = null;
        this.targets = new LinkedList();
    }

    public TapTargetSequence(Dialog dialog2) {
        if (dialog2 == null) {
            throw new IllegalArgumentException("Given null Dialog");
        }
        this.dialog = dialog2;
        this.activity = null;
        this.targets = new LinkedList();
    }

    public boolean cancel() {
        if (this.targets.isEmpty() || !this.active || this.currentView == null || !this.currentView.cancelable) {
            return false;
        }
        this.currentView.dismiss(false);
        this.active = false;
        this.targets.clear();
        if (this.listener != null) {
            this.listener.onSequenceCanceled(this.currentView.target);
        }
        return true;
    }

    public TapTargetSequence considerOuterCircleCanceled(boolean z) {
        this.considerOuterCircleCanceled = z;
        return this;
    }

    public TapTargetSequence continueOnCancel(boolean z) {
        this.continueOnCancel = z;
        return this;
    }

    public TapTargetSequence listener(Listener listener2) {
        this.listener = listener2;
        return this;
    }

    /* access modifiers changed from: package-private */
    public void showNext() {
        try {
            TapTarget remove = this.targets.remove();
            if (this.activity != null) {
                this.currentView = TapTargetView.showFor(this.activity, remove, this.tapTargetListener);
            } else {
                this.currentView = TapTargetView.showFor(this.dialog, remove, this.tapTargetListener);
            }
        } catch (NoSuchElementException e) {
            if (this.listener != null) {
                this.listener.onSequenceFinish();
            }
        }
    }

    public void start() {
        if (!this.targets.isEmpty() && !this.active) {
            this.active = true;
            showNext();
        }
    }

    public void startAt(int i) {
        if (!this.active) {
            if (i < 0 || i >= this.targets.size()) {
                throw new IllegalArgumentException("Given invalid index " + i);
            }
            int size = this.targets.size() - i;
            while (this.targets.peek() != null && this.targets.size() != size) {
                this.targets.poll();
            }
            if (this.targets.size() != size) {
                throw new IllegalStateException("Given index " + i + " not in sequence");
            }
            start();
        }
    }

    public void startWith(int i) {
        if (!this.active) {
            while (this.targets.peek() != null && this.targets.peek().id() != i) {
                this.targets.poll();
            }
            TapTarget peek = this.targets.peek();
            if (peek == null || peek.id() != i) {
                throw new IllegalStateException("Given target " + i + " not in sequence");
            }
            start();
        }
    }

    public TapTargetSequence target(TapTarget tapTarget) {
        this.targets.add(tapTarget);
        return this;
    }

    public TapTargetSequence targets(List<TapTarget> list) {
        this.targets.addAll(list);
        return this;
    }

    public TapTargetSequence targets(TapTarget... tapTargetArr) {
        Collections.addAll(this.targets, tapTargetArr);
        return this;
    }
}
