package com.getkeepsafe.taptargetview;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class TapTarget {
    Rect bounds;
    boolean cancelable;
    final CharSequence description;
    float descriptionTextAlpha;
    private Integer descriptionTextColor;
    private int descriptionTextColorRes;
    private int descriptionTextDimen;
    private int descriptionTextSize;
    Typeface descriptionTypeface;
    private Integer dimColor;
    private int dimColorRes;
    boolean drawShadow;
    Drawable icon;
    int id;
    float outerCircleAlpha;
    private Integer outerCircleColor;
    private int outerCircleColorRes;
    private Integer targetCircleColor;
    private int targetCircleColorRes;
    int targetRadius;
    boolean tintTarget;
    final CharSequence title;
    private Integer titleTextColor;
    private int titleTextColorRes;
    private int titleTextDimen;
    private int titleTextSize;
    Typeface titleTypeface;
    boolean transparentTarget;

    protected TapTarget(Rect rect, CharSequence charSequence, CharSequence charSequence2) {
        this(charSequence, charSequence2);
        if (rect == null) {
            throw new IllegalArgumentException("Cannot pass null bounds or title");
        }
        this.bounds = rect;
    }

    protected TapTarget(CharSequence charSequence, CharSequence charSequence2) {
        this.outerCircleAlpha = 0.96f;
        this.targetRadius = 44;
        this.outerCircleColorRes = -1;
        this.targetCircleColorRes = -1;
        this.dimColorRes = -1;
        this.titleTextColorRes = -1;
        this.descriptionTextColorRes = -1;
        this.outerCircleColor = null;
        this.targetCircleColor = null;
        this.dimColor = null;
        this.titleTextColor = null;
        this.descriptionTextColor = null;
        this.titleTextDimen = -1;
        this.descriptionTextDimen = -1;
        this.titleTextSize = 20;
        this.descriptionTextSize = 18;
        this.id = -1;
        this.drawShadow = false;
        this.cancelable = true;
        this.tintTarget = true;
        this.transparentTarget = false;
        this.descriptionTextAlpha = 0.54f;
        if (charSequence == null) {
            throw new IllegalArgumentException("Cannot pass null title");
        }
        this.title = charSequence;
        this.description = charSequence2;
    }

    private Integer colorResOrInt(Context context, Integer num, int i) {
        return i != -1 ? Integer.valueOf(ContextCompat.getColor(context, i)) : num;
    }

    private int dimenOrSize(Context context, int i, int i2) {
        return i2 != -1 ? context.getResources().getDimensionPixelSize(i2) : UiUtil.sp(context, i);
    }

    public static TapTarget forBounds(Rect rect, CharSequence charSequence) {
        return forBounds(rect, charSequence, (CharSequence) null);
    }

    public static TapTarget forBounds(Rect rect, CharSequence charSequence, CharSequence charSequence2) {
        return new TapTarget(rect, charSequence, charSequence2);
    }

    public static TapTarget forToolbarMenuItem(Toolbar toolbar, int i, CharSequence charSequence) {
        return forToolbarMenuItem(toolbar, i, charSequence, (CharSequence) null);
    }

    public static TapTarget forToolbarMenuItem(Toolbar toolbar, int i, CharSequence charSequence, CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, i, charSequence, charSequence2);
    }

    public static TapTarget forToolbarMenuItem(android.widget.Toolbar toolbar, int i, CharSequence charSequence) {
        return forToolbarMenuItem(toolbar, i, charSequence, (CharSequence) null);
    }

    public static TapTarget forToolbarMenuItem(android.widget.Toolbar toolbar, int i, CharSequence charSequence, CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, i, charSequence, charSequence2);
    }

    public static TapTarget forToolbarNavigationIcon(Toolbar toolbar, CharSequence charSequence) {
        return forToolbarNavigationIcon(toolbar, charSequence, (CharSequence) null);
    }

    public static TapTarget forToolbarNavigationIcon(Toolbar toolbar, CharSequence charSequence, CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, true, charSequence, charSequence2);
    }

    public static TapTarget forToolbarNavigationIcon(android.widget.Toolbar toolbar, CharSequence charSequence) {
        return forToolbarNavigationIcon(toolbar, charSequence, (CharSequence) null);
    }

    public static TapTarget forToolbarNavigationIcon(android.widget.Toolbar toolbar, CharSequence charSequence, CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, true, charSequence, charSequence2);
    }

    public static TapTarget forToolbarOverflow(Toolbar toolbar, CharSequence charSequence) {
        return forToolbarOverflow(toolbar, charSequence, (CharSequence) null);
    }

    public static TapTarget forToolbarOverflow(Toolbar toolbar, CharSequence charSequence, CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, false, charSequence, charSequence2);
    }

    public static TapTarget forToolbarOverflow(android.widget.Toolbar toolbar, CharSequence charSequence) {
        return forToolbarOverflow(toolbar, charSequence, (CharSequence) null);
    }

    public static TapTarget forToolbarOverflow(android.widget.Toolbar toolbar, CharSequence charSequence, CharSequence charSequence2) {
        return new ToolbarTapTarget(toolbar, false, charSequence, charSequence2);
    }

    public static TapTarget forView(View view, CharSequence charSequence) {
        return forView(view, charSequence, (CharSequence) null);
    }

    public static TapTarget forView(View view, CharSequence charSequence, CharSequence charSequence2) {
        return new ViewTapTarget(view, charSequence, charSequence2);
    }

    public Rect bounds() {
        if (this.bounds != null) {
            return this.bounds;
        }
        throw new IllegalStateException("Requesting bounds that are not set! Make sure your target is ready");
    }

    public TapTarget cancelable(boolean z) {
        this.cancelable = z;
        return this;
    }

    public TapTarget descriptionTextAlpha(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Given an invalid alpha value: " + f);
        }
        this.descriptionTextAlpha = f;
        return this;
    }

    public TapTarget descriptionTextColor(int i) {
        this.descriptionTextColorRes = i;
        return this;
    }

    public TapTarget descriptionTextColorInt(int i) {
        this.descriptionTextColor = Integer.valueOf(i);
        return this;
    }

    /* access modifiers changed from: package-private */
    public Integer descriptionTextColorInt(Context context) {
        return colorResOrInt(context, this.descriptionTextColor, this.descriptionTextColorRes);
    }

    public TapTarget descriptionTextDimen(int i) {
        this.descriptionTextDimen = i;
        return this;
    }

    public TapTarget descriptionTextSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Given negative text size");
        }
        this.descriptionTextSize = i;
        return this;
    }

    /* access modifiers changed from: package-private */
    public int descriptionTextSizePx(Context context) {
        return dimenOrSize(context, this.descriptionTextSize, this.descriptionTextDimen);
    }

    public TapTarget descriptionTypeface(Typeface typeface) {
        if (typeface == null) {
            throw new IllegalArgumentException("Cannot use a null typeface");
        }
        this.descriptionTypeface = typeface;
        return this;
    }

    public TapTarget dimColor(int i) {
        this.dimColorRes = i;
        return this;
    }

    public TapTarget dimColorInt(int i) {
        this.dimColor = Integer.valueOf(i);
        return this;
    }

    /* access modifiers changed from: package-private */
    public Integer dimColorInt(Context context) {
        return colorResOrInt(context, this.dimColor, this.dimColorRes);
    }

    public TapTarget drawShadow(boolean z) {
        this.drawShadow = z;
        return this;
    }

    public TapTarget icon(Drawable drawable) {
        return icon(drawable, false);
    }

    public TapTarget icon(Drawable drawable, boolean z) {
        if (drawable == null) {
            throw new IllegalArgumentException("Cannot use null drawable");
        }
        this.icon = drawable;
        if (!z) {
            this.icon.setBounds(new Rect(0, 0, this.icon.getIntrinsicWidth(), this.icon.getIntrinsicHeight()));
        }
        return this;
    }

    public int id() {
        return this.id;
    }

    public TapTarget id(int i) {
        this.id = i;
        return this;
    }

    public void onReady(Runnable runnable) {
        runnable.run();
    }

    public TapTarget outerCircleAlpha(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Given an invalid alpha value: " + f);
        }
        this.outerCircleAlpha = f;
        return this;
    }

    public TapTarget outerCircleColor(int i) {
        this.outerCircleColorRes = i;
        return this;
    }

    public TapTarget outerCircleColorInt(int i) {
        this.outerCircleColor = Integer.valueOf(i);
        return this;
    }

    /* access modifiers changed from: package-private */
    public Integer outerCircleColorInt(Context context) {
        return colorResOrInt(context, this.outerCircleColor, this.outerCircleColorRes);
    }

    public TapTarget targetCircleColor(int i) {
        this.targetCircleColorRes = i;
        return this;
    }

    public TapTarget targetCircleColorInt(int i) {
        this.targetCircleColor = Integer.valueOf(i);
        return this;
    }

    /* access modifiers changed from: package-private */
    public Integer targetCircleColorInt(Context context) {
        return colorResOrInt(context, this.targetCircleColor, this.targetCircleColorRes);
    }

    public TapTarget targetRadius(int i) {
        this.targetRadius = i;
        return this;
    }

    public TapTarget textColor(int i) {
        this.titleTextColorRes = i;
        this.descriptionTextColorRes = i;
        return this;
    }

    public TapTarget textColorInt(int i) {
        this.titleTextColor = Integer.valueOf(i);
        this.descriptionTextColor = Integer.valueOf(i);
        return this;
    }

    public TapTarget textTypeface(Typeface typeface) {
        if (typeface == null) {
            throw new IllegalArgumentException("Cannot use a null typeface");
        }
        this.titleTypeface = typeface;
        this.descriptionTypeface = typeface;
        return this;
    }

    public TapTarget tintTarget(boolean z) {
        this.tintTarget = z;
        return this;
    }

    public TapTarget titleTextColor(int i) {
        this.titleTextColorRes = i;
        return this;
    }

    public TapTarget titleTextColorInt(int i) {
        this.titleTextColor = Integer.valueOf(i);
        return this;
    }

    /* access modifiers changed from: package-private */
    public Integer titleTextColorInt(Context context) {
        return colorResOrInt(context, this.titleTextColor, this.titleTextColorRes);
    }

    public TapTarget titleTextDimen(int i) {
        this.titleTextDimen = i;
        return this;
    }

    public TapTarget titleTextSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Given negative text size");
        }
        this.titleTextSize = i;
        return this;
    }

    /* access modifiers changed from: package-private */
    public int titleTextSizePx(Context context) {
        return dimenOrSize(context, this.titleTextSize, this.titleTextDimen);
    }

    public TapTarget titleTypeface(Typeface typeface) {
        if (typeface == null) {
            throw new IllegalArgumentException("Cannot use a null typeface");
        }
        this.titleTypeface = typeface;
        return this;
    }

    public TapTarget transparentTarget(boolean z) {
        this.transparentTarget = z;
        return this;
    }
}
