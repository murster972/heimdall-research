package com.livefront.bridge;

import android.content.Context;
import android.os.Bundle;

public class Bridge {
    private static BridgeDelegate sDelegate;

    private static void checkInitialization() {
        if (sDelegate == null) {
            throw new IllegalStateException("You must first call initialize before calling any other methods");
        }
    }

    public static void clear(Object obj) {
        checkInitialization();
        sDelegate.clear(obj);
    }

    public static void clearAll(Context context) {
        (sDelegate != null ? sDelegate : new BridgeDelegate(context, new NoOpSavedStateHandler())).clearAll();
    }

    public static void initialize(Context context, SavedStateHandler savedStateHandler) {
        sDelegate = new BridgeDelegate(context, savedStateHandler);
    }

    public static void restoreInstanceState(Object obj, Bundle bundle) {
        checkInitialization();
        sDelegate.restoreInstanceState(obj, bundle);
    }

    public static void saveInstanceState(Object obj, Bundle bundle) {
        checkInitialization();
        sDelegate.saveInstanceState(obj, bundle);
    }
}
