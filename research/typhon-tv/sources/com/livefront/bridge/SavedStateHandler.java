package com.livefront.bridge;

import android.os.Bundle;

public interface SavedStateHandler {
    void restoreInstanceState(Object obj, Bundle bundle);

    void saveInstanceState(Object obj, Bundle bundle);
}
