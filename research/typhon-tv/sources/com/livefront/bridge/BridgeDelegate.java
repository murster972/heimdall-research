package com.livefront.bridge;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Base64;
import com.livefront.bridge.wrapper.WrapperUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;

class BridgeDelegate {
    private static final int AUTO_DATA_CLEAR_INTERVAL_MS = 100;
    private static final String KEY_BUNDLE = "bundle_%s";
    private static final String KEY_UUID = "uuid_%s";
    private static final String TAG = BridgeDelegate.class.getName();
    /* access modifiers changed from: private */
    public boolean mIsClearAllowed = false;
    private boolean mIsFirstRestoreCall = true;
    private long mLastClearTime;
    private Map<Object, String> mObjectUuidMap = new WeakHashMap();
    private Set<String> mRecentUuids = new HashSet();
    private SavedStateHandler mSavedStateHandler;
    private SharedPreferences mSharedPreferences;
    private Map<String, Bundle> mUuidBundleMap = new HashMap();

    BridgeDelegate(Context context, SavedStateHandler savedStateHandler) {
        this.mSharedPreferences = context.getSharedPreferences(TAG, 0);
        this.mSavedStateHandler = savedStateHandler;
        registerForLifecycleEvents(context);
    }

    private void clearDataForUuid(String str) {
        this.mRecentUuids.remove(str);
        this.mUuidBundleMap.remove(str);
        clearDataFromDisk(str);
    }

    private void clearDataFromDisk(String str) {
        this.mSharedPreferences.edit().remove(getKeyForEncodedBundle(str)).apply();
    }

    private void clearStaleData() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.mLastClearTime >= 100) {
            this.mLastClearTime = currentTimeMillis;
            System.gc();
            HashSet<String> hashSet = new HashSet<>(this.mRecentUuids);
            hashSet.removeAll(this.mObjectUuidMap.values());
            for (String clearDataForUuid : hashSet) {
                clearDataForUuid(clearDataForUuid);
            }
        }
    }

    private String getKeyForEncodedBundle(String str) {
        return String.format(KEY_BUNDLE, new Object[]{str});
    }

    private String getKeyForUuid(Object obj) {
        return String.format(KEY_UUID, new Object[]{obj.getClass().getName()});
    }

    private Bundle readFromDisk(String str) {
        String string = this.mSharedPreferences.getString(getKeyForEncodedBundle(str), (String) null);
        if (string == null) {
            return null;
        }
        byte[] decode = Base64.decode(string, 0);
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(decode, 0, decode.length);
        obtain.setDataPosition(0);
        Bundle readBundle = obtain.readBundle(BridgeDelegate.class.getClassLoader());
        obtain.recycle();
        return readBundle;
    }

    @SuppressLint({"NewApi"})
    private void registerForLifecycleEvents(Context context) {
        if (Build.VERSION.SDK_INT < 14) {
            this.mIsClearAllowed = false;
        } else {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacksAdapter() {
                public void onActivityCreated(Activity activity, Bundle bundle) {
                    boolean unused = BridgeDelegate.this.mIsClearAllowed = true;
                }

                public void onActivityDestroyed(Activity activity) {
                    boolean unused = BridgeDelegate.this.mIsClearAllowed = !activity.isChangingConfigurations();
                }
            });
        }
    }

    private void writeToDisk(String str, Bundle bundle) {
        Parcel obtain = Parcel.obtain();
        obtain.writeBundle(bundle);
        this.mSharedPreferences.edit().putString(getKeyForEncodedBundle(str), Base64.encodeToString(obtain.marshall(), 0)).apply();
        obtain.recycle();
    }

    /* access modifiers changed from: package-private */
    public void clear(Object obj) {
        String remove;
        if (this.mIsClearAllowed && (remove = this.mObjectUuidMap.remove(obj)) != null) {
            clearDataForUuid(remove);
        }
    }

    /* access modifiers changed from: package-private */
    public void clearAll() {
        this.mRecentUuids.clear();
        this.mUuidBundleMap.clear();
        this.mObjectUuidMap.clear();
        this.mSharedPreferences.edit().clear().apply();
    }

    /* access modifiers changed from: package-private */
    public void restoreInstanceState(Object obj, Bundle bundle) {
        boolean z = this.mIsFirstRestoreCall;
        this.mIsFirstRestoreCall = false;
        if (bundle != null) {
            String string = this.mObjectUuidMap.containsKey(obj) ? this.mObjectUuidMap.get(obj) : bundle.getString(getKeyForUuid(obj), (String) null);
            if (string != null) {
                this.mObjectUuidMap.put(obj, string);
                Bundle readFromDisk = this.mUuidBundleMap.containsKey(string) ? this.mUuidBundleMap.get(string) : readFromDisk(string);
                if (readFromDisk != null) {
                    WrapperUtils.unwrapOptimizedObjects(readFromDisk);
                    this.mSavedStateHandler.restoreInstanceState(obj, readFromDisk);
                    clearDataForUuid(string);
                }
            }
        } else if (z) {
            this.mSharedPreferences.edit().clear().apply();
        }
    }

    /* access modifiers changed from: package-private */
    public void saveInstanceState(Object obj, Bundle bundle) {
        String str = this.mObjectUuidMap.get(obj);
        if (str == null) {
            str = UUID.randomUUID().toString();
            this.mObjectUuidMap.put(obj, str);
        }
        bundle.putString(getKeyForUuid(obj), str);
        Bundle bundle2 = new Bundle();
        this.mSavedStateHandler.saveInstanceState(obj, bundle2);
        if (!bundle2.isEmpty()) {
            WrapperUtils.wrapOptimizedObjects(bundle2);
            this.mRecentUuids.add(str);
            this.mUuidBundleMap.put(str, bundle2);
            writeToDisk(str, bundle2);
            clearStaleData();
        }
    }
}
