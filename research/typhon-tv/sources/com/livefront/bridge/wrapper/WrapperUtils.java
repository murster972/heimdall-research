package com.livefront.bridge.wrapper;

import android.graphics.Bitmap;
import android.os.Bundle;

public class WrapperUtils {
    public static void unwrapOptimizedObjects(Bundle bundle) {
        for (String str : bundle.keySet()) {
            if (bundle.get(str) instanceof BitmapWrapper) {
                bundle.putParcelable(str, ((BitmapWrapper) bundle.get(str)).getBitmap());
            }
        }
    }

    public static void wrapOptimizedObjects(Bundle bundle) {
        for (String str : bundle.keySet()) {
            if (bundle.get(str) instanceof Bitmap) {
                bundle.putParcelable(str, new BitmapWrapper((Bitmap) bundle.get(str)));
            }
        }
    }
}
