package com.livefront.bridge.wrapper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;

class BitmapWrapper implements Parcelable {
    public static final Parcelable.Creator<BitmapWrapper> CREATOR = new Parcelable.Creator<BitmapWrapper>() {
        public BitmapWrapper createFromParcel(Parcel parcel) {
            return new BitmapWrapper(parcel);
        }

        public BitmapWrapper[] newArray(int i) {
            return new BitmapWrapper[i];
        }
    };
    private Bitmap mBitmap;

    public BitmapWrapper(Bitmap bitmap) {
        this.mBitmap = bitmap;
    }

    protected BitmapWrapper(Parcel parcel) {
        byte[] createByteArray = parcel.createByteArray();
        this.mBitmap = BitmapFactory.decodeByteArray(createByteArray, 0, createByteArray.length);
    }

    public int describeContents() {
        return 0;
    }

    public Bitmap getBitmap() {
        return this.mBitmap;
    }

    public void writeToParcel(Parcel parcel, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.mBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        parcel.writeByteArray(byteArrayOutputStream.toByteArray());
    }
}
