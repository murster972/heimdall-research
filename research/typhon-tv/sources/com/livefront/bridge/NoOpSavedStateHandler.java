package com.livefront.bridge;

import android.os.Bundle;

class NoOpSavedStateHandler implements SavedStateHandler {
    NoOpSavedStateHandler() {
    }

    public void restoreInstanceState(Object obj, Bundle bundle) {
    }

    public void saveInstanceState(Object obj, Bundle bundle) {
    }
}
