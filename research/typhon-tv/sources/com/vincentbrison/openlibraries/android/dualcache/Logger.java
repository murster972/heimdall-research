package com.vincentbrison.openlibraries.android.dualcache;

import android.util.Log;

final class Logger {
    private static final String DEFAULT_LOG_TAG = "dualcache";
    private final boolean isLogEnable;

    Logger(boolean z) {
        this.isLogEnable = z;
    }

    private void log(int i, String str, String str2) {
        if (this.isLogEnable) {
            Log.println(i, str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    public void logError(Throwable th) {
        if (this.isLogEnable) {
            Log.e(DEFAULT_LOG_TAG, "error : ", th);
        }
    }

    /* access modifiers changed from: package-private */
    public void logInfo(String str) {
        log(4, DEFAULT_LOG_TAG, str);
    }

    /* access modifiers changed from: package-private */
    public void logInfo(String str, String str2) {
        log(4, str, str2);
    }

    /* access modifiers changed from: package-private */
    public void logVerbose(String str) {
        log(2, DEFAULT_LOG_TAG, str);
    }

    /* access modifiers changed from: package-private */
    public void logWarning(String str) {
        log(5, DEFAULT_LOG_TAG, str);
    }
}
