package com.vincentbrison.openlibraries.android.dualcache;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class DualCacheLock {
    private final ConcurrentMap<String, Lock> editionLocks = new ConcurrentHashMap();
    private final ReadWriteLock invalidationReadWriteLock = new ReentrantReadWriteLock();

    DualCacheLock() {
    }

    private Lock getLockForGivenDiskEntry(String str) {
        if (!this.editionLocks.containsKey(str)) {
            this.editionLocks.putIfAbsent(str, new ReentrantLock());
        }
        return (Lock) this.editionLocks.get(str);
    }

    /* access modifiers changed from: package-private */
    public void lockDiskEntryWrite(String str) {
        this.invalidationReadWriteLock.readLock().lock();
        getLockForGivenDiskEntry(str).lock();
    }

    /* access modifiers changed from: package-private */
    public void lockFullDiskWrite() {
        this.invalidationReadWriteLock.writeLock().lock();
    }

    /* access modifiers changed from: package-private */
    public void unLockDiskEntryWrite(String str) {
        getLockForGivenDiskEntry(str).unlock();
        this.invalidationReadWriteLock.readLock().unlock();
    }

    /* access modifiers changed from: package-private */
    public void unLockFullDiskWrite() {
        this.invalidationReadWriteLock.writeLock().unlock();
    }
}
