package com.vincentbrison.openlibraries.android.dualcache;

import android.content.Context;
import java.io.File;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class Builder<T> {
    private static final String CACHE_FILE_PREFIX = "dualcache";
    private int appVersion;
    private File diskFolder;
    private DualCacheDiskMode diskMode = null;
    private CacheSerializer<T> diskSerializer;
    private String id;
    private boolean logEnabled = false;
    private int maxDiskSizeBytes;
    private int maxRamSizeBytes;
    private DualCacheRamMode ramMode = null;
    private CacheSerializer<T> ramSerializer;
    private SizeOf<T> sizeOf;

    public Builder(String str, int i) {
        this.id = str;
        this.appVersion = i;
    }

    private File getDefaultDiskCacheFolder(boolean z, Context context) {
        return z ? context.getDir(CACHE_FILE_PREFIX + this.id, 0) : new File(context.getCacheDir().getPath() + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + CACHE_FILE_PREFIX + InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + this.id);
    }

    public DualCache<T> build() {
        if (this.ramMode == null) {
            throw new IllegalStateException("No ram mode set");
        } else if (this.diskMode == null) {
            throw new IllegalStateException("No disk mode set");
        } else {
            DualCache<T> dualCache = new DualCache<>(this.appVersion, new Logger(this.logEnabled), this.ramMode, this.ramSerializer, this.maxRamSizeBytes, this.sizeOf, this.diskMode, this.diskSerializer, this.maxDiskSizeBytes, this.diskFolder);
            boolean equals = dualCache.getRAMMode().equals(DualCacheRamMode.DISABLE);
            boolean equals2 = dualCache.getDiskMode().equals(DualCacheDiskMode.DISABLE);
            if (!equals || !equals2) {
                return dualCache;
            }
            throw new IllegalStateException("The ram cache layer and the disk cache layer are disable. You have to use at least one of those layers.");
        }
    }

    public Builder<T> enableLog() {
        this.logEnabled = true;
        return this;
    }

    public Builder<T> noDisk() {
        this.diskMode = DualCacheDiskMode.DISABLE;
        return this;
    }

    public Builder<T> noRam() {
        this.ramMode = DualCacheRamMode.DISABLE;
        return this;
    }

    public Builder<T> useReferenceInRam(int i, SizeOf<T> sizeOf2) {
        this.ramMode = DualCacheRamMode.ENABLE_WITH_REFERENCE;
        this.maxRamSizeBytes = i;
        this.sizeOf = sizeOf2;
        return this;
    }

    public Builder<T> useSerializerInDisk(int i, File file, CacheSerializer<T> cacheSerializer) {
        this.diskFolder = file;
        this.diskMode = DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER;
        this.maxDiskSizeBytes = i;
        this.diskSerializer = cacheSerializer;
        return this;
    }

    public Builder<T> useSerializerInDisk(int i, boolean z, CacheSerializer<T> cacheSerializer, Context context) {
        return useSerializerInDisk(i, getDefaultDiskCacheFolder(z, context), cacheSerializer);
    }

    public Builder<T> useSerializerInRam(int i, CacheSerializer<T> cacheSerializer) {
        this.ramMode = DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER;
        this.maxRamSizeBytes = i;
        this.ramSerializer = cacheSerializer;
        return this;
    }
}
