package com.vincentbrison.openlibraries.android.dualcache;

public interface CacheSerializer<T> {
    T fromString(String str);

    String toString(T t);
}
