package com.vincentbrison.openlibraries.android.dualcache;

import com.jakewharton.disklrucache.DiskLruCache;
import java.io.File;
import java.io.IOException;

public class DualCache<T> {
    private static final int VALUES_PER_CACHE_ENTRY = 1;
    private final int appVersion;
    private final File diskCacheFolder;
    private DiskLruCache diskLruCache;
    private final DualCacheDiskMode diskMode;
    private final CacheSerializer<T> diskSerializer;
    private final DualCacheLock dualCacheLock = new DualCacheLock();
    private final Logger logger;
    private final LoggerHelper loggerHelper;
    private final int maxDiskSizeBytes;
    private final RamLruCache ramCacheLru;
    private final DualCacheRamMode ramMode;
    private final CacheSerializer<T> ramSerializer;

    DualCache(int i, Logger logger2, DualCacheRamMode dualCacheRamMode, CacheSerializer<T> cacheSerializer, int i2, SizeOf<T> sizeOf, DualCacheDiskMode dualCacheDiskMode, CacheSerializer<T> cacheSerializer2, int i3, File file) {
        this.appVersion = i;
        this.ramMode = dualCacheRamMode;
        this.ramSerializer = cacheSerializer;
        this.diskMode = dualCacheDiskMode;
        this.diskSerializer = cacheSerializer2;
        this.diskCacheFolder = file;
        this.logger = logger2;
        this.loggerHelper = new LoggerHelper(logger2);
        switch (dualCacheRamMode) {
            case ENABLE_WITH_SPECIFIC_SERIALIZER:
                this.ramCacheLru = new StringLruCache(i2);
                break;
            case ENABLE_WITH_REFERENCE:
                this.ramCacheLru = new ReferenceLruCache(i2, sizeOf);
                break;
            default:
                this.ramCacheLru = null;
                break;
        }
        switch (dualCacheDiskMode) {
            case ENABLE_WITH_SPECIFIC_SERIALIZER:
                this.maxDiskSizeBytes = i3;
                try {
                    openDiskLruCache(file);
                    return;
                } catch (IOException e) {
                    logger2.logError(e);
                    return;
                }
            default:
                this.maxDiskSizeBytes = 0;
                return;
        }
    }

    private void openDiskLruCache(File file) throws IOException {
        this.diskLruCache = DiskLruCache.m26310(file, this.appVersion, 1, (long) this.maxDiskSizeBytes);
    }

    public boolean contains(String str) {
        if (!this.ramMode.equals(DualCacheRamMode.DISABLE) && this.ramCacheLru.snapshot().containsKey(str)) {
            return true;
        }
        try {
            this.dualCacheLock.lockDiskEntryWrite(str);
            if (!this.diskMode.equals(DualCacheDiskMode.DISABLE) && this.diskLruCache.m26321(str) != null) {
                return true;
            }
            this.dualCacheLock.unLockDiskEntryWrite(str);
            return false;
        } catch (IOException e) {
            this.logger.logError(e);
        } finally {
            this.dualCacheLock.unLockDiskEntryWrite(str);
        }
    }

    public void delete(String str) {
        if (!this.ramMode.equals(DualCacheRamMode.DISABLE)) {
            this.ramCacheLru.remove(str);
        }
        if (!this.diskMode.equals(DualCacheDiskMode.DISABLE)) {
            try {
                this.dualCacheLock.lockDiskEntryWrite(str);
                this.diskLruCache.m26319(str);
            } catch (IOException e) {
                this.logger.logError(e);
            } finally {
                this.dualCacheLock.unLockDiskEntryWrite(str);
            }
        }
    }

    public T get(String str) {
        T t = null;
        String str2 = null;
        DiskLruCache.Snapshot snapshot = null;
        boolean equals = this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER);
        boolean equals2 = this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_REFERENCE);
        if (equals || equals2) {
            t = this.ramCacheLru.get(str);
        }
        if (t == null) {
            this.loggerHelper.logEntryForKeyIsNotInRam(str);
            if (this.diskMode.equals(DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
                try {
                    this.dualCacheLock.lockDiskEntryWrite(str);
                    snapshot = this.diskLruCache.m26321(str);
                } catch (IOException e) {
                    this.logger.logError(e);
                } finally {
                    this.dualCacheLock.unLockDiskEntryWrite(str);
                }
                if (snapshot != null) {
                    this.loggerHelper.logEntryForKeyIsOnDisk(str);
                    try {
                        str2 = snapshot.m26344(0);
                    } catch (IOException e2) {
                        this.logger.logError(e2);
                    }
                } else {
                    this.loggerHelper.logEntryForKeyIsNotOnDisk(str);
                }
            }
            if (str2 != null) {
                T fromString = this.diskSerializer.fromString(str2);
                if (this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_REFERENCE)) {
                    if (!this.diskMode.equals(DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
                        return fromString;
                    }
                    this.ramCacheLru.put(str, fromString);
                    return fromString;
                } else if (!this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
                    return fromString;
                } else {
                    if (this.diskSerializer == this.ramSerializer) {
                        this.ramCacheLru.put(str, str2);
                        return fromString;
                    }
                    this.ramCacheLru.put(str, this.ramSerializer.toString(fromString));
                    return fromString;
                }
            }
        } else {
            this.loggerHelper.logEntryForKeyIsInRam(str);
            if (this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_REFERENCE)) {
                return t;
            }
            if (this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
                return this.ramSerializer.fromString((String) t);
            }
        }
        return null;
    }

    public DualCacheDiskMode getDiskMode() {
        return this.diskMode;
    }

    public long getDiskUsedInBytes() {
        if (this.diskLruCache == null) {
            return -1;
        }
        return this.diskLruCache.m26320();
    }

    public DualCacheRamMode getRAMMode() {
        return this.ramMode;
    }

    public long getRamUsedInBytes() {
        if (this.ramCacheLru == null) {
            return -1;
        }
        return (long) this.ramCacheLru.size();
    }

    public void invalidate() {
        invalidateDisk();
        invalidateRAM();
    }

    public void invalidateDisk() {
        if (!this.diskMode.equals(DualCacheDiskMode.DISABLE)) {
            try {
                this.dualCacheLock.lockFullDiskWrite();
                this.diskLruCache.m26318();
                openDiskLruCache(this.diskCacheFolder);
            } catch (IOException e) {
                this.logger.logError(e);
            } finally {
                this.dualCacheLock.unLockFullDiskWrite();
            }
        }
    }

    public void invalidateRAM() {
        if (!this.ramMode.equals(DualCacheRamMode.DISABLE)) {
            this.ramCacheLru.evictAll();
        }
    }

    public void put(String str, T t) {
        if (this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_REFERENCE)) {
            this.ramCacheLru.put(str, t);
        }
        String str2 = null;
        if (this.ramMode.equals(DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
            str2 = this.ramSerializer.toString(t);
            this.ramCacheLru.put(str, str2);
        }
        if (this.diskMode.equals(DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
            try {
                this.dualCacheLock.lockDiskEntryWrite(str);
                DiskLruCache.Editor r1 = this.diskLruCache.m26317(str);
                if (this.ramSerializer == this.diskSerializer) {
                    r1.m26329(0, str2);
                } else {
                    r1.m26329(0, this.diskSerializer.toString(t));
                }
                r1.m26328();
            } catch (IOException e) {
                this.logger.logError(e);
            } finally {
                this.dualCacheLock.unLockDiskEntryWrite(str);
            }
        }
    }
}
