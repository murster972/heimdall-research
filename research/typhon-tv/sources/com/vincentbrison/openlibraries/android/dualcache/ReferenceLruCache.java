package com.vincentbrison.openlibraries.android.dualcache;

public class ReferenceLruCache<T> extends RamLruCache<String, T> {
    private SizeOf<T> mHandlerSizeOf;

    public ReferenceLruCache(int i, SizeOf<T> sizeOf) {
        super(i);
        this.mHandlerSizeOf = sizeOf;
    }

    public /* bridge */ /* synthetic */ void resize(int i) {
        super.resize(i);
    }

    /* access modifiers changed from: protected */
    public int sizeOf(String str, T t) {
        return this.mHandlerSizeOf.sizeOf(t);
    }

    public /* bridge */ /* synthetic */ void trimToSize(int i) {
        super.trimToSize(i);
    }
}
