package com.vincentbrison.openlibraries.android.dualcache;

class LoggerHelper {
    private static final String LOG_PREFIX = "Entry for ";
    private final Logger logger;

    LoggerHelper(Logger logger2) {
        this.logger = logger2;
    }

    /* access modifiers changed from: package-private */
    public void logEntryForKeyIsInRam(String str) {
        this.logger.logInfo(LOG_PREFIX + str + " is in RAM.");
    }

    /* access modifiers changed from: package-private */
    public void logEntryForKeyIsNotInRam(String str) {
        this.logger.logInfo(LOG_PREFIX + str + " is not in RAM.");
    }

    /* access modifiers changed from: package-private */
    public void logEntryForKeyIsNotOnDisk(String str) {
        this.logger.logInfo(LOG_PREFIX + str + " is not on disk.");
    }

    /* access modifiers changed from: package-private */
    public void logEntryForKeyIsOnDisk(String str) {
        this.logger.logInfo(LOG_PREFIX + str + " is on disk.");
    }

    /* access modifiers changed from: package-private */
    public void logEntrySavedForKey(String str) {
        this.logger.logInfo(LOG_PREFIX + str + " is saved in cache.");
    }
}
