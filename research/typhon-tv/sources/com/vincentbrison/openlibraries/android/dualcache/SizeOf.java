package com.vincentbrison.openlibraries.android.dualcache;

public interface SizeOf<T> {
    int sizeOf(T t);
}
