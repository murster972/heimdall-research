package com.vincentbrison.openlibraries.android.dualcache;

import java.util.LinkedHashMap;
import java.util.Map;

class RamLruCache<K, V> {
    private int createCount;
    private int evictionCount;
    private int hitCount;
    private final LinkedHashMap<K, V> map;
    private int maxSize;
    private int missCount;
    private int putCount;
    private int size;

    public RamLruCache(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        }
        this.maxSize = i;
        this.map = new LinkedHashMap<>(0, 0.75f, true);
    }

    private int safeSizeOf(K k, V v) {
        int sizeOf = sizeOf(k, v);
        if (sizeOf >= 0) {
            return sizeOf;
        }
        throw new IllegalStateException("Negative size: " + k + "=" + v);
    }

    /* access modifiers changed from: protected */
    public V create(K k) {
        return null;
    }

    public final synchronized int createCount() {
        return this.createCount;
    }

    /* access modifiers changed from: protected */
    public void entryRemoved(boolean z, K k, V v, V v2) {
    }

    public final void evictAll() {
        trimToSize(-1);
    }

    public final synchronized int evictionCount() {
        return this.evictionCount;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        r0 = create(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        if (r0 != null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002b, code lost:
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
        monitor-enter(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r5.createCount++;
        r1 = r5.map.put(r6, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003e, code lost:
        if (r1 == null) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0040, code lost:
        r5.map.put(r6, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0045, code lost:
        monitor-exit(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
        if (r1 == null) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0048, code lost:
        entryRemoved(false, r6, r0, r1);
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r5.size += safeSizeOf(r6, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005c, code lost:
        trimToSize(r5.maxSize);
        r2 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V get(K r6) {
        /*
            r5 = this;
            if (r6 != 0) goto L_0x000b
            java.lang.NullPointerException r3 = new java.lang.NullPointerException
            java.lang.String r4 = "key == null"
            r3.<init>(r4)
            throw r3
        L_0x000b:
            monitor-enter(r5)
            java.util.LinkedHashMap<K, V> r3 = r5.map     // Catch:{ all -> 0x002e }
            java.lang.Object r1 = r3.get(r6)     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x001e
            int r3 = r5.hitCount     // Catch:{ all -> 0x002e }
            int r3 = r3 + 1
            r5.hitCount = r3     // Catch:{ all -> 0x002e }
            monitor-exit(r5)     // Catch:{ all -> 0x002e }
            r2 = r1
            r0 = r1
        L_0x001d:
            return r0
        L_0x001e:
            int r3 = r5.missCount     // Catch:{ all -> 0x002e }
            int r3 = r3 + 1
            r5.missCount = r3     // Catch:{ all -> 0x002e }
            monitor-exit(r5)     // Catch:{ all -> 0x002e }
            java.lang.Object r0 = r5.create(r6)
            if (r0 != 0) goto L_0x0031
            r0 = 0
            r2 = r1
            goto L_0x001d
        L_0x002e:
            r3 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x002e }
            throw r3
        L_0x0031:
            monitor-enter(r5)
            int r3 = r5.createCount     // Catch:{ all -> 0x0059 }
            int r3 = r3 + 1
            r5.createCount = r3     // Catch:{ all -> 0x0059 }
            java.util.LinkedHashMap<K, V> r3 = r5.map     // Catch:{ all -> 0x0059 }
            java.lang.Object r1 = r3.put(r6, r0)     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x004f
            java.util.LinkedHashMap<K, V> r3 = r5.map     // Catch:{ all -> 0x0059 }
            r3.put(r6, r1)     // Catch:{ all -> 0x0059 }
        L_0x0045:
            monitor-exit(r5)     // Catch:{ all -> 0x0059 }
            if (r1 == 0) goto L_0x005c
            r3 = 0
            r5.entryRemoved(r3, r6, r0, r1)
            r2 = r1
            r0 = r1
            goto L_0x001d
        L_0x004f:
            int r3 = r5.size     // Catch:{ all -> 0x0059 }
            int r4 = r5.safeSizeOf(r6, r0)     // Catch:{ all -> 0x0059 }
            int r3 = r3 + r4
            r5.size = r3     // Catch:{ all -> 0x0059 }
            goto L_0x0045
        L_0x0059:
            r3 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0059 }
            throw r3
        L_0x005c:
            int r3 = r5.maxSize
            r5.trimToSize(r3)
            r2 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vincentbrison.openlibraries.android.dualcache.RamLruCache.get(java.lang.Object):java.lang.Object");
    }

    public final synchronized int hitCount() {
        return this.hitCount;
    }

    public final synchronized int maxSize() {
        return this.maxSize;
    }

    public final synchronized int missCount() {
        return this.missCount;
    }

    public final V put(K k, V v) {
        V put;
        if (k == null || v == null) {
            throw new NullPointerException("key == null || value == null");
        }
        synchronized (this) {
            this.putCount++;
            this.size += safeSizeOf(k, v);
            put = this.map.put(k, v);
            if (put != null) {
                this.size -= safeSizeOf(k, put);
            }
        }
        if (put != null) {
            entryRemoved(false, k, put, v);
        }
        trimToSize(this.maxSize);
        return put;
    }

    public final synchronized int putCount() {
        return this.putCount;
    }

    public final V remove(K k) {
        V remove;
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        synchronized (this) {
            remove = this.map.remove(k);
            if (remove != null) {
                this.size -= safeSizeOf(k, remove);
            }
        }
        if (remove != null) {
            entryRemoved(false, k, remove, (V) null);
        }
        return remove;
    }

    public void resize(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        }
        synchronized (this) {
            this.maxSize = i;
        }
        trimToSize(i);
    }

    public final synchronized int size() {
        return this.size;
    }

    /* access modifiers changed from: protected */
    public int sizeOf(K k, V v) {
        return 1;
    }

    public final synchronized Map<K, V> snapshot() {
        return new LinkedHashMap(this.map);
    }

    public final synchronized String toString() {
        String format;
        int i = 0;
        synchronized (this) {
            int i2 = this.hitCount + this.missCount;
            if (i2 != 0) {
                i = (this.hitCount * 100) / i2;
            }
            format = String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", new Object[]{Integer.valueOf(this.maxSize), Integer.valueOf(this.hitCount), Integer.valueOf(this.missCount), Integer.valueOf(i)});
        }
        return format;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.Map$Entry} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void trimToSize(int r9) {
        /*
            r8 = this;
        L_0x0000:
            monitor-enter(r8)
            int r5 = r8.size     // Catch:{ all -> 0x0033 }
            if (r5 < 0) goto L_0x0011
            java.util.LinkedHashMap<K, V> r5 = r8.map     // Catch:{ all -> 0x0033 }
            boolean r5 = r5.isEmpty()     // Catch:{ all -> 0x0033 }
            if (r5 == 0) goto L_0x0036
            int r5 = r8.size     // Catch:{ all -> 0x0033 }
            if (r5 == 0) goto L_0x0036
        L_0x0011:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0033 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0033 }
            r6.<init>()     // Catch:{ all -> 0x0033 }
            java.lang.Class r7 = r8.getClass()     // Catch:{ all -> 0x0033 }
            java.lang.String r7 = r7.getName()     // Catch:{ all -> 0x0033 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0033 }
            java.lang.String r7 = ".sizeOf() is reporting inconsistent results!"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0033 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0033 }
            r5.<init>(r6)     // Catch:{ all -> 0x0033 }
            throw r5     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r5 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0033 }
            throw r5
        L_0x0036:
            int r5 = r8.size     // Catch:{ all -> 0x0033 }
            if (r5 > r9) goto L_0x003c
            monitor-exit(r8)     // Catch:{ all -> 0x0033 }
        L_0x003b:
            return
        L_0x003c:
            r3 = 0
            java.util.LinkedHashMap<K, V> r5 = r8.map     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            java.lang.Class r5 = r5.getClass()     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            java.lang.String r6 = "eldest"
            r7 = 0
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            java.lang.reflect.Method r5 = r5.getMethod(r6, r7)     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            java.util.LinkedHashMap<K, V> r6 = r8.map     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            java.lang.Object r5 = r5.invoke(r6, r7)     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            r0 = r5
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ NoSuchMethodException -> 0x005e, InvocationTargetException -> 0x0063, IllegalAccessException -> 0x0068 }
            r3 = r0
        L_0x005a:
            if (r3 != 0) goto L_0x006d
            monitor-exit(r8)     // Catch:{ all -> 0x0033 }
            goto L_0x003b
        L_0x005e:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0033 }
            goto L_0x005a
        L_0x0063:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0033 }
            goto L_0x005a
        L_0x0068:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0033 }
            goto L_0x005a
        L_0x006d:
            java.lang.Object r2 = r3.getKey()     // Catch:{ all -> 0x0033 }
            java.lang.Object r4 = r3.getValue()     // Catch:{ all -> 0x0033 }
            java.util.LinkedHashMap<K, V> r5 = r8.map     // Catch:{ all -> 0x0033 }
            r5.remove(r2)     // Catch:{ all -> 0x0033 }
            int r5 = r8.size     // Catch:{ all -> 0x0033 }
            int r6 = r8.safeSizeOf(r2, r4)     // Catch:{ all -> 0x0033 }
            int r5 = r5 - r6
            r8.size = r5     // Catch:{ all -> 0x0033 }
            int r5 = r8.evictionCount     // Catch:{ all -> 0x0033 }
            int r5 = r5 + 1
            r8.evictionCount = r5     // Catch:{ all -> 0x0033 }
            monitor-exit(r8)     // Catch:{ all -> 0x0033 }
            r5 = 1
            r6 = 0
            r8.entryRemoved(r5, r2, r4, r6)
            goto L_0x0000
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vincentbrison.openlibraries.android.dualcache.RamLruCache.trimToSize(int):void");
    }
}
