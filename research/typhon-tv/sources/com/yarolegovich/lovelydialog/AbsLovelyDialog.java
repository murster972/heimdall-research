package com.yarolegovich.lovelydialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.yarolegovich.lovelydialog.AbsLovelyDialog;
import com.yarolegovich.lovelydialog.LovelyDialogCompat;

public abstract class AbsLovelyDialog<T extends AbsLovelyDialog> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private TextView f14435;

    /* renamed from: 连任  reason: contains not printable characters */
    private TextView f14436;

    /* renamed from: 靐  reason: contains not printable characters */
    private View f14437;

    /* renamed from: 麤  reason: contains not printable characters */
    private TextView f14438;

    /* renamed from: 齉  reason: contains not printable characters */
    private ImageView f14439;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public Dialog f14440;

    protected class ClickListenerDecorator implements View.OnClickListener {

        /* renamed from: 靐  reason: contains not printable characters */
        private View.OnClickListener f14441;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f14442;

        protected ClickListenerDecorator(View.OnClickListener onClickListener, boolean z) {
            this.f14441 = onClickListener;
            this.f14442 = z;
        }

        public void onClick(View view) {
            if (this.f14441 != null) {
                if (this.f14441 instanceof LovelyDialogCompat.DialogOnClickListenerAdapter) {
                    ((LovelyDialogCompat.DialogOnClickListenerAdapter) this.f14441).m18222(AbsLovelyDialog.this.f14440, view.getId());
                } else {
                    this.f14441.onClick(view);
                }
            }
            if (this.f14442) {
                AbsLovelyDialog.this.m18216();
            }
        }
    }

    public AbsLovelyDialog(Context context) {
        this(context, 0);
    }

    public AbsLovelyDialog(Context context, int i) {
        this(context, i, 0);
    }

    public AbsLovelyDialog(Context context, int i, int i2) {
        i2 = i2 == 0 ? m18219() : i2;
        if (i == 0) {
            m18209(new AlertDialog.Builder(context), i2);
        } else {
            m18209(new AlertDialog.Builder(context, i), i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18209(AlertDialog.Builder builder, int i) {
        this.f14437 = LayoutInflater.from(builder.m529()).inflate(i, (ViewGroup) null);
        this.f14440 = builder.m522(this.f14437).m525();
        this.f14439 = (ImageView) m18211(R.id.ld_icon);
        this.f14436 = (TextView) m18211(R.id.ld_title);
        this.f14435 = (TextView) m18211(R.id.ld_message);
        this.f14438 = (TextView) m18211(R.id.ld_top_title);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public Context m18210() {
        return this.f14437.getContext();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public <ViewClass extends View> ViewClass m18211(int i) {
        return this.f14437.findViewById(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Dialog m18212() {
        this.f14440.show();
        return this.f14440;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public T m18213(int i) {
        m18211(R.id.ld_color_area).setBackgroundColor(i);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public T m18214(CharSequence charSequence) {
        this.f14436.setVisibility(0);
        this.f14436.setText(charSequence);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m18215(int i) {
        return ContextCompat.getColor(m18210(), i);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m18216() {
        this.f14440.dismiss();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Dialog m18217() {
        return this.f14440;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public T m18218(int i) {
        return m18213(m18215(i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m18219();

    /* renamed from: 龘  reason: contains not printable characters */
    public T m18220(int i) {
        this.f14439.setVisibility(0);
        this.f14439.setImageResource(i);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public T m18221(CharSequence charSequence) {
        this.f14435.setVisibility(0);
        this.f14435.setText(charSequence);
        return this;
    }
}
