package com.yarolegovich.lovelydialog;

public class LovelyStandardDialog extends AbsLovelyDialog<LovelyStandardDialog> {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final int f14449 = R.id.ld_btn_no;

    /* renamed from: 齉  reason: contains not printable characters */
    public static final int f14450 = R.id.ld_btn_neutral;

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int f14451 = R.id.ld_btn_yes;

    public enum ButtonLayout {
        HORIZONTAL(R.layout.dialog_standard),
        VERTICAL(R.layout.dialog_standard_vertical);
        
        final int layoutRes;

        private ButtonLayout(int i) {
            this.layoutRes = i;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m18231() {
        return ButtonLayout.HORIZONTAL.layoutRes;
    }
}
