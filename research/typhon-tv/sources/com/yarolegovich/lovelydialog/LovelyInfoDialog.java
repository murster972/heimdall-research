package com.yarolegovich.lovelydialog;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import com.yarolegovich.lovelydialog.AbsLovelyDialog;

public class LovelyInfoDialog extends AbsLovelyDialog<LovelyInfoDialog> {

    /* renamed from: 靐  reason: contains not printable characters */
    private Button f14445 = ((Button) m18211(R.id.ld_btn_confirm));
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public int f14446;
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public CheckBox f14447 = ((CheckBox) m18211(R.id.ld_cb_dont_show_again));

    public LovelyInfoDialog(Context context) {
        super(context);
        this.f14445.setOnClickListener(new AbsLovelyDialog.ClickListenerDecorator((View.OnClickListener) null, true));
        this.f14446 = -1;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static SharedPreferences m18224(Context context) {
        return context.getSharedPreferences("ld_dont_show", 0);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public LovelyInfoDialog m18227(int i) {
        this.f14446 = i;
        this.f14447.setVisibility(0);
        this.f14445.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LovelyInfoDialog.m18224(LovelyInfoDialog.this.m18210()).edit().putBoolean(String.valueOf(LovelyInfoDialog.this.f14446), LovelyInfoDialog.this.f14447.isChecked()).apply();
                LovelyInfoDialog.this.m18216();
            }
        });
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Dialog m18228() {
        boolean z = false;
        if (this.f14446 == -1) {
            return super.m18212();
        }
        if (!m18224(m18210()).getBoolean(String.valueOf(this.f14446), false)) {
            z = true;
        }
        return z ? super.m18212() : super.m18217();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m18229() {
        return R.layout.dialog_info;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public LovelyInfoDialog m18230(boolean z) {
        this.f14447.setChecked(z);
        return this;
    }
}
