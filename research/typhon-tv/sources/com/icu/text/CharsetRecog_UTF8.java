package com.icu.text;

class CharsetRecog_UTF8 extends CharsetRecognizer {
    CharsetRecog_UTF8() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public CharsetMatch m13952(CharsetDetector charsetDetector) {
        int i;
        boolean z = false;
        int i2 = 0;
        int i3 = 0;
        byte[] bArr = charsetDetector.f11259;
        if (charsetDetector.f11255 >= 3 && (bArr[0] & 255) == 239 && (bArr[1] & 255) == 187 && (bArr[2] & 255) == 191) {
            z = true;
        }
        int i4 = 0;
        while (i4 < charsetDetector.f11255) {
            byte b = bArr[i4];
            if ((b & 128) != 0) {
                if ((b & 224) == 192) {
                    i = 1;
                } else if ((b & 240) == 224) {
                    i = 2;
                } else if ((b & 248) == 240) {
                    i = 3;
                } else {
                    i3++;
                }
                while (true) {
                    i4++;
                    if (i4 < charsetDetector.f11255) {
                        if ((bArr[i4] & 192) == 128) {
                            i--;
                            if (i == 0) {
                                i2++;
                                break;
                            }
                        } else {
                            i3++;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            i4++;
        }
        int i5 = 0;
        if (z && i3 == 0) {
            i5 = 100;
        } else if (z && i2 > i3 * 10) {
            i5 = 80;
        } else if (i2 > 3 && i3 == 0) {
            i5 = 100;
        } else if (i2 > 0 && i3 == 0) {
            i5 = 80;
        } else if (i2 == 0 && i3 == 0) {
            i5 = 15;
        } else if (i2 > i3 * 10) {
            i5 = 25;
        }
        if (i5 == 0) {
            return null;
        }
        return new CharsetMatch(charsetDetector, this, i5);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m13953() {
        return "UTF-8";
    }
}
