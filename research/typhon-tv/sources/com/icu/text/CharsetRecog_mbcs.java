package com.icu.text;

import java.util.Arrays;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

abstract class CharsetRecog_mbcs extends CharsetRecognizer {

    static class CharsetRecog_big5 extends CharsetRecog_mbcs {

        /* renamed from: 龘  reason: contains not printable characters */
        static int[] f11275 = {41280, 41281, 41282, 41283, 41287, 41289, 41333, 41334, 42048, 42054, 42055, 42056, 42065, 42068, 42071, 42084, 42090, 42092, 42103, 42147, 42148, 42151, 42177, 42190, 42193, 42207, 42216, 42237, 42304, 42312, 42328, 42345, 42445, 42471, 42583, 42593, 42594, 42600, 42608, 42664, 42675, 42681, 42707, 42715, 42726, 42738, 42816, 42833, 42841, 42970, 43171, 43173, 43181, 43217, 43219, 43236, 43260, 43456, 43474, 43507, 43627, 43706, 43710, 43724, 43772, 44103, 44111, 44208, 44242, 44377, 44745, 45024, 45290, 45423, 45747, 45764, 45935, 46156, 46158, 46412, 46501, 46525, 46544, 46552, 46705, 47085, 47207, 47428, 47832, 47940, 48033, 48593, 49860, 50105, 50240, 50271};

        CharsetRecog_big5() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m13972() {
            return "zh";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13973(CharsetDetector charsetDetector) {
            int r0 = m13969(charsetDetector, f11275);
            if (r0 == 0) {
                return null;
            }
            return new CharsetMatch(charsetDetector, this, r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13974() {
            return "Big5";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m13975(iteratedChar iteratedchar, CharsetDetector charsetDetector) {
            iteratedchar.f11281 = iteratedchar.f11283;
            iteratedchar.f11282 = false;
            int r0 = iteratedchar.m13991(charsetDetector);
            iteratedchar.f11284 = r0;
            if (r0 < 0) {
                return false;
            }
            if (r0 <= 127 || r0 == 255) {
                return true;
            }
            int r1 = iteratedchar.m13991(charsetDetector);
            if (r1 < 0) {
                return false;
            }
            iteratedchar.f11284 = (iteratedchar.f11284 << 8) | r1;
            if (r1 < 64 || r1 == 127 || r1 == 255) {
                iteratedchar.f11282 = true;
            }
            return true;
        }
    }

    static abstract class CharsetRecog_euc extends CharsetRecog_mbcs {

        static class CharsetRecog_euc_jp extends CharsetRecog_euc {

            /* renamed from: 龘  reason: contains not printable characters */
            static int[] f11276 = {41377, 41378, 41379, 41382, 41404, 41418, 41419, 41430, 41431, 42146, 42148, 42150, 42152, 42154, 42155, 42156, 42157, 42159, 42161, 42163, 42165, 42167, 42169, 42171, 42173, 42175, 42176, 42177, 42179, 42180, 42182, 42183, 42184, 42185, 42186, 42187, 42190, 42191, 42192, 42206, 42207, 42209, 42210, 42212, 42216, 42217, 42218, 42219, 42220, 42223, 42226, 42227, 42402, 42403, 42404, 42406, 42407, 42410, 42413, 42415, 42416, 42419, 42421, 42423, 42424, 42425, 42431, 42435, 42438, 42439, 42440, 42441, 42443, 42448, 42453, 42454, 42455, 42462, 42464, 42465, 42469, 42473, 42474, 42475, 42476, 42477, 42483, 47273, 47572, 47854, 48072, 48880, 49079, 50410, 50940, 51133, 51896, 51955, 52188, 52689};

            CharsetRecog_euc_jp() {
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public String m13977() {
                return "ja";
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public CharsetMatch m13978(CharsetDetector charsetDetector) {
                int r0 = m13969(charsetDetector, f11276);
                if (r0 == 0) {
                    return null;
                }
                return new CharsetMatch(charsetDetector, this, r0);
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public String m13979() {
                return "EUC-JP";
            }
        }

        static class CharsetRecog_euc_kr extends CharsetRecog_euc {

            /* renamed from: 龘  reason: contains not printable characters */
            static int[] f11277 = {45217, 45235, 45253, 45261, 45268, 45286, 45293, 45304, 45306, 45308, 45496, 45497, 45511, 45527, 45538, 45994, 46011, 46274, 46287, 46297, 46315, 46501, 46517, 46527, 46535, 46569, 46835, 47023, 47042, 47054, 47270, 47278, 47286, 47288, 47291, 47337, 47531, 47534, 47564, 47566, 47613, 47800, 47822, 47824, 47857, 48103, 48115, 48125, 48301, 48314, 48338, 48374, 48570, 48576, 48579, 48581, 48838, 48840, 48863, 48878, 48888, 48890, 49057, 49065, 49088, 49124, 49131, 49132, 49144, 49319, 49327, 49336, 49338, 49339, 49341, 49351, 49356, 49358, 49359, 49366, 49370, 49381, 49403, 49404, 49572, 49574, 49590, 49622, 49631, 49654, 49656, 50337, 50637, 50862, 51151, 51153, 51154, 51160, 51173, 51373};

            CharsetRecog_euc_kr() {
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public String m13980() {
                return "ko";
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public CharsetMatch m13981(CharsetDetector charsetDetector) {
                int r0 = m13969(charsetDetector, f11277);
                if (r0 == 0) {
                    return null;
                }
                return new CharsetMatch(charsetDetector, this, r0);
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public String m13982() {
                return "EUC-KR";
            }
        }

        CharsetRecog_euc() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m13976(iteratedChar iteratedchar, CharsetDetector charsetDetector) {
            iteratedchar.f11281 = iteratedchar.f11283;
            iteratedchar.f11282 = false;
            int r0 = iteratedchar.m13991(charsetDetector);
            iteratedchar.f11284 = r0;
            if (r0 < 0) {
                iteratedchar.f11280 = true;
            } else if (r0 > 141) {
                int r1 = iteratedchar.m13991(charsetDetector);
                iteratedchar.f11284 = (iteratedchar.f11284 << 8) | r1;
                if (r0 < 161 || r0 > 254) {
                    if (r0 == 142) {
                        if (r1 < 161) {
                            iteratedchar.f11282 = true;
                        }
                    } else if (r0 == 143) {
                        int r2 = iteratedchar.m13991(charsetDetector);
                        iteratedchar.f11284 = (iteratedchar.f11284 << 8) | r2;
                        if (r2 < 161) {
                            iteratedchar.f11282 = true;
                        }
                    }
                } else if (r1 < 161) {
                    iteratedchar.f11282 = true;
                }
            }
            return !iteratedchar.f11280;
        }
    }

    static class CharsetRecog_gb_18030 extends CharsetRecog_mbcs {

        /* renamed from: 龘  reason: contains not printable characters */
        static int[] f11278 = {41377, 41378, 41379, 41380, 41392, 41393, 41457, 41459, 41889, 41900, 41914, 45480, 45496, 45502, 45755, 46025, 46070, 46323, 46525, 46532, 46563, 46767, 46804, 46816, 47010, 47016, 47037, 47062, 47069, 47284, 47327, 47350, 47531, 47561, 47576, 47610, 47613, 47821, 48039, 48086, 48097, 48122, 48316, 48347, 48382, 48588, 48845, 48861, 49076, 49094, 49097, 49332, 49389, 49611, 49883, 50119, 50396, 50410, 50636, 50935, 51192, 51371, 51403, 51413, 51431, 51663, 51706, 51889, 51893, 51911, 51920, 51926, 51957, 51965, 52460, 52728, 52906, 52932, 52946, 52965, 53173, 53186, 53206, 53442, 53445, 53456, 53460, 53671, 53930, 53938, 53941, 53947, 53972, 54211, 54224, 54269, 54466, 54490, 54754, 54992};

        CharsetRecog_gb_18030() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m13983() {
            return "zh";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13984(CharsetDetector charsetDetector) {
            int r0 = m13969(charsetDetector, f11278);
            if (r0 == 0) {
                return null;
            }
            return new CharsetMatch(charsetDetector, this, r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13985() {
            return "GB18030";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m13986(iteratedChar iteratedchar, CharsetDetector charsetDetector) {
            int r3;
            int r1;
            iteratedchar.f11281 = iteratedchar.f11283;
            iteratedchar.f11282 = false;
            int r0 = iteratedchar.m13991(charsetDetector);
            iteratedchar.f11284 = r0;
            if (r0 < 0) {
                iteratedchar.f11280 = true;
            } else if (r0 > 128) {
                int r2 = iteratedchar.m13991(charsetDetector);
                iteratedchar.f11284 = (iteratedchar.f11284 << 8) | r2;
                if (r0 >= 129 && r0 <= 254 && ((r2 < 64 || r2 > 126) && (r2 < 80 || r2 > 254))) {
                    if (r2 < 48 || r2 > 57 || (r3 = iteratedchar.m13991(charsetDetector)) < 129 || r3 > 254 || (r1 = iteratedchar.m13991(charsetDetector)) < 48 || r1 > 57) {
                        iteratedchar.f11282 = true;
                    } else {
                        iteratedchar.f11284 = (iteratedchar.f11284 << 16) | (r3 << 8) | r1;
                    }
                }
            }
            return !iteratedchar.f11280;
        }
    }

    static class CharsetRecog_sjis extends CharsetRecog_mbcs {

        /* renamed from: 龘  reason: contains not printable characters */
        static int[] f11279 = {33088, 33089, 33090, 33093, 33115, 33129, 33130, 33141, 33142, 33440, 33442, 33444, 33449, 33450, 33451, 33453, 33455, 33457, 33459, 33461, 33463, 33469, 33470, 33473, 33476, 33477, 33478, 33480, 33481, 33484, 33485, 33500, 33504, 33511, 33512, 33513, 33514, 33520, 33521, 33601, 33603, 33614, 33615, 33624, 33630, 33634, 33639, 33653, 33654, 33673, 33674, 33675, 33677, 33683, 36502, 37882, 38314};

        CharsetRecog_sjis() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m13987() {
            return "ja";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13988(CharsetDetector charsetDetector) {
            int r0 = m13969(charsetDetector, f11279);
            if (r0 == 0) {
                return null;
            }
            return new CharsetMatch(charsetDetector, this, r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13989() {
            return "Shift_JIS";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m13990(iteratedChar iteratedchar, CharsetDetector charsetDetector) {
            iteratedchar.f11281 = iteratedchar.f11283;
            iteratedchar.f11282 = false;
            int r0 = iteratedchar.m13991(charsetDetector);
            iteratedchar.f11284 = r0;
            if (r0 < 0) {
                return false;
            }
            if (r0 <= 127 || (r0 > 160 && r0 <= 223)) {
                return true;
            }
            int r1 = iteratedchar.m13991(charsetDetector);
            if (r1 < 0) {
                return false;
            }
            iteratedchar.f11284 = (r0 << 8) | r1;
            if ((r1 < 64 || r1 > 127) && (r1 < 128 || r1 > 255)) {
                iteratedchar.f11282 = true;
            }
            return true;
        }
    }

    static class iteratedChar {

        /* renamed from: 连任  reason: contains not printable characters */
        boolean f11280 = false;

        /* renamed from: 靐  reason: contains not printable characters */
        int f11281 = 0;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f11282 = false;

        /* renamed from: 齉  reason: contains not printable characters */
        int f11283 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        int f11284 = 0;

        iteratedChar() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m13991(CharsetDetector charsetDetector) {
            if (this.f11283 >= charsetDetector.f11255) {
                this.f11280 = true;
                return -1;
            }
            byte[] bArr = charsetDetector.f11259;
            int i = this.f11283;
            this.f11283 = i + 1;
            return bArr[i] & 255;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m13992() {
            this.f11284 = 0;
            this.f11281 = -1;
            this.f11283 = 0;
            this.f11282 = false;
            this.f11280 = false;
        }
    }

    CharsetRecog_mbcs() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m13969(CharsetDetector charsetDetector, int[] iArr) {
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        iteratedChar iteratedchar = new iteratedChar();
        iteratedchar.m13992();
        while (m13971(iteratedchar, charsetDetector)) {
            i5++;
            if (iteratedchar.f11282) {
                i4++;
            } else {
                long j = ((long) iteratedchar.f11284) & InternalZipTyphoonApp.ZIP_64_LIMIT;
                if (j <= 255) {
                    i++;
                } else {
                    i2++;
                    if (iArr != null) {
                        if (Arrays.binarySearch(iArr, (int) j) >= 0) {
                            i3++;
                        }
                    }
                }
            }
            if (i4 >= 2 && i4 * 5 >= i2) {
                return 0;
            }
        }
        if (i2 <= 10 && i4 == 0) {
            return (i2 != 0 || i5 >= 10) ? 10 : 0;
        }
        if (i2 < i4 * 20) {
            return 0;
        }
        if (iArr == null) {
            int i6 = (i2 + 30) - (i4 * 20);
            if (i6 > 100) {
                return 100;
            }
            return i6;
        }
        return Math.min((int) ((Math.log((double) (i3 + 1)) * (90.0d / Math.log((double) (((float) i2) / 4.0f)))) + 10.0d), 100);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m13970();

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m13971(iteratedChar iteratedchar, CharsetDetector charsetDetector);
}
