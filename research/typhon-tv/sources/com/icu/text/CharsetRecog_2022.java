package com.icu.text;

abstract class CharsetRecog_2022 extends CharsetRecognizer {

    static class CharsetRecog_2022CN extends CharsetRecog_2022 {

        /* renamed from: 龘  reason: contains not printable characters */
        private byte[][] f11272 = {new byte[]{27, 36, 41, 65}, new byte[]{27, 36, 41, 71}, new byte[]{27, 36, 42, 72}, new byte[]{27, 36, 41, 69}, new byte[]{27, 36, 43, 73}, new byte[]{27, 36, 43, 74}, new byte[]{27, 36, 43, 75}, new byte[]{27, 36, 43, 76}, new byte[]{27, 36, 43, 77}, new byte[]{27, 78}, new byte[]{27, 79}};

        CharsetRecog_2022CN() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13946(CharsetDetector charsetDetector) {
            int r0 = m13945(charsetDetector.f11263, charsetDetector.f11260, this.f11272);
            if (r0 == 0) {
                return null;
            }
            return new CharsetMatch(charsetDetector, this, r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13947() {
            return "ISO-2022-CN";
        }
    }

    static class CharsetRecog_2022JP extends CharsetRecog_2022 {

        /* renamed from: 龘  reason: contains not printable characters */
        private byte[][] f11273 = {new byte[]{27, 36, 40, 67}, new byte[]{27, 36, 40, 68}, new byte[]{27, 36, 64}, new byte[]{27, 36, 65}, new byte[]{27, 36, 66}, new byte[]{27, 38, 64}, new byte[]{27, 40, 66}, new byte[]{27, 40, 72}, new byte[]{27, 40, 73}, new byte[]{27, 40, 74}, new byte[]{27, 46, 65}, new byte[]{27, 46, 70}};

        CharsetRecog_2022JP() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13948(CharsetDetector charsetDetector) {
            int r0 = m13945(charsetDetector.f11263, charsetDetector.f11260, this.f11273);
            if (r0 == 0) {
                return null;
            }
            return new CharsetMatch(charsetDetector, this, r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13949() {
            return "ISO-2022-JP";
        }
    }

    static class CharsetRecog_2022KR extends CharsetRecog_2022 {

        /* renamed from: 龘  reason: contains not printable characters */
        private byte[][] f11274 = {new byte[]{27, 36, 41, 67}};

        CharsetRecog_2022KR() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13950(CharsetDetector charsetDetector) {
            int r0 = m13945(charsetDetector.f11263, charsetDetector.f11260, this.f11274);
            if (r0 == 0) {
                return null;
            }
            return new CharsetMatch(charsetDetector, this, r0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13951() {
            return "ISO-2022-KR";
        }
    }

    CharsetRecog_2022() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m13945(byte[] bArr, int i, byte[][] bArr2) {
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i5 < i) {
            if (bArr[i5] == 27) {
                for (byte[] bArr3 : bArr2) {
                    if (i - i5 >= bArr3.length) {
                        int i6 = 1;
                        while (i6 < bArr3.length) {
                            if (bArr3[i6] == bArr[i5 + i6]) {
                                i6++;
                            }
                        }
                        i2++;
                        i5 += bArr3.length - 1;
                        break;
                    }
                }
                i3++;
            }
            if (bArr[i5] == 14 || bArr[i5] == 15) {
                i4++;
                i5++;
            } else {
                i5++;
            }
        }
        if (i2 == 0) {
            return 0;
        }
        int i7 = ((i2 * 100) - (i3 * 100)) / (i2 + i3);
        if (i2 + i4 < 5) {
            i7 -= (5 - (i2 + i4)) * 10;
        }
        if (i7 < 0) {
            return 0;
        }
        return i7;
    }
}
