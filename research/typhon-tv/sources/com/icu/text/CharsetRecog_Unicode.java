package com.icu.text;

abstract class CharsetRecog_Unicode extends CharsetRecognizer {

    static class CharsetRecog_UTF_16_BE extends CharsetRecog_Unicode {
        CharsetRecog_UTF_16_BE() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13958(CharsetDetector charsetDetector) {
            byte[] bArr = charsetDetector.f11259;
            int i = 10;
            int min = Math.min(bArr.length, 30);
            int i2 = 0;
            while (true) {
                if (i2 >= min - 1) {
                    break;
                }
                int r2 = m13954(bArr[i2], bArr[i2 + 1]);
                if (i2 != 0 || r2 != 65279) {
                    i = m13955(r2, i);
                    if (i == 0 || i == 100) {
                        break;
                    }
                    i2 += 2;
                } else {
                    i = 100;
                    break;
                }
            }
            if (min < 4 && i < 100) {
                i = 0;
            }
            if (i > 0) {
                return new CharsetMatch(charsetDetector, this, i);
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13959() {
            return "UTF-16BE";
        }
    }

    static class CharsetRecog_UTF_16_LE extends CharsetRecog_Unicode {
        CharsetRecog_UTF_16_LE() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13960(CharsetDetector charsetDetector) {
            byte[] bArr = charsetDetector.f11259;
            int i = 10;
            int min = Math.min(bArr.length, 30);
            int i2 = 0;
            while (true) {
                if (i2 >= min - 1) {
                    break;
                }
                int r2 = m13954(bArr[i2 + 1], bArr[i2]);
                if (i2 != 0 || r2 != 65279) {
                    i = m13955(r2, i);
                    if (i == 0 || i == 100) {
                        break;
                    }
                    i2 += 2;
                } else {
                    i = 100;
                    break;
                }
            }
            if (min < 4 && i < 100) {
                i = 0;
            }
            if (i > 0) {
                return new CharsetMatch(charsetDetector, this, i);
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13961() {
            return "UTF-16LE";
        }
    }

    static abstract class CharsetRecog_UTF_32 extends CharsetRecog_Unicode {
        CharsetRecog_UTF_32() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract int m13962(byte[] bArr, int i);

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public CharsetMatch m13963(CharsetDetector charsetDetector) {
            byte[] bArr = charsetDetector.f11259;
            int i = (charsetDetector.f11255 / 4) * 4;
            int i2 = 0;
            int i3 = 0;
            boolean z = false;
            int i4 = 0;
            if (i == 0) {
                return null;
            }
            if (m13962(bArr, 0) == 65279) {
                z = true;
            }
            for (int i5 = 0; i5 < i; i5 += 4) {
                int r0 = m13962(bArr, i5);
                if (r0 < 0 || r0 >= 1114111 || (r0 >= 55296 && r0 <= 57343)) {
                    i3++;
                } else {
                    i2++;
                }
            }
            if (z && i3 == 0) {
                i4 = 100;
            } else if (z && i2 > i3 * 10) {
                i4 = 80;
            } else if (i2 > 3 && i3 == 0) {
                i4 = 100;
            } else if (i2 > 0 && i3 == 0) {
                i4 = 80;
            } else if (i2 > i3 * 10) {
                i4 = 25;
            }
            if (i4 != 0) {
                return new CharsetMatch(charsetDetector, this, i4);
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract String m13964();
    }

    static class CharsetRecog_UTF_32_BE extends CharsetRecog_UTF_32 {
        CharsetRecog_UTF_32_BE() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m13965(byte[] bArr, int i) {
            return ((bArr[i + 0] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8) | (bArr[i + 3] & 255);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13966() {
            return "UTF-32BE";
        }
    }

    static class CharsetRecog_UTF_32_LE extends CharsetRecog_UTF_32 {
        CharsetRecog_UTF_32_LE() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m13967(byte[] bArr, int i) {
            return ((bArr[i + 3] & 255) << 24) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 1] & 255) << 8) | (bArr[i + 0] & 255);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m13968() {
            return "UTF-32LE";
        }
    }

    CharsetRecog_Unicode() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m13954(byte b, byte b2) {
        return ((b & 255) << 8) | (b2 & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m13955(int i, int i2) {
        if (i == 0) {
            i2 -= 10;
        } else if ((i >= 32 && i <= 255) || i == 10) {
            i2 += 10;
        }
        if (i2 < 0) {
            return 0;
        }
        if (i2 > 100) {
            return 100;
        }
        return i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract CharsetMatch m13956(CharsetDetector charsetDetector);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m13957();
}
