package com.icu.text;

import com.icu.text.CharsetRecog_2022;
import com.icu.text.CharsetRecog_Unicode;
import com.icu.text.CharsetRecog_mbcs;
import com.icu.text.CharsetRecog_sbcs;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CharsetDetector {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final List<CSRecognizerInfo> f11254;

    /* renamed from: ʻ  reason: contains not printable characters */
    int f11255;

    /* renamed from: ʼ  reason: contains not printable characters */
    InputStream f11256;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f11257 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean[] f11258;

    /* renamed from: 连任  reason: contains not printable characters */
    byte[] f11259;

    /* renamed from: 靐  reason: contains not printable characters */
    int f11260;

    /* renamed from: 麤  reason: contains not printable characters */
    boolean f11261 = false;

    /* renamed from: 齉  reason: contains not printable characters */
    short[] f11262 = new short[256];

    /* renamed from: 龘  reason: contains not printable characters */
    byte[] f11263 = new byte[8000];

    private static class CSRecognizerInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f11264;

        /* renamed from: 龘  reason: contains not printable characters */
        CharsetRecognizer f11265;

        CSRecognizerInfo(CharsetRecognizer charsetRecognizer, boolean z) {
            this.f11265 = charsetRecognizer;
            this.f11264 = z;
        }
    }

    static {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_UTF8(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_Unicode.CharsetRecog_UTF_16_BE(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_Unicode.CharsetRecog_UTF_16_LE(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_Unicode.CharsetRecog_UTF_32_BE(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_Unicode.CharsetRecog_UTF_32_LE(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_mbcs.CharsetRecog_sjis(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_2022.CharsetRecog_2022JP(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_2022.CharsetRecog_2022CN(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_2022.CharsetRecog_2022KR(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_mbcs.CharsetRecog_gb_18030(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_mbcs.CharsetRecog_euc.CharsetRecog_euc_jp(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_mbcs.CharsetRecog_euc.CharsetRecog_euc_kr(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_mbcs.CharsetRecog_big5(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_1(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_2(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_5_ru(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_6_ar(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_7_el(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_8_I_he(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_8_he(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_windows_1251(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_windows_1256(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_KOI8_R(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_8859_9_tr(), true));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_IBM424_he_rtl(), false));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_IBM424_he_ltr(), false));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_IBM420_ar_rtl(), false));
        arrayList.add(new CSRecognizerInfo(new CharsetRecog_sbcs.CharsetRecog_IBM420_ar_ltr(), false));
        f11254 = Collections.unmodifiableList(arrayList);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m13939() {
        int i = 0;
        boolean z = false;
        int i2 = 0;
        int i3 = 0;
        if (this.f11257) {
            for (int i4 = 0; i4 < this.f11255 && i < this.f11263.length; i4++) {
                byte b = this.f11259[i4];
                if (b == 60) {
                    if (z) {
                        i3++;
                    }
                    z = true;
                    i2++;
                }
                if (!z) {
                    this.f11263[i] = b;
                    i++;
                }
                if (b == 62) {
                    z = false;
                }
            }
            this.f11260 = i;
        }
        if (i2 < 5 || i2 / 5 < i3 || (this.f11260 < 100 && this.f11255 > 600)) {
            int i5 = this.f11255;
            if (i5 > 8000) {
                i5 = 8000;
            }
            int i6 = 0;
            while (i6 < i5) {
                this.f11263[i6] = this.f11259[i6];
                i6++;
            }
            this.f11260 = i6;
        }
        Arrays.fill(this.f11262, 0);
        for (int i7 = 0; i7 < this.f11260; i7++) {
            byte b2 = this.f11263[i7] & 255;
            short[] sArr = this.f11262;
            sArr[b2] = (short) (sArr[b2] + 1);
        }
        this.f11261 = false;
        for (int i8 = 128; i8 <= 159; i8++) {
            if (this.f11262[i8] != 0) {
                this.f11261 = true;
                return;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public CharsetMatch[] m13940() {
        CharsetMatch r2;
        ArrayList arrayList = new ArrayList();
        m13939();
        for (int i = 0; i < f11254.size(); i++) {
            CSRecognizerInfo cSRecognizerInfo = f11254.get(i);
            if ((this.f11258 != null ? this.f11258[i] : cSRecognizerInfo.f11264) && (r2 = cSRecognizerInfo.f11265.m14048(this)) != null) {
                arrayList.add(r2);
            }
        }
        Collections.sort(arrayList);
        Collections.reverse(arrayList);
        return (CharsetMatch[]) arrayList.toArray(new CharsetMatch[arrayList.size()]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CharsetDetector m13941(byte[] bArr) {
        this.f11259 = bArr;
        this.f11255 = bArr.length;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CharsetMatch m13942() {
        CharsetMatch[] r0 = m13940();
        if (r0 == null || r0.length == 0) {
            return null;
        }
        return r0[0];
    }
}
