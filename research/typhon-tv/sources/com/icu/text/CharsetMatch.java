package com.icu.text;

import java.io.InputStream;

public class CharsetMatch implements Comparable<CharsetMatch> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f11266;

    /* renamed from: 连任  reason: contains not printable characters */
    private String f11267;

    /* renamed from: 靐  reason: contains not printable characters */
    private byte[] f11268 = null;

    /* renamed from: 麤  reason: contains not printable characters */
    private InputStream f11269 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f11270;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f11271;

    CharsetMatch(CharsetDetector charsetDetector, CharsetRecognizer charsetRecognizer, int i) {
        this.f11271 = i;
        if (charsetDetector.f11256 == null) {
            this.f11268 = charsetDetector.f11259;
            this.f11270 = charsetDetector.f11255;
        }
        this.f11269 = charsetDetector.f11256;
        this.f11267 = charsetRecognizer.m14049();
        this.f11266 = charsetRecognizer.m14047();
    }

    CharsetMatch(CharsetDetector charsetDetector, CharsetRecognizer charsetRecognizer, int i, String str, String str2) {
        this.f11271 = i;
        if (charsetDetector.f11256 == null) {
            this.f11268 = charsetDetector.f11259;
            this.f11270 = charsetDetector.f11255;
        }
        this.f11269 = charsetDetector.f11256;
        this.f11267 = str;
        this.f11266 = str2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int compareTo(CharsetMatch charsetMatch) {
        if (this.f11271 > charsetMatch.f11271) {
            return 1;
        }
        return this.f11271 < charsetMatch.f11271 ? -1 : 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m13944() {
        return this.f11267;
    }
}
