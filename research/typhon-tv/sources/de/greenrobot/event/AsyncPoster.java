package de.greenrobot.event;

class AsyncPoster implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final EventBus f14460;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PendingPostQueue f14461 = new PendingPostQueue();

    AsyncPoster(EventBus eventBus) {
        this.f14460 = eventBus;
    }

    public void run() {
        PendingPost r0 = this.f14461.m18258();
        if (r0 == null) {
            throw new IllegalStateException("No pending post available");
        }
        this.f14460.m18251(r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18239(Subscription subscription, Object obj) {
        this.f14461.m18260(PendingPost.m18256(subscription, obj));
        EventBus.f14467.execute(this);
    }
}
