package de.greenrobot.event;

final class PendingPostQueue {

    /* renamed from: 靐  reason: contains not printable characters */
    private PendingPost f14496;

    /* renamed from: 龘  reason: contains not printable characters */
    private PendingPost f14497;

    PendingPostQueue() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized PendingPost m18258() {
        PendingPost pendingPost;
        pendingPost = this.f14497;
        if (this.f14497 != null) {
            this.f14497 = this.f14497.f14494;
            if (this.f14497 == null) {
                this.f14496 = null;
            }
        }
        return pendingPost;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized PendingPost m18259(int i) throws InterruptedException {
        if (this.f14497 == null) {
            wait((long) i);
        }
        return m18258();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m18260(PendingPost pendingPost) {
        if (pendingPost == null) {
            throw new NullPointerException("null cannot be enqueued");
        }
        if (this.f14496 != null) {
            this.f14496.f14494 = pendingPost;
            this.f14496 = pendingPost;
        } else if (this.f14497 == null) {
            this.f14496 = pendingPost;
            this.f14497 = pendingPost;
        } else {
            throw new IllegalStateException("Head present, but no tail");
        }
        notifyAll();
    }
}
