package de.greenrobot.event;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

final class HandlerPoster extends Handler {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f14486;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f14487;

    /* renamed from: 齉  reason: contains not printable characters */
    private final EventBus f14488;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PendingPostQueue f14489 = new PendingPostQueue();

    HandlerPoster(EventBus eventBus, Looper looper, int i) {
        super(looper);
        this.f14488 = eventBus;
        this.f14486 = i;
    }

    public void handleMessage(Message message) {
        try {
            long uptimeMillis = SystemClock.uptimeMillis();
            do {
                PendingPost r0 = this.f14489.m18258();
                if (r0 == null) {
                    synchronized (this) {
                        r0 = this.f14489.m18258();
                        if (r0 == null) {
                            this.f14487 = false;
                            this.f14487 = false;
                            return;
                        }
                    }
                }
                this.f14488.m18251(r0);
            } while (SystemClock.uptimeMillis() - uptimeMillis < ((long) this.f14486));
            if (!sendMessage(obtainMessage())) {
                throw new EventBusException("Could not send handler message");
            }
            this.f14487 = true;
        } catch (Throwable th) {
            this.f14487 = false;
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18255(Subscription subscription, Object obj) {
        PendingPost r0 = PendingPost.m18256(subscription, obj);
        synchronized (this) {
            this.f14489.m18260(r0);
            if (!this.f14487) {
                this.f14487 = true;
                if (!sendMessage(obtainMessage())) {
                    throw new EventBusException("Could not send handler message");
                }
            }
        }
    }
}
