package de.greenrobot.event;

import java.util.ArrayList;
import java.util.List;

final class PendingPost {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final List<PendingPost> f14492 = new ArrayList();

    /* renamed from: 靐  reason: contains not printable characters */
    Subscription f14493;

    /* renamed from: 齉  reason: contains not printable characters */
    PendingPost f14494;

    /* renamed from: 龘  reason: contains not printable characters */
    Object f14495;

    private PendingPost(Object obj, Subscription subscription) {
        this.f14495 = obj;
        this.f14493 = subscription;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static PendingPost m18256(Subscription subscription, Object obj) {
        synchronized (f14492) {
            int size = f14492.size();
            if (size <= 0) {
                return new PendingPost(obj, subscription);
            }
            PendingPost remove = f14492.remove(size - 1);
            remove.f14495 = obj;
            remove.f14493 = subscription;
            remove.f14494 = null;
            return remove;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m18257(PendingPost pendingPost) {
        pendingPost.f14495 = null;
        pendingPost.f14493 = null;
        pendingPost.f14494 = null;
        synchronized (f14492) {
            if (f14492.size() < 10000) {
                f14492.add(pendingPost);
            }
        }
    }
}
