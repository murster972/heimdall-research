package de.greenrobot.event;

final class Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    final SubscriberMethod f14508;

    /* renamed from: 麤  reason: contains not printable characters */
    volatile boolean f14509 = true;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f14510;

    /* renamed from: 龘  reason: contains not printable characters */
    final Object f14511;

    Subscription(Object obj, SubscriberMethod subscriberMethod, int i) {
        this.f14511 = obj;
        this.f14508 = subscriberMethod;
        this.f14510 = i;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Subscription)) {
            return false;
        }
        Subscription subscription = (Subscription) obj;
        return this.f14511 == subscription.f14511 && this.f14508.equals(subscription.f14508);
    }

    public int hashCode() {
        return this.f14511.hashCode() + this.f14508.f14503.hashCode();
    }
}
