package de.greenrobot.event;

import android.util.Log;
import com.evernote.android.state.StateSaver;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class SubscriberMethodFinder {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Map<Class<?>, Class<?>> f14506 = new ConcurrentHashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Map<String, List<SubscriberMethod>> f14507 = new HashMap();

    SubscriberMethodFinder() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<SubscriberMethod> m18262(Class<?> cls, String str) {
        List<SubscriberMethod> list;
        ThreadMode threadMode;
        String str2 = cls.getName() + '.' + str;
        synchronized (f14507) {
            list = f14507.get(str2);
        }
        if (list != null) {
            List<SubscriberMethod> list2 = list;
            return list;
        }
        ArrayList arrayList = new ArrayList();
        HashSet hashSet = new HashSet();
        StringBuilder sb = new StringBuilder();
        for (Class<?> cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
            String name = cls2.getName();
            if (name.startsWith(StateSaver.JAVA_PREFIX) || name.startsWith("javax.") || name.startsWith(StateSaver.ANDROID_PREFIX)) {
                break;
            }
            for (Method method : cls2.getMethods()) {
                String name2 = method.getName();
                if (name2.startsWith(str)) {
                    int modifiers = method.getModifiers();
                    if ((modifiers & 1) != 0 && (modifiers & 1032) == 0) {
                        Class[] parameterTypes = method.getParameterTypes();
                        if (parameterTypes.length == 1) {
                            String substring = name2.substring(str.length());
                            if (substring.length() == 0) {
                                threadMode = ThreadMode.PostThread;
                            } else if (substring.equals("MainThread")) {
                                threadMode = ThreadMode.MainThread;
                            } else if (substring.equals("BackgroundThread")) {
                                threadMode = ThreadMode.BackgroundThread;
                            } else if (substring.equals("Async")) {
                                threadMode = ThreadMode.Async;
                            } else if (!f14506.containsKey(cls2)) {
                                throw new EventBusException("Illegal onEvent method, check for typos: " + method);
                            }
                            Class cls3 = parameterTypes[0];
                            sb.setLength(0);
                            sb.append(name2);
                            sb.append('>').append(cls3.getName());
                            if (hashSet.add(sb.toString())) {
                                arrayList.add(new SubscriberMethod(method, threadMode, cls3));
                            }
                        } else {
                            continue;
                        }
                    } else if (!f14506.containsKey(cls2)) {
                        Log.d(EventBus.f14465, "Skipping method (not public, static or abstract): " + cls2 + "." + name2);
                    }
                }
            }
        }
        if (arrayList.isEmpty()) {
            throw new EventBusException("Subscriber " + cls + " has no public methods called " + str);
        }
        synchronized (f14507) {
            f14507.put(str2, arrayList);
        }
        ArrayList arrayList2 = arrayList;
        return arrayList;
    }
}
