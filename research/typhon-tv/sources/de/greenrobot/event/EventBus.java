package de.greenrobot.event;

import android.os.Looper;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EventBus {

    /* renamed from: 靐  reason: contains not printable characters */
    public static String f14465 = "Event";

    /* renamed from: 齉  reason: contains not printable characters */
    private static final Map<Class<?>, List<Class<?>>> f14466 = new HashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    static ExecutorService f14467 = Executors.newCachedThreadPool();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<Class<?>, Object> f14468 = new ConcurrentHashMap();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final ThreadLocal<PostingThreadState> f14469 = new ThreadLocal<PostingThreadState>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public PostingThreadState initialValue() {
            return new PostingThreadState();
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    private final HandlerPoster f14470 = new HandlerPoster(this, Looper.getMainLooper(), 10);

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f14471 = true;

    /* renamed from: ˈ  reason: contains not printable characters */
    private boolean f14472;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final BackgroundPoster f14473 = new BackgroundPoster(this);

    /* renamed from: ٴ  reason: contains not printable characters */
    private final AsyncPoster f14474 = new AsyncPoster(this);

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final SubscriberMethodFinder f14475 = new SubscriberMethodFinder();

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<Object, List<Class<?>>> f14476 = new HashMap();

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<Class<?>, CopyOnWriteArrayList<Subscription>> f14477 = new HashMap();

    static final class PostingThreadState {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f14480;

        /* renamed from: 连任  reason: contains not printable characters */
        Object f14481;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f14482;

        /* renamed from: 麤  reason: contains not printable characters */
        Subscription f14483;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f14484;

        /* renamed from: 龘  reason: contains not printable characters */
        List<Object> f14485 = new ArrayList();

        PostingThreadState() {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<Class<?>> m18241(Class<?> cls) {
        List<Class<?>> list;
        synchronized (f14466) {
            list = f14466.get(cls);
            if (list == null) {
                list = new ArrayList<>();
                for (Class<?> cls2 = cls; cls2 != null; cls2 = cls2.getSuperclass()) {
                    list.add(cls2);
                    m18247(list, (Class<?>[]) cls2.getInterfaces());
                }
                f14466.put(cls, list);
            }
        }
        return list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18242(Subscription subscription, Object obj, boolean z) {
        switch (subscription.f14508.f14502) {
            case PostThread:
                m18252(subscription, obj);
                return;
            case MainThread:
                if (z) {
                    m18252(subscription, obj);
                    return;
                } else {
                    this.f14470.m18255(subscription, obj);
                    return;
                }
            case BackgroundThread:
                if (z) {
                    this.f14473.m18240(subscription, obj);
                    return;
                } else {
                    m18252(subscription, obj);
                    return;
                }
            case Async:
                this.f14474.m18239(subscription, obj);
                return;
            default:
                throw new IllegalStateException("Unknown thread mode: " + subscription.f14508.f14502);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18243(Object obj, PostingThreadState postingThreadState) throws Error {
        CopyOnWriteArrayList copyOnWriteArrayList;
        Class<?> cls = obj.getClass();
        List<Class<?>> r4 = m18241(cls);
        boolean z = false;
        int size = r4.size();
        for (int i = 0; i < size; i++) {
            Class cls2 = r4.get(i);
            synchronized (this) {
                copyOnWriteArrayList = this.f14477.get(cls2);
            }
            if (copyOnWriteArrayList != null && !copyOnWriteArrayList.isEmpty()) {
                Iterator it2 = copyOnWriteArrayList.iterator();
                while (it2.hasNext()) {
                    Subscription subscription = (Subscription) it2.next();
                    postingThreadState.f14481 = obj;
                    postingThreadState.f14483 = subscription;
                    try {
                        m18242(subscription, obj, postingThreadState.f14484);
                        if (postingThreadState.f14480) {
                            break;
                        }
                    } finally {
                        postingThreadState.f14481 = null;
                        postingThreadState.f14483 = null;
                        postingThreadState.f14480 = false;
                    }
                }
                z = true;
            }
        }
        if (!z) {
            Log.d(f14465, "No subscribers registered for event " + cls);
            if (cls != NoSubscriberEvent.class && cls != SubscriberExceptionEvent.class) {
                m18249(new NoSubscriberEvent(this, obj));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18244(Object obj, SubscriberMethod subscriberMethod, boolean z, int i) {
        Object obj2;
        this.f14472 = true;
        Class<?> cls = subscriberMethod.f14504;
        CopyOnWriteArrayList copyOnWriteArrayList = this.f14477.get(cls);
        Subscription subscription = new Subscription(obj, subscriberMethod, i);
        if (copyOnWriteArrayList == null) {
            copyOnWriteArrayList = new CopyOnWriteArrayList();
            this.f14477.put(cls, copyOnWriteArrayList);
        } else {
            Iterator it2 = copyOnWriteArrayList.iterator();
            while (it2.hasNext()) {
                if (((Subscription) it2.next()).equals(subscription)) {
                    throw new EventBusException("Subscriber " + obj.getClass() + " already registered to event " + cls);
                }
            }
        }
        int size = copyOnWriteArrayList.size();
        int i2 = 0;
        while (true) {
            if (i2 > size) {
                break;
            } else if (i2 == size || subscription.f14510 > ((Subscription) copyOnWriteArrayList.get(i2)).f14510) {
                copyOnWriteArrayList.add(i2, subscription);
            } else {
                i2++;
            }
        }
        copyOnWriteArrayList.add(i2, subscription);
        List list = this.f14476.get(obj);
        if (list == null) {
            list = new ArrayList();
            this.f14476.put(obj, list);
        }
        list.add(cls);
        if (z) {
            synchronized (this.f14468) {
                obj2 = this.f14468.get(cls);
            }
            if (obj2 != null) {
                m18242(subscription, obj2, Looper.getMainLooper() == Looper.myLooper());
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18245(Object obj, Class<?> cls) {
        List list = this.f14477.get(cls);
        if (list != null) {
            int size = list.size();
            int i = 0;
            while (i < size) {
                Subscription subscription = (Subscription) list.get(i);
                if (subscription.f14511 == obj) {
                    subscription.f14509 = false;
                    list.remove(i);
                    i--;
                    size--;
                }
                i++;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m18246(Object obj, String str, boolean z, int i) {
        for (SubscriberMethod r1 : this.f14475.m18262(obj.getClass(), str)) {
            m18244(obj, r1, z, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m18247(List<Class<?>> list, Class<?>[] clsArr) {
        for (Class<?> cls : clsArr) {
            if (!list.contains(cls)) {
                list.add(cls);
                m18247(list, (Class<?>[]) cls.getInterfaces());
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18248(Object obj) {
        m18246(obj, "onEvent", true, 0);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m18249(Object obj) {
        PostingThreadState postingThreadState = this.f14469.get();
        List<Object> list = postingThreadState.f14485;
        list.add(obj);
        if (!postingThreadState.f14482) {
            postingThreadState.f14484 = Looper.getMainLooper() == Looper.myLooper();
            postingThreadState.f14482 = true;
            if (postingThreadState.f14480) {
                throw new EventBusException("Internal error. Abort state was not reset");
            }
            while (!list.isEmpty()) {
                try {
                    m18243(list.remove(0), postingThreadState);
                } finally {
                    postingThreadState.f14482 = false;
                    postingThreadState.f14484 = false;
                }
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m18250(Object obj) {
        List<Class> list = this.f14476.get(obj);
        if (list != null) {
            for (Class r0 : list) {
                m18245(obj, (Class<?>) r0);
            }
            this.f14476.remove(obj);
        } else {
            Log.w(f14465, "Subscriber to unregister was not registered before: " + obj.getClass());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18251(PendingPost pendingPost) {
        Object obj = pendingPost.f14495;
        Subscription subscription = pendingPost.f14493;
        PendingPost.m18257(pendingPost);
        if (subscription.f14509) {
            m18252(subscription, obj);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18252(Subscription subscription, Object obj) throws Error {
        try {
            subscription.f14508.f14505.invoke(subscription.f14511, new Object[]{obj});
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (obj instanceof SubscriberExceptionEvent) {
                Log.e(f14465, "SubscriberExceptionEvent subscriber " + subscription.f14511.getClass() + " threw an exception", cause);
                SubscriberExceptionEvent subscriberExceptionEvent = (SubscriberExceptionEvent) obj;
                Log.e(f14465, "Initial event " + subscriberExceptionEvent.f14500 + " caused exception in " + subscriberExceptionEvent.f14499, subscriberExceptionEvent.f14498);
                return;
            }
            if (this.f14471) {
                Log.e(f14465, "Could not dispatch event: " + obj.getClass() + " to subscribing class " + subscription.f14511.getClass(), cause);
            }
            m18249(new SubscriberExceptionEvent(this, cause, obj, subscription.f14511));
        } catch (IllegalAccessException e2) {
            throw new IllegalStateException("Unexpected exception", e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18253(Object obj) {
        m18246(obj, "onEvent", false, 0);
    }
}
