package de.greenrobot.event;

import android.util.Log;

final class BackgroundPoster implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile boolean f14462;

    /* renamed from: 齉  reason: contains not printable characters */
    private final EventBus f14463;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PendingPostQueue f14464 = new PendingPostQueue();

    BackgroundPoster(EventBus eventBus) {
        this.f14463 = eventBus;
    }

    public void run() {
        while (true) {
            try {
                PendingPost r1 = this.f14464.m18259(1000);
                if (r1 == null) {
                    synchronized (this) {
                        r1 = this.f14464.m18258();
                        if (r1 == null) {
                            this.f14462 = false;
                            this.f14462 = false;
                            return;
                        }
                    }
                }
                this.f14463.m18251(r1);
            } catch (InterruptedException e) {
                Log.w("Event", Thread.currentThread().getName() + " was interruppted", e);
                this.f14462 = false;
                return;
            } catch (Throwable th) {
                this.f14462 = false;
                throw th;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18240(Subscription subscription, Object obj) {
        PendingPost r0 = PendingPost.m18256(subscription, obj);
        synchronized (this) {
            this.f14464.m18260(r0);
            if (!this.f14462) {
                this.f14462 = true;
                EventBus.f14467.execute(this);
            }
        }
    }
}
