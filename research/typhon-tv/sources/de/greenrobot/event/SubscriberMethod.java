package de.greenrobot.event;

import java.lang.reflect.Method;

final class SubscriberMethod {

    /* renamed from: 靐  reason: contains not printable characters */
    final ThreadMode f14502;

    /* renamed from: 麤  reason: contains not printable characters */
    String f14503;

    /* renamed from: 齉  reason: contains not printable characters */
    final Class<?> f14504;

    /* renamed from: 龘  reason: contains not printable characters */
    final Method f14505;

    SubscriberMethod(Method method, ThreadMode threadMode, Class<?> cls) {
        this.f14505 = method;
        this.f14502 = threadMode;
        this.f14504 = cls;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m18261() {
        if (this.f14503 == null) {
            StringBuilder sb = new StringBuilder(64);
            sb.append(this.f14505.getDeclaringClass().getName());
            sb.append('#').append(this.f14505.getName());
            sb.append('(').append(this.f14504.getName());
            this.f14503 = sb.toString();
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof SubscriberMethod)) {
            return false;
        }
        m18261();
        SubscriberMethod subscriberMethod = (SubscriberMethod) obj;
        subscriberMethod.m18261();
        return this.f14503.equals(subscriberMethod.f14503);
    }

    public int hashCode() {
        return this.f14505.hashCode();
    }
}
