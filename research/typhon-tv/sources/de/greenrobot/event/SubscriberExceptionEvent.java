package de.greenrobot.event;

public final class SubscriberExceptionEvent {

    /* renamed from: 靐  reason: contains not printable characters */
    public final Throwable f14498;

    /* renamed from: 麤  reason: contains not printable characters */
    public final Object f14499;

    /* renamed from: 齉  reason: contains not printable characters */
    public final Object f14500;

    /* renamed from: 龘  reason: contains not printable characters */
    public final EventBus f14501;

    public SubscriberExceptionEvent(EventBus eventBus, Throwable th, Object obj, Object obj2) {
        this.f14501 = eventBus;
        this.f14498 = th;
        this.f14500 = obj;
        this.f14499 = obj2;
    }
}
