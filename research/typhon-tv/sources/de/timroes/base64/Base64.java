package de.timroes.base64;

import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;

public class Base64 {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final HashMap<Character, Byte> f14562 = new HashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final char[] f14563 = "=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    static {
        for (int i = 0; i < f14563.length; i++) {
            f14562.put(Character.valueOf(f14563[i]), Byte.valueOf((byte) i));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m18327(String str) {
        return m18329(str.getBytes());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static byte[] m18328(byte[] bArr) {
        byte[] bArr2 = new byte[(((bArr.length + 2) / 3) * 4)];
        byte[] bArr3 = new byte[(((bArr.length + 2) / 3) * 3)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        int i = 0;
        for (int i2 = 0; i2 < bArr3.length; i2 += 3) {
            int i3 = i + 1;
            bArr2[i] = (byte) ((bArr3[i2] & 255) >>> 2);
            int i4 = i3 + 1;
            bArr2[i3] = (byte) (((bArr3[i2] & 3) << 4) | ((bArr3[i2 + 1] & 255) >>> 4));
            int i5 = i4 + 1;
            bArr2[i4] = (byte) (((bArr3[i2 + 1] & 15) << 2) | ((bArr3[i2 + 2] & 255) >>> 6));
            i = i5 + 1;
            bArr2[i5] = (byte) (bArr3[i2 + 2] & 63);
        }
        for (int length = bArr3.length - bArr.length; length > 0; length--) {
            bArr2[bArr2.length - length] = -1;
        }
        return bArr2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18329(byte[] bArr) {
        StringBuilder sb = new StringBuilder(((bArr.length + 2) / 3) * 4);
        byte[] r1 = m18328(bArr);
        for (int i = 0; i < r1.length; i++) {
            sb.append(f14563[r1[i] + 1]);
            if (i % 72 == 71) {
                sb.append(StringUtils.LF);
            }
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m18330(Byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        for (int i = 0; i < bArr2.length; i++) {
            bArr2[i] = bArr[i].byteValue();
        }
        return m18329(bArr2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static byte[] m18331(String str) {
        String replaceAll = str.replaceAll("\\r|\\n", "");
        if (replaceAll.length() % 4 != 0) {
            throw new IllegalArgumentException("The length of the input string must be a multiple of four.");
        } else if (!replaceAll.matches("^[A-Za-z0-9+/]*[=]{0,3}$")) {
            throw new IllegalArgumentException("The argument contains illegal characters.");
        } else {
            byte[] bArr = new byte[((replaceAll.length() * 3) / 4)];
            char[] charArray = replaceAll.toCharArray();
            int i = 0;
            int i2 = 0;
            while (i2 < charArray.length) {
                int byteValue = f14562.get(Character.valueOf(charArray[i2])).byteValue() - 1;
                int byteValue2 = f14562.get(Character.valueOf(charArray[i2 + 1])).byteValue() - 1;
                int byteValue3 = f14562.get(Character.valueOf(charArray[i2 + 2])).byteValue() - 1;
                int byteValue4 = f14562.get(Character.valueOf(charArray[i2 + 3])).byteValue() - 1;
                int i3 = i + 1;
                bArr[i] = (byte) ((byteValue << 2) | (byteValue2 >>> 4));
                int i4 = i3 + 1;
                bArr[i3] = (byte) (((byteValue2 & 15) << 4) | (byteValue3 >>> 2));
                bArr[i4] = (byte) (((byteValue3 & 3) << 6) | (byteValue4 & 63));
                i2 += 4;
                i = i4 + 1;
            }
            if (!replaceAll.endsWith("=")) {
                return bArr;
            }
            byte[] bArr2 = new byte[(bArr.length - (replaceAll.length() - replaceAll.indexOf("=")))];
            System.arraycopy(bArr, 0, bArr2, 0, bArr2.length);
            return bArr2;
        }
    }
}
