package de.timroes.axmlrpc;

import de.timroes.axmlrpc.serializer.SerializerHandler;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.oltu.oauth2.common.OAuth;

public class XMLRPCClient {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public CookieManager f14520;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public AuthenticationManager f14521;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public TrustManager[] f14522;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public final SerializerHandler f14523;
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public KeyManager[] f14524;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public Proxy f14525;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int f14526;
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public ResponseParser f14527;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public URL f14528;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public Map<Long, Caller> f14529;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public Map<String, String> f14530;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f14531;

    private class Caller extends Thread {

        /* renamed from: ʻ  reason: contains not printable characters */
        private volatile boolean f14533;

        /* renamed from: ʼ  reason: contains not printable characters */
        private HttpURLConnection f14534;

        /* renamed from: 连任  reason: contains not printable characters */
        private Object[] f14535;

        /* renamed from: 靐  reason: contains not printable characters */
        private XMLRPCCallback f14536;

        /* renamed from: 麤  reason: contains not printable characters */
        private String f14537;

        /* renamed from: 齉  reason: contains not printable characters */
        private long f14538;

        public Caller() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private HttpURLConnection m18293(URLConnection uRLConnection) throws XMLRPCException {
            if (!(uRLConnection instanceof HttpURLConnection)) {
                throw new IllegalArgumentException("The URL is not valid for a http connection.");
            } else if (!(uRLConnection instanceof HttpsURLConnection)) {
                return (HttpURLConnection) uRLConnection;
            } else {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) uRLConnection;
                if (XMLRPCClient.this.m18283(64)) {
                    httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                        public boolean verify(String str, SSLSession sSLSession) {
                            return true;
                        }
                    });
                }
                if (XMLRPCClient.this.f14522 == null) {
                    return httpsURLConnection;
                }
                try {
                    for (String instance : new String[]{"TLS", "SSL"}) {
                        SSLContext instance2 = SSLContext.getInstance(instance);
                        instance2.init(XMLRPCClient.this.f14524, XMLRPCClient.this.f14522, new SecureRandom());
                        httpsURLConnection.setSSLSocketFactory(instance2.getSocketFactory());
                    }
                    return httpsURLConnection;
                } catch (Exception e) {
                    throw new XMLRPCException(e);
                }
            }
        }

        public void run() {
            if (this.f14536 != null) {
                try {
                    XMLRPCClient.this.f14529.put(Long.valueOf(this.f14538), this);
                    this.f14536.m18273(this.f14538, m18294(this.f14537, this.f14535));
                } catch (CancelException e) {
                } catch (XMLRPCServerException e2) {
                    this.f14536.m18272(this.f14538, e2);
                } catch (XMLRPCException e3) {
                    this.f14536.m18271(this.f14538, e3);
                } finally {
                    XMLRPCClient.this.f14529.remove(Long.valueOf(this.f14538));
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x00bf, code lost:
            throw new de.timroes.axmlrpc.XMLRPCTimeoutException("The XMLRPC call timed out.");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x0195, code lost:
            r10 = r18.f14534.getResponseCode();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x00b6 A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:1:0x0002] */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object m18294(java.lang.String r19, java.lang.Object[] r20) throws de.timroes.axmlrpc.XMLRPCException {
            /*
                r18 = this;
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r19
                r1 = r20
                de.timroes.axmlrpc.Call r2 = r13.m18281(r0, r1)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.Proxy r13 = r13.f14525     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 == 0) goto L_0x00c0
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URL r13 = r13.f14528     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r14 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.Proxy r14 = r14.f14525     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URLConnection r3 = r13.openConnection(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x002a:
                r0 = r18
                java.net.HttpURLConnection r13 = r0.m18293(r3)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                r0.f14534 = r13     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 0
                r13.setInstanceFollowRedirects(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = "POST"
                r13.setRequestMethod(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 1
                r13.setDoOutput(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 1
                r13.setDoInput(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                int r13 = r13.f14526     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 <= 0) goto L_0x0082
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r14 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                int r14 = r14.f14526     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                int r14 = r14 * 1000
                r13.setConnectTimeout(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r14 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                int r14 = r14.f14526     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                int r14 = r14 * 1000
                r13.setReadTimeout(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x0082:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.util.Map r13 = r13.f14530     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.util.Set r13 = r13.entrySet()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.util.Iterator r15 = r13.iterator()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x0092:
                boolean r13 = r15.hasNext()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 == 0) goto L_0x00ce
                java.lang.Object r9 = r15.next()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.util.Map$Entry r9 = (java.util.Map.Entry) r9     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r0 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r16 = r0
                java.lang.Object r13 = r9.getKey()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r13 = (java.lang.String) r13     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.Object r14 = r9.getValue()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = (java.lang.String) r14     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r16
                r0.setRequestProperty(r13, r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                goto L_0x0092
            L_0x00b6:
                r4 = move-exception
                de.timroes.axmlrpc.XMLRPCTimeoutException r13 = new de.timroes.axmlrpc.XMLRPCTimeoutException
                java.lang.String r14 = "The XMLRPC call timed out."
                r13.<init>(r14)
                throw r13
            L_0x00c0:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URL r13 = r13.f14528     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URLConnection r3 = r13.openConnection()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                goto L_0x002a
            L_0x00ce:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                de.timroes.axmlrpc.AuthenticationManager r13 = r13.f14521     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r14 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r13.m18263(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                de.timroes.axmlrpc.CookieManager r13 = r13.f14520     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r14 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r13.m18266(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.io.OutputStreamWriter r11 = new java.io.OutputStreamWriter     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.io.OutputStream r13 = r13.getOutputStream()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r11.<init>(r13)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 8192(0x2000, float:1.14794E-41)
                boolean r13 = r13.m18283((int) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r13 = r2.m18265((boolean) r13)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r11.write(r13)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r11.flush()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r11.close()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ IOException -> 0x0194, SocketTimeoutException -> 0x00b6 }
                int r10 = r13.getResponseCode()     // Catch:{ IOException -> 0x0194, SocketTimeoutException -> 0x00b6 }
            L_0x0118:
                r13 = 403(0x193, float:5.65E-43)
                if (r10 == r13) goto L_0x0120
                r13 = 401(0x191, float:5.62E-43)
                if (r10 != r13) goto L_0x01d7
            L_0x0120:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 16
                boolean r13 = r13.m18283((int) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 == 0) goto L_0x019f
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.io.InputStream r6 = r13.getErrorStream()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x0134:
                r13 = 301(0x12d, float:4.22E-43)
                if (r10 == r13) goto L_0x013c
                r13 = 302(0x12e, float:4.23E-43)
                if (r10 != r13) goto L_0x01ed
            L_0x013c:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 32
                boolean r13 = r13.m18283((int) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 == 0) goto L_0x01e4
                r13 = 302(0x12e, float:4.23E-43)
                if (r10 != r13) goto L_0x01e1
                r12 = 1
            L_0x014d:
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = "Location"
                java.lang.String r7 = r13.getHeaderField(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r7 == 0) goto L_0x0160
                int r13 = r7.length()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 > 0) goto L_0x016b
            L_0x0160:
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = "location"
                java.lang.String r7 = r13.getHeaderField(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x016b:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URL r8 = r13.f14528     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URL r14 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14.<init>(r7)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URL unused = r13.f14528 = r14     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r13.disconnect()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.Object r5 = r18.m18294(r19, r20)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r12 == 0) goto L_0x0193
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.net.URL unused = r13.f14528 = r8     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x0193:
                return r5
            L_0x0194:
                r4 = move-exception
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                int r10 = r13.getResponseCode()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                goto L_0x0118
            L_0x019f:
                de.timroes.axmlrpc.XMLRPCException r13 = new de.timroes.axmlrpc.XMLRPCException     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14.<init>()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r15 = "Invalid status code '"
                java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.StringBuilder r14 = r14.append(r10)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r15 = "' returned from server."
                java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = r14.toString()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r13.<init>((java.lang.String) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                throw r13     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x01c0:
                r4 = move-exception
                r0 = r18
                boolean r13 = r0.f14533
                if (r13 == 0) goto L_0x01d1
                r0 = r18
                long r14 = r0.f14538
                r16 = 0
                int r13 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
                if (r13 > 0) goto L_0x025a
            L_0x01d1:
                de.timroes.axmlrpc.XMLRPCException r13 = new de.timroes.axmlrpc.XMLRPCException
                r13.<init>((java.lang.Exception) r4)
                throw r13
            L_0x01d7:
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.io.InputStream r6 = r13.getInputStream()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                goto L_0x0134
            L_0x01e1:
                r12 = 0
                goto L_0x014d
            L_0x01e4:
                de.timroes.axmlrpc.XMLRPCException r13 = new de.timroes.axmlrpc.XMLRPCException     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = "The server responded with a http 301 or 302 status code, but forwarding has not been enabled (FLAGS_FORWARD)."
                r13.<init>((java.lang.String) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                throw r13     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x01ed:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 16
                boolean r13 = r13.m18283((int) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 != 0) goto L_0x0206
                r13 = 200(0xc8, float:2.8E-43)
                if (r10 == r13) goto L_0x0206
                de.timroes.axmlrpc.XMLRPCException r13 = new de.timroes.axmlrpc.XMLRPCException     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = "The status code of the http response must be 200."
                r13.<init>((java.lang.String) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                throw r13     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x0206:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r14 = 1
                boolean r13 = r13.m18283((int) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 == 0) goto L_0x022b
                r0 = r18
                java.net.HttpURLConnection r13 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r13 = r13.getContentType()     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = "text/xml; charset=utf-8"
                boolean r13 = r13.startsWith(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                if (r13 != 0) goto L_0x022b
                de.timroes.axmlrpc.XMLRPCException r13 = new de.timroes.axmlrpc.XMLRPCException     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.String r14 = "The Content-Type of the response must be text/xml."
                r13.<init>((java.lang.String) r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                throw r13     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
            L_0x022b:
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                de.timroes.axmlrpc.CookieManager r13 = r13.f14520     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                java.net.HttpURLConnection r14 = r0.f14534     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r13.m18267(r14)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r13 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                de.timroes.axmlrpc.ResponseParser r13 = r13.f14527     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r14 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                de.timroes.axmlrpc.serializer.SerializerHandler r14 = r14.f14523     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r15 = de.timroes.axmlrpc.XMLRPCClient.this     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                r16 = 8192(0x2000, float:1.14794E-41)
                boolean r15 = r15.m18283((int) r16)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                java.lang.Object r5 = r13.m18270(r14, r6, r15)     // Catch:{ SocketTimeoutException -> 0x00b6, IOException -> 0x01c0 }
                goto L_0x0193
            L_0x025a:
                de.timroes.axmlrpc.XMLRPCClient$CancelException r13 = new de.timroes.axmlrpc.XMLRPCClient$CancelException
                r0 = r18
                de.timroes.axmlrpc.XMLRPCClient r14 = de.timroes.axmlrpc.XMLRPCClient.this
                r15 = 0
                r13.<init>()
                throw r13
            */
            throw new UnsupportedOperationException("Method not decompiled: de.timroes.axmlrpc.XMLRPCClient.Caller.m18294(java.lang.String, java.lang.Object[]):java.lang.Object");
        }
    }

    private class CancelException extends RuntimeException {
        private CancelException() {
        }
    }

    public XMLRPCClient(URL url, int i) {
        this(url, "aXMLRPC", i);
    }

    public XMLRPCClient(URL url, String str, int i) {
        this.f14530 = new ConcurrentHashMap();
        this.f14529 = new ConcurrentHashMap();
        this.f14523 = new SerializerHandler(i);
        this.f14528 = url;
        this.f14531 = i;
        this.f14527 = new ResponseParser();
        this.f14520 = new CookieManager(i);
        this.f14521 = new AuthenticationManager();
        this.f14530.put(OAuth.HeaderType.CONTENT_TYPE, "text/xml; charset=utf-8");
        this.f14530.put(AbstractSpiCall.HEADER_USER_AGENT, str);
        if (m18283(128)) {
            this.f14522 = new TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            }};
        }
        if (m18283(1024)) {
            Properties properties = System.getProperties();
            String property = properties.getProperty("http.proxyHost");
            int parseInt = Integer.parseInt(properties.getProperty("http.proxyPort", "0"));
            if (parseInt > 0 && property.length() > 0 && !property.equals("null")) {
                this.f14525 = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(property, parseInt));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Call m18281(String str, Object[] objArr) {
        if (!m18283(1) || str.matches("^[A-Za-z0-9\\._:/]*$")) {
            return new Call(this.f14523, str, objArr);
        }
        throw new XMLRPCRuntimeException("Method name must only contain A-Z a-z . : _ / ");
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m18283(int i) {
        return (this.f14531 & i) != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18290(String str, Object... objArr) throws XMLRPCException {
        return new Caller().m18294(str, objArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18291(int i) {
        this.f14526 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18292(String str) {
        this.f14530.put(AbstractSpiCall.HEADER_USER_AGENT, str);
    }
}
