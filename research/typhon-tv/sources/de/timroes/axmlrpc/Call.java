package de.timroes.axmlrpc;

import de.timroes.axmlrpc.serializer.SerializerHandler;
import de.timroes.axmlrpc.xmlcreator.SimpleXMLCreator;
import de.timroes.axmlrpc.xmlcreator.XmlElement;

public class Call {

    /* renamed from: 靐  reason: contains not printable characters */
    private Object[] f14515;

    /* renamed from: 齉  reason: contains not printable characters */
    private final SerializerHandler f14516;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f14517;

    public Call(SerializerHandler serializerHandler, String str, Object[] objArr) {
        this.f14517 = str;
        this.f14515 = objArr;
        this.f14516 = serializerHandler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private XmlElement m18264(Object obj) throws XMLRPCException {
        XmlElement xmlElement = new XmlElement("param");
        XmlElement xmlElement2 = new XmlElement("value");
        xmlElement.m18325(xmlElement2);
        xmlElement2.m18325(this.f14516.m18318(obj));
        return xmlElement;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m18265(boolean z) throws XMLRPCException {
        SimpleXMLCreator simpleXMLCreator = new SimpleXMLCreator();
        XmlElement xmlElement = new XmlElement("methodCall");
        simpleXMLCreator.m18324(xmlElement);
        XmlElement xmlElement2 = new XmlElement("methodName");
        xmlElement2.m18326(this.f14517);
        xmlElement.m18325(xmlElement2);
        if (this.f14515 != null && this.f14515.length > 0) {
            XmlElement xmlElement3 = new XmlElement("params");
            xmlElement.m18325(xmlElement3);
            for (Object r4 : this.f14515) {
                xmlElement3.m18325(m18264(r4));
            }
        }
        String simpleXMLCreator2 = simpleXMLCreator.toString();
        if (z) {
            System.out.println(simpleXMLCreator2);
        }
        return simpleXMLCreator2;
    }
}
