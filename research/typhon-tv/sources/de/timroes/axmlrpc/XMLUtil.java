package de.timroes.axmlrpc;

import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLUtil {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m18295(NodeList nodeList) throws XMLRPCException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node item = nodeList.item(i);
            if (item.getNodeType() != 8) {
                if (item.getNodeType() != 3) {
                    throw new XMLRPCException("Element must contain only text elements.");
                }
                sb.append(item.getNodeValue());
            }
        }
        return sb.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m18296(NodeList nodeList) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == 1) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static XmlElement m18297(String str, String str2) {
        XmlElement xmlElement = new XmlElement(str);
        xmlElement.m18326(str2);
        return xmlElement;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: org.w3c.dom.Node} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: org.w3c.dom.Element} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.w3c.dom.Element m18298(org.w3c.dom.NodeList r5) throws de.timroes.axmlrpc.XMLRPCException {
        /*
            r0 = 0
            r1 = 0
        L_0x0002:
            int r3 = r5.getLength()
            if (r1 >= r3) goto L_0x004b
            org.w3c.dom.Node r2 = r5.item(r1)
            short r3 = r2.getNodeType()
            r4 = 3
            if (r3 != r4) goto L_0x0021
            java.lang.String r3 = r2.getNodeValue()
            java.lang.String r3 = r3.trim()
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x0029
        L_0x0021:
            short r3 = r2.getNodeType()
            r4 = 8
            if (r3 != r4) goto L_0x002c
        L_0x0029:
            int r1 = r1 + 1
            goto L_0x0002
        L_0x002c:
            short r3 = r2.getNodeType()
            r4 = 1
            if (r3 == r4) goto L_0x003c
            de.timroes.axmlrpc.XMLRPCException r3 = new de.timroes.axmlrpc.XMLRPCException
            java.lang.String r4 = "Only element nodes allowed."
            r3.<init>((java.lang.String) r4)
            throw r3
        L_0x003c:
            if (r0 == 0) goto L_0x0047
            de.timroes.axmlrpc.XMLRPCException r3 = new de.timroes.axmlrpc.XMLRPCException
            java.lang.String r4 = "Element has more than one children."
            r3.<init>((java.lang.String) r4)
            throw r3
        L_0x0047:
            r0 = r2
            org.w3c.dom.Element r0 = (org.w3c.dom.Element) r0
            goto L_0x0029
        L_0x004b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: de.timroes.axmlrpc.XMLUtil.m18298(org.w3c.dom.NodeList):org.w3c.dom.Element");
    }
}
