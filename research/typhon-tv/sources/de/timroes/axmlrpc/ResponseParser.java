package de.timroes.axmlrpc;

import de.timroes.axmlrpc.serializer.SerializerHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ResponseParser {
    /* renamed from: 龘  reason: contains not printable characters */
    private Object m18268(SerializerHandler serializerHandler, Element element) throws XMLRPCException {
        return serializerHandler.m18319(XMLUtil.m18298(element.getChildNodes()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m18269(Document document, OutputStream outputStream) throws IOException, TransformerException {
        Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
        newTransformer.setOutputProperty("omit-xml-declaration", "no");
        newTransformer.setOutputProperty("method", "xml");
        newTransformer.setOutputProperty("indent", "yes");
        newTransformer.setOutputProperty("encoding", "UTF-8");
        newTransformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        newTransformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(outputStream, "UTF-8")));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18270(SerializerHandler serializerHandler, InputStream inputStream, boolean z) throws XMLRPCException {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            newInstance.setNamespaceAware(true);
            Document parse = newInstance.newDocumentBuilder().parse(inputStream);
            if (z) {
                m18269(parse, (OutputStream) System.out);
            }
            Element documentElement = parse.getDocumentElement();
            if (!documentElement.getNodeName().equals("methodResponse")) {
                throw new XMLRPCException("MethodResponse root tag is missing.");
            }
            Element r2 = XMLUtil.m18298(documentElement.getChildNodes());
            if (r2.getNodeName().equals("params")) {
                Element r22 = XMLUtil.m18298(r2.getChildNodes());
                if (r22.getNodeName().equals("param")) {
                    return m18268(serializerHandler, r22);
                }
                throw new XMLRPCException("The params tag must contain a param tag.");
            } else if (r2.getNodeName().equals("fault")) {
                Map map = (Map) m18268(serializerHandler, r2);
                throw new XMLRPCServerException((String) map.get("faultString"), ((Integer) map.get("faultCode")).intValue());
            } else {
                throw new XMLRPCException("The methodResponse tag must contain a fault or params tag.");
            }
        } catch (XMLRPCServerException e) {
            throw e;
        } catch (Exception e2) {
            throw new XMLRPCException("Error getting result from server.", e2);
        }
    }
}
