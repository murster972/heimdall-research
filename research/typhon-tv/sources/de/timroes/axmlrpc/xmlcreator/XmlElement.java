package de.timroes.axmlrpc.xmlcreator;

import java.util.ArrayList;
import java.util.List;

public class XmlElement {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f14559;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f14560;

    /* renamed from: 龘  reason: contains not printable characters */
    private List<XmlElement> f14561 = new ArrayList();

    public XmlElement(String str) {
        this.f14559 = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.f14560 != null && this.f14560.length() > 0) {
            sb.append("\n<").append(this.f14559).append(">").append(this.f14560).append("</").append(this.f14559).append(">\n");
            return sb.toString();
        } else if (this.f14561.size() > 0) {
            sb.append("\n<").append(this.f14559).append(">");
            for (XmlElement xmlElement : this.f14561) {
                sb.append(xmlElement.toString());
            }
            sb.append("</").append(this.f14559).append(">\n");
            return sb.toString();
        } else {
            sb.append("\n<").append(this.f14559).append("/>\n");
            return sb.toString();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18325(XmlElement xmlElement) {
        this.f14561.add(xmlElement);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18326(String str) {
        this.f14560 = str;
    }
}
