package de.timroes.axmlrpc;

import java.net.HttpURLConnection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class CookieManager {

    /* renamed from: 靐  reason: contains not printable characters */
    private Map<String, String> f14518 = new ConcurrentHashMap();

    /* renamed from: 龘  reason: contains not printable characters */
    private int f14519;

    public CookieManager(int i) {
        this.f14519 = i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m18266(HttpURLConnection httpURLConnection) {
        if ((this.f14519 & 4) != 0) {
            String str = "";
            for (Map.Entry next : this.f14518.entrySet()) {
                str = str + ((String) next.getKey()) + "=" + ((String) next.getValue()) + "; ";
            }
            httpURLConnection.setRequestProperty("Cookie", str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18267(HttpURLConnection httpURLConnection) {
        if ((this.f14519 & 4) != 0) {
            for (int i = 0; i < httpURLConnection.getHeaderFields().size(); i++) {
                String headerFieldKey = httpURLConnection.getHeaderFieldKey(i);
                if (headerFieldKey != null && "Set-Cookie".equalsIgnoreCase(headerFieldKey.toLowerCase())) {
                    String[] split = httpURLConnection.getHeaderField(i).split(";")[0].split("=");
                    if (split.length >= 2) {
                        this.f14518.put(split[0], split[1]);
                    }
                }
            }
        }
    }
}
