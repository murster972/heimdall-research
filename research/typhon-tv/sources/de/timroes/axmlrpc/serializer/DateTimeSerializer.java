package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import fr.turri.jiso8601.Iso8601Deserializer;
import java.text.SimpleDateFormat;
import org.w3c.dom.Element;

public class DateTimeSerializer implements Serializer {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f14542;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SimpleDateFormat f14543 = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

    public DateTimeSerializer(boolean z) {
        this.f14542 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18305(Object obj) {
        return XMLUtil.m18297("dateTime.iso8601", this.f14543.format(obj));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18306(String str) throws XMLRPCException {
        if (this.f14542 && (str == null || str.trim().length() == 0)) {
            return null;
        }
        try {
            return Iso8601Deserializer.m18913(str);
        } catch (Exception e) {
            throw new XMLRPCException("Unable to parse given date.", e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18307(Element element) throws XMLRPCException {
        return m18306(XMLUtil.m18295(element.getChildNodes()));
    }
}
