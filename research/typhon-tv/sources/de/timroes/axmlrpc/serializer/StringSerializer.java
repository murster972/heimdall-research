package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

public class StringSerializer implements Serializer {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f14555;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f14556;

    public StringSerializer(boolean z, boolean z2) {
        this.f14556 = z2;
        this.f14555 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18320(Object obj) {
        String obj2 = obj.toString();
        if (this.f14555) {
            obj2 = obj2.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll("]]>", "]]&gt;");
        }
        return XMLUtil.m18297("string", obj2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18321(Element element) throws XMLRPCException {
        String r0 = XMLUtil.m18295(element.getChildNodes());
        return this.f14556 ? r0.replaceAll("&lt;", "<").replaceAll("&amp;", "&") : r0;
    }
}
