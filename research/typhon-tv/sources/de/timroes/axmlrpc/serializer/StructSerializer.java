package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCRuntimeException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class StructSerializer implements Serializer {

    /* renamed from: 龘  reason: contains not printable characters */
    private final SerializerHandler f14557;

    public StructSerializer(SerializerHandler serializerHandler) {
        this.f14557 = serializerHandler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18322(Object obj) {
        XmlElement xmlElement = new XmlElement("struct");
        try {
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                XmlElement xmlElement2 = new XmlElement("member");
                XmlElement xmlElement3 = new XmlElement("name");
                XmlElement xmlElement4 = new XmlElement("value");
                xmlElement3.m18326((String) entry.getKey());
                xmlElement4.m18325(this.f14557.m18318(entry.getValue()));
                xmlElement2.m18325(xmlElement3);
                xmlElement2.m18325(xmlElement4);
                xmlElement.m18325(xmlElement2);
            }
            return xmlElement;
        } catch (XMLRPCException e) {
            throw new XMLRPCRuntimeException((Exception) e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18323(Element element) throws XMLRPCException {
        HashMap hashMap = new HashMap();
        for (int i = 0; i < element.getChildNodes().getLength(); i++) {
            Node item = element.getChildNodes().item(i);
            if ((item.getNodeType() != 3 || item.getNodeValue().trim().length() > 0) && item.getNodeType() != 8) {
                if (item.getNodeType() != 1 || !"member".equals(item.getNodeName())) {
                    throw new XMLRPCException("Only struct members allowed within a struct.");
                }
                String str = null;
                Object obj = null;
                for (int i2 = 0; i2 < item.getChildNodes().getLength(); i2++) {
                    Node item2 = item.getChildNodes().item(i2);
                    if ((item2.getNodeType() != 3 || item2.getNodeValue().trim().length() > 0) && item2.getNodeType() != 8) {
                        if ("name".equals(item2.getNodeName())) {
                            if (str != null) {
                                throw new XMLRPCException("Name of a struct member cannot be set twice.");
                            }
                            str = XMLUtil.m18295(item2.getChildNodes());
                        } else if (item2.getNodeType() != 1 || !"value".equals(item2.getNodeName())) {
                            throw new XMLRPCException("A struct member must only contain one name and one value.");
                        } else if (obj != null) {
                            throw new XMLRPCException("Value of a struct member cannot be set twice.");
                        } else {
                            obj = this.f14557.m18319((Element) item2);
                        }
                    }
                }
                hashMap.put(str, obj);
            }
        }
        return hashMap;
    }
}
