package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import de.timroes.base64.Base64;
import org.w3c.dom.Element;

public class Base64Serializer implements Serializer {
    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18301(Object obj) {
        return XMLUtil.m18297("base64", Base64.m18330((Byte[]) obj));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18302(Element element) throws XMLRPCException {
        return Base64.m18331(XMLUtil.m18295(element.getChildNodes()));
    }
}
