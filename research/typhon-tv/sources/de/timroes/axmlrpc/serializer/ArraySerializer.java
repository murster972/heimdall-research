package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCRuntimeException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import java.util.ArrayList;
import java.util.Arrays;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class ArraySerializer implements Serializer {

    /* renamed from: 龘  reason: contains not printable characters */
    private final SerializerHandler f14541;

    public ArraySerializer(SerializerHandler serializerHandler) {
        this.f14541 = serializerHandler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18299(Object obj) {
        Iterable asList = obj instanceof Iterable ? (Iterable) obj : Arrays.asList((Object[]) obj);
        XmlElement xmlElement = new XmlElement("array");
        XmlElement xmlElement2 = new XmlElement("data");
        xmlElement.m18325(xmlElement2);
        try {
            for (Object next : asList) {
                XmlElement xmlElement3 = new XmlElement("value");
                xmlElement3.m18325(this.f14541.m18318(next));
                xmlElement2.m18325(xmlElement3);
            }
            return xmlElement;
        } catch (XMLRPCException e) {
            throw new XMLRPCRuntimeException((Exception) e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18300(Element element) throws XMLRPCException {
        ArrayList arrayList = new ArrayList();
        Element r0 = XMLUtil.m18298(element.getChildNodes());
        if (!"data".equals(r0.getNodeName())) {
            throw new XMLRPCException("The array must contain one data tag.");
        }
        for (int i = 0; i < r0.getChildNodes().getLength(); i++) {
            Node item = r0.getChildNodes().item(i);
            if (item != null && ((item.getNodeType() != 3 || item.getNodeValue().trim().length() > 0) && item.getNodeType() != 8)) {
                if (item.getNodeType() != 1) {
                    throw new XMLRPCException("Wrong element inside of array.");
                }
                arrayList.add(this.f14541.m18319((Element) item));
            }
        }
        return arrayList.toArray();
    }
}
