package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

public class IntSerializer implements Serializer {
    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18310(Object obj) {
        return XMLUtil.m18297("int", obj.toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18311(Element element) throws XMLRPCException {
        return Integer.valueOf(Integer.parseInt(XMLUtil.m18295(element.getChildNodes())));
    }
}
