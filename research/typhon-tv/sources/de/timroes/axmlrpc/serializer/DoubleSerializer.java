package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import java.math.BigDecimal;
import org.w3c.dom.Element;

public class DoubleSerializer implements Serializer {
    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18308(Object obj) {
        return XMLUtil.m18297("double", BigDecimal.valueOf(((Number) obj).doubleValue()).toPlainString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18309(Element element) throws XMLRPCException {
        return Double.valueOf(XMLUtil.m18295(element.getChildNodes()));
    }
}
