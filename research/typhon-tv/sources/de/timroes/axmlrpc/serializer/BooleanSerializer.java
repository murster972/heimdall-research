package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import net.pubnative.library.request.PubnativeRequest;
import org.w3c.dom.Element;

public class BooleanSerializer implements Serializer {
    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18303(Object obj) {
        return XMLUtil.m18297("boolean", ((Boolean) obj).booleanValue() ? PubnativeRequest.LEGACY_ZONE_ID : "0");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18304(Element element) throws XMLRPCException {
        return XMLUtil.m18295(element.getChildNodes()).equals(PubnativeRequest.LEGACY_ZONE_ID) ? Boolean.TRUE : Boolean.FALSE;
    }
}
