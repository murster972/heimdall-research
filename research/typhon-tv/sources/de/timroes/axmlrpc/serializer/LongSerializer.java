package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import org.w3c.dom.Element;

class LongSerializer implements Serializer {
    LongSerializer() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18312(Object obj) {
        return XMLUtil.m18297("i8", ((Long) obj).toString());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18313(Element element) throws XMLRPCException {
        return Long.valueOf(Long.parseLong(XMLUtil.m18295(element.getChildNodes())));
    }
}
