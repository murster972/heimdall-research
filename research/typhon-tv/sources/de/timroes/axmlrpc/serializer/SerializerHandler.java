package de.timroes.axmlrpc.serializer;

import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLUtil;
import de.timroes.axmlrpc.xmlcreator.XmlElement;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.w3c.dom.Element;

public class SerializerHandler {

    /* renamed from: ʻ  reason: contains not printable characters */
    private DoubleSerializer f14544;

    /* renamed from: ʼ  reason: contains not printable characters */
    private DateTimeSerializer f14545;

    /* renamed from: ʽ  reason: contains not printable characters */
    private ArraySerializer f14546;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Base64Serializer f14547;

    /* renamed from: ٴ  reason: contains not printable characters */
    private NullSerializer f14548;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f14549;

    /* renamed from: 连任  reason: contains not printable characters */
    private StructSerializer f14550;

    /* renamed from: 靐  reason: contains not printable characters */
    private BooleanSerializer f14551;

    /* renamed from: 麤  reason: contains not printable characters */
    private LongSerializer f14552;

    /* renamed from: 齉  reason: contains not printable characters */
    private IntSerializer f14553;

    /* renamed from: 龘  reason: contains not printable characters */
    private StringSerializer f14554;

    public SerializerHandler() {
        this(8192);
    }

    public SerializerHandler(int i) {
        boolean z = true;
        this.f14551 = new BooleanSerializer();
        this.f14553 = new IntSerializer();
        this.f14552 = new LongSerializer();
        this.f14544 = new DoubleSerializer();
        this.f14547 = new Base64Serializer();
        this.f14548 = new NullSerializer();
        this.f14549 = i;
        this.f14554 = new StringSerializer((i & 4096) == 0, (i & 2048) == 0);
        this.f14550 = new StructSerializer(this);
        this.f14546 = new ArraySerializer(this);
        this.f14545 = new DateTimeSerializer((i & 16384) == 0 ? false : z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public XmlElement m18318(Object obj) throws XMLRPCException {
        Serializer serializer;
        if ((this.f14549 & 8) != 0 && obj == null) {
            serializer = this.f14548;
        } else if (obj instanceof String) {
            serializer = this.f14554;
        } else if (obj instanceof Boolean) {
            serializer = this.f14551;
        } else if ((obj instanceof Double) || (obj instanceof Float) || (obj instanceof BigDecimal)) {
            serializer = this.f14544;
        } else if ((obj instanceof Integer) || (obj instanceof Short) || (obj instanceof Byte)) {
            serializer = this.f14553;
        } else if (obj instanceof Long) {
            if ((this.f14549 & 2) != 0) {
                serializer = this.f14552;
            } else {
                long longValue = ((Long) obj).longValue();
                if (longValue > 2147483647L || longValue < -2147483648L) {
                    throw new XMLRPCException("FLAGS_8BYTE_INT must be set, if values outside the 4 byte integer range should be transfered.");
                }
                serializer = this.f14553;
            }
        } else if (obj instanceof Date) {
            serializer = this.f14545;
        } else if (obj instanceof Calendar) {
            obj = ((Calendar) obj).getTime();
            serializer = this.f14545;
        } else if (obj instanceof Map) {
            serializer = this.f14550;
        } else if (obj instanceof byte[]) {
            byte[] bArr = (byte[]) obj;
            Byte[] bArr2 = new Byte[bArr.length];
            for (int i = 0; i < bArr2.length; i++) {
                bArr2[i] = new Byte(bArr[i]);
            }
            obj = bArr2;
            serializer = this.f14547;
        } else if (obj instanceof Byte[]) {
            serializer = this.f14547;
        } else if ((obj instanceof Iterable) || (obj instanceof Object[])) {
            serializer = this.f14546;
        } else {
            throw new XMLRPCException("No serializer found for type '" + obj.getClass().getName() + "'.");
        }
        return serializer.m18316(obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Object m18319(Element element) throws XMLRPCException {
        Serializer serializer;
        if (!"value".equals(element.getNodeName())) {
            throw new XMLRPCException("Value tag is missing around value.");
        } else if (XMLUtil.m18296(element.getChildNodes())) {
            Element r0 = XMLUtil.m18298(element.getChildNodes());
            String nodeName = (this.f14549 & 512) != 0 ? r0.getLocalName() == null ? r0.getNodeName() : r0.getLocalName() : r0.getNodeName();
            if ((this.f14549 & 8) != 0 && "nil".equals(nodeName)) {
                serializer = this.f14548;
            } else if ("string".equals(nodeName)) {
                serializer = this.f14554;
            } else if ("boolean".equals(nodeName)) {
                serializer = this.f14551;
            } else if ("double".equals(nodeName)) {
                serializer = this.f14544;
            } else if ("int".equals(nodeName) || "i4".equals(nodeName)) {
                serializer = this.f14553;
            } else if ("dateTime.iso8601".equals(nodeName)) {
                serializer = this.f14545;
            } else if ("i8".equals(nodeName)) {
                if ((this.f14549 & 2) != 0) {
                    serializer = this.f14552;
                } else {
                    throw new XMLRPCException("8 byte integer is not in the specification. You must use FLAGS_8BYTE_INT to enable the i8 tag.");
                }
            } else if ("struct".equals(nodeName)) {
                serializer = this.f14550;
            } else if ("array".equals(nodeName)) {
                serializer = this.f14546;
            } else if ("base64".equals(nodeName)) {
                serializer = this.f14547;
            } else {
                throw new XMLRPCException("No deserializer found for type '" + nodeName + "'.");
            }
            return serializer.m18317(r0);
        } else if ((this.f14549 & 256) != 0) {
            return this.f14554.m18321(element);
        } else {
            throw new XMLRPCException("Missing type element inside of value element.");
        }
    }
}
