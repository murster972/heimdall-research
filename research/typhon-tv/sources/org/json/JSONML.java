package org.json;

import java.util.Iterator;

public class JSONML {
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x017a, code lost:
        throw r13.syntaxError("Reserved attribute.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object parse(org.json.XMLTokener r13, boolean r14, org.json.JSONArray r15) throws org.json.JSONException {
        /*
            r2 = 0
            r4 = 0
            r6 = 0
            r8 = 0
        L_0x0004:
            boolean r10 = r13.more()
            if (r10 != 0) goto L_0x0012
            java.lang.String r10 = "Bad XML"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x0012:
            java.lang.Object r9 = r13.nextContent()
            java.lang.Character r10 = org.json.XML.LT
            if (r9 != r10) goto L_0x0216
            java.lang.Object r9 = r13.nextToken()
            boolean r10 = r9 instanceof java.lang.Character
            if (r10 == 0) goto L_0x00e1
            java.lang.Character r10 = org.json.XML.SLASH
            if (r9 != r10) goto L_0x0062
            java.lang.Object r9 = r13.nextToken()
            boolean r10 = r9 instanceof java.lang.String
            if (r10 != 0) goto L_0x004f
            org.json.JSONException r10 = new org.json.JSONException
            java.lang.StringBuffer r11 = new java.lang.StringBuffer
            r11.<init>()
            java.lang.String r12 = "Expected a closing name instead of '"
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.StringBuffer r11 = r11.append(r9)
            java.lang.String r12 = "'."
            java.lang.StringBuffer r11 = r11.append(r12)
            java.lang.String r11 = r11.toString()
            r10.<init>((java.lang.String) r11)
            throw r10
        L_0x004f:
            java.lang.Object r10 = r13.nextToken()
            java.lang.Character r11 = org.json.XML.GT
            if (r10 == r11) goto L_0x005f
            java.lang.String r10 = "Misshaped close tag"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x005f:
            r7 = r6
            r5 = r4
        L_0x0061:
            return r9
        L_0x0062:
            java.lang.Character r10 = org.json.XML.BANG
            if (r9 != r10) goto L_0x00cd
            char r1 = r13.next()
            r10 = 45
            if (r1 != r10) goto L_0x0081
            char r10 = r13.next()
            r11 = 45
            if (r10 != r11) goto L_0x007d
            java.lang.String r10 = "-->"
            r13.skipPast(r10)
            goto L_0x0004
        L_0x007d:
            r13.back()
            goto L_0x0004
        L_0x0081:
            r10 = 91
            if (r1 != r10) goto L_0x00ad
            java.lang.Object r9 = r13.nextToken()
            java.lang.String r10 = "CDATA"
            boolean r10 = r9.equals(r10)
            if (r10 == 0) goto L_0x00a5
            char r10 = r13.next()
            r11 = 91
            if (r10 != r11) goto L_0x00a5
            if (r15 == 0) goto L_0x0004
            java.lang.String r10 = r13.nextCDATA()
            r15.put((java.lang.Object) r10)
            goto L_0x0004
        L_0x00a5:
            java.lang.String r10 = "Expected 'CDATA['"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x00ad:
            r3 = 1
        L_0x00ae:
            java.lang.Object r9 = r13.nextMeta()
            if (r9 != 0) goto L_0x00bc
            java.lang.String r10 = "Missing '>' after '<!'."
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x00bc:
            java.lang.Character r10 = org.json.XML.LT
            if (r9 != r10) goto L_0x00c6
            int r3 = r3 + 1
        L_0x00c2:
            if (r3 > 0) goto L_0x00ae
            goto L_0x0004
        L_0x00c6:
            java.lang.Character r10 = org.json.XML.GT
            if (r9 != r10) goto L_0x00c2
            int r3 = r3 + -1
            goto L_0x00c2
        L_0x00cd:
            java.lang.Character r10 = org.json.XML.QUEST
            if (r9 != r10) goto L_0x00d9
            java.lang.String r10 = "?>"
            r13.skipPast(r10)
            goto L_0x0004
        L_0x00d9:
            java.lang.String r10 = "Misshaped tag"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x00e1:
            boolean r10 = r9 instanceof java.lang.String
            if (r10 != 0) goto L_0x0105
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.String r11 = "Bad tagName '"
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.StringBuffer r10 = r10.append(r9)
            java.lang.String r11 = "'."
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x0105:
            r8 = r9
            java.lang.String r8 = (java.lang.String) r8
            org.json.JSONArray r4 = new org.json.JSONArray
            r4.<init>()
            org.json.JSONObject r6 = new org.json.JSONObject
            r6.<init>()
            if (r14 == 0) goto L_0x012e
            r4.put((java.lang.Object) r8)
            if (r15 == 0) goto L_0x011c
            r15.put((java.lang.Object) r4)
        L_0x011c:
            r9 = 0
        L_0x011d:
            if (r9 != 0) goto L_0x0227
            java.lang.Object r9 = r13.nextToken()
            r0 = r9
        L_0x0124:
            if (r0 != 0) goto L_0x013a
            java.lang.String r10 = "Misshaped tag"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x012e:
            java.lang.String r10 = "tagName"
            r6.put((java.lang.String) r10, (java.lang.Object) r8)
            if (r15 == 0) goto L_0x011c
            r15.put((java.lang.Object) r6)
            goto L_0x011c
        L_0x013a:
            boolean r10 = r0 instanceof java.lang.String
            if (r10 != 0) goto L_0x015d
            if (r14 == 0) goto L_0x0149
            int r10 = r6.length()
            if (r10 <= 0) goto L_0x0149
            r4.put((java.lang.Object) r6)
        L_0x0149:
            java.lang.Character r10 = org.json.XML.SLASH
            if (r0 != r10) goto L_0x01b4
            java.lang.Object r10 = r13.nextToken()
            java.lang.Character r11 = org.json.XML.GT
            if (r10 == r11) goto L_0x01a6
            java.lang.String r10 = "Misshaped tag"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x015d:
            java.lang.String r0 = (java.lang.String) r0
            if (r14 != 0) goto L_0x017b
            java.lang.String r10 = "tagName"
            boolean r10 = r10.equals(r0)
            if (r10 != 0) goto L_0x0173
            java.lang.String r10 = "childNode"
            boolean r10 = r10.equals(r0)
            if (r10 == 0) goto L_0x017b
        L_0x0173:
            java.lang.String r10 = "Reserved attribute."
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x017b:
            java.lang.Object r9 = r13.nextToken()
            java.lang.Character r10 = org.json.XML.EQ
            if (r9 != r10) goto L_0x019e
            java.lang.Object r9 = r13.nextToken()
            boolean r10 = r9 instanceof java.lang.String
            if (r10 != 0) goto L_0x0193
            java.lang.String r10 = "Missing value"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x0193:
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r10 = org.json.XML.stringToValue(r9)
            r6.accumulate(r0, r10)
            r9 = 0
            goto L_0x011d
        L_0x019e:
            java.lang.String r10 = ""
            r6.accumulate(r0, r10)
            goto L_0x011d
        L_0x01a6:
            if (r15 != 0) goto L_0x0004
            if (r14 == 0) goto L_0x01af
            r7 = r6
            r5 = r4
            r9 = r4
            goto L_0x0061
        L_0x01af:
            r7 = r6
            r5 = r4
            r9 = r6
            goto L_0x0061
        L_0x01b4:
            java.lang.Character r10 = org.json.XML.GT
            if (r0 == r10) goto L_0x01c0
            java.lang.String r10 = "Misshaped tag"
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x01c0:
            java.lang.Object r2 = parse(r13, r14, r4)
            java.lang.String r2 = (java.lang.String) r2
            if (r2 == 0) goto L_0x0004
            boolean r10 = r2.equals(r8)
            if (r10 != 0) goto L_0x01f9
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.String r11 = "Mismatched '"
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.StringBuffer r10 = r10.append(r8)
            java.lang.String r11 = "' and '"
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.StringBuffer r10 = r10.append(r2)
            java.lang.String r11 = "'"
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            org.json.JSONException r10 = r13.syntaxError(r10)
            throw r10
        L_0x01f9:
            r8 = 0
            if (r14 != 0) goto L_0x0208
            int r10 = r4.length()
            if (r10 <= 0) goto L_0x0208
            java.lang.String r10 = "childNodes"
            r6.put((java.lang.String) r10, (java.lang.Object) r4)
        L_0x0208:
            if (r15 != 0) goto L_0x0004
            if (r14 == 0) goto L_0x0211
            r7 = r6
            r5 = r4
            r9 = r4
            goto L_0x0061
        L_0x0211:
            r7 = r6
            r5 = r4
            r9 = r6
            goto L_0x0061
        L_0x0216:
            if (r15 == 0) goto L_0x0004
            boolean r10 = r9 instanceof java.lang.String
            if (r10 == 0) goto L_0x0222
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = org.json.XML.stringToValue(r9)
        L_0x0222:
            r15.put((java.lang.Object) r9)
            goto L_0x0004
        L_0x0227:
            r0 = r9
            goto L_0x0124
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.JSONML.parse(org.json.XMLTokener, boolean, org.json.JSONArray):java.lang.Object");
    }

    public static JSONArray toJSONArray(String string) throws JSONException {
        return toJSONArray(new XMLTokener(string));
    }

    public static JSONArray toJSONArray(XMLTokener x) throws JSONException {
        return (JSONArray) parse(x, true, (JSONArray) null);
    }

    public static JSONObject toJSONObject(XMLTokener x) throws JSONException {
        return (JSONObject) parse(x, false, (JSONArray) null);
    }

    public static JSONObject toJSONObject(String string) throws JSONException {
        return toJSONObject(new XMLTokener(string));
    }

    public static String toString(JSONArray ja) throws JSONException {
        int i;
        StringBuffer sb = new StringBuffer();
        String tagName = ja.getString(0);
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Object object = ja.opt(1);
        if (object instanceof JSONObject) {
            i = 2;
            JSONObject jo = (JSONObject) object;
            Iterator keys = jo.keys();
            while (keys.hasNext()) {
                String key = keys.next().toString();
                XML.noSpace(key);
                String value = jo.optString(key);
                if (value != null) {
                    sb.append(' ');
                    sb.append(XML.escape(key));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(value));
                    sb.append('\"');
                }
            }
        } else {
            i = 1;
        }
        int length = ja.length();
        if (i >= length) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            do {
                Object object2 = ja.get(i);
                i++;
                if (object2 != null) {
                    if (object2 instanceof String) {
                        sb.append(XML.escape(object2.toString()));
                        continue;
                    } else if (object2 instanceof JSONObject) {
                        sb.append(toString((JSONObject) object2));
                        continue;
                    } else if (object2 instanceof JSONArray) {
                        sb.append(toString((JSONArray) object2));
                        continue;
                    } else {
                        continue;
                    }
                }
            } while (i < length);
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }

    public static String toString(JSONObject jo) throws JSONException {
        StringBuffer sb = new StringBuffer();
        String tagName = jo.optString("tagName");
        if (tagName == null) {
            return XML.escape(jo.toString());
        }
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Iterator keys = jo.keys();
        while (keys.hasNext()) {
            String key = keys.next().toString();
            if (!"tagName".equals(key) && !"childNodes".equals(key)) {
                XML.noSpace(key);
                String value = jo.optString(key);
                if (value != null) {
                    sb.append(' ');
                    sb.append(XML.escape(key));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(value));
                    sb.append('\"');
                }
            }
        }
        JSONArray ja = jo.optJSONArray("childNodes");
        if (ja == null) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            int length = ja.length();
            for (int i = 0; i < length; i++) {
                Object object = ja.get(i);
                if (object != null) {
                    if (object instanceof String) {
                        sb.append(XML.escape(object.toString()));
                    } else if (object instanceof JSONObject) {
                        sb.append(toString((JSONObject) object));
                    } else if (object instanceof JSONArray) {
                        sb.append(toString((JSONArray) object));
                    } else {
                        sb.append(object.toString());
                    }
                }
            }
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }
}
