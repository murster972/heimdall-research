package org.jsoup.select;

import java.util.Iterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.NodeFilter;

public class NodeTraversor {
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m22416(NodeVisitor visitor, Node root) {
        Node node = root;
        int depth = 0;
        while (node != null) {
            visitor.m22419(node, depth);
            if (node.m21948() > 0) {
                node = node.m21943(0);
                depth++;
            } else {
                while (node.m21941() == null && depth > 0) {
                    visitor.m22418(node, depth);
                    node = node.m21923();
                    depth--;
                }
                visitor.m22418(node, depth);
                if (node != root) {
                    node = node.m21941();
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m22417(NodeVisitor visitor, Elements elements) {
        Validate.m21695((Object) visitor);
        Validate.m21695((Object) elements);
        Iterator it2 = elements.iterator();
        while (it2.hasNext()) {
            m22416(visitor, (Node) (Element) it2.next());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static NodeFilter.FilterResult m22414(NodeFilter filter, Node root) {
        Node node = root;
        int depth = 0;
        while (node != null) {
            NodeFilter.FilterResult result = filter.m22413(node, depth);
            if (result == NodeFilter.FilterResult.STOP) {
                return result;
            }
            if (result != NodeFilter.FilterResult.CONTINUE || node.m21948() <= 0) {
                while (node.m21941() == null && depth > 0) {
                    if ((result == NodeFilter.FilterResult.CONTINUE || result == NodeFilter.FilterResult.SKIP_CHILDREN) && (result = filter.m22412(node, depth)) == NodeFilter.FilterResult.STOP) {
                        return result;
                    }
                    Node prev = node;
                    node = node.m21923();
                    depth--;
                    if (result == NodeFilter.FilterResult.REMOVE) {
                        prev.m21930();
                    }
                    result = NodeFilter.FilterResult.CONTINUE;
                }
                if ((result == NodeFilter.FilterResult.CONTINUE || result == NodeFilter.FilterResult.SKIP_CHILDREN) && (result = filter.m22412(node, depth)) == NodeFilter.FilterResult.STOP) {
                    return result;
                }
                if (node == root) {
                    return result;
                }
                Node prev2 = node;
                node = node.m21941();
                if (result == NodeFilter.FilterResult.REMOVE) {
                    prev2.m21930();
                }
            } else {
                node = node.m21943(0);
                depth++;
            }
        }
        return NodeFilter.FilterResult.CONTINUE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:1:0x000a A[LOOP:0: B:1:0x000a->B:4:0x001c, LOOP_START] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m22415(org.jsoup.select.NodeFilter r4, org.jsoup.select.Elements r5) {
        /*
            org.jsoup.helper.Validate.m21695((java.lang.Object) r4)
            org.jsoup.helper.Validate.m21695((java.lang.Object) r5)
            java.util.Iterator r1 = r5.iterator()
        L_0x000a:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x001e
            java.lang.Object r0 = r1.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            org.jsoup.select.NodeFilter$FilterResult r2 = m22414((org.jsoup.select.NodeFilter) r4, (org.jsoup.nodes.Node) r0)
            org.jsoup.select.NodeFilter$FilterResult r3 = org.jsoup.select.NodeFilter.FilterResult.STOP
            if (r2 != r3) goto L_0x000a
        L_0x001e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.select.NodeTraversor.m22415(org.jsoup.select.NodeFilter, org.jsoup.select.Elements):void");
    }
}
