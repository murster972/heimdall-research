package org.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;

public class Elements extends ArrayList<Element> {
    public Elements() {
    }

    public Elements(int initialCapacity) {
        super(initialCapacity);
    }

    public Elements(Collection<Element> elements) {
        super(elements);
    }

    public Elements(List<Element> elements) {
        super(elements);
    }

    public Elements(Element... elements) {
        super(Arrays.asList(elements));
    }

    public Elements clone() {
        Elements clone = new Elements(size());
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            clone.add(((Element) it2.next()).m21845());
        }
        return clone;
    }

    public String attr(String attributeKey) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (element.m21951(attributeKey)) {
                return element.m21947(attributeKey);
            }
        }
        return "";
    }

    public boolean hasAttr(String attributeKey) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m21951(attributeKey)) {
                return true;
            }
        }
        return false;
    }

    public List<String> eachAttr(String attributeKey) {
        List<String> attrs = new ArrayList<>(size());
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (element.m21951(attributeKey)) {
                attrs.add(element.m21947(attributeKey));
            }
        }
        return attrs;
    }

    public Elements attr(String attributeKey, String attributeValue) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21865(attributeKey, attributeValue);
        }
        return this;
    }

    public Elements removeAttr(String attributeKey) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21944(attributeKey);
        }
        return this;
    }

    public Elements addClass(String className) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21829(className);
        }
        return this;
    }

    public Elements removeClass(String className) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21832(className);
        }
        return this;
    }

    public Elements toggleClass(String className) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21834(className);
        }
        return this;
    }

    public boolean hasClass(String className) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m21872(className)) {
                return true;
            }
        }
        return false;
    }

    public String val() {
        if (size() > 0) {
            return first().m21814();
        }
        return "";
    }

    public Elements val(String value) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21823(value);
        }
        return this;
    }

    public String text() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(StringUtils.SPACE);
            }
            sb.append(element.m21868());
        }
        return sb.toString();
    }

    public boolean hasText() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m21846()) {
                return true;
            }
        }
        return false;
    }

    public List<String> eachText() {
        ArrayList<String> texts = new ArrayList<>(size());
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element el = (Element) it2.next();
            if (el.m21846()) {
                texts.add(el.m21868());
            }
        }
        return texts;
    }

    public String html() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(StringUtils.LF);
            }
            sb.append(element.m21822());
        }
        return sb.toString();
    }

    public String outerHtml() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(StringUtils.LF);
            }
            sb.append(element.m21913());
        }
        return sb.toString();
    }

    public String toString() {
        return outerHtml();
    }

    public Elements tagName(String tagName) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21813(tagName);
        }
        return this;
    }

    public Elements html(String html) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21827(html);
        }
        return this;
    }

    public Elements prepend(String html) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21844(html);
        }
        return this;
    }

    public Elements append(String html) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21843(html);
        }
        return this;
    }

    public Elements before(String html) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21841(html);
        }
        return this;
    }

    public Elements after(String html) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21839(html);
        }
        return this;
    }

    public Elements wrap(String html) {
        Validate.m21697(html);
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21835(html);
        }
        return this;
    }

    public Elements unwrap() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21927();
        }
        return this;
    }

    public Elements empty() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21840();
        }
        return this;
    }

    public Elements remove() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m21930();
        }
        return this;
    }

    public Elements select(String query) {
        return Selector.m22441(query, (Iterable<Element>) this);
    }

    public Elements not(String query) {
        return Selector.m22443((Collection<Element>) this, (Collection<Element>) Selector.m22441(query, (Iterable<Element>) this));
    }

    public Elements eq(int index) {
        if (size() <= index) {
            return new Elements();
        }
        return new Elements((Element) get(index));
    }

    public boolean is(String query) {
        Evaluator eval = QueryParser.m22434(query);
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m21867(eval)) {
                return true;
            }
        }
        return false;
    }

    public Elements next() {
        return m22371((String) null, true, false);
    }

    public Elements next(String query) {
        return m22371(query, true, false);
    }

    public Elements nextAll() {
        return m22371((String) null, true, true);
    }

    public Elements nextAll(String query) {
        return m22371(query, true, true);
    }

    public Elements prev() {
        return m22371((String) null, false, false);
    }

    public Elements prev(String query) {
        return m22371(query, false, false);
    }

    public Elements prevAll() {
        return m22371((String) null, false, true);
    }

    public Elements prevAll(String query) {
        return m22371(query, false, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Elements m22371(String query, boolean next, boolean all) {
        Elements els = new Elements();
        Evaluator eval = query != null ? QueryParser.m22434(query) : null;
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element e = (Element) it2.next();
            do {
                Element sib = next ? e.m21849() : e.m21850();
                if (sib == null) {
                    break;
                }
                if (eval == null) {
                    els.add(sib);
                } else if (sib.m21867(eval)) {
                    els.add(sib);
                }
                e = sib;
            } while (all);
        }
        return els;
    }

    public Elements parents() {
        HashSet<Element> combo = new LinkedHashSet<>();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            combo.addAll(((Element) it2.next()).m21824());
        }
        return new Elements((Collection<Element>) combo);
    }

    public Element first() {
        if (isEmpty()) {
            return null;
        }
        return (Element) get(0);
    }

    public Element last() {
        if (isEmpty()) {
            return null;
        }
        return (Element) get(size() - 1);
    }

    public Elements traverse(NodeVisitor nodeVisitor) {
        NodeTraversor.m22417(nodeVisitor, this);
        return this;
    }

    public Elements filter(NodeFilter nodeFilter) {
        NodeTraversor.m22415(nodeFilter, this);
        return this;
    }

    public List<FormElement> forms() {
        ArrayList<FormElement> forms = new ArrayList<>();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element el = (Element) it2.next();
            if (el instanceof FormElement) {
                forms.add((FormElement) el);
            }
        }
        return forms;
    }
}
