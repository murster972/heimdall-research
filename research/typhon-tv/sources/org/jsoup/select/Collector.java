package org.jsoup.select;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.NodeFilter;

public class Collector {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m22359(Evaluator eval, Element root) {
        Elements elements = new Elements();
        NodeTraversor.m22416((NodeVisitor) new Accumulator(root, elements, eval), (Node) root);
        return elements;
    }

    private static class Accumulator implements NodeVisitor {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Elements f17525;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Evaluator f17526;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Element f17527;

        Accumulator(Element root, Elements elements, Evaluator eval) {
            this.f17527 = root;
            this.f17525 = elements;
            this.f17526 = eval;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m22361(Node node, int depth) {
            if (node instanceof Element) {
                Element el = (Element) node;
                if (this.f17526.m22372(this.f17527, el)) {
                    this.f17525.add(el);
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m22360(Node node, int depth) {
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Element m22358(Evaluator eval, Element root) {
        FirstFinder finder = new FirstFinder(root, eval);
        NodeTraversor.m22414((NodeFilter) finder, (Node) root);
        return finder.f17528;
    }

    private static class FirstFinder implements NodeFilter {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public Element f17528 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Evaluator f17529;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Element f17530;

        FirstFinder(Element root, Evaluator eval) {
            this.f17530 = root;
            this.f17529 = eval;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public NodeFilter.FilterResult m22364(Node node, int depth) {
            if (node instanceof Element) {
                Element el = (Element) node;
                if (this.f17529.m22372(this.f17530, el)) {
                    this.f17528 = el;
                    return NodeFilter.FilterResult.STOP;
                }
            }
            return NodeFilter.FilterResult.CONTINUE;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public NodeFilter.FilterResult m22363(Node node, int depth) {
            return NodeFilter.FilterResult.CONTINUE;
        }
    }
}
