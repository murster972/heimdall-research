package org.jsoup.select;

import java.util.Iterator;
import java.util.regex.Pattern;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.PseudoTextElement;
import org.jsoup.nodes.TextNode;
import org.jsoup.nodes.XmlDeclaration;

public abstract class Evaluator {
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m22372(Element element, Element element2);

    protected Evaluator() {
    }

    public static final class Tag extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17549;

        public Tag(String tagName) {
            this.f17549 = tagName;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22410(Element root, Element element) {
            return element.m21869().equalsIgnoreCase(this.f17549);
        }

        public String toString() {
            return String.format("%s", new Object[]{this.f17549});
        }
    }

    public static final class TagEndsWith extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17550;

        public TagEndsWith(String tagName) {
            this.f17550 = tagName;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22411(Element root, Element element) {
            return element.m21869().endsWith(this.f17550);
        }

        public String toString() {
            return String.format("%s", new Object[]{this.f17550});
        }
    }

    public static final class Id extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17545;

        public Id(String id) {
            this.f17545 = id;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22389(Element root, Element element) {
            return this.f17545.equals(element.m21831());
        }

        public String toString() {
            return String.format("#%s", new Object[]{this.f17545});
        }
    }

    public static final class Class extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17539;

        public Class(String className) {
            this.f17539 = className;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22382(Element root, Element element) {
            return element.m21872(this.f17539);
        }

        public String toString() {
            return String.format(".%s", new Object[]{this.f17539});
        }
    }

    public static final class Attribute extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17533;

        public Attribute(String key) {
            this.f17533 = key;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22374(Element root, Element element) {
            return element.m21951(this.f17533);
        }

        public String toString() {
            return String.format("[%s]", new Object[]{this.f17533});
        }
    }

    public static final class AttributeStarting extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17536;

        public AttributeStarting(String keyPrefix) {
            Validate.m21697(keyPrefix);
            this.f17536 = Normalizer.m21704(keyPrefix);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22375(Element root, Element element) {
            for (org.jsoup.nodes.Attribute attribute : element.m21820().m21727()) {
                if (Normalizer.m21704(attribute.getKey()).startsWith(this.f17536)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return String.format("[^%s]", new Object[]{this.f17536});
        }
    }

    public static final class AttributeWithValue extends AttributeKeyPair {
        public AttributeWithValue(String key, String value) {
            super(key, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22376(Element root, Element element) {
            return element.m21951(this.f17535) && this.f17534.equalsIgnoreCase(element.m21947(this.f17535).trim());
        }

        public String toString() {
            return String.format("[%s=%s]", new Object[]{this.f17535, this.f17534});
        }
    }

    public static final class AttributeWithValueNot extends AttributeKeyPair {
        public AttributeWithValueNot(String key, String value) {
            super(key, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22380(Element root, Element element) {
            return !this.f17534.equalsIgnoreCase(element.m21947(this.f17535));
        }

        public String toString() {
            return String.format("[%s!=%s]", new Object[]{this.f17535, this.f17534});
        }
    }

    public static final class AttributeWithValueStarting extends AttributeKeyPair {
        public AttributeWithValueStarting(String key, String value) {
            super(key, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22381(Element root, Element element) {
            return element.m21951(this.f17535) && Normalizer.m21704(element.m21947(this.f17535)).startsWith(this.f17534);
        }

        public String toString() {
            return String.format("[%s^=%s]", new Object[]{this.f17535, this.f17534});
        }
    }

    public static final class AttributeWithValueEnding extends AttributeKeyPair {
        public AttributeWithValueEnding(String key, String value) {
            super(key, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22378(Element root, Element element) {
            return element.m21951(this.f17535) && Normalizer.m21704(element.m21947(this.f17535)).endsWith(this.f17534);
        }

        public String toString() {
            return String.format("[%s$=%s]", new Object[]{this.f17535, this.f17534});
        }
    }

    public static final class AttributeWithValueContaining extends AttributeKeyPair {
        public AttributeWithValueContaining(String key, String value) {
            super(key, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22377(Element root, Element element) {
            return element.m21951(this.f17535) && Normalizer.m21704(element.m21947(this.f17535)).contains(this.f17534);
        }

        public String toString() {
            return String.format("[%s*=%s]", new Object[]{this.f17535, this.f17534});
        }
    }

    public static final class AttributeWithValueMatching extends Evaluator {

        /* renamed from: 靐  reason: contains not printable characters */
        Pattern f17537;

        /* renamed from: 龘  reason: contains not printable characters */
        String f17538;

        public AttributeWithValueMatching(String key, Pattern pattern) {
            this.f17538 = Normalizer.m21703(key);
            this.f17537 = pattern;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22379(Element root, Element element) {
            return element.m21951(this.f17538) && this.f17537.matcher(element.m21947(this.f17538)).find();
        }

        public String toString() {
            return String.format("[%s~=%s]", new Object[]{this.f17538, this.f17537.toString()});
        }
    }

    public static abstract class AttributeKeyPair extends Evaluator {

        /* renamed from: 靐  reason: contains not printable characters */
        String f17534;

        /* renamed from: 龘  reason: contains not printable characters */
        String f17535;

        public AttributeKeyPair(String key, String value) {
            Validate.m21697(key);
            Validate.m21697(value);
            this.f17535 = Normalizer.m21703(key);
            if ((value.startsWith("\"") && value.endsWith("\"")) || (value.startsWith("'") && value.endsWith("'"))) {
                value = value.substring(1, value.length() - 1);
            }
            this.f17534 = Normalizer.m21703(value);
        }
    }

    public static final class AllElements extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22373(Element root, Element element) {
            return true;
        }

        public String toString() {
            return "*";
        }
    }

    public static final class IndexLessThan extends IndexEvaluator {
        public IndexLessThan(int index) {
            super(index);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22392(Element root, Element element) {
            return root != element && element.m21851() < this.f17546;
        }

        public String toString() {
            return String.format(":lt(%d)", new Object[]{Integer.valueOf(this.f17546)});
        }
    }

    public static final class IndexGreaterThan extends IndexEvaluator {
        public IndexGreaterThan(int index) {
            super(index);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22391(Element root, Element element) {
            return element.m21851() > this.f17546;
        }

        public String toString() {
            return String.format(":gt(%d)", new Object[]{Integer.valueOf(this.f17546)});
        }
    }

    public static final class IndexEquals extends IndexEvaluator {
        public IndexEquals(int index) {
            super(index);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22390(Element root, Element element) {
            return element.m21851() == this.f17546;
        }

        public String toString() {
            return String.format(":eq(%d)", new Object[]{Integer.valueOf(this.f17546)});
        }
    }

    public static final class IsLastChild extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22395(Element root, Element element) {
            Element p = element.m21819();
            return p != null && !(p instanceof Document) && element.m21851() == p.m21828().size() + -1;
        }

        public String toString() {
            return ":last-child";
        }
    }

    public static final class IsFirstOfType extends IsNthOfType {
        public IsFirstOfType() {
            super(0, 1);
        }

        public String toString() {
            return ":first-of-type";
        }
    }

    public static final class IsLastOfType extends IsNthLastOfType {
        public IsLastOfType() {
            super(0, 1);
        }

        public String toString() {
            return ":last-of-type";
        }
    }

    public static abstract class CssNthEvaluator extends Evaluator {

        /* renamed from: 靐  reason: contains not printable characters */
        protected final int f17543;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final int f17544;

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public abstract int m22386(Element element, Element element2);

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract String m22387();

        public CssNthEvaluator(int a, int b) {
            this.f17544 = a;
            this.f17543 = b;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22388(Element root, Element element) {
            Element p = element.m21819();
            if (p == null || (p instanceof Document)) {
                return false;
            }
            int pos = m22386(root, element);
            if (this.f17544 == 0) {
                if (pos != this.f17543) {
                    return false;
                }
                return true;
            } else if ((pos - this.f17543) * this.f17544 < 0 || (pos - this.f17543) % this.f17544 != 0) {
                return false;
            } else {
                return true;
            }
        }

        public String toString() {
            if (this.f17544 == 0) {
                return String.format(":%s(%d)", new Object[]{m22387(), Integer.valueOf(this.f17543)});
            } else if (this.f17543 == 0) {
                return String.format(":%s(%dn)", new Object[]{m22387(), Integer.valueOf(this.f17544)});
            } else {
                return String.format(":%s(%dn%+d)", new Object[]{m22387(), Integer.valueOf(this.f17544), Integer.valueOf(this.f17543)});
            }
        }
    }

    public static final class IsNthChild extends CssNthEvaluator {
        public IsNthChild(int a, int b) {
            super(a, b);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m22396(Element root, Element element) {
            return element.m21851() + 1;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m22397() {
            return "nth-child";
        }
    }

    public static final class IsNthLastChild extends CssNthEvaluator {
        public IsNthLastChild(int a, int b) {
            super(a, b);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m22398(Element root, Element element) {
            return element.m21819().m21828().size() - element.m21851();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m22399() {
            return "nth-last-child";
        }
    }

    public static class IsNthOfType extends CssNthEvaluator {
        public IsNthOfType(int a, int b) {
            super(a, b);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m22402(Element root, Element element) {
            int pos = 0;
            Iterator it2 = element.m21819().m21828().iterator();
            while (it2.hasNext()) {
                Element el = (Element) it2.next();
                if (el.m21871().equals(element.m21871())) {
                    pos++;
                    continue;
                }
                if (el == element) {
                    break;
                }
            }
            return pos;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m22403() {
            return "nth-of-type";
        }
    }

    public static class IsNthLastOfType extends CssNthEvaluator {
        public IsNthLastOfType(int a, int b) {
            super(a, b);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m22400(Element root, Element element) {
            int pos = 0;
            Elements family = element.m21819().m21828();
            for (int i = element.m21851(); i < family.size(); i++) {
                if (((Element) family.get(i)).m21871().equals(element.m21871())) {
                    pos++;
                }
            }
            return pos;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m22401() {
            return "nth-last-of-type";
        }
    }

    public static final class IsFirstChild extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22394(Element root, Element element) {
            Element p = element.m21819();
            return p != null && !(p instanceof Document) && element.m21851() == 0;
        }

        public String toString() {
            return ":first-child";
        }
    }

    public static final class IsRoot extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22406(Element root, Element element) {
            Element r;
            if (root instanceof Document) {
                r = root.m21862(0);
            } else {
                r = root;
            }
            if (element == r) {
                return true;
            }
            return false;
        }

        public String toString() {
            return ":root";
        }
    }

    public static final class IsOnlyChild extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22404(Element root, Element element) {
            Element p = element.m21819();
            return p != null && !(p instanceof Document) && element.m21847().size() == 0;
        }

        public String toString() {
            return ":only-child";
        }
    }

    public static final class IsOnlyOfType extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22405(Element root, Element element) {
            Element p = element.m21819();
            if (p == null || (p instanceof Document)) {
                return false;
            }
            int pos = 0;
            Iterator it2 = p.m21828().iterator();
            while (it2.hasNext()) {
                if (((Element) it2.next()).m21871().equals(element.m21871())) {
                    pos++;
                }
            }
            if (pos != 1) {
                return false;
            }
            return true;
        }

        public String toString() {
            return ":only-of-type";
        }
    }

    public static final class IsEmpty extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22393(Element root, Element element) {
            for (Node n : element.m21921()) {
                if (!(n instanceof Comment) && !(n instanceof XmlDeclaration) && !(n instanceof DocumentType)) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return ":empty";
        }
    }

    public static abstract class IndexEvaluator extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        int f17546;

        public IndexEvaluator(int index) {
            this.f17546 = index;
        }
    }

    public static final class ContainsText extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17542;

        public ContainsText(String searchText) {
            this.f17542 = Normalizer.m21704(searchText);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22385(Element root, Element element) {
            return Normalizer.m21704(element.m21868()).contains(this.f17542);
        }

        public String toString() {
            return String.format(":contains(%s)", new Object[]{this.f17542});
        }
    }

    public static final class ContainsData extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17540;

        public ContainsData(String searchText) {
            this.f17540 = Normalizer.m21704(searchText);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22383(Element root, Element element) {
            return Normalizer.m21704(element.m21848()).contains(this.f17540);
        }

        public String toString() {
            return String.format(":containsData(%s)", new Object[]{this.f17540});
        }
    }

    public static final class ContainsOwnText extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f17541;

        public ContainsOwnText(String searchText) {
            this.f17541 = Normalizer.m21704(searchText);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22384(Element root, Element element) {
            return Normalizer.m21704(element.m21873()).contains(this.f17541);
        }

        public String toString() {
            return String.format(":containsOwn(%s)", new Object[]{this.f17541});
        }
    }

    public static final class Matches extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private Pattern f17547;

        public Matches(Pattern pattern) {
            this.f17547 = pattern;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22408(Element root, Element element) {
            return this.f17547.matcher(element.m21868()).find();
        }

        public String toString() {
            return String.format(":matches(%s)", new Object[]{this.f17547});
        }
    }

    public static final class MatchesOwn extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private Pattern f17548;

        public MatchesOwn(Pattern pattern) {
            this.f17548 = pattern;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22409(Element root, Element element) {
            return this.f17548.matcher(element.m21873()).find();
        }

        public String toString() {
            return String.format(":matchesOwn(%s)", new Object[]{this.f17548});
        }
    }

    public static final class MatchText extends Evaluator {
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22407(Element root, Element element) {
            if (element instanceof PseudoTextElement) {
                return true;
            }
            for (TextNode textNode : element.m21838()) {
                PseudoTextElement pel = new PseudoTextElement(org.jsoup.parser.Tag.m22159(element.m21869()), element.m21858(), element.m21820());
                textNode.m21917(pel);
                pel.m21864((Node) textNode);
            }
            return false;
        }

        public String toString() {
            return ":matchText";
        }
    }
}
