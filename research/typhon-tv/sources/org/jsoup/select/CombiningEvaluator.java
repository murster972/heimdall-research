package org.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Element;

abstract class CombiningEvaluator extends Evaluator {

    /* renamed from: 靐  reason: contains not printable characters */
    int f17531;

    /* renamed from: 龘  reason: contains not printable characters */
    final ArrayList<Evaluator> f17532;

    CombiningEvaluator() {
        this.f17531 = 0;
        this.f17532 = new ArrayList<>();
    }

    CombiningEvaluator(Collection<Evaluator> evaluators) {
        this();
        this.f17532.addAll(evaluators);
        m22365();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Evaluator m22366() {
        if (this.f17531 > 0) {
            return this.f17532.get(this.f17531 - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22367(Evaluator replacement) {
        this.f17532.set(this.f17531 - 1, replacement);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22365() {
        this.f17531 = this.f17532.size();
    }

    static final class And extends CombiningEvaluator {
        And(Collection<Evaluator> evaluators) {
            super(evaluators);
        }

        And(Evaluator... evaluators) {
            this((Collection<Evaluator>) Arrays.asList(evaluators));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22368(Element root, Element node) {
            for (int i = 0; i < this.f17531; i++) {
                if (!((Evaluator) this.f17532.get(i)).m22372(root, node)) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return StringUtil.m21684((Collection) this.f17532, StringUtils.SPACE);
        }
    }

    static final class Or extends CombiningEvaluator {
        Or(Collection<Evaluator> evaluators) {
            if (this.f17531 > 1) {
                this.f17532.add(new And(evaluators));
            } else {
                this.f17532.addAll(evaluators);
            }
            m22365();
        }

        Or(Evaluator... evaluators) {
            this((Collection<Evaluator>) Arrays.asList(evaluators));
        }

        Or() {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m22369(Evaluator e) {
            this.f17532.add(e);
            m22365();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22370(Element root, Element node) {
            for (int i = 0; i < this.f17531; i++) {
                if (((Evaluator) this.f17532.get(i)).m22372(root, node)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return StringUtil.m21684((Collection) this.f17532, ", ");
        }
    }
}
