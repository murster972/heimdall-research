package org.jsoup.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.parser.TokenQueue;
import org.jsoup.select.CombiningEvaluator;
import org.jsoup.select.Evaluator;
import org.jsoup.select.Selector;
import org.jsoup.select.StructuralEvaluator;

public class QueryParser {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Pattern f17552 = Pattern.compile("(([+-])?(\\d+)?)n(\\s*([+-])?\\s*\\d+)?", 2);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Pattern f17553 = Pattern.compile("([+-])?(\\d+)");

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f17554 = {"=", "!=", "^=", "$=", "*=", "~="};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f17555 = {",", ">", "+", "~", StringUtils.SPACE};

    /* renamed from: 连任  reason: contains not printable characters */
    private List<Evaluator> f17556 = new ArrayList();

    /* renamed from: 麤  reason: contains not printable characters */
    private String f17557;

    /* renamed from: 齉  reason: contains not printable characters */
    private TokenQueue f17558;

    private QueryParser(String query) {
        this.f17557 = query;
        this.f17558 = new TokenQueue(query);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Evaluator m22434(String query) {
        try {
            return new QueryParser(query).m22439();
        } catch (IllegalArgumentException e) {
            throw new Selector.SelectorParseException(e.getMessage(), new Object[0]);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Evaluator m22439() {
        this.f17558.m22225();
        if (this.f17558.m22237(f17555)) {
            this.f17556.add(new StructuralEvaluator.Root());
            m22435(this.f17558.m22229());
        } else {
            m22433();
        }
        while (!this.f17558.m22234()) {
            boolean seenWhite = this.f17558.m22225();
            if (this.f17558.m22237(f17555)) {
                m22435(this.f17558.m22229());
            } else if (seenWhite) {
                m22435(' ');
            } else {
                m22433();
            }
        }
        if (this.f17556.size() == 1) {
            return this.f17556.get(0);
        }
        return new CombiningEvaluator.And((Collection<Evaluator>) this.f17556);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22435(char combinator) {
        Evaluator currentEval;
        Evaluator rootEval;
        CombiningEvaluator.Or or;
        Evaluator currentEval2;
        this.f17558.m22225();
        Evaluator newEval = m22434(m22430());
        boolean replaceRightMost = false;
        if (this.f17556.size() == 1) {
            currentEval = this.f17556.get(0);
            rootEval = currentEval;
            if ((rootEval instanceof CombiningEvaluator.Or) && combinator != ',') {
                currentEval = ((CombiningEvaluator.Or) currentEval).m22366();
                replaceRightMost = true;
            }
        } else {
            currentEval = new CombiningEvaluator.And((Collection<Evaluator>) this.f17556);
            rootEval = currentEval;
        }
        this.f17556.clear();
        if (combinator == '>') {
            currentEval2 = new CombiningEvaluator.And(newEval, new StructuralEvaluator.ImmediateParent(currentEval));
        } else if (combinator == ' ') {
            currentEval2 = new CombiningEvaluator.And(newEval, new StructuralEvaluator.Parent(currentEval));
        } else if (combinator == '+') {
            currentEval2 = new CombiningEvaluator.And(newEval, new StructuralEvaluator.ImmediatePreviousSibling(currentEval));
        } else if (combinator == '~') {
            currentEval2 = new CombiningEvaluator.And(newEval, new StructuralEvaluator.PreviousSibling(currentEval));
        } else if (combinator == ',') {
            if (currentEval instanceof CombiningEvaluator.Or) {
                or = (CombiningEvaluator.Or) currentEval;
                or.m22369(newEval);
            } else {
                or = new CombiningEvaluator.Or();
                or.m22369(currentEval);
                or.m22369(newEval);
            }
            currentEval2 = or;
        } else {
            throw new Selector.SelectorParseException("Unknown combinator: " + combinator, new Object[0]);
        }
        if (replaceRightMost) {
            ((CombiningEvaluator.Or) rootEval).m22367(currentEval2);
        } else {
            rootEval = currentEval2;
        }
        this.f17556.add(rootEval);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m22430() {
        StringBuilder sq = new StringBuilder();
        while (!this.f17558.m22234()) {
            if (this.f17558.m22235("(")) {
                sq.append("(").append(this.f17558.m22233('(', ')')).append(")");
            } else if (this.f17558.m22235("[")) {
                sq.append("[").append(this.f17558.m22233('[', ']')).append("]");
            } else if (this.f17558.m22237(f17555)) {
                break;
            } else {
                sq.append(this.f17558.m22229());
            }
        }
        return sq.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m22433() {
        if (this.f17558.m22228("#")) {
            m22432();
        } else if (this.f17558.m22228(".")) {
            m22429();
        } else if (this.f17558.m22232() || this.f17558.m22235("*|")) {
            m22420();
        } else if (this.f17558.m22235("[")) {
            m22421();
        } else if (this.f17558.m22228("*")) {
            m22422();
        } else if (this.f17558.m22228(":lt(")) {
            m22426();
        } else if (this.f17558.m22228(":gt(")) {
            m22427();
        } else if (this.f17558.m22228(":eq(")) {
            m22428();
        } else if (this.f17558.m22235(":has(")) {
            m22423();
        } else if (this.f17558.m22235(":contains(")) {
            m22436(false);
        } else if (this.f17558.m22235(":containsOwn(")) {
            m22436(true);
        } else if (this.f17558.m22235(":containsData(")) {
            m22424();
        } else if (this.f17558.m22235(":matches(")) {
            m22431(false);
        } else if (this.f17558.m22235(":matchesOwn(")) {
            m22431(true);
        } else if (this.f17558.m22235(":not(")) {
            m22438();
        } else if (this.f17558.m22228(":nth-child(")) {
            m22437(false, false);
        } else if (this.f17558.m22228(":nth-last-child(")) {
            m22437(true, false);
        } else if (this.f17558.m22228(":nth-of-type(")) {
            m22437(false, true);
        } else if (this.f17558.m22228(":nth-last-of-type(")) {
            m22437(true, true);
        } else if (this.f17558.m22228(":first-child")) {
            this.f17556.add(new Evaluator.IsFirstChild());
        } else if (this.f17558.m22228(":last-child")) {
            this.f17556.add(new Evaluator.IsLastChild());
        } else if (this.f17558.m22228(":first-of-type")) {
            this.f17556.add(new Evaluator.IsFirstOfType());
        } else if (this.f17558.m22228(":last-of-type")) {
            this.f17556.add(new Evaluator.IsLastOfType());
        } else if (this.f17558.m22228(":only-child")) {
            this.f17556.add(new Evaluator.IsOnlyChild());
        } else if (this.f17558.m22228(":only-of-type")) {
            this.f17556.add(new Evaluator.IsOnlyOfType());
        } else if (this.f17558.m22228(":empty")) {
            this.f17556.add(new Evaluator.IsEmpty());
        } else if (this.f17558.m22228(":root")) {
            this.f17556.add(new Evaluator.IsRoot());
        } else if (this.f17558.m22228(":matchText")) {
            this.f17556.add(new Evaluator.MatchText());
        } else {
            throw new Selector.SelectorParseException("Could not parse query '%s': unexpected token at '%s'", this.f17557, this.f17558.m22223());
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m22432() {
        String id = this.f17558.m22222();
        Validate.m21697(id);
        this.f17556.add(new Evaluator.Id(id));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m22429() {
        String className = this.f17558.m22222();
        Validate.m21697(className);
        this.f17556.add(new Evaluator.Class(className.trim()));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m22420() {
        String tagName = this.f17558.m22221();
        Validate.m21697(tagName);
        if (tagName.startsWith("*|")) {
            this.f17556.add(new CombiningEvaluator.Or(new Evaluator.Tag(Normalizer.m21703(tagName)), new Evaluator.TagEndsWith(Normalizer.m21703(tagName.replace("*|", ":")))));
            return;
        }
        if (tagName.contains("|")) {
            tagName = tagName.replace("|", ":");
        }
        this.f17556.add(new Evaluator.Tag(tagName.trim()));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m22421() {
        TokenQueue cq = new TokenQueue(this.f17558.m22233('[', ']'));
        String key = cq.m22226(f17554);
        Validate.m21697(key);
        cq.m22225();
        if (cq.m22234()) {
            if (key.startsWith("^")) {
                this.f17556.add(new Evaluator.AttributeStarting(key.substring(1)));
            } else {
                this.f17556.add(new Evaluator.Attribute(key));
            }
        } else if (cq.m22228("=")) {
            this.f17556.add(new Evaluator.AttributeWithValue(key, cq.m22223()));
        } else if (cq.m22228("!=")) {
            this.f17556.add(new Evaluator.AttributeWithValueNot(key, cq.m22223()));
        } else if (cq.m22228("^=")) {
            this.f17556.add(new Evaluator.AttributeWithValueStarting(key, cq.m22223()));
        } else if (cq.m22228("$=")) {
            this.f17556.add(new Evaluator.AttributeWithValueEnding(key, cq.m22223()));
        } else if (cq.m22228("*=")) {
            this.f17556.add(new Evaluator.AttributeWithValueContaining(key, cq.m22223()));
        } else if (cq.m22228("~=")) {
            this.f17556.add(new Evaluator.AttributeWithValueMatching(key, Pattern.compile(cq.m22223())));
        } else {
            throw new Selector.SelectorParseException("Could not parse attribute query '%s': unexpected token at '%s'", this.f17557, cq.m22223());
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m22422() {
        this.f17556.add(new Evaluator.AllElements());
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m22426() {
        this.f17556.add(new Evaluator.IndexLessThan(m22425()));
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m22427() {
        this.f17556.add(new Evaluator.IndexGreaterThan(m22425()));
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m22428() {
        this.f17556.add(new Evaluator.IndexEquals(m22425()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22437(boolean backwards, boolean ofType) {
        int b = 0;
        int a = 1;
        String argS = Normalizer.m21703(this.f17558.m22224(")"));
        Matcher mAB = f17552.matcher(argS);
        Matcher mB = f17553.matcher(argS);
        if ("odd".equals(argS)) {
            a = 2;
            b = 1;
        } else if ("even".equals(argS)) {
            a = 2;
            b = 0;
        } else if (mAB.matches()) {
            if (mAB.group(3) != null) {
                a = Integer.parseInt(mAB.group(1).replaceFirst("^\\+", ""));
            }
            if (mAB.group(4) != null) {
                b = Integer.parseInt(mAB.group(4).replaceFirst("^\\+", ""));
            }
        } else if (mB.matches()) {
            a = 0;
            b = Integer.parseInt(mB.group().replaceFirst("^\\+", ""));
        } else {
            throw new Selector.SelectorParseException("Could not parse nth-index '%s': unexpected format", argS);
        }
        if (ofType) {
            if (backwards) {
                this.f17556.add(new Evaluator.IsNthLastOfType(a, b));
            } else {
                this.f17556.add(new Evaluator.IsNthOfType(a, b));
            }
        } else if (backwards) {
            this.f17556.add(new Evaluator.IsNthLastChild(a, b));
        } else {
            this.f17556.add(new Evaluator.IsNthChild(a, b));
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private int m22425() {
        String indexS = this.f17558.m22224(")").trim();
        Validate.m21700(StringUtil.m21677(indexS), "Index must be numeric");
        return Integer.parseInt(indexS);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m22423() {
        this.f17558.m22231(":has");
        String subQuery = this.f17558.m22233('(', ')');
        Validate.m21698(subQuery, ":has(el) subselect must not be empty");
        this.f17556.add(new StructuralEvaluator.Has(m22434(subQuery)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22436(boolean own) {
        this.f17558.m22231(own ? ":containsOwn" : ":contains");
        String searchText = TokenQueue.m22219(this.f17558.m22233('(', ')'));
        Validate.m21698(searchText, ":contains(text) query must not be empty");
        if (own) {
            this.f17556.add(new Evaluator.ContainsOwnText(searchText));
        } else {
            this.f17556.add(new Evaluator.ContainsText(searchText));
        }
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m22424() {
        this.f17558.m22231(":containsData");
        String searchText = TokenQueue.m22219(this.f17558.m22233('(', ')'));
        Validate.m21698(searchText, ":containsData(text) query must not be empty");
        this.f17556.add(new Evaluator.ContainsData(searchText));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m22431(boolean own) {
        this.f17558.m22231(own ? ":matchesOwn" : ":matches");
        String regex = this.f17558.m22233('(', ')');
        Validate.m21698(regex, ":matches(regex) query must not be empty");
        if (own) {
            this.f17556.add(new Evaluator.MatchesOwn(Pattern.compile(regex)));
        } else {
            this.f17556.add(new Evaluator.Matches(Pattern.compile(regex)));
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private void m22438() {
        this.f17558.m22231(":not");
        String subQuery = this.f17558.m22233('(', ')');
        Validate.m21698(subQuery, ":not(selector) subselect must not be empty");
        this.f17556.add(new StructuralEvaluator.Not(m22434(subQuery)));
    }
}
