package org.jsoup.select;

import java.util.Iterator;
import org.jsoup.nodes.Element;

abstract class StructuralEvaluator extends Evaluator {

    /* renamed from: 龘  reason: contains not printable characters */
    Evaluator f17559;

    StructuralEvaluator() {
    }

    static class Root extends Evaluator {
        Root() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22451(Element root, Element element) {
            return root == element;
        }
    }

    static class Has extends StructuralEvaluator {
        public Has(Evaluator evaluator) {
            this.f17559 = evaluator;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22445(Element root, Element element) {
            Iterator it2 = element.m21852().iterator();
            while (it2.hasNext()) {
                Element e = (Element) it2.next();
                if (e != element && this.f17559.m22372(root, e)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return String.format(":has(%s)", new Object[]{this.f17559});
        }
    }

    static class Not extends StructuralEvaluator {
        public Not(Evaluator evaluator) {
            this.f17559 = evaluator;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22448(Element root, Element node) {
            return !this.f17559.m22372(root, node);
        }

        public String toString() {
            return String.format(":not%s", new Object[]{this.f17559});
        }
    }

    static class Parent extends StructuralEvaluator {
        public Parent(Evaluator evaluator) {
            this.f17559 = evaluator;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22449(Element root, Element element) {
            if (root == element) {
                return false;
            }
            for (Element parent = element.m21819(); !this.f17559.m22372(root, parent); parent = parent.m21819()) {
                if (parent == root) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return String.format(":parent%s", new Object[]{this.f17559});
        }
    }

    static class ImmediateParent extends StructuralEvaluator {
        public ImmediateParent(Evaluator evaluator) {
            this.f17559 = evaluator;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22446(Element root, Element element) {
            Element parent;
            if (root == element || (parent = element.m21819()) == null || !this.f17559.m22372(root, parent)) {
                return false;
            }
            return true;
        }

        public String toString() {
            return String.format(":ImmediateParent%s", new Object[]{this.f17559});
        }
    }

    static class PreviousSibling extends StructuralEvaluator {
        public PreviousSibling(Evaluator evaluator) {
            this.f17559 = evaluator;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22450(Element root, Element element) {
            if (root == element) {
                return false;
            }
            for (Element prev = element.m21850(); prev != null; prev = prev.m21850()) {
                if (this.f17559.m22372(root, prev)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return String.format(":prev*%s", new Object[]{this.f17559});
        }
    }

    static class ImmediatePreviousSibling extends StructuralEvaluator {
        public ImmediatePreviousSibling(Evaluator evaluator) {
            this.f17559 = evaluator;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22447(Element root, Element element) {
            Element prev;
            if (root == element || (prev = element.m21850()) == null || !this.f17559.m22372(root, prev)) {
                return false;
            }
            return true;
        }

        public String toString() {
            return String.format(":prev%s", new Object[]{this.f17559});
        }
    }
}
