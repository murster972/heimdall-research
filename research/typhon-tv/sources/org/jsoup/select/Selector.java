package org.jsoup.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;

public class Selector {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m22442(String query, Element root) {
        Validate.m21697(query);
        return m22444(QueryParser.m22434(query), root);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m22444(Evaluator evaluator, Element root) {
        Validate.m21695((Object) evaluator);
        Validate.m21695((Object) root);
        return Collector.m22359(evaluator, root);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m22441(String query, Iterable<Element> roots) {
        Validate.m21697(query);
        Validate.m21695((Object) roots);
        Evaluator evaluator = QueryParser.m22434(query);
        ArrayList<Element> elements = new ArrayList<>();
        IdentityHashMap<Element, Boolean> seenElements = new IdentityHashMap<>();
        for (Element root : roots) {
            Iterator it2 = m22444(evaluator, root).iterator();
            while (it2.hasNext()) {
                Element el = (Element) it2.next();
                if (!seenElements.containsKey(el)) {
                    elements.add(el);
                    seenElements.put(el, Boolean.TRUE);
                }
            }
        }
        return new Elements((List<Element>) elements);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Elements m22443(Collection<Element> elements, Collection<Element> outs) {
        Elements output = new Elements();
        for (Element el : elements) {
            boolean found = false;
            Iterator<Element> it2 = outs.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (el.equals(it2.next())) {
                        found = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!found) {
                output.add(el);
            }
        }
        return output;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Element m22440(String cssQuery, Element root) {
        Validate.m21697(cssQuery);
        return Collector.m22358(QueryParser.m22434(cssQuery), root);
    }

    public static class SelectorParseException extends IllegalStateException {
        public SelectorParseException(String msg, Object... params) {
            super(String.format(msg, params));
        }
    }
}
