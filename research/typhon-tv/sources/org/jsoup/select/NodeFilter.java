package org.jsoup.select;

import org.jsoup.nodes.Node;

public interface NodeFilter {

    public enum FilterResult {
        CONTINUE,
        SKIP_CHILDREN,
        SKIP_ENTIRELY,
        REMOVE,
        STOP
    }

    /* renamed from: 靐  reason: contains not printable characters */
    FilterResult m22412(Node node, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    FilterResult m22413(Node node, int i);
}
