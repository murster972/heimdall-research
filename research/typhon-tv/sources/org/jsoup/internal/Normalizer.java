package org.jsoup.internal;

import java.util.Locale;

public final class Normalizer {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m21704(String input) {
        return input != null ? input.toLowerCase(Locale.ENGLISH) : "";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static String m21703(String input) {
        return m21704(input).trim();
    }
}
