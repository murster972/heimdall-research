package org.jsoup.helper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;

public final class StringUtil {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ThreadLocal<StringBuilder> f17248 = new ThreadLocal<StringBuilder>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public StringBuilder initialValue() {
            return new StringBuilder(8192);
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    static final String[] f17249 = {"", StringUtils.SPACE, "  ", "   ", "    ", "     ", "      ", "       ", "        ", "         ", "          ", "           ", "            ", "             ", "              ", "               ", "                ", "                 ", "                  ", "                   ", "                    "};

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m21684(Collection strings, String sep) {
        return m21685(strings.iterator(), sep);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m21685(Iterator strings, String sep) {
        if (!strings.hasNext()) {
            return "";
        }
        String start = strings.next().toString();
        if (!strings.hasNext()) {
            return start;
        }
        StringBuilder sb = new StringBuilder(64).append(start);
        while (strings.hasNext()) {
            sb.append(sep);
            sb.append(strings.next());
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m21682(int width) {
        if (width < 0) {
            throw new IllegalArgumentException("width must be > 0");
        } else if (width < f17249.length) {
            return f17249[width];
        } else {
            char[] out = new char[width];
            for (int i = 0; i < width; i++) {
                out[i] = ' ';
            }
            return String.valueOf(out);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m21689(String string) {
        if (string == null || string.length() == 0) {
            return true;
        }
        int l = string.length();
        for (int i = 0; i < l; i++) {
            if (!m21676(string.codePointAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m21677(String string) {
        if (string == null || string.length() == 0) {
            return false;
        }
        int l = string.length();
        for (int i = 0; i < l; i++) {
            if (!Character.isDigit(string.codePointAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m21676(int c) {
        return c == 32 || c == 9 || c == 10 || c == 12 || c == 13;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m21681(int c) {
        return c == 32 || c == 9 || c == 10 || c == 12 || c == 13 || c == 160;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static boolean m21679(int c) {
        return Character.getType(c) == 16 && (c == 8203 || c == 8204 || c == 8205 || c == 173);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m21680(String string) {
        StringBuilder sb = m21686();
        m21688(sb, string, false);
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21688(StringBuilder accum, String string, boolean stripLeading) {
        boolean lastWasWhite = false;
        boolean reachedNonWhite = false;
        int len = string.length();
        int i = 0;
        while (i < len) {
            int c = string.codePointAt(i);
            if (m21681(c)) {
                if ((!stripLeading || reachedNonWhite) && !lastWasWhite) {
                    accum.append(' ');
                    lastWasWhite = true;
                }
            } else if (!m21679(c)) {
                accum.appendCodePoint(c);
                lastWasWhite = false;
                reachedNonWhite = true;
            }
            i += Character.charCount(c);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m21690(String needle, String... haystack) {
        for (String equals : haystack) {
            if (equals.equals(needle)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m21678(String needle, String[] haystack) {
        return Arrays.binarySearch(haystack, needle) >= 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static URL m21687(URL base, String relUrl) throws MalformedURLException {
        if (relUrl.startsWith("?")) {
            relUrl = base.getPath() + relUrl;
        }
        if (relUrl.indexOf(46) == 0 && base.getFile().indexOf(47) != 0) {
            base = new URL(base.getProtocol(), base.getHost(), base.getPort(), InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + base.getFile());
        }
        return new URL(base, relUrl);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m21683(String baseUrl, String relUrl) {
        try {
            try {
                return m21687(new URL(baseUrl), relUrl).toExternalForm();
            } catch (MalformedURLException e) {
                return "";
            }
        } catch (MalformedURLException e2) {
            return new URL(relUrl).toExternalForm();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static StringBuilder m21686() {
        StringBuilder sb = f17248.get();
        if (sb.length() > 8192) {
            StringBuilder sb2 = new StringBuilder(8192);
            f17248.set(sb2);
            return sb2;
        }
        sb.delete(0, sb.length());
        return sb;
    }
}
