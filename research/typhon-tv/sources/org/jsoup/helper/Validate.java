package org.jsoup.helper;

public final class Validate {
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21695(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Object must not be null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21696(Object obj, String msg) {
        if (obj == null) {
            throw new IllegalArgumentException(msg);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21699(boolean val) {
        if (!val) {
            throw new IllegalArgumentException("Must be true");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21700(boolean val, String msg) {
        if (!val) {
            throw new IllegalArgumentException(msg);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m21693(boolean val) {
        if (val) {
            throw new IllegalArgumentException("Must be false");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m21694(boolean val, String msg) {
        if (val) {
            throw new IllegalArgumentException(msg);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21701(Object[] objects) {
        m21702(objects, "Array must not contain any null objects");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21702(Object[] objects, String msg) {
        for (Object obj : objects) {
            if (obj == null) {
                throw new IllegalArgumentException(msg);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21697(String string) {
        if (string == null || string.length() == 0) {
            throw new IllegalArgumentException("String must not be empty");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21698(String string, String msg) {
        if (string == null || string.length() == 0) {
            throw new IllegalArgumentException(msg);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m21692(String msg) {
        throw new IllegalArgumentException(msg);
    }
}
