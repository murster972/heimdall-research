package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.nodes.Document;

public class DataNode extends LeafNode {
    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21755(String str) {
        return super.m21903(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21757() {
        return super.m21904();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21758(String str) {
        return super.m21905(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m21759() {
        return super.m21906();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m21760(String str) {
        return super.m21907(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21762(String str) {
        return super.m21908(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21763(String str, String str2) {
        return super.m21909(str, str2);
    }

    public DataNode(String data) {
        this.f17294 = data;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21761() {
        return "#data";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m21754() {
        return m21902();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21764(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        accum.append(m21754());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21756(Appendable accum, int depth, Document.OutputSettings out) {
    }

    public String toString() {
        return m21913();
    }
}
