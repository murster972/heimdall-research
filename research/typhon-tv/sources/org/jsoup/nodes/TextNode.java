package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;

public class TextNode extends LeafNode {
    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21970(String str) {
        return super.m21903(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21972() {
        return super.m21904();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21973(String str) {
        return super.m21905(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m21974() {
        return super.m21906();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m21975(String str) {
        return super.m21907(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21977(String str) {
        return super.m21908(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21978(String str, String str2) {
        return super.m21909(str, str2);
    }

    public TextNode(String text) {
        this.f17294 = text;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21976() {
        return "#text";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m21969() {
        return m21902();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m21968() {
        return StringUtil.m21689(m21902());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21979(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        boolean normaliseWhite;
        if (out.m21780() && ((m21934() == 0 && (this.f17295 instanceof Element) && ((Element) this.f17295).m21871().m22169() && !m21968()) || (out.m21777() && m21932().size() > 0 && !m21968()))) {
            m21950(accum, depth, out);
        }
        if (!out.m21780() || !(m21919() instanceof Element) || Element.m21803(m21919())) {
            normaliseWhite = false;
        } else {
            normaliseWhite = true;
        }
        Entities.m21879(accum, m21902(), out, false, normaliseWhite, false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21971(Appendable accum, int depth, Document.OutputSettings out) {
    }

    public String toString() {
        return m21913();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m21967(StringBuilder sb) {
        return sb.length() != 0 && sb.charAt(sb.length() + -1) == ' ';
    }
}
