package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.UncheckedIOException;
import org.jsoup.nodes.Document;

public class CDataNode extends TextNode {
    public CDataNode(String text) {
        super(text);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21741() {
        return "#cdata";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21742(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        accum.append("<![CDATA[").append(m21969());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21740(Appendable accum, int depth, Document.OutputSettings out) {
        try {
            accum.append("]]>");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
