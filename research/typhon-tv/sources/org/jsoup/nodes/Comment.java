package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.nodes.Document;

public class Comment extends LeafNode {
    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21744(String str) {
        return super.m21903(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21746() {
        return super.m21904();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21747(String str) {
        return super.m21905(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m21748() {
        return super.m21906();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m21749(String str) {
        return super.m21907(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21751(String str) {
        return super.m21908(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21752(String str, String str2) {
        return super.m21909(str, str2);
    }

    public Comment(String data) {
        this.f17294 = data;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21750() {
        return "#comment";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m21743() {
        return m21902();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21753(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        if (out.m21780()) {
            m21950(accum, depth, out);
        }
        accum.append("<!--").append(m21743()).append("-->");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21745(Appendable accum, int depth, Document.OutputSettings out) {
    }

    public String toString() {
        return m21913();
    }
}
