package org.jsoup.nodes;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.ParseSettings;
import org.jsoup.parser.Tag;

public class Document extends Element {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f17260;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f17261 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private QuirksMode f17262 = QuirksMode.noQuirks;

    /* renamed from: 麤  reason: contains not printable characters */
    private OutputSettings f17263 = new OutputSettings();

    public enum QuirksMode {
        noQuirks,
        quirks,
        limitedQuirks
    }

    public Document(String baseUri) {
        super(Tag.m22160("#root", ParseSettings.f17377), baseUri);
        this.f17260 = baseUri;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Element m21774() {
        return m21765(TtmlNode.TAG_BODY, this);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m21772() {
        Element titleEl = m21870(PubnativeAsset.TITLE).first();
        return titleEl != null ? StringUtil.m21680(titleEl.m21868()).trim() : "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Element m21765(String tag, Node node) {
        if (node.m21952().equals(tag)) {
            return (Element) node;
        }
        int size = node.m21948();
        for (int i = 0; i < size; i++) {
            Element found = m21765(tag, node.m21943(i));
            if (found != null) {
                return found;
            }
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m21766() {
        return super.m21822();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Element m21773(String text) {
        m21774().m21853(text);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21775() {
        return "#document";
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Document m21771() {
        Document clone = (Document) super.m21845();
        clone.f17263 = this.f17263.clone();
        return clone;
    }

    public static class OutputSettings implements Cloneable {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f17264 = false;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f17265 = 1;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Syntax f17266 = Syntax.html;

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f17267 = true;

        /* renamed from: 靐  reason: contains not printable characters */
        private Entities.EscapeMode f17268 = Entities.EscapeMode.base;

        /* renamed from: 麤  reason: contains not printable characters */
        private ThreadLocal<CharsetEncoder> f17269 = new ThreadLocal<>();

        /* renamed from: 齉  reason: contains not printable characters */
        private Charset f17270;

        /* renamed from: 龘  reason: contains not printable characters */
        Entities.CoreCharset f17271;

        public enum Syntax {
            html,
            xml
        }

        public OutputSettings() {
            m21785(Charset.forName(InternalZipTyphoonApp.CHARSET_UTF8));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Entities.EscapeMode m21787() {
            return this.f17268;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputSettings m21785(Charset charset) {
            this.f17270 = charset;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputSettings m21784(String charset) {
            m21785(Charset.forName(charset));
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public CharsetEncoder m21781() {
            CharsetEncoder encoder = this.f17270.newEncoder();
            this.f17269.set(encoder);
            this.f17271 = Entities.CoreCharset.m21884(encoder.charset().name());
            return encoder;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public CharsetEncoder m21783() {
            CharsetEncoder encoder = this.f17269.get();
            return encoder != null ? encoder : m21781();
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public Syntax m21782() {
            return this.f17266;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputSettings m21786(Syntax syntax) {
            this.f17266 = syntax;
            return this;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public boolean m21780() {
            return this.f17267;
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public boolean m21777() {
            return this.f17264;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public int m21778() {
            return this.f17265;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        public OutputSettings clone() {
            try {
                OutputSettings clone = (OutputSettings) super.clone();
                clone.m21784(this.f17270.name());
                clone.f17268 = Entities.EscapeMode.valueOf(this.f17268.name());
                return clone;
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public OutputSettings m21768() {
        return this.f17263;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public QuirksMode m21769() {
        return this.f17262;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Document m21776(QuirksMode quirksMode) {
        this.f17262 = quirksMode;
        return this;
    }
}
