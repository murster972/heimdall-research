package org.jsoup.nodes;

import java.io.IOException;
import java.util.Iterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class XmlDeclaration extends LeafNode {

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f17301;

    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21981(String str) {
        return super.m21903(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21983() {
        return super.m21904();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21984(String str) {
        return super.m21905(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m21985() {
        return super.m21906();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m21986(String str) {
        return super.m21907(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21988(String str) {
        return super.m21908(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21989(String str, String str2) {
        return super.m21909(str, str2);
    }

    public XmlDeclaration(String name, boolean isProcessingInstruction) {
        Validate.m21695((Object) name);
        this.f17294 = name;
        this.f17301 = isProcessingInstruction;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21987() {
        return "#declaration";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21980(Appendable accum, Document.OutputSettings out) throws IOException {
        Iterator<Attribute> it2 = m21900().iterator();
        while (it2.hasNext()) {
            Attribute attribute = it2.next();
            if (!attribute.getKey().equals(m21987())) {
                accum.append(' ');
                attribute.m21713(accum, out);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21990(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        accum.append("<").append(this.f17301 ? "!" : "?").append(m21902());
        m21980(accum, out);
        accum.append(this.f17301 ? "!" : "?").append(">");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21982(Appendable accum, int depth, Document.OutputSettings out) {
    }

    public String toString() {
        return m21913();
    }
}
