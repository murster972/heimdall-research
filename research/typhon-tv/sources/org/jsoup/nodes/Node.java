package org.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.SerializationException;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public abstract class Node implements Cloneable {

    /* renamed from: 靐  reason: contains not printable characters */
    Node f17295;

    /* renamed from: 齉  reason: contains not printable characters */
    int f17296;

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m21915(String str);

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract boolean m21918();

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract Attributes m21920();

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract List<Node> m21922();

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m21945(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract String m21946();

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract int m21948();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m21952();

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m21958(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException;

    protected Node() {
    }

    /* renamed from: ــ  reason: contains not printable characters */
    public boolean m21936() {
        return this.f17295 != null;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m21947(String attributeKey) {
        Validate.m21695((Object) attributeKey);
        if (!m21918()) {
            return "";
        }
        String val = m21920().m21729(attributeKey);
        if (val.length() > 0) {
            return val;
        }
        if (attributeKey.startsWith("abs:")) {
            return m21953(attributeKey.substring("abs:".length()));
        }
        return "";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Node m21954(String attributeKey, String attributeValue) {
        m21920().m21728(attributeKey, attributeValue);
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m21951(String attributeKey) {
        Validate.m21695((Object) attributeKey);
        if (attributeKey.startsWith("abs:")) {
            String key = attributeKey.substring("abs:".length());
            if (m21920().m21724(key) && !m21953(key).equals("")) {
                return true;
            }
        }
        return m21920().m21724(attributeKey);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Node m21944(String attributeKey) {
        Validate.m21695((Object) attributeKey);
        m21920().m21722(attributeKey);
        return this;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m21939(final String baseUri) {
        Validate.m21695((Object) baseUri);
        m21955((NodeVisitor) new NodeVisitor() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m21962(Node node, int depth) {
                node.m21915(baseUri);
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m21961(Node node, int depth) {
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21953(String attributeKey) {
        Validate.m21697(attributeKey);
        if (!m21951(attributeKey)) {
            return "";
        }
        return StringUtil.m21683(m21946(), m21947(attributeKey));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Node m21943(int index) {
        return m21922().get(index);
    }

    /* renamed from: ˆˆ  reason: contains not printable characters */
    public List<Node> m21921() {
        return Collections.unmodifiableList(m21922());
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˉˉ  reason: contains not printable characters */
    public Node[] m21924() {
        return (Node[]) m21922().toArray(new Node[m21948()]);
    }

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public Node m21919() {
        return this.f17295;
    }

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public final Node m21923() {
        return this.f17295;
    }

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public Node m21926() {
        Node node = this;
        while (node.f17295 != null) {
            node = node.f17295;
        }
        return node;
    }

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public Document m21925() {
        Node root = m21926();
        if (root instanceof Document) {
            return (Document) root;
        }
        return null;
    }

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public void m21930() {
        Validate.m21695((Object) this.f17295);
        this.f17295.m21916(this);
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public Node m21935(String html) {
        m21912(this.f17296, html);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Node m21914(Node node) {
        Validate.m21695((Object) node);
        Validate.m21695((Object) this.f17295);
        this.f17295.m21956(this.f17296, node);
        return this;
    }

    /* renamed from: י  reason: contains not printable characters */
    public Node m21933(String html) {
        m21912(this.f17296 + 1, html);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21912(int index, String html) {
        Validate.m21695((Object) html);
        Validate.m21695((Object) this.f17295);
        List<Node> nodes = Parser.m22155(html, m21919() instanceof Element ? (Element) m21919() : null, m21946());
        this.f17295.m21956(index, (Node[]) nodes.toArray(new Node[nodes.size()]));
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    /* renamed from: ˏ  reason: contains not printable characters */
    public Node m21928(String html) {
        Element context;
        Validate.m21697(html);
        if (m21919() instanceof Element) {
            context = (Element) m21919();
        } else {
            context = null;
        }
        List<Node> wrapChildren = Parser.m22155(html, context, m21946());
        Node wrapNode = wrapChildren.get(0);
        if (wrapNode == null || !(wrapNode instanceof Element)) {
            return null;
        }
        Element wrap = (Element) wrapNode;
        Element deepest = m21910(wrap);
        this.f17295.m21959(this, (Node) wrap);
        deepest.m21960(this);
        if (wrapChildren.size() <= 0) {
            return this;
        }
        for (int i = 0; i < wrapChildren.size(); i++) {
            Node remainder = wrapChildren.get(i);
            remainder.f17295.m21916(remainder);
            wrap.m21864(remainder);
        }
        return this;
    }

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public Node m21927() {
        Validate.m21695((Object) this.f17295);
        List<Node> childNodes = m21922();
        Node firstChild = childNodes.size() > 0 ? childNodes.get(0) : null;
        this.f17295.m21956(this.f17296, m21924());
        m21930();
        return firstChild;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Element m21910(Element el) {
        List<Element> children = el.m21828();
        if (children.size() > 0) {
            return m21910(children.get(0));
        }
        return el;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m21929() {
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m21917(Node in) {
        Validate.m21695((Object) in);
        Validate.m21695((Object) this.f17295);
        this.f17295.m21959(this, in);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m21931(Node parentNode) {
        Validate.m21695((Object) parentNode);
        if (this.f17295 != null) {
            this.f17295.m21916(this);
        }
        this.f17295 = parentNode;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21959(Node out, Node in) {
        Validate.m21699(out.f17295 == this);
        Validate.m21695((Object) in);
        if (in.f17295 != null) {
            in.f17295.m21916(in);
        }
        int index = out.f17296;
        m21922().set(index, in);
        in.f17295 = this;
        in.m21949(index);
        out.f17295 = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m21916(Node out) {
        Validate.m21699(out.f17295 == this);
        int index = out.f17296;
        m21922().remove(index);
        m21911(index);
        out.f17295 = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21960(Node... children) {
        List<Node> nodes = m21922();
        for (Node child : children) {
            m21937(child);
            nodes.add(child);
            child.m21949(nodes.size() - 1);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21956(int index, Node... children) {
        Validate.m21701((Object[]) children);
        List<Node> nodes = m21922();
        for (Node child : children) {
            m21937(child);
        }
        nodes.addAll(index, Arrays.asList(children));
        m21911(index);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m21937(Node child) {
        child.m21931(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21911(int start) {
        List<Node> childNodes = m21922();
        for (int i = start; i < childNodes.size(); i++) {
            childNodes.get(i).m21949(i);
        }
    }

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public List<Node> m21932() {
        if (this.f17295 == null) {
            return Collections.emptyList();
        }
        List<Node> nodes = this.f17295.m21922();
        List<Node> siblings = new ArrayList<>(nodes.size() - 1);
        for (Node node : nodes) {
            if (node != this) {
                siblings.add(node);
            }
        }
        return siblings;
    }

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public Node m21941() {
        if (this.f17295 == null) {
            return null;
        }
        List<Node> siblings = this.f17295.m21922();
        int index = this.f17296 + 1;
        if (siblings.size() > index) {
            return siblings.get(index);
        }
        return null;
    }

    /* renamed from: יי  reason: contains not printable characters */
    public int m21934() {
        return this.f17296;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m21949(int siblingIndex) {
        this.f17296 = siblingIndex;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Node m21955(NodeVisitor nodeVisitor) {
        Validate.m21695((Object) nodeVisitor);
        NodeTraversor.m22416(nodeVisitor, this);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m21913() {
        StringBuilder accum = new StringBuilder(128);
        m21957((Appendable) accum);
        return accum.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21957(Appendable accum) {
        NodeTraversor.m22416((NodeVisitor) new OuterHtmlVisitor(accum, m21940()), this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    public Document.OutputSettings m21940() {
        Document owner = m21925();
        return owner != null ? owner.m21768() : new Document("").m21768();
    }

    public String toString() {
        return m21913();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m21950(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        accum.append(10).append(StringUtil.m21682(out.m21778() * depth));
    }

    public boolean equals(Object o) {
        return this == o;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Node clone() {
        Node thisClone = m21942((Node) null);
        LinkedList<Node> nodesToProcess = new LinkedList<>();
        nodesToProcess.add(thisClone);
        while (!nodesToProcess.isEmpty()) {
            Node currParent = nodesToProcess.remove();
            int size = currParent.m21948();
            for (int i = 0; i < size; i++) {
                List<Node> childNodes = currParent.m21922();
                Node childClone = childNodes.get(i).m21942(currParent);
                childNodes.set(i, childClone);
                nodesToProcess.add(childClone);
            }
        }
        return thisClone;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public Node m21942(Node parent) {
        try {
            Node clone = (Node) super.clone();
            clone.f17295 = parent;
            clone.f17296 = parent == null ? 0 : this.f17296;
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    private static class OuterHtmlVisitor implements NodeVisitor {

        /* renamed from: 靐  reason: contains not printable characters */
        private Document.OutputSettings f17299;

        /* renamed from: 龘  reason: contains not printable characters */
        private Appendable f17300;

        OuterHtmlVisitor(Appendable accum, Document.OutputSettings out) {
            this.f17300 = accum;
            this.f17299 = out;
            out.m21781();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21964(Node node, int depth) {
            try {
                node.m21958(this.f17300, depth, this.f17299);
            } catch (IOException exception) {
                throw new SerializationException((Throwable) exception);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m21963(Node node, int depth) {
            if (!node.m21952().equals("#text")) {
                try {
                    node.m21945(this.f17300, depth, this.f17299);
                } catch (IOException exception) {
                    throw new SerializationException((Throwable) exception);
                }
            }
        }
    }
}
