package org.jsoup.nodes;

import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

public class FormElement extends Element {

    /* renamed from: 麤  reason: contains not printable characters */
    private final Elements f17292 = new Elements();

    public FormElement(Tag tag, String baseUri, Attributes attributes) {
        super(tag, baseUri, attributes);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public FormElement m21896(Element element) {
        this.f17292.add(element);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m21895(Node out) {
        super.m21916(out);
        this.f17292.remove(out);
    }
}
