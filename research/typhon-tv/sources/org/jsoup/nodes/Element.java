package org.jsoup.nodes;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.ChangeNotifyingArrayList;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Document;
import org.jsoup.parser.ParseSettings;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.jsoup.select.Collector;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;
import org.jsoup.select.Selector;

public class Element extends Node {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Pattern f17274 = Pattern.compile("\\s+");

    /* renamed from: 麤  reason: contains not printable characters */
    private static final List<Node> f17275 = Collections.emptyList();
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public Tag f17276;

    /* renamed from: ʼ  reason: contains not printable characters */
    private WeakReference<List<Element>> f17277;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Attributes f17278;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f17279;

    /* renamed from: 龘  reason: contains not printable characters */
    List<Node> f17280;

    public Element(Tag tag, String baseUri, Attributes attributes) {
        Validate.m21695((Object) tag);
        Validate.m21695((Object) baseUri);
        this.f17280 = f17275;
        this.f17279 = baseUri;
        this.f17278 = attributes;
        this.f17276 = tag;
    }

    public Element(Tag tag, String baseUri) {
        this(tag, baseUri, (Attributes) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public List<Node> m21825() {
        if (this.f17280 == f17275) {
            this.f17280 = new NodeList(this, 4);
        }
        return this.f17280;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m21818() {
        return this.f17278 != null;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Attributes m21820() {
        if (!m21818()) {
            this.f17278 = new Attributes();
        }
        return this.f17278;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m21858() {
        return this.f17279;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m21811(String baseUri) {
        this.f17279 = baseUri;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m21860() {
        return this.f17280.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21861() {
        return this.f17276.m22170();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public String m21869() {
        return this.f17276.m22170();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Element m21813(String tagName) {
        Validate.m21698(tagName, "Tag name must not be empty.");
        this.f17276 = Tag.m22160(tagName, ParseSettings.f17376);
        return this;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Tag m21871() {
        return this.f17276;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m21830() {
        return this.f17276.m22167();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public String m21831() {
        return m21820().m21729("id");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Element m21865(String attributeKey, String attributeValue) {
        super.m21954(attributeKey, attributeValue);
        return this;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final Element m21819() {
        return (Element) this.f17295;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Elements m21824() {
        Elements parents = new Elements();
        m21809(this, parents);
        return parents;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21809(Element el, Elements parents) {
        Element parent = el.m21819();
        if (parent != null && !parent.m21869().equals("#root")) {
            parents.add(parent);
            m21809(parent, parents);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Element m21862(int index) {
        return m21800().get(index);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Elements m21828() {
        return new Elements(m21800());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private List<Element> m21800() {
        List<Element> children;
        if (this.f17277 == null || (children = (List) this.f17277.get()) == null) {
            int size = this.f17280.size();
            children = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                Node node = this.f17280.get(i);
                if (node instanceof Element) {
                    children.add((Element) node);
                }
            }
            this.f17277 = new WeakReference<>(children);
        }
        return children;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m21836() {
        super.m21929();
        this.f17277 = null;
    }

    /* renamed from: י  reason: contains not printable characters */
    public List<TextNode> m21838() {
        List<TextNode> textNodes = new ArrayList<>();
        for (Node node : this.f17280) {
            if (node instanceof TextNode) {
                textNodes.add((TextNode) node);
            }
        }
        return Collections.unmodifiableList(textNodes);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Elements m21815(String cssQuery) {
        return Selector.m22442(cssQuery, this);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public Element m21837(String cssQuery) {
        return Selector.m22440(cssQuery, this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m21867(Evaluator evaluator) {
        return evaluator.m22372((Element) m21926(), this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Element m21864(Node child) {
        Validate.m21695((Object) child);
        m21937(child);
        m21825();
        this.f17280.add(child);
        child.m21949(this.f17280.size() - 1);
        return this;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Element m21843(String html) {
        Validate.m21695((Object) html);
        List<Node> nodes = Parser.m22155(html, this, m21858());
        m21960((Node[]) nodes.toArray(new Node[nodes.size()]));
        return this;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Element m21844(String html) {
        Validate.m21695((Object) html);
        List<Node> nodes = Parser.m22155(html, this, m21858());
        m21956(0, (Node[]) nodes.toArray(new Node[nodes.size()]));
        return this;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Element m21841(String html) {
        return (Element) super.m21935(html);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Element m21810(Node node) {
        return (Element) super.m21914(node);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Element m21839(String html) {
        return (Element) super.m21933(html);
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public Element m21840() {
        this.f17280.clear();
        return this;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Element m21835(String html) {
        return (Element) super.m21928(html);
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public Elements m21847() {
        if (this.f17295 == null) {
            return new Elements(0);
        }
        List<Element> elements = m21819().m21800();
        Elements siblings = new Elements(elements.size() - 1);
        for (Element el : elements) {
            if (el != this) {
                siblings.add(el);
            }
        }
        return siblings;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public Element m21849() {
        if (this.f17295 == null) {
            return null;
        }
        List<Element> siblings = m21819().m21800();
        Integer index = Integer.valueOf(m21804(this, siblings));
        Validate.m21695((Object) index);
        if (siblings.size() > index.intValue() + 1) {
            return siblings.get(index.intValue() + 1);
        }
        return null;
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public Element m21850() {
        if (this.f17295 == null) {
            return null;
        }
        List<Element> siblings = m21819().m21800();
        Integer index = Integer.valueOf(m21804(this, siblings));
        Validate.m21695((Object) index);
        if (index.intValue() > 0) {
            return siblings.get(index.intValue() - 1);
        }
        return null;
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public int m21851() {
        if (m21819() == null) {
            return 0;
        }
        return m21804(this, m21819().m21800());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <E extends Element> int m21804(Element search, List<E> elements) {
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i) == search) {
                return i;
            }
        }
        return 0;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Elements m21870(String tagName) {
        Validate.m21697(tagName);
        return Collector.m22359(new Evaluator.Tag(Normalizer.m21703(tagName)), this);
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public Elements m21852() {
        return Collector.m22359(new Evaluator.AllElements(), this);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public String m21868() {
        final StringBuilder accum = new StringBuilder();
        NodeTraversor.m22416((NodeVisitor) new NodeVisitor() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m21875(Node node, int depth) {
                if (node instanceof TextNode) {
                    Element.m21802(accum, (TextNode) node);
                } else if (node instanceof Element) {
                    Element element = (Element) node;
                    if (accum.length() <= 0) {
                        return;
                    }
                    if ((element.m21830() || element.f17276.m22170().equals(TtmlNode.TAG_BR)) && !TextNode.m21967(accum)) {
                        accum.append(' ');
                    }
                }
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m21874(Node node, int depth) {
                if ((node instanceof Element) && ((Element) node).m21830() && (node.m21941() instanceof TextNode) && !TextNode.m21967(accum)) {
                    accum.append(' ');
                }
            }
        }, (Node) this);
        return accum.toString().trim();
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public String m21873() {
        StringBuilder sb = new StringBuilder();
        m21806(sb);
        return sb.toString().trim();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21806(StringBuilder accum) {
        for (Node child : this.f17280) {
            if (child instanceof TextNode) {
                m21802(accum, (TextNode) child);
            } else if (child instanceof Element) {
                m21808((Element) child, accum);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m21802(StringBuilder accum, TextNode textNode) {
        String text = textNode.m21969();
        if (m21803(textNode.f17295) || (textNode instanceof CDataNode)) {
            accum.append(text);
        } else {
            StringUtil.m21688(accum, text, TextNode.m21967(accum));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21808(Element element, StringBuilder accum) {
        if (element.f17276.m22170().equals(TtmlNode.TAG_BR) && !TextNode.m21967(accum)) {
            accum.append(StringUtils.SPACE);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static boolean m21803(Node node) {
        if (node != null && (node instanceof Element)) {
            Element el = (Element) node;
            int i = 0;
            while (!el.f17276.m22163()) {
                el = el.m21819();
                i++;
                if (i < 6) {
                    if (el == null) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Element m21853(String text) {
        Validate.m21695((Object) text);
        m21840();
        m21864((Node) new TextNode(text));
        return this;
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public boolean m21846() {
        for (Node child : this.f17280) {
            if (child instanceof TextNode) {
                if (!((TextNode) child).m21968()) {
                    return true;
                }
            } else if ((child instanceof Element) && ((Element) child).m21846()) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public String m21848() {
        StringBuilder sb = new StringBuilder();
        for (Node childNode : this.f17280) {
            if (childNode instanceof DataNode) {
                sb.append(((DataNode) childNode).m21754());
            } else if (childNode instanceof Comment) {
                sb.append(((Comment) childNode).m21743());
            } else if (childNode instanceof Element) {
                sb.append(((Element) childNode).m21848());
            } else if (childNode instanceof CDataNode) {
                sb.append(((CDataNode) childNode).m21969());
            }
        }
        return sb.toString();
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public String m21812() {
        return m21947("class").trim();
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public Set<String> m21816() {
        Set<String> classNames = new LinkedHashSet<>(Arrays.asList(f17274.split(m21812())));
        classNames.remove("");
        return classNames;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Element m21863(Set<String> classNames) {
        Validate.m21695((Object) classNames);
        if (classNames.isEmpty()) {
            m21820().m21726("class");
        } else {
            m21820().m21735("class", StringUtil.m21684((Collection) classNames, StringUtils.SPACE));
        }
        return this;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m21872(String className) {
        String classAttr = m21820().m21729("class");
        int len = classAttr.length();
        int wantLen = className.length();
        if (len == 0 || len < wantLen) {
            return false;
        }
        if (len == wantLen) {
            return className.equalsIgnoreCase(classAttr);
        }
        boolean inClass = false;
        int start = 0;
        for (int i = 0; i < len; i++) {
            if (Character.isWhitespace(classAttr.charAt(i))) {
                if (!inClass) {
                    continue;
                } else if (i - start == wantLen && classAttr.regionMatches(true, start, className, 0, wantLen)) {
                    return true;
                } else {
                    inClass = false;
                }
            } else if (!inClass) {
                inClass = true;
                start = i;
            }
        }
        if (!inClass || len - start != wantLen) {
            return false;
        }
        return classAttr.regionMatches(true, start, className, 0, wantLen);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public Element m21829(String className) {
        Validate.m21695((Object) className);
        Set<String> classes = m21816();
        classes.add(className);
        m21863(classes);
        return this;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public Element m21832(String className) {
        Validate.m21695((Object) className);
        Set<String> classes = m21816();
        classes.remove(className);
        m21863(classes);
        return this;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public Element m21834(String className) {
        Validate.m21695((Object) className);
        Set<String> classes = m21816();
        if (classes.contains(className)) {
            classes.remove(className);
        } else {
            classes.add(className);
        }
        m21863(classes);
        return this;
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public String m21814() {
        if (m21869().equals("textarea")) {
            return m21868();
        }
        return m21947("value");
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Element m21823(String value) {
        if (m21869().equals("textarea")) {
            m21853(value);
        } else {
            m21865("value", value);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21866(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        if (out.m21780() && (this.f17276.m22169() || ((m21819() != null && m21819().m21871().m22169()) || out.m21777()))) {
            if (!(accum instanceof StringBuilder)) {
                m21950(accum, depth, out);
            } else if (((StringBuilder) accum).length() > 0) {
                m21950(accum, depth, out);
            }
        }
        accum.append('<').append(m21869());
        if (this.f17278 != null) {
            this.f17278.m21737(accum, out);
        }
        if (!this.f17280.isEmpty() || !this.f17276.m22166()) {
            accum.append('>');
        } else if (out.m21782() != Document.OutputSettings.Syntax.html || !this.f17276.m22168()) {
            accum.append(" />");
        } else {
            accum.append('>');
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21857(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        if (!this.f17280.isEmpty() || !this.f17276.m22166()) {
            if (out.m21780() && !this.f17280.isEmpty() && (this.f17276.m22169() || (out.m21777() && (this.f17280.size() > 1 || (this.f17280.size() == 1 && !(this.f17280.get(0) instanceof TextNode)))))) {
                m21950(accum, depth, out);
            }
            accum.append("</").append(m21869()).append('>');
        }
    }

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public String m21822() {
        StringBuilder accum = StringUtil.m21686();
        m21801(accum);
        return m21940().m21780() ? accum.toString().trim() : accum.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m21801(StringBuilder accum) {
        for (Node node : this.f17280) {
            node.m21957((Appendable) accum);
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Element m21827(String html) {
        m21840();
        m21843(html);
        return this;
    }

    public String toString() {
        return m21913();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Element m21845() {
        return (Element) super.clone();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public Element m21854(Node parent) {
        Element clone = (Element) super.m21942(parent);
        clone.f17278 = this.f17278 != null ? this.f17278.clone() : null;
        clone.f17279 = this.f17279;
        clone.f17280 = new NodeList(clone, this.f17280.size());
        clone.f17280.addAll(this.f17280);
        return clone;
    }

    private static final class NodeList extends ChangeNotifyingArrayList<Node> {
        private final Element owner;

        NodeList(Element owner2, int initialCapacity) {
            super(initialCapacity);
            this.owner = owner2;
        }

        public void onContentsChanged() {
            this.owner.m21836();
        }
    }
}
