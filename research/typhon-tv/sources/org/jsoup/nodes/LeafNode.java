package org.jsoup.nodes;

import java.util.Collections;
import java.util.List;
import org.jsoup.helper.Validate;

abstract class LeafNode extends Node {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final List<Node> f17293 = Collections.emptyList();

    /* renamed from: 龘  reason: contains not printable characters */
    Object f17294;

    LeafNode() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public final boolean m21899() {
        return this.f17294 instanceof Attributes;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final Attributes m21900() {
        m21897();
        return (Attributes) this.f17294;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m21897() {
        if (!m21899()) {
            Object coreValue = this.f17294;
            Attributes attributes = new Attributes();
            this.f17294 = attributes;
            if (coreValue != null) {
                attributes.m21735(m21952(), (String) coreValue);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public String m21902() {
        return m21905(m21952());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m21905(String key) {
        Validate.m21695((Object) key);
        if (!m21899()) {
            return key.equals(m21952()) ? (String) this.f17294 : "";
        }
        return super.m21947(key);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Node m21909(String key, String value) {
        if (m21899() || !key.equals(m21952())) {
            m21897();
            super.m21954(key, value);
        } else {
            this.f17294 = value;
        }
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m21907(String key) {
        m21897();
        return super.m21951(key);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Node m21903(String key) {
        m21897();
        return super.m21944(key);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21908(String key) {
        m21897();
        return super.m21953(key);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m21904() {
        return m21936() ? m21919().m21946() : "";
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m21898(String baseUri) {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m21906() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public List<Node> m21901() {
        return f17293;
    }
}
