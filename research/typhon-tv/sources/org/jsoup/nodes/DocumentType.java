package org.jsoup.nodes;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class DocumentType extends LeafNode {
    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21790(String str) {
        return super.m21903(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21792() {
        return super.m21904();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21793(String str) {
        return super.m21905(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m21794() {
        return super.m21906();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m21795(String str) {
        return super.m21907(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m21797(String str) {
        return super.m21908(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m21798(String str, String str2) {
        return super.m21909(str, str2);
    }

    public DocumentType(String name, String publicId, String systemId) {
        Validate.m21695((Object) name);
        Validate.m21695((Object) publicId);
        Validate.m21695((Object) systemId);
        m21798("name", name);
        m21798("publicId", publicId);
        if (m21788("publicId")) {
            m21798("pubSysKey", "PUBLIC");
        }
        m21798("systemId", systemId);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m21789(String value) {
        if (value != null) {
            m21798("pubSysKey", value);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21796() {
        return "#doctype";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21799(Appendable accum, int depth, Document.OutputSettings out) throws IOException {
        if (out.m21782() != Document.OutputSettings.Syntax.html || m21788("publicId") || m21788("systemId")) {
            accum.append("<!DOCTYPE");
        } else {
            accum.append("<!doctype");
        }
        if (m21788("name")) {
            accum.append(StringUtils.SPACE).append(m21793("name"));
        }
        if (m21788("pubSysKey")) {
            accum.append(StringUtils.SPACE).append(m21793("pubSysKey"));
        }
        if (m21788("publicId")) {
            accum.append(" \"").append(m21793("publicId")).append('\"');
        }
        if (m21788("systemId")) {
            accum.append(" \"").append(m21793("systemId")).append('\"');
        }
        accum.append('>');
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21791(Appendable accum, int depth, Document.OutputSettings out) {
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m21788(String attribute) {
        return !StringUtil.m21689(m21793(attribute));
    }
}
