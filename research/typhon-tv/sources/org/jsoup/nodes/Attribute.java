package org.jsoup.nodes;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import org.jsoup.SerializationException;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class Attribute implements Cloneable, Map.Entry<String, String> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f17250 = {"allowfullscreen", "async", "autofocus", "checked", "compact", "declare", "default", "defer", "disabled", "formnovalidate", "hidden", "inert", "ismap", "itemscope", "multiple", "muted", "nohref", "noresize", "noshade", "novalidate", "nowrap", "open", "readonly", "required", "reversed", "seamless", "selected", "sortable", "truespeed", "typemustmatch"};

    /* renamed from: 麤  reason: contains not printable characters */
    private String f17251;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f17252;

    /* renamed from: 龘  reason: contains not printable characters */
    Attributes f17253;

    public Attribute(String key, String value) {
        this(key, value, (Attributes) null);
    }

    public Attribute(String key, String val, Attributes parent) {
        Validate.m21695((Object) key);
        this.f17252 = key.trim();
        Validate.m21697(key);
        this.f17251 = val;
        this.f17253 = parent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String getKey() {
        return this.f17252;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String getValue() {
        return this.f17251;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String setValue(String val) {
        int i;
        String oldVal = this.f17253.m21732(this.f17252);
        if (!(this.f17253 == null || (i = this.f17253.m21734(this.f17252)) == -1)) {
            this.f17253.f17255[i] = val;
        }
        this.f17251 = val;
        return oldVal;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m21710() {
        StringBuilder accum = new StringBuilder();
        try {
            m21713(accum, new Document("").m21768());
            return accum.toString();
        } catch (IOException exception) {
            throw new SerializationException((Throwable) exception);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static void m21706(String key, String val, Appendable accum, Document.OutputSettings out) throws IOException {
        accum.append(key);
        if (!m21707(key, val, out)) {
            accum.append("=\"");
            Entities.m21879(accum, Attributes.m21716(val), out, true, false, false);
            accum.append('\"');
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21713(Appendable accum, Document.OutputSettings out) throws IOException {
        m21706(this.f17252, this.f17251, accum, out);
    }

    public String toString() {
        return m21710();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static boolean m21707(String key, String val, Document.OutputSettings out) {
        return out.m21782() == Document.OutputSettings.Syntax.html && (val == null || (("".equals(val) || val.equalsIgnoreCase(key)) && m21705(key)));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    protected static boolean m21705(String key) {
        return Arrays.binarySearch(f17250, key) >= 0;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Attribute attribute = (Attribute) o;
        if (this.f17252 == null ? attribute.f17252 != null : !this.f17252.equals(attribute.f17252)) {
            return false;
        }
        if (this.f17251 != null) {
            return this.f17251.equals(attribute.f17251);
        }
        if (attribute.f17251 != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.f17252 != null) {
            result = this.f17252.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.f17251 != null) {
            i = this.f17251.hashCode();
        }
        return i2 + i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Attribute clone() {
        try {
            return (Attribute) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
