package org.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jsoup.SerializationException;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Document;

public class Attributes implements Cloneable, Iterable<Attribute> {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String[] f17254 = new String[0];

    /* renamed from: 靐  reason: contains not printable characters */
    String[] f17255 = f17254;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public int f17256 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    String[] f17257 = f17254;

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21719(int minNewSize) {
        int newSize = 4;
        Validate.m21699(minNewSize >= this.f17256);
        int curSize = this.f17257.length;
        if (curSize < minNewSize) {
            if (curSize >= 4) {
                newSize = this.f17256 * 2;
            }
            if (minNewSize > newSize) {
                newSize = minNewSize;
            }
            this.f17257 = m21721(this.f17257, newSize);
            this.f17255 = m21721(this.f17255, newSize);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] m21721(String[] orig, int size) {
        String[] copy = new String[size];
        System.arraycopy(orig, 0, copy, 0, Math.min(orig.length, size));
        return copy;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21734(String key) {
        Validate.m21695((Object) key);
        for (int i = 0; i < this.f17256; i++) {
            if (key.equals(this.f17257[i])) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m21714(String key) {
        Validate.m21695((Object) key);
        for (int i = 0; i < this.f17256; i++) {
            if (key.equalsIgnoreCase(this.f17257[i])) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static String m21716(String val) {
        return val == null ? "" : val;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m21732(String key) {
        int i = m21734(key);
        return i == -1 ? "" : m21716(this.f17255[i]);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m21729(String key) {
        int i = m21714(key);
        return i == -1 ? "" : m21716(this.f17255[i]);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m21718(String key, String value) {
        m21719(this.f17256 + 1);
        this.f17257[this.f17256] = key;
        this.f17255[this.f17256] = value;
        this.f17256++;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Attributes m21735(String key, String value) {
        int i = m21734(key);
        if (i != -1) {
            this.f17255[i] = value;
        } else {
            m21718(key, value);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21728(String key, String value) {
        int i = m21714(key);
        if (i != -1) {
            this.f17255[i] = value;
            if (!this.f17257[i].equals(key)) {
                this.f17257[i] = key;
                return;
            }
            return;
        }
        m21718(key, value);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Attributes m21736(Attribute attribute) {
        Validate.m21695((Object) attribute);
        m21735(attribute.getKey(), attribute.getValue());
        attribute.f17253 = this;
        return this;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m21717(int index) {
        Validate.m21693(index >= this.f17256);
        int shifted = (this.f17256 - index) - 1;
        if (shifted > 0) {
            System.arraycopy(this.f17257, index + 1, this.f17257, index, shifted);
            System.arraycopy(this.f17255, index + 1, this.f17255, index, shifted);
        }
        this.f17256--;
        this.f17257[this.f17256] = null;
        this.f17255[this.f17256] = null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m21726(String key) {
        int i = m21734(key);
        if (i != -1) {
            m21717(i);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m21722(String key) {
        int i = m21714(key);
        if (i != -1) {
            m21717(i);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m21723(String key) {
        return m21734(key) != -1;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m21724(String key) {
        return m21714(key) != -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21733() {
        return this.f17256;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21738(Attributes incoming) {
        if (incoming.m21733() != 0) {
            m21719(this.f17256 + incoming.f17256);
            Iterator<Attribute> it2 = incoming.iterator();
            while (it2.hasNext()) {
                m21736(it2.next());
            }
        }
    }

    public Iterator<Attribute> iterator() {
        return new Iterator<Attribute>() {

            /* renamed from: 龘  reason: contains not printable characters */
            int f17259 = 0;

            public boolean hasNext() {
                return this.f17259 < Attributes.this.f17256;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Attribute next() {
                Attribute attr = new Attribute(Attributes.this.f17257[this.f17259], Attributes.this.f17255[this.f17259], Attributes.this);
                this.f17259++;
                return attr;
            }

            public void remove() {
                Attributes attributes = Attributes.this;
                int i = this.f17259 - 1;
                this.f17259 = i;
                attributes.m21717(i);
            }
        };
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<Attribute> m21727() {
        Attribute attr;
        ArrayList<Attribute> list = new ArrayList<>(this.f17256);
        for (int i = 0; i < this.f17256; i++) {
            if (this.f17255[i] == null) {
                attr = new BooleanAttribute(this.f17257[i]);
            } else {
                attr = new Attribute(this.f17257[i], this.f17255[i], this);
            }
            list.add(attr);
        }
        return Collections.unmodifiableList(list);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m21731() {
        StringBuilder accum = new StringBuilder();
        try {
            m21737((Appendable) accum, new Document("").m21768());
            return accum.toString();
        } catch (IOException e) {
            throw new SerializationException((Throwable) e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m21737(Appendable accum, Document.OutputSettings out) throws IOException {
        String str;
        int sz = this.f17256;
        for (int i = 0; i < sz; i++) {
            String key = this.f17257[i];
            String val = this.f17255[i];
            accum.append(' ').append(key);
            if (!Attribute.m21707(key, val, out)) {
                accum.append("=\"");
                if (val == null) {
                    str = "";
                } else {
                    str = val;
                }
                Entities.m21879(accum, str, out, true, false, false);
                accum.append('\"');
            }
        }
    }

    public String toString() {
        return m21731();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Attributes that = (Attributes) o;
        if (this.f17256 != that.f17256 || !Arrays.equals(this.f17257, that.f17257)) {
            return false;
        }
        return Arrays.equals(this.f17255, that.f17255);
    }

    public int hashCode() {
        return (((this.f17256 * 31) + Arrays.hashCode(this.f17257)) * 31) + Arrays.hashCode(this.f17255);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Attributes clone() {
        try {
            Attributes clone = (Attributes) super.clone();
            clone.f17256 = this.f17256;
            this.f17257 = m21721(this.f17257, this.f17256);
            this.f17255 = m21721(this.f17255, this.f17256);
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m21725() {
        for (int i = 0; i < this.f17256; i++) {
            this.f17257[i] = Normalizer.m21704(this.f17257[i]);
        }
    }
}
