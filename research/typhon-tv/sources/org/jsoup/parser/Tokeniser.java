package org.jsoup.parser;

import java.util.Arrays;
import org.apache.commons.lang3.CharUtils;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Token;

final class Tokeniser {

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final char[] f17422 = {9, 10, CharUtils.CR, 12, ' ', '<', '&'};

    /* renamed from: 龘  reason: contains not printable characters */
    static final int[] f17423 = {8364, 129, 8218, 402, 8222, 8230, 8224, 8225, 710, 8240, 352, 8249, 338, 141, 381, 143, 144, 8216, 8217, 8220, 8221, 8226, 8211, 8212, 732, 8482, 353, 8250, 339, 157, 382, 376};

    /* renamed from: ʻ  reason: contains not printable characters */
    Token.Character f17424 = new Token.Character();

    /* renamed from: ʼ  reason: contains not printable characters */
    Token.Doctype f17425 = new Token.Doctype();

    /* renamed from: ʽ  reason: contains not printable characters */
    Token.Comment f17426 = new Token.Comment();

    /* renamed from: ʾ  reason: contains not printable characters */
    private Token f17427;

    /* renamed from: ʿ  reason: contains not printable characters */
    private boolean f17428 = false;

    /* renamed from: ˈ  reason: contains not printable characters */
    private TokeniserState f17429 = TokeniserState.Data;

    /* renamed from: ˊ  reason: contains not printable characters */
    private String f17430;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final int[] f17431 = new int[1];

    /* renamed from: ˎ  reason: contains not printable characters */
    private final int[] f17432 = new int[2];

    /* renamed from: ٴ  reason: contains not printable characters */
    private final CharacterReader f17433;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final ParseErrorList f17434;

    /* renamed from: 连任  reason: contains not printable characters */
    Token.EndTag f17435 = new Token.EndTag();

    /* renamed from: 靐  reason: contains not printable characters */
    StringBuilder f17436 = new StringBuilder(1024);

    /* renamed from: 麤  reason: contains not printable characters */
    Token.StartTag f17437 = new Token.StartTag();

    /* renamed from: 齉  reason: contains not printable characters */
    Token.Tag f17438;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private String f17439 = null;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private StringBuilder f17440 = new StringBuilder(1024);

    static {
        Arrays.sort(f17422);
    }

    Tokeniser(CharacterReader reader, ParseErrorList errors) {
        this.f17433 = reader;
        this.f17434 = errors;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Token m22252() {
        while (!this.f17428) {
            this.f17429.m22270(this, this.f17433);
        }
        if (this.f17440.length() > 0) {
            String str = this.f17440.toString();
            this.f17440.delete(0, this.f17440.length());
            this.f17439 = null;
            return this.f17424.m22187(str);
        } else if (this.f17439 != null) {
            Token.Character r1 = this.f17424.m22187(this.f17439);
            this.f17439 = null;
            return r1;
        } else {
            this.f17428 = false;
            return this.f17427;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22255(Token token) {
        Validate.m21694(this.f17428, "There is an unread token pending!");
        this.f17427 = token;
        this.f17428 = true;
        if (token.f17401 == Token.TokenType.StartTag) {
            this.f17430 = ((Token.StartTag) token).f17416;
        } else if (token.f17401 == Token.TokenType.EndTag && ((Token.EndTag) token).f17415 != null) {
            m22245("Attributes incorrectly present on end tag");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22254(String str) {
        if (this.f17439 == null) {
            this.f17439 = str;
            return;
        }
        if (this.f17440.length() == 0) {
            this.f17440.append(this.f17439);
        }
        this.f17440.append(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22257(int[] codepoints) {
        m22254(new String(codepoints, 0, codepoints.length));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22253(char c) {
        m22254(String.valueOf(c));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22256(TokeniserState state) {
        this.f17429 = state;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22246(TokeniserState state) {
        this.f17433.m21995();
        this.f17429 = state;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m22258(Character additionalAllowedCharacter, boolean inAttribute) {
        if (this.f17433.m22010()) {
            return null;
        }
        if ((additionalAllowedCharacter != null && additionalAllowedCharacter.charValue() == this.f17433.m22015()) || this.f17433.m22014(f17422)) {
            return null;
        }
        int[] codeRef = this.f17431;
        this.f17433.m21997();
        if (this.f17433.m22013("#")) {
            boolean isHexMode = this.f17433.m22007("X");
            String numRef = isHexMode ? this.f17433.m22000() : this.f17433.m22024();
            if (numRef.length() == 0) {
                m22238("numeric reference with no numerals");
                this.f17433.m21998();
                return null;
            }
            if (!this.f17433.m22013(";")) {
                m22238("missing semicolon");
            }
            int charval = -1;
            try {
                charval = Integer.valueOf(numRef, isHexMode ? 16 : 10).intValue();
            } catch (NumberFormatException e) {
            }
            if (charval == -1 || ((charval >= 55296 && charval <= 57343) || charval > 1114111)) {
                m22238("character outside of valid range");
                codeRef[0] = 65533;
                return codeRef;
            }
            if (charval >= 128 && charval < f17423.length + 128) {
                m22238("character is not a valid unicode code point");
                charval = f17423[charval - 128];
            }
            codeRef[0] = charval;
            return codeRef;
        }
        String nameRef = this.f17433.m21999();
        boolean looksLegit = this.f17433.m22016(';');
        if (!(Entities.m21877(nameRef) || (Entities.m21882(nameRef) && looksLegit))) {
            this.f17433.m21998();
            if (looksLegit) {
                m22238(String.format("invalid named referenece '%s'", new Object[]{nameRef}));
            }
            return null;
        } else if (!inAttribute || (!this.f17433.m22025() && !this.f17433.m22002() && !this.f17433.m22018('=', '-', '_'))) {
            if (!this.f17433.m22013(";")) {
                m22238("missing semicolon");
            }
            int numChars = Entities.m21878(nameRef, this.f17432);
            if (numChars == 1) {
                codeRef[0] = this.f17432[0];
                return codeRef;
            } else if (numChars == 2) {
                return this.f17432;
            } else {
                Validate.m21692("Unexpected characters returned for " + nameRef);
                return this.f17432;
            }
        } else {
            this.f17433.m21998();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Token.Tag m22251(boolean start) {
        this.f17438 = start ? this.f17437.m22198() : this.f17435.m22208();
        return this.f17438;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22244() {
        this.f17438.m22204();
        m22255((Token) this.f17438);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m22249() {
        this.f17426.m22189();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m22247() {
        m22255((Token) this.f17426);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m22243() {
        this.f17425.m22194();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m22239() {
        m22255((Token) this.f17425);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m22240() {
        Token.m22171(this.f17436);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m22241() {
        return this.f17430 != null && this.f17438.m22205().equalsIgnoreCase(this.f17430);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public String m22242() {
        return this.f17430;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m22250(TokeniserState state) {
        if (this.f17434.m22149()) {
            this.f17434.add(new ParseError(this.f17433.m22019(), "Unexpected character '%s' in input state [%s]", Character.valueOf(this.f17433.m22015()), state));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m22248(TokeniserState state) {
        if (this.f17434.m22149()) {
            this.f17434.add(new ParseError(this.f17433.m22019(), "Unexpectedly reached end of file (EOF) in input state [%s]", state));
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m22238(String message) {
        if (this.f17434.m22149()) {
            this.f17434.add(new ParseError(this.f17433.m22019(), "Invalid character reference: %s", message));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22245(String errorMsg) {
        if (this.f17434.m22149()) {
            this.f17434.add(new ParseError(this.f17433.m22019(), errorMsg));
        }
    }
}
