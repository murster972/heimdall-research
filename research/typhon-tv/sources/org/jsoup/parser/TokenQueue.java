package org.jsoup.parser;

import io.fabric.sdk.android.services.events.EventsFilesManager;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;

public class TokenQueue {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f17420 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f17421;

    public TokenQueue(String data) {
        Validate.m21695((Object) data);
        this.f17421 = data;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22234() {
        return m22220() == 0;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m22220() {
        return this.f17421.length() - this.f17420;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22235(String seq) {
        return this.f17421.regionMatches(true, this.f17420, seq, 0, seq.length());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22237(String... seq) {
        for (String s : seq) {
            if (m22235(s)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22236(char... seq) {
        if (m22234()) {
            return false;
        }
        for (char c : seq) {
            if (this.f17421.charAt(this.f17420) == c) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m22228(String seq) {
        if (!m22235(seq)) {
            return false;
        }
        this.f17420 += seq.length();
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m22227() {
        return !m22234() && StringUtil.m21676((int) this.f17421.charAt(this.f17420));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m22232() {
        return !m22234() && Character.isLetterOrDigit(this.f17421.charAt(this.f17420));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public char m22229() {
        String str = this.f17421;
        int i = this.f17420;
        this.f17420 = i + 1;
        return str.charAt(i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m22231(String seq) {
        if (!m22235(seq)) {
            throw new IllegalStateException("Queue did not match expected sequence");
        }
        int len = seq.length();
        if (len > m22220()) {
            throw new IllegalStateException("Queue not long enough to consume sequence");
        }
        this.f17420 += len;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m22230(String seq) {
        int offset = this.f17421.indexOf(seq, this.f17420);
        if (offset == -1) {
            return m22223();
        }
        String consumed = this.f17421.substring(this.f17420, offset);
        this.f17420 += consumed.length();
        return consumed;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m22226(String... seq) {
        int start = this.f17420;
        while (!m22234() && !m22237(seq)) {
            this.f17420++;
        }
        return this.f17421.substring(start, this.f17420);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m22224(String seq) {
        String data = m22230(seq);
        m22228(seq);
        return data;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x000e A[EDGE_INSN: B:45:0x000e->B:3:0x000e ?: BREAK  , SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String m22233(char r12, char r13) {
        /*
            r11 = this;
            r8 = 1
            r9 = 0
            r7 = -1
            r2 = -1
            r1 = 0
            r5 = 0
            r4 = 0
            r3 = 0
        L_0x0008:
            boolean r10 = r11.m22234()
            if (r10 == 0) goto L_0x0037
        L_0x000e:
            if (r2 < 0) goto L_0x00a8
            java.lang.String r8 = r11.f17421
            java.lang.String r6 = r8.substring(r7, r2)
        L_0x0016:
            if (r1 <= 0) goto L_0x0036
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Did not find balanced marker at '"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r6)
            java.lang.String r9 = "'"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            org.jsoup.helper.Validate.m21692((java.lang.String) r8)
        L_0x0036:
            return r6
        L_0x0037:
            char r10 = r11.m22229()
            java.lang.Character r0 = java.lang.Character.valueOf(r10)
            if (r5 == 0) goto L_0x0045
            r10 = 92
            if (r5 == r10) goto L_0x0090
        L_0x0045:
            r10 = 39
            java.lang.Character r10 = java.lang.Character.valueOf(r10)
            boolean r10 = r0.equals(r10)
            if (r10 == 0) goto L_0x0065
            char r10 = r0.charValue()
            if (r10 == r12) goto L_0x0065
            if (r3 != 0) goto L_0x0065
            if (r4 != 0) goto L_0x0063
            r4 = r8
        L_0x005c:
            if (r4 != 0) goto L_0x0060
            if (r3 == 0) goto L_0x007f
        L_0x0060:
            if (r1 > 0) goto L_0x0008
            goto L_0x000e
        L_0x0063:
            r4 = r9
            goto L_0x005c
        L_0x0065:
            r10 = 34
            java.lang.Character r10 = java.lang.Character.valueOf(r10)
            boolean r10 = r0.equals(r10)
            if (r10 == 0) goto L_0x005c
            char r10 = r0.charValue()
            if (r10 == r12) goto L_0x005c
            if (r4 != 0) goto L_0x005c
            if (r3 != 0) goto L_0x007d
            r3 = r8
        L_0x007c:
            goto L_0x005c
        L_0x007d:
            r3 = r9
            goto L_0x007c
        L_0x007f:
            java.lang.Character r10 = java.lang.Character.valueOf(r12)
            boolean r10 = r0.equals(r10)
            if (r10 == 0) goto L_0x009b
            int r1 = r1 + 1
            r10 = -1
            if (r7 != r10) goto L_0x0090
            int r7 = r11.f17420
        L_0x0090:
            if (r1 <= 0) goto L_0x0096
            if (r5 == 0) goto L_0x0096
            int r2 = r11.f17420
        L_0x0096:
            char r5 = r0.charValue()
            goto L_0x0060
        L_0x009b:
            java.lang.Character r10 = java.lang.Character.valueOf(r13)
            boolean r10 = r0.equals(r10)
            if (r10 == 0) goto L_0x0090
            int r1 = r1 + -1
            goto L_0x0090
        L_0x00a8:
            java.lang.String r6 = ""
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TokenQueue.m22233(char, char):java.lang.String");
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m22219(String in) {
        StringBuilder out = StringUtil.m21686();
        char last = 0;
        for (char c : in.toCharArray()) {
            if (c != '\\') {
                out.append(c);
            } else if (last != 0 && last == '\\') {
                out.append(c);
            }
            last = c;
        }
        return out.toString();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22225() {
        boolean seen = false;
        while (m22227()) {
            this.f17420++;
            seen = true;
        }
        return seen;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m22221() {
        int start = this.f17420;
        while (!m22234()) {
            if (!m22232()) {
                if (!m22237("*|", "|", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, "-")) {
                    break;
                }
            }
            this.f17420++;
        }
        return this.f17421.substring(start, this.f17420);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m22222() {
        int start = this.f17420;
        while (!m22234() && (m22232() || m22236('-', '_'))) {
            this.f17420++;
        }
        return this.f17421.substring(start, this.f17420);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m22223() {
        String remainder = this.f17421.substring(this.f17420, this.f17421.length());
        this.f17420 = this.f17421.length();
        return remainder;
    }

    public String toString() {
        return this.f17421.substring(this.f17420);
    }
}
