package org.jsoup.parser;

import java.io.Reader;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.CDataNode;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.nodes.XmlDeclaration;
import org.jsoup.parser.Token;

public class XmlTreeBuilder extends TreeBuilder {
    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m22356(String str, Attributes attributes) {
        return super.m22346(str, attributes);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ParseSettings m22351() {
        return ParseSettings.f17376;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22352(Reader input, String baseUri, ParseErrorList errors, ParseSettings settings) {
        super.m22345(input, baseUri, errors, settings);
        this.f17516.add(this.f17519);
        this.f17519.m21768().m21786(Document.OutputSettings.Syntax.xml);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22357(Token token) {
        switch (token.f17401) {
            case StartTag:
                m22350(token.m22172());
                return true;
            case EndTag:
                m22349(token.m22174());
                return true;
            case Comment:
                m22354(token.m22179());
                return true;
            case Character:
                m22353(token.m22175());
                return true;
            case Doctype:
                m22355(token.m22183());
                return true;
            case EOF:
                return true;
            default:
                Validate.m21692("Unexpected token type: " + token.f17401);
                return true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22348(Node node) {
        m22342().m21864(node);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Element m22350(Token.StartTag startTag) {
        Tag tag = Tag.m22160(startTag.m22205(), this.f17523);
        Element el = new Element(tag, this.f17514, this.f17523.m22151(startTag.f17415));
        m22348((Node) el);
        if (!startTag.m22202()) {
            this.f17516.add(el);
        } else if (!tag.m22162()) {
            tag.m22165();
        }
        return el;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22354(Token.Comment commentToken) {
        Comment comment = new Comment(commentToken.m22190());
        Node insert = comment;
        if (commentToken.f17404) {
            String data = comment.m21743();
            if (data.length() > 1 && (data.startsWith("!") || data.startsWith("?"))) {
                Document doc = Jsoup.m21675("<" + data.substring(1, data.length() - 1) + ">", this.f17514, Parser.m22154());
                if (doc.m21860() > 0) {
                    Element el = doc.m21862(0);
                    insert = new XmlDeclaration(this.f17523.m22150(el.m21869()), data.startsWith("!"));
                    insert.m21920().m21738(el.m21820());
                }
            }
        }
        m22348(insert);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22353(Token.Character token) {
        String data = token.m22188();
        m22348(token.m22177() ? new CDataNode(data) : new TextNode(data));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22355(Token.Doctype d) {
        DocumentType doctypeNode = new DocumentType(this.f17523.m22150(d.m22195()), d.m22191(), d.m22192());
        doctypeNode.m21789(d.m22196());
        m22348((Node) doctypeNode);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22349(Token.EndTag endTag) {
        String elName = this.f17523.m22150(endTag.f17416);
        Element firstFound = null;
        int pos = this.f17516.size() - 1;
        while (true) {
            if (pos < 0) {
                break;
            }
            Element next = (Element) this.f17516.get(pos);
            if (next.m21861().equals(elName)) {
                firstFound = next;
                break;
            }
            pos--;
        }
        if (firstFound != null) {
            int pos2 = this.f17516.size() - 1;
            while (pos2 >= 0) {
                Element next2 = (Element) this.f17516.get(pos2);
                this.f17516.remove(pos2);
                if (next2 != firstFound) {
                    pos2--;
                } else {
                    return;
                }
            }
        }
    }
}
