package org.jsoup.parser;

import org.apache.commons.lang3.CharUtils;
import org.jsoup.parser.Token;

enum TokeniserState {
    Data {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22271(Tokeniser t, CharacterReader r) {
            switch (r.m22015()) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.m22253(r.m22012());
                    return;
                case '&':
                    t.m22246(CharacterReferenceInData);
                    return;
                case '<':
                    t.m22246(TagOpen);
                    return;
                case 65535:
                    t.m22255((Token) new Token.EOF());
                    return;
                default:
                    t.m22254(r.m22003());
                    return;
            }
        }
    },
    CharacterReferenceInData {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22283(Tokeniser t, CharacterReader r) {
            TokeniserState.m22263(t, Data);
        }
    },
    Rcdata {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22294(Tokeniser t, CharacterReader r) {
            switch (r.m22015()) {
                case 0:
                    t.m22250((TokeniserState) this);
                    r.m21995();
                    t.m22253(65533);
                    return;
                case '&':
                    t.m22246(CharacterReferenceInRcdata);
                    return;
                case '<':
                    t.m22246(RcdataLessthanSign);
                    return;
                case 65535:
                    t.m22255((Token) new Token.EOF());
                    return;
                default:
                    t.m22254(r.m22023('&', '<', 0));
                    return;
            }
        }
    },
    CharacterReferenceInRcdata {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22305(Tokeniser t, CharacterReader r) {
            TokeniserState.m22263(t, Rcdata);
        }
    },
    Rawtext {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22316(Tokeniser t, CharacterReader r) {
            TokeniserState.m22264(t, r, this, RawtextLessthanSign);
        }
    },
    ScriptData {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22327(Tokeniser t, CharacterReader r) {
            TokeniserState.m22264(t, r, this, ScriptDataLessthanSign);
        }
    },
    PLAINTEXT {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22336(Tokeniser t, CharacterReader r) {
            switch (r.m22015()) {
                case 0:
                    t.m22250((TokeniserState) this);
                    r.m21995();
                    t.m22253(65533);
                    return;
                case 65535:
                    t.m22255((Token) new Token.EOF());
                    return;
                default:
                    t.m22254(r.m22008(0));
                    return;
            }
        }
    },
    TagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22337(Tokeniser t, CharacterReader r) {
            switch (r.m22015()) {
                case '!':
                    t.m22246(MarkupDeclarationOpen);
                    return;
                case '/':
                    t.m22246(EndTagOpen);
                    return;
                case '?':
                    t.m22246(BogusComment);
                    return;
                default:
                    if (r.m22025()) {
                        t.m22251(true);
                        t.m22256(TagName);
                        return;
                    }
                    t.m22250((TokeniserState) this);
                    t.m22253('<');
                    t.m22256(Data);
                    return;
            }
        }
    },
    EndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22338(Tokeniser t, CharacterReader r) {
            if (r.m22010()) {
                t.m22248(this);
                t.m22254("</");
                t.m22256(Data);
            } else if (r.m22025()) {
                t.m22251(false);
                t.m22256(TagName);
            } else if (r.m22016('>')) {
                t.m22250((TokeniserState) this);
                t.m22246(Data);
            } else {
                t.m22250((TokeniserState) this);
                t.m22246(BogusComment);
            }
        }
    },
    TagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22272(Tokeniser t, CharacterReader r) {
            t.f17438.m22210(r.m22004());
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.f17438.m22210(TokeniserState.f17459);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BeforeAttributeName);
                    return;
                case '/':
                    t.m22256(SelfClosingStartTag);
                    return;
                case '>':
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.f17438.m22215(c);
                    return;
            }
        }
    },
    RcdataLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22273(Tokeniser t, CharacterReader r) {
            if (r.m22016('/')) {
                t.m22240();
                t.m22246(RCDATAEndTagOpen);
            } else if (!r.m22025() || t.m22242() == null || r.m21996("</" + t.m22242())) {
                t.m22254("<");
                t.m22256(Rcdata);
            } else {
                t.f17438 = t.m22251(false).m22214(t.m22242());
                t.m22244();
                r.m22006();
                t.m22256(Data);
            }
        }
    },
    RCDATAEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22274(Tokeniser t, CharacterReader r) {
            if (r.m22025()) {
                t.m22251(false);
                t.f17438.m22215(r.m22015());
                t.f17436.append(r.m22015());
                t.m22246(RCDATAEndTagName);
                return;
            }
            t.m22254("</");
            t.m22256(Rcdata);
        }
    },
    RCDATAEndTagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22276(Tokeniser t, CharacterReader r) {
            if (r.m22025()) {
                String name = r.m22001();
                t.f17438.m22210(name);
                t.f17436.append(name);
                return;
            }
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    if (t.m22241()) {
                        t.m22256(BeforeAttributeName);
                        return;
                    } else {
                        m22275(t, r);
                        return;
                    }
                case '/':
                    if (t.m22241()) {
                        t.m22256(SelfClosingStartTag);
                        return;
                    } else {
                        m22275(t, r);
                        return;
                    }
                case '>':
                    if (t.m22241()) {
                        t.m22244();
                        t.m22256(Data);
                        return;
                    }
                    m22275(t, r);
                    return;
                default:
                    m22275(t, r);
                    return;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m22275(Tokeniser t, CharacterReader r) {
            t.m22254("</" + t.f17436.toString());
            r.m22006();
            t.m22256(Rcdata);
        }
    },
    RawtextLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22277(Tokeniser t, CharacterReader r) {
            if (r.m22016('/')) {
                t.m22240();
                t.m22246(RawtextEndTagOpen);
                return;
            }
            t.m22253('<');
            t.m22256(Rawtext);
        }
    },
    RawtextEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22278(Tokeniser t, CharacterReader r) {
            TokeniserState.m22260(t, r, RawtextEndTagName, Rawtext);
        }
    },
    RawtextEndTagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22279(Tokeniser t, CharacterReader r) {
            TokeniserState.m22261(t, r, Rawtext);
        }
    },
    ScriptDataLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22280(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case '!':
                    t.m22254("<!");
                    t.m22256(ScriptDataEscapeStart);
                    return;
                case '/':
                    t.m22240();
                    t.m22256(ScriptDataEndTagOpen);
                    return;
                default:
                    t.m22254("<");
                    r.m22006();
                    t.m22256(ScriptData);
                    return;
            }
        }
    },
    ScriptDataEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22281(Tokeniser t, CharacterReader r) {
            TokeniserState.m22260(t, r, ScriptDataEndTagName, ScriptData);
        }
    },
    ScriptDataEndTagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22282(Tokeniser t, CharacterReader r) {
            TokeniserState.m22261(t, r, ScriptData);
        }
    },
    ScriptDataEscapeStart {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22284(Tokeniser t, CharacterReader r) {
            if (r.m22016('-')) {
                t.m22253('-');
                t.m22246(ScriptDataEscapeStartDash);
                return;
            }
            t.m22256(ScriptData);
        }
    },
    ScriptDataEscapeStartDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22285(Tokeniser t, CharacterReader r) {
            if (r.m22016('-')) {
                t.m22253('-');
                t.m22246(ScriptDataEscapedDashDash);
                return;
            }
            t.m22256(ScriptData);
        }
    },
    ScriptDataEscaped {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22286(Tokeniser t, CharacterReader r) {
            if (r.m22010()) {
                t.m22248(this);
                t.m22256(Data);
                return;
            }
            switch (r.m22015()) {
                case 0:
                    t.m22250((TokeniserState) this);
                    r.m21995();
                    t.m22253(65533);
                    return;
                case '-':
                    t.m22253('-');
                    t.m22246(ScriptDataEscapedDash);
                    return;
                case '<':
                    t.m22246(ScriptDataEscapedLessthanSign);
                    return;
                default:
                    t.m22254(r.m22023('-', '<', 0));
                    return;
            }
        }
    },
    ScriptDataEscapedDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22287(Tokeniser t, CharacterReader r) {
            if (r.m22010()) {
                t.m22248(this);
                t.m22256(Data);
                return;
            }
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.m22253(65533);
                    t.m22256(ScriptDataEscaped);
                    return;
                case '-':
                    t.m22253(c);
                    t.m22256(ScriptDataEscapedDashDash);
                    return;
                case '<':
                    t.m22256(ScriptDataEscapedLessthanSign);
                    return;
                default:
                    t.m22253(c);
                    t.m22256(ScriptDataEscaped);
                    return;
            }
        }
    },
    ScriptDataEscapedDashDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22288(Tokeniser t, CharacterReader r) {
            if (r.m22010()) {
                t.m22248(this);
                t.m22256(Data);
                return;
            }
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.m22253(65533);
                    t.m22256(ScriptDataEscaped);
                    return;
                case '-':
                    t.m22253(c);
                    return;
                case '<':
                    t.m22256(ScriptDataEscapedLessthanSign);
                    return;
                case '>':
                    t.m22253(c);
                    t.m22256(ScriptData);
                    return;
                default:
                    t.m22253(c);
                    t.m22256(ScriptDataEscaped);
                    return;
            }
        }
    },
    ScriptDataEscapedLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22289(Tokeniser t, CharacterReader r) {
            if (r.m22025()) {
                t.m22240();
                t.f17436.append(r.m22015());
                t.m22254("<" + r.m22015());
                t.m22246(ScriptDataDoubleEscapeStart);
            } else if (r.m22016('/')) {
                t.m22240();
                t.m22246(ScriptDataEscapedEndTagOpen);
            } else {
                t.m22253('<');
                t.m22256(ScriptDataEscaped);
            }
        }
    },
    ScriptDataEscapedEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22290(Tokeniser t, CharacterReader r) {
            if (r.m22025()) {
                t.m22251(false);
                t.f17438.m22215(r.m22015());
                t.f17436.append(r.m22015());
                t.m22246(ScriptDataEscapedEndTagName);
                return;
            }
            t.m22254("</");
            t.m22256(ScriptDataEscaped);
        }
    },
    ScriptDataEscapedEndTagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22291(Tokeniser t, CharacterReader r) {
            TokeniserState.m22261(t, r, ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscapeStart {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22292(Tokeniser t, CharacterReader r) {
            TokeniserState.m22259(t, r, ScriptDataDoubleEscaped, ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscaped {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22293(Tokeniser t, CharacterReader r) {
            char c = r.m22015();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    r.m21995();
                    t.m22253(65533);
                    return;
                case '-':
                    t.m22253(c);
                    t.m22246(ScriptDataDoubleEscapedDash);
                    return;
                case '<':
                    t.m22253(c);
                    t.m22246(ScriptDataDoubleEscapedLessthanSign);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.m22254(r.m22023('-', '<', 0));
                    return;
            }
        }
    },
    ScriptDataDoubleEscapedDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22295(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.m22253(65533);
                    t.m22256(ScriptDataDoubleEscaped);
                    return;
                case '-':
                    t.m22253(c);
                    t.m22256(ScriptDataDoubleEscapedDashDash);
                    return;
                case '<':
                    t.m22253(c);
                    t.m22256(ScriptDataDoubleEscapedLessthanSign);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.m22253(c);
                    t.m22256(ScriptDataDoubleEscaped);
                    return;
            }
        }
    },
    ScriptDataDoubleEscapedDashDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22296(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.m22253(65533);
                    t.m22256(ScriptDataDoubleEscaped);
                    return;
                case '-':
                    t.m22253(c);
                    return;
                case '<':
                    t.m22253(c);
                    t.m22256(ScriptDataDoubleEscapedLessthanSign);
                    return;
                case '>':
                    t.m22253(c);
                    t.m22256(ScriptData);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.m22253(c);
                    t.m22256(ScriptDataDoubleEscaped);
                    return;
            }
        }
    },
    ScriptDataDoubleEscapedLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22297(Tokeniser t, CharacterReader r) {
            if (r.m22016('/')) {
                t.m22253('/');
                t.m22240();
                t.m22246(ScriptDataDoubleEscapeEnd);
                return;
            }
            t.m22256(ScriptDataDoubleEscaped);
        }
    },
    ScriptDataDoubleEscapeEnd {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22298(Tokeniser t, CharacterReader r) {
            TokeniserState.m22259(t, r, ScriptDataEscaped, ScriptDataDoubleEscaped);
        }
    },
    BeforeAttributeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22299(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17438.m22218();
                    r.m22006();
                    t.m22256(AttributeName);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                case '\'':
                case '<':
                case '=':
                    t.m22250((TokeniserState) this);
                    t.f17438.m22218();
                    t.f17438.m22209(c);
                    t.m22256(AttributeName);
                    return;
                case '/':
                    t.m22256(SelfClosingStartTag);
                    return;
                case '>':
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.f17438.m22218();
                    r.m22006();
                    t.m22256(AttributeName);
                    return;
            }
        }
    },
    AttributeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22300(Tokeniser t, CharacterReader r) {
            t.f17438.m22213(r.m22009(f17457));
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17438.m22209(65533);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(AfterAttributeName);
                    return;
                case '\"':
                case '\'':
                case '<':
                    t.m22250((TokeniserState) this);
                    t.f17438.m22209(c);
                    return;
                case '/':
                    t.m22256(SelfClosingStartTag);
                    return;
                case '=':
                    t.m22256(BeforeAttributeValue);
                    return;
                case '>':
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.f17438.m22209(c);
                    return;
            }
        }
    },
    AfterAttributeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22301(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17438.m22209(65533);
                    t.m22256(AttributeName);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                case '\'':
                case '<':
                    t.m22250((TokeniserState) this);
                    t.f17438.m22218();
                    t.f17438.m22209(c);
                    t.m22256(AttributeName);
                    return;
                case '/':
                    t.m22256(SelfClosingStartTag);
                    return;
                case '=':
                    t.m22256(BeforeAttributeValue);
                    return;
                case '>':
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.f17438.m22218();
                    r.m22006();
                    t.m22256(AttributeName);
                    return;
            }
        }
    },
    BeforeAttributeValue {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22302(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17438.m22212(65533);
                    t.m22256(AttributeValue_unquoted);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    t.m22256(AttributeValue_doubleQuoted);
                    return;
                case '&':
                    r.m22006();
                    t.m22256(AttributeValue_unquoted);
                    return;
                case '\'':
                    t.m22256(AttributeValue_singleQuoted);
                    return;
                case '<':
                case '=':
                case '`':
                    t.m22250((TokeniserState) this);
                    t.f17438.m22212(c);
                    t.m22256(AttributeValue_unquoted);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22244();
                    t.m22256(Data);
                    return;
                default:
                    r.m22006();
                    t.m22256(AttributeValue_unquoted);
                    return;
            }
        }
    },
    AttributeValue_doubleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22303(Tokeniser t, CharacterReader r) {
            String value = r.m22023(f17456);
            if (value.length() > 0) {
                t.f17438.m22211(value);
            } else {
                t.f17438.m22207();
            }
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17438.m22212(65533);
                    return;
                case '\"':
                    t.m22256(AfterAttributeValue_quoted);
                    return;
                case '&':
                    int[] ref = t.m22258('\"', true);
                    if (ref != null) {
                        t.f17438.m22216(ref);
                        return;
                    } else {
                        t.f17438.m22212('&');
                        return;
                    }
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.f17438.m22212(c);
                    return;
            }
        }
    },
    AttributeValue_singleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22304(Tokeniser t, CharacterReader r) {
            String value = r.m22023(f17455);
            if (value.length() > 0) {
                t.f17438.m22211(value);
            } else {
                t.f17438.m22207();
            }
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17438.m22212(65533);
                    return;
                case '&':
                    int[] ref = t.m22258('\'', true);
                    if (ref != null) {
                        t.f17438.m22216(ref);
                        return;
                    } else {
                        t.f17438.m22212('&');
                        return;
                    }
                case '\'':
                    t.m22256(AfterAttributeValue_quoted);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.f17438.m22212(c);
                    return;
            }
        }
    },
    AttributeValue_unquoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22306(Tokeniser t, CharacterReader r) {
            String value = r.m22009(f17458);
            if (value.length() > 0) {
                t.f17438.m22211(value);
            }
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17438.m22212(65533);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BeforeAttributeName);
                    return;
                case '\"':
                case '\'':
                case '<':
                case '=':
                case '`':
                    t.m22250((TokeniserState) this);
                    t.f17438.m22212(c);
                    return;
                case '&':
                    int[] ref = t.m22258('>', true);
                    if (ref != null) {
                        t.f17438.m22216(ref);
                        return;
                    } else {
                        t.f17438.m22212('&');
                        return;
                    }
                case '>':
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.f17438.m22212(c);
                    return;
            }
        }
    },
    AfterAttributeValue_quoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22307(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BeforeAttributeName);
                    return;
                case '/':
                    t.m22256(SelfClosingStartTag);
                    return;
                case '>':
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    r.m22006();
                    t.m22256(BeforeAttributeName);
                    return;
            }
        }
    },
    SelfClosingStartTag {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22308(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case '>':
                    t.f17438.f17417 = true;
                    t.m22244();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    r.m22006();
                    t.m22256(BeforeAttributeName);
                    return;
            }
        }
    },
    BogusComment {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22309(Tokeniser t, CharacterReader r) {
            r.m22006();
            Token.Comment comment = new Token.Comment();
            comment.f17404 = true;
            comment.f17403.append(r.m22008('>'));
            t.m22255((Token) comment);
            t.m22246(Data);
        }
    },
    MarkupDeclarationOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22310(Tokeniser t, CharacterReader r) {
            if (r.m22013("--")) {
                t.m22249();
                t.m22256(CommentStart);
            } else if (r.m22007("DOCTYPE")) {
                t.m22256(Doctype);
            } else if (r.m22013("[CDATA[")) {
                t.m22240();
                t.m22256(CdataSection);
            } else {
                t.m22250((TokeniserState) this);
                t.m22246(BogusComment);
            }
        }
    },
    CommentStart {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22311(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17426.f17403.append(65533);
                    t.m22256(Comment);
                    return;
                case '-':
                    t.m22256(CommentStartDash);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                default:
                    t.f17426.f17403.append(c);
                    t.m22256(Comment);
                    return;
            }
        }
    },
    CommentStartDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22312(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17426.f17403.append(65533);
                    t.m22256(Comment);
                    return;
                case '-':
                    t.m22256(CommentStartDash);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                default:
                    t.f17426.f17403.append(c);
                    t.m22256(Comment);
                    return;
            }
        }
    },
    Comment {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22313(Tokeniser t, CharacterReader r) {
            switch (r.m22015()) {
                case 0:
                    t.m22250((TokeniserState) this);
                    r.m21995();
                    t.f17426.f17403.append(65533);
                    return;
                case '-':
                    t.m22246(CommentEndDash);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                default:
                    t.f17426.f17403.append(r.m22023('-', 0));
                    return;
            }
        }
    },
    CommentEndDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22314(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17426.f17403.append('-').append(65533);
                    t.m22256(Comment);
                    return;
                case '-':
                    t.m22256(CommentEnd);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                default:
                    t.f17426.f17403.append('-').append(c);
                    t.m22256(Comment);
                    return;
            }
        }
    },
    CommentEnd {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22315(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17426.f17403.append("--").append(65533);
                    t.m22256(Comment);
                    return;
                case '!':
                    t.m22250((TokeniserState) this);
                    t.m22256(CommentEndBang);
                    return;
                case '-':
                    t.m22250((TokeniserState) this);
                    t.f17426.f17403.append('-');
                    return;
                case '>':
                    t.m22247();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.f17426.f17403.append("--").append(c);
                    t.m22256(Comment);
                    return;
            }
        }
    },
    CommentEndBang {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22317(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17426.f17403.append("--!").append(65533);
                    t.m22256(Comment);
                    return;
                case '-':
                    t.f17426.f17403.append("--!");
                    t.m22256(CommentEndDash);
                    return;
                case '>':
                    t.m22247();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22247();
                    t.m22256(Data);
                    return;
                default:
                    t.f17426.f17403.append("--!").append(c);
                    t.m22256(Comment);
                    return;
            }
        }
    },
    Doctype {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22318(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BeforeDoctypeName);
                    return;
                case '>':
                    break;
                case 65535:
                    t.m22248(this);
                    break;
                default:
                    t.m22250((TokeniserState) this);
                    t.m22256(BeforeDoctypeName);
                    return;
            }
            t.m22250((TokeniserState) this);
            t.m22243();
            t.f17425.f17405 = true;
            t.m22239();
            t.m22256(Data);
        }
    },
    BeforeDoctypeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22319(Tokeniser t, CharacterReader r) {
            if (r.m22025()) {
                t.m22243();
                t.m22256(DoctypeName);
                return;
            }
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.m22243();
                    t.f17425.f17407.append(65533);
                    t.m22256(DoctypeName);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case 65535:
                    t.m22248(this);
                    t.m22243();
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22243();
                    t.f17425.f17407.append(c);
                    t.m22256(DoctypeName);
                    return;
            }
        }
    },
    DoctypeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22320(Tokeniser t, CharacterReader r) {
            if (r.m22025()) {
                t.f17425.f17407.append(r.m22001());
                return;
            }
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17407.append(65533);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(AfterDoctypeName);
                    return;
                case '>':
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.f17425.f17407.append(c);
                    return;
            }
        }
    },
    AfterDoctypeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22321(Tokeniser t, CharacterReader r) {
            if (r.m22010()) {
                t.m22248(this);
                t.f17425.f17405 = true;
                t.m22239();
                t.m22256(Data);
            } else if (r.m22018(9, 10, CharUtils.CR, 12, ' ')) {
                r.m21995();
            } else if (r.m22016('>')) {
                t.m22239();
                t.m22246(Data);
            } else if (r.m22007("PUBLIC")) {
                t.f17425.f17409 = "PUBLIC";
                t.m22256(AfterDoctypePublicKeyword);
            } else if (r.m22007("SYSTEM")) {
                t.f17425.f17409 = "SYSTEM";
                t.m22256(AfterDoctypeSystemKeyword);
            } else {
                t.m22250((TokeniserState) this);
                t.f17425.f17405 = true;
                t.m22246(BogusDoctype);
            }
        }
    },
    AfterDoctypePublicKeyword {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22322(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BeforeDoctypePublicIdentifier);
                    return;
                case '\"':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypePublicIdentifier_doubleQuoted);
                    return;
                case '\'':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypePublicIdentifier_singleQuoted);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22256(BogusDoctype);
                    return;
            }
        }
    },
    BeforeDoctypePublicIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22323(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    t.m22256(DoctypePublicIdentifier_doubleQuoted);
                    return;
                case '\'':
                    t.m22256(DoctypePublicIdentifier_singleQuoted);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22256(BogusDoctype);
                    return;
            }
        }
    },
    DoctypePublicIdentifier_doubleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22324(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17408.append(65533);
                    return;
                case '\"':
                    t.m22256(AfterDoctypePublicIdentifier);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.f17425.f17408.append(c);
                    return;
            }
        }
    },
    DoctypePublicIdentifier_singleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22325(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17408.append(65533);
                    return;
                case '\'':
                    t.m22256(AfterDoctypePublicIdentifier);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.f17425.f17408.append(c);
                    return;
            }
        }
    },
    AfterDoctypePublicIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22326(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BetweenDoctypePublicAndSystemIdentifiers);
                    return;
                case '\"':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22256(BogusDoctype);
                    return;
            }
        }
    },
    BetweenDoctypePublicAndSystemIdentifiers {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22328(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22256(BogusDoctype);
                    return;
            }
        }
    },
    AfterDoctypeSystemKeyword {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22329(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BeforeDoctypeSystemIdentifier);
                    return;
                case '\"':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    t.m22250((TokeniserState) this);
                    t.m22256(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    return;
            }
        }
    },
    BeforeDoctypeSystemIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22330(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    t.m22256(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    t.m22256(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22256(BogusDoctype);
                    return;
            }
        }
    },
    DoctypeSystemIdentifier_doubleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22331(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17406.append(65533);
                    return;
                case '\"':
                    t.m22256(AfterDoctypeSystemIdentifier);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.f17425.f17406.append(c);
                    return;
            }
        }
    },
    DoctypeSystemIdentifier_singleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22332(Tokeniser t, CharacterReader r) {
            char c = r.m22012();
            switch (c) {
                case 0:
                    t.m22250((TokeniserState) this);
                    t.f17425.f17406.append(65533);
                    return;
                case '\'':
                    t.m22256(AfterDoctypeSystemIdentifier);
                    return;
                case '>':
                    t.m22250((TokeniserState) this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.f17425.f17406.append(c);
                    return;
            }
        }
    },
    AfterDoctypeSystemIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22333(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '>':
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22248(this);
                    t.f17425.f17405 = true;
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    t.m22250((TokeniserState) this);
                    t.m22256(BogusDoctype);
                    return;
            }
        }
    },
    BogusDoctype {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22334(Tokeniser t, CharacterReader r) {
            switch (r.m22012()) {
                case '>':
                    t.m22239();
                    t.m22256(Data);
                    return;
                case 65535:
                    t.m22239();
                    t.m22256(Data);
                    return;
                default:
                    return;
            }
        }
    },
    CdataSection {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22335(Tokeniser t, CharacterReader r) {
            t.f17436.append(r.m22022("]]>"));
            if (r.m22013("]]>") || r.m22010()) {
                t.m22255((Token) new Token.CData(t.f17436.toString()));
                t.m22256(Data);
            }
        }
    };
    

    /* renamed from: ʻי  reason: contains not printable characters */
    static final char[] f17455 = null;

    /* renamed from: ʻـ  reason: contains not printable characters */
    static final char[] f17456 = null;

    /* renamed from: ʻٴ  reason: contains not printable characters */
    static final char[] f17457 = null;

    /* renamed from: ʻᐧ  reason: contains not printable characters */
    static final char[] f17458 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻᴵ  reason: contains not printable characters */
    public static final String f17459 = null;

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m22270(Tokeniser tokeniser, CharacterReader characterReader);

    static {
        f17455 = new char[]{0, '&', '\''};
        f17456 = new char[]{0, '\"', '&'};
        f17457 = new char[]{0, 9, 10, 12, CharUtils.CR, ' ', '\"', '\'', '/', '<', '=', '>'};
        f17458 = new char[]{0, 9, 10, 12, CharUtils.CR, ' ', '\"', '&', '\'', '<', '=', '>', '`'};
        f17459 = String.valueOf(65533);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m22261(Tokeniser t, CharacterReader r, TokeniserState elseTransition) {
        if (r.m22025()) {
            String name = r.m22001();
            t.f17438.m22210(name);
            t.f17436.append(name);
            return;
        }
        boolean needsExitTransition = false;
        if (t.m22241() && !r.m22010()) {
            char c = r.m22012();
            switch (c) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    t.m22256(BeforeAttributeName);
                    break;
                case '/':
                    t.m22256(SelfClosingStartTag);
                    break;
                case '>':
                    t.m22244();
                    t.m22256(Data);
                    break;
                default:
                    t.f17436.append(c);
                    needsExitTransition = true;
                    break;
            }
        } else {
            needsExitTransition = true;
        }
        if (needsExitTransition) {
            t.m22254("</" + t.f17436.toString());
            t.m22256(elseTransition);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static void m22264(Tokeniser t, CharacterReader r, TokeniserState current, TokeniserState advance) {
        switch (r.m22015()) {
            case 0:
                t.m22250(current);
                r.m21995();
                t.m22253(65533);
                return;
            case '<':
                t.m22246(advance);
                return;
            case 65535:
                t.m22255((Token) new Token.EOF());
                return;
            default:
                t.m22254(r.m22023('<', 0));
                return;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m22263(Tokeniser t, TokeniserState advance) {
        int[] c = t.m22258((Character) null, false);
        if (c == null) {
            t.m22253('&');
        } else {
            t.m22257(c);
        }
        t.m22256(advance);
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public static void m22260(Tokeniser t, CharacterReader r, TokeniserState a, TokeniserState b) {
        if (r.m22025()) {
            t.m22251(false);
            t.m22256(a);
            return;
        }
        t.m22254("</");
        t.m22256(b);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m22259(Tokeniser t, CharacterReader r, TokeniserState primary, TokeniserState fallback) {
        if (r.m22025()) {
            String name = r.m22001();
            t.f17436.append(name);
            t.m22254(name);
            return;
        }
        char c = r.m22012();
        switch (c) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
            case '/':
            case '>':
                if (t.f17436.toString().equals("script")) {
                    t.m22256(primary);
                } else {
                    t.m22256(fallback);
                }
                t.m22253(c);
                return;
            default:
                r.m22006();
                t.m22256(fallback);
                return;
        }
    }
}
