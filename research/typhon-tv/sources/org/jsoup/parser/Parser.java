package org.jsoup.parser;

import java.io.StringReader;
import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

public class Parser {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f17380 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    private ParseSettings f17381;

    /* renamed from: 齉  reason: contains not printable characters */
    private ParseErrorList f17382;

    /* renamed from: 龘  reason: contains not printable characters */
    private TreeBuilder f17383;

    public Parser(TreeBuilder treeBuilder) {
        this.f17383 = treeBuilder;
        this.f17381 = treeBuilder.m22344();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Document m22156(String html, String baseUri) {
        this.f17382 = m22158() ? ParseErrorList.tracking(this.f17380) : ParseErrorList.noTracking();
        return this.f17383.m22343(new StringReader(html), baseUri, this.f17382, this.f17381);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22158() {
        return this.f17380 > 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Parser m22157(ParseSettings settings) {
        this.f17381 = settings;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Document m22152(String html, String baseUri) {
        TreeBuilder treeBuilder = new HtmlTreeBuilder();
        return treeBuilder.m22343(new StringReader(html), baseUri, ParseErrorList.noTracking(), treeBuilder.m22344());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<Node> m22155(String fragmentHtml, Element context, String baseUri) {
        HtmlTreeBuilder treeBuilder = new HtmlTreeBuilder();
        return treeBuilder.m22081(fragmentHtml, context, baseUri, ParseErrorList.noTracking(), treeBuilder.m22085());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Parser m22153() {
        return new Parser(new HtmlTreeBuilder());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Parser m22154() {
        return new Parser(new XmlTreeBuilder());
    }
}
