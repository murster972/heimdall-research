package org.jsoup.parser;

import android.support.v4.app.NotificationCompat;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mopub.common.AdType;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.oltu.oauth2.common.OAuth;
import org.jsoup.helper.Validate;

public class Tag {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final String[] f17384 = {"meta", "link", "base", "frame", "img", TtmlNode.TAG_BR, "wbr", "embed", "hr", "input", "keygen", "col", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track"};

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final String[] f17385 = {PubnativeAsset.TITLE, "a", TtmlNode.TAG_P, "h1", "h2", "h3", "h4", "h5", "h6", "pre", "address", "li", "th", "td", "script", TtmlNode.TAG_STYLE, "ins", "del", "s"};

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final String[] f17386 = {"object", "base", "font", TtmlNode.TAG_TT, "i", "b", "u", "big", "small", "em", "strong", "dfn", OAuth.OAUTH_CODE, "samp", "kbd", "var", "cite", "abbr", "time", "acronym", "mark", "ruby", "rt", "rp", "a", "img", TtmlNode.TAG_BR, "wbr", "map", "q", "sub", "sup", "bdo", "iframe", "embed", TtmlNode.TAG_SPAN, "input", "select", "textarea", "label", "button", "optgroup", "option", "legend", "datalist", "keygen", "output", NotificationCompat.CATEGORY_PROGRESS, "meter", "area", "param", "source", "track", "summary", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track", "data", "bdi", "s"};

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final String[] f17387 = {"input", "keygen", "object", "select", "textarea"};

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final String[] f17388 = {AdType.HTML, TtmlNode.TAG_HEAD, TtmlNode.TAG_BODY, "frameset", "script", "noscript", TtmlNode.TAG_STYLE, "meta", "link", PubnativeAsset.TITLE, "frame", "noframes", "section", "nav", "aside", "hgroup", "header", "footer", TtmlNode.TAG_P, "h1", "h2", "h3", "h4", "h5", "h6", "ul", "ol", "pre", TtmlNode.TAG_DIV, "blockquote", "hr", "address", "figure", "figcaption", "form", "fieldset", "ins", "del", "dl", "dt", "dd", "li", "table", "caption", "thead", "tfoot", "tbody", "colgroup", "col", "tr", "th", "td", "video", MimeTypes.BASE_TYPE_AUDIO, "canvas", "details", "menu", "plaintext", "template", "article", "main", "svg", "math"};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Map<String, Tag> f17389 = new HashMap();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final String[] f17390 = {"pre", "plaintext", PubnativeAsset.TITLE, "textarea"};

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final String[] f17391 = {"button", "fieldset", "input", "keygen", "object", "output", "select", "textarea"};

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f17392 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f17393 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f17394 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f17395 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f17396 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f17397 = true;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f17398;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f17399 = true;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f17400 = true;

    static {
        for (String tagName : f17388) {
            m22161(new Tag(tagName));
        }
        for (String tagName2 : f17386) {
            Tag tag = new Tag(tagName2);
            tag.f17400 = false;
            tag.f17399 = false;
            m22161(tag);
        }
        for (String tagName3 : f17384) {
            Tag tag2 = f17389.get(tagName3);
            Validate.m21695((Object) tag2);
            tag2.f17397 = false;
            tag2.f17392 = true;
        }
        for (String tagName4 : f17385) {
            Tag tag3 = f17389.get(tagName4);
            Validate.m21695((Object) tag3);
            tag3.f17399 = false;
        }
        for (String tagName5 : f17390) {
            Tag tag4 = f17389.get(tagName5);
            Validate.m21695((Object) tag4);
            tag4.f17394 = true;
        }
        for (String tagName6 : f17391) {
            Tag tag5 = f17389.get(tagName6);
            Validate.m21695((Object) tag5);
            tag5.f17395 = true;
        }
        for (String tagName7 : f17387) {
            Tag tag6 = f17389.get(tagName7);
            Validate.m21695((Object) tag6);
            tag6.f17396 = true;
        }
    }

    private Tag(String tagName) {
        this.f17398 = tagName;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m22170() {
        return this.f17398;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Tag m22160(String tagName, ParseSettings settings) {
        Validate.m21695((Object) tagName);
        Tag tag = f17389.get(tagName);
        if (tag != null) {
            return tag;
        }
        String tagName2 = settings.m22150(tagName);
        Validate.m21697(tagName2);
        Tag tag2 = f17389.get(tagName2);
        if (tag2 != null) {
            return tag2;
        }
        Tag tag3 = new Tag(tagName2);
        tag3.f17400 = false;
        return tag3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Tag m22159(String tagName) {
        return m22160(tagName, ParseSettings.f17376);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m22167() {
        return this.f17400;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m22169() {
        return this.f17399;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m22168() {
        return this.f17392;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22166() {
        return this.f17392 || this.f17393;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m22162() {
        return f17389.containsKey(this.f17398);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m22163() {
        return this.f17394;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m22164() {
        return this.f17395;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public Tag m22165() {
        this.f17393 = true;
        return this;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tag)) {
            return false;
        }
        Tag tag = (Tag) o;
        if (!this.f17398.equals(tag.f17398) || this.f17397 != tag.f17397 || this.f17392 != tag.f17392 || this.f17399 != tag.f17399 || this.f17400 != tag.f17400 || this.f17394 != tag.f17394 || this.f17393 != tag.f17393 || this.f17395 != tag.f17395) {
            return false;
        }
        if (this.f17396 != tag.f17396) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 1;
        int hashCode = ((this.f17398.hashCode() * 31) + (this.f17400 ? 1 : 0)) * 31;
        if (this.f17399) {
            i = 1;
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 31;
        if (this.f17397) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 31;
        if (this.f17392) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 31;
        if (this.f17393) {
            i4 = 1;
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 31;
        if (this.f17394) {
            i5 = 1;
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 31;
        if (this.f17395) {
            i6 = 1;
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 31;
        if (!this.f17396) {
            i7 = 0;
        }
        return i13 + i7;
    }

    public String toString() {
        return this.f17398;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m22161(Tag tag) {
        f17389.put(tag.f17398, tag);
    }
}
