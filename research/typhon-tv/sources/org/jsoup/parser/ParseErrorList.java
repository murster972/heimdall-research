package org.jsoup.parser;

import java.util.ArrayList;

public class ParseErrorList extends ArrayList<ParseError> {
    private final int maxSize;

    ParseErrorList(int initialCapacity, int maxSize2) {
        super(initialCapacity);
        this.maxSize = maxSize2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22149() {
        return size() < this.maxSize;
    }

    public static ParseErrorList noTracking() {
        return new ParseErrorList(0, 0);
    }

    public static ParseErrorList tracking(int maxSize2) {
        return new ParseErrorList(16, maxSize2);
    }
}
