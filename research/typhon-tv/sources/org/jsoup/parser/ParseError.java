package org.jsoup.parser;

public class ParseError {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f17374;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f17375;

    ParseError(int pos, String errorMsg) {
        this.f17375 = pos;
        this.f17374 = errorMsg;
    }

    ParseError(int pos, String errorFormat, Object... args) {
        this.f17374 = String.format(errorFormat, args);
        this.f17375 = pos;
    }

    public String toString() {
        return this.f17375 + ": " + this.f17374;
    }
}
