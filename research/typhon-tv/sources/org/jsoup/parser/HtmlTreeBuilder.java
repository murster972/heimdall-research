package org.jsoup.parser;

import com.mopub.common.AdType;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.pubnative.library.request.PubnativeAsset;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.CDataNode;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Token;
import org.jsoup.select.Elements;

public class HtmlTreeBuilder extends TreeBuilder {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final String[] f17310 = {"dd", "dt", "li", "optgroup", "option", TtmlNode.TAG_P, "rp", "rt"};

    /* renamed from: ʼ  reason: contains not printable characters */
    static final String[] f17311 = {"address", "applet", "area", "article", "aside", "base", "basefont", "bgsound", "blockquote", TtmlNode.TAG_BODY, TtmlNode.TAG_BR, "button", "caption", TtmlNode.CENTER, "col", "colgroup", "command", "dd", "details", "dir", TtmlNode.TAG_DIV, "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", TtmlNode.TAG_HEAD, "header", "hgroup", "hr", AdType.HTML, "iframe", "img", "input", "isindex", "li", "link", "listing", "marquee", "menu", "meta", "nav", "noembed", "noframes", "noscript", "object", "ol", TtmlNode.TAG_P, "param", "plaintext", "pre", "script", "section", "select", TtmlNode.TAG_STYLE, "summary", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", PubnativeAsset.TITLE, "tr", "ul", "wbr", "xmp"};

    /* renamed from: ʽ  reason: contains not printable characters */
    static final /* synthetic */ boolean f17312 = (!HtmlTreeBuilder.class.desiredAssertionStatus());

    /* renamed from: 连任  reason: contains not printable characters */
    static final String[] f17313 = {"optgroup", "option"};

    /* renamed from: 靐  reason: contains not printable characters */
    static final String[] f17314 = {"ol", "ul"};

    /* renamed from: 麤  reason: contains not printable characters */
    static final String[] f17315 = {AdType.HTML, "table"};

    /* renamed from: 齉  reason: contains not printable characters */
    static final String[] f17316 = {"button"};

    /* renamed from: 龘  reason: contains not printable characters */
    static final String[] f17317 = {"applet", "caption", AdType.HTML, "marquee", "object", "table", "td", "th"};

    /* renamed from: ˆ  reason: contains not printable characters */
    private Element f17318;

    /* renamed from: ˉ  reason: contains not printable characters */
    private FormElement f17319;

    /* renamed from: ˊ  reason: contains not printable characters */
    private HtmlTreeBuilderState f17320;

    /* renamed from: ˋ  reason: contains not printable characters */
    private HtmlTreeBuilderState f17321;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f17322;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Element f17323;

    /* renamed from: י  reason: contains not printable characters */
    private ArrayList<Element> f17324;

    /* renamed from: ـ  reason: contains not printable characters */
    private List<String> f17325;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private Token.EndTag f17326;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f17327;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f17328;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f17329;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private String[] f17330 = {null};

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m22096(String str, Attributes attributes) {
        return super.m22346(str, attributes);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ParseSettings m22085() {
        return ParseSettings.f17377;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22086(Reader input, String baseUri, ParseErrorList errors, ParseSettings settings) {
        super.m22345(input, baseUri, errors, settings);
        this.f17320 = HtmlTreeBuilderState.Initial;
        this.f17321 = null;
        this.f17322 = false;
        this.f17318 = null;
        this.f17319 = null;
        this.f17323 = null;
        this.f17324 = new ArrayList<>();
        this.f17325 = new ArrayList();
        this.f17326 = new Token.EndTag();
        this.f17327 = true;
        this.f17328 = false;
        this.f17329 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<Node> m22081(String inputFragment, Element context, String baseUri, ParseErrorList errors, ParseSettings settings) {
        this.f17320 = HtmlTreeBuilderState.Initial;
        m22086(new StringReader(inputFragment), baseUri, errors, settings);
        this.f17323 = context;
        this.f17329 = true;
        Element root = null;
        if (context != null) {
            if (context.m21925() != null) {
                this.f17519.m21776(context.m21925().m21769());
            }
            String contextTag = context.m21869();
            if (StringUtil.m21690(contextTag, PubnativeAsset.TITLE, "textarea")) {
                this.f17518.m22256(TokeniserState.Rcdata);
            } else {
                if (StringUtil.m21690(contextTag, "iframe", "noembed", "noframes", TtmlNode.TAG_STYLE, "xmp")) {
                    this.f17518.m22256(TokeniserState.Rawtext);
                } else if (contextTag.equals("script")) {
                    this.f17518.m22256(TokeniserState.ScriptData);
                } else if (contextTag.equals("noscript")) {
                    this.f17518.m22256(TokeniserState.Data);
                } else if (contextTag.equals("plaintext")) {
                    this.f17518.m22256(TokeniserState.Data);
                } else {
                    this.f17518.m22256(TokeniserState.Data);
                }
            }
            root = new Element(Tag.m22160(AdType.HTML, settings), baseUri);
            this.f17519.m21864((Node) root);
            this.f17516.add(root);
            m22043();
            Elements contextChain = context.m21824();
            contextChain.add(0, context);
            Iterator it2 = contextChain.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Element parent = (Element) it2.next();
                if (parent instanceof FormElement) {
                    this.f17319 = (FormElement) parent;
                    break;
                }
            }
        }
        m22341();
        if (context != null) {
            return root.m21921();
        }
        return this.f17519.m21921();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22098(Token token) {
        this.f17515 = token;
        return this.f17320.m22111(token, this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22099(Token token, HtmlTreeBuilderState state) {
        this.f17515 = token;
        return state.m22111(token, this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22091(HtmlTreeBuilderState state) {
        this.f17320 = state;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public HtmlTreeBuilderState m22068() {
        return this.f17320;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m22077() {
        this.f17321 = this.f17320;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public HtmlTreeBuilderState m22074() {
        return this.f17321;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22094(boolean framesetOk) {
        this.f17327 = framesetOk;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22063() {
        return this.f17327;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Document m22033() {
        return this.f17519;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public String m22036() {
        return this.f17514;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22087(Element base) {
        if (!this.f17322) {
            String href = base.m21953("href");
            if (href.length() != 0) {
                this.f17514 = href;
                this.f17322 = true;
                this.f17519.m21939(href);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m22039() {
        return this.f17329;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22071(HtmlTreeBuilderState state) {
        if (this.f17522.m22149()) {
            this.f17522.add(new ParseError(this.f17517.m22019(), "Unexpected token [%s] when in state [%s]", this.f17515.m22185(), state));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Element m22083(Token.StartTag startTag) {
        if (startTag.m22202()) {
            Element el = m22067(startTag);
            this.f17516.add(el);
            this.f17518.m22256(TokeniserState.Data);
            this.f17518.m22255((Token) this.f17326.m22208().m22214(el.m21869()));
            Element element = el;
            return el;
        }
        Element el2 = new Element(Tag.m22160(startTag.m22205(), this.f17523), this.f17514, this.f17523.m22151(startTag.f17415));
        m22069(el2);
        Element element2 = el2;
        return el2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Element m22082(String startTagName) {
        Element el = new Element(Tag.m22160(startTagName, this.f17523), this.f17514);
        m22069(el);
        return el;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22069(Element el) {
        m22026((Node) el);
        this.f17516.add(el);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Element m22067(Token.StartTag startTag) {
        Tag tag = Tag.m22160(startTag.m22205(), this.f17523);
        Element el = new Element(tag, this.f17514, startTag.f17415);
        m22026((Node) el);
        if (startTag.m22202()) {
            if (!tag.m22162()) {
                tag.m22165();
            } else if (!tag.m22168()) {
                this.f17518.m22245("Tag cannot be self closing; not a void tag");
            }
        }
        return el;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public FormElement m22084(Token.StartTag startTag, boolean onStack) {
        FormElement el = new FormElement(Tag.m22160(startTag.m22205(), this.f17523), this.f17514, startTag.f17415);
        m22089(el);
        m22026((Node) el);
        if (onStack) {
            this.f17516.add(el);
        }
        return el;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22093(Token.Comment commentToken) {
        m22026((Node) new Comment(commentToken.m22190()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22092(Token.Character characterToken) {
        Node node;
        String tagName = m22342().m21869();
        String data = characterToken.m22188();
        if (characterToken.m22177()) {
            node = new CDataNode(data);
        } else if (tagName.equals("script") || tagName.equals(TtmlNode.TAG_STYLE)) {
            node = new DataNode(data);
        } else {
            node = new TextNode(data);
        }
        m22342().m21864(node);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m22026(Node node) {
        if (this.f17516.size() == 0) {
            this.f17519.m21864(node);
        } else if (m22101()) {
            m22090(node);
        } else {
            m22342().m21864(node);
        }
        if ((node instanceof Element) && ((Element) node).m21871().m22164() && this.f17319 != null) {
            this.f17319.m21896((Element) node);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public Element m22051() {
        return (Element) this.f17516.remove(this.f17516.size() - 1);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m22079(Element element) {
        this.f17516.add(element);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public ArrayList<Element> m22056() {
        return this.f17516;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m22076(Element el) {
        return m22031((ArrayList<Element>) this.f17516, el);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m22031(ArrayList<Element> queue, Element element) {
        for (int pos = queue.size() - 1; pos >= 0; pos--) {
            if (queue.get(pos) == element) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Element m22066(String elName) {
        for (int pos = this.f17516.size() - 1; pos >= 0; pos--) {
            Element next = (Element) this.f17516.get(pos);
            if (next.m21861().equals(elName)) {
                return next;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22065(Element el) {
        for (int pos = this.f17516.size() - 1; pos >= 0; pos--) {
            if (((Element) this.f17516.get(pos)) == el) {
                this.f17516.remove(pos);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m22078(String elName) {
        int pos = this.f17516.size() - 1;
        while (pos >= 0) {
            this.f17516.remove(pos);
            if (!((Element) this.f17516.get(pos)).m21861().equals(elName)) {
                pos--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22095(String... elNames) {
        int pos = this.f17516.size() - 1;
        while (pos >= 0) {
            this.f17516.remove(pos);
            if (!StringUtil.m21678(((Element) this.f17516.get(pos)).m21861(), elNames)) {
                pos--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m22075(String elName) {
        int pos = this.f17516.size() - 1;
        while (pos >= 0 && !((Element) this.f17516.get(pos)).m21861().equals(elName)) {
            this.f17516.remove(pos);
            pos--;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m22060() {
        m22028("table");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m22045() {
        m22028("tbody", "tfoot", "thead", "template");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m22042() {
        m22028("tr", "template");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m22028(String... nodeNames) {
        int pos = this.f17516.size() - 1;
        while (pos >= 0) {
            Element next = (Element) this.f17516.get(pos);
            if (!StringUtil.m21690(next.m21861(), nodeNames) && !next.m21861().equals(AdType.HTML)) {
                this.f17516.remove(pos);
                pos--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Element m22034(Element el) {
        if (f17312 || m22076(el)) {
            for (int pos = this.f17516.size() - 1; pos >= 0; pos--) {
                if (((Element) this.f17516.get(pos)) == el) {
                    return (Element) this.f17516.get(pos - 1);
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22088(Element after, Element in) {
        int i = this.f17516.lastIndexOf(after);
        Validate.m21699(i != -1);
        this.f17516.add(i + 1, in);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22070(Element out, Element in) {
        m22029((ArrayList<Element>) this.f17516, out, in);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22029(ArrayList<Element> queue, Element out, Element in) {
        int i = queue.lastIndexOf(out);
        Validate.m21699(i != -1);
        queue.set(i, in);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m22043() {
        boolean last = false;
        int pos = this.f17516.size() - 1;
        while (pos >= 0) {
            Element node = (Element) this.f17516.get(pos);
            if (pos == 0) {
                last = true;
                node = this.f17323;
            }
            String name = node.m21861();
            if ("select".equals(name)) {
                m22091(HtmlTreeBuilderState.InSelect);
                return;
            } else if ("td".equals(name) || ("th".equals(name) && !last)) {
                m22091(HtmlTreeBuilderState.InCell);
                return;
            } else if ("tr".equals(name)) {
                m22091(HtmlTreeBuilderState.InRow);
                return;
            } else if ("tbody".equals(name) || "thead".equals(name) || "tfoot".equals(name)) {
                m22091(HtmlTreeBuilderState.InTableBody);
                return;
            } else if ("caption".equals(name)) {
                m22091(HtmlTreeBuilderState.InCaption);
                return;
            } else if ("colgroup".equals(name)) {
                m22091(HtmlTreeBuilderState.InColumnGroup);
                return;
            } else if ("table".equals(name)) {
                m22091(HtmlTreeBuilderState.InTable);
                return;
            } else if (TtmlNode.TAG_HEAD.equals(name)) {
                m22091(HtmlTreeBuilderState.InBody);
                return;
            } else if (TtmlNode.TAG_BODY.equals(name)) {
                m22091(HtmlTreeBuilderState.InBody);
                return;
            } else if ("frameset".equals(name)) {
                m22091(HtmlTreeBuilderState.InFrameset);
                return;
            } else if (AdType.HTML.equals(name)) {
                m22091(HtmlTreeBuilderState.BeforeHead);
                return;
            } else if (last) {
                m22091(HtmlTreeBuilderState.InBody);
                return;
            } else {
                pos--;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m22030(String targetName, String[] baseTypes, String[] extraTypes) {
        this.f17330[0] = targetName;
        return m22032(this.f17330, baseTypes, extraTypes);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m22032(String[] targetNames, String[] baseTypes, String[] extraTypes) {
        int top;
        int bottom = this.f17516.size() - 1;
        if (bottom > 100) {
            top = bottom - 100;
        } else {
            top = 0;
        }
        for (int pos = bottom; pos >= top; pos--) {
            String elName = ((Element) this.f17516.get(pos)).m21861();
            if (StringUtil.m21678(elName, targetNames)) {
                return true;
            }
            if (StringUtil.m21678(elName, baseTypes)) {
                return false;
            }
            if (extraTypes != null && StringUtil.m21678(elName, extraTypes)) {
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m22073(String[] targetNames) {
        return m22032(targetNames, f17317, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22064(String targetName) {
        return m22097(targetName, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22097(String targetName, String[] extras) {
        return m22030(targetName, f17317, extras);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m22035(String targetName) {
        return m22097(targetName, f17314);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m22038(String targetName) {
        return m22097(targetName, f17316);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m22040(String targetName) {
        return m22030(targetName, f17315, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m22053(String targetName) {
        for (int pos = this.f17516.size() - 1; pos >= 0; pos--) {
            String elName = ((Element) this.f17516.get(pos)).m21861();
            if (elName.equals(targetName)) {
                return true;
            }
            if (!StringUtil.m21678(elName, f17313)) {
                return false;
            }
        }
        Validate.m21692("Should not be reachable");
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m22037(Element headElement) {
        this.f17318 = headElement;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public Element m22100() {
        return this.f17318;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m22101() {
        return this.f17328;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22072(boolean fosterInserts) {
        this.f17328 = fosterInserts;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public FormElement m22047() {
        return this.f17319;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22089(FormElement formElement) {
        this.f17319 = formElement;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m22048() {
        this.f17325 = new ArrayList();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public List<String> m22049() {
        return this.f17325;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m22057(String excludeTag) {
        while (excludeTag != null && !m22342().m21861().equals(excludeTag) && StringUtil.m21678(m22342().m21861(), f17310)) {
            m22051();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m22044() {
        m22057((String) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m22041(Element el) {
        return StringUtil.m21678(el.m21861(), f17311);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public Element m22046() {
        if (this.f17324.size() > 0) {
            return this.f17324.get(this.f17324.size() - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public Element m22050() {
        int size = this.f17324.size();
        if (size > 0) {
            return this.f17324.remove(size - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m22052(Element in) {
        int numSeen = 0;
        int pos = this.f17324.size() - 1;
        while (true) {
            if (pos >= 0) {
                Element el = this.f17324.get(pos);
                if (el == null) {
                    break;
                }
                if (m22027(in, el)) {
                    numSeen++;
                }
                if (numSeen == 3) {
                    this.f17324.remove(pos);
                    break;
                }
                pos--;
            } else {
                break;
            }
        }
        this.f17324.add(in);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m22027(Element a, Element b) {
        return a.m21861().equals(b.m21861()) && a.m21820().equals(b.m21820());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: י  reason: contains not printable characters */
    public void m22054() {
        Element last = m22046();
        if (last != null && !m22076(last)) {
            Element entry = last;
            int size = this.f17324.size();
            int pos = size - 1;
            boolean skip = false;
            while (true) {
                if (pos == 0) {
                    skip = true;
                    break;
                }
                pos--;
                entry = this.f17324.get(pos);
                if (entry != null) {
                    if (m22076(entry)) {
                        break;
                    }
                } else {
                    break;
                }
            }
            do {
                if (!skip) {
                    pos++;
                    entry = this.f17324.get(pos);
                }
                Validate.m21695((Object) entry);
                skip = false;
                Element newEl = m22082(entry.m21861());
                newEl.m21820().m21738(entry.m21820());
                this.f17324.set(pos, newEl);
            } while (pos != size - 1);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:3:0x000c, LOOP_START, MTH_ENTER_BLOCK] */
    /* renamed from: ـ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m22055() {
        /*
            r2 = this;
        L_0x0000:
            java.util.ArrayList<org.jsoup.nodes.Element> r1 = r2.f17324
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x000e
            org.jsoup.nodes.Element r0 = r2.m22050()
            if (r0 != 0) goto L_0x0000
        L_0x000e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilder.m22055():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m22058(Element el) {
        for (int pos = this.f17324.size() - 1; pos >= 0; pos--) {
            if (this.f17324.get(pos) == el) {
                this.f17324.remove(pos);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m22061(Element el) {
        return m22031(this.f17324, el);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public Element m22059(String nodeName) {
        int pos = this.f17324.size() - 1;
        while (pos >= 0) {
            Element next = this.f17324.get(pos);
            if (next == null) {
                break;
            } else if (next.m21861().equals(nodeName)) {
                return next;
            } else {
                pos--;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m22080(Element out, Element in) {
        m22029(this.f17324, out, in);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m22062() {
        this.f17324.add((Object) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22090(Node in) {
        Element fosterParent;
        Element lastTable = m22066("table");
        boolean isLastTableParent = false;
        if (lastTable == null) {
            fosterParent = (Element) this.f17516.get(0);
        } else if (lastTable.m21819() != null) {
            fosterParent = lastTable.m21819();
            isLastTableParent = true;
        } else {
            fosterParent = m22034(lastTable);
        }
        if (isLastTableParent) {
            Validate.m21695((Object) lastTable);
            lastTable.m21810(in);
            return;
        }
        fosterParent.m21864(in);
    }

    public String toString() {
        return "TreeBuilder{currentToken=" + this.f17515 + ", state=" + this.f17320 + ", currentElement=" + m22342() + '}';
    }
}
