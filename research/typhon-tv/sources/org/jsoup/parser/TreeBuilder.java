package org.jsoup.parser;

import java.io.Reader;
import java.util.ArrayList;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Token;

abstract class TreeBuilder {

    /* renamed from: ʾ  reason: contains not printable characters */
    protected String f17514;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected Token f17515;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected ArrayList<Element> f17516;

    /* renamed from: ˑ  reason: contains not printable characters */
    CharacterReader f17517;

    /* renamed from: ٴ  reason: contains not printable characters */
    Tokeniser f17518;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected Document f17519;

    /* renamed from: 靐  reason: contains not printable characters */
    private Token.EndTag f17520 = new Token.EndTag();

    /* renamed from: 龘  reason: contains not printable characters */
    private Token.StartTag f17521 = new Token.StartTag();

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected ParseErrorList f17522;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected ParseSettings f17523;

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ParseSettings m22344();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m22347(Token token);

    TreeBuilder() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22345(Reader input, String baseUri, ParseErrorList errors, ParseSettings settings) {
        Validate.m21696((Object) input, "String input must not be null");
        Validate.m21696((Object) baseUri, "BaseURI must not be null");
        this.f17519 = new Document(baseUri);
        this.f17523 = settings;
        this.f17517 = new CharacterReader(input);
        this.f17522 = errors;
        this.f17515 = null;
        this.f17518 = new Tokeniser(this.f17517, errors);
        this.f17516 = new ArrayList<>(32);
        this.f17514 = baseUri;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Document m22343(Reader input, String baseUri, ParseErrorList errors, ParseSettings settings) {
        m22345(input, baseUri, errors, settings);
        m22341();
        return this.f17519;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public void m22341() {
        Token token;
        do {
            token = this.f17518.m22252();
            m22347(token);
            token.m22182();
        } while (token.f17401 != Token.TokenType.EOF);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m22340(String name) {
        if (this.f17515 == this.f17521) {
            return m22347(new Token.StartTag().m22214(name));
        }
        return m22347(this.f17521.m22198().m22214(name));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22346(String name, Attributes attrs) {
        if (this.f17515 == this.f17521) {
            return m22347(new Token.StartTag().m22199(name, attrs));
        }
        this.f17521.m22198();
        this.f17521.m22199(name, attrs);
        return m22347(this.f17521);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m22339(String name) {
        if (this.f17515 == this.f17520) {
            return m22347(new Token.EndTag().m22214(name));
        }
        return m22347(this.f17520.m22208().m22214(name));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public Element m22342() {
        int size = this.f17516.size();
        if (size > 0) {
            return this.f17516.get(size - 1);
        }
        return null;
    }
}
