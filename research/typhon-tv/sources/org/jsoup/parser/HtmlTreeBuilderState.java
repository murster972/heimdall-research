package org.jsoup.parser;

import com.mopub.common.AdType;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.ArrayList;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.oltu.oauth2.common.OAuth;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Token;

enum HtmlTreeBuilderState {
    Initial {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22112(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.m22104(t)) {
                return true;
            }
            if (t.m22178()) {
                tb.m22093(t.m22179());
                return true;
            } else if (t.m22184()) {
                Token.Doctype d = t.m22183();
                DocumentType doctype = new DocumentType(tb.f17523.m22150(d.m22195()), d.m22191(), d.m22192());
                doctype.m21789(d.m22196());
                tb.m22033().m21864((Node) doctype);
                if (d.m22193()) {
                    tb.m22033().m21776(Document.QuirksMode.quirks);
                }
                tb.m22091(BeforeHtml);
                return true;
            } else {
                tb.m22091(BeforeHtml);
                return tb.m22098(t);
            }
        }
    },
    BeforeHtml {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22132(Token t, HtmlTreeBuilder tb) {
            if (t.m22184()) {
                tb.m22071((HtmlTreeBuilderState) this);
                return false;
            }
            if (t.m22178()) {
                tb.m22093(t.m22179());
            } else if (HtmlTreeBuilderState.m22104(t)) {
                return true;
            } else {
                if (!t.m22181() || !t.m22172().m22206().equals(AdType.HTML)) {
                    if (t.m22173()) {
                        if (StringUtil.m21690(t.m22174().m22206(), TtmlNode.TAG_HEAD, TtmlNode.TAG_BODY, AdType.HTML, TtmlNode.TAG_BR)) {
                            return m22131(t, tb);
                        }
                    }
                    if (!t.m22173()) {
                        return m22131(t, tb);
                    }
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                }
                tb.m22083(t.m22172());
                tb.m22091(BeforeHead);
            }
            return true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m22131(Token t, HtmlTreeBuilder tb) {
            tb.m22082(AdType.HTML);
            tb.m22091(BeforeHead);
            return tb.m22098(t);
        }
    },
    BeforeHead {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22137(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.m22104(t)) {
                return true;
            }
            if (t.m22178()) {
                tb.m22093(t.m22179());
                return true;
            } else if (t.m22184()) {
                tb.m22071((HtmlTreeBuilderState) this);
                return false;
            } else if (t.m22181() && t.m22172().m22206().equals(AdType.HTML)) {
                return InBody.m22111(t, tb);
            } else {
                if (!t.m22181() || !t.m22172().m22206().equals(TtmlNode.TAG_HEAD)) {
                    if (t.m22173()) {
                        if (StringUtil.m21690(t.m22174().m22206(), TtmlNode.TAG_HEAD, TtmlNode.TAG_BODY, AdType.HTML, TtmlNode.TAG_BR)) {
                            tb.m22340(TtmlNode.TAG_HEAD);
                            return tb.m22098(t);
                        }
                    }
                    if (t.m22173()) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                    tb.m22340(TtmlNode.TAG_HEAD);
                    return tb.m22098(t);
                }
                tb.m22037(tb.m22083(t.m22172()));
                tb.m22091(InHead);
                return true;
            }
        }
    },
    InHead {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22139(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.m22104(t)) {
                tb.m22092(t.m22175());
                return true;
            }
            switch (t.f17401) {
                case Comment:
                    tb.m22093(t.m22179());
                    return true;
                case Doctype:
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                case StartTag:
                    Token.StartTag start = t.m22172();
                    String name = start.m22206();
                    if (name.equals(AdType.HTML)) {
                        return InBody.m22111(t, tb);
                    }
                    if (StringUtil.m21690(name, "base", "basefont", "bgsound", "command", "link")) {
                        Element el = tb.m22067(start);
                        if (!name.equals("base") || !el.m21951("href")) {
                            return true;
                        }
                        tb.m22087(el);
                        return true;
                    } else if (name.equals("meta")) {
                        tb.m22067(start);
                        return true;
                    } else if (name.equals(PubnativeAsset.TITLE)) {
                        HtmlTreeBuilderState.m22106(start, tb);
                        return true;
                    } else {
                        if (StringUtil.m21690(name, "noframes", TtmlNode.TAG_STYLE)) {
                            HtmlTreeBuilderState.m22105(start, tb);
                            return true;
                        } else if (name.equals("noscript")) {
                            tb.m22083(start);
                            tb.m22091(InHeadNoscript);
                            return true;
                        } else if (name.equals("script")) {
                            tb.f17518.m22256(TokeniserState.ScriptData);
                            tb.m22077();
                            tb.m22091(Text);
                            tb.m22083(start);
                            return true;
                        } else if (!name.equals(TtmlNode.TAG_HEAD)) {
                            return m22138(t, (TreeBuilder) tb);
                        } else {
                            tb.m22071((HtmlTreeBuilderState) this);
                            return false;
                        }
                    }
                case EndTag:
                    String name2 = t.m22174().m22206();
                    if (name2.equals(TtmlNode.TAG_HEAD)) {
                        tb.m22051();
                        tb.m22091(AfterHead);
                        return true;
                    }
                    if (StringUtil.m21690(name2, TtmlNode.TAG_BODY, AdType.HTML, TtmlNode.TAG_BR)) {
                        return m22138(t, (TreeBuilder) tb);
                    }
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                default:
                    return m22138(t, (TreeBuilder) tb);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m22138(Token t, TreeBuilder tb) {
            tb.m22339(TtmlNode.TAG_HEAD);
            return tb.m22347(t);
        }
    },
    InHeadNoscript {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x008e, code lost:
            if (org.jsoup.helper.StringUtil.m21690(r8.m22172().m22206(), "basefont", "bgsound", "link", "meta", "noframes", com.google.android.exoplayer2.text.ttml.TtmlNode.TAG_STYLE) != false) goto L_0x0090;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d3, code lost:
            if (org.jsoup.helper.StringUtil.m21690(r8.m22172().m22206(), com.google.android.exoplayer2.text.ttml.TtmlNode.TAG_HEAD, "noscript") == false) goto L_0x00d5;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m22141(org.jsoup.parser.Token r8, org.jsoup.parser.HtmlTreeBuilder r9) {
            /*
                r7 = this;
                r6 = 2
                r1 = 1
                r0 = 0
                boolean r2 = r8.m22184()
                if (r2 == 0) goto L_0x000e
                r9.m22071((org.jsoup.parser.HtmlTreeBuilderState) r7)
            L_0x000c:
                r0 = r1
            L_0x000d:
                return r0
            L_0x000e:
                boolean r2 = r8.m22181()
                if (r2 == 0) goto L_0x002c
                org.jsoup.parser.Token$StartTag r2 = r8.m22172()
                java.lang.String r2 = r2.m22206()
                java.lang.String r3 = "html"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x002c
                org.jsoup.parser.HtmlTreeBuilderState r0 = InBody
                boolean r0 = r9.m22099((org.jsoup.parser.Token) r8, (org.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x002c:
                boolean r2 = r8.m22173()
                if (r2 == 0) goto L_0x004c
                org.jsoup.parser.Token$EndTag r2 = r8.m22174()
                java.lang.String r2 = r2.m22206()
                java.lang.String r3 = "noscript"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x004c
                r9.m22051()
                org.jsoup.parser.HtmlTreeBuilderState r0 = InHead
                r9.m22091((org.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000c
            L_0x004c:
                boolean r2 = org.jsoup.parser.HtmlTreeBuilderState.m22104((org.jsoup.parser.Token) r8)
                if (r2 != 0) goto L_0x0090
                boolean r2 = r8.m22178()
                if (r2 != 0) goto L_0x0090
                boolean r2 = r8.m22181()
                if (r2 == 0) goto L_0x0098
                org.jsoup.parser.Token$StartTag r2 = r8.m22172()
                java.lang.String r2 = r2.m22206()
                r3 = 6
                java.lang.String[] r3 = new java.lang.String[r3]
                java.lang.String r4 = "basefont"
                r3[r0] = r4
                java.lang.String r4 = "bgsound"
                r3[r1] = r4
                java.lang.String r4 = "link"
                r3[r6] = r4
                r4 = 3
                java.lang.String r5 = "meta"
                r3[r4] = r5
                r4 = 4
                java.lang.String r5 = "noframes"
                r3[r4] = r5
                r4 = 5
                java.lang.String r5 = "style"
                r3[r4] = r5
                boolean r2 = org.jsoup.helper.StringUtil.m21690((java.lang.String) r2, (java.lang.String[]) r3)
                if (r2 == 0) goto L_0x0098
            L_0x0090:
                org.jsoup.parser.HtmlTreeBuilderState r0 = InHead
                boolean r0 = r9.m22099((org.jsoup.parser.Token) r8, (org.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x0098:
                boolean r2 = r8.m22173()
                if (r2 == 0) goto L_0x00b5
                org.jsoup.parser.Token$EndTag r2 = r8.m22174()
                java.lang.String r2 = r2.m22206()
                java.lang.String r3 = "br"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x00b5
                boolean r0 = r7.m22140(r8, r9)
                goto L_0x000d
            L_0x00b5:
                boolean r2 = r8.m22181()
                if (r2 == 0) goto L_0x00d5
                org.jsoup.parser.Token$StartTag r2 = r8.m22172()
                java.lang.String r2 = r2.m22206()
                java.lang.String[] r3 = new java.lang.String[r6]
                java.lang.String r4 = "head"
                r3[r0] = r4
                java.lang.String r4 = "noscript"
                r3[r1] = r4
                boolean r1 = org.jsoup.helper.StringUtil.m21690((java.lang.String) r2, (java.lang.String[]) r3)
                if (r1 != 0) goto L_0x00db
            L_0x00d5:
                boolean r1 = r8.m22173()
                if (r1 == 0) goto L_0x00e0
            L_0x00db:
                r9.m22071((org.jsoup.parser.HtmlTreeBuilderState) r7)
                goto L_0x000d
            L_0x00e0:
                boolean r0 = r7.m22140(r8, r9)
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass5.m22141(org.jsoup.parser.Token, org.jsoup.parser.HtmlTreeBuilder):boolean");
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m22140(Token t, HtmlTreeBuilder tb) {
            tb.m22071((HtmlTreeBuilderState) this);
            tb.m22092(new Token.Character().m22187(t.toString()));
            return true;
        }
    },
    AfterHead {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22143(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.m22104(t)) {
                tb.m22092(t.m22175());
            } else if (t.m22178()) {
                tb.m22093(t.m22179());
            } else if (t.m22184()) {
                tb.m22071((HtmlTreeBuilderState) this);
            } else if (t.m22181()) {
                Token.StartTag startTag = t.m22172();
                String name = startTag.m22206();
                if (name.equals(AdType.HTML)) {
                    return tb.m22099(t, InBody);
                }
                if (name.equals(TtmlNode.TAG_BODY)) {
                    tb.m22083(startTag);
                    tb.m22094(false);
                    tb.m22091(InBody);
                } else if (name.equals("frameset")) {
                    tb.m22083(startTag);
                    tb.m22091(InFrameset);
                } else {
                    if (StringUtil.m21690(name, "base", "basefont", "bgsound", "link", "meta", "noframes", "script", TtmlNode.TAG_STYLE, PubnativeAsset.TITLE)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        Element head = tb.m22100();
                        tb.m22079(head);
                        tb.m22099(t, InHead);
                        tb.m22065(head);
                    } else if (name.equals(TtmlNode.TAG_HEAD)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    } else {
                        m22142(t, tb);
                    }
                }
            } else if (t.m22173()) {
                if (StringUtil.m21690(t.m22174().m22206(), TtmlNode.TAG_BODY, AdType.HTML)) {
                    m22142(t, tb);
                } else {
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                }
            } else {
                m22142(t, tb);
            }
            return true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m22142(Token t, HtmlTreeBuilder tb) {
            tb.m22340(TtmlNode.TAG_BODY);
            tb.m22094(true);
            return tb.m22098(t);
        }
    },
    InBody {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:305:0x0a74  */
        /* JADX WARNING: Removed duplicated region for block: B:312:0x0abf A[LOOP:9: B:310:0x0ab9->B:312:0x0abf, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:319:0x0b0e  */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m22145(org.jsoup.parser.Token r40, org.jsoup.parser.HtmlTreeBuilder r41) {
            /*
                r39 = this;
                int[] r36 = org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass24.f17356
                r0 = r40
                org.jsoup.parser.Token$TokenType r0 = r0.f17401
                r37 = r0
                int r37 = r37.ordinal()
                r36 = r36[r37]
                switch(r36) {
                    case 1: goto L_0x0057;
                    case 2: goto L_0x0063;
                    case 3: goto L_0x006d;
                    case 4: goto L_0x095a;
                    case 5: goto L_0x0014;
                    default: goto L_0x0011;
                }
            L_0x0011:
                r36 = 1
            L_0x0013:
                return r36
            L_0x0014:
                org.jsoup.parser.Token$Character r8 = r40.m22175()
                java.lang.String r36 = r8.m22188()
                java.lang.String r37 = org.jsoup.parser.HtmlTreeBuilderState.f17345
                boolean r36 = r36.equals(r37)
                if (r36 == 0) goto L_0x0030
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0030:
                boolean r36 = r41.m22063()
                if (r36 == 0) goto L_0x0045
                boolean r36 = org.jsoup.parser.HtmlTreeBuilderState.m22104((org.jsoup.parser.Token) r8)
                if (r36 == 0) goto L_0x0045
                r41.m22054()
                r0 = r41
                r0.m22092((org.jsoup.parser.Token.Character) r8)
                goto L_0x0011
            L_0x0045:
                r41.m22054()
                r0 = r41
                r0.m22092((org.jsoup.parser.Token.Character) r8)
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                goto L_0x0011
            L_0x0057:
                org.jsoup.parser.Token$Comment r36 = r40.m22179()
                r0 = r41
                r1 = r36
                r0.m22093((org.jsoup.parser.Token.Comment) r1)
                goto L_0x0011
            L_0x0063:
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x006d:
                org.jsoup.parser.Token$StartTag r34 = r40.m22172()
                java.lang.String r23 = r34.m22206()
                java.lang.String r36 = "a"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x00cd
                java.lang.String r36 = "a"
                r0 = r41
                r1 = r36
                org.jsoup.nodes.Element r36 = r0.m22059((java.lang.String) r1)
                if (r36 == 0) goto L_0x00bb
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.lang.String r36 = "a"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
                java.lang.String r36 = "a"
                r0 = r41
                r1 = r36
                org.jsoup.nodes.Element r27 = r0.m22066((java.lang.String) r1)
                if (r27 == 0) goto L_0x00bb
                r0 = r41
                r1 = r27
                r0.m22058((org.jsoup.nodes.Element) r1)
                r0 = r41
                r1 = r27
                r0.m22065((org.jsoup.nodes.Element) r1)
            L_0x00bb:
                r41.m22054()
                r0 = r41
                r1 = r34
                org.jsoup.nodes.Element r3 = r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r0 = r41
                r0.m22052((org.jsoup.nodes.Element) r3)
                goto L_0x0011
            L_0x00cd:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17364
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x00ee
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22067((org.jsoup.parser.Token.StartTag) r1)
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                goto L_0x0011
            L_0x00ee:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17368
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x011a
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x0111
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x0111:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x011a:
                java.lang.String r36 = "span"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0133
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x0133:
                java.lang.String r36 = "li"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x01ad
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                java.util.ArrayList r32 = r41.m22056()
                int r36 = r32.size()
                int r19 = r36 + -1
            L_0x0153:
                if (r19 <= 0) goto L_0x0176
                r0 = r32
                r1 = r19
                java.lang.Object r13 = r0.get(r1)
                org.jsoup.nodes.Element r13 = (org.jsoup.nodes.Element) r13
                java.lang.String r36 = r13.m21861()
                java.lang.String r37 = "li"
                boolean r36 = r36.equals(r37)
                if (r36 == 0) goto L_0x0196
                java.lang.String r36 = "li"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x0176:
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x018d
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x018d:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x0196:
                r0 = r41
                boolean r36 = r0.m22041((org.jsoup.nodes.Element) r13)
                if (r36 == 0) goto L_0x01aa
                java.lang.String r36 = r13.m21861()
                java.lang.String[] r37 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17367
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r36, r37)
                if (r36 == 0) goto L_0x0176
            L_0x01aa:
                int r19 = r19 + -1
                goto L_0x0153
            L_0x01ad:
                java.lang.String r36 = "html"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x01f9
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.util.ArrayList r36 = r41.m22056()
                r37 = 0
                java.lang.Object r18 = r36.get(r37)
                org.jsoup.nodes.Element r18 = (org.jsoup.nodes.Element) r18
                org.jsoup.nodes.Attributes r36 = r34.m22203()
                java.util.Iterator r36 = r36.iterator()
            L_0x01d5:
                boolean r37 = r36.hasNext()
                if (r37 == 0) goto L_0x0011
                java.lang.Object r6 = r36.next()
                org.jsoup.nodes.Attribute r6 = (org.jsoup.nodes.Attribute) r6
                java.lang.String r37 = r6.getKey()
                r0 = r18
                r1 = r37
                boolean r37 = r0.m21951((java.lang.String) r1)
                if (r37 != 0) goto L_0x01d5
                org.jsoup.nodes.Attributes r37 = r18.m21820()
                r0 = r37
                r0.m21736((org.jsoup.nodes.Attribute) r6)
                goto L_0x01d5
            L_0x01f9:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17371
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0213
                org.jsoup.parser.HtmlTreeBuilderState r36 = InHead
                r0 = r41
                r1 = r40
                r2 = r36
                boolean r36 = r0.m22099((org.jsoup.parser.Token) r1, (org.jsoup.parser.HtmlTreeBuilderState) r2)
                goto L_0x0013
            L_0x0213:
                java.lang.String r36 = "body"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x029f
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.util.ArrayList r32 = r41.m22056()
                int r36 = r32.size()
                r37 = 1
                r0 = r36
                r1 = r37
                if (r0 == r1) goto L_0x025c
                int r36 = r32.size()
                r37 = 2
                r0 = r36
                r1 = r37
                if (r0 <= r1) goto L_0x0260
                r36 = 1
                r0 = r32
                r1 = r36
                java.lang.Object r36 = r0.get(r1)
                org.jsoup.nodes.Element r36 = (org.jsoup.nodes.Element) r36
                java.lang.String r36 = r36.m21861()
                java.lang.String r37 = "body"
                boolean r36 = r36.equals(r37)
                if (r36 != 0) goto L_0x0260
            L_0x025c:
                r36 = 0
                goto L_0x0013
            L_0x0260:
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                r36 = 1
                r0 = r32
                r1 = r36
                java.lang.Object r7 = r0.get(r1)
                org.jsoup.nodes.Element r7 = (org.jsoup.nodes.Element) r7
                org.jsoup.nodes.Attributes r36 = r34.m22203()
                java.util.Iterator r36 = r36.iterator()
            L_0x027d:
                boolean r37 = r36.hasNext()
                if (r37 == 0) goto L_0x0011
                java.lang.Object r6 = r36.next()
                org.jsoup.nodes.Attribute r6 = (org.jsoup.nodes.Attribute) r6
                java.lang.String r37 = r6.getKey()
                r0 = r37
                boolean r37 = r7.m21951((java.lang.String) r0)
                if (r37 != 0) goto L_0x027d
                org.jsoup.nodes.Attributes r37 = r7.m21820()
                r0 = r37
                r0.m21736((org.jsoup.nodes.Attribute) r6)
                goto L_0x027d
            L_0x029f:
                java.lang.String r36 = "frameset"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0337
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.util.ArrayList r32 = r41.m22056()
                int r36 = r32.size()
                r37 = 1
                r0 = r36
                r1 = r37
                if (r0 == r1) goto L_0x02e8
                int r36 = r32.size()
                r37 = 2
                r0 = r36
                r1 = r37
                if (r0 <= r1) goto L_0x02ec
                r36 = 1
                r0 = r32
                r1 = r36
                java.lang.Object r36 = r0.get(r1)
                org.jsoup.nodes.Element r36 = (org.jsoup.nodes.Element) r36
                java.lang.String r36 = r36.m21861()
                java.lang.String r37 = "body"
                boolean r36 = r36.equals(r37)
                if (r36 != 0) goto L_0x02ec
            L_0x02e8:
                r36 = 0
                goto L_0x0013
            L_0x02ec:
                boolean r36 = r41.m22063()
                if (r36 != 0) goto L_0x02f6
                r36 = 0
                goto L_0x0013
            L_0x02f6:
                r36 = 1
                r0 = r32
                r1 = r36
                java.lang.Object r29 = r0.get(r1)
                org.jsoup.nodes.Element r29 = (org.jsoup.nodes.Element) r29
                org.jsoup.nodes.Element r36 = r29.m21819()
                if (r36 == 0) goto L_0x030b
                r29.m21930()
            L_0x030b:
                int r36 = r32.size()
                r37 = 1
                r0 = r36
                r1 = r37
                if (r0 <= r1) goto L_0x0325
                int r36 = r32.size()
                int r36 = r36 + -1
                r0 = r32
                r1 = r36
                r0.remove(r1)
                goto L_0x030b
            L_0x0325:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                org.jsoup.parser.HtmlTreeBuilderState r36 = InFrameset
                r0 = r41
                r1 = r36
                r0.m22091((org.jsoup.parser.HtmlTreeBuilderState) r1)
                goto L_0x0011
            L_0x0337:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17370
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x037d
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x035a
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x035a:
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                java.lang.String[] r37 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17370
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r36, r37)
                if (r36 == 0) goto L_0x0374
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r41.m22051()
            L_0x0374:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x037d:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17369
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x03be
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x03a0
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x03a0:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r0 = r41
                org.jsoup.parser.CharacterReader r0 = r0.f17517
                r36 = r0
                java.lang.String r37 = "\n"
                r36.m22013((java.lang.String) r37)
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                goto L_0x0011
            L_0x03be:
                java.lang.String r36 = "form"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0400
                org.jsoup.nodes.FormElement r36 = r41.m22047()
                if (r36 == 0) goto L_0x03dc
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x03dc:
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x03f3
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x03f3:
                r36 = 1
                r0 = r41
                r1 = r34
                r2 = r36
                r0.m22084((org.jsoup.parser.Token.StartTag) r1, (boolean) r2)
                goto L_0x0011
            L_0x0400:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17357
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0479
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                java.util.ArrayList r32 = r41.m22056()
                int r36 = r32.size()
                int r19 = r36 + -1
            L_0x041f:
                if (r19 <= 0) goto L_0x0442
                r0 = r32
                r1 = r19
                java.lang.Object r13 = r0.get(r1)
                org.jsoup.nodes.Element r13 = (org.jsoup.nodes.Element) r13
                java.lang.String r36 = r13.m21861()
                java.lang.String[] r37 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17357
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r36, r37)
                if (r36 == 0) goto L_0x0462
                java.lang.String r36 = r13.m21861()
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x0442:
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x0459
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x0459:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x0462:
                r0 = r41
                boolean r36 = r0.m22041((org.jsoup.nodes.Element) r13)
                if (r36 == 0) goto L_0x0476
                java.lang.String r36 = r13.m21861()
                java.lang.String[] r37 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17367
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r36, r37)
                if (r36 == 0) goto L_0x0442
            L_0x0476:
                int r19 = r19 + -1
                goto L_0x041f
            L_0x0479:
                java.lang.String r36 = "plaintext"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x04b1
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x049d
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x049d:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r0 = r41
                org.jsoup.parser.Tokeniser r0 = r0.f17518
                r36 = r0
                org.jsoup.parser.TokeniserState r37 = org.jsoup.parser.TokeniserState.PLAINTEXT
                r36.m22256((org.jsoup.parser.TokeniserState) r37)
                goto L_0x0011
            L_0x04b1:
                java.lang.String r36 = "button"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x04fa
                java.lang.String r36 = "button"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x04e5
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.lang.String r36 = "button"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
                r0 = r41
                r1 = r34
                r0.m22098((org.jsoup.parser.Token) r1)
                goto L_0x0011
            L_0x04e5:
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                goto L_0x0011
            L_0x04fa:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17358
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0518
                r41.m22054()
                r0 = r41
                r1 = r34
                org.jsoup.nodes.Element r13 = r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r0 = r41
                r0.m22052((org.jsoup.nodes.Element) r13)
                goto L_0x0011
            L_0x0518:
                java.lang.String r36 = "nobr"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0558
                r41.m22054()
                java.lang.String r36 = "nobr"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 == 0) goto L_0x0549
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.lang.String r36 = "nobr"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
                r41.m22054()
            L_0x0549:
                r0 = r41
                r1 = r34
                org.jsoup.nodes.Element r13 = r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r0 = r41
                r0.m22052((org.jsoup.nodes.Element) r13)
                goto L_0x0011
            L_0x0558:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17359
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x057c
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r41.m22062()
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                goto L_0x0011
            L_0x057c:
                java.lang.String r36 = "table"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x05cb
                org.jsoup.nodes.Document r36 = r41.m22033()
                org.jsoup.nodes.Document$QuirksMode r36 = r36.m21769()
                org.jsoup.nodes.Document$QuirksMode r37 = org.jsoup.nodes.Document.QuirksMode.quirks
                r0 = r36
                r1 = r37
                if (r0 == r1) goto L_0x05b0
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x05b0
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x05b0:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                org.jsoup.parser.HtmlTreeBuilderState r36 = InTable
                r0 = r41
                r1 = r36
                r0.m22091((org.jsoup.parser.HtmlTreeBuilderState) r1)
                goto L_0x0011
            L_0x05cb:
                java.lang.String r36 = "input"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0600
                r41.m22054()
                r0 = r41
                r1 = r34
                org.jsoup.nodes.Element r13 = r0.m22067((org.jsoup.parser.Token.StartTag) r1)
                java.lang.String r36 = "type"
                r0 = r36
                java.lang.String r36 = r13.m21947(r0)
                java.lang.String r37 = "hidden"
                boolean r36 = r36.equalsIgnoreCase(r37)
                if (r36 != 0) goto L_0x0011
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                goto L_0x0011
            L_0x0600:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17365
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0615
                r0 = r41
                r1 = r34
                r0.m22067((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x0615:
                java.lang.String r36 = "hr"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x064b
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x0639
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x0639:
                r0 = r41
                r1 = r34
                r0.m22067((org.jsoup.parser.Token.StartTag) r1)
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                goto L_0x0011
            L_0x064b:
                java.lang.String r36 = "image"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0683
                java.lang.String r36 = "svg"
                r0 = r41
                r1 = r36
                org.jsoup.nodes.Element r36 = r0.m22066((java.lang.String) r1)
                if (r36 != 0) goto L_0x067a
                java.lang.String r36 = "img"
                r0 = r34
                r1 = r36
                org.jsoup.parser.Token$Tag r36 = r0.m22214((java.lang.String) r1)
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22098((org.jsoup.parser.Token) r1)
                goto L_0x0013
            L_0x067a:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x0683:
                java.lang.String r36 = "isindex"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0785
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                org.jsoup.nodes.FormElement r36 = r41.m22047()
                if (r36 == 0) goto L_0x06a1
                r36 = 0
                goto L_0x0013
            L_0x06a1:
                java.lang.String r36 = "form"
                r0 = r41
                r1 = r36
                r0.m22340(r1)
                r0 = r34
                org.jsoup.nodes.Attributes r0 = r0.f17415
                r36 = r0
                java.lang.String r37 = "action"
                boolean r36 = r36.m21723(r37)
                if (r36 == 0) goto L_0x06d5
                org.jsoup.nodes.FormElement r15 = r41.m22047()
                java.lang.String r36 = "action"
                r0 = r34
                org.jsoup.nodes.Attributes r0 = r0.f17415
                r37 = r0
                java.lang.String r38 = "action"
                java.lang.String r37 = r37.m21732(r38)
                r0 = r36
                r1 = r37
                r15.m21865((java.lang.String) r0, (java.lang.String) r1)
            L_0x06d5:
                java.lang.String r36 = "hr"
                r0 = r41
                r1 = r36
                r0.m22340(r1)
                java.lang.String r36 = "label"
                r0 = r41
                r1 = r36
                r0.m22340(r1)
                r0 = r34
                org.jsoup.nodes.Attributes r0 = r0.f17415
                r36 = r0
                java.lang.String r37 = "prompt"
                boolean r36 = r36.m21723(r37)
                if (r36 == 0) goto L_0x0746
                r0 = r34
                org.jsoup.nodes.Attributes r0 = r0.f17415
                r36 = r0
                java.lang.String r37 = "prompt"
                java.lang.String r26 = r36.m21732(r37)
            L_0x0705:
                org.jsoup.parser.Token$Character r36 = new org.jsoup.parser.Token$Character
                r36.<init>()
                r0 = r36
                r1 = r26
                org.jsoup.parser.Token$Character r36 = r0.m22187(r1)
                r0 = r41
                r1 = r36
                r0.m22098((org.jsoup.parser.Token) r1)
                org.jsoup.nodes.Attributes r20 = new org.jsoup.nodes.Attributes
                r20.<init>()
                r0 = r34
                org.jsoup.nodes.Attributes r0 = r0.f17415
                r36 = r0
                java.util.Iterator r36 = r36.iterator()
            L_0x0728:
                boolean r37 = r36.hasNext()
                if (r37 == 0) goto L_0x074a
                java.lang.Object r5 = r36.next()
                org.jsoup.nodes.Attribute r5 = (org.jsoup.nodes.Attribute) r5
                java.lang.String r37 = r5.getKey()
                java.lang.String[] r38 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17366
                boolean r37 = org.jsoup.helper.StringUtil.m21678(r37, r38)
                if (r37 != 0) goto L_0x0728
                r0 = r20
                r0.m21736((org.jsoup.nodes.Attribute) r5)
                goto L_0x0728
            L_0x0746:
                java.lang.String r26 = "This is a searchable index. Enter search keywords: "
                goto L_0x0705
            L_0x074a:
                java.lang.String r36 = "name"
                java.lang.String r37 = "isindex"
                r0 = r20
                r1 = r36
                r2 = r37
                r0.m21735((java.lang.String) r1, (java.lang.String) r2)
                java.lang.String r36 = "input"
                r0 = r41
                r1 = r36
                r2 = r20
                r0.m22096((java.lang.String) r1, (org.jsoup.nodes.Attributes) r2)
                java.lang.String r36 = "label"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
                java.lang.String r36 = "hr"
                r0 = r41
                r1 = r36
                r0.m22340(r1)
                java.lang.String r36 = "form"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
                goto L_0x0011
            L_0x0785:
                java.lang.String r36 = "textarea"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x07bb
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r0 = r41
                org.jsoup.parser.Tokeniser r0 = r0.f17518
                r36 = r0
                org.jsoup.parser.TokeniserState r37 = org.jsoup.parser.TokeniserState.Rcdata
                r36.m22256((org.jsoup.parser.TokeniserState) r37)
                r41.m22077()
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                org.jsoup.parser.HtmlTreeBuilderState r36 = Text
                r0 = r41
                r1 = r36
                r0.m22091((org.jsoup.parser.HtmlTreeBuilderState) r1)
                goto L_0x0011
            L_0x07bb:
                java.lang.String r36 = "xmp"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x07f4
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 == 0) goto L_0x07df
                java.lang.String r36 = "p"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x07df:
                r41.m22054()
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                r0 = r34
                r1 = r41
                org.jsoup.parser.HtmlTreeBuilderState.m22105(r0, r1)
                goto L_0x0011
            L_0x07f4:
                java.lang.String r36 = "iframe"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0813
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                r0 = r34
                r1 = r41
                org.jsoup.parser.HtmlTreeBuilderState.m22105(r0, r1)
                goto L_0x0011
            L_0x0813:
                java.lang.String r36 = "noembed"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0829
                r0 = r34
                r1 = r41
                org.jsoup.parser.HtmlTreeBuilderState.m22105(r0, r1)
                goto L_0x0011
            L_0x0829:
                java.lang.String r36 = "select"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x088b
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22094((boolean) r1)
                org.jsoup.parser.HtmlTreeBuilderState r35 = r41.m22068()
                org.jsoup.parser.HtmlTreeBuilderState r36 = InTable
                boolean r36 = r35.equals(r36)
                if (r36 != 0) goto L_0x0875
                org.jsoup.parser.HtmlTreeBuilderState r36 = InCaption
                boolean r36 = r35.equals(r36)
                if (r36 != 0) goto L_0x0875
                org.jsoup.parser.HtmlTreeBuilderState r36 = InTableBody
                boolean r36 = r35.equals(r36)
                if (r36 != 0) goto L_0x0875
                org.jsoup.parser.HtmlTreeBuilderState r36 = InRow
                boolean r36 = r35.equals(r36)
                if (r36 != 0) goto L_0x0875
                org.jsoup.parser.HtmlTreeBuilderState r36 = InCell
                boolean r36 = r35.equals(r36)
                if (r36 == 0) goto L_0x0880
            L_0x0875:
                org.jsoup.parser.HtmlTreeBuilderState r36 = InSelectInTable
                r0 = r41
                r1 = r36
                r0.m22091((org.jsoup.parser.HtmlTreeBuilderState) r1)
                goto L_0x0011
            L_0x0880:
                org.jsoup.parser.HtmlTreeBuilderState r36 = InSelect
                r0 = r41
                r1 = r36
                r0.m22091((org.jsoup.parser.HtmlTreeBuilderState) r1)
                goto L_0x0011
            L_0x088b:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17362
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x08be
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                java.lang.String r37 = "option"
                boolean r36 = r36.equals(r37)
                if (r36 == 0) goto L_0x08b2
                java.lang.String r36 = "option"
                r0 = r41
                r1 = r36
                r0.m22339(r1)
            L_0x08b2:
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x08be:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17360
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0905
                java.lang.String r36 = "ruby"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 == 0) goto L_0x0011
                r41.m22044()
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                java.lang.String r37 = "ruby"
                boolean r36 = r36.equals(r37)
                if (r36 != 0) goto L_0x08fc
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.lang.String r36 = "ruby"
                r0 = r41
                r1 = r36
                r0.m22075((java.lang.String) r1)
            L_0x08fc:
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x0905:
                java.lang.String r36 = "math"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x091e
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x091e:
                java.lang.String r36 = "svg"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0937
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x0937:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17361
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x094e
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x094e:
                r41.m22054()
                r0 = r41
                r1 = r34
                r0.m22083((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x0011
            L_0x095a:
                org.jsoup.parser.Token$EndTag r14 = r40.m22174()
                java.lang.String r23 = r14.m22206()
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17373
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0b3c
                r19 = 0
            L_0x0970:
                r36 = 8
                r0 = r19
                r1 = r36
                if (r0 >= r1) goto L_0x0011
                r0 = r41
                r1 = r23
                org.jsoup.nodes.Element r16 = r0.m22059((java.lang.String) r1)
                if (r16 != 0) goto L_0x0988
                boolean r36 = r39.m22144(r40, r41)
                goto L_0x0013
            L_0x0988:
                r0 = r41
                r1 = r16
                boolean r36 = r0.m22076((org.jsoup.nodes.Element) r1)
                if (r36 != 0) goto L_0x09a4
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r0 = r41
                r1 = r16
                r0.m22058((org.jsoup.nodes.Element) r1)
                r36 = 1
                goto L_0x0013
            L_0x09a4:
                java.lang.String r36 = r16.m21861()
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 != 0) goto L_0x09bd
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x09bd:
                org.jsoup.nodes.Element r36 = r41.m22342()
                r0 = r36
                r1 = r16
                if (r0 == r1) goto L_0x09ce
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x09ce:
                r17 = 0
                r11 = 0
                r30 = 0
                java.util.ArrayList r32 = r41.m22056()
                int r33 = r32.size()
                r31 = 0
            L_0x09dd:
                r0 = r31
                r1 = r33
                if (r0 >= r1) goto L_0x0a16
                r36 = 64
                r0 = r31
                r1 = r36
                if (r0 >= r1) goto L_0x0a16
                r0 = r32
                r1 = r31
                java.lang.Object r13 = r0.get(r1)
                org.jsoup.nodes.Element r13 = (org.jsoup.nodes.Element) r13
                r0 = r16
                if (r13 != r0) goto L_0x0a0a
                int r36 = r31 + -1
                r0 = r32
                r1 = r36
                java.lang.Object r11 = r0.get(r1)
                org.jsoup.nodes.Element r11 = (org.jsoup.nodes.Element) r11
                r30 = 1
            L_0x0a07:
                int r31 = r31 + 1
                goto L_0x09dd
            L_0x0a0a:
                if (r30 == 0) goto L_0x0a07
                r0 = r41
                boolean r36 = r0.m22041((org.jsoup.nodes.Element) r13)
                if (r36 == 0) goto L_0x0a07
                r17 = r13
            L_0x0a16:
                if (r17 != 0) goto L_0x0a2e
                java.lang.String r36 = r16.m21861()
                r0 = r41
                r1 = r36
                r0.m22078((java.lang.String) r1)
                r0 = r41
                r1 = r16
                r0.m22058((org.jsoup.nodes.Element) r1)
                r36 = 1
                goto L_0x0013
            L_0x0a2e:
                r24 = r17
                r22 = r17
                r21 = 0
            L_0x0a34:
                r36 = 3
                r0 = r21
                r1 = r36
                if (r0 >= r1) goto L_0x0a68
                r0 = r41
                r1 = r24
                boolean r36 = r0.m22076((org.jsoup.nodes.Element) r1)
                if (r36 == 0) goto L_0x0a4e
                r0 = r41
                r1 = r24
                org.jsoup.nodes.Element r24 = r0.m22034((org.jsoup.nodes.Element) r1)
            L_0x0a4e:
                r0 = r41
                r1 = r24
                boolean r36 = r0.m22061((org.jsoup.nodes.Element) r1)
                if (r36 != 0) goto L_0x0a62
                r0 = r41
                r1 = r24
                r0.m22065((org.jsoup.nodes.Element) r1)
            L_0x0a5f:
                int r21 = r21 + 1
                goto L_0x0a34
            L_0x0a62:
                r0 = r24
                r1 = r16
                if (r0 != r1) goto L_0x0ac7
            L_0x0a68:
                java.lang.String r36 = r11.m21861()
                java.lang.String[] r37 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17363
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r36, r37)
                if (r36 == 0) goto L_0x0b0e
                org.jsoup.nodes.Element r36 = r22.m21819()
                if (r36 == 0) goto L_0x0a7d
                r22.m21930()
            L_0x0a7d:
                r0 = r41
                r1 = r22
                r0.m22090((org.jsoup.nodes.Node) r1)
            L_0x0a84:
                org.jsoup.nodes.Element r4 = new org.jsoup.nodes.Element
                org.jsoup.parser.Tag r36 = r16.m21871()
                java.lang.String r37 = r41.m22036()
                r0 = r36
                r1 = r37
                r4.<init>(r0, r1)
                org.jsoup.nodes.Attributes r36 = r4.m21820()
                org.jsoup.nodes.Attributes r37 = r16.m21820()
                r36.m21738((org.jsoup.nodes.Attributes) r37)
                java.util.List r36 = r17.m21921()
                int r37 = r17.m21860()
                r0 = r37
                org.jsoup.nodes.Node[] r0 = new org.jsoup.nodes.Node[r0]
                r37 = r0
                java.lang.Object[] r10 = r36.toArray(r37)
                org.jsoup.nodes.Node[] r10 = (org.jsoup.nodes.Node[]) r10
                int r0 = r10.length
                r37 = r0
                r36 = 0
            L_0x0ab9:
                r0 = r36
                r1 = r37
                if (r0 >= r1) goto L_0x0b1e
                r9 = r10[r36]
                r4.m21864((org.jsoup.nodes.Node) r9)
                int r36 = r36 + 1
                goto L_0x0ab9
            L_0x0ac7:
                org.jsoup.nodes.Element r28 = new org.jsoup.nodes.Element
                java.lang.String r36 = r24.m21861()
                org.jsoup.parser.ParseSettings r37 = org.jsoup.parser.ParseSettings.f17376
                org.jsoup.parser.Tag r36 = org.jsoup.parser.Tag.m22160(r36, r37)
                java.lang.String r37 = r41.m22036()
                r0 = r28
                r1 = r36
                r2 = r37
                r0.<init>(r1, r2)
                r0 = r41
                r1 = r24
                r2 = r28
                r0.m22080(r1, r2)
                r0 = r41
                r1 = r24
                r2 = r28
                r0.m22070(r1, r2)
                r24 = r28
                r0 = r22
                r1 = r17
                if (r0 != r1) goto L_0x0afa
            L_0x0afa:
                org.jsoup.nodes.Element r36 = r22.m21819()
                if (r36 == 0) goto L_0x0b03
                r22.m21930()
            L_0x0b03:
                r0 = r24
                r1 = r22
                r0.m21864((org.jsoup.nodes.Node) r1)
                r22 = r24
                goto L_0x0a5f
            L_0x0b0e:
                org.jsoup.nodes.Element r36 = r22.m21819()
                if (r36 == 0) goto L_0x0b17
                r22.m21930()
            L_0x0b17:
                r0 = r22
                r11.m21864((org.jsoup.nodes.Node) r0)
                goto L_0x0a84
            L_0x0b1e:
                r0 = r17
                r0.m21864((org.jsoup.nodes.Node) r4)
                r0 = r41
                r1 = r16
                r0.m22058((org.jsoup.nodes.Element) r1)
                r0 = r41
                r1 = r16
                r0.m22065((org.jsoup.nodes.Element) r1)
                r0 = r41
                r1 = r17
                r0.m22088((org.jsoup.nodes.Element) r1, (org.jsoup.nodes.Element) r4)
                int r19 = r19 + 1
                goto L_0x0970
            L_0x0b3c:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17372
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0b82
                r0 = r41
                r1 = r23
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 != 0) goto L_0x0b5d
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0b5d:
                r41.m22044()
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                r0 = r36
                r1 = r23
                boolean r36 = r0.equals(r1)
                if (r36 != 0) goto L_0x0b79
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x0b79:
                r0 = r41
                r1 = r23
                r0.m22078((java.lang.String) r1)
                goto L_0x0011
            L_0x0b82:
                java.lang.String r36 = "span"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0b95
                boolean r36 = r39.m22144(r40, r41)
                goto L_0x0013
            L_0x0b95:
                java.lang.String r36 = "li"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0be0
                r0 = r41
                r1 = r23
                boolean r36 = r0.m22035((java.lang.String) r1)
                if (r36 != 0) goto L_0x0bb7
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0bb7:
                r0 = r41
                r1 = r23
                r0.m22057((java.lang.String) r1)
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                r0 = r36
                r1 = r23
                boolean r36 = r0.equals(r1)
                if (r36 != 0) goto L_0x0bd7
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x0bd7:
                r0 = r41
                r1 = r23
                r0.m22078((java.lang.String) r1)
                goto L_0x0011
            L_0x0be0:
                java.lang.String r36 = "body"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0c10
                java.lang.String r36 = "body"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 != 0) goto L_0x0c05
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0c05:
                org.jsoup.parser.HtmlTreeBuilderState r36 = AfterBody
                r0 = r41
                r1 = r36
                r0.m22091((org.jsoup.parser.HtmlTreeBuilderState) r1)
                goto L_0x0011
            L_0x0c10:
                java.lang.String r36 = "html"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0c32
                java.lang.String r36 = "body"
                r0 = r41
                r1 = r36
                boolean r25 = r0.m22339(r1)
                if (r25 == 0) goto L_0x0011
                r0 = r41
                boolean r36 = r0.m22098((org.jsoup.parser.Token) r14)
                goto L_0x0013
            L_0x0c32:
                java.lang.String r36 = "form"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0c86
                org.jsoup.nodes.FormElement r12 = r41.m22047()
                r36 = 0
                r0 = r41
                r1 = r36
                r0.m22089((org.jsoup.nodes.FormElement) r1)
                if (r12 == 0) goto L_0x0c58
                r0 = r41
                r1 = r23
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 != 0) goto L_0x0c63
            L_0x0c58:
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0c63:
                r41.m22044()
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                r0 = r36
                r1 = r23
                boolean r36 = r0.equals(r1)
                if (r36 != 0) goto L_0x0c7f
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x0c7f:
                r0 = r41
                r0.m22065((org.jsoup.nodes.Element) r12)
                goto L_0x0011
            L_0x0c86:
                java.lang.String r36 = "p"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0cdc
                r0 = r41
                r1 = r23
                boolean r36 = r0.m22038((java.lang.String) r1)
                if (r36 != 0) goto L_0x0cb3
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r0 = r41
                r1 = r23
                r0.m22340(r1)
                r0 = r41
                boolean r36 = r0.m22098((org.jsoup.parser.Token) r14)
                goto L_0x0013
            L_0x0cb3:
                r0 = r41
                r1 = r23
                r0.m22057((java.lang.String) r1)
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                r0 = r36
                r1 = r23
                boolean r36 = r0.equals(r1)
                if (r36 != 0) goto L_0x0cd3
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x0cd3:
                r0 = r41
                r1 = r23
                r0.m22078((java.lang.String) r1)
                goto L_0x0011
            L_0x0cdc:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17357
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0d26
                r0 = r41
                r1 = r23
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 != 0) goto L_0x0cfd
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0cfd:
                r0 = r41
                r1 = r23
                r0.m22057((java.lang.String) r1)
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                r0 = r36
                r1 = r23
                boolean r36 = r0.equals(r1)
                if (r36 != 0) goto L_0x0d1d
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x0d1d:
                r0 = r41
                r1 = r23
                r0.m22078((java.lang.String) r1)
                goto L_0x0011
            L_0x0d26:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17370
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0d74
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17370
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22073((java.lang.String[]) r1)
                if (r36 != 0) goto L_0x0d49
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0d49:
                r0 = r41
                r1 = r23
                r0.m22057((java.lang.String) r1)
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                r0 = r36
                r1 = r23
                boolean r36 = r0.equals(r1)
                if (r36 != 0) goto L_0x0d69
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x0d69:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17370
                r0 = r41
                r1 = r36
                r0.m22095((java.lang.String[]) r1)
                goto L_0x0011
            L_0x0d74:
                java.lang.String r36 = "sarcasm"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0d87
                boolean r36 = r39.m22144(r40, r41)
                goto L_0x0013
            L_0x0d87:
                java.lang.String[] r36 = org.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f17359
                r0 = r23
                r1 = r36
                boolean r36 = org.jsoup.helper.StringUtil.m21678(r0, r1)
                if (r36 == 0) goto L_0x0ddd
                java.lang.String r36 = "name"
                r0 = r41
                r1 = r36
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 != 0) goto L_0x0011
                r0 = r41
                r1 = r23
                boolean r36 = r0.m22064((java.lang.String) r1)
                if (r36 != 0) goto L_0x0db5
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                r36 = 0
                goto L_0x0013
            L_0x0db5:
                r41.m22044()
                org.jsoup.nodes.Element r36 = r41.m22342()
                java.lang.String r36 = r36.m21861()
                r0 = r36
                r1 = r23
                boolean r36 = r0.equals(r1)
                if (r36 != 0) goto L_0x0dd1
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
            L_0x0dd1:
                r0 = r41
                r1 = r23
                r0.m22078((java.lang.String) r1)
                r41.m22055()
                goto L_0x0011
            L_0x0ddd:
                java.lang.String r36 = "br"
                r0 = r23
                r1 = r36
                boolean r36 = r0.equals(r1)
                if (r36 == 0) goto L_0x0dff
                r0 = r41
                r1 = r39
                r0.m22071((org.jsoup.parser.HtmlTreeBuilderState) r1)
                java.lang.String r36 = "br"
                r0 = r41
                r1 = r36
                r0.m22340(r1)
                r36 = 0
                goto L_0x0013
            L_0x0dff:
                boolean r36 = r39.m22144(r40, r41)
                goto L_0x0013
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass7.m22145(org.jsoup.parser.Token, org.jsoup.parser.HtmlTreeBuilder):boolean");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m22144(Token t, HtmlTreeBuilder tb) {
            String name = tb.f17523.m22150(t.m22174().m22205());
            ArrayList<Element> stack = tb.m22056();
            int pos = stack.size() - 1;
            while (true) {
                if (pos < 0) {
                    break;
                }
                Element node = stack.get(pos);
                if (node.m21861().equals(name)) {
                    tb.m22057(name);
                    if (!name.equals(tb.m22342().m21861())) {
                        tb.m22071((HtmlTreeBuilderState) this);
                    }
                    tb.m22078(name);
                } else if (tb.m22041(node)) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    pos--;
                }
            }
            return true;
        }
    },
    Text {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22146(Token t, HtmlTreeBuilder tb) {
            if (t.m22180()) {
                tb.m22092(t.m22175());
            } else if (t.m22176()) {
                tb.m22071((HtmlTreeBuilderState) this);
                tb.m22051();
                tb.m22091(tb.m22074());
                return tb.m22098(t);
            } else if (t.m22173()) {
                tb.m22051();
                tb.m22091(tb.m22074());
            }
            return true;
        }
    },
    InTable {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22148(Token t, HtmlTreeBuilder tb) {
            if (t.m22180()) {
                tb.m22048();
                tb.m22077();
                tb.m22091(InTableText);
                return tb.m22098(t);
            } else if (t.m22178()) {
                tb.m22093(t.m22179());
                return true;
            } else if (t.m22184()) {
                tb.m22071((HtmlTreeBuilderState) this);
                return false;
            } else if (t.m22181()) {
                Token.StartTag startTag = t.m22172();
                String name = startTag.m22206();
                if (name.equals("caption")) {
                    tb.m22060();
                    tb.m22062();
                    tb.m22083(startTag);
                    tb.m22091(InCaption);
                    return true;
                } else if (name.equals("colgroup")) {
                    tb.m22060();
                    tb.m22083(startTag);
                    tb.m22091(InColumnGroup);
                    return true;
                } else if (name.equals("col")) {
                    tb.m22340("colgroup");
                    return tb.m22098(t);
                } else {
                    if (StringUtil.m21690(name, "tbody", "tfoot", "thead")) {
                        tb.m22060();
                        tb.m22083(startTag);
                        tb.m22091(InTableBody);
                        return true;
                    }
                    if (StringUtil.m21690(name, "td", "th", "tr")) {
                        tb.m22340("tbody");
                        return tb.m22098(t);
                    } else if (name.equals("table")) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        if (tb.m22339("table")) {
                            return tb.m22098(t);
                        }
                        return true;
                    } else {
                        if (StringUtil.m21690(name, TtmlNode.TAG_STYLE, "script")) {
                            return tb.m22099(t, InHead);
                        }
                        if (name.equals("input")) {
                            if (!startTag.f17415.m21732(VastExtensionXmlManager.TYPE).equalsIgnoreCase("hidden")) {
                                return m22147(t, tb);
                            }
                            tb.m22067(startTag);
                            return true;
                        } else if (!name.equals("form")) {
                            return m22147(t, tb);
                        } else {
                            tb.m22071((HtmlTreeBuilderState) this);
                            if (tb.m22047() != null) {
                                return false;
                            }
                            tb.m22084(startTag, false);
                            return true;
                        }
                    }
                }
            } else if (t.m22173()) {
                String name2 = t.m22174().m22206();
                if (!name2.equals("table")) {
                    if (!StringUtil.m21690(name2, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML, "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        return m22147(t, tb);
                    }
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                } else if (!tb.m22040(name2)) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    tb.m22078("table");
                    tb.m22043();
                    return true;
                }
            } else if (!t.m22176()) {
                return m22147(t, tb);
            } else {
                if (!tb.m22342().m21861().equals(AdType.HTML)) {
                    return true;
                }
                tb.m22071((HtmlTreeBuilderState) this);
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m22147(Token t, HtmlTreeBuilder tb) {
            tb.m22071((HtmlTreeBuilderState) this);
            if (!StringUtil.m21690(tb.m22342().m21861(), "table", "tbody", "tfoot", "thead", "tr")) {
                return tb.m22099(t, InBody);
            }
            tb.m22072(true);
            boolean processed = tb.m22099(t, InBody);
            tb.m22072(false);
            return processed;
        }
    },
    InTableText {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22113(Token t, HtmlTreeBuilder tb) {
            switch (AnonymousClass24.f17356[t.f17401.ordinal()]) {
                case 5:
                    Token.Character c = t.m22175();
                    if (c.m22188().equals(HtmlTreeBuilderState.f17345)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                    tb.m22049().add(c.m22188());
                    return true;
                default:
                    if (tb.m22049().size() > 0) {
                        for (String character : tb.m22049()) {
                            if (!HtmlTreeBuilderState.m22103(character)) {
                                tb.m22071((HtmlTreeBuilderState) this);
                                if (StringUtil.m21690(tb.m22342().m21861(), "table", "tbody", "tfoot", "thead", "tr")) {
                                    tb.m22072(true);
                                    tb.m22099((Token) new Token.Character().m22187(character), InBody);
                                    tb.m22072(false);
                                } else {
                                    tb.m22099((Token) new Token.Character().m22187(character), InBody);
                                }
                            } else {
                                tb.m22092(new Token.Character().m22187(character));
                            }
                        }
                        tb.m22048();
                    }
                    tb.m22091(tb.m22074());
                    return tb.m22098(t);
            }
        }
    },
    InCaption {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x009d, code lost:
            if (org.jsoup.helper.StringUtil.m21690(r13.m22172().m22206(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr") == false) goto L_0x009f;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m22114(org.jsoup.parser.Token r13, org.jsoup.parser.HtmlTreeBuilder r14) {
            /*
                r12 = this;
                r11 = 4
                r10 = 3
                r9 = 2
                r4 = 1
                r3 = 0
                boolean r5 = r13.m22173()
                if (r5 == 0) goto L_0x0055
                org.jsoup.parser.Token$EndTag r5 = r13.m22174()
                java.lang.String r5 = r5.m22206()
                java.lang.String r6 = "caption"
                boolean r5 = r5.equals(r6)
                if (r5 == 0) goto L_0x0055
                org.jsoup.parser.Token$EndTag r0 = r13.m22174()
                java.lang.String r1 = r0.m22206()
                boolean r5 = r14.m22040((java.lang.String) r1)
                if (r5 != 0) goto L_0x002e
                r14.m22071((org.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x002d:
                return r3
            L_0x002e:
                r14.m22044()
                org.jsoup.nodes.Element r3 = r14.m22342()
                java.lang.String r3 = r3.m21861()
                java.lang.String r5 = "caption"
                boolean r3 = r3.equals(r5)
                if (r3 != 0) goto L_0x0045
                r14.m22071((org.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x0045:
                java.lang.String r3 = "caption"
                r14.m22078((java.lang.String) r3)
                r14.m22055()
                org.jsoup.parser.HtmlTreeBuilderState r3 = InTable
                r14.m22091((org.jsoup.parser.HtmlTreeBuilderState) r3)
            L_0x0053:
                r3 = r4
                goto L_0x002d
            L_0x0055:
                boolean r5 = r13.m22181()
                if (r5 == 0) goto L_0x009f
                org.jsoup.parser.Token$StartTag r5 = r13.m22172()
                java.lang.String r5 = r5.m22206()
                r6 = 9
                java.lang.String[] r6 = new java.lang.String[r6]
                java.lang.String r7 = "caption"
                r6[r3] = r7
                java.lang.String r7 = "col"
                r6[r4] = r7
                java.lang.String r7 = "colgroup"
                r6[r9] = r7
                java.lang.String r7 = "tbody"
                r6[r10] = r7
                java.lang.String r7 = "td"
                r6[r11] = r7
                r7 = 5
                java.lang.String r8 = "tfoot"
                r6[r7] = r8
                r7 = 6
                java.lang.String r8 = "th"
                r6[r7] = r8
                r7 = 7
                java.lang.String r8 = "thead"
                r6[r7] = r8
                r7 = 8
                java.lang.String r8 = "tr"
                r6[r7] = r8
                boolean r5 = org.jsoup.helper.StringUtil.m21690((java.lang.String) r5, (java.lang.String[]) r6)
                if (r5 != 0) goto L_0x00b6
            L_0x009f:
                boolean r5 = r13.m22173()
                if (r5 == 0) goto L_0x00c8
                org.jsoup.parser.Token$EndTag r5 = r13.m22174()
                java.lang.String r5 = r5.m22206()
                java.lang.String r6 = "table"
                boolean r5 = r5.equals(r6)
                if (r5 == 0) goto L_0x00c8
            L_0x00b6:
                r14.m22071((org.jsoup.parser.HtmlTreeBuilderState) r12)
                java.lang.String r3 = "caption"
                boolean r2 = r14.m22339(r3)
                if (r2 == 0) goto L_0x0053
                boolean r3 = r14.m22098((org.jsoup.parser.Token) r13)
                goto L_0x002d
            L_0x00c8:
                boolean r5 = r13.m22173()
                if (r5 == 0) goto L_0x011e
                org.jsoup.parser.Token$EndTag r5 = r13.m22174()
                java.lang.String r5 = r5.m22206()
                r6 = 10
                java.lang.String[] r6 = new java.lang.String[r6]
                java.lang.String r7 = "body"
                r6[r3] = r7
                java.lang.String r7 = "col"
                r6[r4] = r7
                java.lang.String r4 = "colgroup"
                r6[r9] = r4
                java.lang.String r4 = "html"
                r6[r10] = r4
                java.lang.String r4 = "tbody"
                r6[r11] = r4
                r4 = 5
                java.lang.String r7 = "td"
                r6[r4] = r7
                r4 = 6
                java.lang.String r7 = "tfoot"
                r6[r4] = r7
                r4 = 7
                java.lang.String r7 = "th"
                r6[r4] = r7
                r4 = 8
                java.lang.String r7 = "thead"
                r6[r4] = r7
                r4 = 9
                java.lang.String r7 = "tr"
                r6[r4] = r7
                boolean r4 = org.jsoup.helper.StringUtil.m21690((java.lang.String) r5, (java.lang.String[]) r6)
                if (r4 == 0) goto L_0x011e
                r14.m22071((org.jsoup.parser.HtmlTreeBuilderState) r12)
                goto L_0x002d
            L_0x011e:
                org.jsoup.parser.HtmlTreeBuilderState r3 = InBody
                boolean r3 = r14.m22099((org.jsoup.parser.Token) r13, (org.jsoup.parser.HtmlTreeBuilderState) r3)
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass11.m22114(org.jsoup.parser.Token, org.jsoup.parser.HtmlTreeBuilder):boolean");
        }
    },
    InColumnGroup {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x004e, code lost:
            if (r5.equals(com.mopub.common.AdType.HTML) != false) goto L_0x003f;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m22116(org.jsoup.parser.Token r8, org.jsoup.parser.HtmlTreeBuilder r9) {
            /*
                r7 = this;
                r3 = 0
                r2 = 1
                boolean r4 = org.jsoup.parser.HtmlTreeBuilderState.m22104((org.jsoup.parser.Token) r8)
                if (r4 == 0) goto L_0x0010
                org.jsoup.parser.Token$Character r3 = r8.m22175()
                r9.m22092((org.jsoup.parser.Token.Character) r3)
            L_0x000f:
                return r2
            L_0x0010:
                int[] r4 = org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass24.f17356
                org.jsoup.parser.Token$TokenType r5 = r8.f17401
                int r5 = r5.ordinal()
                r4 = r4[r5]
                switch(r4) {
                    case 1: goto L_0x0022;
                    case 2: goto L_0x002a;
                    case 3: goto L_0x002e;
                    case 4: goto L_0x0067;
                    case 5: goto L_0x001d;
                    case 6: goto L_0x009c;
                    default: goto L_0x001d;
                }
            L_0x001d:
                boolean r2 = r7.m22115((org.jsoup.parser.Token) r8, (org.jsoup.parser.TreeBuilder) r9)
                goto L_0x000f
            L_0x0022:
                org.jsoup.parser.Token$Comment r3 = r8.m22179()
                r9.m22093((org.jsoup.parser.Token.Comment) r3)
                goto L_0x000f
            L_0x002a:
                r9.m22071((org.jsoup.parser.HtmlTreeBuilderState) r7)
                goto L_0x000f
            L_0x002e:
                org.jsoup.parser.Token$StartTag r1 = r8.m22172()
                java.lang.String r5 = r1.m22206()
                r4 = -1
                int r6 = r5.hashCode()
                switch(r6) {
                    case 98688: goto L_0x0051;
                    case 3213227: goto L_0x0047;
                    default: goto L_0x003e;
                }
            L_0x003e:
                r3 = r4
            L_0x003f:
                switch(r3) {
                    case 0: goto L_0x005c;
                    case 1: goto L_0x0063;
                    default: goto L_0x0042;
                }
            L_0x0042:
                boolean r2 = r7.m22115((org.jsoup.parser.Token) r8, (org.jsoup.parser.TreeBuilder) r9)
                goto L_0x000f
            L_0x0047:
                java.lang.String r6 = "html"
                boolean r5 = r5.equals(r6)
                if (r5 == 0) goto L_0x003e
                goto L_0x003f
            L_0x0051:
                java.lang.String r3 = "col"
                boolean r3 = r5.equals(r3)
                if (r3 == 0) goto L_0x003e
                r3 = r2
                goto L_0x003f
            L_0x005c:
                org.jsoup.parser.HtmlTreeBuilderState r2 = InBody
                boolean r2 = r9.m22099((org.jsoup.parser.Token) r8, (org.jsoup.parser.HtmlTreeBuilderState) r2)
                goto L_0x000f
            L_0x0063:
                r9.m22067((org.jsoup.parser.Token.StartTag) r1)
                goto L_0x000f
            L_0x0067:
                org.jsoup.parser.Token$EndTag r0 = r8.m22174()
                java.lang.String r4 = r0.f17418
                java.lang.String r5 = "colgroup"
                boolean r4 = r4.equals(r5)
                if (r4 == 0) goto L_0x0096
                org.jsoup.nodes.Element r4 = r9.m22342()
                java.lang.String r4 = r4.m21861()
                java.lang.String r5 = "html"
                boolean r4 = r4.equals(r5)
                if (r4 == 0) goto L_0x008c
                r9.m22071((org.jsoup.parser.HtmlTreeBuilderState) r7)
                r2 = r3
                goto L_0x000f
            L_0x008c:
                r9.m22051()
                org.jsoup.parser.HtmlTreeBuilderState r3 = InTable
                r9.m22091((org.jsoup.parser.HtmlTreeBuilderState) r3)
                goto L_0x000f
            L_0x0096:
                boolean r2 = r7.m22115((org.jsoup.parser.Token) r8, (org.jsoup.parser.TreeBuilder) r9)
                goto L_0x000f
            L_0x009c:
                org.jsoup.nodes.Element r3 = r9.m22342()
                java.lang.String r3 = r3.m21861()
                java.lang.String r4 = "html"
                boolean r3 = r3.equals(r4)
                if (r3 != 0) goto L_0x000f
                boolean r2 = r7.m22115((org.jsoup.parser.Token) r8, (org.jsoup.parser.TreeBuilder) r9)
                goto L_0x000f
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass12.m22116(org.jsoup.parser.Token, org.jsoup.parser.HtmlTreeBuilder):boolean");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m22115(Token t, TreeBuilder tb) {
            if (tb.m22339("colgroup")) {
                return tb.m22347(t);
            }
            return true;
        }
    },
    InTableBody {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22119(Token t, HtmlTreeBuilder tb) {
            switch (AnonymousClass24.f17356[t.f17401.ordinal()]) {
                case 3:
                    Token.StartTag startTag = t.m22172();
                    String name = startTag.m22206();
                    if (name.equals("template")) {
                        tb.m22083(startTag);
                        break;
                    } else if (name.equals("tr")) {
                        tb.m22045();
                        tb.m22083(startTag);
                        tb.m22091(InRow);
                        break;
                    } else {
                        if (StringUtil.m21690(name, "th", "td")) {
                            tb.m22071((HtmlTreeBuilderState) this);
                            tb.m22340("tr");
                            return tb.m22098((Token) startTag);
                        }
                        if (StringUtil.m21690(name, "caption", "col", "colgroup", "tbody", "tfoot", "thead")) {
                            return m22117(t, tb);
                        }
                        return m22118(t, tb);
                    }
                case 4:
                    String name2 = t.m22174().m22206();
                    if (StringUtil.m21690(name2, "tbody", "tfoot", "thead")) {
                        if (tb.m22040(name2)) {
                            tb.m22045();
                            tb.m22051();
                            tb.m22091(InTable);
                            break;
                        } else {
                            tb.m22071((HtmlTreeBuilderState) this);
                            return false;
                        }
                    } else if (name2.equals("table")) {
                        return m22117(t, tb);
                    } else {
                        if (!StringUtil.m21690(name2, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML, "td", "th", "tr")) {
                            return m22118(t, tb);
                        }
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                default:
                    return m22118(t, tb);
            }
            return true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m22117(Token t, HtmlTreeBuilder tb) {
            if (tb.m22040("tbody") || tb.m22040("thead") || tb.m22064("tfoot")) {
                tb.m22045();
                tb.m22339(tb.m22342().m21861());
                return tb.m22098(t);
            }
            tb.m22071((HtmlTreeBuilderState) this);
            return false;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean m22118(Token t, HtmlTreeBuilder tb) {
            return tb.m22099(t, InTable);
        }
    },
    InRow {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22122(Token t, HtmlTreeBuilder tb) {
            if (t.m22181()) {
                Token.StartTag startTag = t.m22172();
                String name = startTag.m22206();
                if (name.equals("template")) {
                    tb.m22083(startTag);
                } else {
                    if (StringUtil.m21690(name, "th", "td")) {
                        tb.m22042();
                        tb.m22083(startTag);
                        tb.m22091(InCell);
                        tb.m22062();
                    } else {
                        if (StringUtil.m21690(name, "caption", "col", "colgroup", "tbody", "tfoot", "thead", "tr")) {
                            return m22121(t, (TreeBuilder) tb);
                        }
                        return m22120(t, tb);
                    }
                }
            } else if (!t.m22173()) {
                return m22120(t, tb);
            } else {
                String name2 = t.m22174().m22206();
                if (name2.equals("tr")) {
                    if (!tb.m22040(name2)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                    tb.m22042();
                    tb.m22051();
                    tb.m22091(InTableBody);
                } else if (name2.equals("table")) {
                    return m22121(t, (TreeBuilder) tb);
                } else {
                    if (!StringUtil.m21690(name2, "tbody", "tfoot", "thead")) {
                        if (!StringUtil.m21690(name2, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML, "td", "th")) {
                            return m22120(t, tb);
                        }
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    } else if (!tb.m22040(name2)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    } else {
                        tb.m22339("tr");
                        return tb.m22098(t);
                    }
                }
            }
            return true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m22120(Token t, HtmlTreeBuilder tb) {
            return tb.m22099(t, InTable);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m22121(Token t, TreeBuilder tb) {
            if (tb.m22339("tr")) {
                return tb.m22347(t);
            }
            return false;
        }
    },
    InCell {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22125(Token t, HtmlTreeBuilder tb) {
            if (t.m22173()) {
                String name = t.m22174().m22206();
                if (!StringUtil.m21690(name, "td", "th")) {
                    if (StringUtil.m21690(name, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                    if (!StringUtil.m21690(name, "table", "tbody", "tfoot", "thead", "tr")) {
                        return m22123(t, tb);
                    }
                    if (!tb.m22040(name)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                    m22124(tb);
                    return tb.m22098(t);
                } else if (!tb.m22040(name)) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    tb.m22091(InRow);
                    return false;
                } else {
                    tb.m22044();
                    if (!tb.m22342().m21861().equals(name)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                    }
                    tb.m22078(name);
                    tb.m22055();
                    tb.m22091(InRow);
                    return true;
                }
            } else {
                if (t.m22181()) {
                    if (StringUtil.m21690(t.m22172().m22206(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        if (tb.m22040("td") || tb.m22040("th")) {
                            m22124(tb);
                            return tb.m22098(t);
                        }
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                }
                return m22123(t, tb);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m22123(Token t, HtmlTreeBuilder tb) {
            return tb.m22099(t, InBody);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m22124(HtmlTreeBuilder tb) {
            if (tb.m22040("td")) {
                tb.m22339("td");
            } else {
                tb.m22339("th");
            }
        }
    },
    InSelect {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22127(Token t, HtmlTreeBuilder tb) {
            switch (AnonymousClass24.f17356[t.f17401.ordinal()]) {
                case 1:
                    tb.m22093(t.m22179());
                    break;
                case 2:
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                case 3:
                    Token.StartTag start = t.m22172();
                    String name = start.m22206();
                    if (name.equals(AdType.HTML)) {
                        return tb.m22099((Token) start, InBody);
                    }
                    if (name.equals("option")) {
                        if (tb.m22342().m21861().equals("option")) {
                            tb.m22339("option");
                        }
                        tb.m22083(start);
                        break;
                    } else if (name.equals("optgroup")) {
                        if (tb.m22342().m21861().equals("option")) {
                            tb.m22339("option");
                        } else if (tb.m22342().m21861().equals("optgroup")) {
                            tb.m22339("optgroup");
                        }
                        tb.m22083(start);
                        break;
                    } else if (name.equals("select")) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return tb.m22339("select");
                    } else {
                        if (StringUtil.m21690(name, "input", "keygen", "textarea")) {
                            tb.m22071((HtmlTreeBuilderState) this);
                            if (!tb.m22053("select")) {
                                return false;
                            }
                            tb.m22339("select");
                            return tb.m22098((Token) start);
                        } else if (name.equals("script")) {
                            return tb.m22099(t, InHead);
                        } else {
                            return m22126(t, tb);
                        }
                    }
                case 4:
                    String name2 = t.m22174().m22206();
                    char c = 65535;
                    switch (name2.hashCode()) {
                        case -1010136971:
                            if (name2.equals("option")) {
                                c = 1;
                                break;
                            }
                            break;
                        case -906021636:
                            if (name2.equals("select")) {
                                c = 2;
                                break;
                            }
                            break;
                        case -80773204:
                            if (name2.equals("optgroup")) {
                                c = 0;
                                break;
                            }
                            break;
                    }
                    switch (c) {
                        case 0:
                            if (tb.m22342().m21861().equals("option") && tb.m22034(tb.m22342()) != null && tb.m22034(tb.m22342()).m21861().equals("optgroup")) {
                                tb.m22339("option");
                            }
                            if (!tb.m22342().m21861().equals("optgroup")) {
                                tb.m22071((HtmlTreeBuilderState) this);
                                break;
                            } else {
                                tb.m22051();
                                break;
                            }
                        case 1:
                            if (!tb.m22342().m21861().equals("option")) {
                                tb.m22071((HtmlTreeBuilderState) this);
                                break;
                            } else {
                                tb.m22051();
                                break;
                            }
                        case 2:
                            if (tb.m22053(name2)) {
                                tb.m22078(name2);
                                tb.m22043();
                                break;
                            } else {
                                tb.m22071((HtmlTreeBuilderState) this);
                                return false;
                            }
                        default:
                            return m22126(t, tb);
                    }
                case 5:
                    Token.Character c2 = t.m22175();
                    if (!c2.m22188().equals(HtmlTreeBuilderState.f17345)) {
                        tb.m22092(c2);
                        break;
                    } else {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                case 6:
                    if (!tb.m22342().m21861().equals(AdType.HTML)) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        break;
                    }
                    break;
                default:
                    return m22126(t, tb);
            }
            return true;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m22126(Token t, HtmlTreeBuilder tb) {
            tb.m22071((HtmlTreeBuilderState) this);
            return false;
        }
    },
    InSelectInTable {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22128(Token t, HtmlTreeBuilder tb) {
            if (t.m22181()) {
                if (StringUtil.m21690(t.m22172().m22206(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    tb.m22339("select");
                    return tb.m22098(t);
                }
            }
            if (t.m22173()) {
                if (StringUtil.m21690(t.m22174().m22206(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    if (!tb.m22040(t.m22174().m22206())) {
                        return false;
                    }
                    tb.m22339("select");
                    return tb.m22098(t);
                }
            }
            return tb.m22099(t, InSelect);
        }
    },
    AfterBody {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22129(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.m22104(t)) {
                return tb.m22099(t, InBody);
            }
            if (t.m22178()) {
                tb.m22093(t.m22179());
            } else if (t.m22184()) {
                tb.m22071((HtmlTreeBuilderState) this);
                return false;
            } else if (t.m22181() && t.m22172().m22206().equals(AdType.HTML)) {
                return tb.m22099(t, InBody);
            } else {
                if (!t.m22173() || !t.m22174().m22206().equals(AdType.HTML)) {
                    if (!t.m22176()) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        tb.m22091(InBody);
                        return tb.m22098(t);
                    }
                } else if (tb.m22039()) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    tb.m22091(AfterAfterBody);
                }
            }
            return true;
        }
    },
    InFrameset {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22130(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.m22104(t)) {
                tb.m22092(t.m22175());
            } else if (t.m22178()) {
                tb.m22093(t.m22179());
            } else if (t.m22184()) {
                tb.m22071((HtmlTreeBuilderState) this);
                return false;
            } else if (t.m22181()) {
                Token.StartTag start = t.m22172();
                String r4 = start.m22206();
                char c = 65535;
                switch (r4.hashCode()) {
                    case -1644953643:
                        if (r4.equals("frameset")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 3213227:
                        if (r4.equals(AdType.HTML)) {
                            c = 0;
                            break;
                        }
                        break;
                    case 97692013:
                        if (r4.equals("frame")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1192721831:
                        if (r4.equals("noframes")) {
                            c = 3;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        return tb.m22099((Token) start, InBody);
                    case 1:
                        tb.m22083(start);
                        break;
                    case 2:
                        tb.m22067(start);
                        break;
                    case 3:
                        return tb.m22099((Token) start, InHead);
                    default:
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                }
            } else if (!t.m22173() || !t.m22174().m22206().equals("frameset")) {
                if (!t.m22176()) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                } else if (!tb.m22342().m21861().equals(AdType.HTML)) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    return true;
                }
            } else if (tb.m22342().m21861().equals(AdType.HTML)) {
                tb.m22071((HtmlTreeBuilderState) this);
                return false;
            } else {
                tb.m22051();
                if (!tb.m22039() && !tb.m22342().m21861().equals("frameset")) {
                    tb.m22091(AfterFrameset);
                }
            }
            return true;
        }
    },
    AfterFrameset {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22133(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.m22104(t)) {
                tb.m22092(t.m22175());
            } else if (t.m22178()) {
                tb.m22093(t.m22179());
            } else if (t.m22184()) {
                tb.m22071((HtmlTreeBuilderState) this);
                return false;
            } else if (t.m22181() && t.m22172().m22206().equals(AdType.HTML)) {
                return tb.m22099(t, InBody);
            } else {
                if (t.m22173() && t.m22174().m22206().equals(AdType.HTML)) {
                    tb.m22091(AfterAfterFrameset);
                } else if (t.m22181() && t.m22172().m22206().equals("noframes")) {
                    return tb.m22099(t, InHead);
                } else {
                    if (!t.m22176()) {
                        tb.m22071((HtmlTreeBuilderState) this);
                        return false;
                    }
                }
            }
            return true;
        }
    },
    AfterAfterBody {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22134(Token t, HtmlTreeBuilder tb) {
            if (t.m22178()) {
                tb.m22093(t.m22179());
            } else if (t.m22184() || HtmlTreeBuilderState.m22104(t) || (t.m22181() && t.m22172().m22206().equals(AdType.HTML))) {
                return tb.m22099(t, InBody);
            } else {
                if (!t.m22176()) {
                    tb.m22071((HtmlTreeBuilderState) this);
                    tb.m22091(InBody);
                    return tb.m22098(t);
                }
            }
            return true;
        }
    },
    AfterAfterFrameset {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22135(Token t, HtmlTreeBuilder tb) {
            if (t.m22178()) {
                tb.m22093(t.m22179());
            } else if (t.m22184() || HtmlTreeBuilderState.m22104(t) || (t.m22181() && t.m22172().m22206().equals(AdType.HTML))) {
                return tb.m22099(t, InBody);
            } else {
                if (!t.m22176()) {
                    if (t.m22181() && t.m22172().m22206().equals("noframes")) {
                        return tb.m22099(t, InHead);
                    }
                    tb.m22071((HtmlTreeBuilderState) this);
                    return false;
                }
            }
            return true;
        }
    },
    ForeignContent {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22136(Token t, HtmlTreeBuilder tb) {
            return true;
        }
    };
    
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public static String f17345;

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m22111(Token token, HtmlTreeBuilder htmlTreeBuilder);

    static {
        f17345 = String.valueOf(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m22104(Token t) {
        if (t.m22180()) {
            return m22103(t.m22175().m22188());
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m22103(String data) {
        return StringUtil.m21689(data);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static void m22106(Token.StartTag startTag, HtmlTreeBuilder tb) {
        tb.f17518.m22256(TokeniserState.Rcdata);
        tb.m22077();
        tb.m22091(Text);
        tb.m22083(startTag);
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static void m22105(Token.StartTag startTag, HtmlTreeBuilder tb) {
        tb.f17518.m22256(TokeniserState.Rawtext);
        tb.m22077();
        tb.m22091(Text);
        tb.m22083(startTag);
    }

    static final class TyphoonApp {

        /* renamed from: ʻ  reason: contains not printable characters */
        static final String[] f17357 = null;

        /* renamed from: ʼ  reason: contains not printable characters */
        static final String[] f17358 = null;

        /* renamed from: ʽ  reason: contains not printable characters */
        static final String[] f17359 = null;

        /* renamed from: ʾ  reason: contains not printable characters */
        static final String[] f17360 = null;

        /* renamed from: ʿ  reason: contains not printable characters */
        static final String[] f17361 = null;

        /* renamed from: ˈ  reason: contains not printable characters */
        static final String[] f17362 = null;

        /* renamed from: ˊ  reason: contains not printable characters */
        static final String[] f17363 = null;

        /* renamed from: ˑ  reason: contains not printable characters */
        static final String[] f17364 = null;

        /* renamed from: ٴ  reason: contains not printable characters */
        static final String[] f17365 = null;

        /* renamed from: ᐧ  reason: contains not printable characters */
        static final String[] f17366 = null;

        /* renamed from: 连任  reason: contains not printable characters */
        static final String[] f17367 = null;

        /* renamed from: 靐  reason: contains not printable characters */
        static final String[] f17368 = null;

        /* renamed from: 麤  reason: contains not printable characters */
        static final String[] f17369 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        static final String[] f17370 = null;

        /* renamed from: 龘  reason: contains not printable characters */
        static final String[] f17371 = null;

        /* renamed from: ﹶ  reason: contains not printable characters */
        static final String[] f17372 = null;

        /* renamed from: ﾞ  reason: contains not printable characters */
        static final String[] f17373 = null;

        static {
            f17371 = new String[]{"base", "basefont", "bgsound", "command", "link", "meta", "noframes", "script", TtmlNode.TAG_STYLE, PubnativeAsset.TITLE};
            f17368 = new String[]{"address", "article", "aside", "blockquote", TtmlNode.CENTER, "details", "dir", TtmlNode.TAG_DIV, "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "menu", "nav", "ol", TtmlNode.TAG_P, "section", "summary", "ul"};
            f17370 = new String[]{"h1", "h2", "h3", "h4", "h5", "h6"};
            f17369 = new String[]{"listing", "pre"};
            f17367 = new String[]{"address", TtmlNode.TAG_DIV, TtmlNode.TAG_P};
            f17357 = new String[]{"dd", "dt"};
            f17358 = new String[]{"b", "big", OAuth.OAUTH_CODE, "em", "font", "i", "s", "small", "strike", "strong", TtmlNode.TAG_TT, "u"};
            f17359 = new String[]{"applet", "marquee", "object"};
            f17364 = new String[]{"area", TtmlNode.TAG_BR, "embed", "img", "keygen", "wbr"};
            f17365 = new String[]{"param", "source", "track"};
            f17366 = new String[]{"action", "name", "prompt"};
            f17362 = new String[]{"optgroup", "option"};
            f17360 = new String[]{"rp", "rt"};
            f17361 = new String[]{"caption", "col", "colgroup", "frame", TtmlNode.TAG_HEAD, "tbody", "td", "tfoot", "th", "thead", "tr"};
            f17372 = new String[]{"address", "article", "aside", "blockquote", "button", TtmlNode.CENTER, "details", "dir", TtmlNode.TAG_DIV, "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "listing", "menu", "nav", "ol", "pre", "section", "summary", "ul"};
            f17373 = new String[]{"a", "b", "big", OAuth.OAUTH_CODE, "em", "font", "i", "nobr", "s", "small", "strike", "strong", TtmlNode.TAG_TT, "u"};
            f17363 = new String[]{"table", "tbody", "tfoot", "thead", "tr"};
        }
    }
}
