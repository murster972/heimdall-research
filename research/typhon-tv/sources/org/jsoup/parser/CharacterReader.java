package org.jsoup.parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Locale;
import org.jsoup.UncheckedIOException;
import org.jsoup.helper.Validate;

public final class CharacterReader {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f17302;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f17303;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String[] f17304;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f17305;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Reader f17306;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f17307;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f17308;

    /* renamed from: 龘  reason: contains not printable characters */
    private final char[] f17309;

    public CharacterReader(Reader input, int sz) {
        this.f17304 = new String[512];
        Validate.m21695((Object) input);
        Validate.m21699(input.markSupported());
        this.f17306 = input;
        this.f17309 = new char[(sz > 32768 ? 32768 : sz)];
        m21991();
    }

    public CharacterReader(Reader input) {
        this(input, 32768);
    }

    public CharacterReader(String input) {
        this(new StringReader(input), input.length());
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m21991() {
        int i = 24576;
        if (this.f17305 >= this.f17307) {
            try {
                this.f17306.skip((long) this.f17305);
                this.f17306.mark(32768);
                int read = this.f17306.read(this.f17309);
                this.f17306.reset();
                if (read != -1) {
                    this.f17308 = read;
                    this.f17302 += this.f17305;
                    this.f17305 = 0;
                    this.f17303 = 0;
                    if (this.f17308 <= 24576) {
                        i = this.f17308;
                    }
                    this.f17307 = i;
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22019() {
        return this.f17302 + this.f17305;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m22010() {
        m21991();
        return this.f17305 >= this.f17308;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean m21992() {
        return this.f17305 >= this.f17308;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public char m22015() {
        m21991();
        if (m21992()) {
            return 65535;
        }
        return this.f17309[this.f17305];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public char m22012() {
        m21991();
        char val = m21992() ? 65535 : this.f17309[this.f17305];
        this.f17305++;
        return val;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m22006() {
        this.f17305--;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m21995() {
        this.f17305++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m21997() {
        this.f17303 = this.f17305;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m21998() {
        this.f17305 = this.f17303;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22020(char c) {
        m21991();
        for (int i = this.f17305; i < this.f17308; i++) {
            if (c == this.f17309[i]) {
                return i - this.f17305;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22021(CharSequence seq) {
        m21991();
        char startChar = seq.charAt(0);
        int offset = this.f17305;
        while (offset < this.f17308) {
            if (startChar != this.f17309[offset]) {
                do {
                    offset++;
                    if (offset >= this.f17308) {
                        break;
                    }
                } while (startChar == this.f17309[offset]);
            }
            int i = offset + 1;
            int last = (seq.length() + i) - 1;
            if (offset < this.f17308 && last <= this.f17308) {
                int j = 1;
                while (i < last && seq.charAt(j) == this.f17309[i]) {
                    i++;
                    j++;
                }
                if (i == last) {
                    return offset - this.f17305;
                }
            }
            offset++;
        }
        return -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m22008(char c) {
        int offset = m22020(c);
        if (offset == -1) {
            return m22005();
        }
        String consumed = m21993(this.f17309, this.f17304, this.f17305, offset);
        this.f17305 += offset;
        return consumed;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m22022(String seq) {
        int offset = m22021((CharSequence) seq);
        if (offset == -1) {
            return m22005();
        }
        String consumed = m21993(this.f17309, this.f17304, this.f17305, offset);
        this.f17305 += offset;
        return consumed;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m22023(char... chars) {
        m21991();
        int start = this.f17305;
        int remaining = this.f17308;
        char[] val = this.f17309;
        loop0:
        while (this.f17305 < remaining) {
            for (char c : chars) {
                if (val[this.f17305] == c) {
                    break loop0;
                }
            }
            this.f17305++;
        }
        if (this.f17305 > start) {
            return m21993(this.f17309, this.f17304, start, this.f17305 - start);
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m22009(char... chars) {
        m21991();
        int start = this.f17305;
        int remaining = this.f17308;
        char[] val = this.f17309;
        while (this.f17305 < remaining && Arrays.binarySearch(chars, val[this.f17305]) < 0) {
            this.f17305++;
        }
        return this.f17305 > start ? m21993(this.f17309, this.f17304, start, this.f17305 - start) : "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public String m22003() {
        m21991();
        int start = this.f17305;
        int remaining = this.f17308;
        char[] val = this.f17309;
        while (this.f17305 < remaining && (c = val[this.f17305]) != '&' && c != '<' && c != 0) {
            this.f17305++;
        }
        return this.f17305 > start ? m21993(this.f17309, this.f17304, start, this.f17305 - start) : "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public String m22004() {
        m21991();
        int start = this.f17305;
        int remaining = this.f17308;
        char[] val = this.f17309;
        while (this.f17305 < remaining && (c = val[this.f17305]) != 9 && c != 10 && c != 13 && c != 12 && c != ' ' && c != '/' && c != '>' && c != 0) {
            this.f17305++;
        }
        return this.f17305 > start ? m21993(this.f17309, this.f17304, start, this.f17305 - start) : "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public String m22005() {
        m21991();
        String data = m21993(this.f17309, this.f17304, this.f17305, this.f17308 - this.f17305);
        this.f17305 = this.f17308;
        return data;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public String m22001() {
        m21991();
        int start = this.f17305;
        while (this.f17305 < this.f17308 && (((c = this.f17309[this.f17305]) >= 'A' && c <= 'Z') || ((c >= 'a' && c <= 'z') || Character.isLetter(c)))) {
            this.f17305++;
        }
        return m21993(this.f17309, this.f17304, start, this.f17305 - start);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public String m21999() {
        m21991();
        int start = this.f17305;
        while (this.f17305 < this.f17308 && (((c = this.f17309[this.f17305]) >= 'A' && c <= 'Z') || ((c >= 'a' && c <= 'z') || Character.isLetter(c)))) {
            this.f17305++;
        }
        while (!m21992() && (c = this.f17309[this.f17305]) >= '0' && c <= '9') {
            this.f17305++;
        }
        return m21993(this.f17309, this.f17304, start, this.f17305 - start);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public String m22000() {
        m21991();
        int start = this.f17305;
        while (this.f17305 < this.f17308 && (((c = this.f17309[this.f17305]) >= '0' && c <= '9') || ((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f')))) {
            this.f17305++;
        }
        return m21993(this.f17309, this.f17304, start, this.f17305 - start);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public String m22024() {
        m21991();
        int start = this.f17305;
        while (this.f17305 < this.f17308 && (c = this.f17309[this.f17305]) >= '0' && c <= '9') {
            this.f17305++;
        }
        return m21993(this.f17309, this.f17304, start, this.f17305 - start);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m22016(char c) {
        return !m22010() && this.f17309[this.f17305] == c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m22011(String seq) {
        m21991();
        int scanLength = seq.length();
        if (scanLength > this.f17308 - this.f17305) {
            return false;
        }
        for (int offset = 0; offset < scanLength; offset++) {
            if (seq.charAt(offset) != this.f17309[this.f17305 + offset]) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m22017(String seq) {
        m21991();
        int scanLength = seq.length();
        if (scanLength > this.f17308 - this.f17305) {
            return false;
        }
        for (int offset = 0; offset < scanLength; offset++) {
            if (Character.toUpperCase(seq.charAt(offset)) != Character.toUpperCase(this.f17309[this.f17305 + offset])) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m22018(char... seq) {
        if (m22010()) {
            return false;
        }
        m21991();
        char c = this.f17309[this.f17305];
        for (char seek : seq) {
            if (seek == c) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m22014(char[] seq) {
        m21991();
        return !m22010() && Arrays.binarySearch(seq, this.f17309[this.f17305]) >= 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m22025() {
        if (m22010()) {
            return false;
        }
        char c = this.f17309[this.f17305];
        if ((c < 'A' || c > 'Z') && ((c < 'a' || c > 'z') && !Character.isLetter(c))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m22002() {
        char c;
        if (!m22010() && (c = this.f17309[this.f17305]) >= '0' && c <= '9') {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m22013(String seq) {
        m21991();
        if (!m22011(seq)) {
            return false;
        }
        this.f17305 += seq.length();
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22007(String seq) {
        if (!m22017(seq)) {
            return false;
        }
        this.f17305 += seq.length();
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m21996(String seq) {
        return m22021((CharSequence) seq.toLowerCase(Locale.ENGLISH)) > -1 || m22021((CharSequence) seq.toUpperCase(Locale.ENGLISH)) > -1;
    }

    public String toString() {
        return new String(this.f17309, this.f17305, this.f17308 - this.f17305);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m21993(char[] charBuf, String[] stringCache, int start, int count) {
        if (count > 12) {
            return new String(charBuf, start, count);
        }
        if (count < 1) {
            return "";
        }
        int hash = 0;
        int i = 0;
        int offset = start;
        while (i < count) {
            hash = (hash * 31) + charBuf[offset];
            i++;
            offset++;
        }
        int index = hash & (stringCache.length - 1);
        String cached = stringCache[index];
        if (cached == null) {
            String cached2 = new String(charBuf, start, count);
            stringCache[index] = cached2;
            return cached2;
        } else if (m21994(charBuf, start, count, cached)) {
            return cached;
        } else {
            String cached3 = new String(charBuf, start, count);
            stringCache[index] = cached3;
            return cached3;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m21994(char[] charBuf, int start, int count, String cached) {
        if (count != cached.length()) {
            return false;
        }
        int i = start;
        int j = 0;
        while (true) {
            int j2 = j;
            int i2 = i;
            int count2 = count;
            count = count2 - 1;
            if (count2 == 0) {
                return true;
            }
            i = i2 + 1;
            j = j2 + 1;
            if (charBuf[i2] != cached.charAt(j2)) {
                return false;
            }
        }
    }
}
