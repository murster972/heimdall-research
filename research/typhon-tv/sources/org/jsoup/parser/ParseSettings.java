package org.jsoup.parser;

import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Attributes;

public class ParseSettings {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ParseSettings f17376 = new ParseSettings(true, true);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ParseSettings f17377 = new ParseSettings(false, false);

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f17378;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f17379;

    public ParseSettings(boolean tag, boolean attribute) {
        this.f17379 = tag;
        this.f17378 = attribute;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m22150(String name) {
        String name2 = name.trim();
        if (!this.f17379) {
            return Normalizer.m21704(name2);
        }
        return name2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Attributes m22151(Attributes attributes) {
        if (!this.f17378) {
            attributes.m21725();
        }
        return attributes;
    }
}
