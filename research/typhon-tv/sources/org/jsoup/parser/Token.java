package org.jsoup.parser;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Attributes;

abstract class Token {

    /* renamed from: 龘  reason: contains not printable characters */
    TokenType f17401;

    public enum TokenType {
        Doctype,
        StartTag,
        EndTag,
        Comment,
        Character,
        EOF
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract Token m22182();

    private Token() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m22185() {
        return getClass().getSimpleName();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m22171(StringBuilder sb) {
        if (sb != null) {
            sb.delete(0, sb.length());
        }
    }

    static final class Doctype extends Token {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f17405 = false;

        /* renamed from: 连任  reason: contains not printable characters */
        final StringBuilder f17406 = new StringBuilder();

        /* renamed from: 靐  reason: contains not printable characters */
        final StringBuilder f17407 = new StringBuilder();

        /* renamed from: 麤  reason: contains not printable characters */
        final StringBuilder f17408 = new StringBuilder();

        /* renamed from: 齉  reason: contains not printable characters */
        String f17409 = null;

        Doctype() {
            super();
            this.f17401 = TokenType.Doctype;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m22194() {
            m22171(this.f17407);
            this.f17409 = null;
            m22171(this.f17408);
            m22171(this.f17406);
            this.f17405 = false;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public String m22195() {
            return this.f17407.toString();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﾞ  reason: contains not printable characters */
        public String m22196() {
            return this.f17409;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public String m22191() {
            return this.f17408.toString();
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public String m22192() {
            return this.f17406.toString();
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public boolean m22193() {
            return this.f17405;
        }
    }

    static abstract class Tag extends Token {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f17410;

        /* renamed from: ʼ  reason: contains not printable characters */
        private StringBuilder f17411 = new StringBuilder();

        /* renamed from: ʽ  reason: contains not printable characters */
        private String f17412;

        /* renamed from: ˑ  reason: contains not printable characters */
        private boolean f17413 = false;

        /* renamed from: ٴ  reason: contains not printable characters */
        private boolean f17414 = false;

        /* renamed from: 连任  reason: contains not printable characters */
        Attributes f17415;

        /* renamed from: 靐  reason: contains not printable characters */
        protected String f17416;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f17417 = false;

        /* renamed from: 齉  reason: contains not printable characters */
        protected String f17418;

        Tag() {
            super();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public Tag m22208() {
            this.f17416 = null;
            this.f17418 = null;
            this.f17410 = null;
            m22171(this.f17411);
            this.f17412 = null;
            this.f17413 = false;
            this.f17414 = false;
            this.f17417 = false;
            this.f17415 = null;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﾞ  reason: contains not printable characters */
        public final void m22218() {
            String value;
            if (this.f17415 == null) {
                this.f17415 = new Attributes();
            }
            if (this.f17410 != null) {
                this.f17410 = this.f17410.trim();
                if (this.f17410.length() > 0) {
                    if (this.f17414) {
                        value = this.f17411.length() > 0 ? this.f17411.toString() : this.f17412;
                    } else if (this.f17413) {
                        value = "";
                    } else {
                        value = null;
                    }
                    this.f17415.m21735(this.f17410, value);
                }
            }
            this.f17410 = null;
            this.f17413 = false;
            this.f17414 = false;
            m22171(this.f17411);
            this.f17412 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m22204() {
            if (this.f17410 != null) {
                m22218();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m22205() {
            Validate.m21693(this.f17416 == null || this.f17416.length() == 0);
            return this.f17416;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final String m22206() {
            return this.f17418;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final Tag m22214(String name) {
            this.f17416 = name;
            this.f17418 = Normalizer.m21704(name);
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public final boolean m22202() {
            return this.f17417;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˉ  reason: contains not printable characters */
        public final Attributes m22203() {
            return this.f17415;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m22210(String append) {
            if (this.f17416 != null) {
                append = this.f17416.concat(append);
            }
            this.f17416 = append;
            this.f17418 = Normalizer.m21704(this.f17416);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m22215(char append) {
            m22210(String.valueOf(append));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public final void m22213(String append) {
            if (this.f17410 != null) {
                append = this.f17410.concat(append);
            }
            this.f17410 = append;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m22209(char append) {
            m22213(String.valueOf(append));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public final void m22211(String append) {
            m22201();
            if (this.f17411.length() == 0) {
                this.f17412 = append;
            } else {
                this.f17411.append(append);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public final void m22212(char append) {
            m22201();
            this.f17411.append(append);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m22216(int[] appendCodepoints) {
            m22201();
            for (int codepoint : appendCodepoints) {
                this.f17411.appendCodePoint(codepoint);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˏ  reason: contains not printable characters */
        public final void m22207() {
            this.f17413 = true;
        }

        /* renamed from: י  reason: contains not printable characters */
        private void m22201() {
            this.f17414 = true;
            if (this.f17412 != null) {
                this.f17411.append(this.f17412);
                this.f17412 = null;
            }
        }
    }

    static final class StartTag extends Tag {
        StartTag() {
            this.f17415 = new Attributes();
            this.f17401 = TokenType.StartTag;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public Tag m22198() {
            super.m22208();
            this.f17415 = new Attributes();
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public StartTag m22199(String name, Attributes attributes) {
            this.f17416 = name;
            this.f17415 = attributes;
            this.f17418 = Normalizer.m21704(this.f17416);
            return this;
        }

        public String toString() {
            if (this.f17415 == null || this.f17415.m21733() <= 0) {
                return "<" + m22205() + ">";
            }
            return "<" + m22205() + StringUtils.SPACE + this.f17415.toString() + ">";
        }
    }

    static final class EndTag extends Tag {
        EndTag() {
            this.f17401 = TokenType.EndTag;
        }

        public String toString() {
            return "</" + m22205() + ">";
        }
    }

    static final class Comment extends Token {

        /* renamed from: 靐  reason: contains not printable characters */
        final StringBuilder f17403 = new StringBuilder();

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f17404 = false;

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m22189() {
            m22171(this.f17403);
            this.f17404 = false;
            return this;
        }

        Comment() {
            super();
            this.f17401 = TokenType.Comment;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public String m22190() {
            return this.f17403.toString();
        }

        public String toString() {
            return "<!--" + m22190() + "-->";
        }
    }

    static class Character extends Token {

        /* renamed from: 靐  reason: contains not printable characters */
        private String f17402;

        Character() {
            super();
            this.f17401 = TokenType.Character;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m22186() {
            this.f17402 = null;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Character m22187(String data) {
            this.f17402 = data;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public String m22188() {
            return this.f17402;
        }

        public String toString() {
            return m22188();
        }
    }

    static final class CData extends Character {
        CData(String data) {
            m22187(data);
        }

        public String toString() {
            return "<![CDATA[" + m22188() + "]]>";
        }
    }

    static final class EOF extends Token {
        EOF() {
            super();
            this.f17401 = TokenType.EOF;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m22197() {
            return this;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m22184() {
        return this.f17401 == TokenType.Doctype;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final Doctype m22183() {
        return (Doctype) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m22181() {
        return this.f17401 == TokenType.StartTag;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final StartTag m22172() {
        return (StartTag) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m22173() {
        return this.f17401 == TokenType.EndTag;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final EndTag m22174() {
        return (EndTag) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m22178() {
        return this.f17401 == TokenType.Comment;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final Comment m22179() {
        return (Comment) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m22180() {
        return this.f17401 == TokenType.Character;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final boolean m22177() {
        return this instanceof CData;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public final Character m22175() {
        return (Character) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public final boolean m22176() {
        return this.f17401 == TokenType.EOF;
    }
}
