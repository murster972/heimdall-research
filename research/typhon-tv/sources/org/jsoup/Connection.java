package org.jsoup;

public interface Connection {

    public enum Method {
        GET(false),
        POST(true),
        PUT(true),
        DELETE(false),
        PATCH(true),
        HEAD(false),
        OPTIONS(false),
        TRACE(false);
        
        private final boolean hasBody;

        private Method(boolean hasBody2) {
            this.hasBody = hasBody2;
        }

        public final boolean hasBody() {
            return this.hasBody;
        }
    }
}
