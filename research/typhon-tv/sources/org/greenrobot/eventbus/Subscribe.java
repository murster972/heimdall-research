package org.greenrobot.eventbus;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Subscribe {
    /* renamed from: 靐  reason: contains not printable characters */
    boolean m7292() default false;

    /* renamed from: 齉  reason: contains not printable characters */
    int m7293() default 0;

    /* renamed from: 龘  reason: contains not printable characters */
    ThreadMode m7294() default ThreadMode.POSTING;
}
