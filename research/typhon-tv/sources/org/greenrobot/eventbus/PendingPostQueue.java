package org.greenrobot.eventbus;

final class PendingPostQueue {

    /* renamed from: 靐  reason: contains not printable characters */
    private PendingPost f16724;

    /* renamed from: 龘  reason: contains not printable characters */
    private PendingPost f16725;

    PendingPostQueue() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m20842(PendingPost pendingPost) {
        if (pendingPost == null) {
            throw new NullPointerException("null cannot be enqueued");
        }
        if (this.f16724 != null) {
            this.f16724.f16722 = pendingPost;
            this.f16724 = pendingPost;
        } else if (this.f16725 == null) {
            this.f16724 = pendingPost;
            this.f16725 = pendingPost;
        } else {
            throw new IllegalStateException("Head present, but no tail");
        }
        notifyAll();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized PendingPost m20840() {
        PendingPost pendingPost;
        pendingPost = this.f16725;
        if (this.f16725 != null) {
            this.f16725 = this.f16725.f16722;
            if (this.f16725 == null) {
                this.f16724 = null;
            }
        }
        return pendingPost;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized PendingPost m20841(int maxMillisToWait) throws InterruptedException {
        if (this.f16725 == null) {
            wait((long) maxMillisToWait);
        }
        return m20840();
    }
}
