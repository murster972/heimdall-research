package org.greenrobot.eventbus;

import com.evernote.android.state.StateSaver;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.greenrobot.eventbus.meta.SubscriberInfo;
import org.greenrobot.eventbus.meta.SubscriberInfoIndex;

class SubscriberMethodFinder {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final FindState[] f16736 = new FindState[4];

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Map<Class<?>, List<SubscriberMethod>> f16737 = new ConcurrentHashMap();

    /* renamed from: 靐  reason: contains not printable characters */
    private List<SubscriberInfoIndex> f16738;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f16739;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f16740;

    SubscriberMethodFinder(List<SubscriberInfoIndex> subscriberInfoIndexes, boolean strictMethodVerification, boolean ignoreGeneratedIndex) {
        this.f16738 = subscriberInfoIndexes;
        this.f16740 = strictMethodVerification;
        this.f16739 = ignoreGeneratedIndex;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<SubscriberMethod> m20850(Class<?> subscriberClass) {
        List<SubscriberMethod> subscriberMethods;
        List<SubscriberMethod> subscriberMethods2 = f16737.get(subscriberClass);
        if (subscriberMethods2 != null) {
            List<SubscriberMethod> list = subscriberMethods2;
            return subscriberMethods2;
        }
        if (this.f16739) {
            subscriberMethods = m20846(subscriberClass);
        } else {
            subscriberMethods = m20844(subscriberClass);
        }
        if (subscriberMethods.isEmpty()) {
            throw new EventBusException("Subscriber " + subscriberClass + " and its super classes have no public methods with the @Subscribe annotation");
        }
        f16737.put(subscriberClass, subscriberMethods);
        List<SubscriberMethod> list2 = subscriberMethods;
        return subscriberMethods;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private List<SubscriberMethod> m20844(Class<?> subscriberClass) {
        FindState findState = m20849();
        findState.m20854(subscriberClass);
        while (findState.f16741 != null) {
            findState.f16743 = m20845(findState);
            if (findState.f16743 != null) {
                for (SubscriberMethod subscriberMethod : findState.f16743.m20856()) {
                    if (findState.m20855(subscriberMethod.f16735, subscriberMethod.f16734)) {
                        findState.f16748.add(subscriberMethod);
                    }
                }
            } else {
                m20847(findState);
            }
            findState.m20852();
        }
        return m20848(findState);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<SubscriberMethod> m20848(FindState findState) {
        List<SubscriberMethod> subscriberMethods = new ArrayList<>(findState.f16748);
        findState.m20853();
        synchronized (f16736) {
            int i = 0;
            while (true) {
                if (i >= 4) {
                    break;
                } else if (f16736[i] == null) {
                    f16736[i] = findState;
                    break;
                } else {
                    i++;
                }
            }
        }
        return subscriberMethods;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private FindState m20849() {
        synchronized (f16736) {
            for (int i = 0; i < 4; i++) {
                FindState state = f16736[i];
                if (state != null) {
                    f16736[i] = null;
                    return state;
                }
            }
            return new FindState();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private SubscriberInfo m20845(FindState findState) {
        if (!(findState.f16743 == null || findState.f16743.m20857() == null)) {
            SubscriberInfo superclassInfo = findState.f16743.m20857();
            if (findState.f16741 == superclassInfo.m20858()) {
                return superclassInfo;
            }
        }
        if (this.f16738 != null) {
            for (SubscriberInfoIndex index : this.f16738) {
                SubscriberInfo info = index.m20859(findState.f16741);
                if (info != null) {
                    return info;
                }
            }
        }
        return null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private List<SubscriberMethod> m20846(Class<?> subscriberClass) {
        FindState findState = m20849();
        findState.m20854(subscriberClass);
        while (findState.f16741 != null) {
            m20847(findState);
            findState.m20852();
        }
        return m20848(findState);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m20847(FindState findState) {
        Method[] methods;
        try {
            methods = findState.f16741.getDeclaredMethods();
        } catch (Throwable th) {
            methods = findState.f16741.getMethods();
            findState.f16742 = true;
        }
        for (Method method : methods) {
            int modifiers = method.getModifiers();
            if ((modifiers & 1) != 0 && (modifiers & 5192) == 0) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 1) {
                    Subscribe subscribeAnnotation = (Subscribe) method.getAnnotation(Subscribe.class);
                    if (subscribeAnnotation != null) {
                        Class<?> eventType = parameterTypes[0];
                        if (findState.m20855(method, eventType)) {
                            findState.f16748.add(new SubscriberMethod(method, eventType, subscribeAnnotation.m7294(), subscribeAnnotation.m7293(), subscribeAnnotation.m7292()));
                        }
                    }
                } else if (this.f16740 && method.isAnnotationPresent(Subscribe.class)) {
                    throw new EventBusException("@Subscribe method " + (method.getDeclaringClass().getName() + "." + method.getName()) + "must have exactly 1 parameter but has " + parameterTypes.length);
                }
            } else if (this.f16740 && method.isAnnotationPresent(Subscribe.class)) {
                throw new EventBusException((method.getDeclaringClass().getName() + "." + method.getName()) + " is a illegal @Subscribe method: must be public, non-static, and non-abstract");
            }
        }
    }

    static class FindState {

        /* renamed from: ʻ  reason: contains not printable characters */
        Class<?> f16741;

        /* renamed from: ʼ  reason: contains not printable characters */
        boolean f16742;

        /* renamed from: ʽ  reason: contains not printable characters */
        SubscriberInfo f16743;

        /* renamed from: 连任  reason: contains not printable characters */
        Class<?> f16744;

        /* renamed from: 靐  reason: contains not printable characters */
        final Map<Class, Object> f16745 = new HashMap();

        /* renamed from: 麤  reason: contains not printable characters */
        final StringBuilder f16746 = new StringBuilder(128);

        /* renamed from: 齉  reason: contains not printable characters */
        final Map<String, Class> f16747 = new HashMap();

        /* renamed from: 龘  reason: contains not printable characters */
        final List<SubscriberMethod> f16748 = new ArrayList();

        FindState() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20854(Class<?> subscriberClass) {
            this.f16741 = subscriberClass;
            this.f16744 = subscriberClass;
            this.f16742 = false;
            this.f16743 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20853() {
            this.f16748.clear();
            this.f16745.clear();
            this.f16747.clear();
            this.f16746.setLength(0);
            this.f16744 = null;
            this.f16741 = null;
            this.f16742 = false;
            this.f16743 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20855(Method method, Class<?> eventType) {
            Object existing = this.f16745.put(eventType, method);
            if (existing == null) {
                return true;
            }
            if (existing instanceof Method) {
                if (!m20851((Method) existing, eventType)) {
                    throw new IllegalStateException();
                }
                this.f16745.put(eventType, this);
            }
            return m20851(method, eventType);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m20851(Method method, Class<?> eventType) {
            this.f16746.setLength(0);
            this.f16746.append(method.getName());
            this.f16746.append('>').append(eventType.getName());
            String methodKey = this.f16746.toString();
            Class<?> methodClass = method.getDeclaringClass();
            Class<?> methodClassOld = this.f16747.put(methodKey, methodClass);
            if (methodClassOld == null || methodClassOld.isAssignableFrom(methodClass)) {
                return true;
            }
            this.f16747.put(methodKey, methodClassOld);
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m20852() {
            if (this.f16742) {
                this.f16741 = null;
                return;
            }
            this.f16741 = this.f16741.getSuperclass();
            String clazzName = this.f16741.getName();
            if (clazzName.startsWith(StateSaver.JAVA_PREFIX) || clazzName.startsWith("javax.") || clazzName.startsWith(StateSaver.ANDROID_PREFIX)) {
                this.f16741 = null;
            }
        }
    }
}
