package org.greenrobot.eventbus;

public final class SubscriberExceptionEvent {

    /* renamed from: 靐  reason: contains not printable characters */
    public final Throwable f16726;

    /* renamed from: 麤  reason: contains not printable characters */
    public final Object f16727;

    /* renamed from: 齉  reason: contains not printable characters */
    public final Object f16728;

    /* renamed from: 龘  reason: contains not printable characters */
    public final EventBus f16729;

    public SubscriberExceptionEvent(EventBus eventBus, Throwable throwable, Object causingEvent, Object causingSubscriber) {
        this.f16729 = eventBus;
        this.f16726 = throwable;
        this.f16728 = causingEvent;
        this.f16727 = causingSubscriber;
    }
}
