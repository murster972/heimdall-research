package org.greenrobot.eventbus;

public final class NoSubscriberEvent {

    /* renamed from: 靐  reason: contains not printable characters */
    public final Object f16718;

    /* renamed from: 龘  reason: contains not printable characters */
    public final EventBus f16719;

    public NoSubscriberEvent(EventBus eventBus, Object originalEvent) {
        this.f16719 = eventBus;
        this.f16718 = originalEvent;
    }
}
