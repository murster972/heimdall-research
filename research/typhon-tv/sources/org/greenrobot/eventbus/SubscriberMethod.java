package org.greenrobot.eventbus;

import java.lang.reflect.Method;

public class SubscriberMethod {

    /* renamed from: ʻ  reason: contains not printable characters */
    String f16730;

    /* renamed from: 连任  reason: contains not printable characters */
    final boolean f16731;

    /* renamed from: 靐  reason: contains not printable characters */
    final ThreadMode f16732;

    /* renamed from: 麤  reason: contains not printable characters */
    final int f16733;

    /* renamed from: 齉  reason: contains not printable characters */
    final Class<?> f16734;

    /* renamed from: 龘  reason: contains not printable characters */
    final Method f16735;

    public SubscriberMethod(Method method, Class<?> eventType, ThreadMode threadMode, int priority, boolean sticky) {
        this.f16735 = method;
        this.f16732 = threadMode;
        this.f16734 = eventType;
        this.f16733 = priority;
        this.f16731 = sticky;
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SubscriberMethod)) {
            return false;
        }
        m20843();
        SubscriberMethod otherSubscriberMethod = (SubscriberMethod) other;
        otherSubscriberMethod.m20843();
        return this.f16730.equals(otherSubscriberMethod.f16730);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private synchronized void m20843() {
        if (this.f16730 == null) {
            StringBuilder builder = new StringBuilder(64);
            builder.append(this.f16735.getDeclaringClass().getName());
            builder.append('#').append(this.f16735.getName());
            builder.append('(').append(this.f16734.getName());
            this.f16730 = builder.toString();
        }
    }

    public int hashCode() {
        return this.f16735.hashCode();
    }
}
