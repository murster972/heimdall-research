package org.greenrobot.eventbus;

final class Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    final SubscriberMethod f16749;

    /* renamed from: 齉  reason: contains not printable characters */
    volatile boolean f16750 = true;

    /* renamed from: 龘  reason: contains not printable characters */
    final Object f16751;

    Subscription(Object subscriber, SubscriberMethod subscriberMethod) {
        this.f16751 = subscriber;
        this.f16749 = subscriberMethod;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Subscription)) {
            return false;
        }
        Subscription otherSubscription = (Subscription) other;
        if (this.f16751 != otherSubscription.f16751 || !this.f16749.equals(otherSubscription.f16749)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.f16751.hashCode() + this.f16749.f16730.hashCode();
    }
}
