package org.greenrobot.eventbus;

class AsyncPoster implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final EventBus f16670;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PendingPostQueue f16671 = new PendingPostQueue();

    AsyncPoster(EventBus eventBus) {
        this.f16670 = eventBus;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20810(Subscription subscription, Object event) {
        this.f16671.m20842(PendingPost.m20838(subscription, event));
        this.f16670.m20829().execute(this);
    }

    public void run() {
        PendingPost pendingPost = this.f16671.m20840();
        if (pendingPost == null) {
            throw new IllegalStateException("No pending post available");
        }
        this.f16670.m20834(pendingPost);
    }
}
