package org.greenrobot.eventbus;

import android.os.Looper;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

public class EventBus {

    /* renamed from: 靐  reason: contains not printable characters */
    static volatile EventBus f16675;

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Map<Class<?>, List<Class<?>>> f16676 = new HashMap();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final EventBusBuilder f16677 = new EventBusBuilder();

    /* renamed from: 龘  reason: contains not printable characters */
    public static String f16678 = "EventBus";

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Map<Object, List<Class<?>>> f16679;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Map<Class<?>, Object> f16680;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final ThreadLocal<PostingThreadState> f16681;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final ExecutorService f16682;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final boolean f16683;

    /* renamed from: ˆ  reason: contains not printable characters */
    private final int f16684;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final SubscriberMethodFinder f16685;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final boolean f16686;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final boolean f16687;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final boolean f16688;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final HandlerPoster f16689;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final BackgroundPoster f16690;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final AsyncPoster f16691;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Map<Class<?>, CopyOnWriteArrayList<Subscription>> f16692;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final boolean f16693;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final boolean f16694;

    /* renamed from: 龘  reason: contains not printable characters */
    public static EventBus m20814() {
        if (f16675 == null) {
            synchronized (EventBus.class) {
                if (f16675 == null) {
                    f16675 = new EventBus();
                }
            }
        }
        return f16675;
    }

    public EventBus() {
        this(f16677);
    }

    EventBus(EventBusBuilder builder) {
        this.f16681 = new ThreadLocal<PostingThreadState>() {
            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public PostingThreadState initialValue() {
                return new PostingThreadState();
            }
        };
        this.f16692 = new HashMap();
        this.f16679 = new HashMap();
        this.f16680 = new ConcurrentHashMap();
        this.f16689 = new HandlerPoster(this, Looper.getMainLooper(), 10);
        this.f16690 = new BackgroundPoster(this);
        this.f16691 = new AsyncPoster(this);
        this.f16684 = builder.f16708 != null ? builder.f16708.size() : 0;
        this.f16685 = new SubscriberMethodFinder(builder.f16708, builder.f16706, builder.f16705);
        this.f16693 = builder.f16713;
        this.f16694 = builder.f16710;
        this.f16686 = builder.f16712;
        this.f16687 = builder.f16711;
        this.f16683 = builder.f16709;
        this.f16688 = builder.f16704;
        this.f16682 = builder.f16707;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20833(Object subscriber) {
        List<SubscriberMethod> subscriberMethods = this.f16685.m20850(subscriber.getClass());
        synchronized (this) {
            for (SubscriberMethod subscriberMethod : subscriberMethods) {
                m20817(subscriber, subscriberMethod);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20817(Object subscriber, SubscriberMethod subscriberMethod) {
        Class<?> eventType = subscriberMethod.f16734;
        Subscription newSubscription = new Subscription(subscriber, subscriberMethod);
        CopyOnWriteArrayList<Subscription> subscriptions = this.f16692.get(eventType);
        if (subscriptions == null) {
            subscriptions = new CopyOnWriteArrayList<>();
            this.f16692.put(eventType, subscriptions);
        } else if (subscriptions.contains(newSubscription)) {
            throw new EventBusException("Subscriber " + subscriber.getClass() + " already registered to event " + eventType);
        }
        int size = subscriptions.size();
        int i = 0;
        while (true) {
            if (i > size) {
                break;
            } else if (i == size || subscriberMethod.f16733 > subscriptions.get(i).f16749.f16733) {
                subscriptions.add(i, newSubscription);
            } else {
                i++;
            }
        }
        subscriptions.add(i, newSubscription);
        List<Class<?>> subscribedEvents = this.f16679.get(subscriber);
        if (subscribedEvents == null) {
            subscribedEvents = new ArrayList<>();
            this.f16679.put(subscriber, subscribedEvents);
        }
        subscribedEvents.add(eventType);
        if (!subscriberMethod.f16731) {
            return;
        }
        if (this.f16688) {
            for (Map.Entry<Class<?>, Object> entry : this.f16680.entrySet()) {
                if (eventType.isAssignableFrom(entry.getKey())) {
                    m20812(newSubscription, entry.getValue());
                }
            }
            return;
        }
        m20812(newSubscription, this.f16680.get(eventType));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20812(Subscription newSubscription, Object stickyEvent) {
        if (stickyEvent != null) {
            m20820(newSubscription, stickyEvent, Looper.getMainLooper() == Looper.myLooper());
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized boolean m20827(Object subscriber) {
        return this.f16679.containsKey(subscriber);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20815(Object subscriber, Class<?> eventType) {
        List<Subscription> subscriptions = this.f16692.get(eventType);
        if (subscriptions != null) {
            int size = subscriptions.size();
            int i = 0;
            while (i < size) {
                Subscription subscription = subscriptions.get(i);
                if (subscription.f16751 == subscriber) {
                    subscription.f16750 = false;
                    subscriptions.remove(i);
                    i--;
                    size--;
                }
                i++;
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m20830(Object subscriber) {
        List<Class<?>> subscribedTypes = this.f16679.get(subscriber);
        if (subscribedTypes != null) {
            for (Class<?> eventType : subscribedTypes) {
                m20815(subscriber, eventType);
            }
            this.f16679.remove(subscriber);
        } else {
            Log.w(f16678, "Subscriber to unregister was not registered before: " + subscriber.getClass());
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m20828(Object event) {
        boolean z;
        PostingThreadState postingState = this.f16681.get();
        List<Object> eventQueue = postingState.f16702;
        eventQueue.add(event);
        if (!postingState.f16699) {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                z = true;
            } else {
                z = false;
            }
            postingState.f16701 = z;
            postingState.f16699 = true;
            if (postingState.f16697) {
                throw new EventBusException("Internal error. Abort state was not reset");
            }
            while (!eventQueue.isEmpty()) {
                try {
                    m20816(eventQueue.remove(0), postingState);
                } finally {
                    postingState.f16699 = false;
                    postingState.f16701 = false;
                }
            }
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m20824(Object event) {
        PostingThreadState postingState = this.f16681.get();
        if (!postingState.f16699) {
            throw new EventBusException("This method may only be called from inside event handling methods on the posting thread");
        } else if (event == null) {
            throw new EventBusException("Event may not be null");
        } else if (postingState.f16698 != event) {
            throw new EventBusException("Only the currently handled event may be aborted");
        } else if (postingState.f16700.f16749.f16732 != ThreadMode.POSTING) {
            throw new EventBusException(" event handlers may only abort the incoming event");
        } else {
            postingState.f16697 = true;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m20822(Object event) {
        synchronized (this.f16680) {
            this.f16680.put(event.getClass(), event);
        }
        m20828(event);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <T> T m20832(Class<T> eventType) {
        T cast;
        synchronized (this.f16680) {
            cast = eventType.cast(this.f16680.get(eventType));
        }
        return cast;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public <T> T m20825(Class<T> eventType) {
        T cast;
        synchronized (this.f16680) {
            cast = eventType.cast(this.f16680.remove(eventType));
        }
        return cast;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m20823(Object event) {
        boolean z;
        synchronized (this.f16680) {
            Class<?> eventType = event.getClass();
            if (event.equals(this.f16680.get(eventType))) {
                this.f16680.remove(eventType);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m20826() {
        synchronized (this.f16680) {
            this.f16680.clear();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m20831(Class<?> eventClass) {
        CopyOnWriteArrayList<Subscription> subscriptions;
        List<Class<?>> eventTypes = m20813(eventClass);
        if (eventTypes != null) {
            int countTypes = eventTypes.size();
            for (int h = 0; h < countTypes; h++) {
                Class<?> clazz = eventTypes.get(h);
                synchronized (this) {
                    subscriptions = this.f16692.get(clazz);
                }
                if (subscriptions != null && !subscriptions.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20816(Object event, PostingThreadState postingState) throws Error {
        Class<?> eventClass = event.getClass();
        boolean subscriptionFound = false;
        if (this.f16688) {
            List<Class<?>> eventTypes = m20813(eventClass);
            int countTypes = eventTypes.size();
            for (int h = 0; h < countTypes; h++) {
                subscriptionFound |= m20821(event, postingState, eventTypes.get(h));
            }
        } else {
            subscriptionFound = m20821(event, postingState, eventClass);
        }
        if (!subscriptionFound) {
            if (this.f16694) {
                Log.d(f16678, "No subscribers registered for event " + eventClass);
            }
            if (this.f16687 && eventClass != NoSubscriberEvent.class && eventClass != SubscriberExceptionEvent.class) {
                m20828((Object) new NoSubscriberEvent(this, event));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m20821(Object event, PostingThreadState postingState, Class<?> eventClass) {
        CopyOnWriteArrayList<Subscription> subscriptions;
        synchronized (this) {
            subscriptions = this.f16692.get(eventClass);
        }
        if (subscriptions == null || subscriptions.isEmpty()) {
            return false;
        }
        Iterator<Subscription> it2 = subscriptions.iterator();
        while (it2.hasNext()) {
            Subscription subscription = it2.next();
            postingState.f16698 = event;
            postingState.f16700 = subscription;
            try {
                m20820(subscription, event, postingState.f16701);
                if (postingState.f16697) {
                    break;
                }
            } finally {
                postingState.f16698 = null;
                postingState.f16700 = null;
                postingState.f16697 = false;
            }
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20820(Subscription subscription, Object event, boolean isMainThread) {
        switch (subscription.f16749.f16732) {
            case POSTING:
                m20835(subscription, event);
                return;
            case MAIN:
                if (isMainThread) {
                    m20835(subscription, event);
                    return;
                } else {
                    this.f16689.m20837(subscription, event);
                    return;
                }
            case BACKGROUND:
                if (isMainThread) {
                    this.f16690.m20811(subscription, event);
                    return;
                } else {
                    m20835(subscription, event);
                    return;
                }
            case ASYNC:
                this.f16691.m20810(subscription, event);
                return;
            default:
                throw new IllegalStateException("Unknown thread mode: " + subscription.f16749.f16732);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private static List<Class<?>> m20813(Class<?> eventClass) {
        List<Class<?>> eventTypes;
        synchronized (f16676) {
            eventTypes = f16676.get(eventClass);
            if (eventTypes == null) {
                eventTypes = new ArrayList<>();
                for (Class<?> clazz = eventClass; clazz != null; clazz = clazz.getSuperclass()) {
                    eventTypes.add(clazz);
                    m20818(eventTypes, (Class<?>[]) clazz.getInterfaces());
                }
                f16676.put(eventClass, eventTypes);
            }
        }
        return eventTypes;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m20818(List<Class<?>> eventTypes, Class<?>[] interfaces) {
        for (Class<?> interfaceClass : interfaces) {
            if (!eventTypes.contains(interfaceClass)) {
                eventTypes.add(interfaceClass);
                m20818(eventTypes, (Class<?>[]) interfaceClass.getInterfaces());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20834(PendingPost pendingPost) {
        Object event = pendingPost.f16723;
        Subscription subscription = pendingPost.f16721;
        PendingPost.m20839(pendingPost);
        if (subscription.f16750) {
            m20835(subscription, event);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20835(Subscription subscription, Object event) {
        try {
            subscription.f16749.f16735.invoke(subscription.f16751, new Object[]{event});
        } catch (InvocationTargetException e) {
            m20819(subscription, event, e.getCause());
        } catch (IllegalAccessException e2) {
            throw new IllegalStateException("Unexpected exception", e2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20819(Subscription subscription, Object event, Throwable cause) {
        if (event instanceof SubscriberExceptionEvent) {
            if (this.f16693) {
                Log.e(f16678, "SubscriberExceptionEvent subscriber " + subscription.f16751.getClass() + " threw an exception", cause);
                SubscriberExceptionEvent exEvent = (SubscriberExceptionEvent) event;
                Log.e(f16678, "Initial event " + exEvent.f16728 + " caused exception in " + exEvent.f16727, exEvent.f16726);
            }
        } else if (this.f16683) {
            throw new EventBusException("Invoking subscriber failed", cause);
        } else {
            if (this.f16693) {
                Log.e(f16678, "Could not dispatch event: " + event.getClass() + " to subscribing class " + subscription.f16751.getClass(), cause);
            }
            if (this.f16686) {
                m20828((Object) new SubscriberExceptionEvent(this, cause, event, subscription.f16751));
            }
        }
    }

    static final class PostingThreadState {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f16697;

        /* renamed from: 连任  reason: contains not printable characters */
        Object f16698;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f16699;

        /* renamed from: 麤  reason: contains not printable characters */
        Subscription f16700;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f16701;

        /* renamed from: 龘  reason: contains not printable characters */
        final List<Object> f16702 = new ArrayList();

        PostingThreadState() {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public ExecutorService m20829() {
        return this.f16682;
    }

    public String toString() {
        return "EventBus[indexCount=" + this.f16684 + ", eventInheritance=" + this.f16688 + "]";
    }
}
