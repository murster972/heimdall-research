package org.greenrobot.eventbus;

import java.util.ArrayList;
import java.util.List;

final class PendingPost {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final List<PendingPost> f16720 = new ArrayList();

    /* renamed from: 靐  reason: contains not printable characters */
    Subscription f16721;

    /* renamed from: 齉  reason: contains not printable characters */
    PendingPost f16722;

    /* renamed from: 龘  reason: contains not printable characters */
    Object f16723;

    private PendingPost(Object event, Subscription subscription) {
        this.f16723 = event;
        this.f16721 = subscription;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static PendingPost m20838(Subscription subscription, Object event) {
        synchronized (f16720) {
            int size = f16720.size();
            if (size <= 0) {
                return new PendingPost(event, subscription);
            }
            PendingPost pendingPost = f16720.remove(size - 1);
            pendingPost.f16723 = event;
            pendingPost.f16721 = subscription;
            pendingPost.f16722 = null;
            return pendingPost;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m20839(PendingPost pendingPost) {
        pendingPost.f16723 = null;
        pendingPost.f16721 = null;
        pendingPost.f16722 = null;
        synchronized (f16720) {
            if (f16720.size() < 10000) {
                f16720.add(pendingPost);
            }
        }
    }
}
