package org.greenrobot.eventbus;

import android.util.Log;

final class BackgroundPoster implements Runnable {

    /* renamed from: 靐  reason: contains not printable characters */
    private final EventBus f16672;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile boolean f16673;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PendingPostQueue f16674 = new PendingPostQueue();

    BackgroundPoster(EventBus eventBus) {
        this.f16672 = eventBus;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20811(Subscription subscription, Object event) {
        PendingPost pendingPost = PendingPost.m20838(subscription, event);
        synchronized (this) {
            this.f16674.m20842(pendingPost);
            if (!this.f16673) {
                this.f16673 = true;
                this.f16672.m20829().execute(this);
            }
        }
    }

    public void run() {
        while (true) {
            try {
                PendingPost pendingPost = this.f16674.m20841(1000);
                if (pendingPost == null) {
                    synchronized (this) {
                        pendingPost = this.f16674.m20840();
                        if (pendingPost == null) {
                            this.f16673 = false;
                            this.f16673 = false;
                            return;
                        }
                    }
                }
                this.f16672.m20834(pendingPost);
            } catch (InterruptedException e) {
                Log.w("Event", Thread.currentThread().getName() + " was interruppted", e);
                this.f16673 = false;
                return;
            } catch (Throwable th) {
                this.f16673 = false;
                throw th;
            }
        }
    }
}
