package org.greenrobot.eventbus;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;

final class HandlerPoster extends Handler {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f16714;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f16715;

    /* renamed from: 齉  reason: contains not printable characters */
    private final EventBus f16716;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PendingPostQueue f16717 = new PendingPostQueue();

    HandlerPoster(EventBus eventBus, Looper looper, int maxMillisInsideHandleMessage) {
        super(looper);
        this.f16716 = eventBus;
        this.f16714 = maxMillisInsideHandleMessage;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20837(Subscription subscription, Object event) {
        PendingPost pendingPost = PendingPost.m20838(subscription, event);
        synchronized (this) {
            this.f16717.m20842(pendingPost);
            if (!this.f16715) {
                this.f16715 = true;
                if (!sendMessage(obtainMessage())) {
                    throw new EventBusException("Could not send handler message");
                }
            }
        }
    }

    public void handleMessage(Message msg) {
        try {
            long started = SystemClock.uptimeMillis();
            do {
                PendingPost pendingPost = this.f16717.m20840();
                if (pendingPost == null) {
                    synchronized (this) {
                        pendingPost = this.f16717.m20840();
                        if (pendingPost == null) {
                            this.f16715 = false;
                            this.f16715 = false;
                            return;
                        }
                    }
                }
                this.f16716.m20834(pendingPost);
            } while (SystemClock.uptimeMillis() - started < ((long) this.f16714));
            if (!sendMessage(obtainMessage())) {
                throw new EventBusException("Could not send handler message");
            }
            this.f16715 = true;
        } catch (Throwable th) {
            this.f16715 = false;
            throw th;
        }
    }
}
