package org.joda.time;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Days extends BaseSingleFieldPeriod {
    public static final Days FIVE = new Days(5);
    public static final Days FOUR = new Days(4);
    public static final Days MAX_VALUE = new Days(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    public static final Days MIN_VALUE = new Days(Integer.MIN_VALUE);
    public static final Days ONE = new Days(1);
    public static final Days SEVEN = new Days(7);
    public static final Days SIX = new Days(6);
    public static final Days THREE = new Days(3);
    public static final Days TWO = new Days(2);
    public static final Days ZERO = new Days(0);
    private static final long serialVersionUID = 87525275727380865L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final PeriodFormatter f6491 = ISOPeriodFormat.m21532().m21557(PeriodType.days());

    private Days(int i) {
        super(i);
    }

    public static Days days(int i) {
        switch (i) {
            case Integer.MIN_VALUE:
                return MIN_VALUE;
            case 0:
                return ZERO;
            case 1:
                return ONE;
            case 2:
                return TWO;
            case 3:
                return THREE;
            case 4:
                return FOUR;
            case 5:
                return FIVE;
            case 6:
                return SIX;
            case 7:
                return SEVEN;
            case MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT /*2147483647*/:
                return MAX_VALUE;
            default:
                return new Days(i);
        }
    }

    public static Days daysBetween(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        return days(BaseSingleFieldPeriod.m20947(readableInstant, readableInstant2, DurationFieldType.days()));
    }

    public static Days daysBetween(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        return (!(readablePartial instanceof LocalDate) || !(readablePartial2 instanceof LocalDate)) ? days(BaseSingleFieldPeriod.m20948(readablePartial, readablePartial2, (ReadablePeriod) ZERO)) : days(DateTimeUtils.m20895(readablePartial.getChronology()).days().getDifference(((LocalDate) readablePartial2).m7304(), ((LocalDate) readablePartial).m7304()));
    }

    public static Days daysIn(ReadableInterval readableInterval) {
        return readableInterval == null ? ZERO : days(BaseSingleFieldPeriod.m20947((ReadableInstant) readableInterval.getStart(), (ReadableInstant) readableInterval.getEnd(), DurationFieldType.days()));
    }

    @FromString
    public static Days parseDays(String str) {
        return str == null ? ZERO : days(f6491.m21556(str).getDays());
    }

    private Object readResolve() {
        return days(m20950());
    }

    public static Days standardDaysIn(ReadablePeriod readablePeriod) {
        return days(BaseSingleFieldPeriod.m20949(readablePeriod, 86400000));
    }

    public Days dividedBy(int i) {
        return i == 1 ? this : days(m20950() / i);
    }

    public int getDays() {
        return m20950();
    }

    public DurationFieldType getFieldType() {
        return DurationFieldType.days();
    }

    public PeriodType getPeriodType() {
        return PeriodType.days();
    }

    public boolean isGreaterThan(Days days) {
        return days == null ? m20950() > 0 : m20950() > days.m20950();
    }

    public boolean isLessThan(Days days) {
        return days == null ? m20950() < 0 : m20950() < days.m20950();
    }

    public Days minus(int i) {
        return plus(FieldUtils.m21209(i));
    }

    public Days minus(Days days) {
        return days == null ? this : minus(days.m20950());
    }

    public Days multipliedBy(int i) {
        return days(FieldUtils.m21205(m20950(), i));
    }

    public Days negated() {
        return days(FieldUtils.m21209(m20950()));
    }

    public Days plus(int i) {
        return i == 0 ? this : days(FieldUtils.m21210(m20950(), i));
    }

    public Days plus(Days days) {
        return days == null ? this : plus(days.m20950());
    }

    public Duration toStandardDuration() {
        return new Duration(((long) m20950()) * 86400000);
    }

    public Hours toStandardHours() {
        return Hours.hours(FieldUtils.m21205(m20950(), 24));
    }

    public Minutes toStandardMinutes() {
        return Minutes.minutes(FieldUtils.m21205(m20950(), 1440));
    }

    public Seconds toStandardSeconds() {
        return Seconds.seconds(FieldUtils.m21205(m20950(), 86400));
    }

    public Weeks toStandardWeeks() {
        return Weeks.weeks(m20950() / 7);
    }

    @ToString
    public String toString() {
        return "P" + String.valueOf(m20950()) + "D";
    }
}
