package org.joda.time;

import java.io.Serializable;
import java.util.Comparator;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.InstantConverter;

public class DateTimeComparator implements Serializable, Comparator<Object> {
    private static final long serialVersionUID = -6097339773320178364L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final DateTimeComparator f16752 = new DateTimeComparator(DateTimeFieldType.dayOfYear(), (DateTimeFieldType) null);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final DateTimeComparator f16753 = new DateTimeComparator((DateTimeFieldType) null, DateTimeFieldType.dayOfYear());

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeComparator f16754 = new DateTimeComparator((DateTimeFieldType) null, (DateTimeFieldType) null);
    private final DateTimeFieldType iLowerLimit;
    private final DateTimeFieldType iUpperLimit;

    public static DateTimeComparator getInstance() {
        return f16754;
    }

    public static DateTimeComparator getInstance(DateTimeFieldType dateTimeFieldType) {
        return getInstance(dateTimeFieldType, (DateTimeFieldType) null);
    }

    public static DateTimeComparator getInstance(DateTimeFieldType dateTimeFieldType, DateTimeFieldType dateTimeFieldType2) {
        if (dateTimeFieldType == null && dateTimeFieldType2 == null) {
            return f16754;
        }
        if (dateTimeFieldType == DateTimeFieldType.dayOfYear() && dateTimeFieldType2 == null) {
            return f16752;
        }
        if (dateTimeFieldType == null && dateTimeFieldType2 == DateTimeFieldType.dayOfYear()) {
            return f16753;
        }
        return new DateTimeComparator(dateTimeFieldType, dateTimeFieldType2);
    }

    public static DateTimeComparator getDateOnlyInstance() {
        return f16752;
    }

    public static DateTimeComparator getTimeOnlyInstance() {
        return f16753;
    }

    protected DateTimeComparator(DateTimeFieldType dateTimeFieldType, DateTimeFieldType dateTimeFieldType2) {
        this.iLowerLimit = dateTimeFieldType;
        this.iUpperLimit = dateTimeFieldType2;
    }

    public DateTimeFieldType getLowerLimit() {
        return this.iLowerLimit;
    }

    public DateTimeFieldType getUpperLimit() {
        return this.iUpperLimit;
    }

    public int compare(Object obj, Object obj2) {
        InstantConverter r2 = ConverterManager.m21146().m21151(obj);
        Chronology r5 = r2.m21159(obj, (Chronology) null);
        long r22 = r2.m21160(obj, r5);
        if (obj == obj2) {
            return 0;
        }
        InstantConverter r0 = ConverterManager.m21146().m21151(obj2);
        Chronology r6 = r0.m21159(obj2, (Chronology) null);
        long r02 = r0.m21160(obj2, r6);
        if (this.iLowerLimit != null) {
            r22 = this.iLowerLimit.getField(r5).roundFloor(r22);
            r02 = this.iLowerLimit.getField(r6).roundFloor(r02);
        }
        if (this.iUpperLimit != null) {
            r22 = this.iUpperLimit.getField(r5).remainder(r22);
            r02 = this.iUpperLimit.getField(r6).remainder(r02);
        }
        if (r22 < r02) {
            return -1;
        }
        return r22 > r02 ? 1 : 0;
    }

    private Object readResolve() {
        return getInstance(this.iLowerLimit, this.iUpperLimit);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DateTimeComparator)) {
            return false;
        }
        DateTimeComparator dateTimeComparator = (DateTimeComparator) obj;
        if (this.iLowerLimit != dateTimeComparator.getLowerLimit() && (this.iLowerLimit == null || !this.iLowerLimit.equals(dateTimeComparator.getLowerLimit()))) {
            return false;
        }
        if (this.iUpperLimit == dateTimeComparator.getUpperLimit() || (this.iUpperLimit != null && this.iUpperLimit.equals(dateTimeComparator.getUpperLimit()))) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = this.iLowerLimit == null ? 0 : this.iLowerLimit.hashCode();
        if (this.iUpperLimit != null) {
            i = this.iUpperLimit.hashCode();
        }
        return hashCode + (i * 123);
    }

    public String toString() {
        if (this.iLowerLimit == this.iUpperLimit) {
            return "DateTimeComparator[" + (this.iLowerLimit == null ? "" : this.iLowerLimit.getName()) + "]";
        }
        return "DateTimeComparator[" + (this.iLowerLimit == null ? "" : this.iLowerLimit.getName()) + "-" + (this.iUpperLimit == null ? "" : this.iUpperLimit.getName()) + "]";
    }
}
