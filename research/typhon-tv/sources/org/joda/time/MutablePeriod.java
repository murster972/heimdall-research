package org.joda.time;

import java.io.Serializable;
import org.joda.convert.FromString;
import org.joda.time.base.BasePeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public class MutablePeriod extends BasePeriod implements Serializable, Cloneable, ReadWritablePeriod {
    private static final long serialVersionUID = 3436451121567212165L;

    public MutablePeriod() {
        super(0, (PeriodType) null, (Chronology) null);
    }

    public MutablePeriod(int i, int i2, int i3, int i4) {
        super(0, 0, 0, 0, i, i2, i3, i4, PeriodType.standard());
    }

    public MutablePeriod(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        super(i, i2, i3, i4, i5, i6, i7, i8, PeriodType.standard());
    }

    public MutablePeriod(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, PeriodType periodType) {
        super(i, i2, i3, i4, i5, i6, i7, i8, periodType);
    }

    public MutablePeriod(long j) {
        super(j);
    }

    public MutablePeriod(long j, long j2) {
        super(j, j2, (PeriodType) null, (Chronology) null);
    }

    public MutablePeriod(long j, long j2, Chronology chronology) {
        super(j, j2, (PeriodType) null, chronology);
    }

    public MutablePeriod(long j, long j2, PeriodType periodType) {
        super(j, j2, periodType, (Chronology) null);
    }

    public MutablePeriod(long j, long j2, PeriodType periodType, Chronology chronology) {
        super(j, j2, periodType, chronology);
    }

    public MutablePeriod(long j, Chronology chronology) {
        super(j, (PeriodType) null, chronology);
    }

    public MutablePeriod(long j, PeriodType periodType) {
        super(j, periodType, (Chronology) null);
    }

    public MutablePeriod(long j, PeriodType periodType, Chronology chronology) {
        super(j, periodType, chronology);
    }

    public MutablePeriod(Object obj) {
        super(obj, (PeriodType) null, (Chronology) null);
    }

    public MutablePeriod(Object obj, Chronology chronology) {
        super(obj, (PeriodType) null, chronology);
    }

    public MutablePeriod(Object obj, PeriodType periodType) {
        super(obj, periodType, (Chronology) null);
    }

    public MutablePeriod(Object obj, PeriodType periodType, Chronology chronology) {
        super(obj, periodType, chronology);
    }

    public MutablePeriod(PeriodType periodType) {
        super(0, periodType, (Chronology) null);
    }

    public MutablePeriod(ReadableDuration readableDuration, ReadableInstant readableInstant) {
        super(readableDuration, readableInstant, (PeriodType) null);
    }

    public MutablePeriod(ReadableDuration readableDuration, ReadableInstant readableInstant, PeriodType periodType) {
        super(readableDuration, readableInstant, periodType);
    }

    public MutablePeriod(ReadableInstant readableInstant, ReadableDuration readableDuration) {
        super(readableInstant, readableDuration, (PeriodType) null);
    }

    public MutablePeriod(ReadableInstant readableInstant, ReadableDuration readableDuration, PeriodType periodType) {
        super(readableInstant, readableDuration, periodType);
    }

    public MutablePeriod(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        super(readableInstant, readableInstant2, (PeriodType) null);
    }

    public MutablePeriod(ReadableInstant readableInstant, ReadableInstant readableInstant2, PeriodType periodType) {
        super(readableInstant, readableInstant2, periodType);
    }

    @FromString
    public static MutablePeriod parse(String str) {
        return parse(str, ISOPeriodFormat.m21532());
    }

    public static MutablePeriod parse(String str, PeriodFormatter periodFormatter) {
        return periodFormatter.m21556(str).toMutablePeriod();
    }

    public void add(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        setPeriod(FieldUtils.m21210(getYears(), i), FieldUtils.m21210(getMonths(), i2), FieldUtils.m21210(getWeeks(), i3), FieldUtils.m21210(getDays(), i4), FieldUtils.m21210(getHours(), i5), FieldUtils.m21210(getMinutes(), i6), FieldUtils.m21210(getSeconds(), i7), FieldUtils.m21210(getMillis(), i8));
    }

    public void add(long j) {
        add((ReadablePeriod) new Period(j, getPeriodType()));
    }

    public void add(long j, Chronology chronology) {
        add((ReadablePeriod) new Period(j, getPeriodType(), chronology));
    }

    public void add(DurationFieldType durationFieldType, int i) {
        super.m20938(durationFieldType, i);
    }

    public void add(ReadableDuration readableDuration) {
        if (readableDuration != null) {
            add((ReadablePeriod) new Period(readableDuration.getMillis(), getPeriodType()));
        }
    }

    public void add(ReadableInterval readableInterval) {
        if (readableInterval != null) {
            add((ReadablePeriod) readableInterval.toPeriod(getPeriodType()));
        }
    }

    public void add(ReadablePeriod readablePeriod) {
        super.m20943(readablePeriod);
    }

    public void addDays(int i) {
        super.m20938(DurationFieldType.days(), i);
    }

    public void addHours(int i) {
        super.m20938(DurationFieldType.hours(), i);
    }

    public void addMillis(int i) {
        super.m20938(DurationFieldType.millis(), i);
    }

    public void addMinutes(int i) {
        super.m20938(DurationFieldType.minutes(), i);
    }

    public void addMonths(int i) {
        super.m20938(DurationFieldType.months(), i);
    }

    public void addSeconds(int i) {
        super.m20938(DurationFieldType.seconds(), i);
    }

    public void addWeeks(int i) {
        super.m20938(DurationFieldType.weeks(), i);
    }

    public void addYears(int i) {
        super.m20938(DurationFieldType.years(), i);
    }

    public void clear() {
        super.m20944(new int[size()]);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError("Clone error");
        }
    }

    public MutablePeriod copy() {
        return (MutablePeriod) clone();
    }

    public int getDays() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16826);
    }

    public int getHours() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16824);
    }

    public int getMillis() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16807);
    }

    public int getMinutes() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16805);
    }

    public int getMonths() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16825);
    }

    public int getSeconds() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16806);
    }

    public int getWeeks() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16827);
    }

    public int getYears() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16828);
    }

    public void mergePeriod(ReadablePeriod readablePeriod) {
        super.mergePeriod(readablePeriod);
    }

    public void set(DurationFieldType durationFieldType, int i) {
        super.m20942(durationFieldType, i);
    }

    public void setDays(int i) {
        super.m20942(DurationFieldType.days(), i);
    }

    public void setHours(int i) {
        super.m20942(DurationFieldType.hours(), i);
    }

    public void setMillis(int i) {
        super.m20942(DurationFieldType.millis(), i);
    }

    public void setMinutes(int i) {
        super.m20942(DurationFieldType.minutes(), i);
    }

    public void setMonths(int i) {
        super.m20942(DurationFieldType.months(), i);
    }

    public void setPeriod(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        super.setPeriod(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void setPeriod(long j) {
        setPeriod(j, (Chronology) null);
    }

    public void setPeriod(long j, long j2) {
        setPeriod(j, j2, (Chronology) null);
    }

    public void setPeriod(long j, long j2, Chronology chronology) {
        m20944(DateTimeUtils.m20895(chronology).get(this, j, j2));
    }

    public void setPeriod(long j, Chronology chronology) {
        m20944(DateTimeUtils.m20895(chronology).get((ReadablePeriod) this, j));
    }

    public void setPeriod(ReadableDuration readableDuration) {
        setPeriod(readableDuration, (Chronology) null);
    }

    public void setPeriod(ReadableDuration readableDuration, Chronology chronology) {
        setPeriod(DateTimeUtils.m20892(readableDuration), chronology);
    }

    public void setPeriod(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        if (readableInstant == readableInstant2) {
            setPeriod(0);
            return;
        }
        setPeriod(DateTimeUtils.m20893(readableInstant), DateTimeUtils.m20893(readableInstant2), DateTimeUtils.m20896(readableInstant, readableInstant2));
    }

    public void setPeriod(ReadableInterval readableInterval) {
        if (readableInterval == null) {
            setPeriod(0);
            return;
        }
        setPeriod(readableInterval.getStartMillis(), readableInterval.getEndMillis(), DateTimeUtils.m20895(readableInterval.getChronology()));
    }

    public void setPeriod(ReadablePeriod readablePeriod) {
        super.setPeriod(readablePeriod);
    }

    public void setSeconds(int i) {
        super.m20942(DurationFieldType.seconds(), i);
    }

    public void setValue(int i, int i2) {
        super.setValue(i, i2);
    }

    public void setWeeks(int i) {
        super.m20942(DurationFieldType.weeks(), i);
    }

    public void setYears(int i) {
        super.m20942(DurationFieldType.years(), i);
    }
}
