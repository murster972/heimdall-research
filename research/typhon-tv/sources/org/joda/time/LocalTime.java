package org.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseLocal;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.PartialConverter;
import org.joda.time.field.AbstractReadableInstantFieldProperty;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class LocalTime extends BaseLocal implements Serializable, ReadablePartial {
    public static final LocalTime MIDNIGHT = new LocalTime(0, 0, 0, 0);
    private static final long serialVersionUID = -12873158713873L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Set<DurationFieldType> f6495 = new HashSet();
    private final Chronology iChronology;
    private final long iLocalMillis;

    static {
        f6495.add(DurationFieldType.millis());
        f6495.add(DurationFieldType.seconds());
        f6495.add(DurationFieldType.minutes());
        f6495.add(DurationFieldType.hours());
    }

    public LocalTime() {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance());
    }

    public LocalTime(int i, int i2) {
        this(i, i2, 0, 0, ISOChronology.getInstanceUTC());
    }

    public LocalTime(int i, int i2, int i3) {
        this(i, i2, i3, 0, ISOChronology.getInstanceUTC());
    }

    public LocalTime(int i, int i2, int i3, int i4) {
        this(i, i2, i3, i4, ISOChronology.getInstanceUTC());
    }

    public LocalTime(int i, int i2, int i3, int i4, Chronology chronology) {
        Chronology withUTC = DateTimeUtils.m20895(chronology).withUTC();
        long dateTimeMillis = withUTC.getDateTimeMillis(0, i, i2, i3, i4);
        this.iChronology = withUTC;
        this.iLocalMillis = dateTimeMillis;
    }

    public LocalTime(long j) {
        this(j, (Chronology) ISOChronology.getInstance());
    }

    public LocalTime(long j, Chronology chronology) {
        Chronology r0 = DateTimeUtils.m20895(chronology);
        long millisKeepLocal = r0.getZone().getMillisKeepLocal(DateTimeZone.UTC, j);
        Chronology withUTC = r0.withUTC();
        this.iLocalMillis = (long) withUTC.millisOfDay().get(millisKeepLocal);
        this.iChronology = withUTC;
    }

    public LocalTime(long j, DateTimeZone dateTimeZone) {
        this(j, (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public LocalTime(Object obj) {
        this(obj, (Chronology) null);
    }

    public LocalTime(Object obj, Chronology chronology) {
        PartialConverter r0 = ConverterManager.m21146().m21148(obj);
        Chronology r1 = DateTimeUtils.m20895(r0.m21171(obj, chronology));
        this.iChronology = r1.withUTC();
        int[] r02 = r0.m21173(this, obj, r1, ISODateTimeFormat.m21450());
        this.iLocalMillis = this.iChronology.getDateTimeMillis(0, r02[0], r02[1], r02[2], r02[3]);
    }

    public LocalTime(Object obj, DateTimeZone dateTimeZone) {
        PartialConverter r0 = ConverterManager.m21146().m21148(obj);
        Chronology r1 = DateTimeUtils.m20895(r0.m21172(obj, dateTimeZone));
        this.iChronology = r1.withUTC();
        int[] r02 = r0.m21173(this, obj, r1, ISODateTimeFormat.m21450());
        this.iLocalMillis = this.iChronology.getDateTimeMillis(0, r02[0], r02[1], r02[2], r02[3]);
    }

    public LocalTime(Chronology chronology) {
        this(DateTimeUtils.m20891(), chronology);
    }

    public LocalTime(DateTimeZone dateTimeZone) {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public static LocalTime fromCalendarFields(Calendar calendar) {
        if (calendar != null) {
            return new LocalTime(calendar.get(11), calendar.get(12), calendar.get(13), calendar.get(14));
        }
        throw new IllegalArgumentException("The calendar must not be null");
    }

    public static LocalTime fromDateFields(Date date) {
        if (date != null) {
            return new LocalTime(date.getHours(), date.getMinutes(), date.getSeconds(), (((int) (date.getTime() % 1000)) + 1000) % 1000);
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static LocalTime fromMillisOfDay(long j) {
        return fromMillisOfDay(j, (Chronology) null);
    }

    public static LocalTime fromMillisOfDay(long j, Chronology chronology) {
        return new LocalTime(j, DateTimeUtils.m20895(chronology).withUTC());
    }

    public static LocalTime now() {
        return new LocalTime();
    }

    public static LocalTime now(Chronology chronology) {
        if (chronology != null) {
            return new LocalTime(chronology);
        }
        throw new NullPointerException("Chronology must not be null");
    }

    public static LocalTime now(DateTimeZone dateTimeZone) {
        if (dateTimeZone != null) {
            return new LocalTime(dateTimeZone);
        }
        throw new NullPointerException("Zone must not be null");
    }

    @FromString
    public static LocalTime parse(String str) {
        return parse(str, ISODateTimeFormat.m21450());
    }

    public static LocalTime parse(String str, DateTimeFormatter dateTimeFormatter) {
        return dateTimeFormatter.m21242(str);
    }

    private Object readResolve() {
        return this.iChronology == null ? new LocalTime(this.iLocalMillis, (Chronology) ISOChronology.getInstanceUTC()) : !DateTimeZone.UTC.equals(this.iChronology.getZone()) ? new LocalTime(this.iLocalMillis, this.iChronology.withUTC()) : this;
    }

    public int compareTo(ReadablePartial readablePartial) {
        if (this == readablePartial) {
            return 0;
        }
        if (readablePartial instanceof LocalTime) {
            LocalTime localTime = (LocalTime) readablePartial;
            if (this.iChronology.equals(localTime.iChronology)) {
                return this.iLocalMillis < localTime.iLocalMillis ? -1 : this.iLocalMillis == localTime.iLocalMillis ? 0 : 1;
            }
        }
        return super.compareTo(readablePartial);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof LocalTime) {
            LocalTime localTime = (LocalTime) obj;
            if (this.iChronology.equals(localTime.iChronology)) {
                return this.iLocalMillis == localTime.iLocalMillis;
            }
        }
        return super.equals(obj);
    }

    public int get(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("The DateTimeFieldType must not be null");
        } else if (isSupported(dateTimeFieldType)) {
            return dateTimeFieldType.getField(getChronology()).get(m7311());
        } else {
            throw new IllegalArgumentException("Field '" + dateTimeFieldType + "' is not supported");
        }
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public int getHourOfDay() {
        return getChronology().hourOfDay().get(m7311());
    }

    public int getMillisOfDay() {
        return getChronology().millisOfDay().get(m7311());
    }

    public int getMillisOfSecond() {
        return getChronology().millisOfSecond().get(m7311());
    }

    public int getMinuteOfHour() {
        return getChronology().minuteOfHour().get(m7311());
    }

    public int getSecondOfMinute() {
        return getChronology().secondOfMinute().get(m7311());
    }

    public int getValue(int i) {
        switch (i) {
            case 0:
                return getChronology().hourOfDay().get(m7311());
            case 1:
                return getChronology().minuteOfHour().get(m7311());
            case 2:
                return getChronology().secondOfMinute().get(m7311());
            case 3:
                return getChronology().millisOfSecond().get(m7311());
            default:
                throw new IndexOutOfBoundsException("Invalid index: " + i);
        }
    }

    public Property hourOfDay() {
        return new Property(this, getChronology().hourOfDay());
    }

    public boolean isSupported(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null || !isSupported(dateTimeFieldType.getDurationType())) {
            return false;
        }
        DurationFieldType rangeDurationType = dateTimeFieldType.getRangeDurationType();
        return isSupported(rangeDurationType) || rangeDurationType == DurationFieldType.days();
    }

    public boolean isSupported(DurationFieldType durationFieldType) {
        if (durationFieldType == null) {
            return false;
        }
        DurationField field = durationFieldType.getField(getChronology());
        if (f6495.contains(durationFieldType) || field.getUnitMillis() < getChronology().days().getUnitMillis()) {
            return field.isSupported();
        }
        return false;
    }

    public Property millisOfDay() {
        return new Property(this, getChronology().millisOfDay());
    }

    public Property millisOfSecond() {
        return new Property(this, getChronology().millisOfSecond());
    }

    public LocalTime minus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, -1);
    }

    public LocalTime minusHours(int i) {
        return i == 0 ? this : m7313(getChronology().hours().subtract(m7311(), i));
    }

    public LocalTime minusMillis(int i) {
        return i == 0 ? this : m7313(getChronology().millis().subtract(m7311(), i));
    }

    public LocalTime minusMinutes(int i) {
        return i == 0 ? this : m7313(getChronology().minutes().subtract(m7311(), i));
    }

    public LocalTime minusSeconds(int i) {
        return i == 0 ? this : m7313(getChronology().seconds().subtract(m7311(), i));
    }

    public Property minuteOfHour() {
        return new Property(this, getChronology().minuteOfHour());
    }

    public LocalTime plus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, 1);
    }

    public LocalTime plusHours(int i) {
        return i == 0 ? this : m7313(getChronology().hours().add(m7311(), i));
    }

    public LocalTime plusMillis(int i) {
        return i == 0 ? this : m7313(getChronology().millis().add(m7311(), i));
    }

    public LocalTime plusMinutes(int i) {
        return i == 0 ? this : m7313(getChronology().minutes().add(m7311(), i));
    }

    public LocalTime plusSeconds(int i) {
        return i == 0 ? this : m7313(getChronology().seconds().add(m7311(), i));
    }

    public Property property(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("The DateTimeFieldType must not be null");
        } else if (isSupported(dateTimeFieldType)) {
            return new Property(this, dateTimeFieldType.getField(getChronology()));
        } else {
            throw new IllegalArgumentException("Field '" + dateTimeFieldType + "' is not supported");
        }
    }

    public Property secondOfMinute() {
        return new Property(this, getChronology().secondOfMinute());
    }

    public int size() {
        return 4;
    }

    public DateTime toDateTimeToday() {
        return toDateTimeToday((DateTimeZone) null);
    }

    public DateTime toDateTimeToday(DateTimeZone dateTimeZone) {
        Chronology withZone = getChronology().withZone(dateTimeZone);
        return new DateTime(withZone.set(this, DateTimeUtils.m20891()), withZone);
    }

    @ToString
    public String toString() {
        return ISODateTimeFormat.m21439().m21246((ReadablePartial) this);
    }

    public String toString(String str) {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21246((ReadablePartial) this);
    }

    public String toString(String str, Locale locale) throws IllegalArgumentException {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21247(locale).m21246((ReadablePartial) this);
    }

    public LocalTime withField(DateTimeFieldType dateTimeFieldType, int i) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field must not be null");
        } else if (isSupported(dateTimeFieldType)) {
            return m7313(dateTimeFieldType.getField(getChronology()).set(m7311(), i));
        } else {
            throw new IllegalArgumentException("Field '" + dateTimeFieldType + "' is not supported");
        }
    }

    public LocalTime withFieldAdded(DurationFieldType durationFieldType, int i) {
        if (durationFieldType == null) {
            throw new IllegalArgumentException("Field must not be null");
        } else if (isSupported(durationFieldType)) {
            return i == 0 ? this : m7313(durationFieldType.getField(getChronology()).add(m7311(), i));
        } else {
            throw new IllegalArgumentException("Field '" + durationFieldType + "' is not supported");
        }
    }

    public LocalTime withFields(ReadablePartial readablePartial) {
        return readablePartial == null ? this : m7313(getChronology().set(readablePartial, m7311()));
    }

    public LocalTime withHourOfDay(int i) {
        return m7313(getChronology().hourOfDay().set(m7311(), i));
    }

    public LocalTime withMillisOfDay(int i) {
        return m7313(getChronology().millisOfDay().set(m7311(), i));
    }

    public LocalTime withMillisOfSecond(int i) {
        return m7313(getChronology().millisOfSecond().set(m7311(), i));
    }

    public LocalTime withMinuteOfHour(int i) {
        return m7313(getChronology().minuteOfHour().set(m7311(), i));
    }

    public LocalTime withPeriodAdded(ReadablePeriod readablePeriod, int i) {
        return (readablePeriod == null || i == 0) ? this : m7313(getChronology().add(readablePeriod, m7311(), i));
    }

    public LocalTime withSecondOfMinute(int i) {
        return m7313(getChronology().secondOfMinute().set(m7311(), i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m7311() {
        return this.iLocalMillis;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeField m7312(int i, Chronology chronology) {
        switch (i) {
            case 0:
                return chronology.hourOfDay();
            case 1:
                return chronology.minuteOfHour();
            case 2:
                return chronology.secondOfMinute();
            case 3:
                return chronology.millisOfSecond();
            default:
                throw new IndexOutOfBoundsException("Invalid index: " + i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public LocalTime m7313(long j) {
        return j == m7311() ? this : new LocalTime(j, getChronology());
    }

    public static final class Property extends AbstractReadableInstantFieldProperty {
        private static final long serialVersionUID = -325842547277223L;

        /* renamed from: 靐  reason: contains not printable characters */
        private transient DateTimeField f16802;

        /* renamed from: 龘  reason: contains not printable characters */
        private transient LocalTime f16803;

        Property(LocalTime localTime, DateTimeField dateTimeField) {
            this.f16803 = localTime;
            this.f16802 = dateTimeField;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeObject(this.f16803);
            objectOutputStream.writeObject(this.f16802.getType());
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            this.f16803 = (LocalTime) objectInputStream.readObject();
            this.f16802 = ((DateTimeFieldType) objectInputStream.readObject()).getField(this.f16803.getChronology());
        }

        public DateTimeField getField() {
            return this.f16802;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m20914() {
            return this.f16803.m7311();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public Chronology m20913() {
            return this.f16803.getChronology();
        }

        public LocalTime getLocalTime() {
            return this.f16803;
        }

        public LocalTime addCopy(int i) {
            return this.f16803.m7313(this.f16802.add(this.f16803.m7311(), i));
        }

        public LocalTime addCopy(long j) {
            return this.f16803.m7313(this.f16802.add(this.f16803.m7311(), j));
        }

        public LocalTime addNoWrapToCopy(int i) {
            long add = this.f16802.add(this.f16803.m7311(), i);
            if (((long) this.f16803.getChronology().millisOfDay().get(add)) == add) {
                return this.f16803.m7313(add);
            }
            throw new IllegalArgumentException("The addition exceeded the boundaries of LocalTime");
        }

        public LocalTime addWrapFieldToCopy(int i) {
            return this.f16803.m7313(this.f16802.addWrapField(this.f16803.m7311(), i));
        }

        public LocalTime setCopy(int i) {
            return this.f16803.m7313(this.f16802.set(this.f16803.m7311(), i));
        }

        public LocalTime setCopy(String str, Locale locale) {
            return this.f16803.m7313(this.f16802.set(this.f16803.m7311(), str, locale));
        }

        public LocalTime setCopy(String str) {
            return setCopy(str, (Locale) null);
        }

        public LocalTime withMaximumValue() {
            return setCopy(getMaximumValue());
        }

        public LocalTime withMinimumValue() {
            return setCopy(getMinimumValue());
        }

        public LocalTime roundFloorCopy() {
            return this.f16803.m7313(this.f16802.roundFloor(this.f16803.m7311()));
        }

        public LocalTime roundCeilingCopy() {
            return this.f16803.m7313(this.f16802.roundCeiling(this.f16803.m7311()));
        }

        public LocalTime roundHalfFloorCopy() {
            return this.f16803.m7313(this.f16802.roundHalfFloor(this.f16803.m7311()));
        }

        public LocalTime roundHalfCeilingCopy() {
            return this.f16803.m7313(this.f16802.roundHalfCeiling(this.f16803.m7311()));
        }

        public LocalTime roundHalfEvenCopy() {
            return this.f16803.m7313(this.f16802.roundHalfEven(this.f16803.m7311()));
        }
    }
}
