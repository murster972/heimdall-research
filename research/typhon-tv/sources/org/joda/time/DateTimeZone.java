package org.joda.time;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.time.TimeZones;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.chrono.BaseChronology;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.FormatUtils;
import org.joda.time.tz.DefaultNameProvider;
import org.joda.time.tz.FixedDateTimeZone;
import org.joda.time.tz.NameProvider;
import org.joda.time.tz.Provider;
import org.joda.time.tz.UTCProvider;
import org.joda.time.tz.ZoneInfoProvider;

public abstract class DateTimeZone implements Serializable {
    public static final DateTimeZone UTC = UTCDateTimeZone.f16831;
    private static final long serialVersionUID = 5546345482340108586L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final AtomicReference<NameProvider> f6488 = new AtomicReference<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final AtomicReference<DateTimeZone> f6489 = new AtomicReference<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final AtomicReference<Provider> f6490 = new AtomicReference<>();
    private final String iID;

    protected DateTimeZone(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Id must not be null");
        }
        this.iID = str;
    }

    @FromString
    public static DateTimeZone forID(String str) {
        if (str == null) {
            return getDefault();
        }
        if (str.equals("UTC")) {
            return UTC;
        }
        DateTimeZone r0 = getProvider().m21662(str);
        if (r0 != null) {
            return r0;
        }
        if (str.startsWith("+") || str.startsWith("-")) {
            int r02 = m7298(str);
            return ((long) r02) == 0 ? UTC : m7301(m7299(r02), r02);
        }
        throw new IllegalArgumentException("The datetime zone id '" + str + "' is not recognised");
    }

    public static DateTimeZone forOffsetHours(int i) throws IllegalArgumentException {
        return forOffsetHoursMinutes(i, 0);
    }

    public static DateTimeZone forOffsetHoursMinutes(int i, int i2) throws IllegalArgumentException {
        int i3;
        if (i == 0 && i2 == 0) {
            return UTC;
        }
        if (i < -23 || i > 23) {
            throw new IllegalArgumentException("Hours out of range: " + i);
        } else if (i2 < -59 || i2 > 59) {
            throw new IllegalArgumentException("Minutes out of range: " + i2);
        } else if (i <= 0 || i2 >= 0) {
            int i4 = i * 60;
            if (i4 < 0) {
                try {
                    i3 = i4 - Math.abs(i2);
                } catch (ArithmeticException e) {
                    throw new IllegalArgumentException("Offset is too large");
                }
            } else {
                i3 = i4 + i2;
            }
            return forOffsetMillis(FieldUtils.m21205(i3, 60000));
        } else {
            throw new IllegalArgumentException("Positive hours must not have negative minutes: " + i2);
        }
    }

    public static DateTimeZone forOffsetMillis(int i) {
        if (i >= -86399999 && i <= 86399999) {
            return m7301(m7299(i), i);
        }
        throw new IllegalArgumentException("Millis out of range: " + i);
    }

    public static DateTimeZone forTimeZone(TimeZone timeZone) {
        char charAt;
        if (timeZone == null) {
            return getDefault();
        }
        String id = timeZone.getID();
        if (id == null) {
            throw new IllegalArgumentException("The TimeZone id must not be null");
        } else if (id.equals("UTC")) {
            return UTC;
        } else {
            DateTimeZone dateTimeZone = null;
            String r2 = m7296(id);
            Provider provider = getProvider();
            if (r2 != null) {
                dateTimeZone = provider.m21662(r2);
            }
            if (dateTimeZone == null) {
                dateTimeZone = provider.m21662(id);
            }
            if (dateTimeZone != null) {
                return dateTimeZone;
            }
            if (r2 != null || (!id.startsWith("GMT+") && !id.startsWith("GMT-"))) {
                throw new IllegalArgumentException("The datetime zone id '" + id + "' is not recognised");
            }
            String substring = id.substring(3);
            if (substring.length() > 2 && (charAt = substring.charAt(1)) > '9' && Character.isDigit(charAt)) {
                substring = m7300(substring);
            }
            int r0 = m7298(substring);
            return ((long) r0) == 0 ? UTC : m7301(m7299(r0), r0);
        }
    }

    public static Set<String> getAvailableIDs() {
        return getProvider().m21661();
    }

    public static DateTimeZone getDefault() {
        DateTimeZone dateTimeZone = f6489.get();
        if (dateTimeZone != null) {
            return dateTimeZone;
        }
        try {
            String property = System.getProperty("user.timezone");
            if (property != null) {
                dateTimeZone = forID(property);
            }
        } catch (RuntimeException e) {
        }
        if (dateTimeZone == null) {
            try {
                dateTimeZone = forTimeZone(TimeZone.getDefault());
            } catch (IllegalArgumentException e2) {
            }
        }
        if (dateTimeZone == null) {
            dateTimeZone = UTC;
        }
        return !f6489.compareAndSet((Object) null, dateTimeZone) ? f6489.get() : dateTimeZone;
    }

    public static NameProvider getNameProvider() {
        NameProvider nameProvider = f6488.get();
        if (nameProvider != null) {
            return nameProvider;
        }
        NameProvider r0 = m7297();
        return !f6488.compareAndSet((Object) null, r0) ? f6488.get() : r0;
    }

    public static Provider getProvider() {
        Provider provider = f6490.get();
        if (provider != null) {
            return provider;
        }
        Provider r0 = m7302();
        return !f6490.compareAndSet((Object) null, r0) ? f6490.get() : r0;
    }

    public static void setDefault(DateTimeZone dateTimeZone) throws SecurityException {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(new JodaTimePermission("DateTimeZone.setDefault"));
        }
        if (dateTimeZone == null) {
            throw new IllegalArgumentException("The datetime zone must not be null");
        }
        f6489.set(dateTimeZone);
    }

    public static void setNameProvider(NameProvider nameProvider) throws SecurityException {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(new JodaTimePermission("DateTimeZone.setNameProvider"));
        }
        if (nameProvider == null) {
            nameProvider = m7297();
        }
        f6488.set(nameProvider);
    }

    public static void setProvider(Provider provider) throws SecurityException {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(new JodaTimePermission("DateTimeZone.setProvider"));
        }
        if (provider == null) {
            provider = m7302();
        } else {
            m7303(provider);
        }
        f6490.set(provider);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String m7296(String str) {
        return LazyInit.f16784.get(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static NameProvider m7297() {
        NameProvider nameProvider;
        try {
            String property = System.getProperty("org.joda.time.DateTimeZone.NameProvider");
            nameProvider = property != null ? (NameProvider) Class.forName(property).newInstance() : null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } catch (SecurityException e2) {
            nameProvider = null;
        }
        return nameProvider == null ? new DefaultNameProvider() : nameProvider;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m7298(String str) {
        return -((int) LazyInit.f16783.m21244(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m7299(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        if (i >= 0) {
            stringBuffer.append('+');
        } else {
            stringBuffer.append('-');
            i = -i;
        }
        int i2 = i / 3600000;
        FormatUtils.m21435(stringBuffer, i2, 2);
        int i3 = i - (i2 * 3600000);
        int i4 = i3 / 60000;
        stringBuffer.append(':');
        FormatUtils.m21435(stringBuffer, i4, 2);
        int i5 = i3 - (i4 * 60000);
        if (i5 == 0) {
            return stringBuffer.toString();
        }
        int i6 = i5 / 1000;
        stringBuffer.append(':');
        FormatUtils.m21435(stringBuffer, i6, 2);
        int i7 = i5 - (i6 * 1000);
        if (i7 == 0) {
            return stringBuffer.toString();
        }
        stringBuffer.append('.');
        FormatUtils.m21435(stringBuffer, i7, 3);
        return stringBuffer.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m7300(String str) {
        StringBuilder sb = new StringBuilder(str);
        for (int i = 0; i < sb.length(); i++) {
            int digit = Character.digit(sb.charAt(i), 10);
            if (digit >= 0) {
                sb.setCharAt(i, (char) (digit + 48));
            }
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static DateTimeZone m7301(String str, int i) {
        return i == 0 ? UTC : new FixedDateTimeZone(str, (String) null, i, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Provider m7302() {
        try {
            String property = System.getProperty("org.joda.time.DateTimeZone.Provider");
            if (property != null) {
                return m7303((Provider) Class.forName(property).newInstance());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } catch (SecurityException e2) {
        }
        try {
            String property2 = System.getProperty("org.joda.time.DateTimeZone.Folder");
            if (property2 != null) {
                return m7303((Provider) new ZoneInfoProvider(new File(property2)));
            }
        } catch (Exception e3) {
            throw new RuntimeException(e3);
        } catch (SecurityException e4) {
        }
        try {
            return m7303((Provider) new ZoneInfoProvider("org/joda/time/tz/data"));
        } catch (Exception e5) {
            e5.printStackTrace();
            return new UTCProvider();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Provider m7303(Provider provider) {
        Set<String> r0 = provider.m21661();
        if (r0 == null || r0.size() == 0) {
            throw new IllegalArgumentException("The provider doesn't have any available ids");
        } else if (!r0.contains("UTC")) {
            throw new IllegalArgumentException("The provider doesn't support UTC");
        } else if (UTC.equals(provider.m21662("UTC"))) {
            return provider;
        } else {
            throw new IllegalArgumentException("Invalid UTC zone provided");
        }
    }

    public long adjustOffset(long j, boolean z) {
        long j2 = j - 10800000;
        long offset = (long) getOffset(j2);
        long offset2 = (long) getOffset(10800000 + j);
        if (offset <= offset2) {
            return j;
        }
        long j3 = offset - offset2;
        long nextTransition = nextTransition(j2);
        long j4 = nextTransition - j3;
        return (j < j4 || j >= nextTransition + j3) ? j : j - j4 >= j3 ? !z ? j - j3 : j : z ? j + j3 : j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005e A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long convertLocalToUTC(long r14, boolean r16) {
        /*
            r13 = this;
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r10 = 0
            int r5 = r13.getOffset((long) r14)
            long r0 = (long) r5
            long r0 = r14 - r0
            int r4 = r13.getOffset((long) r0)
            if (r5 == r4) goto L_0x005f
            if (r16 != 0) goto L_0x0018
            if (r5 >= 0) goto L_0x005f
        L_0x0018:
            long r0 = (long) r5
            long r0 = r14 - r0
            long r0 = r13.nextTransition(r0)
            long r6 = (long) r5
            long r6 = r14 - r6
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x0027
            r0 = r2
        L_0x0027:
            long r6 = (long) r4
            long r6 = r14 - r6
            long r6 = r13.nextTransition(r6)
            long r8 = (long) r4
            long r8 = r14 - r8
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 != 0) goto L_0x0061
        L_0x0035:
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x005f
            if (r16 == 0) goto L_0x0045
            org.joda.time.IllegalInstantException r0 = new org.joda.time.IllegalInstantException
            java.lang.String r1 = r13.getID()
            r0.<init>(r14, r1)
            throw r0
        L_0x0045:
            r0 = r5
        L_0x0046:
            long r2 = (long) r0
            long r2 = r14 - r2
            long r4 = r14 ^ r2
            int r1 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r1 >= 0) goto L_0x005e
            long r0 = (long) r0
            long r0 = r0 ^ r14
            int r0 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x005e
            java.lang.ArithmeticException r0 = new java.lang.ArithmeticException
            java.lang.String r1 = "Subtracting time zone offset caused overflow"
            r0.<init>(r1)
            throw r0
        L_0x005e:
            return r2
        L_0x005f:
            r0 = r4
            goto L_0x0046
        L_0x0061:
            r2 = r6
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: org.joda.time.DateTimeZone.convertLocalToUTC(long, boolean):long");
    }

    public long convertLocalToUTC(long j, boolean z, long j2) {
        int offset = getOffset(j2);
        long j3 = j - ((long) offset);
        return getOffset(j3) == offset ? j3 : convertLocalToUTC(j, z);
    }

    public long convertUTCToLocal(long j) {
        int offset = getOffset(j);
        long j2 = ((long) offset) + j;
        if ((j ^ j2) >= 0 || (((long) offset) ^ j) < 0) {
            return j2;
        }
        throw new ArithmeticException("Adding time zone offset caused overflow");
    }

    public abstract boolean equals(Object obj);

    @ToString
    public final String getID() {
        return this.iID;
    }

    public long getMillisKeepLocal(DateTimeZone dateTimeZone, long j) {
        DateTimeZone dateTimeZone2 = dateTimeZone == null ? getDefault() : dateTimeZone;
        return dateTimeZone2 == this ? j : dateTimeZone2.convertLocalToUTC(convertUTCToLocal(j), false, j);
    }

    public final String getName(long j) {
        return getName(j, (Locale) null);
    }

    public String getName(long j, Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        String nameKey = getNameKey(j);
        if (nameKey == null) {
            return this.iID;
        }
        NameProvider nameProvider = getNameProvider();
        String r0 = nameProvider instanceof DefaultNameProvider ? ((DefaultNameProvider) nameProvider).m21656(locale, this.iID, nameKey, isStandardOffset(j)) : nameProvider.m21659(locale, this.iID, nameKey);
        return r0 == null ? m7299(getOffset(j)) : r0;
    }

    public abstract String getNameKey(long j);

    public abstract int getOffset(long j);

    public final int getOffset(ReadableInstant readableInstant) {
        return readableInstant == null ? getOffset(DateTimeUtils.m20891()) : getOffset(readableInstant.getMillis());
    }

    public int getOffsetFromLocal(long j) {
        long j2 = Long.MAX_VALUE;
        int offset = getOffset(j);
        long j3 = j - ((long) offset);
        int offset2 = getOffset(j3);
        if (offset != offset2) {
            if (offset - offset2 < 0) {
                long nextTransition = nextTransition(j3);
                if (nextTransition == j - ((long) offset)) {
                    nextTransition = Long.MAX_VALUE;
                }
                long nextTransition2 = nextTransition(j - ((long) offset2));
                if (nextTransition2 != j - ((long) offset2)) {
                    j2 = nextTransition2;
                }
                if (nextTransition != j2) {
                    return offset;
                }
            }
        } else if (offset >= 0) {
            long previousTransition = previousTransition(j3);
            if (previousTransition < j3) {
                int offset3 = getOffset(previousTransition);
                if (j3 - previousTransition <= ((long) (offset3 - offset))) {
                    return offset3;
                }
            }
        }
        return offset2;
    }

    public final String getShortName(long j) {
        return getShortName(j, (Locale) null);
    }

    public String getShortName(long j, Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        String nameKey = getNameKey(j);
        if (nameKey == null) {
            return this.iID;
        }
        NameProvider nameProvider = getNameProvider();
        String r0 = nameProvider instanceof DefaultNameProvider ? ((DefaultNameProvider) nameProvider).m21658(locale, this.iID, nameKey, isStandardOffset(j)) : nameProvider.m21660(locale, this.iID, nameKey);
        return r0 == null ? m7299(getOffset(j)) : r0;
    }

    public abstract int getStandardOffset(long j);

    public int hashCode() {
        return getID().hashCode() + 57;
    }

    public abstract boolean isFixed();

    public boolean isLocalDateTimeGap(LocalDateTime localDateTime) {
        if (isFixed()) {
            return false;
        }
        try {
            localDateTime.toDateTime(this);
            return false;
        } catch (IllegalInstantException e) {
            return true;
        }
    }

    public boolean isStandardOffset(long j) {
        return getOffset(j) == getStandardOffset(j);
    }

    public abstract long nextTransition(long j);

    public abstract long previousTransition(long j);

    public String toString() {
        return getID();
    }

    public TimeZone toTimeZone() {
        return TimeZone.getTimeZone(this.iID);
    }

    /* access modifiers changed from: protected */
    public Object writeReplace() throws ObjectStreamException {
        return new Stub(this.iID);
    }

    private static final class Stub implements Serializable {
        private static final long serialVersionUID = -6471952376487863581L;

        /* renamed from: 龘  reason: contains not printable characters */
        private transient String f16785;

        Stub(String str) {
            this.f16785 = str;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeUTF(this.f16785);
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException {
            this.f16785 = objectInputStream.readUTF();
        }

        private Object readResolve() throws ObjectStreamException {
            return DateTimeZone.forID(this.f16785);
        }
    }

    static final class LazyInit {

        /* renamed from: 靐  reason: contains not printable characters */
        static final DateTimeFormatter f16783 = m20905();

        /* renamed from: 龘  reason: contains not printable characters */
        static final Map<String, String> f16784 = m20904();

        /* renamed from: 龘  reason: contains not printable characters */
        private static DateTimeFormatter m20905() {
            return new DateTimeFormatterBuilder().m21305((String) null, true, 2, 4).m21298().m21248((Chronology) new BaseChronology() {
                private static final long serialVersionUID = -3128740902654445468L;

                public DateTimeZone getZone() {
                    return null;
                }

                public Chronology withUTC() {
                    return this;
                }

                public Chronology withZone(DateTimeZone dateTimeZone) {
                    return this;
                }

                public String toString() {
                    return getClass().getName();
                }
            });
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private static Map<String, String> m20904() {
            HashMap hashMap = new HashMap();
            hashMap.put(TimeZones.GMT_ID, "UTC");
            hashMap.put("WET", "WET");
            hashMap.put("CET", "CET");
            hashMap.put("MET", "CET");
            hashMap.put("ECT", "CET");
            hashMap.put("EET", "EET");
            hashMap.put("MIT", "Pacific/Apia");
            hashMap.put("HST", "Pacific/Honolulu");
            hashMap.put("AST", "America/Anchorage");
            hashMap.put("PST", "America/Los_Angeles");
            hashMap.put("MST", "America/Denver");
            hashMap.put("PNT", "America/Phoenix");
            hashMap.put("CST", "America/Chicago");
            hashMap.put("EST", "America/New_York");
            hashMap.put("IET", "America/Indiana/Indianapolis");
            hashMap.put("PRT", "America/Puerto_Rico");
            hashMap.put("CNT", "America/St_Johns");
            hashMap.put("AGT", "America/Argentina/Buenos_Aires");
            hashMap.put("BET", "America/Sao_Paulo");
            hashMap.put("ART", "Africa/Cairo");
            hashMap.put("CAT", "Africa/Harare");
            hashMap.put("EAT", "Africa/Addis_Ababa");
            hashMap.put("NET", "Asia/Yerevan");
            hashMap.put("PLT", "Asia/Karachi");
            hashMap.put("IST", "Asia/Kolkata");
            hashMap.put("BST", "Asia/Dhaka");
            hashMap.put("VST", "Asia/Ho_Chi_Minh");
            hashMap.put("CTT", "Asia/Shanghai");
            hashMap.put("JST", "Asia/Tokyo");
            hashMap.put("ACT", "Australia/Darwin");
            hashMap.put("AET", "Australia/Sydney");
            hashMap.put("SST", "Pacific/Guadalcanal");
            hashMap.put("NST", "Pacific/Auckland");
            return Collections.unmodifiableMap(hashMap);
        }
    }
}
