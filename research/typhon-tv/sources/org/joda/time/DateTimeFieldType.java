package org.joda.time;

import java.io.Serializable;

public abstract class DateTimeFieldType implements Serializable {
    private static final long serialVersionUID = -42615285973990L;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final DateTimeFieldType f16755 = new StandardDateTimeFieldType("dayOfYear", (byte) 6, DurationFieldType.days(), DurationFieldType.years());
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final DateTimeFieldType f16756 = new StandardDateTimeFieldType("monthOfYear", (byte) 7, DurationFieldType.months(), DurationFieldType.years());
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final DateTimeFieldType f16757 = new StandardDateTimeFieldType("dayOfMonth", (byte) 8, DurationFieldType.days(), DurationFieldType.months());
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final DateTimeFieldType f16758 = new StandardDateTimeFieldType("halfdayOfDay", (byte) 13, DurationFieldType.halfdays(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final DateTimeFieldType f16759 = new StandardDateTimeFieldType("hourOfHalfday", (byte) 14, DurationFieldType.hours(), DurationFieldType.halfdays());
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public static final DateTimeFieldType f16760 = new StandardDateTimeFieldType("secondOfDay", (byte) 20, DurationFieldType.seconds(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final DateTimeFieldType f16761 = new StandardDateTimeFieldType("dayOfWeek", (byte) 12, DurationFieldType.days(), DurationFieldType.weeks());
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public static final DateTimeFieldType f16762 = new StandardDateTimeFieldType("secondOfMinute", (byte) 21, DurationFieldType.seconds(), DurationFieldType.minutes());
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public static final DateTimeFieldType f16763 = new StandardDateTimeFieldType("hourOfDay", (byte) 17, DurationFieldType.hours(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final DateTimeFieldType f16764 = new StandardDateTimeFieldType("minuteOfDay", (byte) 18, DurationFieldType.minutes(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final DateTimeFieldType f16765 = new StandardDateTimeFieldType("minuteOfHour", (byte) 19, DurationFieldType.minutes(), DurationFieldType.hours());
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public static final DateTimeFieldType f16766 = new StandardDateTimeFieldType("millisOfDay", (byte) 22, DurationFieldType.millis(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final DateTimeFieldType f16767 = new StandardDateTimeFieldType("weekyearOfCentury", (byte) 9, DurationFieldType.weekyears(), DurationFieldType.centuries());
    /* access modifiers changed from: private */

    /* renamed from: י  reason: contains not printable characters */
    public static final DateTimeFieldType f16768 = new StandardDateTimeFieldType("millisOfSecond", (byte) 23, DurationFieldType.millis(), DurationFieldType.seconds());
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public static final DateTimeFieldType f16769 = new StandardDateTimeFieldType("weekyear", (byte) 10, DurationFieldType.weekyears(), (DurationFieldType) null);
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final DateTimeFieldType f16770 = new StandardDateTimeFieldType("weekOfWeekyear", (byte) 11, DurationFieldType.weeks(), DurationFieldType.weekyears());
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final DateTimeFieldType f16771 = new StandardDateTimeFieldType("year", (byte) 5, DurationFieldType.years(), (DurationFieldType) null);
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final DateTimeFieldType f16772 = new StandardDateTimeFieldType("yearOfEra", (byte) 2, DurationFieldType.years(), DurationFieldType.eras());
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static final DateTimeFieldType f16773 = new StandardDateTimeFieldType("yearOfCentury", (byte) 4, DurationFieldType.years(), DurationFieldType.centuries());
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final DateTimeFieldType f16774 = new StandardDateTimeFieldType("centuryOfEra", (byte) 3, DurationFieldType.centuries(), DurationFieldType.eras());
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final DateTimeFieldType f16775 = new StandardDateTimeFieldType("era", (byte) 1, DurationFieldType.eras(), (DurationFieldType) null);
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static final DateTimeFieldType f16776 = new StandardDateTimeFieldType("clockhourOfHalfday", (byte) 15, DurationFieldType.hours(), DurationFieldType.halfdays());
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static final DateTimeFieldType f16777 = new StandardDateTimeFieldType("clockhourOfDay", (byte) 16, DurationFieldType.hours(), DurationFieldType.days());
    private final String iName;

    public abstract DurationFieldType getDurationType();

    public abstract DateTimeField getField(Chronology chronology);

    public abstract DurationFieldType getRangeDurationType();

    protected DateTimeFieldType(String str) {
        this.iName = str;
    }

    public static DateTimeFieldType millisOfSecond() {
        return f16768;
    }

    public static DateTimeFieldType millisOfDay() {
        return f16766;
    }

    public static DateTimeFieldType secondOfMinute() {
        return f16762;
    }

    public static DateTimeFieldType secondOfDay() {
        return f16760;
    }

    public static DateTimeFieldType minuteOfHour() {
        return f16765;
    }

    public static DateTimeFieldType minuteOfDay() {
        return f16764;
    }

    public static DateTimeFieldType hourOfDay() {
        return f16763;
    }

    public static DateTimeFieldType clockhourOfDay() {
        return f16777;
    }

    public static DateTimeFieldType hourOfHalfday() {
        return f16759;
    }

    public static DateTimeFieldType clockhourOfHalfday() {
        return f16776;
    }

    public static DateTimeFieldType halfdayOfDay() {
        return f16758;
    }

    public static DateTimeFieldType dayOfWeek() {
        return f16761;
    }

    public static DateTimeFieldType dayOfMonth() {
        return f16757;
    }

    public static DateTimeFieldType dayOfYear() {
        return f16755;
    }

    public static DateTimeFieldType weekOfWeekyear() {
        return f16770;
    }

    public static DateTimeFieldType weekyear() {
        return f16769;
    }

    public static DateTimeFieldType weekyearOfCentury() {
        return f16767;
    }

    public static DateTimeFieldType monthOfYear() {
        return f16756;
    }

    public static DateTimeFieldType year() {
        return f16771;
    }

    public static DateTimeFieldType yearOfEra() {
        return f16772;
    }

    public static DateTimeFieldType yearOfCentury() {
        return f16773;
    }

    public static DateTimeFieldType centuryOfEra() {
        return f16774;
    }

    public static DateTimeFieldType era() {
        return f16775;
    }

    public String getName() {
        return this.iName;
    }

    public boolean isSupported(Chronology chronology) {
        return getField(chronology).isSupported();
    }

    public String toString() {
        return getName();
    }

    private static class StandardDateTimeFieldType extends DateTimeFieldType {
        private static final long serialVersionUID = -9937958251642L;
        private final byte iOrdinal;

        /* renamed from: 靐  reason: contains not printable characters */
        private final transient DurationFieldType f16778;

        /* renamed from: 龘  reason: contains not printable characters */
        private final transient DurationFieldType f16779;

        StandardDateTimeFieldType(String str, byte b, DurationFieldType durationFieldType, DurationFieldType durationFieldType2) {
            super(str);
            this.iOrdinal = b;
            this.f16779 = durationFieldType;
            this.f16778 = durationFieldType2;
        }

        public DurationFieldType getDurationType() {
            return this.f16779;
        }

        public DurationFieldType getRangeDurationType() {
            return this.f16778;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StandardDateTimeFieldType)) {
                return false;
            }
            if (this.iOrdinal != ((StandardDateTimeFieldType) obj).iOrdinal) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return 1 << this.iOrdinal;
        }

        public DateTimeField getField(Chronology chronology) {
            Chronology r0 = DateTimeUtils.m20895(chronology);
            switch (this.iOrdinal) {
                case 1:
                    return r0.era();
                case 2:
                    return r0.yearOfEra();
                case 3:
                    return r0.centuryOfEra();
                case 4:
                    return r0.yearOfCentury();
                case 5:
                    return r0.year();
                case 6:
                    return r0.dayOfYear();
                case 7:
                    return r0.monthOfYear();
                case 8:
                    return r0.dayOfMonth();
                case 9:
                    return r0.weekyearOfCentury();
                case 10:
                    return r0.weekyear();
                case 11:
                    return r0.weekOfWeekyear();
                case 12:
                    return r0.dayOfWeek();
                case 13:
                    return r0.halfdayOfDay();
                case 14:
                    return r0.hourOfHalfday();
                case 15:
                    return r0.clockhourOfHalfday();
                case 16:
                    return r0.clockhourOfDay();
                case 17:
                    return r0.hourOfDay();
                case 18:
                    return r0.minuteOfDay();
                case 19:
                    return r0.minuteOfHour();
                case 20:
                    return r0.secondOfDay();
                case 21:
                    return r0.secondOfMinute();
                case 22:
                    return r0.millisOfDay();
                case 23:
                    return r0.millisOfSecond();
                default:
                    throw new InternalError();
            }
        }

        private Object readResolve() {
            switch (this.iOrdinal) {
                case 1:
                    return DateTimeFieldType.f16775;
                case 2:
                    return DateTimeFieldType.f16772;
                case 3:
                    return DateTimeFieldType.f16774;
                case 4:
                    return DateTimeFieldType.f16773;
                case 5:
                    return DateTimeFieldType.f16771;
                case 6:
                    return DateTimeFieldType.f16755;
                case 7:
                    return DateTimeFieldType.f16756;
                case 8:
                    return DateTimeFieldType.f16757;
                case 9:
                    return DateTimeFieldType.f16767;
                case 10:
                    return DateTimeFieldType.f16769;
                case 11:
                    return DateTimeFieldType.f16770;
                case 12:
                    return DateTimeFieldType.f16761;
                case 13:
                    return DateTimeFieldType.f16758;
                case 14:
                    return DateTimeFieldType.f16759;
                case 15:
                    return DateTimeFieldType.f16776;
                case 16:
                    return DateTimeFieldType.f16777;
                case 17:
                    return DateTimeFieldType.f16763;
                case 18:
                    return DateTimeFieldType.f16764;
                case 19:
                    return DateTimeFieldType.f16765;
                case 20:
                    return DateTimeFieldType.f16760;
                case 21:
                    return DateTimeFieldType.f16762;
                case 22:
                    return DateTimeFieldType.f16766;
                case 23:
                    return DateTimeFieldType.f16768;
                default:
                    return this;
            }
        }
    }
}
