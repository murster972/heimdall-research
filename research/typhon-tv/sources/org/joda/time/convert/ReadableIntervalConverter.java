package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.ReadWritableInterval;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadableInterval;

class ReadableIntervalConverter extends AbstractConverter implements DurationConverter, IntervalConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadableIntervalConverter f17019 = new ReadableIntervalConverter();

    protected ReadableIntervalConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21183(Object obj) {
        return ((ReadableInterval) obj).toDurationMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21186(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        ReadableInterval readableInterval = (ReadableInterval) obj;
        int[] iArr = (chronology != null ? chronology : DateTimeUtils.m20897(readableInterval)).get(readWritablePeriod, readableInterval.getStartMillis(), readableInterval.getEndMillis());
        for (int i = 0; i < iArr.length; i++) {
            readWritablePeriod.setValue(i, iArr[i]);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m21182(Object obj, Chronology chronology) {
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21185(ReadWritableInterval readWritableInterval, Object obj, Chronology chronology) {
        ReadableInterval readableInterval = (ReadableInterval) obj;
        readWritableInterval.setInterval(readableInterval);
        if (chronology != null) {
            readWritableInterval.setChronology(chronology);
        } else {
            readWritableInterval.setChronology(readableInterval.getChronology());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21184() {
        return ReadableInterval.class;
    }
}
