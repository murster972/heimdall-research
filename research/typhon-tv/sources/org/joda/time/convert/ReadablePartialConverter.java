package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

class ReadablePartialConverter extends AbstractConverter implements PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadablePartialConverter f17020 = new ReadablePartialConverter();

    protected ReadablePartialConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m21189(Object obj, DateTimeZone dateTimeZone) {
        return m21187(obj, (Chronology) null).withZone(dateTimeZone);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m21187(Object obj, Chronology chronology) {
        if (chronology == null) {
            return DateTimeUtils.m20895(((ReadablePartial) obj).getChronology());
        }
        return chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m21190(ReadablePartial readablePartial, Object obj, Chronology chronology) {
        ReadablePartial readablePartial2 = (ReadablePartial) obj;
        int size = readablePartial.size();
        int[] iArr = new int[size];
        for (int i = 0; i < size; i++) {
            iArr[i] = readablePartial2.get(readablePartial.getFieldType(i));
        }
        chronology.validate(readablePartial, iArr);
        return iArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21188() {
        return ReadablePartial.class;
    }
}
