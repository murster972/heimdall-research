package org.joda.time.convert;

public final class ConverterManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static ConverterManager f17004;

    /* renamed from: ʻ  reason: contains not printable characters */
    private ConverterSet f17005 = new ConverterSet(new Converter[]{ReadableIntervalConverter.f17019, StringConverter.f17022, NullConverter.f17016});

    /* renamed from: 连任  reason: contains not printable characters */
    private ConverterSet f17006 = new ConverterSet(new Converter[]{ReadableDurationConverter.f17017, ReadablePeriodConverter.f17021, ReadableIntervalConverter.f17019, StringConverter.f17022, NullConverter.f17016});

    /* renamed from: 靐  reason: contains not printable characters */
    private ConverterSet f17007 = new ConverterSet(new Converter[]{ReadableInstantConverter.f17018, StringConverter.f17022, CalendarConverter.f17003, DateConverter.f17014, LongConverter.f17015, NullConverter.f17016});

    /* renamed from: 麤  reason: contains not printable characters */
    private ConverterSet f17008 = new ConverterSet(new Converter[]{ReadableDurationConverter.f17017, ReadableIntervalConverter.f17019, StringConverter.f17022, LongConverter.f17015, NullConverter.f17016});

    /* renamed from: 齉  reason: contains not printable characters */
    private ConverterSet f17009 = new ConverterSet(new Converter[]{ReadablePartialConverter.f17020, ReadableInstantConverter.f17018, StringConverter.f17022, CalendarConverter.f17003, DateConverter.f17014, LongConverter.f17015, NullConverter.f17016});

    /* renamed from: 龘  reason: contains not printable characters */
    public static ConverterManager m21146() {
        if (f17004 == null) {
            f17004 = new ConverterManager();
        }
        return f17004;
    }

    protected ConverterManager() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public InstantConverter m21151(Object obj) {
        InstantConverter instantConverter = (InstantConverter) this.f17007.m21154(obj == null ? null : obj.getClass());
        if (instantConverter != null) {
            return instantConverter;
        }
        throw new IllegalArgumentException("No instant converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PartialConverter m21148(Object obj) {
        PartialConverter partialConverter = (PartialConverter) this.f17009.m21154(obj == null ? null : obj.getClass());
        if (partialConverter != null) {
            return partialConverter;
        }
        throw new IllegalArgumentException("No partial converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DurationConverter m21150(Object obj) {
        DurationConverter durationConverter = (DurationConverter) this.f17008.m21154(obj == null ? null : obj.getClass());
        if (durationConverter != null) {
            return durationConverter;
        }
        throw new IllegalArgumentException("No duration converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public PeriodConverter m21149(Object obj) {
        PeriodConverter periodConverter = (PeriodConverter) this.f17006.m21154(obj == null ? null : obj.getClass());
        if (periodConverter != null) {
            return periodConverter;
        }
        throw new IllegalArgumentException("No period converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public IntervalConverter m21147(Object obj) {
        IntervalConverter intervalConverter = (IntervalConverter) this.f17005.m21154(obj == null ? null : obj.getClass());
        if (intervalConverter != null) {
            return intervalConverter;
        }
        throw new IllegalArgumentException("No interval converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    public String toString() {
        return "ConverterManager[" + this.f17007.m21153() + " instant," + this.f17009.m21153() + " partial," + this.f17008.m21153() + " duration," + this.f17006.m21153() + " period," + this.f17005.m21153() + " interval]";
    }
}
