package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormatter;

public interface PartialConverter extends Converter {
    /* renamed from: 靐  reason: contains not printable characters */
    Chronology m21171(Object obj, Chronology chronology);

    /* renamed from: 龘  reason: contains not printable characters */
    Chronology m21172(Object obj, DateTimeZone dateTimeZone);

    /* renamed from: 龘  reason: contains not printable characters */
    int[] m21173(ReadablePartial readablePartial, Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter);
}
