package org.joda.time.convert;

import org.joda.time.Chronology;

class LongConverter extends AbstractConverter implements DurationConverter, InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final LongConverter f17015 = new LongConverter();

    protected LongConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21165(Object obj, Chronology chronology) {
        return ((Long) obj).longValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21164(Object obj) {
        return ((Long) obj).longValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21166() {
        return Long.class;
    }
}
