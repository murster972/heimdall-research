package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableInstant;
import org.joda.time.chrono.ISOChronology;

class ReadableInstantConverter extends AbstractConverter implements InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadableInstantConverter f17018 = new ReadableInstantConverter();

    protected ReadableInstantConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m21181(Object obj, DateTimeZone dateTimeZone) {
        Chronology chronology = ((ReadableInstant) obj).getChronology();
        if (chronology == null) {
            return ISOChronology.getInstance(dateTimeZone);
        }
        if (chronology.getZone() == dateTimeZone) {
            return chronology;
        }
        Chronology withZone = chronology.withZone(dateTimeZone);
        if (withZone == null) {
            return ISOChronology.getInstance(dateTimeZone);
        }
        return withZone;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m21178(Object obj, Chronology chronology) {
        if (chronology == null) {
            return DateTimeUtils.m20895(((ReadableInstant) obj).getChronology());
        }
        return chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21179(Object obj, Chronology chronology) {
        return ((ReadableInstant) obj).getMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21180() {
        return ReadableInstant.class;
    }
}
