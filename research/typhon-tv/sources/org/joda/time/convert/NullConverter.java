package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.ReadWritableInterval;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePeriod;

class NullConverter extends AbstractConverter implements DurationConverter, InstantConverter, IntervalConverter, PartialConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final NullConverter f17016 = new NullConverter();

    protected NullConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21167(Object obj) {
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21170(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        readWritablePeriod.setPeriod((ReadablePeriod) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21169(ReadWritableInterval readWritableInterval, Object obj, Chronology chronology) {
        readWritableInterval.setChronology(chronology);
        long r0 = DateTimeUtils.m20891();
        readWritableInterval.setInterval(r0, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21168() {
        return null;
    }
}
