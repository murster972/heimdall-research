package org.joda.time.convert;

class ConverterSet {

    /* renamed from: 靐  reason: contains not printable characters */
    private Entry[] f17010 = new Entry[16];

    /* renamed from: 龘  reason: contains not printable characters */
    private final Converter[] f17011;

    ConverterSet(Converter[] converterArr) {
        this.f17011 = converterArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Converter m21154(Class<?> cls) throws IllegalStateException {
        Entry[] entryArr = this.f17010;
        int length = entryArr.length;
        int hashCode = cls == null ? 0 : cls.hashCode() & (length - 1);
        while (true) {
            Entry entry = entryArr[hashCode];
            if (entry == null) {
                Converter r3 = m21152(this, cls);
                Entry entry2 = new Entry(cls, r3);
                Entry[] entryArr2 = (Entry[]) entryArr.clone();
                entryArr2[hashCode] = entry2;
                for (int i = 0; i < length; i++) {
                    if (entryArr2[i] == null) {
                        this.f17010 = entryArr2;
                        return r3;
                    }
                }
                int i2 = length << 1;
                Entry[] entryArr3 = new Entry[i2];
                for (int i3 = 0; i3 < length; i3++) {
                    Entry entry3 = entryArr2[i3];
                    Class<?> cls2 = entry3.f17013;
                    int hashCode2 = cls2 == null ? 0 : cls2.hashCode() & (i2 - 1);
                    while (entryArr3[hashCode2] != null) {
                        int i4 = hashCode2 + 1;
                        if (i4 >= i2) {
                            i4 = 0;
                        }
                    }
                    entryArr3[hashCode2] = entry3;
                }
                this.f17010 = entryArr3;
                return r3;
            } else if (entry.f17013 == cls) {
                return entry.f17012;
            } else {
                int i5 = hashCode + 1;
                if (i5 >= length) {
                    hashCode = 0;
                } else {
                    hashCode = i5;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21153() {
        return this.f17011.length;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ConverterSet m21155(int i, Converter[] converterArr) {
        int i2;
        Converter[] converterArr2 = this.f17011;
        int length = converterArr2.length;
        if (i >= length) {
            throw new IndexOutOfBoundsException();
        }
        if (converterArr != null) {
            converterArr[0] = converterArr2[i];
        }
        Converter[] converterArr3 = new Converter[(length - 1)];
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            if (i3 != i) {
                i2 = i4 + 1;
                converterArr3[i4] = converterArr2[i3];
            } else {
                i2 = i4;
            }
            i3++;
            i4 = i2;
        }
        return new ConverterSet(converterArr3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Converter m21152(ConverterSet converterSet, Class<?> cls) {
        Converter[] converterArr = converterSet.f17011;
        int length = converterArr.length;
        int i = length;
        int i2 = length;
        ConverterSet converterSet2 = converterSet;
        while (true) {
            int i3 = i - 1;
            if (i3 >= 0) {
                Converter converter = converterArr[i3];
                Class<?> r7 = converter.m21145();
                if (r7 == cls) {
                    return converter;
                }
                if (r7 == null || (cls != null && !r7.isAssignableFrom(cls))) {
                    converterSet2 = converterSet2.m21155(i3, (Converter[]) null);
                    converterArr = converterSet2.f17011;
                    i2 = converterArr.length;
                }
                i = i3;
            } else if (cls == null || i2 == 0) {
                return null;
            } else {
                if (i2 == 1) {
                    return converterArr[0];
                }
                int i4 = i2;
                int i5 = i2;
                Converter[] converterArr2 = converterArr;
                ConverterSet converterSet3 = converterSet2;
                while (true) {
                    int i6 = i4 - 1;
                    if (i6 < 0) {
                        break;
                    }
                    Class<?> r8 = converterArr2[i6].m21145();
                    int i7 = i5;
                    int i8 = i5;
                    while (true) {
                        i7--;
                        if (i7 < 0) {
                            break;
                        } else if (i7 != i6 && converterArr2[i7].m21145().isAssignableFrom(r8)) {
                            converterSet3 = converterSet3.m21155(i7, (Converter[]) null);
                            converterArr2 = converterSet3.f17011;
                            int length2 = converterArr2.length;
                            i6 = length2 - 1;
                            i8 = length2;
                        }
                    }
                    i4 = i6;
                    i5 = i8;
                }
                if (i5 == 1) {
                    return converterArr2[0];
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to find best converter for type \"");
                sb.append(cls.getName());
                sb.append("\" from remaining set: ");
                for (int i9 = 0; i9 < i5; i9++) {
                    Converter converter2 = converterArr2[i9];
                    Class<?> r5 = converter2.m21145();
                    sb.append(converter2.getClass().getName());
                    sb.append('[');
                    sb.append(r5 == null ? null : r5.getName());
                    sb.append("], ");
                }
                throw new IllegalStateException(sb.toString());
            }
        }
    }

    static class Entry {

        /* renamed from: 靐  reason: contains not printable characters */
        final Converter f17012;

        /* renamed from: 龘  reason: contains not printable characters */
        final Class<?> f17013;

        Entry(Class<?> cls, Converter converter) {
            this.f17013 = cls;
            this.f17012 = converter;
        }
    }
}
