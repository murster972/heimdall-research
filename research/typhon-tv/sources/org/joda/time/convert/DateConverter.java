package org.joda.time.convert;

import java.util.Date;
import org.joda.time.Chronology;

final class DateConverter extends AbstractConverter implements InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final DateConverter f17014 = new DateConverter();

    protected DateConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21156(Object obj, Chronology chronology) {
        return ((Date) obj).getTime();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21157() {
        return Date.class;
    }
}
