package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.PeriodType;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePeriod;

class ReadablePeriodConverter extends AbstractConverter implements PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadablePeriodConverter f17021 = new ReadablePeriodConverter();

    protected ReadablePeriodConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21192(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        readWritablePeriod.setPeriod((ReadablePeriod) obj);
    }

    public PeriodType a_(Object obj) {
        return ((ReadablePeriod) obj).getPeriodType();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21191() {
        return ReadablePeriod.class;
    }
}
