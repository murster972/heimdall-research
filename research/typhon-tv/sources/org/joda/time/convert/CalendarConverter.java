package org.joda.time.convert;

import java.util.Calendar;
import java.util.GregorianCalendar;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.BuddhistChronology;
import org.joda.time.chrono.GJChronology;
import org.joda.time.chrono.GregorianChronology;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.JulianChronology;

final class CalendarConverter extends AbstractConverter implements InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final CalendarConverter f17003 = new CalendarConverter();

    protected CalendarConverter() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m21141(Object obj, Chronology chronology) {
        DateTimeZone dateTimeZone;
        if (chronology != null) {
            return chronology;
        }
        Calendar calendar = (Calendar) obj;
        try {
            dateTimeZone = DateTimeZone.forTimeZone(calendar.getTimeZone());
        } catch (IllegalArgumentException e) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return m21144((Object) calendar, dateTimeZone);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m21144(Object obj, DateTimeZone dateTimeZone) {
        if (obj.getClass().getName().endsWith(".BuddhistCalendar")) {
            return BuddhistChronology.getInstance(dateTimeZone);
        }
        if (!(obj instanceof GregorianCalendar)) {
            return ISOChronology.getInstance(dateTimeZone);
        }
        long time = ((GregorianCalendar) obj).getGregorianChange().getTime();
        if (time == Long.MIN_VALUE) {
            return GregorianChronology.getInstance(dateTimeZone);
        }
        if (time == Long.MAX_VALUE) {
            return JulianChronology.getInstance(dateTimeZone);
        }
        return GJChronology.getInstance(dateTimeZone, time, 4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21142(Object obj, Chronology chronology) {
        return ((Calendar) obj).getTime().getTime();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21143() {
        return Calendar.class;
    }
}
