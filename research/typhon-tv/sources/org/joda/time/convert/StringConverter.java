package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.ReadWritableInterval;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePartial;
import org.joda.time.ReadablePeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

class StringConverter extends AbstractConverter implements DurationConverter, InstantConverter, IntervalConverter, PartialConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final StringConverter f17022 = new StringConverter();

    protected StringConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21194(Object obj, Chronology chronology) {
        return ISODateTimeFormat.m21449().m21248(chronology).m21244((String) obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m21198(ReadablePartial readablePartial, Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter) {
        if (dateTimeFormatter.m21235() != null) {
            chronology = chronology.withZone(dateTimeFormatter.m21235());
        }
        return chronology.get(readablePartial, dateTimeFormatter.m21248(chronology).m21244((String) obj));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21193(Object obj) {
        long parseLong;
        int i;
        int i2 = 1;
        String str = (String) obj;
        int length = str.length();
        if (length < 4 || !((str.charAt(0) == 'P' || str.charAt(0) == 'p') && ((str.charAt(1) == 'T' || str.charAt(1) == 't') && (str.charAt(length - 1) == 'S' || str.charAt(length - 1) == 's')))) {
            throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
        }
        String substring = str.substring(2, length - 1);
        boolean z = false;
        int i3 = -1;
        for (int i4 = 0; i4 < substring.length(); i4++) {
            if (substring.charAt(i4) < '0' || substring.charAt(i4) > '9') {
                if (i4 == 0 && substring.charAt(0) == '-') {
                    z = true;
                } else {
                    if (z) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    if (i4 > i && substring.charAt(i4) == '.' && i3 == -1) {
                        i3 = i4;
                    } else {
                        throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
                    }
                }
            }
        }
        long j = 0;
        if (!z) {
            i2 = 0;
        }
        if (i3 > 0) {
            long parseLong2 = Long.parseLong(substring.substring(i2, i3));
            String substring2 = substring.substring(i3 + 1);
            if (substring2.length() != 3) {
                substring2 = (substring2 + "000").substring(0, 3);
            }
            j = (long) Integer.parseInt(substring2);
            parseLong = parseLong2;
        } else if (z) {
            parseLong = Long.parseLong(substring.substring(i2, substring.length()));
        } else {
            parseLong = Long.parseLong(substring);
        }
        if (z) {
            return FieldUtils.m21215(FieldUtils.m21214(-parseLong, 1000), -j);
        }
        return FieldUtils.m21215(FieldUtils.m21214(parseLong, 1000), j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21197(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        String str = (String) obj;
        PeriodFormatter r0 = ISOPeriodFormat.m21532();
        readWritablePeriod.clear();
        int r1 = r0.m21554(readWritablePeriod, str, 0);
        if (r1 < str.length()) {
            if (r1 < 0) {
                r0.m21557(readWritablePeriod.getPeriodType()).m21552(str);
            }
            throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21196(ReadWritableInterval readWritableInterval, Object obj, Chronology chronology) {
        Period period;
        Chronology chronology2;
        long j;
        String str = (String) obj;
        int indexOf = str.indexOf(47);
        if (indexOf < 0) {
            throw new IllegalArgumentException("Format requires a '/' separator: " + str);
        }
        String substring = str.substring(0, indexOf);
        if (substring.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        String substring2 = str.substring(indexOf + 1);
        if (substring2.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        DateTimeFormatter r6 = ISODateTimeFormat.m21449().m21248(chronology);
        PeriodFormatter r7 = ISOPeriodFormat.m21532();
        long j2 = 0;
        char charAt = substring.charAt(0);
        if (charAt == 'P' || charAt == 'p') {
            period = r7.m21557(a_(substring)).m21556(substring);
            chronology2 = null;
        } else {
            DateTime r0 = r6.m21236(substring);
            j2 = r0.getMillis();
            chronology2 = r0.getChronology();
            period = null;
        }
        char charAt2 = substring2.charAt(0);
        if (charAt2 != 'P' && charAt2 != 'p') {
            DateTime r1 = r6.m21236(substring2);
            long millis = r1.getMillis();
            if (chronology2 == null) {
                chronology2 = r1.getChronology();
            }
            if (chronology == null) {
                chronology = chronology2;
            }
            if (period != null) {
                j2 = chronology.add((ReadablePeriod) period, millis, -1);
                j = millis;
            } else {
                j = millis;
            }
        } else if (period != null) {
            throw new IllegalArgumentException("Interval composed of two durations: " + str);
        } else {
            Period r12 = r7.m21557(a_(substring2)).m21556(substring2);
            if (chronology == null) {
                chronology = chronology2;
            }
            j = chronology.add((ReadablePeriod) r12, j2, 1);
        }
        readWritableInterval.setInterval(j2, j);
        readWritableInterval.setChronology(chronology);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21195() {
        return String.class;
    }
}
