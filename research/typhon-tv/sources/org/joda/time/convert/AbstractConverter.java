package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.PeriodType;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormatter;

public abstract class AbstractConverter implements Converter {
    protected AbstractConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21137(Object obj, Chronology chronology) {
        return DateTimeUtils.m20891();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m21138(Object obj, DateTimeZone dateTimeZone) {
        return ISOChronology.getInstance(dateTimeZone);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m21135(Object obj, Chronology chronology) {
        return DateTimeUtils.m20895(chronology);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m21139(ReadablePartial readablePartial, Object obj, Chronology chronology) {
        return chronology.get(readablePartial, m21137(obj, chronology));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m21140(ReadablePartial readablePartial, Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter) {
        return m21139(readablePartial, obj, chronology);
    }

    public PeriodType a_(Object obj) {
        return PeriodType.standard();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m21136(Object obj, Chronology chronology) {
        return false;
    }

    public String toString() {
        return "Converter[" + (m21145() == null ? "null" : m21145().getName()) + "]";
    }
}
