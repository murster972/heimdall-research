package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadableDuration;
import org.joda.time.ReadablePeriod;

class ReadableDurationConverter extends AbstractConverter implements DurationConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadableDurationConverter f17017 = new ReadableDurationConverter();

    protected ReadableDurationConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21175(Object obj) {
        return ((ReadableDuration) obj).getMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21177(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        int[] iArr = DateTimeUtils.m20895(chronology).get((ReadablePeriod) readWritablePeriod, ((ReadableDuration) obj).getMillis());
        for (int i = 0; i < iArr.length; i++) {
            readWritablePeriod.setValue(i, iArr[i]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m21176() {
        return ReadableDuration.class;
    }
}
