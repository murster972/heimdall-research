package org.joda.time.field;

import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;

public abstract class ImpreciseDateTimeField extends BaseDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    final long f17030;

    /* renamed from: 龘  reason: contains not printable characters */
    private final DurationField f17031;

    public abstract long add(long j, int i);

    public abstract long add(long j, long j2);

    public ImpreciseDateTimeField(DateTimeFieldType dateTimeFieldType, long j) {
        super(dateTimeFieldType);
        this.f17030 = j;
        this.f17031 = new LinkedDurationField(dateTimeFieldType.getDurationType());
    }

    public int getDifference(long j, long j2) {
        return FieldUtils.m21213(getDifferenceAsLong(j, j2));
    }

    public long getDifferenceAsLong(long j, long j2) {
        if (j < j2) {
            return -getDifferenceAsLong(j2, j);
        }
        long j3 = (j - j2) / this.f17030;
        if (add(j2, j3) < j) {
            do {
                j3++;
            } while (add(j2, j3) <= j);
            return j3 - 1;
        } else if (add(j2, j3) <= j) {
            return j3;
        } else {
            do {
                j3--;
            } while (add(j2, j3) > j);
            return j3;
        }
    }

    public final DurationField getDurationField() {
        return this.f17031;
    }

    private final class LinkedDurationField extends BaseDurationField {
        private static final long serialVersionUID = -203813474600094134L;

        LinkedDurationField(DurationFieldType durationFieldType) {
            super(durationFieldType);
        }

        public boolean isPrecise() {
            return false;
        }

        public long getUnitMillis() {
            return ImpreciseDateTimeField.this.f17030;
        }

        public int getValue(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifference(j2 + j, j2);
        }

        public long getValueAsLong(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifferenceAsLong(j2 + j, j2);
        }

        public long getMillis(int i, long j) {
            return ImpreciseDateTimeField.this.add(j, i) - j;
        }

        public long getMillis(long j, long j2) {
            return ImpreciseDateTimeField.this.add(j2, j) - j2;
        }

        public long add(long j, int i) {
            return ImpreciseDateTimeField.this.add(j, i);
        }

        public long add(long j, long j2) {
            return ImpreciseDateTimeField.this.add(j, j2);
        }

        public int getDifference(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifferenceAsLong(j, j2);
        }
    }
}
