package org.joda.time.field;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;

public abstract class PreciseDurationDateTimeField extends BaseDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final DurationField f17037;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f17038;

    public PreciseDurationDateTimeField(DateTimeFieldType dateTimeFieldType, DurationField durationField) {
        super(dateTimeFieldType);
        if (!durationField.isPrecise()) {
            throw new IllegalArgumentException("Unit duration field must be precise");
        }
        this.f17038 = durationField.getUnitMillis();
        if (this.f17038 < 1) {
            throw new IllegalArgumentException("The unit milliseconds must be at least 1");
        }
        this.f17037 = durationField;
    }

    public boolean isLenient() {
        return false;
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, getMinimumValue(), m21220(j, i));
        return (((long) (i - get(j))) * this.f17038) + j;
    }

    public long roundFloor(long j) {
        if (j >= 0) {
            return j - (j % this.f17038);
        }
        long j2 = 1 + j;
        return (j2 - (j2 % this.f17038)) - this.f17038;
    }

    public long roundCeiling(long j) {
        if (j <= 0) {
            return j - (j % this.f17038);
        }
        long j2 = j - 1;
        return (j2 - (j2 % this.f17038)) + this.f17038;
    }

    public long remainder(long j) {
        if (j >= 0) {
            return j % this.f17038;
        }
        return (((j + 1) % this.f17038) + this.f17038) - 1;
    }

    public DurationField getDurationField() {
        return this.f17037;
    }

    public int getMinimumValue() {
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m21221() {
        return this.f17038;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21220(long j, int i) {
        return getMaximumValue(j);
    }
}
