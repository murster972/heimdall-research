package org.joda.time.field;

import org.joda.time.Chronology;
import org.joda.time.DateTimeField;

public final class SkipUndoDateTimeField extends DelegatedDateTimeField {
    private static final long serialVersionUID = -5875876968979L;
    private final Chronology iChronology;
    private final int iSkip;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient int f17043;

    public SkipUndoDateTimeField(Chronology chronology, DateTimeField dateTimeField) {
        this(chronology, dateTimeField, 0);
    }

    public SkipUndoDateTimeField(Chronology chronology, DateTimeField dateTimeField, int i) {
        super(dateTimeField);
        this.iChronology = chronology;
        int minimumValue = super.getMinimumValue();
        if (minimumValue < i) {
            this.f17043 = minimumValue + 1;
        } else if (minimumValue == i + 1) {
            this.f17043 = i;
        } else {
            this.f17043 = minimumValue;
        }
        this.iSkip = i;
    }

    public int get(long j) {
        int i = super.get(j);
        if (i < this.iSkip) {
            return i + 1;
        }
        return i;
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, this.f17043, getMaximumValue());
        if (i <= this.iSkip) {
            i--;
        }
        return super.set(j, i);
    }

    public int getMinimumValue() {
        return this.f17043;
    }

    private Object readResolve() {
        return getType().getField(this.iChronology);
    }
}
