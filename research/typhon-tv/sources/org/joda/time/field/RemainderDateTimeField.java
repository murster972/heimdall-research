package org.joda.time.field;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;

public class RemainderDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    final DurationField f17039;

    /* renamed from: 齉  reason: contains not printable characters */
    final DurationField f17040;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f17041;

    public RemainderDateTimeField(DateTimeField dateTimeField, DurationField durationField, DateTimeFieldType dateTimeFieldType, int i) {
        super(dateTimeField, dateTimeFieldType);
        if (i < 2) {
            throw new IllegalArgumentException("The divisor must be at least 2");
        }
        this.f17040 = durationField;
        this.f17039 = dateTimeField.getDurationField();
        this.f17041 = i;
    }

    public RemainderDateTimeField(DividedDateTimeField dividedDateTimeField) {
        this(dividedDateTimeField, dividedDateTimeField.getType());
    }

    public RemainderDateTimeField(DividedDateTimeField dividedDateTimeField, DateTimeFieldType dateTimeFieldType) {
        this(dividedDateTimeField, dividedDateTimeField.m21203().getDurationField(), dateTimeFieldType);
    }

    public RemainderDateTimeField(DividedDateTimeField dividedDateTimeField, DurationField durationField, DateTimeFieldType dateTimeFieldType) {
        super(dividedDateTimeField.m21203(), dateTimeFieldType);
        this.f17041 = dividedDateTimeField.f17029;
        this.f17039 = durationField;
        this.f17040 = dividedDateTimeField.f17026;
    }

    public int get(long j) {
        int i = m21203().get(j);
        if (i >= 0) {
            return i % this.f17041;
        }
        return ((i + 1) % this.f17041) + (this.f17041 - 1);
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m21212(get(j), i, 0, this.f17041 - 1));
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, 0, this.f17041 - 1);
        return m21203().set(j, (m21222(m21203().get(j)) * this.f17041) + i);
    }

    public DurationField getDurationField() {
        return this.f17039;
    }

    public DurationField getRangeDurationField() {
        return this.f17040;
    }

    public int getMinimumValue() {
        return 0;
    }

    public int getMaximumValue() {
        return this.f17041 - 1;
    }

    public long roundFloor(long j) {
        return m21203().roundFloor(j);
    }

    public long roundCeiling(long j) {
        return m21203().roundCeiling(j);
    }

    public long roundHalfFloor(long j) {
        return m21203().roundHalfFloor(j);
    }

    public long roundHalfCeiling(long j) {
        return m21203().roundHalfCeiling(j);
    }

    public long roundHalfEven(long j) {
        return m21203().roundHalfEven(j);
    }

    public long remainder(long j) {
        return m21203().remainder(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m21222(int i) {
        if (i >= 0) {
            return i / this.f17041;
        }
        return ((i + 1) / this.f17041) - 1;
    }
}
