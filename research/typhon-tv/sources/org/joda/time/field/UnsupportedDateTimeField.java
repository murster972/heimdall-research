package org.joda.time.field;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;

public final class UnsupportedDateTimeField extends DateTimeField implements Serializable {
    private static final long serialVersionUID = -1934618396111902255L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static HashMap<DateTimeFieldType, UnsupportedDateTimeField> f17044;
    private final DurationField iDurationField;
    private final DateTimeFieldType iType;

    public static synchronized UnsupportedDateTimeField getInstance(DateTimeFieldType dateTimeFieldType, DurationField durationField) {
        UnsupportedDateTimeField unsupportedDateTimeField;
        synchronized (UnsupportedDateTimeField.class) {
            if (f17044 == null) {
                f17044 = new HashMap<>(7);
                unsupportedDateTimeField = null;
            } else {
                unsupportedDateTimeField = f17044.get(dateTimeFieldType);
                if (!(unsupportedDateTimeField == null || unsupportedDateTimeField.getDurationField() == durationField)) {
                    unsupportedDateTimeField = null;
                }
            }
            if (unsupportedDateTimeField == null) {
                unsupportedDateTimeField = new UnsupportedDateTimeField(dateTimeFieldType, durationField);
                f17044.put(dateTimeFieldType, unsupportedDateTimeField);
            }
        }
        return unsupportedDateTimeField;
    }

    private UnsupportedDateTimeField(DateTimeFieldType dateTimeFieldType, DurationField durationField) {
        if (dateTimeFieldType == null || durationField == null) {
            throw new IllegalArgumentException();
        }
        this.iType = dateTimeFieldType;
        this.iDurationField = durationField;
    }

    public DateTimeFieldType getType() {
        return this.iType;
    }

    public String getName() {
        return this.iType.getName();
    }

    public boolean isSupported() {
        return false;
    }

    public boolean isLenient() {
        return false;
    }

    public int get(long j) {
        throw m21223();
    }

    public String getAsText(long j, Locale locale) {
        throw m21223();
    }

    public String getAsText(long j) {
        throw m21223();
    }

    public String getAsText(ReadablePartial readablePartial, int i, Locale locale) {
        throw m21223();
    }

    public String getAsText(ReadablePartial readablePartial, Locale locale) {
        throw m21223();
    }

    public String getAsText(int i, Locale locale) {
        throw m21223();
    }

    public String getAsShortText(long j, Locale locale) {
        throw m21223();
    }

    public String getAsShortText(long j) {
        throw m21223();
    }

    public String getAsShortText(ReadablePartial readablePartial, int i, Locale locale) {
        throw m21223();
    }

    public String getAsShortText(ReadablePartial readablePartial, Locale locale) {
        throw m21223();
    }

    public String getAsShortText(int i, Locale locale) {
        throw m21223();
    }

    public long add(long j, int i) {
        return getDurationField().add(j, i);
    }

    public long add(long j, long j2) {
        return getDurationField().add(j, j2);
    }

    public int[] add(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m21223();
    }

    public int[] addWrapPartial(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m21223();
    }

    public long addWrapField(long j, int i) {
        throw m21223();
    }

    public int[] addWrapField(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m21223();
    }

    public int getDifference(long j, long j2) {
        return getDurationField().getDifference(j, j2);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return getDurationField().getDifferenceAsLong(j, j2);
    }

    public long set(long j, int i) {
        throw m21223();
    }

    public int[] set(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m21223();
    }

    public long set(long j, String str, Locale locale) {
        throw m21223();
    }

    public long set(long j, String str) {
        throw m21223();
    }

    public int[] set(ReadablePartial readablePartial, int i, int[] iArr, String str, Locale locale) {
        throw m21223();
    }

    public DurationField getDurationField() {
        return this.iDurationField;
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public boolean isLeap(long j) {
        throw m21223();
    }

    public int getLeapAmount(long j) {
        throw m21223();
    }

    public DurationField getLeapDurationField() {
        return null;
    }

    public int getMinimumValue() {
        throw m21223();
    }

    public int getMinimumValue(long j) {
        throw m21223();
    }

    public int getMinimumValue(ReadablePartial readablePartial) {
        throw m21223();
    }

    public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
        throw m21223();
    }

    public int getMaximumValue() {
        throw m21223();
    }

    public int getMaximumValue(long j) {
        throw m21223();
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        throw m21223();
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        throw m21223();
    }

    public int getMaximumTextLength(Locale locale) {
        throw m21223();
    }

    public int getMaximumShortTextLength(Locale locale) {
        throw m21223();
    }

    public long roundFloor(long j) {
        throw m21223();
    }

    public long roundCeiling(long j) {
        throw m21223();
    }

    public long roundHalfFloor(long j) {
        throw m21223();
    }

    public long roundHalfCeiling(long j) {
        throw m21223();
    }

    public long roundHalfEven(long j) {
        throw m21223();
    }

    public long remainder(long j) {
        throw m21223();
    }

    public String toString() {
        return "UnsupportedDateTimeField";
    }

    private Object readResolve() {
        return getInstance(this.iType, this.iDurationField);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private UnsupportedOperationException m21223() {
        return new UnsupportedOperationException(this.iType + " field is unsupported");
    }
}
