package org.joda.time.field;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;

public final class ZeroIsMaxDateTimeField extends DecoratedDateTimeField {
    public ZeroIsMaxDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType) {
        super(dateTimeField, dateTimeFieldType);
        if (dateTimeField.getMinimumValue() != 0) {
            throw new IllegalArgumentException("Wrapped field's minumum value must be zero");
        }
    }

    public int get(long j) {
        int i = m21203().get(j);
        if (i == 0) {
            return getMaximumValue();
        }
        return i;
    }

    public long add(long j, int i) {
        return m21203().add(j, i);
    }

    public long add(long j, long j2) {
        return m21203().add(j, j2);
    }

    public long addWrapField(long j, int i) {
        return m21203().addWrapField(j, i);
    }

    public int[] addWrapField(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        return m21203().addWrapField(readablePartial, i, iArr, i2);
    }

    public int getDifference(long j, long j2) {
        return m21203().getDifference(j, j2);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return m21203().getDifferenceAsLong(j, j2);
    }

    public long set(long j, int i) {
        int maximumValue = getMaximumValue();
        FieldUtils.m21217((DateTimeField) this, i, 1, maximumValue);
        if (i == maximumValue) {
            i = 0;
        }
        return m21203().set(j, i);
    }

    public boolean isLeap(long j) {
        return m21203().isLeap(j);
    }

    public int getLeapAmount(long j) {
        return m21203().getLeapAmount(j);
    }

    public DurationField getLeapDurationField() {
        return m21203().getLeapDurationField();
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMinimumValue(long j) {
        return 1;
    }

    public int getMinimumValue(ReadablePartial readablePartial) {
        return 1;
    }

    public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
        return 1;
    }

    public int getMaximumValue() {
        return m21203().getMaximumValue() + 1;
    }

    public int getMaximumValue(long j) {
        return m21203().getMaximumValue(j) + 1;
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        return m21203().getMaximumValue(readablePartial) + 1;
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        return m21203().getMaximumValue(readablePartial, iArr) + 1;
    }

    public long roundFloor(long j) {
        return m21203().roundFloor(j);
    }

    public long roundCeiling(long j) {
        return m21203().roundCeiling(j);
    }

    public long roundHalfFloor(long j) {
        return m21203().roundHalfFloor(j);
    }

    public long roundHalfCeiling(long j) {
        return m21203().roundHalfCeiling(j);
    }

    public long roundHalfEven(long j) {
        return m21203().roundHalfEven(j);
    }

    public long remainder(long j) {
        return m21203().remainder(j);
    }
}
