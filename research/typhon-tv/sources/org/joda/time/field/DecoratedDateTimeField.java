package org.joda.time.field;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;

public abstract class DecoratedDateTimeField extends BaseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeField f17024;

    protected DecoratedDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType) {
        super(dateTimeFieldType);
        if (dateTimeField == null) {
            throw new IllegalArgumentException("The field must not be null");
        } else if (!dateTimeField.isSupported()) {
            throw new IllegalArgumentException("The field must be supported");
        } else {
            this.f17024 = dateTimeField;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final DateTimeField m21203() {
        return this.f17024;
    }

    public boolean isLenient() {
        return this.f17024.isLenient();
    }

    public int get(long j) {
        return this.f17024.get(j);
    }

    public long set(long j, int i) {
        return this.f17024.set(j, i);
    }

    public DurationField getDurationField() {
        return this.f17024.getDurationField();
    }

    public DurationField getRangeDurationField() {
        return this.f17024.getRangeDurationField();
    }

    public int getMinimumValue() {
        return this.f17024.getMinimumValue();
    }

    public int getMaximumValue() {
        return this.f17024.getMaximumValue();
    }

    public long roundFloor(long j) {
        return this.f17024.roundFloor(j);
    }
}
