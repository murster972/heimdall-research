package org.joda.time.field;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;

public class DividedDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f17025;

    /* renamed from: 靐  reason: contains not printable characters */
    final DurationField f17026;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f17027;

    /* renamed from: 齉  reason: contains not printable characters */
    final DurationField f17028;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f17029;

    public DividedDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType, int i) {
        this(dateTimeField, dateTimeField.getRangeDurationField(), dateTimeFieldType, i);
    }

    public DividedDateTimeField(DateTimeField dateTimeField, DurationField durationField, DateTimeFieldType dateTimeFieldType, int i) {
        super(dateTimeField, dateTimeFieldType);
        if (i < 2) {
            throw new IllegalArgumentException("The divisor must be at least 2");
        }
        DurationField durationField2 = dateTimeField.getDurationField();
        if (durationField2 == null) {
            this.f17026 = null;
        } else {
            this.f17026 = new ScaledDurationField(durationField2, dateTimeFieldType.getDurationType(), i);
        }
        this.f17028 = durationField;
        this.f17029 = i;
        int minimumValue = dateTimeField.getMinimumValue();
        int i2 = minimumValue >= 0 ? minimumValue / i : ((minimumValue + 1) / i) - 1;
        int maximumValue = dateTimeField.getMaximumValue();
        int i3 = maximumValue >= 0 ? maximumValue / i : ((maximumValue + 1) / i) - 1;
        this.f17027 = i2;
        this.f17025 = i3;
    }

    public DurationField getRangeDurationField() {
        if (this.f17028 != null) {
            return this.f17028;
        }
        return super.getRangeDurationField();
    }

    public int get(long j) {
        int i = m21203().get(j);
        if (i >= 0) {
            return i / this.f17029;
        }
        return ((i + 1) / this.f17029) - 1;
    }

    public long add(long j, int i) {
        return m21203().add(j, this.f17029 * i);
    }

    public long add(long j, long j2) {
        return m21203().add(j, ((long) this.f17029) * j2);
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m21212(get(j), i, this.f17027, this.f17025));
    }

    public int getDifference(long j, long j2) {
        return m21203().getDifference(j, j2) / this.f17029;
    }

    public long getDifferenceAsLong(long j, long j2) {
        return m21203().getDifferenceAsLong(j, j2) / ((long) this.f17029);
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, this.f17027, this.f17025);
        return m21203().set(j, m21204(m21203().get(j)) + (this.f17029 * i));
    }

    public DurationField getDurationField() {
        return this.f17026;
    }

    public int getMinimumValue() {
        return this.f17027;
    }

    public int getMaximumValue() {
        return this.f17025;
    }

    public long roundFloor(long j) {
        DateTimeField r0 = m21203();
        return r0.roundFloor(r0.set(j, get(j) * this.f17029));
    }

    public long remainder(long j) {
        return set(j, get(m21203().remainder(j)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m21204(int i) {
        if (i >= 0) {
            return i % this.f17029;
        }
        return (this.f17029 - 1) + ((i + 1) % this.f17029);
    }
}
