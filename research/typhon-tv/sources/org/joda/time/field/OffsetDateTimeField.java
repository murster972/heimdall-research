package org.joda.time.field;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;

public class OffsetDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f17032;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f17033;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f17034;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public OffsetDateTimeField(DateTimeField dateTimeField, int i) {
        this(dateTimeField, dateTimeField == null ? null : dateTimeField.getType(), i, Integer.MIN_VALUE, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    public OffsetDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType, int i) {
        this(dateTimeField, dateTimeFieldType, i, Integer.MIN_VALUE, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    public OffsetDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType, int i, int i2, int i3) {
        super(dateTimeField, dateTimeFieldType);
        if (i == 0) {
            throw new IllegalArgumentException("The offset cannot be zero");
        }
        this.f17034 = i;
        if (i2 < dateTimeField.getMinimumValue() + i) {
            this.f17032 = dateTimeField.getMinimumValue() + i;
        } else {
            this.f17032 = i2;
        }
        if (i3 > dateTimeField.getMaximumValue() + i) {
            this.f17033 = dateTimeField.getMaximumValue() + i;
        } else {
            this.f17033 = i3;
        }
    }

    public int get(long j) {
        return super.get(j) + this.f17034;
    }

    public long add(long j, int i) {
        long add = super.add(j, i);
        FieldUtils.m21217((DateTimeField) this, get(add), this.f17032, this.f17033);
        return add;
    }

    public long add(long j, long j2) {
        long add = super.add(j, j2);
        FieldUtils.m21217((DateTimeField) this, get(add), this.f17032, this.f17033);
        return add;
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m21212(get(j), i, this.f17032, this.f17033));
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, this.f17032, this.f17033);
        return super.set(j, i - this.f17034);
    }

    public boolean isLeap(long j) {
        return m21203().isLeap(j);
    }

    public int getLeapAmount(long j) {
        return m21203().getLeapAmount(j);
    }

    public DurationField getLeapDurationField() {
        return m21203().getLeapDurationField();
    }

    public int getMinimumValue() {
        return this.f17032;
    }

    public int getMaximumValue() {
        return this.f17033;
    }

    public long roundFloor(long j) {
        return m21203().roundFloor(j);
    }

    public long roundCeiling(long j) {
        return m21203().roundCeiling(j);
    }

    public long roundHalfFloor(long j) {
        return m21203().roundHalfFloor(j);
    }

    public long roundHalfCeiling(long j) {
        return m21203().roundHalfCeiling(j);
    }

    public long roundHalfEven(long j) {
        return m21203().roundHalfEven(j);
    }

    public long remainder(long j) {
        return m21203().remainder(j);
    }
}
