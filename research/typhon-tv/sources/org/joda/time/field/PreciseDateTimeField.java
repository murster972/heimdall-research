package org.joda.time.field;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;

public class PreciseDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f17035;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DurationField f17036;

    public PreciseDateTimeField(DateTimeFieldType dateTimeFieldType, DurationField durationField, DurationField durationField2) {
        super(dateTimeFieldType, durationField);
        if (!durationField2.isPrecise()) {
            throw new IllegalArgumentException("Range duration field must be precise");
        }
        this.f17035 = (int) (durationField2.getUnitMillis() / m21221());
        if (this.f17035 < 2) {
            throw new IllegalArgumentException("The effective range must be at least 2");
        }
        this.f17036 = durationField2;
    }

    public int get(long j) {
        if (j >= 0) {
            return (int) ((j / m21221()) % ((long) this.f17035));
        }
        return (this.f17035 - 1) + ((int) (((1 + j) / m21221()) % ((long) this.f17035)));
    }

    public long addWrapField(long j, int i) {
        int i2 = get(j);
        return (((long) (FieldUtils.m21212(i2, i, getMinimumValue(), getMaximumValue()) - i2)) * m21221()) + j;
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, getMinimumValue(), getMaximumValue());
        return (((long) (i - get(j))) * this.f17038) + j;
    }

    public DurationField getRangeDurationField() {
        return this.f17036;
    }

    public int getMaximumValue() {
        return this.f17035 - 1;
    }
}
