package org.joda.time.field;

import java.io.Serializable;
import java.util.HashMap;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;

public final class UnsupportedDurationField extends DurationField implements Serializable {
    private static final long serialVersionUID = -6390301302770925357L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static HashMap<DurationFieldType, UnsupportedDurationField> f17045;
    private final DurationFieldType iType;

    public static synchronized UnsupportedDurationField getInstance(DurationFieldType durationFieldType) {
        UnsupportedDurationField unsupportedDurationField;
        synchronized (UnsupportedDurationField.class) {
            if (f17045 == null) {
                f17045 = new HashMap<>(7);
                unsupportedDurationField = null;
            } else {
                unsupportedDurationField = f17045.get(durationFieldType);
            }
            if (unsupportedDurationField == null) {
                unsupportedDurationField = new UnsupportedDurationField(durationFieldType);
                f17045.put(durationFieldType, unsupportedDurationField);
            }
        }
        return unsupportedDurationField;
    }

    private UnsupportedDurationField(DurationFieldType durationFieldType) {
        this.iType = durationFieldType;
    }

    public final DurationFieldType getType() {
        return this.iType;
    }

    public String getName() {
        return this.iType.getName();
    }

    public boolean isSupported() {
        return false;
    }

    public boolean isPrecise() {
        return true;
    }

    public int getValue(long j) {
        throw m21224();
    }

    public long getValueAsLong(long j) {
        throw m21224();
    }

    public int getValue(long j, long j2) {
        throw m21224();
    }

    public long getValueAsLong(long j, long j2) {
        throw m21224();
    }

    public long getMillis(int i) {
        throw m21224();
    }

    public long getMillis(long j) {
        throw m21224();
    }

    public long getMillis(int i, long j) {
        throw m21224();
    }

    public long getMillis(long j, long j2) {
        throw m21224();
    }

    public long add(long j, int i) {
        throw m21224();
    }

    public long add(long j, long j2) {
        throw m21224();
    }

    public int getDifference(long j, long j2) {
        throw m21224();
    }

    public long getDifferenceAsLong(long j, long j2) {
        throw m21224();
    }

    public long getUnitMillis() {
        return 0;
    }

    public int compareTo(DurationField durationField) {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UnsupportedDurationField)) {
            return false;
        }
        UnsupportedDurationField unsupportedDurationField = (UnsupportedDurationField) obj;
        if (unsupportedDurationField.getName() != null) {
            return unsupportedDurationField.getName().equals(getName());
        }
        if (getName() != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return getName().hashCode();
    }

    public String toString() {
        return "UnsupportedDurationField[" + getName() + ']';
    }

    private Object readResolve() {
        return getInstance(this.iType);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private UnsupportedOperationException m21224() {
        return new UnsupportedOperationException(this.iType + " field is unsupported");
    }
}
