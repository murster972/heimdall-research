package org.joda.time.field;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.IllegalFieldValueException;

public class FieldUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m21209(int i) {
        if (i != Integer.MIN_VALUE) {
            return -i;
        }
        throw new ArithmeticException("Integer.MIN_VALUE cannot be negated");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m21210(int i, int i2) {
        int i3 = i + i2;
        if ((i ^ i3) >= 0 || (i ^ i2) < 0) {
            return i3;
        }
        throw new ArithmeticException("The calculation caused an overflow: " + i + " + " + i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m21215(long j, long j2) {
        long j3 = j + j2;
        if ((j ^ j3) >= 0 || (j ^ j2) < 0) {
            return j3;
        }
        throw new ArithmeticException("The calculation caused an overflow: " + j + " + " + j2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static long m21206(long j, long j2) {
        long j3 = j - j2;
        if ((j ^ j3) >= 0 || (j ^ j2) >= 0) {
            return j3;
        }
        throw new ArithmeticException("The calculation caused an overflow: " + j + " - " + j2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m21205(int i, int i2) {
        long j = ((long) i) * ((long) i2);
        if (j >= -2147483648L && j <= 2147483647L) {
            return (int) j;
        }
        throw new ArithmeticException("Multiplication overflows an int: " + i + " * " + i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m21214(long j, int i) {
        switch (i) {
            case -1:
                if (j != Long.MIN_VALUE) {
                    return -j;
                }
                throw new ArithmeticException("Multiplication overflows a long: " + j + " * " + i);
            case 0:
                return 0;
            case 1:
                return j;
            default:
                long j2 = ((long) i) * j;
                if (j2 / ((long) i) == j) {
                    return j2;
                }
                throw new ArithmeticException("Multiplication overflows a long: " + j + " * " + i);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static long m21208(long j, long j2) {
        if (j2 == 1) {
            return j;
        }
        if (j == 1) {
            return j2;
        }
        if (j == 0 || j2 == 0) {
            return 0;
        }
        long j3 = j * j2;
        if (j3 / j2 == j && ((j != Long.MIN_VALUE || j2 != -1) && (j2 != Long.MIN_VALUE || j != -1))) {
            return j3;
        }
        throw new ArithmeticException("Multiplication overflows a long: " + j + " * " + j2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static long m21207(long j, long j2) {
        if (j != Long.MIN_VALUE || j2 != -1) {
            return j / j2;
        }
        throw new ArithmeticException("Multiplication overflows a long: " + j + " / " + j2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m21216(long j, long j2, RoundingMode roundingMode) {
        if (j != Long.MIN_VALUE || j2 != -1) {
            return new BigDecimal(j).divide(new BigDecimal(j2), roundingMode).longValue();
        }
        throw new ArithmeticException("Multiplication overflows a long: " + j + " / " + j2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m21213(long j) {
        if (-2147483648L <= j && j <= 2147483647L) {
            return (int) j;
        }
        throw new ArithmeticException("Value cannot fit in an int: " + j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21217(DateTimeField dateTimeField, int i, int i2, int i3) {
        if (i < i2 || i > i3) {
            throw new IllegalFieldValueException(dateTimeField.getType(), (Number) Integer.valueOf(i), (Number) Integer.valueOf(i2), (Number) Integer.valueOf(i3));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21218(DateTimeFieldType dateTimeFieldType, int i, int i2, int i3) {
        if (i < i2 || i > i3) {
            throw new IllegalFieldValueException(dateTimeFieldType, (Number) Integer.valueOf(i), (Number) Integer.valueOf(i2), (Number) Integer.valueOf(i3));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m21212(int i, int i2, int i3, int i4) {
        return m21211(i + i2, i3, i4);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m21211(int i, int i2, int i3) {
        if (i2 >= i3) {
            throw new IllegalArgumentException("MIN > MAX");
        }
        int i4 = (i3 - i2) + 1;
        int i5 = i - i2;
        if (i5 >= 0) {
            return (i5 % i4) + i2;
        }
        int i6 = (-i5) % i4;
        if (i6 == 0) {
            return i2 + 0;
        }
        return (i4 - i6) + i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m21219(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }
}
