package org.joda.time;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Hours extends BaseSingleFieldPeriod {
    public static final Hours EIGHT = new Hours(8);
    public static final Hours FIVE = new Hours(5);
    public static final Hours FOUR = new Hours(4);
    public static final Hours MAX_VALUE = new Hours(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    public static final Hours MIN_VALUE = new Hours(Integer.MIN_VALUE);
    public static final Hours ONE = new Hours(1);
    public static final Hours SEVEN = new Hours(7);
    public static final Hours SIX = new Hours(6);
    public static final Hours THREE = new Hours(3);
    public static final Hours TWO = new Hours(2);
    public static final Hours ZERO = new Hours(0);
    private static final long serialVersionUID = 87525275727380864L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final PeriodFormatter f6492 = ISOPeriodFormat.m21532().m21557(PeriodType.hours());

    private Hours(int i) {
        super(i);
    }

    public static Hours hours(int i) {
        switch (i) {
            case Integer.MIN_VALUE:
                return MIN_VALUE;
            case 0:
                return ZERO;
            case 1:
                return ONE;
            case 2:
                return TWO;
            case 3:
                return THREE;
            case 4:
                return FOUR;
            case 5:
                return FIVE;
            case 6:
                return SIX;
            case 7:
                return SEVEN;
            case 8:
                return EIGHT;
            case MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT /*2147483647*/:
                return MAX_VALUE;
            default:
                return new Hours(i);
        }
    }

    public static Hours hoursBetween(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        return hours(BaseSingleFieldPeriod.m20947(readableInstant, readableInstant2, DurationFieldType.hours()));
    }

    public static Hours hoursBetween(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        return (!(readablePartial instanceof LocalTime) || !(readablePartial2 instanceof LocalTime)) ? hours(BaseSingleFieldPeriod.m20948(readablePartial, readablePartial2, (ReadablePeriod) ZERO)) : hours(DateTimeUtils.m20895(readablePartial.getChronology()).hours().getDifference(((LocalTime) readablePartial2).m7311(), ((LocalTime) readablePartial).m7311()));
    }

    public static Hours hoursIn(ReadableInterval readableInterval) {
        return readableInterval == null ? ZERO : hours(BaseSingleFieldPeriod.m20947((ReadableInstant) readableInterval.getStart(), (ReadableInstant) readableInterval.getEnd(), DurationFieldType.hours()));
    }

    @FromString
    public static Hours parseHours(String str) {
        return str == null ? ZERO : hours(f6492.m21556(str).getHours());
    }

    private Object readResolve() {
        return hours(m20950());
    }

    public static Hours standardHoursIn(ReadablePeriod readablePeriod) {
        return hours(BaseSingleFieldPeriod.m20949(readablePeriod, 3600000));
    }

    public Hours dividedBy(int i) {
        return i == 1 ? this : hours(m20950() / i);
    }

    public DurationFieldType getFieldType() {
        return DurationFieldType.hours();
    }

    public int getHours() {
        return m20950();
    }

    public PeriodType getPeriodType() {
        return PeriodType.hours();
    }

    public boolean isGreaterThan(Hours hours) {
        return hours == null ? m20950() > 0 : m20950() > hours.m20950();
    }

    public boolean isLessThan(Hours hours) {
        return hours == null ? m20950() < 0 : m20950() < hours.m20950();
    }

    public Hours minus(int i) {
        return plus(FieldUtils.m21209(i));
    }

    public Hours minus(Hours hours) {
        return hours == null ? this : minus(hours.m20950());
    }

    public Hours multipliedBy(int i) {
        return hours(FieldUtils.m21205(m20950(), i));
    }

    public Hours negated() {
        return hours(FieldUtils.m21209(m20950()));
    }

    public Hours plus(int i) {
        return i == 0 ? this : hours(FieldUtils.m21210(m20950(), i));
    }

    public Hours plus(Hours hours) {
        return hours == null ? this : plus(hours.m20950());
    }

    public Days toStandardDays() {
        return Days.days(m20950() / 24);
    }

    public Duration toStandardDuration() {
        return new Duration(((long) m20950()) * 3600000);
    }

    public Minutes toStandardMinutes() {
        return Minutes.minutes(FieldUtils.m21205(m20950(), 60));
    }

    public Seconds toStandardSeconds() {
        return Seconds.seconds(FieldUtils.m21205(m20950(), 3600));
    }

    public Weeks toStandardWeeks() {
        return Weeks.weeks(m20950() / 168);
    }

    @ToString
    public String toString() {
        return "PT" + String.valueOf(m20950()) + "H";
    }
}
