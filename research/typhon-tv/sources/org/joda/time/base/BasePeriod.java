package org.joda.time.base;

import java.io.Serializable;
import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.Duration;
import org.joda.time.DurationFieldType;
import org.joda.time.MutablePeriod;
import org.joda.time.PeriodType;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadableDuration;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;
import org.joda.time.ReadablePeriod;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.PeriodConverter;
import org.joda.time.field.FieldUtils;

public abstract class BasePeriod extends AbstractPeriod implements Serializable, ReadablePeriod {
    private static final long serialVersionUID = -2110953284060001145L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ReadablePeriod f16832 = new AbstractPeriod() {
        public int getValue(int i) {
            return 0;
        }

        public PeriodType getPeriodType() {
            return PeriodType.time();
        }
    };
    private final PeriodType iType;
    private final int[] iValues;

    protected BasePeriod(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, PeriodType periodType) {
        this.iType = m20941(periodType);
        this.iValues = m20937(i, i2, i3, i4, i5, i6, i7, i8);
    }

    protected BasePeriod(long j, long j2, PeriodType periodType, Chronology chronology) {
        PeriodType r1 = m20941(periodType);
        Chronology r0 = DateTimeUtils.m20895(chronology);
        this.iType = r1;
        this.iValues = r0.get(this, j, j2);
    }

    protected BasePeriod(ReadableInstant readableInstant, ReadableInstant readableInstant2, PeriodType periodType) {
        PeriodType r1 = m20941(periodType);
        if (readableInstant == null && readableInstant2 == null) {
            this.iType = r1;
            this.iValues = new int[size()];
            return;
        }
        long r2 = DateTimeUtils.m20893(readableInstant);
        long r4 = DateTimeUtils.m20893(readableInstant2);
        Chronology r0 = DateTimeUtils.m20896(readableInstant, readableInstant2);
        this.iType = r1;
        this.iValues = r0.get(this, r2, r4);
    }

    protected BasePeriod(ReadablePartial readablePartial, ReadablePartial readablePartial2, PeriodType periodType) {
        if (readablePartial == null || readablePartial2 == null) {
            throw new IllegalArgumentException("ReadablePartial objects must not be null");
        } else if ((readablePartial instanceof BaseLocal) && (readablePartial2 instanceof BaseLocal) && readablePartial.getClass() == readablePartial2.getClass()) {
            PeriodType r1 = m20941(periodType);
            long r2 = ((BaseLocal) readablePartial).m20934();
            long r4 = ((BaseLocal) readablePartial2).m20934();
            Chronology r0 = DateTimeUtils.m20895(readablePartial.getChronology());
            this.iType = r1;
            this.iValues = r0.get(this, r2, r4);
        } else if (readablePartial.size() != readablePartial2.size()) {
            throw new IllegalArgumentException("ReadablePartial objects must have the same set of fields");
        } else {
            int size = readablePartial.size();
            for (int i = 0; i < size; i++) {
                if (readablePartial.getFieldType(i) != readablePartial2.getFieldType(i)) {
                    throw new IllegalArgumentException("ReadablePartial objects must have the same set of fields");
                }
            }
            if (!DateTimeUtils.m20901(readablePartial)) {
                throw new IllegalArgumentException("ReadablePartial objects must be contiguous");
            }
            this.iType = m20941(periodType);
            Chronology withUTC = DateTimeUtils.m20895(readablePartial.getChronology()).withUTC();
            this.iValues = withUTC.get(this, withUTC.set(readablePartial, 0), withUTC.set(readablePartial2, 0));
        }
    }

    protected BasePeriod(ReadableInstant readableInstant, ReadableDuration readableDuration, PeriodType periodType) {
        PeriodType r1 = m20941(periodType);
        long r2 = DateTimeUtils.m20893(readableInstant);
        long r4 = FieldUtils.m21215(r2, DateTimeUtils.m20892(readableDuration));
        Chronology r0 = DateTimeUtils.m20888(readableInstant);
        this.iType = r1;
        this.iValues = r0.get(this, r2, r4);
    }

    protected BasePeriod(ReadableDuration readableDuration, ReadableInstant readableInstant, PeriodType periodType) {
        PeriodType r1 = m20941(periodType);
        long r2 = DateTimeUtils.m20892(readableDuration);
        long r4 = DateTimeUtils.m20893(readableInstant);
        long r22 = FieldUtils.m21206(r4, r2);
        Chronology r0 = DateTimeUtils.m20888(readableInstant);
        this.iType = r1;
        this.iValues = r0.get(this, r22, r4);
    }

    protected BasePeriod(long j) {
        this.iType = PeriodType.standard();
        int[] iArr = ISOChronology.getInstanceUTC().get(f16832, j);
        this.iValues = new int[8];
        System.arraycopy(iArr, 0, this.iValues, 4, 4);
    }

    protected BasePeriod(long j, PeriodType periodType, Chronology chronology) {
        PeriodType r0 = m20941(periodType);
        Chronology r1 = DateTimeUtils.m20895(chronology);
        this.iType = r0;
        this.iValues = r1.get((ReadablePeriod) this, j);
    }

    protected BasePeriod(Object obj, PeriodType periodType, Chronology chronology) {
        PeriodConverter r0 = ConverterManager.m21146().m21149(obj);
        PeriodType r1 = m20941(periodType == null ? r0.a_(obj) : periodType);
        this.iType = r1;
        if (this instanceof ReadWritablePeriod) {
            this.iValues = new int[size()];
            r0.m21174((ReadWritablePeriod) this, obj, DateTimeUtils.m20895(chronology));
            return;
        }
        this.iValues = new MutablePeriod(obj, r1, chronology).getValues();
    }

    protected BasePeriod(int[] iArr, PeriodType periodType) {
        this.iType = periodType;
        this.iValues = iArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodType m20941(PeriodType periodType) {
        return DateTimeUtils.m20899(periodType);
    }

    public PeriodType getPeriodType() {
        return this.iType;
    }

    public int getValue(int i) {
        return this.iValues[i];
    }

    public Duration toDurationFrom(ReadableInstant readableInstant) {
        long r0 = DateTimeUtils.m20893(readableInstant);
        return new Duration(r0, DateTimeUtils.m20888(readableInstant).add((ReadablePeriod) this, r0, 1));
    }

    public Duration toDurationTo(ReadableInstant readableInstant) {
        long r0 = DateTimeUtils.m20893(readableInstant);
        return new Duration(DateTimeUtils.m20888(readableInstant).add((ReadablePeriod) this, r0, -1), r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20936(DurationFieldType durationFieldType, int[] iArr, int i) {
        int indexOf = indexOf(durationFieldType);
        if (indexOf != -1) {
            iArr[indexOf] = i;
        } else if (i != 0) {
            throw new IllegalArgumentException("Period does not support field '" + durationFieldType.getName() + "'");
        }
    }

    /* access modifiers changed from: protected */
    public void setPeriod(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            m20944(new int[size()]);
        } else {
            m20935(readablePeriod);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20935(ReadablePeriod readablePeriod) {
        int[] iArr = new int[size()];
        int size = readablePeriod.size();
        for (int i = 0; i < size; i++) {
            m20936(readablePeriod.getFieldType(i), iArr, readablePeriod.getValue(i));
        }
        m20944(iArr);
    }

    /* access modifiers changed from: protected */
    public void setPeriod(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        m20944(m20937(i, i2, i3, i4, i5, i6, i7, i8));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int[] m20937(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int[] iArr = new int[size()];
        m20936(DurationFieldType.years(), iArr, i);
        m20936(DurationFieldType.months(), iArr, i2);
        m20936(DurationFieldType.weeks(), iArr, i3);
        m20936(DurationFieldType.days(), iArr, i4);
        m20936(DurationFieldType.hours(), iArr, i5);
        m20936(DurationFieldType.minutes(), iArr, i6);
        m20936(DurationFieldType.seconds(), iArr, i7);
        m20936(DurationFieldType.millis(), iArr, i8);
        return iArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20942(DurationFieldType durationFieldType, int i) {
        m20945(this.iValues, durationFieldType, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20945(int[] iArr, DurationFieldType durationFieldType, int i) {
        int indexOf = indexOf(durationFieldType);
        if (indexOf != -1) {
            iArr[indexOf] = i;
        } else if (i != 0 || durationFieldType == null) {
            throw new IllegalArgumentException("Period does not support field '" + durationFieldType + "'");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m20938(DurationFieldType durationFieldType, int i) {
        m20939(this.iValues, durationFieldType, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m20939(int[] iArr, DurationFieldType durationFieldType, int i) {
        int indexOf = indexOf(durationFieldType);
        if (indexOf != -1) {
            iArr[indexOf] = FieldUtils.m21210(iArr[indexOf], i);
        } else if (i != 0 || durationFieldType == null) {
            throw new IllegalArgumentException("Period does not support field '" + durationFieldType + "'");
        }
    }

    /* access modifiers changed from: protected */
    public void mergePeriod(ReadablePeriod readablePeriod) {
        if (readablePeriod != null) {
            m20944(m20946(getValues(), readablePeriod));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m20946(int[] iArr, ReadablePeriod readablePeriod) {
        int size = readablePeriod.size();
        for (int i = 0; i < size; i++) {
            m20936(readablePeriod.getFieldType(i), iArr, readablePeriod.getValue(i));
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20943(ReadablePeriod readablePeriod) {
        if (readablePeriod != null) {
            m20944(m20940(getValues(), readablePeriod));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public int[] m20940(int[] iArr, ReadablePeriod readablePeriod) {
        int size = readablePeriod.size();
        for (int i = 0; i < size; i++) {
            DurationFieldType fieldType = readablePeriod.getFieldType(i);
            int value = readablePeriod.getValue(i);
            if (value != 0) {
                int indexOf = indexOf(fieldType);
                if (indexOf == -1) {
                    throw new IllegalArgumentException("Period does not support field '" + fieldType.getName() + "'");
                }
                iArr[indexOf] = FieldUtils.m21210(getValue(indexOf), value);
            }
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void setValue(int i, int i2) {
        this.iValues[i] = i2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20944(int[] iArr) {
        System.arraycopy(iArr, 0, this.iValues, 0, this.iValues.length);
    }
}
