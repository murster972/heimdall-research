package org.joda.time.base;

import java.io.Serializable;
import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableDateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.InstantConverter;

public abstract class BaseDateTime extends AbstractDateTime implements Serializable, ReadableDateTime {
    private static final long serialVersionUID = -6728882245981L;
    private volatile Chronology iChronology;
    private volatile long iMillis;

    public BaseDateTime() {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance());
    }

    public BaseDateTime(DateTimeZone dateTimeZone) {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public BaseDateTime(Chronology chronology) {
        this(DateTimeUtils.m20891(), chronology);
    }

    public BaseDateTime(long j) {
        this(j, (Chronology) ISOChronology.getInstance());
    }

    public BaseDateTime(long j, DateTimeZone dateTimeZone) {
        this(j, (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public BaseDateTime(long j, Chronology chronology) {
        this.iChronology = m20932(chronology);
        this.iMillis = m20931(j, this.iChronology);
        m20930();
    }

    public BaseDateTime(Object obj, DateTimeZone dateTimeZone) {
        InstantConverter r0 = ConverterManager.m21146().m21151(obj);
        Chronology r1 = m20932(r0.m21161(obj, dateTimeZone));
        this.iChronology = r1;
        this.iMillis = m20931(r0.m21160(obj, r1), r1);
        m20930();
    }

    public BaseDateTime(Object obj, Chronology chronology) {
        InstantConverter r0 = ConverterManager.m21146().m21151(obj);
        this.iChronology = m20932(r0.m21159(obj, chronology));
        this.iMillis = m20931(r0.m21160(obj, chronology), this.iChronology);
        m20930();
    }

    public BaseDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this(i, i2, i3, i4, i5, i6, i7, (Chronology) ISOChronology.getInstance());
    }

    public BaseDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7, DateTimeZone dateTimeZone) {
        this(i, i2, i3, i4, i5, i6, i7, (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public BaseDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7, Chronology chronology) {
        this.iChronology = m20932(chronology);
        this.iMillis = m20931(this.iChronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7), this.iChronology);
        m20930();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20930() {
        if (this.iMillis == Long.MIN_VALUE || this.iMillis == Long.MAX_VALUE) {
            this.iChronology = this.iChronology.withUTC();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m20932(Chronology chronology) {
        return DateTimeUtils.m20895(chronology);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m20931(long j, Chronology chronology) {
        return j;
    }

    public long getMillis() {
        return this.iMillis;
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    /* access modifiers changed from: protected */
    public void setMillis(long j) {
        this.iMillis = m20931(j, this.iChronology);
    }

    /* access modifiers changed from: protected */
    public void setChronology(Chronology chronology) {
        this.iChronology = m20932(chronology);
    }
}
