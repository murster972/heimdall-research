package org.joda.time.base;

import java.io.Serializable;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.ReadablePartial;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.PartialConverter;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public abstract class BasePartial extends AbstractPartial implements Serializable, ReadablePartial {
    private static final long serialVersionUID = 2353678632973660L;
    private final Chronology iChronology;
    private final int[] iValues;

    protected BasePartial() {
        this(DateTimeUtils.m20891(), (Chronology) null);
    }

    protected BasePartial(Chronology chronology) {
        this(DateTimeUtils.m20891(), chronology);
    }

    protected BasePartial(long j) {
        this(j, (Chronology) null);
    }

    protected BasePartial(long j, Chronology chronology) {
        Chronology r0 = DateTimeUtils.m20895(chronology);
        this.iChronology = r0.withUTC();
        this.iValues = r0.get((ReadablePartial) this, j);
    }

    protected BasePartial(Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter) {
        PartialConverter r0 = ConverterManager.m21146().m21148(obj);
        Chronology r1 = DateTimeUtils.m20895(r0.m21171(obj, chronology));
        this.iChronology = r1.withUTC();
        this.iValues = r0.m21173(this, obj, r1, dateTimeFormatter);
    }

    protected BasePartial(int[] iArr, Chronology chronology) {
        Chronology r0 = DateTimeUtils.m20895(chronology);
        this.iChronology = r0.withUTC();
        r0.validate(this, iArr);
        this.iValues = iArr;
    }

    protected BasePartial(BasePartial basePartial, int[] iArr) {
        this.iChronology = basePartial.iChronology;
        this.iValues = iArr;
    }

    protected BasePartial(BasePartial basePartial, Chronology chronology) {
        this.iChronology = chronology.withUTC();
        this.iValues = basePartial.iValues;
    }

    public int getValue(int i) {
        return this.iValues[i];
    }

    public int[] getValues() {
        return (int[]) this.iValues.clone();
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public String toString(String str) {
        if (str == null) {
            return toString();
        }
        return DateTimeFormat.m21228(str).m21246((ReadablePartial) this);
    }

    public String toString(String str, Locale locale) throws IllegalArgumentException {
        if (str == null) {
            return toString();
        }
        return DateTimeFormat.m21228(str).m21247(locale).m21246((ReadablePartial) this);
    }
}
