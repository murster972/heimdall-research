package org.joda.time.base;

import java.io.Serializable;
import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.MutableInterval;
import org.joda.time.ReadWritableInterval;
import org.joda.time.ReadableDuration;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadableInterval;
import org.joda.time.ReadablePeriod;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.IntervalConverter;
import org.joda.time.field.FieldUtils;

public abstract class BaseInterval extends AbstractInterval implements Serializable, ReadableInterval {
    private static final long serialVersionUID = 576586928732749278L;
    private volatile Chronology iChronology;
    private volatile long iEndMillis;
    private volatile long iStartMillis;

    protected BaseInterval(long j, long j2, Chronology chronology) {
        this.iChronology = DateTimeUtils.m20895(chronology);
        m20925(j, j2);
        this.iStartMillis = j;
        this.iEndMillis = j2;
    }

    protected BaseInterval(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        if (readableInstant == null && readableInstant2 == null) {
            long r0 = DateTimeUtils.m20891();
            this.iEndMillis = r0;
            this.iStartMillis = r0;
            this.iChronology = ISOChronology.getInstance();
            return;
        }
        this.iChronology = DateTimeUtils.m20888(readableInstant);
        this.iStartMillis = DateTimeUtils.m20893(readableInstant);
        this.iEndMillis = DateTimeUtils.m20893(readableInstant2);
        m20925(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadableInstant readableInstant, ReadableDuration readableDuration) {
        this.iChronology = DateTimeUtils.m20888(readableInstant);
        this.iStartMillis = DateTimeUtils.m20893(readableInstant);
        this.iEndMillis = FieldUtils.m21215(this.iStartMillis, DateTimeUtils.m20892(readableDuration));
        m20925(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadableDuration readableDuration, ReadableInstant readableInstant) {
        this.iChronology = DateTimeUtils.m20888(readableInstant);
        this.iEndMillis = DateTimeUtils.m20893(readableInstant);
        this.iStartMillis = FieldUtils.m21215(this.iEndMillis, -DateTimeUtils.m20892(readableDuration));
        m20925(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadableInstant readableInstant, ReadablePeriod readablePeriod) {
        Chronology r0 = DateTimeUtils.m20888(readableInstant);
        this.iChronology = r0;
        this.iStartMillis = DateTimeUtils.m20893(readableInstant);
        if (readablePeriod == null) {
            this.iEndMillis = this.iStartMillis;
        } else {
            this.iEndMillis = r0.add(readablePeriod, this.iStartMillis, 1);
        }
        m20925(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadablePeriod readablePeriod, ReadableInstant readableInstant) {
        Chronology r0 = DateTimeUtils.m20888(readableInstant);
        this.iChronology = r0;
        this.iEndMillis = DateTimeUtils.m20893(readableInstant);
        if (readablePeriod == null) {
            this.iStartMillis = this.iEndMillis;
        } else {
            this.iStartMillis = r0.add(readablePeriod, this.iEndMillis, -1);
        }
        m20925(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(Object obj, Chronology chronology) {
        IntervalConverter r1 = ConverterManager.m21146().m21147(obj);
        if (r1.m21162(obj, chronology)) {
            ReadableInterval readableInterval = (ReadableInterval) obj;
            this.iChronology = chronology == null ? readableInterval.getChronology() : chronology;
            this.iStartMillis = readableInterval.getStartMillis();
            this.iEndMillis = readableInterval.getEndMillis();
        } else if (this instanceof ReadWritableInterval) {
            r1.m21163((ReadWritableInterval) this, obj, chronology);
        } else {
            MutableInterval mutableInterval = new MutableInterval();
            r1.m21163(mutableInterval, obj, chronology);
            this.iChronology = mutableInterval.getChronology();
            this.iStartMillis = mutableInterval.getStartMillis();
            this.iEndMillis = mutableInterval.getEndMillis();
        }
        m20925(this.iStartMillis, this.iEndMillis);
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public long getStartMillis() {
        return this.iStartMillis;
    }

    public long getEndMillis() {
        return this.iEndMillis;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20933(long j, long j2, Chronology chronology) {
        m20925(j, j2);
        this.iStartMillis = j;
        this.iEndMillis = j2;
        this.iChronology = DateTimeUtils.m20895(chronology);
    }
}
