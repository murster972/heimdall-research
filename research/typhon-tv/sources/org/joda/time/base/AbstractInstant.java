package org.joda.time.base;

import java.util.Date;
import org.joda.convert.ToString;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;
import org.joda.time.MutableDateTime;
import org.joda.time.ReadableInstant;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public abstract class AbstractInstant implements ReadableInstant {
    protected AbstractInstant() {
    }

    public int compareTo(ReadableInstant readableInstant) {
        if (this == readableInstant) {
            return 0;
        }
        long millis = readableInstant.getMillis();
        long millis2 = getMillis();
        if (millis2 != millis) {
            return millis2 < millis ? -1 : 1;
        }
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadableInstant)) {
            return false;
        }
        ReadableInstant readableInstant = (ReadableInstant) obj;
        return getMillis() == readableInstant.getMillis() && FieldUtils.m21219((Object) getChronology(), (Object) readableInstant.getChronology());
    }

    public int get(DateTimeField dateTimeField) {
        if (dateTimeField != null) {
            return dateTimeField.get(getMillis());
        }
        throw new IllegalArgumentException("The DateTimeField must not be null");
    }

    public int get(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return dateTimeFieldType.getField(getChronology()).get(getMillis());
        }
        throw new IllegalArgumentException("The DateTimeFieldType must not be null");
    }

    public DateTimeZone getZone() {
        return getChronology().getZone();
    }

    public int hashCode() {
        return ((int) (getMillis() ^ (getMillis() >>> 32))) + getChronology().hashCode();
    }

    public boolean isAfter(long j) {
        return getMillis() > j;
    }

    public boolean isAfter(ReadableInstant readableInstant) {
        return isAfter(DateTimeUtils.m20893(readableInstant));
    }

    public boolean isAfterNow() {
        return isAfter(DateTimeUtils.m20891());
    }

    public boolean isBefore(long j) {
        return getMillis() < j;
    }

    public boolean isBefore(ReadableInstant readableInstant) {
        return isBefore(DateTimeUtils.m20893(readableInstant));
    }

    public boolean isBeforeNow() {
        return isBefore(DateTimeUtils.m20891());
    }

    public boolean isEqual(long j) {
        return getMillis() == j;
    }

    public boolean isEqual(ReadableInstant readableInstant) {
        return isEqual(DateTimeUtils.m20893(readableInstant));
    }

    public boolean isEqualNow() {
        return isEqual(DateTimeUtils.m20891());
    }

    public boolean isSupported(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            return false;
        }
        return dateTimeFieldType.getField(getChronology()).isSupported();
    }

    public Date toDate() {
        return new Date(getMillis());
    }

    public DateTime toDateTime() {
        return new DateTime(getMillis(), getZone());
    }

    public DateTime toDateTime(Chronology chronology) {
        return new DateTime(getMillis(), chronology);
    }

    public DateTime toDateTime(DateTimeZone dateTimeZone) {
        return new DateTime(getMillis(), DateTimeUtils.m20895(getChronology()).withZone(dateTimeZone));
    }

    public DateTime toDateTimeISO() {
        return new DateTime(getMillis(), (Chronology) ISOChronology.getInstance(getZone()));
    }

    public Instant toInstant() {
        return new Instant(getMillis());
    }

    public MutableDateTime toMutableDateTime() {
        return new MutableDateTime(getMillis(), getZone());
    }

    public MutableDateTime toMutableDateTime(Chronology chronology) {
        return new MutableDateTime(getMillis(), chronology);
    }

    public MutableDateTime toMutableDateTime(DateTimeZone dateTimeZone) {
        return new MutableDateTime(getMillis(), DateTimeUtils.m20895(getChronology()).withZone(dateTimeZone));
    }

    public MutableDateTime toMutableDateTimeISO() {
        return new MutableDateTime(getMillis(), (Chronology) ISOChronology.getInstance(getZone()));
    }

    @ToString
    public String toString() {
        return ISODateTimeFormat.m21444().m21245((ReadableInstant) this);
    }

    public String toString(DateTimeFormatter dateTimeFormatter) {
        return dateTimeFormatter == null ? toString() : dateTimeFormatter.m21245((ReadableInstant) this);
    }
}
