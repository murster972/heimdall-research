package org.joda.time.format;

import java.io.IOException;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;

public class DateTimeFormatter {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final DateTimeZone f17048;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Integer f17049;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f17050;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Chronology f17051;

    /* renamed from: 靐  reason: contains not printable characters */
    private final InternalParser f17052;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f17053;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Locale f17054;

    /* renamed from: 龘  reason: contains not printable characters */
    private final InternalPrinter f17055;

    DateTimeFormatter(InternalPrinter internalPrinter, InternalParser internalParser) {
        this.f17055 = internalPrinter;
        this.f17052 = internalParser;
        this.f17054 = null;
        this.f17053 = false;
        this.f17051 = null;
        this.f17048 = null;
        this.f17049 = null;
        this.f17050 = 2000;
    }

    private DateTimeFormatter(InternalPrinter internalPrinter, InternalParser internalParser, Locale locale, boolean z, Chronology chronology, DateTimeZone dateTimeZone, Integer num, int i) {
        this.f17055 = internalPrinter;
        this.f17052 = internalParser;
        this.f17054 = locale;
        this.f17053 = z;
        this.f17051 = chronology;
        this.f17048 = dateTimeZone;
        this.f17049 = num;
        this.f17050 = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public InternalPrinter m21250() {
        return this.f17055;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeParser m21239() {
        return InternalParserDateTimeParser.m21535(this.f17052);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public InternalParser m21243() {
        return this.f17052;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m21247(Locale locale) {
        if (locale == m21240() || (locale != null && locale.equals(m21240()))) {
            return this;
        }
        return new DateTimeFormatter(this.f17055, this.f17052, locale, this.f17053, this.f17051, this.f17048, this.f17049, this.f17050);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Locale m21240() {
        return this.f17054;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatter m21237() {
        return this.f17053 ? this : new DateTimeFormatter(this.f17055, this.f17052, this.f17054, true, this.f17051, (DateTimeZone) null, this.f17049, this.f17050);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m21248(Chronology chronology) {
        if (this.f17051 == chronology) {
            return this;
        }
        return new DateTimeFormatter(this.f17055, this.f17052, this.f17054, this.f17053, chronology, this.f17048, this.f17049, this.f17050);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatter m21234() {
        return m21249(DateTimeZone.UTC);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m21249(DateTimeZone dateTimeZone) {
        if (this.f17048 == dateTimeZone) {
            return this;
        }
        return new DateTimeFormatter(this.f17055, this.f17052, this.f17054, false, this.f17051, dateTimeZone, this.f17049, this.f17050);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeZone m21235() {
        return this.f17048;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21252(Appendable appendable, ReadableInstant readableInstant) throws IOException {
        m21233(appendable, DateTimeUtils.m20893(readableInstant), DateTimeUtils.m20888(readableInstant));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21254(StringBuffer stringBuffer, long j) {
        try {
            m21251((Appendable) stringBuffer, j);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21251(Appendable appendable, long j) throws IOException {
        m21233(appendable, j, (Chronology) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21253(Appendable appendable, ReadablePartial readablePartial) throws IOException {
        InternalPrinter r0 = m21230();
        if (readablePartial == null) {
            throw new IllegalArgumentException("The partial must not be null");
        }
        r0.m21541(appendable, readablePartial, this.f17054);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21245(ReadableInstant readableInstant) {
        StringBuilder sb = new StringBuilder(m21230().m21539());
        try {
            m21252((Appendable) sb, readableInstant);
        } catch (IOException e) {
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21246(ReadablePartial readablePartial) {
        StringBuilder sb = new StringBuilder(m21230().m21539());
        try {
            m21253((Appendable) sb, readablePartial);
        } catch (IOException e) {
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21233(Appendable appendable, long j, Chronology chronology) throws IOException {
        InternalPrinter r2 = m21230();
        Chronology r3 = m21232(chronology);
        DateTimeZone zone = r3.getZone();
        int offset = zone.getOffset(j);
        long j2 = ((long) offset) + j;
        if ((j ^ j2) < 0 && (((long) offset) ^ j) >= 0) {
            zone = DateTimeZone.UTC;
            offset = 0;
            j2 = j;
        }
        r2.m21540(appendable, j2, r3.withUTC(), offset, zone, this.f17054);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private InternalPrinter m21230() {
        InternalPrinter internalPrinter = this.f17055;
        if (internalPrinter != null) {
            return internalPrinter;
        }
        throw new UnsupportedOperationException("Printing not supported");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21244(String str) {
        return new DateTimeParserBucket(0, m21232(this.f17051), this.f17054, this.f17049, this.f17050).m21400(m21231(), (CharSequence) str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public LocalDate m21238(String str) {
        return m21241(str).toLocalDate();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public LocalTime m21242(String str) {
        return m21241(str).toLocalTime();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public LocalDateTime m21241(String str) {
        InternalParser r0 = m21231();
        Chronology withUTC = m21232((Chronology) null).withUTC();
        DateTimeParserBucket dateTimeParserBucket = new DateTimeParserBucket(0, withUTC, this.f17054, this.f17049, this.f17050);
        int r02 = r0.m21534(dateTimeParserBucket, str, 0);
        if (r02 < 0) {
            r02 ^= -1;
        } else if (r02 >= str.length()) {
            long r2 = dateTimeParserBucket.m21402(true, str);
            if (dateTimeParserBucket.m21398() != null) {
                withUTC = withUTC.withZone(DateTimeZone.forOffsetMillis(dateTimeParserBucket.m21398().intValue()));
            } else if (dateTimeParserBucket.m21399() != null) {
                withUTC = withUTC.withZone(dateTimeParserBucket.m21399());
            }
            return new LocalDateTime(r2, withUTC);
        }
        throw new IllegalArgumentException(FormatUtils.m21430(str, r02));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTime m21236(String str) {
        InternalParser r0 = m21231();
        Chronology r4 = m21232((Chronology) null);
        DateTimeParserBucket dateTimeParserBucket = new DateTimeParserBucket(0, r4, this.f17054, this.f17049, this.f17050);
        int r02 = r0.m21534(dateTimeParserBucket, str, 0);
        if (r02 < 0) {
            r02 ^= -1;
        } else if (r02 >= str.length()) {
            long r2 = dateTimeParserBucket.m21402(true, str);
            if (this.f17053 && dateTimeParserBucket.m21398() != null) {
                r4 = r4.withZone(DateTimeZone.forOffsetMillis(dateTimeParserBucket.m21398().intValue()));
            } else if (dateTimeParserBucket.m21399() != null) {
                r4 = r4.withZone(dateTimeParserBucket.m21399());
            }
            DateTime dateTime = new DateTime(r2, r4);
            if (this.f17048 != null) {
                return dateTime.withZone(this.f17048);
            }
            return dateTime;
        }
        throw new IllegalArgumentException(FormatUtils.m21430(str, r02));
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private InternalParser m21231() {
        InternalParser internalParser = this.f17052;
        if (internalParser != null) {
            return internalParser;
        }
        throw new UnsupportedOperationException("Parsing not supported");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Chronology m21232(Chronology chronology) {
        Chronology r0 = DateTimeUtils.m20895(chronology);
        if (this.f17051 != null) {
            r0 = this.f17051;
        }
        if (this.f17048 != null) {
            return r0.withZone(this.f17048);
        }
        return r0;
    }
}
