package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

class DateTimePrinterInternalPrinter implements InternalPrinter {

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimePrinter f17116;

    /* renamed from: 龘  reason: contains not printable characters */
    static InternalPrinter m21424(DateTimePrinter dateTimePrinter) {
        if (dateTimePrinter instanceof InternalPrinterDateTimePrinter) {
            return (InternalPrinter) dateTimePrinter;
        }
        if (dateTimePrinter == null) {
            return null;
        }
        return new DateTimePrinterInternalPrinter(dateTimePrinter);
    }

    private DateTimePrinterInternalPrinter(DateTimePrinter dateTimePrinter) {
        this.f17116 = dateTimePrinter;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21425() {
        return this.f17116.m21419();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21426(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
        if (appendable instanceof StringBuffer) {
            this.f17116.m21422((StringBuffer) appendable, j, chronology, i, dateTimeZone, locale);
        } else if (appendable instanceof Writer) {
            this.f17116.m21420((Writer) appendable, j, chronology, i, dateTimeZone, locale);
        } else {
            StringBuffer stringBuffer = new StringBuffer(m21425());
            this.f17116.m21422(stringBuffer, j, chronology, i, dateTimeZone, locale);
            appendable.append(stringBuffer);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21427(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        if (appendable instanceof StringBuffer) {
            this.f17116.m21423((StringBuffer) appendable, readablePartial, locale);
        } else if (appendable instanceof Writer) {
            this.f17116.m21421((Writer) appendable, readablePartial, locale);
        } else {
            StringBuffer stringBuffer = new StringBuffer(m21425());
            this.f17116.m21423(stringBuffer, readablePartial, locale);
            appendable.append(stringBuffer);
        }
    }
}
