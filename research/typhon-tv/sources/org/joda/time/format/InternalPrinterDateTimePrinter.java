package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

class InternalPrinterDateTimePrinter implements DateTimePrinter, InternalPrinter {

    /* renamed from: 龘  reason: contains not printable characters */
    private final InternalPrinter f17179;

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21542() {
        return this.f17179.m21539();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21547(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
        try {
            this.f17179.m21540(stringBuffer, j, chronology, i, dateTimeZone, locale);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21543(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
        this.f17179.m21540(writer, j, chronology, i, dateTimeZone, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21545(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
        this.f17179.m21540(appendable, j, chronology, i, dateTimeZone, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21548(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
        try {
            this.f17179.m21541(stringBuffer, readablePartial, locale);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21544(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
        this.f17179.m21541(writer, readablePartial, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21546(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        this.f17179.m21541(appendable, readablePartial, locale);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof InternalPrinterDateTimePrinter) {
            return this.f17179.equals(((InternalPrinterDateTimePrinter) obj).f17179);
        }
        return false;
    }
}
