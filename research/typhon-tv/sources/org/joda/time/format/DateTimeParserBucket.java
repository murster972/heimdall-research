package org.joda.time.format;

import java.util.Arrays;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.IllegalInstantException;

public class DateTimeParserBucket {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Integer f17093;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeZone f17094;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public Integer f17095;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Object f17096;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f17097;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Integer f17098;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public SavedField[] f17099;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int f17100;

    /* renamed from: 连任  reason: contains not printable characters */
    private final DateTimeZone f17101;

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f17102;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f17103;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Locale f17104;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Chronology f17105;

    public DateTimeParserBucket(long j, Chronology chronology, Locale locale, Integer num, int i) {
        Chronology r0 = DateTimeUtils.m20895(chronology);
        this.f17102 = j;
        this.f17101 = r0.getZone();
        this.f17105 = r0.withUTC();
        this.f17104 = locale == null ? Locale.getDefault() : locale;
        this.f17103 = i;
        this.f17093 = num;
        this.f17094 = this.f17101;
        this.f17098 = this.f17093;
        this.f17099 = new SavedField[8];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m21400(InternalParser internalParser, CharSequence charSequence) {
        int r0 = internalParser.m21534(this, charSequence, 0);
        if (r0 < 0) {
            r0 ^= -1;
        } else if (r0 >= charSequence.length()) {
            return m21401(true, charSequence);
        }
        throw new IllegalArgumentException(FormatUtils.m21430(charSequence.toString(), r0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m21403() {
        return this.f17105;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Locale m21397() {
        return this.f17104;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeZone m21399() {
        return this.f17094;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21408(DateTimeZone dateTimeZone) {
        this.f17096 = null;
        this.f17094 = dateTimeZone;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Integer m21398() {
        return this.f17095;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21404(Integer num) {
        this.f17096 = null;
        this.f17095 = num;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Integer m21396() {
        return this.f17098;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21405(DateTimeField dateTimeField, int i) {
        m21383().m21412(dateTimeField, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21406(DateTimeFieldType dateTimeFieldType, int i) {
        m21383().m21412(dateTimeFieldType.getField(this.f17105), i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m21407(DateTimeFieldType dateTimeFieldType, String str, Locale locale) {
        m21383().m21413(dateTimeFieldType.getField(this.f17105), str, locale);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private SavedField m21383() {
        SavedField[] savedFieldArr;
        SavedField savedField;
        SavedField[] savedFieldArr2 = this.f17099;
        int i = this.f17100;
        if (i == savedFieldArr2.length || this.f17097) {
            savedFieldArr = new SavedField[(i == savedFieldArr2.length ? i * 2 : savedFieldArr2.length)];
            System.arraycopy(savedFieldArr2, 0, savedFieldArr, 0, i);
            this.f17099 = savedFieldArr;
            this.f17097 = false;
        } else {
            savedFieldArr = savedFieldArr2;
        }
        this.f17096 = null;
        SavedField savedField2 = savedFieldArr[i];
        if (savedField2 == null) {
            SavedField savedField3 = new SavedField();
            savedFieldArr[i] = savedField3;
            savedField = savedField3;
        } else {
            savedField = savedField2;
        }
        this.f17100 = i + 1;
        return savedField;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m21395() {
        if (this.f17096 == null) {
            this.f17096 = new SavedState();
        }
        return this.f17096;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m21409(Object obj) {
        if (!(obj instanceof SavedState) || !((SavedState) obj).m21414(this)) {
            return false;
        }
        this.f17096 = obj;
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21402(boolean z, String str) {
        return m21401(z, (CharSequence) str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m21401(boolean z, CharSequence charSequence) {
        boolean z2;
        SavedField[] savedFieldArr = this.f17099;
        int i = this.f17100;
        if (this.f17097) {
            savedFieldArr = (SavedField[]) this.f17099.clone();
            this.f17099 = savedFieldArr;
            this.f17097 = false;
        }
        m21392(savedFieldArr, i);
        if (i > 0) {
            DurationField field = DurationFieldType.months().getField(this.f17105);
            DurationField field2 = DurationFieldType.days().getField(this.f17105);
            DurationField durationField = savedFieldArr[0].f17109.getDurationField();
            if (m21387(durationField, field) >= 0 && m21387(durationField, field2) <= 0) {
                m21406(DateTimeFieldType.year(), this.f17103);
                return m21401(z, charSequence);
            }
        }
        long j = this.f17102;
        int i2 = 0;
        while (i2 < i) {
            try {
                j = savedFieldArr[i2].m21411(j, z);
                i2++;
            } catch (IllegalFieldValueException e) {
                if (charSequence != null) {
                    e.prependMessage("Cannot parse \"" + charSequence + '\"');
                }
                throw e;
            }
        }
        if (z) {
            for (int i3 = 0; i3 < i; i3++) {
                SavedField savedField = savedFieldArr[i3];
                if (i3 == i - 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                j = savedField.m21411(j, z2);
            }
        }
        long j2 = j;
        if (this.f17095 != null) {
            return j2 - ((long) this.f17095.intValue());
        }
        if (this.f17094 == null) {
            return j2;
        }
        int offsetFromLocal = this.f17094.getOffsetFromLocal(j2);
        long j3 = j2 - ((long) offsetFromLocal);
        if (offsetFromLocal == this.f17094.getOffset(j3)) {
            return j3;
        }
        String str = "Illegal instant due to time zone offset transition (" + this.f17094 + ')';
        if (charSequence != null) {
            str = "Cannot parse \"" + charSequence + "\": " + str;
        }
        throw new IllegalInstantException(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21392(SavedField[] savedFieldArr, int i) {
        if (i > 10) {
            Arrays.sort(savedFieldArr, 0, i);
            return;
        }
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = i2;
            while (i3 > 0 && savedFieldArr[i3 - 1].compareTo(savedFieldArr[i3]) > 0) {
                SavedField savedField = savedFieldArr[i3];
                savedFieldArr[i3] = savedFieldArr[i3 - 1];
                savedFieldArr[i3 - 1] = savedField;
                i3--;
            }
        }
    }

    class SavedState {

        /* renamed from: 靐  reason: contains not printable characters */
        final Integer f17111;

        /* renamed from: 麤  reason: contains not printable characters */
        final int f17112;

        /* renamed from: 齉  reason: contains not printable characters */
        final SavedField[] f17113;

        /* renamed from: 龘  reason: contains not printable characters */
        final DateTimeZone f17114;

        SavedState() {
            this.f17114 = DateTimeParserBucket.this.f17094;
            this.f17111 = DateTimeParserBucket.this.f17095;
            this.f17113 = DateTimeParserBucket.this.f17099;
            this.f17112 = DateTimeParserBucket.this.f17100;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m21414(DateTimeParserBucket dateTimeParserBucket) {
            if (dateTimeParserBucket != DateTimeParserBucket.this) {
                return false;
            }
            DateTimeZone unused = dateTimeParserBucket.f17094 = this.f17114;
            Integer unused2 = dateTimeParserBucket.f17095 = this.f17111;
            SavedField[] unused3 = dateTimeParserBucket.f17099 = this.f17113;
            if (this.f17112 < dateTimeParserBucket.f17100) {
                boolean unused4 = dateTimeParserBucket.f17097 = true;
            }
            int unused5 = dateTimeParserBucket.f17100 = this.f17112;
            return true;
        }
    }

    static class SavedField implements Comparable<SavedField> {

        /* renamed from: 靐  reason: contains not printable characters */
        int f17106;

        /* renamed from: 麤  reason: contains not printable characters */
        Locale f17107;

        /* renamed from: 齉  reason: contains not printable characters */
        String f17108;

        /* renamed from: 龘  reason: contains not printable characters */
        DateTimeField f17109;

        SavedField() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m21412(DateTimeField dateTimeField, int i) {
            this.f17109 = dateTimeField;
            this.f17106 = i;
            this.f17108 = null;
            this.f17107 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m21413(DateTimeField dateTimeField, String str, Locale locale) {
            this.f17109 = dateTimeField;
            this.f17106 = 0;
            this.f17108 = str;
            this.f17107 = locale;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m21411(long j, boolean z) {
            long j2;
            if (this.f17108 == null) {
                j2 = this.f17109.setExtended(j, this.f17106);
            } else {
                j2 = this.f17109.set(j, this.f17108, this.f17107);
            }
            if (z) {
                return this.f17109.roundFloor(j2);
            }
            return j2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int compareTo(SavedField savedField) {
            DateTimeField dateTimeField = savedField.f17109;
            int r0 = DateTimeParserBucket.m21387(this.f17109.getRangeDurationField(), dateTimeField.getRangeDurationField());
            return r0 != 0 ? r0 : DateTimeParserBucket.m21387(this.f17109.getDurationField(), dateTimeField.getDurationField());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m21387(DurationField durationField, DurationField durationField2) {
        if (durationField == null || !durationField.isSupported()) {
            if (durationField2 == null || !durationField2.isSupported()) {
                return 0;
            }
            return -1;
        } else if (durationField2 == null || !durationField2.isSupported()) {
            return 1;
        } else {
            return -durationField.compareTo(durationField2);
        }
    }
}
