package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

public interface DateTimePrinter {
    /* renamed from: 龘  reason: contains not printable characters */
    int m21419();

    /* renamed from: 龘  reason: contains not printable characters */
    void m21420(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m21421(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m21422(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale);

    /* renamed from: 龘  reason: contains not printable characters */
    void m21423(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale);
}
