package org.joda.time.format;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReferenceArray;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class DateTimeFormat {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final AtomicReferenceArray<DateTimeFormatter> f17046 = new AtomicReferenceArray<>(25);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ConcurrentHashMap<String, DateTimeFormatter> f17047 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeFormatter m21228(String str) {
        return m21226(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21229(DateTimeFormatterBuilder dateTimeFormatterBuilder, String str) {
        int length = str.length();
        int[] iArr = new int[1];
        int i = 0;
        while (i < length) {
            iArr[0] = i;
            String r0 = m21227(str, iArr);
            int i2 = iArr[0];
            int length2 = r0.length();
            if (length2 != 0) {
                char charAt = r0.charAt(0);
                switch (charAt) {
                    case '\'':
                        String substring = r0.substring(1);
                        if (substring.length() != 1) {
                            dateTimeFormatterBuilder.m21303(new String(substring));
                            break;
                        } else {
                            dateTimeFormatterBuilder.m21299(substring.charAt(0));
                            break;
                        }
                    case 'C':
                        dateTimeFormatterBuilder.m21271(length2, length2);
                        break;
                    case 'D':
                        dateTimeFormatterBuilder.m21278(length2);
                        break;
                    case 'E':
                        if (length2 < 4) {
                            dateTimeFormatterBuilder.m21266();
                            break;
                        } else {
                            dateTimeFormatterBuilder.m21281();
                            break;
                        }
                    case 'G':
                        dateTimeFormatterBuilder.m21275();
                        break;
                    case 'H':
                        dateTimeFormatterBuilder.m21292(length2);
                        break;
                    case 'K':
                        dateTimeFormatterBuilder.m21267(length2);
                        break;
                    case 'M':
                        if (length2 >= 3) {
                            if (length2 < 4) {
                                dateTimeFormatterBuilder.m21272();
                                break;
                            } else {
                                dateTimeFormatterBuilder.m21269();
                                break;
                            }
                        } else {
                            dateTimeFormatterBuilder.m21274(length2);
                            break;
                        }
                    case 'S':
                        dateTimeFormatterBuilder.m21301(length2, length2);
                        break;
                    case 'Y':
                    case 'x':
                    case 'y':
                        if (length2 != 2) {
                            int i3 = 9;
                            if (i2 + 1 < length) {
                                iArr[0] = iArr[0] + 1;
                                if (m21225(m21227(str, iArr))) {
                                    i3 = length2;
                                }
                                iArr[0] = iArr[0] - 1;
                            }
                            switch (charAt) {
                                case 'Y':
                                    dateTimeFormatterBuilder.m21268(length2, i3);
                                    break;
                                case 'x':
                                    dateTimeFormatterBuilder.m21293(length2, i3);
                                    break;
                                case 'y':
                                    dateTimeFormatterBuilder.m21283(length2, i3);
                                    break;
                            }
                        } else {
                            boolean z = true;
                            if (i2 + 1 < length) {
                                iArr[0] = iArr[0] + 1;
                                if (m21225(m21227(str, iArr))) {
                                    z = false;
                                }
                                iArr[0] = iArr[0] - 1;
                            }
                            switch (charAt) {
                                case 'x':
                                    dateTimeFormatterBuilder.m21286(new DateTime().getWeekyear() - 30, z);
                                    break;
                                default:
                                    dateTimeFormatterBuilder.m21302(new DateTime().getYear() - 30, z);
                                    break;
                            }
                        }
                    case 'Z':
                        if (length2 != 1) {
                            if (length2 != 2) {
                                dateTimeFormatterBuilder.m21279();
                                break;
                            } else {
                                dateTimeFormatterBuilder.m21304((String) null, "Z", true, 2, 2);
                                break;
                            }
                        } else {
                            dateTimeFormatterBuilder.m21304((String) null, "Z", false, 2, 2);
                            break;
                        }
                    case 'a':
                        dateTimeFormatterBuilder.m21291();
                        break;
                    case 'd':
                        dateTimeFormatterBuilder.m21276(length2);
                        break;
                    case 'e':
                        dateTimeFormatterBuilder.m21273(length2);
                        break;
                    case 'h':
                        dateTimeFormatterBuilder.m21270(length2);
                        break;
                    case 'k':
                        dateTimeFormatterBuilder.m21282(length2);
                        break;
                    case 'm':
                        dateTimeFormatterBuilder.m21294(length2);
                        break;
                    case 's':
                        dateTimeFormatterBuilder.m21284(length2);
                        break;
                    case 'w':
                        dateTimeFormatterBuilder.m21280(length2);
                        break;
                    case 'z':
                        if (length2 < 4) {
                            dateTimeFormatterBuilder.m21306((Map<String, DateTimeZone>) null);
                            break;
                        } else {
                            dateTimeFormatterBuilder.m21277();
                            break;
                        }
                    default:
                        throw new IllegalArgumentException("Illegal pattern component: " + r0);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0069, code lost:
        r2 = r2 - 1;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m21227(java.lang.String r11, int[] r12) {
        /*
            r10 = 97
            r9 = 90
            r8 = 65
            r7 = 39
            r1 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r2 = r12[r1]
            int r4 = r11.length()
            char r0 = r11.charAt(r2)
            if (r0 < r8) goto L_0x001c
            if (r0 <= r9) goto L_0x0022
        L_0x001c:
            if (r0 < r10) goto L_0x0037
            r5 = 122(0x7a, float:1.71E-43)
            if (r0 > r5) goto L_0x0037
        L_0x0022:
            r3.append(r0)
        L_0x0025:
            int r5 = r2 + 1
            if (r5 >= r4) goto L_0x006b
            int r5 = r2 + 1
            char r5 = r11.charAt(r5)
            if (r5 != r0) goto L_0x006b
            r3.append(r0)
            int r2 = r2 + 1
            goto L_0x0025
        L_0x0037:
            r3.append(r7)
            r0 = r1
        L_0x003b:
            if (r2 >= r4) goto L_0x006b
            char r5 = r11.charAt(r2)
            if (r5 != r7) goto L_0x005d
            int r6 = r2 + 1
            if (r6 >= r4) goto L_0x0057
            int r6 = r2 + 1
            char r6 = r11.charAt(r6)
            if (r6 != r7) goto L_0x0057
            int r2 = r2 + 1
            r3.append(r5)
        L_0x0054:
            int r2 = r2 + 1
            goto L_0x003b
        L_0x0057:
            if (r0 != 0) goto L_0x005b
            r0 = 1
            goto L_0x0054
        L_0x005b:
            r0 = r1
            goto L_0x0054
        L_0x005d:
            if (r0 != 0) goto L_0x0072
            if (r5 < r8) goto L_0x0063
            if (r5 <= r9) goto L_0x0069
        L_0x0063:
            if (r5 < r10) goto L_0x0072
            r6 = 122(0x7a, float:1.71E-43)
            if (r5 > r6) goto L_0x0072
        L_0x0069:
            int r2 = r2 + -1
        L_0x006b:
            r12[r1] = r2
            java.lang.String r0 = r3.toString()
            return r0
        L_0x0072:
            r3.append(r5)
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.DateTimeFormat.m21227(java.lang.String, int[]):java.lang.String");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m21225(String str) {
        int length = str.length();
        if (length > 0) {
            switch (str.charAt(0)) {
                case 'C':
                case 'D':
                case 'F':
                case 'H':
                case 'K':
                case 'S':
                case 'W':
                case 'Y':
                case 'c':
                case 'd':
                case 'e':
                case 'h':
                case 'k':
                case 'm':
                case 's':
                case 'w':
                case 'x':
                case 'y':
                    return true;
                case 'M':
                    if (length <= 2) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0031, code lost:
        r0 = f17047.putIfAbsent(r3, r1);
     */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.joda.time.format.DateTimeFormatter m21226(java.lang.String r3) {
        /*
            if (r3 == 0) goto L_0x0008
            int r0 = r3.length()
            if (r0 != 0) goto L_0x0011
        L_0x0008:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Invalid pattern specification"
            r0.<init>(r1)
            throw r0
        L_0x0011:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, org.joda.time.format.DateTimeFormatter> r0 = f17047
            java.lang.Object r0 = r0.get(r3)
            org.joda.time.format.DateTimeFormatter r0 = (org.joda.time.format.DateTimeFormatter) r0
            if (r0 != 0) goto L_0x003b
            org.joda.time.format.DateTimeFormatterBuilder r0 = new org.joda.time.format.DateTimeFormatterBuilder
            r0.<init>()
            m21229((org.joda.time.format.DateTimeFormatterBuilder) r0, (java.lang.String) r3)
            org.joda.time.format.DateTimeFormatter r1 = r0.m21298()
            java.util.concurrent.ConcurrentHashMap<java.lang.String, org.joda.time.format.DateTimeFormatter> r0 = f17047
            int r0 = r0.size()
            r2 = 500(0x1f4, float:7.0E-43)
            if (r0 >= r2) goto L_0x003c
            java.util.concurrent.ConcurrentHashMap<java.lang.String, org.joda.time.format.DateTimeFormatter> r0 = f17047
            java.lang.Object r0 = r0.putIfAbsent(r3, r1)
            org.joda.time.format.DateTimeFormatter r0 = (org.joda.time.format.DateTimeFormatter) r0
            if (r0 == 0) goto L_0x003c
        L_0x003b:
            return r0
        L_0x003c:
            r0 = r1
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.DateTimeFormat.m21226(java.lang.String):org.joda.time.format.DateTimeFormatter");
    }
}
