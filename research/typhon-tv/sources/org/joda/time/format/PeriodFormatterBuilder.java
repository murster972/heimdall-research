package org.joda.time.format;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;
import org.joda.time.DurationFieldType;
import org.joda.time.PeriodType;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePeriod;

public class PeriodFormatterBuilder {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ConcurrentMap<String, Pattern> f17184 = new ConcurrentHashMap();

    /* renamed from: ʻ  reason: contains not printable characters */
    private PeriodFieldAffix f17185;

    /* renamed from: ʼ  reason: contains not printable characters */
    private List<Object> f17186;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f17187;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f17188;

    /* renamed from: ٴ  reason: contains not printable characters */
    private FieldFormatter[] f17189;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f17190;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f17191;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f17192;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f17193;

    interface PeriodFieldAffix {
        /* renamed from: 靐  reason: contains not printable characters */
        int m21607(String str, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        int m21608(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        int m21609(String str, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m21610(StringBuffer stringBuffer, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m21611(Set<PeriodFieldAffix> set);

        /* renamed from: 龘  reason: contains not printable characters */
        String[] m21612();
    }

    public PeriodFormatterBuilder() {
        m21573();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodFormatter m21577() {
        PeriodFormatter r1 = m21560(this.f17186, this.f17187, this.f17188);
        for (FieldFormatter fieldFormatter : this.f17189) {
            if (fieldFormatter != null) {
                fieldFormatter.m21599(this.f17189);
            }
        }
        this.f17189 = (FieldFormatter[]) this.f17189.clone();
        return r1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m21573() {
        this.f17191 = 1;
        this.f17193 = 2;
        this.f17192 = 10;
        this.f17190 = false;
        this.f17185 = null;
        if (this.f17186 == null) {
            this.f17186 = new ArrayList();
        } else {
            this.f17186.clear();
        }
        this.f17187 = false;
        this.f17188 = false;
        this.f17189 = new FieldFormatter[10];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodFormatterBuilder m21578(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Literal must not be null");
        }
        m21559();
        Literal literal = new Literal(str);
        m21563((PeriodPrinter) literal, (PeriodParser) literal);
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PeriodFormatterBuilder m21575() {
        m21564(0);
        return this;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public PeriodFormatterBuilder m21574() {
        m21564(1);
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public PeriodFormatterBuilder m21571() {
        m21564(2);
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public PeriodFormatterBuilder m21567() {
        m21564(3);
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public PeriodFormatterBuilder m21568() {
        m21564(4);
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public PeriodFormatterBuilder m21569() {
        m21564(5);
        return this;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public PeriodFormatterBuilder m21570() {
        m21564(9);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21564(int i) {
        m21565(i, this.f17191);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21565(int i, int i2) {
        FieldFormatter fieldFormatter = new FieldFormatter(i2, this.f17193, this.f17192, this.f17190, i, this.f17189, this.f17185, (PeriodFieldAffix) null);
        m21563((PeriodPrinter) fieldFormatter, (PeriodParser) fieldFormatter);
        this.f17189[i] = fieldFormatter;
        this.f17185 = null;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PeriodFormatterBuilder m21572(String str) {
        if (str != null) {
            return m21562((PeriodFieldAffix) new SimpleAffix(str));
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodFormatterBuilder m21562(PeriodFieldAffix periodFieldAffix) {
        Object obj;
        Object obj2;
        if (this.f17186.size() > 0) {
            obj2 = this.f17186.get(this.f17186.size() - 2);
            obj = this.f17186.get(this.f17186.size() - 1);
        } else {
            obj = null;
            obj2 = null;
        }
        if (obj2 == null || obj == null || obj2 != obj || !(obj2 instanceof FieldFormatter)) {
            throw new IllegalStateException("No field to apply suffix to");
        }
        m21559();
        FieldFormatter fieldFormatter = new FieldFormatter((FieldFormatter) obj2, periodFieldAffix);
        this.f17186.set(this.f17186.size() - 2, fieldFormatter);
        this.f17186.set(this.f17186.size() - 1, fieldFormatter);
        this.f17189[fieldFormatter.m21592()] = fieldFormatter;
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PeriodFormatterBuilder m21576(String str) {
        return m21561(str, str, (String[]) null, false, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodFormatterBuilder m21561(String str, String str2, String[] strArr, boolean z, boolean z2) {
        List<Object> list;
        if (str == null || str2 == null) {
            throw new IllegalArgumentException();
        }
        m21559();
        List<Object> list2 = this.f17186;
        if (list2.size() != 0) {
            Separator separator = null;
            int size = list2.size();
            while (true) {
                int i = size - 1;
                if (i < 0) {
                    list = list2;
                    break;
                } else if (list2.get(i) instanceof Separator) {
                    separator = (Separator) list2.get(i);
                    list = list2.subList(i + 1, list2.size());
                    break;
                } else {
                    size = i - 1;
                }
            }
            if (separator == null || list.size() != 0) {
                Object[] r1 = m21566(list);
                list.clear();
                Separator separator2 = new Separator(str, str2, strArr, (PeriodPrinter) r1[0], (PeriodParser) r1[1], z, z2);
                list.add(separator2);
                list.add(separator2);
            } else {
                throw new IllegalStateException("Cannot have two adjacent separators");
            }
        } else if (z2 && !z) {
            Separator separator3 = new Separator(str, str2, strArr, Literal.f17208, Literal.f17208, z, z2);
            m21563((PeriodPrinter) separator3, (PeriodParser) separator3);
        }
        return this;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m21559() throws IllegalStateException {
        if (this.f17185 != null) {
            throw new IllegalStateException("Prefix not followed by field");
        }
        this.f17185 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodFormatterBuilder m21563(PeriodPrinter periodPrinter, PeriodParser periodParser) {
        boolean z;
        boolean z2 = true;
        this.f17186.add(periodPrinter);
        this.f17186.add(periodParser);
        boolean z3 = this.f17187;
        if (periodPrinter == null) {
            z = true;
        } else {
            z = false;
        }
        this.f17187 = z | z3;
        boolean z4 = this.f17188;
        if (periodParser != null) {
            z2 = false;
        }
        this.f17188 = z4 | z2;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static PeriodFormatter m21560(List<Object> list, boolean z, boolean z2) {
        if (!z || !z2) {
            int size = list.size();
            if (size >= 2 && (list.get(0) instanceof Separator)) {
                Separator separator = (Separator) list.get(0);
                if (separator.f17213 == null && separator.f17211 == null) {
                    PeriodFormatter r1 = m21560(list.subList(2, size), z, z2);
                    Separator r12 = separator.m21618(r1.m21558(), r1.m21553());
                    return new PeriodFormatter(r12, r12);
                }
            }
            Object[] r3 = m21566(list);
            if (z) {
                return new PeriodFormatter((PeriodPrinter) null, (PeriodParser) r3[1]);
            }
            if (z2) {
                return new PeriodFormatter((PeriodPrinter) r3[0], (PeriodParser) null);
            }
            return new PeriodFormatter((PeriodPrinter) r3[0], (PeriodParser) r3[1]);
        }
        throw new IllegalStateException("Builder has created neither a printer nor a parser");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object[] m21566(List<Object> list) {
        switch (list.size()) {
            case 0:
                return new Object[]{Literal.f17208, Literal.f17208};
            case 1:
                return new Object[]{list.get(0), list.get(1)};
            default:
                Composite composite = new Composite(list);
                return new Object[]{composite, composite};
        }
    }

    static abstract class IgnorableAffix implements PeriodFieldAffix {

        /* renamed from: 龘  reason: contains not printable characters */
        private volatile String[] f17207;

        IgnorableAffix() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21601(Set<PeriodFieldAffix> set) {
            if (this.f17207 == null) {
                int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
                String str = null;
                String[] r5 = m21612();
                int length = r5.length;
                int i2 = 0;
                while (i2 < length) {
                    String str2 = r5[i2];
                    if (str2.length() < i) {
                        i = str2.length();
                    } else {
                        str2 = str;
                    }
                    i2++;
                    str = str2;
                }
                HashSet hashSet = new HashSet();
                for (PeriodFieldAffix next : set) {
                    if (next != null) {
                        for (String str3 : next.m21612()) {
                            if (str3.length() > i || (str3.equalsIgnoreCase(str) && !str3.equals(str))) {
                                hashSet.add(str3);
                            }
                        }
                    }
                }
                this.f17207 = (String[]) hashSet.toArray(new String[hashSet.size()]);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m21602(int i, String str, int i2) {
            if (this.f17207 != null) {
                for (String str2 : this.f17207) {
                    int length = str2.length();
                    if ((i < length && str.regionMatches(true, i2, str2, 0, length)) || (i == length && str.regionMatches(false, i2, str2, 0, length))) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    static class SimpleAffix extends IgnorableAffix {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f17219;

        SimpleAffix(String str) {
            this.f17219 = str;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21621(int i) {
            return this.f17219.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21623(StringBuffer stringBuffer, int i) {
            stringBuffer.append(this.f17219);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21622(String str, int i) {
            String str2 = this.f17219;
            int length = str2.length();
            if (!str.regionMatches(true, i, str2, 0, length) || m21602(length, str, i)) {
                return i ^ -1;
            }
            return i + length;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21620(String str, int i) {
            String str2 = this.f17219;
            int length = str2.length();
            int length2 = str.length();
            int i2 = i;
            while (i2 < length2) {
                if (str.regionMatches(true, i2, str2, 0, length) && !m21602(length, str, i2)) {
                    return i2;
                }
                switch (str.charAt(i2)) {
                    case '+':
                    case ',':
                    case '-':
                    case '.':
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        i2++;
                }
                return i ^ -1;
            }
            return i ^ -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String[] m21624() {
            return new String[]{this.f17219};
        }
    }

    static class CompositeAffix extends IgnorableAffix {

        /* renamed from: 靐  reason: contains not printable characters */
        private final PeriodFieldAffix f17196;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String[] f17197;

        /* renamed from: 龘  reason: contains not printable characters */
        private final PeriodFieldAffix f17198;

        CompositeAffix(PeriodFieldAffix periodFieldAffix, PeriodFieldAffix periodFieldAffix2) {
            this.f17198 = periodFieldAffix;
            this.f17196 = periodFieldAffix2;
            HashSet hashSet = new HashSet();
            for (String str : this.f17198.m21612()) {
                String[] r7 = this.f17196.m21612();
                int length = r7.length;
                for (int i = 0; i < length; i++) {
                    hashSet.add(str + r7[i]);
                }
            }
            this.f17197 = (String[]) hashSet.toArray(new String[hashSet.size()]);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21586(int i) {
            return this.f17198.m21608(i) + this.f17196.m21608(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21588(StringBuffer stringBuffer, int i) {
            this.f17198.m21610(stringBuffer, i);
            this.f17196.m21610(stringBuffer, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21587(String str, int i) {
            int r0 = this.f17198.m21609(str, i);
            if (r0 < 0) {
                return r0;
            }
            int r02 = this.f17196.m21609(str, r0);
            if (r02 < 0 || !m21602(m21587(str, r02) - r02, str, i)) {
                return r02;
            }
            return i ^ -1;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21585(String str, int i) {
            int r1;
            int r0 = this.f17198.m21607(str, i);
            if (r0 < 0 || ((r1 = this.f17196.m21607(str, this.f17198.m21609(str, r0))) >= 0 && m21602(this.f17196.m21609(str, r1) - r0, str, i))) {
                return i ^ -1;
            }
            return r0 > 0 ? r0 : r1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String[] m21589() {
            return (String[]) this.f17197.clone();
        }
    }

    static class FieldFormatter implements PeriodParser, PeriodPrinter {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final FieldFormatter[] f17199;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final PeriodFieldAffix f17200;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final PeriodFieldAffix f17201;

        /* renamed from: 连任  reason: contains not printable characters */
        private final int f17202;

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f17203;

        /* renamed from: 麤  reason: contains not printable characters */
        private final boolean f17204;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f17205;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f17206;

        FieldFormatter(int i, int i2, int i3, boolean z, int i4, FieldFormatter[] fieldFormatterArr, PeriodFieldAffix periodFieldAffix, PeriodFieldAffix periodFieldAffix2) {
            this.f17206 = i;
            this.f17203 = i2;
            this.f17205 = i3;
            this.f17204 = z;
            this.f17202 = i4;
            this.f17199 = fieldFormatterArr;
            this.f17200 = periodFieldAffix;
            this.f17201 = periodFieldAffix2;
        }

        FieldFormatter(FieldFormatter fieldFormatter, PeriodFieldAffix periodFieldAffix) {
            this.f17206 = fieldFormatter.f17206;
            this.f17203 = fieldFormatter.f17203;
            this.f17205 = fieldFormatter.f17205;
            this.f17204 = fieldFormatter.f17204;
            this.f17202 = fieldFormatter.f17202;
            this.f17199 = fieldFormatter.f17199;
            this.f17200 = fieldFormatter.f17200;
            this.f17201 = fieldFormatter.f17201 != null ? new CompositeAffix(fieldFormatter.f17201, periodFieldAffix) : periodFieldAffix;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21599(FieldFormatter[] fieldFormatterArr) {
            HashSet hashSet = new HashSet();
            HashSet hashSet2 = new HashSet();
            for (FieldFormatter fieldFormatter : fieldFormatterArr) {
                if (fieldFormatter != null && !equals(fieldFormatter)) {
                    hashSet.add(fieldFormatter.f17200);
                    hashSet2.add(fieldFormatter.f17201);
                }
            }
            if (this.f17200 != null) {
                this.f17200.m21611((Set<PeriodFieldAffix>) hashSet);
            }
            if (this.f17201 != null) {
                this.f17201.m21611((Set<PeriodFieldAffix>) hashSet2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21594(ReadablePeriod readablePeriod, int i, Locale locale) {
            if (i <= 0) {
                return 0;
            }
            if (this.f17203 == 4 || m21596(readablePeriod) != Long.MAX_VALUE) {
                return 1;
            }
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21595(ReadablePeriod readablePeriod, Locale locale) {
            long r2 = m21596(readablePeriod);
            if (r2 == Long.MAX_VALUE) {
                return 0;
            }
            int max = Math.max(FormatUtils.m21428(r2), this.f17206);
            if (this.f17202 >= 8) {
                max = (r2 < 0 ? Math.max(max, 5) : Math.max(max, 4)) + 1;
                if (this.f17202 == 9 && Math.abs(r2) % 1000 == 0) {
                    max -= 4;
                }
                r2 /= 1000;
            }
            int i = (int) r2;
            if (this.f17200 != null) {
                max += this.f17200.m21608(i);
            }
            if (this.f17201 != null) {
                return max + this.f17201.m21608(i);
            }
            return max;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21597(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            long r2 = m21596(readablePeriod);
            if (r2 != Long.MAX_VALUE) {
                int i = (int) r2;
                if (this.f17202 >= 8) {
                    i = (int) (r2 / 1000);
                }
                if (this.f17200 != null) {
                    this.f17200.m21610(stringBuffer, i);
                }
                int length = stringBuffer.length();
                int i2 = this.f17206;
                if (i2 <= 1) {
                    FormatUtils.m21434(stringBuffer, i);
                } else {
                    FormatUtils.m21435(stringBuffer, i, i2);
                }
                if (this.f17202 >= 8) {
                    int abs = (int) (Math.abs(r2) % 1000);
                    if (this.f17202 == 8 || abs > 0) {
                        if (r2 < 0 && r2 > -1000) {
                            stringBuffer.insert(length, '-');
                        }
                        stringBuffer.append('.');
                        FormatUtils.m21435(stringBuffer, abs, 3);
                    }
                }
                if (this.f17201 != null) {
                    this.f17201.m21610(stringBuffer, i);
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:105:0x015a, code lost:
            r5 = r6;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m21593(org.joda.time.ReadWritablePeriod r10, java.lang.String r11, int r12, java.util.Locale r13) {
            /*
                r9 = this;
                int r0 = r9.f17203
                r1 = 4
                if (r0 != r1) goto L_0x0011
                r0 = 1
            L_0x0006:
                int r1 = r11.length()
                if (r12 < r1) goto L_0x0013
                if (r0 == 0) goto L_0x0010
                r12 = r12 ^ -1
            L_0x0010:
                return r12
            L_0x0011:
                r0 = 0
                goto L_0x0006
            L_0x0013:
                org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f17200
                if (r1 == 0) goto L_0x0020
                org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f17200
                int r12 = r1.m21609((java.lang.String) r11, (int) r12)
                if (r12 < 0) goto L_0x0084
                r0 = 1
            L_0x0020:
                r2 = -1
                org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f17201
                if (r1 == 0) goto L_0x015d
                if (r0 != 0) goto L_0x015d
                org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f17201
                int r2 = r1.m21607(r11, r12)
                if (r2 < 0) goto L_0x0089
                r0 = 1
                r7 = r2
            L_0x0031:
                if (r0 != 0) goto L_0x003f
                org.joda.time.PeriodType r0 = r10.getPeriodType()
                int r1 = r9.f17202
                boolean r0 = r9.m21600((org.joda.time.PeriodType) r0, (int) r1)
                if (r0 == 0) goto L_0x0010
            L_0x003f:
                if (r7 <= 0) goto L_0x0090
                int r0 = r9.f17205
                int r1 = r7 - r12
                int r0 = java.lang.Math.min(r0, r1)
            L_0x0049:
                r1 = 0
                r4 = -1
                r3 = 0
                r6 = 0
                r5 = r0
                r2 = r12
            L_0x004f:
                if (r1 >= r5) goto L_0x015a
                int r0 = r2 + r1
                char r0 = r11.charAt(r0)
                if (r1 != 0) goto L_0x00b3
                r8 = 45
                if (r0 == r8) goto L_0x0061
                r8 = 43
                if (r0 != r8) goto L_0x00b3
            L_0x0061:
                boolean r8 = r9.f17204
                if (r8 != 0) goto L_0x00b3
                r6 = 45
                if (r0 != r6) goto L_0x009c
                r6 = 1
            L_0x006a:
                int r0 = r1 + 1
                if (r0 >= r5) goto L_0x015a
                int r0 = r2 + r1
                int r0 = r0 + 1
                char r0 = r11.charAt(r0)
                r8 = 48
                if (r0 < r8) goto L_0x015a
                r8 = 57
                if (r0 <= r8) goto L_0x009e
                r5 = r6
            L_0x007f:
                if (r3 != 0) goto L_0x00e9
                r12 = r2 ^ -1
                goto L_0x0010
            L_0x0084:
                if (r0 != 0) goto L_0x0010
                r12 = r12 ^ -1
                goto L_0x0010
            L_0x0089:
                if (r0 != 0) goto L_0x008e
                r12 = r2 ^ -1
                goto L_0x0010
            L_0x008e:
                r12 = r2
                goto L_0x0010
            L_0x0090:
                int r0 = r9.f17205
                int r1 = r11.length()
                int r1 = r1 - r12
                int r0 = java.lang.Math.min(r0, r1)
                goto L_0x0049
            L_0x009c:
                r6 = 0
                goto L_0x006a
            L_0x009e:
                if (r6 == 0) goto L_0x00af
                int r0 = r1 + 1
            L_0x00a2:
                int r1 = r5 + 1
                int r5 = r11.length()
                int r5 = r5 - r2
                int r5 = java.lang.Math.min(r1, r5)
                r1 = r0
                goto L_0x004f
            L_0x00af:
                int r2 = r2 + 1
                r0 = r1
                goto L_0x00a2
            L_0x00b3:
                r8 = 48
                if (r0 < r8) goto L_0x00c0
                r8 = 57
                if (r0 > r8) goto L_0x00c0
                r0 = 1
            L_0x00bc:
                int r1 = r1 + 1
                r3 = r0
                goto L_0x004f
            L_0x00c0:
                r8 = 46
                if (r0 == r8) goto L_0x00c8
                r8 = 44
                if (r0 != r8) goto L_0x015a
            L_0x00c8:
                int r0 = r9.f17202
                r8 = 8
                if (r0 == r8) goto L_0x00d4
                int r0 = r9.f17202
                r8 = 9
                if (r0 != r8) goto L_0x015a
            L_0x00d4:
                if (r4 < 0) goto L_0x00d8
                r5 = r6
                goto L_0x007f
            L_0x00d8:
                int r0 = r2 + r1
                int r4 = r0 + 1
                int r0 = r5 + 1
                int r5 = r11.length()
                int r5 = r5 - r2
                int r5 = java.lang.Math.min(r0, r5)
                r0 = r3
                goto L_0x00bc
            L_0x00e9:
                if (r7 < 0) goto L_0x00f2
                int r0 = r2 + r1
                if (r0 == r7) goto L_0x00f2
                r12 = r2
                goto L_0x0010
            L_0x00f2:
                int r0 = r9.f17202
                r3 = 8
                if (r0 == r3) goto L_0x0118
                int r0 = r9.f17202
                r3 = 9
                if (r0 == r3) goto L_0x0118
                int r0 = r9.f17202
                int r3 = r9.m21590((java.lang.String) r11, (int) r2, (int) r1)
                r9.m21598((org.joda.time.ReadWritablePeriod) r10, (int) r0, (int) r3)
            L_0x0107:
                int r0 = r2 + r1
                if (r0 < 0) goto L_0x0115
                org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f17201
                if (r1 == 0) goto L_0x0115
                org.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f17201
                int r0 = r1.m21609((java.lang.String) r11, (int) r0)
            L_0x0115:
                r12 = r0
                goto L_0x0010
            L_0x0118:
                if (r4 >= 0) goto L_0x0128
                r0 = 6
                int r3 = r9.m21590((java.lang.String) r11, (int) r2, (int) r1)
                r9.m21598((org.joda.time.ReadWritablePeriod) r10, (int) r0, (int) r3)
                r0 = 7
                r3 = 0
                r9.m21598((org.joda.time.ReadWritablePeriod) r10, (int) r0, (int) r3)
                goto L_0x0107
            L_0x0128:
                int r0 = r4 - r2
                int r0 = r0 + -1
                int r3 = r9.m21590((java.lang.String) r11, (int) r2, (int) r0)
                r0 = 6
                r9.m21598((org.joda.time.ReadWritablePeriod) r10, (int) r0, (int) r3)
                int r0 = r2 + r1
                int r0 = r0 - r4
                if (r0 > 0) goto L_0x013f
                r0 = 0
            L_0x013a:
                r3 = 7
                r9.m21598((org.joda.time.ReadWritablePeriod) r10, (int) r3, (int) r0)
                goto L_0x0107
            L_0x013f:
                r6 = 3
                if (r0 < r6) goto L_0x014d
                r0 = 3
                int r0 = r9.m21590((java.lang.String) r11, (int) r4, (int) r0)
            L_0x0147:
                if (r5 != 0) goto L_0x014b
                if (r3 >= 0) goto L_0x013a
            L_0x014b:
                int r0 = -r0
                goto L_0x013a
            L_0x014d:
                int r4 = r9.m21590((java.lang.String) r11, (int) r4, (int) r0)
                r6 = 1
                if (r0 != r6) goto L_0x0157
                int r0 = r4 * 100
                goto L_0x0147
            L_0x0157:
                int r0 = r4 * 10
                goto L_0x0147
            L_0x015a:
                r5 = r6
                goto L_0x007f
            L_0x015d:
                r7 = r2
                goto L_0x0031
            */
            throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.PeriodFormatterBuilder.FieldFormatter.m21593(org.joda.time.ReadWritablePeriod, java.lang.String, int, java.util.Locale):int");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m21590(String str, int i, int i2) {
            int i3;
            boolean z = false;
            if (i2 >= 10) {
                return Integer.parseInt(str.substring(i, i + i2));
            }
            if (i2 <= 0) {
                return 0;
            }
            int i4 = i + 1;
            char charAt = str.charAt(i);
            int i5 = i2 - 1;
            if (charAt == '-') {
                i5--;
                if (i5 < 0) {
                    return 0;
                }
                z = true;
                i3 = i4 + 1;
                charAt = str.charAt(i4);
            } else {
                i3 = i4;
            }
            int i6 = charAt - '0';
            int i7 = i3;
            while (true) {
                int i8 = i5 - 1;
                if (i5 <= 0) {
                    break;
                }
                i6 = (((i6 << 1) + (i6 << 3)) + str.charAt(i7)) - 48;
                i5 = i8;
                i7++;
            }
            return z ? -i6 : i6;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m21596(ReadablePeriod readablePeriod) {
            PeriodType periodType;
            long j;
            if (this.f17203 == 4) {
                periodType = null;
            } else {
                periodType = readablePeriod.getPeriodType();
            }
            if (periodType != null && !m21600(periodType, this.f17202)) {
                return Long.MAX_VALUE;
            }
            switch (this.f17202) {
                case 0:
                    j = (long) readablePeriod.get(DurationFieldType.years());
                    break;
                case 1:
                    j = (long) readablePeriod.get(DurationFieldType.months());
                    break;
                case 2:
                    j = (long) readablePeriod.get(DurationFieldType.weeks());
                    break;
                case 3:
                    j = (long) readablePeriod.get(DurationFieldType.days());
                    break;
                case 4:
                    j = (long) readablePeriod.get(DurationFieldType.hours());
                    break;
                case 5:
                    j = (long) readablePeriod.get(DurationFieldType.minutes());
                    break;
                case 6:
                    j = (long) readablePeriod.get(DurationFieldType.seconds());
                    break;
                case 7:
                    j = (long) readablePeriod.get(DurationFieldType.millis());
                    break;
                case 8:
                case 9:
                    j = ((long) readablePeriod.get(DurationFieldType.millis())) + (((long) readablePeriod.get(DurationFieldType.seconds())) * 1000);
                    break;
                default:
                    return Long.MAX_VALUE;
            }
            if (j == 0) {
                switch (this.f17203) {
                    case 1:
                        if (!m21591(readablePeriod) || this.f17199[this.f17202] != this) {
                            return Long.MAX_VALUE;
                        }
                        int min = Math.min(this.f17202, 8) - 1;
                        while (min >= 0 && min <= 9) {
                            if (m21600(periodType, min) && this.f17199[min] != null) {
                                return Long.MAX_VALUE;
                            }
                            min--;
                        }
                        break;
                    case 2:
                        if (m21591(readablePeriod) && this.f17199[this.f17202] == this) {
                            for (int i = this.f17202 + 1; i <= 9; i++) {
                                if (m21600(periodType, i) && this.f17199[i] != null) {
                                    return Long.MAX_VALUE;
                                }
                            }
                            break;
                        } else {
                            return Long.MAX_VALUE;
                        }
                        break;
                    case 5:
                        return Long.MAX_VALUE;
                }
            }
            return j;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m21591(ReadablePeriod readablePeriod) {
            int size = readablePeriod.size();
            for (int i = 0; i < size; i++) {
                if (readablePeriod.getValue(i) != 0) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m21600(PeriodType periodType, int i) {
            switch (i) {
                case 0:
                    return periodType.isSupported(DurationFieldType.years());
                case 1:
                    return periodType.isSupported(DurationFieldType.months());
                case 2:
                    return periodType.isSupported(DurationFieldType.weeks());
                case 3:
                    return periodType.isSupported(DurationFieldType.days());
                case 4:
                    return periodType.isSupported(DurationFieldType.hours());
                case 5:
                    return periodType.isSupported(DurationFieldType.minutes());
                case 6:
                    return periodType.isSupported(DurationFieldType.seconds());
                case 7:
                    return periodType.isSupported(DurationFieldType.millis());
                case 8:
                case 9:
                    return periodType.isSupported(DurationFieldType.seconds()) || periodType.isSupported(DurationFieldType.millis());
                default:
                    return false;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m21598(ReadWritablePeriod readWritablePeriod, int i, int i2) {
            switch (i) {
                case 0:
                    readWritablePeriod.setYears(i2);
                    return;
                case 1:
                    readWritablePeriod.setMonths(i2);
                    return;
                case 2:
                    readWritablePeriod.setWeeks(i2);
                    return;
                case 3:
                    readWritablePeriod.setDays(i2);
                    return;
                case 4:
                    readWritablePeriod.setHours(i2);
                    return;
                case 5:
                    readWritablePeriod.setMinutes(i2);
                    return;
                case 6:
                    readWritablePeriod.setSeconds(i2);
                    return;
                case 7:
                    readWritablePeriod.setMillis(i2);
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m21592() {
            return this.f17202;
        }
    }

    static class Literal implements PeriodParser, PeriodPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        static final Literal f17208 = new Literal("");

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f17209;

        Literal(String str) {
            this.f17209 = str;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21604(ReadablePeriod readablePeriod, int i, Locale locale) {
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21605(ReadablePeriod readablePeriod, Locale locale) {
            return this.f17209.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21606(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            stringBuffer.append(this.f17209);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21603(ReadWritablePeriod readWritablePeriod, String str, int i, Locale locale) {
            if (str.regionMatches(true, i, this.f17209, 0, this.f17209.length())) {
                return this.f17209.length() + i;
            }
            return i ^ -1;
        }
    }

    static class Separator implements PeriodParser, PeriodPrinter {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final PeriodPrinter f17210;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public volatile PeriodPrinter f17211;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final PeriodParser f17212;
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public volatile PeriodParser f17213;

        /* renamed from: 连任  reason: contains not printable characters */
        private final boolean f17214;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f17215;

        /* renamed from: 麤  reason: contains not printable characters */
        private final boolean f17216;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String[] f17217;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f17218;

        Separator(String str, String str2, String[] strArr, PeriodPrinter periodPrinter, PeriodParser periodParser, boolean z, boolean z2) {
            this.f17218 = str;
            this.f17215 = str2;
            if ((str2 == null || str.equals(str2)) && (strArr == null || strArr.length == 0)) {
                this.f17217 = new String[]{str};
            } else {
                TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
                treeSet.add(str);
                treeSet.add(str2);
                if (strArr != null) {
                    int length = strArr.length;
                    while (true) {
                        length--;
                        if (length < 0) {
                            break;
                        }
                        treeSet.add(strArr[length]);
                    }
                }
                ArrayList arrayList = new ArrayList(treeSet);
                Collections.reverse(arrayList);
                this.f17217 = (String[]) arrayList.toArray(new String[arrayList.size()]);
            }
            this.f17210 = periodPrinter;
            this.f17212 = periodParser;
            this.f17216 = z;
            this.f17214 = z2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21616(ReadablePeriod readablePeriod, int i, Locale locale) {
            int r0 = this.f17210.m21626(readablePeriod, i, locale);
            if (r0 < i) {
                return r0 + this.f17211.m21626(readablePeriod, i, locale);
            }
            return r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21617(ReadablePeriod readablePeriod, Locale locale) {
            int i;
            PeriodPrinter periodPrinter = this.f17210;
            PeriodPrinter periodPrinter2 = this.f17211;
            int r1 = periodPrinter.m21627(readablePeriod, locale) + periodPrinter2.m21627(readablePeriod, locale);
            if (this.f17216) {
                if (periodPrinter.m21626(readablePeriod, 1, locale) <= 0) {
                    return r1;
                }
                if (!this.f17214) {
                    return r1 + this.f17218.length();
                }
                int r0 = periodPrinter2.m21626(readablePeriod, 2, locale);
                if (r0 > 0) {
                    i = (r0 > 1 ? this.f17218 : this.f17215).length() + r1;
                } else {
                    i = r1;
                }
                return i;
            } else if (!this.f17214 || periodPrinter2.m21626(readablePeriod, 1, locale) <= 0) {
                return r1;
            } else {
                return r1 + this.f17218.length();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21619(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            PeriodPrinter periodPrinter = this.f17210;
            PeriodPrinter periodPrinter2 = this.f17211;
            periodPrinter.m21628(stringBuffer, readablePeriod, locale);
            if (this.f17216) {
                if (periodPrinter.m21626(readablePeriod, 1, locale) > 0) {
                    if (this.f17214) {
                        int r0 = periodPrinter2.m21626(readablePeriod, 2, locale);
                        if (r0 > 0) {
                            stringBuffer.append(r0 > 1 ? this.f17218 : this.f17215);
                        }
                    } else {
                        stringBuffer.append(this.f17218);
                    }
                }
            } else if (this.f17214 && periodPrinter2.m21626(readablePeriod, 1, locale) > 0) {
                stringBuffer.append(this.f17218);
            }
            periodPrinter2.m21628(stringBuffer, readablePeriod, locale);
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0035  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0040  */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m21615(org.joda.time.ReadWritablePeriod r11, java.lang.String r12, int r13, java.util.Locale r14) {
            /*
                r10 = this;
                r1 = 1
                r4 = 0
                org.joda.time.format.PeriodParser r0 = r10.f17212
                int r2 = r0.m21625(r11, r12, r13, r14)
                if (r2 >= 0) goto L_0x000b
            L_0x000a:
                return r2
            L_0x000b:
                r6 = -1
                if (r2 <= r13) goto L_0x0056
                java.lang.String[] r8 = r10.f17217
                int r9 = r8.length
                r7 = r4
            L_0x0012:
                if (r7 >= r9) goto L_0x0056
                r3 = r8[r7]
                if (r3 == 0) goto L_0x0029
                int r0 = r3.length()
                if (r0 == 0) goto L_0x0029
                int r5 = r3.length()
                r0 = r12
                boolean r0 = r0.regionMatches(r1, r2, r3, r4, r5)
                if (r0 == 0) goto L_0x003c
            L_0x0029:
                if (r3 != 0) goto L_0x0037
            L_0x002b:
                int r2 = r2 + r4
                r0 = r4
            L_0x002d:
                org.joda.time.format.PeriodParser r3 = r10.f17213
                int r3 = r3.m21625(r11, r12, r2, r14)
                if (r3 >= 0) goto L_0x0040
                r2 = r3
                goto L_0x000a
            L_0x0037:
                int r4 = r3.length()
                goto L_0x002b
            L_0x003c:
                int r0 = r7 + 1
                r7 = r0
                goto L_0x0012
            L_0x0040:
                if (r1 == 0) goto L_0x0049
                if (r3 != r2) goto L_0x0049
                if (r0 <= 0) goto L_0x0049
                r2 = r2 ^ -1
                goto L_0x000a
            L_0x0049:
                if (r3 <= r2) goto L_0x0054
                if (r1 != 0) goto L_0x0054
                boolean r0 = r10.f17216
                if (r0 != 0) goto L_0x0054
                r2 = r2 ^ -1
                goto L_0x000a
            L_0x0054:
                r2 = r3
                goto L_0x000a
            L_0x0056:
                r0 = r6
                r1 = r4
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.PeriodFormatterBuilder.Separator.m21615(org.joda.time.ReadWritablePeriod, java.lang.String, int, java.util.Locale):int");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Separator m21618(PeriodPrinter periodPrinter, PeriodParser periodParser) {
            this.f17211 = periodPrinter;
            this.f17213 = periodParser;
            return this;
        }
    }

    static class Composite implements PeriodParser, PeriodPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final PeriodParser[] f17194;

        /* renamed from: 龘  reason: contains not printable characters */
        private final PeriodPrinter[] f17195;

        Composite(List<Object> list) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            m21579(list, (List<Object>) arrayList, (List<Object>) arrayList2);
            if (arrayList.size() <= 0) {
                this.f17195 = null;
            } else {
                this.f17195 = (PeriodPrinter[]) arrayList.toArray(new PeriodPrinter[arrayList.size()]);
            }
            if (arrayList2.size() <= 0) {
                this.f17194 = null;
            } else {
                this.f17194 = (PeriodParser[]) arrayList2.toArray(new PeriodParser[arrayList2.size()]);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21582(ReadablePeriod readablePeriod, int i, Locale locale) {
            int i2 = 0;
            PeriodPrinter[] periodPrinterArr = this.f17195;
            int length = periodPrinterArr.length;
            while (i2 < i) {
                length--;
                if (length < 0) {
                    break;
                }
                i2 += periodPrinterArr[length].m21626(readablePeriod, (int) MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, locale);
            }
            return i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21583(ReadablePeriod readablePeriod, Locale locale) {
            int i = 0;
            PeriodPrinter[] periodPrinterArr = this.f17195;
            int length = periodPrinterArr.length;
            while (true) {
                length--;
                if (length < 0) {
                    return i;
                }
                i += periodPrinterArr[length].m21627(readablePeriod, locale);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21584(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            for (PeriodPrinter r3 : this.f17195) {
                r3.m21628(stringBuffer, readablePeriod, locale);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21581(ReadWritablePeriod readWritablePeriod, String str, int i, Locale locale) {
            PeriodParser[] periodParserArr = this.f17194;
            if (periodParserArr == null) {
                throw new UnsupportedOperationException();
            }
            int length = periodParserArr.length;
            for (int i2 = 0; i2 < length && i >= 0; i2++) {
                i = periodParserArr[i2].m21625(readWritablePeriod, str, i, locale);
            }
            return i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m21579(List<Object> list, List<Object> list2, List<Object> list3) {
            int size = list.size();
            for (int i = 0; i < size; i += 2) {
                Object obj = list.get(i);
                if (obj instanceof PeriodPrinter) {
                    if (obj instanceof Composite) {
                        m21580(list2, (Object[]) ((Composite) obj).f17195);
                    } else {
                        list2.add(obj);
                    }
                }
                Object obj2 = list.get(i + 1);
                if (obj2 instanceof PeriodParser) {
                    if (obj2 instanceof Composite) {
                        m21580(list3, (Object[]) ((Composite) obj2).f17194);
                    } else {
                        list3.add(obj2);
                    }
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m21580(List<Object> list, Object[] objArr) {
            if (objArr != null) {
                for (Object add : objArr) {
                    list.add(add);
                }
            }
        }
    }
}
