package org.joda.time.format;

import java.io.IOException;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

interface InternalPrinter {
    /* renamed from: 龘  reason: contains not printable characters */
    int m21539();

    /* renamed from: 龘  reason: contains not printable characters */
    void m21540(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m21541(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException;
}
