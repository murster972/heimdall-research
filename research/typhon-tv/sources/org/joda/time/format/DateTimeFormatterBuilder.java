package org.joda.time.format;

import com.google.android.exoplayer2.C;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.MutableDateTime;
import org.joda.time.ReadablePartial;
import org.joda.time.field.MillisDurationField;
import org.joda.time.field.PreciseDateTimeField;

public class DateTimeFormatterBuilder {

    /* renamed from: 靐  reason: contains not printable characters */
    private Object f17056;

    /* renamed from: 龘  reason: contains not printable characters */
    private ArrayList<Object> f17057 = new ArrayList<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m21298() {
        InternalPrinter internalPrinter;
        InternalParser internalParser;
        Object r1 = m21255();
        if (m21257(r1)) {
            internalPrinter = (InternalPrinter) r1;
        } else {
            internalPrinter = null;
        }
        if (m21260(r1)) {
            internalParser = (InternalParser) r1;
        } else {
            internalParser = null;
        }
        if (internalPrinter != null || internalParser != null) {
            return new DateTimeFormatter(internalPrinter, internalParser);
        }
        throw new UnsupportedOperationException("Both printing and parsing not supported");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeParser m21290() {
        Object r0 = m21255();
        if (m21260(r0)) {
            return InternalParserDateTimeParser.m21535((InternalParser) r0);
        }
        throw new UnsupportedOperationException("Parsing is not supported");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m21297() {
        return m21258(m21255());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21310(DateTimeFormatter dateTimeFormatter) {
        if (dateTimeFormatter != null) {
            return m21262(dateTimeFormatter.m21250(), dateTimeFormatter.m21243());
        }
        throw new IllegalArgumentException("No formatter supplied");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21311(DateTimeParser dateTimeParser) {
        m21259(dateTimeParser);
        return m21262((InternalPrinter) null, DateTimeParserInternalParser.m21415(dateTimeParser));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21312(DateTimePrinter dateTimePrinter, DateTimeParser[] dateTimeParserArr) {
        int i = 0;
        if (dateTimePrinter != null) {
            m21264(dateTimePrinter);
        }
        if (dateTimeParserArr == null) {
            throw new IllegalArgumentException("No parsers supplied");
        }
        int length = dateTimeParserArr.length;
        if (length != 1) {
            InternalParser[] internalParserArr = new InternalParser[length];
            while (i < length - 1) {
                InternalParser r3 = DateTimeParserInternalParser.m21415(dateTimeParserArr[i]);
                internalParserArr[i] = r3;
                if (r3 == null) {
                    throw new IllegalArgumentException("Incomplete parser array");
                }
                i++;
            }
            internalParserArr[i] = DateTimeParserInternalParser.m21415(dateTimeParserArr[i]);
            return m21262(DateTimePrinterInternalPrinter.m21424(dateTimePrinter), (InternalParser) new MatchingParser(internalParserArr));
        } else if (dateTimeParserArr[0] != null) {
            return m21262(DateTimePrinterInternalPrinter.m21424(dateTimePrinter), DateTimeParserInternalParser.m21415(dateTimeParserArr[0]));
        } else {
            throw new IllegalArgumentException("No parser supplied");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21289(DateTimeParser dateTimeParser) {
        m21259(dateTimeParser);
        return m21262((InternalPrinter) null, (InternalParser) new MatchingParser(new InternalParser[]{DateTimeParserInternalParser.m21415(dateTimeParser), null}));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m21259(DateTimeParser dateTimeParser) {
        if (dateTimeParser == null) {
            throw new IllegalArgumentException("No parser supplied");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m21264(DateTimePrinter dateTimePrinter) {
        if (dateTimePrinter == null) {
            throw new IllegalArgumentException("No printer supplied");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeFormatterBuilder m21261(Object obj) {
        this.f17056 = null;
        this.f17057.add(obj);
        this.f17057.add(obj);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeFormatterBuilder m21262(InternalPrinter internalPrinter, InternalParser internalParser) {
        this.f17056 = null;
        this.f17057.add(internalPrinter);
        this.f17057.add(internalParser);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21299(char c) {
        return m21261((Object) new CharacterLiteral(c));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21303(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Literal must not be null");
        }
        switch (str.length()) {
            case 0:
                return this;
            case 1:
                return m21261((Object) new CharacterLiteral(str.charAt(0)));
            default:
                return m21261((Object) new StringLiteral(str));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21309(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i2 = i;
        }
        if (i < 0 || i2 <= 0) {
            throw new IllegalArgumentException();
        } else if (i <= 1) {
            return m21261((Object) new UnpaddedNumber(dateTimeFieldType, i2, false));
        } else {
            return m21261((Object) new PaddedNumber(dateTimeFieldType, i2, false, i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21308(DateTimeFieldType dateTimeFieldType, int i) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        } else if (i > 0) {
            return m21261((Object) new FixedNumber(dateTimeFieldType, i, false));
        } else {
            throw new IllegalArgumentException("Illegal number of digits: " + i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21288(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i2 = i;
        }
        if (i < 0 || i2 <= 0) {
            throw new IllegalArgumentException();
        } else if (i <= 1) {
            return m21261((Object) new UnpaddedNumber(dateTimeFieldType, i2, true));
        } else {
            return m21261((Object) new PaddedNumber(dateTimeFieldType, i2, true, i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21307(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return m21261((Object) new TextField(dateTimeFieldType, false));
        }
        throw new IllegalArgumentException("Field type must not be null");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21287(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return m21261((Object) new TextField(dateTimeFieldType, true));
        }
        throw new IllegalArgumentException("Field type must not be null");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21296(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i2 = i;
        }
        if (i >= 0 && i2 > 0) {
            return m21261((Object) new Fraction(dateTimeFieldType, i, i2));
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21301(int i, int i2) {
        return m21296(DateTimeFieldType.secondOfDay(), i, i2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21285(int i, int i2) {
        return m21296(DateTimeFieldType.minuteOfDay(), i, i2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21295(int i, int i2) {
        return m21296(DateTimeFieldType.hourOfDay(), i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21300(int i) {
        return m21309(DateTimeFieldType.millisOfSecond(), i, 3);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21284(int i) {
        return m21309(DateTimeFieldType.secondOfMinute(), i, 2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21294(int i) {
        return m21309(DateTimeFieldType.minuteOfHour(), i, 2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21292(int i) {
        return m21309(DateTimeFieldType.hourOfDay(), i, 2);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21282(int i) {
        return m21309(DateTimeFieldType.clockhourOfDay(), i, 2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21267(int i) {
        return m21309(DateTimeFieldType.hourOfHalfday(), i, 2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21270(int i) {
        return m21309(DateTimeFieldType.clockhourOfHalfday(), i, 2);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21273(int i) {
        return m21309(DateTimeFieldType.dayOfWeek(), i, 1);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21276(int i) {
        return m21309(DateTimeFieldType.dayOfMonth(), i, 2);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21278(int i) {
        return m21309(DateTimeFieldType.dayOfYear(), i, 3);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21280(int i) {
        return m21309(DateTimeFieldType.weekOfWeekyear(), i, 2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21293(int i, int i2) {
        return m21288(DateTimeFieldType.weekyear(), i, i2);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21274(int i) {
        return m21309(DateTimeFieldType.monthOfYear(), i, 2);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21283(int i, int i2) {
        return m21288(DateTimeFieldType.year(), i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21302(int i, boolean z) {
        return m21261((Object) new TwoDigitYear(DateTimeFieldType.year(), i, z));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21286(int i, boolean z) {
        return m21261((Object) new TwoDigitYear(DateTimeFieldType.weekyear(), i, z));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21268(int i, int i2) {
        return m21309(DateTimeFieldType.yearOfEra(), i, i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21271(int i, int i2) {
        return m21288(DateTimeFieldType.centuryOfEra(), i, i2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21291() {
        return m21307(DateTimeFieldType.halfdayOfDay());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21281() {
        return m21307(DateTimeFieldType.dayOfWeek());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21266() {
        return m21287(DateTimeFieldType.dayOfWeek());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21269() {
        return m21307(DateTimeFieldType.monthOfYear());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21272() {
        return m21287(DateTimeFieldType.monthOfYear());
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21275() {
        return m21307(DateTimeFieldType.era());
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21277() {
        return m21262((InternalPrinter) new TimeZoneName(0, (Map<String, DateTimeZone>) null), (InternalParser) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21306(Map<String, DateTimeZone> map) {
        TimeZoneName timeZoneName = new TimeZoneName(1, map);
        return m21262((InternalPrinter) timeZoneName, (InternalParser) timeZoneName);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21279() {
        return m21262((InternalPrinter) TimeZoneId.INSTANCE, (InternalParser) TimeZoneId.INSTANCE);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21305(String str, boolean z, int i, int i2) {
        return m21261((Object) new TimeZoneOffset(str, str, z, i, i2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m21304(String str, String str2, boolean z, int i, int i2) {
        return m21261((Object) new TimeZoneOffset(str, str2, z, i, i2));
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private Object m21255() {
        Object obj = this.f17056;
        if (obj == null) {
            if (this.f17057.size() == 2) {
                Object obj2 = this.f17057.get(0);
                Object obj3 = this.f17057.get(1);
                if (obj2 == null) {
                    obj = obj3;
                } else if (obj2 == obj3 || obj3 == null) {
                    obj = obj2;
                }
            }
            if (obj == null) {
                obj = new Composite(this.f17057);
            }
            this.f17056 = obj;
        }
        return obj;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m21257(Object obj) {
        if (!(obj instanceof InternalPrinter)) {
            return false;
        }
        if (obj instanceof Composite) {
            return ((Composite) obj).m21322();
        }
        return true;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m21260(Object obj) {
        if (!(obj instanceof InternalParser)) {
            return false;
        }
        if (obj instanceof Composite) {
            return ((Composite) obj).m21321();
        }
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m21258(Object obj) {
        return m21257(obj) || m21260(obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m21263(Appendable appendable, int i) throws IOException {
        while (true) {
            i--;
            if (i >= 0) {
                appendable.append(65533);
            } else {
                return;
            }
        }
    }

    static class CharacterLiteral implements InternalParser, InternalPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        private final char f17058;

        CharacterLiteral(char c) {
            this.f17058 = c;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21314() {
            return 1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21316(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(this.f17058);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21317(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            appendable.append(this.f17058);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21313() {
            return 1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21315(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            char upperCase;
            char upperCase2;
            if (i >= charSequence.length()) {
                return i ^ -1;
            }
            char charAt = charSequence.charAt(i);
            char c = this.f17058;
            if (charAt == c || (upperCase = Character.toUpperCase(charAt)) == (upperCase2 = Character.toUpperCase(c)) || Character.toLowerCase(upperCase) == Character.toLowerCase(upperCase2)) {
                return i + 1;
            }
            return i ^ -1;
        }
    }

    static class StringLiteral implements InternalParser, InternalPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f17072;

        StringLiteral(String str) {
            this.f17072 = str;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21343() {
            return this.f17072.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21345(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(this.f17072);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21346(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            appendable.append(this.f17072);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21342() {
            return this.f17072.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21344(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            if (DateTimeFormatterBuilder.m21256(charSequence, i, this.f17072)) {
                return this.f17072.length() + i;
            }
            return i ^ -1;
        }
    }

    static abstract class NumberFormatter implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        protected final int f17068;

        /* renamed from: 齉  reason: contains not printable characters */
        protected final boolean f17069;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final DateTimeFieldType f17070;

        NumberFormatter(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            this.f17070 = dateTimeFieldType;
            this.f17068 = i;
            this.f17069 = z;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21337() {
            return this.f17068;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:49:0x00bd, code lost:
            r4 = r1;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m21338(org.joda.time.format.DateTimeParserBucket r12, java.lang.CharSequence r13, int r14) {
            /*
                r11 = this;
                r10 = 48
                r9 = 45
                r8 = 43
                r2 = 1
                r3 = 0
                int r0 = r11.f17068
                int r1 = r13.length()
                int r1 = r1 - r14
                int r4 = java.lang.Math.min(r0, r1)
                r5 = r3
                r0 = r3
                r1 = r3
                r6 = r4
            L_0x0017:
                if (r5 >= r6) goto L_0x00bd
                int r4 = r14 + r5
                char r4 = r13.charAt(r4)
                if (r5 != 0) goto L_0x005a
                if (r4 == r9) goto L_0x0025
                if (r4 != r8) goto L_0x005a
            L_0x0025:
                boolean r7 = r11.f17069
                if (r7 == 0) goto L_0x005a
                if (r4 != r9) goto L_0x0047
                r1 = r2
            L_0x002c:
                if (r4 != r8) goto L_0x0049
                r0 = r2
            L_0x002f:
                int r4 = r5 + 1
                if (r4 >= r6) goto L_0x00bd
                int r4 = r14 + r5
                int r4 = r4 + 1
                char r4 = r13.charAt(r4)
                if (r4 < r10) goto L_0x00bd
                r7 = 57
                if (r4 <= r7) goto L_0x004b
                r4 = r1
            L_0x0042:
                if (r5 != 0) goto L_0x0066
                r1 = r14 ^ -1
            L_0x0046:
                return r1
            L_0x0047:
                r1 = r3
                goto L_0x002c
            L_0x0049:
                r0 = r3
                goto L_0x002f
            L_0x004b:
                int r4 = r5 + 1
                int r5 = r6 + 1
                int r6 = r13.length()
                int r6 = r6 - r14
                int r6 = java.lang.Math.min(r5, r6)
                r5 = r4
                goto L_0x0017
            L_0x005a:
                if (r4 < r10) goto L_0x00bd
                r7 = 57
                if (r4 <= r7) goto L_0x0062
                r4 = r1
                goto L_0x0042
            L_0x0062:
                int r4 = r5 + 1
                r5 = r4
                goto L_0x0017
            L_0x0066:
                r1 = 9
                if (r5 < r1) goto L_0x0091
                if (r0 == 0) goto L_0x0082
                int r0 = r14 + 1
                int r1 = r14 + r5
                java.lang.CharSequence r0 = r13.subSequence(r0, r1)
                java.lang.String r0 = r0.toString()
                int r0 = java.lang.Integer.parseInt(r0)
            L_0x007c:
                org.joda.time.DateTimeFieldType r2 = r11.f17070
                r12.m21406((org.joda.time.DateTimeFieldType) r2, (int) r0)
                goto L_0x0046
            L_0x0082:
                int r1 = r14 + r5
                java.lang.CharSequence r0 = r13.subSequence(r14, r1)
                java.lang.String r0 = r0.toString()
                int r0 = java.lang.Integer.parseInt(r0)
                goto L_0x007c
            L_0x0091:
                if (r4 != 0) goto L_0x0095
                if (r0 == 0) goto L_0x00bb
            L_0x0095:
                int r0 = r14 + 1
            L_0x0097:
                int r2 = r0 + 1
                char r0 = r13.charAt(r0)     // Catch:{ StringIndexOutOfBoundsException -> 0x00b3 }
                int r0 = r0 + -48
                int r1 = r14 + r5
            L_0x00a1:
                if (r2 >= r1) goto L_0x00b7
                int r3 = r0 << 3
                int r0 = r0 << 1
                int r0 = r0 + r3
                int r3 = r2 + 1
                char r2 = r13.charAt(r2)
                int r0 = r0 + r2
                int r0 = r0 + -48
                r2 = r3
                goto L_0x00a1
            L_0x00b3:
                r0 = move-exception
                r1 = r14 ^ -1
                goto L_0x0046
            L_0x00b7:
                if (r4 == 0) goto L_0x007c
                int r0 = -r0
                goto L_0x007c
            L_0x00bb:
                r0 = r14
                goto L_0x0097
            L_0x00bd:
                r4 = r1
                goto L_0x0042
            */
            throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.DateTimeFormatterBuilder.NumberFormatter.m21338(org.joda.time.format.DateTimeParserBucket, java.lang.CharSequence, int):int");
        }
    }

    static class UnpaddedNumber extends NumberFormatter {
        protected UnpaddedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            super(dateTimeFieldType, i, z);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21378() {
            return this.f17068;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21379(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                FormatUtils.m21431(appendable, this.f17070.getField(chronology).get(j));
            } catch (RuntimeException e) {
                appendable.append(65533);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21380(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            if (readablePartial.isSupported(this.f17070)) {
                try {
                    FormatUtils.m21431(appendable, readablePartial.get(this.f17070));
                } catch (RuntimeException e) {
                    appendable.append(65533);
                }
            } else {
                appendable.append(65533);
            }
        }
    }

    static class PaddedNumber extends NumberFormatter {

        /* renamed from: 麤  reason: contains not printable characters */
        protected final int f17071;

        protected PaddedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z, int i2) {
            super(dateTimeFieldType, i, z);
            this.f17071 = i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21339() {
            return this.f17068;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21340(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                FormatUtils.m21432(appendable, this.f17070.getField(chronology).get(j), this.f17071);
            } catch (RuntimeException e) {
                DateTimeFormatterBuilder.m21263(appendable, this.f17071);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21341(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            if (readablePartial.isSupported(this.f17070)) {
                try {
                    FormatUtils.m21432(appendable, readablePartial.get(this.f17070), this.f17071);
                } catch (RuntimeException e) {
                    DateTimeFormatterBuilder.m21263(appendable, this.f17071);
                }
            } else {
                DateTimeFormatterBuilder.m21263(appendable, this.f17071);
            }
        }
    }

    static class FixedNumber extends PaddedNumber {
        protected FixedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            super(dateTimeFieldType, i, z, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21327(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            int i2;
            char charAt;
            int r0 = super.m21338(dateTimeParserBucket, charSequence, i);
            if (r0 < 0 || r0 == (i2 = this.f17068 + i)) {
                return r0;
            }
            if (this.f17069 && ((charAt = charSequence.charAt(i)) == '-' || charAt == '+')) {
                i2++;
            }
            if (r0 > i2) {
                return (i2 + 1) ^ -1;
            }
            if (r0 < i2) {
                return r0 ^ -1;
            }
            return r0;
        }
    }

    static class TwoDigitYear implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f17090;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f17091;

        /* renamed from: 龘  reason: contains not printable characters */
        private final DateTimeFieldType f17092;

        TwoDigitYear(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            this.f17092 = dateTimeFieldType;
            this.f17090 = i;
            this.f17091 = z;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21373() {
            return this.f17091 ? 4 : 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21375(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            int i2;
            int i3;
            int charAt;
            int i4;
            boolean z;
            int i5 = 0;
            int length = charSequence.length() - i;
            if (this.f17091) {
                int i6 = 0;
                boolean z2 = false;
                boolean z3 = false;
                int i7 = length;
                while (i6 < i7) {
                    char charAt2 = charSequence.charAt(i + i6);
                    if (i6 != 0 || (charAt2 != '-' && charAt2 != '+')) {
                        if (charAt2 < '0' || charAt2 > '9') {
                            break;
                        }
                        i6++;
                    } else {
                        if (charAt2 == '-') {
                            z = true;
                        } else {
                            z = false;
                        }
                        if (z) {
                            i6++;
                            z2 = z;
                            z3 = true;
                        } else {
                            i++;
                            i7--;
                            z2 = z;
                            z3 = true;
                        }
                    }
                }
                if (i6 == 0) {
                    return i ^ -1;
                }
                if (z3 || i6 != 2) {
                    if (i6 >= 9) {
                        i4 = i + i6;
                        charAt = Integer.parseInt(charSequence.subSequence(i, i4).toString());
                    } else {
                        if (z2) {
                            i3 = i + 1;
                        } else {
                            i3 = i;
                        }
                        try {
                            charAt = charSequence.charAt(i3) - '0';
                            i4 = i + i6;
                            for (int i8 = i3 + 1; i8 < i4; i8++) {
                                charAt = (((charAt << 1) + (charAt << 3)) + charSequence.charAt(i8)) - 48;
                            }
                            if (z2) {
                                charAt = -charAt;
                            }
                        } catch (StringIndexOutOfBoundsException e) {
                            return i ^ -1;
                        }
                    }
                    dateTimeParserBucket.m21406(this.f17092, charAt);
                    return i4;
                }
            } else if (Math.min(2, length) < 2) {
                return i ^ -1;
            }
            char charAt3 = charSequence.charAt(i);
            if (charAt3 < '0' || charAt3 > '9') {
                return i ^ -1;
            }
            int i9 = charAt3 - '0';
            char charAt4 = charSequence.charAt(i + 1);
            if (charAt4 < '0' || charAt4 > '9') {
                return i ^ -1;
            }
            int i10 = (((i9 << 1) + (i9 << 3)) + charAt4) - 48;
            int i11 = this.f17090;
            if (dateTimeParserBucket.m21396() != null) {
                i11 = dateTimeParserBucket.m21396().intValue();
            }
            int i12 = i11 - 50;
            if (i12 >= 0) {
                i2 = i12 % 100;
            } else {
                i2 = ((i12 + 1) % 100) + 99;
            }
            if (i10 < i2) {
                i5 = 100;
            }
            dateTimeParserBucket.m21406(this.f17092, ((i5 + i12) - i2) + i10);
            return i + 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21374() {
            return 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21376(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            int r0 = m21371(j, chronology);
            if (r0 < 0) {
                appendable.append(65533);
                appendable.append(65533);
                return;
            }
            FormatUtils.m21432(appendable, r0, 2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m21371(long j, Chronology chronology) {
            try {
                int i = this.f17092.getField(chronology).get(j);
                if (i < 0) {
                    i = -i;
                }
                return i % 100;
            } catch (RuntimeException e) {
                return -1;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21377(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            int r0 = m21372(readablePartial);
            if (r0 < 0) {
                appendable.append(65533);
                appendable.append(65533);
                return;
            }
            FormatUtils.m21432(appendable, r0, 2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m21372(ReadablePartial readablePartial) {
            if (readablePartial.isSupported(this.f17092)) {
                try {
                    int i = readablePartial.get(this.f17092);
                    if (i < 0) {
                        i = -i;
                    }
                    return i % 100;
                } catch (RuntimeException e) {
                }
            }
            return -1;
        }
    }

    static class TextField implements InternalParser, InternalPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        private static Map<Locale, Map<DateTimeFieldType, Object[]>> f17073 = new ConcurrentHashMap();

        /* renamed from: 靐  reason: contains not printable characters */
        private final DateTimeFieldType f17074;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f17075;

        TextField(DateTimeFieldType dateTimeFieldType, boolean z) {
            this.f17074 = dateTimeFieldType;
            this.f17075 = z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21350() {
            return this.f17075 ? 6 : 20;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21352(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                appendable.append(m21347(j, chronology, locale));
            } catch (RuntimeException e) {
                appendable.append(65533);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21353(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            try {
                appendable.append(m21348(readablePartial, locale));
            } catch (RuntimeException e) {
                appendable.append(65533);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m21347(long j, Chronology chronology, Locale locale) {
            DateTimeField field = this.f17074.getField(chronology);
            if (this.f17075) {
                return field.getAsShortText(j, locale);
            }
            return field.getAsText(j, locale);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m21348(ReadablePartial readablePartial, Locale locale) {
            if (!readablePartial.isSupported(this.f17074)) {
                return "�";
            }
            DateTimeField field = this.f17074.getField(readablePartial.getChronology());
            if (this.f17075) {
                return field.getAsShortText(readablePartial, locale);
            }
            return field.getAsText(readablePartial, locale);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21349() {
            return m21350();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21351(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            ConcurrentHashMap concurrentHashMap;
            Map map;
            int intValue;
            Locale r4 = dateTimeParserBucket.m21397();
            Map map2 = f17073.get(r4);
            if (map2 == null) {
                ConcurrentHashMap concurrentHashMap2 = new ConcurrentHashMap();
                f17073.put(r4, concurrentHashMap2);
                concurrentHashMap = concurrentHashMap2;
            } else {
                concurrentHashMap = map2;
            }
            Object[] objArr = (Object[]) concurrentHashMap.get(this.f17074);
            if (objArr == null) {
                ConcurrentHashMap concurrentHashMap3 = new ConcurrentHashMap(32);
                MutableDateTime.Property property = new MutableDateTime(0, DateTimeZone.UTC).property(this.f17074);
                int minimumValueOverall = property.getMinimumValueOverall();
                int maximumValueOverall = property.getMaximumValueOverall();
                if (maximumValueOverall - minimumValueOverall > 32) {
                    return i ^ -1;
                }
                intValue = property.getMaximumTextLength(r4);
                while (minimumValueOverall <= maximumValueOverall) {
                    property.set(minimumValueOverall);
                    concurrentHashMap3.put(property.getAsShortText(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsShortText(r4).toLowerCase(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsShortText(r4).toUpperCase(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsText(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsText(r4).toLowerCase(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsText(r4).toUpperCase(r4), Boolean.TRUE);
                    minimumValueOverall++;
                }
                if ("en".equals(r4.getLanguage()) && this.f17074 == DateTimeFieldType.era()) {
                    concurrentHashMap3.put("BCE", Boolean.TRUE);
                    concurrentHashMap3.put("bce", Boolean.TRUE);
                    concurrentHashMap3.put("CE", Boolean.TRUE);
                    concurrentHashMap3.put("ce", Boolean.TRUE);
                    intValue = 3;
                }
                concurrentHashMap.put(this.f17074, new Object[]{concurrentHashMap3, Integer.valueOf(intValue)});
                map = concurrentHashMap3;
            } else {
                map = (Map) objArr[0];
                intValue = ((Integer) objArr[1]).intValue();
            }
            for (int min = Math.min(charSequence.length(), intValue + i); min > i; min--) {
                String obj = charSequence.subSequence(i, min).toString();
                if (map.containsKey(obj)) {
                    dateTimeParserBucket.m21407(this.f17074, obj, r4);
                    return min;
                }
            }
            return i ^ -1;
        }
    }

    static class Fraction implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        protected int f17063;

        /* renamed from: 齉  reason: contains not printable characters */
        private final DateTimeFieldType f17064;

        /* renamed from: 龘  reason: contains not printable characters */
        protected int f17065;

        protected Fraction(DateTimeFieldType dateTimeFieldType, int i, int i2) {
            this.f17064 = dateTimeFieldType;
            i2 = i2 > 18 ? 18 : i2;
            this.f17065 = i;
            this.f17063 = i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21330() {
            return this.f17063;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21333(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            m21332(appendable, j, chronology);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21334(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            m21332(appendable, readablePartial.getChronology().set(readablePartial, 0), readablePartial.getChronology());
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m21332(Appendable appendable, long j, Chronology chronology) throws IOException {
            String l;
            DateTimeField field = this.f17064.getField(chronology);
            int i = this.f17065;
            try {
                long remainder = field.remainder(j);
                if (remainder == 0) {
                    while (true) {
                        i--;
                        if (i >= 0) {
                            appendable.append('0');
                        } else {
                            return;
                        }
                    }
                } else {
                    long[] r0 = m21328(remainder, field);
                    long j2 = r0[0];
                    int i2 = (int) r0[1];
                    if ((2147483647L & j2) == j2) {
                        l = Integer.toString((int) j2);
                    } else {
                        l = Long.toString(j2);
                    }
                    int length = l.length();
                    while (length < i2) {
                        appendable.append('0');
                        i--;
                        i2--;
                    }
                    if (i < i2) {
                        while (i < i2 && length > 1 && l.charAt(length - 1) == '0') {
                            i2--;
                            length--;
                        }
                        if (length < l.length()) {
                            for (int i3 = 0; i3 < length; i3++) {
                                appendable.append(l.charAt(i3));
                            }
                            return;
                        }
                    }
                    appendable.append(l);
                }
            } catch (RuntimeException e) {
                DateTimeFormatterBuilder.m21263(appendable, i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private long[] m21328(long j, DateTimeField dateTimeField) {
            int i;
            long j2;
            long unitMillis = dateTimeField.getDurationField().getUnitMillis();
            int i2 = this.f17063;
            while (true) {
                switch (i) {
                    case 1:
                        j2 = 10;
                        break;
                    case 2:
                        j2 = 100;
                        break;
                    case 3:
                        j2 = 1000;
                        break;
                    case 4:
                        j2 = 10000;
                        break;
                    case 5:
                        j2 = 100000;
                        break;
                    case 6:
                        j2 = C.MICROS_PER_SECOND;
                        break;
                    case 7:
                        j2 = 10000000;
                        break;
                    case 8:
                        j2 = 100000000;
                        break;
                    case 9:
                        j2 = C.NANOS_PER_SECOND;
                        break;
                    case 10:
                        j2 = 10000000000L;
                        break;
                    case 11:
                        j2 = 100000000000L;
                        break;
                    case 12:
                        j2 = 1000000000000L;
                        break;
                    case 13:
                        j2 = 10000000000000L;
                        break;
                    case 14:
                        j2 = 100000000000000L;
                        break;
                    case 15:
                        j2 = 1000000000000000L;
                        break;
                    case 16:
                        j2 = 10000000000000000L;
                        break;
                    case 17:
                        j2 = 100000000000000000L;
                        break;
                    case 18:
                        j2 = 1000000000000000000L;
                        break;
                    default:
                        j2 = 1;
                        break;
                }
                if ((unitMillis * j2) / j2 == unitMillis) {
                    return new long[]{(j2 * j) / unitMillis, (long) i};
                }
                i2 = i - 1;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21329() {
            return this.f17063;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21331(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            DateTimeField field = this.f17064.getField(dateTimeParserBucket.m21403());
            int min = Math.min(this.f17063, charSequence.length() - i);
            long j = 0;
            long unitMillis = field.getDurationField().getUnitMillis() * 10;
            int i2 = 0;
            while (i2 < min) {
                char charAt = charSequence.charAt(i + i2);
                if (charAt < '0' || charAt > '9') {
                    break;
                }
                i2++;
                unitMillis /= 10;
                j += ((long) (charAt - '0')) * unitMillis;
            }
            long j2 = j / 10;
            if (i2 == 0) {
                return i ^ -1;
            }
            if (j2 > 2147483647L) {
                return i ^ -1;
            }
            dateTimeParserBucket.m21405((DateTimeField) new PreciseDateTimeField(DateTimeFieldType.millisOfSecond(), MillisDurationField.INSTANCE, field.getDurationField()), (int) j2);
            return i2 + i;
        }
    }

    static class TimeZoneOffset implements InternalParser, InternalPrinter {

        /* renamed from: 连任  reason: contains not printable characters */
        private final int f17085;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f17086;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f17087;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f17088;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f17089;

        TimeZoneOffset(String str, String str2, boolean z, int i, int i2) {
            this.f17089 = str;
            this.f17086 = str2;
            this.f17088 = z;
            if (i <= 0 || i2 < i) {
                throw new IllegalArgumentException();
            }
            if (i > 4) {
                i2 = 4;
                i = 4;
            }
            this.f17087 = i;
            this.f17085 = i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21367() {
            int i = (this.f17087 + 1) << 1;
            if (this.f17088) {
                i += this.f17087 - 1;
            }
            if (this.f17089 == null || this.f17089.length() <= i) {
                return i;
            }
            return this.f17089.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21369(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            if (dateTimeZone != null) {
                if (i != 0 || this.f17089 == null) {
                    if (i >= 0) {
                        appendable.append('+');
                    } else {
                        appendable.append('-');
                        i = -i;
                    }
                    int i2 = i / 3600000;
                    FormatUtils.m21432(appendable, i2, 2);
                    if (this.f17085 != 1) {
                        int i3 = i - (i2 * 3600000);
                        if (i3 != 0 || this.f17087 > 1) {
                            int i4 = i3 / 60000;
                            if (this.f17088) {
                                appendable.append(':');
                            }
                            FormatUtils.m21432(appendable, i4, 2);
                            if (this.f17085 != 2) {
                                int i5 = i3 - (i4 * 60000);
                                if (i5 != 0 || this.f17087 > 2) {
                                    int i6 = i5 / 1000;
                                    if (this.f17088) {
                                        appendable.append(':');
                                    }
                                    FormatUtils.m21432(appendable, i6, 2);
                                    if (this.f17085 != 3) {
                                        int i7 = i5 - (i6 * 1000);
                                        if (i7 != 0 || this.f17087 > 3) {
                                            if (this.f17088) {
                                                appendable.append('.');
                                            }
                                            FormatUtils.m21432(appendable, i7, 3);
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                appendable.append(this.f17089);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21370(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21366() {
            return m21367();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21368(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            boolean z;
            int i2;
            int i3;
            int i4;
            char charAt;
            boolean z2 = false;
            int length = charSequence.length() - i;
            if (this.f17086 != null) {
                if (this.f17086.length() == 0) {
                    if (length <= 0 || !((charAt = charSequence.charAt(i)) == '-' || charAt == '+')) {
                        dateTimeParserBucket.m21404((Integer) 0);
                        return i;
                    }
                } else if (DateTimeFormatterBuilder.m21256(charSequence, i, this.f17086)) {
                    dateTimeParserBucket.m21404((Integer) 0);
                    return i + this.f17086.length();
                }
            }
            if (length <= 1) {
                return i ^ -1;
            }
            char charAt2 = charSequence.charAt(i);
            if (charAt2 == '-') {
                z = true;
            } else if (charAt2 != '+') {
                return i ^ -1;
            } else {
                z = false;
            }
            int i5 = length - 1;
            int i6 = i + 1;
            if (m21365(charSequence, i6, 2) < 2) {
                return i6 ^ -1;
            }
            int r5 = FormatUtils.m21429(charSequence, i6);
            if (r5 > 23) {
                return i6 ^ -1;
            }
            int i7 = r5 * 3600000;
            int i8 = i5 - 2;
            int i9 = i6 + 2;
            if (i8 <= 0) {
                i2 = i7;
                i3 = i9;
            } else {
                char charAt3 = charSequence.charAt(i9);
                if (charAt3 == ':') {
                    i8--;
                    i9++;
                    z2 = true;
                } else if (charAt3 < '0' || charAt3 > '9') {
                    i2 = i7;
                    i3 = i9;
                }
                int r6 = m21365(charSequence, i9, 2);
                if (r6 == 0 && !z2) {
                    i2 = i7;
                    i3 = i9;
                } else if (r6 < 2) {
                    return i9 ^ -1;
                } else {
                    int r62 = FormatUtils.m21429(charSequence, i9);
                    if (r62 > 59) {
                        return i9 ^ -1;
                    }
                    int i10 = i7 + (r62 * 60000);
                    int i11 = i8 - 2;
                    int i12 = i9 + 2;
                    if (i11 <= 0) {
                        i2 = i10;
                        i3 = i12;
                    } else {
                        if (z2) {
                            if (charSequence.charAt(i12) != ':') {
                                i2 = i10;
                                i3 = i12;
                            } else {
                                i11--;
                                i12++;
                            }
                        }
                        int r63 = m21365(charSequence, i12, 2);
                        if (r63 == 0 && !z2) {
                            i2 = i10;
                            i3 = i12;
                        } else if (r63 < 2) {
                            return i12 ^ -1;
                        } else {
                            int r64 = FormatUtils.m21429(charSequence, i12);
                            if (r64 > 59) {
                                return i12 ^ -1;
                            }
                            int i13 = i10 + (r64 * 1000);
                            int i14 = i11 - 2;
                            int i15 = i12 + 2;
                            if (i14 <= 0) {
                                i2 = i13;
                                i3 = i15;
                            } else {
                                if (z2) {
                                    if (charSequence.charAt(i15) == '.' || charSequence.charAt(i15) == ',') {
                                        int i16 = i14 - 1;
                                        i15++;
                                    } else {
                                        i2 = i13;
                                        i3 = i15;
                                    }
                                }
                                int r65 = m21365(charSequence, i15, 3);
                                if (r65 == 0 && !z2) {
                                    i2 = i13;
                                    i3 = i15;
                                } else if (r65 < 1) {
                                    return i15 ^ -1;
                                } else {
                                    int i17 = i15 + 1;
                                    int charAt4 = ((charSequence.charAt(i15) - '0') * 100) + i13;
                                    if (r65 > 1) {
                                        int i18 = i17 + 1;
                                        i2 = ((charSequence.charAt(i17) - '0') * 10) + charAt4;
                                        if (r65 > 2) {
                                            i2 += charSequence.charAt(i18) - '0';
                                            i3 = i18 + 1;
                                        } else {
                                            i3 = i18;
                                        }
                                    } else {
                                        i2 = charAt4;
                                        i3 = i17;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (z) {
                i4 = -i2;
            } else {
                i4 = i2;
            }
            dateTimeParserBucket.m21404(Integer.valueOf(i4));
            return i3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m21365(CharSequence charSequence, int i, int i2) {
            int i3 = 0;
            for (int min = Math.min(charSequence.length() - i, i2); min > 0; min--) {
                char charAt = charSequence.charAt(i + i3);
                if (charAt < '0' || charAt > '9') {
                    break;
                }
                i3++;
            }
            return i3;
        }
    }

    static class TimeZoneName implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f17083;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, DateTimeZone> f17084;

        TimeZoneName(int i, Map<String, DateTimeZone> map) {
            this.f17083 = i;
            this.f17084 = map;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21361() {
            return this.f17083 == 1 ? 4 : 20;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21363(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(m21359(j - ((long) i), dateTimeZone, locale));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m21359(long j, DateTimeZone dateTimeZone, Locale locale) {
            if (dateTimeZone == null) {
                return "";
            }
            switch (this.f17083) {
                case 0:
                    return dateTimeZone.getName(j, locale);
                case 1:
                    return dateTimeZone.getShortName(j, locale);
                default:
                    return "";
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21364(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21360() {
            return this.f17083 == 1 ? 4 : 20;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21362(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            Map<String, DateTimeZone> map = this.f17084;
            Map<String, DateTimeZone> r1 = map != null ? map : DateTimeUtils.m20887();
            String str = null;
            for (String next : r1.keySet()) {
                if (!DateTimeFormatterBuilder.m21265(charSequence, i, next) || (str != null && next.length() <= str.length())) {
                    next = str;
                }
                str = next;
            }
            if (str == null) {
                return i ^ -1;
            }
            dateTimeParserBucket.m21408(r1.get(str));
            return str.length() + i;
        }
    }

    enum TimeZoneId implements InternalParser, InternalPrinter {
        INSTANCE;
        

        /* renamed from: ʻ  reason: contains not printable characters */
        private static final List<String> f17076 = null;

        /* renamed from: 连任  reason: contains not printable characters */
        private static final Map<String, List<String>> f17078 = null;

        /* renamed from: 靐  reason: contains not printable characters */
        static final int f17079 = 0;

        /* renamed from: 麤  reason: contains not printable characters */
        private static final List<String> f17080 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        static final int f17081 = 0;

        static {
            f17076 = new ArrayList();
            f17080 = new ArrayList(DateTimeZone.getAvailableIDs());
            Collections.sort(f17080);
            f17078 = new HashMap();
            int i = 0;
            int i2 = 0;
            for (String next : f17080) {
                int indexOf = next.indexOf(47);
                if (indexOf >= 0) {
                    if (indexOf < next.length()) {
                        indexOf++;
                    }
                    int max = Math.max(i, indexOf);
                    String substring = next.substring(0, indexOf + 1);
                    String substring2 = next.substring(indexOf);
                    if (!f17078.containsKey(substring)) {
                        f17078.put(substring, new ArrayList());
                    }
                    f17078.get(substring).add(substring2);
                    i = max;
                } else {
                    f17076.add(next);
                }
                i2 = Math.max(i2, next.length());
            }
            f17079 = i2;
            f17081 = i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21355() {
            return f17079;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21357(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(dateTimeZone != null ? dateTimeZone.getID() : "");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21358(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21354() {
            return f17079;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21356(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            int i2;
            List<String> list;
            String str;
            List<String> list2 = f17076;
            int length = charSequence.length();
            int min = Math.min(length, f17081 + i);
            String str2 = "";
            int i3 = i;
            while (true) {
                if (i3 >= min) {
                    i2 = i;
                    list = list2;
                    break;
                } else if (charSequence.charAt(i3) == '/') {
                    str2 = charSequence.subSequence(i, i3 + 1).toString();
                    i2 = i + str2.length();
                    if (i3 < length) {
                        str = str2 + charSequence.charAt(i3 + 1);
                    } else {
                        str = str2;
                    }
                    List<String> list3 = f17078.get(str);
                    if (list3 == null) {
                        return i ^ -1;
                    }
                    list = list3;
                } else {
                    i3++;
                }
            }
            String str3 = null;
            int i4 = 0;
            while (i4 < list.size()) {
                String str4 = list.get(i4);
                if (!DateTimeFormatterBuilder.m21265(charSequence, i2, str4) || (str3 != null && str4.length() <= str3.length())) {
                    str4 = str3;
                }
                i4++;
                str3 = str4;
            }
            if (str3 == null) {
                return i ^ -1;
            }
            dateTimeParserBucket.m21408(DateTimeZone.forID(str2 + str3));
            return str3.length() + i2;
        }
    }

    static class Composite implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final InternalParser[] f17059;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f17060;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f17061;

        /* renamed from: 龘  reason: contains not printable characters */
        private final InternalPrinter[] f17062;

        Composite(List<Object> list) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            m21318(list, (List<Object>) arrayList, (List<Object>) arrayList2);
            if (arrayList.contains((Object) null) || arrayList.isEmpty()) {
                this.f17062 = null;
                this.f17061 = 0;
            } else {
                int size = arrayList.size();
                this.f17062 = new InternalPrinter[size];
                int i = 0;
                for (int i2 = 0; i2 < size; i2++) {
                    InternalPrinter internalPrinter = (InternalPrinter) arrayList.get(i2);
                    i += internalPrinter.m21539();
                    this.f17062[i2] = internalPrinter;
                }
                this.f17061 = i;
            }
            if (arrayList2.contains((Object) null) || arrayList2.isEmpty()) {
                this.f17059 = null;
                this.f17060 = 0;
                return;
            }
            int size2 = arrayList2.size();
            this.f17059 = new InternalParser[size2];
            int i3 = 0;
            int i4 = 0;
            while (i3 < size2) {
                InternalParser internalParser = (InternalParser) arrayList2.get(i3);
                int r2 = internalParser.m21533() + i4;
                this.f17059[i3] = internalParser;
                i3++;
                i4 = r2;
            }
            this.f17060 = i4;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21323() {
            return this.f17061;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21325(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            Locale locale2;
            InternalPrinter[] internalPrinterArr = this.f17062;
            if (internalPrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            if (locale == null) {
                locale2 = Locale.getDefault();
            } else {
                locale2 = locale;
            }
            for (InternalPrinter r0 : internalPrinterArr) {
                r0.m21540(appendable, j, chronology, i, dateTimeZone, locale2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m21326(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            InternalPrinter[] internalPrinterArr = this.f17062;
            if (internalPrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            if (locale == null) {
                locale = Locale.getDefault();
            }
            for (InternalPrinter r3 : internalPrinterArr) {
                r3.m21541(appendable, readablePartial, locale);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21320() {
            return this.f17060;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m21324(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            InternalParser[] internalParserArr = this.f17059;
            if (internalParserArr == null) {
                throw new UnsupportedOperationException();
            }
            int length = internalParserArr.length;
            for (int i2 = 0; i2 < length && i >= 0; i2++) {
                i = internalParserArr[i2].m21534(dateTimeParserBucket, charSequence, i);
            }
            return i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m21322() {
            return this.f17062 != null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public boolean m21321() {
            return this.f17059 != null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m21318(List<Object> list, List<Object> list2, List<Object> list3) {
            int size = list.size();
            for (int i = 0; i < size; i += 2) {
                Object obj = list.get(i);
                if (obj instanceof Composite) {
                    m21319(list2, ((Composite) obj).f17062);
                } else {
                    list2.add(obj);
                }
                Object obj2 = list.get(i + 1);
                if (obj2 instanceof Composite) {
                    m21319(list3, ((Composite) obj2).f17059);
                } else {
                    list3.add(obj2);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m21319(List<Object> list, Object[] objArr) {
            if (objArr != null) {
                for (Object add : objArr) {
                    list.add(add);
                }
            }
        }
    }

    static class MatchingParser implements InternalParser {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f17066;

        /* renamed from: 龘  reason: contains not printable characters */
        private final InternalParser[] f17067;

        MatchingParser(InternalParser[] internalParserArr) {
            int i;
            this.f17067 = internalParserArr;
            int i2 = 0;
            int length = internalParserArr.length;
            while (true) {
                length--;
                if (length >= 0) {
                    InternalParser internalParser = internalParserArr[length];
                    if (internalParser == null || (i = internalParser.m21533()) <= i2) {
                        i = i2;
                    }
                    i2 = i;
                } else {
                    this.f17066 = i2;
                    return;
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21335() {
            return this.f17066;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
            r11.m21409(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
            return r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            return r0 ^ -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0016, code lost:
            if (r4 > r13) goto L_0x001c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
            if (r4 != r13) goto L_0x0053;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
            if (r1 == false) goto L_0x0053;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
            if (r2 == null) goto L_0x0021;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m21336(org.joda.time.format.DateTimeParserBucket r11, java.lang.CharSequence r12, int r13) {
            /*
                r10 = this;
                r5 = 0
                org.joda.time.format.InternalParser[] r7 = r10.f17067
                int r8 = r7.length
                java.lang.Object r9 = r11.m21395()
                r2 = 0
                r6 = r5
                r0 = r13
                r4 = r13
            L_0x000c:
                if (r6 >= r8) goto L_0x0058
                r1 = r7[r6]
                if (r1 != 0) goto L_0x0023
                if (r4 > r13) goto L_0x0015
            L_0x0014:
                return r13
            L_0x0015:
                r1 = 1
            L_0x0016:
                if (r4 > r13) goto L_0x001c
                if (r4 != r13) goto L_0x0053
                if (r1 == 0) goto L_0x0053
            L_0x001c:
                if (r2 == 0) goto L_0x0021
                r11.m21409((java.lang.Object) r2)
            L_0x0021:
                r13 = r4
                goto L_0x0014
            L_0x0023:
                int r3 = r1.m21534(r11, r12, r13)
                if (r3 < r13) goto L_0x004a
                if (r3 <= r4) goto L_0x0056
                int r1 = r12.length()
                if (r3 >= r1) goto L_0x003b
                int r1 = r6 + 1
                if (r1 >= r8) goto L_0x003b
                int r1 = r6 + 1
                r1 = r7[r1]
                if (r1 != 0) goto L_0x003d
            L_0x003b:
                r13 = r3
                goto L_0x0014
            L_0x003d:
                java.lang.Object r1 = r11.m21395()
                r2 = r1
            L_0x0042:
                r11.m21409((java.lang.Object) r9)
                int r1 = r6 + 1
                r6 = r1
                r4 = r3
                goto L_0x000c
            L_0x004a:
                if (r3 >= 0) goto L_0x0056
                r1 = r3 ^ -1
                if (r1 <= r0) goto L_0x0056
                r0 = r1
                r3 = r4
                goto L_0x0042
            L_0x0053:
                r13 = r0 ^ -1
                goto L_0x0014
            L_0x0056:
                r3 = r4
                goto L_0x0042
            L_0x0058:
                r1 = r5
                goto L_0x0016
            */
            throw new UnsupportedOperationException("Method not decompiled: org.joda.time.format.DateTimeFormatterBuilder.MatchingParser.m21336(org.joda.time.format.DateTimeParserBucket, java.lang.CharSequence, int):int");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m21265(CharSequence charSequence, int i, String str) {
        int length = str.length();
        if (charSequence.length() - i < length) {
            return false;
        }
        for (int i2 = 0; i2 < length; i2++) {
            if (charSequence.charAt(i + i2) != str.charAt(i2)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m21256(CharSequence charSequence, int i, String str) {
        char upperCase;
        char upperCase2;
        int length = str.length();
        if (charSequence.length() - i < length) {
            return false;
        }
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = charSequence.charAt(i + i2);
            char charAt2 = str.charAt(i2);
            if (charAt != charAt2 && (upperCase = Character.toUpperCase(charAt)) != (upperCase2 = Character.toUpperCase(charAt2)) && Character.toLowerCase(upperCase) != Character.toLowerCase(upperCase2)) {
                return false;
            }
        }
        return true;
    }
}
