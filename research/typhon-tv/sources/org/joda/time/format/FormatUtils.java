package org.joda.time.format;

import java.io.IOException;

public class FormatUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final double f17117 = Math.log(10.0d);

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21435(StringBuffer stringBuffer, int i, int i2) {
        try {
            m21432((Appendable) stringBuffer, i, i2);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21432(Appendable appendable, int i, int i2) throws IOException {
        int i3;
        if (i < 0) {
            appendable.append('-');
            if (i != Integer.MIN_VALUE) {
                i = -i;
            } else {
                while (i2 > 10) {
                    appendable.append('0');
                    i2--;
                }
                appendable.append("2147483648");
                return;
            }
        }
        if (i < 10) {
            while (i2 > 1) {
                appendable.append('0');
                i2--;
            }
            appendable.append((char) (i + 48));
        } else if (i < 100) {
            while (i2 > 2) {
                appendable.append('0');
                i2--;
            }
            int i4 = ((i + 1) * 13421772) >> 27;
            appendable.append((char) (i4 + 48));
            appendable.append((char) (((i - (i4 << 3)) - (i4 << 1)) + 48));
        } else {
            if (i < 1000) {
                i3 = 3;
            } else if (i < 10000) {
                i3 = 4;
            } else {
                i3 = ((int) (Math.log((double) i) / f17117)) + 1;
            }
            while (i2 > i3) {
                appendable.append('0');
                i2--;
            }
            appendable.append(Integer.toString(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21434(StringBuffer stringBuffer, int i) {
        try {
            m21431((Appendable) stringBuffer, i);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21431(Appendable appendable, int i) throws IOException {
        if (i < 0) {
            appendable.append('-');
            if (i != Integer.MIN_VALUE) {
                i = -i;
            } else {
                appendable.append("2147483648");
                return;
            }
        }
        if (i < 10) {
            appendable.append((char) (i + 48));
        } else if (i < 100) {
            int i2 = ((i + 1) * 13421772) >> 27;
            appendable.append((char) (i2 + 48));
            appendable.append((char) (((i - (i2 << 3)) - (i2 << 1)) + 48));
        } else {
            appendable.append(Integer.toString(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21436(StringBuffer stringBuffer, long j) {
        try {
            m21433((Appendable) stringBuffer, j);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m21433(Appendable appendable, long j) throws IOException {
        int i = (int) j;
        if (((long) i) == j) {
            m21431(appendable, i);
        } else {
            appendable.append(Long.toString(j));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m21428(long j) {
        if (j < 0) {
            if (j != Long.MIN_VALUE) {
                return m21428(-j) + 1;
            }
            return 20;
        } else if (j < 10) {
            return 1;
        } else {
            if (j < 100) {
                return 2;
            }
            if (j < 1000) {
                return 3;
            }
            if (j < 10000) {
                return 4;
            }
            return ((int) (Math.log((double) j) / f17117)) + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m21429(CharSequence charSequence, int i) {
        int charAt = charSequence.charAt(i) - '0';
        return (((charAt << 1) + (charAt << 3)) + charSequence.charAt(i + 1)) - 48;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m21430(String str, int i) {
        String concat;
        int i2 = i + 32;
        if (str.length() <= i2 + 3) {
            concat = str;
        } else {
            concat = str.substring(0, i2).concat("...");
        }
        if (i <= 0) {
            return "Invalid format: \"" + concat + '\"';
        }
        if (i >= str.length()) {
            return "Invalid format: \"" + concat + "\" is too short";
        }
        return "Invalid format: \"" + concat + "\" is malformed at \"" + concat.substring(i) + '\"';
    }
}
