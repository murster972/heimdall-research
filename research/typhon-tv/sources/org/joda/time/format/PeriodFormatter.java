package org.joda.time.format;

import java.util.Locale;
import org.joda.time.MutablePeriod;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePeriod;

public class PeriodFormatter {

    /* renamed from: 靐  reason: contains not printable characters */
    private final PeriodParser f17180;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PeriodType f17181;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Locale f17182;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PeriodPrinter f17183;

    public PeriodFormatter(PeriodPrinter periodPrinter, PeriodParser periodParser) {
        this.f17183 = periodPrinter;
        this.f17180 = periodParser;
        this.f17182 = null;
        this.f17181 = null;
    }

    PeriodFormatter(PeriodPrinter periodPrinter, PeriodParser periodParser, Locale locale, PeriodType periodType) {
        this.f17183 = periodPrinter;
        this.f17180 = periodParser;
        this.f17182 = locale;
        this.f17181 = periodType;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodPrinter m21558() {
        return this.f17183;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PeriodParser m21553() {
        return this.f17180;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodFormatter m21557(PeriodType periodType) {
        return periodType == this.f17181 ? this : new PeriodFormatter(this.f17183, this.f17180, this.f17182, periodType);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21555(ReadablePeriod readablePeriod) {
        m21551();
        m21549(readablePeriod);
        PeriodPrinter r0 = m21558();
        StringBuffer stringBuffer = new StringBuffer(r0.m21627(readablePeriod, this.f17182));
        r0.m21628(stringBuffer, readablePeriod, this.f17182);
        return stringBuffer.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m21551() {
        if (this.f17183 == null) {
            throw new UnsupportedOperationException("Printing not supported");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m21549(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            throw new IllegalArgumentException("Period must not be null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21554(ReadWritablePeriod readWritablePeriod, String str, int i) {
        m21550();
        m21549((ReadablePeriod) readWritablePeriod);
        return m21553().m21625(readWritablePeriod, str, i, this.f17182);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Period m21556(String str) {
        m21550();
        return m21552(str).toPeriod();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MutablePeriod m21552(String str) {
        m21550();
        MutablePeriod mutablePeriod = new MutablePeriod(0, this.f17181);
        int r0 = m21553().m21625(mutablePeriod, str, 0, this.f17182);
        if (r0 < 0) {
            r0 ^= -1;
        } else if (r0 >= str.length()) {
            return mutablePeriod;
        }
        throw new IllegalArgumentException(FormatUtils.m21430(str, r0));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m21550() {
        if (this.f17180 == null) {
            throw new UnsupportedOperationException("Parsing not supported");
        }
    }
}
