package org.joda.time.format;

import java.util.Collection;
import java.util.HashSet;
import org.joda.time.DateTimeFieldType;

public class ISODateTimeFormat {
    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeFormatter m21453(Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        if (collection == null || collection.size() == 0) {
            throw new IllegalArgumentException("The fields must not be null or empty");
        }
        HashSet hashSet = new HashSet(collection);
        int size = hashSet.size();
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        if (hashSet.contains(DateTimeFieldType.monthOfYear())) {
            z3 = m21457(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.dayOfYear())) {
            z3 = m21448(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.weekOfWeekyear())) {
            z3 = m21451(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.dayOfMonth())) {
            z3 = m21457(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.dayOfWeek())) {
            z3 = m21451(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.remove(DateTimeFieldType.year())) {
            dateTimeFormatterBuilder.m21310(TyphoonApp.f17170);
            z3 = true;
        } else if (hashSet.remove(DateTimeFieldType.weekyear())) {
            dateTimeFormatterBuilder.m21310(TyphoonApp.f17168);
            z3 = true;
        } else {
            z3 = false;
        }
        if (hashSet.size() < size) {
            z4 = true;
        } else {
            z4 = false;
        }
        m21455(dateTimeFormatterBuilder, hashSet, z, z2, z3, z4);
        if (!dateTimeFormatterBuilder.m21297()) {
            throw new IllegalArgumentException("No valid format for fields: " + collection);
        }
        try {
            collection.retainAll(hashSet);
        } catch (UnsupportedOperationException e) {
        }
        return dateTimeFormatterBuilder.m21298();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m21457(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        if (collection.remove(DateTimeFieldType.year())) {
            dateTimeFormatterBuilder.m21310(TyphoonApp.f17170);
            if (collection.remove(DateTimeFieldType.monthOfYear())) {
                if (collection.remove(DateTimeFieldType.dayOfMonth())) {
                    m21456(dateTimeFormatterBuilder, z);
                    dateTimeFormatterBuilder.m21274(2);
                    m21456(dateTimeFormatterBuilder, z);
                    dateTimeFormatterBuilder.m21276(2);
                    return false;
                }
                dateTimeFormatterBuilder.m21299('-');
                dateTimeFormatterBuilder.m21274(2);
                return true;
            } else if (!collection.remove(DateTimeFieldType.dayOfMonth())) {
                return true;
            } else {
                m21454(collection, z2);
                dateTimeFormatterBuilder.m21299('-');
                dateTimeFormatterBuilder.m21299('-');
                dateTimeFormatterBuilder.m21276(2);
                return false;
            }
        } else if (collection.remove(DateTimeFieldType.monthOfYear())) {
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21274(2);
            if (!collection.remove(DateTimeFieldType.dayOfMonth())) {
                return true;
            }
            m21456(dateTimeFormatterBuilder, z);
            dateTimeFormatterBuilder.m21276(2);
            return false;
        } else if (!collection.remove(DateTimeFieldType.dayOfMonth())) {
            return false;
        } else {
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21276(2);
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m21448(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        if (collection.remove(DateTimeFieldType.year())) {
            dateTimeFormatterBuilder.m21310(TyphoonApp.f17170);
            if (!collection.remove(DateTimeFieldType.dayOfYear())) {
                return true;
            }
            m21456(dateTimeFormatterBuilder, z);
            dateTimeFormatterBuilder.m21278(3);
            return false;
        } else if (!collection.remove(DateTimeFieldType.dayOfYear())) {
            return false;
        } else {
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21278(3);
            return false;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean m21451(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        if (collection.remove(DateTimeFieldType.weekyear())) {
            dateTimeFormatterBuilder.m21310(TyphoonApp.f17168);
            if (collection.remove(DateTimeFieldType.weekOfWeekyear())) {
                m21456(dateTimeFormatterBuilder, z);
                dateTimeFormatterBuilder.m21299('W');
                dateTimeFormatterBuilder.m21280(2);
                if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
                    return true;
                }
                m21456(dateTimeFormatterBuilder, z);
                dateTimeFormatterBuilder.m21273(1);
                return false;
            } else if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
                return true;
            } else {
                m21454(collection, z2);
                m21456(dateTimeFormatterBuilder, z);
                dateTimeFormatterBuilder.m21299('W');
                dateTimeFormatterBuilder.m21299('-');
                dateTimeFormatterBuilder.m21273(1);
                return false;
            }
        } else if (collection.remove(DateTimeFieldType.weekOfWeekyear())) {
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21299('W');
            dateTimeFormatterBuilder.m21280(2);
            if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
                return true;
            }
            m21456(dateTimeFormatterBuilder, z);
            dateTimeFormatterBuilder.m21273(1);
            return false;
        } else if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
            return false;
        } else {
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21299('W');
            dateTimeFormatterBuilder.m21299('-');
            dateTimeFormatterBuilder.m21273(1);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21455(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2, boolean z3, boolean z4) {
        boolean remove = collection.remove(DateTimeFieldType.hourOfDay());
        boolean remove2 = collection.remove(DateTimeFieldType.minuteOfHour());
        boolean remove3 = collection.remove(DateTimeFieldType.secondOfMinute());
        boolean remove4 = collection.remove(DateTimeFieldType.millisOfSecond());
        if (remove || remove2 || remove3 || remove4) {
            if (remove || remove2 || remove3 || remove4) {
                if (z2 && z3) {
                    throw new IllegalArgumentException("No valid ISO8601 format for fields because Date was reduced precision: " + collection);
                } else if (z4) {
                    dateTimeFormatterBuilder.m21299('T');
                }
            }
            if ((!remove || !remove2 || !remove3) && (!remove || remove3 || remove4)) {
                if (z2 && z4) {
                    throw new IllegalArgumentException("No valid ISO8601 format for fields because Time was truncated: " + collection);
                } else if ((remove || ((!remove2 || !remove3) && ((!remove2 || remove4) && !remove3))) && z2) {
                    throw new IllegalArgumentException("No valid ISO8601 format for fields: " + collection);
                }
            }
            if (remove) {
                dateTimeFormatterBuilder.m21292(2);
            } else if (remove2 || remove3 || remove4) {
                dateTimeFormatterBuilder.m21299('-');
            }
            if (z && remove && remove2) {
                dateTimeFormatterBuilder.m21299(':');
            }
            if (remove2) {
                dateTimeFormatterBuilder.m21294(2);
            } else if (remove3 || remove4) {
                dateTimeFormatterBuilder.m21299('-');
            }
            if (z && remove2 && remove3) {
                dateTimeFormatterBuilder.m21299(':');
            }
            if (remove3) {
                dateTimeFormatterBuilder.m21284(2);
            } else if (remove4) {
                dateTimeFormatterBuilder.m21299('-');
            }
            if (remove4) {
                dateTimeFormatterBuilder.m21299('.');
                dateTimeFormatterBuilder.m21300(3);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21454(Collection<DateTimeFieldType> collection, boolean z) {
        if (z) {
            throw new IllegalArgumentException("No valid ISO8601 format for fields: " + collection);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21456(DateTimeFormatterBuilder dateTimeFormatterBuilder, boolean z) {
        if (z) {
            dateTimeFormatterBuilder.m21299('-');
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeFormatter m21452() {
        return TyphoonApp.f17153;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static DateTimeFormatter m21447() {
        return TyphoonApp.f17174;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static DateTimeFormatter m21450() {
        return TyphoonApp.f17120;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static DateTimeFormatter m21449() {
        return TyphoonApp.f17121;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static DateTimeFormatter m21446() {
        return TyphoonApp.f17122;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static DateTimeFormatter m21437() {
        return TyphoonApp.f17123;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static DateTimeFormatter m21438() {
        return m21440();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static DateTimeFormatter m21439() {
        return TyphoonApp.f17160;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static DateTimeFormatter m21443() {
        return TyphoonApp.f17164;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static DateTimeFormatter m21444() {
        return TyphoonApp.f17176;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static DateTimeFormatter m21445() {
        return TyphoonApp.f17138;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static DateTimeFormatter m21442() {
        return TyphoonApp.f17130;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static DateTimeFormatter m21440() {
        return TyphoonApp.f17173;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static DateTimeFormatter m21441() {
        return TyphoonApp.f17126;
    }

    static final class TyphoonApp {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static final DateTimeFormatter f17118 = m21471();

        /* renamed from: ʻʻ  reason: contains not printable characters */
        private static final DateTimeFormatter f17119 = m21482();
        /* access modifiers changed from: private */

        /* renamed from: ʻʼ  reason: contains not printable characters */
        public static final DateTimeFormatter f17120 = m21487();
        /* access modifiers changed from: private */

        /* renamed from: ʻʽ  reason: contains not printable characters */
        public static final DateTimeFormatter f17121 = m21499();
        /* access modifiers changed from: private */

        /* renamed from: ʻʾ  reason: contains not printable characters */
        public static final DateTimeFormatter f17122 = m21503();
        /* access modifiers changed from: private */

        /* renamed from: ʻʿ  reason: contains not printable characters */
        public static final DateTimeFormatter f17123 = m21505();

        /* renamed from: ʼ  reason: contains not printable characters */
        private static final DateTimeFormatter f17124 = m21472();

        /* renamed from: ʼʼ  reason: contains not printable characters */
        private static final DateTimeFormatter f17125 = m21510();
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public static final DateTimeFormatter f17126 = m21474();

        /* renamed from: ʽʽ  reason: contains not printable characters */
        private static final DateTimeFormatter f17127 = m21531();

        /* renamed from: ʾ  reason: contains not printable characters */
        private static final DateTimeFormatter f17128 = m21473();

        /* renamed from: ʾʾ  reason: contains not printable characters */
        private static final DateTimeFormatter f17129 = m21480();
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public static final DateTimeFormatter f17130 = m21504();

        /* renamed from: ʿʿ  reason: contains not printable characters */
        private static final DateTimeFormatter f17131 = m21512();

        /* renamed from: ˆ  reason: contains not printable characters */
        private static final DateTimeFormatter f17132 = m21529();

        /* renamed from: ˆˆ  reason: contains not printable characters */
        private static final DateTimeFormatter f17133 = m21484();

        /* renamed from: ˈ  reason: contains not printable characters */
        private static final DateTimeFormatter f17134 = m21478();

        /* renamed from: ˈˈ  reason: contains not printable characters */
        private static final DateTimeFormatter f17135 = m21488();

        /* renamed from: ˉ  reason: contains not printable characters */
        private static final DateTimeFormatter f17136 = m21460();

        /* renamed from: ˉˉ  reason: contains not printable characters */
        private static final DateTimeFormatter f17137 = m21506();
        /* access modifiers changed from: private */

        /* renamed from: ˊ  reason: contains not printable characters */
        public static final DateTimeFormatter f17138 = m21520();

        /* renamed from: ˊˊ  reason: contains not printable characters */
        private static final DateTimeFormatter f17139 = m21490();

        /* renamed from: ˋ  reason: contains not printable characters */
        private static final DateTimeFormatter f17140 = m21527();

        /* renamed from: ˋˋ  reason: contains not printable characters */
        private static final DateTimeFormatter f17141 = m21492();

        /* renamed from: ˎ  reason: contains not printable characters */
        private static final DateTimeFormatter f17142 = m21508();

        /* renamed from: ˎˎ  reason: contains not printable characters */
        private static final DateTimeFormatter f17143 = m21494();

        /* renamed from: ˏ  reason: contains not printable characters */
        private static final DateTimeFormatter f17144 = m21461();

        /* renamed from: ˏˏ  reason: contains not printable characters */
        private static final DateTimeFormatter f17145 = m21496();

        /* renamed from: ˑ  reason: contains not printable characters */
        private static final DateTimeFormatter f17146 = m21475();

        /* renamed from: ˑˑ  reason: contains not printable characters */
        private static final DateTimeFormatter f17147 = m21500();

        /* renamed from: י  reason: contains not printable characters */
        private static final DateTimeFormatter f17148 = m21462();

        /* renamed from: יי  reason: contains not printable characters */
        private static final DateTimeFormatter f17149 = m21502();

        /* renamed from: ـ  reason: contains not printable characters */
        private static final DateTimeFormatter f17150 = m21463();

        /* renamed from: ــ  reason: contains not printable characters */
        private static final DateTimeFormatter f17151 = m21486();

        /* renamed from: ٴ  reason: contains not printable characters */
        private static final DateTimeFormatter f17152 = m21476();
        /* access modifiers changed from: private */

        /* renamed from: ٴٴ  reason: contains not printable characters */
        public static final DateTimeFormatter f17153 = m21493();

        /* renamed from: ᐧ  reason: contains not printable characters */
        private static final DateTimeFormatter f17154 = m21477();

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        private static final DateTimeFormatter f17155 = m21526();

        /* renamed from: ᴵ  reason: contains not printable characters */
        private static final DateTimeFormatter f17156 = m21464();

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        private static final DateTimeFormatter f17157 = m21459();

        /* renamed from: ᵎ  reason: contains not printable characters */
        private static final DateTimeFormatter f17158 = m21465();

        /* renamed from: ᵎᵎ  reason: contains not printable characters */
        private static final DateTimeFormatter f17159 = m21516();
        /* access modifiers changed from: private */

        /* renamed from: ᵔ  reason: contains not printable characters */
        public static final DateTimeFormatter f17160 = m21511();

        /* renamed from: ᵔᵔ  reason: contains not printable characters */
        private static final DateTimeFormatter f17161 = m21498();

        /* renamed from: ᵢ  reason: contains not printable characters */
        private static final DateTimeFormatter f17162 = m21513();

        /* renamed from: ᵢᵢ  reason: contains not printable characters */
        private static final DateTimeFormatter f17163 = m21495();
        /* access modifiers changed from: private */

        /* renamed from: ⁱ  reason: contains not printable characters */
        public static final DateTimeFormatter f17164 = m21515();

        /* renamed from: ⁱⁱ  reason: contains not printable characters */
        private static final DateTimeFormatter f17165 = m21491();

        /* renamed from: 连任  reason: contains not printable characters */
        private static final DateTimeFormatter f17166 = m21470();

        /* renamed from: 靐  reason: contains not printable characters */
        private static final DateTimeFormatter f17167 = m21467();
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public static final DateTimeFormatter f17168 = m21469();

        /* renamed from: 齉  reason: contains not printable characters */
        private static final DateTimeFormatter f17169 = m21468();
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public static final DateTimeFormatter f17170 = m21466();

        /* renamed from: ﹳ  reason: contains not printable characters */
        private static final DateTimeFormatter f17171 = m21517();

        /* renamed from: ﹳﹳ  reason: contains not printable characters */
        private static final DateTimeFormatter f17172 = m21530();
        /* access modifiers changed from: private */

        /* renamed from: ﹶ  reason: contains not printable characters */
        public static final DateTimeFormatter f17173 = m21514();
        /* access modifiers changed from: private */

        /* renamed from: ﹶﹶ  reason: contains not printable characters */
        public static final DateTimeFormatter f17174 = m21497();

        /* renamed from: ﾞ  reason: contains not printable characters */
        private static final DateTimeFormatter f17175 = m21518();
        /* access modifiers changed from: private */

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public static final DateTimeFormatter f17176 = m21519();

        /* renamed from: ﾞ  reason: contains not printable characters */
        private static DateTimeFormatter m21530() {
            if (f17172 != null) {
                return f17172;
            }
            return new DateTimeFormatterBuilder().m21310(m21495()).m21289(new DateTimeFormatterBuilder().m21299('T').m21310(m21478()).m21290()).m21298();
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        private static DateTimeFormatter m21493() {
            if (f17153 == null) {
                return m21495().m21234();
            }
            return f17153;
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        private static DateTimeFormatter m21495() {
            if (f17163 != null) {
                return f17163;
            }
            return new DateTimeFormatterBuilder().m21312((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m21310(m21466()).m21289(new DateTimeFormatterBuilder().m21310(m21467()).m21289(m21468().m21239()).m21290()).m21290(), new DateTimeFormatterBuilder().m21310(m21469()).m21310(m21470()).m21289(m21471().m21239()).m21290(), new DateTimeFormatterBuilder().m21310(m21466()).m21310(m21472()).m21290()}).m21298();
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        private static DateTimeFormatter m21497() {
            if (f17174 == null) {
                return new DateTimeFormatterBuilder().m21289(m21473().m21239()).m21310(m21491()).m21289(m21478().m21239()).m21298();
            }
            return f17174;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        private static DateTimeFormatter m21487() {
            if (f17120 == null) {
                return new DateTimeFormatterBuilder().m21289(m21473().m21239()).m21310(m21491()).m21298().m21234();
            }
            return f17120;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        private static DateTimeFormatter m21491() {
            if (f17165 != null) {
                return f17165;
            }
            DateTimeParser r0 = new DateTimeFormatterBuilder().m21312((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m21299('.').m21290(), new DateTimeFormatterBuilder().m21299(',').m21290()}).m21290();
            return new DateTimeFormatterBuilder().m21310(m21474()).m21312((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m21310(m21475()).m21312((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m21310(m21476()).m21289(new DateTimeFormatterBuilder().m21311(r0).m21301(1, 9).m21290()).m21290(), new DateTimeFormatterBuilder().m21311(r0).m21285(1, 9).m21290(), null}).m21290(), new DateTimeFormatterBuilder().m21311(r0).m21295(1, 9).m21290(), null}).m21298();
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        private static DateTimeFormatter m21499() {
            if (f17121 != null) {
                return f17121;
            }
            DateTimeParser r0 = new DateTimeFormatterBuilder().m21299('T').m21310(m21491()).m21289(m21478().m21239()).m21290();
            return new DateTimeFormatterBuilder().m21312((DateTimePrinter) null, new DateTimeParser[]{r0, m21503().m21239()}).m21298();
        }

        /* renamed from: י  reason: contains not printable characters */
        private static DateTimeFormatter m21503() {
            if (f17122 != null) {
                return f17122;
            }
            return new DateTimeFormatterBuilder().m21310(m21495()).m21289(new DateTimeFormatterBuilder().m21299('T').m21289(m21491().m21239()).m21289(m21478().m21239()).m21290()).m21298();
        }

        /* renamed from: ـ  reason: contains not printable characters */
        private static DateTimeFormatter m21505() {
            if (f17123 != null) {
                return f17123;
            }
            return new DateTimeFormatterBuilder().m21310(m21495()).m21289(new DateTimeFormatterBuilder().m21299('T').m21310(m21491()).m21290()).m21298().m21234();
        }

        /* renamed from: ᴵ  reason: contains not printable characters */
        private static DateTimeFormatter m21511() {
            if (f17160 == null) {
                return new DateTimeFormatterBuilder().m21310(m21460()).m21310(m21478()).m21298();
            }
            return f17160;
        }

        /* renamed from: ᵎ  reason: contains not printable characters */
        private static DateTimeFormatter m21513() {
            if (f17162 == null) {
                return new DateTimeFormatterBuilder().m21310(m21508()).m21310(m21478()).m21298();
            }
            return f17162;
        }

        /* renamed from: ᵔ  reason: contains not printable characters */
        private static DateTimeFormatter m21515() {
            if (f17164 == null) {
                return new DateTimeFormatterBuilder().m21310(m21473()).m21310(m21511()).m21298();
            }
            return f17164;
        }

        /* renamed from: ᵢ  reason: contains not printable characters */
        private static DateTimeFormatter m21517() {
            if (f17171 == null) {
                return new DateTimeFormatterBuilder().m21310(m21473()).m21310(m21513()).m21298();
            }
            return f17171;
        }

        /* renamed from: ⁱ  reason: contains not printable characters */
        private static DateTimeFormatter m21519() {
            if (f17176 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21438()).m21310(m21515()).m21298();
            }
            return f17176;
        }

        /* renamed from: ﹳ  reason: contains not printable characters */
        private static DateTimeFormatter m21526() {
            if (f17155 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21438()).m21310(m21517()).m21298();
            }
            return f17155;
        }

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        private static DateTimeFormatter m21531() {
            if (f17127 == null) {
                return new DateTimeFormatterBuilder().m21310(m21466()).m21310(m21472()).m21298();
            }
            return f17127;
        }

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        private static DateTimeFormatter m21510() {
            if (f17125 == null) {
                return new DateTimeFormatterBuilder().m21310(m21531()).m21310(m21515()).m21298();
            }
            return f17125;
        }

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        private static DateTimeFormatter m21512() {
            if (f17131 == null) {
                return new DateTimeFormatterBuilder().m21310(m21531()).m21310(m21517()).m21298();
            }
            return f17131;
        }

        /* renamed from: ʻʻ  reason: contains not printable characters */
        private static DateTimeFormatter m21459() {
            if (f17157 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21445()).m21310(m21515()).m21298();
            }
            return f17157;
        }

        /* renamed from: ʽʽ  reason: contains not printable characters */
        private static DateTimeFormatter m21482() {
            if (f17119 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21445()).m21310(m21517()).m21298();
            }
            return f17119;
        }

        /* renamed from: ʼʼ  reason: contains not printable characters */
        private static DateTimeFormatter m21480() {
            if (f17129 == null) {
                return new DateTimeFormatterBuilder().m21283(4, 4).m21308(DateTimeFieldType.monthOfYear(), 2).m21308(DateTimeFieldType.dayOfMonth(), 2).m21298();
            }
            return f17129;
        }

        /* renamed from: ʿʿ  reason: contains not printable characters */
        private static DateTimeFormatter m21486() {
            if (f17151 == null) {
                return new DateTimeFormatterBuilder().m21308(DateTimeFieldType.hourOfDay(), 2).m21308(DateTimeFieldType.minuteOfHour(), 2).m21308(DateTimeFieldType.secondOfMinute(), 2).m21299('.').m21301(3, 9).m21305("Z", false, 2, 2).m21298();
            }
            return f17151;
        }

        /* renamed from: ʾʾ  reason: contains not printable characters */
        private static DateTimeFormatter m21484() {
            if (f17133 == null) {
                return new DateTimeFormatterBuilder().m21308(DateTimeFieldType.hourOfDay(), 2).m21308(DateTimeFieldType.minuteOfHour(), 2).m21308(DateTimeFieldType.secondOfMinute(), 2).m21305("Z", false, 2, 2).m21298();
            }
            return f17133;
        }

        /* renamed from: ــ  reason: contains not printable characters */
        private static DateTimeFormatter m21506() {
            if (f17137 == null) {
                return new DateTimeFormatterBuilder().m21310(m21473()).m21310(m21486()).m21298();
            }
            return f17137;
        }

        /* renamed from: ˆˆ  reason: contains not printable characters */
        private static DateTimeFormatter m21488() {
            if (f17135 == null) {
                return new DateTimeFormatterBuilder().m21310(m21473()).m21310(m21484()).m21298();
            }
            return f17135;
        }

        /* renamed from: ˉˉ  reason: contains not printable characters */
        private static DateTimeFormatter m21492() {
            if (f17141 == null) {
                return new DateTimeFormatterBuilder().m21310(m21480()).m21310(m21506()).m21298();
            }
            return f17141;
        }

        /* renamed from: ˈˈ  reason: contains not printable characters */
        private static DateTimeFormatter m21490() {
            if (f17139 == null) {
                return new DateTimeFormatterBuilder().m21310(m21480()).m21310(m21488()).m21298();
            }
            return f17139;
        }

        /* renamed from: ˋˋ  reason: contains not printable characters */
        private static DateTimeFormatter m21496() {
            if (f17145 == null) {
                return new DateTimeFormatterBuilder().m21283(4, 4).m21308(DateTimeFieldType.dayOfYear(), 3).m21298();
            }
            return f17145;
        }

        /* renamed from: ˊˊ  reason: contains not printable characters */
        private static DateTimeFormatter m21494() {
            if (f17143 == null) {
                return new DateTimeFormatterBuilder().m21310(m21496()).m21310(m21506()).m21298();
            }
            return f17143;
        }

        /* renamed from: ˏˏ  reason: contains not printable characters */
        private static DateTimeFormatter m21500() {
            if (f17147 == null) {
                return new DateTimeFormatterBuilder().m21310(m21496()).m21310(m21488()).m21298();
            }
            return f17147;
        }

        /* renamed from: ˎˎ  reason: contains not printable characters */
        private static DateTimeFormatter m21498() {
            if (f17161 == null) {
                return new DateTimeFormatterBuilder().m21293(4, 4).m21299('W').m21308(DateTimeFieldType.weekOfWeekyear(), 2).m21308(DateTimeFieldType.dayOfWeek(), 1).m21298();
            }
            return f17161;
        }

        /* renamed from: ˑˑ  reason: contains not printable characters */
        private static DateTimeFormatter m21502() {
            if (f17149 == null) {
                return new DateTimeFormatterBuilder().m21310(m21498()).m21310(m21506()).m21298();
            }
            return f17149;
        }

        /* renamed from: ᵔᵔ  reason: contains not printable characters */
        private static DateTimeFormatter m21516() {
            if (f17159 == null) {
                return new DateTimeFormatterBuilder().m21310(m21498()).m21310(m21488()).m21298();
            }
            return f17159;
        }

        /* renamed from: יי  reason: contains not printable characters */
        private static DateTimeFormatter m21504() {
            if (f17130 == null) {
                return new DateTimeFormatterBuilder().m21310(m21466()).m21310(m21467()).m21298();
            }
            return f17130;
        }

        /* renamed from: ᵎᵎ  reason: contains not printable characters */
        private static DateTimeFormatter m21514() {
            if (f17173 == null) {
                return new DateTimeFormatterBuilder().m21310(m21466()).m21310(m21467()).m21310(m21468()).m21298();
            }
            return f17173;
        }

        /* renamed from: ᵢᵢ  reason: contains not printable characters */
        private static DateTimeFormatter m21518() {
            if (f17175 == null) {
                return new DateTimeFormatterBuilder().m21310(m21469()).m21310(m21470()).m21298();
            }
            return f17175;
        }

        /* renamed from: ⁱⁱ  reason: contains not printable characters */
        private static DateTimeFormatter m21520() {
            if (f17138 == null) {
                return new DateTimeFormatterBuilder().m21310(m21469()).m21310(m21470()).m21310(m21471()).m21298();
            }
            return f17138;
        }

        /* renamed from: ﹳﹳ  reason: contains not printable characters */
        private static DateTimeFormatter m21527() {
            if (f17140 == null) {
                return new DateTimeFormatterBuilder().m21310(m21474()).m21310(m21475()).m21298();
            }
            return f17140;
        }

        /* renamed from: ٴٴ  reason: contains not printable characters */
        private static DateTimeFormatter m21508() {
            if (f17142 == null) {
                return new DateTimeFormatterBuilder().m21310(m21474()).m21310(m21475()).m21310(m21476()).m21298();
            }
            return f17142;
        }

        /* renamed from: ﹶﹶ  reason: contains not printable characters */
        private static DateTimeFormatter m21529() {
            if (f17132 == null) {
                return new DateTimeFormatterBuilder().m21310(m21474()).m21310(m21475()).m21310(m21476()).m21299('.').m21301(3, 3).m21298();
            }
            return f17132;
        }

        /* renamed from: ʻʼ  reason: contains not printable characters */
        private static DateTimeFormatter m21460() {
            if (f17136 == null) {
                return new DateTimeFormatterBuilder().m21310(m21474()).m21310(m21475()).m21310(m21476()).m21310(m21477()).m21298();
            }
            return f17136;
        }

        /* renamed from: ʻʽ  reason: contains not printable characters */
        private static DateTimeFormatter m21461() {
            if (f17144 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21438()).m21310(m21473()).m21310(ISODateTimeFormat.m21441()).m21298();
            }
            return f17144;
        }

        /* renamed from: ʻʾ  reason: contains not printable characters */
        private static DateTimeFormatter m21462() {
            if (f17148 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21438()).m21310(m21473()).m21310(m21527()).m21298();
            }
            return f17148;
        }

        /* renamed from: ʻʿ  reason: contains not printable characters */
        private static DateTimeFormatter m21463() {
            if (f17150 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21438()).m21310(m21473()).m21310(m21508()).m21298();
            }
            return f17150;
        }

        /* renamed from: ʻˆ  reason: contains not printable characters */
        private static DateTimeFormatter m21464() {
            if (f17156 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21438()).m21310(m21473()).m21310(m21529()).m21298();
            }
            return f17156;
        }

        /* renamed from: ʻˈ  reason: contains not printable characters */
        private static DateTimeFormatter m21465() {
            if (f17158 == null) {
                return new DateTimeFormatterBuilder().m21310(ISODateTimeFormat.m21438()).m21310(m21473()).m21310(m21460()).m21298();
            }
            return f17158;
        }

        /* renamed from: ʻˉ  reason: contains not printable characters */
        private static DateTimeFormatter m21466() {
            if (f17170 == null) {
                return new DateTimeFormatterBuilder().m21283(4, 9).m21298();
            }
            return f17170;
        }

        /* renamed from: ʻˊ  reason: contains not printable characters */
        private static DateTimeFormatter m21467() {
            if (f17167 == null) {
                return new DateTimeFormatterBuilder().m21299('-').m21274(2).m21298();
            }
            return f17167;
        }

        /* renamed from: ʻˋ  reason: contains not printable characters */
        private static DateTimeFormatter m21468() {
            if (f17169 == null) {
                return new DateTimeFormatterBuilder().m21299('-').m21276(2).m21298();
            }
            return f17169;
        }

        /* renamed from: ʻˎ  reason: contains not printable characters */
        private static DateTimeFormatter m21469() {
            if (f17168 == null) {
                return new DateTimeFormatterBuilder().m21293(4, 9).m21298();
            }
            return f17168;
        }

        /* renamed from: ʻˏ  reason: contains not printable characters */
        private static DateTimeFormatter m21470() {
            if (f17166 == null) {
                return new DateTimeFormatterBuilder().m21303("-W").m21280(2).m21298();
            }
            return f17166;
        }

        /* renamed from: ʻˑ  reason: contains not printable characters */
        private static DateTimeFormatter m21471() {
            if (f17118 == null) {
                return new DateTimeFormatterBuilder().m21299('-').m21273(1).m21298();
            }
            return f17118;
        }

        /* renamed from: ʻי  reason: contains not printable characters */
        private static DateTimeFormatter m21472() {
            if (f17124 == null) {
                return new DateTimeFormatterBuilder().m21299('-').m21278(3).m21298();
            }
            return f17124;
        }

        /* renamed from: ʻـ  reason: contains not printable characters */
        private static DateTimeFormatter m21473() {
            if (f17128 == null) {
                return new DateTimeFormatterBuilder().m21299('T').m21298();
            }
            return f17128;
        }

        /* renamed from: ʻٴ  reason: contains not printable characters */
        private static DateTimeFormatter m21474() {
            if (f17126 == null) {
                return new DateTimeFormatterBuilder().m21292(2).m21298();
            }
            return f17126;
        }

        /* renamed from: ʻᐧ  reason: contains not printable characters */
        private static DateTimeFormatter m21475() {
            if (f17146 == null) {
                return new DateTimeFormatterBuilder().m21299(':').m21294(2).m21298();
            }
            return f17146;
        }

        /* renamed from: ʻᴵ  reason: contains not printable characters */
        private static DateTimeFormatter m21476() {
            if (f17152 == null) {
                return new DateTimeFormatterBuilder().m21299(':').m21284(2).m21298();
            }
            return f17152;
        }

        /* renamed from: ʻᵎ  reason: contains not printable characters */
        private static DateTimeFormatter m21477() {
            if (f17154 == null) {
                return new DateTimeFormatterBuilder().m21299('.').m21301(3, 9).m21298();
            }
            return f17154;
        }

        /* renamed from: ʻᵔ  reason: contains not printable characters */
        private static DateTimeFormatter m21478() {
            if (f17134 == null) {
                return new DateTimeFormatterBuilder().m21305("Z", true, 2, 4).m21298();
            }
            return f17134;
        }
    }
}
