package org.joda.time.format;

class InternalParserDateTimeParser implements DateTimeParser, InternalParser {

    /* renamed from: 龘  reason: contains not printable characters */
    private final InternalParser f17178;

    /* renamed from: 龘  reason: contains not printable characters */
    static DateTimeParser m21535(InternalParser internalParser) {
        if (internalParser instanceof DateTimeParserInternalParser) {
            return ((DateTimeParserInternalParser) internalParser).m21418();
        }
        if (internalParser instanceof DateTimeParser) {
            return (DateTimeParser) internalParser;
        }
        if (internalParser == null) {
            return null;
        }
        return new InternalParserDateTimeParser(internalParser);
    }

    private InternalParserDateTimeParser(InternalParser internalParser) {
        this.f17178 = internalParser;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m21536() {
        return this.f17178.m21533();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21537(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
        return this.f17178.m21534(dateTimeParserBucket, charSequence, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21538(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
        return this.f17178.m21534(dateTimeParserBucket, str, i);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof InternalParserDateTimeParser) {
            return this.f17178.equals(((InternalParserDateTimeParser) obj).f17178);
        }
        return false;
    }
}
