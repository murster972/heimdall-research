package org.joda.time.format;

class DateTimeParserInternalParser implements InternalParser {

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeParser f17115;

    /* renamed from: 龘  reason: contains not printable characters */
    static InternalParser m21415(DateTimeParser dateTimeParser) {
        if (dateTimeParser instanceof InternalParserDateTimeParser) {
            return (InternalParser) dateTimeParser;
        }
        if (dateTimeParser == null) {
            return null;
        }
        return new DateTimeParserInternalParser(dateTimeParser);
    }

    private DateTimeParserInternalParser(DateTimeParser dateTimeParser) {
        this.f17115 = dateTimeParser;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeParser m21418() {
        return this.f17115;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m21416() {
        return this.f17115.m21381();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21417(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
        return this.f17115.m21382(dateTimeParserBucket, charSequence.toString(), i);
    }
}
