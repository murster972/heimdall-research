package org.joda.time;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Weeks extends BaseSingleFieldPeriod {
    public static final Weeks MAX_VALUE = new Weeks(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    public static final Weeks MIN_VALUE = new Weeks(Integer.MIN_VALUE);
    public static final Weeks ONE = new Weeks(1);
    public static final Weeks THREE = new Weeks(3);
    public static final Weeks TWO = new Weeks(2);
    public static final Weeks ZERO = new Weeks(0);
    private static final long serialVersionUID = 87525275727380866L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final PeriodFormatter f6502 = ISOPeriodFormat.m21532().m21557(PeriodType.weeks());

    private Weeks(int i) {
        super(i);
    }

    @FromString
    public static Weeks parseWeeks(String str) {
        return str == null ? ZERO : weeks(f6502.m21556(str).getWeeks());
    }

    private Object readResolve() {
        return weeks(m20950());
    }

    public static Weeks standardWeeksIn(ReadablePeriod readablePeriod) {
        return weeks(BaseSingleFieldPeriod.m20949(readablePeriod, 604800000));
    }

    public static Weeks weeks(int i) {
        switch (i) {
            case Integer.MIN_VALUE:
                return MIN_VALUE;
            case 0:
                return ZERO;
            case 1:
                return ONE;
            case 2:
                return TWO;
            case 3:
                return THREE;
            case MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT /*2147483647*/:
                return MAX_VALUE;
            default:
                return new Weeks(i);
        }
    }

    public static Weeks weeksBetween(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        return weeks(BaseSingleFieldPeriod.m20947(readableInstant, readableInstant2, DurationFieldType.weeks()));
    }

    public static Weeks weeksBetween(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        return (!(readablePartial instanceof LocalDate) || !(readablePartial2 instanceof LocalDate)) ? weeks(BaseSingleFieldPeriod.m20948(readablePartial, readablePartial2, (ReadablePeriod) ZERO)) : weeks(DateTimeUtils.m20895(readablePartial.getChronology()).weeks().getDifference(((LocalDate) readablePartial2).m7304(), ((LocalDate) readablePartial).m7304()));
    }

    public static Weeks weeksIn(ReadableInterval readableInterval) {
        return readableInterval == null ? ZERO : weeks(BaseSingleFieldPeriod.m20947((ReadableInstant) readableInterval.getStart(), (ReadableInstant) readableInterval.getEnd(), DurationFieldType.weeks()));
    }

    public Weeks dividedBy(int i) {
        return i == 1 ? this : weeks(m20950() / i);
    }

    public DurationFieldType getFieldType() {
        return DurationFieldType.weeks();
    }

    public PeriodType getPeriodType() {
        return PeriodType.weeks();
    }

    public int getWeeks() {
        return m20950();
    }

    public boolean isGreaterThan(Weeks weeks) {
        return weeks == null ? m20950() > 0 : m20950() > weeks.m20950();
    }

    public boolean isLessThan(Weeks weeks) {
        return weeks == null ? m20950() < 0 : m20950() < weeks.m20950();
    }

    public Weeks minus(int i) {
        return plus(FieldUtils.m21209(i));
    }

    public Weeks minus(Weeks weeks) {
        return weeks == null ? this : minus(weeks.m20950());
    }

    public Weeks multipliedBy(int i) {
        return weeks(FieldUtils.m21205(m20950(), i));
    }

    public Weeks negated() {
        return weeks(FieldUtils.m21209(m20950()));
    }

    public Weeks plus(int i) {
        return i == 0 ? this : weeks(FieldUtils.m21210(m20950(), i));
    }

    public Weeks plus(Weeks weeks) {
        return weeks == null ? this : plus(weeks.m20950());
    }

    public Days toStandardDays() {
        return Days.days(FieldUtils.m21205(m20950(), 7));
    }

    public Duration toStandardDuration() {
        return new Duration(((long) m20950()) * 604800000);
    }

    public Hours toStandardHours() {
        return Hours.hours(FieldUtils.m21205(m20950(), 168));
    }

    public Minutes toStandardMinutes() {
        return Minutes.minutes(FieldUtils.m21205(m20950(), 10080));
    }

    public Seconds toStandardSeconds() {
        return Seconds.seconds(FieldUtils.m21205(m20950(), 604800));
    }

    @ToString
    public String toString() {
        return "P" + String.valueOf(m20950()) + "W";
    }
}
