package org.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseLocal;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.PartialConverter;
import org.joda.time.field.AbstractReadableInstantFieldProperty;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class LocalDateTime extends BaseLocal implements Serializable, ReadablePartial {
    private static final long serialVersionUID = -268716875315837168L;
    private final Chronology iChronology;
    private final long iLocalMillis;

    public LocalDateTime() {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance());
    }

    public LocalDateTime(int i, int i2, int i3, int i4, int i5) {
        this(i, i2, i3, i4, i5, 0, 0, ISOChronology.getInstanceUTC());
    }

    public LocalDateTime(int i, int i2, int i3, int i4, int i5, int i6) {
        this(i, i2, i3, i4, i5, i6, 0, ISOChronology.getInstanceUTC());
    }

    public LocalDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this(i, i2, i3, i4, i5, i6, i7, ISOChronology.getInstanceUTC());
    }

    public LocalDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7, Chronology chronology) {
        Chronology withUTC = DateTimeUtils.m20895(chronology).withUTC();
        long dateTimeMillis = withUTC.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        this.iChronology = withUTC;
        this.iLocalMillis = dateTimeMillis;
    }

    public LocalDateTime(long j) {
        this(j, (Chronology) ISOChronology.getInstance());
    }

    public LocalDateTime(long j, Chronology chronology) {
        Chronology r0 = DateTimeUtils.m20895(chronology);
        this.iLocalMillis = r0.getZone().getMillisKeepLocal(DateTimeZone.UTC, j);
        this.iChronology = r0.withUTC();
    }

    public LocalDateTime(long j, DateTimeZone dateTimeZone) {
        this(j, (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public LocalDateTime(Object obj) {
        this(obj, (Chronology) null);
    }

    public LocalDateTime(Object obj, Chronology chronology) {
        PartialConverter r0 = ConverterManager.m21146().m21148(obj);
        Chronology r1 = DateTimeUtils.m20895(r0.m21171(obj, chronology));
        this.iChronology = r1.withUTC();
        int[] r02 = r0.m21173(this, obj, r1, ISODateTimeFormat.m21437());
        this.iLocalMillis = this.iChronology.getDateTimeMillis(r02[0], r02[1], r02[2], r02[3]);
    }

    public LocalDateTime(Object obj, DateTimeZone dateTimeZone) {
        PartialConverter r0 = ConverterManager.m21146().m21148(obj);
        Chronology r1 = DateTimeUtils.m20895(r0.m21172(obj, dateTimeZone));
        this.iChronology = r1.withUTC();
        int[] r02 = r0.m21173(this, obj, r1, ISODateTimeFormat.m21437());
        this.iLocalMillis = this.iChronology.getDateTimeMillis(r02[0], r02[1], r02[2], r02[3]);
    }

    public LocalDateTime(Chronology chronology) {
        this(DateTimeUtils.m20891(), chronology);
    }

    public LocalDateTime(DateTimeZone dateTimeZone) {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public static LocalDateTime fromCalendarFields(Calendar calendar) {
        if (calendar == null) {
            throw new IllegalArgumentException("The calendar must not be null");
        }
        int i = calendar.get(0);
        int i2 = calendar.get(1);
        if (i != 1) {
            i2 = 1 - i2;
        }
        return new LocalDateTime(i2, calendar.get(2) + 1, calendar.get(5), calendar.get(11), calendar.get(12), calendar.get(13), calendar.get(14));
    }

    public static LocalDateTime fromDateFields(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date.getTime() >= 0) {
            return new LocalDateTime(date.getYear() + 1900, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), (((int) (date.getTime() % 1000)) + 1000) % 1000);
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(date);
            return fromCalendarFields(gregorianCalendar);
        }
    }

    public static LocalDateTime now() {
        return new LocalDateTime();
    }

    public static LocalDateTime now(Chronology chronology) {
        if (chronology != null) {
            return new LocalDateTime(chronology);
        }
        throw new NullPointerException("Chronology must not be null");
    }

    public static LocalDateTime now(DateTimeZone dateTimeZone) {
        if (dateTimeZone != null) {
            return new LocalDateTime(dateTimeZone);
        }
        throw new NullPointerException("Zone must not be null");
    }

    @FromString
    public static LocalDateTime parse(String str) {
        return parse(str, ISODateTimeFormat.m21437());
    }

    public static LocalDateTime parse(String str, DateTimeFormatter dateTimeFormatter) {
        return dateTimeFormatter.m21241(str);
    }

    private Object readResolve() {
        return this.iChronology == null ? new LocalDateTime(this.iLocalMillis, (Chronology) ISOChronology.getInstanceUTC()) : !DateTimeZone.UTC.equals(this.iChronology.getZone()) ? new LocalDateTime(this.iLocalMillis, this.iChronology.withUTC()) : this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0069, code lost:
        if (fromCalendarFields(r0).equals(r8) == false) goto L_0x006b;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Date m7307(java.util.Date r9, java.util.TimeZone r10) {
        /*
            r8 = this;
            r6 = 1000(0x3e8, double:4.94E-321)
            java.util.Calendar r1 = java.util.Calendar.getInstance(r10)
            r1.setTime(r9)
            org.joda.time.LocalDateTime r0 = fromCalendarFields(r1)
            boolean r2 = r0.isBefore(r8)
            if (r2 == 0) goto L_0x004a
        L_0x0013:
            boolean r2 = r0.isBefore(r8)
            if (r2 == 0) goto L_0x0029
            long r2 = r1.getTimeInMillis()
            r4 = 60000(0xea60, double:2.9644E-319)
            long r2 = r2 + r4
            r1.setTimeInMillis(r2)
            org.joda.time.LocalDateTime r0 = fromCalendarFields(r1)
            goto L_0x0013
        L_0x0029:
            boolean r0 = r0.isBefore(r8)
            if (r0 != 0) goto L_0x003c
            long r2 = r1.getTimeInMillis()
            long r2 = r2 - r6
            r1.setTimeInMillis(r2)
            org.joda.time.LocalDateTime r0 = fromCalendarFields(r1)
            goto L_0x0029
        L_0x003c:
            long r2 = r1.getTimeInMillis()
            long r2 = r2 + r6
            r1.setTimeInMillis(r2)
            r0 = r1
        L_0x0045:
            java.util.Date r0 = r0.getTime()
            return r0
        L_0x004a:
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x006b
            java.util.Calendar r0 = java.util.Calendar.getInstance(r10)
            long r2 = r1.getTimeInMillis()
            int r4 = r10.getDSTSavings()
            long r4 = (long) r4
            long r2 = r2 - r4
            r0.setTimeInMillis(r2)
            org.joda.time.LocalDateTime r2 = fromCalendarFields(r0)
            boolean r2 = r2.equals(r8)
            if (r2 != 0) goto L_0x0045
        L_0x006b:
            r0 = r1
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: org.joda.time.LocalDateTime.m7307(java.util.Date, java.util.TimeZone):java.util.Date");
    }

    public Property centuryOfEra() {
        return new Property(this, getChronology().centuryOfEra());
    }

    public int compareTo(ReadablePartial readablePartial) {
        if (this == readablePartial) {
            return 0;
        }
        if (readablePartial instanceof LocalDateTime) {
            LocalDateTime localDateTime = (LocalDateTime) readablePartial;
            if (this.iChronology.equals(localDateTime.iChronology)) {
                return this.iLocalMillis < localDateTime.iLocalMillis ? -1 : this.iLocalMillis == localDateTime.iLocalMillis ? 0 : 1;
            }
        }
        return super.compareTo(readablePartial);
    }

    public Property dayOfMonth() {
        return new Property(this, getChronology().dayOfMonth());
    }

    public Property dayOfWeek() {
        return new Property(this, getChronology().dayOfWeek());
    }

    public Property dayOfYear() {
        return new Property(this, getChronology().dayOfYear());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof LocalDateTime) {
            LocalDateTime localDateTime = (LocalDateTime) obj;
            if (this.iChronology.equals(localDateTime.iChronology)) {
                return this.iLocalMillis == localDateTime.iLocalMillis;
            }
        }
        return super.equals(obj);
    }

    public Property era() {
        return new Property(this, getChronology().era());
    }

    public int get(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return dateTimeFieldType.getField(getChronology()).get(m7308());
        }
        throw new IllegalArgumentException("The DateTimeFieldType must not be null");
    }

    public int getCenturyOfEra() {
        return getChronology().centuryOfEra().get(m7308());
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public int getDayOfMonth() {
        return getChronology().dayOfMonth().get(m7308());
    }

    public int getDayOfWeek() {
        return getChronology().dayOfWeek().get(m7308());
    }

    public int getDayOfYear() {
        return getChronology().dayOfYear().get(m7308());
    }

    public int getEra() {
        return getChronology().era().get(m7308());
    }

    public int getHourOfDay() {
        return getChronology().hourOfDay().get(m7308());
    }

    public int getMillisOfDay() {
        return getChronology().millisOfDay().get(m7308());
    }

    public int getMillisOfSecond() {
        return getChronology().millisOfSecond().get(m7308());
    }

    public int getMinuteOfHour() {
        return getChronology().minuteOfHour().get(m7308());
    }

    public int getMonthOfYear() {
        return getChronology().monthOfYear().get(m7308());
    }

    public int getSecondOfMinute() {
        return getChronology().secondOfMinute().get(m7308());
    }

    public int getValue(int i) {
        switch (i) {
            case 0:
                return getChronology().year().get(m7308());
            case 1:
                return getChronology().monthOfYear().get(m7308());
            case 2:
                return getChronology().dayOfMonth().get(m7308());
            case 3:
                return getChronology().millisOfDay().get(m7308());
            default:
                throw new IndexOutOfBoundsException("Invalid index: " + i);
        }
    }

    public int getWeekOfWeekyear() {
        return getChronology().weekOfWeekyear().get(m7308());
    }

    public int getWeekyear() {
        return getChronology().weekyear().get(m7308());
    }

    public int getYear() {
        return getChronology().year().get(m7308());
    }

    public int getYearOfCentury() {
        return getChronology().yearOfCentury().get(m7308());
    }

    public int getYearOfEra() {
        return getChronology().yearOfEra().get(m7308());
    }

    public Property hourOfDay() {
        return new Property(this, getChronology().hourOfDay());
    }

    public boolean isSupported(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            return false;
        }
        return dateTimeFieldType.getField(getChronology()).isSupported();
    }

    public boolean isSupported(DurationFieldType durationFieldType) {
        if (durationFieldType == null) {
            return false;
        }
        return durationFieldType.getField(getChronology()).isSupported();
    }

    public Property millisOfDay() {
        return new Property(this, getChronology().millisOfDay());
    }

    public Property millisOfSecond() {
        return new Property(this, getChronology().millisOfSecond());
    }

    public LocalDateTime minus(ReadableDuration readableDuration) {
        return withDurationAdded(readableDuration, -1);
    }

    public LocalDateTime minus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, -1);
    }

    public LocalDateTime minusDays(int i) {
        return i == 0 ? this : m7310(getChronology().days().subtract(m7308(), i));
    }

    public LocalDateTime minusHours(int i) {
        return i == 0 ? this : m7310(getChronology().hours().subtract(m7308(), i));
    }

    public LocalDateTime minusMillis(int i) {
        return i == 0 ? this : m7310(getChronology().millis().subtract(m7308(), i));
    }

    public LocalDateTime minusMinutes(int i) {
        return i == 0 ? this : m7310(getChronology().minutes().subtract(m7308(), i));
    }

    public LocalDateTime minusMonths(int i) {
        return i == 0 ? this : m7310(getChronology().months().subtract(m7308(), i));
    }

    public LocalDateTime minusSeconds(int i) {
        return i == 0 ? this : m7310(getChronology().seconds().subtract(m7308(), i));
    }

    public LocalDateTime minusWeeks(int i) {
        return i == 0 ? this : m7310(getChronology().weeks().subtract(m7308(), i));
    }

    public LocalDateTime minusYears(int i) {
        return i == 0 ? this : m7310(getChronology().years().subtract(m7308(), i));
    }

    public Property minuteOfHour() {
        return new Property(this, getChronology().minuteOfHour());
    }

    public Property monthOfYear() {
        return new Property(this, getChronology().monthOfYear());
    }

    public LocalDateTime plus(ReadableDuration readableDuration) {
        return withDurationAdded(readableDuration, 1);
    }

    public LocalDateTime plus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, 1);
    }

    public LocalDateTime plusDays(int i) {
        return i == 0 ? this : m7310(getChronology().days().add(m7308(), i));
    }

    public LocalDateTime plusHours(int i) {
        return i == 0 ? this : m7310(getChronology().hours().add(m7308(), i));
    }

    public LocalDateTime plusMillis(int i) {
        return i == 0 ? this : m7310(getChronology().millis().add(m7308(), i));
    }

    public LocalDateTime plusMinutes(int i) {
        return i == 0 ? this : m7310(getChronology().minutes().add(m7308(), i));
    }

    public LocalDateTime plusMonths(int i) {
        return i == 0 ? this : m7310(getChronology().months().add(m7308(), i));
    }

    public LocalDateTime plusSeconds(int i) {
        return i == 0 ? this : m7310(getChronology().seconds().add(m7308(), i));
    }

    public LocalDateTime plusWeeks(int i) {
        return i == 0 ? this : m7310(getChronology().weeks().add(m7308(), i));
    }

    public LocalDateTime plusYears(int i) {
        return i == 0 ? this : m7310(getChronology().years().add(m7308(), i));
    }

    public Property property(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("The DateTimeFieldType must not be null");
        } else if (isSupported(dateTimeFieldType)) {
            return new Property(this, dateTimeFieldType.getField(getChronology()));
        } else {
            throw new IllegalArgumentException("Field '" + dateTimeFieldType + "' is not supported");
        }
    }

    public Property secondOfMinute() {
        return new Property(this, getChronology().secondOfMinute());
    }

    public int size() {
        return 4;
    }

    public Date toDate() {
        Date date = new Date(getYear() - 1900, getMonthOfYear() - 1, getDayOfMonth(), getHourOfDay(), getMinuteOfHour(), getSecondOfMinute());
        date.setTime(date.getTime() + ((long) getMillisOfSecond()));
        return m7307(date, TimeZone.getDefault());
    }

    public Date toDate(TimeZone timeZone) {
        Calendar instance = Calendar.getInstance(timeZone);
        instance.clear();
        instance.set(getYear(), getMonthOfYear() - 1, getDayOfMonth(), getHourOfDay(), getMinuteOfHour(), getSecondOfMinute());
        Date time = instance.getTime();
        time.setTime(time.getTime() + ((long) getMillisOfSecond()));
        return m7307(time, timeZone);
    }

    public DateTime toDateTime() {
        return toDateTime((DateTimeZone) null);
    }

    public DateTime toDateTime(DateTimeZone dateTimeZone) {
        return new DateTime(getYear(), getMonthOfYear(), getDayOfMonth(), getHourOfDay(), getMinuteOfHour(), getSecondOfMinute(), getMillisOfSecond(), this.iChronology.withZone(DateTimeUtils.m20898(dateTimeZone)));
    }

    public LocalDate toLocalDate() {
        return new LocalDate(m7308(), getChronology());
    }

    public LocalTime toLocalTime() {
        return new LocalTime(m7308(), getChronology());
    }

    @ToString
    public String toString() {
        return ISODateTimeFormat.m21444().m21246((ReadablePartial) this);
    }

    public String toString(String str) {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21246((ReadablePartial) this);
    }

    public String toString(String str, Locale locale) throws IllegalArgumentException {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21247(locale).m21246((ReadablePartial) this);
    }

    public Property weekOfWeekyear() {
        return new Property(this, getChronology().weekOfWeekyear());
    }

    public Property weekyear() {
        return new Property(this, getChronology().weekyear());
    }

    public LocalDateTime withCenturyOfEra(int i) {
        return m7310(getChronology().centuryOfEra().set(m7308(), i));
    }

    public LocalDateTime withDate(int i, int i2, int i3) {
        Chronology chronology = getChronology();
        return m7310(chronology.dayOfMonth().set(chronology.monthOfYear().set(chronology.year().set(m7308(), i), i2), i3));
    }

    public LocalDateTime withDayOfMonth(int i) {
        return m7310(getChronology().dayOfMonth().set(m7308(), i));
    }

    public LocalDateTime withDayOfWeek(int i) {
        return m7310(getChronology().dayOfWeek().set(m7308(), i));
    }

    public LocalDateTime withDayOfYear(int i) {
        return m7310(getChronology().dayOfYear().set(m7308(), i));
    }

    public LocalDateTime withDurationAdded(ReadableDuration readableDuration, int i) {
        return (readableDuration == null || i == 0) ? this : m7310(getChronology().add(m7308(), readableDuration.getMillis(), i));
    }

    public LocalDateTime withEra(int i) {
        return m7310(getChronology().era().set(m7308(), i));
    }

    public LocalDateTime withField(DateTimeFieldType dateTimeFieldType, int i) {
        if (dateTimeFieldType != null) {
            return m7310(dateTimeFieldType.getField(getChronology()).set(m7308(), i));
        }
        throw new IllegalArgumentException("Field must not be null");
    }

    public LocalDateTime withFieldAdded(DurationFieldType durationFieldType, int i) {
        if (durationFieldType != null) {
            return i == 0 ? this : m7310(durationFieldType.getField(getChronology()).add(m7308(), i));
        }
        throw new IllegalArgumentException("Field must not be null");
    }

    public LocalDateTime withFields(ReadablePartial readablePartial) {
        return readablePartial == null ? this : m7310(getChronology().set(readablePartial, m7308()));
    }

    public LocalDateTime withHourOfDay(int i) {
        return m7310(getChronology().hourOfDay().set(m7308(), i));
    }

    public LocalDateTime withMillisOfDay(int i) {
        return m7310(getChronology().millisOfDay().set(m7308(), i));
    }

    public LocalDateTime withMillisOfSecond(int i) {
        return m7310(getChronology().millisOfSecond().set(m7308(), i));
    }

    public LocalDateTime withMinuteOfHour(int i) {
        return m7310(getChronology().minuteOfHour().set(m7308(), i));
    }

    public LocalDateTime withMonthOfYear(int i) {
        return m7310(getChronology().monthOfYear().set(m7308(), i));
    }

    public LocalDateTime withPeriodAdded(ReadablePeriod readablePeriod, int i) {
        return (readablePeriod == null || i == 0) ? this : m7310(getChronology().add(readablePeriod, m7308(), i));
    }

    public LocalDateTime withSecondOfMinute(int i) {
        return m7310(getChronology().secondOfMinute().set(m7308(), i));
    }

    public LocalDateTime withTime(int i, int i2, int i3, int i4) {
        Chronology chronology = getChronology();
        return m7310(chronology.millisOfSecond().set(chronology.secondOfMinute().set(chronology.minuteOfHour().set(chronology.hourOfDay().set(m7308(), i), i2), i3), i4));
    }

    public LocalDateTime withWeekOfWeekyear(int i) {
        return m7310(getChronology().weekOfWeekyear().set(m7308(), i));
    }

    public LocalDateTime withWeekyear(int i) {
        return m7310(getChronology().weekyear().set(m7308(), i));
    }

    public LocalDateTime withYear(int i) {
        return m7310(getChronology().year().set(m7308(), i));
    }

    public LocalDateTime withYearOfCentury(int i) {
        return m7310(getChronology().yearOfCentury().set(m7308(), i));
    }

    public LocalDateTime withYearOfEra(int i) {
        return m7310(getChronology().yearOfEra().set(m7308(), i));
    }

    public Property year() {
        return new Property(this, getChronology().year());
    }

    public Property yearOfCentury() {
        return new Property(this, getChronology().yearOfCentury());
    }

    public Property yearOfEra() {
        return new Property(this, getChronology().yearOfEra());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m7308() {
        return this.iLocalMillis;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeField m7309(int i, Chronology chronology) {
        switch (i) {
            case 0:
                return chronology.year();
            case 1:
                return chronology.monthOfYear();
            case 2:
                return chronology.dayOfMonth();
            case 3:
                return chronology.millisOfDay();
            default:
                throw new IndexOutOfBoundsException("Invalid index: " + i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public LocalDateTime m7310(long j) {
        return j == m7308() ? this : new LocalDateTime(j, getChronology());
    }

    public static final class Property extends AbstractReadableInstantFieldProperty {
        private static final long serialVersionUID = -358138762846288L;

        /* renamed from: 靐  reason: contains not printable characters */
        private transient DateTimeField f16800;

        /* renamed from: 龘  reason: contains not printable characters */
        private transient LocalDateTime f16801;

        Property(LocalDateTime localDateTime, DateTimeField dateTimeField) {
            this.f16801 = localDateTime;
            this.f16800 = dateTimeField;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeObject(this.f16801);
            objectOutputStream.writeObject(this.f16800.getType());
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            this.f16801 = (LocalDateTime) objectInputStream.readObject();
            this.f16800 = ((DateTimeFieldType) objectInputStream.readObject()).getField(this.f16801.getChronology());
        }

        public DateTimeField getField() {
            return this.f16800;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m20912() {
            return this.f16801.m7308();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public Chronology m20911() {
            return this.f16801.getChronology();
        }

        public LocalDateTime getLocalDateTime() {
            return this.f16801;
        }

        public LocalDateTime addToCopy(int i) {
            return this.f16801.m7310(this.f16800.add(this.f16801.m7308(), i));
        }

        public LocalDateTime addToCopy(long j) {
            return this.f16801.m7310(this.f16800.add(this.f16801.m7308(), j));
        }

        public LocalDateTime addWrapFieldToCopy(int i) {
            return this.f16801.m7310(this.f16800.addWrapField(this.f16801.m7308(), i));
        }

        public LocalDateTime setCopy(int i) {
            return this.f16801.m7310(this.f16800.set(this.f16801.m7308(), i));
        }

        public LocalDateTime setCopy(String str, Locale locale) {
            return this.f16801.m7310(this.f16800.set(this.f16801.m7308(), str, locale));
        }

        public LocalDateTime setCopy(String str) {
            return setCopy(str, (Locale) null);
        }

        public LocalDateTime withMaximumValue() {
            return setCopy(getMaximumValue());
        }

        public LocalDateTime withMinimumValue() {
            return setCopy(getMinimumValue());
        }

        public LocalDateTime roundFloorCopy() {
            return this.f16801.m7310(this.f16800.roundFloor(this.f16801.m7308()));
        }

        public LocalDateTime roundCeilingCopy() {
            return this.f16801.m7310(this.f16800.roundCeiling(this.f16801.m7308()));
        }

        public LocalDateTime roundHalfFloorCopy() {
            return this.f16801.m7310(this.f16800.roundHalfFloor(this.f16801.m7308()));
        }

        public LocalDateTime roundHalfCeilingCopy() {
            return this.f16801.m7310(this.f16800.roundHalfCeiling(this.f16801.m7308()));
        }

        public LocalDateTime roundHalfEvenCopy() {
            return this.f16801.m7310(this.f16800.roundHalfEven(this.f16801.m7308()));
        }
    }
}
