package org.joda.time;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Minutes extends BaseSingleFieldPeriod {
    public static final Minutes MAX_VALUE = new Minutes(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    public static final Minutes MIN_VALUE = new Minutes(Integer.MIN_VALUE);
    public static final Minutes ONE = new Minutes(1);
    public static final Minutes THREE = new Minutes(3);
    public static final Minutes TWO = new Minutes(2);
    public static final Minutes ZERO = new Minutes(0);
    private static final long serialVersionUID = 87525275727380863L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final PeriodFormatter f6496 = ISOPeriodFormat.m21532().m21557(PeriodType.minutes());

    private Minutes(int i) {
        super(i);
    }

    public static Minutes minutes(int i) {
        switch (i) {
            case Integer.MIN_VALUE:
                return MIN_VALUE;
            case 0:
                return ZERO;
            case 1:
                return ONE;
            case 2:
                return TWO;
            case 3:
                return THREE;
            case MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT /*2147483647*/:
                return MAX_VALUE;
            default:
                return new Minutes(i);
        }
    }

    public static Minutes minutesBetween(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        return minutes(BaseSingleFieldPeriod.m20947(readableInstant, readableInstant2, DurationFieldType.minutes()));
    }

    public static Minutes minutesBetween(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        return (!(readablePartial instanceof LocalTime) || !(readablePartial2 instanceof LocalTime)) ? minutes(BaseSingleFieldPeriod.m20948(readablePartial, readablePartial2, (ReadablePeriod) ZERO)) : minutes(DateTimeUtils.m20895(readablePartial.getChronology()).minutes().getDifference(((LocalTime) readablePartial2).m7311(), ((LocalTime) readablePartial).m7311()));
    }

    public static Minutes minutesIn(ReadableInterval readableInterval) {
        return readableInterval == null ? ZERO : minutes(BaseSingleFieldPeriod.m20947((ReadableInstant) readableInterval.getStart(), (ReadableInstant) readableInterval.getEnd(), DurationFieldType.minutes()));
    }

    @FromString
    public static Minutes parseMinutes(String str) {
        return str == null ? ZERO : minutes(f6496.m21556(str).getMinutes());
    }

    private Object readResolve() {
        return minutes(m20950());
    }

    public static Minutes standardMinutesIn(ReadablePeriod readablePeriod) {
        return minutes(BaseSingleFieldPeriod.m20949(readablePeriod, 60000));
    }

    public Minutes dividedBy(int i) {
        return i == 1 ? this : minutes(m20950() / i);
    }

    public DurationFieldType getFieldType() {
        return DurationFieldType.minutes();
    }

    public int getMinutes() {
        return m20950();
    }

    public PeriodType getPeriodType() {
        return PeriodType.minutes();
    }

    public boolean isGreaterThan(Minutes minutes) {
        return minutes == null ? m20950() > 0 : m20950() > minutes.m20950();
    }

    public boolean isLessThan(Minutes minutes) {
        return minutes == null ? m20950() < 0 : m20950() < minutes.m20950();
    }

    public Minutes minus(int i) {
        return plus(FieldUtils.m21209(i));
    }

    public Minutes minus(Minutes minutes) {
        return minutes == null ? this : minus(minutes.m20950());
    }

    public Minutes multipliedBy(int i) {
        return minutes(FieldUtils.m21205(m20950(), i));
    }

    public Minutes negated() {
        return minutes(FieldUtils.m21209(m20950()));
    }

    public Minutes plus(int i) {
        return i == 0 ? this : minutes(FieldUtils.m21210(m20950(), i));
    }

    public Minutes plus(Minutes minutes) {
        return minutes == null ? this : plus(minutes.m20950());
    }

    public Days toStandardDays() {
        return Days.days(m20950() / 1440);
    }

    public Duration toStandardDuration() {
        return new Duration(((long) m20950()) * 60000);
    }

    public Hours toStandardHours() {
        return Hours.hours(m20950() / 60);
    }

    public Seconds toStandardSeconds() {
        return Seconds.seconds(FieldUtils.m21205(m20950(), 60));
    }

    public Weeks toStandardWeeks() {
        return Weeks.weeks(m20950() / 10080);
    }

    @ToString
    public String toString() {
        return "PT" + String.valueOf(m20950()) + "M";
    }
}
