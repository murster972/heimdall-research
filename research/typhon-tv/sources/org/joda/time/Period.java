package org.joda.time;

import java.io.Serializable;
import org.joda.convert.FromString;
import org.joda.time.base.BasePeriod;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Period extends BasePeriod implements Serializable, ReadablePeriod {
    public static final Period ZERO = new Period();
    private static final long serialVersionUID = 741052353876488155L;

    public Period() {
        super(0, (PeriodType) null, (Chronology) null);
    }

    public Period(int i, int i2, int i3, int i4) {
        super(0, 0, 0, 0, i, i2, i3, i4, PeriodType.standard());
    }

    public Period(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        super(i, i2, i3, i4, i5, i6, i7, i8, PeriodType.standard());
    }

    public Period(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, PeriodType periodType) {
        super(i, i2, i3, i4, i5, i6, i7, i8, periodType);
    }

    public Period(long j) {
        super(j);
    }

    public Period(long j, long j2) {
        super(j, j2, (PeriodType) null, (Chronology) null);
    }

    public Period(long j, long j2, Chronology chronology) {
        super(j, j2, (PeriodType) null, chronology);
    }

    public Period(long j, long j2, PeriodType periodType) {
        super(j, j2, periodType, (Chronology) null);
    }

    public Period(long j, long j2, PeriodType periodType, Chronology chronology) {
        super(j, j2, periodType, chronology);
    }

    public Period(long j, Chronology chronology) {
        super(j, (PeriodType) null, chronology);
    }

    public Period(long j, PeriodType periodType) {
        super(j, periodType, (Chronology) null);
    }

    public Period(long j, PeriodType periodType, Chronology chronology) {
        super(j, periodType, chronology);
    }

    public Period(Object obj) {
        super(obj, (PeriodType) null, (Chronology) null);
    }

    public Period(Object obj, Chronology chronology) {
        super(obj, (PeriodType) null, chronology);
    }

    public Period(Object obj, PeriodType periodType) {
        super(obj, periodType, (Chronology) null);
    }

    public Period(Object obj, PeriodType periodType, Chronology chronology) {
        super(obj, periodType, chronology);
    }

    public Period(ReadableDuration readableDuration, ReadableInstant readableInstant) {
        super(readableDuration, readableInstant, (PeriodType) null);
    }

    public Period(ReadableDuration readableDuration, ReadableInstant readableInstant, PeriodType periodType) {
        super(readableDuration, readableInstant, periodType);
    }

    public Period(ReadableInstant readableInstant, ReadableDuration readableDuration) {
        super(readableInstant, readableDuration, (PeriodType) null);
    }

    public Period(ReadableInstant readableInstant, ReadableDuration readableDuration, PeriodType periodType) {
        super(readableInstant, readableDuration, periodType);
    }

    public Period(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        super(readableInstant, readableInstant2, (PeriodType) null);
    }

    public Period(ReadableInstant readableInstant, ReadableInstant readableInstant2, PeriodType periodType) {
        super(readableInstant, readableInstant2, periodType);
    }

    public Period(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        super(readablePartial, readablePartial2, (PeriodType) null);
    }

    public Period(ReadablePartial readablePartial, ReadablePartial readablePartial2, PeriodType periodType) {
        super(readablePartial, readablePartial2, periodType);
    }

    private Period(int[] iArr, PeriodType periodType) {
        super(iArr, periodType);
    }

    public static Period days(int i) {
        return new Period(new int[]{0, 0, 0, i, 0, 0, 0, 0}, PeriodType.standard());
    }

    public static Period fieldDifference(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        if (readablePartial == null || readablePartial2 == null) {
            throw new IllegalArgumentException("ReadablePartial objects must not be null");
        } else if (readablePartial.size() != readablePartial2.size()) {
            throw new IllegalArgumentException("ReadablePartial objects must have the same set of fields");
        } else {
            DurationFieldType[] durationFieldTypeArr = new DurationFieldType[readablePartial.size()];
            int[] iArr = new int[readablePartial.size()];
            int i = 0;
            int size = readablePartial.size();
            while (i < size) {
                if (readablePartial.getFieldType(i) != readablePartial2.getFieldType(i)) {
                    throw new IllegalArgumentException("ReadablePartial objects must have the same set of fields");
                }
                durationFieldTypeArr[i] = readablePartial.getFieldType(i).getDurationType();
                if (i <= 0 || durationFieldTypeArr[i - 1] != durationFieldTypeArr[i]) {
                    iArr[i] = readablePartial2.getValue(i) - readablePartial.getValue(i);
                    i++;
                } else {
                    throw new IllegalArgumentException("ReadablePartial objects must not have overlapping fields");
                }
            }
            return new Period(iArr, PeriodType.forFields(durationFieldTypeArr));
        }
    }

    public static Period hours(int i) {
        return new Period(new int[]{0, 0, 0, 0, i, 0, 0, 0}, PeriodType.standard());
    }

    public static Period millis(int i) {
        return new Period(new int[]{0, 0, 0, 0, 0, 0, 0, i}, PeriodType.standard());
    }

    public static Period minutes(int i) {
        return new Period(new int[]{0, 0, 0, 0, 0, i, 0, 0}, PeriodType.standard());
    }

    public static Period months(int i) {
        return new Period(new int[]{0, i, 0, 0, 0, 0, 0, 0}, PeriodType.standard());
    }

    @FromString
    public static Period parse(String str) {
        return parse(str, ISOPeriodFormat.m21532());
    }

    public static Period parse(String str, PeriodFormatter periodFormatter) {
        return periodFormatter.m21556(str);
    }

    public static Period seconds(int i) {
        return new Period(new int[]{0, 0, 0, 0, 0, 0, i, 0}, PeriodType.standard());
    }

    public static Period weeks(int i) {
        return new Period(new int[]{0, 0, i, 0, 0, 0, 0, 0}, PeriodType.standard());
    }

    public static Period years(int i) {
        return new Period(new int[]{i, 0, 0, 0, 0, 0, 0, 0, 0}, PeriodType.standard());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7315(String str) {
        if (getMonths() != 0) {
            throw new UnsupportedOperationException("Cannot convert to " + str + " as this period contains months and months vary in length");
        } else if (getYears() != 0) {
            throw new UnsupportedOperationException("Cannot convert to " + str + " as this period contains years and years vary in length");
        }
    }

    public int getDays() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16826);
    }

    public int getHours() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16824);
    }

    public int getMillis() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16807);
    }

    public int getMinutes() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16805);
    }

    public int getMonths() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16825);
    }

    public int getSeconds() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16806);
    }

    public int getWeeks() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16827);
    }

    public int getYears() {
        return getPeriodType().m20922((ReadablePeriod) this, PeriodType.f16828);
    }

    public Period minus(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16828, values, -readablePeriod.get(DurationFieldType.f16795));
        getPeriodType().m20921(this, PeriodType.f16825, values, -readablePeriod.get(DurationFieldType.f16793));
        getPeriodType().m20921(this, PeriodType.f16827, values, -readablePeriod.get(DurationFieldType.f16786));
        getPeriodType().m20921(this, PeriodType.f16826, values, -readablePeriod.get(DurationFieldType.f16787));
        getPeriodType().m20921(this, PeriodType.f16824, values, -readablePeriod.get(DurationFieldType.f16790));
        getPeriodType().m20921(this, PeriodType.f16805, values, -readablePeriod.get(DurationFieldType.f16791));
        getPeriodType().m20921(this, PeriodType.f16806, values, -readablePeriod.get(DurationFieldType.f16792));
        getPeriodType().m20921(this, PeriodType.f16807, values, -readablePeriod.get(DurationFieldType.f16789));
        return new Period(values, getPeriodType());
    }

    public Period minusDays(int i) {
        return plusDays(-i);
    }

    public Period minusHours(int i) {
        return plusHours(-i);
    }

    public Period minusMillis(int i) {
        return plusMillis(-i);
    }

    public Period minusMinutes(int i) {
        return plusMinutes(-i);
    }

    public Period minusMonths(int i) {
        return plusMonths(-i);
    }

    public Period minusSeconds(int i) {
        return plusSeconds(-i);
    }

    public Period minusWeeks(int i) {
        return plusWeeks(-i);
    }

    public Period minusYears(int i) {
        return plusYears(-i);
    }

    public Period multipliedBy(int i) {
        if (this == ZERO || i == 1) {
            return this;
        }
        int[] values = getValues();
        for (int i2 = 0; i2 < values.length; i2++) {
            values[i2] = FieldUtils.m21205(values[i2], i);
        }
        return new Period(values, getPeriodType());
    }

    public Period negated() {
        return multipliedBy(-1);
    }

    public Period normalizedStandard() {
        return normalizedStandard(PeriodType.standard());
    }

    public Period normalizedStandard(PeriodType periodType) {
        PeriodType r3 = DateTimeUtils.m20899(periodType);
        Period period = new Period(((long) getMillis()) + (((long) getSeconds()) * 1000) + (((long) getMinutes()) * 60000) + (((long) getHours()) * 3600000) + (((long) getDays()) * 86400000) + (((long) getWeeks()) * 604800000), r3, (Chronology) ISOChronology.getInstanceUTC());
        int years = getYears();
        int months = getMonths();
        if (!(years == 0 && months == 0)) {
            long j = ((long) months) + (((long) years) * 12);
            if (r3.isSupported(DurationFieldType.f16795)) {
                int r4 = FieldUtils.m21213(j / 12);
                period = period.withYears(r4);
                j -= (long) (r4 * 12);
            }
            if (r3.isSupported(DurationFieldType.f16793)) {
                int r32 = FieldUtils.m21213(j);
                period = period.withMonths(r32);
                j -= (long) r32;
            }
            if (j != 0) {
                throw new UnsupportedOperationException("Unable to normalize as PeriodType is missing either years or months but period has a month/year amount: " + toString());
            }
        }
        return period;
    }

    public Period plus(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16828, values, readablePeriod.get(DurationFieldType.f16795));
        getPeriodType().m20921(this, PeriodType.f16825, values, readablePeriod.get(DurationFieldType.f16793));
        getPeriodType().m20921(this, PeriodType.f16827, values, readablePeriod.get(DurationFieldType.f16786));
        getPeriodType().m20921(this, PeriodType.f16826, values, readablePeriod.get(DurationFieldType.f16787));
        getPeriodType().m20921(this, PeriodType.f16824, values, readablePeriod.get(DurationFieldType.f16790));
        getPeriodType().m20921(this, PeriodType.f16805, values, readablePeriod.get(DurationFieldType.f16791));
        getPeriodType().m20921(this, PeriodType.f16806, values, readablePeriod.get(DurationFieldType.f16792));
        getPeriodType().m20921(this, PeriodType.f16807, values, readablePeriod.get(DurationFieldType.f16789));
        return new Period(values, getPeriodType());
    }

    public Period plusDays(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16826, values, i);
        return new Period(values, getPeriodType());
    }

    public Period plusHours(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16824, values, i);
        return new Period(values, getPeriodType());
    }

    public Period plusMillis(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16807, values, i);
        return new Period(values, getPeriodType());
    }

    public Period plusMinutes(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16805, values, i);
        return new Period(values, getPeriodType());
    }

    public Period plusMonths(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16825, values, i);
        return new Period(values, getPeriodType());
    }

    public Period plusSeconds(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16806, values, i);
        return new Period(values, getPeriodType());
    }

    public Period plusWeeks(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16827, values, i);
        return new Period(values, getPeriodType());
    }

    public Period plusYears(int i) {
        if (i == 0) {
            return this;
        }
        int[] values = getValues();
        getPeriodType().m20921(this, PeriodType.f16828, values, i);
        return new Period(values, getPeriodType());
    }

    public Period toPeriod() {
        return this;
    }

    public Days toStandardDays() {
        m7315("Days");
        return Days.days(FieldUtils.m21213(FieldUtils.m21215(FieldUtils.m21215((((((long) getMillis()) + (((long) getSeconds()) * 1000)) + (((long) getMinutes()) * 60000)) + (((long) getHours()) * 3600000)) / 86400000, (long) getDays()), ((long) getWeeks()) * 7)));
    }

    public Duration toStandardDuration() {
        m7315("Duration");
        return new Duration(((long) getMillis()) + (((long) getSeconds()) * 1000) + (((long) getMinutes()) * 60000) + (((long) getHours()) * 3600000) + (((long) getDays()) * 86400000) + (((long) getWeeks()) * 604800000));
    }

    public Hours toStandardHours() {
        m7315("Hours");
        return Hours.hours(FieldUtils.m21213(FieldUtils.m21215(FieldUtils.m21215(FieldUtils.m21215(((((long) getMillis()) + (((long) getSeconds()) * 1000)) + (((long) getMinutes()) * 60000)) / 3600000, (long) getHours()), ((long) getDays()) * 24), ((long) getWeeks()) * 168)));
    }

    public Minutes toStandardMinutes() {
        m7315("Minutes");
        return Minutes.minutes(FieldUtils.m21213(FieldUtils.m21215(FieldUtils.m21215(FieldUtils.m21215(FieldUtils.m21215((((long) getMillis()) + (((long) getSeconds()) * 1000)) / 60000, (long) getMinutes()), ((long) getHours()) * 60), ((long) getDays()) * 1440), ((long) getWeeks()) * 10080)));
    }

    public Seconds toStandardSeconds() {
        m7315("Seconds");
        return Seconds.seconds(FieldUtils.m21213(FieldUtils.m21215(FieldUtils.m21215(FieldUtils.m21215(FieldUtils.m21215(FieldUtils.m21215((long) (getMillis() / 1000), (long) getSeconds()), ((long) getMinutes()) * 60), ((long) getHours()) * 3600), ((long) getDays()) * 86400), ((long) getWeeks()) * 604800)));
    }

    public Weeks toStandardWeeks() {
        m7315("Weeks");
        return Weeks.weeks(FieldUtils.m21213((((((((long) getMillis()) + (((long) getSeconds()) * 1000)) + (((long) getMinutes()) * 60000)) + (((long) getHours()) * 3600000)) + (((long) getDays()) * 86400000)) / 604800000) + ((long) getWeeks())));
    }

    public Period withDays(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16826, values, i);
        return new Period(values, getPeriodType());
    }

    public Period withField(DurationFieldType durationFieldType, int i) {
        if (durationFieldType == null) {
            throw new IllegalArgumentException("Field must not be null");
        }
        int[] values = getValues();
        super.m20945(values, durationFieldType, i);
        return new Period(values, getPeriodType());
    }

    public Period withFieldAdded(DurationFieldType durationFieldType, int i) {
        if (durationFieldType == null) {
            throw new IllegalArgumentException("Field must not be null");
        } else if (i == 0) {
            return this;
        } else {
            int[] values = getValues();
            super.m20939(values, durationFieldType, i);
            return new Period(values, getPeriodType());
        }
    }

    public Period withFields(ReadablePeriod readablePeriod) {
        return readablePeriod == null ? this : new Period(super.m20946(getValues(), readablePeriod), getPeriodType());
    }

    public Period withHours(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16824, values, i);
        return new Period(values, getPeriodType());
    }

    public Period withMillis(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16807, values, i);
        return new Period(values, getPeriodType());
    }

    public Period withMinutes(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16805, values, i);
        return new Period(values, getPeriodType());
    }

    public Period withMonths(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16825, values, i);
        return new Period(values, getPeriodType());
    }

    public Period withPeriodType(PeriodType periodType) {
        PeriodType r1 = DateTimeUtils.m20899(periodType);
        return r1.equals(getPeriodType()) ? this : new Period((Object) this, r1);
    }

    public Period withSeconds(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16806, values, i);
        return new Period(values, getPeriodType());
    }

    public Period withWeeks(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16827, values, i);
        return new Period(values, getPeriodType());
    }

    public Period withYears(int i) {
        int[] values = getValues();
        getPeriodType().m20923(this, PeriodType.f16828, values, i);
        return new Period(values, getPeriodType());
    }
}
