package org.joda.time;

import java.io.Serializable;
import java.math.RoundingMode;
import org.joda.convert.FromString;
import org.joda.time.base.BaseDuration;
import org.joda.time.field.FieldUtils;

public final class Duration extends BaseDuration implements Serializable, ReadableDuration {
    public static final Duration ZERO = new Duration(0);
    private static final long serialVersionUID = 2471658376918L;

    public Duration(long j) {
        super(j);
    }

    public Duration(long j, long j2) {
        super(j, j2);
    }

    public Duration(Object obj) {
        super(obj);
    }

    public Duration(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        super(readableInstant, readableInstant2);
    }

    public static Duration millis(long j) {
        return j == 0 ? ZERO : new Duration(j);
    }

    @FromString
    public static Duration parse(String str) {
        return new Duration((Object) str);
    }

    public static Duration standardDays(long j) {
        return j == 0 ? ZERO : new Duration(FieldUtils.m21214(j, 86400000));
    }

    public static Duration standardHours(long j) {
        return j == 0 ? ZERO : new Duration(FieldUtils.m21214(j, 3600000));
    }

    public static Duration standardMinutes(long j) {
        return j == 0 ? ZERO : new Duration(FieldUtils.m21214(j, 60000));
    }

    public static Duration standardSeconds(long j) {
        return j == 0 ? ZERO : new Duration(FieldUtils.m21214(j, 1000));
    }

    public Duration dividedBy(long j) {
        return j == 1 ? this : new Duration(FieldUtils.m21207(getMillis(), j));
    }

    public Duration dividedBy(long j, RoundingMode roundingMode) {
        return j == 1 ? this : new Duration(FieldUtils.m21216(getMillis(), j, roundingMode));
    }

    public long getStandardDays() {
        return getMillis() / 86400000;
    }

    public long getStandardHours() {
        return getMillis() / 3600000;
    }

    public long getStandardMinutes() {
        return getMillis() / 60000;
    }

    public long getStandardSeconds() {
        return getMillis() / 1000;
    }

    public Duration minus(long j) {
        return withDurationAdded(j, -1);
    }

    public Duration minus(ReadableDuration readableDuration) {
        return readableDuration == null ? this : withDurationAdded(readableDuration.getMillis(), -1);
    }

    public Duration multipliedBy(long j) {
        return j == 1 ? this : new Duration(FieldUtils.m21208(getMillis(), j));
    }

    public Duration negated() {
        if (getMillis() != Long.MIN_VALUE) {
            return new Duration(-getMillis());
        }
        throw new ArithmeticException("Negation of this duration would overflow");
    }

    public Duration plus(long j) {
        return withDurationAdded(j, 1);
    }

    public Duration plus(ReadableDuration readableDuration) {
        return readableDuration == null ? this : withDurationAdded(readableDuration.getMillis(), 1);
    }

    public Duration toDuration() {
        return this;
    }

    public Days toStandardDays() {
        return Days.days(FieldUtils.m21213(getStandardDays()));
    }

    public Hours toStandardHours() {
        return Hours.hours(FieldUtils.m21213(getStandardHours()));
    }

    public Minutes toStandardMinutes() {
        return Minutes.minutes(FieldUtils.m21213(getStandardMinutes()));
    }

    public Seconds toStandardSeconds() {
        return Seconds.seconds(FieldUtils.m21213(getStandardSeconds()));
    }

    public Duration withDurationAdded(long j, int i) {
        if (j == 0 || i == 0) {
            return this;
        }
        return new Duration(FieldUtils.m21215(getMillis(), FieldUtils.m21214(j, i)));
    }

    public Duration withDurationAdded(ReadableDuration readableDuration, int i) {
        return (readableDuration == null || i == 0) ? this : withDurationAdded(readableDuration.getMillis(), i);
    }

    public Duration withMillis(long j) {
        return j == getMillis() ? this : new Duration(j);
    }
}
