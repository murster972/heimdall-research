package org.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.SkipDateTimeField;

public final class JulianChronology extends BasicGJChronology {
    private static final long serialVersionUID = -8731039522547897247L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, JulianChronology[]> f16988 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final JulianChronology f16989 = getInstance(DateTimeZone.UTC);

    /* renamed from: ˑ  reason: contains not printable characters */
    static int m21107(int i) {
        if (i > 0) {
            return i;
        }
        if (i != 0) {
            return i + 1;
        }
        throw new IllegalFieldValueException(DateTimeFieldType.year(), (Number) Integer.valueOf(i), (Number) null, (Number) null);
    }

    public static JulianChronology getInstanceUTC() {
        return f16989;
    }

    public static JulianChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), 4);
    }

    public static JulianChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, 4);
    }

    public static JulianChronology getInstance(DateTimeZone dateTimeZone, int i) {
        JulianChronology[] julianChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        JulianChronology[] julianChronologyArr2 = f16988.get(dateTimeZone);
        if (julianChronologyArr2 == null) {
            julianChronologyArr = new JulianChronology[7];
            JulianChronology[] putIfAbsent = f16988.putIfAbsent(dateTimeZone, julianChronologyArr);
            if (putIfAbsent != null) {
                julianChronologyArr = putIfAbsent;
            }
        } else {
            julianChronologyArr = julianChronologyArr2;
        }
        try {
            JulianChronology julianChronology = julianChronologyArr[i - 1];
            if (julianChronology == null) {
                synchronized (julianChronologyArr) {
                    julianChronology = julianChronologyArr[i - 1];
                    if (julianChronology == null) {
                        if (dateTimeZone == DateTimeZone.UTC) {
                            julianChronology = new JulianChronology((Chronology) null, (Object) null, i);
                        } else {
                            julianChronology = new JulianChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, i), dateTimeZone), (Object) null, i);
                        }
                        julianChronologyArr[i - 1] = julianChronology;
                    }
                }
            }
            return julianChronology;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
    }

    JulianChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    private Object readResolve() {
        Chronology r1 = m20953();
        int minimumDaysInFirstWeek = getMinimumDaysInFirstWeek();
        if (minimumDaysInFirstWeek == 0) {
            minimumDaysInFirstWeek = 4;
        }
        return r1 == null ? getInstance(DateTimeZone.UTC, minimumDaysInFirstWeek) : getInstance(r1.getZone(), minimumDaysInFirstWeek);
    }

    public Chronology withUTC() {
        return f16989;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public long m21116(int i, int i2, int i3) throws IllegalArgumentException {
        return super.m20985(m21107(i), i2, i3);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m21115(int i) {
        return (i & 3) == 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m21109(int i) {
        int i2;
        int i3 = i - 1968;
        if (i3 <= 0) {
            i2 = (i3 + 3) >> 2;
        } else {
            i2 = i3 >> 2;
            if (!m21115(i)) {
                i2++;
            }
        }
        return ((((long) i2) + (((long) i3) * 365)) * 86400000) - 62035200000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m21114() {
        return -292269054;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m21108() {
        return 292272992;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m21110() {
        return 31557600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m21111() {
        return 15778800000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m21112() {
        return 2629800000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m21113() {
        return 31083663600000L;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21117(AssembledChronology.Fields fields) {
        if (m20953() == null) {
            super.m21002(fields);
            fields.f16903 = new SkipDateTimeField(this, fields.f16903);
            fields.f16893 = new SkipDateTimeField(this, fields.f16893);
        }
    }
}
