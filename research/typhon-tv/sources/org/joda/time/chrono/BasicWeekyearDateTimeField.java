package org.joda.time.chrono;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.ImpreciseDateTimeField;

final class BasicWeekyearDateTimeField extends ImpreciseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f16936;

    BasicWeekyearDateTimeField(BasicChronology basicChronology) {
        super(DateTimeFieldType.weekyear(), basicChronology.m20971());
        this.f16936 = basicChronology;
    }

    public boolean isLenient() {
        return false;
    }

    public int get(long j) {
        return this.f16936.m20978(j);
    }

    public long add(long j, int i) {
        return i == 0 ? j : set(j, get(j) + i);
    }

    public long add(long j, long j2) {
        return add(j, FieldUtils.m21213(j2));
    }

    public long addWrapField(long j, int i) {
        return add(j, i);
    }

    public long getDifferenceAsLong(long j, long j2) {
        long j3;
        if (j < j2) {
            return (long) (-getDifference(j2, j));
        }
        int i = get(j);
        int i2 = get(j2);
        long remainder = remainder(j);
        long remainder2 = remainder(j2);
        if (remainder2 < 31449600000L || this.f16936.m20981(i) > 52) {
            j3 = remainder2;
        } else {
            j3 = remainder2 - 604800000;
        }
        int i3 = i - i2;
        if (remainder < j3) {
            i3--;
        }
        return (long) i3;
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, Math.abs(i), this.f16936.m20977(), this.f16936.m20962());
        int i2 = get(j);
        if (i2 == i) {
            return j;
        }
        int r4 = this.f16936.m20967(j);
        int r1 = this.f16936.m20981(i2);
        int r0 = this.f16936.m20981(i);
        if (r0 >= r1) {
            r0 = r1;
        }
        int r12 = this.f16936.m20964(j);
        if (r12 <= r0) {
            r0 = r12;
        }
        long r2 = this.f16936.m20965(j, i);
        int i3 = get(r2);
        if (i3 < i) {
            r2 += 604800000;
        } else if (i3 > i) {
            r2 -= 604800000;
        }
        return this.f16936.dayOfWeek().set((((long) (r0 - this.f16936.m20964(r2))) * 604800000) + r2, r4);
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public boolean isLeap(long j) {
        return this.f16936.m20981(this.f16936.m20978(j)) > 52;
    }

    public int getLeapAmount(long j) {
        return this.f16936.m20981(this.f16936.m20978(j)) - 52;
    }

    public DurationField getLeapDurationField() {
        return this.f16936.weeks();
    }

    public int getMinimumValue() {
        return this.f16936.m20977();
    }

    public int getMaximumValue() {
        return this.f16936.m20962();
    }

    public long roundFloor(long j) {
        long roundFloor = this.f16936.weekOfWeekyear().roundFloor(j);
        int r2 = this.f16936.m20964(roundFloor);
        if (r2 > 1) {
            return roundFloor - (((long) (r2 - 1)) * 604800000);
        }
        return roundFloor;
    }

    public long remainder(long j) {
        return j - roundFloor(j);
    }
}
