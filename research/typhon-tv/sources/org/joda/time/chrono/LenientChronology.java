package org.joda.time.chrono;

import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.LenientDateTimeField;

public final class LenientChronology extends AssembledChronology {
    private static final long serialVersionUID = -3148237568046877177L;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient Chronology f16990;

    public static LenientChronology getInstance(Chronology chronology) {
        if (chronology != null) {
            return new LenientChronology(chronology);
        }
        throw new IllegalArgumentException("Must supply a chronology");
    }

    private LenientChronology(Chronology chronology) {
        super(chronology, (Object) null);
    }

    public Chronology withUTC() {
        if (this.f16990 == null) {
            if (getZone() == DateTimeZone.UTC) {
                this.f16990 = this;
            } else {
                this.f16990 = getInstance(m20953().withUTC());
            }
        }
        return this.f16990;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        if (dateTimeZone == DateTimeZone.UTC) {
            return withUTC();
        }
        return dateTimeZone != getZone() ? getInstance(m20953().withZone(dateTimeZone)) : this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21119(AssembledChronology.Fields fields) {
        fields.f16903 = m21118(fields.f16903);
        fields.f16888 = m21118(fields.f16888);
        fields.f16890 = m21118(fields.f16890);
        fields.f16870 = m21118(fields.f16870);
        fields.f16873 = m21118(fields.f16873);
        fields.f16885 = m21118(fields.f16885);
        fields.f16889 = m21118(fields.f16889);
        fields.f16891 = m21118(fields.f16891);
        fields.f16900 = m21118(fields.f16900);
        fields.f16892 = m21118(fields.f16892);
        fields.f16893 = m21118(fields.f16893);
        fields.f16894 = m21118(fields.f16894);
        fields.f16874 = m21118(fields.f16874);
        fields.f16875 = m21118(fields.f16875);
        fields.f16901 = m21118(fields.f16901);
        fields.f16902 = m21118(fields.f16902);
        fields.f16879 = m21118(fields.f16879);
        fields.f16880 = m21118(fields.f16880);
        fields.f16881 = m21118(fields.f16881);
        fields.f16878 = m21118(fields.f16878);
        fields.f16876 = m21118(fields.f16876);
        fields.f16882 = m21118(fields.f16882);
        fields.f16884 = m21118(fields.f16884);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeField m21118(DateTimeField dateTimeField) {
        return LenientDateTimeField.getInstance(dateTimeField, m20953());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LenientChronology)) {
            return false;
        }
        return m20953().equals(((LenientChronology) obj).m20953());
    }

    public int hashCode() {
        return 236548278 + (m20953().hashCode() * 7);
    }

    public String toString() {
        return "LenientChronology[" + m20953().toString() + ']';
    }
}
