package org.joda.time.chrono;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.Instant;
import org.joda.time.LocalDate;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.BaseDateTimeField;
import org.joda.time.field.DecoratedDurationField;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class GJChronology extends AssembledChronology {
    private static final long serialVersionUID = -2545574827706931671L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<GJCacheKey, GJChronology> f16950 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    static final Instant f16951 = new Instant(-12219292800000L);
    private Instant iCutoverInstant;
    private long iCutoverMillis;
    /* access modifiers changed from: private */
    public long iGapDuration;
    /* access modifiers changed from: private */
    public GregorianChronology iGregorianChronology;
    private JulianChronology iJulianChronology;

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m21043(long j, Chronology chronology, Chronology chronology2) {
        return chronology2.getDateTimeMillis(chronology.year().get(j), chronology.monthOfYear().get(j), chronology.dayOfMonth().get(j), chronology.millisOfDay().get(j));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static long m21041(long j, Chronology chronology, Chronology chronology2) {
        return chronology2.millisOfDay().set(chronology2.dayOfWeek().set(chronology2.weekOfWeekyear().set(chronology2.weekyear().set(0, chronology.weekyear().get(j)), chronology.weekOfWeekyear().get(j)), chronology.dayOfWeek().get(j)), chronology.millisOfDay().get(j));
    }

    public static GJChronology getInstanceUTC() {
        return getInstance(DateTimeZone.UTC, (ReadableInstant) f16951, 4);
    }

    public static GJChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), (ReadableInstant) f16951, 4);
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, (ReadableInstant) f16951, 4);
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone, ReadableInstant readableInstant) {
        return getInstance(dateTimeZone, readableInstant, 4);
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone, ReadableInstant readableInstant, int i) {
        Instant instant;
        GJChronology gJChronology;
        DateTimeZone r2 = DateTimeUtils.m20898(dateTimeZone);
        if (readableInstant == null) {
            instant = f16951;
        } else {
            Instant instant2 = readableInstant.toInstant();
            if (new LocalDate(instant2.getMillis(), (Chronology) GregorianChronology.getInstance(r2)).getYear() <= 0) {
                throw new IllegalArgumentException("Cutover too early. Must be on or after 0001-01-01.");
            }
            instant = instant2;
        }
        GJCacheKey gJCacheKey = new GJCacheKey(r2, instant, i);
        GJChronology gJChronology2 = f16950.get(gJCacheKey);
        if (gJChronology2 != null) {
            return gJChronology2;
        }
        if (r2 == DateTimeZone.UTC) {
            gJChronology = new GJChronology(JulianChronology.getInstance(r2, i), GregorianChronology.getInstance(r2, i), instant);
        } else {
            GJChronology instance = getInstance(DateTimeZone.UTC, (ReadableInstant) instant, i);
            gJChronology = new GJChronology(ZonedChronology.getInstance(instance, r2), instance.iJulianChronology, instance.iGregorianChronology, instance.iCutoverInstant);
        }
        GJChronology putIfAbsent = f16950.putIfAbsent(gJCacheKey, gJChronology);
        if (putIfAbsent != null) {
            return putIfAbsent;
        }
        return gJChronology;
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone, long j, int i) {
        Instant instant;
        if (j == f16951.getMillis()) {
            instant = null;
        } else {
            instant = new Instant(j);
        }
        return getInstance(dateTimeZone, (ReadableInstant) instant, i);
    }

    private GJChronology(JulianChronology julianChronology, GregorianChronology gregorianChronology, Instant instant) {
        super((Chronology) null, new Object[]{julianChronology, gregorianChronology, instant});
    }

    private GJChronology(Chronology chronology, JulianChronology julianChronology, GregorianChronology gregorianChronology, Instant instant) {
        super(chronology, new Object[]{julianChronology, gregorianChronology, instant});
    }

    private Object readResolve() {
        return getInstance(getZone(), (ReadableInstant) this.iCutoverInstant, getMinimumDaysInFirstWeek());
    }

    public DateTimeZone getZone() {
        Chronology r0 = m20953();
        if (r0 != null) {
            return r0.getZone();
        }
        return DateTimeZone.UTC;
    }

    public Chronology withUTC() {
        return withZone(DateTimeZone.UTC);
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone, (ReadableInstant) this.iCutoverInstant, getMinimumDaysInFirstWeek());
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology r0 = m20953();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4);
        }
        long dateTimeMillis = this.iGregorianChronology.getDateTimeMillis(i, i2, i3, i4);
        if (dateTimeMillis >= this.iCutoverMillis) {
            return dateTimeMillis;
        }
        long dateTimeMillis2 = this.iJulianChronology.getDateTimeMillis(i, i2, i3, i4);
        if (dateTimeMillis2 < this.iCutoverMillis) {
            return dateTimeMillis2;
        }
        throw new IllegalArgumentException("Specified date does not exist");
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        long dateTimeMillis;
        Chronology r0 = m20953();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        }
        try {
            dateTimeMillis = this.iGregorianChronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        } catch (IllegalFieldValueException e) {
            IllegalFieldValueException illegalFieldValueException = e;
            if (i2 == 2 && i3 == 29) {
                dateTimeMillis = this.iGregorianChronology.getDateTimeMillis(i, i2, 28, i4, i5, i6, i7);
                if (dateTimeMillis >= this.iCutoverMillis) {
                    throw illegalFieldValueException;
                }
            } else {
                throw illegalFieldValueException;
            }
        }
        if (dateTimeMillis >= this.iCutoverMillis) {
            return dateTimeMillis;
        }
        long dateTimeMillis2 = this.iJulianChronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        if (dateTimeMillis2 < this.iCutoverMillis) {
            return dateTimeMillis2;
        }
        throw new IllegalArgumentException("Specified date does not exist");
    }

    public Instant getGregorianCutover() {
        return this.iCutoverInstant;
    }

    public int getMinimumDaysInFirstWeek() {
        return this.iGregorianChronology.getMinimumDaysInFirstWeek();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GJChronology)) {
            return false;
        }
        GJChronology gJChronology = (GJChronology) obj;
        if (this.iCutoverMillis == gJChronology.iCutoverMillis && getMinimumDaysInFirstWeek() == gJChronology.getMinimumDaysInFirstWeek() && getZone().equals(gJChronology.getZone())) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ("GJ".hashCode() * 11) + getZone().hashCode() + getMinimumDaysInFirstWeek() + this.iCutoverInstant.hashCode();
    }

    public String toString() {
        DateTimeFormatter r0;
        StringBuffer stringBuffer = new StringBuffer(60);
        stringBuffer.append("GJChronology");
        stringBuffer.append('[');
        stringBuffer.append(getZone().getID());
        if (this.iCutoverMillis != f16951.getMillis()) {
            stringBuffer.append(",cutover=");
            if (withUTC().dayOfYear().remainder(this.iCutoverMillis) == 0) {
                r0 = ISODateTimeFormat.m21438();
            } else {
                r0 = ISODateTimeFormat.m21444();
            }
            r0.m21248(withUTC()).m21254(stringBuffer, this.iCutoverMillis);
        }
        if (getMinimumDaysInFirstWeek() != 4) {
            stringBuffer.append(",mdfw=");
            stringBuffer.append(getMinimumDaysInFirstWeek());
        }
        stringBuffer.append(']');
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21049(AssembledChronology.Fields fields) {
        Object[] objArr = (Object[]) m20952();
        JulianChronology julianChronology = (JulianChronology) objArr[0];
        GregorianChronology gregorianChronology = (GregorianChronology) objArr[1];
        Instant instant = (Instant) objArr[2];
        this.iCutoverMillis = instant.getMillis();
        this.iJulianChronology = julianChronology;
        this.iGregorianChronology = gregorianChronology;
        this.iCutoverInstant = instant;
        if (m20953() == null) {
            if (julianChronology.getMinimumDaysInFirstWeek() != gregorianChronology.getMinimumDaysInFirstWeek()) {
                throw new IllegalArgumentException();
            }
            this.iGapDuration = this.iCutoverMillis - m21048(this.iCutoverMillis);
            fields.m20957((Chronology) gregorianChronology);
            if (gregorianChronology.millisOfDay().get(this.iCutoverMillis) == 0) {
                fields.f16874 = new CutoverField(this, julianChronology.millisOfSecond(), fields.f16874, this.iCutoverMillis);
                fields.f16875 = new CutoverField(this, julianChronology.millisOfDay(), fields.f16875, this.iCutoverMillis);
                fields.f16901 = new CutoverField(this, julianChronology.secondOfMinute(), fields.f16901, this.iCutoverMillis);
                fields.f16902 = new CutoverField(this, julianChronology.secondOfDay(), fields.f16902, this.iCutoverMillis);
                fields.f16879 = new CutoverField(this, julianChronology.minuteOfHour(), fields.f16879, this.iCutoverMillis);
                fields.f16880 = new CutoverField(this, julianChronology.minuteOfDay(), fields.f16880, this.iCutoverMillis);
                fields.f16881 = new CutoverField(this, julianChronology.hourOfDay(), fields.f16881, this.iCutoverMillis);
                fields.f16878 = new CutoverField(this, julianChronology.hourOfHalfday(), fields.f16878, this.iCutoverMillis);
                fields.f16876 = new CutoverField(this, julianChronology.clockhourOfDay(), fields.f16876, this.iCutoverMillis);
                fields.f16882 = new CutoverField(this, julianChronology.clockhourOfHalfday(), fields.f16882, this.iCutoverMillis);
                fields.f16884 = new CutoverField(this, julianChronology.halfdayOfDay(), fields.f16884, this.iCutoverMillis);
            }
            fields.f16873 = new CutoverField(this, julianChronology.era(), fields.f16873, this.iCutoverMillis);
            fields.f16903 = new ImpreciseCutoverField(this, julianChronology.year(), fields.f16903, this.iCutoverMillis);
            fields.f16886 = fields.f16903.getDurationField();
            fields.f16888 = new ImpreciseCutoverField(this, julianChronology.yearOfEra(), fields.f16888, fields.f16886, this.iCutoverMillis);
            fields.f16870 = new ImpreciseCutoverField(this, julianChronology.centuryOfEra(), fields.f16870, this.iCutoverMillis);
            fields.f16887 = fields.f16870.getDurationField();
            fields.f16890 = new ImpreciseCutoverField(this, julianChronology.yearOfCentury(), fields.f16890, fields.f16886, fields.f16887, this.iCutoverMillis);
            fields.f16900 = new ImpreciseCutoverField(this, julianChronology.monthOfYear(), fields.f16900, (DurationField) null, fields.f16886, this.iCutoverMillis);
            fields.f16883 = fields.f16900.getDurationField();
            fields.f16893 = new ImpreciseCutoverField(julianChronology.weekyear(), fields.f16893, (DurationField) null, this.iCutoverMillis, true);
            fields.f16872 = fields.f16893.getDurationField();
            fields.f16894 = new ImpreciseCutoverField(this, julianChronology.weekyearOfCentury(), fields.f16894, fields.f16872, fields.f16887, this.iCutoverMillis);
            fields.f16891 = new CutoverField(julianChronology.dayOfYear(), fields.f16891, fields.f16886, gregorianChronology.year().roundCeiling(this.iCutoverMillis), false);
            fields.f16892 = new CutoverField(julianChronology.weekOfWeekyear(), fields.f16892, fields.f16872, gregorianChronology.weekyear().roundCeiling(this.iCutoverMillis), true);
            CutoverField cutoverField = new CutoverField(this, julianChronology.dayOfMonth(), fields.f16889, this.iCutoverMillis);
            cutoverField.f16952 = fields.f16883;
            fields.f16889 = cutoverField;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m21048(long j) {
        return m21043(j, this.iJulianChronology, this.iGregorianChronology);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public long m21045(long j) {
        return m21043(j, this.iGregorianChronology, this.iJulianChronology);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m21047(long j) {
        return m21041(j, this.iJulianChronology, this.iGregorianChronology);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public long m21046(long j) {
        return m21041(j, this.iGregorianChronology, this.iJulianChronology);
    }

    private class CutoverField extends BaseDateTimeField {

        /* renamed from: ʻ  reason: contains not printable characters */
        protected DurationField f16952;

        /* renamed from: 连任  reason: contains not printable characters */
        protected DurationField f16954;

        /* renamed from: 靐  reason: contains not printable characters */
        final DateTimeField f16955;

        /* renamed from: 麤  reason: contains not printable characters */
        final boolean f16956;

        /* renamed from: 齉  reason: contains not printable characters */
        final long f16957;

        /* renamed from: 龘  reason: contains not printable characters */
        final DateTimeField f16958;

        CutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, long j) {
            this(gJChronology, dateTimeField, dateTimeField2, j, false);
        }

        CutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, long j, boolean z) {
            this(dateTimeField, dateTimeField2, (DurationField) null, j, z);
        }

        CutoverField(DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, long j, boolean z) {
            super(dateTimeField2.getType());
            this.f16958 = dateTimeField;
            this.f16955 = dateTimeField2;
            this.f16957 = j;
            this.f16956 = z;
            this.f16954 = dateTimeField2.getDurationField();
            if (durationField == null && (durationField = dateTimeField2.getRangeDurationField()) == null) {
                durationField = dateTimeField.getRangeDurationField();
            }
            this.f16952 = durationField;
        }

        public boolean isLenient() {
            return false;
        }

        public int get(long j) {
            if (j >= this.f16957) {
                return this.f16955.get(j);
            }
            return this.f16958.get(j);
        }

        public String getAsText(long j, Locale locale) {
            if (j >= this.f16957) {
                return this.f16955.getAsText(j, locale);
            }
            return this.f16958.getAsText(j, locale);
        }

        public String getAsText(int i, Locale locale) {
            return this.f16955.getAsText(i, locale);
        }

        public String getAsShortText(long j, Locale locale) {
            if (j >= this.f16957) {
                return this.f16955.getAsShortText(j, locale);
            }
            return this.f16958.getAsShortText(j, locale);
        }

        public String getAsShortText(int i, Locale locale) {
            return this.f16955.getAsShortText(i, locale);
        }

        public long add(long j, int i) {
            return this.f16955.add(j, i);
        }

        public long add(long j, long j2) {
            return this.f16955.add(j, j2);
        }

        public int[] add(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
            if (i2 == 0) {
                return iArr;
            }
            if (!DateTimeUtils.m20901(readablePartial)) {
                return super.add(readablePartial, i, iArr, i2);
            }
            long j = 0;
            int size = readablePartial.size();
            for (int i3 = 0; i3 < size; i3++) {
                j = readablePartial.getFieldType(i3).getField(GJChronology.this).set(j, iArr[i3]);
            }
            return GJChronology.this.get(readablePartial, add(j, i2));
        }

        public int getDifference(long j, long j2) {
            return this.f16955.getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            return this.f16955.getDifferenceAsLong(j, j2);
        }

        public long set(long j, int i) {
            long j2;
            if (j >= this.f16957) {
                j2 = this.f16955.set(j, i);
                if (j2 < this.f16957) {
                    if (GJChronology.this.iGapDuration + j2 < this.f16957) {
                        j2 = m21050(j2);
                    }
                    if (get(j2) != i) {
                        throw new IllegalFieldValueException(this.f16955.getType(), (Number) Integer.valueOf(i), (Number) null, (Number) null);
                    }
                }
            } else {
                j2 = this.f16958.set(j, i);
                if (j2 >= this.f16957) {
                    if (j2 - GJChronology.this.iGapDuration >= this.f16957) {
                        j2 = m21051(j2);
                    }
                    if (get(j2) != i) {
                        throw new IllegalFieldValueException(this.f16958.getType(), (Number) Integer.valueOf(i), (Number) null, (Number) null);
                    }
                }
            }
            return j2;
        }

        public long set(long j, String str, Locale locale) {
            if (j >= this.f16957) {
                long j2 = this.f16955.set(j, str, locale);
                if (j2 >= this.f16957 || GJChronology.this.iGapDuration + j2 >= this.f16957) {
                    return j2;
                }
                return m21050(j2);
            }
            long j3 = this.f16958.set(j, str, locale);
            if (j3 < this.f16957 || j3 - GJChronology.this.iGapDuration < this.f16957) {
                return j3;
            }
            return m21051(j3);
        }

        public DurationField getDurationField() {
            return this.f16954;
        }

        public DurationField getRangeDurationField() {
            return this.f16952;
        }

        public boolean isLeap(long j) {
            if (j >= this.f16957) {
                return this.f16955.isLeap(j);
            }
            return this.f16958.isLeap(j);
        }

        public int getLeapAmount(long j) {
            if (j >= this.f16957) {
                return this.f16955.getLeapAmount(j);
            }
            return this.f16958.getLeapAmount(j);
        }

        public DurationField getLeapDurationField() {
            return this.f16955.getLeapDurationField();
        }

        public int getMinimumValue() {
            return this.f16958.getMinimumValue();
        }

        public int getMinimumValue(ReadablePartial readablePartial) {
            return this.f16958.getMinimumValue(readablePartial);
        }

        public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
            return this.f16958.getMinimumValue(readablePartial, iArr);
        }

        public int getMinimumValue(long j) {
            if (j < this.f16957) {
                return this.f16958.getMinimumValue(j);
            }
            int minimumValue = this.f16955.getMinimumValue(j);
            if (this.f16955.set(j, minimumValue) < this.f16957) {
                return this.f16955.get(this.f16957);
            }
            return minimumValue;
        }

        public int getMaximumValue() {
            return this.f16955.getMaximumValue();
        }

        public int getMaximumValue(long j) {
            if (j >= this.f16957) {
                return this.f16955.getMaximumValue(j);
            }
            int maximumValue = this.f16958.getMaximumValue(j);
            if (this.f16958.set(j, maximumValue) >= this.f16957) {
                return this.f16958.get(this.f16958.add(this.f16957, -1));
            }
            return maximumValue;
        }

        public int getMaximumValue(ReadablePartial readablePartial) {
            return getMaximumValue(GJChronology.getInstanceUTC().set(readablePartial, 0));
        }

        public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
            GJChronology instanceUTC = GJChronology.getInstanceUTC();
            long j = 0;
            int size = readablePartial.size();
            for (int i = 0; i < size; i++) {
                DateTimeField field = readablePartial.getFieldType(i).getField(instanceUTC);
                if (iArr[i] <= field.getMaximumValue(j)) {
                    j = field.set(j, iArr[i]);
                }
            }
            return getMaximumValue(j);
        }

        public long roundFloor(long j) {
            if (j < this.f16957) {
                return this.f16958.roundFloor(j);
            }
            long roundFloor = this.f16955.roundFloor(j);
            if (roundFloor >= this.f16957 || GJChronology.this.iGapDuration + roundFloor >= this.f16957) {
                return roundFloor;
            }
            return m21050(roundFloor);
        }

        public long roundCeiling(long j) {
            if (j >= this.f16957) {
                return this.f16955.roundCeiling(j);
            }
            long roundCeiling = this.f16958.roundCeiling(j);
            if (roundCeiling < this.f16957 || roundCeiling - GJChronology.this.iGapDuration < this.f16957) {
                return roundCeiling;
            }
            return m21051(roundCeiling);
        }

        public int getMaximumTextLength(Locale locale) {
            return Math.max(this.f16958.getMaximumTextLength(locale), this.f16955.getMaximumTextLength(locale));
        }

        public int getMaximumShortTextLength(Locale locale) {
            return Math.max(this.f16958.getMaximumShortTextLength(locale), this.f16955.getMaximumShortTextLength(locale));
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m21051(long j) {
            if (this.f16956) {
                return GJChronology.this.m21047(j);
            }
            return GJChronology.this.m21048(j);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public long m21050(long j) {
            if (this.f16956) {
                return GJChronology.this.m21046(j);
            }
            return GJChronology.this.m21045(j);
        }
    }

    private final class ImpreciseCutoverField extends CutoverField {
        ImpreciseCutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, long j) {
            this(dateTimeField, dateTimeField2, (DurationField) null, j, false);
        }

        ImpreciseCutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, long j) {
            this(dateTimeField, dateTimeField2, durationField, j, false);
        }

        ImpreciseCutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, DurationField durationField2, long j) {
            this(dateTimeField, dateTimeField2, durationField, j, false);
            this.f16952 = durationField2;
        }

        ImpreciseCutoverField(DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, long j, boolean z) {
            super(GJChronology.this, dateTimeField, dateTimeField2, j, z);
            this.f16954 = durationField == null ? new LinkedDurationField(this.f16954, this) : durationField;
        }

        public long add(long j, int i) {
            if (j >= this.f16957) {
                long add = this.f16955.add(j, i);
                if (add >= this.f16957 || GJChronology.this.iGapDuration + add >= this.f16957) {
                    return add;
                }
                if (this.f16956) {
                    if (GJChronology.this.iGregorianChronology.weekyear().get(add) <= 0) {
                        add = GJChronology.this.iGregorianChronology.weekyear().add(add, -1);
                    }
                } else if (GJChronology.this.iGregorianChronology.year().get(add) <= 0) {
                    add = GJChronology.this.iGregorianChronology.year().add(add, -1);
                }
                return m21050(add);
            }
            long add2 = this.f16958.add(j, i);
            if (add2 < this.f16957 || add2 - GJChronology.this.iGapDuration < this.f16957) {
                return add2;
            }
            return m21051(add2);
        }

        public long add(long j, long j2) {
            if (j >= this.f16957) {
                long add = this.f16955.add(j, j2);
                if (add >= this.f16957 || GJChronology.this.iGapDuration + add >= this.f16957) {
                    return add;
                }
                if (this.f16956) {
                    if (GJChronology.this.iGregorianChronology.weekyear().get(add) <= 0) {
                        add = GJChronology.this.iGregorianChronology.weekyear().add(add, -1);
                    }
                } else if (GJChronology.this.iGregorianChronology.year().get(add) <= 0) {
                    add = GJChronology.this.iGregorianChronology.year().add(add, -1);
                }
                return m21050(add);
            }
            long add2 = this.f16958.add(j, j2);
            if (add2 < this.f16957 || add2 - GJChronology.this.iGapDuration < this.f16957) {
                return add2;
            }
            return m21051(add2);
        }

        public int getDifference(long j, long j2) {
            if (j >= this.f16957) {
                if (j2 >= this.f16957) {
                    return this.f16955.getDifference(j, j2);
                }
                return this.f16958.getDifference(m21050(j), j2);
            } else if (j2 < this.f16957) {
                return this.f16958.getDifference(j, j2);
            } else {
                return this.f16955.getDifference(m21051(j), j2);
            }
        }

        public long getDifferenceAsLong(long j, long j2) {
            if (j >= this.f16957) {
                if (j2 >= this.f16957) {
                    return this.f16955.getDifferenceAsLong(j, j2);
                }
                return this.f16958.getDifferenceAsLong(m21050(j), j2);
            } else if (j2 < this.f16957) {
                return this.f16958.getDifferenceAsLong(j, j2);
            } else {
                return this.f16955.getDifferenceAsLong(m21051(j), j2);
            }
        }

        public int getMinimumValue(long j) {
            if (j >= this.f16957) {
                return this.f16955.getMinimumValue(j);
            }
            return this.f16958.getMinimumValue(j);
        }

        public int getMaximumValue(long j) {
            if (j >= this.f16957) {
                return this.f16955.getMaximumValue(j);
            }
            return this.f16958.getMaximumValue(j);
        }
    }

    private static class LinkedDurationField extends DecoratedDurationField {
        private static final long serialVersionUID = 4097975388007713084L;
        private final ImpreciseCutoverField iField;

        LinkedDurationField(DurationField durationField, ImpreciseCutoverField impreciseCutoverField) {
            super(durationField, durationField.getType());
            this.iField = impreciseCutoverField;
        }

        public long add(long j, int i) {
            return this.iField.add(j, i);
        }

        public long add(long j, long j2) {
            return this.iField.add(j, j2);
        }

        public int getDifference(long j, long j2) {
            return this.iField.getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            return this.iField.getDifferenceAsLong(j, j2);
        }
    }
}
