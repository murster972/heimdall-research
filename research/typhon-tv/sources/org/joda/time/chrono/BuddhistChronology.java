package org.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationFieldType;
import org.joda.time.ReadableDateTime;
import org.joda.time.ReadableInstant;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.DelegatedDateTimeField;
import org.joda.time.field.DividedDateTimeField;
import org.joda.time.field.OffsetDateTimeField;
import org.joda.time.field.RemainderDateTimeField;
import org.joda.time.field.SkipUndoDateTimeField;
import org.joda.time.field.UnsupportedDurationField;

public final class BuddhistChronology extends AssembledChronology {
    public static final int BE = 1;
    private static final long serialVersionUID = -3474595157769370126L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, BuddhistChronology> f16938 = new ConcurrentHashMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final BuddhistChronology f16939 = getInstance(DateTimeZone.UTC);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField f16940 = new BasicSingleEraDateTimeField("BE");

    public static BuddhistChronology getInstanceUTC() {
        return f16939;
    }

    public static BuddhistChronology getInstance() {
        return getInstance(DateTimeZone.getDefault());
    }

    public static BuddhistChronology getInstance(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        BuddhistChronology buddhistChronology = f16938.get(dateTimeZone);
        if (buddhistChronology != null) {
            return buddhistChronology;
        }
        BuddhistChronology buddhistChronology2 = new BuddhistChronology(GJChronology.getInstance(dateTimeZone, (ReadableInstant) null), (Object) null);
        BuddhistChronology buddhistChronology3 = new BuddhistChronology(LimitChronology.getInstance(buddhistChronology2, new DateTime(1, 1, 1, 0, 0, 0, 0, (Chronology) buddhistChronology2), (ReadableDateTime) null), "");
        BuddhistChronology putIfAbsent = f16938.putIfAbsent(dateTimeZone, buddhistChronology3);
        return putIfAbsent != null ? putIfAbsent : buddhistChronology3;
    }

    private BuddhistChronology(Chronology chronology, Object obj) {
        super(chronology, obj);
    }

    private Object readResolve() {
        Chronology r0 = m20953();
        return r0 == null ? getInstanceUTC() : getInstance(r0.getZone());
    }

    public Chronology withUTC() {
        return f16939;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BuddhistChronology) {
            return getZone().equals(((BuddhistChronology) obj).getZone());
        }
        return false;
    }

    public int hashCode() {
        return ("Buddhist".hashCode() * 11) + getZone().hashCode();
    }

    public String toString() {
        DateTimeZone zone = getZone();
        if (zone != null) {
            return "BuddhistChronology" + '[' + zone.getID() + ']';
        }
        return "BuddhistChronology";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21028(AssembledChronology.Fields fields) {
        if (m20952() == null) {
            fields.f16877 = UnsupportedDurationField.getInstance(DurationFieldType.eras());
            fields.f16903 = new OffsetDateTimeField(new SkipUndoDateTimeField(this, fields.f16903), 543);
            DateTimeField dateTimeField = fields.f16888;
            fields.f16888 = new DelegatedDateTimeField(fields.f16903, fields.f16877, DateTimeFieldType.yearOfEra());
            fields.f16893 = new OffsetDateTimeField(new SkipUndoDateTimeField(this, fields.f16893), 543);
            fields.f16870 = new DividedDateTimeField(new OffsetDateTimeField(fields.f16888, 99), fields.f16877, DateTimeFieldType.centuryOfEra(), 100);
            fields.f16887 = fields.f16870.getDurationField();
            fields.f16890 = new OffsetDateTimeField(new RemainderDateTimeField((DividedDateTimeField) fields.f16870), DateTimeFieldType.yearOfCentury(), 1);
            fields.f16894 = new OffsetDateTimeField(new RemainderDateTimeField(fields.f16893, fields.f16887, DateTimeFieldType.weekyearOfCentury(), 100), DateTimeFieldType.weekyearOfCentury(), 1);
            fields.f16873 = f16940;
        }
    }
}
