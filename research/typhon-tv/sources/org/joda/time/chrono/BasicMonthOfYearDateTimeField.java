package org.joda.time.chrono;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.ImpreciseDateTimeField;

class BasicMonthOfYearDateTimeField extends ImpreciseDateTimeField {

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f16931;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f16932 = this.f16933.m20966();

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f16933;

    BasicMonthOfYearDateTimeField(BasicChronology basicChronology, int i) {
        super(DateTimeFieldType.monthOfYear(), basicChronology.m20974());
        this.f16933 = basicChronology;
        this.f16931 = i;
    }

    public boolean isLenient() {
        return false;
    }

    public int get(long j) {
        return this.f16933.m20983(j);
    }

    public long add(long j, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        if (i == 0) {
            return j;
        }
        long r6 = (long) this.f16933.m20970(j);
        int r4 = this.f16933.m20996(j);
        int r5 = this.f16933.m20997(j, r4);
        int i8 = (r5 - 1) + i;
        if (r5 <= 0 || i8 >= 0) {
            i2 = i8;
            i3 = r4;
        } else {
            if (Math.signum((float) (this.f16932 + i)) == Math.signum((float) i)) {
                i6 = r4 - 1;
                i7 = this.f16932 + i;
            } else {
                i6 = r4 + 1;
                i7 = i - this.f16932;
            }
            i2 = i7 + (r5 - 1);
            i3 = i6;
        }
        if (i2 >= 0) {
            i4 = i3 + (i2 / this.f16932);
            i5 = (i2 % this.f16932) + 1;
        } else {
            i4 = ((i2 / this.f16932) + i3) - 1;
            int abs = Math.abs(i2) % this.f16932;
            if (abs == 0) {
                abs = this.f16932;
            }
            i5 = (this.f16932 - abs) + 1;
            if (i5 == 1) {
                i4++;
            }
        }
        int r3 = this.f16933.m20998(j, r4, r5);
        int r2 = this.f16933.m20982(i4, i5);
        if (r3 <= r2) {
            r2 = r3;
        }
        return this.f16933.m21000(i4, i5, r2) + r6;
    }

    public long add(long j, long j2) {
        long j3;
        long j4;
        int i = (int) j2;
        if (((long) i) == j2) {
            return add(j, i);
        }
        long r4 = (long) this.f16933.m20970(j);
        int r6 = this.f16933.m20996(j);
        int r7 = this.f16933.m20997(j, r6);
        long j5 = ((long) (r7 - 1)) + j2;
        if (j5 >= 0) {
            j3 = ((long) r6) + (j5 / ((long) this.f16932));
            j4 = (j5 % ((long) this.f16932)) + 1;
        } else {
            j3 = (((long) r6) + (j5 / ((long) this.f16932))) - 1;
            int abs = (int) (Math.abs(j5) % ((long) this.f16932));
            if (abs == 0) {
                abs = this.f16932;
            }
            j4 = (long) ((this.f16932 - abs) + 1);
            if (j4 == 1) {
                j3++;
            }
        }
        if (j3 < ((long) this.f16933.m20977()) || j3 > ((long) this.f16933.m20962())) {
            throw new IllegalArgumentException("Magnitude of add amount is too large: " + j2);
        }
        int i2 = (int) j3;
        int i3 = (int) j4;
        int r1 = this.f16933.m20998(j, r6, r7);
        int r0 = this.f16933.m20982(i2, i3);
        if (r1 <= r0) {
            r0 = r1;
        }
        return this.f16933.m21000(i2, i3, r0) + r4;
    }

    public int[] add(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        if (i2 == 0) {
            return iArr;
        }
        if (readablePartial.size() > 0 && readablePartial.getFieldType(0).equals(DateTimeFieldType.monthOfYear()) && i == 0) {
            return set(readablePartial, 0, iArr, ((((iArr[0] - 1) + (i2 % 12)) + 12) % 12) + 1);
        }
        if (!DateTimeUtils.m20901(readablePartial)) {
            return super.add(readablePartial, i, iArr, i2);
        }
        long j = 0;
        int size = readablePartial.size();
        for (int i3 = 0; i3 < size; i3++) {
            j = readablePartial.getFieldType(i3).getField(this.f16933).set(j, iArr[i3]);
        }
        return this.f16933.get(readablePartial, add(j, i2));
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m21212(get(j), i, 1, this.f16932));
    }

    public long getDifferenceAsLong(long j, long j2) {
        if (j < j2) {
            return (long) (-getDifference(j2, j));
        }
        int r2 = this.f16933.m20996(j);
        int r3 = this.f16933.m20997(j, r2);
        int r4 = this.f16933.m20996(j2);
        int r5 = this.f16933.m20997(j2, r4);
        long j3 = ((((long) (r2 - r4)) * ((long) this.f16932)) + ((long) r3)) - ((long) r5);
        int r6 = this.f16933.m20998(j, r2, r3);
        if (r6 == this.f16933.m20982(r2, r3) && this.f16933.m20998(j2, r4, r5) > r6) {
            j2 = this.f16933.dayOfMonth().set(j2, r6);
        }
        if (j - this.f16933.m20999(r2, r3) < j2 - this.f16933.m20999(r4, r5)) {
            return j3 - 1;
        }
        return j3;
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, 1, this.f16932);
        int r2 = this.f16933.m20996(j);
        int r1 = this.f16933.m20984(j, r2);
        int r0 = this.f16933.m20982(r2, i);
        if (r1 <= r0) {
            r0 = r1;
        }
        return this.f16933.m21000(r2, i, r0) + ((long) this.f16933.m20970(j));
    }

    public DurationField getRangeDurationField() {
        return this.f16933.years();
    }

    public boolean isLeap(long j) {
        int r1 = this.f16933.m20996(j);
        if (!this.f16933.m20980(r1) || this.f16933.m20997(j, r1) != this.f16931) {
            return false;
        }
        return true;
    }

    public int getLeapAmount(long j) {
        return isLeap(j) ? 1 : 0;
    }

    public DurationField getLeapDurationField() {
        return this.f16933.days();
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMaximumValue() {
        return this.f16932;
    }

    public long roundFloor(long j) {
        int r0 = this.f16933.m20996(j);
        return this.f16933.m20999(r0, this.f16933.m20997(j, r0));
    }

    public long remainder(long j) {
        return j - roundFloor(j);
    }
}
