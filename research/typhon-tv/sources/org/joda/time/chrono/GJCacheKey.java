package org.joda.time.chrono;

import org.joda.time.DateTimeZone;
import org.joda.time.Instant;

class GJCacheKey {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Instant f16947;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f16948;

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeZone f16949;

    GJCacheKey(DateTimeZone dateTimeZone, Instant instant, int i) {
        this.f16949 = dateTimeZone;
        this.f16947 = instant;
        this.f16948 = i;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((this.f16947 == null ? 0 : this.f16947.hashCode()) + 31) * 31) + this.f16948) * 31;
        if (this.f16949 != null) {
            i = this.f16949.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof GJCacheKey)) {
            return false;
        }
        GJCacheKey gJCacheKey = (GJCacheKey) obj;
        if (this.f16947 == null) {
            if (gJCacheKey.f16947 != null) {
                return false;
            }
        } else if (!this.f16947.equals(gJCacheKey.f16947)) {
            return false;
        }
        if (this.f16948 != gJCacheKey.f16948) {
            return false;
        }
        if (this.f16949 == null) {
            if (gJCacheKey.f16949 != null) {
                return false;
            }
            return true;
        } else if (!this.f16949.equals(gJCacheKey.f16949)) {
            return false;
        } else {
            return true;
        }
    }
}
