package org.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.AssembledChronology;

public final class GregorianChronology extends BasicGJChronology {
    private static final long serialVersionUID = -861407383323710522L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, GregorianChronology[]> f16979 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final GregorianChronology f16980 = getInstance(DateTimeZone.UTC);

    public static GregorianChronology getInstanceUTC() {
        return f16980;
    }

    public static GregorianChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), 4);
    }

    public static GregorianChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, 4);
    }

    public static GregorianChronology getInstance(DateTimeZone dateTimeZone, int i) {
        GregorianChronology[] gregorianChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        GregorianChronology[] gregorianChronologyArr2 = f16979.get(dateTimeZone);
        if (gregorianChronologyArr2 == null) {
            gregorianChronologyArr = new GregorianChronology[7];
            GregorianChronology[] putIfAbsent = f16979.putIfAbsent(dateTimeZone, gregorianChronologyArr);
            if (putIfAbsent != null) {
                gregorianChronologyArr = putIfAbsent;
            }
        } else {
            gregorianChronologyArr = gregorianChronologyArr2;
        }
        try {
            GregorianChronology gregorianChronology = gregorianChronologyArr[i - 1];
            if (gregorianChronology == null) {
                synchronized (gregorianChronologyArr) {
                    gregorianChronology = gregorianChronologyArr[i - 1];
                    if (gregorianChronology == null) {
                        if (dateTimeZone == DateTimeZone.UTC) {
                            gregorianChronology = new GregorianChronology((Chronology) null, (Object) null, i);
                        } else {
                            gregorianChronology = new GregorianChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, i), dateTimeZone), (Object) null, i);
                        }
                        gregorianChronologyArr[i - 1] = gregorianChronology;
                    }
                }
            }
            return gregorianChronology;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
    }

    private GregorianChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    private Object readResolve() {
        Chronology r1 = m20953();
        int minimumDaysInFirstWeek = getMinimumDaysInFirstWeek();
        if (minimumDaysInFirstWeek == 0) {
            minimumDaysInFirstWeek = 4;
        }
        return r1 == null ? getInstance(DateTimeZone.UTC, minimumDaysInFirstWeek) : getInstance(r1.getZone(), minimumDaysInFirstWeek);
    }

    public Chronology withUTC() {
        return f16980;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21084(AssembledChronology.Fields fields) {
        if (m20953() == null) {
            super.m21002(fields);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m21083(int i) {
        return (i & 3) == 0 && (i % 100 != 0 || i % 400 == 0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m21077(int i) {
        int i2;
        int i3 = i / 100;
        if (i < 0) {
            i2 = (((i3 + 3) >> 2) + (((i + 3) >> 2) - i3)) - 1;
        } else {
            i2 = (i3 >> 2) + ((i >> 2) - i3);
            if (m21083(i)) {
                i2--;
            }
        }
        return (((long) (i2 - 719527)) + (((long) i) * 365)) * 86400000;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m21082() {
        return -292275054;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m21076() {
        return 292278993;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m21078() {
        return 31556952000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m21079() {
        return 15778476000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m21080() {
        return 2629746000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m21081() {
        return 31083597720000L;
    }
}
