package org.joda.time.chrono;

import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;
import org.joda.time.field.PreciseDurationDateTimeField;

final class BasicDayOfYearDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BasicChronology f16926;

    BasicDayOfYearDateTimeField(BasicChronology basicChronology, DurationField durationField) {
        super(DateTimeFieldType.dayOfYear(), durationField);
        this.f16926 = basicChronology;
    }

    public int get(long j) {
        return this.f16926.m20987(j);
    }

    public DurationField getRangeDurationField() {
        return this.f16926.years();
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMaximumValue() {
        return this.f16926.m20990();
    }

    public int getMaximumValue(long j) {
        return this.f16926.m20995(this.f16926.m20996(j));
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        if (!readablePartial.isSupported(DateTimeFieldType.year())) {
            return this.f16926.m20990();
        }
        return this.f16926.m20995(readablePartial.get(DateTimeFieldType.year()));
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        int size = readablePartial.size();
        for (int i = 0; i < size; i++) {
            if (readablePartial.getFieldType(i) == DateTimeFieldType.year()) {
                return this.f16926.m20995(iArr[i]);
            }
        }
        return this.f16926.m20990();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21004(long j, int i) {
        int r0 = this.f16926.m20990() - 1;
        return (i > r0 || i < 1) ? getMaximumValue(j) : r0;
    }

    public boolean isLeap(long j) {
        return this.f16926.m20975(j);
    }
}
