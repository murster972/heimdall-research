package org.joda.time.chrono;

import java.util.Locale;

final class GJMonthOfYearDateTimeField extends BasicMonthOfYearDateTimeField {
    GJMonthOfYearDateTimeField(BasicChronology basicChronology) {
        super(basicChronology, 2);
    }

    public String getAsText(int i, Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21065(i);
    }

    public String getAsShortText(int i, Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21071(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21075(String str, Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21064(str);
    }

    public int getMaximumTextLength(Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21063();
    }

    public int getMaximumShortTextLength(Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21069();
    }
}
