package org.joda.time.chrono;

import java.io.IOException;
import java.io.ObjectInputStream;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;

public abstract class AssembledChronology extends BaseChronology {
    private static final long serialVersionUID = -6728465968995518215L;
    private final Chronology iBase;
    private final Object iParam;

    /* renamed from: ʻ  reason: contains not printable characters */
    private transient DurationField f16833;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private transient DateTimeField f16834;

    /* renamed from: ʼ  reason: contains not printable characters */
    private transient DurationField f16835;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private transient int f16836;

    /* renamed from: ʽ  reason: contains not printable characters */
    private transient DurationField f16837;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private transient DateTimeField f16838;

    /* renamed from: ʾ  reason: contains not printable characters */
    private transient DateTimeField f16839;

    /* renamed from: ʿ  reason: contains not printable characters */
    private transient DateTimeField f16840;

    /* renamed from: ˆ  reason: contains not printable characters */
    private transient DateTimeField f16841;

    /* renamed from: ˈ  reason: contains not printable characters */
    private transient DurationField f16842;

    /* renamed from: ˉ  reason: contains not printable characters */
    private transient DateTimeField f16843;

    /* renamed from: ˊ  reason: contains not printable characters */
    private transient DateTimeField f16844;

    /* renamed from: ˋ  reason: contains not printable characters */
    private transient DateTimeField f16845;

    /* renamed from: ˎ  reason: contains not printable characters */
    private transient DateTimeField f16846;

    /* renamed from: ˏ  reason: contains not printable characters */
    private transient DateTimeField f16847;

    /* renamed from: ˑ  reason: contains not printable characters */
    private transient DurationField f16848;

    /* renamed from: י  reason: contains not printable characters */
    private transient DateTimeField f16849;

    /* renamed from: ـ  reason: contains not printable characters */
    private transient DateTimeField f16850;

    /* renamed from: ٴ  reason: contains not printable characters */
    private transient DurationField f16851;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private transient DurationField f16852;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private transient DateTimeField f16853;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private transient DateTimeField f16854;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private transient DateTimeField f16855;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private transient DateTimeField f16856;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private transient DateTimeField f16857;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private transient DateTimeField f16858;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private transient DateTimeField f16859;

    /* renamed from: 连任  reason: contains not printable characters */
    private transient DurationField f16860;

    /* renamed from: 靐  reason: contains not printable characters */
    private transient DurationField f16861;

    /* renamed from: 麤  reason: contains not printable characters */
    private transient DurationField f16862;

    /* renamed from: 齉  reason: contains not printable characters */
    private transient DurationField f16863;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient DurationField f16864;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private transient DateTimeField f16865;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private transient DateTimeField f16866;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private transient DateTimeField f16867;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private transient DateTimeField f16868;

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m20954(Fields fields);

    protected AssembledChronology(Chronology chronology, Object obj) {
        this.iBase = chronology;
        this.iParam = obj;
        m20951();
    }

    public DateTimeZone getZone() {
        Chronology chronology = this.iBase;
        if (chronology != null) {
            return chronology.getZone();
        }
        return null;
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology chronology = this.iBase;
        if (chronology == null || (this.f16836 & 6) != 6) {
            return super.getDateTimeMillis(i, i2, i3, i4);
        }
        return chronology.getDateTimeMillis(i, i2, i3, i4);
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        Chronology chronology = this.iBase;
        if (chronology == null || (this.f16836 & 5) != 5) {
            return super.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        }
        return chronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
    }

    public long getDateTimeMillis(long j, int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology chronology = this.iBase;
        if (chronology == null || (this.f16836 & 1) != 1) {
            return super.getDateTimeMillis(j, i, i2, i3, i4);
        }
        return chronology.getDateTimeMillis(j, i, i2, i3, i4);
    }

    public final DurationField millis() {
        return this.f16864;
    }

    public final DateTimeField millisOfSecond() {
        return this.f16839;
    }

    public final DateTimeField millisOfDay() {
        return this.f16840;
    }

    public final DurationField seconds() {
        return this.f16861;
    }

    public final DateTimeField secondOfMinute() {
        return this.f16866;
    }

    public final DateTimeField secondOfDay() {
        return this.f16867;
    }

    public final DurationField minutes() {
        return this.f16863;
    }

    public final DateTimeField minuteOfHour() {
        return this.f16844;
    }

    public final DateTimeField minuteOfDay() {
        return this.f16845;
    }

    public final DurationField hours() {
        return this.f16862;
    }

    public final DateTimeField hourOfDay() {
        return this.f16846;
    }

    public final DateTimeField clockhourOfDay() {
        return this.f16841;
    }

    public final DurationField halfdays() {
        return this.f16860;
    }

    public final DateTimeField hourOfHalfday() {
        return this.f16843;
    }

    public final DateTimeField clockhourOfHalfday() {
        return this.f16847;
    }

    public final DateTimeField halfdayOfDay() {
        return this.f16849;
    }

    public final DurationField days() {
        return this.f16833;
    }

    public final DateTimeField dayOfWeek() {
        return this.f16850;
    }

    public final DateTimeField dayOfMonth() {
        return this.f16854;
    }

    public final DateTimeField dayOfYear() {
        return this.f16856;
    }

    public final DurationField weeks() {
        return this.f16835;
    }

    public final DateTimeField weekOfWeekyear() {
        return this.f16857;
    }

    public final DurationField weekyears() {
        return this.f16837;
    }

    public final DateTimeField weekyear() {
        return this.f16858;
    }

    public final DateTimeField weekyearOfCentury() {
        return this.f16859;
    }

    public final DurationField months() {
        return this.f16848;
    }

    public final DateTimeField monthOfYear() {
        return this.f16865;
    }

    public final DurationField years() {
        return this.f16851;
    }

    public final DateTimeField year() {
        return this.f16868;
    }

    public final DateTimeField yearOfEra() {
        return this.f16853;
    }

    public final DateTimeField yearOfCentury() {
        return this.f16855;
    }

    public final DurationField centuries() {
        return this.f16852;
    }

    public final DateTimeField centuryOfEra() {
        return this.f16834;
    }

    public final DurationField eras() {
        return this.f16842;
    }

    public final DateTimeField era() {
        return this.f16838;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Chronology m20953() {
        return this.iBase;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final Object m20952() {
        return this.iParam;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m20951() {
        int i = 0;
        Fields fields = new Fields();
        if (this.iBase != null) {
            fields.m20957(this.iBase);
        }
        m20954(fields);
        DurationField durationField = fields.f16899;
        if (durationField == null) {
            durationField = super.millis();
        }
        this.f16864 = durationField;
        DurationField durationField2 = fields.f16896;
        if (durationField2 == null) {
            durationField2 = super.seconds();
        }
        this.f16861 = durationField2;
        DurationField durationField3 = fields.f16898;
        if (durationField3 == null) {
            durationField3 = super.minutes();
        }
        this.f16863 = durationField3;
        DurationField durationField4 = fields.f16897;
        if (durationField4 == null) {
            durationField4 = super.hours();
        }
        this.f16862 = durationField4;
        DurationField durationField5 = fields.f16895;
        if (durationField5 == null) {
            durationField5 = super.halfdays();
        }
        this.f16860 = durationField5;
        DurationField durationField6 = fields.f16869;
        if (durationField6 == null) {
            durationField6 = super.days();
        }
        this.f16833 = durationField6;
        DurationField durationField7 = fields.f16871;
        if (durationField7 == null) {
            durationField7 = super.weeks();
        }
        this.f16835 = durationField7;
        DurationField durationField8 = fields.f16872;
        if (durationField8 == null) {
            durationField8 = super.weekyears();
        }
        this.f16837 = durationField8;
        DurationField durationField9 = fields.f16883;
        if (durationField9 == null) {
            durationField9 = super.months();
        }
        this.f16848 = durationField9;
        DurationField durationField10 = fields.f16886;
        if (durationField10 == null) {
            durationField10 = super.years();
        }
        this.f16851 = durationField10;
        DurationField durationField11 = fields.f16887;
        if (durationField11 == null) {
            durationField11 = super.centuries();
        }
        this.f16852 = durationField11;
        DurationField durationField12 = fields.f16877;
        if (durationField12 == null) {
            durationField12 = super.eras();
        }
        this.f16842 = durationField12;
        DateTimeField dateTimeField = fields.f16874;
        if (dateTimeField == null) {
            dateTimeField = super.millisOfSecond();
        }
        this.f16839 = dateTimeField;
        DateTimeField dateTimeField2 = fields.f16875;
        if (dateTimeField2 == null) {
            dateTimeField2 = super.millisOfDay();
        }
        this.f16840 = dateTimeField2;
        DateTimeField dateTimeField3 = fields.f16901;
        if (dateTimeField3 == null) {
            dateTimeField3 = super.secondOfMinute();
        }
        this.f16866 = dateTimeField3;
        DateTimeField dateTimeField4 = fields.f16902;
        if (dateTimeField4 == null) {
            dateTimeField4 = super.secondOfDay();
        }
        this.f16867 = dateTimeField4;
        DateTimeField dateTimeField5 = fields.f16879;
        if (dateTimeField5 == null) {
            dateTimeField5 = super.minuteOfHour();
        }
        this.f16844 = dateTimeField5;
        DateTimeField dateTimeField6 = fields.f16880;
        if (dateTimeField6 == null) {
            dateTimeField6 = super.minuteOfDay();
        }
        this.f16845 = dateTimeField6;
        DateTimeField dateTimeField7 = fields.f16881;
        if (dateTimeField7 == null) {
            dateTimeField7 = super.hourOfDay();
        }
        this.f16846 = dateTimeField7;
        DateTimeField dateTimeField8 = fields.f16876;
        if (dateTimeField8 == null) {
            dateTimeField8 = super.clockhourOfDay();
        }
        this.f16841 = dateTimeField8;
        DateTimeField dateTimeField9 = fields.f16878;
        if (dateTimeField9 == null) {
            dateTimeField9 = super.hourOfHalfday();
        }
        this.f16843 = dateTimeField9;
        DateTimeField dateTimeField10 = fields.f16882;
        if (dateTimeField10 == null) {
            dateTimeField10 = super.clockhourOfHalfday();
        }
        this.f16847 = dateTimeField10;
        DateTimeField dateTimeField11 = fields.f16884;
        if (dateTimeField11 == null) {
            dateTimeField11 = super.halfdayOfDay();
        }
        this.f16849 = dateTimeField11;
        DateTimeField dateTimeField12 = fields.f16885;
        if (dateTimeField12 == null) {
            dateTimeField12 = super.dayOfWeek();
        }
        this.f16850 = dateTimeField12;
        DateTimeField dateTimeField13 = fields.f16889;
        if (dateTimeField13 == null) {
            dateTimeField13 = super.dayOfMonth();
        }
        this.f16854 = dateTimeField13;
        DateTimeField dateTimeField14 = fields.f16891;
        if (dateTimeField14 == null) {
            dateTimeField14 = super.dayOfYear();
        }
        this.f16856 = dateTimeField14;
        DateTimeField dateTimeField15 = fields.f16892;
        if (dateTimeField15 == null) {
            dateTimeField15 = super.weekOfWeekyear();
        }
        this.f16857 = dateTimeField15;
        DateTimeField dateTimeField16 = fields.f16893;
        if (dateTimeField16 == null) {
            dateTimeField16 = super.weekyear();
        }
        this.f16858 = dateTimeField16;
        DateTimeField dateTimeField17 = fields.f16894;
        if (dateTimeField17 == null) {
            dateTimeField17 = super.weekyearOfCentury();
        }
        this.f16859 = dateTimeField17;
        DateTimeField dateTimeField18 = fields.f16900;
        if (dateTimeField18 == null) {
            dateTimeField18 = super.monthOfYear();
        }
        this.f16865 = dateTimeField18;
        DateTimeField dateTimeField19 = fields.f16903;
        if (dateTimeField19 == null) {
            dateTimeField19 = super.year();
        }
        this.f16868 = dateTimeField19;
        DateTimeField dateTimeField20 = fields.f16888;
        if (dateTimeField20 == null) {
            dateTimeField20 = super.yearOfEra();
        }
        this.f16853 = dateTimeField20;
        DateTimeField dateTimeField21 = fields.f16890;
        if (dateTimeField21 == null) {
            dateTimeField21 = super.yearOfCentury();
        }
        this.f16855 = dateTimeField21;
        DateTimeField dateTimeField22 = fields.f16870;
        if (dateTimeField22 == null) {
            dateTimeField22 = super.centuryOfEra();
        }
        this.f16834 = dateTimeField22;
        DateTimeField dateTimeField23 = fields.f16873;
        if (dateTimeField23 == null) {
            dateTimeField23 = super.era();
        }
        this.f16838 = dateTimeField23;
        if (this.iBase != null) {
            int i2 = ((this.f16846 == this.iBase.hourOfDay() && this.f16844 == this.iBase.minuteOfHour() && this.f16866 == this.iBase.secondOfMinute() && this.f16839 == this.iBase.millisOfSecond()) ? 1 : 0) | (this.f16840 == this.iBase.millisOfDay() ? 2 : 0);
            if (this.f16868 == this.iBase.year() && this.f16865 == this.iBase.monthOfYear() && this.f16854 == this.iBase.dayOfMonth()) {
                i = 4;
            }
            i |= i2;
        }
        this.f16836 = i;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        m20951();
    }

    public static final class Fields {

        /* renamed from: ʻ  reason: contains not printable characters */
        public DurationField f16869;

        /* renamed from: ʻʻ  reason: contains not printable characters */
        public DateTimeField f16870;

        /* renamed from: ʼ  reason: contains not printable characters */
        public DurationField f16871;

        /* renamed from: ʽ  reason: contains not printable characters */
        public DurationField f16872;

        /* renamed from: ʽʽ  reason: contains not printable characters */
        public DateTimeField f16873;

        /* renamed from: ʾ  reason: contains not printable characters */
        public DateTimeField f16874;

        /* renamed from: ʿ  reason: contains not printable characters */
        public DateTimeField f16875;

        /* renamed from: ˆ  reason: contains not printable characters */
        public DateTimeField f16876;

        /* renamed from: ˈ  reason: contains not printable characters */
        public DurationField f16877;

        /* renamed from: ˉ  reason: contains not printable characters */
        public DateTimeField f16878;

        /* renamed from: ˊ  reason: contains not printable characters */
        public DateTimeField f16879;

        /* renamed from: ˋ  reason: contains not printable characters */
        public DateTimeField f16880;

        /* renamed from: ˎ  reason: contains not printable characters */
        public DateTimeField f16881;

        /* renamed from: ˏ  reason: contains not printable characters */
        public DateTimeField f16882;

        /* renamed from: ˑ  reason: contains not printable characters */
        public DurationField f16883;

        /* renamed from: י  reason: contains not printable characters */
        public DateTimeField f16884;

        /* renamed from: ـ  reason: contains not printable characters */
        public DateTimeField f16885;

        /* renamed from: ٴ  reason: contains not printable characters */
        public DurationField f16886;

        /* renamed from: ᐧ  reason: contains not printable characters */
        public DurationField f16887;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public DateTimeField f16888;

        /* renamed from: ᴵ  reason: contains not printable characters */
        public DateTimeField f16889;

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        public DateTimeField f16890;

        /* renamed from: ᵎ  reason: contains not printable characters */
        public DateTimeField f16891;

        /* renamed from: ᵔ  reason: contains not printable characters */
        public DateTimeField f16892;

        /* renamed from: ᵢ  reason: contains not printable characters */
        public DateTimeField f16893;

        /* renamed from: ⁱ  reason: contains not printable characters */
        public DateTimeField f16894;

        /* renamed from: 连任  reason: contains not printable characters */
        public DurationField f16895;

        /* renamed from: 靐  reason: contains not printable characters */
        public DurationField f16896;

        /* renamed from: 麤  reason: contains not printable characters */
        public DurationField f16897;

        /* renamed from: 齉  reason: contains not printable characters */
        public DurationField f16898;

        /* renamed from: 龘  reason: contains not printable characters */
        public DurationField f16899;

        /* renamed from: ﹳ  reason: contains not printable characters */
        public DateTimeField f16900;

        /* renamed from: ﹶ  reason: contains not printable characters */
        public DateTimeField f16901;

        /* renamed from: ﾞ  reason: contains not printable characters */
        public DateTimeField f16902;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public DateTimeField f16903;

        Fields() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20957(Chronology chronology) {
            DurationField millis = chronology.millis();
            if (m20956(millis)) {
                this.f16899 = millis;
            }
            DurationField seconds = chronology.seconds();
            if (m20956(seconds)) {
                this.f16896 = seconds;
            }
            DurationField minutes = chronology.minutes();
            if (m20956(minutes)) {
                this.f16898 = minutes;
            }
            DurationField hours = chronology.hours();
            if (m20956(hours)) {
                this.f16897 = hours;
            }
            DurationField halfdays = chronology.halfdays();
            if (m20956(halfdays)) {
                this.f16895 = halfdays;
            }
            DurationField days = chronology.days();
            if (m20956(days)) {
                this.f16869 = days;
            }
            DurationField weeks = chronology.weeks();
            if (m20956(weeks)) {
                this.f16871 = weeks;
            }
            DurationField weekyears = chronology.weekyears();
            if (m20956(weekyears)) {
                this.f16872 = weekyears;
            }
            DurationField months = chronology.months();
            if (m20956(months)) {
                this.f16883 = months;
            }
            DurationField years = chronology.years();
            if (m20956(years)) {
                this.f16886 = years;
            }
            DurationField centuries = chronology.centuries();
            if (m20956(centuries)) {
                this.f16887 = centuries;
            }
            DurationField eras = chronology.eras();
            if (m20956(eras)) {
                this.f16877 = eras;
            }
            DateTimeField millisOfSecond = chronology.millisOfSecond();
            if (m20955(millisOfSecond)) {
                this.f16874 = millisOfSecond;
            }
            DateTimeField millisOfDay = chronology.millisOfDay();
            if (m20955(millisOfDay)) {
                this.f16875 = millisOfDay;
            }
            DateTimeField secondOfMinute = chronology.secondOfMinute();
            if (m20955(secondOfMinute)) {
                this.f16901 = secondOfMinute;
            }
            DateTimeField secondOfDay = chronology.secondOfDay();
            if (m20955(secondOfDay)) {
                this.f16902 = secondOfDay;
            }
            DateTimeField minuteOfHour = chronology.minuteOfHour();
            if (m20955(minuteOfHour)) {
                this.f16879 = minuteOfHour;
            }
            DateTimeField minuteOfDay = chronology.minuteOfDay();
            if (m20955(minuteOfDay)) {
                this.f16880 = minuteOfDay;
            }
            DateTimeField hourOfDay = chronology.hourOfDay();
            if (m20955(hourOfDay)) {
                this.f16881 = hourOfDay;
            }
            DateTimeField clockhourOfDay = chronology.clockhourOfDay();
            if (m20955(clockhourOfDay)) {
                this.f16876 = clockhourOfDay;
            }
            DateTimeField hourOfHalfday = chronology.hourOfHalfday();
            if (m20955(hourOfHalfday)) {
                this.f16878 = hourOfHalfday;
            }
            DateTimeField clockhourOfHalfday = chronology.clockhourOfHalfday();
            if (m20955(clockhourOfHalfday)) {
                this.f16882 = clockhourOfHalfday;
            }
            DateTimeField halfdayOfDay = chronology.halfdayOfDay();
            if (m20955(halfdayOfDay)) {
                this.f16884 = halfdayOfDay;
            }
            DateTimeField dayOfWeek = chronology.dayOfWeek();
            if (m20955(dayOfWeek)) {
                this.f16885 = dayOfWeek;
            }
            DateTimeField dayOfMonth = chronology.dayOfMonth();
            if (m20955(dayOfMonth)) {
                this.f16889 = dayOfMonth;
            }
            DateTimeField dayOfYear = chronology.dayOfYear();
            if (m20955(dayOfYear)) {
                this.f16891 = dayOfYear;
            }
            DateTimeField weekOfWeekyear = chronology.weekOfWeekyear();
            if (m20955(weekOfWeekyear)) {
                this.f16892 = weekOfWeekyear;
            }
            DateTimeField weekyear = chronology.weekyear();
            if (m20955(weekyear)) {
                this.f16893 = weekyear;
            }
            DateTimeField weekyearOfCentury = chronology.weekyearOfCentury();
            if (m20955(weekyearOfCentury)) {
                this.f16894 = weekyearOfCentury;
            }
            DateTimeField monthOfYear = chronology.monthOfYear();
            if (m20955(monthOfYear)) {
                this.f16900 = monthOfYear;
            }
            DateTimeField year = chronology.year();
            if (m20955(year)) {
                this.f16903 = year;
            }
            DateTimeField yearOfEra = chronology.yearOfEra();
            if (m20955(yearOfEra)) {
                this.f16888 = yearOfEra;
            }
            DateTimeField yearOfCentury = chronology.yearOfCentury();
            if (m20955(yearOfCentury)) {
                this.f16890 = yearOfCentury;
            }
            DateTimeField centuryOfEra = chronology.centuryOfEra();
            if (m20955(centuryOfEra)) {
                this.f16870 = centuryOfEra;
            }
            DateTimeField era = chronology.era();
            if (m20955(era)) {
                this.f16873 = era;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static boolean m20956(DurationField durationField) {
            if (durationField == null) {
                return false;
            }
            return durationField.isSupported();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static boolean m20955(DateTimeField dateTimeField) {
            if (dateTimeField == null) {
                return false;
            }
            return dateTimeField.isSupported();
        }
    }
}
