package org.joda.time.chrono;

import java.util.Locale;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.field.PreciseDurationDateTimeField;

final class GJDayOfWeekDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BasicChronology f16960;

    GJDayOfWeekDateTimeField(BasicChronology basicChronology, DurationField durationField) {
        super(DateTimeFieldType.dayOfWeek(), durationField);
        this.f16960 = basicChronology;
    }

    public int get(long j) {
        return this.f16960.m20967(j);
    }

    public String getAsText(int i, Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21068(i);
    }

    public String getAsShortText(int i, Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21062(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21052(String str, Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21070(str);
    }

    public DurationField getRangeDurationField() {
        return this.f16960.weeks();
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMaximumValue() {
        return 7;
    }

    public int getMaximumTextLength(Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21066();
    }

    public int getMaximumShortTextLength(Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21061();
    }
}
