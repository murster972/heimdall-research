package org.joda.time.chrono;

import org.joda.time.Chronology;

abstract class BasicGJChronology extends BasicChronology {
    private static final long serialVersionUID = 538276888268L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int[] f16927 = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    /* renamed from: 麤  reason: contains not printable characters */
    private static final long[] f16928 = new long[12];

    /* renamed from: 齉  reason: contains not printable characters */
    private static final long[] f16929 = new long[12];

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int[] f16930 = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    static {
        long j = 0;
        int i = 0;
        long j2 = 0;
        while (true) {
            long j3 = j;
            if (i < 11) {
                j = (((long) f16930[i]) * 86400000) + j3;
                f16929[i + 1] = j;
                j2 += ((long) f16927[i]) * 86400000;
                f16928[i + 1] = j2;
                i++;
            } else {
                return;
            }
        }
    }

    BasicGJChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m21021(long j) {
        return dayOfMonth().get(j) == 29 && monthOfYear().isLeap(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21025(long j, int i) {
        int r5 = (int) ((j - m20989(i)) >> 10);
        if (m20980(i)) {
            if (r5 < 15356250) {
                if (r5 < 7678125) {
                    if (r5 < 2615625) {
                        return 1;
                    }
                    return r5 < 5062500 ? 2 : 3;
                } else if (r5 < 10209375) {
                    return 4;
                } else {
                    return r5 < 12825000 ? 5 : 6;
                }
            } else if (r5 < 23118750) {
                if (r5 < 17971875) {
                    return 7;
                }
                return r5 < 20587500 ? 8 : 9;
            } else if (r5 < 25734375) {
                return 10;
            } else {
                return r5 < 28265625 ? 11 : 12;
            }
        } else if (r5 < 15271875) {
            if (r5 < 7593750) {
                if (r5 >= 2615625) {
                    return r5 < 4978125 ? 2 : 3;
                }
                return 1;
            } else if (r5 < 10125000) {
                return 4;
            } else {
                return r5 < 12740625 ? 5 : 6;
            }
        } else if (r5 < 23034375) {
            if (r5 < 17887500) {
                return 7;
            }
            return r5 < 20503125 ? 8 : 9;
        } else if (r5 < 25650000) {
            return 10;
        } else {
            return r5 < 28181250 ? 11 : 12;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m21023(int i, int i2) {
        if (m20980(i)) {
            return f16927[i2 - 1];
        }
        return f16930[i2 - 1];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m21019(int i) {
        return f16927[i - 1];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m21022(long j, int i) {
        if (i > 28 || i < 1) {
            return m20972(j);
        }
        return 28;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m21024(int i, int i2) {
        if (m20980(i)) {
            return f16928[i2 - 1];
        }
        return f16929[i2 - 1];
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0034  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m21026(long r10, long r12) {
        /*
            r9 = this;
            int r6 = r9.m20996((long) r10)
            int r7 = r9.m20996((long) r12)
            long r0 = r9.m20989((int) r6)
            long r4 = r10 - r0
            long r0 = r9.m20989((int) r7)
            long r0 = r12 - r0
            r2 = 5097600000(0x12fd73400, double:2.518549036E-314)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x004d
            boolean r2 = r9.m20980((int) r7)
            if (r2 == 0) goto L_0x0038
            boolean r2 = r9.m20980((int) r6)
            if (r2 != 0) goto L_0x004d
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 - r2
            r2 = r0
        L_0x002e:
            int r0 = r6 - r7
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x0036
            int r0 = r0 + -1
        L_0x0036:
            long r0 = (long) r0
            return r0
        L_0x0038:
            r2 = 5097600000(0x12fd73400, double:2.518549036E-314)
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x004d
            boolean r2 = r9.m20980((int) r6)
            if (r2 == 0) goto L_0x004d
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r4 = r4 - r2
            r2 = r0
            goto L_0x002e
        L_0x004d:
            r2 = r0
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.joda.time.chrono.BasicGJChronology.m21026(long, long):long");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public long m21020(long j, int i) {
        int r1 = m20996(j);
        int r0 = m20992(j, r1);
        int r2 = m20970(j);
        if (r0 > 59) {
            if (m20980(r1)) {
                if (!m20980(i)) {
                    r0--;
                }
            } else if (m20980(i)) {
                r0++;
            }
        }
        return m21000(i, 1, r0) + ((long) r2);
    }
}
