package org.joda.time.chrono;

import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;
import org.joda.time.field.PreciseDurationDateTimeField;

final class BasicDayOfMonthDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BasicChronology f16925;

    BasicDayOfMonthDateTimeField(BasicChronology basicChronology, DurationField durationField) {
        super(DateTimeFieldType.dayOfMonth(), durationField);
        this.f16925 = basicChronology;
    }

    public int get(long j) {
        return this.f16925.m20991(j);
    }

    public DurationField getRangeDurationField() {
        return this.f16925.months();
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMaximumValue() {
        return this.f16925.m20986();
    }

    public int getMaximumValue(long j) {
        return this.f16925.m20972(j);
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        if (!readablePartial.isSupported(DateTimeFieldType.monthOfYear())) {
            return getMaximumValue();
        }
        int i = readablePartial.get(DateTimeFieldType.monthOfYear());
        if (!readablePartial.isSupported(DateTimeFieldType.year())) {
            return this.f16925.m20963(i);
        }
        return this.f16925.m20982(readablePartial.get(DateTimeFieldType.year()), i);
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        int size = readablePartial.size();
        for (int i = 0; i < size; i++) {
            if (readablePartial.getFieldType(i) == DateTimeFieldType.monthOfYear()) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < size; i3++) {
                    if (readablePartial.getFieldType(i3) == DateTimeFieldType.year()) {
                        return this.f16925.m20982(iArr[i3], i2);
                    }
                }
                return this.f16925.m20963(i2);
            }
        }
        return getMaximumValue();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21003(long j, int i) {
        return this.f16925.m20979(j, i);
    }

    public boolean isLeap(long j) {
        return this.f16925.m20975(j);
    }
}
