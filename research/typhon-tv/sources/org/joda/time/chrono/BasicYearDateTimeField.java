package org.joda.time.chrono;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.ImpreciseDateTimeField;

class BasicYearDateTimeField extends ImpreciseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final BasicChronology f16937;

    BasicYearDateTimeField(BasicChronology basicChronology) {
        super(DateTimeFieldType.year(), basicChronology.m20971());
        this.f16937 = basicChronology;
    }

    public boolean isLenient() {
        return false;
    }

    public int get(long j) {
        return this.f16937.m20996(j);
    }

    public long add(long j, int i) {
        return i == 0 ? j : set(j, FieldUtils.m21210(get(j), i));
    }

    public long add(long j, long j2) {
        return add(j, FieldUtils.m21213(j2));
    }

    public long addWrapField(long j, int i) {
        return i == 0 ? j : set(j, FieldUtils.m21212(this.f16937.m20996(j), i, this.f16937.m20977(), this.f16937.m20962()));
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, this.f16937.m20977(), this.f16937.m20962());
        return this.f16937.m20965(j, i);
    }

    public long setExtended(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, this.f16937.m20977() - 1, this.f16937.m20962() + 1);
        return this.f16937.m20965(j, i);
    }

    public long getDifferenceAsLong(long j, long j2) {
        if (j < j2) {
            return -this.f16937.m21001(j2, j);
        }
        return this.f16937.m21001(j, j2);
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public boolean isLeap(long j) {
        return this.f16937.m20980(get(j));
    }

    public int getLeapAmount(long j) {
        if (this.f16937.m20980(get(j))) {
            return 1;
        }
        return 0;
    }

    public DurationField getLeapDurationField() {
        return this.f16937.days();
    }

    public int getMinimumValue() {
        return this.f16937.m20977();
    }

    public int getMaximumValue() {
        return this.f16937.m20962();
    }

    public long roundFloor(long j) {
        return this.f16937.m20989(get(j));
    }

    public long roundCeiling(long j) {
        int i = get(j);
        if (j != this.f16937.m20989(i)) {
            return this.f16937.m20989(i + 1);
        }
        return j;
    }

    public long remainder(long j) {
        return j - roundFloor(j);
    }
}
