package org.joda.time.chrono;

import java.util.HashMap;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.IllegalInstantException;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.BaseDateTimeField;
import org.joda.time.field.BaseDurationField;

public final class ZonedChronology extends AssembledChronology {
    private static final long serialVersionUID = -1079258847191166848L;

    public static ZonedChronology getInstance(Chronology chronology, DateTimeZone dateTimeZone) {
        if (chronology == null) {
            throw new IllegalArgumentException("Must supply a chronology");
        }
        Chronology withUTC = chronology.withUTC();
        if (withUTC == null) {
            throw new IllegalArgumentException("UTC chronology must not be null");
        } else if (dateTimeZone != null) {
            return new ZonedChronology(withUTC, dateTimeZone);
        } else {
            throw new IllegalArgumentException("DateTimeZone must not be null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m21129(DurationField durationField) {
        return durationField != null && durationField.getUnitMillis() < 43200000;
    }

    private ZonedChronology(Chronology chronology, DateTimeZone dateTimeZone) {
        super(chronology, dateTimeZone);
    }

    public DateTimeZone getZone() {
        return (DateTimeZone) m20952();
    }

    public Chronology withUTC() {
        return m20953();
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        if (dateTimeZone == m20952()) {
            return this;
        }
        if (dateTimeZone == DateTimeZone.UTC) {
            return m20953();
        }
        return new ZonedChronology(m20953(), dateTimeZone);
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        return m21126(m20953().getDateTimeMillis(i, i2, i3, i4));
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        return m21126(m20953().getDateTimeMillis(i, i2, i3, i4, i5, i6, i7));
    }

    public long getDateTimeMillis(long j, int i, int i2, int i3, int i4) throws IllegalArgumentException {
        return m21126(m20953().getDateTimeMillis(((long) getZone().getOffset(j)) + j, i, i2, i3, i4));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m21126(long j) {
        if (j == Long.MAX_VALUE) {
            return Long.MAX_VALUE;
        }
        if (j == Long.MIN_VALUE) {
            return Long.MIN_VALUE;
        }
        DateTimeZone zone = getZone();
        int offsetFromLocal = zone.getOffsetFromLocal(j);
        long j2 = j - ((long) offsetFromLocal);
        if (j > 604800000 && j2 < 0) {
            return Long.MAX_VALUE;
        }
        if (j < -604800000 && j2 > 0) {
            return Long.MIN_VALUE;
        }
        if (offsetFromLocal == zone.getOffset(j2)) {
            return j2;
        }
        throw new IllegalInstantException(j, zone.getID());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21130(AssembledChronology.Fields fields) {
        HashMap hashMap = new HashMap();
        fields.f16877 = m21128(fields.f16877, (HashMap<Object, Object>) hashMap);
        fields.f16887 = m21128(fields.f16887, (HashMap<Object, Object>) hashMap);
        fields.f16886 = m21128(fields.f16886, (HashMap<Object, Object>) hashMap);
        fields.f16883 = m21128(fields.f16883, (HashMap<Object, Object>) hashMap);
        fields.f16872 = m21128(fields.f16872, (HashMap<Object, Object>) hashMap);
        fields.f16871 = m21128(fields.f16871, (HashMap<Object, Object>) hashMap);
        fields.f16869 = m21128(fields.f16869, (HashMap<Object, Object>) hashMap);
        fields.f16895 = m21128(fields.f16895, (HashMap<Object, Object>) hashMap);
        fields.f16897 = m21128(fields.f16897, (HashMap<Object, Object>) hashMap);
        fields.f16898 = m21128(fields.f16898, (HashMap<Object, Object>) hashMap);
        fields.f16896 = m21128(fields.f16896, (HashMap<Object, Object>) hashMap);
        fields.f16899 = m21128(fields.f16899, (HashMap<Object, Object>) hashMap);
        fields.f16903 = m21127(fields.f16903, (HashMap<Object, Object>) hashMap);
        fields.f16888 = m21127(fields.f16888, (HashMap<Object, Object>) hashMap);
        fields.f16890 = m21127(fields.f16890, (HashMap<Object, Object>) hashMap);
        fields.f16870 = m21127(fields.f16870, (HashMap<Object, Object>) hashMap);
        fields.f16873 = m21127(fields.f16873, (HashMap<Object, Object>) hashMap);
        fields.f16885 = m21127(fields.f16885, (HashMap<Object, Object>) hashMap);
        fields.f16889 = m21127(fields.f16889, (HashMap<Object, Object>) hashMap);
        fields.f16891 = m21127(fields.f16891, (HashMap<Object, Object>) hashMap);
        fields.f16900 = m21127(fields.f16900, (HashMap<Object, Object>) hashMap);
        fields.f16892 = m21127(fields.f16892, (HashMap<Object, Object>) hashMap);
        fields.f16893 = m21127(fields.f16893, (HashMap<Object, Object>) hashMap);
        fields.f16894 = m21127(fields.f16894, (HashMap<Object, Object>) hashMap);
        fields.f16874 = m21127(fields.f16874, (HashMap<Object, Object>) hashMap);
        fields.f16875 = m21127(fields.f16875, (HashMap<Object, Object>) hashMap);
        fields.f16901 = m21127(fields.f16901, (HashMap<Object, Object>) hashMap);
        fields.f16902 = m21127(fields.f16902, (HashMap<Object, Object>) hashMap);
        fields.f16879 = m21127(fields.f16879, (HashMap<Object, Object>) hashMap);
        fields.f16880 = m21127(fields.f16880, (HashMap<Object, Object>) hashMap);
        fields.f16881 = m21127(fields.f16881, (HashMap<Object, Object>) hashMap);
        fields.f16878 = m21127(fields.f16878, (HashMap<Object, Object>) hashMap);
        fields.f16876 = m21127(fields.f16876, (HashMap<Object, Object>) hashMap);
        fields.f16882 = m21127(fields.f16882, (HashMap<Object, Object>) hashMap);
        fields.f16884 = m21127(fields.f16884, (HashMap<Object, Object>) hashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DurationField m21128(DurationField durationField, HashMap<Object, Object> hashMap) {
        if (durationField == null || !durationField.isSupported()) {
            return durationField;
        }
        if (hashMap.containsKey(durationField)) {
            return (DurationField) hashMap.get(durationField);
        }
        ZonedDurationField zonedDurationField = new ZonedDurationField(durationField, getZone());
        hashMap.put(durationField, zonedDurationField);
        return zonedDurationField;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeField m21127(DateTimeField dateTimeField, HashMap<Object, Object> hashMap) {
        if (dateTimeField == null || !dateTimeField.isSupported()) {
            return dateTimeField;
        }
        if (hashMap.containsKey(dateTimeField)) {
            return (DateTimeField) hashMap.get(dateTimeField);
        }
        ZonedDateTimeField zonedDateTimeField = new ZonedDateTimeField(dateTimeField, getZone(), m21128(dateTimeField.getDurationField(), hashMap), m21128(dateTimeField.getRangeDurationField(), hashMap), m21128(dateTimeField.getLeapDurationField(), hashMap));
        hashMap.put(dateTimeField, zonedDateTimeField);
        return zonedDateTimeField;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ZonedChronology)) {
            return false;
        }
        ZonedChronology zonedChronology = (ZonedChronology) obj;
        if (!m20953().equals(zonedChronology.m20953()) || !getZone().equals(zonedChronology.getZone())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return 326565 + (getZone().hashCode() * 11) + (m20953().hashCode() * 7);
    }

    public String toString() {
        return "ZonedChronology[" + m20953() + ", " + getZone().getID() + ']';
    }

    static class ZonedDurationField extends BaseDurationField {
        private static final long serialVersionUID = -485345310999208286L;
        final DurationField iField;
        final boolean iTimeField;
        final DateTimeZone iZone;

        ZonedDurationField(DurationField durationField, DateTimeZone dateTimeZone) {
            super(durationField.getType());
            if (!durationField.isSupported()) {
                throw new IllegalArgumentException();
            }
            this.iField = durationField;
            this.iTimeField = ZonedChronology.m21129(durationField);
            this.iZone = dateTimeZone;
        }

        public boolean isPrecise() {
            if (this.iTimeField) {
                return this.iField.isPrecise();
            }
            return this.iField.isPrecise() && this.iZone.isFixed();
        }

        public long getUnitMillis() {
            return this.iField.getUnitMillis();
        }

        public int getValue(long j, long j2) {
            return this.iField.getValue(j, m21133(j2));
        }

        public long getValueAsLong(long j, long j2) {
            return this.iField.getValueAsLong(j, m21133(j2));
        }

        public long getMillis(int i, long j) {
            return this.iField.getMillis(i, m21133(j));
        }

        public long getMillis(long j, long j2) {
            return this.iField.getMillis(j, m21133(j2));
        }

        public long add(long j, int i) {
            int r0 = m21134(j);
            long add = this.iField.add(((long) r0) + j, i);
            if (!this.iTimeField) {
                r0 = m21132(add);
            }
            return add - ((long) r0);
        }

        public long add(long j, long j2) {
            int r0 = m21134(j);
            long add = this.iField.add(((long) r0) + j, j2);
            if (!this.iTimeField) {
                r0 = m21132(add);
            }
            return add - ((long) r0);
        }

        public int getDifference(long j, long j2) {
            int r1 = m21134(j2);
            return this.iField.getDifference(((long) (this.iTimeField ? r1 : m21134(j))) + j, ((long) r1) + j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            int r1 = m21134(j2);
            return this.iField.getDifferenceAsLong(((long) (this.iTimeField ? r1 : m21134(j))) + j, ((long) r1) + j2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m21134(long j) {
            int offset = this.iZone.getOffset(j);
            if (((((long) offset) + j) ^ j) >= 0 || (((long) offset) ^ j) < 0) {
                return offset;
            }
            throw new ArithmeticException("Adding time zone offset caused overflow");
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private int m21132(long j) {
            int offsetFromLocal = this.iZone.getOffsetFromLocal(j);
            if (((j - ((long) offsetFromLocal)) ^ j) >= 0 || (((long) offsetFromLocal) ^ j) >= 0) {
                return offsetFromLocal;
            }
            throw new ArithmeticException("Subtracting time zone offset caused overflow");
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private long m21133(long j) {
            return this.iZone.convertUTCToLocal(j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ZonedDurationField)) {
                return false;
            }
            ZonedDurationField zonedDurationField = (ZonedDurationField) obj;
            if (!this.iField.equals(zonedDurationField.iField) || !this.iZone.equals(zonedDurationField.iZone)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.iField.hashCode() ^ this.iZone.hashCode();
        }
    }

    static final class ZonedDateTimeField extends BaseDateTimeField {

        /* renamed from: ʻ  reason: contains not printable characters */
        final DurationField f16997;

        /* renamed from: 连任  reason: contains not printable characters */
        final DurationField f16998;

        /* renamed from: 靐  reason: contains not printable characters */
        final DateTimeZone f16999;

        /* renamed from: 麤  reason: contains not printable characters */
        final boolean f17000;

        /* renamed from: 齉  reason: contains not printable characters */
        final DurationField f17001;

        /* renamed from: 龘  reason: contains not printable characters */
        final DateTimeField f17002;

        ZonedDateTimeField(DateTimeField dateTimeField, DateTimeZone dateTimeZone, DurationField durationField, DurationField durationField2, DurationField durationField3) {
            super(dateTimeField.getType());
            if (!dateTimeField.isSupported()) {
                throw new IllegalArgumentException();
            }
            this.f17002 = dateTimeField;
            this.f16999 = dateTimeZone;
            this.f17001 = durationField;
            this.f17000 = ZonedChronology.m21129(durationField);
            this.f16998 = durationField2;
            this.f16997 = durationField3;
        }

        public boolean isLenient() {
            return this.f17002.isLenient();
        }

        public int get(long j) {
            return this.f17002.get(this.f16999.convertUTCToLocal(j));
        }

        public String getAsText(long j, Locale locale) {
            return this.f17002.getAsText(this.f16999.convertUTCToLocal(j), locale);
        }

        public String getAsShortText(long j, Locale locale) {
            return this.f17002.getAsShortText(this.f16999.convertUTCToLocal(j), locale);
        }

        public String getAsText(int i, Locale locale) {
            return this.f17002.getAsText(i, locale);
        }

        public String getAsShortText(int i, Locale locale) {
            return this.f17002.getAsShortText(i, locale);
        }

        public long add(long j, int i) {
            if (this.f17000) {
                int r0 = m21131(j);
                return this.f17002.add(((long) r0) + j, i) - ((long) r0);
            }
            return this.f16999.convertLocalToUTC(this.f17002.add(this.f16999.convertUTCToLocal(j), i), false, j);
        }

        public long add(long j, long j2) {
            if (this.f17000) {
                int r0 = m21131(j);
                return this.f17002.add(((long) r0) + j, j2) - ((long) r0);
            }
            return this.f16999.convertLocalToUTC(this.f17002.add(this.f16999.convertUTCToLocal(j), j2), false, j);
        }

        public long addWrapField(long j, int i) {
            if (this.f17000) {
                int r0 = m21131(j);
                return this.f17002.addWrapField(((long) r0) + j, i) - ((long) r0);
            }
            return this.f16999.convertLocalToUTC(this.f17002.addWrapField(this.f16999.convertUTCToLocal(j), i), false, j);
        }

        public long set(long j, int i) {
            long j2 = this.f17002.set(this.f16999.convertUTCToLocal(j), i);
            long convertLocalToUTC = this.f16999.convertLocalToUTC(j2, false, j);
            if (get(convertLocalToUTC) == i) {
                return convertLocalToUTC;
            }
            IllegalInstantException illegalInstantException = new IllegalInstantException(j2, this.f16999.getID());
            IllegalFieldValueException illegalFieldValueException = new IllegalFieldValueException(this.f17002.getType(), Integer.valueOf(i), illegalInstantException.getMessage());
            illegalFieldValueException.initCause(illegalInstantException);
            throw illegalFieldValueException;
        }

        public long set(long j, String str, Locale locale) {
            return this.f16999.convertLocalToUTC(this.f17002.set(this.f16999.convertUTCToLocal(j), str, locale), false, j);
        }

        public int getDifference(long j, long j2) {
            int r1 = m21131(j2);
            return this.f17002.getDifference(((long) (this.f17000 ? r1 : m21131(j))) + j, ((long) r1) + j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            int r1 = m21131(j2);
            return this.f17002.getDifferenceAsLong(((long) (this.f17000 ? r1 : m21131(j))) + j, ((long) r1) + j2);
        }

        public final DurationField getDurationField() {
            return this.f17001;
        }

        public final DurationField getRangeDurationField() {
            return this.f16998;
        }

        public boolean isLeap(long j) {
            return this.f17002.isLeap(this.f16999.convertUTCToLocal(j));
        }

        public int getLeapAmount(long j) {
            return this.f17002.getLeapAmount(this.f16999.convertUTCToLocal(j));
        }

        public final DurationField getLeapDurationField() {
            return this.f16997;
        }

        public long roundFloor(long j) {
            if (this.f17000) {
                int r0 = m21131(j);
                return this.f17002.roundFloor(((long) r0) + j) - ((long) r0);
            }
            return this.f16999.convertLocalToUTC(this.f17002.roundFloor(this.f16999.convertUTCToLocal(j)), false, j);
        }

        public long roundCeiling(long j) {
            if (this.f17000) {
                int r0 = m21131(j);
                return this.f17002.roundCeiling(((long) r0) + j) - ((long) r0);
            }
            return this.f16999.convertLocalToUTC(this.f17002.roundCeiling(this.f16999.convertUTCToLocal(j)), false, j);
        }

        public long remainder(long j) {
            return this.f17002.remainder(this.f16999.convertUTCToLocal(j));
        }

        public int getMinimumValue() {
            return this.f17002.getMinimumValue();
        }

        public int getMinimumValue(long j) {
            return this.f17002.getMinimumValue(this.f16999.convertUTCToLocal(j));
        }

        public int getMinimumValue(ReadablePartial readablePartial) {
            return this.f17002.getMinimumValue(readablePartial);
        }

        public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
            return this.f17002.getMinimumValue(readablePartial, iArr);
        }

        public int getMaximumValue() {
            return this.f17002.getMaximumValue();
        }

        public int getMaximumValue(long j) {
            return this.f17002.getMaximumValue(this.f16999.convertUTCToLocal(j));
        }

        public int getMaximumValue(ReadablePartial readablePartial) {
            return this.f17002.getMaximumValue(readablePartial);
        }

        public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
            return this.f17002.getMaximumValue(readablePartial, iArr);
        }

        public int getMaximumTextLength(Locale locale) {
            return this.f17002.getMaximumTextLength(locale);
        }

        public int getMaximumShortTextLength(Locale locale) {
            return this.f17002.getMaximumShortTextLength(locale);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m21131(long j) {
            int offset = this.f16999.getOffset(j);
            if (((((long) offset) + j) ^ j) >= 0 || (((long) offset) ^ j) < 0) {
                return offset;
            }
            throw new ArithmeticException("Adding time zone offset caused overflow");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ZonedDateTimeField)) {
                return false;
            }
            ZonedDateTimeField zonedDateTimeField = (ZonedDateTimeField) obj;
            if (!this.f17002.equals(zonedDateTimeField.f17002) || !this.f16999.equals(zonedDateTimeField.f16999) || !this.f17001.equals(zonedDateTimeField.f17001) || !this.f16998.equals(zonedDateTimeField.f16998)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.f17002.hashCode() ^ this.f16999.hashCode();
        }
    }
}
