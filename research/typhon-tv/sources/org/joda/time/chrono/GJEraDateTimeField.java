package org.joda.time.chrono;

import java.util.Locale;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;
import org.joda.time.field.BaseDateTimeField;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.UnsupportedDurationField;

final class GJEraDateTimeField extends BaseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f16961;

    GJEraDateTimeField(BasicChronology basicChronology) {
        super(DateTimeFieldType.era());
        this.f16961 = basicChronology;
    }

    public boolean isLenient() {
        return false;
    }

    public int get(long j) {
        if (this.f16961.m20996(j) <= 0) {
            return 0;
        }
        return 1;
    }

    public String getAsText(int i, Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21074(i);
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, 0, 1);
        if (get(j) == i) {
            return j;
        }
        return this.f16961.m20965(j, -this.f16961.m20996(j));
    }

    public long set(long j, String str, Locale locale) {
        return set(j, GJLocaleSymbols.m21055(locale).m21073(str));
    }

    public long roundFloor(long j) {
        if (get(j) == 1) {
            return this.f16961.m20965(0, 1);
        }
        return Long.MIN_VALUE;
    }

    public long roundCeiling(long j) {
        if (get(j) == 0) {
            return this.f16961.m20965(0, 1);
        }
        return Long.MAX_VALUE;
    }

    public long roundHalfFloor(long j) {
        return roundFloor(j);
    }

    public long roundHalfCeiling(long j) {
        return roundFloor(j);
    }

    public long roundHalfEven(long j) {
        return roundFloor(j);
    }

    public DurationField getDurationField() {
        return UnsupportedDurationField.getInstance(DurationFieldType.eras());
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public int getMinimumValue() {
        return 0;
    }

    public int getMaximumValue() {
        return 1;
    }

    public int getMaximumTextLength(Locale locale) {
        return GJLocaleSymbols.m21055(locale).m21072();
    }
}
