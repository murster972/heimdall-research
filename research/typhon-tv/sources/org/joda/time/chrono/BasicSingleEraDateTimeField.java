package org.joda.time.chrono;

import java.util.Locale;
import net.pubnative.library.request.PubnativeRequest;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.field.BaseDateTimeField;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.UnsupportedDurationField;

final class BasicSingleEraDateTimeField extends BaseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f16934;

    BasicSingleEraDateTimeField(String str) {
        super(DateTimeFieldType.era());
        this.f16934 = str;
    }

    public boolean isLenient() {
        return false;
    }

    public int get(long j) {
        return 1;
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, 1, 1);
        return j;
    }

    public long set(long j, String str, Locale locale) {
        if (this.f16934.equals(str) || PubnativeRequest.LEGACY_ZONE_ID.equals(str)) {
            return j;
        }
        throw new IllegalFieldValueException(DateTimeFieldType.era(), str);
    }

    public long roundFloor(long j) {
        return Long.MIN_VALUE;
    }

    public long roundCeiling(long j) {
        return Long.MAX_VALUE;
    }

    public long roundHalfFloor(long j) {
        return Long.MIN_VALUE;
    }

    public long roundHalfCeiling(long j) {
        return Long.MIN_VALUE;
    }

    public long roundHalfEven(long j) {
        return Long.MIN_VALUE;
    }

    public DurationField getDurationField() {
        return UnsupportedDurationField.getInstance(DurationFieldType.eras());
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMaximumValue() {
        return 1;
    }

    public String getAsText(int i, Locale locale) {
        return this.f16934;
    }

    public int getMaximumTextLength(Locale locale) {
        return this.f16934.length();
    }
}
