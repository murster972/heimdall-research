package org.joda.time.chrono;

import java.text.DateFormatSymbols;
import java.util.Locale;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.IllegalFieldValueException;

class GJLocaleSymbols {

    /* renamed from: 龘  reason: contains not printable characters */
    private static ConcurrentMap<Locale, GJLocaleSymbols> f16962 = new ConcurrentHashMap();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String[] f16963;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String[] f16964;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final TreeMap<String, Integer> f16965;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f16966;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f16967;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f16968;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final TreeMap<String, Integer> f16969;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final TreeMap<String, Integer> f16970;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f16971;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String[] f16972;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String[] f16973;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String[] f16974;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String[] f16975;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final int f16976;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final int f16977;

    /* renamed from: 龘  reason: contains not printable characters */
    static GJLocaleSymbols m21055(Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        GJLocaleSymbols gJLocaleSymbols = (GJLocaleSymbols) f16962.get(locale);
        if (gJLocaleSymbols != null) {
            return gJLocaleSymbols;
        }
        GJLocaleSymbols gJLocaleSymbols2 = new GJLocaleSymbols(locale);
        GJLocaleSymbols putIfAbsent = f16962.putIfAbsent(locale, gJLocaleSymbols2);
        return putIfAbsent != null ? putIfAbsent : gJLocaleSymbols2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] m21058(String[] strArr) {
        String[] strArr2 = new String[13];
        for (int i = 1; i < 13; i++) {
            strArr2[i] = strArr[i - 1];
        }
        return strArr2;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String[] m21053(String[] strArr) {
        int i;
        String[] strArr2 = new String[8];
        for (int i2 = 1; i2 < 8; i2++) {
            if (i2 < 7) {
                i = i2 + 1;
            } else {
                i = 1;
            }
            strArr2[i2] = strArr[i];
        }
        return strArr2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21057(TreeMap<String, Integer> treeMap, String[] strArr, Integer[] numArr) {
        int length = strArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                String str = strArr[length];
                if (str != null) {
                    treeMap.put(str, numArr[length]);
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m21056(TreeMap<String, Integer> treeMap, int i, int i2, Integer[] numArr) {
        while (i <= i2) {
            treeMap.put(String.valueOf(i).intern(), numArr[i]);
            i++;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m21054(String[] strArr) {
        int i;
        int i2 = 0;
        int length = strArr.length;
        while (true) {
            length--;
            if (length < 0) {
                return i2;
            }
            String str = strArr[length];
            if (str == null || (i = str.length()) <= i2) {
                i = i2;
            }
            i2 = i;
        }
    }

    private GJLocaleSymbols(Locale locale) {
        DateFormatSymbols r0 = DateTimeUtils.m20894(locale);
        this.f16973 = r0.getEras();
        this.f16975 = m21053(r0.getWeekdays());
        this.f16974 = m21053(r0.getShortWeekdays());
        this.f16972 = m21058(r0.getMonths());
        this.f16963 = m21058(r0.getShortMonths());
        this.f16964 = r0.getAmPmStrings();
        Integer[] numArr = new Integer[13];
        for (int i = 0; i < 13; i++) {
            numArr[i] = Integer.valueOf(i);
        }
        this.f16965 = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        m21057(this.f16965, this.f16973, numArr);
        if ("en".equals(locale.getLanguage())) {
            this.f16965.put("BCE", numArr[0]);
            this.f16965.put("CE", numArr[1]);
        }
        this.f16969 = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        m21057(this.f16969, this.f16975, numArr);
        m21057(this.f16969, this.f16974, numArr);
        m21056(this.f16969, 1, 7, numArr);
        this.f16970 = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        m21057(this.f16970, this.f16972, numArr);
        m21057(this.f16970, this.f16963, numArr);
        m21056(this.f16970, 1, 12, numArr);
        this.f16971 = m21054(this.f16973);
        this.f16968 = m21054(this.f16975);
        this.f16966 = m21054(this.f16974);
        this.f16967 = m21054(this.f16972);
        this.f16976 = m21054(this.f16963);
        this.f16977 = m21054(this.f16964);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m21074(int i) {
        return this.f16973[i];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21073(String str) {
        Integer num = this.f16965.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalFieldValueException(DateTimeFieldType.era(), str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m21072() {
        return this.f16971;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m21065(int i) {
        return this.f16972[i];
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m21071(int i) {
        return this.f16963[i];
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m21064(String str) {
        Integer num = this.f16970.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalFieldValueException(DateTimeFieldType.monthOfYear(), str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m21063() {
        return this.f16967;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m21069() {
        return this.f16976;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m21068(int i) {
        return this.f16975[i];
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m21062(int i) {
        return this.f16974[i];
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m21070(String str) {
        Integer num = this.f16969.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalFieldValueException(DateTimeFieldType.dayOfWeek(), str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m21066() {
        return this.f16968;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m21061() {
        return this.f16966;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m21060(int i) {
        return this.f16964[i];
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m21067(String str) {
        String[] strArr = this.f16964;
        int length = strArr.length;
        do {
            length--;
            if (length < 0) {
                throw new IllegalFieldValueException(DateTimeFieldType.halfdayOfDay(), str);
            }
        } while (!strArr[length].equalsIgnoreCase(str));
        return length;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m21059() {
        return this.f16977;
    }
}
