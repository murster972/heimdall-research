package org.joda.time.chrono;

import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.DividedDateTimeField;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.MillisDurationField;
import org.joda.time.field.OffsetDateTimeField;
import org.joda.time.field.PreciseDateTimeField;
import org.joda.time.field.PreciseDurationField;
import org.joda.time.field.RemainderDateTimeField;
import org.joda.time.field.ZeroIsMaxDateTimeField;

abstract class BasicChronology extends AssembledChronology {
    private static final long serialVersionUID = 8283225332206808863L;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final DurationField f16904 = new PreciseDurationField(DurationFieldType.days(), 86400000);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final DurationField f16905 = new PreciseDurationField(DurationFieldType.weeks(), 604800000);

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final DateTimeField f16906 = new PreciseDateTimeField(DateTimeFieldType.millisOfSecond(), f16919, f16916);

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final DateTimeField f16907 = new PreciseDateTimeField(DateTimeFieldType.minuteOfDay(), f16918, f16904);

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final DateTimeField f16908 = new PreciseDateTimeField(DateTimeFieldType.hourOfDay(), f16917, f16904);

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final DateTimeField f16909 = new PreciseDateTimeField(DateTimeFieldType.minuteOfHour(), f16918, f16917);

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final DateTimeField f16910 = new ZeroIsMaxDateTimeField(f16920, DateTimeFieldType.clockhourOfHalfday());

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final DateTimeField f16911 = new HalfdayField();

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final DateTimeField f16912 = new PreciseDateTimeField(DateTimeFieldType.millisOfDay(), f16919, f16904);

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final DateTimeField f16913 = new PreciseDateTimeField(DateTimeFieldType.secondOfMinute(), f16916, f16918);

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final DateTimeField f16914 = new PreciseDateTimeField(DateTimeFieldType.secondOfDay(), f16916, f16904);
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final DurationField f16915 = new PreciseDurationField(DurationFieldType.halfdays(), 43200000);

    /* renamed from: 靐  reason: contains not printable characters */
    private static final DurationField f16916 = new PreciseDurationField(DurationFieldType.seconds(), 1000);

    /* renamed from: 麤  reason: contains not printable characters */
    private static final DurationField f16917 = new PreciseDurationField(DurationFieldType.hours(), 3600000);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final DurationField f16918 = new PreciseDurationField(DurationFieldType.minutes(), 60000);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DurationField f16919 = MillisDurationField.INSTANCE;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final DateTimeField f16920 = new PreciseDateTimeField(DateTimeFieldType.hourOfHalfday(), f16917, f16915);

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final DateTimeField f16921 = new ZeroIsMaxDateTimeField(f16908, DateTimeFieldType.clockhourOfDay());
    private final int iMinDaysInFirstWeek;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final transient YearInfo[] f16922 = new YearInfo[1024];

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m20962();

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m20963(int i);

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract long m20965(long j, int i);

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract long m20968(int i);

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract long m20971();

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract long m20973();

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public abstract long m20974();

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public abstract long m20976();

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public abstract int m20977();

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public abstract boolean m20980(int i);

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract int m20982(int i, int i2);

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract long m20994(int i, int i2);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m20997(long j, int i);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract long m21001(long j, long j2);

    BasicChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj);
        if (i < 1 || i > 7) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
        this.iMinDaysInFirstWeek = i;
    }

    public DateTimeZone getZone() {
        Chronology r0 = m20953();
        if (r0 != null) {
            return r0.getZone();
        }
        return DateTimeZone.UTC;
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology r0 = m20953();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4);
        }
        FieldUtils.m21218(DateTimeFieldType.millisOfDay(), i4, 0, 86399999);
        return m20961(i, i2, i3, i4);
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        Chronology r0 = m20953();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        }
        FieldUtils.m21218(DateTimeFieldType.hourOfDay(), i4, 0, 23);
        FieldUtils.m21218(DateTimeFieldType.minuteOfHour(), i5, 0, 59);
        FieldUtils.m21218(DateTimeFieldType.secondOfMinute(), i6, 0, 59);
        FieldUtils.m21218(DateTimeFieldType.millisOfSecond(), i7, 0, 999);
        return m20961(i, i2, i3, (int) ((long) ((3600000 * i4) + (60000 * i5) + (i6 * 1000) + i7)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m20961(int i, int i2, int i3, int i4) {
        long j;
        long r2 = m20985(i, i2, i3);
        if (r2 == Long.MIN_VALUE) {
            i4 -= 86400000;
            j = m20985(i, i2, i3 + 1);
        } else {
            j = r2;
        }
        long j2 = ((long) i4) + j;
        if (j2 < 0 && j > 0) {
            return Long.MAX_VALUE;
        }
        if (j2 <= 0 || j >= 0) {
            return j2;
        }
        return Long.MIN_VALUE;
    }

    public int getMinimumDaysInFirstWeek() {
        return this.iMinDaysInFirstWeek;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BasicChronology basicChronology = (BasicChronology) obj;
        if (getMinimumDaysInFirstWeek() != basicChronology.getMinimumDaysInFirstWeek() || !getZone().equals(basicChronology.getZone())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (getClass().getName().hashCode() * 11) + getZone().hashCode() + getMinimumDaysInFirstWeek();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(60);
        String name = getClass().getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf >= 0) {
            name = name.substring(lastIndexOf + 1);
        }
        sb.append(name);
        sb.append('[');
        DateTimeZone zone = getZone();
        if (zone != null) {
            sb.append(zone.getID());
        }
        if (getMinimumDaysInFirstWeek() != 4) {
            sb.append(",mdfw=");
            sb.append(getMinimumDaysInFirstWeek());
        }
        sb.append(']');
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21002(AssembledChronology.Fields fields) {
        fields.f16899 = f16919;
        fields.f16896 = f16916;
        fields.f16898 = f16918;
        fields.f16897 = f16917;
        fields.f16895 = f16915;
        fields.f16869 = f16904;
        fields.f16871 = f16905;
        fields.f16874 = f16906;
        fields.f16875 = f16912;
        fields.f16901 = f16913;
        fields.f16902 = f16914;
        fields.f16879 = f16909;
        fields.f16880 = f16907;
        fields.f16881 = f16908;
        fields.f16878 = f16920;
        fields.f16876 = f16921;
        fields.f16882 = f16910;
        fields.f16884 = f16911;
        fields.f16903 = new BasicYearDateTimeField(this);
        fields.f16888 = new GJYearOfEraDateTimeField(fields.f16903, this);
        fields.f16870 = new DividedDateTimeField(new OffsetDateTimeField(fields.f16888, 99), DateTimeFieldType.centuryOfEra(), 100);
        fields.f16887 = fields.f16870.getDurationField();
        fields.f16890 = new OffsetDateTimeField(new RemainderDateTimeField((DividedDateTimeField) fields.f16870), DateTimeFieldType.yearOfCentury(), 1);
        fields.f16873 = new GJEraDateTimeField(this);
        fields.f16885 = new GJDayOfWeekDateTimeField(this, fields.f16869);
        fields.f16889 = new BasicDayOfMonthDateTimeField(this, fields.f16869);
        fields.f16891 = new BasicDayOfYearDateTimeField(this, fields.f16869);
        fields.f16900 = new GJMonthOfYearDateTimeField(this);
        fields.f16893 = new BasicWeekyearDateTimeField(this);
        fields.f16892 = new BasicWeekOfWeekyearDateTimeField(this, fields.f16871);
        fields.f16894 = new OffsetDateTimeField(new RemainderDateTimeField(fields.f16893, fields.f16887, DateTimeFieldType.weekyearOfCentury(), 100), DateTimeFieldType.weekyearOfCentury(), 1);
        fields.f16886 = fields.f16903.getDurationField();
        fields.f16883 = fields.f16900.getDurationField();
        fields.f16872 = fields.f16893.getDurationField();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m20990() {
        return 366;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m20995(int i) {
        return m20980(i) ? 366 : 365;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m20981(int i) {
        return (int) ((m20993(i + 1) - m20993(i)) / 604800000);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m20993(int i) {
        long r0 = m20989(i);
        int r2 = m20967(r0);
        if (r2 > 8 - this.iMinDaysInFirstWeek) {
            return r0 + (((long) (8 - r2)) * 86400000);
        }
        return r0 - (((long) (r2 - 1)) * 86400000);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public long m20989(int i) {
        return m20960(i).f16923;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m20999(int i, int i2) {
        return m20989(i) + m20994(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m21000(int i, int i2, int i3) {
        return m20989(i) + m20994(i, i2) + (((long) (i3 - 1)) * 86400000);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m20996(long j) {
        long j2 = 31536000000L;
        long r4 = m20973();
        long r0 = (j >> 1) + m20976();
        if (r0 < 0) {
            r0 = (r0 - r4) + 1;
        }
        int i = (int) (r0 / r4);
        long r42 = m20989(i);
        long j3 = j - r42;
        if (j3 < 0) {
            return i - 1;
        }
        if (j3 < 31536000000L) {
            return i;
        }
        if (m20980(i)) {
            j2 = 31622400000L;
        }
        if (j2 + r42 <= j) {
            return i + 1;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m20983(long j) {
        return m20997(j, m20996(j));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m20991(long j) {
        int r0 = m20996(j);
        return m20998(j, r0, m20997(j, r0));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m20984(long j, int i) {
        return m20998(j, i, m20997(j, i));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m20998(long j, int i, int i2) {
        return ((int) ((j - (m20989(i) + m20994(i, i2))) / 86400000)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m20987(long j) {
        return m20992(j, m20996(j));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m20992(long j, int i) {
        return ((int) ((j - m20989(i)) / 86400000)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m20978(long j) {
        int r0 = m20996(j);
        int r1 = m20988(j, r0);
        if (r1 == 1) {
            return m20996(604800000 + j);
        }
        if (r1 > 51) {
            return m20996(j - 1209600000);
        }
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m20964(long j) {
        return m20988(j, m20996(j));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m20988(long j, int i) {
        long r0 = m20993(i);
        if (j < r0) {
            return m20981(i - 1);
        }
        if (j >= m20993(i + 1)) {
            return 1;
        }
        return ((int) ((j - r0) / 604800000)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m20967(long j) {
        long j2;
        if (j >= 0) {
            j2 = j / 86400000;
        } else {
            j2 = (j - 86399999) / 86400000;
            if (j2 < -3) {
                return ((int) ((j2 + 4) % 7)) + 7;
            }
        }
        return ((int) ((j2 + 3) % 7)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m20970(long j) {
        if (j >= 0) {
            return (int) (j % 86400000);
        }
        return 86399999 + ((int) ((1 + j) % 86400000));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m20986() {
        return 31;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public int m20972(long j) {
        int r0 = m20996(j);
        return m20982(r0, m20997(j, r0));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m20979(long j, int i) {
        return m20972(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public long m20985(int i, int i2, int i3) {
        FieldUtils.m21218(DateTimeFieldType.year(), i, m20977() - 1, m20962() + 1);
        FieldUtils.m21218(DateTimeFieldType.monthOfYear(), i2, 1, m20969(i));
        FieldUtils.m21218(DateTimeFieldType.dayOfMonth(), i3, 1, m20982(i, i2));
        long r0 = m21000(i, i2, i3);
        if (r0 < 0 && i == m20962() + 1) {
            return Long.MAX_VALUE;
        }
        if (r0 <= 0 || i != m20977() - 1) {
            return r0;
        }
        return Long.MIN_VALUE;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m20975(long j) {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m20969(int i) {
        return m20966();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m20966() {
        return 12;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private YearInfo m20960(int i) {
        YearInfo yearInfo = this.f16922[i & 1023];
        if (yearInfo != null && yearInfo.f16924 == i) {
            return yearInfo;
        }
        YearInfo yearInfo2 = new YearInfo(i, m20968(i));
        this.f16922[i & 1023] = yearInfo2;
        return yearInfo2;
    }

    private static class HalfdayField extends PreciseDateTimeField {
        HalfdayField() {
            super(DateTimeFieldType.halfdayOfDay(), BasicChronology.f16915, BasicChronology.f16904);
        }

        public String getAsText(int i, Locale locale) {
            return GJLocaleSymbols.m21055(locale).m21060(i);
        }

        public long set(long j, String str, Locale locale) {
            return set(j, GJLocaleSymbols.m21055(locale).m21067(str));
        }

        public int getMaximumTextLength(Locale locale) {
            return GJLocaleSymbols.m21055(locale).m21059();
        }
    }

    private static class YearInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        public final long f16923;

        /* renamed from: 龘  reason: contains not printable characters */
        public final int f16924;

        YearInfo(int i, long j) {
            this.f16924 = i;
            this.f16923 = j;
        }
    }
}
