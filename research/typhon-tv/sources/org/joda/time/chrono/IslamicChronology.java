package org.joda.time.chrono;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableDateTime;
import org.joda.time.chrono.AssembledChronology;

public final class IslamicChronology extends BasicChronology {
    public static final int AH = 1;
    public static final LeapYearPatternType LEAP_YEAR_15_BASED = new LeapYearPatternType(0, 623158436);
    public static final LeapYearPatternType LEAP_YEAR_16_BASED = new LeapYearPatternType(1, 623191204);
    public static final LeapYearPatternType LEAP_YEAR_HABASH_AL_HASIB = new LeapYearPatternType(3, 153692453);
    public static final LeapYearPatternType LEAP_YEAR_INDIAN = new LeapYearPatternType(2, 690562340);
    private static final long serialVersionUID = -3663823829888L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, IslamicChronology[]> f16985 = new ConcurrentHashMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final IslamicChronology f16986 = getInstance(DateTimeZone.UTC);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField f16987 = new BasicSingleEraDateTimeField("AH");
    private final LeapYearPatternType iLeapYears;

    public static IslamicChronology getInstanceUTC() {
        return f16986;
    }

    public static IslamicChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), LEAP_YEAR_16_BASED);
    }

    public static IslamicChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, LEAP_YEAR_16_BASED);
    }

    public static IslamicChronology getInstance(DateTimeZone dateTimeZone, LeapYearPatternType leapYearPatternType) {
        IslamicChronology[] islamicChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        IslamicChronology[] islamicChronologyArr2 = f16985.get(dateTimeZone);
        if (islamicChronologyArr2 == null) {
            IslamicChronology[] islamicChronologyArr3 = new IslamicChronology[4];
            IslamicChronology[] putIfAbsent = f16985.putIfAbsent(dateTimeZone, islamicChronologyArr3);
            islamicChronologyArr = putIfAbsent != null ? putIfAbsent : islamicChronologyArr3;
        } else {
            islamicChronologyArr = islamicChronologyArr2;
        }
        IslamicChronology islamicChronology = islamicChronologyArr[leapYearPatternType.index];
        if (islamicChronology == null) {
            synchronized (islamicChronologyArr) {
                islamicChronology = islamicChronologyArr[leapYearPatternType.index];
                if (islamicChronology == null) {
                    if (dateTimeZone == DateTimeZone.UTC) {
                        IslamicChronology islamicChronology2 = new IslamicChronology((Chronology) null, (Object) null, leapYearPatternType);
                        islamicChronology = new IslamicChronology(LimitChronology.getInstance(islamicChronology2, new DateTime(1, 1, 1, 0, 0, 0, 0, (Chronology) islamicChronology2), (ReadableDateTime) null), (Object) null, leapYearPatternType);
                    } else {
                        islamicChronology = new IslamicChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, leapYearPatternType), dateTimeZone), (Object) null, leapYearPatternType);
                    }
                    islamicChronologyArr[leapYearPatternType.index] = islamicChronology;
                }
            }
        }
        return islamicChronology;
    }

    IslamicChronology(Chronology chronology, Object obj, LeapYearPatternType leapYearPatternType) {
        super(chronology, obj, 4);
        this.iLeapYears = leapYearPatternType;
    }

    private Object readResolve() {
        Chronology r0 = m20953();
        return r0 == null ? getInstanceUTC() : getInstance(r0.getZone());
    }

    public LeapYearPatternType getLeapYearPatternType() {
        return this.iLeapYears;
    }

    public Chronology withUTC() {
        return f16986;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IslamicChronology)) {
            return false;
        }
        return getLeapYearPatternType().index == ((IslamicChronology) obj).getLeapYearPatternType().index && super.equals(obj);
    }

    public int hashCode() {
        return (super.hashCode() * 13) + getLeapYearPatternType().hashCode();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21102(long j) {
        long j2;
        long j3;
        long j4 = j - -42521587200000L;
        long j5 = j4 % 918518400000L;
        int i = (int) ((30 * (j4 / 918518400000L)) + 1);
        if (m21095(i)) {
            j2 = 30672000000L;
        } else {
            j2 = 30585600000L;
        }
        while (j5 >= j2) {
            j5 -= j2;
            i++;
            if (m21095(i)) {
                j3 = 30672000000L;
            } else {
                j3 = 30585600000L;
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public long m21088(long j, int i) {
        int r0 = m20992(j, m21102(j));
        int r1 = m20970(j);
        if (r0 > 354 && !m21095(i)) {
            r0--;
        }
        return ((long) r1) + m21000(i, 1, r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m21104(long j, long j2) {
        int r0 = m21102(j);
        int r1 = m21102(j2);
        int i = r0 - r1;
        if (j - m20989(r0) < j2 - m20989(r1)) {
            i--;
        }
        return (long) i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m21100(int i, int i2) {
        int i3 = i2 - 1;
        if (i3 % 2 == 1) {
            return (((long) (i3 / 2)) * 5097600000L) + 2592000000L;
        }
        return ((long) (i3 / 2)) * 5097600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m21099(long j) {
        int r0 = m20987(j) - 1;
        if (r0 == 354) {
            return 30;
        }
        return ((r0 % 59) % 30) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m21095(int i) {
        return this.iLeapYears.m21106(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m21098() {
        return 355;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21101(int i) {
        return m21095(i) ? 355 : 354;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m21096(int i, int i2) {
        if ((i2 != 12 || !m21095(i)) && (i2 - 1) % 2 != 0) {
            return 29;
        }
        return 30;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m21097() {
        return 30;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m21087(int i) {
        if (i == 12 || (i - 1) % 2 == 0) {
            return 30;
        }
        return 29;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21103(long j, int i) {
        int r0 = (int) ((j - m20989(i)) / 86400000);
        if (r0 == 354) {
            return 12;
        }
        return ((r0 * 2) / 59) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m21090() {
        return 30617280288L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m21091() {
        return 15308640144L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m21092() {
        return 2551440384L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m21089(int i) {
        if (i > 292271022) {
            throw new ArithmeticException("Year is too large: " + i + " > " + 292271022);
        } else if (i < -292269337) {
            throw new ArithmeticException("Year is too small: " + i + " < " + -292269337);
        } else {
            int i2 = i - 1;
            long j = -42521587200000L + (((long) (i2 / 30)) * 918518400000L);
            int i3 = (i2 % 30) + 1;
            for (int i4 = 1; i4 < i3; i4++) {
                j += m21095(i4) ? 30672000000L : 30585600000L;
            }
            return j;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m21094() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m21086() {
        return 292271022;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m21093() {
        return 21260793600000L;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21105(AssembledChronology.Fields fields) {
        if (m20953() == null) {
            super.m21002(fields);
            fields.f16873 = f16987;
            fields.f16900 = new BasicMonthOfYearDateTimeField(this, 12);
            fields.f16883 = fields.f16900.getDurationField();
        }
    }

    public static class LeapYearPatternType implements Serializable {
        private static final long serialVersionUID = 26581275372698L;
        final byte index;
        final int pattern;

        LeapYearPatternType(int i, int i2) {
            this.index = (byte) i;
            this.pattern = i2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m21106(int i) {
            if (((1 << (i % 30)) & this.pattern) > 0) {
                return true;
            }
            return false;
        }

        private Object readResolve() {
            switch (this.index) {
                case 0:
                    return IslamicChronology.LEAP_YEAR_15_BASED;
                case 1:
                    return IslamicChronology.LEAP_YEAR_16_BASED;
                case 2:
                    return IslamicChronology.LEAP_YEAR_INDIAN;
                case 3:
                    return IslamicChronology.LEAP_YEAR_HABASH_AL_HASIB;
                default:
                    return this;
            }
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof LeapYearPatternType) || this.index != ((LeapYearPatternType) obj).index) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.index;
        }
    }
}
