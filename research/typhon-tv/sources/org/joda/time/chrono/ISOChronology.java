package org.joda.time.chrono;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.DividedDateTimeField;
import org.joda.time.field.RemainderDateTimeField;

public final class ISOChronology extends AssembledChronology {
    private static final long serialVersionUID = -6212696554273812441L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, ISOChronology> f16981 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ISOChronology f16982 = new ISOChronology(GregorianChronology.getInstanceUTC());

    static {
        f16981.put(DateTimeZone.UTC, f16982);
    }

    public static ISOChronology getInstanceUTC() {
        return f16982;
    }

    public static ISOChronology getInstance() {
        return getInstance(DateTimeZone.getDefault());
    }

    public static ISOChronology getInstance(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        ISOChronology iSOChronology = f16981.get(dateTimeZone);
        if (iSOChronology != null) {
            return iSOChronology;
        }
        ISOChronology iSOChronology2 = new ISOChronology(ZonedChronology.getInstance(f16982, dateTimeZone));
        ISOChronology putIfAbsent = f16981.putIfAbsent(dateTimeZone, iSOChronology2);
        return putIfAbsent != null ? putIfAbsent : iSOChronology2;
    }

    private ISOChronology(Chronology chronology) {
        super(chronology, (Object) null);
    }

    public Chronology withUTC() {
        return f16982;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    public String toString() {
        DateTimeZone zone = getZone();
        if (zone != null) {
            return "ISOChronology" + '[' + zone.getID() + ']';
        }
        return "ISOChronology";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21085(AssembledChronology.Fields fields) {
        if (m20953().getZone() == DateTimeZone.UTC) {
            fields.f16870 = new DividedDateTimeField(ISOYearOfEraDateTimeField.f16984, DateTimeFieldType.centuryOfEra(), 100);
            fields.f16887 = fields.f16870.getDurationField();
            fields.f16890 = new RemainderDateTimeField((DividedDateTimeField) fields.f16870, DateTimeFieldType.yearOfCentury());
            fields.f16894 = new RemainderDateTimeField((DividedDateTimeField) fields.f16870, fields.f16872, DateTimeFieldType.weekyearOfCentury());
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ISOChronology) {
            return getZone().equals(((ISOChronology) obj).getZone());
        }
        return false;
    }

    public int hashCode() {
        return ("ISO".hashCode() * 11) + getZone().hashCode();
    }

    private Object writeReplace() {
        return new Stub(getZone());
    }

    private static final class Stub implements Serializable {
        private static final long serialVersionUID = -6212696554273812441L;

        /* renamed from: 龘  reason: contains not printable characters */
        private transient DateTimeZone f16983;

        Stub(DateTimeZone dateTimeZone) {
            this.f16983 = dateTimeZone;
        }

        private Object readResolve() {
            return ISOChronology.getInstance(this.f16983);
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeObject(this.f16983);
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            this.f16983 = (DateTimeZone) objectInputStream.readObject();
        }
    }
}
