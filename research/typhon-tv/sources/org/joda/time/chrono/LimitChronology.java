package org.joda.time.chrono;

import java.util.HashMap;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;
import org.joda.time.MutableDateTime;
import org.joda.time.ReadableDateTime;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.DecoratedDateTimeField;
import org.joda.time.field.DecoratedDurationField;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class LimitChronology extends AssembledChronology {
    private static final long serialVersionUID = 7670866536893052522L;
    final DateTime iLowerLimit;
    final DateTime iUpperLimit;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient LimitChronology f16991;

    public static LimitChronology getInstance(Chronology chronology, ReadableDateTime readableDateTime, ReadableDateTime readableDateTime2) {
        DateTime dateTime = null;
        if (chronology == null) {
            throw new IllegalArgumentException("Must supply a chronology");
        }
        DateTime dateTime2 = readableDateTime == null ? null : readableDateTime.toDateTime();
        if (readableDateTime2 != null) {
            dateTime = readableDateTime2.toDateTime();
        }
        if (dateTime2 == null || dateTime == null || dateTime2.isBefore(dateTime)) {
            return new LimitChronology(chronology, dateTime2, dateTime);
        }
        throw new IllegalArgumentException("The lower limit must be come before than the upper limit");
    }

    private LimitChronology(Chronology chronology, DateTime dateTime, DateTime dateTime2) {
        super(chronology, (Object) null);
        this.iLowerLimit = dateTime;
        this.iUpperLimit = dateTime2;
    }

    public DateTime getLowerLimit() {
        return this.iLowerLimit;
    }

    public DateTime getUpperLimit() {
        return this.iUpperLimit;
    }

    public Chronology withUTC() {
        return withZone(DateTimeZone.UTC);
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        if (dateTimeZone == getZone()) {
            return this;
        }
        if (dateTimeZone == DateTimeZone.UTC && this.f16991 != null) {
            return this.f16991;
        }
        DateTime dateTime = this.iLowerLimit;
        if (dateTime != null) {
            MutableDateTime mutableDateTime = dateTime.toMutableDateTime();
            mutableDateTime.setZoneRetainFields(dateTimeZone);
            dateTime = mutableDateTime.toDateTime();
        }
        DateTime dateTime2 = this.iUpperLimit;
        if (dateTime2 != null) {
            MutableDateTime mutableDateTime2 = dateTime2.toMutableDateTime();
            mutableDateTime2.setZoneRetainFields(dateTimeZone);
            dateTime2 = mutableDateTime2.toDateTime();
        }
        LimitChronology instance = getInstance(m20953().withZone(dateTimeZone), dateTime, dateTime2);
        if (dateTimeZone == DateTimeZone.UTC) {
            this.f16991 = instance;
        }
        return instance;
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        long dateTimeMillis = m20953().getDateTimeMillis(i, i2, i3, i4);
        m21122(dateTimeMillis, "resulting");
        return dateTimeMillis;
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        long dateTimeMillis = m20953().getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        m21122(dateTimeMillis, "resulting");
        return dateTimeMillis;
    }

    public long getDateTimeMillis(long j, int i, int i2, int i3, int i4) throws IllegalArgumentException {
        m21122(j, (String) null);
        long dateTimeMillis = m20953().getDateTimeMillis(j, i, i2, i3, i4);
        m21122(dateTimeMillis, "resulting");
        return dateTimeMillis;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21123(AssembledChronology.Fields fields) {
        HashMap hashMap = new HashMap();
        fields.f16877 = m21121(fields.f16877, (HashMap<Object, Object>) hashMap);
        fields.f16887 = m21121(fields.f16887, (HashMap<Object, Object>) hashMap);
        fields.f16886 = m21121(fields.f16886, (HashMap<Object, Object>) hashMap);
        fields.f16883 = m21121(fields.f16883, (HashMap<Object, Object>) hashMap);
        fields.f16872 = m21121(fields.f16872, (HashMap<Object, Object>) hashMap);
        fields.f16871 = m21121(fields.f16871, (HashMap<Object, Object>) hashMap);
        fields.f16869 = m21121(fields.f16869, (HashMap<Object, Object>) hashMap);
        fields.f16895 = m21121(fields.f16895, (HashMap<Object, Object>) hashMap);
        fields.f16897 = m21121(fields.f16897, (HashMap<Object, Object>) hashMap);
        fields.f16898 = m21121(fields.f16898, (HashMap<Object, Object>) hashMap);
        fields.f16896 = m21121(fields.f16896, (HashMap<Object, Object>) hashMap);
        fields.f16899 = m21121(fields.f16899, (HashMap<Object, Object>) hashMap);
        fields.f16903 = m21120(fields.f16903, (HashMap<Object, Object>) hashMap);
        fields.f16888 = m21120(fields.f16888, (HashMap<Object, Object>) hashMap);
        fields.f16890 = m21120(fields.f16890, (HashMap<Object, Object>) hashMap);
        fields.f16870 = m21120(fields.f16870, (HashMap<Object, Object>) hashMap);
        fields.f16873 = m21120(fields.f16873, (HashMap<Object, Object>) hashMap);
        fields.f16885 = m21120(fields.f16885, (HashMap<Object, Object>) hashMap);
        fields.f16889 = m21120(fields.f16889, (HashMap<Object, Object>) hashMap);
        fields.f16891 = m21120(fields.f16891, (HashMap<Object, Object>) hashMap);
        fields.f16900 = m21120(fields.f16900, (HashMap<Object, Object>) hashMap);
        fields.f16892 = m21120(fields.f16892, (HashMap<Object, Object>) hashMap);
        fields.f16893 = m21120(fields.f16893, (HashMap<Object, Object>) hashMap);
        fields.f16894 = m21120(fields.f16894, (HashMap<Object, Object>) hashMap);
        fields.f16874 = m21120(fields.f16874, (HashMap<Object, Object>) hashMap);
        fields.f16875 = m21120(fields.f16875, (HashMap<Object, Object>) hashMap);
        fields.f16901 = m21120(fields.f16901, (HashMap<Object, Object>) hashMap);
        fields.f16902 = m21120(fields.f16902, (HashMap<Object, Object>) hashMap);
        fields.f16879 = m21120(fields.f16879, (HashMap<Object, Object>) hashMap);
        fields.f16880 = m21120(fields.f16880, (HashMap<Object, Object>) hashMap);
        fields.f16881 = m21120(fields.f16881, (HashMap<Object, Object>) hashMap);
        fields.f16878 = m21120(fields.f16878, (HashMap<Object, Object>) hashMap);
        fields.f16876 = m21120(fields.f16876, (HashMap<Object, Object>) hashMap);
        fields.f16882 = m21120(fields.f16882, (HashMap<Object, Object>) hashMap);
        fields.f16884 = m21120(fields.f16884, (HashMap<Object, Object>) hashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DurationField m21121(DurationField durationField, HashMap<Object, Object> hashMap) {
        if (durationField == null || !durationField.isSupported()) {
            return durationField;
        }
        if (hashMap.containsKey(durationField)) {
            return (DurationField) hashMap.get(durationField);
        }
        LimitDurationField limitDurationField = new LimitDurationField(durationField);
        hashMap.put(durationField, limitDurationField);
        return limitDurationField;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeField m21120(DateTimeField dateTimeField, HashMap<Object, Object> hashMap) {
        if (dateTimeField == null || !dateTimeField.isSupported()) {
            return dateTimeField;
        }
        if (hashMap.containsKey(dateTimeField)) {
            return (DateTimeField) hashMap.get(dateTimeField);
        }
        LimitDateTimeField limitDateTimeField = new LimitDateTimeField(dateTimeField, m21121(dateTimeField.getDurationField(), hashMap), m21121(dateTimeField.getRangeDurationField(), hashMap), m21121(dateTimeField.getLeapDurationField(), hashMap));
        hashMap.put(dateTimeField, limitDateTimeField);
        return limitDateTimeField;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21122(long j, String str) {
        DateTime dateTime = this.iLowerLimit;
        if (dateTime == null || j >= dateTime.getMillis()) {
            DateTime dateTime2 = this.iUpperLimit;
            if (dateTime2 != null && j >= dateTime2.getMillis()) {
                throw new LimitException(str, false);
            }
            return;
        }
        throw new LimitException(str, true);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LimitChronology)) {
            return false;
        }
        LimitChronology limitChronology = (LimitChronology) obj;
        if (!m20953().equals(limitChronology.m20953()) || !FieldUtils.m21219((Object) getLowerLimit(), (Object) limitChronology.getLowerLimit()) || !FieldUtils.m21219((Object) getUpperLimit(), (Object) limitChronology.getUpperLimit())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (getLowerLimit() != null) {
            i = getLowerLimit().hashCode();
        } else {
            i = 0;
        }
        int i3 = i + 317351877;
        if (getUpperLimit() != null) {
            i2 = getUpperLimit().hashCode();
        }
        return i3 + i2 + (m20953().hashCode() * 7);
    }

    public String toString() {
        return "LimitChronology[" + m20953().toString() + ", " + (getLowerLimit() == null ? "NoLimit" : getLowerLimit().toString()) + ", " + (getUpperLimit() == null ? "NoLimit" : getUpperLimit().toString()) + ']';
    }

    private class LimitException extends IllegalArgumentException {
        private static final long serialVersionUID = -5924689995607498581L;
        private final boolean iIsLow;

        LimitException(String str, boolean z) {
            super(str);
            this.iIsLow = z;
        }

        public String getMessage() {
            StringBuffer stringBuffer = new StringBuffer(85);
            stringBuffer.append("The");
            String message = super.getMessage();
            if (message != null) {
                stringBuffer.append(' ');
                stringBuffer.append(message);
            }
            stringBuffer.append(" instant is ");
            DateTimeFormatter r1 = ISODateTimeFormat.m21444().m21248(LimitChronology.this.m20953());
            if (this.iIsLow) {
                stringBuffer.append("below the supported minimum of ");
                r1.m21254(stringBuffer, LimitChronology.this.getLowerLimit().getMillis());
            } else {
                stringBuffer.append("above the supported maximum of ");
                r1.m21254(stringBuffer, LimitChronology.this.getUpperLimit().getMillis());
            }
            stringBuffer.append(" (");
            stringBuffer.append(LimitChronology.this.m20953());
            stringBuffer.append(')');
            return stringBuffer.toString();
        }

        public String toString() {
            return "IllegalArgumentException: " + getMessage();
        }
    }

    private class LimitDurationField extends DecoratedDurationField {
        private static final long serialVersionUID = 8049297699408782284L;

        LimitDurationField(DurationField durationField) {
            super(durationField, durationField.getType());
        }

        public int getValue(long j, long j2) {
            LimitChronology.this.m21122(j2, (String) null);
            return getWrappedField().getValue(j, j2);
        }

        public long getValueAsLong(long j, long j2) {
            LimitChronology.this.m21122(j2, (String) null);
            return getWrappedField().getValueAsLong(j, j2);
        }

        public long getMillis(int i, long j) {
            LimitChronology.this.m21122(j, (String) null);
            return getWrappedField().getMillis(i, j);
        }

        public long getMillis(long j, long j2) {
            LimitChronology.this.m21122(j2, (String) null);
            return getWrappedField().getMillis(j, j2);
        }

        public long add(long j, int i) {
            LimitChronology.this.m21122(j, (String) null);
            long add = getWrappedField().add(j, i);
            LimitChronology.this.m21122(add, "resulting");
            return add;
        }

        public long add(long j, long j2) {
            LimitChronology.this.m21122(j, (String) null);
            long add = getWrappedField().add(j, j2);
            LimitChronology.this.m21122(add, "resulting");
            return add;
        }

        public int getDifference(long j, long j2) {
            LimitChronology.this.m21122(j, "minuend");
            LimitChronology.this.m21122(j2, "subtrahend");
            return getWrappedField().getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            LimitChronology.this.m21122(j, "minuend");
            LimitChronology.this.m21122(j2, "subtrahend");
            return getWrappedField().getDifferenceAsLong(j, j2);
        }
    }

    private class LimitDateTimeField extends DecoratedDateTimeField {

        /* renamed from: 靐  reason: contains not printable characters */
        private final DurationField f16992;

        /* renamed from: 麤  reason: contains not printable characters */
        private final DurationField f16993;

        /* renamed from: 齉  reason: contains not printable characters */
        private final DurationField f16994;

        LimitDateTimeField(DateTimeField dateTimeField, DurationField durationField, DurationField durationField2, DurationField durationField3) {
            super(dateTimeField, dateTimeField.getType());
            this.f16992 = durationField;
            this.f16994 = durationField2;
            this.f16993 = durationField3;
        }

        public int get(long j) {
            LimitChronology.this.m21122(j, (String) null);
            return m21203().get(j);
        }

        public String getAsText(long j, Locale locale) {
            LimitChronology.this.m21122(j, (String) null);
            return m21203().getAsText(j, locale);
        }

        public String getAsShortText(long j, Locale locale) {
            LimitChronology.this.m21122(j, (String) null);
            return m21203().getAsShortText(j, locale);
        }

        public long add(long j, int i) {
            LimitChronology.this.m21122(j, (String) null);
            long add = m21203().add(j, i);
            LimitChronology.this.m21122(add, "resulting");
            return add;
        }

        public long add(long j, long j2) {
            LimitChronology.this.m21122(j, (String) null);
            long add = m21203().add(j, j2);
            LimitChronology.this.m21122(add, "resulting");
            return add;
        }

        public long addWrapField(long j, int i) {
            LimitChronology.this.m21122(j, (String) null);
            long addWrapField = m21203().addWrapField(j, i);
            LimitChronology.this.m21122(addWrapField, "resulting");
            return addWrapField;
        }

        public int getDifference(long j, long j2) {
            LimitChronology.this.m21122(j, "minuend");
            LimitChronology.this.m21122(j2, "subtrahend");
            return m21203().getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            LimitChronology.this.m21122(j, "minuend");
            LimitChronology.this.m21122(j2, "subtrahend");
            return m21203().getDifferenceAsLong(j, j2);
        }

        public long set(long j, int i) {
            LimitChronology.this.m21122(j, (String) null);
            long j2 = m21203().set(j, i);
            LimitChronology.this.m21122(j2, "resulting");
            return j2;
        }

        public long set(long j, String str, Locale locale) {
            LimitChronology.this.m21122(j, (String) null);
            long j2 = m21203().set(j, str, locale);
            LimitChronology.this.m21122(j2, "resulting");
            return j2;
        }

        public final DurationField getDurationField() {
            return this.f16992;
        }

        public final DurationField getRangeDurationField() {
            return this.f16994;
        }

        public boolean isLeap(long j) {
            LimitChronology.this.m21122(j, (String) null);
            return m21203().isLeap(j);
        }

        public int getLeapAmount(long j) {
            LimitChronology.this.m21122(j, (String) null);
            return m21203().getLeapAmount(j);
        }

        public final DurationField getLeapDurationField() {
            return this.f16993;
        }

        public long roundFloor(long j) {
            LimitChronology.this.m21122(j, (String) null);
            long roundFloor = m21203().roundFloor(j);
            LimitChronology.this.m21122(roundFloor, "resulting");
            return roundFloor;
        }

        public long roundCeiling(long j) {
            LimitChronology.this.m21122(j, (String) null);
            long roundCeiling = m21203().roundCeiling(j);
            LimitChronology.this.m21122(roundCeiling, "resulting");
            return roundCeiling;
        }

        public long roundHalfFloor(long j) {
            LimitChronology.this.m21122(j, (String) null);
            long roundHalfFloor = m21203().roundHalfFloor(j);
            LimitChronology.this.m21122(roundHalfFloor, "resulting");
            return roundHalfFloor;
        }

        public long roundHalfCeiling(long j) {
            LimitChronology.this.m21122(j, (String) null);
            long roundHalfCeiling = m21203().roundHalfCeiling(j);
            LimitChronology.this.m21122(roundHalfCeiling, "resulting");
            return roundHalfCeiling;
        }

        public long roundHalfEven(long j) {
            LimitChronology.this.m21122(j, (String) null);
            long roundHalfEven = m21203().roundHalfEven(j);
            LimitChronology.this.m21122(roundHalfEven, "resulting");
            return roundHalfEven;
        }

        public long remainder(long j) {
            LimitChronology.this.m21122(j, (String) null);
            long remainder = m21203().remainder(j);
            LimitChronology.this.m21122(remainder, "resulting");
            return remainder;
        }

        public int getMinimumValue(long j) {
            LimitChronology.this.m21122(j, (String) null);
            return m21203().getMinimumValue(j);
        }

        public int getMaximumValue(long j) {
            LimitChronology.this.m21122(j, (String) null);
            return m21203().getMaximumValue(j);
        }

        public int getMaximumTextLength(Locale locale) {
            return m21203().getMaximumTextLength(locale);
        }

        public int getMaximumShortTextLength(Locale locale) {
            return m21203().getMaximumShortTextLength(locale);
        }
    }
}
