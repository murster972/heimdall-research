package org.joda.time.chrono;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;
import org.joda.time.field.DecoratedDateTimeField;
import org.joda.time.field.FieldUtils;

final class GJYearOfEraDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f16978;

    GJYearOfEraDateTimeField(DateTimeField dateTimeField, BasicChronology basicChronology) {
        super(dateTimeField, DateTimeFieldType.yearOfEra());
        this.f16978 = basicChronology;
    }

    public DurationField getRangeDurationField() {
        return this.f16978.eras();
    }

    public int get(long j) {
        int i = m21203().get(j);
        if (i <= 0) {
            return 1 - i;
        }
        return i;
    }

    public long add(long j, int i) {
        return m21203().add(j, i);
    }

    public long add(long j, long j2) {
        return m21203().add(j, j2);
    }

    public long addWrapField(long j, int i) {
        return m21203().addWrapField(j, i);
    }

    public int[] addWrapField(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        return m21203().addWrapField(readablePartial, i, iArr, i2);
    }

    public int getDifference(long j, long j2) {
        return m21203().getDifference(j, j2);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return m21203().getDifferenceAsLong(j, j2);
    }

    public long set(long j, int i) {
        FieldUtils.m21217((DateTimeField) this, i, 1, getMaximumValue());
        if (this.f16978.m20996(j) <= 0) {
            i = 1 - i;
        }
        return super.set(j, i);
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMaximumValue() {
        return m21203().getMaximumValue();
    }

    public long roundFloor(long j) {
        return m21203().roundFloor(j);
    }

    public long roundCeiling(long j) {
        return m21203().roundCeiling(j);
    }

    public long remainder(long j) {
        return m21203().remainder(j);
    }
}
