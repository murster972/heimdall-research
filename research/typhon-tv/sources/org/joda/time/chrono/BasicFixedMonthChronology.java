package org.joda.time.chrono;

import org.joda.time.Chronology;

abstract class BasicFixedMonthChronology extends BasicChronology {
    private static final long serialVersionUID = 261387371998L;

    BasicFixedMonthChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public long m21006(long j, int i) {
        int r0 = m20992(j, m20996(j));
        int r1 = m20970(j);
        if (r0 > 365 && !m21011(i)) {
            r0--;
        }
        return ((long) r1) + m21000(i, 1, r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m21018(long j, long j2) {
        int r0 = m20996(j);
        int r1 = m20996(j2);
        int i = r0 - r1;
        if (j - m20989(r0) < j2 - m20989(r1)) {
            i--;
        }
        return (long) i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m21016(int i, int i2) {
        return ((long) (i2 - 1)) * 2592000000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m21015(long j) {
        return ((m20987(j) - 1) % 30) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m21011(int i) {
        return (i & 3) == 3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m21012(int i, int i2) {
        if (i2 != 13) {
            return 30;
        }
        return m21011(i) ? 6 : 5;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m21014() {
        return 30;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m21005(int i) {
        return i != 13 ? 30 : 6;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m21013(long j) {
        return ((m20987(j) - 1) / 30) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m21017(long j, int i) {
        return ((int) ((j - m20989(i)) / 2592000000L)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m21007() {
        return 13;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m21008() {
        return 31557600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m21009() {
        return 15778800000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m21010() {
        return 2592000000L;
    }
}
