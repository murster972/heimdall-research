package org.joda.time.chrono;

import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.AssembledChronology;
import org.joda.time.field.StrictDateTimeField;

public final class StrictChronology extends AssembledChronology {
    private static final long serialVersionUID = 6633006628097111960L;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient Chronology f16996;

    public static StrictChronology getInstance(Chronology chronology) {
        if (chronology != null) {
            return new StrictChronology(chronology);
        }
        throw new IllegalArgumentException("Must supply a chronology");
    }

    private StrictChronology(Chronology chronology) {
        super(chronology, (Object) null);
    }

    public Chronology withUTC() {
        if (this.f16996 == null) {
            if (getZone() == DateTimeZone.UTC) {
                this.f16996 = this;
            } else {
                this.f16996 = getInstance(m20953().withUTC());
            }
        }
        return this.f16996;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        if (dateTimeZone == DateTimeZone.UTC) {
            return withUTC();
        }
        return dateTimeZone != getZone() ? getInstance(m20953().withZone(dateTimeZone)) : this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m21125(AssembledChronology.Fields fields) {
        fields.f16903 = m21124(fields.f16903);
        fields.f16888 = m21124(fields.f16888);
        fields.f16890 = m21124(fields.f16890);
        fields.f16870 = m21124(fields.f16870);
        fields.f16873 = m21124(fields.f16873);
        fields.f16885 = m21124(fields.f16885);
        fields.f16889 = m21124(fields.f16889);
        fields.f16891 = m21124(fields.f16891);
        fields.f16900 = m21124(fields.f16900);
        fields.f16892 = m21124(fields.f16892);
        fields.f16893 = m21124(fields.f16893);
        fields.f16894 = m21124(fields.f16894);
        fields.f16874 = m21124(fields.f16874);
        fields.f16875 = m21124(fields.f16875);
        fields.f16901 = m21124(fields.f16901);
        fields.f16902 = m21124(fields.f16902);
        fields.f16879 = m21124(fields.f16879);
        fields.f16880 = m21124(fields.f16880);
        fields.f16881 = m21124(fields.f16881);
        fields.f16878 = m21124(fields.f16878);
        fields.f16876 = m21124(fields.f16876);
        fields.f16882 = m21124(fields.f16882);
        fields.f16884 = m21124(fields.f16884);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField m21124(DateTimeField dateTimeField) {
        return StrictDateTimeField.getInstance(dateTimeField);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StrictChronology)) {
            return false;
        }
        return m20953().equals(((StrictChronology) obj).m20953());
    }

    public int hashCode() {
        return 352831696 + (m20953().hashCode() * 7);
    }

    public String toString() {
        return "StrictChronology[" + m20953().toString() + ']';
    }
}
