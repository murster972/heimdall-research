package org.joda.time;

public interface ReadWritableInterval extends ReadableInterval {
    void setChronology(Chronology chronology);

    void setInterval(long j, long j2);

    void setInterval(ReadableInterval readableInterval);
}
