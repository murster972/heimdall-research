package org.joda.time;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.joda.time.field.FieldUtils;

public class PeriodType implements Serializable {
    private static final long serialVersionUID = 2274324892792009998L;

    /* renamed from: ʻ  reason: contains not printable characters */
    static int f16805 = 5;

    /* renamed from: ʼ  reason: contains not printable characters */
    static int f16806 = 6;

    /* renamed from: ʽ  reason: contains not printable characters */
    static int f16807 = 7;

    /* renamed from: ʾ  reason: contains not printable characters */
    private static PeriodType f16808;

    /* renamed from: ʿ  reason: contains not printable characters */
    private static PeriodType f16809;

    /* renamed from: ˆ  reason: contains not printable characters */
    private static PeriodType f16810;

    /* renamed from: ˈ  reason: contains not printable characters */
    private static PeriodType f16811;

    /* renamed from: ˉ  reason: contains not printable characters */
    private static PeriodType f16812;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static PeriodType f16813;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static PeriodType f16814;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static PeriodType f16815;

    /* renamed from: ˏ  reason: contains not printable characters */
    private static PeriodType f16816;

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final Map<PeriodType, Object> f16817 = new HashMap(32);

    /* renamed from: י  reason: contains not printable characters */
    private static PeriodType f16818;

    /* renamed from: ـ  reason: contains not printable characters */
    private static PeriodType f16819;

    /* renamed from: ٴ  reason: contains not printable characters */
    private static PeriodType f16820;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static PeriodType f16821;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private static PeriodType f16822;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private static PeriodType f16823;

    /* renamed from: 连任  reason: contains not printable characters */
    static int f16824 = 4;

    /* renamed from: 靐  reason: contains not printable characters */
    static int f16825 = 1;

    /* renamed from: 麤  reason: contains not printable characters */
    static int f16826 = 3;

    /* renamed from: 齉  reason: contains not printable characters */
    static int f16827 = 2;

    /* renamed from: 龘  reason: contains not printable characters */
    static int f16828 = 0;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static PeriodType f16829;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static PeriodType f16830;
    private final int[] iIndices;
    private final String iName;
    private final DurationFieldType[] iTypes;

    public static PeriodType standard() {
        PeriodType periodType = f16820;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Standard", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.months(), DurationFieldType.weeks(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, 1, 2, 3, 4, 5, 6, 7});
        f16820 = periodType2;
        return periodType2;
    }

    public static PeriodType yearMonthDayTime() {
        PeriodType periodType = f16821;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearMonthDayTime", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.months(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, 1, -1, 2, 3, 4, 5, 6});
        f16821 = periodType2;
        return periodType2;
    }

    public static PeriodType yearMonthDay() {
        PeriodType periodType = f16811;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearMonthDay", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.months(), DurationFieldType.days()}, new int[]{0, 1, -1, 2, -1, -1, -1, -1});
        f16811 = periodType2;
        return periodType2;
    }

    public static PeriodType yearWeekDayTime() {
        PeriodType periodType = f16808;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearWeekDayTime", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.weeks(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, -1, 1, 2, 3, 4, 5, 6});
        f16808 = periodType2;
        return periodType2;
    }

    public static PeriodType yearWeekDay() {
        PeriodType periodType = f16809;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearWeekDay", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.weeks(), DurationFieldType.days()}, new int[]{0, -1, 1, 2, -1, -1, -1, -1});
        f16809 = periodType2;
        return periodType2;
    }

    public static PeriodType yearDayTime() {
        PeriodType periodType = f16829;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearDayTime", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, -1, -1, 1, 2, 3, 4, 5});
        f16829 = periodType2;
        return periodType2;
    }

    public static PeriodType yearDay() {
        PeriodType periodType = f16830;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearDay", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.days()}, new int[]{0, -1, -1, 1, -1, -1, -1, -1});
        f16830 = periodType2;
        return periodType2;
    }

    public static PeriodType dayTime() {
        PeriodType periodType = f16813;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("DayTime", new DurationFieldType[]{DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{-1, -1, -1, 0, 1, 2, 3, 4});
        f16813 = periodType2;
        return periodType2;
    }

    public static PeriodType time() {
        PeriodType periodType = f16814;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Time", new DurationFieldType[]{DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{-1, -1, -1, -1, 0, 1, 2, 3});
        f16814 = periodType2;
        return periodType2;
    }

    public static PeriodType years() {
        PeriodType periodType = f16815;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Years", new DurationFieldType[]{DurationFieldType.years()}, new int[]{0, -1, -1, -1, -1, -1, -1, -1});
        f16815 = periodType2;
        return periodType2;
    }

    public static PeriodType months() {
        PeriodType periodType = f16810;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Months", new DurationFieldType[]{DurationFieldType.months()}, new int[]{-1, 0, -1, -1, -1, -1, -1, -1});
        f16810 = periodType2;
        return periodType2;
    }

    public static PeriodType weeks() {
        PeriodType periodType = f16812;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Weeks", new DurationFieldType[]{DurationFieldType.weeks()}, new int[]{-1, -1, 0, -1, -1, -1, -1, -1});
        f16812 = periodType2;
        return periodType2;
    }

    public static PeriodType days() {
        PeriodType periodType = f16816;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Days", new DurationFieldType[]{DurationFieldType.days()}, new int[]{-1, -1, -1, 0, -1, -1, -1, -1});
        f16816 = periodType2;
        return periodType2;
    }

    public static PeriodType hours() {
        PeriodType periodType = f16818;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Hours", new DurationFieldType[]{DurationFieldType.hours()}, new int[]{-1, -1, -1, -1, 0, -1, -1, -1});
        f16818 = periodType2;
        return periodType2;
    }

    public static PeriodType minutes() {
        PeriodType periodType = f16819;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Minutes", new DurationFieldType[]{DurationFieldType.minutes()}, new int[]{-1, -1, -1, -1, -1, 0, -1, -1});
        f16819 = periodType2;
        return periodType2;
    }

    public static PeriodType seconds() {
        PeriodType periodType = f16822;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Seconds", new DurationFieldType[]{DurationFieldType.seconds()}, new int[]{-1, -1, -1, -1, -1, -1, 0, -1});
        f16822 = periodType2;
        return periodType2;
    }

    public static PeriodType millis() {
        PeriodType periodType = f16823;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Millis", new DurationFieldType[]{DurationFieldType.millis()}, new int[]{-1, -1, -1, -1, -1, -1, -1, 0});
        f16823 = periodType2;
        return periodType2;
    }

    public static synchronized PeriodType forFields(DurationFieldType[] durationFieldTypeArr) {
        PeriodType periodType;
        synchronized (PeriodType.class) {
            if (durationFieldTypeArr != null) {
                if (durationFieldTypeArr.length != 0) {
                    for (DurationFieldType durationFieldType : durationFieldTypeArr) {
                        if (durationFieldType == null) {
                            throw new IllegalArgumentException("Types array must not contain null");
                        }
                    }
                    Map<PeriodType, Object> map = f16817;
                    if (map.isEmpty()) {
                        map.put(standard(), standard());
                        map.put(yearMonthDayTime(), yearMonthDayTime());
                        map.put(yearMonthDay(), yearMonthDay());
                        map.put(yearWeekDayTime(), yearWeekDayTime());
                        map.put(yearWeekDay(), yearWeekDay());
                        map.put(yearDayTime(), yearDayTime());
                        map.put(yearDay(), yearDay());
                        map.put(dayTime(), dayTime());
                        map.put(time(), time());
                        map.put(years(), years());
                        map.put(months(), months());
                        map.put(weeks(), weeks());
                        map.put(days(), days());
                        map.put(hours(), hours());
                        map.put(minutes(), minutes());
                        map.put(seconds(), seconds());
                        map.put(millis(), millis());
                    }
                    PeriodType periodType2 = new PeriodType((String) null, durationFieldTypeArr, (int[]) null);
                    Object obj = map.get(periodType2);
                    if (obj instanceof PeriodType) {
                        periodType = (PeriodType) obj;
                    } else if (obj != null) {
                        throw new IllegalArgumentException("PeriodType does not support fields: " + obj);
                    } else {
                        PeriodType standard = standard();
                        ArrayList arrayList = new ArrayList(Arrays.asList(durationFieldTypeArr));
                        if (!arrayList.remove(DurationFieldType.years())) {
                            standard = standard.withYearsRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.months())) {
                            standard = standard.withMonthsRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.weeks())) {
                            standard = standard.withWeeksRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.days())) {
                            standard = standard.withDaysRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.hours())) {
                            standard = standard.withHoursRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.minutes())) {
                            standard = standard.withMinutesRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.seconds())) {
                            standard = standard.withSecondsRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.millis())) {
                            standard = standard.withMillisRemoved();
                        }
                        if (arrayList.size() > 0) {
                            map.put(periodType2, arrayList);
                            throw new IllegalArgumentException("PeriodType does not support fields: " + arrayList);
                        }
                        PeriodType periodType3 = new PeriodType((String) null, standard.iTypes, (int[]) null);
                        periodType = (PeriodType) map.get(periodType3);
                        if (periodType != null) {
                            map.put(periodType3, periodType);
                        } else {
                            map.put(periodType3, standard);
                            periodType = standard;
                        }
                    }
                }
            }
            throw new IllegalArgumentException("Types array must not be null or empty");
        }
        return periodType;
    }

    protected PeriodType(String str, DurationFieldType[] durationFieldTypeArr, int[] iArr) {
        this.iName = str;
        this.iTypes = durationFieldTypeArr;
        this.iIndices = iArr;
    }

    public String getName() {
        return this.iName;
    }

    public int size() {
        return this.iTypes.length;
    }

    public DurationFieldType getFieldType(int i) {
        return this.iTypes[i];
    }

    public boolean isSupported(DurationFieldType durationFieldType) {
        return indexOf(durationFieldType) >= 0;
    }

    public int indexOf(DurationFieldType durationFieldType) {
        int size = size();
        for (int i = 0; i < size; i++) {
            if (this.iTypes[i] == durationFieldType) {
                return i;
            }
        }
        return -1;
    }

    public String toString() {
        return "PeriodType[" + getName() + "]";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m20922(ReadablePeriod readablePeriod, int i) {
        int i2 = this.iIndices[i];
        if (i2 == -1) {
            return 0;
        }
        return readablePeriod.getValue(i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20923(ReadablePeriod readablePeriod, int i, int[] iArr, int i2) {
        int i3 = this.iIndices[i];
        if (i3 == -1) {
            throw new UnsupportedOperationException("Field is not supported");
        }
        iArr[i3] = i2;
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m20921(ReadablePeriod readablePeriod, int i, int[] iArr, int i2) {
        if (i2 == 0) {
            return false;
        }
        int i3 = this.iIndices[i];
        if (i3 == -1) {
            throw new UnsupportedOperationException("Field is not supported");
        }
        iArr[i3] = FieldUtils.m21210(iArr[i3], i2);
        return true;
    }

    public PeriodType withYearsRemoved() {
        return m20920(0, "NoYears");
    }

    public PeriodType withMonthsRemoved() {
        return m20920(1, "NoMonths");
    }

    public PeriodType withWeeksRemoved() {
        return m20920(2, "NoWeeks");
    }

    public PeriodType withDaysRemoved() {
        return m20920(3, "NoDays");
    }

    public PeriodType withHoursRemoved() {
        return m20920(4, "NoHours");
    }

    public PeriodType withMinutesRemoved() {
        return m20920(5, "NoMinutes");
    }

    public PeriodType withSecondsRemoved() {
        return m20920(6, "NoSeconds");
    }

    public PeriodType withMillisRemoved() {
        return m20920(7, "NoMillis");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodType m20920(int i, String str) {
        int i2 = this.iIndices[i];
        if (i2 == -1) {
            return this;
        }
        DurationFieldType[] durationFieldTypeArr = new DurationFieldType[(size() - 1)];
        for (int i3 = 0; i3 < this.iTypes.length; i3++) {
            if (i3 < i2) {
                durationFieldTypeArr[i3] = this.iTypes[i3];
            } else if (i3 > i2) {
                durationFieldTypeArr[i3 - 1] = this.iTypes[i3];
            }
        }
        int[] iArr = new int[8];
        for (int i4 = 0; i4 < iArr.length; i4++) {
            if (i4 < i) {
                iArr[i4] = this.iIndices[i4];
            } else if (i4 > i) {
                iArr[i4] = this.iIndices[i4] == -1 ? -1 : this.iIndices[i4] - 1;
            } else {
                iArr[i4] = -1;
            }
        }
        return new PeriodType(getName() + str, durationFieldTypeArr, iArr);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PeriodType)) {
            return false;
        }
        return Arrays.equals(this.iTypes, ((PeriodType) obj).iTypes);
    }

    public int hashCode() {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= this.iTypes.length) {
                return i3;
            }
            i = this.iTypes[i2].hashCode() + i3;
            i2++;
        }
    }
}
