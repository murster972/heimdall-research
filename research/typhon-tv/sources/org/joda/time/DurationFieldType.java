package org.joda.time;

import java.io.Serializable;

public abstract class DurationFieldType implements Serializable {
    private static final long serialVersionUID = 8765135187319L;

    /* renamed from: ʻ  reason: contains not printable characters */
    static final DurationFieldType f16786 = new StandardDurationFieldType("weeks", (byte) 6);

    /* renamed from: ʼ  reason: contains not printable characters */
    static final DurationFieldType f16787 = new StandardDurationFieldType("days", (byte) 7);

    /* renamed from: ʽ  reason: contains not printable characters */
    static final DurationFieldType f16788 = new StandardDurationFieldType("halfdays", (byte) 8);

    /* renamed from: ˈ  reason: contains not printable characters */
    static final DurationFieldType f16789 = new StandardDurationFieldType("millis", (byte) 12);

    /* renamed from: ˑ  reason: contains not printable characters */
    static final DurationFieldType f16790 = new StandardDurationFieldType("hours", (byte) 9);

    /* renamed from: ٴ  reason: contains not printable characters */
    static final DurationFieldType f16791 = new StandardDurationFieldType("minutes", (byte) 10);

    /* renamed from: ᐧ  reason: contains not printable characters */
    static final DurationFieldType f16792 = new StandardDurationFieldType("seconds", (byte) 11);

    /* renamed from: 连任  reason: contains not printable characters */
    static final DurationFieldType f16793 = new StandardDurationFieldType("months", (byte) 5);

    /* renamed from: 靐  reason: contains not printable characters */
    static final DurationFieldType f16794 = new StandardDurationFieldType("centuries", (byte) 2);

    /* renamed from: 麤  reason: contains not printable characters */
    static final DurationFieldType f16795 = new StandardDurationFieldType("years", (byte) 4);

    /* renamed from: 齉  reason: contains not printable characters */
    static final DurationFieldType f16796 = new StandardDurationFieldType("weekyears", (byte) 3);

    /* renamed from: 龘  reason: contains not printable characters */
    static final DurationFieldType f16797 = new StandardDurationFieldType("eras", (byte) 1);
    private final String iName;

    public abstract DurationField getField(Chronology chronology);

    protected DurationFieldType(String str) {
        this.iName = str;
    }

    public static DurationFieldType millis() {
        return f16789;
    }

    public static DurationFieldType seconds() {
        return f16792;
    }

    public static DurationFieldType minutes() {
        return f16791;
    }

    public static DurationFieldType hours() {
        return f16790;
    }

    public static DurationFieldType halfdays() {
        return f16788;
    }

    public static DurationFieldType days() {
        return f16787;
    }

    public static DurationFieldType weeks() {
        return f16786;
    }

    public static DurationFieldType weekyears() {
        return f16796;
    }

    public static DurationFieldType months() {
        return f16793;
    }

    public static DurationFieldType years() {
        return f16795;
    }

    public static DurationFieldType centuries() {
        return f16794;
    }

    public static DurationFieldType eras() {
        return f16797;
    }

    public String getName() {
        return this.iName;
    }

    public boolean isSupported(Chronology chronology) {
        return getField(chronology).isSupported();
    }

    public String toString() {
        return getName();
    }

    private static class StandardDurationFieldType extends DurationFieldType {
        private static final long serialVersionUID = 31156755687123L;
        private final byte iOrdinal;

        StandardDurationFieldType(String str, byte b) {
            super(str);
            this.iOrdinal = b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StandardDurationFieldType)) {
                return false;
            }
            if (this.iOrdinal != ((StandardDurationFieldType) obj).iOrdinal) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return 1 << this.iOrdinal;
        }

        public DurationField getField(Chronology chronology) {
            Chronology r0 = DateTimeUtils.m20895(chronology);
            switch (this.iOrdinal) {
                case 1:
                    return r0.eras();
                case 2:
                    return r0.centuries();
                case 3:
                    return r0.weekyears();
                case 4:
                    return r0.years();
                case 5:
                    return r0.months();
                case 6:
                    return r0.weeks();
                case 7:
                    return r0.days();
                case 8:
                    return r0.halfdays();
                case 9:
                    return r0.hours();
                case 10:
                    return r0.minutes();
                case 11:
                    return r0.seconds();
                case 12:
                    return r0.millis();
                default:
                    throw new InternalError();
            }
        }

        private Object readResolve() {
            switch (this.iOrdinal) {
                case 1:
                    return f16797;
                case 2:
                    return f16794;
                case 3:
                    return f16796;
                case 4:
                    return f16795;
                case 5:
                    return f16793;
                case 6:
                    return f16786;
                case 7:
                    return f16787;
                case 8:
                    return f16788;
                case 9:
                    return f16790;
                case 10:
                    return f16791;
                case 11:
                    return f16792;
                case 12:
                    return f16789;
                default:
                    return this;
            }
        }
    }
}
