package org.joda.time;

import java.text.DateFormatSymbols;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.time.TimeZones;
import org.joda.time.chrono.ISOChronology;

public class DateTimeUtils {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile MillisProvider f16780 = f16782;

    /* renamed from: 齉  reason: contains not printable characters */
    private static final AtomicReference<Map<String, DateTimeZone>> f16781 = new AtomicReference<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final MillisProvider f16782 = new SystemMillisProvider();

    public interface MillisProvider {
        /* renamed from: 龘  reason: contains not printable characters */
        long m20902();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final long m20891() {
        return f16780.m20902();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final long m20893(ReadableInstant readableInstant) {
        if (readableInstant == null) {
            return m20891();
        }
        return readableInstant.getMillis();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Chronology m20888(ReadableInstant readableInstant) {
        if (readableInstant == null) {
            return ISOChronology.getInstance();
        }
        Chronology chronology = readableInstant.getChronology();
        if (chronology == null) {
            return ISOChronology.getInstance();
        }
        return chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Chronology m20896(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        Chronology chronology = null;
        if (readableInstant != null) {
            chronology = readableInstant.getChronology();
        } else if (readableInstant2 != null) {
            chronology = readableInstant2.getChronology();
        }
        if (chronology == null) {
            return ISOChronology.getInstance();
        }
        return chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Chronology m20897(ReadableInterval readableInterval) {
        if (readableInterval == null) {
            return ISOChronology.getInstance();
        }
        Chronology chronology = readableInterval.getChronology();
        if (chronology == null) {
            return ISOChronology.getInstance();
        }
        return chronology;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ReadableInterval m20889(ReadableInterval readableInterval) {
        if (readableInterval != null) {
            return readableInterval;
        }
        long r0 = m20891();
        return new Interval(r0, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Chronology m20895(Chronology chronology) {
        if (chronology == null) {
            return ISOChronology.getInstance();
        }
        return chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final DateTimeZone m20898(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            return DateTimeZone.getDefault();
        }
        return dateTimeZone;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final PeriodType m20899(PeriodType periodType) {
        if (periodType == null) {
            return PeriodType.standard();
        }
        return periodType;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final long m20892(ReadableDuration readableDuration) {
        if (readableDuration == null) {
            return 0;
        }
        return readableDuration.getMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final boolean m20901(ReadablePartial readablePartial) {
        if (readablePartial == null) {
            throw new IllegalArgumentException("Partial must not be null");
        }
        DurationFieldType durationFieldType = null;
        for (int i = 0; i < readablePartial.size(); i++) {
            DateTimeField field = readablePartial.getField(i);
            if (i > 0 && (field.getRangeDurationField() == null || field.getRangeDurationField().getType() != durationFieldType)) {
                return false;
            }
            durationFieldType = field.getDurationField().getType();
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final DateFormatSymbols m20894(Locale locale) {
        try {
            return (DateFormatSymbols) DateFormatSymbols.class.getMethod("getInstance", new Class[]{Locale.class}).invoke((Object) null, new Object[]{locale});
        } catch (Exception e) {
            return new DateFormatSymbols(locale);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Map<String, DateTimeZone> m20887() {
        Map<String, DateTimeZone> map = f16781.get();
        if (map != null) {
            return map;
        }
        Map<String, DateTimeZone> r0 = m20890();
        if (!f16781.compareAndSet((Object) null, r0)) {
            return f16781.get();
        }
        return r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static Map<String, DateTimeZone> m20890() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("UT", DateTimeZone.UTC);
        linkedHashMap.put("UTC", DateTimeZone.UTC);
        linkedHashMap.put(TimeZones.GMT_ID, DateTimeZone.UTC);
        m20900(linkedHashMap, "EST", "America/New_York");
        m20900(linkedHashMap, "EDT", "America/New_York");
        m20900(linkedHashMap, "CST", "America/Chicago");
        m20900(linkedHashMap, "CDT", "America/Chicago");
        m20900(linkedHashMap, "MST", "America/Denver");
        m20900(linkedHashMap, "MDT", "America/Denver");
        m20900(linkedHashMap, "PST", "America/Los_Angeles");
        m20900(linkedHashMap, "PDT", "America/Los_Angeles");
        return Collections.unmodifiableMap(linkedHashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m20900(Map<String, DateTimeZone> map, String str, String str2) {
        try {
            map.put(str, DateTimeZone.forID(str2));
        } catch (RuntimeException e) {
        }
    }

    static class SystemMillisProvider implements MillisProvider {
        SystemMillisProvider() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m20903() {
            return System.currentTimeMillis();
        }
    }
}
