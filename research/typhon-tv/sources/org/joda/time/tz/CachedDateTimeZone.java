package org.joda.time.tz;

import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.joda.time.DateTimeZone;

public class CachedDateTimeZone extends DateTimeZone {
    private static final long serialVersionUID = 5472298452022250685L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f17220;
    private final DateTimeZone iZone;

    /* renamed from: 靐  reason: contains not printable characters */
    private final transient Info[] f17221 = new Info[(f17220 + 1)];

    static {
        Integer num;
        int i;
        try {
            num = Integer.getInteger("org.joda.time.tz.CachedDateTimeZone.size");
        } catch (SecurityException e) {
            num = null;
        }
        if (num == null) {
            i = 512;
        } else {
            int i2 = 0;
            for (int intValue = num.intValue() - 1; intValue > 0; intValue >>= 1) {
                i2++;
            }
            i = 1 << i2;
        }
        f17220 = i - 1;
    }

    public static CachedDateTimeZone forZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone instanceof CachedDateTimeZone) {
            return (CachedDateTimeZone) dateTimeZone;
        }
        return new CachedDateTimeZone(dateTimeZone);
    }

    private CachedDateTimeZone(DateTimeZone dateTimeZone) {
        super(dateTimeZone.getID());
        this.iZone = dateTimeZone;
    }

    public DateTimeZone getUncachedZone() {
        return this.iZone;
    }

    public String getNameKey(long j) {
        return m21630(j).m21633(j);
    }

    public int getOffset(long j) {
        return m21630(j).m21631(j);
    }

    public int getStandardOffset(long j) {
        return m21630(j).m21632(j);
    }

    public boolean isFixed() {
        return this.iZone.isFixed();
    }

    public long nextTransition(long j) {
        return this.iZone.nextTransition(j);
    }

    public long previousTransition(long j) {
        return this.iZone.previousTransition(j);
    }

    public int hashCode() {
        return this.iZone.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CachedDateTimeZone) {
            return this.iZone.equals(((CachedDateTimeZone) obj).iZone);
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Info m21630(long j) {
        int i = (int) (j >> 32);
        Info[] infoArr = this.f17221;
        int i2 = i & f17220;
        Info info = infoArr[i2];
        if (info != null && ((int) (info.f17227 >> 32)) == i) {
            return info;
        }
        Info r0 = m21629(j);
        infoArr[i2] = r0;
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Info m21629(long j) {
        long j2 = j & -4294967296L;
        Info info = new Info(this.iZone, j2);
        long j3 = j2 | InternalZipTyphoonApp.ZIP_64_LIMIT;
        Info info2 = info;
        long j4 = j2;
        while (true) {
            long nextTransition = this.iZone.nextTransition(j4);
            if (nextTransition == j4 || nextTransition > j3) {
                return info;
            }
            Info info3 = new Info(this.iZone, nextTransition);
            info2.f17226 = info3;
            info2 = info3;
            j4 = nextTransition;
        }
        return info;
    }

    private static final class Info {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f17222 = Integer.MIN_VALUE;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f17223 = Integer.MIN_VALUE;

        /* renamed from: 靐  reason: contains not printable characters */
        public final DateTimeZone f17224;

        /* renamed from: 麤  reason: contains not printable characters */
        private String f17225;

        /* renamed from: 齉  reason: contains not printable characters */
        Info f17226;

        /* renamed from: 龘  reason: contains not printable characters */
        public final long f17227;

        Info(DateTimeZone dateTimeZone, long j) {
            this.f17227 = j;
            this.f17224 = dateTimeZone;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m21633(long j) {
            if (this.f17226 != null && j >= this.f17226.f17227) {
                return this.f17226.m21633(j);
            }
            if (this.f17225 == null) {
                this.f17225 = this.f17224.getNameKey(this.f17227);
            }
            return this.f17225;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21631(long j) {
            if (this.f17226 != null && j >= this.f17226.f17227) {
                return this.f17226.m21631(j);
            }
            if (this.f17223 == Integer.MIN_VALUE) {
                this.f17223 = this.f17224.getOffset(this.f17227);
            }
            return this.f17223;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public int m21632(long j) {
            if (this.f17226 != null && j >= this.f17226.f17227) {
                return this.f17226.m21632(j);
            }
            if (this.f17222 == Integer.MIN_VALUE) {
                this.f17222 = this.f17224.getStandardOffset(this.f17227);
            }
            return this.f17222;
        }
    }
}
