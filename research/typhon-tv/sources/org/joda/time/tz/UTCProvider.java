package org.joda.time.tz;

import java.util.Collections;
import java.util.Set;
import org.joda.time.DateTimeZone;

public final class UTCProvider implements Provider {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Set<String> f17239 = Collections.singleton("UTC");

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeZone m21664(String str) {
        if ("UTC".equalsIgnoreCase(str)) {
            return DateTimeZone.UTC;
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<String> m21663() {
        return f17239;
    }
}
