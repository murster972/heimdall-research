package org.joda.time.tz;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;

public class DateTimeZoneBuilder {
    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeZone m21636(InputStream inputStream, String str) throws IOException {
        if (inputStream instanceof DataInput) {
            return m21635((DataInput) inputStream, str);
        }
        return m21635((DataInput) new DataInputStream(inputStream), str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeZone m21635(DataInput dataInput, String str) throws IOException {
        switch (dataInput.readUnsignedByte()) {
            case 67:
                return CachedDateTimeZone.forZone(PrecalculatedZone.m21646(dataInput, str));
            case 70:
                FixedDateTimeZone fixedDateTimeZone = new FixedDateTimeZone(str, dataInput.readUTF(), (int) m21634(dataInput), (int) m21634(dataInput));
                if (fixedDateTimeZone.equals(DateTimeZone.UTC)) {
                    return DateTimeZone.UTC;
                }
                return fixedDateTimeZone;
            case 80:
                return PrecalculatedZone.m21646(dataInput, str);
            default:
                throw new IOException("Invalid encoding");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static long m21634(DataInput dataInput) throws IOException {
        int readUnsignedByte = dataInput.readUnsignedByte();
        switch (readUnsignedByte >> 6) {
            case 1:
                return ((long) (((readUnsignedByte << 26) >> 2) | (dataInput.readUnsignedByte() << 16) | (dataInput.readUnsignedByte() << 8) | dataInput.readUnsignedByte())) * 60000;
            case 2:
                return (((((long) readUnsignedByte) << 58) >> 26) | ((long) (dataInput.readUnsignedByte() << 24)) | ((long) (dataInput.readUnsignedByte() << 16)) | ((long) (dataInput.readUnsignedByte() << 8)) | ((long) dataInput.readUnsignedByte())) * 1000;
            case 3:
                return dataInput.readLong();
            default:
                return ((long) ((readUnsignedByte << 26) >> 26)) * 1800000;
        }
    }

    private static final class OfYear {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f17228;

        /* renamed from: 连任  reason: contains not printable characters */
        final boolean f17229;

        /* renamed from: 靐  reason: contains not printable characters */
        final int f17230;

        /* renamed from: 麤  reason: contains not printable characters */
        final int f17231;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f17232;

        /* renamed from: 龘  reason: contains not printable characters */
        final char f17233;

        /* renamed from: 龘  reason: contains not printable characters */
        static OfYear m21643(DataInput dataInput) throws IOException {
            return new OfYear((char) dataInput.readUnsignedByte(), dataInput.readUnsignedByte(), dataInput.readByte(), dataInput.readUnsignedByte(), dataInput.readBoolean(), (int) DateTimeZoneBuilder.m21634(dataInput));
        }

        OfYear(char c, int i, int i2, int i3, boolean z, int i4) {
            if (c == 'u' || c == 'w' || c == 's') {
                this.f17233 = c;
                this.f17230 = i;
                this.f17232 = i2;
                this.f17231 = i3;
                this.f17229 = z;
                this.f17228 = i4;
                return;
            }
            throw new IllegalArgumentException("Unknown mode: " + c);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m21645(long j, int i, int i2) {
            if (this.f17233 == 'w') {
                i += i2;
            } else if (this.f17233 != 's') {
                i = 0;
            }
            long j2 = ((long) i) + j;
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long r0 = m21642(instanceUTC, instanceUTC.millisOfDay().add(instanceUTC.millisOfDay().set(instanceUTC.monthOfYear().set(j2, this.f17230), 0), this.f17228));
            if (this.f17231 != 0) {
                r0 = m21640(instanceUTC, r0);
                if (r0 <= j2) {
                    r0 = m21640(instanceUTC, m21642(instanceUTC, instanceUTC.monthOfYear().set(instanceUTC.year().add(r0, 1), this.f17230)));
                }
            } else if (r0 <= j2) {
                r0 = m21642(instanceUTC, instanceUTC.year().add(r0, 1));
            }
            return r0 - ((long) i);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m21644(long j, int i, int i2) {
            if (this.f17233 == 'w') {
                i += i2;
            } else if (this.f17233 != 's') {
                i = 0;
            }
            long j2 = ((long) i) + j;
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long r0 = m21639(instanceUTC, instanceUTC.millisOfDay().add(instanceUTC.millisOfDay().set(instanceUTC.monthOfYear().set(j2, this.f17230), 0), this.f17228));
            if (this.f17231 != 0) {
                r0 = m21640(instanceUTC, r0);
                if (r0 >= j2) {
                    r0 = m21640(instanceUTC, m21639(instanceUTC, instanceUTC.monthOfYear().set(instanceUTC.year().add(r0, -1), this.f17230)));
                }
            } else if (r0 >= j2) {
                r0 = m21639(instanceUTC, instanceUTC.year().add(r0, -1));
            }
            return r0 - ((long) i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OfYear)) {
                return false;
            }
            OfYear ofYear = (OfYear) obj;
            if (this.f17233 == ofYear.f17233 && this.f17230 == ofYear.f17230 && this.f17232 == ofYear.f17232 && this.f17231 == ofYear.f17231 && this.f17229 == ofYear.f17229 && this.f17228 == ofYear.f17228) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "[OfYear]\nMode: " + this.f17233 + 10 + "MonthOfYear: " + this.f17230 + 10 + "DayOfMonth: " + this.f17232 + 10 + "DayOfWeek: " + this.f17231 + 10 + "AdvanceDayOfWeek: " + this.f17229 + 10 + "MillisOfDay: " + this.f17228 + 10;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private long m21642(Chronology chronology, long j) {
            try {
                return m21641(chronology, j);
            } catch (IllegalArgumentException e) {
                if (this.f17230 == 2 && this.f17232 == 29) {
                    while (!chronology.year().isLeap(j)) {
                        j = chronology.year().add(j, 1);
                    }
                    return m21641(chronology, j);
                }
                throw e;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private long m21639(Chronology chronology, long j) {
            try {
                return m21641(chronology, j);
            } catch (IllegalArgumentException e) {
                if (this.f17230 == 2 && this.f17232 == 29) {
                    while (!chronology.year().isLeap(j)) {
                        j = chronology.year().add(j, -1);
                    }
                    return m21641(chronology, j);
                }
                throw e;
            }
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private long m21641(Chronology chronology, long j) {
            if (this.f17232 >= 0) {
                return chronology.dayOfMonth().set(j, this.f17232);
            }
            return chronology.dayOfMonth().add(chronology.monthOfYear().add(chronology.dayOfMonth().set(j, 1), 1), this.f17232);
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private long m21640(Chronology chronology, long j) {
            int i = this.f17231 - chronology.dayOfWeek().get(j);
            if (i == 0) {
                return j;
            }
            if (this.f17229) {
                if (i < 0) {
                    i += 7;
                }
            } else if (i > 0) {
                i -= 7;
            }
            return chronology.dayOfWeek().add(j, i);
        }
    }

    private static final class Recurrence {

        /* renamed from: 靐  reason: contains not printable characters */
        final String f17234;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f17235;

        /* renamed from: 龘  reason: contains not printable characters */
        final OfYear f17236;

        /* renamed from: 龘  reason: contains not printable characters */
        static Recurrence m21647(DataInput dataInput) throws IOException {
            return new Recurrence(OfYear.m21643(dataInput), dataInput.readUTF(), (int) DateTimeZoneBuilder.m21634(dataInput));
        }

        Recurrence(OfYear ofYear, String str, int i) {
            this.f17236 = ofYear;
            this.f17234 = str;
            this.f17235 = i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m21650(long j, int i, int i2) {
            return this.f17236.m21645(j, i, i2);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m21649(long j, int i, int i2) {
            return this.f17236.m21644(j, i, i2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m21651() {
            return this.f17234;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m21648() {
            return this.f17235;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Recurrence)) {
                return false;
            }
            Recurrence recurrence = (Recurrence) obj;
            if (this.f17235 != recurrence.f17235 || !this.f17234.equals(recurrence.f17234) || !this.f17236.equals(recurrence.f17236)) {
                return false;
            }
            return true;
        }

        public String toString() {
            return this.f17236 + " named " + this.f17234 + " at " + this.f17235;
        }
    }

    private static final class DSTZone extends DateTimeZone {
        private static final long serialVersionUID = 6941492635554961361L;
        final Recurrence iEndRecurrence;
        final int iStandardOffset;
        final Recurrence iStartRecurrence;

        /* renamed from: 龘  reason: contains not printable characters */
        static DSTZone m21637(DataInput dataInput, String str) throws IOException {
            return new DSTZone(str, (int) DateTimeZoneBuilder.m21634(dataInput), Recurrence.m21647(dataInput), Recurrence.m21647(dataInput));
        }

        DSTZone(String str, int i, Recurrence recurrence, Recurrence recurrence2) {
            super(str);
            this.iStandardOffset = i;
            this.iStartRecurrence = recurrence;
            this.iEndRecurrence = recurrence2;
        }

        public String getNameKey(long j) {
            return m21638(j).m21651();
        }

        public int getOffset(long j) {
            return this.iStandardOffset + m21638(j).m21648();
        }

        public int getStandardOffset(long j) {
            return this.iStandardOffset;
        }

        public boolean isFixed() {
            return false;
        }

        public long nextTransition(long j) {
            long j2;
            long j3;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                long r0 = recurrence.m21650(j, i, recurrence2.m21648());
                if (j > 0 && r0 < 0) {
                    r0 = j;
                }
                j2 = r0;
            } catch (IllegalArgumentException e) {
                j2 = j;
            } catch (ArithmeticException e2) {
                j2 = j;
            }
            try {
                long r02 = recurrence2.m21650(j, i, recurrence.m21648());
                if (j <= 0 || r02 >= 0) {
                    j = r02;
                }
                j3 = j;
            } catch (IllegalArgumentException e3) {
                j3 = j;
            } catch (ArithmeticException e4) {
                j3 = j;
            }
            if (j2 > j3) {
                return j3;
            }
            return j2;
        }

        public long previousTransition(long j) {
            long j2;
            long j3;
            long j4 = j + 1;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                long r0 = recurrence.m21649(j4, i, recurrence2.m21648());
                if (j4 < 0 && r0 > 0) {
                    r0 = j4;
                }
                j2 = r0;
            } catch (IllegalArgumentException e) {
                j2 = j4;
            } catch (ArithmeticException e2) {
                j2 = j4;
            }
            try {
                long r02 = recurrence2.m21649(j4, i, recurrence.m21648());
                if (j4 >= 0 || r02 <= 0) {
                    j4 = r02;
                }
                j3 = j4;
            } catch (IllegalArgumentException e3) {
                j3 = j4;
            } catch (ArithmeticException e4) {
                j3 = j4;
            }
            if (j2 > j3) {
                j3 = j2;
            }
            return j3 - 1;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DSTZone)) {
                return false;
            }
            DSTZone dSTZone = (DSTZone) obj;
            if (!getID().equals(dSTZone.getID()) || this.iStandardOffset != dSTZone.iStandardOffset || !this.iStartRecurrence.equals(dSTZone.iStartRecurrence) || !this.iEndRecurrence.equals(dSTZone.iEndRecurrence)) {
                return false;
            }
            return true;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Recurrence m21638(long j) {
            long j2;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                j2 = recurrence.m21650(j, i, recurrence2.m21648());
            } catch (IllegalArgumentException e) {
                j2 = j;
            } catch (ArithmeticException e2) {
                j2 = j;
            }
            try {
                j = recurrence2.m21650(j, i, recurrence.m21648());
            } catch (ArithmeticException | IllegalArgumentException e3) {
            }
            if (j2 > j) {
                return recurrence;
            }
            return recurrence2;
        }
    }

    private static final class PrecalculatedZone extends DateTimeZone {
        private static final long serialVersionUID = 7811976468055766265L;
        private final String[] iNameKeys;
        private final int[] iStandardOffsets;
        private final DSTZone iTailZone;
        private final long[] iTransitions;
        private final int[] iWallOffsets;

        /* renamed from: 龘  reason: contains not printable characters */
        static PrecalculatedZone m21646(DataInput dataInput, String str) throws IOException {
            int readUnsignedShort;
            int readUnsignedShort2 = dataInput.readUnsignedShort();
            String[] strArr = new String[readUnsignedShort2];
            for (int i = 0; i < readUnsignedShort2; i++) {
                strArr[i] = dataInput.readUTF();
            }
            int readInt = dataInput.readInt();
            long[] jArr = new long[readInt];
            int[] iArr = new int[readInt];
            int[] iArr2 = new int[readInt];
            String[] strArr2 = new String[readInt];
            for (int i2 = 0; i2 < readInt; i2++) {
                jArr[i2] = DateTimeZoneBuilder.m21634(dataInput);
                iArr[i2] = (int) DateTimeZoneBuilder.m21634(dataInput);
                iArr2[i2] = (int) DateTimeZoneBuilder.m21634(dataInput);
                if (readUnsignedShort2 < 256) {
                    try {
                        readUnsignedShort = dataInput.readUnsignedByte();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        throw new IOException("Invalid encoding");
                    }
                } else {
                    readUnsignedShort = dataInput.readUnsignedShort();
                }
                strArr2[i2] = strArr[readUnsignedShort];
            }
            DSTZone dSTZone = null;
            if (dataInput.readBoolean()) {
                dSTZone = DSTZone.m21637(dataInput, str);
            }
            return new PrecalculatedZone(str, jArr, iArr, iArr2, strArr2, dSTZone);
        }

        private PrecalculatedZone(String str, long[] jArr, int[] iArr, int[] iArr2, String[] strArr, DSTZone dSTZone) {
            super(str);
            this.iTransitions = jArr;
            this.iWallOffsets = iArr;
            this.iStandardOffsets = iArr2;
            this.iNameKeys = strArr;
            this.iTailZone = dSTZone;
        }

        public String getNameKey(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iNameKeys[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i < jArr.length) {
                if (i > 0) {
                    return this.iNameKeys[i - 1];
                }
                return "UTC";
            } else if (this.iTailZone == null) {
                return this.iNameKeys[i - 1];
            } else {
                return this.iTailZone.getNameKey(j);
            }
        }

        public int getOffset(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iWallOffsets[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i < jArr.length) {
                if (i > 0) {
                    return this.iWallOffsets[i - 1];
                }
                return 0;
            } else if (this.iTailZone == null) {
                return this.iWallOffsets[i - 1];
            } else {
                return this.iTailZone.getOffset(j);
            }
        }

        public int getStandardOffset(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iStandardOffsets[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i < jArr.length) {
                if (i > 0) {
                    return this.iStandardOffsets[i - 1];
                }
                return 0;
            } else if (this.iTailZone == null) {
                return this.iStandardOffsets[i - 1];
            } else {
                return this.iTailZone.getStandardOffset(j);
            }
        }

        public boolean isFixed() {
            return false;
        }

        public long nextTransition(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            int i = binarySearch >= 0 ? binarySearch + 1 : binarySearch ^ -1;
            if (i < jArr.length) {
                return jArr[i];
            }
            if (this.iTailZone == null) {
                return j;
            }
            long j2 = jArr[jArr.length - 1];
            if (j < j2) {
                j = j2;
            }
            return this.iTailZone.nextTransition(j);
        }

        public long previousTransition(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch < 0) {
                int i = binarySearch ^ -1;
                if (i >= jArr.length) {
                    if (this.iTailZone != null) {
                        long previousTransition = this.iTailZone.previousTransition(j);
                        if (previousTransition < j) {
                            return previousTransition;
                        }
                    }
                    long j2 = jArr[i - 1];
                    if (j2 > Long.MIN_VALUE) {
                        return j2 - 1;
                    }
                    return j;
                } else if (i <= 0) {
                    return j;
                } else {
                    long j3 = jArr[i - 1];
                    if (j3 > Long.MIN_VALUE) {
                        return j3 - 1;
                    }
                    return j;
                }
            } else if (j > Long.MIN_VALUE) {
                return j - 1;
            } else {
                return j;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PrecalculatedZone)) {
                return false;
            }
            PrecalculatedZone precalculatedZone = (PrecalculatedZone) obj;
            if (getID().equals(precalculatedZone.getID()) && Arrays.equals(this.iTransitions, precalculatedZone.iTransitions) && Arrays.equals(this.iNameKeys, precalculatedZone.iNameKeys) && Arrays.equals(this.iWallOffsets, precalculatedZone.iWallOffsets) && Arrays.equals(this.iStandardOffsets, precalculatedZone.iStandardOffsets)) {
                if (this.iTailZone == null) {
                    if (precalculatedZone.iTailZone == null) {
                        return true;
                    }
                } else if (this.iTailZone.equals(precalculatedZone.iTailZone)) {
                    return true;
                }
            }
            return false;
        }
    }
}
