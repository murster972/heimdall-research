package org.joda.time;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Years extends BaseSingleFieldPeriod {
    public static final Years MAX_VALUE = new Years(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    public static final Years MIN_VALUE = new Years(Integer.MIN_VALUE);
    public static final Years ONE = new Years(1);
    public static final Years THREE = new Years(3);
    public static final Years TWO = new Years(2);
    public static final Years ZERO = new Years(0);
    private static final long serialVersionUID = 87525275727380868L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final PeriodFormatter f6505 = ISOPeriodFormat.m21532().m21557(PeriodType.years());

    private Years(int i) {
        super(i);
    }

    @FromString
    public static Years parseYears(String str) {
        return str == null ? ZERO : years(f6505.m21556(str).getYears());
    }

    private Object readResolve() {
        return years(m20950());
    }

    public static Years years(int i) {
        switch (i) {
            case Integer.MIN_VALUE:
                return MIN_VALUE;
            case 0:
                return ZERO;
            case 1:
                return ONE;
            case 2:
                return TWO;
            case 3:
                return THREE;
            case MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT /*2147483647*/:
                return MAX_VALUE;
            default:
                return new Years(i);
        }
    }

    public static Years yearsBetween(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        return years(BaseSingleFieldPeriod.m20947(readableInstant, readableInstant2, DurationFieldType.years()));
    }

    public static Years yearsBetween(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        return (!(readablePartial instanceof LocalDate) || !(readablePartial2 instanceof LocalDate)) ? years(BaseSingleFieldPeriod.m20948(readablePartial, readablePartial2, (ReadablePeriod) ZERO)) : years(DateTimeUtils.m20895(readablePartial.getChronology()).years().getDifference(((LocalDate) readablePartial2).m7304(), ((LocalDate) readablePartial).m7304()));
    }

    public static Years yearsIn(ReadableInterval readableInterval) {
        return readableInterval == null ? ZERO : years(BaseSingleFieldPeriod.m20947((ReadableInstant) readableInterval.getStart(), (ReadableInstant) readableInterval.getEnd(), DurationFieldType.years()));
    }

    public Years dividedBy(int i) {
        return i == 1 ? this : years(m20950() / i);
    }

    public DurationFieldType getFieldType() {
        return DurationFieldType.years();
    }

    public PeriodType getPeriodType() {
        return PeriodType.years();
    }

    public int getYears() {
        return m20950();
    }

    public boolean isGreaterThan(Years years) {
        return years == null ? m20950() > 0 : m20950() > years.m20950();
    }

    public boolean isLessThan(Years years) {
        return years == null ? m20950() < 0 : m20950() < years.m20950();
    }

    public Years minus(int i) {
        return plus(FieldUtils.m21209(i));
    }

    public Years minus(Years years) {
        return years == null ? this : minus(years.m20950());
    }

    public Years multipliedBy(int i) {
        return years(FieldUtils.m21205(m20950(), i));
    }

    public Years negated() {
        return years(FieldUtils.m21209(m20950()));
    }

    public Years plus(int i) {
        return i == 0 ? this : years(FieldUtils.m21210(m20950(), i));
    }

    public Years plus(Years years) {
        return years == null ? this : plus(years.m20950());
    }

    @ToString
    public String toString() {
        return "P" + String.valueOf(m20950()) + "Y";
    }
}
