package org.joda.time;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Seconds extends BaseSingleFieldPeriod {
    public static final Seconds MAX_VALUE = new Seconds(MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    public static final Seconds MIN_VALUE = new Seconds(Integer.MIN_VALUE);
    public static final Seconds ONE = new Seconds(1);
    public static final Seconds THREE = new Seconds(3);
    public static final Seconds TWO = new Seconds(2);
    public static final Seconds ZERO = new Seconds(0);
    private static final long serialVersionUID = 87525275727380862L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final PeriodFormatter f6500 = ISOPeriodFormat.m21532().m21557(PeriodType.seconds());

    private Seconds(int i) {
        super(i);
    }

    @FromString
    public static Seconds parseSeconds(String str) {
        return str == null ? ZERO : seconds(f6500.m21556(str).getSeconds());
    }

    private Object readResolve() {
        return seconds(m20950());
    }

    public static Seconds seconds(int i) {
        switch (i) {
            case Integer.MIN_VALUE:
                return MIN_VALUE;
            case 0:
                return ZERO;
            case 1:
                return ONE;
            case 2:
                return TWO;
            case 3:
                return THREE;
            case MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT /*2147483647*/:
                return MAX_VALUE;
            default:
                return new Seconds(i);
        }
    }

    public static Seconds secondsBetween(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        return seconds(BaseSingleFieldPeriod.m20947(readableInstant, readableInstant2, DurationFieldType.seconds()));
    }

    public static Seconds secondsBetween(ReadablePartial readablePartial, ReadablePartial readablePartial2) {
        return (!(readablePartial instanceof LocalTime) || !(readablePartial2 instanceof LocalTime)) ? seconds(BaseSingleFieldPeriod.m20948(readablePartial, readablePartial2, (ReadablePeriod) ZERO)) : seconds(DateTimeUtils.m20895(readablePartial.getChronology()).seconds().getDifference(((LocalTime) readablePartial2).m7311(), ((LocalTime) readablePartial).m7311()));
    }

    public static Seconds secondsIn(ReadableInterval readableInterval) {
        return readableInterval == null ? ZERO : seconds(BaseSingleFieldPeriod.m20947((ReadableInstant) readableInterval.getStart(), (ReadableInstant) readableInterval.getEnd(), DurationFieldType.seconds()));
    }

    public static Seconds standardSecondsIn(ReadablePeriod readablePeriod) {
        return seconds(BaseSingleFieldPeriod.m20949(readablePeriod, 1000));
    }

    public Seconds dividedBy(int i) {
        return i == 1 ? this : seconds(m20950() / i);
    }

    public DurationFieldType getFieldType() {
        return DurationFieldType.seconds();
    }

    public PeriodType getPeriodType() {
        return PeriodType.seconds();
    }

    public int getSeconds() {
        return m20950();
    }

    public boolean isGreaterThan(Seconds seconds) {
        return seconds == null ? m20950() > 0 : m20950() > seconds.m20950();
    }

    public boolean isLessThan(Seconds seconds) {
        return seconds == null ? m20950() < 0 : m20950() < seconds.m20950();
    }

    public Seconds minus(int i) {
        return plus(FieldUtils.m21209(i));
    }

    public Seconds minus(Seconds seconds) {
        return seconds == null ? this : minus(seconds.m20950());
    }

    public Seconds multipliedBy(int i) {
        return seconds(FieldUtils.m21205(m20950(), i));
    }

    public Seconds negated() {
        return seconds(FieldUtils.m21209(m20950()));
    }

    public Seconds plus(int i) {
        return i == 0 ? this : seconds(FieldUtils.m21210(m20950(), i));
    }

    public Seconds plus(Seconds seconds) {
        return seconds == null ? this : plus(seconds.m20950());
    }

    public Days toStandardDays() {
        return Days.days(m20950() / 86400);
    }

    public Duration toStandardDuration() {
        return new Duration(((long) m20950()) * 1000);
    }

    public Hours toStandardHours() {
        return Hours.hours(m20950() / 3600);
    }

    public Minutes toStandardMinutes() {
        return Minutes.minutes(m20950() / 60);
    }

    public Weeks toStandardWeeks() {
        return Weeks.weeks(m20950() / 604800);
    }

    @ToString
    public String toString() {
        return "PT" + String.valueOf(m20950()) + "S";
    }
}
