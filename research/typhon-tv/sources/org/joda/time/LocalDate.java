package org.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseLocal;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.PartialConverter;
import org.joda.time.field.AbstractReadableInstantFieldProperty;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class LocalDate extends BaseLocal implements Serializable, ReadablePartial {
    private static final long serialVersionUID = -8775358157899L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Set<DurationFieldType> f6493 = new HashSet();
    private final Chronology iChronology;
    private final long iLocalMillis;

    /* renamed from: 靐  reason: contains not printable characters */
    private transient int f6494;

    static {
        f6493.add(DurationFieldType.days());
        f6493.add(DurationFieldType.weeks());
        f6493.add(DurationFieldType.months());
        f6493.add(DurationFieldType.weekyears());
        f6493.add(DurationFieldType.years());
        f6493.add(DurationFieldType.centuries());
        f6493.add(DurationFieldType.eras());
    }

    public LocalDate() {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance());
    }

    public LocalDate(int i, int i2, int i3) {
        this(i, i2, i3, ISOChronology.getInstanceUTC());
    }

    public LocalDate(int i, int i2, int i3, Chronology chronology) {
        Chronology withUTC = DateTimeUtils.m20895(chronology).withUTC();
        long dateTimeMillis = withUTC.getDateTimeMillis(i, i2, i3, 0);
        this.iChronology = withUTC;
        this.iLocalMillis = dateTimeMillis;
    }

    public LocalDate(long j) {
        this(j, (Chronology) ISOChronology.getInstance());
    }

    public LocalDate(long j, Chronology chronology) {
        Chronology r0 = DateTimeUtils.m20895(chronology);
        long millisKeepLocal = r0.getZone().getMillisKeepLocal(DateTimeZone.UTC, j);
        Chronology withUTC = r0.withUTC();
        this.iLocalMillis = withUTC.dayOfMonth().roundFloor(millisKeepLocal);
        this.iChronology = withUTC;
    }

    public LocalDate(long j, DateTimeZone dateTimeZone) {
        this(j, (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public LocalDate(Object obj) {
        this(obj, (Chronology) null);
    }

    public LocalDate(Object obj, Chronology chronology) {
        PartialConverter r0 = ConverterManager.m21146().m21148(obj);
        Chronology r1 = DateTimeUtils.m20895(r0.m21171(obj, chronology));
        this.iChronology = r1.withUTC();
        int[] r02 = r0.m21173(this, obj, r1, ISODateTimeFormat.m21452());
        this.iLocalMillis = this.iChronology.getDateTimeMillis(r02[0], r02[1], r02[2], 0);
    }

    public LocalDate(Object obj, DateTimeZone dateTimeZone) {
        PartialConverter r0 = ConverterManager.m21146().m21148(obj);
        Chronology r1 = DateTimeUtils.m20895(r0.m21172(obj, dateTimeZone));
        this.iChronology = r1.withUTC();
        int[] r02 = r0.m21173(this, obj, r1, ISODateTimeFormat.m21452());
        this.iLocalMillis = this.iChronology.getDateTimeMillis(r02[0], r02[1], r02[2], 0);
    }

    public LocalDate(Chronology chronology) {
        this(DateTimeUtils.m20891(), chronology);
    }

    public LocalDate(DateTimeZone dateTimeZone) {
        this(DateTimeUtils.m20891(), (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public static LocalDate fromCalendarFields(Calendar calendar) {
        if (calendar == null) {
            throw new IllegalArgumentException("The calendar must not be null");
        }
        int i = calendar.get(0);
        int i2 = calendar.get(1);
        if (i != 1) {
            i2 = 1 - i2;
        }
        return new LocalDate(i2, calendar.get(2) + 1, calendar.get(5));
    }

    public static LocalDate fromDateFields(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date.getTime() >= 0) {
            return new LocalDate(date.getYear() + 1900, date.getMonth() + 1, date.getDate());
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(date);
            return fromCalendarFields(gregorianCalendar);
        }
    }

    public static LocalDate now() {
        return new LocalDate();
    }

    public static LocalDate now(Chronology chronology) {
        if (chronology != null) {
            return new LocalDate(chronology);
        }
        throw new NullPointerException("Chronology must not be null");
    }

    public static LocalDate now(DateTimeZone dateTimeZone) {
        if (dateTimeZone != null) {
            return new LocalDate(dateTimeZone);
        }
        throw new NullPointerException("Zone must not be null");
    }

    @FromString
    public static LocalDate parse(String str) {
        return parse(str, ISODateTimeFormat.m21452());
    }

    public static LocalDate parse(String str, DateTimeFormatter dateTimeFormatter) {
        return dateTimeFormatter.m21238(str);
    }

    private Object readResolve() {
        return this.iChronology == null ? new LocalDate(this.iLocalMillis, (Chronology) ISOChronology.getInstanceUTC()) : !DateTimeZone.UTC.equals(this.iChronology.getZone()) ? new LocalDate(this.iLocalMillis, this.iChronology.withUTC()) : this;
    }

    public Property centuryOfEra() {
        return new Property(this, getChronology().centuryOfEra());
    }

    public int compareTo(ReadablePartial readablePartial) {
        if (this == readablePartial) {
            return 0;
        }
        if (readablePartial instanceof LocalDate) {
            LocalDate localDate = (LocalDate) readablePartial;
            if (this.iChronology.equals(localDate.iChronology)) {
                return this.iLocalMillis < localDate.iLocalMillis ? -1 : this.iLocalMillis == localDate.iLocalMillis ? 0 : 1;
            }
        }
        return super.compareTo(readablePartial);
    }

    public Property dayOfMonth() {
        return new Property(this, getChronology().dayOfMonth());
    }

    public Property dayOfWeek() {
        return new Property(this, getChronology().dayOfWeek());
    }

    public Property dayOfYear() {
        return new Property(this, getChronology().dayOfYear());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof LocalDate) {
            LocalDate localDate = (LocalDate) obj;
            if (this.iChronology.equals(localDate.iChronology)) {
                return this.iLocalMillis == localDate.iLocalMillis;
            }
        }
        return super.equals(obj);
    }

    public Property era() {
        return new Property(this, getChronology().era());
    }

    public int get(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("The DateTimeFieldType must not be null");
        } else if (isSupported(dateTimeFieldType)) {
            return dateTimeFieldType.getField(getChronology()).get(m7304());
        } else {
            throw new IllegalArgumentException("Field '" + dateTimeFieldType + "' is not supported");
        }
    }

    public int getCenturyOfEra() {
        return getChronology().centuryOfEra().get(m7304());
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public int getDayOfMonth() {
        return getChronology().dayOfMonth().get(m7304());
    }

    public int getDayOfWeek() {
        return getChronology().dayOfWeek().get(m7304());
    }

    public int getDayOfYear() {
        return getChronology().dayOfYear().get(m7304());
    }

    public int getEra() {
        return getChronology().era().get(m7304());
    }

    public int getMonthOfYear() {
        return getChronology().monthOfYear().get(m7304());
    }

    public int getValue(int i) {
        switch (i) {
            case 0:
                return getChronology().year().get(m7304());
            case 1:
                return getChronology().monthOfYear().get(m7304());
            case 2:
                return getChronology().dayOfMonth().get(m7304());
            default:
                throw new IndexOutOfBoundsException("Invalid index: " + i);
        }
    }

    public int getWeekOfWeekyear() {
        return getChronology().weekOfWeekyear().get(m7304());
    }

    public int getWeekyear() {
        return getChronology().weekyear().get(m7304());
    }

    public int getYear() {
        return getChronology().year().get(m7304());
    }

    public int getYearOfCentury() {
        return getChronology().yearOfCentury().get(m7304());
    }

    public int getYearOfEra() {
        return getChronology().yearOfEra().get(m7304());
    }

    public int hashCode() {
        int i = this.f6494;
        if (i != 0) {
            return i;
        }
        int hashCode = super.hashCode();
        this.f6494 = hashCode;
        return hashCode;
    }

    public boolean isSupported(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            return false;
        }
        DurationFieldType durationType = dateTimeFieldType.getDurationType();
        if (f6493.contains(durationType) || durationType.getField(getChronology()).getUnitMillis() >= getChronology().days().getUnitMillis()) {
            return dateTimeFieldType.getField(getChronology()).isSupported();
        }
        return false;
    }

    public boolean isSupported(DurationFieldType durationFieldType) {
        if (durationFieldType == null) {
            return false;
        }
        DurationField field = durationFieldType.getField(getChronology());
        if (f6493.contains(durationFieldType) || field.getUnitMillis() >= getChronology().days().getUnitMillis()) {
            return field.isSupported();
        }
        return false;
    }

    public LocalDate minus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, -1);
    }

    public LocalDate minusDays(int i) {
        return i == 0 ? this : m7306(getChronology().days().subtract(m7304(), i));
    }

    public LocalDate minusMonths(int i) {
        return i == 0 ? this : m7306(getChronology().months().subtract(m7304(), i));
    }

    public LocalDate minusWeeks(int i) {
        return i == 0 ? this : m7306(getChronology().weeks().subtract(m7304(), i));
    }

    public LocalDate minusYears(int i) {
        return i == 0 ? this : m7306(getChronology().years().subtract(m7304(), i));
    }

    public Property monthOfYear() {
        return new Property(this, getChronology().monthOfYear());
    }

    public LocalDate plus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, 1);
    }

    public LocalDate plusDays(int i) {
        return i == 0 ? this : m7306(getChronology().days().add(m7304(), i));
    }

    public LocalDate plusMonths(int i) {
        return i == 0 ? this : m7306(getChronology().months().add(m7304(), i));
    }

    public LocalDate plusWeeks(int i) {
        return i == 0 ? this : m7306(getChronology().weeks().add(m7304(), i));
    }

    public LocalDate plusYears(int i) {
        return i == 0 ? this : m7306(getChronology().years().add(m7304(), i));
    }

    public Property property(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("The DateTimeFieldType must not be null");
        } else if (isSupported(dateTimeFieldType)) {
            return new Property(this, dateTimeFieldType.getField(getChronology()));
        } else {
            throw new IllegalArgumentException("Field '" + dateTimeFieldType + "' is not supported");
        }
    }

    public int size() {
        return 3;
    }

    public Date toDate() {
        int dayOfMonth = getDayOfMonth();
        Date date = new Date(getYear() - 1900, getMonthOfYear() - 1, dayOfMonth);
        LocalDate fromDateFields = fromDateFields(date);
        if (fromDateFields.isBefore(this)) {
            while (!fromDateFields.equals(this)) {
                date.setTime(date.getTime() + 3600000);
                fromDateFields = fromDateFields(date);
            }
            while (date.getDate() == dayOfMonth) {
                date.setTime(date.getTime() - 1000);
            }
            date.setTime(date.getTime() + 1000);
            return date;
        }
        if (fromDateFields.equals(this)) {
            Date date2 = new Date(date.getTime() - ((long) TimeZone.getDefault().getDSTSavings()));
            if (date2.getDate() == dayOfMonth) {
                return date2;
            }
        }
        return date;
    }

    @Deprecated
    public DateMidnight toDateMidnight() {
        return toDateMidnight((DateTimeZone) null);
    }

    @Deprecated
    public DateMidnight toDateMidnight(DateTimeZone dateTimeZone) {
        return new DateMidnight(getYear(), getMonthOfYear(), getDayOfMonth(), getChronology().withZone(DateTimeUtils.m20898(dateTimeZone)));
    }

    public DateTime toDateTime(LocalTime localTime) {
        return toDateTime(localTime, (DateTimeZone) null);
    }

    public DateTime toDateTime(LocalTime localTime, DateTimeZone dateTimeZone) {
        if (localTime == null) {
            return toDateTimeAtCurrentTime(dateTimeZone);
        }
        if (getChronology() != localTime.getChronology()) {
            throw new IllegalArgumentException("The chronology of the time does not match");
        }
        return new DateTime(getYear(), getMonthOfYear(), getDayOfMonth(), localTime.getHourOfDay(), localTime.getMinuteOfHour(), localTime.getSecondOfMinute(), localTime.getMillisOfSecond(), getChronology().withZone(dateTimeZone));
    }

    public DateTime toDateTimeAtCurrentTime() {
        return toDateTimeAtCurrentTime((DateTimeZone) null);
    }

    public DateTime toDateTimeAtCurrentTime(DateTimeZone dateTimeZone) {
        Chronology withZone = getChronology().withZone(DateTimeUtils.m20898(dateTimeZone));
        return new DateTime(withZone.set(this, DateTimeUtils.m20891()), withZone);
    }

    @Deprecated
    public DateTime toDateTimeAtMidnight() {
        return toDateTimeAtMidnight((DateTimeZone) null);
    }

    @Deprecated
    public DateTime toDateTimeAtMidnight(DateTimeZone dateTimeZone) {
        return new DateTime(getYear(), getMonthOfYear(), getDayOfMonth(), 0, 0, 0, 0, getChronology().withZone(DateTimeUtils.m20898(dateTimeZone)));
    }

    public DateTime toDateTimeAtStartOfDay() {
        return toDateTimeAtStartOfDay((DateTimeZone) null);
    }

    public DateTime toDateTimeAtStartOfDay(DateTimeZone dateTimeZone) {
        DateTimeZone r0 = DateTimeUtils.m20898(dateTimeZone);
        Chronology withZone = getChronology().withZone(r0);
        return new DateTime(withZone.dayOfMonth().roundFloor(r0.convertLocalToUTC(m7304() + 21600000, false)), withZone);
    }

    public Interval toInterval() {
        return toInterval((DateTimeZone) null);
    }

    public Interval toInterval(DateTimeZone dateTimeZone) {
        DateTimeZone r0 = DateTimeUtils.m20898(dateTimeZone);
        return new Interval((ReadableInstant) toDateTimeAtStartOfDay(r0), (ReadableInstant) plusDays(1).toDateTimeAtStartOfDay(r0));
    }

    public LocalDateTime toLocalDateTime(LocalTime localTime) {
        if (localTime == null) {
            throw new IllegalArgumentException("The time must not be null");
        } else if (getChronology() == localTime.getChronology()) {
            return new LocalDateTime(m7304() + localTime.m7311(), getChronology());
        } else {
            throw new IllegalArgumentException("The chronology of the time does not match");
        }
    }

    @ToString
    public String toString() {
        return ISODateTimeFormat.m21438().m21246((ReadablePartial) this);
    }

    public String toString(String str) {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21246((ReadablePartial) this);
    }

    public String toString(String str, Locale locale) throws IllegalArgumentException {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21247(locale).m21246((ReadablePartial) this);
    }

    public Property weekOfWeekyear() {
        return new Property(this, getChronology().weekOfWeekyear());
    }

    public Property weekyear() {
        return new Property(this, getChronology().weekyear());
    }

    public LocalDate withCenturyOfEra(int i) {
        return m7306(getChronology().centuryOfEra().set(m7304(), i));
    }

    public LocalDate withDayOfMonth(int i) {
        return m7306(getChronology().dayOfMonth().set(m7304(), i));
    }

    public LocalDate withDayOfWeek(int i) {
        return m7306(getChronology().dayOfWeek().set(m7304(), i));
    }

    public LocalDate withDayOfYear(int i) {
        return m7306(getChronology().dayOfYear().set(m7304(), i));
    }

    public LocalDate withEra(int i) {
        return m7306(getChronology().era().set(m7304(), i));
    }

    public LocalDate withField(DateTimeFieldType dateTimeFieldType, int i) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field must not be null");
        } else if (isSupported(dateTimeFieldType)) {
            return m7306(dateTimeFieldType.getField(getChronology()).set(m7304(), i));
        } else {
            throw new IllegalArgumentException("Field '" + dateTimeFieldType + "' is not supported");
        }
    }

    public LocalDate withFieldAdded(DurationFieldType durationFieldType, int i) {
        if (durationFieldType == null) {
            throw new IllegalArgumentException("Field must not be null");
        } else if (isSupported(durationFieldType)) {
            return i == 0 ? this : m7306(durationFieldType.getField(getChronology()).add(m7304(), i));
        } else {
            throw new IllegalArgumentException("Field '" + durationFieldType + "' is not supported");
        }
    }

    public LocalDate withFields(ReadablePartial readablePartial) {
        return readablePartial == null ? this : m7306(getChronology().set(readablePartial, m7304()));
    }

    public LocalDate withMonthOfYear(int i) {
        return m7306(getChronology().monthOfYear().set(m7304(), i));
    }

    public LocalDate withPeriodAdded(ReadablePeriod readablePeriod, int i) {
        if (readablePeriod == null || i == 0) {
            return this;
        }
        long r2 = m7304();
        Chronology chronology = getChronology();
        for (int i2 = 0; i2 < readablePeriod.size(); i2++) {
            long r4 = (long) FieldUtils.m21205(readablePeriod.getValue(i2), i);
            DurationFieldType fieldType = readablePeriod.getFieldType(i2);
            if (isSupported(fieldType)) {
                r2 = fieldType.getField(chronology).add(r2, r4);
            }
        }
        return m7306(r2);
    }

    public LocalDate withWeekOfWeekyear(int i) {
        return m7306(getChronology().weekOfWeekyear().set(m7304(), i));
    }

    public LocalDate withWeekyear(int i) {
        return m7306(getChronology().weekyear().set(m7304(), i));
    }

    public LocalDate withYear(int i) {
        return m7306(getChronology().year().set(m7304(), i));
    }

    public LocalDate withYearOfCentury(int i) {
        return m7306(getChronology().yearOfCentury().set(m7304(), i));
    }

    public LocalDate withYearOfEra(int i) {
        return m7306(getChronology().yearOfEra().set(m7304(), i));
    }

    public Property year() {
        return new Property(this, getChronology().year());
    }

    public Property yearOfCentury() {
        return new Property(this, getChronology().yearOfCentury());
    }

    public Property yearOfEra() {
        return new Property(this, getChronology().yearOfEra());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m7304() {
        return this.iLocalMillis;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeField m7305(int i, Chronology chronology) {
        switch (i) {
            case 0:
                return chronology.year();
            case 1:
                return chronology.monthOfYear();
            case 2:
                return chronology.dayOfMonth();
            default:
                throw new IndexOutOfBoundsException("Invalid index: " + i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public LocalDate m7306(long j) {
        long roundFloor = this.iChronology.dayOfMonth().roundFloor(j);
        return roundFloor == m7304() ? this : new LocalDate(roundFloor, getChronology());
    }

    public static final class Property extends AbstractReadableInstantFieldProperty {
        private static final long serialVersionUID = -3193829732634L;

        /* renamed from: 靐  reason: contains not printable characters */
        private transient DateTimeField f16798;

        /* renamed from: 龘  reason: contains not printable characters */
        private transient LocalDate f16799;

        Property(LocalDate localDate, DateTimeField dateTimeField) {
            this.f16799 = localDate;
            this.f16798 = dateTimeField;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeObject(this.f16799);
            objectOutputStream.writeObject(this.f16798.getType());
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            this.f16799 = (LocalDate) objectInputStream.readObject();
            this.f16798 = ((DateTimeFieldType) objectInputStream.readObject()).getField(this.f16799.getChronology());
        }

        public DateTimeField getField() {
            return this.f16798;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m20910() {
            return this.f16799.m7304();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public Chronology m20909() {
            return this.f16799.getChronology();
        }

        public LocalDate getLocalDate() {
            return this.f16799;
        }

        public LocalDate addToCopy(int i) {
            return this.f16799.m7306(this.f16798.add(this.f16799.m7304(), i));
        }

        public LocalDate addWrapFieldToCopy(int i) {
            return this.f16799.m7306(this.f16798.addWrapField(this.f16799.m7304(), i));
        }

        public LocalDate setCopy(int i) {
            return this.f16799.m7306(this.f16798.set(this.f16799.m7304(), i));
        }

        public LocalDate setCopy(String str, Locale locale) {
            return this.f16799.m7306(this.f16798.set(this.f16799.m7304(), str, locale));
        }

        public LocalDate setCopy(String str) {
            return setCopy(str, (Locale) null);
        }

        public LocalDate withMaximumValue() {
            return setCopy(getMaximumValue());
        }

        public LocalDate withMinimumValue() {
            return setCopy(getMinimumValue());
        }

        public LocalDate roundFloorCopy() {
            return this.f16799.m7306(this.f16798.roundFloor(this.f16799.m7304()));
        }

        public LocalDate roundCeilingCopy() {
            return this.f16799.m7306(this.f16798.roundCeiling(this.f16799.m7304()));
        }

        public LocalDate roundHalfFloorCopy() {
            return this.f16799.m7306(this.f16798.roundHalfFloor(this.f16799.m7304()));
        }

        public LocalDate roundHalfCeilingCopy() {
            return this.f16799.m7306(this.f16798.roundHalfCeiling(this.f16799.m7304()));
        }

        public LocalDate roundHalfEvenCopy() {
            return this.f16799.m7306(this.f16798.roundHalfEven(this.f16799.m7304()));
        }
    }
}
