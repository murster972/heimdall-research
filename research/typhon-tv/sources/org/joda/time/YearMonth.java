package org.joda.time;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BasePartial;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.field.AbstractPartialFieldProperty;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class YearMonth extends BasePartial implements Serializable, ReadablePartial {
    public static final int MONTH_OF_YEAR = 1;
    public static final int YEAR = 0;
    private static final long serialVersionUID = 797544782896179L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeFieldType[] f6503 = {DateTimeFieldType.year(), DateTimeFieldType.monthOfYear()};

    public YearMonth() {
    }

    public YearMonth(int i, int i2) {
        this(i, i2, (Chronology) null);
    }

    public YearMonth(int i, int i2, Chronology chronology) {
        super(new int[]{i, i2}, chronology);
    }

    public YearMonth(long j) {
        super(j);
    }

    public YearMonth(long j, Chronology chronology) {
        super(j, chronology);
    }

    public YearMonth(Object obj) {
        super(obj, (Chronology) null, ISODateTimeFormat.m21452());
    }

    public YearMonth(Object obj, Chronology chronology) {
        super(obj, DateTimeUtils.m20895(chronology), ISODateTimeFormat.m21452());
    }

    public YearMonth(Chronology chronology) {
        super(chronology);
    }

    public YearMonth(DateTimeZone dateTimeZone) {
        super((Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    YearMonth(YearMonth yearMonth, Chronology chronology) {
        super((BasePartial) yearMonth, chronology);
    }

    YearMonth(YearMonth yearMonth, int[] iArr) {
        super((BasePartial) yearMonth, iArr);
    }

    public static YearMonth fromCalendarFields(Calendar calendar) {
        if (calendar != null) {
            return new YearMonth(calendar.get(1), calendar.get(2) + 1);
        }
        throw new IllegalArgumentException("The calendar must not be null");
    }

    public static YearMonth fromDateFields(Date date) {
        if (date != null) {
            return new YearMonth(date.getYear() + 1900, date.getMonth() + 1);
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static YearMonth now() {
        return new YearMonth();
    }

    public static YearMonth now(Chronology chronology) {
        if (chronology != null) {
            return new YearMonth(chronology);
        }
        throw new NullPointerException("Chronology must not be null");
    }

    public static YearMonth now(DateTimeZone dateTimeZone) {
        if (dateTimeZone != null) {
            return new YearMonth(dateTimeZone);
        }
        throw new NullPointerException("Zone must not be null");
    }

    @FromString
    public static YearMonth parse(String str) {
        return parse(str, ISODateTimeFormat.m21452());
    }

    public static YearMonth parse(String str, DateTimeFormatter dateTimeFormatter) {
        LocalDate r0 = dateTimeFormatter.m21238(str);
        return new YearMonth(r0.getYear(), r0.getMonthOfYear());
    }

    private Object readResolve() {
        return !DateTimeZone.UTC.equals(getChronology().getZone()) ? new YearMonth(this, getChronology().withUTC()) : this;
    }

    public DateTimeFieldType getFieldType(int i) {
        return f6503[i];
    }

    public DateTimeFieldType[] getFieldTypes() {
        return (DateTimeFieldType[]) f6503.clone();
    }

    public int getMonthOfYear() {
        return getValue(1);
    }

    public int getYear() {
        return getValue(0);
    }

    public YearMonth minus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, -1);
    }

    public YearMonth minusMonths(int i) {
        return withFieldAdded(DurationFieldType.months(), FieldUtils.m21209(i));
    }

    public YearMonth minusYears(int i) {
        return withFieldAdded(DurationFieldType.years(), FieldUtils.m21209(i));
    }

    public Property monthOfYear() {
        return new Property(this, 1);
    }

    public YearMonth plus(ReadablePeriod readablePeriod) {
        return withPeriodAdded(readablePeriod, 1);
    }

    public YearMonth plusMonths(int i) {
        return withFieldAdded(DurationFieldType.months(), i);
    }

    public YearMonth plusYears(int i) {
        return withFieldAdded(DurationFieldType.years(), i);
    }

    public Property property(DateTimeFieldType dateTimeFieldType) {
        return new Property(this, m20927(dateTimeFieldType));
    }

    public int size() {
        return 2;
    }

    public Interval toInterval() {
        return toInterval((DateTimeZone) null);
    }

    public Interval toInterval(DateTimeZone dateTimeZone) {
        DateTimeZone r0 = DateTimeUtils.m20898(dateTimeZone);
        return new Interval((ReadableInstant) toLocalDate(1).toDateTimeAtStartOfDay(r0), (ReadableInstant) plusMonths(1).toLocalDate(1).toDateTimeAtStartOfDay(r0));
    }

    public LocalDate toLocalDate(int i) {
        return new LocalDate(getYear(), getMonthOfYear(), i, getChronology());
    }

    @ToString
    public String toString() {
        return ISODateTimeFormat.m21442().m21246((ReadablePartial) this);
    }

    public String toString(String str) {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21246((ReadablePartial) this);
    }

    public String toString(String str, Locale locale) throws IllegalArgumentException {
        return str == null ? toString() : DateTimeFormat.m21228(str).m21247(locale).m21246((ReadablePartial) this);
    }

    public YearMonth withChronologyRetainFields(Chronology chronology) {
        Chronology withUTC = DateTimeUtils.m20895(chronology).withUTC();
        if (withUTC == getChronology()) {
            return this;
        }
        YearMonth yearMonth = new YearMonth(this, withUTC);
        withUTC.validate(yearMonth, getValues());
        return yearMonth;
    }

    public YearMonth withField(DateTimeFieldType dateTimeFieldType, int i) {
        int r0 = m20927(dateTimeFieldType);
        if (i == getValue(r0)) {
            return this;
        }
        return new YearMonth(this, getField(r0).set(this, r0, getValues(), i));
    }

    public YearMonth withFieldAdded(DurationFieldType durationFieldType, int i) {
        int r0 = m20926(durationFieldType);
        if (i == 0) {
            return this;
        }
        return new YearMonth(this, getField(r0).add(this, r0, getValues(), i));
    }

    public YearMonth withMonthOfYear(int i) {
        return new YearMonth(this, getChronology().monthOfYear().set(this, 1, getValues(), i));
    }

    public YearMonth withPeriodAdded(ReadablePeriod readablePeriod, int i) {
        if (readablePeriod == null || i == 0) {
            return this;
        }
        int[] values = getValues();
        for (int i2 = 0; i2 < readablePeriod.size(); i2++) {
            int r2 = m20928(readablePeriod.getFieldType(i2));
            if (r2 >= 0) {
                values = getField(r2).add(this, r2, values, FieldUtils.m21205(readablePeriod.getValue(i2), i));
            }
        }
        return new YearMonth(this, values);
    }

    public YearMonth withYear(int i) {
        return new YearMonth(this, getChronology().year().set(this, 0, getValues(), i));
    }

    public Property year() {
        return new Property(this, 0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeField m7318(int i, Chronology chronology) {
        switch (i) {
            case 0:
                return chronology.year();
            case 1:
                return chronology.monthOfYear();
            default:
                throw new IndexOutOfBoundsException("Invalid index: " + i);
        }
    }

    public static class Property extends AbstractPartialFieldProperty implements Serializable {
        private static final long serialVersionUID = 5727734012190224363L;
        private final YearMonth iBase;
        private final int iFieldIndex;

        Property(YearMonth yearMonth, int i) {
            this.iBase = yearMonth;
            this.iFieldIndex = i;
        }

        public DateTimeField getField() {
            return this.iBase.getField(this.iFieldIndex);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public ReadablePartial m20924() {
            return this.iBase;
        }

        public YearMonth getYearMonth() {
            return this.iBase;
        }

        public int get() {
            return this.iBase.getValue(this.iFieldIndex);
        }

        public YearMonth addToCopy(int i) {
            return new YearMonth(this.iBase, getField().add(this.iBase, this.iFieldIndex, this.iBase.getValues(), i));
        }

        public YearMonth addWrapFieldToCopy(int i) {
            return new YearMonth(this.iBase, getField().addWrapField(this.iBase, this.iFieldIndex, this.iBase.getValues(), i));
        }

        public YearMonth setCopy(int i) {
            return new YearMonth(this.iBase, getField().set(this.iBase, this.iFieldIndex, this.iBase.getValues(), i));
        }

        public YearMonth setCopy(String str, Locale locale) {
            return new YearMonth(this.iBase, getField().set(this.iBase, this.iFieldIndex, this.iBase.getValues(), str, locale));
        }

        public YearMonth setCopy(String str) {
            return setCopy(str, (Locale) null);
        }
    }
}
