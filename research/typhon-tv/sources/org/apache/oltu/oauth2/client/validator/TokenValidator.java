package org.apache.oltu.oauth2.client.validator;

import org.apache.oltu.oauth2.common.OAuth;

public class TokenValidator extends OAuthClientValidator {
    public TokenValidator() {
        this.requiredParams.put("access_token", new String[0]);
        this.notAllowedParams.add(OAuth.OAUTH_CODE);
    }
}
