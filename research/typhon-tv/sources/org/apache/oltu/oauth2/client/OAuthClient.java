package org.apache.oltu.oauth2.client;

import java.util.HashMap;
import java.util.Map;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthClientResponse;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

public class OAuthClient {
    protected HttpClient httpClient;

    public OAuthClient(HttpClient oauthClient) {
        this.httpClient = oauthClient;
    }

    public <T extends OAuthAccessTokenResponse> T accessToken(OAuthClientRequest request, Class<T> responseClass) throws OAuthSystemException, OAuthProblemException {
        return accessToken(request, OAuth.HttpMethod.POST, responseClass);
    }

    public <T extends OAuthAccessTokenResponse> T accessToken(OAuthClientRequest request, String requestMethod, Class<T> responseClass) throws OAuthSystemException, OAuthProblemException {
        Map<String, String> headers = new HashMap<>();
        headers.put(OAuth.HeaderType.CONTENT_TYPE, OAuth.ContentType.URL_ENCODED);
        return (OAuthAccessTokenResponse) this.httpClient.execute(request, headers, requestMethod, responseClass);
    }

    public OAuthJSONAccessTokenResponse accessToken(OAuthClientRequest request) throws OAuthSystemException, OAuthProblemException {
        return (OAuthJSONAccessTokenResponse) accessToken(request, OAuthJSONAccessTokenResponse.class);
    }

    public OAuthJSONAccessTokenResponse accessToken(OAuthClientRequest request, String requestMethod) throws OAuthSystemException, OAuthProblemException {
        return (OAuthJSONAccessTokenResponse) accessToken(request, requestMethod, OAuthJSONAccessTokenResponse.class);
    }

    public <T extends OAuthClientResponse> T resource(OAuthClientRequest request, String requestMethod, Class<T> responseClass) throws OAuthSystemException, OAuthProblemException {
        return this.httpClient.execute(request, (Map<String, String>) null, requestMethod, responseClass);
    }

    public void shutdown() {
        this.httpClient.shutdown();
    }
}
