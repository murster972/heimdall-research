package org.apache.oltu.oauth2.client.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.oltu.oauth2.client.response.OAuthClientResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

public abstract class OAuthClientValidator {
    protected List<String> notAllowedParams = new ArrayList();
    protected Map<String, String[]> requiredParams = new HashMap();

    public void validate(OAuthClientResponse response) throws OAuthProblemException {
        validateErrorResponse(response);
        validateParameters(response);
    }

    public void validateParameters(OAuthClientResponse response) throws OAuthProblemException {
        validateRequiredParameters(response);
        validateNotAllowedParameters(response);
    }

    public void validateErrorResponse(OAuthClientResponse response) throws OAuthProblemException {
        String error = response.getParam("error");
        if (!OAuthUtils.isEmpty(error)) {
            String errorDesc = response.getParam(OAuthError.OAUTH_ERROR_DESCRIPTION);
            String errorUri = response.getParam(OAuthError.OAUTH_ERROR_URI);
            throw OAuthProblemException.error(error).description(errorDesc).uri(errorUri).state(response.getParam(OAuth.OAUTH_STATE));
        }
    }

    public void validateRequiredParameters(OAuthClientResponse response) throws OAuthProblemException {
        Set<String> missingParameters = new HashSet<>();
        for (Map.Entry<String, String[]> requiredParam : this.requiredParams.entrySet()) {
            String paramName = requiredParam.getKey();
            if (OAuthUtils.isEmpty(response.getParam(paramName))) {
                missingParameters.add(paramName);
            } else {
                String[] dependentParams = requiredParam.getValue();
                if (!OAuthUtils.hasEmptyValues(dependentParams)) {
                    for (String dependentParam : dependentParams) {
                        if (OAuthUtils.isEmpty(response.getParam(dependentParam))) {
                            missingParameters.add(dependentParam);
                        }
                    }
                }
            }
        }
        if (!missingParameters.isEmpty()) {
            throw OAuthUtils.handleMissingParameters(missingParameters);
        }
    }

    public void validateNotAllowedParameters(OAuthClientResponse response) throws OAuthProblemException {
        List<String> notAllowedParameters = new ArrayList<>();
        for (String requiredParam : this.notAllowedParams) {
            if (!OAuthUtils.isEmpty(response.getParam(requiredParam))) {
                notAllowedParameters.add(requiredParam);
            }
        }
        if (!notAllowedParameters.isEmpty()) {
            throw OAuthUtils.handleNotAllowedParametersOAuthException(notAllowedParameters);
        }
    }
}
