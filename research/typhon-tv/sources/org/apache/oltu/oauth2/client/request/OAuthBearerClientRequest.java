package org.apache.oltu.oauth2.client.request;

import org.apache.oltu.oauth2.client.request.OAuthClientRequest;

public class OAuthBearerClientRequest extends OAuthClientRequest.OAuthRequestBuilder {
    public OAuthBearerClientRequest(String url) {
        super(url);
    }

    public OAuthBearerClientRequest setAccessToken(String accessToken) {
        this.parameters.put("access_token", accessToken);
        return this;
    }
}
