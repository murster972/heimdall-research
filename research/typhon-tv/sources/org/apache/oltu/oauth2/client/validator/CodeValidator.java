package org.apache.oltu.oauth2.client.validator;

import org.apache.oltu.oauth2.common.OAuth;

public class CodeValidator extends OAuthClientValidator {
    public CodeValidator() {
        this.requiredParams.put(OAuth.OAUTH_CODE, new String[0]);
        this.notAllowedParams.add("access_token");
        this.notAllowedParams.add(OAuth.OAUTH_EXPIRES_IN);
    }
}
