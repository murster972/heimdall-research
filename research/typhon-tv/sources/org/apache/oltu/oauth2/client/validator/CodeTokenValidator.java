package org.apache.oltu.oauth2.client.validator;

import org.apache.oltu.oauth2.common.OAuth;

public class CodeTokenValidator extends OAuthClientValidator {
    public CodeTokenValidator() {
        this.requiredParams.put(OAuth.OAUTH_CODE, new String[0]);
        this.requiredParams.put("access_token", new String[0]);
        this.notAllowedParams.add("access_token");
    }
}
