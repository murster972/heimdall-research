package org.apache.oltu.oauth2.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthClientResponse;
import org.apache.oltu.oauth2.client.response.OAuthClientResponseFactory;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

public class URLConnectionClient implements HttpClient {
    public <T extends OAuthClientResponse> T execute(OAuthClientRequest request, Map<String, String> headers, String requestMethod, Class<T> responseClass) throws OAuthSystemException, OAuthProblemException {
        InputStream inputStream;
        String responseBody = null;
        try {
            URLConnection c = new URL(request.getLocationUri()).openConnection();
            int responseCode = -1;
            if (c instanceof HttpURLConnection) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) c;
                if (headers != null && !headers.isEmpty()) {
                    for (Map.Entry<String, String> header : headers.entrySet()) {
                        httpURLConnection.addRequestProperty(header.getKey(), header.getValue());
                    }
                }
                if (request.getHeaders() != null) {
                    for (Map.Entry<String, String> header2 : request.getHeaders().entrySet()) {
                        httpURLConnection.addRequestProperty(header2.getKey(), header2.getValue());
                    }
                }
                if (!OAuthUtils.isEmpty(requestMethod)) {
                    httpURLConnection.setRequestMethod(requestMethod);
                    if (requestMethod.equals(OAuth.HttpMethod.POST)) {
                        httpURLConnection.setDoOutput(true);
                        PrintWriter pw = new PrintWriter(httpURLConnection.getOutputStream());
                        pw.print(request.getBody());
                        pw.flush();
                        pw.close();
                    }
                } else {
                    httpURLConnection.setRequestMethod(OAuth.HttpMethod.GET);
                }
                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 400 || responseCode == 401) {
                    inputStream = httpURLConnection.getErrorStream();
                } else {
                    inputStream = httpURLConnection.getInputStream();
                }
                responseBody = OAuthUtils.saveStreamAsString(inputStream);
            }
            return OAuthClientResponseFactory.createCustomResponse(responseBody, c.getContentType(), responseCode, responseClass);
        } catch (IOException e) {
            throw new OAuthSystemException((Throwable) e);
        }
    }

    public void shutdown() {
    }
}
