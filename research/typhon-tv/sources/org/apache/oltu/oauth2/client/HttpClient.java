package org.apache.oltu.oauth2.client;

import java.util.Map;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthClientResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

public interface HttpClient {
    <T extends OAuthClientResponse> T execute(OAuthClientRequest oAuthClientRequest, Map<String, String> map, String str, Class<T> cls) throws OAuthSystemException, OAuthProblemException;

    void shutdown();
}
