package org.apache.oltu.oauth2.client.response;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.oltu.oauth2.client.validator.CodeTokenValidator;
import org.apache.oltu.oauth2.client.validator.CodeValidator;
import org.apache.oltu.oauth2.client.validator.OAuthClientValidator;
import org.apache.oltu.oauth2.client.validator.TokenValidator;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

public class OAuthAuthzResponse extends OAuthClientResponse {
    private HttpServletRequest request;

    protected OAuthAuthzResponse(HttpServletRequest request2, OAuthClientValidator validator) {
        this.request = request2;
        for (Map.Entry<String, String[]> entry : request2.getParameterMap().entrySet()) {
            String key = entry.getKey();
            String[] values = entry.getValue();
            if (!OAuthUtils.hasEmptyValues(values)) {
                this.parameters.put(key, values[0]);
            }
        }
        this.validator = validator;
    }

    public static OAuthAuthzResponse oauthCodeAuthzResponse(HttpServletRequest request2) throws OAuthProblemException {
        OAuthAuthzResponse response = new OAuthAuthzResponse(request2, new CodeValidator());
        response.validate();
        return response;
    }

    public static OAuthAuthzResponse oAuthCodeAndTokenAuthzResponse(HttpServletRequest request2) throws OAuthProblemException {
        OAuthAuthzResponse response = new OAuthAuthzResponse(request2, new CodeTokenValidator());
        response.validate();
        return response;
    }

    public static OAuthAuthzResponse oauthTokenAuthzResponse(HttpServletRequest request2) throws OAuthProblemException {
        OAuthAuthzResponse response = new OAuthAuthzResponse(request2, new TokenValidator());
        response.validate();
        return response;
    }

    public String getAccessToken() {
        return getParam("access_token");
    }

    public Long getExpiresIn() {
        String value = getParam(OAuth.OAUTH_EXPIRES_IN);
        if (value == null) {
            return null;
        }
        return Long.valueOf(value);
    }

    public String getScope() {
        return getParam(OAuth.OAUTH_SCOPE);
    }

    public String getCode() {
        return getParam(OAuth.OAUTH_CODE);
    }

    public String getState() {
        return getParam(OAuth.OAUTH_STATE);
    }

    public HttpServletRequest getRequest() {
        return this.request;
    }

    /* access modifiers changed from: protected */
    public void setBody(String body) {
        this.body = body;
    }

    /* access modifiers changed from: protected */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /* access modifiers changed from: protected */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
