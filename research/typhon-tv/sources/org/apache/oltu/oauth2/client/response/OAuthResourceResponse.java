package org.apache.oltu.oauth2.client.response;

import org.apache.oltu.oauth2.common.exception.OAuthProblemException;

public class OAuthResourceResponse extends OAuthClientResponse {
    public String getBody() {
        return this.body;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public String getContentType() {
        return this.contentType;
    }

    /* access modifiers changed from: protected */
    public void setBody(String body) throws OAuthProblemException {
        this.body = body;
    }

    /* access modifiers changed from: protected */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /* access modifiers changed from: protected */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    /* access modifiers changed from: protected */
    public void init(String body, String contentType, int responseCode) throws OAuthProblemException {
        setBody(body);
        setContentType(contentType);
        setResponseCode(responseCode);
    }
}
