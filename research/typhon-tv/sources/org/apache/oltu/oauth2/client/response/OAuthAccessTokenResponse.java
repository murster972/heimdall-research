package org.apache.oltu.oauth2.client.response;

import org.apache.oltu.oauth2.client.validator.TokenValidator;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.token.OAuthToken;

public abstract class OAuthAccessTokenResponse extends OAuthClientResponse {
    public abstract String getAccessToken();

    public abstract Long getExpiresIn();

    public abstract OAuthToken getOAuthToken();

    public abstract String getRefreshToken();

    public abstract String getScope();

    public String getBody() {
        return this.body;
    }

    /* access modifiers changed from: protected */
    public void init(String body, String contentType, int responseCode) throws OAuthProblemException {
        this.validator = new TokenValidator();
        super.init(body, contentType, responseCode);
    }
}
