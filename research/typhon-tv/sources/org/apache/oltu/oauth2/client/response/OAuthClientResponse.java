package org.apache.oltu.oauth2.client.response;

import java.util.HashMap;
import java.util.Map;
import org.apache.oltu.oauth2.client.validator.OAuthClientValidator;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;

public abstract class OAuthClientResponse {
    protected String body;
    protected String contentType;
    protected Map<String, Object> parameters = new HashMap();
    protected int responseCode;
    protected OAuthClientValidator validator;

    /* access modifiers changed from: protected */
    public abstract void setBody(String str) throws OAuthProblemException;

    /* access modifiers changed from: protected */
    public abstract void setContentType(String str);

    /* access modifiers changed from: protected */
    public abstract void setResponseCode(int i);

    public String getParam(String param) {
        Object value = this.parameters.get(param);
        if (value == null) {
            return null;
        }
        return String.valueOf(value);
    }

    /* access modifiers changed from: protected */
    public void init(String body2, String contentType2, int responseCode2) throws OAuthProblemException {
        setBody(body2);
        setContentType(contentType2);
        setResponseCode(responseCode2);
        validate();
    }

    /* access modifiers changed from: protected */
    public void validate() throws OAuthProblemException {
        this.validator.validate(this);
    }
}
