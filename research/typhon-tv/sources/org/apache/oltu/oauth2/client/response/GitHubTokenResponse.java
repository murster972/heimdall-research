package org.apache.oltu.oauth2.client.response;

import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.token.BasicOAuthToken;
import org.apache.oltu.oauth2.common.token.OAuthToken;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

public class GitHubTokenResponse extends OAuthAccessTokenResponse {
    public String getAccessToken() {
        return getParam("access_token");
    }

    public Long getExpiresIn() {
        String value = getParam(OAuth.OAUTH_EXPIRES_IN);
        if (value == null) {
            return null;
        }
        return Long.valueOf(value);
    }

    public String getRefreshToken() {
        return getParam(OAuth.OAUTH_EXPIRES_IN);
    }

    public String getScope() {
        return getParam(OAuth.OAUTH_SCOPE);
    }

    public OAuthToken getOAuthToken() {
        return new BasicOAuthToken(getAccessToken(), getExpiresIn(), getRefreshToken(), getScope());
    }

    /* access modifiers changed from: protected */
    public void setBody(String body) {
        this.body = body;
        this.parameters = OAuthUtils.decodeForm(body);
    }

    /* access modifiers changed from: protected */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /* access modifiers changed from: protected */
    public void setResponseCode(int code) {
        this.responseCode = code;
    }
}
