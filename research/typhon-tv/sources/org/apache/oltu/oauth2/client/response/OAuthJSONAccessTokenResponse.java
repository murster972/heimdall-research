package org.apache.oltu.oauth2.client.response;

import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.token.BasicOAuthToken;
import org.apache.oltu.oauth2.common.token.OAuthToken;
import org.apache.oltu.oauth2.common.utils.JSONUtils;

public class OAuthJSONAccessTokenResponse extends OAuthAccessTokenResponse {
    public String getAccessToken() {
        return getParam("access_token");
    }

    public Long getExpiresIn() {
        String value = getParam(OAuth.OAUTH_EXPIRES_IN);
        if (value == null) {
            return null;
        }
        return Long.valueOf(value);
    }

    public String getScope() {
        return getParam(OAuth.OAUTH_SCOPE);
    }

    public OAuthToken getOAuthToken() {
        return new BasicOAuthToken(getAccessToken(), getExpiresIn(), getRefreshToken(), getScope());
    }

    public String getRefreshToken() {
        return getParam(OAuth.OAUTH_REFRESH_TOKEN);
    }

    /* access modifiers changed from: protected */
    public void setBody(String body) throws OAuthProblemException {
        try {
            this.body = body;
            this.parameters = JSONUtils.parseJSON(body);
        } catch (Throwable th) {
            throw OAuthProblemException.error(OAuthError.CodeResponse.UNSUPPORTED_RESPONSE_TYPE, "Invalid response! Response body is not application/json encoded");
        }
    }

    /* access modifiers changed from: protected */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /* access modifiers changed from: protected */
    public void setResponseCode(int code) {
        this.responseCode = code;
    }
}
