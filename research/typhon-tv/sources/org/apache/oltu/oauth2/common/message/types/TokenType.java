package org.apache.oltu.oauth2.common.message.types;

public enum TokenType {
    BEARER("Bearer"),
    MAC("MAC");
    
    private String tokenType;

    private TokenType(String grantType) {
        this.tokenType = grantType;
    }

    public String toString() {
        return this.tokenType;
    }
}
