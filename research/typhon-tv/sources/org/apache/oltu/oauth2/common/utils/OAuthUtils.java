package org.apache.oltu.oauth2.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

public final class OAuthUtils {
    public static final String AUTH_SCHEME = "Bearer";
    private static final String DEFAULT_CONTENT_CHARSET = "UTF-8";
    private static final String ENCODING = "UTF-8";
    public static final String MULTIPART = "multipart/";
    private static final String NAME_VALUE_SEPARATOR = "=";
    private static final Pattern NVP = Pattern.compile("(\\S*)\\s*\\=\\s*\"([^\"]*)\"");
    private static final Pattern OAUTH_HEADER = Pattern.compile("\\s*(\\w*)\\s+(.*)");
    private static final String PARAMETER_SEPARATOR = "&";

    public static String format(Collection<? extends Map.Entry<String, Object>> parameters, String encoding) {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<String, Object> parameter : parameters) {
            String value = parameter.getValue() == null ? null : String.valueOf(parameter.getValue());
            if (!isEmpty(parameter.getKey()) && !isEmpty(value)) {
                String encodedName = encode(parameter.getKey(), encoding);
                String encodedValue = value != null ? encode(value, encoding) : "";
                if (result.length() > 0) {
                    result.append(PARAMETER_SEPARATOR);
                }
                result.append(encodedName);
                result.append(NAME_VALUE_SEPARATOR);
                result.append(encodedValue);
            }
        }
        return result.toString();
    }

    private static String encode(String content, String encoding) {
        if (encoding == null) {
            encoding = "UTF-8";
        }
        try {
            return URLEncoder.encode(content, encoding);
        } catch (UnsupportedEncodingException problem) {
            throw new IllegalArgumentException(problem);
        }
    }

    public static String saveStreamAsString(InputStream is) throws IOException {
        return toString(is, "UTF-8");
    }

    /* JADX INFO: finally extract failed */
    public static String toString(InputStream is, String defaultCharset) throws IOException {
        if (is == null) {
            throw new IllegalArgumentException("InputStream may not be null");
        }
        String charset = defaultCharset;
        if (charset == null) {
            charset = "UTF-8";
        }
        Reader reader = new InputStreamReader(is, charset);
        StringBuilder sb = new StringBuilder();
        try {
            char[] tmp = new char[4096];
            while (true) {
                int l = reader.read(tmp);
                if (l != -1) {
                    sb.append(tmp, 0, l);
                } else {
                    reader.close();
                    return sb.toString();
                }
            }
        } catch (Throwable th) {
            reader.close();
            throw th;
        }
    }

    public static OAuthProblemException handleOAuthProblemException(String message) {
        return OAuthProblemException.error("invalid_request").description(message);
    }

    public static OAuthProblemException handleMissingParameters(Set<String> missingParams) {
        StringBuffer sb = new StringBuffer("Missing parameters: ");
        if (!isEmpty(missingParams)) {
            for (String missingParam : missingParams) {
                sb.append(missingParam).append(StringUtils.SPACE);
            }
        }
        return handleOAuthProblemException(sb.toString().trim());
    }

    public static OAuthProblemException handleBadContentTypeException(String expectedContentType) {
        return handleOAuthProblemException("Bad request content type. Expecting: " + expectedContentType);
    }

    public static OAuthProblemException handleNotAllowedParametersOAuthException(List<String> notAllowedParams) {
        StringBuffer sb = new StringBuffer("Not allowed parameters: ");
        if (notAllowedParams != null) {
            for (String notAllowed : notAllowedParams) {
                sb.append(notAllowed).append(StringUtils.SPACE);
            }
        }
        return handleOAuthProblemException(sb.toString().trim());
    }

    public static Map<String, Object> decodeForm(String form) {
        String name;
        String value;
        Map<String, Object> params = new HashMap<>();
        if (!isEmpty(form)) {
            for (String nvp : form.split("\\&")) {
                int equals = nvp.indexOf(61);
                if (equals < 0) {
                    name = decodePercent(nvp);
                    value = null;
                } else {
                    name = decodePercent(nvp.substring(0, equals));
                    value = decodePercent(nvp.substring(equals + 1));
                }
                params.put(name, value);
            }
        }
        return params;
    }

    public static boolean isFormEncoded(String contentType) {
        if (contentType == null) {
            return false;
        }
        int semi = contentType.indexOf(";");
        if (semi >= 0) {
            contentType = contentType.substring(0, semi);
        }
        return OAuth.ContentType.URL_ENCODED.equalsIgnoreCase(contentType.trim());
    }

    public static String decodePercent(String s) {
        try {
            return URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException wow) {
            throw new RuntimeException(wow.getMessage(), wow);
        }
    }

    public static String percentEncode(Iterable values) {
        StringBuilder p = new StringBuilder();
        for (Object v : values) {
            if (!isEmpty(toString(v))) {
                if (p.length() > 0) {
                    p.append(PARAMETER_SEPARATOR);
                }
                p.append(percentEncode(toString(v)));
            }
        }
        return p.toString();
    }

    public static String percentEncode(String s) {
        if (s == null) {
            return "";
        }
        try {
            return URLEncoder.encode(s, "UTF-8").replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
        } catch (UnsupportedEncodingException wow) {
            throw new RuntimeException(wow.getMessage(), wow);
        }
    }

    private static final String toString(Object from) {
        if (from == null) {
            return null;
        }
        return from.toString();
    }

    private static boolean isEmpty(Set<String> missingParams) {
        if (missingParams == null || missingParams.size() == 0) {
            return true;
        }
        return false;
    }

    public static <T> T instantiateClass(Class<T> clazz) throws OAuthSystemException {
        return instantiateClassWithParameters(clazz, (Class<?>[]) null, (Object[]) null);
    }

    public static <T> T instantiateClassWithParameters(Class<T> clazz, Class<?>[] paramsTypes, Object[] paramValues) throws OAuthSystemException {
        if (paramsTypes == null || paramValues == null) {
            return clazz.newInstance();
        }
        try {
            if (paramsTypes.length != paramValues.length) {
                throw new IllegalArgumentException("Number of types and values must be equal");
            } else if (paramsTypes.length == 0 && paramValues.length == 0) {
                return clazz.newInstance();
            } else {
                return clazz.getConstructor(paramsTypes).newInstance(paramValues);
            }
        } catch (NoSuchMethodException e) {
            throw new OAuthSystemException((Throwable) e);
        } catch (InstantiationException e2) {
            throw new OAuthSystemException((Throwable) e2);
        } catch (IllegalAccessException e3) {
            throw new OAuthSystemException((Throwable) e3);
        } catch (InvocationTargetException e4) {
            throw new OAuthSystemException((Throwable) e4);
        }
    }

    public static String getAuthHeaderField(String authHeader) {
        if (authHeader != null) {
            Matcher m = OAUTH_HEADER.matcher(authHeader);
            if (m.matches() && "Bearer".equalsIgnoreCase(m.group(1))) {
                return m.group(2);
            }
        }
        return null;
    }

    public static Map<String, String> decodeOAuthHeader(String header) {
        Map<String, String> headerValues = new HashMap<>();
        if (header != null) {
            Matcher m = OAUTH_HEADER.matcher(header);
            if (m.matches() && "Bearer".equalsIgnoreCase(m.group(1))) {
                for (String nvp : m.group(2).split("\\s*,\\s*")) {
                    Matcher m2 = NVP.matcher(nvp);
                    if (m2.matches()) {
                        headerValues.put(decodePercent(m2.group(1)), decodePercent(m2.group(2)));
                    }
                }
            }
        }
        return headerValues;
    }

    public static String[] decodeClientAuthenticationHeader(String authenticationHeader) {
        String[] tokens;
        if (authenticationHeader == null || "".equals(authenticationHeader) || (tokens = authenticationHeader.split(StringUtils.SPACE)) == null) {
            return null;
        }
        if ((tokens[0] != null && !"".equals(tokens[0]) && !tokens[0].equalsIgnoreCase("basic")) || tokens[1] == null || "".equals(tokens[1])) {
            return null;
        }
        String decodedCreds = new String(Base64.decodeBase64(tokens[1]));
        if (!decodedCreds.contains(":") || decodedCreds.split(":").length != 2) {
            return null;
        }
        String[] creds = decodedCreds.split(":");
        if (isEmpty(creds[0]) || isEmpty(creds[1])) {
            return null;
        }
        return decodedCreds.split(":");
    }

    public static String encodeOAuthHeader(Map<String, Object> entries) {
        StringBuffer sb = new StringBuffer();
        sb.append("Bearer").append(StringUtils.SPACE);
        for (Map.Entry<String, Object> entry : entries.entrySet()) {
            String value = entry.getValue() == null ? null : String.valueOf(entry.getValue());
            if (!isEmpty(entry.getKey()) && !isEmpty(value)) {
                sb.append(entry.getKey());
                sb.append("=\"");
                sb.append(value);
                sb.append("\",");
            }
        }
        return sb.substring(0, sb.length() - 1);
    }

    public static String encodeAuthorizationBearerHeader(Map<String, Object> entries) {
        StringBuffer sb = new StringBuffer();
        sb.append("Bearer").append(StringUtils.SPACE);
        for (Map.Entry<String, Object> entry : entries.entrySet()) {
            String value = entry.getValue() == null ? null : String.valueOf(entry.getValue());
            if (!isEmpty(entry.getKey()) && !isEmpty(value)) {
                sb.append(value);
            }
        }
        return sb.toString();
    }

    public static boolean isEmpty(String value) {
        return value == null || "".equals(value);
    }

    public static boolean hasEmptyValues(String[] array) {
        if (array == null || array.length == 0) {
            return true;
        }
        for (String s : array) {
            if (isEmpty(s)) {
                return true;
            }
        }
        return false;
    }

    public static String getAuthzMethod(String header) {
        if (header != null) {
            Matcher m = OAUTH_HEADER.matcher(header);
            if (m.matches()) {
                return m.group(1);
            }
        }
        return null;
    }

    public static Set<String> decodeScopes(String s) {
        Set<String> scopes = new HashSet<>();
        if (!isEmpty(s)) {
            StringTokenizer tokenizer = new StringTokenizer(s, StringUtils.SPACE);
            while (tokenizer.hasMoreElements()) {
                scopes.add(tokenizer.nextToken());
            }
        }
        return scopes;
    }

    public static String encodeScopes(Set<String> s) {
        StringBuffer scopes = new StringBuffer();
        for (String scope : s) {
            scopes.append(scope).append(StringUtils.SPACE);
        }
        return scopes.toString().trim();
    }

    public static boolean isMultipart(HttpServletRequest request) {
        String contentType;
        if ("post".equals(request.getMethod().toLowerCase()) && (contentType = request.getContentType()) != null && contentType.toLowerCase().startsWith(MULTIPART)) {
            return true;
        }
        return false;
    }

    public static boolean hasContentType(String requestContentType, String requiredContentType) {
        if (isEmpty(requiredContentType) || isEmpty(requestContentType)) {
            return false;
        }
        StringTokenizer tokenizer = new StringTokenizer(requestContentType, ";");
        while (tokenizer.hasMoreTokens()) {
            if (requiredContentType.equals(tokenizer.nextToken())) {
                return true;
            }
        }
        return false;
    }
}
