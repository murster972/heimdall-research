package org.apache.oltu.oauth2.common.parameters;

import java.util.Map;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthMessage;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

public class WWWAuthHeaderParametersApplier implements OAuthParametersApplier {
    public OAuthMessage applyOAuthParameters(OAuthMessage message, Map<String, Object> params) throws OAuthSystemException {
        message.addHeader(OAuth.HeaderType.WWW_AUTHENTICATE, OAuthUtils.encodeOAuthHeader(params));
        return message;
    }
}
