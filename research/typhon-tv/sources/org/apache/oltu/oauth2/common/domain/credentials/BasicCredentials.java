package org.apache.oltu.oauth2.common.domain.credentials;

public class BasicCredentials implements Credentials {
    private String clientId;
    private String clientSecret;
    private Long expiresIn;
    private Long issuedAt;

    BasicCredentials() {
    }

    public BasicCredentials(String clientId2, String clientSecret2, Long issuedAt2, Long expiresIn2) {
        this.clientId = clientId2;
        this.clientSecret = clientSecret2;
        this.issuedAt = issuedAt2;
        this.expiresIn = expiresIn2;
    }

    public String getClientId() {
        return this.clientId;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public Long getIssuedAt() {
        return this.issuedAt;
    }

    public Long getExpiresIn() {
        return this.expiresIn;
    }

    public void setClientId(String clientId2) {
        this.clientId = clientId2;
    }

    public void setClientSecret(String clientSecret2) {
        this.clientSecret = clientSecret2;
    }

    public void setIssuedAt(Long issuedAt2) {
        this.issuedAt = issuedAt2;
    }

    public void setExpiresIn(Long expiresIn2) {
        this.expiresIn = expiresIn2;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BasicCredentials that = (BasicCredentials) o;
        if (this.clientId == null ? that.clientId != null : !this.clientId.equals(that.clientId)) {
            return false;
        }
        if (this.clientSecret == null ? that.clientSecret != null : !this.clientSecret.equals(that.clientSecret)) {
            return false;
        }
        if (this.expiresIn == null ? that.expiresIn != null : !this.expiresIn.equals(that.expiresIn)) {
            return false;
        }
        if (this.issuedAt != null) {
            if (this.issuedAt.equals(that.issuedAt)) {
                return true;
            }
        } else if (that.issuedAt == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3 = 0;
        if (this.clientId != null) {
            result = this.clientId.hashCode();
        } else {
            result = 0;
        }
        int i4 = result * 31;
        if (this.clientSecret != null) {
            i = this.clientSecret.hashCode();
        } else {
            i = 0;
        }
        int i5 = (i4 + i) * 31;
        if (this.issuedAt != null) {
            i2 = this.issuedAt.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        if (this.expiresIn != null) {
            i3 = this.expiresIn.hashCode();
        }
        return i6 + i3;
    }
}
