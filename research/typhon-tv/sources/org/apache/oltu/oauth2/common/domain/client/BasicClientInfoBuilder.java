package org.apache.oltu.oauth2.common.domain.client;

public class BasicClientInfoBuilder {
    private BasicClientInfo info = new BasicClientInfo();

    private BasicClientInfoBuilder() {
    }

    public static BasicClientInfoBuilder clientInfo() {
        return new BasicClientInfoBuilder();
    }

    public BasicClientInfo build() {
        return this.info;
    }

    public BasicClientInfoBuilder setName(String value) {
        this.info.setName(value);
        return this;
    }

    public BasicClientInfoBuilder setClientId(String value) {
        this.info.setClientId(value);
        return this;
    }

    public BasicClientInfoBuilder setClientUrl(String value) {
        this.info.setClientUri(value);
        return this;
    }

    public BasicClientInfoBuilder setClientSecret(String value) {
        this.info.setClientSecret(value);
        return this;
    }

    public BasicClientInfoBuilder setIconUri(String value) {
        this.info.setIconUri(value);
        return this;
    }

    public BasicClientInfoBuilder setRedirectUri(String value) {
        this.info.setRedirectUri(value);
        return this;
    }

    public BasicClientInfoBuilder setDescription(String value) {
        this.info.setDescription(value);
        return this;
    }

    public BasicClientInfoBuilder setExpiresIn(Long value) {
        this.info.setExpiresIn(value);
        return this;
    }

    public BasicClientInfoBuilder setIssuedAt(Long value) {
        this.info.setIssuedAt(value);
        return this;
    }
}
