package org.apache.oltu.oauth2.common.domain.client;

public interface ClientInfo {
    String getClientId();

    String getClientSecret();

    String getClientUri();

    String getDescription();

    Long getExpiresIn();

    String getIconUri();

    Long getIssuedAt();

    String getName();

    String getRedirectUri();
}
