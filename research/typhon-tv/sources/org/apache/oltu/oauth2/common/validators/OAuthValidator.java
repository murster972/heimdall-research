package org.apache.oltu.oauth2.common.validators;

import javax.servlet.http.HttpServletRequest;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;

public interface OAuthValidator<T extends HttpServletRequest> {
    void performAllValidations(T t) throws OAuthProblemException;

    void validateClientAuthenticationCredentials(T t) throws OAuthProblemException;

    void validateContentType(T t) throws OAuthProblemException;

    void validateMethod(T t) throws OAuthProblemException;

    void validateNotAllowedParameters(T t) throws OAuthProblemException;

    void validateOptionalParameters(T t) throws OAuthProblemException;

    void validateRequiredParameters(T t) throws OAuthProblemException;
}
