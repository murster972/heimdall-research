package org.apache.oltu.oauth2.common.domain.credentials;

public class BasicCredentialsBuilder {
    protected BasicCredentials credentials = new BasicCredentials();

    private BasicCredentialsBuilder() {
    }

    public static BasicCredentialsBuilder credentials() {
        return new BasicCredentialsBuilder();
    }

    public BasicCredentials build() {
        return this.credentials;
    }

    public BasicCredentialsBuilder setClientId(String value) {
        this.credentials.setClientId(value);
        return this;
    }

    public BasicCredentialsBuilder setClientSecret(String value) {
        this.credentials.setClientSecret(value);
        return this;
    }

    public BasicCredentialsBuilder setExpiresIn(Long value) {
        this.credentials.setExpiresIn(value);
        return this;
    }

    public BasicCredentialsBuilder setIssuedAt(Long value) {
        this.credentials.setIssuedAt(value);
        return this;
    }
}
