package org.apache.oltu.oauth2.common.token;

public class BasicOAuthToken implements OAuthToken {
    protected String accessToken;
    protected Long expiresIn;
    protected String refreshToken;
    protected String scope;

    public BasicOAuthToken() {
    }

    public BasicOAuthToken(String accessToken2, Long expiresIn2, String refreshToken2, String scope2) {
        this.accessToken = accessToken2;
        this.expiresIn = expiresIn2;
        this.refreshToken = refreshToken2;
        this.scope = scope2;
    }

    public BasicOAuthToken(String accessToken2) {
        this(accessToken2, (Long) null, (String) null, (String) null);
    }

    public BasicOAuthToken(String accessToken2, Long expiresIn2) {
        this(accessToken2, expiresIn2, (String) null, (String) null);
    }

    public BasicOAuthToken(String accessToken2, Long expiresIn2, String scope2) {
        this(accessToken2, expiresIn2, (String) null, scope2);
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public Long getExpiresIn() {
        return this.expiresIn;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    public String getScope() {
        return this.scope;
    }
}
