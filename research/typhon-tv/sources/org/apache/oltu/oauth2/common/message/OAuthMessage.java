package org.apache.oltu.oauth2.common.message;

import java.util.Map;

public interface OAuthMessage {
    void addHeader(String str, String str2);

    String getBody();

    String getHeader(String str);

    Map<String, String> getHeaders();

    String getLocationUri();

    void setBody(String str);

    void setHeaders(Map<String, String> map);

    void setLocationUri(String str);
}
