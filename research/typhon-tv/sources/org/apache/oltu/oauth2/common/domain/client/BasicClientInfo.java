package org.apache.oltu.oauth2.common.domain.client;

public class BasicClientInfo implements ClientInfo {
    protected String clientId;
    protected String clientSecret;
    protected String clientUri;
    protected String description;
    protected Long expiresIn;
    protected String iconUri;
    protected Long issuedAt;
    protected String name;
    protected String redirectUri;

    public String getClientId() {
        return this.clientId;
    }

    public String getClientSecret() {
        return this.clientSecret;
    }

    public String getRedirectUri() {
        return this.redirectUri;
    }

    public String getName() {
        return this.name;
    }

    public String getIconUri() {
        return this.iconUri;
    }

    public String getClientUri() {
        return this.clientUri;
    }

    public String getDescription() {
        return this.description;
    }

    public void setClientUri(String clientUri2) {
        this.clientUri = clientUri2;
    }

    public Long getIssuedAt() {
        return this.issuedAt;
    }

    public void setIssuedAt(Long issuedAt2) {
        this.issuedAt = issuedAt2;
    }

    public Long getExpiresIn() {
        return this.expiresIn;
    }

    public void setExpiresIn(Long expiresIn2) {
        this.expiresIn = expiresIn2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void setClientId(String clientId2) {
        this.clientId = clientId2;
    }

    public void setClientSecret(String clientSecret2) {
        this.clientSecret = clientSecret2;
    }

    public void setRedirectUri(String redirectUri2) {
        this.redirectUri = redirectUri2;
    }

    public void setIconUri(String iconUri2) {
        this.iconUri = iconUri2;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BasicClientInfo that = (BasicClientInfo) o;
        if (this.clientId == null ? that.clientId != null : !this.clientId.equals(that.clientId)) {
            return false;
        }
        if (this.clientSecret == null ? that.clientSecret != null : !this.clientSecret.equals(that.clientSecret)) {
            return false;
        }
        if (this.clientUri == null ? that.clientUri != null : !this.clientUri.equals(that.clientUri)) {
            return false;
        }
        if (this.description == null ? that.description != null : !this.description.equals(that.description)) {
            return false;
        }
        if (this.expiresIn == null ? that.expiresIn != null : !this.expiresIn.equals(that.expiresIn)) {
            return false;
        }
        if (this.iconUri == null ? that.iconUri != null : !this.iconUri.equals(that.iconUri)) {
            return false;
        }
        if (this.issuedAt == null ? that.issuedAt != null : !this.issuedAt.equals(that.issuedAt)) {
            return false;
        }
        if (this.name == null ? that.name != null : !this.name.equals(that.name)) {
            return false;
        }
        if (this.redirectUri != null) {
            if (this.redirectUri.equals(that.redirectUri)) {
                return true;
            }
        } else if (that.redirectUri == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8 = 0;
        if (this.name != null) {
            result = this.name.hashCode();
        } else {
            result = 0;
        }
        int i9 = result * 31;
        if (this.clientId != null) {
            i = this.clientId.hashCode();
        } else {
            i = 0;
        }
        int i10 = (i9 + i) * 31;
        if (this.clientSecret != null) {
            i2 = this.clientSecret.hashCode();
        } else {
            i2 = 0;
        }
        int i11 = (i10 + i2) * 31;
        if (this.redirectUri != null) {
            i3 = this.redirectUri.hashCode();
        } else {
            i3 = 0;
        }
        int i12 = (i11 + i3) * 31;
        if (this.clientUri != null) {
            i4 = this.clientUri.hashCode();
        } else {
            i4 = 0;
        }
        int i13 = (i12 + i4) * 31;
        if (this.description != null) {
            i5 = this.description.hashCode();
        } else {
            i5 = 0;
        }
        int i14 = (i13 + i5) * 31;
        if (this.iconUri != null) {
            i6 = this.iconUri.hashCode();
        } else {
            i6 = 0;
        }
        int i15 = (i14 + i6) * 31;
        if (this.issuedAt != null) {
            i7 = this.issuedAt.hashCode();
        } else {
            i7 = 0;
        }
        int i16 = (i15 + i7) * 31;
        if (this.expiresIn != null) {
            i8 = this.expiresIn.hashCode();
        }
        return i16 + i8;
    }
}
