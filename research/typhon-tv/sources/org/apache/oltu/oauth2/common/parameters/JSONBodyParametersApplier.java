package org.apache.oltu.oauth2.common.parameters;

import java.util.Map;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthMessage;
import org.apache.oltu.oauth2.common.utils.JSONUtils;

public class JSONBodyParametersApplier implements OAuthParametersApplier {
    public OAuthMessage applyOAuthParameters(OAuthMessage message, Map<String, Object> params) throws OAuthSystemException {
        try {
            message.setBody(JSONUtils.buildJSON(params));
            return message;
        } catch (Throwable e) {
            throw new OAuthSystemException(e);
        }
    }
}
