package org.apache.oltu.oauth2.common.exception;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

public class OAuthProblemException extends Exception {
    private String description;
    private String error;
    private Map<String, String> parameters;
    private String redirectUri;
    private int responseStatus;
    private String scope;
    private String state;
    private String uri;

    protected OAuthProblemException(String error2) {
        this(error2, "");
    }

    protected OAuthProblemException(String error2, String description2) {
        super(error2 + StringUtils.SPACE + description2);
        this.parameters = new HashMap();
        this.description = description2;
        this.error = error2;
    }

    public static OAuthProblemException error(String error2) {
        return new OAuthProblemException(error2);
    }

    public static OAuthProblemException error(String error2, String description2) {
        return new OAuthProblemException(error2, description2);
    }

    public OAuthProblemException description(String description2) {
        this.description = description2;
        return this;
    }

    public OAuthProblemException uri(String uri2) {
        this.uri = uri2;
        return this;
    }

    public OAuthProblemException state(String state2) {
        this.state = state2;
        return this;
    }

    public OAuthProblemException scope(String scope2) {
        this.scope = scope2;
        return this;
    }

    public OAuthProblemException responseStatus(int responseStatus2) {
        this.responseStatus = responseStatus2;
        return this;
    }

    public OAuthProblemException setParameter(String name, String value) {
        this.parameters.put(name, value);
        return this;
    }

    public String getError() {
        return this.error;
    }

    public String getDescription() {
        return this.description;
    }

    public String getUri() {
        return this.uri;
    }

    public String getState() {
        return this.state;
    }

    public String getScope() {
        return this.scope;
    }

    public int getResponseStatus() {
        if (this.responseStatus == 0) {
            return 400;
        }
        return this.responseStatus;
    }

    public String get(String name) {
        return this.parameters.get(name);
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public String getRedirectUri() {
        return this.redirectUri;
    }

    public void setRedirectUri(String redirectUri2) {
        this.redirectUri = redirectUri2;
    }

    public String getMessage() {
        StringBuilder b = new StringBuilder();
        if (!OAuthUtils.isEmpty(this.error)) {
            b.append(this.error);
        }
        if (!OAuthUtils.isEmpty(this.description)) {
            b.append(", ").append(this.description);
        }
        if (!OAuthUtils.isEmpty(this.uri)) {
            b.append(", ").append(this.uri);
        }
        if (!OAuthUtils.isEmpty(this.state)) {
            b.append(", ").append(this.state);
        }
        if (!OAuthUtils.isEmpty(this.scope)) {
            b.append(", ").append(this.scope);
        }
        return b.toString();
    }

    public String toString() {
        return "OAuthProblemException{error='" + this.error + '\'' + ", description='" + this.description + '\'' + ", uri='" + this.uri + '\'' + ", state='" + this.state + '\'' + ", scope='" + this.scope + '\'' + ", redirectUri='" + this.redirectUri + '\'' + ", responseStatus=" + this.responseStatus + ", parameters=" + this.parameters + '}';
    }
}
