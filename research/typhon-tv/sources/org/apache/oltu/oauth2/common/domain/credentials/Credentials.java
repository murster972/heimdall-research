package org.apache.oltu.oauth2.common.domain.credentials;

public interface Credentials {
    String getClientId();

    String getClientSecret();

    Long getExpiresIn();

    Long getIssuedAt();
}
