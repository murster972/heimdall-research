package org.apache.oltu.oauth2.common.validators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.utils.OAuthUtils;

public abstract class AbstractValidator<T extends HttpServletRequest> implements OAuthValidator<T> {
    protected boolean enforceClientAuthentication;
    protected List<String> notAllowedParams = new ArrayList();
    protected Map<String, String[]> optionalParams = new HashMap();
    protected List<String> requiredParams = new ArrayList();

    public void validateMethod(T request) throws OAuthProblemException {
        if (!request.getMethod().equals(OAuth.HttpMethod.POST)) {
            throw OAuthUtils.handleOAuthProblemException("Method not set to POST.");
        }
    }

    public void validateContentType(T request) throws OAuthProblemException {
        if (!OAuthUtils.hasContentType(request.getContentType(), OAuth.ContentType.URL_ENCODED)) {
            throw OAuthUtils.handleBadContentTypeException(OAuth.ContentType.URL_ENCODED);
        }
    }

    public void validateRequiredParameters(T request) throws OAuthProblemException {
        Set<String> missingParameters = new HashSet<>();
        for (String requiredParam : this.requiredParams) {
            if (OAuthUtils.isEmpty(request.getParameter(requiredParam))) {
                missingParameters.add(requiredParam);
            }
        }
        if (!missingParameters.isEmpty()) {
            throw OAuthUtils.handleMissingParameters(missingParameters);
        }
    }

    public void validateOptionalParameters(T request) throws OAuthProblemException {
        Set<String> missingParameters = new HashSet<>();
        for (Map.Entry<String, String[]> requiredParam : this.optionalParams.entrySet()) {
            if (!OAuthUtils.isEmpty(request.getParameter(requiredParam.getKey()))) {
                String[] dependentParams = requiredParam.getValue();
                if (!OAuthUtils.hasEmptyValues(dependentParams)) {
                    for (String dependentParam : dependentParams) {
                        if (OAuthUtils.isEmpty(request.getParameter(dependentParam))) {
                            missingParameters.add(dependentParam);
                        }
                    }
                }
            }
        }
        if (!missingParameters.isEmpty()) {
            throw OAuthUtils.handleMissingParameters(missingParameters);
        }
    }

    public void validateNotAllowedParameters(T request) throws OAuthProblemException {
        List<String> notAllowedParameters = new ArrayList<>();
        for (String requiredParam : this.notAllowedParams) {
            if (!OAuthUtils.isEmpty(request.getParameter(requiredParam))) {
                notAllowedParameters.add(requiredParam);
            }
        }
        if (!notAllowedParameters.isEmpty()) {
            throw OAuthUtils.handleNotAllowedParametersOAuthException(notAllowedParameters);
        }
    }

    public void validateClientAuthenticationCredentials(T request) throws OAuthProblemException {
        if (this.enforceClientAuthentication) {
            Set<String> missingParameters = new HashSet<>();
            String[] clientCreds = OAuthUtils.decodeClientAuthenticationHeader(request.getHeader(OAuth.HeaderType.AUTHORIZATION));
            if (clientCreds == null || OAuthUtils.isEmpty(clientCreds[0]) || OAuthUtils.isEmpty(clientCreds[1])) {
                if (OAuthUtils.isEmpty(request.getParameter(OAuth.OAUTH_CLIENT_ID))) {
                    missingParameters.add(OAuth.OAUTH_CLIENT_ID);
                }
                if (OAuthUtils.isEmpty(request.getParameter(OAuth.OAUTH_CLIENT_SECRET))) {
                    missingParameters.add(OAuth.OAUTH_CLIENT_SECRET);
                }
            }
            if (!missingParameters.isEmpty()) {
                throw OAuthUtils.handleMissingParameters(missingParameters);
            }
        }
    }

    public void performAllValidations(T request) throws OAuthProblemException {
        validateContentType(request);
        validateMethod(request);
        validateRequiredParameters(request);
        validateOptionalParameters(request);
        validateNotAllowedParameters(request);
        validateClientAuthenticationCredentials(request);
    }
}
