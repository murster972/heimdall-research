package org.apache.oltu.oauth2.common.token;

public interface OAuthToken {
    String getAccessToken();

    Long getExpiresIn();

    String getRefreshToken();

    String getScope();
}
