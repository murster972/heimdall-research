package org.apache.oltu.oauth2.common.parameters;

import java.util.Map;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthMessage;

public interface OAuthParametersApplier {
    OAuthMessage applyOAuthParameters(OAuthMessage oAuthMessage, Map<String, Object> map) throws OAuthSystemException;
}
