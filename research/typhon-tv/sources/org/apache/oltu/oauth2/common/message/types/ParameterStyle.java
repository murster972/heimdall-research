package org.apache.oltu.oauth2.common.message.types;

public enum ParameterStyle {
    BODY(TtmlNode.TAG_BODY),
    QUERY("query"),
    HEADER("header");
    
    private String parameterStyle;

    private ParameterStyle(String parameterStyle2) {
        this.parameterStyle = parameterStyle2;
    }

    public String toString() {
        return this.parameterStyle;
    }
}
