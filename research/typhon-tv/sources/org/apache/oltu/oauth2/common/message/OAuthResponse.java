package org.apache.oltu.oauth2.common.message;

import java.util.HashMap;
import java.util.Map;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.parameters.BodyURLEncodedParametersApplier;
import org.apache.oltu.oauth2.common.parameters.FragmentParametersApplier;
import org.apache.oltu.oauth2.common.parameters.JSONBodyParametersApplier;
import org.apache.oltu.oauth2.common.parameters.OAuthParametersApplier;
import org.apache.oltu.oauth2.common.parameters.QueryParameterApplier;
import org.apache.oltu.oauth2.common.parameters.WWWAuthHeaderParametersApplier;

public class OAuthResponse implements OAuthMessage {
    protected String body;
    protected Map<String, String> headers = new HashMap();
    protected int responseStatus;
    protected String uri;

    protected OAuthResponse(String uri2, int responseStatus2) {
        this.uri = uri2;
        this.responseStatus = responseStatus2;
    }

    public static OAuthResponseBuilder status(int code) {
        return new OAuthResponseBuilder(code);
    }

    public static OAuthErrorResponseBuilder errorResponse(int code) {
        return new OAuthErrorResponseBuilder(code);
    }

    public String getLocationUri() {
        return this.uri;
    }

    public void setLocationUri(String uri2) {
        this.uri = uri2;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body2) {
        this.body = body2;
    }

    public String getHeader(String name) {
        return this.headers.get(name);
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public void setHeaders(Map<String, String> headers2) {
        this.headers = headers2;
    }

    public int getResponseStatus() {
        return this.responseStatus;
    }

    public void addHeader(String name, String header) {
        this.headers.put(name, header);
    }

    public static class OAuthResponseBuilder {
        protected OAuthParametersApplier applier;
        protected String location;
        protected Map<String, Object> parameters = new HashMap();
        protected int responseCode;

        public OAuthResponseBuilder(int responseCode2) {
            this.responseCode = responseCode2;
        }

        public OAuthResponseBuilder location(String location2) {
            this.location = location2;
            return this;
        }

        public OAuthResponseBuilder setScope(String value) {
            this.parameters.put(OAuth.OAUTH_SCOPE, value);
            return this;
        }

        public OAuthResponseBuilder setParam(String key, String value) {
            this.parameters.put(key, value);
            return this;
        }

        public OAuthResponse buildQueryMessage() throws OAuthSystemException {
            OAuthResponse msg = new OAuthResponse(this.location, this.responseCode);
            this.applier = new QueryParameterApplier();
            if (this.parameters.containsKey("access_token")) {
                this.applier = new FragmentParametersApplier();
            } else {
                this.applier = new QueryParameterApplier();
            }
            return (OAuthResponse) this.applier.applyOAuthParameters(msg, this.parameters);
        }

        public OAuthResponse buildBodyMessage() throws OAuthSystemException {
            OAuthResponse msg = new OAuthResponse(this.location, this.responseCode);
            this.applier = new BodyURLEncodedParametersApplier();
            return (OAuthResponse) this.applier.applyOAuthParameters(msg, this.parameters);
        }

        public OAuthResponse buildJSONMessage() throws OAuthSystemException {
            OAuthResponse msg = new OAuthResponse(this.location, this.responseCode);
            this.applier = new JSONBodyParametersApplier();
            return (OAuthResponse) this.applier.applyOAuthParameters(msg, this.parameters);
        }

        public OAuthResponse buildHeaderMessage() throws OAuthSystemException {
            OAuthResponse msg = new OAuthResponse(this.location, this.responseCode);
            this.applier = new WWWAuthHeaderParametersApplier();
            return (OAuthResponse) this.applier.applyOAuthParameters(msg, this.parameters);
        }
    }

    public static class OAuthErrorResponseBuilder extends OAuthResponseBuilder {
        public OAuthErrorResponseBuilder(int responseCode) {
            super(responseCode);
        }

        public OAuthErrorResponseBuilder error(OAuthProblemException ex) {
            this.parameters.put("error", ex.getError());
            this.parameters.put(OAuthError.OAUTH_ERROR_DESCRIPTION, ex.getDescription());
            this.parameters.put(OAuthError.OAUTH_ERROR_URI, ex.getUri());
            this.parameters.put(OAuth.OAUTH_STATE, ex.getState());
            return this;
        }

        public OAuthErrorResponseBuilder setError(String error) {
            this.parameters.put("error", error);
            return this;
        }

        public OAuthErrorResponseBuilder setErrorDescription(String desc) {
            this.parameters.put(OAuthError.OAUTH_ERROR_DESCRIPTION, desc);
            return this;
        }

        public OAuthErrorResponseBuilder setErrorUri(String state) {
            this.parameters.put(OAuthError.OAUTH_ERROR_URI, state);
            return this;
        }

        public OAuthErrorResponseBuilder setState(String state) {
            this.parameters.put(OAuth.OAUTH_STATE, state);
            return this;
        }

        public OAuthErrorResponseBuilder setRealm(String realm) {
            this.parameters.put(OAuth.WWWAuthHeader.REALM, realm);
            return this;
        }

        public OAuthErrorResponseBuilder location(String location) {
            this.location = location;
            return this;
        }
    }
}
