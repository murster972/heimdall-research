package org.apache.oltu.oauth2.common.message.types;

import org.apache.oltu.oauth2.common.OAuth;

public enum ResponseType {
    CODE(OAuth.OAUTH_CODE),
    TOKEN("token");
    
    private String code;

    private ResponseType(String code2) {
        this.code = code2;
    }

    public String toString() {
        return this.code;
    }
}
