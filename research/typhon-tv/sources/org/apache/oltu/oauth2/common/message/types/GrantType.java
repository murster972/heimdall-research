package org.apache.oltu.oauth2.common.message.types;

import org.apache.oltu.oauth2.common.OAuth;

public enum GrantType {
    AUTHORIZATION_CODE("authorization_code"),
    PASSWORD(OAuth.OAUTH_PASSWORD),
    REFRESH_TOKEN(OAuth.OAUTH_REFRESH_TOKEN),
    CLIENT_CREDENTIALS("client_credentials");
    
    private String grantType;

    private GrantType(String grantType2) {
        this.grantType = grantType2;
    }

    public String toString() {
        return this.grantType;
    }
}
