package org.apache.commons.logging.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Hashtable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogConfigurationException;
import org.apache.commons.logging.LogFactory;

public class LogFactoryImpl extends LogFactory {
    public static final String ALLOW_FLAWED_CONTEXT_PROPERTY = "org.apache.commons.logging.Log.allowFlawedContext";
    public static final String ALLOW_FLAWED_DISCOVERY_PROPERTY = "org.apache.commons.logging.Log.allowFlawedDiscovery";
    public static final String ALLOW_FLAWED_HIERARCHY_PROPERTY = "org.apache.commons.logging.Log.allowFlawedHierarchy";
    private static final String LOGGING_IMPL_JDK14_LOGGER = "org.apache.commons.logging.impl.Jdk14Logger";
    private static final String LOGGING_IMPL_LOG4J_LOGGER = "org.apache.commons.logging.impl.Log4JLogger";
    private static final String LOGGING_IMPL_LUMBERJACK_LOGGER = "org.apache.commons.logging.impl.Jdk13LumberjackLogger";
    private static final String LOGGING_IMPL_SIMPLE_LOGGER = "org.apache.commons.logging.impl.SimpleLog";
    public static final String LOG_PROPERTY = "org.apache.commons.logging.Log";
    protected static final String LOG_PROPERTY_OLD = "org.apache.commons.logging.log";
    private static final String PKG_IMPL = "org.apache.commons.logging.impl.";
    private static final int PKG_LEN = PKG_IMPL.length();
    static Class class$java$lang$String;
    static Class class$org$apache$commons$logging$Log;
    static Class class$org$apache$commons$logging$LogFactory;
    static Class class$org$apache$commons$logging$impl$LogFactoryImpl;
    private static final String[] classesToDiscover = {LOGGING_IMPL_LOG4J_LOGGER, LOGGING_IMPL_JDK14_LOGGER, LOGGING_IMPL_LUMBERJACK_LOGGER, LOGGING_IMPL_SIMPLE_LOGGER};
    private boolean allowFlawedContext;
    private boolean allowFlawedDiscovery;
    private boolean allowFlawedHierarchy;
    protected Hashtable attributes = new Hashtable();
    private String diagnosticPrefix;
    protected Hashtable instances = new Hashtable();
    private String logClassName;
    protected Constructor logConstructor = null;
    protected Class[] logConstructorSignature;
    protected Method logMethod;
    protected Class[] logMethodSignature;
    private boolean useTCCL = true;

    static ClassLoader access$000() throws LogConfigurationException {
        return directGetContextClassLoader();
    }

    public LogFactoryImpl() {
        Class cls;
        Class cls2;
        Class[] clsArr = new Class[1];
        if (class$java$lang$String == null) {
            cls = class$("java.lang.String");
            class$java$lang$String = cls;
        } else {
            cls = class$java$lang$String;
        }
        clsArr[0] = cls;
        this.logConstructorSignature = clsArr;
        this.logMethod = null;
        Class[] clsArr2 = new Class[1];
        if (class$org$apache$commons$logging$LogFactory == null) {
            cls2 = class$("org.apache.commons.logging.LogFactory");
            class$org$apache$commons$logging$LogFactory = cls2;
        } else {
            cls2 = class$org$apache$commons$logging$LogFactory;
        }
        clsArr2[0] = cls2;
        this.logMethodSignature = clsArr2;
        initDiagnostics();
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Instance created.");
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public Object getAttribute(String name) {
        return this.attributes.get(name);
    }

    public String[] getAttributeNames() {
        return (String[]) this.attributes.keySet().toArray(new String[this.attributes.size()]);
    }

    public Log getInstance(Class clazz) throws LogConfigurationException {
        return getInstance(clazz.getName());
    }

    public Log getInstance(String name) throws LogConfigurationException {
        Log instance = (Log) this.instances.get(name);
        if (instance != null) {
            return instance;
        }
        Log instance2 = newInstance(name);
        this.instances.put(name, instance2);
        return instance2;
    }

    public void release() {
        logDiagnostic("Releasing all known loggers");
        this.instances.clear();
    }

    public void removeAttribute(String name) {
        this.attributes.remove(name);
    }

    public void setAttribute(String name, Object value) {
        if (this.logConstructor != null) {
            logDiagnostic("setAttribute: call too late; configuration already performed.");
        }
        if (value == null) {
            this.attributes.remove(name);
        } else {
            this.attributes.put(name, value);
        }
        if (name.equals("use_tccl")) {
            this.useTCCL = value != null && Boolean.valueOf(value.toString()).booleanValue();
        }
    }

    protected static ClassLoader getContextClassLoader() throws LogConfigurationException {
        return LogFactory.getContextClassLoader();
    }

    protected static boolean isDiagnosticsEnabled() {
        return LogFactory.isDiagnosticsEnabled();
    }

    protected static ClassLoader getClassLoader(Class clazz) {
        return LogFactory.getClassLoader(clazz);
    }

    private void initDiagnostics() {
        String classLoaderName;
        ClassLoader classLoader = getClassLoader(getClass());
        if (classLoader == null) {
            classLoaderName = "BOOTLOADER";
        } else {
            try {
                classLoaderName = objectId(classLoader);
            } catch (SecurityException e) {
                classLoaderName = "UNKNOWN";
            }
        }
        this.diagnosticPrefix = new StringBuffer().append("[LogFactoryImpl@").append(System.identityHashCode(this)).append(" from ").append(classLoaderName).append("] ").toString();
    }

    /* access modifiers changed from: protected */
    public void logDiagnostic(String msg) {
        if (isDiagnosticsEnabled()) {
            logRawDiagnostic(new StringBuffer().append(this.diagnosticPrefix).append(msg).toString());
        }
    }

    /* access modifiers changed from: protected */
    public String getLogClassName() {
        if (this.logClassName == null) {
            discoverLogImplementation(getClass().getName());
        }
        return this.logClassName;
    }

    /* access modifiers changed from: protected */
    public Constructor getLogConstructor() throws LogConfigurationException {
        if (this.logConstructor == null) {
            discoverLogImplementation(getClass().getName());
        }
        return this.logConstructor;
    }

    /* access modifiers changed from: protected */
    public boolean isJdk13LumberjackAvailable() {
        return isLogLibraryAvailable("Jdk13Lumberjack", LOGGING_IMPL_LUMBERJACK_LOGGER);
    }

    /* access modifiers changed from: protected */
    public boolean isJdk14Available() {
        return isLogLibraryAvailable("Jdk14", LOGGING_IMPL_JDK14_LOGGER);
    }

    /* access modifiers changed from: protected */
    public boolean isLog4JAvailable() {
        return isLogLibraryAvailable("Log4J", LOGGING_IMPL_LOG4J_LOGGER);
    }

    /* JADX WARNING: type inference failed for: r0v0, types: [java.lang.Throwable] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.logging.Log newInstance(java.lang.String r8) throws org.apache.commons.logging.LogConfigurationException {
        /*
            r7 = this;
            java.lang.reflect.Constructor r6 = r7.logConstructor     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            if (r6 != 0) goto L_0x0018
            org.apache.commons.logging.Log r2 = r7.discoverLogImplementation(r8)     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
        L_0x0008:
            java.lang.reflect.Method r6 = r7.logMethod     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            if (r6 == 0) goto L_0x0017
            r6 = 1
            java.lang.Object[] r4 = new java.lang.Object[r6]     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            r6 = 0
            r4[r6] = r7     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            java.lang.reflect.Method r6 = r7.logMethod     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            r6.invoke(r2, r4)     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
        L_0x0017:
            return r2
        L_0x0018:
            r6 = 1
            java.lang.Object[] r4 = new java.lang.Object[r6]     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            r6 = 0
            r4[r6] = r8     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            java.lang.reflect.Constructor r6 = r7.logConstructor     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            java.lang.Object r2 = r6.newInstance(r4)     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            org.apache.commons.logging.Log r2 = (org.apache.commons.logging.Log) r2     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            goto L_0x0008
        L_0x0027:
            r3 = move-exception
            throw r3
        L_0x0029:
            r1 = move-exception
            java.lang.Throwable r0 = r1.getTargetException()
            org.apache.commons.logging.LogConfigurationException r6 = new org.apache.commons.logging.LogConfigurationException
            if (r0 != 0) goto L_0x0036
        L_0x0032:
            r6.<init>((java.lang.Throwable) r1)
            throw r6
        L_0x0036:
            r1 = r0
            goto L_0x0032
        L_0x0038:
            r5 = move-exception
            handleThrowable(r5)
            org.apache.commons.logging.LogConfigurationException r6 = new org.apache.commons.logging.LogConfigurationException
            r6.<init>((java.lang.Throwable) r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.impl.LogFactoryImpl.newInstance(java.lang.String):org.apache.commons.logging.Log");
    }

    private static ClassLoader getContextClassLoaderInternal() throws LogConfigurationException {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                return LogFactoryImpl.access$000();
            }
        });
    }

    private static String getSystemProperty(String key, String def) throws SecurityException {
        return (String) AccessController.doPrivileged(new PrivilegedAction(key, def) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f16645;

            /* renamed from: 龘  reason: contains not printable characters */
            private final String f16646;

            {
                this.f16646 = r1;
                this.f16645 = r2;
            }

            public Object run() {
                return System.getProperty(this.f16646, this.f16645);
            }
        });
    }

    private ClassLoader getParentClassLoader(ClassLoader cl) {
        try {
            return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction(this, cl) {

                /* renamed from: 靐  reason: contains not printable characters */
                private final LogFactoryImpl f16647;

                /* renamed from: 龘  reason: contains not printable characters */
                private final ClassLoader f16648;

                {
                    this.f16647 = r1;
                    this.f16648 = r2;
                }

                public Object run() {
                    return this.f16648.getParent();
                }
            });
        } catch (SecurityException e) {
            logDiagnostic("[SECURITY] Unable to obtain parent classloader");
            return null;
        }
    }

    private boolean isLogLibraryAvailable(String name, String classname) {
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Checking for '").append(name).append("'.").toString());
        }
        try {
            if (createLogFromClass(classname, getClass().getName(), false) != null) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("Found '").append(name).append("'.").toString());
                }
                return true;
            } else if (!isDiagnosticsEnabled()) {
                return false;
            } else {
                logDiagnostic(new StringBuffer().append("Did not find '").append(name).append("'.").toString());
                return false;
            }
        } catch (LogConfigurationException e) {
            if (!isDiagnosticsEnabled()) {
                return false;
            }
            logDiagnostic(new StringBuffer().append("Logging system '").append(name).append("' is available but not useable.").toString());
            return false;
        }
    }

    private String getConfigurationValue(String property) {
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("[ENV] Trying to get configuration for item ").append(property).toString());
        }
        Object valueObj = getAttribute(property);
        if (valueObj != null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("[ENV] Found LogFactory attribute [").append(valueObj).append("] for ").append(property).toString());
            }
            return valueObj.toString();
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("[ENV] No LogFactory attribute found for ").append(property).toString());
        }
        try {
            String value = getSystemProperty(property, (String) null);
            if (value == null) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("[ENV] No system property found for property ").append(property).toString());
                }
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("[ENV] No configuration defined for item ").append(property).toString());
                }
                return null;
            } else if (!isDiagnosticsEnabled()) {
                return value;
            } else {
                logDiagnostic(new StringBuffer().append("[ENV] Found system property [").append(value).append("] for ").append(property).toString());
                return value;
            }
        } catch (SecurityException e) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("[ENV] Security prevented reading system property ").append(property).toString());
            }
        }
    }

    private boolean getBooleanConfiguration(String key, boolean dflt) {
        String val = getConfigurationValue(key);
        return val == null ? dflt : Boolean.valueOf(val).booleanValue();
    }

    private void initConfiguration() {
        this.allowFlawedContext = getBooleanConfiguration("org.apache.commons.logging.Log.allowFlawedContext", true);
        this.allowFlawedDiscovery = getBooleanConfiguration("org.apache.commons.logging.Log.allowFlawedDiscovery", true);
        this.allowFlawedHierarchy = getBooleanConfiguration("org.apache.commons.logging.Log.allowFlawedHierarchy", true);
    }

    private Log discoverLogImplementation(String logCategory) throws LogConfigurationException {
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Discovering a Log implementation...");
        }
        initConfiguration();
        Log result = null;
        String specifiedLogClassName = findUserSpecifiedLogClassName();
        if (specifiedLogClassName != null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("Attempting to load user-specified log class '").append(specifiedLogClassName).append("'...").toString());
            }
            Log result2 = createLogFromClass(specifiedLogClassName, logCategory, true);
            if (result2 == null) {
                StringBuffer messageBuffer = new StringBuffer("User-specified log class '");
                messageBuffer.append(specifiedLogClassName);
                messageBuffer.append("' cannot be found or is not useable.");
                informUponSimilarName(messageBuffer, specifiedLogClassName, LOGGING_IMPL_LOG4J_LOGGER);
                informUponSimilarName(messageBuffer, specifiedLogClassName, LOGGING_IMPL_JDK14_LOGGER);
                informUponSimilarName(messageBuffer, specifiedLogClassName, LOGGING_IMPL_LUMBERJACK_LOGGER);
                informUponSimilarName(messageBuffer, specifiedLogClassName, LOGGING_IMPL_SIMPLE_LOGGER);
                throw new LogConfigurationException(messageBuffer.toString());
            }
            Log log = result2;
            return result2;
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic("No user-specified Log implementation; performing discovery using the standard supported logging implementations...");
        }
        for (int i = 0; i < classesToDiscover.length && result == null; i++) {
            result = createLogFromClass(classesToDiscover[i], logCategory, true);
        }
        if (result == null) {
            throw new LogConfigurationException("No suitable Log implementation");
        }
        Log log2 = result;
        return result;
    }

    private void informUponSimilarName(StringBuffer messageBuffer, String name, String candidate) {
        if (!name.equals(candidate)) {
            if (name.regionMatches(true, 0, candidate, 0, PKG_LEN + 5)) {
                messageBuffer.append(" Did you mean '");
                messageBuffer.append(candidate);
                messageBuffer.append("'?");
            }
        }
    }

    private String findUserSpecifiedLogClassName() {
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Trying to get log class from attribute 'org.apache.commons.logging.Log'");
        }
        String specifiedClass = (String) getAttribute("org.apache.commons.logging.Log");
        if (specifiedClass == null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("Trying to get log class from attribute 'org.apache.commons.logging.log'");
            }
            specifiedClass = (String) getAttribute(LOG_PROPERTY_OLD);
        }
        if (specifiedClass == null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("Trying to get log class from system property 'org.apache.commons.logging.Log'");
            }
            try {
                specifiedClass = getSystemProperty("org.apache.commons.logging.Log", (String) null);
            } catch (SecurityException e) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("No access allowed to system property 'org.apache.commons.logging.Log' - ").append(e.getMessage()).toString());
                }
            }
        }
        if (specifiedClass == null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("Trying to get log class from system property 'org.apache.commons.logging.log'");
            }
            try {
                specifiedClass = getSystemProperty(LOG_PROPERTY_OLD, (String) null);
            } catch (SecurityException e2) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("No access allowed to system property 'org.apache.commons.logging.log' - ").append(e2.getMessage()).toString());
                }
            }
        }
        if (specifiedClass != null) {
            return specifiedClass.trim();
        }
        return specifiedClass;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v31, resolved type: org.apache.commons.logging.Log} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.commons.logging.Log createLogFromClass(java.lang.String r21, java.lang.String r22, boolean r23) throws org.apache.commons.logging.LogConfigurationException {
        /*
            r20 = this;
            boolean r17 = isDiagnosticsEnabled()
            if (r17 == 0) goto L_0x002c
            java.lang.StringBuffer r17 = new java.lang.StringBuffer
            r17.<init>()
            java.lang.String r18 = "Attempting to instantiate '"
            java.lang.StringBuffer r17 = r17.append(r18)
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)
            java.lang.String r18 = "'"
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r17 = r17.toString()
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)
        L_0x002c:
            r17 = 1
            r0 = r17
            java.lang.Object[] r12 = new java.lang.Object[r0]
            r17 = 0
            r12[r17] = r22
            r7 = 0
            r4 = 0
            r8 = 0
            java.lang.ClassLoader r5 = r20.getBaseClassLoader()
        L_0x003d:
            java.lang.StringBuffer r17 = new java.lang.StringBuffer
            r17.<init>()
            java.lang.String r18 = "Trying to load '"
            java.lang.StringBuffer r17 = r17.append(r18)
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)
            java.lang.String r18 = "' from classloader "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = objectId(r5)
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r17 = r17.toString()
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)
            boolean r17 = isDiagnosticsEnabled()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            if (r17 == 0) goto L_0x00ce
            java.lang.StringBuffer r17 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17.<init>()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r18 = 46
            r19 = 47
            r0 = r21
            r1 = r18
            r2 = r19
            java.lang.String r18 = r0.replace(r1, r2)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = ".class"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r13 = r17.toString()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            if (r5 == 0) goto L_0x0178
            java.net.URL r16 = r5.getResource(r13)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
        L_0x0099:
            if (r16 != 0) goto L_0x0194
            java.lang.StringBuffer r17 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17.<init>()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "Class '"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "' ["
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r17
            java.lang.StringBuffer r17 = r0.append(r13)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "] cannot be found."
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r17 = r17.toString()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
        L_0x00ce:
            r17 = 1
            r0 = r21
            r1 = r17
            java.lang.Class r3 = java.lang.Class.forName(r0, r1, r5)     // Catch:{ ClassNotFoundException -> 0x020f }
        L_0x00d8:
            r0 = r20
            java.lang.Class[] r0 = r0.logConstructorSignature     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17 = r0
            r0 = r17
            java.lang.reflect.Constructor r4 = r3.getConstructor(r0)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.Object r10 = r4.newInstance(r12)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            boolean r0 = r10 instanceof org.apache.commons.logging.Log     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17 = r0
            if (r17 == 0) goto L_0x02d0
            r8 = r3
            r0 = r10
            org.apache.commons.logging.Log r0 = (org.apache.commons.logging.Log) r0     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r7 = r0
        L_0x00f3:
            if (r8 == 0) goto L_0x0177
            if (r23 == 0) goto L_0x0177
            r0 = r21
            r1 = r20
            r1.logClassName = r0
            r0 = r20
            r0.logConstructor = r4
            java.lang.String r17 = "setLogFactory"
            r0 = r20
            java.lang.Class[] r0 = r0.logMethodSignature     // Catch:{ Throwable -> 0x02ed }
            r18 = r0
            r0 = r17
            r1 = r18
            java.lang.reflect.Method r17 = r8.getMethod(r0, r1)     // Catch:{ Throwable -> 0x02ed }
            r0 = r17
            r1 = r20
            r1.logMethod = r0     // Catch:{ Throwable -> 0x02ed }
            java.lang.StringBuffer r17 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x02ed }
            r17.<init>()     // Catch:{ Throwable -> 0x02ed }
            java.lang.String r18 = "Found method setLogFactory(LogFactory) in '"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ Throwable -> 0x02ed }
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)     // Catch:{ Throwable -> 0x02ed }
            java.lang.String r18 = "'"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ Throwable -> 0x02ed }
            java.lang.String r17 = r17.toString()     // Catch:{ Throwable -> 0x02ed }
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)     // Catch:{ Throwable -> 0x02ed }
        L_0x013e:
            java.lang.StringBuffer r17 = new java.lang.StringBuffer
            r17.<init>()
            java.lang.String r18 = "Log adapter '"
            java.lang.StringBuffer r17 = r17.append(r18)
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)
            java.lang.String r18 = "' from classloader "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.ClassLoader r18 = r8.getClassLoader()
            java.lang.String r18 = objectId(r18)
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = " has been selected for use."
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r17 = r17.toString()
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)
        L_0x0177:
            return r7
        L_0x0178:
            java.lang.StringBuffer r17 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17.<init>()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r17
            java.lang.StringBuffer r17 = r0.append(r13)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = ".class"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r17 = r17.toString()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.net.URL r16 = java.lang.ClassLoader.getSystemResource(r17)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            goto L_0x0099
        L_0x0194:
            java.lang.StringBuffer r17 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17.<init>()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "Class '"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "' was found at '"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r17
            r1 = r16
            java.lang.StringBuffer r17 = r0.append(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "'"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r17 = r17.toString()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            goto L_0x00ce
        L_0x01cb:
            r6 = move-exception
            java.lang.String r9 = r6.getMessage()
            java.lang.StringBuffer r17 = new java.lang.StringBuffer
            r17.<init>()
            java.lang.String r18 = "The log adapter '"
            java.lang.StringBuffer r17 = r17.append(r18)
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)
            java.lang.String r18 = "' is missing dependencies when loaded via classloader "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = objectId(r5)
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = ": "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = r9.trim()
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r17 = r17.toString()
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)
            goto L_0x00f3
        L_0x020f:
            r11 = move-exception
            java.lang.String r9 = r11.getMessage()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.StringBuffer r17 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17.<init>()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "The log adapter '"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "' is not available via classloader "
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = objectId(r5)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = ": "
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = r9.trim()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r17 = r17.toString()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.Class r3 = java.lang.Class.forName(r21)     // Catch:{ ClassNotFoundException -> 0x0257 }
            goto L_0x00d8
        L_0x0257:
            r14 = move-exception
            java.lang.String r9 = r14.getMessage()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.StringBuffer r17 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r17.<init>()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "The log adapter '"
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = "' is not available via the LogFactoryImpl class classloader: "
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r18 = r9.trim()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.StringBuffer r17 = r17.append(r18)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            java.lang.String r17 = r17.toString()     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
            goto L_0x00f3
        L_0x028c:
            r6 = move-exception
            java.lang.String r9 = r6.getMessage()
            java.lang.StringBuffer r17 = new java.lang.StringBuffer
            r17.<init>()
            java.lang.String r18 = "The log adapter '"
            java.lang.StringBuffer r17 = r17.append(r18)
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)
            java.lang.String r18 = "' is unable to initialize itself when loaded via classloader "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = objectId(r5)
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = ": "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = r9.trim()
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r17 = r17.toString()
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)
            goto L_0x00f3
        L_0x02d0:
            r0 = r20
            r0.handleFlawedHierarchy(r5, r3)     // Catch:{ NoClassDefFoundError -> 0x01cb, ExceptionInInitializerError -> 0x028c, LogConfigurationException -> 0x02df, Throwable -> 0x02e1 }
        L_0x02d5:
            if (r5 == 0) goto L_0x00f3
            r0 = r20
            java.lang.ClassLoader r5 = r0.getParentClassLoader(r5)
            goto L_0x003d
        L_0x02df:
            r6 = move-exception
            throw r6
        L_0x02e1:
            r15 = move-exception
            handleThrowable(r15)
            r0 = r20
            r1 = r21
            r0.handleFlawedDiscovery(r1, r5, r15)
            goto L_0x02d5
        L_0x02ed:
            r15 = move-exception
            handleThrowable(r15)
            r17 = 0
            r0 = r17
            r1 = r20
            r1.logMethod = r0
            java.lang.StringBuffer r17 = new java.lang.StringBuffer
            r17.<init>()
            java.lang.String r18 = "[INFO] '"
            java.lang.StringBuffer r17 = r17.append(r18)
            r0 = r17
            r1 = r21
            java.lang.StringBuffer r17 = r0.append(r1)
            java.lang.String r18 = "' from classloader "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = objectId(r5)
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = " does not declare optional method "
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r18 = "setLogFactory(LogFactory)"
            java.lang.StringBuffer r17 = r17.append(r18)
            java.lang.String r17 = r17.toString()
            r0 = r20
            r1 = r17
            r0.logDiagnostic(r1)
            goto L_0x013e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.impl.LogFactoryImpl.createLogFromClass(java.lang.String, java.lang.String, boolean):org.apache.commons.logging.Log");
    }

    private ClassLoader getBaseClassLoader() throws LogConfigurationException {
        Class cls;
        if (class$org$apache$commons$logging$impl$LogFactoryImpl == null) {
            cls = class$("org.apache.commons.logging.impl.LogFactoryImpl");
            class$org$apache$commons$logging$impl$LogFactoryImpl = cls;
        } else {
            cls = class$org$apache$commons$logging$impl$LogFactoryImpl;
        }
        ClassLoader thisClassLoader = getClassLoader(cls);
        if (!this.useTCCL) {
            return thisClassLoader;
        }
        ClassLoader contextClassLoader = getContextClassLoaderInternal();
        ClassLoader baseClassLoader = getLowestClassLoader(contextClassLoader, thisClassLoader);
        if (baseClassLoader != null) {
            if (baseClassLoader != contextClassLoader) {
                if (!this.allowFlawedContext) {
                    throw new LogConfigurationException("Bad classloader hierarchy; LogFactoryImpl was loaded via a classloader that is not related to the current context classloader.");
                } else if (isDiagnosticsEnabled()) {
                    logDiagnostic("Warning: the context classloader is an ancestor of the classloader that loaded LogFactoryImpl; it should be the same or a descendant. The application using commons-logging should ensure the context classloader is used correctly.");
                }
            }
            return baseClassLoader;
        } else if (this.allowFlawedContext) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("[WARNING] the context classloader is not part of a parent-child relationship with the classloader that loaded LogFactoryImpl.");
            }
            return contextClassLoader;
        } else {
            throw new LogConfigurationException("Bad classloader hierarchy; LogFactoryImpl was loaded via a classloader that is not related to the current context classloader.");
        }
    }

    private ClassLoader getLowestClassLoader(ClassLoader c1, ClassLoader c2) {
        if (c1 == null) {
            return c2;
        }
        if (c2 == null) {
            return c1;
        }
        ClassLoader current = c1;
        while (current != null) {
            if (current == c2) {
                return c1;
            }
            current = getParentClassLoader(current);
        }
        ClassLoader current2 = c2;
        while (current2 != null) {
            if (current2 == c1) {
                return c2;
            }
            current2 = getParentClassLoader(current2);
        }
        return null;
    }

    private void handleFlawedDiscovery(String logAdapterClassName, ClassLoader classLoader, Throwable discoveryFlaw) {
        Throwable cause;
        Throwable cause2;
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Could not instantiate Log '").append(logAdapterClassName).append("' -- ").append(discoveryFlaw.getClass().getName()).append(": ").append(discoveryFlaw.getLocalizedMessage()).toString());
            if ((discoveryFlaw instanceof InvocationTargetException) && (cause = ((InvocationTargetException) discoveryFlaw).getTargetException()) != null) {
                logDiagnostic(new StringBuffer().append("... InvocationTargetException: ").append(cause.getClass().getName()).append(": ").append(cause.getLocalizedMessage()).toString());
                if ((cause instanceof ExceptionInInitializerError) && (cause2 = ((ExceptionInInitializerError) cause).getException()) != null) {
                    StringWriter sw = new StringWriter();
                    cause2.printStackTrace(new PrintWriter(sw, true));
                    logDiagnostic(new StringBuffer().append("... ExceptionInInitializerError: ").append(sw.toString()).toString());
                }
            }
        }
        if (!this.allowFlawedDiscovery) {
            throw new LogConfigurationException(discoveryFlaw);
        }
    }

    private void handleFlawedHierarchy(ClassLoader badClassLoader, Class badClass) throws LogConfigurationException {
        Class cls;
        Class cls2;
        Class cls3;
        Class cls4;
        boolean implementsLog = false;
        if (class$org$apache$commons$logging$Log == null) {
            cls = class$("org.apache.commons.logging.Log");
            class$org$apache$commons$logging$Log = cls;
        } else {
            cls = class$org$apache$commons$logging$Log;
        }
        String logInterfaceName = cls.getName();
        Class[] interfaces = badClass.getInterfaces();
        int i = 0;
        while (true) {
            if (i >= interfaces.length) {
                break;
            } else if (logInterfaceName.equals(interfaces[i].getName())) {
                implementsLog = true;
                break;
            } else {
                i++;
            }
        }
        if (implementsLog) {
            if (isDiagnosticsEnabled()) {
                try {
                    if (class$org$apache$commons$logging$Log == null) {
                        cls4 = class$("org.apache.commons.logging.Log");
                        class$org$apache$commons$logging$Log = cls4;
                    } else {
                        cls4 = class$org$apache$commons$logging$Log;
                    }
                    logDiagnostic(new StringBuffer().append("Class '").append(badClass.getName()).append("' was found in classloader ").append(objectId(badClassLoader)).append(". It is bound to a Log interface which is not").append(" the one loaded from classloader ").append(objectId(getClassLoader(cls4))).toString());
                } catch (Throwable t) {
                    handleThrowable(t);
                    logDiagnostic(new StringBuffer().append("Error while trying to output diagnostics about bad class '").append(badClass).append("'").toString());
                }
            }
            if (!this.allowFlawedHierarchy) {
                StringBuffer msg = new StringBuffer();
                msg.append("Terminating logging for this context ");
                msg.append("due to bad log hierarchy. ");
                msg.append("You have more than one version of '");
                if (class$org$apache$commons$logging$Log == null) {
                    cls3 = class$("org.apache.commons.logging.Log");
                    class$org$apache$commons$logging$Log = cls3;
                } else {
                    cls3 = class$org$apache$commons$logging$Log;
                }
                msg.append(cls3.getName());
                msg.append("' visible.");
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(msg.toString());
                }
                throw new LogConfigurationException(msg.toString());
            } else if (isDiagnosticsEnabled()) {
                StringBuffer msg2 = new StringBuffer();
                msg2.append("Warning: bad log hierarchy. ");
                msg2.append("You have more than one version of '");
                if (class$org$apache$commons$logging$Log == null) {
                    cls2 = class$("org.apache.commons.logging.Log");
                    class$org$apache$commons$logging$Log = cls2;
                } else {
                    cls2 = class$org$apache$commons$logging$Log;
                }
                msg2.append(cls2.getName());
                msg2.append("' visible.");
                logDiagnostic(msg2.toString());
            }
        } else if (!this.allowFlawedDiscovery) {
            StringBuffer msg3 = new StringBuffer();
            msg3.append("Terminating logging for this context. ");
            msg3.append("Log class '");
            msg3.append(badClass.getName());
            msg3.append("' does not implement the Log interface.");
            if (isDiagnosticsEnabled()) {
                logDiagnostic(msg3.toString());
            }
            throw new LogConfigurationException(msg3.toString());
        } else if (isDiagnosticsEnabled()) {
            StringBuffer msg4 = new StringBuffer();
            msg4.append("[WARNING] Log class '");
            msg4.append(badClass.getName());
            msg4.append("' does not implement the Log interface.");
            logDiagnostic(msg4.toString());
        }
    }
}
