package org.apache.commons.logging.impl;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class WeakHashtable extends Hashtable {
    private static final int MAX_CHANGES_BEFORE_PURGE = 100;
    private static final int PARTIAL_PURGE_COUNT = 10;
    private static final long serialVersionUID = -1546036869799732453L;
    private int changeCount = 0;
    private final ReferenceQueue queue = new ReferenceQueue();

    public boolean containsKey(Object key) {
        return super.containsKey(new Referenced(key, (AnonymousClass1) null));
    }

    public Enumeration elements() {
        purge();
        return super.elements();
    }

    public Set entrySet() {
        purge();
        Set<Map.Entry> referencedEntries = super.entrySet();
        Set unreferencedEntries = new HashSet();
        for (Map.Entry entry : referencedEntries) {
            Object key = Referenced.m20800((Referenced) entry.getKey());
            Object value = entry.getValue();
            if (key != null) {
                unreferencedEntries.add(new Entry(key, value, (AnonymousClass1) null));
            }
        }
        return unreferencedEntries;
    }

    public Object get(Object key) {
        return super.get(new Referenced(key, (AnonymousClass1) null));
    }

    public Enumeration keys() {
        purge();
        return new Enumeration(this, super.keys()) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final WeakHashtable f16650;

            /* renamed from: 龘  reason: contains not printable characters */
            private final Enumeration f16651;

            {
                this.f16650 = r1;
                this.f16651 = r2;
            }

            public boolean hasMoreElements() {
                return this.f16651.hasMoreElements();
            }

            public Object nextElement() {
                return Referenced.m20800((Referenced) this.f16651.nextElement());
            }
        };
    }

    public Set keySet() {
        purge();
        Set<Referenced> referencedKeys = super.keySet();
        Set unreferencedKeys = new HashSet();
        for (Referenced referenceKey : referencedKeys) {
            Object keyValue = Referenced.m20800(referenceKey);
            if (keyValue != null) {
                unreferencedKeys.add(keyValue);
            }
        }
        return unreferencedKeys;
    }

    public synchronized Object put(Object key, Object value) {
        if (key == null) {
            throw new NullPointerException("Null keys are not allowed");
        } else if (value == null) {
            throw new NullPointerException("Null values are not allowed");
        } else {
            int i = this.changeCount;
            this.changeCount = i + 1;
            if (i > 100) {
                purge();
                this.changeCount = 0;
            } else if (this.changeCount % 10 == 0) {
                purgeOne();
            }
        }
        return super.put(new Referenced(key, this.queue, (AnonymousClass1) null), value);
    }

    public void putAll(Map t) {
        if (t != null) {
            for (Map.Entry entry : t.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    public Collection values() {
        purge();
        return super.values();
    }

    public synchronized Object remove(Object key) {
        int i = this.changeCount;
        this.changeCount = i + 1;
        if (i > 100) {
            purge();
            this.changeCount = 0;
        } else if (this.changeCount % 10 == 0) {
            purgeOne();
        }
        return super.remove(new Referenced(key, (AnonymousClass1) null));
    }

    public boolean isEmpty() {
        purge();
        return super.isEmpty();
    }

    public int size() {
        purge();
        return super.size();
    }

    public String toString() {
        purge();
        return super.toString();
    }

    /* access modifiers changed from: protected */
    public void rehash() {
        purge();
        super.rehash();
    }

    private void purge() {
        List toRemove = new ArrayList();
        synchronized (this.queue) {
            while (true) {
                WeakKey key = (WeakKey) this.queue.poll();
                if (key == null) {
                    break;
                }
                toRemove.add(WeakKey.m20802(key));
            }
        }
        int size = toRemove.size();
        for (int i = 0; i < size; i++) {
            super.remove(toRemove.get(i));
        }
    }

    private void purgeOne() {
        synchronized (this.queue) {
            WeakKey key = (WeakKey) this.queue.poll();
            if (key != null) {
                super.remove(WeakKey.m20802(key));
            }
        }
    }

    private static final class Entry implements Map.Entry {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Object f16652;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f16653;

        Entry(Object x0, Object x1, AnonymousClass1 x2) {
            this(x0, x1);
        }

        private Entry(Object key, Object value) {
            this.f16653 = key;
            this.f16652 = value;
        }

        public boolean equals(Object o) {
            if (o == null || !(o instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) o;
            if (getKey() != null ? getKey().equals(entry.getKey()) : entry.getKey() == null) {
                return getValue() != null ? getValue().equals(entry.getValue()) : entry.getValue() == null;
            }
        }

        public int hashCode() {
            int i = 0;
            int hashCode = getKey() == null ? 0 : getKey().hashCode();
            if (getValue() != null) {
                i = getValue().hashCode();
            }
            return hashCode ^ i;
        }

        public Object setValue(Object value) {
            throw new UnsupportedOperationException("Entry.setValue is not supported.");
        }

        public Object getValue() {
            return this.f16652;
        }

        public Object getKey() {
            return this.f16653;
        }
    }

    private static final class Referenced {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f16654;

        /* renamed from: 龘  reason: contains not printable characters */
        private final WeakReference f16655;

        Referenced(Object x0, ReferenceQueue x1, AnonymousClass1 x2) {
            this(x0, x1);
        }

        Referenced(Object x0, AnonymousClass1 x1) {
            this(x0);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Object m20800(Referenced x0) {
            return x0.m20799();
        }

        private Referenced(Object referant) {
            this.f16655 = new WeakReference(referant);
            this.f16654 = referant.hashCode();
        }

        private Referenced(Object key, ReferenceQueue queue) {
            this.f16655 = new WeakKey(key, queue, this, (AnonymousClass1) null);
            this.f16654 = key.hashCode();
        }

        public int hashCode() {
            return this.f16654;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Object m20799() {
            return this.f16655.get();
        }

        public boolean equals(Object o) {
            boolean result;
            if (!(o instanceof Referenced)) {
                return false;
            }
            Referenced otherKey = (Referenced) o;
            Object thisKeyValue = m20799();
            Object otherKeyValue = otherKey.m20799();
            if (thisKeyValue != null) {
                return thisKeyValue.equals(otherKeyValue);
            }
            if (otherKeyValue == null) {
                result = true;
            } else {
                result = false;
            }
            if (!result || hashCode() != otherKey.hashCode()) {
                return false;
            }
            return true;
        }
    }

    private static final class WeakKey extends WeakReference {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Referenced f16656;

        WeakKey(Object x0, ReferenceQueue x1, Referenced x2, AnonymousClass1 x3) {
            this(x0, x1, x2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Referenced m20802(WeakKey x0) {
            return x0.m20801();
        }

        private WeakKey(Object key, ReferenceQueue queue, Referenced referenced) {
            super(key, queue);
            this.f16656 = referenced;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Referenced m20801() {
            return this.f16656;
        }
    }
}
