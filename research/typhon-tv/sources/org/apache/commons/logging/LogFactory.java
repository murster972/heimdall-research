package org.apache.commons.logging;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public abstract class LogFactory {
    public static final String DIAGNOSTICS_DEST_PROPERTY = "org.apache.commons.logging.diagnostics.dest";
    public static final String FACTORY_DEFAULT = "org.apache.commons.logging.impl.LogFactoryImpl";
    public static final String FACTORY_PROPERTIES = "commons-logging.properties";
    public static final String FACTORY_PROPERTY = "org.apache.commons.logging.LogFactory";
    public static final String HASHTABLE_IMPLEMENTATION_PROPERTY = "org.apache.commons.logging.LogFactory.HashtableImpl";
    public static final String PRIORITY_KEY = "priority";
    protected static final String SERVICE_ID = "META-INF/services/org.apache.commons.logging.LogFactory";
    public static final String TCCL_KEY = "use_tccl";
    private static final String WEAK_HASHTABLE_CLASSNAME = "org.apache.commons.logging.impl.WeakHashtable";
    static Class class$org$apache$commons$logging$LogFactory;
    private static final String diagnosticPrefix;
    private static PrintStream diagnosticsStream;
    protected static Hashtable factories;
    protected static volatile LogFactory nullClassLoaderFactory = null;
    private static final ClassLoader thisClassLoader;

    public abstract Object getAttribute(String str);

    public abstract String[] getAttributeNames();

    public abstract Log getInstance(Class cls) throws LogConfigurationException;

    public abstract Log getInstance(String str) throws LogConfigurationException;

    public abstract void release();

    public abstract void removeAttribute(String str);

    public abstract void setAttribute(String str, Object obj);

    static void access$000(String x0) {
        logDiagnostic(x0);
    }

    static {
        Class cls;
        String classLoaderName;
        Class cls2;
        diagnosticsStream = null;
        factories = null;
        if (class$org$apache$commons$logging$LogFactory == null) {
            cls = class$("org.apache.commons.logging.LogFactory");
            class$org$apache$commons$logging$LogFactory = cls;
        } else {
            cls = class$org$apache$commons$logging$LogFactory;
        }
        thisClassLoader = getClassLoader(cls);
        try {
            ClassLoader classLoader = thisClassLoader;
            if (thisClassLoader == null) {
                classLoaderName = "BOOTLOADER";
            } else {
                classLoaderName = objectId(classLoader);
            }
        } catch (SecurityException e) {
            classLoaderName = "UNKNOWN";
        }
        diagnosticPrefix = new StringBuffer().append("[LogFactory from ").append(classLoaderName).append("] ").toString();
        diagnosticsStream = initDiagnostics();
        if (class$org$apache$commons$logging$LogFactory == null) {
            cls2 = class$("org.apache.commons.logging.LogFactory");
            class$org$apache$commons$logging$LogFactory = cls2;
        } else {
            cls2 = class$org$apache$commons$logging$LogFactory;
        }
        logClassLoaderEnvironment(cls2);
        factories = createFactoryStore();
        if (isDiagnosticsEnabled()) {
            logDiagnostic("BOOTSTRAP COMPLETED");
        }
    }

    protected LogFactory() {
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.Hashtable} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final java.util.Hashtable createFactoryStore() {
        /*
            r3 = 0
            java.lang.String r6 = "org.apache.commons.logging.LogFactory.HashtableImpl"
            r7 = 0
            java.lang.String r4 = getSystemProperty(r6, r7)     // Catch:{ SecurityException -> 0x0022 }
        L_0x0009:
            if (r4 != 0) goto L_0x000e
            java.lang.String r4 = "org.apache.commons.logging.impl.WeakHashtable"
        L_0x000e:
            java.lang.Class r2 = java.lang.Class.forName(r4)     // Catch:{ Throwable -> 0x0025 }
            java.lang.Object r6 = r2.newInstance()     // Catch:{ Throwable -> 0x0025 }
            r0 = r6
            java.util.Hashtable r0 = (java.util.Hashtable) r0     // Catch:{ Throwable -> 0x0025 }
            r3 = r0
        L_0x001a:
            if (r3 != 0) goto L_0x0021
            java.util.Hashtable r3 = new java.util.Hashtable
            r3.<init>()
        L_0x0021:
            return r3
        L_0x0022:
            r1 = move-exception
            r4 = 0
            goto L_0x0009
        L_0x0025:
            r5 = move-exception
            handleThrowable(r5)
            java.lang.String r6 = "org.apache.commons.logging.impl.WeakHashtable"
            boolean r6 = r6.equals(r4)
            if (r6 != 0) goto L_0x001a
            boolean r6 = isDiagnosticsEnabled()
            if (r6 == 0) goto L_0x003f
            java.lang.String r6 = "[ERROR] LogFactory: Load of custom hashtable failed"
            logDiagnostic(r6)
            goto L_0x001a
        L_0x003f:
            java.io.PrintStream r6 = java.lang.System.err
            java.lang.String r7 = "[ERROR] LogFactory: Load of custom hashtable failed"
            r6.println(r7)
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.LogFactory.createFactoryStore():java.util.Hashtable");
    }

    private static String trim(String src) {
        if (src == null) {
            return null;
        }
        return src.trim();
    }

    protected static void handleThrowable(Throwable t) {
        if (t instanceof ThreadDeath) {
            throw ((ThreadDeath) t);
        } else if (t instanceof VirtualMachineError) {
            throw ((VirtualMachineError) t);
        }
    }

    public static LogFactory getFactory() throws LogConfigurationException {
        BufferedReader rd;
        String useTCCLStr;
        ClassLoader contextClassLoader = getContextClassLoaderInternal();
        if (contextClassLoader == null && isDiagnosticsEnabled()) {
            logDiagnostic("Context classloader is null.");
        }
        LogFactory factory = getCachedFactory(contextClassLoader);
        if (factory != null) {
            LogFactory logFactory = factory;
            return factory;
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("[LOOKUP] LogFactory implementation requested for the first time for context classloader ").append(objectId(contextClassLoader)).toString());
            logHierarchy("[LOOKUP] ", contextClassLoader);
        }
        Properties props = getConfigurationFile(contextClassLoader, "commons-logging.properties");
        ClassLoader baseClassLoader = contextClassLoader;
        if (!(props == null || (useTCCLStr = props.getProperty("use_tccl")) == null || Boolean.valueOf(useTCCLStr).booleanValue())) {
            baseClassLoader = thisClassLoader;
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic("[LOOKUP] Looking for system property [org.apache.commons.logging.LogFactory] to define the LogFactory subclass to use...");
        }
        try {
            String factoryClass = getSystemProperty("org.apache.commons.logging.LogFactory", (String) null);
            if (factoryClass != null) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("[LOOKUP] Creating an instance of LogFactory class '").append(factoryClass).append("' as specified by system property ").append("org.apache.commons.logging.LogFactory").toString());
                }
                factory = newFactory(factoryClass, baseClassLoader, contextClassLoader);
            } else if (isDiagnosticsEnabled()) {
                logDiagnostic("[LOOKUP] No system property [org.apache.commons.logging.LogFactory] defined.");
            }
        } catch (SecurityException e) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("[LOOKUP] A security exception occurred while trying to create an instance of the custom factory class: [").append(trim(e.getMessage())).append("]. Trying alternative implementations...").toString());
            }
        } catch (RuntimeException e2) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("[LOOKUP] An exception occurred while trying to create an instance of the custom factory class: [").append(trim(e2.getMessage())).append("] as specified by a system property.").toString());
            }
            throw e2;
        }
        if (factory == null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("[LOOKUP] Looking for a resource file of name [META-INF/services/org.apache.commons.logging.LogFactory] to define the LogFactory subclass to use...");
            }
            try {
                InputStream is = getResourceAsStream(contextClassLoader, SERVICE_ID);
                if (is != null) {
                    try {
                        rd = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    } catch (UnsupportedEncodingException e3) {
                        rd = new BufferedReader(new InputStreamReader(is));
                    }
                    String factoryClassName = rd.readLine();
                    rd.close();
                    if (factoryClassName != null && !"".equals(factoryClassName)) {
                        if (isDiagnosticsEnabled()) {
                            logDiagnostic(new StringBuffer().append("[LOOKUP]  Creating an instance of LogFactory class ").append(factoryClassName).append(" as specified by file '").append(SERVICE_ID).append("' which was present in the path of the context classloader.").toString());
                        }
                        factory = newFactory(factoryClassName, baseClassLoader, contextClassLoader);
                    }
                } else if (isDiagnosticsEnabled()) {
                    logDiagnostic("[LOOKUP] No resource file with name 'META-INF/services/org.apache.commons.logging.LogFactory' found.");
                }
            } catch (Exception ex) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("[LOOKUP] A security exception occurred while trying to create an instance of the custom factory class: [").append(trim(ex.getMessage())).append("]. Trying alternative implementations...").toString());
                }
            }
        }
        if (factory == null) {
            if (props != null) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic("[LOOKUP] Looking in properties file for entry with key 'org.apache.commons.logging.LogFactory' to define the LogFactory subclass to use...");
                }
                String factoryClass2 = props.getProperty("org.apache.commons.logging.LogFactory");
                if (factoryClass2 != null) {
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(new StringBuffer().append("[LOOKUP] Properties file specifies LogFactory subclass '").append(factoryClass2).append("'").toString());
                    }
                    factory = newFactory(factoryClass2, baseClassLoader, contextClassLoader);
                } else if (isDiagnosticsEnabled()) {
                    logDiagnostic("[LOOKUP] Properties file has no entry specifying LogFactory subclass.");
                }
            } else if (isDiagnosticsEnabled()) {
                logDiagnostic("[LOOKUP] No properties file available to determine LogFactory subclass from..");
            }
        }
        if (factory == null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("[LOOKUP] Loading the default LogFactory implementation 'org.apache.commons.logging.impl.LogFactoryImpl' via the same classloader that loaded this LogFactory class (ie not looking in the context classloader).");
            }
            factory = newFactory("org.apache.commons.logging.impl.LogFactoryImpl", thisClassLoader, contextClassLoader);
        }
        if (factory != null) {
            cacheFactory(contextClassLoader, factory);
            if (props != null) {
                Enumeration names = props.propertyNames();
                while (names.hasMoreElements()) {
                    String name = (String) names.nextElement();
                    factory.setAttribute(name, props.getProperty(name));
                }
            }
        }
        LogFactory logFactory2 = factory;
        return factory;
    }

    public static Log getLog(Class clazz) throws LogConfigurationException {
        return getFactory().getInstance(clazz);
    }

    public static Log getLog(String name) throws LogConfigurationException {
        return getFactory().getInstance(name);
    }

    public static void release(ClassLoader classLoader) {
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Releasing factory for classloader ").append(objectId(classLoader)).toString());
        }
        Hashtable factories2 = factories;
        synchronized (factories2) {
            if (classLoader != null) {
                LogFactory factory = (LogFactory) factories2.get(classLoader);
                if (factory != null) {
                    factory.release();
                    factories2.remove(classLoader);
                }
            } else if (nullClassLoaderFactory != null) {
                nullClassLoaderFactory.release();
                nullClassLoaderFactory = null;
            }
        }
    }

    public static void releaseAll() {
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Releasing factory for all classloaders.");
        }
        Hashtable factories2 = factories;
        synchronized (factories2) {
            Enumeration elements = factories2.elements();
            while (elements.hasMoreElements()) {
                ((LogFactory) elements.nextElement()).release();
            }
            factories2.clear();
            if (nullClassLoaderFactory != null) {
                nullClassLoaderFactory.release();
                nullClassLoaderFactory = null;
            }
        }
    }

    protected static ClassLoader getClassLoader(Class clazz) {
        try {
            return clazz.getClassLoader();
        } catch (SecurityException ex) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("Unable to get classloader for class '").append(clazz).append("' due to security restrictions - ").append(ex.getMessage()).toString());
            }
            throw ex;
        }
    }

    protected static ClassLoader getContextClassLoader() throws LogConfigurationException {
        return directGetContextClassLoader();
    }

    private static ClassLoader getContextClassLoaderInternal() throws LogConfigurationException {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                return LogFactory.directGetContextClassLoader();
            }
        });
    }

    protected static ClassLoader directGetContextClassLoader() throws LogConfigurationException {
        try {
            return Thread.currentThread().getContextClassLoader();
        } catch (SecurityException e) {
            return null;
        }
    }

    private static LogFactory getCachedFactory(ClassLoader contextClassLoader) {
        if (contextClassLoader == null) {
            return nullClassLoaderFactory;
        }
        return (LogFactory) factories.get(contextClassLoader);
    }

    private static void cacheFactory(ClassLoader classLoader, LogFactory factory) {
        if (factory == null) {
            return;
        }
        if (classLoader == null) {
            nullClassLoaderFactory = factory;
        } else {
            factories.put(classLoader, factory);
        }
    }

    protected static LogFactory newFactory(String factoryClass, ClassLoader classLoader, ClassLoader contextClassLoader) throws LogConfigurationException {
        Object result = AccessController.doPrivileged(new PrivilegedAction(factoryClass, classLoader) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final ClassLoader f16636;

            /* renamed from: 龘  reason: contains not printable characters */
            private final String f16637;

            {
                this.f16637 = r1;
                this.f16636 = r2;
            }

            public Object run() {
                return LogFactory.createFactory(this.f16637, this.f16636);
            }
        });
        if (result instanceof LogConfigurationException) {
            LogConfigurationException ex = (LogConfigurationException) result;
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("An error occurred while loading the factory class:").append(ex.getMessage()).toString());
            }
            throw ex;
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Created object ").append(objectId(result)).append(" to manage classloader ").append(objectId(contextClassLoader)).toString());
        }
        return (LogFactory) result;
    }

    protected static LogFactory newFactory(String factoryClass, ClassLoader classLoader) {
        return newFactory(factoryClass, classLoader, (ClassLoader) null);
    }

    protected static Object createFactory(String factoryClass, ClassLoader classLoader) {
        Class cls;
        Class cls2;
        Class cls3;
        Class cls4;
        Class logFactoryClass = null;
        if (classLoader != null) {
            try {
                logFactoryClass = classLoader.loadClass(factoryClass);
                if (class$org$apache$commons$logging$LogFactory == null) {
                    cls3 = class$("org.apache.commons.logging.LogFactory");
                    class$org$apache$commons$logging$LogFactory = cls3;
                } else {
                    cls3 = class$org$apache$commons$logging$LogFactory;
                }
                if (cls3.isAssignableFrom(logFactoryClass)) {
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(new StringBuffer().append("Loaded class ").append(logFactoryClass.getName()).append(" from classloader ").append(objectId(classLoader)).toString());
                    }
                } else if (isDiagnosticsEnabled()) {
                    StringBuffer append = new StringBuffer().append("Factory class ").append(logFactoryClass.getName()).append(" loaded from classloader ").append(objectId(logFactoryClass.getClassLoader())).append(" does not extend '");
                    if (class$org$apache$commons$logging$LogFactory == null) {
                        cls4 = class$("org.apache.commons.logging.LogFactory");
                        class$org$apache$commons$logging$LogFactory = cls4;
                    } else {
                        cls4 = class$org$apache$commons$logging$LogFactory;
                    }
                    logDiagnostic(append.append(cls4.getName()).append("' as loaded by this classloader.").toString());
                    logHierarchy("[BAD CL TREE] ", classLoader);
                }
                return (LogFactory) logFactoryClass.newInstance();
            } catch (ClassNotFoundException ex) {
                if (classLoader == thisClassLoader) {
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(new StringBuffer().append("Unable to locate any class called '").append(factoryClass).append("' via classloader ").append(objectId(classLoader)).toString());
                    }
                    throw ex;
                }
            } catch (NoClassDefFoundError e) {
                if (classLoader == thisClassLoader) {
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(new StringBuffer().append("Class '").append(factoryClass).append("' cannot be loaded").append(" via classloader ").append(objectId(classLoader)).append(" - it depends on some other class that cannot be found.").toString());
                    }
                    throw e;
                }
            } catch (ClassCastException e2) {
                if (classLoader == thisClassLoader) {
                    boolean implementsLogFactory = implementsLogFactory(logFactoryClass);
                    StringBuffer msg = new StringBuffer();
                    msg.append("The application has specified that a custom LogFactory implementation ");
                    msg.append("should be used but Class '");
                    msg.append(factoryClass);
                    msg.append("' cannot be converted to '");
                    if (class$org$apache$commons$logging$LogFactory == null) {
                        cls = class$("org.apache.commons.logging.LogFactory");
                        class$org$apache$commons$logging$LogFactory = cls;
                    } else {
                        cls = class$org$apache$commons$logging$LogFactory;
                    }
                    msg.append(cls.getName());
                    msg.append("'. ");
                    if (implementsLogFactory) {
                        msg.append("The conflict is caused by the presence of multiple LogFactory classes ");
                        msg.append("in incompatible classloaders. ");
                        msg.append("Background can be found in http://commons.apache.org/logging/tech.html. ");
                        msg.append("If you have not explicitly specified a custom LogFactory then it is likely ");
                        msg.append("that the container has set one without your knowledge. ");
                        msg.append("In this case, consider using the commons-logging-adapters.jar file or ");
                        msg.append("specifying the standard LogFactory from the command line. ");
                    } else {
                        msg.append("Please check the custom implementation. ");
                    }
                    msg.append("Help can be found @http://commons.apache.org/logging/troubleshooting.html.");
                    if (isDiagnosticsEnabled()) {
                        logDiagnostic(msg.toString());
                    }
                    throw new ClassCastException(msg.toString());
                }
            } catch (Exception e3) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic("Unable to create LogFactory instance.");
                }
                if (logFactoryClass != null) {
                    if (class$org$apache$commons$logging$LogFactory == null) {
                        cls2 = class$("org.apache.commons.logging.LogFactory");
                        class$org$apache$commons$logging$LogFactory = cls2;
                    } else {
                        cls2 = class$org$apache$commons$logging$LogFactory;
                    }
                    if (!cls2.isAssignableFrom(logFactoryClass)) {
                        return new LogConfigurationException("The chosen LogFactory implementation does not extend LogFactory. Please check your configuration.", e3);
                    }
                }
                return new LogConfigurationException((Throwable) e3);
            }
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Unable to load factory class via classloader ").append(objectId(classLoader)).append(" - trying the classloader associated with this LogFactory.").toString());
        }
        return (LogFactory) Class.forName(factoryClass).newInstance();
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    private static boolean implementsLogFactory(Class logFactoryClass) {
        boolean implementsLogFactory = false;
        if (logFactoryClass != null) {
            try {
                ClassLoader logFactoryClassLoader = logFactoryClass.getClassLoader();
                if (logFactoryClassLoader == null) {
                    logDiagnostic("[CUSTOM LOG FACTORY] was loaded by the boot classloader");
                } else {
                    logHierarchy("[CUSTOM LOG FACTORY] ", logFactoryClassLoader);
                    implementsLogFactory = Class.forName("org.apache.commons.logging.LogFactory", false, logFactoryClassLoader).isAssignableFrom(logFactoryClass);
                    if (implementsLogFactory) {
                        logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] ").append(logFactoryClass.getName()).append(" implements LogFactory but was loaded by an incompatible classloader.").toString());
                    } else {
                        logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] ").append(logFactoryClass.getName()).append(" does not implement LogFactory.").toString());
                    }
                }
            } catch (SecurityException e) {
                logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] SecurityException thrown whilst trying to determine whether the compatibility was caused by a classloader conflict: ").append(e.getMessage()).toString());
            } catch (LinkageError e2) {
                logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] LinkageError thrown whilst trying to determine whether the compatibility was caused by a classloader conflict: ").append(e2.getMessage()).toString());
            } catch (ClassNotFoundException e3) {
                logDiagnostic("[CUSTOM LOG FACTORY] LogFactory class cannot be loaded by classloader which loaded the custom LogFactory implementation. Is the custom factory in the right classloader?");
            }
        }
        return implementsLogFactory;
    }

    private static InputStream getResourceAsStream(ClassLoader loader, String name) {
        return (InputStream) AccessController.doPrivileged(new PrivilegedAction(loader, name) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f16638;

            /* renamed from: 龘  reason: contains not printable characters */
            private final ClassLoader f16639;

            {
                this.f16639 = r1;
                this.f16638 = r2;
            }

            public Object run() {
                if (this.f16639 != null) {
                    return this.f16639.getResourceAsStream(this.f16638);
                }
                return ClassLoader.getSystemResourceAsStream(this.f16638);
            }
        });
    }

    private static Enumeration getResources(ClassLoader loader, String name) {
        return (Enumeration) AccessController.doPrivileged(new PrivilegedAction(loader, name) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f16640;

            /* renamed from: 龘  reason: contains not printable characters */
            private final ClassLoader f16641;

            {
                this.f16641 = r1;
                this.f16640 = r2;
            }

            public Object run() {
                try {
                    if (this.f16641 != null) {
                        return this.f16641.getResources(this.f16640);
                    }
                    return ClassLoader.getSystemResources(this.f16640);
                } catch (IOException e) {
                    if (!LogFactory.isDiagnosticsEnabled()) {
                        return null;
                    }
                    LogFactory.access$000(new StringBuffer().append("Exception while trying to find configuration file ").append(this.f16640).append(":").append(e.getMessage()).toString());
                    return null;
                } catch (NoSuchMethodError e2) {
                    return null;
                }
            }
        });
    }

    private static Properties getProperties(URL url) {
        return (Properties) AccessController.doPrivileged(new PrivilegedAction(url) {

            /* renamed from: 龘  reason: contains not printable characters */
            private final URL f16642;

            {
                this.f16642 = r1;
            }

            public Object run() {
                InputStream stream = null;
                try {
                    URLConnection connection = this.f16642.openConnection();
                    connection.setUseCaches(false);
                    InputStream stream2 = connection.getInputStream();
                    if (stream2 != null) {
                        Properties props = new Properties();
                        props.load(stream2);
                        stream2.close();
                        InputStream stream3 = null;
                        if (stream3 == null) {
                            return props;
                        }
                        try {
                            stream3.close();
                            return props;
                        } catch (IOException e) {
                            if (!LogFactory.isDiagnosticsEnabled()) {
                                return props;
                            }
                            LogFactory.access$000(new StringBuffer().append("Unable to close stream for URL ").append(this.f16642).toString());
                            return props;
                        }
                    } else {
                        if (stream2 != null) {
                            try {
                                stream2.close();
                            } catch (IOException e2) {
                                if (LogFactory.isDiagnosticsEnabled()) {
                                    LogFactory.access$000(new StringBuffer().append("Unable to close stream for URL ").append(this.f16642).toString());
                                }
                            }
                        }
                        return null;
                    }
                } catch (IOException e3) {
                    if (LogFactory.isDiagnosticsEnabled()) {
                        LogFactory.access$000(new StringBuffer().append("Unable to read URL ").append(this.f16642).toString());
                    }
                    if (stream != null) {
                        try {
                            stream.close();
                        } catch (IOException e4) {
                            if (LogFactory.isDiagnosticsEnabled()) {
                                LogFactory.access$000(new StringBuffer().append("Unable to close stream for URL ").append(this.f16642).toString());
                            }
                        }
                    }
                } catch (Throwable th) {
                    if (stream != null) {
                        try {
                            stream.close();
                        } catch (IOException e5) {
                            if (LogFactory.isDiagnosticsEnabled()) {
                                LogFactory.access$000(new StringBuffer().append("Unable to close stream for URL ").append(this.f16642).toString());
                            }
                        }
                    }
                    throw th;
                }
            }
        });
    }

    private static final Properties getConfigurationFile(ClassLoader classLoader, String fileName) {
        Properties props = null;
        double priority = 0.0d;
        URL propsUrl = null;
        try {
            Enumeration urls = getResources(classLoader, fileName);
            if (urls == null) {
                return null;
            }
            while (urls.hasMoreElements()) {
                URL url = (URL) urls.nextElement();
                Properties newProps = getProperties(url);
                if (newProps != null) {
                    if (props == null) {
                        propsUrl = url;
                        props = newProps;
                        String priorityStr = props.getProperty("priority");
                        priority = 0.0d;
                        if (priorityStr != null) {
                            priority = Double.parseDouble(priorityStr);
                        }
                        if (isDiagnosticsEnabled()) {
                            logDiagnostic(new StringBuffer().append("[LOOKUP] Properties file found at '").append(url).append("'").append(" with priority ").append(priority).toString());
                        }
                    } else {
                        String newPriorityStr = newProps.getProperty("priority");
                        double newPriority = 0.0d;
                        if (newPriorityStr != null) {
                            newPriority = Double.parseDouble(newPriorityStr);
                        }
                        if (newPriority > priority) {
                            if (isDiagnosticsEnabled()) {
                                logDiagnostic(new StringBuffer().append("[LOOKUP] Properties file at '").append(url).append("'").append(" with priority ").append(newPriority).append(" overrides file at '").append(propsUrl).append("'").append(" with priority ").append(priority).toString());
                            }
                            propsUrl = url;
                            props = newProps;
                            priority = newPriority;
                        } else if (isDiagnosticsEnabled()) {
                            logDiagnostic(new StringBuffer().append("[LOOKUP] Properties file at '").append(url).append("'").append(" with priority ").append(newPriority).append(" does not override file at '").append(propsUrl).append("'").append(" with priority ").append(priority).toString());
                        }
                    }
                }
            }
            if (isDiagnosticsEnabled()) {
                if (props == null) {
                    logDiagnostic(new StringBuffer().append("[LOOKUP] No properties file of name '").append(fileName).append("' found.").toString());
                } else {
                    logDiagnostic(new StringBuffer().append("[LOOKUP] Properties file of name '").append(fileName).append("' found at '").append(propsUrl).append('\"').toString());
                }
            }
            Properties properties = props;
            return props;
        } catch (SecurityException e) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("SecurityException thrown while trying to find/read config files.");
            }
        }
    }

    private static String getSystemProperty(String key, String def) throws SecurityException {
        return (String) AccessController.doPrivileged(new PrivilegedAction(key, def) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f16643;

            /* renamed from: 龘  reason: contains not printable characters */
            private final String f16644;

            {
                this.f16644 = r1;
                this.f16643 = r2;
            }

            public Object run() {
                return System.getProperty(this.f16644, this.f16643);
            }
        });
    }

    private static PrintStream initDiagnostics() {
        try {
            String dest = getSystemProperty("org.apache.commons.logging.diagnostics.dest", (String) null);
            if (dest == null) {
                return null;
            }
            if (dest.equals("STDOUT")) {
                return System.out;
            }
            if (dest.equals("STDERR")) {
                return System.err;
            }
            try {
                return new PrintStream(new FileOutputStream(dest, true));
            } catch (IOException e) {
                return null;
            }
        } catch (SecurityException e2) {
            return null;
        }
    }

    protected static boolean isDiagnosticsEnabled() {
        return diagnosticsStream != null;
    }

    private static final void logDiagnostic(String msg) {
        if (diagnosticsStream != null) {
            diagnosticsStream.print(diagnosticPrefix);
            diagnosticsStream.println(msg);
            diagnosticsStream.flush();
        }
    }

    protected static final void logRawDiagnostic(String msg) {
        if (diagnosticsStream != null) {
            diagnosticsStream.println(msg);
            diagnosticsStream.flush();
        }
    }

    private static void logClassLoaderEnvironment(Class clazz) {
        if (isDiagnosticsEnabled()) {
            try {
                logDiagnostic(new StringBuffer().append("[ENV] Extension directories (java.ext.dir): ").append(System.getProperty("java.ext.dir")).toString());
                logDiagnostic(new StringBuffer().append("[ENV] Application classpath (java.class.path): ").append(System.getProperty("java.class.path")).toString());
            } catch (SecurityException e) {
                logDiagnostic("[ENV] Security setting prevent interrogation of system classpaths.");
            }
            String className = clazz.getName();
            try {
                ClassLoader classLoader = getClassLoader(clazz);
                logDiagnostic(new StringBuffer().append("[ENV] Class ").append(className).append(" was loaded via classloader ").append(objectId(classLoader)).toString());
                logHierarchy(new StringBuffer().append("[ENV] Ancestry of classloader which loaded ").append(className).append(" is ").toString(), classLoader);
            } catch (SecurityException e2) {
                logDiagnostic(new StringBuffer().append("[ENV] Security forbids determining the classloader for ").append(className).toString());
            }
        }
    }

    private static void logHierarchy(String prefix, ClassLoader classLoader) {
        if (isDiagnosticsEnabled()) {
            if (classLoader != null) {
                logDiagnostic(new StringBuffer().append(prefix).append(objectId(classLoader)).append(" == '").append(classLoader.toString()).append("'").toString());
            }
            try {
                ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
                if (classLoader != null) {
                    StringBuffer buf = new StringBuffer(new StringBuffer().append(prefix).append("ClassLoader tree:").toString());
                    do {
                        buf.append(objectId(classLoader));
                        if (classLoader == systemClassLoader) {
                            buf.append(" (SYSTEM) ");
                        }
                        try {
                            classLoader = classLoader.getParent();
                            buf.append(" --> ");
                        } catch (SecurityException e) {
                            buf.append(" --> SECRET");
                        }
                    } while (classLoader != null);
                    buf.append("BOOT");
                    logDiagnostic(buf.toString());
                }
            } catch (SecurityException e2) {
                logDiagnostic(new StringBuffer().append(prefix).append("Security forbids determining the system classloader.").toString());
            }
        }
    }

    public static String objectId(Object o) {
        if (o == null) {
            return "null";
        }
        return new StringBuffer().append(o.getClass().getName()).append("@").append(System.identityHashCode(o)).toString();
    }
}
