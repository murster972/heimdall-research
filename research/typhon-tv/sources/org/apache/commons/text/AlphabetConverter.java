package org.apache.commons.text;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public final class AlphabetConverter {
    private static final String ARROW = " -> ";
    private final int encodedLetterLength;
    private final Map<String, String> encodedToOriginal;
    private final Map<Integer, String> originalToEncoded;

    private AlphabetConverter(Map<Integer, String> originalToEncoded2, Map<String, String> encodedToOriginal2, int encodedLetterLength2) {
        this.originalToEncoded = originalToEncoded2;
        this.encodedToOriginal = encodedToOriginal2;
        this.encodedLetterLength = encodedLetterLength2;
    }

    public String encode(String original) throws UnsupportedEncodingException {
        if (original == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < original.length()) {
            int codepoint = original.codePointAt(i);
            String nextLetter = this.originalToEncoded.get(Integer.valueOf(codepoint));
            if (nextLetter == null) {
                throw new UnsupportedEncodingException("Couldn't find encoding for '" + codePointToString(codepoint) + "' in " + original);
            }
            sb.append(nextLetter);
            i += Character.charCount(codepoint);
        }
        return sb.toString();
    }

    public String decode(String encoded) throws UnsupportedEncodingException {
        if (encoded == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        int j = 0;
        while (j < encoded.length()) {
            Integer i = Integer.valueOf(encoded.codePointAt(j));
            String s = codePointToString(i.intValue());
            if (s.equals(this.originalToEncoded.get(i))) {
                result.append(s);
                j++;
            } else if (this.encodedLetterLength + j > encoded.length()) {
                throw new UnsupportedEncodingException("Unexpected end of string while decoding " + encoded);
            } else {
                String nextGroup = encoded.substring(j, this.encodedLetterLength + j);
                String next = this.encodedToOriginal.get(nextGroup);
                if (next == null) {
                    throw new UnsupportedEncodingException("Unexpected string without decoding (" + nextGroup + ") in " + encoded);
                }
                result.append(next);
                j += this.encodedLetterLength;
            }
        }
        return result.toString();
    }

    public int getEncodedCharLength() {
        return this.encodedLetterLength;
    }

    public Map<Integer, String> getOriginalToEncoded() {
        return Collections.unmodifiableMap(this.originalToEncoded);
    }

    /* JADX WARNING: type inference failed for: r14v0, types: [java.util.Iterator, java.util.Iterator<java.lang.Integer>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addSingleEncoding(int r11, java.lang.String r12, java.util.Collection<java.lang.Integer> r13, java.util.Iterator<java.lang.Integer> r14, java.util.Map<java.lang.Integer, java.lang.String> r15) {
        /*
            r10 = this;
            if (r11 <= 0) goto L_0x0049
            java.util.Iterator r9 = r13.iterator()
        L_0x0006:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x006d
            java.lang.Object r0 = r9.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r6 = r0.intValue()
            boolean r0 = r14.hasNext()
            if (r0 == 0) goto L_0x006d
            int r0 = r10.encodedLetterLength
            if (r11 != r0) goto L_0x002a
            java.lang.Integer r0 = java.lang.Integer.valueOf(r6)
            boolean r0 = r15.containsKey(r0)
            if (r0 != 0) goto L_0x0006
        L_0x002a:
            int r1 = r11 + -1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r12)
            java.lang.String r2 = codePointToString(r6)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r0.toString()
            r0 = r10
            r3 = r13
            r4 = r14
            r5 = r15
            r0.addSingleEncoding(r1, r2, r3, r4, r5)
            goto L_0x0006
        L_0x0049:
            java.lang.Object r7 = r14.next()
            java.lang.Integer r7 = (java.lang.Integer) r7
        L_0x004f:
            boolean r0 = r15.containsKey(r7)
            if (r0 == 0) goto L_0x0075
            int r0 = r7.intValue()
            java.lang.String r8 = codePointToString(r0)
            java.util.Map<java.lang.Integer, java.lang.String> r0 = r10.originalToEncoded
            r0.put(r7, r8)
            java.util.Map<java.lang.String, java.lang.String> r0 = r10.encodedToOriginal
            r0.put(r8, r8)
            boolean r0 = r14.hasNext()
            if (r0 != 0) goto L_0x006e
        L_0x006d:
            return
        L_0x006e:
            java.lang.Object r7 = r14.next()
            java.lang.Integer r7 = (java.lang.Integer) r7
            goto L_0x004f
        L_0x0075:
            int r0 = r7.intValue()
            java.lang.String r8 = codePointToString(r0)
            java.util.Map<java.lang.Integer, java.lang.String> r0 = r10.originalToEncoded
            r0.put(r7, r12)
            java.util.Map<java.lang.String, java.lang.String> r0 = r10.encodedToOriginal
            r0.put(r12, r8)
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.text.AlphabetConverter.addSingleEncoding(int, java.lang.String, java.util.Collection, java.util.Iterator, java.util.Map):void");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, String> entry : this.originalToEncoded.entrySet()) {
            sb.append(codePointToString(entry.getKey().intValue())).append(ARROW).append(entry.getValue()).append(System.lineSeparator());
        }
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AlphabetConverter)) {
            return false;
        }
        AlphabetConverter other = (AlphabetConverter) obj;
        if (!this.originalToEncoded.equals(other.originalToEncoded) || !this.encodedToOriginal.equals(other.encodedToOriginal) || this.encodedLetterLength != other.encodedLetterLength) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.originalToEncoded, this.encodedToOriginal, Integer.valueOf(this.encodedLetterLength)});
    }

    public static AlphabetConverter createConverterFromMap(Map<Integer, String> originalToEncoded2) {
        Map<Integer, String> unmodifiableOriginalToEncoded = Collections.unmodifiableMap(originalToEncoded2);
        Map<String, String> encodedToOriginal2 = new LinkedHashMap<>();
        int encodedLetterLength2 = 1;
        for (Map.Entry<Integer, String> e : unmodifiableOriginalToEncoded.entrySet()) {
            encodedToOriginal2.put(e.getValue(), codePointToString(e.getKey().intValue()));
            if (e.getValue().length() > encodedLetterLength2) {
                encodedLetterLength2 = e.getValue().length();
            }
        }
        return new AlphabetConverter(unmodifiableOriginalToEncoded, encodedToOriginal2, encodedLetterLength2);
    }

    public static AlphabetConverter createConverterFromChars(Character[] original, Character[] encoding, Character[] doNotEncode) {
        return createConverter(convertCharsToIntegers(original), convertCharsToIntegers(encoding), convertCharsToIntegers(doNotEncode));
    }

    private static Integer[] convertCharsToIntegers(Character[] chars) {
        if (chars == null || chars.length == 0) {
            return new Integer[0];
        }
        Integer[] integers = new Integer[chars.length];
        for (int i = 0; i < chars.length; i++) {
            integers[i] = Integer.valueOf(chars[i].charValue());
        }
        return integers;
    }

    public static AlphabetConverter createConverter(Integer[] original, Integer[] encoding, Integer[] doNotEncode) {
        Integer next;
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>(Arrays.asList(original));
        LinkedHashSet linkedHashSet2 = new LinkedHashSet(Arrays.asList(encoding));
        Set<Integer> doNotEncodeCopy = new LinkedHashSet<>(Arrays.asList(doNotEncode));
        Map<Integer, String> originalToEncoded2 = new LinkedHashMap<>();
        Map<String, String> encodedToOriginal2 = new LinkedHashMap<>();
        Map<Integer, String> doNotEncodeMap = new HashMap<>();
        for (Integer intValue : doNotEncodeCopy) {
            int i = intValue.intValue();
            if (!linkedHashSet.contains(Integer.valueOf(i))) {
                throw new IllegalArgumentException("Can not use 'do not encode' list because original alphabet does not contain '" + codePointToString(i) + "'");
            } else if (!linkedHashSet2.contains(Integer.valueOf(i))) {
                throw new IllegalArgumentException("Can not use 'do not encode' list because encoding alphabet does not contain '" + codePointToString(i) + "'");
            } else {
                doNotEncodeMap.put(Integer.valueOf(i), codePointToString(i));
            }
        }
        if (linkedHashSet2.size() >= linkedHashSet.size()) {
            Iterator it2 = linkedHashSet2.iterator();
            for (Integer intValue2 : linkedHashSet) {
                int originalLetter = intValue2.intValue();
                String originalLetterAsString = codePointToString(originalLetter);
                if (doNotEncodeMap.containsKey(Integer.valueOf(originalLetter))) {
                    originalToEncoded2.put(Integer.valueOf(originalLetter), originalLetterAsString);
                    encodedToOriginal2.put(originalLetterAsString, originalLetterAsString);
                } else {
                    Object next2 = it2.next();
                    while (true) {
                        next = (Integer) next2;
                        if (!doNotEncodeCopy.contains(next)) {
                            break;
                        }
                        next2 = it2.next();
                    }
                    String encodedLetter = codePointToString(next.intValue());
                    originalToEncoded2.put(Integer.valueOf(originalLetter), encodedLetter);
                    encodedToOriginal2.put(encodedLetter, originalLetterAsString);
                }
            }
            return new AlphabetConverter(originalToEncoded2, encodedToOriginal2, 1);
        } else if (linkedHashSet2.size() - doNotEncodeCopy.size() < 2) {
            throw new IllegalArgumentException("Must have at least two encoding characters (excluding those in the 'do not encode' list), but has " + (linkedHashSet2.size() - doNotEncodeCopy.size()));
        } else {
            int lettersSoFar = 1;
            int lettersLeft = (linkedHashSet.size() - doNotEncodeCopy.size()) / (linkedHashSet2.size() - doNotEncodeCopy.size());
            while (lettersLeft / linkedHashSet2.size() >= 1) {
                lettersLeft /= linkedHashSet2.size();
                lettersSoFar++;
            }
            int encodedLetterLength2 = lettersSoFar + 1;
            AlphabetConverter ac = new AlphabetConverter(originalToEncoded2, encodedToOriginal2, encodedLetterLength2);
            ac.addSingleEncoding(encodedLetterLength2, "", linkedHashSet2, linkedHashSet.iterator(), doNotEncodeMap);
            return ac;
        }
    }

    private static String codePointToString(int i) {
        if (Character.charCount(i) == 1) {
            return String.valueOf((char) i);
        }
        return new String(Character.toChars(i));
    }
}
