package org.apache.commons.text.diff;

public abstract class EditCommand<T> {
    private final T object;

    public abstract void accept(CommandVisitor<T> commandVisitor);

    protected EditCommand(T object2) {
        this.object = object2;
    }

    /* access modifiers changed from: protected */
    public T getObject() {
        return this.object;
    }
}
