package org.apache.commons.text.diff;

public class StringsComparator {
    private final String left;
    private final String right;
    private final int[] vDown;
    private final int[] vUp;

    public StringsComparator(String left2, String right2) {
        this.left = left2;
        this.right = right2;
        int size = left2.length() + right2.length() + 2;
        this.vDown = new int[size];
        this.vUp = new int[size];
    }

    public EditScript<Character> getScript() {
        EditScript<Character> script = new EditScript<>();
        buildScript(0, this.left.length(), 0, this.right.length(), script);
        return script;
    }

    private void buildScript(int start1, int end1, int start2, int end2, EditScript<Character> script) {
        Snake middle = getMiddleSnake(start1, end1, start2, end2);
        if (middle == null || ((middle.m20805() == end1 && middle.m20804() == end1 - end2) || (middle.m20803() == start1 && middle.m20804() == start1 - start2))) {
            int i = start1;
            int j = start2;
            while (true) {
                if (i >= end1 && j >= end2) {
                    return;
                }
                if (i < end1 && j < end2 && this.left.charAt(i) == this.right.charAt(j)) {
                    script.append((KeepCommand<Character>) new KeepCommand(Character.valueOf(this.left.charAt(i))));
                    i++;
                    j++;
                } else if (end1 - start1 > end2 - start2) {
                    script.append((DeleteCommand<Character>) new DeleteCommand(Character.valueOf(this.left.charAt(i))));
                    i++;
                } else {
                    script.append((InsertCommand<Character>) new InsertCommand(Character.valueOf(this.right.charAt(j))));
                    j++;
                }
            }
        } else {
            buildScript(start1, middle.m20805(), start2, middle.m20805() - middle.m20804(), script);
            for (int i2 = middle.m20805(); i2 < middle.m20803(); i2++) {
                script.append((KeepCommand<Character>) new KeepCommand(Character.valueOf(this.left.charAt(i2))));
            }
            buildScript(middle.m20803(), end1, middle.m20803() - middle.m20804(), end2, script);
        }
    }

    private Snake getMiddleSnake(int start1, int end1, int start2, int end2) {
        int m = end1 - start1;
        int n = end2 - start2;
        if (m == 0 || n == 0) {
            return null;
        }
        int delta = m - n;
        int sum = n + m;
        if (sum % 2 != 0) {
            sum++;
        }
        int offset = sum / 2;
        this.vDown[offset + 1] = start1;
        this.vUp[offset + 1] = end1 + 1;
        for (int d = 0; d <= offset; d++) {
            int k = -d;
            while (k <= d) {
                int i = k + offset;
                if (k == (-d) || (k != d && this.vDown[i - 1] < this.vDown[i + 1])) {
                    this.vDown[i] = this.vDown[i + 1];
                } else {
                    this.vDown[i] = this.vDown[i - 1] + 1;
                }
                int x = this.vDown[i];
                int y = ((x - start1) + start2) - k;
                while (x < end1 && y < end2 && this.left.charAt(x) == this.right.charAt(y)) {
                    x++;
                    this.vDown[i] = x;
                    y++;
                }
                if (delta % 2 == 0 || delta - d > k || k > delta + d || this.vUp[i - delta] > this.vDown[i]) {
                    k += 2;
                } else {
                    return buildSnake(this.vUp[i - delta], (k + start1) - start2, end1, end2);
                }
            }
            int k2 = delta - d;
            while (k2 <= delta + d) {
                int i2 = (k2 + offset) - delta;
                if (k2 == delta - d || (k2 != delta + d && this.vUp[i2 + 1] <= this.vUp[i2 - 1])) {
                    this.vUp[i2] = this.vUp[i2 + 1] - 1;
                } else {
                    this.vUp[i2] = this.vUp[i2 - 1];
                }
                int x2 = this.vUp[i2] - 1;
                int y2 = ((x2 - start1) + start2) - k2;
                while (true) {
                    int x3 = x2;
                    if (x3 >= start1 && y2 >= start2 && this.left.charAt(x3) == this.right.charAt(y2)) {
                        x2 = x3 - 1;
                        this.vUp[i2] = x3;
                        y2--;
                    }
                }
                if (delta % 2 != 0 || (-d) > k2 || k2 > d || this.vUp[i2] > this.vDown[i2 + delta]) {
                    k2 += 2;
                } else {
                    return buildSnake(this.vUp[i2], (k2 + start1) - start2, end1, end2);
                }
            }
        }
        throw new RuntimeException("Internal Error");
    }

    private Snake buildSnake(int start, int diag, int end1, int end2) {
        int end = start;
        while (end - diag < end2 && end < end1 && this.left.charAt(end) == this.right.charAt(end - diag)) {
            end++;
        }
        return new Snake(start, end, diag);
    }

    private static class Snake {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f16667;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f16668;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f16669;

        Snake(int start, int end, int diag) {
            this.f16669 = start;
            this.f16667 = end;
            this.f16668 = diag;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20805() {
            return this.f16669;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m20803() {
            return this.f16667;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public int m20804() {
            return this.f16668;
        }
    }
}
