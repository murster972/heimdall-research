package org.apache.commons.text;

import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

public class CaseUtils {
    public static String toCamelCase(String str, boolean capitalizeFirstLetter, char... delimiters) {
        boolean capitalizeNext;
        int outOffset;
        if (StringUtils.isEmpty(str)) {
            return str;
        }
        String str2 = str.toLowerCase();
        int strLen = str2.length();
        int[] newCodePoints = new int[strLen];
        Set<Integer> delimiterSet = generateDelimiterSet(delimiters);
        boolean capitalizeNext2 = false;
        if (capitalizeFirstLetter) {
            capitalizeNext2 = true;
        }
        int index = 0;
        int outOffset2 = 0;
        while (index < strLen) {
            int codePoint = str2.codePointAt(index);
            if (delimiterSet.contains(Integer.valueOf(codePoint))) {
                capitalizeNext = true;
                if (outOffset2 == 0) {
                    capitalizeNext = false;
                }
                index += Character.charCount(codePoint);
                outOffset = outOffset2;
            } else if (capitalizeNext || (outOffset2 == 0 && capitalizeFirstLetter)) {
                int titleCaseCodePoint = Character.toTitleCase(codePoint);
                outOffset = outOffset2 + 1;
                newCodePoints[outOffset2] = titleCaseCodePoint;
                index += Character.charCount(titleCaseCodePoint);
                capitalizeNext = false;
            } else {
                outOffset = outOffset2 + 1;
                newCodePoints[outOffset2] = codePoint;
                index += Character.charCount(codePoint);
            }
            outOffset2 = outOffset;
        }
        if (outOffset2 != 0) {
            return new String(newCodePoints, 0, outOffset2);
        }
        return str2;
    }

    private static Set<Integer> generateDelimiterSet(char[] delimiters) {
        Set<Integer> delimiterHashSet = new HashSet<>();
        delimiterHashSet.add(Integer.valueOf(Character.codePointAt(new char[]{' '}, 0)));
        if (!(delimiters == null || delimiters.length == 0)) {
            for (int index = 0; index < delimiters.length; index++) {
                delimiterHashSet.add(Integer.valueOf(Character.codePointAt(delimiters, index)));
            }
        }
        return delimiterHashSet;
    }
}
