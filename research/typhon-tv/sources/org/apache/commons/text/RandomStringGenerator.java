package org.apache.commons.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.lang3.Validate;

public final class RandomStringGenerator {
    private final List<Character> characterList;
    private final Set<CharacterPredicate> inclusivePredicates;
    private final int maximumCodePoint;
    private final int minimumCodePoint;
    private final TextRandomProvider random;

    private RandomStringGenerator(int minimumCodePoint2, int maximumCodePoint2, Set<CharacterPredicate> inclusivePredicates2, TextRandomProvider random2, List<Character> characterList2) {
        this.minimumCodePoint = minimumCodePoint2;
        this.maximumCodePoint = maximumCodePoint2;
        this.inclusivePredicates = inclusivePredicates2;
        this.random = random2;
        this.characterList = characterList2;
    }

    private int generateRandomNumber(int minInclusive, int maxInclusive) {
        if (this.random != null) {
            return this.random.nextInt((maxInclusive - minInclusive) + 1) + minInclusive;
        }
        return ThreadLocalRandom.current().nextInt(minInclusive, maxInclusive + 1);
    }

    private int generateRandomNumber(List<Character> characterList2) {
        int listSize = characterList2.size();
        if (this.random != null) {
            return String.valueOf(characterList2.get(this.random.nextInt(listSize))).codePointAt(0);
        }
        return String.valueOf(characterList2.get(ThreadLocalRandom.current().nextInt(0, listSize))).codePointAt(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004d, code lost:
        if (r2 == false) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String generate(int r11) {
        /*
            r10 = this;
            if (r11 != 0) goto L_0x0006
            java.lang.String r6 = ""
        L_0x0005:
            return r6
        L_0x0006:
            if (r11 <= 0) goto L_0x005a
            r6 = 1
        L_0x0009:
            java.lang.String r7 = "Length %d is smaller than zero."
            long r8 = (long) r11
            org.apache.commons.lang3.Validate.isTrue((boolean) r6, (java.lang.String) r7, (long) r8)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r11)
            long r4 = (long) r11
        L_0x0016:
            java.util.List<java.lang.Character> r6 = r10.characterList
            if (r6 == 0) goto L_0x005c
            java.util.List<java.lang.Character> r6 = r10.characterList
            boolean r6 = r6.isEmpty()
            if (r6 != 0) goto L_0x005c
            java.util.List<java.lang.Character> r6 = r10.characterList
            int r1 = r10.generateRandomNumber(r6)
        L_0x0028:
            int r6 = java.lang.Character.getType(r1)
            switch(r6) {
                case 0: goto L_0x004f;
                case 18: goto L_0x004f;
                case 19: goto L_0x004f;
                default: goto L_0x002f;
            }
        L_0x002f:
            java.util.Set<org.apache.commons.text.CharacterPredicate> r6 = r10.inclusivePredicates
            if (r6 == 0) goto L_0x0065
            r2 = 0
            java.util.Set<org.apache.commons.text.CharacterPredicate> r6 = r10.inclusivePredicates
            java.util.Iterator r6 = r6.iterator()
        L_0x003a:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x004d
            java.lang.Object r3 = r6.next()
            org.apache.commons.text.CharacterPredicate r3 = (org.apache.commons.text.CharacterPredicate) r3
            boolean r7 = r3.test(r1)
            if (r7 == 0) goto L_0x003a
            r2 = 1
        L_0x004d:
            if (r2 != 0) goto L_0x0065
        L_0x004f:
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x0016
            java.lang.String r6 = r0.toString()
            goto L_0x0005
        L_0x005a:
            r6 = 0
            goto L_0x0009
        L_0x005c:
            int r6 = r10.minimumCodePoint
            int r7 = r10.maximumCodePoint
            int r1 = r10.generateRandomNumber(r6, r7)
            goto L_0x0028
        L_0x0065:
            r0.appendCodePoint(r1)
            r6 = 1
            long r4 = r4 - r6
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.text.RandomStringGenerator.generate(int):java.lang.String");
    }

    public String generate(int minLengthInclusive, int maxLengthInclusive) {
        boolean z;
        boolean z2;
        if (minLengthInclusive >= 0) {
            z = true;
        } else {
            z = false;
        }
        Validate.isTrue(z, "Minimum length %d is smaller than zero.", (long) minLengthInclusive);
        if (minLengthInclusive <= maxLengthInclusive) {
            z2 = true;
        } else {
            z2 = false;
        }
        Validate.isTrue(z2, "Maximum length %d is smaller than minimum length %d.", Integer.valueOf(maxLengthInclusive), Integer.valueOf(minLengthInclusive));
        return generate(generateRandomNumber(minLengthInclusive, maxLengthInclusive));
    }

    public static class Builder implements Builder<RandomStringGenerator> {
        public static final int DEFAULT_LENGTH = 0;
        public static final int DEFAULT_MAXIMUM_CODE_POINT = 1114111;
        public static final int DEFAULT_MINIMUM_CODE_POINT = 0;
        private List<Character> characterList;
        private Set<CharacterPredicate> inclusivePredicates;
        private int maximumCodePoint = 1114111;
        private int minimumCodePoint = 0;
        private TextRandomProvider random;

        public Builder withinRange(int minimumCodePoint2, int maximumCodePoint2) {
            boolean z;
            boolean z2 = true;
            Validate.isTrue(minimumCodePoint2 <= maximumCodePoint2, "Minimum code point %d is larger than maximum code point %d", Integer.valueOf(minimumCodePoint2), Integer.valueOf(maximumCodePoint2));
            if (minimumCodePoint2 >= 0) {
                z = true;
            } else {
                z = false;
            }
            Validate.isTrue(z, "Minimum code point %d is negative", (long) minimumCodePoint2);
            if (maximumCodePoint2 > 1114111) {
                z2 = false;
            }
            Validate.isTrue(z2, "Value %d is larger than Character.MAX_CODE_POINT.", (long) maximumCodePoint2);
            this.minimumCodePoint = minimumCodePoint2;
            this.maximumCodePoint = maximumCodePoint2;
            return this;
        }

        public Builder withinRange(char[]... pairs) {
            boolean z;
            boolean z2;
            this.characterList = new ArrayList();
            for (char[] pair : pairs) {
                if (pair.length == 2) {
                    z = true;
                } else {
                    z = false;
                }
                Validate.isTrue(z, "Each pair must contain minimum and maximum code point", new Object[0]);
                char minimumCodePoint2 = pair[0];
                char maximumCodePoint2 = pair[1];
                if (minimumCodePoint2 <= maximumCodePoint2) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                Validate.isTrue(z2, "Minimum code point %d is larger than maximum code point %d", Integer.valueOf(minimumCodePoint2), Integer.valueOf(maximumCodePoint2));
                for (int index = minimumCodePoint2; index <= maximumCodePoint2; index++) {
                    this.characterList.add(Character.valueOf((char) index));
                }
            }
            return this;
        }

        public Builder filteredBy(CharacterPredicate... predicates) {
            if (predicates == null || predicates.length == 0) {
                this.inclusivePredicates = null;
            } else {
                if (this.inclusivePredicates == null) {
                    this.inclusivePredicates = new HashSet();
                } else {
                    this.inclusivePredicates.clear();
                }
                Collections.addAll(this.inclusivePredicates, predicates);
            }
            return this;
        }

        public Builder usingRandom(TextRandomProvider random2) {
            this.random = random2;
            return this;
        }

        public Builder selectFrom(char... chars) {
            this.characterList = new ArrayList();
            for (char c : chars) {
                this.characterList.add(Character.valueOf(c));
            }
            return this;
        }

        public RandomStringGenerator build() {
            return new RandomStringGenerator(this.minimumCodePoint, this.maximumCodePoint, this.inclusivePredicates, this.random, this.characterList);
        }
    }
}
