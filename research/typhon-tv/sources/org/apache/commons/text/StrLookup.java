package org.apache.commons.text;

import java.util.Map;
import java.util.ResourceBundle;

public abstract class StrLookup<V> {
    private static final StrLookup<String> NONE_LOOKUP = new MapStrLookup((Map) null);
    private static final StrLookup<String> SYSTEM_PROPERTIES_LOOKUP = new SystemPropertiesStrLookup();

    public abstract String lookup(String str);

    public static StrLookup<?> noneLookup() {
        return NONE_LOOKUP;
    }

    public static StrLookup<String> systemPropertiesLookup() {
        return SYSTEM_PROPERTIES_LOOKUP;
    }

    public static <V> StrLookup<V> mapLookup(Map<String, V> map) {
        return new MapStrLookup(map);
    }

    public static StrLookup<String> resourceBundleLookup(ResourceBundle resourceBundle) {
        return new ResourceBundleLookup(resourceBundle);
    }

    protected StrLookup() {
    }

    static class MapStrLookup<V> extends StrLookup<V> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, V> f16662;

        MapStrLookup(Map<String, V> map) {
            this.f16662 = map;
        }

        public String lookup(String key) {
            Object obj;
            if (this.f16662 == null || (obj = this.f16662.get(key)) == null) {
                return null;
            }
            return obj.toString();
        }

        public String toString() {
            return super.toString() + " [map=" + this.f16662 + "]";
        }
    }

    private static final class ResourceBundleLookup extends StrLookup<String> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final ResourceBundle f16663;

        private ResourceBundleLookup(ResourceBundle resourceBundle) {
            this.f16663 = resourceBundle;
        }

        public String lookup(String key) {
            if (this.f16663 == null || key == null || !this.f16663.containsKey(key)) {
                return null;
            }
            return this.f16663.getString(key);
        }

        public String toString() {
            return super.toString() + " [resourceBundle=" + this.f16663 + "]";
        }
    }

    private static final class SystemPropertiesStrLookup extends StrLookup<String> {
        private SystemPropertiesStrLookup() {
        }

        public String lookup(String key) {
            if (key.length() <= 0) {
                return null;
            }
            try {
                return System.getProperty(key);
            } catch (SecurityException e) {
                return null;
            }
        }
    }
}
