package org.apache.commons.text.similarity;

import java.util.Arrays;

public class JaroWinklerDistance implements SimilarityScore<Double> {
    public static final int INDEX_NOT_FOUND = -1;

    public Double apply(CharSequence left, CharSequence right) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        int[] mtp = matches(left, right);
        double m = (double) mtp[0];
        if (m == 0.0d) {
            return Double.valueOf(0.0d);
        }
        double j = (((m / ((double) left.length())) + (m / ((double) right.length()))) + ((m - ((double) mtp[1])) / m)) / 3.0d;
        return Double.valueOf(j < 0.7d ? j : j + (Math.min(0.1d, 1.0d / ((double) mtp[3])) * ((double) mtp[2]) * (1.0d - j)));
    }

    protected static int[] matches(CharSequence first, CharSequence second) {
        CharSequence max;
        CharSequence min;
        if (first.length() > second.length()) {
            max = first;
            min = second;
        } else {
            max = second;
            min = first;
        }
        int range = Math.max((max.length() / 2) - 1, 0);
        int[] matchIndexes = new int[min.length()];
        Arrays.fill(matchIndexes, -1);
        boolean[] matchFlags = new boolean[max.length()];
        int matches = 0;
        for (int mi = 0; mi < min.length(); mi++) {
            char c1 = min.charAt(mi);
            int xi = Math.max(mi - range, 0);
            int xn = Math.min(mi + range + 1, max.length());
            while (true) {
                if (xi < xn) {
                    if (!matchFlags[xi] && c1 == max.charAt(xi)) {
                        matchIndexes[mi] = xi;
                        matchFlags[xi] = true;
                        matches++;
                        break;
                    }
                    xi++;
                } else {
                    break;
                }
            }
        }
        char[] ms1 = new char[matches];
        char[] ms2 = new char[matches];
        int si = 0;
        for (int i = 0; i < min.length(); i++) {
            if (matchIndexes[i] != -1) {
                ms1[si] = min.charAt(i);
                si++;
            }
        }
        int si2 = 0;
        for (int i2 = 0; i2 < max.length(); i2++) {
            if (matchFlags[i2]) {
                ms2[si2] = max.charAt(i2);
                si2++;
            }
        }
        int transpositions = 0;
        for (int mi2 = 0; mi2 < ms1.length; mi2++) {
            if (ms1[mi2] != ms2[mi2]) {
                transpositions++;
            }
        }
        int prefix = 0;
        int mi3 = 0;
        while (mi3 < min.length() && first.charAt(mi3) == second.charAt(mi3)) {
            prefix++;
            mi3++;
        }
        return new int[]{matches, transpositions / 2, prefix, max.length()};
    }
}
