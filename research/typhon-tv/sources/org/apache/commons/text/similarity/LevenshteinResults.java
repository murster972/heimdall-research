package org.apache.commons.text.similarity;

import java.util.Objects;

public class LevenshteinResults {
    private final Integer deleteCount;
    private final Integer distance;
    private final Integer insertCount;
    private final Integer substituteCount;

    public LevenshteinResults(Integer distance2, Integer insertCount2, Integer deleteCount2, Integer substituteCount2) {
        this.distance = distance2;
        this.insertCount = insertCount2;
        this.deleteCount = deleteCount2;
        this.substituteCount = substituteCount2;
    }

    public Integer getDistance() {
        return this.distance;
    }

    public Integer getInsertCount() {
        return this.insertCount;
    }

    public Integer getDeleteCount() {
        return this.deleteCount;
    }

    public Integer getSubstituteCount() {
        return this.substituteCount;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LevenshteinResults result = (LevenshteinResults) o;
        if (!Objects.equals(this.distance, result.distance) || !Objects.equals(this.insertCount, result.insertCount) || !Objects.equals(this.deleteCount, result.deleteCount) || !Objects.equals(this.substituteCount, result.substituteCount)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.distance, this.insertCount, this.deleteCount, this.substituteCount});
    }

    public String toString() {
        return "Distance: " + this.distance + ", Insert: " + this.insertCount + ", Delete: " + this.deleteCount + ", Substitute: " + this.substituteCount;
    }
}
