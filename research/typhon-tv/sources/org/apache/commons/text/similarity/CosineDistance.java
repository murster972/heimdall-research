package org.apache.commons.text.similarity;

public class CosineDistance implements EditDistance<Double> {
    private final CosineSimilarity cosineSimilarity = new CosineSimilarity();
    private final Tokenizer<CharSequence> tokenizer = new RegexTokenizer();

    public Double apply(CharSequence left, CharSequence right) {
        return Double.valueOf(1.0d - this.cosineSimilarity.cosineSimilarity(Counter.m20806((CharSequence[]) this.tokenizer.m20809(left)), Counter.m20806((CharSequence[]) this.tokenizer.m20809(right))).doubleValue());
    }
}
