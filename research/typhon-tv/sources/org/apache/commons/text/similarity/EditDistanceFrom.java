package org.apache.commons.text.similarity;

import org.apache.commons.lang3.Validate;

public class EditDistanceFrom<R> {
    private final EditDistance<R> editDistance;
    private final CharSequence left;

    public EditDistanceFrom(EditDistance<R> editDistance2, CharSequence left2) {
        boolean z;
        if (editDistance2 != null) {
            z = true;
        } else {
            z = false;
        }
        Validate.isTrue(z, "The edit distance may not be null.", new Object[0]);
        this.editDistance = editDistance2;
        this.left = left2;
    }

    public R apply(CharSequence right) {
        return this.editDistance.apply(this.left, right);
    }

    public CharSequence getLeft() {
        return this.left;
    }

    public EditDistance<R> getEditDistance() {
        return this.editDistance;
    }
}
