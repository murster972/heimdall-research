package org.apache.commons.text.similarity;

public class HammingDistance implements EditDistance<Integer> {
    public Integer apply(CharSequence left, CharSequence right) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (left.length() != right.length()) {
            throw new IllegalArgumentException("Strings must have the same length");
        } else {
            int distance = 0;
            for (int i = 0; i < left.length(); i++) {
                if (left.charAt(i) != right.charAt(i)) {
                    distance++;
                }
            }
            return Integer.valueOf(distance);
        }
    }
}
