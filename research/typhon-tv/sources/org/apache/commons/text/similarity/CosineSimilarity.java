package org.apache.commons.text.similarity;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CosineSimilarity {
    public Double cosineSimilarity(Map<CharSequence, Integer> leftVector, Map<CharSequence, Integer> rightVector) {
        double cosineSimilarity;
        if (leftVector == null || rightVector == null) {
            throw new IllegalArgumentException("Vectors must not be null");
        }
        double dotProduct = dot(leftVector, rightVector, getIntersection(leftVector, rightVector));
        double d1 = 0.0d;
        for (Integer value : leftVector.values()) {
            d1 += Math.pow((double) value.intValue(), 2.0d);
        }
        double d2 = 0.0d;
        for (Integer value2 : rightVector.values()) {
            d2 += Math.pow((double) value2.intValue(), 2.0d);
        }
        if (d1 <= 0.0d || d2 <= 0.0d) {
            cosineSimilarity = 0.0d;
        } else {
            cosineSimilarity = dotProduct / (Math.sqrt(d1) * Math.sqrt(d2));
        }
        return Double.valueOf(cosineSimilarity);
    }

    private Set<CharSequence> getIntersection(Map<CharSequence, Integer> leftVector, Map<CharSequence, Integer> rightVector) {
        Set<CharSequence> intersection = new HashSet<>(leftVector.keySet());
        intersection.retainAll(rightVector.keySet());
        return intersection;
    }

    private double dot(Map<CharSequence, Integer> leftVector, Map<CharSequence, Integer> rightVector, Set<CharSequence> intersection) {
        long dotProduct = 0;
        for (CharSequence key : intersection) {
            dotProduct += (long) (rightVector.get(key).intValue() * leftVector.get(key).intValue());
        }
        return (double) dotProduct;
    }
}
