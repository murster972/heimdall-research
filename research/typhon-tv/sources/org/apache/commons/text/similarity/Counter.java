package org.apache.commons.text.similarity;

import java.util.HashMap;
import java.util.Map;

final class Counter {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Map<CharSequence, Integer> m20806(CharSequence[] tokens) {
        Map<CharSequence, Integer> innerCounter = new HashMap<>();
        for (CharSequence token : tokens) {
            if (innerCounter.containsKey(token)) {
                innerCounter.put(token, Integer.valueOf(innerCounter.get(token).intValue() + 1));
            } else {
                innerCounter.put(token, 1);
            }
        }
        return innerCounter;
    }
}
