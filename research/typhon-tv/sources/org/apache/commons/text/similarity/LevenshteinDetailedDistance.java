package org.apache.commons.text.similarity;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.lang.reflect.Array;
import java.util.Arrays;

public class LevenshteinDetailedDistance implements EditDistance<LevenshteinResults> {
    private static final LevenshteinDetailedDistance DEFAULT_INSTANCE = new LevenshteinDetailedDistance();
    private final Integer threshold;

    public LevenshteinDetailedDistance() {
        this((Integer) null);
    }

    public LevenshteinDetailedDistance(Integer threshold2) {
        if (threshold2 == null || threshold2.intValue() >= 0) {
            this.threshold = threshold2;
            return;
        }
        throw new IllegalArgumentException("Threshold must not be negative");
    }

    public LevenshteinResults apply(CharSequence left, CharSequence right) {
        if (this.threshold != null) {
            return limitedCompare(left, right, this.threshold.intValue());
        }
        return unlimitedCompare(left, right);
    }

    public static LevenshteinDetailedDistance getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public Integer getThreshold() {
        return this.threshold;
    }

    private static LevenshteinResults limitedCompare(CharSequence left, CharSequence right, int threshold2) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (threshold2 < 0) {
            throw new IllegalArgumentException("Threshold must not be negative");
        } else {
            int n = left.length();
            int m = right.length();
            if (n == 0) {
                if (m <= threshold2) {
                    return new LevenshteinResults(Integer.valueOf(m), Integer.valueOf(m), 0, 0);
                }
                return new LevenshteinResults(-1, 0, 0, 0);
            } else if (m == 0) {
                return n <= threshold2 ? new LevenshteinResults(Integer.valueOf(n), 0, Integer.valueOf(n), 0) : new LevenshteinResults(-1, 0, 0, 0);
            } else {
                boolean swapped = false;
                if (n > m) {
                    CharSequence tmp = left;
                    left = right;
                    right = tmp;
                    n = m;
                    m = right.length();
                    swapped = true;
                }
                int[] p = new int[(n + 1)];
                int[] d = new int[(n + 1)];
                int[][] matrix = (int[][]) Array.newInstance(Integer.TYPE, new int[]{m + 1, n + 1});
                for (int index = 0; index <= n; index++) {
                    matrix[0][index] = index;
                }
                for (int index2 = 0; index2 <= m; index2++) {
                    matrix[index2][0] = index2;
                }
                int boundary = Math.min(n, threshold2) + 1;
                for (int i = 0; i < boundary; i++) {
                    p[i] = i;
                }
                Arrays.fill(p, boundary, p.length, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                Arrays.fill(d, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                int j = 1;
                while (j <= m) {
                    char rightJ = right.charAt(j - 1);
                    d[0] = j;
                    int min = Math.max(1, j - threshold2);
                    int max = j > MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - threshold2 ? n : Math.min(n, j + threshold2);
                    if (min > max) {
                        return new LevenshteinResults(-1, 0, 0, 0);
                    }
                    if (min > 1) {
                        d[min - 1] = Integer.MAX_VALUE;
                    }
                    for (int i2 = min; i2 <= max; i2++) {
                        if (left.charAt(i2 - 1) == rightJ) {
                            d[i2] = p[i2 - 1];
                        } else {
                            d[i2] = Math.min(Math.min(d[i2 - 1], p[i2]), p[i2 - 1]) + 1;
                        }
                        matrix[j][i2] = d[i2];
                    }
                    int[] tempD = p;
                    p = d;
                    d = tempD;
                    j++;
                }
                if (p[n] <= threshold2) {
                    return findDetailedResults(left, right, matrix, swapped);
                }
                return new LevenshteinResults(-1, 0, 0, 0);
            }
        }
    }

    private static LevenshteinResults unlimitedCompare(CharSequence left, CharSequence right) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        int n = left.length();
        int m = right.length();
        if (n == 0) {
            return new LevenshteinResults(Integer.valueOf(m), Integer.valueOf(m), 0, 0);
        }
        if (m == 0) {
            return new LevenshteinResults(Integer.valueOf(n), 0, Integer.valueOf(n), 0);
        }
        boolean swapped = false;
        if (n > m) {
            CharSequence tmp = left;
            left = right;
            right = tmp;
            n = m;
            m = right.length();
            swapped = true;
        }
        int[] p = new int[(n + 1)];
        int[] d = new int[(n + 1)];
        int[][] matrix = (int[][]) Array.newInstance(Integer.TYPE, new int[]{m + 1, n + 1});
        for (int index = 0; index <= n; index++) {
            matrix[0][index] = index;
        }
        for (int index2 = 0; index2 <= m; index2++) {
            matrix[index2][0] = index2;
        }
        for (int i = 0; i <= n; i++) {
            p[i] = i;
        }
        for (int j = 1; j <= m; j++) {
            char rightJ = right.charAt(j - 1);
            d[0] = j;
            for (int i2 = 1; i2 <= n; i2++) {
                d[i2] = Math.min(Math.min(d[i2 - 1] + 1, p[i2] + 1), p[i2 - 1] + (left.charAt(i2 + -1) == rightJ ? 0 : 1));
                matrix[j][i2] = d[i2];
            }
            int[] tempD = p;
            p = d;
            d = tempD;
        }
        return findDetailedResults(left, right, matrix, swapped);
    }

    private static LevenshteinResults findDetailedResults(CharSequence left, CharSequence right, int[][] matrix, boolean swapped) {
        int dataAtLeft;
        int dataAtTop;
        int dataAtDiagonal;
        int delCount = 0;
        int addCount = 0;
        int subCount = 0;
        int rowIndex = right.length();
        int columnIndex = left.length();
        while (rowIndex >= 0 && columnIndex >= 0) {
            if (columnIndex == 0) {
                dataAtLeft = -1;
            } else {
                dataAtLeft = matrix[rowIndex][columnIndex - 1];
            }
            if (rowIndex == 0) {
                dataAtTop = -1;
            } else {
                dataAtTop = matrix[rowIndex - 1][columnIndex];
            }
            if (rowIndex <= 0 || columnIndex <= 0) {
                dataAtDiagonal = -1;
            } else {
                dataAtDiagonal = matrix[rowIndex - 1][columnIndex - 1];
            }
            if (dataAtLeft == -1 && dataAtTop == -1 && dataAtDiagonal == -1) {
                break;
            }
            int data = matrix[rowIndex][columnIndex];
            if (columnIndex > 0 && rowIndex > 0) {
                if (left.charAt(columnIndex - 1) == right.charAt(rowIndex - 1)) {
                    columnIndex--;
                    rowIndex--;
                }
            }
            boolean deleted = false;
            boolean added = false;
            if ((data - 1 == dataAtLeft && data <= dataAtDiagonal && data <= dataAtTop) || (dataAtDiagonal == -1 && dataAtTop == -1)) {
                columnIndex--;
                if (swapped) {
                    addCount++;
                    added = true;
                } else {
                    delCount++;
                    deleted = true;
                }
            } else if ((data - 1 == dataAtTop && data <= dataAtDiagonal && data <= dataAtLeft) || (dataAtDiagonal == -1 && dataAtLeft == -1)) {
                rowIndex--;
                if (swapped) {
                    delCount++;
                    deleted = true;
                } else {
                    addCount++;
                    added = true;
                }
            }
            if (!added && !deleted) {
                subCount++;
                columnIndex--;
                rowIndex--;
            }
        }
        return new LevenshteinResults(Integer.valueOf(addCount + delCount + subCount), Integer.valueOf(addCount), Integer.valueOf(delCount), Integer.valueOf(subCount));
    }
}
