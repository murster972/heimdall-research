package org.apache.commons.text.similarity;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.Arrays;

public class LevenshteinDistance implements EditDistance<Integer> {
    private static final LevenshteinDistance DEFAULT_INSTANCE = new LevenshteinDistance();
    private final Integer threshold;

    public LevenshteinDistance() {
        this((Integer) null);
    }

    public LevenshteinDistance(Integer threshold2) {
        if (threshold2 == null || threshold2.intValue() >= 0) {
            this.threshold = threshold2;
            return;
        }
        throw new IllegalArgumentException("Threshold must not be negative");
    }

    public Integer apply(CharSequence left, CharSequence right) {
        if (this.threshold != null) {
            return Integer.valueOf(limitedCompare(left, right, this.threshold.intValue()));
        }
        return Integer.valueOf(unlimitedCompare(left, right));
    }

    public static LevenshteinDistance getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public Integer getThreshold() {
        return this.threshold;
    }

    private static int limitedCompare(CharSequence left, CharSequence right, int threshold2) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (threshold2 < 0) {
            throw new IllegalArgumentException("Threshold must not be negative");
        } else {
            int n = left.length();
            int m = right.length();
            if (n == 0) {
                if (m <= threshold2) {
                    return m;
                }
                return -1;
            } else if (m != 0) {
                if (n > m) {
                    CharSequence tmp = left;
                    left = right;
                    right = tmp;
                    n = m;
                    m = right.length();
                }
                int[] p = new int[(n + 1)];
                int[] d = new int[(n + 1)];
                int boundary = Math.min(n, threshold2) + 1;
                for (int i = 0; i < boundary; i++) {
                    p[i] = i;
                }
                Arrays.fill(p, boundary, p.length, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                Arrays.fill(d, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                int j = 1;
                while (j <= m) {
                    char rightJ = right.charAt(j - 1);
                    d[0] = j;
                    int min = Math.max(1, j - threshold2);
                    int max = j > MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - threshold2 ? n : Math.min(n, j + threshold2);
                    if (min > max) {
                        return -1;
                    }
                    if (min > 1) {
                        d[min - 1] = Integer.MAX_VALUE;
                    }
                    for (int i2 = min; i2 <= max; i2++) {
                        if (left.charAt(i2 - 1) == rightJ) {
                            d[i2] = p[i2 - 1];
                        } else {
                            d[i2] = Math.min(Math.min(d[i2 - 1], p[i2]), p[i2 - 1]) + 1;
                        }
                    }
                    int[] tempD = p;
                    p = d;
                    d = tempD;
                    j++;
                }
                if (p[n] <= threshold2) {
                    return p[n];
                }
                return -1;
            } else if (n <= threshold2) {
                return n;
            } else {
                return -1;
            }
        }
    }

    private static int unlimitedCompare(CharSequence left, CharSequence right) {
        if (left == null || right == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        int n = left.length();
        int m = right.length();
        if (n == 0) {
            int i = m;
            int i2 = n;
            return m;
        } else if (m == 0) {
            int i3 = m;
            int i4 = n;
            return n;
        } else {
            if (n > m) {
                CharSequence tmp = left;
                left = right;
                right = tmp;
                n = m;
                m = right.length();
            }
            int[] p = new int[(n + 1)];
            for (int i5 = 0; i5 <= n; i5++) {
                p[i5] = i5;
            }
            for (int j = 1; j <= m; j++) {
                int upperLeft = p[0];
                char rightJ = right.charAt(j - 1);
                p[0] = j;
                for (int i6 = 1; i6 <= n; i6++) {
                    int upper = p[i6];
                    p[i6] = Math.min(Math.min(p[i6 - 1] + 1, p[i6] + 1), upperLeft + (left.charAt(i6 + -1) == rightJ ? 0 : 1));
                    upperLeft = upper;
                }
            }
            int i7 = m;
            int i8 = n;
            return p[n];
        }
    }
}
