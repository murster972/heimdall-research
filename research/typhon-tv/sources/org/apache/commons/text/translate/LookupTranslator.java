package org.apache.commons.text.translate;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.Writer;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class LookupTranslator extends CharSequenceTranslator {
    private final int longest;
    private final Map<String, String> lookupMap;
    private final HashSet<Character> prefixSet;
    private final int shortest;

    public LookupTranslator(Map<CharSequence, CharSequence> lookupMap2) {
        if (lookupMap2 == null) {
            throw new InvalidParameterException("lookupMap cannot be null");
        }
        this.lookupMap = new HashMap();
        this.prefixSet = new HashSet<>();
        int currentShortest = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        int currentLongest = 0;
        for (Map.Entry<CharSequence, CharSequence> pair : lookupMap2.entrySet()) {
            this.lookupMap.put(pair.getKey().toString(), pair.getValue().toString());
            this.prefixSet.add(Character.valueOf(pair.getKey().charAt(0)));
            int sz = pair.getKey().length();
            currentShortest = sz < currentShortest ? sz : currentShortest;
            if (sz > currentLongest) {
                currentLongest = sz;
            }
        }
        this.shortest = currentShortest;
        this.longest = currentLongest;
    }

    public int translate(CharSequence input, int index, Writer out) throws IOException {
        if (this.prefixSet.contains(Character.valueOf(input.charAt(index)))) {
            int max = this.longest;
            if (this.longest + index > input.length()) {
                max = input.length() - index;
            }
            for (int i = max; i >= this.shortest; i--) {
                String result = this.lookupMap.get(input.subSequence(index, index + i).toString());
                if (result != null) {
                    out.write(result);
                    return i;
                }
            }
        }
        return 0;
    }
}
