package org.apache.commons.text.translate;

import java.io.IOException;
import java.io.Writer;

abstract class SinglePassTranslator extends CharSequenceTranslator {
    /* access modifiers changed from: package-private */
    public abstract void translateWhole(CharSequence charSequence, Writer writer) throws IOException;

    SinglePassTranslator() {
    }

    public int translate(CharSequence input, int index, Writer out) throws IOException {
        if (index != 0) {
            throw new IllegalArgumentException(getClassName() + ".translate(final CharSequence input, final int index, final Writer out) can not handle a non-zero index.");
        }
        translateWhole(input, out);
        return Character.codePointCount(input, index, input.length());
    }

    private String getClassName() {
        Class<?> cls = getClass();
        return cls.isAnonymousClass() ? cls.getName() : cls.getSimpleName();
    }
}
