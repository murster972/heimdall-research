package org.apache.commons.text.translate;

import java.io.IOException;
import java.io.Writer;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

public final class CsvTranslators {
    private static final char CSV_DELIMITER = ',';
    /* access modifiers changed from: private */
    public static final String CSV_ESCAPED_QUOTE_STR = (CSV_QUOTE_STR + CSV_QUOTE_STR);
    private static final char CSV_QUOTE = '\"';
    /* access modifiers changed from: private */
    public static final String CSV_QUOTE_STR = String.valueOf(CSV_QUOTE);
    /* access modifiers changed from: private */
    public static final char[] CSV_SEARCH_CHARS = {CSV_DELIMITER, CSV_QUOTE, CharUtils.CR, 10};

    private CsvTranslators() {
    }

    public static class CsvEscaper extends SinglePassTranslator {
        public /* bridge */ /* synthetic */ int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
            return super.translate(charSequence, i, writer);
        }

        /* access modifiers changed from: package-private */
        public void translateWhole(CharSequence input, Writer out) throws IOException {
            String inputSting = input.toString();
            if (StringUtils.containsNone((CharSequence) inputSting, CsvTranslators.CSV_SEARCH_CHARS)) {
                out.write(inputSting);
                return;
            }
            out.write(34);
            out.write(StringUtils.replace(inputSting, CsvTranslators.CSV_QUOTE_STR, CsvTranslators.CSV_ESCAPED_QUOTE_STR));
            out.write(34);
        }
    }

    public static class CsvUnescaper extends SinglePassTranslator {
        public /* bridge */ /* synthetic */ int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
            return super.translate(charSequence, i, writer);
        }

        /* access modifiers changed from: package-private */
        public void translateWhole(CharSequence input, Writer out) throws IOException {
            if (input.charAt(0) == '\"' && input.charAt(input.length() - 1) == '\"') {
                String quoteless = input.subSequence(1, input.length() - 1).toString();
                if (StringUtils.containsAny((CharSequence) quoteless, CsvTranslators.CSV_SEARCH_CHARS)) {
                    out.write(StringUtils.replace(quoteless, CsvTranslators.CSV_ESCAPED_QUOTE_STR, CsvTranslators.CSV_QUOTE_STR));
                } else {
                    out.write(input.toString());
                }
            } else {
                out.write(input.toString());
            }
        }
    }
}
