package org.apache.commons.lang3.math;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.Validate;

public class NumberUtils {
    public static final Byte BYTE_MINUS_ONE = (byte) -1;
    public static final Byte BYTE_ONE = (byte) 1;
    public static final Byte BYTE_ZERO = (byte) 0;
    public static final Double DOUBLE_MINUS_ONE = Double.valueOf(-1.0d);
    public static final Double DOUBLE_ONE = Double.valueOf(1.0d);
    public static final Double DOUBLE_ZERO = Double.valueOf(0.0d);
    public static final Float FLOAT_MINUS_ONE = Float.valueOf(-1.0f);
    public static final Float FLOAT_ONE = Float.valueOf(1.0f);
    public static final Float FLOAT_ZERO = Float.valueOf(0.0f);
    public static final Integer INTEGER_MINUS_ONE = -1;
    public static final Integer INTEGER_ONE = 1;
    public static final Integer INTEGER_ZERO = 0;
    public static final Long LONG_MINUS_ONE = -1L;
    public static final Long LONG_ONE = 1L;
    public static final Long LONG_ZERO = 0L;
    public static final Short SHORT_MINUS_ONE = -1;
    public static final Short SHORT_ONE = 1;
    public static final Short SHORT_ZERO = 0;

    public static int compare(byte b, byte b2) {
        return b - b2;
    }

    public static int compare(int i, int i2) {
        if (i == i2) {
            return 0;
        }
        return i < i2 ? -1 : 1;
    }

    public static int compare(long j, long j2) {
        if (j == j2) {
            return 0;
        }
        return j < j2 ? -1 : 1;
    }

    public static int compare(short s, short s2) {
        if (s == s2) {
            return 0;
        }
        return s < s2 ? -1 : 1;
    }

    public static BigDecimal createBigDecimal(String str) {
        if (str == null) {
            return null;
        }
        if (StringUtils.isBlank(str)) {
            throw new NumberFormatException("A blank string is not a valid number");
        } else if (!str.trim().startsWith("--")) {
            return new BigDecimal(str);
        } else {
            throw new NumberFormatException(str + " is not a valid number.");
        }
    }

    public static BigInteger createBigInteger(String str) {
        if (str == null) {
            return null;
        }
        int i = 0;
        int i2 = 10;
        boolean z = false;
        if (str.startsWith("-")) {
            z = true;
            i = 1;
        }
        if (str.startsWith("0x", i) || str.startsWith("0X", i)) {
            i2 = 16;
            i += 2;
        } else if (str.startsWith("#", i)) {
            i2 = 16;
            i++;
        } else if (str.startsWith("0", i) && str.length() > i + 1) {
            i2 = 8;
            i++;
        }
        BigInteger bigInteger = new BigInteger(str.substring(i), i2);
        return z ? bigInteger.negate() : bigInteger;
    }

    public static Double createDouble(String str) {
        if (str == null) {
            return null;
        }
        return Double.valueOf(str);
    }

    public static Float createFloat(String str) {
        if (str == null) {
            return null;
        }
        return Float.valueOf(str);
    }

    public static Integer createInteger(String str) {
        if (str == null) {
            return null;
        }
        return Integer.decode(str);
    }

    public static Long createLong(String str) {
        if (str == null) {
            return null;
        }
        return Long.decode(str);
    }

    public static Number createNumber(String str) throws NumberFormatException {
        String mantissa;
        String str2;
        if (str == null) {
            return null;
        }
        if (StringUtils.isBlank(str)) {
            throw new NumberFormatException("A blank string is not a valid number");
        }
        int i = 0;
        String[] strArr = {"0x", "0X", "-0x", "-0X", "#", "-#"};
        int length = strArr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                break;
            }
            String str3 = strArr[i2];
            if (str.startsWith(str3)) {
                i = 0 + str3.length();
                break;
            }
            i2++;
        }
        if (i > 0) {
            char c = 0;
            int i3 = i;
            while (i3 < str.length() && (c = str.charAt(i3)) == '0') {
                i++;
                i3++;
            }
            int length2 = str.length() - i;
            return (length2 > 16 || (length2 == 16 && c > '7')) ? createBigInteger(str) : (length2 > 8 || (length2 == 8 && c > '7')) ? createLong(str) : createInteger(str);
        }
        char charAt = str.charAt(str.length() - 1);
        int indexOf = str.indexOf(46);
        int indexOf2 = str.indexOf(101) + str.indexOf(69) + 1;
        if (indexOf > -1) {
            if (indexOf2 <= -1) {
                str2 = str.substring(indexOf + 1);
            } else if (indexOf2 < indexOf || indexOf2 > str.length()) {
                throw new NumberFormatException(str + " is not a valid number.");
            } else {
                str2 = str.substring(indexOf + 1, indexOf2);
            }
            mantissa = getMantissa(str, indexOf);
        } else {
            if (indexOf2 <= -1) {
                mantissa = getMantissa(str);
            } else if (indexOf2 > str.length()) {
                throw new NumberFormatException(str + " is not a valid number.");
            } else {
                mantissa = getMantissa(str, indexOf2);
            }
            str2 = null;
        }
        if (Character.isDigit(charAt) || charAt == '.') {
            String substring = (indexOf2 <= -1 || indexOf2 >= str.length() + -1) ? null : str.substring(indexOf2 + 1, str.length());
            if (str2 == null && substring == null) {
                try {
                    return createInteger(str);
                } catch (NumberFormatException e) {
                    try {
                        return createLong(str);
                    } catch (NumberFormatException e2) {
                        return createBigInteger(str);
                    }
                }
            } else {
                boolean z = isAllZeros(mantissa) && isAllZeros(substring);
                try {
                    Float createFloat = createFloat(str);
                    Double createDouble = createDouble(str);
                    if (!createFloat.isInfinite() && ((createFloat.floatValue() != 0.0f || z) && createFloat.toString().equals(createDouble.toString()))) {
                        return createFloat;
                    }
                    if (!createDouble.isInfinite() && (createDouble.doubleValue() != 0.0d || z)) {
                        BigDecimal createBigDecimal = createBigDecimal(str);
                        return createBigDecimal.compareTo(BigDecimal.valueOf(createDouble.doubleValue())) == 0 ? createDouble : createBigDecimal;
                    }
                    return createBigDecimal(str);
                } catch (NumberFormatException e3) {
                }
            }
        } else {
            String substring2 = (indexOf2 <= -1 || indexOf2 >= str.length() + -1) ? null : str.substring(indexOf2 + 1, str.length() - 1);
            String substring3 = str.substring(0, str.length() - 1);
            boolean z2 = isAllZeros(mantissa) && isAllZeros(substring2);
            switch (charAt) {
                case 'D':
                case 'd':
                    break;
                case 'F':
                case 'f':
                    try {
                        Float createFloat2 = createFloat(str);
                        if (!createFloat2.isInfinite() && (createFloat2.floatValue() != 0.0f || z2)) {
                            return createFloat2;
                        }
                    } catch (NumberFormatException e4) {
                        break;
                    }
                case 'L':
                case 'l':
                    if (str2 == null && substring2 == null && ((substring3.charAt(0) == '-' && isDigits(substring3.substring(1))) || isDigits(substring3))) {
                        try {
                            return createLong(substring3);
                        } catch (NumberFormatException e5) {
                            return createBigInteger(substring3);
                        }
                    } else {
                        throw new NumberFormatException(str + " is not a valid number.");
                    }
            }
            try {
                Double createDouble2 = createDouble(str);
                if (!createDouble2.isInfinite() && (((double) createDouble2.floatValue()) != 0.0d || z2)) {
                    return createDouble2;
                }
            } catch (NumberFormatException e6) {
            }
            try {
                return createBigDecimal(substring3);
            } catch (NumberFormatException e7) {
            }
        }
        throw new NumberFormatException(str + " is not a valid number.");
    }

    private static String getMantissa(String str) {
        return getMantissa(str, str.length());
    }

    private static String getMantissa(String str, int i) {
        char charAt = str.charAt(0);
        return charAt == '-' || charAt == '+' ? str.substring(1, i) : str.substring(0, i);
    }

    private static boolean isAllZeros(String str) {
        if (str == null) {
            return true;
        }
        for (int length = str.length() - 1; length >= 0; length--) {
            if (str.charAt(length) != '0') {
                return false;
            }
        }
        return str.length() > 0;
    }

    public static boolean isCreatable(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        int i = (charArray[0] == '-' || charArray[0] == '+') ? 1 : 0;
        boolean z5 = i == 1 && charArray[0] == '+';
        if (length > i + 1 && charArray[i] == '0') {
            if (charArray[i + 1] == 'x' || charArray[i + 1] == 'X') {
                int i2 = i + 2;
                if (i2 == length) {
                    return false;
                }
                while (i2 < charArray.length) {
                    if ((charArray[i2] < '0' || charArray[i2] > '9') && ((charArray[i2] < 'a' || charArray[i2] > 'f') && (charArray[i2] < 'A' || charArray[i2] > 'F'))) {
                        return false;
                    }
                    i2++;
                }
                return true;
            } else if (Character.isDigit(charArray[i + 1])) {
                for (int i3 = i + 1; i3 < charArray.length; i3++) {
                    if (charArray[i3] < '0' || charArray[i3] > '7') {
                        return false;
                    }
                }
                return true;
            }
        }
        int i4 = length - 1;
        int i5 = i;
        while (true) {
            if (i5 < i4 || (i5 < i4 + 1 && z3 && !z4)) {
                if (charArray[i5] >= '0' && charArray[i5] <= '9') {
                    z4 = true;
                    z3 = false;
                } else if (charArray[i5] == '.') {
                    if (z2 || z) {
                        return false;
                    }
                    z2 = true;
                } else if (charArray[i5] == 'e' || charArray[i5] == 'E') {
                    if (z || !z4) {
                        return false;
                    }
                    z = true;
                    z3 = true;
                } else if ((charArray[i5] != '+' && charArray[i5] != '-') || !z3) {
                    return false;
                } else {
                    z3 = false;
                    z4 = false;
                }
                i5++;
            }
        }
        if (i5 >= charArray.length) {
            return !z3 && z4;
        } else if (charArray[i5] >= '0' && charArray[i5] <= '9') {
            return !SystemUtils.IS_JAVA_1_6 || !z5 || z2;
        } else {
            if (charArray[i5] == 'e' || charArray[i5] == 'E') {
                return false;
            }
            if (charArray[i5] == '.') {
                if (z2 || z) {
                    return false;
                }
                return z4;
            } else if (!z3 && (charArray[i5] == 'd' || charArray[i5] == 'D' || charArray[i5] == 'f' || charArray[i5] == 'F')) {
                return z4;
            } else {
                if (charArray[i5] != 'l' && charArray[i5] != 'L') {
                    return false;
                }
                return z4 && !z && !z2;
            }
        }
    }

    public static boolean isDigits(String str) {
        return StringUtils.isNumeric(str);
    }

    @Deprecated
    public static boolean isNumber(String str) {
        return isCreatable(str);
    }

    public static boolean isParsable(String str) {
        if (StringUtils.isEmpty(str) || str.charAt(str.length() - 1) == '.') {
            return false;
        }
        if (str.charAt(0) != '-') {
            return withDecimalsParsing(str, 0);
        }
        if (str.length() != 1) {
            return withDecimalsParsing(str, 1);
        }
        return false;
    }

    public static byte max(byte b, byte b2, byte b3) {
        if (b2 > b) {
            b = b2;
        }
        return b3 > b ? b3 : b;
    }

    public static byte max(byte... bArr) {
        validateArray(bArr);
        byte b = bArr[0];
        for (int i = 1; i < bArr.length; i++) {
            if (bArr[i] > b) {
                b = bArr[i];
            }
        }
        return b;
    }

    public static double max(double d, double d2, double d3) {
        return Math.max(Math.max(d, d2), d3);
    }

    public static double max(double... dArr) {
        validateArray(dArr);
        double d = dArr[0];
        for (int i = 1; i < dArr.length; i++) {
            if (Double.isNaN(dArr[i])) {
                return Double.NaN;
            }
            if (dArr[i] > d) {
                d = dArr[i];
            }
        }
        return d;
    }

    public static float max(float f, float f2, float f3) {
        return Math.max(Math.max(f, f2), f3);
    }

    public static float max(float... fArr) {
        validateArray(fArr);
        float f = fArr[0];
        for (int i = 1; i < fArr.length; i++) {
            if (Float.isNaN(fArr[i])) {
                return Float.NaN;
            }
            if (fArr[i] > f) {
                f = fArr[i];
            }
        }
        return f;
    }

    public static int max(int i, int i2, int i3) {
        if (i2 > i) {
            i = i2;
        }
        return i3 > i ? i3 : i;
    }

    public static int max(int... iArr) {
        validateArray(iArr);
        int i = iArr[0];
        for (int i2 = 1; i2 < iArr.length; i2++) {
            if (iArr[i2] > i) {
                i = iArr[i2];
            }
        }
        return i;
    }

    public static long max(long j, long j2, long j3) {
        if (j2 > j) {
            j = j2;
        }
        return j3 > j ? j3 : j;
    }

    public static long max(long... jArr) {
        validateArray(jArr);
        long j = jArr[0];
        for (int i = 1; i < jArr.length; i++) {
            if (jArr[i] > j) {
                j = jArr[i];
            }
        }
        return j;
    }

    public static short max(short s, short s2, short s3) {
        if (s2 > s) {
            s = s2;
        }
        return s3 > s ? s3 : s;
    }

    public static short max(short... sArr) {
        validateArray(sArr);
        short s = sArr[0];
        for (int i = 1; i < sArr.length; i++) {
            if (sArr[i] > s) {
                s = sArr[i];
            }
        }
        return s;
    }

    public static byte min(byte b, byte b2, byte b3) {
        if (b2 < b) {
            b = b2;
        }
        return b3 < b ? b3 : b;
    }

    public static byte min(byte... bArr) {
        validateArray(bArr);
        byte b = bArr[0];
        for (int i = 1; i < bArr.length; i++) {
            if (bArr[i] < b) {
                b = bArr[i];
            }
        }
        return b;
    }

    public static double min(double d, double d2, double d3) {
        return Math.min(Math.min(d, d2), d3);
    }

    public static double min(double... dArr) {
        validateArray(dArr);
        double d = dArr[0];
        for (int i = 1; i < dArr.length; i++) {
            if (Double.isNaN(dArr[i])) {
                return Double.NaN;
            }
            if (dArr[i] < d) {
                d = dArr[i];
            }
        }
        return d;
    }

    public static float min(float f, float f2, float f3) {
        return Math.min(Math.min(f, f2), f3);
    }

    public static float min(float... fArr) {
        validateArray(fArr);
        float f = fArr[0];
        for (int i = 1; i < fArr.length; i++) {
            if (Float.isNaN(fArr[i])) {
                return Float.NaN;
            }
            if (fArr[i] < f) {
                f = fArr[i];
            }
        }
        return f;
    }

    public static int min(int i, int i2, int i3) {
        if (i2 < i) {
            i = i2;
        }
        return i3 < i ? i3 : i;
    }

    public static int min(int... iArr) {
        validateArray(iArr);
        int i = iArr[0];
        for (int i2 = 1; i2 < iArr.length; i2++) {
            if (iArr[i2] < i) {
                i = iArr[i2];
            }
        }
        return i;
    }

    public static long min(long j, long j2, long j3) {
        if (j2 < j) {
            j = j2;
        }
        return j3 < j ? j3 : j;
    }

    public static long min(long... jArr) {
        validateArray(jArr);
        long j = jArr[0];
        for (int i = 1; i < jArr.length; i++) {
            if (jArr[i] < j) {
                j = jArr[i];
            }
        }
        return j;
    }

    public static short min(short s, short s2, short s3) {
        if (s2 < s) {
            s = s2;
        }
        return s3 < s ? s3 : s;
    }

    public static short min(short... sArr) {
        validateArray(sArr);
        short s = sArr[0];
        for (int i = 1; i < sArr.length; i++) {
            if (sArr[i] < s) {
                s = sArr[i];
            }
        }
        return s;
    }

    public static byte toByte(String str) {
        return toByte(str, (byte) 0);
    }

    public static byte toByte(String str, byte b) {
        if (str == null) {
            return b;
        }
        try {
            return Byte.parseByte(str);
        } catch (NumberFormatException e) {
            return b;
        }
    }

    public static double toDouble(String str) {
        return toDouble(str, 0.0d);
    }

    public static double toDouble(String str, double d) {
        if (str == null) {
            return d;
        }
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return d;
        }
    }

    public static float toFloat(String str) {
        return toFloat(str, 0.0f);
    }

    public static float toFloat(String str, float f) {
        if (str == null) {
            return f;
        }
        try {
            return Float.parseFloat(str);
        } catch (NumberFormatException e) {
            return f;
        }
    }

    public static int toInt(String str) {
        return toInt(str, 0);
    }

    public static int toInt(String str, int i) {
        if (str == null) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return i;
        }
    }

    public static long toLong(String str) {
        return toLong(str, 0);
    }

    public static long toLong(String str, long j) {
        if (str == null) {
            return j;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return j;
        }
    }

    public static short toShort(String str) {
        return toShort(str, 0);
    }

    public static short toShort(String str, short s) {
        if (str == null) {
            return s;
        }
        try {
            return Short.parseShort(str);
        } catch (NumberFormatException e) {
            return s;
        }
    }

    private static void validateArray(Object obj) {
        boolean z = true;
        Validate.isTrue(obj != null, "The Array must not be null", new Object[0]);
        if (Array.getLength(obj) == 0) {
            z = false;
        }
        Validate.isTrue(z, "Array cannot be empty.", new Object[0]);
    }

    private static boolean withDecimalsParsing(String str, int i) {
        int i2 = 0;
        for (int i3 = i; i3 < str.length(); i3++) {
            boolean z = str.charAt(i3) == '.';
            if (z) {
                i2++;
            }
            if (i2 > 1) {
                return false;
            }
            if (!z && !Character.isDigit(str.charAt(i3))) {
                return false;
            }
        }
        return true;
    }
}
