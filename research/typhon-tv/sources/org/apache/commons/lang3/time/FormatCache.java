package org.apache.commons.lang3.time;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.commons.lang3.Validate;

abstract class FormatCache<F extends Format> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentMap<MultipartKey, String> f16624 = new ConcurrentHashMap(7);

    /* renamed from: 龘  reason: contains not printable characters */
    private final ConcurrentMap<MultipartKey, F> f16625 = new ConcurrentHashMap(7);

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract F m20778(String str, TimeZone timeZone, Locale locale);

    FormatCache() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public F m20780() {
        return m20781(3, 3, TimeZone.getDefault(), Locale.getDefault());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public F m20779(String pattern, TimeZone timeZone, Locale locale) {
        Validate.notNull(pattern, "pattern must not be null", new Object[0]);
        if (timeZone == null) {
            timeZone = TimeZone.getDefault();
        }
        if (locale == null) {
            locale = Locale.getDefault();
        }
        MultipartKey key = new MultipartKey(pattern, timeZone, locale);
        F format = (Format) this.f16625.get(key);
        if (format != null) {
            return format;
        }
        F format2 = m20778(pattern, timeZone, locale);
        F previousValue = (Format) this.f16625.putIfAbsent(key, format2);
        if (previousValue != null) {
            return previousValue;
        }
        return format2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private F m20776(Integer dateStyle, Integer timeStyle, TimeZone timeZone, Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        return m20779(m20775(dateStyle, timeStyle, locale), timeZone, locale);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public F m20781(int dateStyle, int timeStyle, TimeZone timeZone, Locale locale) {
        return m20776(Integer.valueOf(dateStyle), Integer.valueOf(timeStyle), timeZone, locale);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public F m20782(int dateStyle, TimeZone timeZone, Locale locale) {
        return m20776(Integer.valueOf(dateStyle), (Integer) null, timeZone, locale);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public F m20777(int timeStyle, TimeZone timeZone, Locale locale) {
        return m20776((Integer) null, Integer.valueOf(timeStyle), timeZone, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m20775(Integer dateStyle, Integer timeStyle, Locale locale) {
        DateFormat formatter;
        MultipartKey key = new MultipartKey(dateStyle, timeStyle, locale);
        String pattern = (String) f16624.get(key);
        if (pattern != null) {
            return pattern;
        }
        if (dateStyle == null) {
            try {
                formatter = DateFormat.getTimeInstance(timeStyle.intValue(), locale);
            } catch (ClassCastException e) {
                throw new IllegalArgumentException("No date time pattern for locale: " + locale);
            }
        } else if (timeStyle == null) {
            formatter = DateFormat.getDateInstance(dateStyle.intValue(), locale);
        } else {
            formatter = DateFormat.getDateTimeInstance(dateStyle.intValue(), timeStyle.intValue(), locale);
        }
        String pattern2 = ((SimpleDateFormat) formatter).toPattern();
        String previous = f16624.putIfAbsent(key, pattern2);
        if (previous != null) {
            return previous;
        }
        return pattern2;
    }

    private static class MultipartKey {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f16626;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object[] f16627;

        MultipartKey(Object... keys) {
            this.f16627 = keys;
        }

        public boolean equals(Object obj) {
            return Arrays.equals(this.f16627, ((MultipartKey) obj).f16627);
        }

        public int hashCode() {
            if (this.f16626 == 0) {
                int rc = 0;
                for (Object key : this.f16627) {
                    if (key != null) {
                        rc = (rc * 7) + key.hashCode();
                    }
                }
                this.f16626 = rc;
            }
            return this.f16626;
        }
    }
}
