package org.apache.commons.lang3.time;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;

public class DateUtils {
    public static final long MILLIS_PER_DAY = 86400000;
    public static final long MILLIS_PER_HOUR = 3600000;
    public static final long MILLIS_PER_MINUTE = 60000;
    public static final long MILLIS_PER_SECOND = 1000;
    public static final int RANGE_MONTH_MONDAY = 6;
    public static final int RANGE_MONTH_SUNDAY = 5;
    public static final int RANGE_WEEK_CENTER = 4;
    public static final int RANGE_WEEK_MONDAY = 2;
    public static final int RANGE_WEEK_RELATIVE = 3;
    public static final int RANGE_WEEK_SUNDAY = 1;
    public static final int SEMI_MONTH = 1001;
    private static final int[][] fields = {new int[]{14}, new int[]{13}, new int[]{12}, new int[]{11, 10}, new int[]{5, 5, 9}, new int[]{2, 1001}, new int[]{1}, new int[]{0}};

    private enum ModifyType {
        TRUNCATE,
        ROUND,
        CEILING
    }

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (cal1.get(0) == cal2.get(0) && cal1.get(1) == cal2.get(1) && cal1.get(6) == cal2.get(6)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSameInstant(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            return date1.getTime() == date2.getTime();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameInstant(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.getTime().getTime() == cal2.getTime().getTime();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameLocalTime(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (cal1.get(14) == cal2.get(14) && cal1.get(13) == cal2.get(13) && cal1.get(12) == cal2.get(12) && cal1.get(11) == cal2.get(11) && cal1.get(6) == cal2.get(6) && cal1.get(1) == cal2.get(1) && cal1.get(0) == cal2.get(0) && cal1.getClass() == cal2.getClass()) {
            return true;
        } else {
            return false;
        }
    }

    public static Date parseDate(String str, String... parsePatterns) throws ParseException {
        return parseDate(str, (Locale) null, parsePatterns);
    }

    public static Date parseDate(String str, Locale locale, String... parsePatterns) throws ParseException {
        return parseDateWithLeniency(str, locale, parsePatterns, true);
    }

    public static Date parseDateStrictly(String str, String... parsePatterns) throws ParseException {
        return parseDateStrictly(str, (Locale) null, parsePatterns);
    }

    public static Date parseDateStrictly(String str, Locale locale, String... parsePatterns) throws ParseException {
        return parseDateWithLeniency(str, locale, parsePatterns, false);
    }

    private static Date parseDateWithLeniency(String str, Locale locale, String[] parsePatterns, boolean lenient) throws ParseException {
        Locale lcl;
        if (str == null || parsePatterns == null) {
            throw new IllegalArgumentException("Date and Patterns must not be null");
        }
        TimeZone tz = TimeZone.getDefault();
        if (locale == null) {
            lcl = Locale.getDefault();
        } else {
            lcl = locale;
        }
        ParsePosition pos = new ParsePosition(0);
        Calendar calendar = Calendar.getInstance(tz, lcl);
        calendar.setLenient(lenient);
        for (String parsePattern : parsePatterns) {
            FastDateParser fdp = new FastDateParser(parsePattern, tz, lcl);
            calendar.clear();
            try {
                if (fdp.parse(str, pos, calendar) && pos.getIndex() == str.length()) {
                    return calendar.getTime();
                }
            } catch (IllegalArgumentException e) {
            }
            pos.setIndex(0);
        }
        throw new ParseException("Unable to parse the date: " + str, -1);
    }

    public static Date addYears(Date date, int amount) {
        return add(date, 1, amount);
    }

    public static Date addMonths(Date date, int amount) {
        return add(date, 2, amount);
    }

    public static Date addWeeks(Date date, int amount) {
        return add(date, 3, amount);
    }

    public static Date addDays(Date date, int amount) {
        return add(date, 5, amount);
    }

    public static Date addHours(Date date, int amount) {
        return add(date, 11, amount);
    }

    public static Date addMinutes(Date date, int amount) {
        return add(date, 12, amount);
    }

    public static Date addSeconds(Date date, int amount) {
        return add(date, 13, amount);
    }

    public static Date addMilliseconds(Date date, int amount) {
        return add(date, 14, amount);
    }

    private static Date add(Date date, int calendarField, int amount) {
        validateDateNotNull(date);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }

    public static Date setYears(Date date, int amount) {
        return set(date, 1, amount);
    }

    public static Date setMonths(Date date, int amount) {
        return set(date, 2, amount);
    }

    public static Date setDays(Date date, int amount) {
        return set(date, 5, amount);
    }

    public static Date setHours(Date date, int amount) {
        return set(date, 11, amount);
    }

    public static Date setMinutes(Date date, int amount) {
        return set(date, 12, amount);
    }

    public static Date setSeconds(Date date, int amount) {
        return set(date, 13, amount);
    }

    public static Date setMilliseconds(Date date, int amount) {
        return set(date, 14, amount);
    }

    private static Date set(Date date, int calendarField, int amount) {
        validateDateNotNull(date);
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(calendarField, amount);
        return c.getTime();
    }

    public static Calendar toCalendar(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static Calendar toCalendar(Date date, TimeZone tz) {
        Calendar c = Calendar.getInstance(tz);
        c.setTime(date);
        return c;
    }

    public static Date round(Date date, int field) {
        validateDateNotNull(date);
        Calendar gval = Calendar.getInstance();
        gval.setTime(date);
        modify(gval, field, ModifyType.ROUND);
        return gval.getTime();
    }

    public static Calendar round(Calendar date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar rounded = (Calendar) date.clone();
        modify(rounded, field, ModifyType.ROUND);
        return rounded;
    }

    public static Date round(Object date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date instanceof Date) {
            return round((Date) date, field);
        } else {
            if (date instanceof Calendar) {
                return round((Calendar) date, field).getTime();
            }
            throw new ClassCastException("Could not round " + date);
        }
    }

    public static Date truncate(Date date, int field) {
        validateDateNotNull(date);
        Calendar gval = Calendar.getInstance();
        gval.setTime(date);
        modify(gval, field, ModifyType.TRUNCATE);
        return gval.getTime();
    }

    public static Calendar truncate(Calendar date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar truncated = (Calendar) date.clone();
        modify(truncated, field, ModifyType.TRUNCATE);
        return truncated;
    }

    public static Date truncate(Object date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date instanceof Date) {
            return truncate((Date) date, field);
        } else {
            if (date instanceof Calendar) {
                return truncate((Calendar) date, field).getTime();
            }
            throw new ClassCastException("Could not truncate " + date);
        }
    }

    public static Date ceiling(Date date, int field) {
        validateDateNotNull(date);
        Calendar gval = Calendar.getInstance();
        gval.setTime(date);
        modify(gval, field, ModifyType.CEILING);
        return gval.getTime();
    }

    public static Calendar ceiling(Calendar date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar ceiled = (Calendar) date.clone();
        modify(ceiled, field, ModifyType.CEILING);
        return ceiled;
    }

    public static Date ceiling(Object date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date instanceof Date) {
            return ceiling((Date) date, field);
        } else {
            if (date instanceof Calendar) {
                return ceiling((Calendar) date, field).getTime();
            }
            throw new ClassCastException("Could not find ceiling of for type: " + date.getClass());
        }
    }

    private static void modify(Calendar val, int field, ModifyType modType) {
        if (val.get(1) > 280000000) {
            throw new ArithmeticException("Calendar value too large for accurate calculations");
        } else if (field != 14) {
            Date date = val.getTime();
            long time = date.getTime();
            boolean done = false;
            int millisecs = val.get(14);
            if (ModifyType.TRUNCATE == modType || millisecs < 500) {
                time -= (long) millisecs;
            }
            if (field == 13) {
                done = true;
            }
            int seconds = val.get(13);
            if (!done && (ModifyType.TRUNCATE == modType || seconds < 30)) {
                time -= ((long) seconds) * 1000;
            }
            if (field == 12) {
                done = true;
            }
            int minutes = val.get(12);
            if (!done && (ModifyType.TRUNCATE == modType || minutes < 30)) {
                time -= ((long) minutes) * 60000;
            }
            if (date.getTime() != time) {
                date.setTime(time);
                val.setTime(date);
            }
            boolean roundUp = false;
            for (int[] aField : fields) {
                int[] arr$ = aField;
                int len$ = arr$.length;
                int i$ = 0;
                while (i$ < len$) {
                    if (arr$[i$] != field) {
                        i$++;
                    } else if (modType != ModifyType.CEILING && (modType != ModifyType.ROUND || !roundUp)) {
                        return;
                    } else {
                        if (field == 1001) {
                            if (val.get(5) == 1) {
                                val.add(5, 15);
                                return;
                            }
                            val.add(5, -15);
                            val.add(2, 1);
                            return;
                        } else if (field != 9) {
                            val.add(aField[0], 1);
                            return;
                        } else if (val.get(11) == 0) {
                            val.add(11, 12);
                            return;
                        } else {
                            val.add(11, -12);
                            val.add(5, 1);
                            return;
                        }
                    }
                }
                int offset = 0;
                boolean offsetSet = false;
                switch (field) {
                    case 9:
                        if (aField[0] == 11) {
                            int offset2 = val.get(11);
                            if (offset2 >= 12) {
                                offset2 -= 12;
                            }
                            roundUp = offset >= 6;
                            offsetSet = true;
                            break;
                        }
                        break;
                    case 1001:
                        if (aField[0] == 5) {
                            offset = val.get(5) - 1;
                            if (offset >= 15) {
                                offset -= 15;
                            }
                            roundUp = offset > 7;
                            offsetSet = true;
                            break;
                        }
                        break;
                }
                if (!offsetSet) {
                    int min = val.getActualMinimum(aField[0]);
                    int max = val.getActualMaximum(aField[0]);
                    offset = val.get(aField[0]) - min;
                    roundUp = offset > (max - min) / 2;
                }
                if (offset != 0) {
                    val.set(aField[0], val.get(aField[0]) - offset);
                }
            }
            throw new IllegalArgumentException("The field " + field + " is not supported");
        }
    }

    public static Iterator<Calendar> iterator(Date focus, int rangeStyle) {
        validateDateNotNull(focus);
        Calendar gval = Calendar.getInstance();
        gval.setTime(focus);
        return iterator(gval, rangeStyle);
    }

    public static Iterator<Calendar> iterator(Calendar focus, int rangeStyle) {
        Calendar start;
        Calendar end;
        if (focus == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        int startCutoff = 1;
        int endCutoff = 7;
        switch (rangeStyle) {
            case 1:
            case 2:
            case 3:
            case 4:
                start = truncate(focus, 5);
                end = truncate(focus, 5);
                switch (rangeStyle) {
                    case 2:
                        startCutoff = 2;
                        endCutoff = 1;
                        break;
                    case 3:
                        startCutoff = focus.get(7);
                        endCutoff = startCutoff - 1;
                        break;
                    case 4:
                        startCutoff = focus.get(7) - 3;
                        endCutoff = focus.get(7) + 3;
                        break;
                }
            case 5:
            case 6:
                start = truncate(focus, 2);
                end = (Calendar) start.clone();
                end.add(2, 1);
                end.add(5, -1);
                if (rangeStyle == 6) {
                    startCutoff = 2;
                    endCutoff = 1;
                    break;
                }
                break;
            default:
                throw new IllegalArgumentException("The range style " + rangeStyle + " is not valid.");
        }
        if (startCutoff < 1) {
            startCutoff += 7;
        }
        if (startCutoff > 7) {
            startCutoff -= 7;
        }
        if (endCutoff < 1) {
            endCutoff += 7;
        }
        if (endCutoff > 7) {
            endCutoff -= 7;
        }
        while (start.get(7) != startCutoff) {
            start.add(5, -1);
        }
        while (end.get(7) != endCutoff) {
            end.add(5, 1);
        }
        return new DateIterator(start, end);
    }

    public static Iterator<?> iterator(Object focus, int rangeStyle) {
        if (focus == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (focus instanceof Date) {
            return iterator((Date) focus, rangeStyle);
        } else {
            if (focus instanceof Calendar) {
                return iterator((Calendar) focus, rangeStyle);
            }
            throw new ClassCastException("Could not iterate based on " + focus);
        }
    }

    public static long getFragmentInMilliseconds(Date date, int fragment) {
        return getFragment(date, fragment, TimeUnit.MILLISECONDS);
    }

    public static long getFragmentInSeconds(Date date, int fragment) {
        return getFragment(date, fragment, TimeUnit.SECONDS);
    }

    public static long getFragmentInMinutes(Date date, int fragment) {
        return getFragment(date, fragment, TimeUnit.MINUTES);
    }

    public static long getFragmentInHours(Date date, int fragment) {
        return getFragment(date, fragment, TimeUnit.HOURS);
    }

    public static long getFragmentInDays(Date date, int fragment) {
        return getFragment(date, fragment, TimeUnit.DAYS);
    }

    public static long getFragmentInMilliseconds(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, TimeUnit.MILLISECONDS);
    }

    public static long getFragmentInSeconds(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, TimeUnit.SECONDS);
    }

    public static long getFragmentInMinutes(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, TimeUnit.MINUTES);
    }

    public static long getFragmentInHours(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, TimeUnit.HOURS);
    }

    public static long getFragmentInDays(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, TimeUnit.DAYS);
    }

    private static long getFragment(Date date, int fragment, TimeUnit unit) {
        validateDateNotNull(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getFragment(calendar, fragment, unit);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0067, code lost:
        r2 = r2 + r8.convert((long) r6.get(12), java.util.concurrent.TimeUnit.MINUTES);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0075, code lost:
        r2 = r2 + r8.convert((long) r6.get(13), java.util.concurrent.TimeUnit.SECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return r2 + r8.convert((long) r6.get(14), java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long getFragment(java.util.Calendar r6, int r7, java.util.concurrent.TimeUnit r8) {
        /*
            if (r6 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "The date must not be null"
            r1.<init>(r4)
            throw r1
        L_0x000b:
            r2 = 0
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.DAYS
            if (r8 != r1) goto L_0x0039
            r0 = 0
        L_0x0012:
            switch(r7) {
                case 1: goto L_0x003b;
                case 2: goto L_0x004a;
                default: goto L_0x0015;
            }
        L_0x0015:
            switch(r7) {
                case 1: goto L_0x0059;
                case 2: goto L_0x0059;
                case 3: goto L_0x0018;
                case 4: goto L_0x0018;
                case 5: goto L_0x0059;
                case 6: goto L_0x0059;
                case 7: goto L_0x0018;
                case 8: goto L_0x0018;
                case 9: goto L_0x0018;
                case 10: goto L_0x0018;
                case 11: goto L_0x0067;
                case 12: goto L_0x0075;
                case 13: goto L_0x0083;
                case 14: goto L_0x0091;
                default: goto L_0x0018;
            }
        L_0x0018:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "The fragment "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r5 = " is not supported"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.<init>(r4)
            throw r1
        L_0x0039:
            r0 = 1
            goto L_0x0012
        L_0x003b:
            r1 = 6
            int r1 = r6.get(r1)
            int r1 = r1 - r0
            long r4 = (long) r1
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.DAYS
            long r4 = r8.convert(r4, r1)
            long r2 = r2 + r4
            goto L_0x0015
        L_0x004a:
            r1 = 5
            int r1 = r6.get(r1)
            int r1 = r1 - r0
            long r4 = (long) r1
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.DAYS
            long r4 = r8.convert(r4, r1)
            long r2 = r2 + r4
            goto L_0x0015
        L_0x0059:
            r1 = 11
            int r1 = r6.get(r1)
            long r4 = (long) r1
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.HOURS
            long r4 = r8.convert(r4, r1)
            long r2 = r2 + r4
        L_0x0067:
            r1 = 12
            int r1 = r6.get(r1)
            long r4 = (long) r1
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.MINUTES
            long r4 = r8.convert(r4, r1)
            long r2 = r2 + r4
        L_0x0075:
            r1 = 13
            int r1 = r6.get(r1)
            long r4 = (long) r1
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = r8.convert(r4, r1)
            long r2 = r2 + r4
        L_0x0083:
            r1 = 14
            int r1 = r6.get(r1)
            long r4 = (long) r1
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r4 = r8.convert(r4, r1)
            long r2 = r2 + r4
        L_0x0091:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.time.DateUtils.getFragment(java.util.Calendar, int, java.util.concurrent.TimeUnit):long");
    }

    public static boolean truncatedEquals(Calendar cal1, Calendar cal2, int field) {
        return truncatedCompareTo(cal1, cal2, field) == 0;
    }

    public static boolean truncatedEquals(Date date1, Date date2, int field) {
        return truncatedCompareTo(date1, date2, field) == 0;
    }

    public static int truncatedCompareTo(Calendar cal1, Calendar cal2, int field) {
        return truncate(cal1, field).compareTo(truncate(cal2, field));
    }

    public static int truncatedCompareTo(Date date1, Date date2, int field) {
        return truncate(date1, field).compareTo(truncate(date2, field));
    }

    private static void validateDateNotNull(Date date) {
        boolean z;
        if (date != null) {
            z = true;
        } else {
            z = false;
        }
        Validate.isTrue(z, "The date must not be null", new Object[0]);
    }

    static class DateIterator implements Iterator<Calendar> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Calendar f16569;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Calendar f16570;

        DateIterator(Calendar startFinal, Calendar endFinal) {
            this.f16570 = endFinal;
            this.f16569 = startFinal;
            this.f16569.add(5, -1);
        }

        public boolean hasNext() {
            return this.f16569.before(this.f16570);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Calendar next() {
            if (this.f16569.equals(this.f16570)) {
                throw new NoSuchElementException();
            }
            this.f16569.add(5, 1);
            return (Calendar) this.f16569.clone();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
