package org.apache.commons.lang3.time;

import java.util.Date;
import java.util.TimeZone;

class GmtTimeZone extends TimeZone {
    static final long serialVersionUID = 1;
    private final int offset;
    private final String zoneId;

    GmtTimeZone(boolean negate, int hours, int minutes) {
        if (hours >= 24) {
            throw new IllegalArgumentException(hours + " hours out of range");
        } else if (minutes >= 60) {
            throw new IllegalArgumentException(minutes + " minutes out of range");
        } else {
            int milliseconds = ((hours * 60) + minutes) * 60000;
            this.offset = negate ? -milliseconds : milliseconds;
            this.zoneId = m20783(m20783(new StringBuilder(9).append(TimeZones.GMT_ID).append(negate ? '-' : '+'), hours).append(':'), minutes).toString();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static StringBuilder m20783(StringBuilder sb, int n) {
        return sb.append((char) ((n / 10) + 48)).append((char) ((n % 10) + 48));
    }

    public int getOffset(int era, int year, int month, int day, int dayOfWeek, int milliseconds) {
        return this.offset;
    }

    public void setRawOffset(int offsetMillis) {
        throw new UnsupportedOperationException();
    }

    public int getRawOffset() {
        return this.offset;
    }

    public String getID() {
        return this.zoneId;
    }

    public boolean useDaylightTime() {
        return false;
    }

    public boolean inDaylightTime(Date date) {
        return false;
    }

    public String toString() {
        return "[GmtTimeZone id=\"" + this.zoneId + "\",offset=" + this.offset + ']';
    }

    public int hashCode() {
        return this.offset;
    }

    public boolean equals(Object other) {
        if ((other instanceof GmtTimeZone) && this.zoneId == ((GmtTimeZone) other).zoneId) {
            return true;
        }
        return false;
    }
}
