package org.apache.commons.lang3.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FastDateParser implements Serializable, DateParser {
    private static final Strategy ABBREVIATED_YEAR_STRATEGY = new NumberStrategy(1) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20703(FastDateParser parser, int iValue) {
            return iValue < 100 ? parser.adjustYear(iValue) : iValue;
        }
    };
    private static final Strategy DAY_OF_MONTH_STRATEGY = new NumberStrategy(5);
    private static final Strategy DAY_OF_WEEK_IN_MONTH_STRATEGY = new NumberStrategy(8);
    private static final Strategy DAY_OF_WEEK_STRATEGY = new NumberStrategy(7) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20705(FastDateParser parser, int iValue) {
            if (iValue != 7) {
                return iValue + 1;
            }
            return 1;
        }
    };
    private static final Strategy DAY_OF_YEAR_STRATEGY = new NumberStrategy(6);
    private static final Strategy HOUR12_STRATEGY = new NumberStrategy(10) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20707(FastDateParser parser, int iValue) {
            if (iValue == 12) {
                return 0;
            }
            return iValue;
        }
    };
    private static final Strategy HOUR24_OF_DAY_STRATEGY = new NumberStrategy(11) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20706(FastDateParser parser, int iValue) {
            if (iValue == 24) {
                return 0;
            }
            return iValue;
        }
    };
    private static final Strategy HOUR_OF_DAY_STRATEGY = new NumberStrategy(11);
    private static final Strategy HOUR_STRATEGY = new NumberStrategy(10);
    static final Locale JAPANESE_IMPERIAL = new Locale("ja", "JP", "JP");
    private static final Strategy LITERAL_YEAR_STRATEGY = new NumberStrategy(1);
    /* access modifiers changed from: private */
    public static final Comparator<String> LONGER_FIRST_LOWERCASE = new Comparator<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(String left, String right) {
            return right.compareTo(left);
        }
    };
    private static final Strategy MILLISECOND_STRATEGY = new NumberStrategy(14);
    private static final Strategy MINUTE_STRATEGY = new NumberStrategy(12);
    private static final Strategy NUMBER_MONTH_STRATEGY = new NumberStrategy(2) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20704(FastDateParser parser, int iValue) {
            return iValue - 1;
        }
    };
    private static final Strategy SECOND_STRATEGY = new NumberStrategy(13);
    private static final Strategy WEEK_OF_MONTH_STRATEGY = new NumberStrategy(4);
    private static final Strategy WEEK_OF_YEAR_STRATEGY = new NumberStrategy(3);
    private static final ConcurrentMap<Locale, Strategy>[] caches = new ConcurrentMap[17];
    private static final long serialVersionUID = 3;
    private final int century;
    private final Locale locale;
    /* access modifiers changed from: private */
    public final String pattern;
    private transient List<StrategyAndWidth> patterns;
    private final int startYear;
    private final TimeZone timeZone;

    protected FastDateParser(String pattern2, TimeZone timeZone2, Locale locale2) {
        this(pattern2, timeZone2, locale2, (Date) null);
    }

    protected FastDateParser(String pattern2, TimeZone timeZone2, Locale locale2, Date centuryStart) {
        int centuryStartYear;
        this.pattern = pattern2;
        this.timeZone = timeZone2;
        this.locale = locale2;
        Calendar definingCalendar = Calendar.getInstance(timeZone2, locale2);
        if (centuryStart != null) {
            definingCalendar.setTime(centuryStart);
            centuryStartYear = definingCalendar.get(1);
        } else if (locale2.equals(JAPANESE_IMPERIAL)) {
            centuryStartYear = 0;
        } else {
            definingCalendar.setTime(new Date());
            centuryStartYear = definingCalendar.get(1) - 80;
        }
        this.century = (centuryStartYear / 100) * 100;
        this.startYear = centuryStartYear - this.century;
        init(definingCalendar);
    }

    private void init(Calendar definingCalendar) {
        this.patterns = new ArrayList();
        StrategyParser fm = new StrategyParser(definingCalendar);
        while (true) {
            StrategyAndWidth field = fm.m20727();
            if (field != null) {
                this.patterns.add(field);
            } else {
                return;
            }
        }
    }

    private static class StrategyAndWidth {

        /* renamed from: 靐  reason: contains not printable characters */
        final int f16586;

        /* renamed from: 龘  reason: contains not printable characters */
        final Strategy f16587;

        StrategyAndWidth(Strategy strategy, int width) {
            this.f16587 = strategy;
            this.f16586 = width;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20724(ListIterator<StrategyAndWidth> lt) {
            if (!this.f16587.m20722() || !lt.hasNext()) {
                return 0;
            }
            Strategy nextStrategy = lt.next().f16587;
            lt.previous();
            if (nextStrategy.m20722()) {
                return this.f16586;
            }
            return 0;
        }
    }

    private class StrategyParser {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Calendar f16588;

        /* renamed from: 齉  reason: contains not printable characters */
        private int f16589;

        StrategyParser(Calendar definingCalendar) {
            this.f16588 = definingCalendar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public StrategyAndWidth m20727() {
            if (this.f16589 >= FastDateParser.this.pattern.length()) {
                return null;
            }
            char c = FastDateParser.this.pattern.charAt(this.f16589);
            if (FastDateParser.isFormatLetter(c)) {
                return m20726(c);
            }
            return m20725();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private StrategyAndWidth m20726(char c) {
            int begin = this.f16589;
            do {
                int i = this.f16589 + 1;
                this.f16589 = i;
                if (i >= FastDateParser.this.pattern.length() || FastDateParser.this.pattern.charAt(this.f16589) != c) {
                    int width = this.f16589 - begin;
                }
                int i2 = this.f16589 + 1;
                this.f16589 = i2;
                break;
            } while (FastDateParser.this.pattern.charAt(this.f16589) != c);
            int width2 = this.f16589 - begin;
            return new StrategyAndWidth(FastDateParser.this.getStrategy(c, width2, this.f16588), width2);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private StrategyAndWidth m20725() {
            boolean activeQuote = false;
            StringBuilder sb = new StringBuilder();
            while (this.f16589 < FastDateParser.this.pattern.length()) {
                char c = FastDateParser.this.pattern.charAt(this.f16589);
                if (!activeQuote && FastDateParser.isFormatLetter(c)) {
                    break;
                }
                if (c == '\'') {
                    int i = this.f16589 + 1;
                    this.f16589 = i;
                    if (i == FastDateParser.this.pattern.length() || FastDateParser.this.pattern.charAt(this.f16589) != '\'') {
                        activeQuote = !activeQuote;
                    }
                }
                this.f16589++;
                sb.append(c);
            }
            if (activeQuote) {
                throw new IllegalArgumentException("Unterminated quote");
            }
            String formatField = sb.toString();
            return new StrategyAndWidth(new CopyQuotedStrategy(formatField), formatField.length());
        }
    }

    /* access modifiers changed from: private */
    public static boolean isFormatLetter(char c) {
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
    }

    public String getPattern() {
        return this.pattern;
    }

    public TimeZone getTimeZone() {
        return this.timeZone;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FastDateParser)) {
            return false;
        }
        FastDateParser other = (FastDateParser) obj;
        if (!this.pattern.equals(other.pattern) || !this.timeZone.equals(other.timeZone) || !this.locale.equals(other.locale)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.pattern.hashCode() + ((this.timeZone.hashCode() + (this.locale.hashCode() * 13)) * 13);
    }

    public String toString() {
        return "FastDateParser[" + this.pattern + "," + this.locale + "," + this.timeZone.getID() + "]";
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        init(Calendar.getInstance(this.timeZone, this.locale));
    }

    public Object parseObject(String source) throws ParseException {
        return parse(source);
    }

    public Date parse(String source) throws ParseException {
        ParsePosition pp = new ParsePosition(0);
        Date date = parse(source, pp);
        if (date != null) {
            return date;
        }
        if (this.locale.equals(JAPANESE_IMPERIAL)) {
            throw new ParseException("(The " + this.locale + " locale does not support dates before 1868 AD)\n" + "Unparseable date: \"" + source, pp.getErrorIndex());
        }
        throw new ParseException("Unparseable date: " + source, pp.getErrorIndex());
    }

    public Object parseObject(String source, ParsePosition pos) {
        return parse(source, pos);
    }

    public Date parse(String source, ParsePosition pos) {
        Calendar cal = Calendar.getInstance(this.timeZone, this.locale);
        cal.clear();
        if (parse(source, pos, cal)) {
            return cal.getTime();
        }
        return null;
    }

    public boolean parse(String source, ParsePosition pos, Calendar calendar) {
        ListIterator<StrategyAndWidth> lt = this.patterns.listIterator();
        while (lt.hasNext()) {
            StrategyAndWidth strategyAndWidth = lt.next();
            if (!strategyAndWidth.f16587.m20723(this, calendar, source, pos, strategyAndWidth.m20724(lt))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static StringBuilder simpleQuote(StringBuilder sb, String value) {
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            switch (c) {
                case '$':
                case '(':
                case ')':
                case '*':
                case '+':
                case '.':
                case '?':
                case '[':
                case '\\':
                case '^':
                case '{':
                case '|':
                    sb.append('\\');
                    break;
            }
            sb.append(c);
        }
        return sb;
    }

    /* access modifiers changed from: private */
    public static Map<String, Integer> appendDisplayNames(Calendar cal, Locale locale2, int field, StringBuilder regex) {
        Map<String, Integer> values = new HashMap<>();
        Map<String, Integer> displayNames = cal.getDisplayNames(field, 0, locale2);
        TreeSet<String> sorted = new TreeSet<>(LONGER_FIRST_LOWERCASE);
        for (Map.Entry<String, Integer> displayName : displayNames.entrySet()) {
            String key = displayName.getKey().toLowerCase(locale2);
            if (sorted.add(key)) {
                values.put(key, displayName.getValue());
            }
        }
        Iterator i$ = sorted.iterator();
        while (i$.hasNext()) {
            simpleQuote(regex, i$.next()).append('|');
        }
        return values;
    }

    /* access modifiers changed from: private */
    public int adjustYear(int twoDigitYear) {
        int trial = this.century + twoDigitYear;
        return twoDigitYear >= this.startYear ? trial : trial + 100;
    }

    private static abstract class Strategy {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract boolean m20723(FastDateParser fastDateParser, Calendar calendar, String str, ParsePosition parsePosition, int i);

        private Strategy() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20722() {
            return false;
        }
    }

    private static abstract class PatternStrategy extends Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        private Pattern f16585;

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m20719(FastDateParser fastDateParser, Calendar calendar, String str);

        private PatternStrategy() {
            super();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20718(StringBuilder regex) {
            m20717(regex.toString());
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20717(String regex) {
            this.f16585 = Pattern.compile(regex);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20720() {
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20721(FastDateParser parser, Calendar calendar, String source, ParsePosition pos, int maxWidth) {
            Matcher matcher = this.f16585.matcher(source.substring(pos.getIndex()));
            if (!matcher.lookingAt()) {
                pos.setErrorIndex(pos.getIndex());
                return false;
            }
            pos.setIndex(pos.getIndex() + matcher.end(1));
            m20719(parser, calendar, matcher.group(1));
            return true;
        }
    }

    /* access modifiers changed from: private */
    public Strategy getStrategy(char f, int width, Calendar definingCalendar) {
        switch (f) {
            case 'D':
                return DAY_OF_YEAR_STRATEGY;
            case 'E':
                return getLocaleSpecificStrategy(7, definingCalendar);
            case 'F':
                return DAY_OF_WEEK_IN_MONTH_STRATEGY;
            case 'G':
                return getLocaleSpecificStrategy(0, definingCalendar);
            case 'H':
                return HOUR_OF_DAY_STRATEGY;
            case 'K':
                return HOUR_STRATEGY;
            case 'M':
                return width >= 3 ? getLocaleSpecificStrategy(2, definingCalendar) : NUMBER_MONTH_STRATEGY;
            case 'S':
                return MILLISECOND_STRATEGY;
            case 'W':
                return WEEK_OF_MONTH_STRATEGY;
            case 'X':
                return ISO8601TimeZoneStrategy.m20712(width);
            case 'Y':
            case 'y':
                return width > 2 ? LITERAL_YEAR_STRATEGY : ABBREVIATED_YEAR_STRATEGY;
            case 'Z':
                if (width == 2) {
                    return ISO8601TimeZoneStrategy.f16582;
                }
                break;
            case 'a':
                return getLocaleSpecificStrategy(9, definingCalendar);
            case 'd':
                return DAY_OF_MONTH_STRATEGY;
            case 'h':
                return HOUR12_STRATEGY;
            case 'k':
                return HOUR24_OF_DAY_STRATEGY;
            case 'm':
                return MINUTE_STRATEGY;
            case 's':
                return SECOND_STRATEGY;
            case 'u':
                return DAY_OF_WEEK_STRATEGY;
            case 'w':
                return WEEK_OF_YEAR_STRATEGY;
            case 'z':
                break;
            default:
                throw new IllegalArgumentException("Format '" + f + "' not supported");
        }
        return getLocaleSpecificStrategy(15, definingCalendar);
    }

    private static ConcurrentMap<Locale, Strategy> getCache(int field) {
        ConcurrentMap<Locale, Strategy> concurrentMap;
        synchronized (caches) {
            if (caches[field] == null) {
                caches[field] = new ConcurrentHashMap(3);
            }
            concurrentMap = caches[field];
        }
        return concurrentMap;
    }

    private Strategy getLocaleSpecificStrategy(int field, Calendar definingCalendar) {
        ConcurrentMap<Locale, Strategy> cache = getCache(field);
        Strategy strategy = (Strategy) cache.get(this.locale);
        if (strategy == null) {
            strategy = field == 15 ? new TimeZoneStrategy(this.locale) : new CaseInsensitiveTextStrategy(field, definingCalendar, this.locale);
            Strategy inCache = cache.putIfAbsent(this.locale, strategy);
            if (inCache != null) {
                Strategy strategy2 = strategy;
                return inCache;
            }
        }
        Strategy strategy3 = strategy;
        return strategy;
    }

    private static class CopyQuotedStrategy extends Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f16580;

        CopyQuotedStrategy(String formatField) {
            super();
            this.f16580 = formatField;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20709() {
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20710(FastDateParser parser, Calendar calendar, String source, ParsePosition pos, int maxWidth) {
            int idx = 0;
            while (idx < this.f16580.length()) {
                int sIdx = idx + pos.getIndex();
                if (sIdx == source.length()) {
                    pos.setErrorIndex(sIdx);
                    return false;
                } else if (this.f16580.charAt(idx) != source.charAt(sIdx)) {
                    pos.setErrorIndex(sIdx);
                    return false;
                } else {
                    idx++;
                }
            }
            pos.setIndex(this.f16580.length() + pos.getIndex());
            return true;
        }
    }

    private static class CaseInsensitiveTextStrategy extends PatternStrategy {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f16577;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Map<String, Integer> f16578;

        /* renamed from: 龘  reason: contains not printable characters */
        final Locale f16579;

        CaseInsensitiveTextStrategy(int field, Calendar definingCalendar, Locale locale) {
            super();
            this.f16577 = field;
            this.f16579 = locale;
            StringBuilder regex = new StringBuilder();
            regex.append("((?iu)");
            this.f16578 = FastDateParser.appendDisplayNames(definingCalendar, locale, field, regex);
            regex.setLength(regex.length() - 1);
            regex.append(")");
            m20718(regex);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20708(FastDateParser parser, Calendar cal, String value) {
            cal.set(this.f16577, this.f16578.get(value.toLowerCase(this.f16579)).intValue());
        }
    }

    private static class NumberStrategy extends Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f16584;

        NumberStrategy(int field) {
            super();
            this.f16584 = field;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20715() {
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20716(FastDateParser parser, Calendar calendar, String source, ParsePosition pos, int maxWidth) {
            int idx = pos.getIndex();
            int last = source.length();
            if (maxWidth == 0) {
                while (idx < last && Character.isWhitespace(source.charAt(idx))) {
                    idx++;
                }
                pos.setIndex(idx);
            } else {
                int end = idx + maxWidth;
                if (last > end) {
                    last = end;
                }
            }
            while (idx < last && Character.isDigit(source.charAt(idx))) {
                idx++;
            }
            if (pos.getIndex() == idx) {
                pos.setErrorIndex(idx);
                return false;
            }
            int value = Integer.parseInt(source.substring(pos.getIndex(), idx));
            pos.setIndex(idx);
            calendar.set(this.f16584, m20714(parser, value));
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m20714(FastDateParser parser, int iValue) {
            return iValue;
        }
    }

    static class TimeZoneStrategy extends PatternStrategy {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Map<String, TzInfo> f16591 = new HashMap();

        /* renamed from: 龘  reason: contains not printable characters */
        private final Locale f16592;

        private static class TzInfo {

            /* renamed from: 靐  reason: contains not printable characters */
            int f16593;

            /* renamed from: 龘  reason: contains not printable characters */
            TimeZone f16594;

            TzInfo(TimeZone tz, boolean useDst) {
                this.f16594 = tz;
                this.f16593 = useDst ? tz.getDSTSavings() : 0;
            }
        }

        TimeZoneStrategy(Locale locale) {
            super();
            this.f16592 = locale;
            StringBuilder sb = new StringBuilder();
            sb.append("((?iu)[+-]\\d{4}|GMT[+-]\\d{1,2}:\\d{2}");
            Set<String> sorted = new TreeSet<>(FastDateParser.LONGER_FIRST_LOWERCASE);
            for (String[] zoneNames : DateFormatSymbols.getInstance(locale).getZoneStrings()) {
                String tzId = zoneNames[0];
                if (!tzId.equalsIgnoreCase(TimeZones.GMT_ID)) {
                    TimeZone tz = TimeZone.getTimeZone(tzId);
                    TzInfo standard = new TzInfo(tz, false);
                    TzInfo tzInfo = standard;
                    for (int i = 1; i < zoneNames.length; i++) {
                        switch (i) {
                            case 3:
                                tzInfo = new TzInfo(tz, true);
                                break;
                            case 5:
                                tzInfo = standard;
                                break;
                        }
                        if (zoneNames[i] != null) {
                            String key = zoneNames[i].toLowerCase(locale);
                            if (sorted.add(key)) {
                                this.f16591.put(key, tzInfo);
                            }
                        }
                    }
                }
            }
            for (String zoneName : sorted) {
                StringBuilder unused = FastDateParser.simpleQuote(sb.append('|'), zoneName);
            }
            sb.append(")");
            m20718(sb);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20728(FastDateParser parser, Calendar cal, String timeZone) {
            TimeZone tz = FastTimeZone.getGmtTimeZone(timeZone);
            if (tz != null) {
                cal.setTimeZone(tz);
                return;
            }
            TzInfo tzInfo = this.f16591.get(timeZone.toLowerCase(this.f16592));
            cal.set(16, tzInfo.f16593);
            cal.set(15, tzInfo.f16594.getRawOffset());
        }
    }

    private static class ISO8601TimeZoneStrategy extends PatternStrategy {

        /* renamed from: 靐  reason: contains not printable characters */
        private static final Strategy f16581 = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}\\d{2}))");
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public static final Strategy f16582 = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}(?::)\\d{2}))");

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Strategy f16583 = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}))");

        ISO8601TimeZoneStrategy(String pattern) {
            super();
            m20717(pattern);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m20713(FastDateParser parser, Calendar cal, String value) {
            cal.setTimeZone(FastTimeZone.getGmtTimeZone(value));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Strategy m20712(int tokenLen) {
            switch (tokenLen) {
                case 1:
                    return f16583;
                case 2:
                    return f16581;
                case 3:
                    return f16582;
                default:
                    throw new IllegalArgumentException("invalid number of X");
            }
        }
    }
}
