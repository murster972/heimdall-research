package org.apache.commons.lang3.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.FieldPosition;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class FastDatePrinter implements Serializable, DatePrinter {
    public static final int FULL = 0;
    public static final int LONG = 1;
    private static final int MAX_DIGITS = 10;
    public static final int MEDIUM = 2;
    public static final int SHORT = 3;
    private static final ConcurrentMap<TimeZoneDisplayKey, String> cTimeZoneDisplayCache = new ConcurrentHashMap(7);
    private static final long serialVersionUID = 1;
    private final Locale mLocale;
    private transient int mMaxLengthEstimate;
    private final String mPattern;
    private transient Rule[] mRules;
    private final TimeZone mTimeZone;

    private interface NumberRule extends Rule {
        /* renamed from: 龘  reason: contains not printable characters */
        void m20737(Appendable appendable, int i) throws IOException;
    }

    private interface Rule {
        /* renamed from: 龘  reason: contains not printable characters */
        int m20741();

        /* renamed from: 龘  reason: contains not printable characters */
        void m20742(Appendable appendable, Calendar calendar) throws IOException;
    }

    protected FastDatePrinter(String str, TimeZone timeZone, Locale locale) {
        this.mPattern = str;
        this.mTimeZone = timeZone;
        this.mLocale = locale;
        init();
    }

    /* access modifiers changed from: private */
    public static void appendDigits(Appendable appendable, int i) throws IOException {
        appendable.append((char) ((i / 10) + 48));
        appendable.append((char) ((i % 10) + 48));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (r10 < 100) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        r9.append((char) ((r10 / 100) + 48));
        r10 = r10 % 100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        if (r10 < 10) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        r9.append((char) ((r10 / 10) + 48));
        r10 = r10 % 10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        r9.append((char) (r10 + 48));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0050, code lost:
        r9.append('0');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        r9.append('0');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void appendFullDigits(java.lang.Appendable r9, int r10, int r11) throws java.io.IOException {
        /*
            r8 = 100
            r7 = 10
            r6 = 48
            r5 = 10000(0x2710, float:1.4013E-41)
            if (r10 >= r5) goto L_0x0058
            r3 = 4
            r5 = 1000(0x3e8, float:1.401E-42)
            if (r10 >= r5) goto L_0x0019
            int r3 = r3 + -1
            if (r10 >= r8) goto L_0x0019
            int r3 = r3 + -1
            if (r10 >= r7) goto L_0x0019
            int r3 = r3 + -1
        L_0x0019:
            int r2 = r11 - r3
        L_0x001b:
            if (r2 <= 0) goto L_0x0023
            r9.append(r6)
            int r2 = r2 + -1
            goto L_0x001b
        L_0x0023:
            switch(r3) {
                case 1: goto L_0x0049;
                case 2: goto L_0x003d;
                case 3: goto L_0x0031;
                case 4: goto L_0x0027;
                default: goto L_0x0026;
            }
        L_0x0026:
            return
        L_0x0027:
            int r5 = r10 / 1000
            int r5 = r5 + 48
            char r5 = (char) r5
            r9.append(r5)
            int r10 = r10 % 1000
        L_0x0031:
            if (r10 < r8) goto L_0x0050
            int r5 = r10 / 100
            int r5 = r5 + 48
            char r5 = (char) r5
            r9.append(r5)
            int r10 = r10 % 100
        L_0x003d:
            if (r10 < r7) goto L_0x0054
            int r5 = r10 / 10
            int r5 = r5 + 48
            char r5 = (char) r5
            r9.append(r5)
            int r10 = r10 % 10
        L_0x0049:
            int r5 = r10 + 48
            char r5 = (char) r5
            r9.append(r5)
            goto L_0x0026
        L_0x0050:
            r9.append(r6)
            goto L_0x003d
        L_0x0054:
            r9.append(r6)
            goto L_0x0049
        L_0x0058:
            char[] r4 = new char[r7]
            r0 = 0
            r1 = r0
        L_0x005c:
            if (r10 == 0) goto L_0x006b
            int r0 = r1 + 1
            int r5 = r10 % 10
            int r5 = r5 + 48
            char r5 = (char) r5
            r4[r1] = r5
            int r10 = r10 / 10
            r1 = r0
            goto L_0x005c
        L_0x006b:
            if (r1 >= r11) goto L_0x007d
            r9.append(r6)
            int r11 = r11 + -1
            goto L_0x006b
        L_0x0073:
            int r0 = r0 + -1
            if (r0 < 0) goto L_0x0026
            char r5 = r4[r0]
            r9.append(r5)
            goto L_0x0073
        L_0x007d:
            r0 = r1
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.time.FastDatePrinter.appendFullDigits(java.lang.Appendable, int, int):void");
    }

    private <B extends Appendable> B applyRules(Calendar calendar, B b) {
        try {
            for (Rule r4 : this.mRules) {
                r4.m20742(b, calendar);
            }
        } catch (IOException e) {
            ExceptionUtils.rethrow(e);
        }
        return b;
    }

    private String applyRulesToString(Calendar calendar) {
        return ((StringBuilder) applyRules(calendar, new StringBuilder(this.mMaxLengthEstimate))).toString();
    }

    static String getTimeZoneDisplay(TimeZone timeZone, boolean z, int i, Locale locale) {
        TimeZoneDisplayKey timeZoneDisplayKey = new TimeZoneDisplayKey(timeZone, z, i, locale);
        String str = (String) cTimeZoneDisplayCache.get(timeZoneDisplayKey);
        if (str != null) {
            return str;
        }
        String displayName = timeZone.getDisplayName(z, i, locale);
        String putIfAbsent = cTimeZoneDisplayCache.putIfAbsent(timeZoneDisplayKey, displayName);
        return putIfAbsent != null ? putIfAbsent : displayName;
    }

    private void init() {
        List<Rule> parsePattern = parsePattern();
        this.mRules = (Rule[]) parsePattern.toArray(new Rule[parsePattern.size()]);
        int i = 0;
        int length = this.mRules.length;
        while (true) {
            length--;
            if (length >= 0) {
                i += this.mRules[length].m20741();
            } else {
                this.mMaxLengthEstimate = i;
                return;
            }
        }
    }

    private Calendar newCalendar() {
        return Calendar.getInstance(this.mTimeZone, this.mLocale);
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        init();
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public StringBuffer applyRules(Calendar calendar, StringBuffer stringBuffer) {
        return (StringBuffer) applyRules(calendar, stringBuffer);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FastDatePrinter)) {
            return false;
        }
        FastDatePrinter fastDatePrinter = (FastDatePrinter) obj;
        return this.mPattern.equals(fastDatePrinter.mPattern) && this.mTimeZone.equals(fastDatePrinter.mTimeZone) && this.mLocale.equals(fastDatePrinter.mLocale);
    }

    public <B extends Appendable> B format(long j, B b) {
        Calendar newCalendar = newCalendar();
        newCalendar.setTimeInMillis(j);
        return applyRules(newCalendar, b);
    }

    public <B extends Appendable> B format(Calendar calendar, B b) {
        if (!calendar.getTimeZone().equals(this.mTimeZone)) {
            calendar = (Calendar) calendar.clone();
            calendar.setTimeZone(this.mTimeZone);
        }
        return applyRules(calendar, b);
    }

    public <B extends Appendable> B format(Date date, B b) {
        Calendar newCalendar = newCalendar();
        newCalendar.setTime(date);
        return applyRules(newCalendar, b);
    }

    public String format(long j) {
        Calendar newCalendar = newCalendar();
        newCalendar.setTimeInMillis(j);
        return applyRulesToString(newCalendar);
    }

    /* access modifiers changed from: package-private */
    public String format(Object obj) {
        if (obj instanceof Date) {
            return format((Date) obj);
        }
        if (obj instanceof Calendar) {
            return format((Calendar) obj);
        }
        if (obj instanceof Long) {
            return format(((Long) obj).longValue());
        }
        throw new IllegalArgumentException("Unknown class: " + (obj == null ? "<null>" : obj.getClass().getName()));
    }

    public String format(Calendar calendar) {
        return ((StringBuilder) format(calendar, new StringBuilder(this.mMaxLengthEstimate))).toString();
    }

    public String format(Date date) {
        Calendar newCalendar = newCalendar();
        newCalendar.setTime(date);
        return applyRulesToString(newCalendar);
    }

    public StringBuffer format(long j, StringBuffer stringBuffer) {
        Calendar newCalendar = newCalendar();
        newCalendar.setTimeInMillis(j);
        return (StringBuffer) applyRules(newCalendar, stringBuffer);
    }

    @Deprecated
    public StringBuffer format(Object obj, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        if (obj instanceof Date) {
            return format((Date) obj, stringBuffer);
        }
        if (obj instanceof Calendar) {
            return format((Calendar) obj, stringBuffer);
        }
        if (obj instanceof Long) {
            return format(((Long) obj).longValue(), stringBuffer);
        }
        throw new IllegalArgumentException("Unknown class: " + (obj == null ? "<null>" : obj.getClass().getName()));
    }

    public StringBuffer format(Calendar calendar, StringBuffer stringBuffer) {
        return format(calendar.getTime(), stringBuffer);
    }

    public StringBuffer format(Date date, StringBuffer stringBuffer) {
        Calendar newCalendar = newCalendar();
        newCalendar.setTime(date);
        return (StringBuffer) applyRules(newCalendar, stringBuffer);
    }

    public Locale getLocale() {
        return this.mLocale;
    }

    public int getMaxLengthEstimate() {
        return this.mMaxLengthEstimate;
    }

    public String getPattern() {
        return this.mPattern;
    }

    public TimeZone getTimeZone() {
        return this.mTimeZone;
    }

    public int hashCode() {
        return this.mPattern.hashCode() + ((this.mTimeZone.hashCode() + (this.mLocale.hashCode() * 13)) * 13);
    }

    /* access modifiers changed from: protected */
    public List<Rule> parsePattern() {
        Object stringLiteral;
        TwoDigitYearField selectNumberRule;
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(this.mLocale);
        ArrayList arrayList = new ArrayList();
        String[] eras = dateFormatSymbols.getEras();
        String[] months = dateFormatSymbols.getMonths();
        String[] shortMonths = dateFormatSymbols.getShortMonths();
        String[] weekdays = dateFormatSymbols.getWeekdays();
        String[] shortWeekdays = dateFormatSymbols.getShortWeekdays();
        String[] amPmStrings = dateFormatSymbols.getAmPmStrings();
        int length = this.mPattern.length();
        int[] iArr = new int[1];
        int i = 0;
        while (i < length) {
            iArr[0] = i;
            String parseToken = parseToken(this.mPattern, iArr);
            int i2 = iArr[0];
            int length2 = parseToken.length();
            if (length2 == 0) {
                return arrayList;
            }
            char charAt = parseToken.charAt(0);
            switch (charAt) {
                case '\'':
                    String substring = parseToken.substring(1);
                    if (substring.length() != 1) {
                        stringLiteral = new StringLiteral(substring);
                        break;
                    } else {
                        stringLiteral = new CharacterLiteral(substring.charAt(0));
                        break;
                    }
                case 'D':
                    stringLiteral = selectNumberRule(6, length2);
                    break;
                case 'E':
                    stringLiteral = new TextField(7, length2 < 4 ? shortWeekdays : weekdays);
                    break;
                case 'F':
                    stringLiteral = selectNumberRule(8, length2);
                    break;
                case 'G':
                    stringLiteral = new TextField(0, eras);
                    break;
                case 'H':
                    stringLiteral = selectNumberRule(11, length2);
                    break;
                case 'K':
                    stringLiteral = selectNumberRule(10, length2);
                    break;
                case 'M':
                    if (length2 < 4) {
                        if (length2 != 3) {
                            if (length2 != 2) {
                                stringLiteral = UnpaddedMonthField.f16621;
                                break;
                            } else {
                                stringLiteral = TwoDigitMonthField.f16618;
                                break;
                            }
                        } else {
                            stringLiteral = new TextField(2, shortMonths);
                            break;
                        }
                    } else {
                        stringLiteral = new TextField(2, months);
                        break;
                    }
                case 'S':
                    stringLiteral = selectNumberRule(14, length2);
                    break;
                case 'W':
                    stringLiteral = selectNumberRule(4, length2);
                    break;
                case 'X':
                    stringLiteral = Iso8601_Rule.m20734(length2);
                    break;
                case 'Y':
                case 'y':
                    if (length2 == 2) {
                        selectNumberRule = TwoDigitYearField.f16620;
                    } else {
                        if (length2 < 4) {
                            length2 = 4;
                        }
                        selectNumberRule = selectNumberRule(1, length2);
                    }
                    if (charAt != 'Y') {
                        stringLiteral = selectNumberRule;
                        break;
                    } else {
                        stringLiteral = new WeekYear(selectNumberRule);
                        break;
                    }
                case 'Z':
                    if (length2 != 1) {
                        if (length2 != 2) {
                            stringLiteral = TimeZoneNumberRule.f16614;
                            break;
                        } else {
                            stringLiteral = Iso8601_Rule.f16598;
                            break;
                        }
                    } else {
                        stringLiteral = TimeZoneNumberRule.f16613;
                        break;
                    }
                case 'a':
                    stringLiteral = new TextField(9, amPmStrings);
                    break;
                case 'd':
                    stringLiteral = selectNumberRule(5, length2);
                    break;
                case 'h':
                    stringLiteral = new TwelveHourField(selectNumberRule(10, length2));
                    break;
                case 'k':
                    stringLiteral = new TwentyFourHourField(selectNumberRule(11, length2));
                    break;
                case 'm':
                    stringLiteral = selectNumberRule(12, length2);
                    break;
                case 's':
                    stringLiteral = selectNumberRule(13, length2);
                    break;
                case 'u':
                    stringLiteral = new DayInWeekField(selectNumberRule(7, length2));
                    break;
                case 'w':
                    stringLiteral = selectNumberRule(3, length2);
                    break;
                case 'z':
                    if (length2 < 4) {
                        stringLiteral = new TimeZoneNameRule(this.mTimeZone, this.mLocale, 0);
                        break;
                    } else {
                        stringLiteral = new TimeZoneNameRule(this.mTimeZone, this.mLocale, 1);
                        break;
                    }
                default:
                    throw new IllegalArgumentException("Illegal pattern component: " + parseToken);
            }
            arrayList.add(stringLiteral);
            i = i2 + 1;
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0069, code lost:
        r2 = r2 - 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String parseToken(java.lang.String r13, int[] r14) {
        /*
            r12 = this;
            r11 = 97
            r10 = 90
            r9 = 65
            r8 = 39
            r6 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r2 = r14[r6]
            int r4 = r13.length()
            char r1 = r13.charAt(r2)
            if (r1 < r9) goto L_0x001c
            if (r1 <= r10) goto L_0x0022
        L_0x001c:
            if (r1 < r11) goto L_0x0037
            r7 = 122(0x7a, float:1.71E-43)
            if (r1 > r7) goto L_0x0037
        L_0x0022:
            r0.append(r1)
        L_0x0025:
            int r7 = r2 + 1
            if (r7 >= r4) goto L_0x006b
            int r7 = r2 + 1
            char r5 = r13.charAt(r7)
            if (r5 != r1) goto L_0x006b
            r0.append(r1)
            int r2 = r2 + 1
            goto L_0x0025
        L_0x0037:
            r0.append(r8)
            r3 = 0
        L_0x003b:
            if (r2 >= r4) goto L_0x006b
            char r1 = r13.charAt(r2)
            if (r1 != r8) goto L_0x005d
            int r7 = r2 + 1
            if (r7 >= r4) goto L_0x0057
            int r7 = r2 + 1
            char r7 = r13.charAt(r7)
            if (r7 != r8) goto L_0x0057
            int r2 = r2 + 1
            r0.append(r1)
        L_0x0054:
            int r2 = r2 + 1
            goto L_0x003b
        L_0x0057:
            if (r3 != 0) goto L_0x005b
            r3 = 1
        L_0x005a:
            goto L_0x0054
        L_0x005b:
            r3 = r6
            goto L_0x005a
        L_0x005d:
            if (r3 != 0) goto L_0x0072
            if (r1 < r9) goto L_0x0063
            if (r1 <= r10) goto L_0x0069
        L_0x0063:
            if (r1 < r11) goto L_0x0072
            r7 = 122(0x7a, float:1.71E-43)
            if (r1 > r7) goto L_0x0072
        L_0x0069:
            int r2 = r2 + -1
        L_0x006b:
            r14[r6] = r2
            java.lang.String r6 = r0.toString()
            return r6
        L_0x0072:
            r0.append(r1)
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.time.FastDatePrinter.parseToken(java.lang.String, int[]):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public NumberRule selectNumberRule(int i, int i2) {
        switch (i2) {
            case 1:
                return new UnpaddedNumberField(i);
            case 2:
                return new TwoDigitNumberField(i);
            default:
                return new PaddedNumberField(i, i2);
        }
    }

    public String toString() {
        return "FastDatePrinter[" + this.mPattern + "," + this.mLocale + "," + this.mTimeZone.getID() + "]";
    }

    private static class CharacterLiteral implements Rule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final char f16595;

        CharacterLiteral(char value) {
            this.f16595 = value;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20729() {
            return 1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20730(Appendable buffer, Calendar calendar) throws IOException {
            buffer.append(this.f16595);
        }
    }

    private static class StringLiteral implements Rule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f16603;

        StringLiteral(String value) {
            this.f16603 = value;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20743() {
            return this.f16603.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20744(Appendable buffer, Calendar calendar) throws IOException {
            buffer.append(this.f16603);
        }
    }

    private static class TextField implements Rule {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String[] f16604;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f16605;

        TextField(int field, String[] values) {
            this.f16605 = field;
            this.f16604 = values;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20745() {
            int max = 0;
            int i = this.f16604.length;
            while (true) {
                i--;
                if (i < 0) {
                    return max;
                }
                int len = this.f16604[i].length();
                if (len > max) {
                    max = len;
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20746(Appendable buffer, Calendar calendar) throws IOException {
            buffer.append(this.f16604[calendar.get(this.f16605)]);
        }
    }

    private static class UnpaddedNumberField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f16622;

        UnpaddedNumberField(int field) {
            this.f16622 = field;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20769() {
            return 4;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20771(Appendable buffer, Calendar calendar) throws IOException {
            m20770(buffer, calendar.get(this.f16622));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m20770(Appendable buffer, int value) throws IOException {
            if (value < 10) {
                buffer.append((char) (value + 48));
            } else if (value < 100) {
                FastDatePrinter.appendDigits(buffer, value);
            } else {
                FastDatePrinter.appendFullDigits(buffer, value, 1);
            }
        }
    }

    private static class UnpaddedMonthField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        static final UnpaddedMonthField f16621 = new UnpaddedMonthField();

        UnpaddedMonthField() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20766() {
            return 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20768(Appendable buffer, Calendar calendar) throws IOException {
            m20767(buffer, calendar.get(2) + 1);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m20767(Appendable buffer, int value) throws IOException {
            if (value < 10) {
                buffer.append((char) (value + 48));
            } else {
                FastDatePrinter.appendDigits(buffer, value);
            }
        }
    }

    private static class PaddedNumberField implements NumberRule {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f16601;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f16602;

        PaddedNumberField(int field, int size) {
            if (size < 3) {
                throw new IllegalArgumentException();
            }
            this.f16602 = field;
            this.f16601 = size;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20738() {
            return this.f16601;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20740(Appendable buffer, Calendar calendar) throws IOException {
            m20739(buffer, calendar.get(this.f16602));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m20739(Appendable buffer, int value) throws IOException {
            FastDatePrinter.appendFullDigits(buffer, value, this.f16601);
        }
    }

    private static class TwoDigitNumberField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f16619;

        TwoDigitNumberField(int field) {
            this.f16619 = field;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20760() {
            return 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20762(Appendable buffer, Calendar calendar) throws IOException {
            m20761(buffer, calendar.get(this.f16619));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m20761(Appendable buffer, int value) throws IOException {
            if (value < 100) {
                FastDatePrinter.appendDigits(buffer, value);
            } else {
                FastDatePrinter.appendFullDigits(buffer, value, 2);
            }
        }
    }

    private static class TwoDigitYearField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        static final TwoDigitYearField f16620 = new TwoDigitYearField();

        TwoDigitYearField() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20763() {
            return 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20765(Appendable buffer, Calendar calendar) throws IOException {
            m20764(buffer, calendar.get(1) % 100);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m20764(Appendable buffer, int value) throws IOException {
            FastDatePrinter.appendDigits(buffer, value);
        }
    }

    private static class TwoDigitMonthField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        static final TwoDigitMonthField f16618 = new TwoDigitMonthField();

        TwoDigitMonthField() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20757() {
            return 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20759(Appendable buffer, Calendar calendar) throws IOException {
            m20758(buffer, calendar.get(2) + 1);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m20758(Appendable buffer, int value) throws IOException {
            FastDatePrinter.appendDigits(buffer, value);
        }
    }

    private static class TwelveHourField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final NumberRule f16616;

        TwelveHourField(NumberRule rule) {
            this.f16616 = rule;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20751() {
            return this.f16616.m20741();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20753(Appendable buffer, Calendar calendar) throws IOException {
            int value = calendar.get(10);
            if (value == 0) {
                value = calendar.getLeastMaximum(10) + 1;
            }
            this.f16616.m20737(buffer, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20752(Appendable buffer, int value) throws IOException {
            this.f16616.m20737(buffer, value);
        }
    }

    private static class TwentyFourHourField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final NumberRule f16617;

        TwentyFourHourField(NumberRule rule) {
            this.f16617 = rule;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20754() {
            return this.f16617.m20741();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20756(Appendable buffer, Calendar calendar) throws IOException {
            int value = calendar.get(11);
            if (value == 0) {
                value = calendar.getMaximum(11) + 1;
            }
            this.f16617.m20737(buffer, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20755(Appendable buffer, int value) throws IOException {
            this.f16617.m20737(buffer, value);
        }
    }

    private static class DayInWeekField implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final NumberRule f16596;

        DayInWeekField(NumberRule rule) {
            this.f16596 = rule;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20731() {
            return this.f16596.m20741();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20733(Appendable buffer, Calendar calendar) throws IOException {
            int i = 7;
            int value = calendar.get(7);
            NumberRule numberRule = this.f16596;
            if (value != 1) {
                i = value - 1;
            }
            numberRule.m20737(buffer, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20732(Appendable buffer, int value) throws IOException {
            this.f16596.m20737(buffer, value);
        }
    }

    private static class WeekYear implements NumberRule {

        /* renamed from: 龘  reason: contains not printable characters */
        private final NumberRule f16623;

        WeekYear(NumberRule rule) {
            this.f16623 = rule;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20772() {
            return this.f16623.m20741();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20774(Appendable buffer, Calendar calendar) throws IOException {
            this.f16623.m20737(buffer, calendar.getWeekYear());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20773(Appendable buffer, int value) throws IOException {
            this.f16623.m20737(buffer, value);
        }
    }

    private static class TimeZoneNameRule implements Rule {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f16609;

        /* renamed from: 麤  reason: contains not printable characters */
        private final String f16610;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String f16611;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Locale f16612;

        TimeZoneNameRule(TimeZone timeZone, Locale locale, int style) {
            this.f16612 = locale;
            this.f16609 = style;
            this.f16611 = FastDatePrinter.getTimeZoneDisplay(timeZone, false, style, locale);
            this.f16610 = FastDatePrinter.getTimeZoneDisplay(timeZone, true, style, locale);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20747() {
            return Math.max(this.f16611.length(), this.f16610.length());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20748(Appendable buffer, Calendar calendar) throws IOException {
            TimeZone zone = calendar.getTimeZone();
            if (calendar.get(16) != 0) {
                buffer.append(FastDatePrinter.getTimeZoneDisplay(zone, true, this.f16609, this.f16612));
            } else {
                buffer.append(FastDatePrinter.getTimeZoneDisplay(zone, false, this.f16609, this.f16612));
            }
        }
    }

    private static class TimeZoneNumberRule implements Rule {

        /* renamed from: 靐  reason: contains not printable characters */
        static final TimeZoneNumberRule f16613 = new TimeZoneNumberRule(false);

        /* renamed from: 龘  reason: contains not printable characters */
        static final TimeZoneNumberRule f16614 = new TimeZoneNumberRule(true);

        /* renamed from: 齉  reason: contains not printable characters */
        final boolean f16615;

        TimeZoneNumberRule(boolean colon) {
            this.f16615 = colon;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20749() {
            return 5;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20750(Appendable buffer, Calendar calendar) throws IOException {
            int offset = calendar.get(15) + calendar.get(16);
            if (offset < 0) {
                buffer.append('-');
                offset = -offset;
            } else {
                buffer.append('+');
            }
            int hours = offset / 3600000;
            FastDatePrinter.appendDigits(buffer, hours);
            if (this.f16615) {
                buffer.append(':');
            }
            FastDatePrinter.appendDigits(buffer, (offset / 60000) - (hours * 60));
        }
    }

    private static class Iso8601_Rule implements Rule {

        /* renamed from: 靐  reason: contains not printable characters */
        static final Iso8601_Rule f16597 = new Iso8601_Rule(5);

        /* renamed from: 齉  reason: contains not printable characters */
        static final Iso8601_Rule f16598 = new Iso8601_Rule(6);

        /* renamed from: 龘  reason: contains not printable characters */
        static final Iso8601_Rule f16599 = new Iso8601_Rule(3);

        /* renamed from: 麤  reason: contains not printable characters */
        final int f16600;

        /* renamed from: 龘  reason: contains not printable characters */
        static Iso8601_Rule m20734(int tokenLen) {
            switch (tokenLen) {
                case 1:
                    return f16599;
                case 2:
                    return f16597;
                case 3:
                    return f16598;
                default:
                    throw new IllegalArgumentException("invalid number of X");
            }
        }

        Iso8601_Rule(int length) {
            this.f16600 = length;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20735() {
            return this.f16600;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20736(Appendable buffer, Calendar calendar) throws IOException {
            int offset = calendar.get(15) + calendar.get(16);
            if (offset == 0) {
                buffer.append("Z");
                return;
            }
            if (offset < 0) {
                buffer.append('-');
                offset = -offset;
            } else {
                buffer.append('+');
            }
            int hours = offset / 3600000;
            FastDatePrinter.appendDigits(buffer, hours);
            if (this.f16600 >= 5) {
                if (this.f16600 == 6) {
                    buffer.append(':');
                }
                FastDatePrinter.appendDigits(buffer, (offset / 60000) - (hours * 60));
            }
        }
    }

    private static class TimeZoneDisplayKey {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f16606;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Locale f16607;

        /* renamed from: 龘  reason: contains not printable characters */
        private final TimeZone f16608;

        TimeZoneDisplayKey(TimeZone timeZone, boolean daylight, int style, Locale locale) {
            this.f16608 = timeZone;
            if (daylight) {
                this.f16606 = Integer.MIN_VALUE | style;
            } else {
                this.f16606 = style;
            }
            this.f16607 = locale;
        }

        public int hashCode() {
            return (((this.f16606 * 31) + this.f16607.hashCode()) * 31) + this.f16608.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TimeZoneDisplayKey)) {
                return false;
            }
            TimeZoneDisplayKey other = (TimeZoneDisplayKey) obj;
            if (!this.f16608.equals(other.f16608) || this.f16606 != other.f16606 || !this.f16607.equals(other.f16607)) {
                return false;
            }
            return true;
        }
    }
}
