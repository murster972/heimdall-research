package org.apache.commons.lang3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

public class SerializationUtils {
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0049, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004a, code lost:
        if (r2 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004c, code lost:
        if (r6 != null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        throw r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0057, code lost:
        r2.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T extends java.io.Serializable> T clone(T r8) {
        /*
            r6 = 0
            if (r8 != 0) goto L_0x0005
            r4 = r6
        L_0x0004:
            return r4
        L_0x0005:
            byte[] r3 = serialize(r8)
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            r0.<init>(r3)
            org.apache.commons.lang3.SerializationUtils$ClassLoaderAwareObjectInputStream r2 = new org.apache.commons.lang3.SerializationUtils$ClassLoaderAwareObjectInputStream     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            java.lang.Class r7 = r8.getClass()     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            java.lang.ClassLoader r7 = r7.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            r2.<init>(r0, r7)     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            r7 = 0
            java.lang.Object r4 = r2.readObject()     // Catch:{ Throwable -> 0x0047 }
            java.io.Serializable r4 = (java.io.Serializable) r4     // Catch:{ Throwable -> 0x0047 }
            if (r2 == 0) goto L_0x0004
            if (r6 == 0) goto L_0x0039
            r2.close()     // Catch:{ Throwable -> 0x002a }
            goto L_0x0004
        L_0x002a:
            r5 = move-exception
            r7.addSuppressed(r5)     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            goto L_0x0004
        L_0x002f:
            r1 = move-exception
            org.apache.commons.lang3.SerializationException r6 = new org.apache.commons.lang3.SerializationException
            java.lang.String r7 = "ClassNotFoundException while reading cloned object data"
            r6.<init>(r7, r1)
            throw r6
        L_0x0039:
            r2.close()     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            goto L_0x0004
        L_0x003d:
            r1 = move-exception
            org.apache.commons.lang3.SerializationException r6 = new org.apache.commons.lang3.SerializationException
            java.lang.String r7 = "IOException while reading or closing cloned object data"
            r6.<init>(r7, r1)
            throw r6
        L_0x0047:
            r6 = move-exception
            throw r6     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r7 = move-exception
            if (r2 == 0) goto L_0x0051
            if (r6 == 0) goto L_0x0057
            r2.close()     // Catch:{ Throwable -> 0x0052 }
        L_0x0051:
            throw r7     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
        L_0x0052:
            r5 = move-exception
            r6.addSuppressed(r5)     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            goto L_0x0051
        L_0x0057:
            r2.close()     // Catch:{ ClassNotFoundException -> 0x002f, IOException -> 0x003d }
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.SerializationUtils.clone(java.io.Serializable):java.io.Serializable");
    }

    public static <T extends Serializable> T roundtrip(T msg) {
        return (Serializable) deserialize(serialize(msg));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0031, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0032, code lost:
        if (r1 != null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0034, code lost:
        if (r4 != null) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x003f, code lost:
        r1.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void serialize(java.io.Serializable r6, java.io.OutputStream r7) {
        /*
            r4 = 0
            if (r7 == 0) goto L_0x001d
            r3 = 1
        L_0x0004:
            java.lang.String r5 = "The OutputStream must not be null"
            java.lang.Object[] r4 = new java.lang.Object[r4]
            org.apache.commons.lang3.Validate.isTrue((boolean) r3, (java.lang.String) r5, (java.lang.Object[]) r4)
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0024 }
            r1.<init>(r7)     // Catch:{ IOException -> 0x0024 }
            r4 = 0
            r1.writeObject(r6)     // Catch:{ Throwable -> 0x002f }
            if (r1 == 0) goto L_0x001c
            if (r4 == 0) goto L_0x002b
            r1.close()     // Catch:{ Throwable -> 0x001f }
        L_0x001c:
            return
        L_0x001d:
            r3 = r4
            goto L_0x0004
        L_0x001f:
            r2 = move-exception
            r4.addSuppressed(r2)     // Catch:{ IOException -> 0x0024 }
            goto L_0x001c
        L_0x0024:
            r0 = move-exception
            org.apache.commons.lang3.SerializationException r3 = new org.apache.commons.lang3.SerializationException
            r3.<init>((java.lang.Throwable) r0)
            throw r3
        L_0x002b:
            r1.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x001c
        L_0x002f:
            r4 = move-exception
            throw r4     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r3 = move-exception
            if (r1 == 0) goto L_0x0039
            if (r4 == 0) goto L_0x003f
            r1.close()     // Catch:{ Throwable -> 0x003a }
        L_0x0039:
            throw r3     // Catch:{ IOException -> 0x0024 }
        L_0x003a:
            r2 = move-exception
            r4.addSuppressed(r2)     // Catch:{ IOException -> 0x0024 }
            goto L_0x0039
        L_0x003f:
            r1.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.SerializationUtils.serialize(java.io.Serializable, java.io.OutputStream):void");
    }

    public static byte[] serialize(Serializable obj) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
        serialize(obj, baos);
        return baos.toByteArray();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0034, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0035, code lost:
        if (r1 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0037, code lost:
        if (r5 != null) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0042, code lost:
        r1.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> T deserialize(java.io.InputStream r7) {
        /*
            r5 = 0
            if (r7 == 0) goto L_0x001e
            r4 = 1
        L_0x0004:
            java.lang.String r6 = "The InputStream must not be null"
            java.lang.Object[] r5 = new java.lang.Object[r5]
            org.apache.commons.lang3.Validate.isTrue((boolean) r4, (java.lang.String) r6, (java.lang.Object[]) r5)
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ ClassNotFoundException -> 0x0025, IOException -> 0x0030 }
            r1.<init>(r7)     // Catch:{ ClassNotFoundException -> 0x0025, IOException -> 0x0030 }
            r5 = 0
            java.lang.Object r2 = r1.readObject()     // Catch:{ Throwable -> 0x0032 }
            if (r1 == 0) goto L_0x001d
            if (r5 == 0) goto L_0x002c
            r1.close()     // Catch:{ Throwable -> 0x0020 }
        L_0x001d:
            return r2
        L_0x001e:
            r4 = r5
            goto L_0x0004
        L_0x0020:
            r3 = move-exception
            r5.addSuppressed(r3)     // Catch:{ ClassNotFoundException -> 0x0025, IOException -> 0x0030 }
            goto L_0x001d
        L_0x0025:
            r0 = move-exception
        L_0x0026:
            org.apache.commons.lang3.SerializationException r4 = new org.apache.commons.lang3.SerializationException
            r4.<init>((java.lang.Throwable) r0)
            throw r4
        L_0x002c:
            r1.close()     // Catch:{ ClassNotFoundException -> 0x0025, IOException -> 0x0030 }
            goto L_0x001d
        L_0x0030:
            r0 = move-exception
            goto L_0x0026
        L_0x0032:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r4 = move-exception
            if (r1 == 0) goto L_0x003c
            if (r5 == 0) goto L_0x0042
            r1.close()     // Catch:{ Throwable -> 0x003d }
        L_0x003c:
            throw r4     // Catch:{ ClassNotFoundException -> 0x0025, IOException -> 0x0030 }
        L_0x003d:
            r3 = move-exception
            r5.addSuppressed(r3)     // Catch:{ ClassNotFoundException -> 0x0025, IOException -> 0x0030 }
            goto L_0x003c
        L_0x0042:
            r1.close()     // Catch:{ ClassNotFoundException -> 0x0025, IOException -> 0x0030 }
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.SerializationUtils.deserialize(java.io.InputStream):java.lang.Object");
    }

    public static <T> T deserialize(byte[] objectData) {
        boolean z;
        if (objectData != null) {
            z = true;
        } else {
            z = false;
        }
        Validate.isTrue(z, "The byte[] must not be null", new Object[0]);
        return deserialize((InputStream) new ByteArrayInputStream(objectData));
    }

    static class ClassLoaderAwareObjectInputStream extends ObjectInputStream {

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Map<String, Class<?>> f16530 = new HashMap();

        /* renamed from: 靐  reason: contains not printable characters */
        private final ClassLoader f16531;

        static {
            f16530.put("byte", Byte.TYPE);
            f16530.put("short", Short.TYPE);
            f16530.put("int", Integer.TYPE);
            f16530.put(PubnativeRequest.Parameters.LONG, Long.TYPE);
            f16530.put("float", Float.TYPE);
            f16530.put("double", Double.TYPE);
            f16530.put("boolean", Boolean.TYPE);
            f16530.put("char", Character.TYPE);
            f16530.put("void", Void.TYPE);
        }

        ClassLoaderAwareObjectInputStream(InputStream in, ClassLoader classLoader) throws IOException {
            super(in);
            this.f16531 = classLoader;
        }

        /* access modifiers changed from: protected */
        public Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
            String name = desc.getName();
            try {
                return Class.forName(name, false, this.f16531);
            } catch (ClassNotFoundException e) {
                try {
                    return Class.forName(name, false, Thread.currentThread().getContextClassLoader());
                } catch (ClassNotFoundException cnfe) {
                    Class<?> cls = f16530.get(name);
                    if (cls != null) {
                        return cls;
                    }
                    throw cnfe;
                }
            }
        }
    }
}
