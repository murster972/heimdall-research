package org.apache.commons.lang3.text;

import java.util.Map;

@Deprecated
public abstract class StrLookup<V> {
    private static final StrLookup<String> NONE_LOOKUP = new MapStrLookup((Map) null);
    private static final StrLookup<String> SYSTEM_PROPERTIES_LOOKUP = new SystemPropertiesStrLookup();

    protected StrLookup() {
    }

    public static <V> StrLookup<V> mapLookup(Map<String, V> map) {
        return new MapStrLookup(map);
    }

    public static StrLookup<?> noneLookup() {
        return NONE_LOOKUP;
    }

    public static StrLookup<String> systemPropertiesLookup() {
        return SYSTEM_PROPERTIES_LOOKUP;
    }

    public abstract String lookup(String str);

    static class MapStrLookup<V> extends StrLookup<V> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, V> f16565;

        MapStrLookup(Map<String, V> map) {
            this.f16565 = map;
        }

        public String lookup(String key) {
            Object obj;
            if (this.f16565 == null || (obj = this.f16565.get(key)) == null) {
                return null;
            }
            return obj.toString();
        }
    }

    private static class SystemPropertiesStrLookup extends StrLookup<String> {
        private SystemPropertiesStrLookup() {
        }

        public String lookup(String key) {
            if (key.length() > 0) {
                try {
                    return System.getProperty(key);
                } catch (SecurityException e) {
                }
            }
            return null;
        }
    }
}
