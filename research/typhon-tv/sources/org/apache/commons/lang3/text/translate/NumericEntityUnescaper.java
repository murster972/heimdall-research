package org.apache.commons.lang3.text.translate;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.EnumSet;

@Deprecated
public class NumericEntityUnescaper extends CharSequenceTranslator {
    private final EnumSet<OPTION> options;

    public enum OPTION {
        semiColonRequired,
        semiColonOptional,
        errorIfNoSemiColon
    }

    public NumericEntityUnescaper(OPTION... optionArr) {
        if (optionArr.length > 0) {
            this.options = EnumSet.copyOf(Arrays.asList(optionArr));
            return;
        }
        this.options = EnumSet.copyOf(Arrays.asList(new OPTION[]{OPTION.semiColonRequired}));
    }

    public boolean isSet(OPTION option) {
        return this.options != null && this.options.contains(option);
    }

    public int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
        int parseInt;
        int length = charSequence.length();
        if (charSequence.charAt(i) != '&' || i >= length - 2 || charSequence.charAt(i + 1) != '#') {
            return 0;
        }
        int i2 = i + 2;
        boolean z = false;
        char charAt = charSequence.charAt(i2);
        if (charAt == 'x' || charAt == 'X') {
            i2++;
            z = true;
            if (i2 == length) {
                return 0;
            }
        }
        int i3 = i2;
        while (i3 < length && ((charSequence.charAt(i3) >= '0' && charSequence.charAt(i3) <= '9') || ((charSequence.charAt(i3) >= 'a' && charSequence.charAt(i3) <= 'f') || (charSequence.charAt(i3) >= 'A' && charSequence.charAt(i3) <= 'F')))) {
            i3++;
        }
        boolean z2 = i3 != length && charSequence.charAt(i3) == ';';
        if (!z2) {
            if (isSet(OPTION.semiColonRequired)) {
                return 0;
            }
            if (isSet(OPTION.errorIfNoSemiColon)) {
                throw new IllegalArgumentException("Semi-colon required at end of numeric entity");
            }
        }
        if (z) {
            try {
                parseInt = Integer.parseInt(charSequence.subSequence(i2, i3).toString(), 16);
            } catch (NumberFormatException e) {
                return 0;
            }
        } else {
            parseInt = Integer.parseInt(charSequence.subSequence(i2, i3).toString(), 10);
        }
        if (parseInt > 65535) {
            char[] chars = Character.toChars(parseInt);
            writer.write(chars[0]);
            writer.write(chars[1]);
        } else {
            writer.write(parseInt);
        }
        return (z2 ? 1 : 0) + ((i3 + 2) - i2) + (z ? 1 : 0);
    }
}
