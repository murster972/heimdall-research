package org.apache.commons.lang3.text.translate;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;

@Deprecated
public class LookupTranslator extends CharSequenceTranslator {
    private final int longest;
    private final HashMap<String, String> lookupMap = new HashMap<>();
    private final HashSet<Character> prefixSet = new HashSet<>();
    private final int shortest;

    public LookupTranslator(CharSequence[]... charSequenceArr) {
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        int i2 = 0;
        if (charSequenceArr != null) {
            for (CharSequence[] charSequenceArr2 : charSequenceArr) {
                this.lookupMap.put(charSequenceArr2[0].toString(), charSequenceArr2[1].toString());
                this.prefixSet.add(Character.valueOf(charSequenceArr2[0].charAt(0)));
                int length = charSequenceArr2[0].length();
                i = length < i ? length : i;
                if (length > i2) {
                    i2 = length;
                }
            }
        }
        this.shortest = i;
        this.longest = i2;
    }

    public int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
        if (this.prefixSet.contains(Character.valueOf(charSequence.charAt(i)))) {
            int i2 = this.longest;
            if (this.longest + i > charSequence.length()) {
                i2 = charSequence.length() - i;
            }
            for (int i3 = i2; i3 >= this.shortest; i3--) {
                String str = this.lookupMap.get(charSequence.subSequence(i, i + i3).toString());
                if (str != null) {
                    writer.write(str);
                    return i3;
                }
            }
        }
        return 0;
    }
}
