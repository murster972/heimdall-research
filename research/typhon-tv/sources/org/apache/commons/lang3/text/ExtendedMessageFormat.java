package org.apache.commons.lang3.text;

import java.text.Format;
import java.text.MessageFormat;
import java.text.ParsePosition;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.ObjectUtils;

@Deprecated
public class ExtendedMessageFormat extends MessageFormat {
    static final /* synthetic */ boolean $assertionsDisabled = (!ExtendedMessageFormat.class.desiredAssertionStatus());
    private static final String DUMMY_PATTERN = "";
    private static final char END_FE = '}';
    private static final int HASH_SEED = 31;
    private static final char QUOTE = '\'';
    private static final char START_FE = '{';
    private static final char START_FMT = ',';
    private static final long serialVersionUID = -2362048321261811743L;
    private final Map<String, ? extends FormatFactory> registry;
    private String toPattern;

    public ExtendedMessageFormat(String str) {
        this(str, Locale.getDefault());
    }

    public ExtendedMessageFormat(String str, Locale locale) {
        this(str, locale, (Map<String, ? extends FormatFactory>) null);
    }

    public ExtendedMessageFormat(String str, Locale locale, Map<String, ? extends FormatFactory> map) {
        super("");
        setLocale(locale);
        this.registry = map;
        applyPattern(str);
    }

    public ExtendedMessageFormat(String str, Map<String, ? extends FormatFactory> map) {
        this(str, Locale.getDefault(), map);
    }

    private StringBuilder appendQuotedString(String str, ParsePosition parsePosition, StringBuilder sb) {
        if ($assertionsDisabled || str.toCharArray()[parsePosition.getIndex()] == '\'') {
            if (sb != null) {
                sb.append(QUOTE);
            }
            next(parsePosition);
            int index = parsePosition.getIndex();
            char[] charArray = str.toCharArray();
            int i = index;
            int index2 = parsePosition.getIndex();
            while (index2 < str.length()) {
                switch (charArray[parsePosition.getIndex()]) {
                    case '\'':
                        next(parsePosition);
                        if (sb == null) {
                            return null;
                        }
                        return sb.append(charArray, i, parsePosition.getIndex() - i);
                    default:
                        next(parsePosition);
                        index2++;
                }
            }
            throw new IllegalArgumentException("Unterminated quoted string at position " + index);
        }
        throw new AssertionError("Quoted string must start with quote character");
    }

    private boolean containsElements(Collection<?> collection) {
        if (collection == null || collection.isEmpty()) {
            return false;
        }
        for (Object obj : collection) {
            if (obj != null) {
                return true;
            }
        }
        return false;
    }

    private Format getFormat(String str) {
        if (this.registry != null) {
            String str2 = str;
            String str3 = null;
            int indexOf = str.indexOf(44);
            if (indexOf > 0) {
                str2 = str.substring(0, indexOf).trim();
                str3 = str.substring(indexOf + 1).trim();
            }
            FormatFactory formatFactory = (FormatFactory) this.registry.get(str2);
            if (formatFactory != null) {
                return formatFactory.getFormat(str2, str3, getLocale());
            }
        }
        return null;
    }

    private void getQuotedString(String str, ParsePosition parsePosition) {
        appendQuotedString(str, parsePosition, (StringBuilder) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0050, code lost:
        r3 = r3 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String insertFormats(java.lang.String r9, java.util.ArrayList<java.lang.String> r10) {
        /*
            r8 = this;
            boolean r6 = r8.containsElements(r10)
            if (r6 != 0) goto L_0x0007
        L_0x0006:
            return r9
        L_0x0007:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            int r6 = r9.length()
            int r6 = r6 * 2
            r5.<init>(r6)
            java.text.ParsePosition r4 = new java.text.ParsePosition
            r6 = 0
            r4.<init>(r6)
            r3 = -1
            r2 = 0
        L_0x001a:
            int r6 = r4.getIndex()
            int r7 = r9.length()
            if (r6 >= r7) goto L_0x0067
            int r6 = r4.getIndex()
            char r0 = r9.charAt(r6)
            switch(r0) {
                case 39: goto L_0x0036;
                case 123: goto L_0x003a;
                case 125: goto L_0x0064;
                default: goto L_0x002f;
            }
        L_0x002f:
            r5.append(r0)
            r8.next(r4)
            goto L_0x001a
        L_0x0036:
            r8.appendQuotedString(r9, r4, r5)
            goto L_0x001a
        L_0x003a:
            int r2 = r2 + 1
            r6 = 123(0x7b, float:1.72E-43)
            java.lang.StringBuilder r6 = r5.append(r6)
            java.text.ParsePosition r7 = r8.next(r4)
            int r7 = r8.readArgumentIndex(r9, r7)
            r6.append(r7)
            r6 = 1
            if (r2 != r6) goto L_0x001a
            int r3 = r3 + 1
            java.lang.Object r1 = r10.get(r3)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x001a
            r6 = 44
            java.lang.StringBuilder r6 = r5.append(r6)
            r6.append(r1)
            goto L_0x001a
        L_0x0064:
            int r2 = r2 + -1
            goto L_0x002f
        L_0x0067:
            java.lang.String r9 = r5.toString()
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.text.ExtendedMessageFormat.insertFormats(java.lang.String, java.util.ArrayList):java.lang.String");
    }

    private ParsePosition next(ParsePosition parsePosition) {
        parsePosition.setIndex(parsePosition.getIndex() + 1);
        return parsePosition;
    }

    private String parseFormatDescription(String str, ParsePosition parsePosition) {
        int index = parsePosition.getIndex();
        seekNonWs(str, parsePosition);
        int index2 = parsePosition.getIndex();
        int i = 1;
        while (parsePosition.getIndex() < str.length()) {
            switch (str.charAt(parsePosition.getIndex())) {
                case '\'':
                    getQuotedString(str, parsePosition);
                    break;
                case '{':
                    i++;
                    break;
                case '}':
                    i--;
                    if (i != 0) {
                        break;
                    } else {
                        return str.substring(index2, parsePosition.getIndex());
                    }
            }
            next(parsePosition);
        }
        throw new IllegalArgumentException("Unterminated format element at position " + index);
    }

    private int readArgumentIndex(String str, ParsePosition parsePosition) {
        int index = parsePosition.getIndex();
        seekNonWs(str, parsePosition);
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        while (!z && parsePosition.getIndex() < str.length()) {
            char charAt = str.charAt(parsePosition.getIndex());
            if (Character.isWhitespace(charAt)) {
                seekNonWs(str, parsePosition);
                charAt = str.charAt(parsePosition.getIndex());
                if (!(charAt == ',' || charAt == '}')) {
                    z = true;
                    next(parsePosition);
                }
            }
            if ((charAt == ',' || charAt == '}') && sb.length() > 0) {
                try {
                    return Integer.parseInt(sb.toString());
                } catch (NumberFormatException e) {
                }
            }
            z = !Character.isDigit(charAt);
            sb.append(charAt);
            next(parsePosition);
        }
        if (z) {
            throw new IllegalArgumentException("Invalid format argument index at position " + index + ": " + str.substring(index, parsePosition.getIndex()));
        }
        throw new IllegalArgumentException("Unterminated format element at position " + index);
    }

    private void seekNonWs(String str, ParsePosition parsePosition) {
        char[] charArray = str.toCharArray();
        do {
            int isMatch = StrMatcher.splitMatcher().isMatch(charArray, parsePosition.getIndex());
            parsePosition.setIndex(parsePosition.getIndex() + isMatch);
            if (isMatch <= 0 || parsePosition.getIndex() >= str.length()) {
            }
            int isMatch2 = StrMatcher.splitMatcher().isMatch(charArray, parsePosition.getIndex());
            parsePosition.setIndex(parsePosition.getIndex() + isMatch2);
            return;
        } while (parsePosition.getIndex() >= str.length());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00a6, code lost:
        r7 = parseFormatDescription(r21, next(r14));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void applyPattern(java.lang.String r21) {
        /*
            r20 = this;
            r0 = r20
            java.util.Map<java.lang.String, ? extends org.apache.commons.lang3.text.FormatFactory> r0 = r0.registry
            r17 = r0
            if (r17 != 0) goto L_0x0016
            super.applyPattern(r21)
            java.lang.String r17 = super.toPattern()
            r0 = r17
            r1 = r20
            r1.toPattern = r0
        L_0x0015:
            return
        L_0x0016:
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            int r17 = r21.length()
            r16.<init>(r17)
            java.text.ParsePosition r14 = new java.text.ParsePosition
            r17 = 0
            r0 = r17
            r14.<init>(r0)
            char[] r3 = r21.toCharArray()
            r5 = 0
        L_0x0037:
            int r17 = r14.getIndex()
            int r18 = r21.length()
            r0 = r17
            r1 = r18
            if (r0 >= r1) goto L_0x011c
            int r17 = r14.getIndex()
            char r17 = r3[r17]
            switch(r17) {
                case 39: goto L_0x005d;
                case 123: goto L_0x0067;
                default: goto L_0x004e;
            }
        L_0x004e:
            int r17 = r14.getIndex()
            char r17 = r3[r17]
            r16.append(r17)
            r0 = r20
            r0.next(r14)
            goto L_0x0037
        L_0x005d:
            r0 = r20
            r1 = r21
            r2 = r16
            r0.appendQuotedString(r1, r14, r2)
            goto L_0x0037
        L_0x0067:
            int r5 = r5 + 1
            r0 = r20
            r1 = r21
            r0.seekNonWs(r1, r14)
            int r15 = r14.getIndex()
            r0 = r20
            java.text.ParsePosition r17 = r0.next(r14)
            r0 = r20
            r1 = r21
            r2 = r17
            int r11 = r0.readArgumentIndex(r1, r2)
            r17 = 123(0x7b, float:1.72E-43)
            java.lang.StringBuilder r17 = r16.append(r17)
            r0 = r17
            r0.append(r11)
            r0 = r20
            r1 = r21
            r0.seekNonWs(r1, r14)
            r6 = 0
            r7 = 0
            int r17 = r14.getIndex()
            char r17 = r3[r17]
            r18 = 44
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00c9
            r0 = r20
            java.text.ParsePosition r17 = r0.next(r14)
            r0 = r20
            r1 = r21
            r2 = r17
            java.lang.String r7 = r0.parseFormatDescription(r1, r2)
            r0 = r20
            java.text.Format r6 = r0.getFormat(r7)
            if (r6 != 0) goto L_0x00c9
            r17 = 44
            java.lang.StringBuilder r17 = r16.append(r17)
            r0 = r17
            r0.append(r7)
        L_0x00c9:
            r9.add(r6)
            if (r6 != 0) goto L_0x00cf
            r7 = 0
        L_0x00cf:
            r8.add(r7)
            int r17 = r9.size()
            r0 = r17
            if (r0 != r5) goto L_0x0116
            r17 = 1
        L_0x00dc:
            org.apache.commons.lang3.Validate.isTrue(r17)
            int r17 = r8.size()
            r0 = r17
            if (r0 != r5) goto L_0x0119
            r17 = 1
        L_0x00e9:
            org.apache.commons.lang3.Validate.isTrue(r17)
            int r17 = r14.getIndex()
            char r17 = r3[r17]
            r18 = 125(0x7d, float:1.75E-43)
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x004e
            java.lang.IllegalArgumentException r17 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "Unreadable format element at position "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            java.lang.StringBuilder r18 = r0.append(r15)
            java.lang.String r18 = r18.toString()
            r17.<init>(r18)
            throw r17
        L_0x0116:
            r17 = 0
            goto L_0x00dc
        L_0x0119:
            r17 = 0
            goto L_0x00e9
        L_0x011c:
            java.lang.String r17 = r16.toString()
            r0 = r20
            r1 = r17
            super.applyPattern(r1)
            java.lang.String r17 = super.toPattern()
            r0 = r20
            r1 = r17
            java.lang.String r17 = r0.insertFormats(r1, r8)
            r0 = r17
            r1 = r20
            r1.toPattern = r0
            r0 = r20
            boolean r17 = r0.containsElements(r9)
            if (r17 == 0) goto L_0x0015
            java.text.Format[] r13 = r20.getFormats()
            r10 = 0
            java.util.Iterator r12 = r9.iterator()
        L_0x014a:
            boolean r17 = r12.hasNext()
            if (r17 == 0) goto L_0x015d
            java.lang.Object r4 = r12.next()
            java.text.Format r4 = (java.text.Format) r4
            if (r4 == 0) goto L_0x015a
            r13[r10] = r4
        L_0x015a:
            int r10 = r10 + 1
            goto L_0x014a
        L_0x015d:
            r0 = r20
            super.setFormats(r13)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.text.ExtendedMessageFormat.applyPattern(java.lang.String):void");
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (obj == null || !super.equals(obj) || ObjectUtils.notEqual(getClass(), obj.getClass())) {
            return false;
        }
        ExtendedMessageFormat extendedMessageFormat = (ExtendedMessageFormat) obj;
        if (ObjectUtils.notEqual(this.toPattern, extendedMessageFormat.toPattern)) {
            return false;
        }
        if (ObjectUtils.notEqual(this.registry, extendedMessageFormat.registry)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (((super.hashCode() * 31) + Objects.hashCode(this.registry)) * 31) + Objects.hashCode(this.toPattern);
    }

    public void setFormat(int i, Format format) {
        throw new UnsupportedOperationException();
    }

    public void setFormatByArgumentIndex(int i, Format format) {
        throw new UnsupportedOperationException();
    }

    public void setFormats(Format[] formatArr) {
        throw new UnsupportedOperationException();
    }

    public void setFormatsByArgumentIndex(Format[] formatArr) {
        throw new UnsupportedOperationException();
    }

    public String toPattern() {
        return this.toPattern;
    }
}
