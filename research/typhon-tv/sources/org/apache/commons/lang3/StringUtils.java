package org.apache.commons.lang3;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Pattern;

public class StringUtils {
    public static final String CR = "\r";
    public static final String EMPTY = "";
    public static final int INDEX_NOT_FOUND = -1;
    public static final String LF = "\n";
    private static final int PAD_LIMIT = 8192;
    public static final String SPACE = " ";

    public static String abbreviate(String str, int i) {
        return abbreviate(str, "...", 0, i);
    }

    public static String abbreviate(String str, int i, int i2) {
        return abbreviate(str, "...", i, i2);
    }

    public static String abbreviate(String str, String str2, int i) {
        return abbreviate(str, str2, 0, i);
    }

    public static String abbreviate(String str, String str2, int i, int i2) {
        if (isEmpty(str) || isEmpty(str2)) {
            return str;
        }
        int length = str2.length();
        int i3 = length + 1;
        int i4 = length + length + 1;
        if (i2 < i3) {
            throw new IllegalArgumentException(String.format("Minimum abbreviation width is %d", new Object[]{Integer.valueOf(i3)}));
        } else if (str.length() <= i2) {
            return str;
        } else {
            if (i > str.length()) {
                i = str.length();
            }
            if (str.length() - i < i2 - length) {
                i = str.length() - (i2 - length);
            }
            if (i <= length + 1) {
                return str.substring(0, i2 - length) + str2;
            }
            if (i2 >= i4) {
                return (i + i2) - length < str.length() ? str2 + abbreviate(str.substring(i), str2, i2 - length) : str2 + str.substring(str.length() - (i2 - length));
            }
            throw new IllegalArgumentException(String.format("Minimum abbreviation width with offset is %d", new Object[]{Integer.valueOf(i4)}));
        }
    }

    public static String abbreviateMiddle(String str, String str2, int i) {
        if (isEmpty(str) || isEmpty(str2) || i >= str.length() || i < str2.length() + 2) {
            return str;
        }
        int length = i - str2.length();
        return str.substring(0, (length / 2) + (length % 2)) + str2 + str.substring(str.length() - (length / 2));
    }

    private static String appendIfMissing(String str, CharSequence charSequence, boolean z, CharSequence... charSequenceArr) {
        if (str == null || isEmpty(charSequence) || endsWith(str, charSequence, z)) {
            return str;
        }
        if (charSequenceArr != null && charSequenceArr.length > 0) {
            for (CharSequence endsWith : charSequenceArr) {
                if (endsWith(str, endsWith, z)) {
                    return str;
                }
            }
        }
        return str + charSequence.toString();
    }

    public static String appendIfMissing(String str, CharSequence charSequence, CharSequence... charSequenceArr) {
        return appendIfMissing(str, charSequence, false, charSequenceArr);
    }

    public static String appendIfMissingIgnoreCase(String str, CharSequence charSequence, CharSequence... charSequenceArr) {
        return appendIfMissing(str, charSequence, true, charSequenceArr);
    }

    public static String capitalize(String str) {
        int length;
        int codePointAt;
        int titleCase;
        if (str == null || (length = str.length()) == 0 || (codePointAt = str.codePointAt(0)) == (titleCase = Character.toTitleCase(codePointAt))) {
            return str;
        }
        int[] iArr = new int[length];
        int i = 0 + 1;
        iArr[0] = titleCase;
        int charCount = Character.charCount(codePointAt);
        while (charCount < length) {
            int codePointAt2 = str.codePointAt(charCount);
            iArr[i] = codePointAt2;
            charCount += Character.charCount(codePointAt2);
            i++;
        }
        return new String(iArr, 0, i);
    }

    public static String center(String str, int i) {
        return center(str, i, ' ');
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        r1 = r3.length();
        r0 = r4 - r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String center(java.lang.String r3, int r4, char r5) {
        /*
            if (r3 == 0) goto L_0x0004
            if (r4 > 0) goto L_0x0005
        L_0x0004:
            return r3
        L_0x0005:
            int r1 = r3.length()
            int r0 = r4 - r1
            if (r0 <= 0) goto L_0x0004
            int r2 = r0 / 2
            int r2 = r2 + r1
            java.lang.String r3 = leftPad((java.lang.String) r3, (int) r2, (char) r5)
            java.lang.String r3 = rightPad((java.lang.String) r3, (int) r4, (char) r5)
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.StringUtils.center(java.lang.String, int, char):java.lang.String");
    }

    public static String center(String str, int i, String str2) {
        if (str == null || i <= 0) {
            return str;
        }
        if (isEmpty(str2)) {
            str2 = SPACE;
        }
        int length = str.length();
        int i2 = i - length;
        return i2 > 0 ? rightPad(leftPad(str, (i2 / 2) + length, str2), i, str2) : str;
    }

    public static String chomp(String str) {
        if (isEmpty(str)) {
            return str;
        }
        if (str.length() == 1) {
            char charAt = str.charAt(0);
            return (charAt == 13 || charAt == 10) ? "" : str;
        }
        int length = str.length() - 1;
        char charAt2 = str.charAt(length);
        if (charAt2 == 10) {
            if (str.charAt(length - 1) == 13) {
                length--;
            }
        } else if (charAt2 != 13) {
            length++;
        }
        return str.substring(0, length);
    }

    @Deprecated
    public static String chomp(String str, String str2) {
        return removeEnd(str, str2);
    }

    public static String chop(String str) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (length < 2) {
            return "";
        }
        int i = length - 1;
        String substring = str.substring(0, i);
        return (str.charAt(i) == 10 && substring.charAt(i + -1) == 13) ? substring.substring(0, i - 1) : substring;
    }

    public static int compare(String str, String str2) {
        return compare(str, str2, true);
    }

    public static int compare(String str, String str2, boolean z) {
        int i = 1;
        if (str == str2) {
            return 0;
        }
        if (str == null) {
            return !z ? 1 : -1;
        }
        if (str2 != null) {
            return str.compareTo(str2);
        }
        if (!z) {
            i = -1;
        }
        return i;
    }

    public static int compareIgnoreCase(String str, String str2) {
        return compareIgnoreCase(str, str2, true);
    }

    public static int compareIgnoreCase(String str, String str2, boolean z) {
        int i = 1;
        if (str == str2) {
            return 0;
        }
        if (str == null) {
            return !z ? 1 : -1;
        }
        if (str2 != null) {
            return str.compareToIgnoreCase(str2);
        }
        if (!z) {
            i = -1;
        }
        return i;
    }

    public static boolean contains(CharSequence charSequence, int i) {
        return !isEmpty(charSequence) && CharSequenceUtils.indexOf(charSequence, i, 0) >= 0;
    }

    public static boolean contains(CharSequence charSequence, CharSequence charSequence2) {
        return (charSequence == null || charSequence2 == null || CharSequenceUtils.indexOf(charSequence, charSequence2, 0) < 0) ? false : true;
    }

    public static boolean containsAny(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence2 == null) {
            return false;
        }
        return containsAny(charSequence, CharSequenceUtils.toCharArray(charSequence2));
    }

    public static boolean containsAny(CharSequence charSequence, char... cArr) {
        if (isEmpty(charSequence) || ArrayUtils.isEmpty(cArr)) {
            return false;
        }
        int length = charSequence.length();
        int length2 = cArr.length;
        int i = length - 1;
        int i2 = length2 - 1;
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = charSequence.charAt(i3);
            for (int i4 = 0; i4 < length2; i4++) {
                if (cArr[i4] == charAt) {
                    if (!Character.isHighSurrogate(charAt) || i4 == i2) {
                        return true;
                    }
                    if (i3 < i && cArr[i4 + 1] == charSequence.charAt(i3 + 1)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean containsAny(CharSequence charSequence, CharSequence... charSequenceArr) {
        if (isEmpty(charSequence) || ArrayUtils.isEmpty((Object[]) charSequenceArr)) {
            return false;
        }
        for (CharSequence contains : charSequenceArr) {
            if (contains(charSequence, contains)) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsIgnoreCase(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            return false;
        }
        int length = charSequence2.length();
        int length2 = charSequence.length() - length;
        for (int i = 0; i <= length2; i++) {
            if (CharSequenceUtils.regionMatches(charSequence, true, i, charSequence2, 0, length)) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsNone(CharSequence charSequence, String str) {
        if (charSequence == null || str == null) {
            return true;
        }
        return containsNone(charSequence, str.toCharArray());
    }

    public static boolean containsNone(CharSequence charSequence, char... cArr) {
        if (charSequence == null || cArr == null) {
            return true;
        }
        int length = charSequence.length();
        int i = length - 1;
        int length2 = cArr.length;
        int i2 = length2 - 1;
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = charSequence.charAt(i3);
            for (int i4 = 0; i4 < length2; i4++) {
                if (cArr[i4] == charAt) {
                    if (!Character.isHighSurrogate(charAt) || i4 == i2) {
                        return false;
                    }
                    if (i3 < i && cArr[i4 + 1] == charSequence.charAt(i3 + 1)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static boolean containsOnly(CharSequence charSequence, String str) {
        if (charSequence == null || str == null) {
            return false;
        }
        return containsOnly(charSequence, str.toCharArray());
    }

    public static boolean containsOnly(CharSequence charSequence, char... cArr) {
        if (cArr == null || charSequence == null) {
            return false;
        }
        if (charSequence.length() == 0) {
            return true;
        }
        if (cArr.length == 0) {
            return false;
        }
        return indexOfAnyBut(charSequence, cArr) == -1;
    }

    public static boolean containsWhitespace(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (Character.isWhitespace(charSequence.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private static void convertRemainingAccentCharacters(StringBuilder sb) {
        for (int i = 0; i < sb.length(); i++) {
            if (sb.charAt(i) == 321) {
                sb.deleteCharAt(i);
                sb.insert(i, 'L');
            } else if (sb.charAt(i) == 322) {
                sb.deleteCharAt(i);
                sb.insert(i, 'l');
            }
        }
    }

    public static int countMatches(CharSequence charSequence, char c) {
        if (isEmpty(charSequence)) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < charSequence.length(); i2++) {
            if (c == charSequence.charAt(i2)) {
                i++;
            }
        }
        return i;
    }

    public static int countMatches(CharSequence charSequence, CharSequence charSequence2) {
        if (isEmpty(charSequence) || isEmpty(charSequence2)) {
            return 0;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            int indexOf = CharSequenceUtils.indexOf(charSequence, charSequence2, i2);
            if (indexOf == -1) {
                return i;
            }
            i++;
            i2 = indexOf + charSequence2.length();
        }
    }

    public static <T extends CharSequence> T defaultIfBlank(T t, T t2) {
        return isBlank(t) ? t2 : t;
    }

    public static <T extends CharSequence> T defaultIfEmpty(T t, T t2) {
        return isEmpty(t) ? t2 : t;
    }

    public static String defaultString(String str) {
        return str == null ? "" : str;
    }

    public static String defaultString(String str, String str2) {
        return str == null ? str2 : str;
    }

    public static String deleteWhitespace(String str) {
        int i;
        if (isEmpty(str)) {
            return str;
        }
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            if (!Character.isWhitespace(str.charAt(i2))) {
                i = i3 + 1;
                cArr[i3] = str.charAt(i2);
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        return i3 != length ? new String(cArr, 0, i3) : str;
    }

    public static String difference(String str, String str2) {
        if (str == null) {
            return str2;
        }
        if (str2 == null) {
            return str;
        }
        int indexOfDifference = indexOfDifference(str, str2);
        return indexOfDifference == -1 ? "" : str2.substring(indexOfDifference);
    }

    public static boolean endsWith(CharSequence charSequence, CharSequence charSequence2) {
        return endsWith(charSequence, charSequence2, false);
    }

    private static boolean endsWith(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        if (charSequence == null || charSequence2 == null) {
            return charSequence == null && charSequence2 == null;
        }
        if (charSequence2.length() > charSequence.length()) {
            return false;
        }
        return CharSequenceUtils.regionMatches(charSequence, z, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length());
    }

    public static boolean endsWithAny(CharSequence charSequence, CharSequence... charSequenceArr) {
        if (isEmpty(charSequence) || ArrayUtils.isEmpty((Object[]) charSequenceArr)) {
            return false;
        }
        for (CharSequence endsWith : charSequenceArr) {
            if (endsWith(charSequence, endsWith)) {
                return true;
            }
        }
        return false;
    }

    public static boolean endsWithIgnoreCase(CharSequence charSequence, CharSequence charSequence2) {
        return endsWith(charSequence, charSequence2, true);
    }

    public static boolean equals(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == charSequence2) {
            return true;
        }
        if (charSequence == null || charSequence2 == null || charSequence.length() != charSequence2.length()) {
            return false;
        }
        if ((charSequence instanceof String) && (charSequence2 instanceof String)) {
            return charSequence.equals(charSequence2);
        }
        return CharSequenceUtils.regionMatches(charSequence, false, 0, charSequence2, 0, charSequence.length());
    }

    public static boolean equalsAny(CharSequence charSequence, CharSequence... charSequenceArr) {
        if (ArrayUtils.isNotEmpty((T[]) charSequenceArr)) {
            for (CharSequence equals : charSequenceArr) {
                if (equals(charSequence, equals)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean equalsAnyIgnoreCase(CharSequence charSequence, CharSequence... charSequenceArr) {
        if (ArrayUtils.isNotEmpty((T[]) charSequenceArr)) {
            for (CharSequence equalsIgnoreCase : charSequenceArr) {
                if (equalsIgnoreCase(charSequence, equalsIgnoreCase)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean equalsIgnoreCase(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            return charSequence == charSequence2;
        }
        if (charSequence == charSequence2) {
            return true;
        }
        if (charSequence.length() != charSequence2.length()) {
            return false;
        }
        return CharSequenceUtils.regionMatches(charSequence, true, 0, charSequence2, 0, charSequence.length());
    }

    public static String getCommonPrefix(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return "";
        }
        int indexOfDifference = indexOfDifference(strArr);
        return indexOfDifference == -1 ? strArr[0] == null ? "" : strArr[0] : indexOfDifference == 0 ? "" : strArr[0].substring(0, indexOfDifference);
    }

    public static String getDigits(String str) {
        if (isEmpty(str)) {
            return str;
        }
        int length = str.length();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (Character.isDigit(charAt)) {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    @Deprecated
    public static int getFuzzyDistance(CharSequence charSequence, CharSequence charSequence2, Locale locale) {
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (locale == null) {
            throw new IllegalArgumentException("Locale must not be null");
        } else {
            String lowerCase = charSequence.toString().toLowerCase(locale);
            String lowerCase2 = charSequence2.toString().toLowerCase(locale);
            int i = 0;
            int i2 = 0;
            int i3 = Integer.MIN_VALUE;
            for (int i4 = 0; i4 < lowerCase2.length(); i4++) {
                char charAt = lowerCase2.charAt(i4);
                boolean z = false;
                while (i2 < lowerCase.length() && !z) {
                    if (charAt == lowerCase.charAt(i2)) {
                        i++;
                        if (i3 + 1 == i2) {
                            i += 2;
                        }
                        i3 = i2;
                        z = true;
                    }
                    i2++;
                }
            }
            return i;
        }
    }

    @Deprecated
    public static double getJaroWinklerDistance(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        int[] matches = matches(charSequence, charSequence2);
        double d = (double) matches[0];
        if (d == 0.0d) {
            return 0.0d;
        }
        double length = (((d / ((double) charSequence.length())) + (d / ((double) charSequence2.length()))) + ((d - ((double) matches[1])) / d)) / 3.0d;
        return ((double) Math.round(100.0d * (length < 0.7d ? length : length + ((Math.min(0.1d, 1.0d / ((double) matches[3])) * ((double) matches[2])) * (1.0d - length))))) / 100.0d;
    }

    @Deprecated
    public static int getLevenshteinDistance(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        int length = charSequence.length();
        int length2 = charSequence2.length();
        if (length == 0) {
            int i = length2;
            int i2 = length;
            return length2;
        } else if (length2 == 0) {
            int i3 = length2;
            int i4 = length;
            return length;
        } else {
            if (length > length2) {
                CharSequence charSequence3 = charSequence;
                charSequence = charSequence2;
                charSequence2 = charSequence3;
                length = length2;
                length2 = charSequence2.length();
            }
            int[] iArr = new int[(length + 1)];
            for (int i5 = 0; i5 <= length; i5++) {
                iArr[i5] = i5;
            }
            for (int i6 = 1; i6 <= length2; i6++) {
                int i7 = iArr[0];
                char charAt = charSequence2.charAt(i6 - 1);
                iArr[0] = i6;
                for (int i8 = 1; i8 <= length; i8++) {
                    int i9 = iArr[i8];
                    iArr[i8] = Math.min(Math.min(iArr[i8 - 1] + 1, iArr[i8] + 1), i7 + (charSequence.charAt(i8 + -1) == charAt ? 0 : 1));
                    i7 = i9;
                }
            }
            int i10 = length2;
            int i11 = length;
            return iArr[length];
        }
    }

    @Deprecated
    public static int getLevenshteinDistance(CharSequence charSequence, CharSequence charSequence2, int i) {
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (i < 0) {
            throw new IllegalArgumentException("Threshold must not be negative");
        } else {
            int length = charSequence.length();
            int length2 = charSequence2.length();
            if (length == 0) {
                if (length2 <= i) {
                    return length2;
                }
                return -1;
            } else if (length2 == 0) {
                if (length <= i) {
                    return length;
                }
                return -1;
            } else if (Math.abs(length - length2) > i) {
                return -1;
            } else {
                if (length > length2) {
                    CharSequence charSequence3 = charSequence;
                    charSequence = charSequence2;
                    charSequence2 = charSequence3;
                    length = length2;
                    length2 = charSequence2.length();
                }
                int[] iArr = new int[(length + 1)];
                int[] iArr2 = new int[(length + 1)];
                int min = Math.min(length, i) + 1;
                for (int i2 = 0; i2 < min; i2++) {
                    iArr[i2] = i2;
                }
                Arrays.fill(iArr, min, iArr.length, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                Arrays.fill(iArr2, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                int i3 = 1;
                while (i3 <= length2) {
                    char charAt = charSequence2.charAt(i3 - 1);
                    iArr2[0] = i3;
                    int max = Math.max(1, i3 - i);
                    int min2 = i3 > MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - i ? length : Math.min(length, i3 + i);
                    if (max > min2) {
                        return -1;
                    }
                    if (max > 1) {
                        iArr2[max - 1] = Integer.MAX_VALUE;
                    }
                    for (int i4 = max; i4 <= min2; i4++) {
                        if (charSequence.charAt(i4 - 1) == charAt) {
                            iArr2[i4] = iArr[i4 - 1];
                        } else {
                            iArr2[i4] = Math.min(Math.min(iArr2[i4 - 1], iArr[i4]), iArr[i4 - 1]) + 1;
                        }
                    }
                    int[] iArr3 = iArr;
                    iArr = iArr2;
                    iArr2 = iArr3;
                    i3++;
                }
                if (iArr[length] <= i) {
                    return iArr[length];
                }
                return -1;
            }
        }
    }

    public static int indexOf(CharSequence charSequence, int i) {
        if (isEmpty(charSequence)) {
            return -1;
        }
        return CharSequenceUtils.indexOf(charSequence, i, 0);
    }

    public static int indexOf(CharSequence charSequence, int i, int i2) {
        if (isEmpty(charSequence)) {
            return -1;
        }
        return CharSequenceUtils.indexOf(charSequence, i, i2);
    }

    public static int indexOf(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            return -1;
        }
        return CharSequenceUtils.indexOf(charSequence, charSequence2, 0);
    }

    public static int indexOf(CharSequence charSequence, CharSequence charSequence2, int i) {
        if (charSequence == null || charSequence2 == null) {
            return -1;
        }
        return CharSequenceUtils.indexOf(charSequence, charSequence2, i);
    }

    public static int indexOfAny(CharSequence charSequence, String str) {
        if (isEmpty(charSequence) || isEmpty(str)) {
            return -1;
        }
        return indexOfAny(charSequence, str.toCharArray());
    }

    public static int indexOfAny(CharSequence charSequence, char... cArr) {
        if (isEmpty(charSequence) || ArrayUtils.isEmpty(cArr)) {
            return -1;
        }
        int length = charSequence.length();
        int i = length - 1;
        int length2 = cArr.length;
        int i2 = length2 - 1;
        int i3 = 0;
        while (i3 < length) {
            char charAt = charSequence.charAt(i3);
            for (int i4 = 0; i4 < length2; i4++) {
                if (cArr[i4] == charAt && (i3 >= i || i4 >= i2 || !Character.isHighSurrogate(charAt) || cArr[i4 + 1] == charSequence.charAt(i3 + 1))) {
                    return i3;
                }
            }
            i3++;
        }
        return -1;
    }

    public static int indexOfAny(CharSequence charSequence, CharSequence... charSequenceArr) {
        int indexOf;
        if (charSequence == null || charSequenceArr == null) {
            return -1;
        }
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        for (CharSequence charSequence2 : charSequenceArr) {
            if (!(charSequence2 == null || (indexOf = CharSequenceUtils.indexOf(charSequence, charSequence2, 0)) == -1 || indexOf >= i)) {
                i = indexOf;
            }
        }
        if (i == Integer.MAX_VALUE) {
            return -1;
        }
        return i;
    }

    public static int indexOfAnyBut(CharSequence charSequence, CharSequence charSequence2) {
        if (isEmpty(charSequence) || isEmpty(charSequence2)) {
            return -1;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            char charAt = charSequence.charAt(i);
            boolean z = CharSequenceUtils.indexOf(charSequence2, (int) charAt, 0) >= 0;
            if (i + 1 < length && Character.isHighSurrogate(charAt)) {
                char charAt2 = charSequence.charAt(i + 1);
                if (z && CharSequenceUtils.indexOf(charSequence2, (int) charAt2, 0) < 0) {
                    return i;
                }
            } else if (!z) {
                return i;
            }
        }
        return -1;
    }

    public static int indexOfAnyBut(CharSequence charSequence, char... cArr) {
        if (isEmpty(charSequence) || ArrayUtils.isEmpty(cArr)) {
            return -1;
        }
        int length = charSequence.length();
        int i = length - 1;
        int length2 = cArr.length;
        int i2 = length2 - 1;
        int i3 = 0;
        while (i3 < length) {
            char charAt = charSequence.charAt(i3);
            int i4 = 0;
            while (i4 < length2) {
                if (cArr[i4] != charAt || (i3 < i && i4 < i2 && Character.isHighSurrogate(charAt) && cArr[i4 + 1] != charSequence.charAt(i3 + 1))) {
                    i4++;
                } else {
                    i3++;
                }
            }
            return i3;
        }
        return -1;
    }

    public static int indexOfDifference(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == charSequence2) {
            return -1;
        }
        if (charSequence == null || charSequence2 == null) {
            return 0;
        }
        int i = 0;
        while (i < charSequence.length() && i < charSequence2.length() && charSequence.charAt(i) == charSequence2.charAt(i)) {
            i++;
        }
        if (i < charSequence2.length() || i < charSequence.length()) {
            return i;
        }
        return -1;
    }

    public static int indexOfDifference(CharSequence... charSequenceArr) {
        if (charSequenceArr == null || charSequenceArr.length <= 1) {
            return -1;
        }
        boolean z = false;
        boolean z2 = true;
        int length = charSequenceArr.length;
        int i = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
        int i2 = 0;
        for (CharSequence charSequence : charSequenceArr) {
            if (charSequence == null) {
                z = true;
                i = 0;
            } else {
                z2 = false;
                i = Math.min(charSequence.length(), i);
                i2 = Math.max(charSequence.length(), i2);
            }
        }
        if (z2 || (i2 == 0 && !z)) {
            return -1;
        }
        if (i == 0) {
            return 0;
        }
        int i3 = -1;
        for (int i4 = 0; i4 < i; i4++) {
            char charAt = charSequenceArr[0].charAt(i4);
            int i5 = 1;
            while (true) {
                if (i5 >= length) {
                    break;
                } else if (charSequenceArr[i5].charAt(i4) != charAt) {
                    i3 = i4;
                    break;
                } else {
                    i5++;
                }
            }
            if (i3 != -1) {
                break;
            }
        }
        return (i3 != -1 || i == i2) ? i3 : i;
    }

    public static int indexOfIgnoreCase(CharSequence charSequence, CharSequence charSequence2) {
        return indexOfIgnoreCase(charSequence, charSequence2, 0);
    }

    public static int indexOfIgnoreCase(CharSequence charSequence, CharSequence charSequence2, int i) {
        if (charSequence == null || charSequence2 == null) {
            int i2 = i;
            return -1;
        }
        if (i < 0) {
            i = 0;
        }
        int length = (charSequence.length() - charSequence2.length()) + 1;
        if (i > length) {
            int i3 = i;
            return -1;
        } else if (charSequence2.length() == 0) {
            int i4 = i;
            return i;
        } else {
            for (int i5 = i; i5 < length; i5++) {
                if (CharSequenceUtils.regionMatches(charSequence, true, i5, charSequence2, 0, charSequence2.length())) {
                    int i6 = i;
                    return i5;
                }
            }
            int i7 = i;
            return -1;
        }
    }

    public static boolean isAllBlank(CharSequence... charSequenceArr) {
        if (ArrayUtils.isEmpty((Object[]) charSequenceArr)) {
            return true;
        }
        for (CharSequence isNotBlank : charSequenceArr) {
            if (isNotBlank(isNotBlank)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllEmpty(CharSequence... charSequenceArr) {
        if (ArrayUtils.isEmpty((Object[]) charSequenceArr)) {
            return true;
        }
        for (CharSequence isNotEmpty : charSequenceArr) {
            if (isNotEmpty(isNotEmpty)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllLowerCase(CharSequence charSequence) {
        if (charSequence == null || isEmpty(charSequence)) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isLowerCase(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAllUpperCase(CharSequence charSequence) {
        if (charSequence == null || isEmpty(charSequence)) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isUpperCase(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAlpha(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isLetter(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAlphaSpace(CharSequence charSequence) {
        if (charSequence == null) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isLetter(charSequence.charAt(i)) && charSequence.charAt(i) != ' ') {
                return false;
            }
        }
        return true;
    }

    public static boolean isAlphanumeric(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isLetterOrDigit(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAlphanumericSpace(CharSequence charSequence) {
        if (charSequence == null) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isLetterOrDigit(charSequence.charAt(i)) && charSequence.charAt(i) != ' ') {
                return false;
            }
        }
        return true;
    }

    public static boolean isAnyBlank(CharSequence... charSequenceArr) {
        if (ArrayUtils.isEmpty((Object[]) charSequenceArr)) {
            return false;
        }
        for (CharSequence isBlank : charSequenceArr) {
            if (isBlank(isBlank)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAnyEmpty(CharSequence... charSequenceArr) {
        if (ArrayUtils.isEmpty((Object[]) charSequenceArr)) {
            return false;
        }
        for (CharSequence isEmpty : charSequenceArr) {
            if (isEmpty(isEmpty)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAsciiPrintable(CharSequence charSequence) {
        if (charSequence == null) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!CharUtils.isAsciiPrintable(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isBlank(CharSequence charSequence) {
        int length;
        if (charSequence == null || (length = charSequence.length()) == 0) {
            return true;
        }
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEmpty(CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }

    public static boolean isMixedCase(CharSequence charSequence) {
        if (isEmpty(charSequence) || charSequence.length() == 1) {
            return false;
        }
        boolean z = false;
        boolean z2 = false;
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (z && z2) {
                return true;
            }
            if (Character.isUpperCase(charSequence.charAt(i))) {
                z = true;
            } else if (Character.isLowerCase(charSequence.charAt(i))) {
                z2 = true;
            }
        }
        return z && z2;
    }

    public static boolean isNoneBlank(CharSequence... charSequenceArr) {
        return !isAnyBlank(charSequenceArr);
    }

    public static boolean isNoneEmpty(CharSequence... charSequenceArr) {
        return !isAnyEmpty(charSequenceArr);
    }

    public static boolean isNotBlank(CharSequence charSequence) {
        return !isBlank(charSequence);
    }

    public static boolean isNotEmpty(CharSequence charSequence) {
        return !isEmpty(charSequence);
    }

    public static boolean isNumeric(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isDigit(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNumericSpace(CharSequence charSequence) {
        if (charSequence == null) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isDigit(charSequence.charAt(i)) && charSequence.charAt(i) != ' ') {
                return false;
            }
        }
        return true;
    }

    public static boolean isWhitespace(CharSequence charSequence) {
        if (charSequence == null) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String join(Iterable<?> iterable, char c) {
        if (iterable == null) {
            return null;
        }
        return join(iterable.iterator(), c);
    }

    public static String join(Iterable<?> iterable, String str) {
        if (iterable == null) {
            return null;
        }
        return join(iterable.iterator(), str);
    }

    public static String join(Iterator<?> it2, char c) {
        if (it2 == null) {
            return null;
        }
        if (!it2.hasNext()) {
            return "";
        }
        Object next = it2.next();
        if (!it2.hasNext()) {
            return Objects.toString(next, "");
        }
        StringBuilder sb = new StringBuilder(256);
        if (next != null) {
            sb.append(next);
        }
        while (it2.hasNext()) {
            sb.append(c);
            Object next2 = it2.next();
            if (next2 != null) {
                sb.append(next2);
            }
        }
        return sb.toString();
    }

    public static String join(Iterator<?> it2, String str) {
        if (it2 == null) {
            return null;
        }
        if (!it2.hasNext()) {
            return "";
        }
        Object next = it2.next();
        if (!it2.hasNext()) {
            return Objects.toString(next, "");
        }
        StringBuilder sb = new StringBuilder(256);
        if (next != null) {
            sb.append(next);
        }
        while (it2.hasNext()) {
            if (str != null) {
                sb.append(str);
            }
            Object next2 = it2.next();
            if (next2 != null) {
                sb.append(next2);
            }
        }
        return sb.toString();
    }

    public static String join(byte[] bArr, char c) {
        if (bArr == null) {
            return null;
        }
        return join(bArr, c, 0, bArr.length);
    }

    public static String join(byte[] bArr, char c, int i, int i2) {
        if (bArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            sb.append(bArr[i4]);
        }
        return sb.toString();
    }

    public static String join(char[] cArr, char c) {
        if (cArr == null) {
            return null;
        }
        return join(cArr, c, 0, cArr.length);
    }

    public static String join(char[] cArr, char c, int i, int i2) {
        if (cArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            sb.append(cArr[i4]);
        }
        return sb.toString();
    }

    public static String join(double[] dArr, char c) {
        if (dArr == null) {
            return null;
        }
        return join(dArr, c, 0, dArr.length);
    }

    public static String join(double[] dArr, char c, int i, int i2) {
        if (dArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            sb.append(dArr[i4]);
        }
        return sb.toString();
    }

    public static String join(float[] fArr, char c) {
        if (fArr == null) {
            return null;
        }
        return join(fArr, c, 0, fArr.length);
    }

    public static String join(float[] fArr, char c, int i, int i2) {
        if (fArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            sb.append(fArr[i4]);
        }
        return sb.toString();
    }

    public static String join(int[] iArr, char c) {
        if (iArr == null) {
            return null;
        }
        return join(iArr, c, 0, iArr.length);
    }

    public static String join(int[] iArr, char c, int i, int i2) {
        if (iArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            sb.append(iArr[i4]);
        }
        return sb.toString();
    }

    public static String join(long[] jArr, char c) {
        if (jArr == null) {
            return null;
        }
        return join(jArr, c, 0, jArr.length);
    }

    public static String join(long[] jArr, char c, int i, int i2) {
        if (jArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            sb.append(jArr[i4]);
        }
        return sb.toString();
    }

    @SafeVarargs
    public static <T> String join(T... tArr) {
        return join((Object[]) tArr, (String) null);
    }

    public static String join(Object[] objArr, char c) {
        if (objArr == null) {
            return null;
        }
        return join(objArr, c, 0, objArr.length);
    }

    public static String join(Object[] objArr, char c, int i, int i2) {
        if (objArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            if (objArr[i4] != null) {
                sb.append(objArr[i4]);
            }
        }
        return sb.toString();
    }

    public static String join(Object[] objArr, String str) {
        if (objArr == null) {
            return null;
        }
        return join(objArr, str, 0, objArr.length);
    }

    public static String join(Object[] objArr, String str, int i, int i2) {
        if (objArr == null) {
            return null;
        }
        if (str == null) {
            str = "";
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(str);
            }
            if (objArr[i4] != null) {
                sb.append(objArr[i4]);
            }
        }
        return sb.toString();
    }

    public static String join(short[] sArr, char c) {
        if (sArr == null) {
            return null;
        }
        return join(sArr, c, 0, sArr.length);
    }

    public static String join(short[] sArr, char c, int i, int i2) {
        if (sArr == null) {
            return null;
        }
        int i3 = i2 - i;
        if (i3 <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(i3 * 16);
        for (int i4 = i; i4 < i2; i4++) {
            if (i4 > i) {
                sb.append(c);
            }
            sb.append(sArr[i4]);
        }
        return sb.toString();
    }

    public static String joinWith(String str, Object... objArr) {
        if (objArr == null) {
            throw new IllegalArgumentException("Object varargs must not be null");
        }
        String defaultString = defaultString(str, "");
        StringBuilder sb = new StringBuilder();
        Iterator it2 = Arrays.asList(objArr).iterator();
        while (it2.hasNext()) {
            sb.append(Objects.toString(it2.next(), ""));
            if (it2.hasNext()) {
                sb.append(defaultString);
            }
        }
        return sb.toString();
    }

    public static int lastIndexOf(CharSequence charSequence, int i) {
        if (isEmpty(charSequence)) {
            return -1;
        }
        return CharSequenceUtils.lastIndexOf(charSequence, i, charSequence.length());
    }

    public static int lastIndexOf(CharSequence charSequence, int i, int i2) {
        if (isEmpty(charSequence)) {
            return -1;
        }
        return CharSequenceUtils.lastIndexOf(charSequence, i, i2);
    }

    public static int lastIndexOf(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            return -1;
        }
        return CharSequenceUtils.lastIndexOf(charSequence, charSequence2, charSequence.length());
    }

    public static int lastIndexOf(CharSequence charSequence, CharSequence charSequence2, int i) {
        if (charSequence == null || charSequence2 == null) {
            return -1;
        }
        return CharSequenceUtils.lastIndexOf(charSequence, charSequence2, i);
    }

    public static int lastIndexOfAny(CharSequence charSequence, CharSequence... charSequenceArr) {
        int lastIndexOf;
        if (charSequence == null || charSequenceArr == null) {
            return -1;
        }
        int i = -1;
        for (CharSequence charSequence2 : charSequenceArr) {
            if (charSequence2 != null && (lastIndexOf = CharSequenceUtils.lastIndexOf(charSequence, charSequence2, charSequence.length())) > i) {
                i = lastIndexOf;
            }
        }
        return i;
    }

    public static int lastIndexOfIgnoreCase(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            return -1;
        }
        return lastIndexOfIgnoreCase(charSequence, charSequence2, charSequence.length());
    }

    public static int lastIndexOfIgnoreCase(CharSequence charSequence, CharSequence charSequence2, int i) {
        if (charSequence == null || charSequence2 == null) {
            return -1;
        }
        int length = i > charSequence.length() - charSequence2.length() ? charSequence.length() - charSequence2.length() : i;
        if (length < 0) {
            int i2 = length;
            return -1;
        } else if (charSequence2.length() == 0) {
            int i3 = length;
            return length;
        } else {
            for (int i4 = length; i4 >= 0; i4--) {
                if (CharSequenceUtils.regionMatches(charSequence, true, i4, charSequence2, 0, charSequence2.length())) {
                    int i5 = length;
                    return i4;
                }
            }
            int i6 = length;
            return -1;
        }
    }

    public static int lastOrdinalIndexOf(CharSequence charSequence, CharSequence charSequence2, int i) {
        return ordinalIndexOf(charSequence, charSequence2, i, true);
    }

    public static String left(String str, int i) {
        if (str == null) {
            return null;
        }
        return i < 0 ? "" : str.length() > i ? str.substring(0, i) : str;
    }

    public static String leftPad(String str, int i) {
        return leftPad(str, i, ' ');
    }

    public static String leftPad(String str, int i, char c) {
        if (str == null) {
            return null;
        }
        int length = i - str.length();
        return length > 0 ? length > 8192 ? leftPad(str, i, String.valueOf(c)) : repeat(c, length).concat(str) : str;
    }

    public static String leftPad(String str, int i, String str2) {
        if (str == null) {
            return null;
        }
        if (isEmpty(str2)) {
            str2 = SPACE;
        }
        int length = str2.length();
        int length2 = i - str.length();
        if (length2 <= 0) {
            return str;
        }
        if (length == 1 && length2 <= 8192) {
            return leftPad(str, i, str2.charAt(0));
        }
        if (length2 == length) {
            return str2.concat(str);
        }
        if (length2 < length) {
            return str2.substring(0, length2).concat(str);
        }
        char[] cArr = new char[length2];
        char[] charArray = str2.toCharArray();
        for (int i2 = 0; i2 < length2; i2++) {
            cArr[i2] = charArray[i2 % length];
        }
        return new String(cArr).concat(str);
    }

    public static int length(CharSequence charSequence) {
        if (charSequence == null) {
            return 0;
        }
        return charSequence.length();
    }

    public static String lowerCase(String str) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase();
    }

    public static String lowerCase(String str, Locale locale) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase(locale);
    }

    private static int[] matches(CharSequence charSequence, CharSequence charSequence2) {
        CharSequence charSequence3;
        CharSequence charSequence4;
        if (charSequence.length() > charSequence2.length()) {
            charSequence3 = charSequence;
            charSequence4 = charSequence2;
        } else {
            charSequence3 = charSequence2;
            charSequence4 = charSequence;
        }
        int max = Math.max((charSequence3.length() / 2) - 1, 0);
        int[] iArr = new int[charSequence4.length()];
        Arrays.fill(iArr, -1);
        boolean[] zArr = new boolean[charSequence3.length()];
        int i = 0;
        for (int i2 = 0; i2 < charSequence4.length(); i2++) {
            char charAt = charSequence4.charAt(i2);
            int max2 = Math.max(i2 - max, 0);
            int min = Math.min(i2 + max + 1, charSequence3.length());
            while (true) {
                if (max2 < min) {
                    if (!zArr[max2] && charAt == charSequence3.charAt(max2)) {
                        iArr[i2] = max2;
                        zArr[max2] = true;
                        i++;
                        break;
                    }
                    max2++;
                } else {
                    break;
                }
            }
        }
        char[] cArr = new char[i];
        char[] cArr2 = new char[i];
        int i3 = 0;
        for (int i4 = 0; i4 < charSequence4.length(); i4++) {
            if (iArr[i4] != -1) {
                cArr[i3] = charSequence4.charAt(i4);
                i3++;
            }
        }
        int i5 = 0;
        for (int i6 = 0; i6 < charSequence3.length(); i6++) {
            if (zArr[i6]) {
                cArr2[i5] = charSequence3.charAt(i6);
                i5++;
            }
        }
        int i7 = 0;
        for (int i8 = 0; i8 < cArr.length; i8++) {
            if (cArr[i8] != cArr2[i8]) {
                i7++;
            }
        }
        int i9 = 0;
        int i10 = 0;
        while (i10 < charSequence4.length() && charSequence.charAt(i10) == charSequence2.charAt(i10)) {
            i9++;
            i10++;
        }
        return new int[]{i, i7 / 2, i9, charSequence3.length()};
    }

    public static String mid(String str, int i, int i2) {
        if (str == null) {
            return null;
        }
        if (i2 < 0 || i > str.length()) {
            return "";
        }
        if (i < 0) {
            i = 0;
        }
        return str.length() <= i + i2 ? str.substring(i) : str.substring(i, i + i2);
    }

    public static String normalizeSpace(String str) {
        int i;
        if (isEmpty(str)) {
            return str;
        }
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = 0;
        boolean z = true;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            char charAt = str.charAt(i3);
            if (!Character.isWhitespace(charAt)) {
                z = false;
                i = i4 + 1;
                if (charAt == 160) {
                    charAt = ' ';
                }
                cArr[i4] = charAt;
                i2 = 0;
            } else {
                if (i2 != 0 || z) {
                    i = i4;
                } else {
                    i = i4 + 1;
                    cArr[i4] = SPACE.charAt(0);
                }
                i2++;
            }
            i3++;
            i4 = i;
        }
        if (z) {
            return "";
        }
        return new String(cArr, 0, i4 - (i2 > 0 ? 1 : 0)).trim();
    }

    public static int ordinalIndexOf(CharSequence charSequence, CharSequence charSequence2, int i) {
        return ordinalIndexOf(charSequence, charSequence2, i, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int ordinalIndexOf(java.lang.CharSequence r3, java.lang.CharSequence r4, int r5, boolean r6) {
        /*
            r1 = -1
            if (r3 == 0) goto L_0x0007
            if (r4 == 0) goto L_0x0007
            if (r5 > 0) goto L_0x0008
        L_0x0007:
            return r1
        L_0x0008:
            int r2 = r4.length()
            if (r2 != 0) goto L_0x0018
            if (r6 == 0) goto L_0x0016
            int r2 = r3.length()
        L_0x0014:
            r1 = r2
            goto L_0x0007
        L_0x0016:
            r2 = 0
            goto L_0x0014
        L_0x0018:
            r0 = 0
            if (r6 == 0) goto L_0x001f
            int r1 = r3.length()
        L_0x001f:
            if (r6 == 0) goto L_0x002e
            int r2 = r1 + -1
            int r1 = org.apache.commons.lang3.CharSequenceUtils.lastIndexOf((java.lang.CharSequence) r3, (java.lang.CharSequence) r4, (int) r2)
        L_0x0027:
            if (r1 < 0) goto L_0x0007
            int r0 = r0 + 1
            if (r0 < r5) goto L_0x001f
            goto L_0x0007
        L_0x002e:
            int r2 = r1 + 1
            int r1 = org.apache.commons.lang3.CharSequenceUtils.indexOf((java.lang.CharSequence) r3, (java.lang.CharSequence) r4, (int) r2)
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.StringUtils.ordinalIndexOf(java.lang.CharSequence, java.lang.CharSequence, int, boolean):int");
    }

    public static String overlay(String str, String str2, int i, int i2) {
        if (str == null) {
            return null;
        }
        if (str2 == null) {
            str2 = "";
        }
        int length = str.length();
        if (i < 0) {
            i = 0;
        }
        if (i > length) {
            i = length;
        }
        if (i2 < 0) {
            i2 = 0;
        }
        if (i2 > length) {
            i2 = length;
        }
        if (i > i2) {
            int i3 = i;
            i = i2;
            i2 = i3;
        }
        return str.substring(0, i) + str2 + str.substring(i2);
    }

    private static String prependIfMissing(String str, CharSequence charSequence, boolean z, CharSequence... charSequenceArr) {
        if (str == null || isEmpty(charSequence) || startsWith(str, charSequence, z)) {
            return str;
        }
        if (charSequenceArr != null && charSequenceArr.length > 0) {
            for (CharSequence startsWith : charSequenceArr) {
                if (startsWith(str, startsWith, z)) {
                    return str;
                }
            }
        }
        return charSequence.toString() + str;
    }

    public static String prependIfMissing(String str, CharSequence charSequence, CharSequence... charSequenceArr) {
        return prependIfMissing(str, charSequence, false, charSequenceArr);
    }

    public static String prependIfMissingIgnoreCase(String str, CharSequence charSequence, CharSequence... charSequenceArr) {
        return prependIfMissing(str, charSequence, true, charSequenceArr);
    }

    public static String remove(String str, char c) {
        if (isEmpty(str) || str.indexOf(c) == -1) {
            return str;
        }
        char[] charArray = str.toCharArray();
        int i = 0;
        for (int i2 = 0; i2 < charArray.length; i2++) {
            if (charArray[i2] != c) {
                charArray[i] = charArray[i2];
                i++;
            }
        }
        return new String(charArray, 0, i);
    }

    public static String remove(String str, String str2) {
        return (isEmpty(str) || isEmpty(str2)) ? str : replace(str, str2, "", -1);
    }

    public static String removeAll(String str, String str2) {
        return replaceAll(str, str2, "");
    }

    public static String removeEnd(String str, String str2) {
        return (isEmpty(str) || isEmpty(str2) || !str.endsWith(str2)) ? str : str.substring(0, str.length() - str2.length());
    }

    public static String removeEndIgnoreCase(String str, String str2) {
        return (isEmpty(str) || isEmpty(str2) || !endsWithIgnoreCase(str, str2)) ? str : str.substring(0, str.length() - str2.length());
    }

    public static String removeFirst(String str, String str2) {
        return replaceFirst(str, str2, "");
    }

    public static String removeIgnoreCase(String str, String str2) {
        return (isEmpty(str) || isEmpty(str2)) ? str : replaceIgnoreCase(str, str2, "", -1);
    }

    public static String removePattern(String str, String str2) {
        return replacePattern(str, str2, "");
    }

    public static String removeStart(String str, String str2) {
        return (isEmpty(str) || isEmpty(str2) || !str.startsWith(str2)) ? str : str.substring(str2.length());
    }

    public static String removeStartIgnoreCase(String str, String str2) {
        return (isEmpty(str) || isEmpty(str2) || !startsWithIgnoreCase(str, str2)) ? str : str.substring(str2.length());
    }

    public static String repeat(char c, int i) {
        if (i <= 0) {
            return "";
        }
        char[] cArr = new char[i];
        for (int i2 = i - 1; i2 >= 0; i2--) {
            cArr[i2] = c;
        }
        return new String(cArr);
    }

    public static String repeat(String str, int i) {
        if (str == null) {
            return null;
        }
        if (i <= 0) {
            return "";
        }
        int length = str.length();
        if (i == 1 || length == 0) {
            return str;
        }
        if (length == 1 && i <= 8192) {
            return repeat(str.charAt(0), i);
        }
        int i2 = length * i;
        switch (length) {
            case 1:
                return repeat(str.charAt(0), i);
            case 2:
                char charAt = str.charAt(0);
                char charAt2 = str.charAt(1);
                char[] cArr = new char[i2];
                for (int i3 = (i * 2) - 2; i3 >= 0; i3 = (i3 - 1) - 1) {
                    cArr[i3] = charAt;
                    cArr[i3 + 1] = charAt2;
                }
                return new String(cArr);
            default:
                StringBuilder sb = new StringBuilder(i2);
                for (int i4 = 0; i4 < i; i4++) {
                    sb.append(str);
                }
                return sb.toString();
        }
    }

    public static String repeat(String str, String str2, int i) {
        return (str == null || str2 == null) ? repeat(str, i) : removeEnd(repeat(str + str2, i), str2);
    }

    public static String replace(String str, String str2, String str3) {
        return replace(str, str2, str3, -1);
    }

    public static String replace(String str, String str2, String str3, int i) {
        return replace(str, str2, str3, i, false);
    }

    private static String replace(String str, String str2, String str3, int i, boolean z) {
        int i2 = 64;
        if (isEmpty(str) || isEmpty(str2) || str3 == null || i == 0) {
            return str;
        }
        String str4 = str;
        if (z) {
            str4 = str.toLowerCase();
            str2 = str2.toLowerCase();
        }
        int i3 = 0;
        int indexOf = str4.indexOf(str2, 0);
        if (indexOf == -1) {
            return str;
        }
        int length = str2.length();
        int length2 = str3.length() - length;
        if (length2 < 0) {
            length2 = 0;
        }
        if (i < 0) {
            i2 = 16;
        } else if (i <= 64) {
            i2 = i;
        }
        StringBuilder sb = new StringBuilder(str.length() + (length2 * i2));
        while (indexOf != -1) {
            sb.append(str, i3, indexOf).append(str3);
            i3 = indexOf + length;
            i--;
            if (i == 0) {
                break;
            }
            indexOf = str4.indexOf(str2, i3);
        }
        sb.append(str, i3, str.length());
        return sb.toString();
    }

    public static String replaceAll(String str, String str2, String str3) {
        return (str == null || str2 == null || str3 == null) ? str : str.replaceAll(str2, str3);
    }

    public static String replaceChars(String str, char c, char c2) {
        if (str == null) {
            return null;
        }
        return str.replace(c, c2);
    }

    public static String replaceChars(String str, String str2, String str3) {
        if (isEmpty(str) || isEmpty(str2)) {
            return str;
        }
        if (str3 == null) {
            str3 = "";
        }
        boolean z = false;
        int length = str3.length();
        int length2 = str.length();
        StringBuilder sb = new StringBuilder(length2);
        for (int i = 0; i < length2; i++) {
            char charAt = str.charAt(i);
            int indexOf = str2.indexOf(charAt);
            if (indexOf >= 0) {
                z = true;
                if (indexOf < length) {
                    sb.append(str3.charAt(indexOf));
                }
            } else {
                sb.append(charAt);
            }
        }
        return z ? sb.toString() : str;
    }

    public static String replaceEach(String str, String[] strArr, String[] strArr2) {
        return replaceEach(str, strArr, strArr2, false, 0);
    }

    private static String replaceEach(String str, String[] strArr, String[] strArr2, boolean z, int i) {
        int length;
        if (str == null || str.isEmpty() || strArr == null || strArr.length == 0 || strArr2 == null || strArr2.length == 0) {
            return str;
        }
        if (i < 0) {
            throw new IllegalStateException("Aborting to protect against StackOverflowError - output of one loop is the input of another");
        }
        int length2 = strArr.length;
        int length3 = strArr2.length;
        if (length2 != length3) {
            throw new IllegalArgumentException("Search and Replace array lengths don't match: " + length2 + " vs " + length3);
        }
        boolean[] zArr = new boolean[length2];
        int i2 = -1;
        int i3 = -1;
        for (int i4 = 0; i4 < length2; i4++) {
            if (!zArr[i4] && strArr[i4] != null && !strArr[i4].isEmpty() && strArr2[i4] != null) {
                int indexOf = str.indexOf(strArr[i4]);
                if (indexOf == -1) {
                    zArr[i4] = true;
                } else if (i2 == -1 || indexOf < i2) {
                    i2 = indexOf;
                    i3 = i4;
                }
            }
        }
        if (i2 == -1) {
            return str;
        }
        int i5 = 0;
        int i6 = 0;
        for (int i7 = 0; i7 < strArr.length; i7++) {
            if (!(strArr[i7] == null || strArr2[i7] == null || (length = strArr2[i7].length() - strArr[i7].length()) <= 0)) {
                i6 += length * 3;
            }
        }
        StringBuilder sb = new StringBuilder(str.length() + Math.min(i6, str.length() / 5));
        while (i2 != -1) {
            for (int i8 = i5; i8 < i2; i8++) {
                sb.append(str.charAt(i8));
            }
            sb.append(strArr2[i3]);
            i5 = i2 + strArr[i3].length();
            i2 = -1;
            i3 = -1;
            for (int i9 = 0; i9 < length2; i9++) {
                if (!zArr[i9] && strArr[i9] != null && !strArr[i9].isEmpty() && strArr2[i9] != null) {
                    int indexOf2 = str.indexOf(strArr[i9], i5);
                    if (indexOf2 == -1) {
                        zArr[i9] = true;
                    } else if (i2 == -1 || indexOf2 < i2) {
                        i2 = indexOf2;
                        i3 = i9;
                    }
                }
            }
        }
        int length4 = str.length();
        for (int i10 = i5; i10 < length4; i10++) {
            sb.append(str.charAt(i10));
        }
        String sb2 = sb.toString();
        return !z ? sb2 : replaceEach(sb2, strArr, strArr2, z, i - 1);
    }

    public static String replaceEachRepeatedly(String str, String[] strArr, String[] strArr2) {
        return replaceEach(str, strArr, strArr2, true, strArr == null ? 0 : strArr.length);
    }

    public static String replaceFirst(String str, String str2, String str3) {
        return (str == null || str2 == null || str3 == null) ? str : str.replaceFirst(str2, str3);
    }

    public static String replaceIgnoreCase(String str, String str2, String str3) {
        return replaceIgnoreCase(str, str2, str3, -1);
    }

    public static String replaceIgnoreCase(String str, String str2, String str3, int i) {
        return replace(str, str2, str3, i, true);
    }

    public static String replaceOnce(String str, String str2, String str3) {
        return replace(str, str2, str3, 1);
    }

    public static String replaceOnceIgnoreCase(String str, String str2, String str3) {
        return replaceIgnoreCase(str, str2, str3, 1);
    }

    public static String replacePattern(String str, String str2, String str3) {
        return (str == null || str2 == null || str3 == null) ? str : Pattern.compile(str2, 32).matcher(str).replaceAll(str3);
    }

    public static String reverse(String str) {
        if (str == null) {
            return null;
        }
        return new StringBuilder(str).reverse().toString();
    }

    public static String reverseDelimited(String str, char c) {
        if (str == null) {
            return null;
        }
        String[] split = split(str, c);
        ArrayUtils.reverse((Object[]) split);
        return join((Object[]) split, c);
    }

    public static String right(String str, int i) {
        if (str == null) {
            return null;
        }
        return i < 0 ? "" : str.length() > i ? str.substring(str.length() - i) : str;
    }

    public static String rightPad(String str, int i) {
        return rightPad(str, i, ' ');
    }

    public static String rightPad(String str, int i, char c) {
        if (str == null) {
            return null;
        }
        int length = i - str.length();
        return length > 0 ? length > 8192 ? rightPad(str, i, String.valueOf(c)) : str.concat(repeat(c, length)) : str;
    }

    public static String rightPad(String str, int i, String str2) {
        if (str == null) {
            return null;
        }
        if (isEmpty(str2)) {
            str2 = SPACE;
        }
        int length = str2.length();
        int length2 = i - str.length();
        if (length2 <= 0) {
            return str;
        }
        if (length == 1 && length2 <= 8192) {
            return rightPad(str, i, str2.charAt(0));
        }
        if (length2 == length) {
            return str.concat(str2);
        }
        if (length2 < length) {
            return str.concat(str2.substring(0, length2));
        }
        char[] cArr = new char[length2];
        char[] charArray = str2.toCharArray();
        for (int i2 = 0; i2 < length2; i2++) {
            cArr[i2] = charArray[i2 % length];
        }
        return str.concat(new String(cArr));
    }

    public static String rotate(String str, int i) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (i == 0 || length == 0 || i % length == 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder(length);
        int i2 = -(i % length);
        sb.append(substring(str, i2));
        sb.append(substring(str, 0, i2));
        return sb.toString();
    }

    public static String[] split(String str) {
        return split(str, (String) null, -1);
    }

    public static String[] split(String str, char c) {
        return splitWorker(str, c, false);
    }

    public static String[] split(String str, String str2) {
        return splitWorker(str, str2, -1, false);
    }

    public static String[] split(String str, String str2, int i) {
        return splitWorker(str, str2, i, false);
    }

    public static String[] splitByCharacterType(String str) {
        return splitByCharacterType(str, false);
    }

    private static String[] splitByCharacterType(String str, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.isEmpty()) {
            return ArrayUtils.EMPTY_STRING_ARRAY;
        }
        char[] charArray = str.toCharArray();
        ArrayList arrayList = new ArrayList();
        int i = 0;
        int type = Character.getType(charArray[0]);
        for (int i2 = 0 + 1; i2 < charArray.length; i2++) {
            int type2 = Character.getType(charArray[i2]);
            if (type2 != type) {
                if (z && type2 == 2 && type == 1) {
                    int i3 = i2 - 1;
                    if (i3 != i) {
                        arrayList.add(new String(charArray, i, i3 - i));
                        i = i3;
                    }
                } else {
                    arrayList.add(new String(charArray, i, i2 - i));
                    i = i2;
                }
                type = type2;
            }
        }
        arrayList.add(new String(charArray, i, charArray.length - i));
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public static String[] splitByCharacterTypeCamelCase(String str) {
        return splitByCharacterType(str, true);
    }

    public static String[] splitByWholeSeparator(String str, String str2) {
        return splitByWholeSeparatorWorker(str, str2, -1, false);
    }

    public static String[] splitByWholeSeparator(String str, String str2, int i) {
        return splitByWholeSeparatorWorker(str, str2, i, false);
    }

    public static String[] splitByWholeSeparatorPreserveAllTokens(String str, String str2) {
        return splitByWholeSeparatorWorker(str, str2, -1, true);
    }

    public static String[] splitByWholeSeparatorPreserveAllTokens(String str, String str2, int i) {
        return splitByWholeSeparatorWorker(str, str2, i, true);
    }

    private static String[] splitByWholeSeparatorWorker(String str, String str2, int i, boolean z) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (length == 0) {
            return ArrayUtils.EMPTY_STRING_ARRAY;
        }
        if (str2 == null || "".equals(str2)) {
            return splitWorker(str, (String) null, i, z);
        }
        int length2 = str2.length();
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 < length) {
            i4 = str.indexOf(str2, i3);
            if (i4 <= -1) {
                arrayList.add(str.substring(i3));
                i4 = length;
            } else if (i4 > i3) {
                i2++;
                if (i2 == i) {
                    i4 = length;
                    arrayList.add(str.substring(i3));
                } else {
                    arrayList.add(str.substring(i3, i4));
                    i3 = i4 + length2;
                }
            } else {
                if (z) {
                    i2++;
                    if (i2 == i) {
                        i4 = length;
                        arrayList.add(str.substring(i3));
                    } else {
                        arrayList.add("");
                    }
                }
                i3 = i4 + length2;
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public static String[] splitPreserveAllTokens(String str) {
        return splitWorker(str, (String) null, -1, true);
    }

    public static String[] splitPreserveAllTokens(String str, char c) {
        return splitWorker(str, c, true);
    }

    public static String[] splitPreserveAllTokens(String str, String str2) {
        return splitWorker(str, str2, -1, true);
    }

    public static String[] splitPreserveAllTokens(String str, String str2, int i) {
        return splitWorker(str, str2, i, true);
    }

    private static String[] splitWorker(String str, char c, boolean z) {
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (length == 0) {
            return ArrayUtils.EMPTY_STRING_ARRAY;
        }
        ArrayList arrayList = new ArrayList();
        int i = 0;
        int i2 = 0;
        boolean z2 = false;
        boolean z3 = false;
        while (i < length) {
            if (str.charAt(i) == c) {
                if (z2 || z) {
                    arrayList.add(str.substring(i2, i));
                    z2 = false;
                    z3 = true;
                }
                i++;
                i2 = i;
            } else {
                z3 = false;
                z2 = true;
                i++;
            }
        }
        if (z2 || (z && z3)) {
            arrayList.add(str.substring(i2, i));
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    private static String[] splitWorker(String str, String str2, int i, boolean z) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        if (str == null) {
            return null;
        }
        int length = str.length();
        if (length == 0) {
            return ArrayUtils.EMPTY_STRING_ARRAY;
        }
        ArrayList arrayList = new ArrayList();
        int i8 = 0;
        int i9 = 0;
        boolean z2 = false;
        boolean z3 = false;
        if (str2 == null) {
            i2 = 1;
            while (i8 < length) {
                if (Character.isWhitespace(str.charAt(i8))) {
                    if (z2 || z) {
                        z3 = true;
                        i7 = i2 + 1;
                        if (i2 == i) {
                            i8 = length;
                            z3 = false;
                        }
                        arrayList.add(str.substring(i9, i8));
                        z2 = false;
                    } else {
                        i7 = i2;
                    }
                    i8++;
                    i9 = i8;
                    i2 = i7;
                } else {
                    z3 = false;
                    z2 = true;
                    i8++;
                }
            }
            int i10 = i2;
        } else if (str2.length() == 1) {
            char charAt = str2.charAt(0);
            int i11 = 1;
            while (i8 < length) {
                if (str.charAt(i8) == charAt) {
                    if (z2 || z) {
                        boolean z4 = true;
                        i6 = i11 + 1;
                        if (i11 == i) {
                            i8 = length;
                            z4 = false;
                        }
                        arrayList.add(str.substring(i9, i8));
                        z2 = false;
                    } else {
                        i6 = i11;
                    }
                    i5 = i8 + 1;
                    i9 = i5;
                    i11 = i6;
                } else {
                    z3 = false;
                    z2 = true;
                    i5 = i8 + 1;
                }
            }
            int i12 = i11;
        } else {
            i2 = 1;
            while (i8 < length) {
                if (str2.indexOf(str.charAt(i8)) >= 0) {
                    if (z2 || z) {
                        boolean z5 = true;
                        i4 = i2 + 1;
                        if (i2 == i) {
                            i8 = length;
                            z5 = false;
                        }
                        arrayList.add(str.substring(i9, i8));
                        z2 = false;
                    } else {
                        i4 = i2;
                    }
                    i3 = i8 + 1;
                    i9 = i3;
                    i2 = i4;
                } else {
                    z3 = false;
                    z2 = true;
                    i3 = i8 + 1;
                }
            }
            int i102 = i2;
        }
        if (z2 || (z && z3)) {
            arrayList.add(str.substring(i9, i8));
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public static boolean startsWith(CharSequence charSequence, CharSequence charSequence2) {
        return startsWith(charSequence, charSequence2, false);
    }

    private static boolean startsWith(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        if (charSequence == null || charSequence2 == null) {
            return charSequence == null && charSequence2 == null;
        }
        if (charSequence2.length() > charSequence.length()) {
            return false;
        }
        return CharSequenceUtils.regionMatches(charSequence, z, 0, charSequence2, 0, charSequence2.length());
    }

    public static boolean startsWithAny(CharSequence charSequence, CharSequence... charSequenceArr) {
        if (isEmpty(charSequence) || ArrayUtils.isEmpty((Object[]) charSequenceArr)) {
            return false;
        }
        for (CharSequence startsWith : charSequenceArr) {
            if (startsWith(charSequence, startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static boolean startsWithIgnoreCase(CharSequence charSequence, CharSequence charSequence2) {
        return startsWith(charSequence, charSequence2, true);
    }

    public static String strip(String str) {
        return strip(str, (String) null);
    }

    public static String strip(String str, String str2) {
        return isEmpty(str) ? str : stripEnd(stripStart(str, str2), str2);
    }

    public static String stripAccents(String str) {
        if (str == null) {
            return null;
        }
        Pattern compile = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        StringBuilder sb = new StringBuilder(Normalizer.normalize(str, Normalizer.Form.NFD));
        convertRemainingAccentCharacters(sb);
        return compile.matcher(sb).replaceAll("");
    }

    public static String[] stripAll(String... strArr) {
        return stripAll(strArr, (String) null);
    }

    public static String[] stripAll(String[] strArr, String str) {
        int length;
        if (strArr == null || (length = strArr.length) == 0) {
            return strArr;
        }
        String[] strArr2 = new String[length];
        for (int i = 0; i < length; i++) {
            strArr2[i] = strip(strArr[i], str);
        }
        return strArr2;
    }

    public static String stripEnd(String str, String str2) {
        int length;
        if (str == null || (length = str.length()) == 0) {
            return str;
        }
        if (str2 == null) {
            while (length != 0 && Character.isWhitespace(str.charAt(length - 1))) {
                length--;
            }
        } else if (str2.isEmpty()) {
            return str;
        } else {
            while (length != 0 && str2.indexOf(str.charAt(length - 1)) != -1) {
                length--;
            }
        }
        return str.substring(0, length);
    }

    public static String stripStart(String str, String str2) {
        int length;
        if (str == null || (length = str.length()) == 0) {
            return str;
        }
        int i = 0;
        if (str2 == null) {
            while (i != length && Character.isWhitespace(str.charAt(i))) {
                i++;
            }
        } else if (str2.isEmpty()) {
            return str;
        } else {
            while (i != length && str2.indexOf(str.charAt(i)) != -1) {
                i++;
            }
        }
        return str.substring(i);
    }

    public static String stripToEmpty(String str) {
        return str == null ? "" : strip(str, (String) null);
    }

    public static String stripToNull(String str) {
        if (str == null) {
            return null;
        }
        String strip = strip(str, (String) null);
        if (strip.isEmpty()) {
            strip = null;
        }
        return strip;
    }

    public static String substring(String str, int i) {
        if (str == null) {
            return null;
        }
        if (i < 0) {
            i += str.length();
        }
        if (i < 0) {
            i = 0;
        }
        return i > str.length() ? "" : str.substring(i);
    }

    public static String substring(String str, int i, int i2) {
        if (str == null) {
            return null;
        }
        if (i2 < 0) {
            i2 += str.length();
        }
        if (i < 0) {
            i += str.length();
        }
        if (i2 > str.length()) {
            i2 = str.length();
        }
        if (i > i2) {
            return "";
        }
        if (i < 0) {
            i = 0;
        }
        if (i2 < 0) {
            i2 = 0;
        }
        return str.substring(i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000d, code lost:
        r0 = r2.indexOf(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String substringAfter(java.lang.String r2, java.lang.String r3) {
        /*
            boolean r1 = isEmpty(r2)
            if (r1 == 0) goto L_0x0007
        L_0x0006:
            return r2
        L_0x0007:
            if (r3 != 0) goto L_0x000d
            java.lang.String r2 = ""
            goto L_0x0006
        L_0x000d:
            int r0 = r2.indexOf(r3)
            r1 = -1
            if (r0 != r1) goto L_0x0018
            java.lang.String r2 = ""
            goto L_0x0006
        L_0x0018:
            int r1 = r3.length()
            int r1 = r1 + r0
            java.lang.String r2 = r2.substring(r1)
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.StringUtils.substringAfter(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        r0 = r3.lastIndexOf(r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String substringAfterLast(java.lang.String r3, java.lang.String r4) {
        /*
            boolean r1 = isEmpty(r3)
            if (r1 == 0) goto L_0x0007
        L_0x0006:
            return r3
        L_0x0007:
            boolean r1 = isEmpty(r4)
            if (r1 == 0) goto L_0x0011
            java.lang.String r3 = ""
            goto L_0x0006
        L_0x0011:
            int r0 = r3.lastIndexOf(r4)
            r1 = -1
            if (r0 == r1) goto L_0x0023
            int r1 = r3.length()
            int r2 = r4.length()
            int r1 = r1 - r2
            if (r0 != r1) goto L_0x0027
        L_0x0023:
            java.lang.String r3 = ""
            goto L_0x0006
        L_0x0027:
            int r1 = r4.length()
            int r1 = r1 + r0
            java.lang.String r3 = r3.substring(r1)
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.StringUtils.substringAfterLast(java.lang.String, java.lang.String):java.lang.String");
    }

    public static String substringBefore(String str, String str2) {
        if (isEmpty(str) || str2 == null) {
            return str;
        }
        if (str2.isEmpty()) {
            return "";
        }
        int indexOf = str.indexOf(str2);
        return indexOf != -1 ? str.substring(0, indexOf) : str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000d, code lost:
        r0 = r2.lastIndexOf(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String substringBeforeLast(java.lang.String r2, java.lang.String r3) {
        /*
            boolean r1 = isEmpty(r2)
            if (r1 != 0) goto L_0x000c
            boolean r1 = isEmpty(r3)
            if (r1 == 0) goto L_0x000d
        L_0x000c:
            return r2
        L_0x000d:
            int r0 = r2.lastIndexOf(r3)
            r1 = -1
            if (r0 == r1) goto L_0x000c
            r1 = 0
            java.lang.String r2 = r2.substring(r1, r0)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.StringUtils.substringBeforeLast(java.lang.String, java.lang.String):java.lang.String");
    }

    public static String substringBetween(String str, String str2) {
        return substringBetween(str, str2, str2);
    }

    public static String substringBetween(String str, String str2, String str3) {
        int indexOf;
        int indexOf2;
        if (str == null || str2 == null || str3 == null || (indexOf = str.indexOf(str2)) == -1 || (indexOf2 = str.indexOf(str3, str2.length() + indexOf)) == -1) {
            return null;
        }
        return str.substring(str2.length() + indexOf, indexOf2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        r5 = r5 + r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String[] substringsBetween(java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = 0
            if (r9 == 0) goto L_0x000f
            boolean r8 = isEmpty(r10)
            if (r8 != 0) goto L_0x000f
            boolean r8 = isEmpty(r11)
            if (r8 == 0) goto L_0x0010
        L_0x000f:
            return r7
        L_0x0010:
            int r6 = r9.length()
            if (r6 != 0) goto L_0x0019
            java.lang.String[] r7 = org.apache.commons.lang3.ArrayUtils.EMPTY_STRING_ARRAY
            goto L_0x000f
        L_0x0019:
            int r0 = r11.length()
            int r3 = r10.length()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r4 = 0
        L_0x0027:
            int r8 = r6 - r0
            if (r4 >= r8) goto L_0x0031
            int r5 = r9.indexOf(r10, r4)
            if (r5 >= 0) goto L_0x0044
        L_0x0031:
            boolean r8 = r2.isEmpty()
            if (r8 != 0) goto L_0x000f
            int r7 = r2.size()
            java.lang.String[] r7 = new java.lang.String[r7]
            java.lang.Object[] r7 = r2.toArray(r7)
            java.lang.String[] r7 = (java.lang.String[]) r7
            goto L_0x000f
        L_0x0044:
            int r5 = r5 + r3
            int r1 = r9.indexOf(r11, r5)
            if (r1 < 0) goto L_0x0031
            java.lang.String r8 = r9.substring(r5, r1)
            r2.add(r8)
            int r4 = r1 + r0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.StringUtils.substringsBetween(java.lang.String, java.lang.String, java.lang.String):java.lang.String[]");
    }

    public static String swapCase(String str) {
        if (isEmpty(str)) {
            return str;
        }
        int length = str.length();
        int[] iArr = new int[length];
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            int lowerCase = Character.isUpperCase(codePointAt) ? Character.toLowerCase(codePointAt) : Character.isTitleCase(codePointAt) ? Character.toLowerCase(codePointAt) : Character.isLowerCase(codePointAt) ? Character.toUpperCase(codePointAt) : codePointAt;
            iArr[i2] = lowerCase;
            i += Character.charCount(lowerCase);
            i2++;
        }
        return new String(iArr, 0, i2);
    }

    public static int[] toCodePoints(CharSequence charSequence) {
        if (charSequence == null) {
            return null;
        }
        if (charSequence.length() == 0) {
            return ArrayUtils.EMPTY_INT_ARRAY;
        }
        String charSequence2 = charSequence.toString();
        int[] iArr = new int[charSequence2.codePointCount(0, charSequence2.length())];
        int i = 0;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = charSequence2.codePointAt(i);
            i += Character.charCount(iArr[i2]);
        }
        return iArr;
    }

    public static String toEncodedString(byte[] bArr, Charset charset) {
        if (charset == null) {
            charset = Charset.defaultCharset();
        }
        return new String(bArr, charset);
    }

    @Deprecated
    public static String toString(byte[] bArr, String str) throws UnsupportedEncodingException {
        return str != null ? new String(bArr, str) : new String(bArr, Charset.defaultCharset());
    }

    public static String trim(String str) {
        if (str == null) {
            return null;
        }
        return str.trim();
    }

    public static String trimToEmpty(String str) {
        return str == null ? "" : str.trim();
    }

    public static String trimToNull(String str) {
        String trim = trim(str);
        if (isEmpty(trim)) {
            return null;
        }
        return trim;
    }

    public static String truncate(String str, int i) {
        return truncate(str, 0, i);
    }

    public static String truncate(String str, int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("offset cannot be negative");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("maxWith cannot be negative");
        } else if (str == null) {
            return null;
        } else {
            if (i > str.length()) {
                return "";
            }
            if (str.length() <= i2) {
                return str.substring(i);
            }
            return str.substring(i, i + i2 > str.length() ? str.length() : i + i2);
        }
    }

    public static String uncapitalize(String str) {
        int length;
        int codePointAt;
        int lowerCase;
        if (str == null || (length = str.length()) == 0 || (codePointAt = str.codePointAt(0)) == (lowerCase = Character.toLowerCase(codePointAt))) {
            return str;
        }
        int[] iArr = new int[length];
        int i = 0 + 1;
        iArr[0] = lowerCase;
        int charCount = Character.charCount(codePointAt);
        while (charCount < length) {
            int codePointAt2 = str.codePointAt(charCount);
            iArr[i] = codePointAt2;
            charCount += Character.charCount(codePointAt2);
            i++;
        }
        return new String(iArr, 0, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        r0 = r3.length() - 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String unwrap(java.lang.String r3, char r4) {
        /*
            boolean r2 = isEmpty(r3)
            if (r2 != 0) goto L_0x0008
            if (r4 != 0) goto L_0x0009
        L_0x0008:
            return r3
        L_0x0009:
            r2 = 0
            char r2 = r3.charAt(r2)
            if (r2 != r4) goto L_0x0008
            int r2 = r3.length()
            int r2 = r2 + -1
            char r2 = r3.charAt(r2)
            if (r2 != r4) goto L_0x0008
            r1 = 0
            int r2 = r3.length()
            int r0 = r2 + -1
            r2 = -1
            if (r0 == r2) goto L_0x0008
            r2 = 1
            java.lang.String r3 = r3.substring(r2, r0)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.StringUtils.unwrap(java.lang.String, char):java.lang.String");
    }

    public static String unwrap(String str, String str2) {
        if (isEmpty(str) || isEmpty(str2) || !startsWith(str, str2) || !endsWith(str, str2)) {
            return str;
        }
        int indexOf = str.indexOf(str2);
        int lastIndexOf = str.lastIndexOf(str2);
        return (indexOf == -1 || lastIndexOf == -1) ? str : str.substring(indexOf + str2.length(), lastIndexOf);
    }

    public static String upperCase(String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase();
    }

    public static String upperCase(String str, Locale locale) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase(locale);
    }

    public static String wrap(String str, char c) {
        return (isEmpty(str) || c == 0) ? str : c + str + c;
    }

    public static String wrap(String str, String str2) {
        return (isEmpty(str) || isEmpty(str2)) ? str : str2.concat(str).concat(str2);
    }

    public static String wrapIfMissing(String str, char c) {
        if (isEmpty(str) || c == 0) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str.length() + 2);
        if (str.charAt(0) != c) {
            sb.append(c);
        }
        sb.append(str);
        if (str.charAt(str.length() - 1) != c) {
            sb.append(c);
        }
        return sb.toString();
    }

    public static String wrapIfMissing(String str, String str2) {
        if (isEmpty(str) || isEmpty(str2)) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str.length() + str2.length() + str2.length());
        if (!str.startsWith(str2)) {
            sb.append(str2);
        }
        sb.append(str);
        if (!str.endsWith(str2)) {
            sb.append(str2);
        }
        return sb.toString();
    }
}
