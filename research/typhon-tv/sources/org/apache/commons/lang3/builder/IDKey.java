package org.apache.commons.lang3.builder;

final class IDKey {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f16536;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f16537;

    IDKey(Object _value) {
        this.f16536 = System.identityHashCode(_value);
        this.f16537 = _value;
    }

    public int hashCode() {
        return this.f16536;
    }

    public boolean equals(Object other) {
        if (!(other instanceof IDKey)) {
            return false;
        }
        IDKey idKey = (IDKey) other;
        if (this.f16536 == idKey.f16536 && this.f16537 == idKey.f16537) {
            return true;
        }
        return false;
    }
}
