package org.apache.commons.lang3.concurrent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;

public class Memoizer<I, O> implements Computable<I, O> {
    private final ConcurrentMap<I, Future<O>> cache;
    /* access modifiers changed from: private */
    public final Computable<I, O> computable;
    private final boolean recalculate;

    public Memoizer(Computable<I, O> computable2) {
        this(computable2, false);
    }

    public Memoizer(Computable<I, O> computable2, boolean recalculate2) {
        this.cache = new ConcurrentHashMap();
        this.computable = computable2;
        this.recalculate = recalculate2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000a, code lost:
        r3 = new java.util.concurrent.FutureTask<>(new org.apache.commons.lang3.concurrent.Memoizer.AnonymousClass1(r5));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public O compute(final I r6) throws java.lang.InterruptedException {
        /*
            r5 = this;
        L_0x0000:
            java.util.concurrent.ConcurrentMap<I, java.util.concurrent.Future<O>> r4 = r5.cache
            java.lang.Object r2 = r4.get(r6)
            java.util.concurrent.Future r2 = (java.util.concurrent.Future) r2
            if (r2 != 0) goto L_0x0022
            org.apache.commons.lang3.concurrent.Memoizer$1 r1 = new org.apache.commons.lang3.concurrent.Memoizer$1
            r1.<init>(r6)
            java.util.concurrent.FutureTask r3 = new java.util.concurrent.FutureTask
            r3.<init>(r1)
            java.util.concurrent.ConcurrentMap<I, java.util.concurrent.Future<O>> r4 = r5.cache
            java.lang.Object r2 = r4.putIfAbsent(r6, r3)
            java.util.concurrent.Future r2 = (java.util.concurrent.Future) r2
            if (r2 != 0) goto L_0x0022
            r2 = r3
            r3.run()
        L_0x0022:
            java.lang.Object r4 = r2.get()     // Catch:{ CancellationException -> 0x0027, ExecutionException -> 0x002e }
            return r4
        L_0x0027:
            r0 = move-exception
            java.util.concurrent.ConcurrentMap<I, java.util.concurrent.Future<O>> r4 = r5.cache
            r4.remove(r6, r2)
            goto L_0x0000
        L_0x002e:
            r0 = move-exception
            boolean r4 = r5.recalculate
            if (r4 == 0) goto L_0x0038
            java.util.concurrent.ConcurrentMap<I, java.util.concurrent.Future<O>> r4 = r5.cache
            r4.remove(r6, r2)
        L_0x0038:
            java.lang.Throwable r4 = r0.getCause()
            java.lang.RuntimeException r4 = r5.launderException(r4)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang3.concurrent.Memoizer.compute(java.lang.Object):java.lang.Object");
    }

    private RuntimeException launderException(Throwable throwable) {
        if (throwable instanceof RuntimeException) {
            return (RuntimeException) throwable;
        }
        if (throwable instanceof Error) {
            throw ((Error) throwable);
        }
        throw new IllegalStateException("Unchecked exception", throwable);
    }
}
