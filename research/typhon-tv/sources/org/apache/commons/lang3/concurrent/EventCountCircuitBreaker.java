package org.apache.commons.lang3.concurrent;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.concurrent.AbstractCircuitBreaker;

public class EventCountCircuitBreaker extends AbstractCircuitBreaker<Integer> {
    private static final Map<AbstractCircuitBreaker.State, StateStrategy> STRATEGY_MAP = createStrategyMap();
    private final AtomicReference<CheckIntervalData> checkIntervalData;
    private final long closingInterval;
    private final int closingThreshold;
    private final long openingInterval;
    private final int openingThreshold;

    public EventCountCircuitBreaker(int openingThreshold2, long openingInterval2, TimeUnit openingUnit, int closingThreshold2, long closingInterval2, TimeUnit closingUnit) {
        this.checkIntervalData = new AtomicReference<>(new CheckIntervalData(0, 0));
        this.openingThreshold = openingThreshold2;
        this.openingInterval = openingUnit.toNanos(openingInterval2);
        this.closingThreshold = closingThreshold2;
        this.closingInterval = closingUnit.toNanos(closingInterval2);
    }

    public EventCountCircuitBreaker(int openingThreshold2, long checkInterval, TimeUnit checkUnit, int closingThreshold2) {
        this(openingThreshold2, checkInterval, checkUnit, closingThreshold2, checkInterval, checkUnit);
    }

    public EventCountCircuitBreaker(int threshold, long checkInterval, TimeUnit checkUnit) {
        this(threshold, checkInterval, checkUnit, threshold);
    }

    public int getOpeningThreshold() {
        return this.openingThreshold;
    }

    public long getOpeningInterval() {
        return this.openingInterval;
    }

    public int getClosingThreshold() {
        return this.closingThreshold;
    }

    public long getClosingInterval() {
        return this.closingInterval;
    }

    public boolean checkState() {
        return performStateCheck(0);
    }

    public boolean incrementAndCheckState(Integer increment) throws CircuitBreakingException {
        return performStateCheck(1);
    }

    public boolean incrementAndCheckState() {
        return incrementAndCheckState((Integer) 1);
    }

    public void open() {
        super.open();
        this.checkIntervalData.set(new CheckIntervalData(0, now()));
    }

    public void close() {
        super.close();
        this.checkIntervalData.set(new CheckIntervalData(0, now()));
    }

    private boolean performStateCheck(int increment) {
        AbstractCircuitBreaker.State currentState;
        CheckIntervalData currentData;
        CheckIntervalData nextData;
        do {
            long time = now();
            currentState = (AbstractCircuitBreaker.State) this.state.get();
            currentData = this.checkIntervalData.get();
            nextData = nextCheckIntervalData(increment, currentData, currentState, time);
        } while (!updateCheckIntervalData(currentData, nextData));
        if (stateStrategy(currentState).m20671(this, currentData, nextData)) {
            currentState = currentState.oppositeState();
            changeStateAndStartNewCheckInterval(currentState);
        }
        return !isOpen(currentState);
    }

    private boolean updateCheckIntervalData(CheckIntervalData currentData, CheckIntervalData nextData) {
        return currentData == nextData || this.checkIntervalData.compareAndSet(currentData, nextData);
    }

    private void changeStateAndStartNewCheckInterval(AbstractCircuitBreaker.State newState) {
        changeState(newState);
        this.checkIntervalData.set(new CheckIntervalData(0, now()));
    }

    private CheckIntervalData nextCheckIntervalData(int increment, CheckIntervalData currentData, AbstractCircuitBreaker.State currentState, long time) {
        if (stateStrategy(currentState).m20670(this, currentData, time)) {
            return new CheckIntervalData(increment, time);
        }
        return currentData.m20668(increment);
    }

    /* access modifiers changed from: package-private */
    public long now() {
        return System.nanoTime();
    }

    private static StateStrategy stateStrategy(AbstractCircuitBreaker.State state) {
        return STRATEGY_MAP.get(state);
    }

    private static Map<AbstractCircuitBreaker.State, StateStrategy> createStrategyMap() {
        Map<AbstractCircuitBreaker.State, StateStrategy> map = new EnumMap<>(AbstractCircuitBreaker.State.class);
        map.put(AbstractCircuitBreaker.State.CLOSED, new StateStrategyClosed());
        map.put(AbstractCircuitBreaker.State.OPEN, new StateStrategyOpen());
        return map;
    }

    private static class CheckIntervalData {

        /* renamed from: 靐  reason: contains not printable characters */
        private final long f16541;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f16542;

        CheckIntervalData(int count, long intervalStart) {
            this.f16542 = count;
            this.f16541 = intervalStart;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20667() {
            return this.f16542;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m20666() {
            return this.f16541;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public CheckIntervalData m20668(int delta) {
            return delta != 0 ? new CheckIntervalData(m20667() + delta, m20666()) : this;
        }
    }

    private static abstract class StateStrategy {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract long m20669(EventCountCircuitBreaker eventCountCircuitBreaker);

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract boolean m20671(EventCountCircuitBreaker eventCountCircuitBreaker, CheckIntervalData checkIntervalData, CheckIntervalData checkIntervalData2);

        private StateStrategy() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20670(EventCountCircuitBreaker breaker, CheckIntervalData currentData, long now) {
            return now - currentData.m20666() > m20669(breaker);
        }
    }

    private static class StateStrategyClosed extends StateStrategy {
        private StateStrategyClosed() {
            super();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20673(EventCountCircuitBreaker breaker, CheckIntervalData currentData, CheckIntervalData nextData) {
            return nextData.m20667() > breaker.getOpeningThreshold();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m20672(EventCountCircuitBreaker breaker) {
            return breaker.getOpeningInterval();
        }
    }

    private static class StateStrategyOpen extends StateStrategy {
        private StateStrategyOpen() {
            super();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20675(EventCountCircuitBreaker breaker, CheckIntervalData currentData, CheckIntervalData nextData) {
            return nextData.m20666() != currentData.m20666() && currentData.m20667() < breaker.getClosingThreshold();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m20674(EventCountCircuitBreaker breaker) {
            return breaker.getClosingInterval();
        }
    }
}
