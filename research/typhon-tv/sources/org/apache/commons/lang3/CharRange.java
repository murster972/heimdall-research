package org.apache.commons.lang3;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

final class CharRange implements Serializable, Iterable<Character> {
    private static final long serialVersionUID = 8270183163158333422L;
    /* access modifiers changed from: private */
    public final char end;
    /* access modifiers changed from: private */
    public final boolean negated;
    /* access modifiers changed from: private */
    public final char start;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient String f16514;

    private CharRange(char start2, char end2, boolean negated2) {
        if (start2 > end2) {
            char temp = start2;
            start2 = end2;
            end2 = temp;
        }
        this.start = start2;
        this.end = end2;
        this.negated = negated2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static CharRange m20617(char ch) {
        return new CharRange(ch, ch, false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static CharRange m20614(char ch) {
        return new CharRange(ch, ch, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static CharRange m20618(char start2, char end2) {
        return new CharRange(start2, end2, false);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static CharRange m20615(char start2, char end2) {
        return new CharRange(start2, end2, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20621() {
        return this.negated;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m20620(char ch) {
        return (ch >= this.start && ch <= this.end) != this.negated;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CharRange)) {
            return false;
        }
        CharRange other = (CharRange) obj;
        if (this.start == other.start && this.end == other.end && this.negated == other.negated) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.negated ? 1 : 0) + (this.end * 7) + this.start + 'S';
    }

    public String toString() {
        if (this.f16514 == null) {
            StringBuilder buf = new StringBuilder(4);
            if (m20621()) {
                buf.append('^');
            }
            buf.append(this.start);
            if (this.start != this.end) {
                buf.append('-');
                buf.append(this.end);
            }
            this.f16514 = buf.toString();
        }
        return this.f16514;
    }

    public Iterator<Character> iterator() {
        return new CharacterIterator();
    }

    private static class CharacterIterator implements Iterator<Character> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final CharRange f16515;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f16516;

        /* renamed from: 龘  reason: contains not printable characters */
        private char f16517;

        private CharacterIterator(CharRange r) {
            this.f16515 = r;
            this.f16516 = true;
            if (!this.f16515.negated) {
                this.f16517 = this.f16515.start;
            } else if (this.f16515.start != 0) {
                this.f16517 = 0;
            } else if (this.f16515.end == 65535) {
                this.f16516 = false;
            } else {
                this.f16517 = (char) (this.f16515.end + 1);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m20622() {
            if (this.f16515.negated) {
                if (this.f16517 == 65535) {
                    this.f16516 = false;
                } else if (this.f16517 + 1 != this.f16515.start) {
                    this.f16517 = (char) (this.f16517 + 1);
                } else if (this.f16515.end == 65535) {
                    this.f16516 = false;
                } else {
                    this.f16517 = (char) (this.f16515.end + 1);
                }
            } else if (this.f16517 < this.f16515.end) {
                this.f16517 = (char) (this.f16517 + 1);
            } else {
                this.f16516 = false;
            }
        }

        public boolean hasNext() {
            return this.f16516;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Character next() {
            if (!this.f16516) {
                throw new NoSuchElementException();
            }
            char cur = this.f16517;
            m20622();
            return Character.valueOf(cur);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
