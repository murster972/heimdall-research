package org.apache.commons.lang3.reflect;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.apache.commons.lang3.ClassUtils;

abstract class MemberUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Class<?>[] f16549 = {Byte.TYPE, Short.TYPE, Character.TYPE, Integer.TYPE, Long.TYPE, Float.TYPE, Double.TYPE};

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20684(AccessibleObject o) {
        if (o == null || o.isAccessible()) {
            return false;
        }
        Member m = (Member) o;
        if (!o.isAccessible() && Modifier.isPublic(m.getModifiers()) && m20683(m.getDeclaringClass().getModifiers())) {
            try {
                o.setAccessible(true);
                return true;
            } catch (SecurityException e) {
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20683(int modifiers) {
        return (modifiers & 7) == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20686(Member m) {
        return m != null && Modifier.isPublic(m.getModifiers()) && !m.isSynthetic();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m20680(Constructor<?> left, Constructor<?> right, Class<?>[] actual) {
        return m20682(Executable.m20689(left), Executable.m20689(right), actual);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m20681(Method left, Method right, Class<?>[] actual) {
        return m20682(Executable.m20690(left), Executable.m20690(right), actual);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m20682(Executable left, Executable right, Class<?>[] actual) {
        float leftCost = m20679(actual, left);
        float rightCost = m20679(actual, right);
        if (leftCost < rightCost) {
            return -1;
        }
        return rightCost < leftCost ? 1 : 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static float m20679(Class<?>[] srcArgs, Executable executable) {
        Class<?>[] destArgs = executable.m20694();
        boolean isVarArgs = executable.m20693();
        float totalCost = 0.0f;
        long normalArgsLen = isVarArgs ? (long) (destArgs.length - 1) : (long) destArgs.length;
        if (((long) srcArgs.length) < normalArgsLen) {
            return Float.MAX_VALUE;
        }
        for (int i = 0; ((long) i) < normalArgsLen; i++) {
            totalCost += m20678(srcArgs[i], destArgs[i]);
        }
        if (isVarArgs) {
            boolean noVarArgsPassed = srcArgs.length < destArgs.length;
            boolean explicitArrayForVarags = srcArgs.length == destArgs.length && srcArgs[srcArgs.length + -1].isArray();
            Class<?> destClass = destArgs[destArgs.length - 1].getComponentType();
            if (noVarArgsPassed) {
                totalCost += m20678(destClass, (Class<?>) Object.class) + 0.001f;
            } else if (explicitArrayForVarags) {
                totalCost += m20678(srcArgs[srcArgs.length - 1].getComponentType(), destClass) + 0.001f;
            } else {
                for (int i2 = destArgs.length - 1; i2 < srcArgs.length; i2++) {
                    totalCost += m20678(srcArgs[i2], destClass) + 0.001f;
                }
            }
        }
        float f = totalCost;
        return totalCost;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static float m20678(Class<?> srcClass, Class<?> destClass) {
        if (destClass.isPrimitive()) {
            return m20677(srcClass, destClass);
        }
        float cost = 0.0f;
        Class<? super Object> srcClass2 = srcClass;
        while (true) {
            if (srcClass2 != null && !destClass.equals(srcClass2)) {
                if (destClass.isInterface() && ClassUtils.isAssignable(srcClass2, destClass)) {
                    cost += 0.25f;
                    break;
                }
                cost += 1.0f;
                srcClass2 = srcClass2.getSuperclass();
            } else {
                break;
            }
        }
        if (srcClass2 == null) {
            return cost + 1.5f;
        }
        return cost;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static float m20677(Class<?> srcClass, Class<?> destClass) {
        float cost = 0.0f;
        Class<?> cls = srcClass;
        if (!cls.isPrimitive()) {
            cost = 0.0f + 0.1f;
            cls = ClassUtils.wrapperToPrimitive(cls);
        }
        int i = 0;
        while (cls != destClass && i < f16549.length) {
            if (cls == f16549[i]) {
                cost += 0.1f;
                if (i < f16549.length - 1) {
                    cls = f16549[i + 1];
                }
            }
            i++;
        }
        return cost;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20687(Method method, Class<?>[] parameterTypes) {
        return m20688(Executable.m20690(method), parameterTypes);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20685(Constructor<?> method, Class<?>[] parameterTypes) {
        return m20688(Executable.m20689(method), parameterTypes);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m20688(Executable method, Class<?>[] parameterTypes) {
        Class<?>[] methodParameterTypes = method.m20694();
        if (ClassUtils.isAssignable(parameterTypes, methodParameterTypes, true)) {
            return true;
        }
        if (!method.m20693()) {
            return false;
        }
        int i = 0;
        while (i < methodParameterTypes.length - 1 && i < parameterTypes.length) {
            if (!ClassUtils.isAssignable(parameterTypes[i], methodParameterTypes[i], true)) {
                return false;
            }
            i++;
        }
        Class<?> varArgParameterType = methodParameterTypes[methodParameterTypes.length - 1].getComponentType();
        while (i < parameterTypes.length) {
            if (!ClassUtils.isAssignable(parameterTypes[i], varArgParameterType, true)) {
                return false;
            }
            i++;
        }
        return true;
    }

    private static final class Executable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f16550;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Class<?>[] f16551;

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public static Executable m20690(Method method) {
            return new Executable(method);
        }

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public static Executable m20689(Constructor<?> constructor) {
            return new Executable(constructor);
        }

        private Executable(Method method) {
            this.f16551 = method.getParameterTypes();
            this.f16550 = method.isVarArgs();
        }

        private Executable(Constructor<?> constructor) {
            this.f16551 = constructor.getParameterTypes();
            this.f16550 = constructor.isVarArgs();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Class<?>[] m20694() {
            return this.f16551;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m20693() {
            return this.f16550;
        }
    }
}
