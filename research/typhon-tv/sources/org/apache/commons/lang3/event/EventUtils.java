package org.apache.commons.lang3.event;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.reflect.MethodUtils;

public class EventUtils {
    public static <L> void addEventListener(Object eventSource, Class<L> listenerType, L listener) {
        try {
            MethodUtils.invokeMethod(eventSource, "add" + listenerType.getSimpleName(), listener);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Class " + eventSource.getClass().getName() + " does not have a public add" + listenerType.getSimpleName() + " method which takes a parameter of type " + listenerType.getName() + ".");
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("Class " + eventSource.getClass().getName() + " does not have an accessible add" + listenerType.getSimpleName() + " method which takes a parameter of type " + listenerType.getName() + ".");
        } catch (InvocationTargetException e3) {
            throw new RuntimeException("Unable to add listener.", e3.getCause());
        }
    }

    public static <L> void bindEventsToMethod(Object target, String methodName, Object eventSource, Class<L> listenerType, String... eventTypes) {
        addEventListener(eventSource, listenerType, listenerType.cast(Proxy.newProxyInstance(target.getClass().getClassLoader(), new Class[]{listenerType}, new EventBindingInvocationHandler(target, methodName, eventTypes))));
    }

    private static class EventBindingInvocationHandler implements InvocationHandler {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f16546;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Set<String> f16547;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f16548;

        EventBindingInvocationHandler(Object target, String methodName, String[] eventTypes) {
            this.f16548 = target;
            this.f16546 = methodName;
            this.f16547 = new HashSet(Arrays.asList(eventTypes));
        }

        public Object invoke(Object proxy, Method method, Object[] parameters) throws Throwable {
            if (!this.f16547.isEmpty() && !this.f16547.contains(method.getName())) {
                return null;
            }
            if (m20676(method)) {
                return MethodUtils.invokeMethod(this.f16548, this.f16546, parameters);
            }
            return MethodUtils.invokeMethod(this.f16548, this.f16546);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m20676(Method method) {
            return MethodUtils.getAccessibleMethod(this.f16548.getClass(), this.f16546, method.getParameterTypes()) != null;
        }
    }
}
