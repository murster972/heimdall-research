package org.apache.commons.codec.digest;

import java.util.Random;

class B64 {
    /* renamed from: 龘  reason: contains not printable characters */
    static void m20585(byte b2, byte b1, byte b0, int outLen, StringBuilder buffer) {
        int w = ((b2 << 16) & 16777215) | ((b1 << 8) & 65535) | (b0 & 255);
        int n = outLen;
        while (true) {
            int n2 = n;
            n = n2 - 1;
            if (n2 > 0) {
                buffer.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(w & 63));
                w >>= 6;
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m20584(int num) {
        StringBuilder saltString = new StringBuilder();
        for (int i = 1; i <= num; i++) {
            saltString.append("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(new Random().nextInt("./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".length())));
        }
        return saltString.toString();
    }
}
