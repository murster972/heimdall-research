package org.apache.commons.codec.language.bm;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.codec.language.bm.Languages;

public class Lang {
    private static final String LANGUAGE_RULES_RN = "org/apache/commons/codec/language/bm/lang.txt";
    private static final Map<NameType, Lang> Langs = new EnumMap(NameType.class);
    private final Languages languages;
    private final List<LangRule> rules;

    private static final class LangRule {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public final Set<String> f16486;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Pattern f16487;
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public final boolean f16488;

        private LangRule(Pattern pattern, Set<String> languages, boolean acceptOnMatch) {
            this.f16487 = pattern;
            this.f16486 = languages;
            this.f16488 = acceptOnMatch;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m20600(String txt) {
            return this.f16487.matcher(txt).find();
        }
    }

    static {
        for (NameType s : NameType.values()) {
            Langs.put(s, loadFromResource(LANGUAGE_RULES_RN, Languages.getInstance(s)));
        }
    }

    public static Lang instance(NameType nameType) {
        return Langs.get(nameType);
    }

    public static Lang loadFromResource(String languageRulesResourceName, Languages languages2) {
        List<LangRule> rules2 = new ArrayList<>();
        InputStream lRulesIS = Lang.class.getClassLoader().getResourceAsStream(languageRulesResourceName);
        if (lRulesIS == null) {
            throw new IllegalStateException("Unable to resolve required resource:org/apache/commons/codec/language/bm/lang.txt");
        }
        Scanner scanner = new Scanner(lRulesIS, "UTF-8");
        boolean inExtendedComment = false;
        while (scanner.hasNextLine()) {
            String rawLine = scanner.nextLine();
            String line = rawLine;
            if (inExtendedComment) {
                if (line.endsWith("*/")) {
                    inExtendedComment = false;
                }
            } else if (line.startsWith("/*")) {
                inExtendedComment = true;
            } else {
                int cmtI = line.indexOf("//");
                if (cmtI >= 0) {
                    line = line.substring(0, cmtI);
                }
                String line2 = line.trim();
                if (line2.length() != 0) {
                    String[] parts = line2.split("\\s+");
                    if (parts.length != 3) {
                        throw new IllegalArgumentException("Malformed line '" + rawLine + "' in language resource '" + languageRulesResourceName + "'");
                    }
                    Pattern pattern = Pattern.compile(parts[0]);
                    String[] langs = parts[1].split("\\+");
                    rules2.add(new LangRule(pattern, new HashSet(Arrays.asList(langs)), parts[2].equals("true")));
                } else {
                    continue;
                }
            }
        }
        return new Lang(rules2, languages2);
    }

    private Lang(List<LangRule> rules2, Languages languages2) {
        this.rules = Collections.unmodifiableList(rules2);
        this.languages = languages2;
    }

    public String guessLanguage(String text) {
        Languages.LanguageSet ls = guessLanguages(text);
        return ls.isSingleton() ? ls.getAny() : "any";
    }

    public Languages.LanguageSet guessLanguages(String input) {
        String text = input.toLowerCase(Locale.ENGLISH);
        Set<String> langs = new HashSet<>(this.languages.getLanguages());
        for (LangRule rule : this.rules) {
            if (rule.m20600(text)) {
                if (rule.f16488) {
                    langs.retainAll(rule.f16486);
                } else {
                    langs.removeAll(rule.f16486);
                }
            }
        }
        Languages.LanguageSet ls = Languages.LanguageSet.from(langs);
        return ls.equals(Languages.NO_LANGUAGES) ? Languages.ANY_LANGUAGE : ls;
    }
}
