package org.apache.commons.codec.language.bm;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.codec.language.bm.Languages;
import org.apache.commons.codec.language.bm.Rule;
import org.apache.commons.lang3.StringUtils;

public class PhoneticEngine {
    private static final int DEFAULT_MAX_PHONEMES = 20;
    private static final Map<NameType, Set<String>> NAME_PREFIXES = new EnumMap(NameType.class);
    private final boolean concat;
    private final Lang lang;
    private final int maxPhonemes;
    private final NameType nameType;
    private final RuleType ruleType;

    static final class PhonemeBuilder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Set<Rule.Phoneme> f16492;

        /* renamed from: 龘  reason: contains not printable characters */
        public static PhonemeBuilder m20601(Languages.LanguageSet languages) {
            return new PhonemeBuilder(Collections.singleton(new Rule.Phoneme("", languages)));
        }

        private PhonemeBuilder(Set<Rule.Phoneme> phonemes) {
            this.f16492 = phonemes;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PhonemeBuilder m20604(CharSequence str) {
            Set<Rule.Phoneme> newPhonemes = new LinkedHashSet<>();
            for (Rule.Phoneme ph : this.f16492) {
                newPhonemes.add(ph.append(str));
            }
            return new PhonemeBuilder(newPhonemes);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PhonemeBuilder m20605(Rule.PhonemeExpr phonemeExpr, int maxPhonemes) {
            Set<Rule.Phoneme> newPhonemes = new LinkedHashSet<>();
            loop0:
            for (Rule.Phoneme left : this.f16492) {
                Iterator i$ = phonemeExpr.getPhonemes().iterator();
                while (true) {
                    if (i$.hasNext()) {
                        Rule.Phoneme join = left.join(i$.next());
                        if (!join.getLanguages().isEmpty()) {
                            if (newPhonemes.size() >= maxPhonemes) {
                                break loop0;
                            }
                            newPhonemes.add(join);
                        }
                    }
                }
            }
            return new PhonemeBuilder(newPhonemes);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Set<Rule.Phoneme> m20603() {
            return this.f16492;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m20602() {
            StringBuilder sb = new StringBuilder();
            for (Rule.Phoneme ph : this.f16492) {
                if (sb.length() > 0) {
                    sb.append("|");
                }
                sb.append(ph.getPhonemeText());
            }
            return sb.toString();
        }
    }

    private static final class RulesApplication {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f16493;

        /* renamed from: 连任  reason: contains not printable characters */
        private final int f16494;

        /* renamed from: 靐  reason: contains not printable characters */
        private final CharSequence f16495;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f16496;

        /* renamed from: 齉  reason: contains not printable characters */
        private PhonemeBuilder f16497;

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<Rule> f16498;

        public RulesApplication(List<Rule> finalRules, CharSequence input, PhonemeBuilder phonemeBuilder, int i, int maxPhonemes) {
            if (finalRules == null) {
                throw new NullPointerException("The finalRules argument must not be null");
            }
            this.f16498 = finalRules;
            this.f16497 = phonemeBuilder;
            this.f16495 = input;
            this.f16496 = i;
            this.f16494 = maxPhonemes;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20609() {
            return this.f16496;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public PhonemeBuilder m20606() {
            return this.f16497;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public RulesApplication m20608() {
            this.f16493 = false;
            int patternLength = 0;
            Iterator i$ = this.f16498.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Rule rule = i$.next();
                patternLength = rule.getPattern().length();
                if (rule.patternAndContextMatches(this.f16495, this.f16496)) {
                    this.f16497 = this.f16497.m20605(rule.getPhoneme(), this.f16494);
                    this.f16493 = true;
                    break;
                }
            }
            if (!this.f16493) {
                patternLength = 1;
            }
            this.f16496 += patternLength;
            return this;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean m20607() {
            return this.f16493;
        }
    }

    static {
        NAME_PREFIXES.put(NameType.ASHKENAZI, Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"bar", "ben", "da", "de", "van", "von"}))));
        NAME_PREFIXES.put(NameType.SEPHARDIC, Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{PubnativeRequest.Parameters.ASSET_LAYOUT, "el", "da", "dal", "de", "del", "dela", "de la", "della", "des", "di", "do", "dos", "du", "van", "von"}))));
        NAME_PREFIXES.put(NameType.GENERIC, Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"da", "dal", "de", "del", "dela", "de la", "della", "des", "di", "do", "dos", "du", "van", "von"}))));
    }

    private static CharSequence cacheSubSequence(final CharSequence cached) {
        final CharSequence[][] cache = (CharSequence[][]) Array.newInstance(CharSequence.class, new int[]{cached.length(), cached.length()});
        return new CharSequence() {
            public char charAt(int index) {
                return cached.charAt(index);
            }

            public int length() {
                return cached.length();
            }

            public CharSequence subSequence(int start, int end) {
                if (start == end) {
                    return "";
                }
                CharSequence res = cache[start][end - 1];
                if (res != null) {
                    return res;
                }
                CharSequence res2 = cached.subSequence(start, end);
                cache[start][end - 1] = res2;
                return res2;
            }
        };
    }

    private static String join(Iterable<String> strings, String sep) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> si = strings.iterator();
        if (si.hasNext()) {
            sb.append(si.next());
        }
        while (si.hasNext()) {
            sb.append(sep).append(si.next());
        }
        return sb.toString();
    }

    public PhoneticEngine(NameType nameType2, RuleType ruleType2, boolean concat2) {
        this(nameType2, ruleType2, concat2, 20);
    }

    public PhoneticEngine(NameType nameType2, RuleType ruleType2, boolean concat2, int maxPhonemes2) {
        if (ruleType2 == RuleType.RULES) {
            throw new IllegalArgumentException("ruleType must not be " + RuleType.RULES);
        }
        this.nameType = nameType2;
        this.ruleType = ruleType2;
        this.concat = concat2;
        this.lang = Lang.instance(nameType2);
        this.maxPhonemes = maxPhonemes2;
    }

    private PhonemeBuilder applyFinalRules(PhonemeBuilder phonemeBuilder, List<Rule> finalRules) {
        if (finalRules == null) {
            throw new NullPointerException("finalRules can not be null");
        } else if (finalRules.isEmpty()) {
            return phonemeBuilder;
        } else {
            Set<Rule.Phoneme> phonemes = new TreeSet<>(Rule.Phoneme.COMPARATOR);
            for (Rule.Phoneme phoneme : phonemeBuilder.m20603()) {
                PhonemeBuilder subBuilder = PhonemeBuilder.m20601(phoneme.getLanguages());
                CharSequence phonemeText = cacheSubSequence(phoneme.getPhonemeText());
                int i = 0;
                while (i < phonemeText.length()) {
                    RulesApplication rulesApplication = new RulesApplication(finalRules, phonemeText, subBuilder, i, this.maxPhonemes).m20608();
                    boolean found = rulesApplication.m20607();
                    subBuilder = rulesApplication.m20606();
                    if (!found) {
                        subBuilder = subBuilder.m20604(phonemeText.subSequence(i, i + 1));
                    }
                    i = rulesApplication.m20609();
                }
                phonemes.addAll(subBuilder.m20603());
            }
            return new PhonemeBuilder(phonemes);
        }
    }

    public String encode(String input) {
        return encode(input, this.lang.guessLanguages(input));
    }

    public String encode(String input, Languages.LanguageSet languageSet) {
        String input2;
        List<Rule> rules = Rule.getInstance(this.nameType, RuleType.RULES, languageSet);
        List<Rule> finalRules1 = Rule.getInstance(this.nameType, this.ruleType, "common");
        List<Rule> finalRules2 = Rule.getInstance(this.nameType, this.ruleType, languageSet);
        String input3 = input.toLowerCase(Locale.ENGLISH).replace('-', ' ').trim();
        if (this.nameType == NameType.GENERIC) {
            if (input3.length() < 2 || !input3.substring(0, 2).equals("d'")) {
                for (String l : NAME_PREFIXES.get(this.nameType)) {
                    if (input3.startsWith(l + StringUtils.SPACE)) {
                        String remainder = input3.substring(l.length() + 1);
                        return "(" + encode(remainder) + ")-(" + encode(l + remainder) + ")";
                    }
                }
            } else {
                String remainder2 = input3.substring(2);
                return "(" + encode(remainder2) + ")-(" + encode("d" + remainder2) + ")";
            }
        }
        List<String> words = Arrays.asList(input3.split("\\s+"));
        ArrayList<String> arrayList = new ArrayList<>();
        switch (this.nameType) {
            case SEPHARDIC:
                for (String aWord : words) {
                    String[] parts = aWord.split("'");
                    arrayList.add(parts[parts.length - 1]);
                }
                arrayList.removeAll(NAME_PREFIXES.get(this.nameType));
                break;
            case ASHKENAZI:
                arrayList.addAll(words);
                arrayList.removeAll(NAME_PREFIXES.get(this.nameType));
                break;
            case GENERIC:
                arrayList.addAll(words);
                break;
            default:
                throw new IllegalStateException("Unreachable case: " + this.nameType);
        }
        if (this.concat) {
            input2 = join(arrayList, StringUtils.SPACE);
        } else if (arrayList.size() == 1) {
            input2 = words.iterator().next();
        } else {
            StringBuilder result = new StringBuilder();
            for (String word : arrayList) {
                result.append("-").append(encode(word));
            }
            return result.substring(1);
        }
        PhonemeBuilder phonemeBuilder = PhonemeBuilder.m20601(languageSet);
        CharSequence inputCache = cacheSubSequence(input2);
        int i = 0;
        while (i < inputCache.length()) {
            RulesApplication rulesApplication = new RulesApplication(rules, inputCache, phonemeBuilder, i, this.maxPhonemes).m20608();
            i = rulesApplication.m20609();
            phonemeBuilder = rulesApplication.m20606();
        }
        return applyFinalRules(applyFinalRules(phonemeBuilder, finalRules1), finalRules2).m20602();
    }

    public Lang getLang() {
        return this.lang;
    }

    public NameType getNameType() {
        return this.nameType;
    }

    public RuleType getRuleType() {
        return this.ruleType;
    }

    public boolean isConcat() {
        return this.concat;
    }

    public int getMaxPhonemes() {
        return this.maxPhonemes;
    }
}
