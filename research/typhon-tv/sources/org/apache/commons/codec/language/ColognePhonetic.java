package org.apache.commons.codec.language;

import java.util.Locale;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.StringEncoder;

public class ColognePhonetic implements StringEncoder {
    private static final char[] AEIJOUY = {'A', 'E', 'I', 'J', 'O', 'U', 'Y'};
    private static final char[] AHKLOQRUX = {'A', 'H', 'K', 'L', 'O', 'Q', 'R', 'U', 'X'};
    private static final char[] AHOUKQX = {'A', 'H', 'O', 'U', 'K', 'Q', 'X'};
    private static final char[] CKQ = {'C', 'K', 'Q'};
    private static final char[] GKQ = {'G', 'K', 'Q'};
    private static final char[][] PREPROCESS_MAP = {new char[]{196, 'A'}, new char[]{220, 'U'}, new char[]{214, 'O'}, new char[]{223, 'S'}};
    private static final char[] SCZ = {'S', 'C', 'Z'};
    private static final char[] SZ = {'S', 'Z'};
    private static final char[] TDX = {'T', 'D', 'X'};
    private static final char[] WFPV = {'W', 'F', 'P', 'V'};

    private abstract class CologneBuffer {

        /* renamed from: 靐  reason: contains not printable characters */
        protected int f16481 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final char[] f16483;

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract char[] m20587(int i, int i2);

        public CologneBuffer(char[] data) {
            this.f16483 = data;
            this.f16481 = data.length;
        }

        public CologneBuffer(int buffSize) {
            this.f16483 = new char[buffSize];
            this.f16481 = 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m20586() {
            return this.f16481;
        }

        public String toString() {
            return new String(m20587(0, this.f16481));
        }
    }

    private class CologneOutputBuffer extends CologneBuffer {
        public CologneOutputBuffer(int buffSize) {
            super(buffSize);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20593(char chr) {
            this.f16483[this.f16481] = chr;
            this.f16481++;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public char[] m20594(int start, int length) {
            char[] newData = new char[length];
            System.arraycopy(this.f16483, start, newData, 0, length);
            return newData;
        }
    }

    private class CologneInputBuffer extends CologneBuffer {
        public CologneInputBuffer(char[] data) {
            super(data);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m20591(char ch) {
            this.f16481++;
            this.f16483[m20590()] = ch;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public char[] m20592(int start, int length) {
            char[] newData = new char[length];
            System.arraycopy(this.f16483, (this.f16483.length - this.f16481) + start, newData, 0, length);
            return newData;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public char m20588() {
            return this.f16483[m20590()];
        }

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public int m20590() {
            return this.f16483.length - this.f16481;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public char m20589() {
            this.f16481--;
            return m20588();
        }
    }

    private static boolean arrayContains(char[] arr, char key) {
        for (char element : arr) {
            if (element == key) {
                return true;
            }
        }
        return false;
    }

    public String colognePhonetic(String text) {
        char nextChar;
        char code;
        if (text == null) {
            return null;
        }
        String text2 = preprocess(text);
        CologneOutputBuffer output = new CologneOutputBuffer(text2.length() * 2);
        CologneInputBuffer input = new CologneInputBuffer(text2.toCharArray());
        char lastChar = '-';
        char lastCode = '/';
        int rightLength = input.m20586();
        while (rightLength > 0) {
            char chr = input.m20589();
            rightLength = input.m20586();
            if (rightLength > 0) {
                nextChar = input.m20588();
            } else {
                nextChar = '-';
            }
            if (arrayContains(AEIJOUY, chr)) {
                code = '0';
            } else if (chr == 'H' || chr < 'A' || chr > 'Z') {
                if (lastCode != '/') {
                    code = '-';
                }
            } else if (chr == 'B' || (chr == 'P' && nextChar != 'H')) {
                code = '1';
            } else if ((chr == 'D' || chr == 'T') && !arrayContains(SCZ, nextChar)) {
                code = '2';
            } else if (arrayContains(WFPV, chr)) {
                code = '3';
            } else if (arrayContains(GKQ, chr)) {
                code = '4';
            } else if (chr == 'X' && !arrayContains(CKQ, lastChar)) {
                code = '4';
                input.m20591('S');
                rightLength++;
            } else if (chr == 'S' || chr == 'Z') {
                code = '8';
            } else if (chr == 'C') {
                if (lastCode == '/') {
                    if (arrayContains(AHKLOQRUX, nextChar)) {
                        code = '4';
                    } else {
                        code = '8';
                    }
                } else if (arrayContains(SZ, lastChar) || !arrayContains(AHOUKQX, nextChar)) {
                    code = '8';
                } else {
                    code = '4';
                }
            } else if (arrayContains(TDX, chr)) {
                code = '8';
            } else if (chr == 'R') {
                code = '7';
            } else if (chr == 'L') {
                code = '5';
            } else if (chr == 'M' || chr == 'N') {
                code = '6';
            } else {
                code = chr;
            }
            if (code != '-' && ((lastCode != code && (code != '0' || lastCode == '/')) || code < '0' || code > '8')) {
                output.m20593(code);
            }
            lastChar = chr;
            lastCode = code;
        }
        return output.toString();
    }

    public Object encode(Object object) throws EncoderException {
        if (object instanceof String) {
            return encode((String) object);
        }
        throw new EncoderException("This method's parameter was expected to be of the type " + String.class.getName() + ". But actually it was of the type " + object.getClass().getName() + ".");
    }

    public String encode(String text) {
        return colognePhonetic(text);
    }

    public boolean isEncodeEqual(String text1, String text2) {
        return colognePhonetic(text1).equals(colognePhonetic(text2));
    }

    private String preprocess(String text) {
        char[] chrs = text.toUpperCase(Locale.GERMAN).toCharArray();
        for (int index = 0; index < chrs.length; index++) {
            if (chrs[index] > 'Z') {
                char[][] arr$ = PREPROCESS_MAP;
                int len$ = arr$.length;
                int i$ = 0;
                while (true) {
                    if (i$ >= len$) {
                        break;
                    }
                    char[] element = arr$[i$];
                    if (chrs[index] == element[0]) {
                        chrs[index] = element[1];
                        break;
                    }
                    i$++;
                }
            }
        }
        return new String(chrs);
    }
}
