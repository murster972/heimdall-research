package org.apache.commons.codec.language;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.StringEncoder;

public class Soundex implements StringEncoder {
    public static final Soundex US_ENGLISH = new Soundex();
    private static final char[] US_ENGLISH_MAPPING = US_ENGLISH_MAPPING_STRING.toCharArray();
    public static final String US_ENGLISH_MAPPING_STRING = "01230120022455012623010202";
    @Deprecated
    private int maxLength;
    private final char[] soundexMapping;

    public Soundex() {
        this.maxLength = 4;
        this.soundexMapping = US_ENGLISH_MAPPING;
    }

    public Soundex(String str) {
        this.maxLength = 4;
        this.soundexMapping = str.toCharArray();
    }

    public Soundex(char[] cArr) {
        this.maxLength = 4;
        this.soundexMapping = new char[cArr.length];
        System.arraycopy(cArr, 0, this.soundexMapping, 0, cArr.length);
    }

    private char getMappingCode(String str, int i) {
        char map = map(str.charAt(i));
        if (i <= 1 || map == '0') {
            return map;
        }
        char charAt = str.charAt(i - 1);
        if ('H' != charAt && 'W' != charAt) {
            return map;
        }
        char charAt2 = str.charAt(i - 2);
        if (map(charAt2) == map || 'H' == charAt2 || 'W' == charAt2) {
            return 0;
        }
        return map;
    }

    private char[] getSoundexMapping() {
        return this.soundexMapping;
    }

    private char map(char c) {
        int i = c - 'A';
        if (i >= 0 && i < getSoundexMapping().length) {
            return getSoundexMapping()[i];
        }
        throw new IllegalArgumentException("The character is not mapped: " + c);
    }

    public int difference(String str, String str2) throws EncoderException {
        return SoundexUtils.m20596(this, str, str2);
    }

    public Object encode(Object obj) throws EncoderException {
        if (obj instanceof String) {
            return soundex((String) obj);
        }
        throw new EncoderException("Parameter supplied to Soundex encode is not of type java.lang.String");
    }

    public String encode(String str) {
        return soundex(str);
    }

    @Deprecated
    public int getMaxLength() {
        return this.maxLength;
    }

    @Deprecated
    public void setMaxLength(int i) {
        this.maxLength = i;
    }

    public String soundex(String str) {
        if (str == null) {
            String str2 = str;
            return null;
        }
        String r11 = SoundexUtils.m20597(str);
        if (r11.length() == 0) {
            String str3 = r11;
            return r11;
        }
        char[] cArr = {'0', '0', '0', '0'};
        int i = 1;
        int i2 = 1;
        cArr[0] = r11.charAt(0);
        char mappingCode = getMappingCode(r11, 0);
        while (i < r11.length() && i2 < cArr.length) {
            int i3 = i + 1;
            char mappingCode2 = getMappingCode(r11, i);
            if (mappingCode2 != 0) {
                if (!(mappingCode2 == '0' || mappingCode2 == mappingCode)) {
                    cArr[i2] = mappingCode2;
                    i2++;
                }
                mappingCode = mappingCode2;
                i = i3;
            } else {
                i = i3;
            }
        }
        String str4 = r11;
        return new String(cArr);
    }
}
