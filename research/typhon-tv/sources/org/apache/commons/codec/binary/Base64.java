package org.apache.commons.codec.binary;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.math.BigInteger;
import org.apache.commons.codec.binary.BaseNCodec;

public class Base64 extends BaseNCodec {
    private static final int BITS_PER_ENCODED_BYTE = 6;
    private static final int BYTES_PER_ENCODED_BLOCK = 4;
    private static final int BYTES_PER_UNENCODED_BLOCK = 3;
    static final byte[] CHUNK_SEPARATOR = {13, 10};
    private static final byte[] DECODE_TABLE = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private static final int MASK_6BITS = 63;
    private static final byte[] STANDARD_ENCODE_TABLE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] URL_SAFE_ENCODE_TABLE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private final int decodeSize;
    private final byte[] decodeTable;
    private final int encodeSize;
    private final byte[] encodeTable;
    private final byte[] lineSeparator;

    public Base64() {
        this(0);
    }

    public Base64(int i) {
        this(i, CHUNK_SEPARATOR);
    }

    public Base64(int i, byte[] bArr) {
        this(i, bArr, false);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Base64(int i, byte[] bArr, boolean z) {
        super(3, 4, i, bArr == null ? 0 : bArr.length);
        this.decodeTable = DECODE_TABLE;
        if (bArr == null) {
            this.encodeSize = 4;
            this.lineSeparator = null;
        } else if (containsAlphabetOrPad(bArr)) {
            throw new IllegalArgumentException("lineSeparator must not contain base64 characters: [" + StringUtils.newStringUtf8(bArr) + "]");
        } else if (i > 0) {
            this.encodeSize = bArr.length + 4;
            this.lineSeparator = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.lineSeparator, 0, bArr.length);
        } else {
            this.encodeSize = 4;
            this.lineSeparator = null;
        }
        this.decodeSize = this.encodeSize - 1;
        this.encodeTable = z ? URL_SAFE_ENCODE_TABLE : STANDARD_ENCODE_TABLE;
    }

    public Base64(boolean z) {
        this(76, CHUNK_SEPARATOR, z);
    }

    public static byte[] decodeBase64(String str) {
        return new Base64().decode(str);
    }

    public static byte[] decodeBase64(byte[] bArr) {
        return new Base64().decode(bArr);
    }

    public static BigInteger decodeInteger(byte[] bArr) {
        return new BigInteger(1, decodeBase64(bArr));
    }

    public static byte[] encodeBase64(byte[] bArr) {
        return encodeBase64(bArr, false);
    }

    public static byte[] encodeBase64(byte[] bArr, boolean z) {
        return encodeBase64(bArr, z, false);
    }

    public static byte[] encodeBase64(byte[] bArr, boolean z, boolean z2) {
        return encodeBase64(bArr, z, z2, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    public static byte[] encodeBase64(byte[] bArr, boolean z, boolean z2, int i) {
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        Base64 base64 = z ? new Base64(z2) : new Base64(0, CHUNK_SEPARATOR, z2);
        long encodedLength = base64.getEncodedLength(bArr);
        if (encodedLength <= ((long) i)) {
            return base64.encode(bArr);
        }
        throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + encodedLength + ") than the specified maximum size of " + i);
    }

    public static byte[] encodeBase64Chunked(byte[] bArr) {
        return encodeBase64(bArr, true);
    }

    public static String encodeBase64String(byte[] bArr) {
        return StringUtils.newStringUtf8(encodeBase64(bArr, false));
    }

    public static byte[] encodeBase64URLSafe(byte[] bArr) {
        return encodeBase64(bArr, false, true);
    }

    public static String encodeBase64URLSafeString(byte[] bArr) {
        return StringUtils.newStringUtf8(encodeBase64(bArr, false, true));
    }

    public static byte[] encodeInteger(BigInteger bigInteger) {
        if (bigInteger != null) {
            return encodeBase64(toIntegerBytes(bigInteger), false);
        }
        throw new NullPointerException("encodeInteger called with null parameter");
    }

    @Deprecated
    public static boolean isArrayByteBase64(byte[] bArr) {
        return isBase64(bArr);
    }

    public static boolean isBase64(byte b) {
        return b == 61 || (b >= 0 && b < DECODE_TABLE.length && DECODE_TABLE[b] != -1);
    }

    public static boolean isBase64(String str) {
        return isBase64(StringUtils.getBytesUtf8(str));
    }

    public static boolean isBase64(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            if (!isBase64(bArr[i]) && !isWhiteSpace(bArr[i])) {
                return false;
            }
        }
        return true;
    }

    static byte[] toIntegerBytes(BigInteger bigInteger) {
        int bitLength = ((bigInteger.bitLength() + 7) >> 3) << 3;
        byte[] byteArray = bigInteger.toByteArray();
        if (bigInteger.bitLength() % 8 != 0 && (bigInteger.bitLength() / 8) + 1 == bitLength / 8) {
            return byteArray;
        }
        int i = 0;
        int length = byteArray.length;
        if (bigInteger.bitLength() % 8 == 0) {
            i = 1;
            length--;
        }
        byte[] bArr = new byte[(bitLength / 8)];
        System.arraycopy(byteArray, i, bArr, (bitLength / 8) - length, length);
        return bArr;
    }

    /* access modifiers changed from: package-private */
    public void decode(byte[] bArr, int i, int i2, BaseNCodec.Context context) {
        byte b;
        if (!context.f16473) {
            if (i2 < 0) {
                context.f16473 = true;
            }
            int i3 = 0;
            int i4 = i;
            while (true) {
                if (i3 >= i2) {
                    int i5 = i4;
                    break;
                }
                byte[] ensureBufferSize = ensureBufferSize(this.decodeSize, context);
                int i6 = i4 + 1;
                byte b2 = bArr[i4];
                if (b2 == 61) {
                    context.f16473 = true;
                    break;
                }
                if (b2 >= 0 && b2 < DECODE_TABLE.length && (b = DECODE_TABLE[b2]) >= 0) {
                    context.f16475 = (context.f16475 + 1) % 4;
                    context.f16480 = (context.f16480 << 6) + b;
                    if (context.f16475 == 0) {
                        int i7 = context.f16478;
                        context.f16478 = i7 + 1;
                        ensureBufferSize[i7] = (byte) ((context.f16480 >> 16) & 255);
                        int i8 = context.f16478;
                        context.f16478 = i8 + 1;
                        ensureBufferSize[i8] = (byte) ((context.f16480 >> 8) & 255);
                        int i9 = context.f16478;
                        context.f16478 = i9 + 1;
                        ensureBufferSize[i9] = (byte) (context.f16480 & 255);
                    }
                }
                i3++;
                i4 = i6;
            }
            if (context.f16473 && context.f16475 != 0) {
                byte[] ensureBufferSize2 = ensureBufferSize(this.decodeSize, context);
                switch (context.f16475) {
                    case 1:
                        return;
                    case 2:
                        context.f16480 >>= 4;
                        int i10 = context.f16478;
                        context.f16478 = i10 + 1;
                        ensureBufferSize2[i10] = (byte) (context.f16480 & 255);
                        return;
                    case 3:
                        context.f16480 >>= 2;
                        int i11 = context.f16478;
                        context.f16478 = i11 + 1;
                        ensureBufferSize2[i11] = (byte) ((context.f16480 >> 8) & 255);
                        int i12 = context.f16478;
                        context.f16478 = i12 + 1;
                        ensureBufferSize2[i12] = (byte) (context.f16480 & 255);
                        return;
                    default:
                        throw new IllegalStateException("Impossible modulus " + context.f16475);
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void encode(byte[] r11, int r12, int r13, org.apache.commons.codec.binary.BaseNCodec.Context r14) {
        /*
            r10 = this;
            r9 = 61
            r8 = 0
            boolean r5 = r14.f16473
            if (r5 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r13 >= 0) goto L_0x00e1
            r5 = 1
            r14.f16473 = r5
            int r5 = r14.f16475
            if (r5 != 0) goto L_0x0015
            int r5 = r10.lineLength
            if (r5 == 0) goto L_0x0007
        L_0x0015:
            int r5 = r10.encodeSize
            byte[] r1 = r10.ensureBufferSize(r5, r14)
            int r4 = r14.f16478
            int r5 = r14.f16475
            switch(r5) {
                case 0: goto L_0x0078;
                case 1: goto L_0x003e;
                case 2: goto L_0x009c;
                default: goto L_0x0022;
            }
        L_0x0022:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Impossible modulus "
            java.lang.StringBuilder r6 = r6.append(r7)
            int r7 = r14.f16475
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.<init>(r6)
            throw r5
        L_0x003e:
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 >> 2
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 << 4
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            byte[] r5 = r10.encodeTable
            byte[] r6 = STANDARD_ENCODE_TABLE
            if (r5 != r6) goto L_0x0078
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            r1[r5] = r9
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            r1[r5] = r9
        L_0x0078:
            int r5 = r14.f16474
            int r6 = r14.f16478
            int r6 = r6 - r4
            int r5 = r5 + r6
            r14.f16474 = r5
            int r5 = r10.lineLength
            if (r5 <= 0) goto L_0x0007
            int r5 = r14.f16474
            if (r5 <= 0) goto L_0x0007
            byte[] r5 = r10.lineSeparator
            int r6 = r14.f16478
            byte[] r7 = r10.lineSeparator
            int r7 = r7.length
            java.lang.System.arraycopy(r5, r8, r1, r6, r7)
            int r5 = r14.f16478
            byte[] r6 = r10.lineSeparator
            int r6 = r6.length
            int r5 = r5 + r6
            r14.f16478 = r5
            goto L_0x0007
        L_0x009c:
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 >> 10
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 >> 4
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 << 2
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            byte[] r5 = r10.encodeTable
            byte[] r6 = STANDARD_ENCODE_TABLE
            if (r5 != r6) goto L_0x0078
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            r1[r5] = r9
            goto L_0x0078
        L_0x00e1:
            r2 = 0
            r3 = r12
        L_0x00e3:
            if (r2 >= r13) goto L_0x0175
            int r5 = r10.encodeSize
            byte[] r1 = r10.ensureBufferSize(r5, r14)
            int r5 = r14.f16475
            int r5 = r5 + 1
            int r5 = r5 % 3
            r14.f16475 = r5
            int r12 = r3 + 1
            byte r0 = r11[r3]
            if (r0 >= 0) goto L_0x00fb
            int r0 = r0 + 256
        L_0x00fb:
            int r5 = r14.f16480
            int r5 = r5 << 8
            int r5 = r5 + r0
            r14.f16480 = r5
            int r5 = r14.f16475
            if (r5 != 0) goto L_0x0170
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 >> 18
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 >> 12
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            int r7 = r7 >> 6
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            int r5 = r14.f16478
            int r6 = r5 + 1
            r14.f16478 = r6
            byte[] r6 = r10.encodeTable
            int r7 = r14.f16480
            r7 = r7 & 63
            byte r6 = r6[r7]
            r1[r5] = r6
            int r5 = r14.f16474
            int r5 = r5 + 4
            r14.f16474 = r5
            int r5 = r10.lineLength
            if (r5 <= 0) goto L_0x0170
            int r5 = r10.lineLength
            int r6 = r14.f16474
            if (r5 > r6) goto L_0x0170
            byte[] r5 = r10.lineSeparator
            int r6 = r14.f16478
            byte[] r7 = r10.lineSeparator
            int r7 = r7.length
            java.lang.System.arraycopy(r5, r8, r1, r6, r7)
            int r5 = r14.f16478
            byte[] r6 = r10.lineSeparator
            int r6 = r6.length
            int r5 = r5 + r6
            r14.f16478 = r5
            r14.f16474 = r8
        L_0x0170:
            int r2 = r2 + 1
            r3 = r12
            goto L_0x00e3
        L_0x0175:
            r12 = r3
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.codec.binary.Base64.encode(byte[], int, int, org.apache.commons.codec.binary.BaseNCodec$Context):void");
    }

    /* access modifiers changed from: protected */
    public boolean isInAlphabet(byte b) {
        return b >= 0 && b < this.decodeTable.length && this.decodeTable[b] != -1;
    }

    public boolean isUrlSafe() {
        return this.encodeTable == URL_SAFE_ENCODE_TABLE;
    }
}
