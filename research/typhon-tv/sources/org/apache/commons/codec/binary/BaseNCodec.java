package org.apache.commons.codec.binary;

import java.util.Arrays;
import org.apache.commons.codec.BinaryDecoder;
import org.apache.commons.codec.BinaryEncoder;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;

public abstract class BaseNCodec implements BinaryDecoder, BinaryEncoder {
    private static final int DEFAULT_BUFFER_RESIZE_FACTOR = 2;
    private static final int DEFAULT_BUFFER_SIZE = 8192;
    static final int EOF = -1;
    protected static final int MASK_8BITS = 255;
    public static final int MIME_CHUNK_SIZE = 76;
    protected static final byte PAD_DEFAULT = 61;
    public static final int PEM_CHUNK_SIZE = 64;
    protected final byte PAD = PAD_DEFAULT;
    private final int chunkSeparatorLength;
    private final int encodedBlockSize;
    protected final int lineLength;
    private final int unencodedBlockSize;

    /* access modifiers changed from: package-private */
    public abstract void decode(byte[] bArr, int i, int i2, Context context);

    /* access modifiers changed from: package-private */
    public abstract void encode(byte[] bArr, int i, int i2, Context context);

    /* access modifiers changed from: protected */
    public abstract boolean isInAlphabet(byte b);

    static class Context {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f16473;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f16474;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f16475;

        /* renamed from: 连任  reason: contains not printable characters */
        int f16476;

        /* renamed from: 靐  reason: contains not printable characters */
        long f16477;

        /* renamed from: 麤  reason: contains not printable characters */
        int f16478;

        /* renamed from: 齉  reason: contains not printable characters */
        byte[] f16479;

        /* renamed from: 龘  reason: contains not printable characters */
        int f16480;

        Context() {
        }

        public String toString() {
            return String.format("%s[buffer=%s, currentLinePos=%s, eof=%s, ibitWorkArea=%s, lbitWorkArea=%s, modulus=%s, pos=%s, readPos=%s]", new Object[]{getClass().getSimpleName(), Arrays.toString(this.f16479), Integer.valueOf(this.f16474), Boolean.valueOf(this.f16473), Integer.valueOf(this.f16480), Long.valueOf(this.f16477), Integer.valueOf(this.f16475), Integer.valueOf(this.f16478), Integer.valueOf(this.f16476)});
        }
    }

    protected BaseNCodec(int unencodedBlockSize2, int encodedBlockSize2, int lineLength2, int chunkSeparatorLength2) {
        boolean useChunking;
        int i = 0;
        this.unencodedBlockSize = unencodedBlockSize2;
        this.encodedBlockSize = encodedBlockSize2;
        if (lineLength2 <= 0 || chunkSeparatorLength2 <= 0) {
            useChunking = false;
        } else {
            useChunking = true;
        }
        this.lineLength = useChunking ? (lineLength2 / encodedBlockSize2) * encodedBlockSize2 : i;
        this.chunkSeparatorLength = chunkSeparatorLength2;
    }

    /* access modifiers changed from: package-private */
    public boolean hasData(Context context) {
        return context.f16479 != null;
    }

    /* access modifiers changed from: package-private */
    public int available(Context context) {
        if (context.f16479 != null) {
            return context.f16478 - context.f16476;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getDefaultBufferSize() {
        return 8192;
    }

    private byte[] resizeBuffer(Context context) {
        if (context.f16479 == null) {
            context.f16479 = new byte[getDefaultBufferSize()];
            context.f16478 = 0;
            context.f16476 = 0;
        } else {
            byte[] b = new byte[(context.f16479.length * 2)];
            System.arraycopy(context.f16479, 0, b, 0, context.f16479.length);
            context.f16479 = b;
        }
        return context.f16479;
    }

    /* access modifiers changed from: protected */
    public byte[] ensureBufferSize(int size, Context context) {
        if (context.f16479 == null || context.f16479.length < context.f16478 + size) {
            return resizeBuffer(context);
        }
        return context.f16479;
    }

    /* access modifiers changed from: package-private */
    public int readResults(byte[] b, int bPos, int bAvail, Context context) {
        if (context.f16479 != null) {
            int len = Math.min(available(context), bAvail);
            System.arraycopy(context.f16479, context.f16476, b, bPos, len);
            context.f16476 += len;
            if (context.f16476 < context.f16478) {
                return len;
            }
            context.f16479 = null;
            return len;
        }
        return context.f16473 ? -1 : 0;
    }

    protected static boolean isWhiteSpace(byte byteToCheck) {
        switch (byteToCheck) {
            case 9:
            case 10:
            case 13:
            case 32:
                return true;
            default:
                return false;
        }
    }

    public Object encode(Object obj) throws EncoderException {
        if (obj instanceof byte[]) {
            return encode((byte[]) (byte[]) obj);
        }
        throw new EncoderException("Parameter supplied to Base-N encode is not a byte[]");
    }

    public String encodeToString(byte[] pArray) {
        return StringUtils.newStringUtf8(encode(pArray));
    }

    public String encodeAsString(byte[] pArray) {
        return StringUtils.newStringUtf8(encode(pArray));
    }

    public Object decode(Object obj) throws DecoderException {
        if (obj instanceof byte[]) {
            return decode((byte[]) (byte[]) obj);
        }
        if (obj instanceof String) {
            return decode((String) obj);
        }
        throw new DecoderException("Parameter supplied to Base-N decode is not a byte[] or a String");
    }

    public byte[] decode(String pArray) {
        return decode(StringUtils.getBytesUtf8(pArray));
    }

    public byte[] decode(byte[] pArray) {
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        Context context = new Context();
        decode(pArray, 0, pArray.length, context);
        decode(pArray, 0, -1, context);
        byte[] result = new byte[context.f16478];
        readResults(result, 0, result.length, context);
        return result;
    }

    public byte[] encode(byte[] pArray) {
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        Context context = new Context();
        encode(pArray, 0, pArray.length, context);
        encode(pArray, 0, -1, context);
        byte[] buf = new byte[(context.f16478 - context.f16476)];
        readResults(buf, 0, buf.length, context);
        return buf;
    }

    public boolean isInAlphabet(byte[] arrayOctet, boolean allowWSPad) {
        for (int i = 0; i < arrayOctet.length; i++) {
            if (!isInAlphabet(arrayOctet[i]) && (!allowWSPad || (arrayOctet[i] != 61 && !isWhiteSpace(arrayOctet[i])))) {
                return false;
            }
        }
        return true;
    }

    public boolean isInAlphabet(String basen) {
        return isInAlphabet(StringUtils.getBytesUtf8(basen), true);
    }

    /* access modifiers changed from: protected */
    public boolean containsAlphabetOrPad(byte[] arrayOctet) {
        if (arrayOctet == null) {
            return false;
        }
        for (byte element : arrayOctet) {
            if (61 == element || isInAlphabet(element)) {
                return true;
            }
        }
        return false;
    }

    public long getEncodedLength(byte[] pArray) {
        long len = ((long) (((pArray.length + this.unencodedBlockSize) - 1) / this.unencodedBlockSize)) * ((long) this.encodedBlockSize);
        if (this.lineLength > 0) {
            return len + ((((((long) this.lineLength) + len) - 1) / ((long) this.lineLength)) * ((long) this.chunkSeparatorLength));
        }
        return len;
    }
}
