package p009if;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

/* renamed from: if.IoriYagami  reason: invalid package */
public final class IoriYagami implements Shermie {

    /* renamed from: 连任  reason: contains not printable characters */
    private final CRC32 f15149 = new CRC32();

    /* renamed from: 靐  reason: contains not printable characters */
    private final ChangKoehan f15150;

    /* renamed from: 麤  reason: contains not printable characters */
    private final SaishuKusanagi f15151;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Inflater f15152;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f15153 = 0;

    public IoriYagami(Shermie shermie) {
        if (shermie == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f15152 = new Inflater(true);
        this.f15150 = LeonaHeidern.m18966(shermie);
        this.f15151 = new SaishuKusanagi(this.f15150, this.f15152);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m18954() throws IOException {
        this.f15150.m6711(10);
        byte r7 = this.f15150.m6703().m6724(3);
        boolean z = ((r7 >> 1) & 1) == 1;
        if (z) {
            m18956(this.f15150.m6703(), 0, 10);
        }
        m18957("ID1ID2", 8075, (int) this.f15150.m6702());
        this.f15150.m6704(8);
        if (((r7 >> 2) & 1) == 1) {
            this.f15150.m6711(2);
            if (z) {
                m18956(this.f15150.m6703(), 0, 2);
            }
            short r8 = this.f15150.m6703().m6723();
            this.f15150.m6711((long) r8);
            if (z) {
                m18956(this.f15150.m6703(), 0, (long) r8);
            }
            this.f15150.m6704((long) r8);
        }
        if (((r7 >> 3) & 1) == 1) {
            long r82 = this.f15150.m6709((byte) 0);
            if (r82 == -1) {
                throw new EOFException();
            }
            if (z) {
                m18956(this.f15150.m6703(), 0, 1 + r82);
            }
            this.f15150.m6704(1 + r82);
        }
        if (((r7 >> 4) & 1) == 1) {
            long r83 = this.f15150.m6709((byte) 0);
            if (r83 == -1) {
                throw new EOFException();
            }
            if (z) {
                m18956(this.f15150.m6703(), 0, 1 + r83);
            }
            this.f15150.m6704(1 + r83);
        }
        if (z) {
            m18957("FHCRC", (int) this.f15150.m6695(), (int) (short) ((int) this.f15149.getValue()));
            this.f15149.reset();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m18955() throws IOException {
        m18957("CRC", this.f15150.m6697(), (int) this.f15149.getValue());
        m18957("ISIZE", this.f15150.m6697(), (int) this.f15152.getBytesWritten());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18956(GoroDaimon goroDaimon, long j, long j2) {
        ChizuruKagura chizuruKagura = goroDaimon.f6169;
        while (j >= ((long) (chizuruKagura.f6163 - chizuruKagura.f6161))) {
            j -= (long) (chizuruKagura.f6163 - chizuruKagura.f6161);
            chizuruKagura = chizuruKagura.f6158;
        }
        while (j2 > 0) {
            int i = (int) (((long) chizuruKagura.f6161) + j);
            int min = (int) Math.min((long) (chizuruKagura.f6163 - i), j2);
            this.f15149.update(chizuruKagura.f6164, i, min);
            j2 -= (long) min;
            chizuruKagura = chizuruKagura.f6158;
            j = 0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m18957(String str, int i, int i2) throws IOException {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}));
        }
    }

    public void close() throws IOException {
        this.f15151.close();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m18958(GoroDaimon goroDaimon, long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j == 0) {
            return 0;
        } else {
            if (this.f15153 == 0) {
                m18954();
                this.f15153 = 1;
            }
            if (this.f15153 == 1) {
                long j2 = goroDaimon.f6168;
                long r4 = this.f15151.m19007(goroDaimon, j);
                if (r4 != -1) {
                    m18956(goroDaimon, j2, r4);
                    return r4;
                }
                this.f15153 = 2;
            }
            if (this.f15153 == 2) {
                m18955();
                this.f15153 = 3;
                if (!this.f15150.m6708()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18959() {
        return this.f15150.m19010();
    }
}
