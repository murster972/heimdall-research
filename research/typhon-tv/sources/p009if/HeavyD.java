package p009if;

import java.io.IOException;
import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

/* renamed from: if.HeavyD  reason: invalid package */
public final class HeavyD implements YashiroNanakase {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Deflater f15146;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f15147;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ChinGentsai f15148;

    HeavyD(ChinGentsai chinGentsai, Deflater deflater) {
        if (chinGentsai == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f15148 = chinGentsai;
            this.f15146 = deflater;
        }
    }

    @IgnoreJRERequirement
    /* renamed from: 龘  reason: contains not printable characters */
    private void m18951(boolean z) throws IOException {
        ChizuruKagura r2;
        GoroDaimon r1 = this.f15148.m18930();
        while (true) {
            r2 = r1.m6743(1);
            int deflate = z ? this.f15146.deflate(r2.f6164, r2.f6163, 8192 - r2.f6163, 2) : this.f15146.deflate(r2.f6164, r2.f6163, 8192 - r2.f6163);
            if (deflate > 0) {
                r2.f6163 += deflate;
                r1.f6168 += (long) deflate;
                this.f15148.m18926();
            } else if (this.f15146.needsInput()) {
                break;
            }
        }
        if (r2.f6161 == r2.f6163) {
            r1.f6169 = r2.m6715();
            Goenitz.m6720(r2);
        }
    }

    public void a_(GoroDaimon goroDaimon, long j) throws IOException {
        Orochi.m18998(goroDaimon.f6168, 0, j);
        while (j > 0) {
            ChizuruKagura chizuruKagura = goroDaimon.f6169;
            int min = (int) Math.min(j, (long) (chizuruKagura.f6163 - chizuruKagura.f6161));
            this.f15146.setInput(chizuruKagura.f6164, chizuruKagura.f6161, min);
            m18951(false);
            goroDaimon.f6168 -= (long) min;
            chizuruKagura.f6161 += min;
            if (chizuruKagura.f6161 == chizuruKagura.f6163) {
                goroDaimon.f6169 = chizuruKagura.m6715();
                Goenitz.m6720(chizuruKagura);
            }
            j -= (long) min;
        }
    }

    public void close() throws IOException {
        if (!this.f15147) {
            Throwable th = null;
            try {
                m18952();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f15146.end();
                th = th;
            } catch (Throwable th3) {
                th = th3;
                if (th != null) {
                    th = th;
                }
            }
            try {
                this.f15148.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.f15147 = true;
            if (th != null) {
                Orochi.m18999(th);
            }
        }
    }

    public void flush() throws IOException {
        m18951(true);
        this.f15148.flush();
    }

    public String toString() {
        return "DeflaterSink(" + this.f15148 + ")";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m18952() throws IOException {
        this.f15146.finish();
        m18951(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18953() {
        return this.f15148.m19014();
    }
}
