package p009if;

import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Deflater;

/* renamed from: if.RugalBernstein  reason: invalid package */
public final class RugalBernstein implements YashiroNanakase {

    /* renamed from: 连任  reason: contains not printable characters */
    private final CRC32 f15169 = new CRC32();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Deflater f15170;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f15171;

    /* renamed from: 齉  reason: contains not printable characters */
    private final HeavyD f15172;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ChinGentsai f15173;

    public RugalBernstein(YashiroNanakase yashiroNanakase) {
        if (yashiroNanakase == null) {
            throw new IllegalArgumentException("sink == null");
        }
        this.f15170 = new Deflater(-1, true);
        this.f15173 = LeonaHeidern.m18967(yashiroNanakase);
        this.f15172 = new HeavyD(this.f15173, this.f15170);
        m19001();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19001() {
        GoroDaimon r0 = this.f15173.m18930();
        r0.m6753(8075);
        r0.m6770(8);
        r0.m6770(0);
        r0.m6761(0);
        r0.m6770(0);
        r0.m6770(0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19002(GoroDaimon goroDaimon, long j) {
        ChizuruKagura chizuruKagura = goroDaimon.f6169;
        while (j > 0) {
            int min = (int) Math.min(j, (long) (chizuruKagura.f6163 - chizuruKagura.f6161));
            this.f15169.update(chizuruKagura.f6164, chizuruKagura.f6161, min);
            j -= (long) min;
            chizuruKagura = chizuruKagura.f6158;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m19003() throws IOException {
        this.f15173.m18931((int) this.f15169.getValue());
        this.f15173.m18931((int) this.f15170.getBytesRead());
    }

    public void a_(GoroDaimon goroDaimon, long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j != 0) {
            m19002(goroDaimon, j);
            this.f15172.a_(goroDaimon, j);
        }
    }

    public void close() throws IOException {
        if (!this.f15171) {
            Throwable th = null;
            try {
                this.f15172.m18952();
                m19003();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f15170.end();
                th = th;
            } catch (Throwable th3) {
                th = th3;
                if (th != null) {
                    th = th;
                }
            }
            try {
                this.f15173.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.f15171 = true;
            if (th != null) {
                Orochi.m18999(th);
            }
        }
    }

    public void flush() throws IOException {
        this.f15172.flush();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m19004() {
        return this.f15173.m19014();
    }
}
