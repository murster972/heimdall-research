package p009if;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

/* renamed from: if.YashiroNanakase  reason: invalid package */
public interface YashiroNanakase extends Closeable, Flushable {
    void a_(GoroDaimon goroDaimon, long j) throws IOException;

    void close() throws IOException;

    void flush() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    Chris m19014();
}
