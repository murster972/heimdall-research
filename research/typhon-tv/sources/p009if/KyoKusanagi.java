package p009if;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.C;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

/* renamed from: if.KyoKusanagi  reason: invalid package */
public class KyoKusanagi extends Chris {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final long f6170 = TimeUnit.MILLISECONDS.toNanos(f6172);
    @Nullable

    /* renamed from: 齉  reason: contains not printable characters */
    static KyoKusanagi f6171;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final long f6172 = TimeUnit.SECONDS.toMillis(60);
    @Nullable

    /* renamed from: ʻ  reason: contains not printable characters */
    private KyoKusanagi f6173;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f6174;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f6175;

    /* renamed from: if.KyoKusanagi$KyoKusanagi  reason: collision with other inner class name */
    private static final class C0037KyoKusanagi extends Thread {
        C0037KyoKusanagi() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            r0.m6795();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
            L_0x0000:
                java.lang.Class<if.KyoKusanagi> r1 = p009if.KyoKusanagi.class
                monitor-enter(r1)     // Catch:{ InterruptedException -> 0x000e }
                if.KyoKusanagi r0 = p009if.KyoKusanagi.m6785()     // Catch:{ all -> 0x000b }
                if (r0 != 0) goto L_0x0010
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                goto L_0x0000
            L_0x000b:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                throw r0     // Catch:{ InterruptedException -> 0x000e }
            L_0x000e:
                r0 = move-exception
                goto L_0x0000
            L_0x0010:
                if.KyoKusanagi r2 = p009if.KyoKusanagi.f6171     // Catch:{ all -> 0x000b }
                if (r0 != r2) goto L_0x0019
                r0 = 0
                p009if.KyoKusanagi.f6171 = r0     // Catch:{ all -> 0x000b }
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                return
            L_0x0019:
                monitor-exit(r1)     // Catch:{ all -> 0x000b }
                r0.m6795()     // Catch:{ InterruptedException -> 0x000e }
                goto L_0x0000
            */
            throw new UnsupportedOperationException("Method not decompiled: p009if.KyoKusanagi.C0037KyoKusanagi.run():void");
        }
    }

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    static KyoKusanagi m6785() throws InterruptedException {
        KyoKusanagi kyoKusanagi = f6171.f6173;
        if (kyoKusanagi == null) {
            long nanoTime = System.nanoTime();
            KyoKusanagi.class.wait(f6172);
            if (f6171.f6173 != null || System.nanoTime() - nanoTime < f6170) {
                return null;
            }
            return f6171;
        }
        long r2 = kyoKusanagi.m6786(System.nanoTime());
        if (r2 > 0) {
            long j = r2 / C.MICROS_PER_SECOND;
            KyoKusanagi.class.wait(j, (int) (r2 - (C.MICROS_PER_SECOND * j)));
            return null;
        }
        f6171.f6173 = kyoKusanagi.f6173;
        kyoKusanagi.f6173 = null;
        return kyoKusanagi;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private long m6786(long j) {
        return this.f6174 - j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized void m6787(KyoKusanagi kyoKusanagi, long j, boolean z) {
        synchronized (KyoKusanagi.class) {
            if (f6171 == null) {
                f6171 = new KyoKusanagi();
                new C0037KyoKusanagi().start();
            }
            long nanoTime = System.nanoTime();
            if (j != 0 && z) {
                kyoKusanagi.f6174 = Math.min(j, kyoKusanagi.m18944() - nanoTime) + nanoTime;
            } else if (j != 0) {
                kyoKusanagi.f6174 = nanoTime + j;
            } else if (z) {
                kyoKusanagi.f6174 = kyoKusanagi.m18944();
            } else {
                throw new AssertionError();
            }
            long r4 = kyoKusanagi.m6786(nanoTime);
            KyoKusanagi kyoKusanagi2 = f6171;
            while (kyoKusanagi2.f6173 != null && r4 >= kyoKusanagi2.f6173.m6786(nanoTime)) {
                kyoKusanagi2 = kyoKusanagi2.f6173;
            }
            kyoKusanagi.f6173 = kyoKusanagi2.f6173;
            kyoKusanagi2.f6173 = kyoKusanagi;
            if (kyoKusanagi2 == f6171) {
                KyoKusanagi.class.notify();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized boolean m6788(KyoKusanagi kyoKusanagi) {
        boolean z;
        synchronized (KyoKusanagi.class) {
            KyoKusanagi kyoKusanagi2 = f6171;
            while (true) {
                if (kyoKusanagi2 == null) {
                    z = true;
                    break;
                } else if (kyoKusanagi2.f6173 == kyoKusanagi) {
                    kyoKusanagi2.f6173 = kyoKusanagi.f6173;
                    kyoKusanagi.f6173 = null;
                    z = false;
                    break;
                } else {
                    kyoKusanagi2 = kyoKusanagi2.f6173;
                }
            }
        }
        return z;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final void m6789() {
        if (this.f6175) {
            throw new IllegalStateException("Unbalanced enter/exit");
        }
        long y_ = y_();
        boolean r2 = m18945();
        if (y_ != 0 || r2) {
            this.f6175 = true;
            m6787(this, y_, r2);
        }
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m6790() {
        if (!this.f6175) {
            return false;
        }
        this.f6175 = false;
        return m6788(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final IOException m6791(IOException iOException) throws IOException {
        return !m6790() ? iOException : m6794(iOException);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Shermie m6792(final Shermie shermie) {
        return new Shermie() {
            public void close() throws IOException {
                try {
                    shermie.close();
                    KyoKusanagi.this.m6796(true);
                } catch (IOException e) {
                    throw KyoKusanagi.this.m6791(e);
                } catch (Throwable th) {
                    KyoKusanagi.this.m6796(false);
                    throw th;
                }
            }

            public String toString() {
                return "AsyncTimeout.source(" + shermie + ")";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public long m18961(GoroDaimon goroDaimon, long j) throws IOException {
                KyoKusanagi.this.m6789();
                try {
                    long r0 = shermie.m19009(goroDaimon, j);
                    KyoKusanagi.this.m6796(true);
                    return r0;
                } catch (IOException e) {
                    throw KyoKusanagi.this.m6791(e);
                } catch (Throwable th) {
                    KyoKusanagi.this.m6796(false);
                    throw th;
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Chris m18962() {
                return KyoKusanagi.this;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final YashiroNanakase m6793(final YashiroNanakase yashiroNanakase) {
        return new YashiroNanakase() {
            public void a_(GoroDaimon goroDaimon, long j) throws IOException {
                Orochi.m18998(goroDaimon.f6168, 0, j);
                long j2 = j;
                while (j2 > 0) {
                    ChizuruKagura chizuruKagura = goroDaimon.f6169;
                    long j3 = 0;
                    while (true) {
                        if (j3 >= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                            break;
                        }
                        j3 += (long) (goroDaimon.f6169.f6163 - goroDaimon.f6169.f6161);
                        if (j3 >= j2) {
                            j3 = j2;
                            break;
                        }
                        chizuruKagura = chizuruKagura.f6158;
                    }
                    KyoKusanagi.this.m6789();
                    try {
                        yashiroNanakase.a_(goroDaimon, j3);
                        j2 -= j3;
                        KyoKusanagi.this.m6796(true);
                    } catch (IOException e) {
                        throw KyoKusanagi.this.m6791(e);
                    } catch (Throwable th) {
                        KyoKusanagi.this.m6796(false);
                        throw th;
                    }
                }
            }

            public void close() throws IOException {
                KyoKusanagi.this.m6789();
                try {
                    yashiroNanakase.close();
                    KyoKusanagi.this.m6796(true);
                } catch (IOException e) {
                    throw KyoKusanagi.this.m6791(e);
                } catch (Throwable th) {
                    KyoKusanagi.this.m6796(false);
                    throw th;
                }
            }

            public void flush() throws IOException {
                KyoKusanagi.this.m6789();
                try {
                    yashiroNanakase.flush();
                    KyoKusanagi.this.m6796(true);
                } catch (IOException e) {
                    throw KyoKusanagi.this.m6791(e);
                } catch (Throwable th) {
                    KyoKusanagi.this.m6796(false);
                    throw th;
                }
            }

            public String toString() {
                return "AsyncTimeout.sink(" + yashiroNanakase + ")";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Chris m18960() {
                return KyoKusanagi.this;
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public IOException m6794(@Nullable IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m6795() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m6796(boolean z) throws IOException {
        if (m6790() && z) {
            throw m6794((IOException) null);
        }
    }
}
