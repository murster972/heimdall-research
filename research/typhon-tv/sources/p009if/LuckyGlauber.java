package p009if;

import java.io.IOException;

/* renamed from: if.LuckyGlauber  reason: invalid package */
public abstract class LuckyGlauber implements Shermie {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Shermie f15164;

    public LuckyGlauber(Shermie shermie) {
        if (shermie == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f15164 = shermie;
    }

    public void close() throws IOException {
        this.f15164.close();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f15164.toString() + ")";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m18980(GoroDaimon goroDaimon, long j) throws IOException {
        return this.f15164.m19009(goroDaimon, j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18981() {
        return this.f15164.m19010();
    }
}
