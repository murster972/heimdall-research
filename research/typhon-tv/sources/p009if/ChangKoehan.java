package p009if;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

/* renamed from: if.ChangKoehan  reason: invalid package */
public interface ChangKoehan extends Shermie {
    /* renamed from: ʻ  reason: contains not printable characters */
    short m6695() throws IOException;

    /* renamed from: ʼ  reason: contains not printable characters */
    int m6696() throws IOException;

    /* renamed from: ʽ  reason: contains not printable characters */
    int m6697() throws IOException;

    /* renamed from: ˈ  reason: contains not printable characters */
    InputStream m6698();

    /* renamed from: ˑ  reason: contains not printable characters */
    long m6699() throws IOException;

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    String m6700() throws IOException;

    /* renamed from: ᐧ  reason: contains not printable characters */
    String m6701() throws IOException;

    /* renamed from: 连任  reason: contains not printable characters */
    short m6702() throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    GoroDaimon m6703();

    /* renamed from: 靐  reason: contains not printable characters */
    void m6704(long j) throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    byte m6705() throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    byte[] m6706(long j) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    ChoiBounge m6707(long j) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean m6708() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    long m6709(byte b) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m6710(Charset charset) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m6711(long j) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m6712(byte[] bArr) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m6713(long j, ChoiBounge choiBounge) throws IOException;
}
