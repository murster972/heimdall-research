package p009if;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;

/* renamed from: if.LeonaHeidern  reason: invalid package */
public final class LeonaHeidern {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Logger f15158 = Logger.getLogger(LeonaHeidern.class.getName());

    private LeonaHeidern() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Shermie m18963(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        KyoKusanagi r0 = m18965(socket);
        return r0.m6792(m18970(socket.getInputStream(), (Chris) r0));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static YashiroNanakase m18964(File file) throws FileNotFoundException {
        if (file != null) {
            return m18971((OutputStream) new FileOutputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static KyoKusanagi m18965(final Socket socket) {
        return new KyoKusanagi() {
            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public IOException m18978(@Nullable IOException iOException) {
                SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
                if (iOException != null) {
                    socketTimeoutException.initCause(iOException);
                }
                return socketTimeoutException;
            }

            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m18979() {
                try {
                    socket.close();
                } catch (Exception e) {
                    LeonaHeidern.f15158.log(Level.WARNING, "Failed to close timed out socket " + socket, e);
                } catch (AssertionError e2) {
                    if (LeonaHeidern.m18974(e2)) {
                        LeonaHeidern.f15158.log(Level.WARNING, "Failed to close timed out socket " + socket, e2);
                        return;
                    }
                    throw e2;
                }
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ChangKoehan m18966(Shermie shermie) {
        return new Vice(shermie);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ChinGentsai m18967(YashiroNanakase yashiroNanakase) {
        return new Mature(yashiroNanakase);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Shermie m18968(File file) throws FileNotFoundException {
        if (file != null) {
            return m18969((InputStream) new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Shermie m18969(InputStream inputStream) {
        return m18970(inputStream, new Chris());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Shermie m18970(final InputStream inputStream, final Chris chris) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (chris != null) {
            return new Shermie() {
                public void close() throws IOException {
                    inputStream.close();
                }

                public String toString() {
                    return "source(" + inputStream + ")";
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public long m18976(GoroDaimon goroDaimon, long j) throws IOException {
                    if (j < 0) {
                        throw new IllegalArgumentException("byteCount < 0: " + j);
                    } else if (j == 0) {
                        return 0;
                    } else {
                        try {
                            Chris.this.m18942();
                            ChizuruKagura r0 = goroDaimon.m6743(1);
                            int read = inputStream.read(r0.f6164, r0.f6163, (int) Math.min(j, (long) (8192 - r0.f6163)));
                            if (read == -1) {
                                return -1;
                            }
                            r0.f6163 += read;
                            goroDaimon.f6168 += (long) read;
                            return (long) read;
                        } catch (AssertionError e) {
                            if (LeonaHeidern.m18974(e)) {
                                throw new IOException(e);
                            }
                            throw e;
                        }
                    }
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public Chris m18977() {
                    return Chris.this;
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static YashiroNanakase m18971(OutputStream outputStream) {
        return m18972(outputStream, new Chris());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static YashiroNanakase m18972(final OutputStream outputStream, final Chris chris) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (chris != null) {
            return new YashiroNanakase() {
                public void a_(GoroDaimon goroDaimon, long j) throws IOException {
                    Orochi.m18998(goroDaimon.f6168, 0, j);
                    while (j > 0) {
                        Chris.this.m18942();
                        ChizuruKagura chizuruKagura = goroDaimon.f6169;
                        int min = (int) Math.min(j, (long) (chizuruKagura.f6163 - chizuruKagura.f6161));
                        outputStream.write(chizuruKagura.f6164, chizuruKagura.f6161, min);
                        chizuruKagura.f6161 += min;
                        j -= (long) min;
                        goroDaimon.f6168 -= (long) min;
                        if (chizuruKagura.f6161 == chizuruKagura.f6163) {
                            goroDaimon.f6169 = chizuruKagura.m6715();
                            Goenitz.m6720(chizuruKagura);
                        }
                    }
                }

                public void close() throws IOException {
                    outputStream.close();
                }

                public void flush() throws IOException {
                    outputStream.flush();
                }

                public String toString() {
                    return "sink(" + outputStream + ")";
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public Chris m18975() {
                    return Chris.this;
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static YashiroNanakase m18973(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        KyoKusanagi r0 = m18965(socket);
        return r0.m6793(m18972(socket.getOutputStream(), (Chris) r0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m18974(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
