package p009if;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

/* renamed from: if.Vice  reason: invalid package */
final class Vice implements ChangKoehan {

    /* renamed from: 靐  reason: contains not printable characters */
    public final Shermie f6176;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f6177;

    /* renamed from: 龘  reason: contains not printable characters */
    public final GoroDaimon f6178 = new GoroDaimon();

    Vice(Shermie shermie) {
        if (shermie == null) {
            throw new NullPointerException("source == null");
        }
        this.f6176 = shermie;
    }

    public void close() throws IOException {
        if (!this.f6177) {
            this.f6177 = true;
            this.f6176.close();
            this.f6178.m6732();
        }
    }

    public String toString() {
        return "buffer(" + this.f6176 + ")";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m6797(long j) throws IOException {
        m6819(j);
        return this.f6178.m6729(j);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public short m6798() throws IOException {
        m6819(2);
        return this.f6178.m6723();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6799() throws IOException {
        m6819(4);
        return this.f6178.m6725();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m6800(long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("limit < 0: " + j);
        }
        long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
        long r0 = m6815((byte) 10, 0, j2);
        if (r0 != -1) {
            return this.f6178.m6745(r0);
        }
        if (j2 < Long.MAX_VALUE && m6807(j2) && this.f6178.m6724(j2 - 1) == 13 && m6807(1 + j2) && this.f6178.m6724(j2) == 10) {
            return this.f6178.m6745(j2);
        }
        GoroDaimon goroDaimon = new GoroDaimon();
        this.f6178.m6775(goroDaimon, 0, Math.min(32, this.f6178.m6731()));
        throw new EOFException("\\n not found: limit=" + Math.min(this.f6178.m6731(), j) + " content=" + goroDaimon.m6736().e() + 8230);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6801() throws IOException {
        m6819(4);
        return this.f6178.m6727();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public InputStream m6802() {
        return new InputStream() {
            public int available() throws IOException {
                if (!Vice.this.f6177) {
                    return (int) Math.min(Vice.this.f6178.f6168, 2147483647L);
                }
                throw new IOException("closed");
            }

            public void close() throws IOException {
                Vice.this.close();
            }

            public int read() throws IOException {
                if (Vice.this.f6177) {
                    throw new IOException("closed");
                } else if (Vice.this.f6178.f6168 == 0 && Vice.this.f6176.m19009(Vice.this.f6178, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return -1;
                } else {
                    return Vice.this.f6178.m6758() & 255;
                }
            }

            public int read(byte[] bArr, int i, int i2) throws IOException {
                if (Vice.this.f6177) {
                    throw new IOException("closed");
                }
                Orochi.m18998((long) bArr.length, (long) i, (long) i2);
                if (Vice.this.f6178.f6168 == 0 && Vice.this.f6176.m19009(Vice.this.f6178, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return -1;
                }
                return Vice.this.f6178.m6752(bArr, i, i2);
            }

            public String toString() {
                return Vice.this + ".inputStream()";
            }
        };
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public long m6803() throws IOException {
        m6819(1);
        int i = 0;
        while (true) {
            if (!m6807((long) (i + 1))) {
                break;
            }
            byte r2 = this.f6178.m6724((long) i);
            if ((r2 >= 48 && r2 <= 57) || ((r2 >= 97 && r2 <= 102) || (r2 >= 65 && r2 <= 70))) {
                i++;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", new Object[]{Byte.valueOf(r2)}));
            }
        }
        return this.f6178.m6740();
    }

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    public String m6804() throws IOException {
        long r0 = m6814((byte) 10);
        if (r0 != -1) {
            return this.f6178.m6745(r0);
        }
        if (this.f6178.f6168 != 0) {
            return m6797(this.f6178.f6168);
        }
        return null;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String m6805() throws IOException {
        return m6800(Long.MAX_VALUE);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public short m6806() throws IOException {
        m6819(2);
        return this.f6178.m6751();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m6807(long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f6177) {
            throw new IllegalStateException("closed");
        } else {
            while (this.f6178.f6168 < j) {
                if (this.f6176.m19009(this.f6178, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return false;
                }
            }
            return true;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m6808() {
        return this.f6178;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6809(long j) throws IOException {
        if (this.f6177) {
            throw new IllegalStateException("closed");
        }
        while (j > 0) {
            if (this.f6178.f6168 == 0 && this.f6176.m19009(this.f6178, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.f6178.m6731());
            this.f6178.m6757(min);
            j -= min;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public byte m6810() throws IOException {
        m6819(1);
        return this.f6178.m6758();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public byte[] m6811(long j) throws IOException {
        m6819(j);
        return this.f6178.m6760(j);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public ChoiBounge m6812(long j) throws IOException {
        m6819(j);
        return this.f6178.m6762(j);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m6813() throws IOException {
        if (!this.f6177) {
            return this.f6178.m6765() && this.f6176.m19009(this.f6178, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1;
        }
        throw new IllegalStateException("closed");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m6814(byte b) throws IOException {
        return m6815(b, 0, Long.MAX_VALUE);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m6815(byte b, long j, long j2) throws IOException {
        if (this.f6177) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(j), Long.valueOf(j2)}));
        } else {
            long j3 = j;
            while (j3 < j2) {
                long r0 = this.f6178.m6767(b, j3, j2);
                if (r0 != -1) {
                    return r0;
                }
                long j4 = this.f6178.f6168;
                if (j4 >= j2 || this.f6176.m19009(this.f6178, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return -1;
                }
                j3 = Math.max(j3, j4);
            }
            return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m6816(GoroDaimon goroDaimon, long j) throws IOException {
        if (goroDaimon == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f6177) {
            throw new IllegalStateException("closed");
        } else if (this.f6178.f6168 == 0 && this.f6176.m19009(this.f6178, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
            return -1;
        } else {
            return this.f6178.m6768(goroDaimon, Math.min(j, this.f6178.f6168));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m6817() {
        return this.f6176.m19010();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6818(Charset charset) throws IOException {
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        }
        this.f6178.m6769(this.f6176);
        return this.f6178.m6778(charset);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6819(long j) throws IOException {
        if (!m6807(j)) {
            throw new EOFException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6820(byte[] bArr) throws IOException {
        try {
            m6819((long) bArr.length);
            this.f6178.m6780(bArr);
        } catch (EOFException e) {
            EOFException eOFException = e;
            int i = 0;
            while (this.f6178.f6168 > 0) {
                int r2 = this.f6178.m6752(bArr, i, (int) this.f6178.f6168);
                if (r2 == -1) {
                    throw new AssertionError();
                }
                i += r2;
            }
            throw eOFException;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6821(long j, ChoiBounge choiBounge) throws IOException {
        return m6822(j, choiBounge, 0, choiBounge.g());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6822(long j, ChoiBounge choiBounge, int i, int i2) throws IOException {
        if (this.f6177) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || i < 0 || i2 < 0 || choiBounge.g() - i < i2) {
            return false;
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                long j2 = ((long) i3) + j;
                if (!m6807(1 + j2) || this.f6178.m6724(j2) != choiBounge.a(i + i3)) {
                    return false;
                }
            }
            return true;
        }
    }
}
