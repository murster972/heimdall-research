package p009if;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

/* renamed from: if.GoroDaimon  reason: invalid package */
public final class GoroDaimon implements ChangKoehan, ChinGentsai, Cloneable {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final byte[] f6167 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: 靐  reason: contains not printable characters */
    long f6168;
    @Nullable

    /* renamed from: 龘  reason: contains not printable characters */
    ChizuruKagura f6169;

    public void a_(GoroDaimon goroDaimon, long j) {
        if (goroDaimon == null) {
            throw new IllegalArgumentException("source == null");
        } else if (goroDaimon == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            Orochi.m18998(goroDaimon.f6168, 0, j);
            while (j > 0) {
                if (j < ((long) (goroDaimon.f6169.f6163 - goroDaimon.f6169.f6161))) {
                    ChizuruKagura chizuruKagura = this.f6169 != null ? this.f6169.f6159 : null;
                    if (chizuruKagura != null && chizuruKagura.f6160) {
                        if ((((long) chizuruKagura.f6163) + j) - ((long) (chizuruKagura.f6162 ? 0 : chizuruKagura.f6161)) <= PlaybackStateCompat.ACTION_PLAY_FROM_URI) {
                            goroDaimon.f6169.m6718(chizuruKagura, (int) j);
                            goroDaimon.f6168 -= j;
                            this.f6168 += j;
                            return;
                        }
                    }
                    goroDaimon.f6169 = goroDaimon.f6169.m6716((int) j);
                }
                ChizuruKagura chizuruKagura2 = goroDaimon.f6169;
                long j2 = (long) (chizuruKagura2.f6163 - chizuruKagura2.f6161);
                goroDaimon.f6169 = chizuruKagura2.m6715();
                if (this.f6169 == null) {
                    this.f6169 = chizuruKagura2;
                    ChizuruKagura chizuruKagura3 = this.f6169;
                    ChizuruKagura chizuruKagura4 = this.f6169;
                    ChizuruKagura chizuruKagura5 = this.f6169;
                    chizuruKagura4.f6159 = chizuruKagura5;
                    chizuruKagura3.f6158 = chizuruKagura5;
                } else {
                    this.f6169.f6159.m6717(chizuruKagura2).m6714();
                }
                goroDaimon.f6168 -= j2;
                this.f6168 += j2;
                j -= j2;
            }
        }
    }

    public void close() {
    }

    public boolean equals(Object obj) {
        long j = 0;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GoroDaimon)) {
            return false;
        }
        GoroDaimon goroDaimon = (GoroDaimon) obj;
        if (this.f6168 != goroDaimon.f6168) {
            return false;
        }
        if (this.f6168 == 0) {
            return true;
        }
        ChizuruKagura chizuruKagura = this.f6169;
        ChizuruKagura chizuruKagura2 = goroDaimon.f6169;
        int i = chizuruKagura.f6161;
        int i2 = chizuruKagura2.f6161;
        while (j < this.f6168) {
            long min = (long) Math.min(chizuruKagura.f6163 - i, chizuruKagura2.f6163 - i2);
            int i3 = 0;
            while (((long) i3) < min) {
                int i4 = i + 1;
                int i5 = i2 + 1;
                if (chizuruKagura.f6164[i] != chizuruKagura2.f6164[i2]) {
                    return false;
                }
                i3++;
                i2 = i5;
                i = i4;
            }
            if (i == chizuruKagura.f6163) {
                chizuruKagura = chizuruKagura.f6158;
                i = chizuruKagura.f6161;
            }
            if (i2 == chizuruKagura2.f6163) {
                chizuruKagura2 = chizuruKagura2.f6158;
                i2 = chizuruKagura2.f6161;
            }
            j += min;
        }
        return true;
    }

    public void flush() {
    }

    public int hashCode() {
        ChizuruKagura chizuruKagura = this.f6169;
        if (chizuruKagura == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = chizuruKagura.f6163;
            for (int i3 = chizuruKagura.f6161; i3 < i2; i3++) {
                i = (i * 31) + chizuruKagura.f6164[i3];
            }
            chizuruKagura = chizuruKagura.f6158;
        } while (chizuruKagura != this.f6169);
        return i;
    }

    public String toString() {
        return m6739().toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public GoroDaimon m6770(int i) {
        ChizuruKagura r0 = m6743(1);
        byte[] bArr = r0.f6164;
        int i2 = r0.f6163;
        r0.f6163 = i2 + 1;
        bArr[i2] = (byte) i;
        this.f6168++;
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public short m6723() {
        return Orochi.m18997(m6751());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public byte m6724(long j) {
        Orochi.m18998(this.f6168, j, 1);
        ChizuruKagura chizuruKagura = this.f6169;
        while (true) {
            int i = chizuruKagura.f6163 - chizuruKagura.f6161;
            if (j < ((long) i)) {
                return chizuruKagura.f6164[chizuruKagura.f6161 + ((int) j)];
            }
            j -= (long) i;
            chizuruKagura = chizuruKagura.f6158;
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m6725() {
        if (this.f6168 < 4) {
            throw new IllegalStateException("size < 4: " + this.f6168);
        }
        ChizuruKagura chizuruKagura = this.f6169;
        int i = chizuruKagura.f6161;
        int i2 = chizuruKagura.f6163;
        if (i2 - i < 4) {
            return ((m6758() & 255) << 24) | ((m6758() & 255) << 16) | ((m6758() & 255) << 8) | (m6758() & 255);
        }
        byte[] bArr = chizuruKagura.f6164;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
        int i5 = i4 + 1;
        byte b2 = b | ((bArr[i4] & 255) << 8);
        int i6 = i5 + 1;
        byte b3 = b2 | (bArr[i5] & 255);
        this.f6168 -= 4;
        if (i6 == i2) {
            this.f6169 = chizuruKagura.m6715();
            Goenitz.m6720(chizuruKagura);
            return b3;
        }
        chizuruKagura.f6161 = i6;
        return b3;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public GoroDaimon m6753(int i) {
        ChizuruKagura r0 = m6743(2);
        byte[] bArr = r0.f6164;
        int i2 = r0.f6163;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        r0.f6163 = i3 + 1;
        this.f6168 += 2;
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m6727() {
        return Orochi.m18996(m6725());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public GoroDaimon m6761(int i) {
        ChizuruKagura r0 = m6743(4);
        byte[] bArr = r0.f6164;
        int i2 = r0.f6163;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        r0.f6163 = i5 + 1;
        this.f6168 += 4;
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m6729(long j) throws EOFException {
        return m6777(j, Orochi.f15168);
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public long m6731() {
        return this.f6168;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public void m6732() {
        try {
            m6757(this.f6168);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public GoroDaimon m6721(long j) {
        if (j == 0) {
            return m6770(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        ChizuruKagura r2 = m6743(numberOfTrailingZeros);
        byte[] bArr = r2.f6164;
        int i = r2.f6163;
        for (int i2 = (r2.f6163 + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = f6167[(int) (15 & j)];
            j >>>= 4;
        }
        r2.f6163 += numberOfTrailingZeros;
        this.f6168 = ((long) numberOfTrailingZeros) + this.f6168;
        return this;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public InputStream m6734() {
        return new InputStream() {
            public int available() {
                return (int) Math.min(GoroDaimon.this.f6168, 2147483647L);
            }

            public void close() {
            }

            public int read() {
                if (GoroDaimon.this.f6168 > 0) {
                    return GoroDaimon.this.m6758() & 255;
                }
                return -1;
            }

            public int read(byte[] bArr, int i, int i2) {
                return GoroDaimon.this.m6752(bArr, i, i2);
            }

            public String toString() {
                return GoroDaimon.this + ".inputStream()";
            }
        };
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public GoroDaimon clone() {
        GoroDaimon goroDaimon = new GoroDaimon();
        if (this.f6168 == 0) {
            return goroDaimon;
        }
        goroDaimon.f6169 = new ChizuruKagura(this.f6169);
        ChizuruKagura chizuruKagura = goroDaimon.f6169;
        ChizuruKagura chizuruKagura2 = goroDaimon.f6169;
        ChizuruKagura chizuruKagura3 = goroDaimon.f6169;
        chizuruKagura2.f6159 = chizuruKagura3;
        chizuruKagura.f6158 = chizuruKagura3;
        for (ChizuruKagura chizuruKagura4 = this.f6169.f6158; chizuruKagura4 != this.f6169; chizuruKagura4 = chizuruKagura4.f6158) {
            goroDaimon.f6169.f6159.m6717(new ChizuruKagura(chizuruKagura4));
        }
        goroDaimon.f6168 = this.f6168;
        return goroDaimon;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public ChoiBounge m6736() {
        return new ChoiBounge(m6738());
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public String m6737() {
        try {
            return m6777(this.f6168, Orochi.f15168);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public byte[] m6738() {
        try {
            return m6760(this.f6168);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public ChoiBounge m6739() {
        if (this.f6168 <= 2147483647L) {
            return m6746((int) this.f6168);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.f6168);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public long m6740() {
        int i;
        int i2;
        if (this.f6168 == 0) {
            throw new IllegalStateException("size == 0");
        }
        boolean z = false;
        int i3 = 0;
        long j = 0;
        while (true) {
            ChizuruKagura chizuruKagura = this.f6169;
            byte[] bArr = chizuruKagura.f6164;
            int i4 = chizuruKagura.f6161;
            int i5 = chizuruKagura.f6163;
            i = i3;
            while (true) {
                if (i4 >= i5) {
                    break;
                }
                byte b = bArr[i4];
                if (b >= 48 && b <= 57) {
                    i2 = b - 48;
                } else if (b >= 97 && b <= 102) {
                    i2 = (b - 97) + 10;
                } else if (b >= 65 && b <= 70) {
                    i2 = (b - 65) + 10;
                } else if (i == 0) {
                    throw new NumberFormatException("Expected leading [0-9a-fA-F] character but was 0x" + Integer.toHexString(b));
                } else {
                    z = true;
                }
                if ((-1152921504606846976L & j) != 0) {
                    throw new NumberFormatException("Number too large: " + new GoroDaimon().m6721(j).m6770((int) b).m6737());
                }
                j = (j << 4) | ((long) i2);
                i++;
                i4++;
            }
            if (i4 == i5) {
                this.f6169 = chizuruKagura.m6715();
                Goenitz.m6720(chizuruKagura);
            } else {
                chizuruKagura.f6161 = i4;
            }
            if (z || this.f6169 == null) {
                this.f6168 -= (long) i;
            } else {
                i3 = i;
            }
        }
        this.f6168 -= (long) i;
        return j;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public GoroDaimon m6759(int i) {
        return m6761(Orochi.m18996(i));
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public String m6742(long j) throws EOFException {
        long j2 = Long.MAX_VALUE;
        if (j < 0) {
            throw new IllegalArgumentException("limit < 0: " + j);
        }
        if (j != Long.MAX_VALUE) {
            j2 = j + 1;
        }
        long r6 = m6767((byte) 10, 0, j2);
        if (r6 != -1) {
            return m6745(r6);
        }
        if (j2 < m6731() && m6724(j2 - 1) == 13 && m6724(j2) == 10) {
            return m6745(j2);
        }
        GoroDaimon goroDaimon = new GoroDaimon();
        m6775(goroDaimon, 0, Math.min(32, m6731()));
        throw new EOFException("\\n not found: limit=" + Math.min(m6731(), j) + " content=" + goroDaimon.m6736().e() + 8230);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public ChizuruKagura m6743(int i) {
        if (i < 1 || i > 8192) {
            throw new IllegalArgumentException();
        } else if (this.f6169 == null) {
            this.f6169 = Goenitz.m6719();
            ChizuruKagura chizuruKagura = this.f6169;
            ChizuruKagura chizuruKagura2 = this.f6169;
            ChizuruKagura chizuruKagura3 = this.f6169;
            chizuruKagura2.f6159 = chizuruKagura3;
            chizuruKagura.f6158 = chizuruKagura3;
            return chizuruKagura3;
        } else {
            ChizuruKagura chizuruKagura4 = this.f6169.f6159;
            return (chizuruKagura4.f6163 + i > 8192 || !chizuruKagura4.f6160) ? chizuruKagura4.m6717(Goenitz.m6719()) : chizuruKagura4;
        }
    }

    @Nullable
    /* renamed from: ٴ  reason: contains not printable characters */
    public String m6744() throws EOFException {
        long r0 = m6766((byte) 10);
        if (r0 != -1) {
            return m6745(r0);
        }
        if (this.f6168 != 0) {
            return m6729(this.f6168);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public String m6745(long j) throws EOFException {
        if (j <= 0 || m6724(j - 1) != 13) {
            String r0 = m6729(j);
            m6757(1);
            return r0;
        }
        String r02 = m6729(j - 1);
        m6757(2);
        return r02;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public ChoiBounge m6746(int i) {
        return i == 0 ? ChoiBounge.b : new ShingoYabuki(this, i);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public GoroDaimon m6749(long j) {
        boolean z;
        long j2;
        if (j == 0) {
            return m6770(48);
        }
        if (j < 0) {
            j2 = -j;
            if (j2 < 0) {
                return m6771("-9223372036854775808");
            }
            z = true;
        } else {
            z = false;
            j2 = j;
        }
        int i = j2 < 100000000 ? j2 < 10000 ? j2 < 100 ? j2 < 10 ? 1 : 2 : j2 < 1000 ? 3 : 4 : j2 < C.MICROS_PER_SECOND ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8 : j2 < 1000000000000L ? j2 < 10000000000L ? j2 < C.NANOS_PER_SECOND ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
        if (z) {
            i++;
        }
        ChizuruKagura r5 = m6743(i);
        byte[] bArr = r5.f6164;
        int i2 = r5.f6163 + i;
        while (j2 != 0) {
            i2--;
            bArr[i2] = f6167[(int) (j2 % 10)];
            j2 /= 10;
        }
        if (z) {
            bArr[i2 - 1] = 45;
        }
        r5.f6163 += i;
        this.f6168 = ((long) i) + this.f6168;
        return this;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public String m6748() throws EOFException {
        return m6742(Long.MAX_VALUE);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public GoroDaimon m6750(int i) {
        if (i < 128) {
            m6770(i);
        } else if (i < 2048) {
            m6770((i >> 6) | PsExtractor.AUDIO_STREAM);
            m6770((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                m6770((i >> 12) | 224);
                m6770(((i >> 6) & 63) | 128);
                m6770((i & 63) | 128);
            } else {
                m6770(63);
            }
        } else if (i <= 1114111) {
            m6770((i >> 18) | 240);
            m6770(((i >> 12) & 63) | 128);
            m6770(((i >> 6) & 63) | 128);
            m6770((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public short m6751() {
        if (this.f6168 < 2) {
            throw new IllegalStateException("size < 2: " + this.f6168);
        }
        ChizuruKagura chizuruKagura = this.f6169;
        int i = chizuruKagura.f6161;
        int i2 = chizuruKagura.f6163;
        if (i2 - i < 2) {
            return (short) (((m6758() & 255) << 8) | (m6758() & 255));
        }
        byte[] bArr = chizuruKagura.f6164;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
        this.f6168 -= 2;
        if (i4 == i2) {
            this.f6169 = chizuruKagura.m6715();
            Goenitz.m6720(chizuruKagura);
        } else {
            chizuruKagura.f6161 = i4;
        }
        return (short) b;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m6752(byte[] bArr, int i, int i2) {
        Orochi.m18998((long) bArr.length, (long) i, (long) i2);
        ChizuruKagura chizuruKagura = this.f6169;
        if (chizuruKagura == null) {
            return -1;
        }
        int min = Math.min(i2, chizuruKagura.f6163 - chizuruKagura.f6161);
        System.arraycopy(chizuruKagura.f6164, chizuruKagura.f6161, bArr, i, min);
        chizuruKagura.f6161 += min;
        this.f6168 -= (long) min;
        if (chizuruKagura.f6161 != chizuruKagura.f6163) {
            return min;
        }
        this.f6169 = chizuruKagura.m6715();
        Goenitz.m6720(chizuruKagura);
        return min;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m6755() {
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m6771(String str) {
        return m6776(str, 0, str.length());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6757(long j) throws EOFException {
        while (j > 0) {
            if (this.f6169 == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f6169.f6163 - this.f6169.f6161));
            this.f6168 -= (long) min;
            j -= (long) min;
            ChizuruKagura chizuruKagura = this.f6169;
            chizuruKagura.f6161 = min + chizuruKagura.f6161;
            if (this.f6169.f6161 == this.f6169.f6163) {
                ChizuruKagura chizuruKagura2 = this.f6169;
                this.f6169 = chizuruKagura2.m6715();
                Goenitz.m6720(chizuruKagura2);
            }
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public byte m6758() {
        if (this.f6168 == 0) {
            throw new IllegalStateException("size == 0");
        }
        ChizuruKagura chizuruKagura = this.f6169;
        int i = chizuruKagura.f6161;
        int i2 = chizuruKagura.f6163;
        int i3 = i + 1;
        byte b = chizuruKagura.f6164[i];
        this.f6168--;
        if (i3 == i2) {
            this.f6169 = chizuruKagura.m6715();
            Goenitz.m6720(chizuruKagura);
        } else {
            chizuruKagura.f6161 = i3;
        }
        return b;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public byte[] m6760(long j) throws EOFException {
        Orochi.m18998(this.f6168, 0, j);
        if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        }
        byte[] bArr = new byte[((int) j)];
        m6780(bArr);
        return bArr;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public ChoiBounge m6762(long j) throws EOFException {
        return new ChoiBounge(m6760(j));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public GoroDaimon m6754(byte[] bArr) {
        if (bArr != null) {
            return m6772(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public GoroDaimon m6772(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        Orochi.m18998((long) bArr.length, (long) i, (long) i2);
        int i3 = i + i2;
        while (i < i3) {
            ChizuruKagura r1 = m6743(1);
            int min = Math.min(i3 - i, 8192 - r1.f6163);
            System.arraycopy(bArr, i, r1.f6164, r1.f6163, min);
            i += min;
            r1.f6163 = min + r1.f6163;
        }
        this.f6168 += (long) i2;
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m6765() {
        return this.f6168 == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m6766(byte b) {
        return m6767(b, 0, Long.MAX_VALUE);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m6767(byte b, long j, long j2) {
        ChizuruKagura chizuruKagura;
        ChizuruKagura chizuruKagura2;
        long j3;
        if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("size=%s fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(this.f6168), Long.valueOf(j), Long.valueOf(j2)}));
        }
        if (j2 > this.f6168) {
            j2 = this.f6168;
        }
        if (j == j2 || (chizuruKagura = this.f6169) == null) {
            return -1;
        }
        if (this.f6168 - j >= j) {
            long j4 = 0;
            ChizuruKagura chizuruKagura3 = chizuruKagura;
            while (true) {
                long j5 = ((long) (chizuruKagura2.f6163 - chizuruKagura2.f6161)) + j3;
                if (j5 >= j) {
                    break;
                }
                chizuruKagura3 = chizuruKagura2.f6158;
                j4 = j5;
            }
        } else {
            j3 = this.f6168;
            chizuruKagura2 = chizuruKagura;
            while (j3 > j) {
                chizuruKagura2 = chizuruKagura2.f6159;
                j3 -= (long) (chizuruKagura2.f6163 - chizuruKagura2.f6161);
            }
        }
        long j6 = j3;
        while (j6 < j2) {
            byte[] bArr = chizuruKagura2.f6164;
            int min = (int) Math.min((long) chizuruKagura2.f6163, (((long) chizuruKagura2.f6161) + j2) - j6);
            for (int i = (int) ((((long) chizuruKagura2.f6161) + j) - j6); i < min; i++) {
                if (bArr[i] == b) {
                    return ((long) (i - chizuruKagura2.f6161)) + j6;
                }
            }
            long j7 = ((long) (chizuruKagura2.f6163 - chizuruKagura2.f6161)) + j6;
            chizuruKagura2 = chizuruKagura2.f6158;
            j6 = j7;
            j = j7;
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m6768(GoroDaimon goroDaimon, long j) {
        if (goroDaimon == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f6168 == 0) {
            return -1;
        } else {
            if (j > this.f6168) {
                j = this.f6168;
            }
            goroDaimon.a_(this, j);
            return j;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m6769(Shermie shermie) throws IOException {
        if (shermie == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long r2 = shermie.m19009(this, PlaybackStateCompat.ACTION_PLAY_FROM_URI);
            if (r2 == -1) {
                return j;
            }
            j += r2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m6773() {
        return Chris.f15141;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m6774(ChoiBounge choiBounge) {
        if (choiBounge == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        choiBounge.m18940(this);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public GoroDaimon m6775(GoroDaimon goroDaimon, long j, long j2) {
        if (goroDaimon == null) {
            throw new IllegalArgumentException("out == null");
        }
        Orochi.m18998(this.f6168, j, j2);
        if (j2 != 0) {
            goroDaimon.f6168 += j2;
            ChizuruKagura chizuruKagura = this.f6169;
            while (j >= ((long) (chizuruKagura.f6163 - chizuruKagura.f6161))) {
                j -= (long) (chizuruKagura.f6163 - chizuruKagura.f6161);
                chizuruKagura = chizuruKagura.f6158;
            }
            while (j2 > 0) {
                ChizuruKagura chizuruKagura2 = new ChizuruKagura(chizuruKagura);
                chizuruKagura2.f6161 = (int) (((long) chizuruKagura2.f6161) + j);
                chizuruKagura2.f6163 = Math.min(chizuruKagura2.f6161 + ((int) j2), chizuruKagura2.f6163);
                if (goroDaimon.f6169 == null) {
                    chizuruKagura2.f6159 = chizuruKagura2;
                    chizuruKagura2.f6158 = chizuruKagura2;
                    goroDaimon.f6169 = chizuruKagura2;
                } else {
                    goroDaimon.f6169.f6159.m6717(chizuruKagura2);
                }
                j2 -= (long) (chizuruKagura2.f6163 - chizuruKagura2.f6161);
                chizuruKagura = chizuruKagura.f6158;
                j = 0;
            }
        }
        return this;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 152 */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p009if.GoroDaimon m6776(java.lang.String r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 57343(0xdfff, float:8.0355E-41)
            r7 = 128(0x80, float:1.794E-43)
            if (r10 != 0) goto L_0x0010
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "string == null"
            r0.<init>(r1)
            throw r0
        L_0x0010:
            if (r11 >= 0) goto L_0x002c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "beginIndex < 0: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002c:
            if (r12 >= r11) goto L_0x0053
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex < beginIndex: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " < "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0053:
            int r0 = r10.length()
            if (r12 <= r0) goto L_0x0096
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex > string.length: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " > "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r10.length()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0082:
            r0 = 0
        L_0x0083:
            r2 = 56319(0xdbff, float:7.892E-41)
            if (r1 > r2) goto L_0x008f
            r2 = 56320(0xdc00, float:7.8921E-41)
            if (r0 < r2) goto L_0x008f
            if (r0 <= r8) goto L_0x011a
        L_0x008f:
            r0 = 63
            r9.m6770((int) r0)
            int r11 = r11 + 1
        L_0x0096:
            if (r11 >= r12) goto L_0x014b
            char r1 = r10.charAt(r11)
            if (r1 >= r7) goto L_0x00d8
            r0 = 1
            if.ChizuruKagura r2 = r9.m6743((int) r0)
            byte[] r3 = r2.f6164
            int r0 = r2.f6163
            int r4 = r0 - r11
            int r0 = 8192 - r4
            int r5 = java.lang.Math.min(r12, r0)
            int r0 = r11 + 1
            int r6 = r4 + r11
            byte r1 = (byte) r1
            r3[r6] = r1
        L_0x00b6:
            if (r0 >= r5) goto L_0x00be
            char r6 = r10.charAt(r0)
            if (r6 < r7) goto L_0x00d0
        L_0x00be:
            int r1 = r0 + r4
            int r3 = r2.f6163
            int r1 = r1 - r3
            int r3 = r2.f6163
            int r3 = r3 + r1
            r2.f6163 = r3
            long r2 = r9.f6168
            long r4 = (long) r1
            long r2 = r2 + r4
            r9.f6168 = r2
        L_0x00ce:
            r11 = r0
            goto L_0x0096
        L_0x00d0:
            int r1 = r0 + 1
            int r0 = r0 + r4
            byte r6 = (byte) r6
            r3[r0] = r6
            r0 = r1
            goto L_0x00b6
        L_0x00d8:
            r0 = 2048(0x800, float:2.87E-42)
            if (r1 >= r0) goto L_0x00ed
            int r0 = r1 >> 6
            r0 = r0 | 192(0xc0, float:2.69E-43)
            r9.m6770((int) r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.m6770((int) r0)
            int r0 = r11 + 1
            goto L_0x00ce
        L_0x00ed:
            r0 = 55296(0xd800, float:7.7486E-41)
            if (r1 < r0) goto L_0x00f4
            if (r1 <= r8) goto L_0x010e
        L_0x00f4:
            int r0 = r1 >> 12
            r0 = r0 | 224(0xe0, float:3.14E-43)
            r9.m6770((int) r0)
            int r0 = r1 >> 6
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.m6770((int) r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.m6770((int) r0)
            int r0 = r11 + 1
            goto L_0x00ce
        L_0x010e:
            int r0 = r11 + 1
            if (r0 >= r12) goto L_0x0082
            int r0 = r11 + 1
            char r0 = r10.charAt(r0)
            goto L_0x0083
        L_0x011a:
            r2 = 65536(0x10000, float:9.18355E-41)
            r3 = -55297(0xffffffffffff27ff, float:NaN)
            r1 = r1 & r3
            int r1 = r1 << 10
            r3 = -56321(0xffffffffffff23ff, float:NaN)
            r0 = r0 & r3
            r0 = r0 | r1
            int r0 = r0 + r2
            int r1 = r0 >> 18
            r1 = r1 | 240(0xf0, float:3.36E-43)
            r9.m6770((int) r1)
            int r1 = r0 >> 12
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.m6770((int) r1)
            int r1 = r0 >> 6
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.m6770((int) r1)
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.m6770((int) r0)
            int r0 = r11 + 2
            goto L_0x00ce
        L_0x014b:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: p009if.GoroDaimon.m6776(java.lang.String, int, int):if.GoroDaimon");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6777(long j, Charset charset) throws EOFException {
        Orochi.m18998(this.f6168, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            ChizuruKagura chizuruKagura = this.f6169;
            if (((long) chizuruKagura.f6161) + j > ((long) chizuruKagura.f6163)) {
                return new String(m6760(j), charset);
            }
            String str = new String(chizuruKagura.f6164, chizuruKagura.f6161, (int) j, charset);
            chizuruKagura.f6161 = (int) (((long) chizuruKagura.f6161) + j);
            this.f6168 -= j;
            if (chizuruKagura.f6161 != chizuruKagura.f6163) {
                return str;
            }
            this.f6169 = chizuruKagura.m6715();
            Goenitz.m6720(chizuruKagura);
            return str;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m6778(Charset charset) {
        try {
            return m6777(this.f6168, charset);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6779(long j) throws EOFException {
        if (this.f6168 < j) {
            throw new EOFException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6780(byte[] bArr) throws EOFException {
        int i = 0;
        while (i < bArr.length) {
            int r1 = m6752(bArr, i, bArr.length - i);
            if (r1 == -1) {
                throw new EOFException();
            }
            i += r1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6781(long j, ChoiBounge choiBounge) {
        return m6782(j, choiBounge, 0, choiBounge.g());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m6782(long j, ChoiBounge choiBounge, int i, int i2) {
        if (j < 0 || i < 0 || i2 < 0 || this.f6168 - j < ((long) i2) || choiBounge.g() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (m6724(((long) i3) + j) != choiBounge.a(i + i3)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public GoroDaimon m6730() {
        return this;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public long m6784() {
        long j = this.f6168;
        if (j == 0) {
            return 0;
        }
        ChizuruKagura chizuruKagura = this.f6169.f6159;
        return (chizuruKagura.f6163 >= 8192 || !chizuruKagura.f6160) ? j : j - ((long) (chizuruKagura.f6163 - chizuruKagura.f6161));
    }
}
