package p009if;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;

/* renamed from: if.Mature  reason: invalid package */
final class Mature implements ChinGentsai {

    /* renamed from: 靐  reason: contains not printable characters */
    public final YashiroNanakase f15165;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f15166;

    /* renamed from: 龘  reason: contains not printable characters */
    public final GoroDaimon f15167 = new GoroDaimon();

    Mature(YashiroNanakase yashiroNanakase) {
        if (yashiroNanakase == null) {
            throw new NullPointerException("sink == null");
        }
        this.f15165 = yashiroNanakase;
    }

    public void a_(GoroDaimon goroDaimon, long j) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.a_(goroDaimon, j);
        m18983();
    }

    public void close() throws IOException {
        if (!this.f15166) {
            Throwable th = null;
            try {
                if (this.f15167.f6168 > 0) {
                    this.f15165.a_(this.f15167, this.f15167.f6168);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f15165.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.f15166 = true;
            if (th != null) {
                Orochi.m18999(th);
            }
        }
    }

    public void flush() throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        if (this.f15167.f6168 > 0) {
            this.f15165.a_(this.f15167, this.f15167.f6168);
        }
        this.f15165.flush();
    }

    public String toString() {
        return "buffer(" + this.f15165 + ")";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public ChinGentsai m18982(long j) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6721(j);
        return m18983();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public ChinGentsai m18983() throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        long r0 = this.f15167.m6784();
        if (r0 > 0) {
            this.f15165.a_(this.f15167, r0);
        }
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public ChinGentsai m18984(long j) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6749(j);
        return m18983();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ChinGentsai m18985(int i) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6753(i);
        return m18983();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ChinGentsai m18986(byte[] bArr) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6754(bArr);
        return m18983();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public GoroDaimon m18987() {
        return this.f15167;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ChinGentsai m18988(int i) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6759(i);
        return m18983();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public ChinGentsai m18989(int i) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6761(i);
        return m18983();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m18990(Shermie shermie) throws IOException {
        if (shermie == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long r2 = shermie.m19009(this.f15167, PlaybackStateCompat.ACTION_PLAY_FROM_URI);
            if (r2 == -1) {
                return j;
            }
            j += r2;
            m18983();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ChinGentsai m18991(int i) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6770(i);
        return m18983();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ChinGentsai m18992(String str) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6771(str);
        return m18983();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ChinGentsai m18993(byte[] bArr, int i, int i2) throws IOException {
        if (this.f15166) {
            throw new IllegalStateException("closed");
        }
        this.f15167.m6772(bArr, i, i2);
        return m18983();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18994() {
        return this.f15165.m19014();
    }
}
