package p009if;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/* renamed from: if.SaishuKusanagi  reason: invalid package */
public final class SaishuKusanagi implements Shermie {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Inflater f15174;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f15175;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f15176;

    /* renamed from: 龘  reason: contains not printable characters */
    private final ChangKoehan f15177;

    SaishuKusanagi(ChangKoehan changKoehan, Inflater inflater) {
        if (changKoehan == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f15177 = changKoehan;
            this.f15174 = inflater;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m19005() throws IOException {
        if (this.f15176 != 0) {
            int remaining = this.f15176 - this.f15174.getRemaining();
            this.f15176 -= remaining;
            this.f15177.m6704((long) remaining);
        }
    }

    public void close() throws IOException {
        if (!this.f15175) {
            this.f15174.end();
            this.f15175 = true;
            this.f15177.close();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m19006() throws IOException {
        if (!this.f15174.needsInput()) {
            return false;
        }
        m19005();
        if (this.f15174.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f15177.m6708()) {
            return true;
        } else {
            ChizuruKagura chizuruKagura = this.f15177.m6703().f6169;
            this.f15176 = chizuruKagura.f6163 - chizuruKagura.f6161;
            this.f15174.setInput(chizuruKagura.f6164, chizuruKagura.f6161, this.f15176);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m19007(GoroDaimon goroDaimon, long j) throws IOException {
        boolean r0;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f15175) {
            throw new IllegalStateException("closed");
        } else if (j == 0) {
            return 0;
        } else {
            do {
                r0 = m19006();
                try {
                    ChizuruKagura r1 = goroDaimon.m6743(1);
                    int inflate = this.f15174.inflate(r1.f6164, r1.f6163, 8192 - r1.f6163);
                    if (inflate > 0) {
                        r1.f6163 += inflate;
                        goroDaimon.f6168 += (long) inflate;
                        return (long) inflate;
                    } else if (this.f15174.finished() || this.f15174.needsDictionary()) {
                        m19005();
                        if (r1.f6161 == r1.f6163) {
                            goroDaimon.f6169 = r1.m6715();
                            Goenitz.m6720(r1);
                        }
                        return -1;
                    }
                } catch (DataFormatException e) {
                    throw new IOException(e);
                }
            } while (!r0);
            throw new EOFException("source exhausted prematurely");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m19008() {
        return this.f15177.m19010();
    }
}
