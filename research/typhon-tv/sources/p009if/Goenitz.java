package p009if;

import android.support.v4.media.session.PlaybackStateCompat;
import javax.annotation.Nullable;

/* renamed from: if.Goenitz  reason: invalid package */
final class Goenitz {

    /* renamed from: 靐  reason: contains not printable characters */
    static long f6165;
    @Nullable

    /* renamed from: 龘  reason: contains not printable characters */
    static ChizuruKagura f6166;

    private Goenitz() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ChizuruKagura m6719() {
        synchronized (Goenitz.class) {
            if (f6166 == null) {
                return new ChizuruKagura();
            }
            ChizuruKagura chizuruKagura = f6166;
            f6166 = chizuruKagura.f6158;
            chizuruKagura.f6158 = null;
            f6165 -= PlaybackStateCompat.ACTION_PLAY_FROM_URI;
            return chizuruKagura;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m6720(ChizuruKagura chizuruKagura) {
        if (chizuruKagura.f6158 != null || chizuruKagura.f6159 != null) {
            throw new IllegalArgumentException();
        } else if (!chizuruKagura.f6162) {
            synchronized (Goenitz.class) {
                if (f6165 + PlaybackStateCompat.ACTION_PLAY_FROM_URI <= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                    f6165 += PlaybackStateCompat.ACTION_PLAY_FROM_URI;
                    chizuruKagura.f6158 = f6166;
                    chizuruKagura.f6163 = 0;
                    chizuruKagura.f6161 = 0;
                    f6166 = chizuruKagura;
                }
            }
        }
    }
}
