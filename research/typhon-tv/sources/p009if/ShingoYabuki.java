package p009if;

import java.util.Arrays;

/* renamed from: if.ShingoYabuki  reason: invalid package */
final class ShingoYabuki extends ChoiBounge {

    /* renamed from: 连任  reason: contains not printable characters */
    final transient int[] f15178;

    /* renamed from: 麤  reason: contains not printable characters */
    final transient byte[][] f15179;

    ShingoYabuki(GoroDaimon goroDaimon, int i) {
        super((byte[]) null);
        Orochi.m18998(goroDaimon.f6168, 0, (long) i);
        ChizuruKagura chizuruKagura = goroDaimon.f6169;
        int i2 = 0;
        int i3 = 0;
        while (i3 < i) {
            if (chizuruKagura.f6163 == chizuruKagura.f6161) {
                throw new AssertionError("s.limit == s.pos");
            }
            i3 += chizuruKagura.f6163 - chizuruKagura.f6161;
            i2++;
            chizuruKagura = chizuruKagura.f6158;
        }
        this.f15179 = new byte[i2][];
        this.f15178 = new int[(i2 * 2)];
        ChizuruKagura chizuruKagura2 = goroDaimon.f6169;
        int i4 = 0;
        int i5 = 0;
        while (i5 < i) {
            this.f15179[i4] = chizuruKagura2.f6164;
            int i6 = (chizuruKagura2.f6163 - chizuruKagura2.f6161) + i5;
            if (i6 > i) {
                i6 = i;
            }
            this.f15178[i4] = i6;
            this.f15178[this.f15179.length + i4] = chizuruKagura2.f6161;
            chizuruKagura2.f6162 = true;
            i4++;
            chizuruKagura2 = chizuruKagura2.f6158;
            i5 = i6;
        }
    }

    private Object writeReplace() {
        return m19012();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m19011(int i) {
        int binarySearch = Arrays.binarySearch(this.f15178, 0, this.f15179.length, i + 1);
        return binarySearch >= 0 ? binarySearch : binarySearch ^ -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ChoiBounge m19012() {
        return new ChoiBounge(h());
    }

    public byte a(int i) {
        Orochi.m18998((long) this.f15178[this.f15179.length - 1], (long) i, 1);
        int r1 = m19011(i);
        return this.f15179[r1][(i - (r1 == 0 ? 0 : this.f15178[r1 - 1])) + this.f15178[this.f15179.length + r1]];
    }

    public ChoiBounge a(int i, int i2) {
        return m19012().a(i, i2);
    }

    public String a() {
        return m19012().a();
    }

    public boolean a(int i, ChoiBounge choiBounge, int i2, int i3) {
        if (i < 0 || i > g() - i3) {
            return false;
        }
        int r2 = m19011(i);
        while (i3 > 0) {
            int i4 = r2 == 0 ? 0 : this.f15178[r2 - 1];
            int min = Math.min(i3, ((this.f15178[r2] - i4) + i4) - i);
            if (!choiBounge.a(i2, this.f15179[r2], (i - i4) + this.f15178[this.f15179.length + r2], min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            r2++;
        }
        return true;
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        if (i < 0 || i > g() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int r2 = m19011(i);
        while (i3 > 0) {
            int i4 = r2 == 0 ? 0 : this.f15178[r2 - 1];
            int min = Math.min(i3, ((this.f15178[r2] - i4) + i4) - i);
            if (!Orochi.m19000(this.f15179[r2], (i - i4) + this.f15178[this.f15179.length + r2], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            r2++;
        }
        return true;
    }

    public String b() {
        return m19012().b();
    }

    public ChoiBounge c() {
        return m19012().c();
    }

    public ChoiBounge d() {
        return m19012().d();
    }

    public String e() {
        return m19012().e();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof ChoiBounge) && ((ChoiBounge) obj).g() == g() && a(0, (ChoiBounge) obj, 0, g());
    }

    public ChoiBounge f() {
        return m19012().f();
    }

    public int g() {
        return this.f15178[this.f15179.length - 1];
    }

    public byte[] h() {
        byte[] bArr = new byte[this.f15178[this.f15179.length - 1]];
        int length = this.f15179.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = this.f15178[length + i];
            int i4 = this.f15178[i];
            System.arraycopy(this.f15179[i], i3, bArr, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return bArr;
    }

    public int hashCode() {
        int i = this.f15139;
        if (i == 0) {
            i = 1;
            int length = this.f15179.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                byte[] bArr = this.f15179[i2];
                int i4 = this.f15178[length + i2];
                int i5 = this.f15178[i2];
                int i6 = i4 + (i5 - i3);
                int i7 = i;
                while (i4 < i6) {
                    i7 = bArr[i4] + (i7 * 31);
                    i4++;
                }
                i2++;
                i3 = i5;
                i = i7;
            }
            this.f15139 = i;
        }
        return i;
    }

    public String toString() {
        return m19012().toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19013(GoroDaimon goroDaimon) {
        int length = this.f15179.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int i3 = this.f15178[length + i];
            int i4 = this.f15178[i];
            ChizuruKagura chizuruKagura = new ChizuruKagura(this.f15179[i], i3, (i3 + i4) - i2);
            if (goroDaimon.f6169 == null) {
                chizuruKagura.f6159 = chizuruKagura;
                chizuruKagura.f6158 = chizuruKagura;
                goroDaimon.f6169 = chizuruKagura;
            } else {
                goroDaimon.f6169.f6159.m6717(chizuruKagura);
            }
            i++;
            i2 = i4;
        }
        goroDaimon.f6168 += (long) i2;
    }
}
