package p009if;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

/* renamed from: if.Chris  reason: invalid package */
public class Chris {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Chris f15141 = new Chris() {
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m18948() throws IOException {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18949(long j) {
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Chris m18950(long j, TimeUnit timeUnit) {
            return this;
        }
    };

    /* renamed from: 麤  reason: contains not printable characters */
    private long f15142;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f15143;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f15144;

    public long y_() {
        return this.f15142;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Chris m18941() {
        this.f15144 = false;
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m18942() throws IOException {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f15144 && this.f15143 - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Chris m18943() {
        this.f15142 = 0;
        return this;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public long m18944() {
        if (this.f15144) {
            return this.f15143;
        }
        throw new IllegalStateException("No deadline");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m18945() {
        return this.f15144;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18946(long j) {
        this.f15144 = true;
        this.f15143 = j;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18947(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            this.f15142 = timeUnit.toNanos(j);
            return this;
        }
    }
}
