package p009if;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/* renamed from: if.BrianBattler  reason: invalid package */
public class BrianBattler extends Chris {

    /* renamed from: 龘  reason: contains not printable characters */
    private Chris f15137;

    public BrianBattler(Chris chris) {
        if (chris == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f15137 = chris;
    }

    public long y_() {
        return this.f15137.y_();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Chris m18916() {
        return this.f15137.m18941();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m18917() throws IOException {
        this.f15137.m18942();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Chris m18918() {
        return this.f15137.m18943();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public long m18919() {
        return this.f15137.m18944();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m18920() {
        return this.f15137.m18945();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final BrianBattler m18921(Chris chris) {
        if (chris == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f15137 = chris;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Chris m18922() {
        return this.f15137;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18923(long j) {
        return this.f15137.m18946(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chris m18924(long j, TimeUnit timeUnit) {
        return this.f15137.m18947(j, timeUnit);
    }
}
