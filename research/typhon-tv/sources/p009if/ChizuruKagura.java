package p009if;

import javax.annotation.Nullable;

/* renamed from: if.ChizuruKagura  reason: invalid package */
final class ChizuruKagura {

    /* renamed from: ʻ  reason: contains not printable characters */
    ChizuruKagura f6158;

    /* renamed from: ʼ  reason: contains not printable characters */
    ChizuruKagura f6159;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean f6160;

    /* renamed from: 靐  reason: contains not printable characters */
    int f6161;

    /* renamed from: 麤  reason: contains not printable characters */
    boolean f6162;

    /* renamed from: 齉  reason: contains not printable characters */
    int f6163;

    /* renamed from: 龘  reason: contains not printable characters */
    final byte[] f6164;

    ChizuruKagura() {
        this.f6164 = new byte[8192];
        this.f6160 = true;
        this.f6162 = false;
    }

    ChizuruKagura(ChizuruKagura chizuruKagura) {
        this(chizuruKagura.f6164, chizuruKagura.f6161, chizuruKagura.f6163);
        chizuruKagura.f6162 = true;
    }

    ChizuruKagura(byte[] bArr, int i, int i2) {
        this.f6164 = bArr;
        this.f6161 = i;
        this.f6163 = i2;
        this.f6160 = false;
        this.f6162 = true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m6714() {
        if (this.f6159 == this) {
            throw new IllegalStateException();
        } else if (this.f6159.f6160) {
            int i = this.f6163 - this.f6161;
            if (i <= (this.f6159.f6162 ? 0 : this.f6159.f6161) + (8192 - this.f6159.f6163)) {
                m6718(this.f6159, i);
                m6715();
                Goenitz.m6720(this);
            }
        }
    }

    @Nullable
    /* renamed from: 龘  reason: contains not printable characters */
    public ChizuruKagura m6715() {
        ChizuruKagura chizuruKagura = this.f6158 != this ? this.f6158 : null;
        this.f6159.f6158 = this.f6158;
        this.f6158.f6159 = this.f6159;
        this.f6158 = null;
        this.f6159 = null;
        return chizuruKagura;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ChizuruKagura m6716(int i) {
        ChizuruKagura r0;
        if (i <= 0 || i > this.f6163 - this.f6161) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            r0 = new ChizuruKagura(this);
        } else {
            r0 = Goenitz.m6719();
            System.arraycopy(this.f6164, this.f6161, r0.f6164, 0, i);
        }
        r0.f6163 = r0.f6161 + i;
        this.f6161 += i;
        this.f6159.m6717(r0);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ChizuruKagura m6717(ChizuruKagura chizuruKagura) {
        chizuruKagura.f6159 = this;
        chizuruKagura.f6158 = this.f6158;
        this.f6158.f6159 = chizuruKagura;
        this.f6158 = chizuruKagura;
        return chizuruKagura;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m6718(ChizuruKagura chizuruKagura, int i) {
        if (!chizuruKagura.f6160) {
            throw new IllegalArgumentException();
        }
        if (chizuruKagura.f6163 + i > 8192) {
            if (chizuruKagura.f6162) {
                throw new IllegalArgumentException();
            } else if ((chizuruKagura.f6163 + i) - chizuruKagura.f6161 > 8192) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(chizuruKagura.f6164, chizuruKagura.f6161, chizuruKagura.f6164, 0, chizuruKagura.f6163 - chizuruKagura.f6161);
                chizuruKagura.f6163 -= chizuruKagura.f6161;
                chizuruKagura.f6161 = 0;
            }
        }
        System.arraycopy(this.f6164, this.f6161, chizuruKagura.f6164, chizuruKagura.f6163, i);
        chizuruKagura.f6163 += i;
        this.f6161 += i;
    }
}
