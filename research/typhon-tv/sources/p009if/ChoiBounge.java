package p009if;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;

/* renamed from: if.ChoiBounge  reason: invalid package */
public class ChoiBounge implements Serializable, Comparable<ChoiBounge> {
    public static final ChoiBounge b = a(new byte[0]);
    private static final long serialVersionUID = 1;

    /* renamed from: 龘  reason: contains not printable characters */
    static final char[] f15138 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    final byte[] c;

    /* renamed from: 靐  reason: contains not printable characters */
    transient int f15139;

    /* renamed from: 齉  reason: contains not printable characters */
    transient String f15140;

    ChoiBounge(byte[] bArr) {
        this.c = bArr;
    }

    public static ChoiBounge a(InputStream inputStream, int i) throws IOException {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (i < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + i);
        } else {
            byte[] bArr = new byte[i];
            int i2 = 0;
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read == -1) {
                    throw new EOFException();
                }
                i2 += read;
            }
            return new ChoiBounge(bArr);
        }
    }

    public static ChoiBounge a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        }
        ChoiBounge choiBounge = new ChoiBounge(str.getBytes(Orochi.f15168));
        choiBounge.f15140 = str;
        return choiBounge;
    }

    public static ChoiBounge a(byte... bArr) {
        if (bArr != null) {
            return new ChoiBounge((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    public static ChoiBounge b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("hex == null");
        } else if (str.length() % 2 != 0) {
            throw new IllegalArgumentException("Unexpected hex string: " + str);
        } else {
            byte[] bArr = new byte[(str.length() / 2)];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) ((m18937(str.charAt(i * 2)) << 4) + m18937(str.charAt((i * 2) + 1)));
            }
            return a(bArr);
        }
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        ChoiBounge a = a((InputStream) objectInputStream, objectInputStream.readInt());
        try {
            Field declaredField = ChoiBounge.class.getDeclaredField("c");
            declaredField.setAccessible(true);
            declaredField.set(this, a.c);
        } catch (NoSuchFieldException e) {
            throw new AssertionError();
        } catch (IllegalAccessException e2) {
            throw new AssertionError();
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.c.length);
        objectOutputStream.write(this.c);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m18937(char c2) {
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        if (c2 >= 'a' && c2 <= 'f') {
            return (c2 - 'a') + 10;
        }
        if (c2 >= 'A' && c2 <= 'F') {
            return (c2 - 'A') + 10;
        }
        throw new IllegalArgumentException("Unexpected hex digit: " + c2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m18938(String str, int i) {
        int length = str.length();
        int i2 = 0;
        int i3 = 0;
        while (i3 < length) {
            if (i2 == i) {
                return i3;
            }
            int codePointAt = str.codePointAt(i3);
            if ((Character.isISOControl(codePointAt) && codePointAt != 10 && codePointAt != 13) || codePointAt == 65533) {
                return -1;
            }
            i2++;
            i3 += Character.charCount(codePointAt);
        }
        return str.length();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private ChoiBounge m18939(String str) {
        try {
            return a(MessageDigest.getInstance(str).digest(this.c));
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public byte a(int i) {
        return this.c[i];
    }

    public ChoiBounge a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0");
        } else if (i2 > this.c.length) {
            throw new IllegalArgumentException("endIndex > length(" + this.c.length + ")");
        } else {
            int i3 = i2 - i;
            if (i3 < 0) {
                throw new IllegalArgumentException("endIndex < beginIndex");
            } else if (i == 0 && i2 == this.c.length) {
                return this;
            } else {
                byte[] bArr = new byte[i3];
                System.arraycopy(this.c, i, bArr, 0, i3);
                return new ChoiBounge(bArr);
            }
        }
    }

    public String a() {
        String str = this.f15140;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.c, Orochi.f15168);
        this.f15140 = str2;
        return str2;
    }

    public boolean a(int i, ChoiBounge choiBounge, int i2, int i3) {
        return choiBounge.a(i2, this.c, i, i3);
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        return i >= 0 && i <= this.c.length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && Orochi.m19000(this.c, i, bArr, i2, i3);
    }

    public final boolean a(ChoiBounge choiBounge) {
        return a(0, choiBounge, 0, choiBounge.g());
    }

    /* renamed from: b */
    public int compareTo(ChoiBounge choiBounge) {
        int g = g();
        int g2 = choiBounge.g();
        int min = Math.min(g, g2);
        for (int i = 0; i < min; i++) {
            byte a = a(i) & 255;
            byte a2 = choiBounge.a(i) & 255;
            if (a != a2) {
                return a < a2 ? -1 : 1;
            }
        }
        if (g == g2) {
            return 0;
        }
        return g >= g2 ? 1 : -1;
    }

    public String b() {
        return BenimaruNikaido.m18914(this.c);
    }

    public ChoiBounge c() {
        return m18939("SHA-1");
    }

    public ChoiBounge d() {
        return m18939("SHA-256");
    }

    public String e() {
        char[] cArr = new char[(this.c.length * 2)];
        byte[] bArr = this.c;
        int length = bArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            byte b2 = bArr[i];
            int i3 = i2 + 1;
            cArr[i2] = f15138[(b2 >> 4) & 15];
            cArr[i3] = f15138[b2 & 15];
            i++;
            i2 = i3 + 1;
        }
        return new String(cArr);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof ChoiBounge) && ((ChoiBounge) obj).g() == this.c.length && ((ChoiBounge) obj).a(0, this.c, 0, this.c.length);
    }

    public ChoiBounge f() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.c.length) {
                return this;
            }
            byte b2 = this.c[i2];
            if (b2 < 65 || b2 > 90) {
                i = i2 + 1;
            } else {
                byte[] bArr = (byte[]) this.c.clone();
                bArr[i2] = (byte) (b2 + 32);
                for (int i3 = i2 + 1; i3 < bArr.length; i3++) {
                    byte b3 = bArr[i3];
                    if (b3 >= 65 && b3 <= 90) {
                        bArr[i3] = (byte) (b3 + 32);
                    }
                }
                return new ChoiBounge(bArr);
            }
        }
    }

    public int g() {
        return this.c.length;
    }

    public byte[] h() {
        return (byte[]) this.c.clone();
    }

    public int hashCode() {
        int i = this.f15139;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.c);
        this.f15139 = hashCode;
        return hashCode;
    }

    public String toString() {
        if (this.c.length == 0) {
            return "[size=0]";
        }
        String a = a();
        int r1 = m18938(a, 64);
        if (r1 == -1) {
            return this.c.length <= 64 ? "[hex=" + e() + "]" : "[size=" + this.c.length + " hex=" + a(0, 64).e() + "…]";
        }
        String replace = a.substring(0, r1).replace("\\", "\\\\").replace(StringUtils.LF, "\\n").replace(StringUtils.CR, "\\r");
        return r1 < a.length() ? "[size=" + this.c.length + " text=" + replace + "…]" : "[text=" + replace + "]";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m18940(GoroDaimon goroDaimon) {
        goroDaimon.m6772(this.c, 0, this.c.length);
    }
}
