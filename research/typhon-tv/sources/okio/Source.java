package okio;

import java.io.Closeable;
import java.io.IOException;

public interface Source extends Closeable {
    void close() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    long m20568(Buffer buffer, long j) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    Timeout m20569();
}
