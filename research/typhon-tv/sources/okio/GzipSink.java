package okio;

import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Deflater;

public final class GzipSink implements Sink {

    /* renamed from: 连任  reason: contains not printable characters */
    private final CRC32 f16439 = new CRC32();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Deflater f16440;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f16441;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DeflaterSink f16442;

    /* renamed from: 龘  reason: contains not printable characters */
    private final BufferedSink f16443;

    public GzipSink(Sink sink) {
        if (sink == null) {
            throw new IllegalArgumentException("sink == null");
        }
        this.f16440 = new Deflater(-1, true);
        this.f16443 = Okio.m20506(sink);
        this.f16442 = new DeflaterSink(this.f16443, this.f16440);
        m20488();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20488() {
        Buffer r0 = this.f16443.m20447();
        r0.m7234(8075);
        r0.m7238(8);
        r0.m7238(0);
        r0.m7229(0);
        r0.m7238(0);
        r0.m7238(0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20489(Buffer buffer, long j) {
        Segment segment = buffer.f6475;
        while (j > 0) {
            int min = (int) Math.min(j, (long) (segment.f6484 - segment.f6482));
            this.f16439.update(segment.f6485, segment.f6482, min);
            j -= (long) min;
            segment = segment.f6479;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m20490() throws IOException {
        this.f16443.m20438((int) this.f16439.getValue());
        this.f16443.m20438((int) this.f16440.getBytesRead());
    }

    public void a_(Buffer buffer, long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j != 0) {
            m20489(buffer, j);
            this.f16442.a_(buffer, j);
        }
    }

    public void close() throws IOException {
        if (!this.f16441) {
            Throwable th = null;
            try {
                this.f16442.m20475();
                m20490();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f16440.end();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            try {
                this.f16443.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.f16441 = true;
            if (th != null) {
                Util.m20582(th);
            }
        }
    }

    public void flush() throws IOException {
        this.f16442.flush();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20491() {
        return this.f16443.m20567();
    }
}
