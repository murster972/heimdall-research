package okio;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ForwardingTimeout extends Timeout {

    /* renamed from: 龘  reason: contains not printable characters */
    private Timeout f16438;

    public ForwardingTimeout(Timeout timeout) {
        if (timeout == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f16438 = timeout;
    }

    public long A_() {
        return this.f16438.A_();
    }

    public boolean B_() {
        return this.f16438.B_();
    }

    public Timeout C_() {
        return this.f16438.C_();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Timeout m20481() {
        return this.f16438.m20570();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m20482() throws IOException {
        this.f16438.m20571();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public long m20483() {
        return this.f16438.m20572();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ForwardingTimeout m20484(Timeout timeout) {
        if (timeout == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f16438 = timeout;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Timeout m20485() {
        return this.f16438;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20486(long j) {
        return this.f16438.m20573(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20487(long j, TimeUnit timeUnit) {
        return this.f16438.m20574(j, timeUnit);
    }
}
