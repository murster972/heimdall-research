package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class InflaterSource implements Source {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Inflater f16449;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f16450;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f16451;

    /* renamed from: 龘  reason: contains not printable characters */
    private final BufferedSource f16452;

    InflaterSource(BufferedSource bufferedSource, Inflater inflater) {
        if (bufferedSource == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f16452 = bufferedSource;
            this.f16449 = inflater;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m20498() throws IOException {
        if (this.f16451 != 0) {
            int remaining = this.f16451 - this.f16449.getRemaining();
            this.f16451 -= remaining;
            this.f16452.m20461((long) remaining);
        }
    }

    public void close() throws IOException {
        if (!this.f16450) {
            this.f16449.end();
            this.f16450 = true;
            this.f16452.close();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m20499() throws IOException {
        if (!this.f16449.needsInput()) {
            return false;
        }
        m20498();
        if (this.f16449.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f16452.m20452()) {
            return true;
        } else {
            Segment segment = this.f16452.m20466().f6475;
            this.f16451 = segment.f6484 - segment.f6482;
            this.f16449.setInput(segment.f6485, segment.f6482, this.f16451);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20500(Buffer buffer, long j) throws IOException {
        boolean r2;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f16450) {
            throw new IllegalStateException("closed");
        } else if (j == 0) {
            return 0;
        } else {
            do {
                r2 = m20499();
                try {
                    Segment r3 = buffer.m7209(1);
                    int inflate = this.f16449.inflate(r3.f6485, r3.f6484, (int) Math.min(j, (long) (8192 - r3.f6484)));
                    if (inflate > 0) {
                        r3.f6484 += inflate;
                        buffer.f6474 += (long) inflate;
                        return (long) inflate;
                    } else if (this.f16449.finished() || this.f16449.needsDictionary()) {
                        m20498();
                        if (r3.f6482 == r3.f6484) {
                            buffer.f6475 = r3.m7284();
                            SegmentPool.m7291(r3);
                        }
                        return -1;
                    }
                } catch (DataFormatException e) {
                    throw new IOException(e);
                }
            } while (!r2);
            throw new EOFException("source exhausted prematurely");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20501() {
        return this.f16452.m20569();
    }
}
