package okio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;

public final class Okio {

    /* renamed from: 龘  reason: contains not printable characters */
    static final Logger f16453 = Logger.getLogger(Okio.class.getName());

    private Okio() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Sink m20502(File file) throws FileNotFoundException {
        if (file != null) {
            return m20509((OutputStream) new FileOutputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Source m20503(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getInputStream() == null) {
            throw new IOException("socket's input stream == null");
        } else {
            AsyncTimeout r1 = m20504(socket);
            return r1.m7205(m20514(socket.getInputStream(), (Timeout) r1));
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static AsyncTimeout m20504(final Socket socket) {
        return new AsyncTimeout() {
            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public IOException m20520(@Nullable IOException iOException) {
                SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
                if (iOException != null) {
                    socketTimeoutException.initCause(iOException);
                }
                return socketTimeoutException;
            }

            /* access modifiers changed from: protected */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m20521() {
                try {
                    socket.close();
                } catch (Exception e) {
                    Okio.f16453.log(Level.WARNING, "Failed to close timed out socket " + socket, e);
                } catch (AssertionError e2) {
                    if (Okio.m20515(e2)) {
                        Okio.f16453.log(Level.WARNING, "Failed to close timed out socket " + socket, e2);
                        return;
                    }
                    throw e2;
                }
            }
        };
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Sink m20505(File file) throws FileNotFoundException {
        if (file != null) {
            return m20509((OutputStream) new FileOutputStream(file, true));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static BufferedSink m20506(Sink sink) {
        return new RealBufferedSink(sink);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static BufferedSource m20507(Source source) {
        return new RealBufferedSource(source);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Sink m20508() {
        return new Sink() {
            public void a_(Buffer buffer, long j) throws IOException {
                buffer.m7230(j);
            }

            public void close() throws IOException {
            }

            public void flush() throws IOException {
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Timeout m20519() {
                return Timeout.f16468;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Sink m20509(OutputStream outputStream) {
        return m20510(outputStream, new Timeout());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Sink m20510(final OutputStream outputStream, final Timeout timeout) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (timeout != null) {
            return new Sink() {
                public void a_(Buffer buffer, long j) throws IOException {
                    Util.m20581(buffer.f6474, 0, j);
                    while (j > 0) {
                        Timeout.this.m20571();
                        Segment segment = buffer.f6475;
                        int min = (int) Math.min(j, (long) (segment.f6484 - segment.f6482));
                        outputStream.write(segment.f6485, segment.f6482, min);
                        segment.f6482 += min;
                        j -= (long) min;
                        buffer.f6474 -= (long) min;
                        if (segment.f6482 == segment.f6484) {
                            buffer.f6475 = segment.m7284();
                            SegmentPool.m7291(segment);
                        }
                    }
                }

                public void close() throws IOException {
                    outputStream.close();
                }

                public void flush() throws IOException {
                    outputStream.flush();
                }

                public String toString() {
                    return "sink(" + outputStream + ")";
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public Timeout m20516() {
                    return Timeout.this;
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Sink m20511(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getOutputStream() == null) {
            throw new IOException("socket's output stream == null");
        } else {
            AsyncTimeout r1 = m20504(socket);
            return r1.m7204(m20510(socket.getOutputStream(), (Timeout) r1));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Source m20512(File file) throws FileNotFoundException {
        if (file != null) {
            return m20513((InputStream) new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Source m20513(InputStream inputStream) {
        return m20514(inputStream, new Timeout());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Source m20514(final InputStream inputStream, final Timeout timeout) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (timeout != null) {
            return new Source() {
                public void close() throws IOException {
                    inputStream.close();
                }

                public String toString() {
                    return "source(" + inputStream + ")";
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public long m20517(Buffer buffer, long j) throws IOException {
                    if (j < 0) {
                        throw new IllegalArgumentException("byteCount < 0: " + j);
                    } else if (j == 0) {
                        return 0;
                    } else {
                        try {
                            Timeout.this.m20571();
                            Segment r3 = buffer.m7209(1);
                            int read = inputStream.read(r3.f6485, r3.f6484, (int) Math.min(j, (long) (8192 - r3.f6484)));
                            if (read == -1) {
                                return -1;
                            }
                            r3.f6484 += read;
                            buffer.f6474 += (long) read;
                            return (long) read;
                        } catch (AssertionError e) {
                            if (Okio.m20515(e)) {
                                throw new IOException(e);
                            }
                            throw e;
                        }
                    }
                }

                /* renamed from: 龘  reason: contains not printable characters */
                public Timeout m20518() {
                    return Timeout.this;
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m20515(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
