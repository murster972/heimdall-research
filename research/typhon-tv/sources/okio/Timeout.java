package okio;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class Timeout {

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Timeout f16468 = new Timeout() {
        /* renamed from: ʼ  reason: contains not printable characters */
        public void m20575() throws IOException {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20576(long j) {
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Timeout m20577(long j, TimeUnit timeUnit) {
            return this;
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    private long f16469;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f16470;

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean f16471;

    public long A_() {
        return this.f16470;
    }

    public boolean B_() {
        return this.f16471;
    }

    public Timeout C_() {
        this.f16470 = 0;
        return this;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Timeout m20570() {
        this.f16471 = false;
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m20571() throws IOException {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f16471 && this.f16469 - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public long m20572() {
        if (this.f16471) {
            return this.f16469;
        }
        throw new IllegalStateException("No deadline");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20573(long j) {
        this.f16471 = true;
        this.f16469 = j;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20574(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            this.f16470 = timeUnit.toNanos(j);
            return this;
        }
    }
}
