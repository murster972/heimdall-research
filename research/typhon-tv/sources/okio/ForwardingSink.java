package okio;

import java.io.IOException;

public abstract class ForwardingSink implements Sink {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Sink f16436;

    public ForwardingSink(Sink sink) {
        if (sink == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f16436 = sink;
    }

    public void a_(Buffer buffer, long j) throws IOException {
        this.f16436.a_(buffer, j);
    }

    public void close() throws IOException {
        this.f16436.close();
    }

    public void flush() throws IOException {
        this.f16436.flush();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f16436.toString() + ")";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20477() {
        return this.f16436.m20567();
    }
}
