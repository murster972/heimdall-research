package okio;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import javax.annotation.Nullable;

public final class Buffer implements Cloneable, ByteChannel, BufferedSink, BufferedSource {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final byte[] f6473 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: 靐  reason: contains not printable characters */
    long f6474;
    @Nullable

    /* renamed from: 龘  reason: contains not printable characters */
    Segment f6475;

    public void a_(Buffer buffer, long j) {
        if (buffer == null) {
            throw new IllegalArgumentException("source == null");
        } else if (buffer == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            Util.m20581(buffer.f6474, 0, j);
            while (j > 0) {
                if (j < ((long) (buffer.f6475.f6484 - buffer.f6475.f6482))) {
                    Segment segment = this.f6475 != null ? this.f6475.f6480 : null;
                    if (segment != null && segment.f6481) {
                        if ((j + ((long) segment.f6484)) - ((long) (segment.f6483 ? 0 : segment.f6482)) <= PlaybackStateCompat.ACTION_PLAY_FROM_URI) {
                            buffer.f6475.m7289(segment, (int) j);
                            buffer.f6474 -= j;
                            this.f6474 += j;
                            return;
                        }
                    }
                    buffer.f6475 = buffer.f6475.m7287((int) j);
                }
                Segment segment2 = buffer.f6475;
                long j2 = (long) (segment2.f6484 - segment2.f6482);
                buffer.f6475 = segment2.m7284();
                if (this.f6475 == null) {
                    this.f6475 = segment2;
                    Segment segment3 = this.f6475;
                    Segment segment4 = this.f6475;
                    Segment segment5 = this.f6475;
                    segment4.f6480 = segment5;
                    segment3.f6479 = segment5;
                } else {
                    this.f6475.f6480.m7288(segment2).m7285();
                }
                buffer.f6474 -= j2;
                this.f6474 += j2;
                j -= j2;
            }
        }
    }

    public void close() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006c, code lost:
        if (r8 != r11.f6484) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006e, code lost:
        r11 = r11.f6479;
        r5 = r11.f6482;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0074, code lost:
        if (r10 != r12.f6484) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0076, code lost:
        r12 = r12.f6479;
        r9 = r12.f6482;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007a, code lost:
        r6 = r6 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007e, code lost:
        r9 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0080, code lost:
        r5 = r8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r19) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x0008
            r14 = 1
        L_0x0007:
            return r14
        L_0x0008:
            r0 = r19
            boolean r14 = r0 instanceof okio.Buffer
            if (r14 != 0) goto L_0x0010
            r14 = 0
            goto L_0x0007
        L_0x0010:
            r13 = r19
            okio.Buffer r13 = (okio.Buffer) r13
            r0 = r18
            long r14 = r0.f6474
            long r0 = r13.f6474
            r16 = r0
            int r14 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r14 == 0) goto L_0x0022
            r14 = 0
            goto L_0x0007
        L_0x0022:
            r0 = r18
            long r14 = r0.f6474
            r16 = 0
            int r14 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r14 != 0) goto L_0x002e
            r14 = 1
            goto L_0x0007
        L_0x002e:
            r0 = r18
            okio.Segment r11 = r0.f6475
            okio.Segment r12 = r13.f6475
            int r5 = r11.f6482
            int r9 = r12.f6482
            r6 = 0
        L_0x003a:
            r0 = r18
            long r14 = r0.f6474
            int r14 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r14 >= 0) goto L_0x007c
            int r14 = r11.f6484
            int r14 = r14 - r5
            int r15 = r12.f6484
            int r15 = r15 - r9
            int r14 = java.lang.Math.min(r14, r15)
            long r2 = (long) r14
            r4 = 0
            r10 = r9
            r8 = r5
        L_0x0050:
            long r14 = (long) r4
            int r14 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            if (r14 >= 0) goto L_0x006a
            byte[] r14 = r11.f6485
            int r5 = r8 + 1
            byte r14 = r14[r8]
            byte[] r15 = r12.f6485
            int r9 = r10 + 1
            byte r15 = r15[r10]
            if (r14 == r15) goto L_0x0065
            r14 = 0
            goto L_0x0007
        L_0x0065:
            int r4 = r4 + 1
            r10 = r9
            r8 = r5
            goto L_0x0050
        L_0x006a:
            int r14 = r11.f6484
            if (r8 != r14) goto L_0x0080
            okio.Segment r11 = r11.f6479
            int r5 = r11.f6482
        L_0x0072:
            int r14 = r12.f6484
            if (r10 != r14) goto L_0x007e
            okio.Segment r12 = r12.f6479
            int r9 = r12.f6482
        L_0x007a:
            long r6 = r6 + r2
            goto L_0x003a
        L_0x007c:
            r14 = 1
            goto L_0x0007
        L_0x007e:
            r9 = r10
            goto L_0x007a
        L_0x0080:
            r5 = r8
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.equals(java.lang.Object):boolean");
    }

    public void flush() {
    }

    public int hashCode() {
        Segment segment = this.f6475;
        if (segment == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = segment.f6484;
            for (int i3 = segment.f6482; i3 < i2; i3++) {
                i = (i * 31) + segment.f6485[i3];
            }
            segment = segment.f6479;
        } while (segment != this.f6475);
        return i;
    }

    public boolean isOpen() {
        return true;
    }

    public int read(ByteBuffer byteBuffer) throws IOException {
        Segment segment = this.f6475;
        if (segment == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), segment.f6484 - segment.f6482);
        byteBuffer.put(segment.f6485, segment.f6482, min);
        segment.f6482 += min;
        this.f6474 -= (long) min;
        if (segment.f6482 != segment.f6484) {
            return min;
        }
        this.f6475 = segment.m7284();
        SegmentPool.m7291(segment);
        return min;
    }

    public String toString() {
        return m7231().toString();
    }

    public int write(ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer == null) {
            throw new IllegalArgumentException("source == null");
        }
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            Segment r2 = m7209(1);
            int min = Math.min(i, 8192 - r2.f6484);
            byteBuffer.get(r2.f6485, r2.f6484, min);
            i -= min;
            r2.f6484 += min;
        }
        this.f6474 += (long) remaining;
        return remaining;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m7208(long j) throws EOFException {
        if (j < 0) {
            throw new IllegalArgumentException("limit < 0: " + j);
        }
        long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
        long r14 = m7259((byte) 10, 0, j2);
        if (r14 != -1) {
            return m7212(r14);
        }
        if (j2 < m7242()) {
            if (m7252(j2 - 1) == 13 && m7252(j2) == 10) {
                return m7212(j2);
            }
        }
        Buffer buffer = new Buffer();
        m7269(buffer, 0, Math.min(32, m7242()));
        throw new EOFException("\\n not found: limit=" + Math.min(m7242(), j) + " content=" + buffer.m7277().hex() + 8230);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Segment m7209(int i) {
        if (i < 1 || i > 8192) {
            throw new IllegalArgumentException();
        } else if (this.f6475 == null) {
            this.f6475 = SegmentPool.m7290();
            Segment segment = this.f6475;
            Segment segment2 = this.f6475;
            Segment segment3 = this.f6475;
            segment2.f6480 = segment3;
            segment.f6479 = segment3;
            return segment3;
        } else {
            Segment segment4 = this.f6475.f6480;
            return (segment4.f6484 + i > 8192 || !segment4.f6481) ? segment4.m7288(SegmentPool.m7290()) : segment4;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m7210() {
        return this.f6474 == 0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public InputStream m7211() {
        return new InputStream() {
            public int available() {
                return (int) Math.min(Buffer.this.f6474, 2147483647L);
            }

            public void close() {
            }

            public int read() {
                if (Buffer.this.f6474 > 0) {
                    return Buffer.this.m7228() & 255;
                }
                return -1;
            }

            public int read(byte[] bArr, int i, int i2) {
                return Buffer.this.m7257(bArr, i, i2);
            }

            public String toString() {
                return Buffer.this + ".inputStream()";
            }
        };
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public String m7212(long j) throws EOFException {
        if (j <= 0 || m7252(j - 1) != 13) {
            String r0 = m7239(j);
            m7230(1);
            String str = r0;
            return r0;
        }
        String r02 = m7239(j - 1);
        m7230(2);
        String str2 = r02;
        return r02;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public ByteString m7213(int i) {
        return i == 0 ? ByteString.EMPTY : new SegmentedByteString(this, i);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public long m7214() {
        long j = this.f6474;
        if (j == 0) {
            long j2 = j;
            return 0;
        }
        Segment segment = this.f6475.f6480;
        if (segment.f6484 < 8192 && segment.f6481) {
            j -= (long) (segment.f6484 - segment.f6482);
        }
        long j3 = j;
        return j;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public byte[] m7216(long j) throws EOFException {
        Util.m20581(this.f6474, 0, j);
        if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        }
        byte[] bArr = new byte[((int) j)];
        m7273(bArr);
        return bArr;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m7217() {
        return Util.m20579(m7236());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0068, code lost:
        if (r10 != false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006a, code lost:
        r5.m7228();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008a, code lost:
        throw new java.lang.NumberFormatException("Number too large: " + r5.m7224());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c8, code lost:
        if (r11 != r9) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ca, code lost:
        r24.f6475 = r17.m7284();
        okio.SegmentPool.m7291(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00d7, code lost:
        if (r8 != false) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f7, code lost:
        r17.f6482 = r11;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00a9 A[SYNTHETIC] */
    /* renamed from: ʿ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m7219() {
        /*
            r24 = this;
            r0 = r24
            long r0 = r0.f6474
            r20 = r0
            r22 = 0
            int r20 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
            if (r20 != 0) goto L_0x0015
            java.lang.IllegalStateException r20 = new java.lang.IllegalStateException
            java.lang.String r21 = "size == 0"
            r20.<init>(r21)
            throw r20
        L_0x0015:
            r18 = 0
            r16 = 0
            r10 = 0
            r8 = 0
            r14 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            r12 = -7
        L_0x0022:
            r0 = r24
            okio.Segment r0 = r0.f6475
            r17 = r0
            r0 = r17
            byte[] r6 = r0.f6485
            r0 = r17
            int r11 = r0.f6482
            r0 = r17
            int r9 = r0.f6484
        L_0x0034:
            if (r11 >= r9) goto L_0x00c8
            byte r4 = r6[r11]
            r20 = 48
            r0 = r20
            if (r4 < r0) goto L_0x0099
            r20 = 57
            r0 = r20
            if (r4 > r0) goto L_0x0099
            int r7 = 48 - r4
            int r20 = (r18 > r14 ? 1 : (r18 == r14 ? 0 : -1))
            if (r20 < 0) goto L_0x0055
            int r20 = (r18 > r14 ? 1 : (r18 == r14 ? 0 : -1))
            if (r20 != 0) goto L_0x008b
            long r0 = (long) r7
            r20 = r0
            int r20 = (r20 > r12 ? 1 : (r20 == r12 ? 0 : -1))
            if (r20 >= 0) goto L_0x008b
        L_0x0055:
            okio.Buffer r20 = new okio.Buffer
            r20.<init>()
            r0 = r20
            r1 = r18
            okio.Buffer r20 = r0.m7218((long) r1)
            r0 = r20
            okio.Buffer r5 = r0.m7238((int) r4)
            if (r10 != 0) goto L_0x006d
            r5.m7228()
        L_0x006d:
            java.lang.NumberFormatException r20 = new java.lang.NumberFormatException
            java.lang.StringBuilder r21 = new java.lang.StringBuilder
            r21.<init>()
            java.lang.String r22 = "Number too large: "
            java.lang.StringBuilder r21 = r21.append(r22)
            java.lang.String r22 = r5.m7224()
            java.lang.StringBuilder r21 = r21.append(r22)
            java.lang.String r21 = r21.toString()
            r20.<init>(r21)
            throw r20
        L_0x008b:
            r20 = 10
            long r18 = r18 * r20
            long r0 = (long) r7
            r20 = r0
            long r18 = r18 + r20
        L_0x0094:
            int r11 = r11 + 1
            int r16 = r16 + 1
            goto L_0x0034
        L_0x0099:
            r20 = 45
            r0 = r20
            if (r4 != r0) goto L_0x00a7
            if (r16 != 0) goto L_0x00a7
            r10 = 1
            r20 = 1
            long r12 = r12 - r20
            goto L_0x0094
        L_0x00a7:
            if (r16 != 0) goto L_0x00c7
            java.lang.NumberFormatException r20 = new java.lang.NumberFormatException
            java.lang.StringBuilder r21 = new java.lang.StringBuilder
            r21.<init>()
            java.lang.String r22 = "Expected leading [0-9] or '-' character but was 0x"
            java.lang.StringBuilder r21 = r21.append(r22)
            java.lang.String r22 = java.lang.Integer.toHexString(r4)
            java.lang.StringBuilder r21 = r21.append(r22)
            java.lang.String r21 = r21.toString()
            r20.<init>(r21)
            throw r20
        L_0x00c7:
            r8 = 1
        L_0x00c8:
            if (r11 != r9) goto L_0x00f7
            okio.Segment r20 = r17.m7284()
            r0 = r20
            r1 = r24
            r1.f6475 = r0
            okio.SegmentPool.m7291(r17)
        L_0x00d7:
            if (r8 != 0) goto L_0x00e1
            r0 = r24
            okio.Segment r0 = r0.f6475
            r20 = r0
            if (r20 != 0) goto L_0x0022
        L_0x00e1:
            r0 = r24
            long r0 = r0.f6474
            r20 = r0
            r0 = r16
            long r0 = (long) r0
            r22 = r0
            long r20 = r20 - r22
            r0 = r20
            r2 = r24
            r2.f6474 = r0
            if (r10 == 0) goto L_0x00fc
        L_0x00f6:
            return r18
        L_0x00f7:
            r0 = r17
            r0.f6482 = r11
            goto L_0x00d7
        L_0x00fc:
            r0 = r18
            long r0 = -r0
            r18 = r0
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.m7219():long");
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public byte[] m7220() {
        try {
            return m7216(this.f6474);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public short m7222() {
        return Util.m20580(m7235());
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public void m7223() {
        try {
            m7230(this.f6474);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public String m7224() {
        try {
            return m7263(this.f6474, Util.f16472);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public String m7225() throws EOFException {
        return m7208(Long.MAX_VALUE);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m7226() throws EOFException {
        byte b;
        int i;
        byte b2;
        if (this.f6474 == 0) {
            throw new EOFException();
        }
        byte r1 = m7252(0);
        if ((r1 & 128) == 0) {
            b = r1 & Byte.MAX_VALUE;
            i = 1;
            b2 = 0;
        } else if ((r1 & 224) == 192) {
            b = r1 & 31;
            i = 2;
            b2 = 128;
        } else if ((r1 & 240) == 224) {
            b = r1 & 15;
            i = 3;
            b2 = 2048;
        } else if ((r1 & 248) == 240) {
            b = r1 & 7;
            i = 4;
            b2 = 65536;
        } else {
            m7230(1);
            return 65533;
        }
        if (this.f6474 < ((long) i)) {
            throw new EOFException("size < " + i + ": " + this.f6474 + " (to read code point prefixed 0x" + Integer.toHexString(r1) + ")");
        }
        int i2 = 1;
        while (i2 < i) {
            byte r0 = m7252((long) i2);
            if ((r0 & 192) == 128) {
                b = (b << 6) | (r0 & 63);
                i2++;
            } else {
                m7230((long) i2);
                return 65533;
            }
        }
        m7230((long) i);
        if (b > 1114111) {
            return 65533;
        }
        if (b >= 55296 && b <= 57343) {
            return 65533;
        }
        if (b < b2) {
            return 65533;
        }
        return b;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public Buffer clone() {
        Buffer buffer = new Buffer();
        if (this.f6474 != 0) {
            buffer.f6475 = this.f6475.m7286();
            Segment segment = buffer.f6475;
            Segment segment2 = buffer.f6475;
            Segment segment3 = buffer.f6475;
            segment2.f6480 = segment3;
            segment.f6479 = segment3;
            for (Segment segment4 = this.f6475.f6479; segment4 != this.f6475; segment4 = segment4.f6479) {
                buffer.f6475.f6480.m7288(segment4.m7286());
            }
            buffer.f6474 = this.f6474;
        }
        return buffer;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public byte m7228() {
        if (this.f6474 == 0) {
            throw new IllegalStateException("size == 0");
        }
        Segment segment = this.f6475;
        int i = segment.f6482;
        int i2 = segment.f6484;
        int i3 = i + 1;
        byte b = segment.f6485[i];
        this.f6474--;
        if (i3 == i2) {
            this.f6475 = segment.m7284();
            SegmentPool.m7291(segment);
        } else {
            segment.f6482 = i3;
        }
        return b;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m7230(long j) throws EOFException {
        while (j > 0) {
            if (this.f6475 == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f6475.f6484 - this.f6475.f6482));
            this.f6474 -= (long) min;
            j -= (long) min;
            this.f6475.f6482 += min;
            if (this.f6475.f6482 == this.f6475.f6484) {
                Segment segment = this.f6475;
                this.f6475 = segment.m7284();
                SegmentPool.m7291(segment);
            }
        }
    }

    /* renamed from: י  reason: contains not printable characters */
    public ByteString m7231() {
        if (this.f6474 <= 2147483647L) {
            return m7213((int) this.f6474);
        }
        throw new IllegalArgumentException("size > Integer.MAX_VALUE: " + this.f6474);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Buffer m7218(long j) {
        if (j == 0) {
            return m7238(48);
        }
        boolean z = false;
        if (j < 0) {
            j = -j;
            if (j < 0) {
                return m7246("-9223372036854775808");
            }
            z = true;
        }
        int i = j < 100000000 ? j < 10000 ? j < 100 ? j < 10 ? 1 : 2 : j < 1000 ? 3 : 4 : j < C.MICROS_PER_SECOND ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8 : j < 1000000000000L ? j < 10000000000L ? j < C.NANOS_PER_SECOND ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
        if (z) {
            i++;
        }
        Segment r4 = m7209(i);
        byte[] bArr = r4.f6485;
        int i2 = r4.f6484 + i;
        while (j != 0) {
            i2--;
            bArr[i2] = f6473[(int) (j % 10)];
            j /= 10;
        }
        if (z) {
            bArr[i2 - 1] = 45;
        }
        r4.f6484 += i;
        this.f6474 += (long) i;
        return this;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public short m7235() {
        if (this.f6474 < 2) {
            throw new IllegalStateException("size < 2: " + this.f6474);
        }
        Segment segment = this.f6475;
        int i = segment.f6482;
        int i2 = segment.f6484;
        if (i2 - i < 2) {
            return (short) (((m7228() & 255) << 8) | (m7228() & 255));
        }
        byte[] bArr = segment.f6485;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
        this.f6474 -= 2;
        if (i4 == i2) {
            this.f6475 = segment.m7284();
            SegmentPool.m7291(segment);
        } else {
            segment.f6482 = i4;
        }
        return (short) b;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int m7236() {
        if (this.f6474 < 4) {
            throw new IllegalStateException("size < 4: " + this.f6474);
        }
        Segment segment = this.f6475;
        int i = segment.f6482;
        int i2 = segment.f6484;
        if (i2 - i < 4) {
            return ((m7228() & 255) << 24) | ((m7228() & 255) << 16) | ((m7228() & 255) << 8) | (m7228() & 255);
        }
        byte[] bArr = segment.f6485;
        int i3 = i + 1;
        int i4 = i3 + 1;
        int i5 = i4 + 1;
        int i6 = i5 + 1;
        byte b = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16) | ((bArr[i4] & 255) << 8) | (bArr[i5] & 255);
        this.f6474 -= 4;
        if (i6 == i2) {
            this.f6475 = segment.m7284();
            SegmentPool.m7291(segment);
            return b;
        }
        segment.f6482 = i6;
        return b;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Buffer m7221(long j) {
        if (j == 0) {
            return m7238(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        Segment r3 = m7209(numberOfTrailingZeros);
        byte[] bArr = r3.f6485;
        int i = r3.f6484;
        for (int i2 = (r3.f6484 + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = f6473[(int) (15 & j)];
            j >>>= 4;
        }
        r3.f6484 += numberOfTrailingZeros;
        this.f6474 += (long) numberOfTrailingZeros;
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m7239(long j) throws EOFException {
        return m7263(j, Util.f16472);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Buffer m7232() {
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Buffer m7215(int i) {
        return m7229(Util.m20579(i));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m7242() {
        return this.f6474;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Buffer m7238(int i) {
        Segment r0 = m7209(1);
        byte[] bArr = r0.f6485;
        int i2 = r0.f6484;
        r0.f6484 = i2 + 1;
        bArr[i2] = (byte) i;
        this.f6474++;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Buffer m7255(byte[] bArr) {
        if (bArr != null) {
            return m7256(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Buffer m7256(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        Util.m20581((long) bArr.length, (long) i, (long) i2);
        int i3 = i + i2;
        while (i < i3) {
            Segment r7 = m7209(1);
            int min = Math.min(i3 - i, 8192 - r7.f6484);
            System.arraycopy(bArr, i, r7.f6485, r7.f6484, min);
            i += min;
            r7.f6484 += min;
        }
        this.f6474 += (long) i2;
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m7248(long j) {
        return this.f6474 >= j;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public OutputStream m7249() {
        return new OutputStream() {
            public void close() {
            }

            public void flush() {
            }

            public String toString() {
                return Buffer.this + ".outputStream()";
            }

            public void write(int i) {
                Buffer.this.m7238((int) (byte) i);
            }

            public void write(byte[] bArr, int i, int i2) {
                Buffer.this.m7256(bArr, i, i2);
            }
        };
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Buffer m7229(int i) {
        Segment r3 = m7209(4);
        byte[] bArr = r3.f6485;
        int i2 = r3.f6484;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        r3.f6484 = i5 + 1;
        this.f6474 += 4;
        return this;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ByteString m7251(long j) throws EOFException {
        return new ByteString(m7216(j));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public byte m7252(long j) {
        Util.m20581(this.f6474, j, 1);
        if (this.f6474 - j > j) {
            Segment segment = this.f6475;
            while (true) {
                int i = segment.f6484 - segment.f6482;
                if (j < ((long) i)) {
                    return segment.f6485[segment.f6482 + ((int) j)];
                }
                j -= (long) i;
                segment = segment.f6479;
            }
        } else {
            long j2 = j - this.f6474;
            Segment segment2 = this.f6475.f6480;
            while (true) {
                j2 += (long) (segment2.f6484 - segment2.f6482);
                if (j2 >= 0) {
                    return segment2.f6485[segment2.f6482 + ((int) j2)];
                }
                segment2 = segment2.f6480;
            }
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Buffer m7253() {
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Buffer m7234(int i) {
        Segment r3 = m7209(2);
        byte[] bArr = r3.f6485;
        int i2 = r3.f6484;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        r3.f6484 = i3 + 1;
        this.f6474 += 2;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m7257(byte[] bArr, int i, int i2) {
        Util.m20581((long) bArr.length, (long) i, (long) i2);
        Segment segment = this.f6475;
        if (segment == null) {
            return -1;
        }
        int min = Math.min(i2, segment.f6484 - segment.f6482);
        System.arraycopy(segment.f6485, segment.f6482, bArr, i, min);
        segment.f6482 += min;
        this.f6474 -= (long) min;
        if (segment.f6482 != segment.f6484) {
            return min;
        }
        this.f6475 = segment.m7284();
        SegmentPool.m7291(segment);
        return min;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m7258(byte b) {
        return m7259(b, 0, Long.MAX_VALUE);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m7259(byte b, long j, long j2) {
        Segment segment;
        long j3;
        if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("size=%s fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(this.f6474), Long.valueOf(j), Long.valueOf(j2)}));
        }
        if (j2 > this.f6474) {
            j2 = this.f6474;
        }
        if (j == j2 || (segment = this.f6475) == null) {
            return -1;
        }
        if (this.f6474 - j >= j) {
            long j4 = 0;
            while (true) {
                long j5 = j3 + ((long) (segment.f6484 - segment.f6482));
                if (j5 >= j) {
                    break;
                }
                segment = segment.f6479;
                j4 = j5;
            }
        } else {
            j3 = this.f6474;
            while (j3 > j) {
                segment = segment.f6480;
                j3 -= (long) (segment.f6484 - segment.f6482);
            }
        }
        while (j3 < j2) {
            byte[] bArr = segment.f6485;
            int min = (int) Math.min((long) segment.f6484, (((long) segment.f6482) + j2) - j3);
            for (int i = (int) ((((long) segment.f6482) + j) - j3); i < min; i++) {
                if (bArr[i] == b) {
                    return ((long) (i - segment.f6482)) + j3;
                }
            }
            j3 += (long) (segment.f6484 - segment.f6482);
            j = j3;
            segment = segment.f6479;
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m7260(Buffer buffer, long j) {
        if (buffer == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f6474 == 0) {
            long j2 = j;
            return -1;
        } else {
            if (j > this.f6474) {
                j = this.f6474;
            }
            buffer.a_(this, j);
            long j3 = j;
            return j;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m7261(Sink sink) throws IOException {
        long j = this.f6474;
        if (j > 0) {
            sink.a_(this, j);
        }
        return j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m7262(Source source) throws IOException {
        if (source == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long r0 = source.m20568(this, PlaybackStateCompat.ACTION_PLAY_FROM_URI);
            if (r0 == -1) {
                return j;
            }
            j += r0;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7263(long j, Charset charset) throws EOFException {
        Util.m20581(this.f6474, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            Segment segment = this.f6475;
            if (((long) segment.f6482) + j > ((long) segment.f6484)) {
                return new String(m7216(j), charset);
            }
            String str = new String(segment.f6485, segment.f6482, (int) j, charset);
            segment.f6482 = (int) (((long) segment.f6482) + j);
            this.f6474 -= j;
            if (segment.f6482 != segment.f6484) {
                return str;
            }
            this.f6475 = segment.m7284();
            SegmentPool.m7291(segment);
            return str;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m7264(Charset charset) {
        try {
            return m7263(this.f6474, charset);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Buffer m7265(int i) {
        if (i < 128) {
            m7238(i);
        } else if (i < 2048) {
            m7238((i >> 6) | PsExtractor.AUDIO_STREAM);
            m7238((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                m7238((i >> 12) | 224);
                m7238(((i >> 6) & 63) | 128);
                m7238((i & 63) | 128);
            } else {
                m7238(63);
            }
        } else if (i <= 1114111) {
            m7238((i >> 18) | 240);
            m7238(((i >> 12) & 63) | 128);
            m7238(((i >> 6) & 63) | 128);
            m7238((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Buffer m7246(String str) {
        return m7267(str, 0, str.length());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Buffer m7267(String str, int i, int i2) {
        char c;
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalArgumentException("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 > str.length()) {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        } else {
            int i3 = i;
            while (true) {
                int i4 = i3;
                if (i4 >= i2) {
                    return this;
                }
                char charAt = str.charAt(i4);
                if (charAt < 128) {
                    Segment r11 = m7209(1);
                    byte[] bArr = r11.f6485;
                    int i5 = r11.f6484 - i4;
                    int min = Math.min(i2, 8192 - i5);
                    bArr[i5 + i4] = (byte) charAt;
                    int i6 = i4 + 1;
                    while (i6 < min) {
                        char charAt2 = str.charAt(i6);
                        if (charAt2 >= 128) {
                            break;
                        }
                        bArr[i5 + i6] = (byte) charAt2;
                        i6++;
                    }
                    int i7 = (i6 + i5) - r11.f6484;
                    r11.f6484 += i7;
                    this.f6474 += (long) i7;
                    i3 = i6;
                } else if (charAt < 2048) {
                    m7238((charAt >> 6) | PsExtractor.AUDIO_STREAM);
                    m7238((int) (charAt & '?') | 128);
                    i3 = i4 + 1;
                } else if (charAt < 55296 || charAt > 57343) {
                    m7238((charAt >> 12) | 224);
                    m7238(((charAt >> 6) & 63) | 128);
                    m7238((int) (charAt & '?') | 128);
                    i3 = i4 + 1;
                } else {
                    if (i4 + 1 < i2) {
                        c = str.charAt(i4 + 1);
                    } else {
                        c = 0;
                    }
                    if (charAt > 56319 || c < 56320 || c > 57343) {
                        m7238(63);
                        i3 = i4 + 1;
                    } else {
                        int i8 = 0 + (((10239 & charAt) << 10) | (9215 & c));
                        m7238((i8 >> 18) | 240);
                        m7238(((i8 >> 12) & 63) | 128);
                        m7238(((i8 >> 6) & 63) | 128);
                        m7238((i8 & 63) | 128);
                        i3 = i4 + 2;
                    }
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Buffer m7268(String str, int i, int i2, Charset charset) {
        if (str == null) {
            throw new IllegalArgumentException("string == null");
        } else if (i < 0) {
            throw new IllegalAccessError("beginIndex < 0: " + i);
        } else if (i2 < i) {
            throw new IllegalArgumentException("endIndex < beginIndex: " + i2 + " < " + i);
        } else if (i2 > str.length()) {
            throw new IllegalArgumentException("endIndex > string.length: " + i2 + " > " + str.length());
        } else if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (charset.equals(Util.f16472)) {
            return m7267(str, i, i2);
        } else {
            byte[] bytes = str.substring(i, i2).getBytes(charset);
            return m7256(bytes, 0, bytes.length);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Buffer m7269(Buffer buffer, long j, long j2) {
        if (buffer == null) {
            throw new IllegalArgumentException("out == null");
        }
        Util.m20581(this.f6474, j, j2);
        if (j2 != 0) {
            buffer.f6474 += j2;
            Segment segment = this.f6475;
            while (j >= ((long) (segment.f6484 - segment.f6482))) {
                j -= (long) (segment.f6484 - segment.f6482);
                segment = segment.f6479;
            }
            while (j2 > 0) {
                Segment r6 = segment.m7286();
                r6.f6482 = (int) (((long) r6.f6482) + j);
                r6.f6484 = Math.min(r6.f6482 + ((int) j2), r6.f6484);
                if (buffer.f6475 == null) {
                    r6.f6480 = r6;
                    r6.f6479 = r6;
                    buffer.f6475 = r6;
                } else {
                    buffer.f6475.f6480.m7288(r6);
                }
                j2 -= (long) (r6.f6484 - r6.f6482);
                j = 0;
                segment = segment.f6479;
            }
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Buffer m7247(ByteString byteString) {
        if (byteString == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        byteString.m7282(this);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m7271() {
        return Timeout.f16468;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7272(long j) throws EOFException {
        if (this.f6474 < j) {
            throw new EOFException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7273(byte[] bArr) throws EOFException {
        int i = 0;
        while (i < bArr.length) {
            int r1 = m7257(bArr, i, bArr.length - i);
            if (r1 == -1) {
                throw new EOFException();
            }
            i += r1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7274(long j, ByteString byteString) {
        return m7275(j, byteString, 0, byteString.size());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m7275(long j, ByteString byteString, int i, int i2) {
        if (j < 0 || i < 0 || i2 < 0 || this.f6474 - j < ((long) i2) || byteString.size() - i < i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (m7252(((long) i3) + j) != byteString.getByte(i + i3)) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009e, code lost:
        if (r8 != r7) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a0, code lost:
        r18.f6475 = r10.m7284();
        okio.SegmentPool.m7291(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ab, code lost:
        if (r6 != false) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00cb, code lost:
        r10.f6482 = r8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007f A[SYNTHETIC] */
    /* renamed from: ﹶ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m7276() {
        /*
            r18 = this;
            r0 = r18
            long r14 = r0.f6474
            r16 = 0
            int r11 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r11 != 0) goto L_0x0013
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r14 = "size == 0"
            r11.<init>(r14)
            throw r11
        L_0x0013:
            r12 = 0
            r9 = 0
            r6 = 0
        L_0x0017:
            r0 = r18
            okio.Segment r10 = r0.f6475
            byte[] r4 = r10.f6485
            int r8 = r10.f6482
            int r7 = r10.f6484
        L_0x0021:
            if (r8 >= r7) goto L_0x009e
            byte r2 = r4[r8]
            r11 = 48
            if (r2 < r11) goto L_0x0063
            r11 = 57
            if (r2 > r11) goto L_0x0063
            int r5 = r2 + -48
        L_0x002f:
            r14 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r14 = r14 & r12
            r16 = 0
            int r11 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r11 == 0) goto L_0x00c1
            okio.Buffer r11 = new okio.Buffer
            r11.<init>()
            okio.Buffer r11 = r11.m7221((long) r12)
            okio.Buffer r3 = r11.m7238((int) r2)
            java.lang.NumberFormatException r11 = new java.lang.NumberFormatException
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Number too large: "
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r3.m7224()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r11.<init>(r14)
            throw r11
        L_0x0063:
            r11 = 97
            if (r2 < r11) goto L_0x0070
            r11 = 102(0x66, float:1.43E-43)
            if (r2 > r11) goto L_0x0070
            int r11 = r2 + -97
            int r5 = r11 + 10
            goto L_0x002f
        L_0x0070:
            r11 = 65
            if (r2 < r11) goto L_0x007d
            r11 = 70
            if (r2 > r11) goto L_0x007d
            int r11 = r2 + -65
            int r5 = r11 + 10
            goto L_0x002f
        L_0x007d:
            if (r9 != 0) goto L_0x009d
            java.lang.NumberFormatException r11 = new java.lang.NumberFormatException
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Expected leading [0-9a-fA-F] character but was 0x"
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = java.lang.Integer.toHexString(r2)
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r11.<init>(r14)
            throw r11
        L_0x009d:
            r6 = 1
        L_0x009e:
            if (r8 != r7) goto L_0x00cb
            okio.Segment r11 = r10.m7284()
            r0 = r18
            r0.f6475 = r11
            okio.SegmentPool.m7291(r10)
        L_0x00ab:
            if (r6 != 0) goto L_0x00b3
            r0 = r18
            okio.Segment r11 = r0.f6475
            if (r11 != 0) goto L_0x0017
        L_0x00b3:
            r0 = r18
            long r14 = r0.f6474
            long r0 = (long) r9
            r16 = r0
            long r14 = r14 - r16
            r0 = r18
            r0.f6474 = r14
            return r12
        L_0x00c1:
            r11 = 4
            long r12 = r12 << r11
            long r14 = (long) r5
            long r12 = r12 | r14
            int r8 = r8 + 1
            int r9 = r9 + 1
            goto L_0x0021
        L_0x00cb:
            r10.f6482 = r8
            goto L_0x00ab
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.m7276():long");
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public ByteString m7277() {
        return new ByteString(m7220());
    }
}
