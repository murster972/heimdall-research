package okio;

import java.nio.charset.Charset;

final class Util {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Charset f16472 = Charset.forName("UTF-8");

    /* renamed from: 靐  reason: contains not printable characters */
    private static <T extends Throwable> void m20578(Throwable th) throws Throwable {
        throw th;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m20579(int i) {
        return ((-16777216 & i) >>> 24) | ((16711680 & i) >>> 8) | ((65280 & i) << 8) | ((i & 255) << 24);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static short m20580(short s) {
        short s2 = s & 65535;
        return (short) (((65280 & s2) >>> 8) | ((s2 & 255) << 8));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m20581(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException(String.format("size=%s offset=%s byteCount=%s", new Object[]{Long.valueOf(j), Long.valueOf(j2), Long.valueOf(j3)}));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m20582(Throwable th) {
        m20578(th);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m20583(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            if (bArr[i4 + i] != bArr2[i4 + i2]) {
                return false;
            }
        }
        return true;
    }
}
