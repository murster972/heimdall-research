package okio;

import android.support.v4.media.session.PlaybackStateCompat;
import com.google.android.exoplayer2.C;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

public class AsyncTimeout extends Timeout {
    @Nullable

    /* renamed from: 靐  reason: contains not printable characters */
    static AsyncTimeout f6467;

    /* renamed from: 麤  reason: contains not printable characters */
    private static final long f6468 = TimeUnit.MILLISECONDS.toNanos(f6469);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final long f6469 = TimeUnit.SECONDS.toMillis(60);
    @Nullable

    /* renamed from: ʻ  reason: contains not printable characters */
    private AsyncTimeout f6470;

    /* renamed from: ʼ  reason: contains not printable characters */
    private long f6471;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f6472;

    private static final class Watchdog extends Thread {
        Watchdog() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            r0.m7206();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
            L_0x0000:
                java.lang.Class<okio.AsyncTimeout> r2 = okio.AsyncTimeout.class
                monitor-enter(r2)     // Catch:{ InterruptedException -> 0x000e }
                okio.AsyncTimeout r0 = okio.AsyncTimeout.m7197()     // Catch:{ all -> 0x000b }
                if (r0 != 0) goto L_0x0010
                monitor-exit(r2)     // Catch:{ all -> 0x000b }
                goto L_0x0000
            L_0x000b:
                r1 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x000b }
                throw r1     // Catch:{ InterruptedException -> 0x000e }
            L_0x000e:
                r1 = move-exception
                goto L_0x0000
            L_0x0010:
                okio.AsyncTimeout r1 = okio.AsyncTimeout.f6467     // Catch:{ all -> 0x000b }
                if (r0 != r1) goto L_0x0019
                r1 = 0
                okio.AsyncTimeout.f6467 = r1     // Catch:{ all -> 0x000b }
                monitor-exit(r2)     // Catch:{ all -> 0x000b }
                return
            L_0x0019:
                monitor-exit(r2)     // Catch:{ all -> 0x000b }
                r0.m7206()     // Catch:{ InterruptedException -> 0x000e }
                goto L_0x0000
            */
            throw new UnsupportedOperationException("Method not decompiled: okio.AsyncTimeout.Watchdog.run():void");
        }
    }

    @Nullable
    /* renamed from: 连任  reason: contains not printable characters */
    static AsyncTimeout m7197() throws InterruptedException {
        AsyncTimeout asyncTimeout = f6467.f6470;
        if (asyncTimeout == null) {
            long nanoTime = System.nanoTime();
            AsyncTimeout.class.wait(f6469);
            if (f6467.f6470 != null || System.nanoTime() - nanoTime < f6468) {
                return null;
            }
            return f6467;
        }
        long r6 = asyncTimeout.m7198(System.nanoTime());
        if (r6 > 0) {
            long j = r6 / C.MICROS_PER_SECOND;
            AsyncTimeout.class.wait(j, (int) (r6 - (j * C.MICROS_PER_SECOND)));
            return null;
        }
        f6467.f6470 = asyncTimeout.f6470;
        asyncTimeout.f6470 = null;
        return asyncTimeout;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private long m7198(long j) {
        return this.f6471 - j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized void m7199(AsyncTimeout asyncTimeout, long j, boolean z) {
        synchronized (AsyncTimeout.class) {
            if (f6467 == null) {
                f6467 = new AsyncTimeout();
                new Watchdog().start();
            }
            long nanoTime = System.nanoTime();
            if (j != 0 && z) {
                asyncTimeout.f6471 = Math.min(j, asyncTimeout.m20572() - nanoTime) + nanoTime;
            } else if (j != 0) {
                asyncTimeout.f6471 = nanoTime + j;
            } else if (z) {
                asyncTimeout.f6471 = asyncTimeout.m20572();
            } else {
                throw new AssertionError();
            }
            long r4 = asyncTimeout.m7198(nanoTime);
            AsyncTimeout asyncTimeout2 = f6467;
            while (asyncTimeout2.f6470 != null && r4 >= asyncTimeout2.f6470.m7198(nanoTime)) {
                asyncTimeout2 = asyncTimeout2.f6470;
            }
            asyncTimeout.f6470 = asyncTimeout2.f6470;
            asyncTimeout2.f6470 = asyncTimeout;
            if (asyncTimeout2 == f6467) {
                AsyncTimeout.class.notify();
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static synchronized boolean m7200(AsyncTimeout asyncTimeout) {
        boolean z;
        synchronized (AsyncTimeout.class) {
            AsyncTimeout asyncTimeout2 = f6467;
            while (true) {
                if (asyncTimeout2 == null) {
                    z = true;
                    break;
                } else if (asyncTimeout2.f6470 == asyncTimeout) {
                    asyncTimeout2.f6470 = asyncTimeout.f6470;
                    asyncTimeout.f6470 = null;
                    z = false;
                    break;
                } else {
                    asyncTimeout2 = asyncTimeout2.f6470;
                }
            }
        }
        return z;
    }

    public final boolean z_() {
        if (!this.f6472) {
            return false;
        }
        this.f6472 = false;
        return m7200(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public final IOException m7201(IOException iOException) throws IOException {
        return !z_() ? iOException : m7203(iOException);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final void m7202() {
        if (this.f6472) {
            throw new IllegalStateException("Unbalanced enter/exit");
        }
        long A_ = A_();
        boolean B_ = B_();
        if (A_ != 0 || B_) {
            this.f6472 = true;
            m7199(this, A_, B_);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public IOException m7203(@Nullable IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Sink m7204(final Sink sink) {
        return new Sink() {
            public void a_(Buffer buffer, long j) throws IOException {
                Util.m20581(buffer.f6474, 0, j);
                while (j > 0) {
                    long j2 = 0;
                    Segment segment = buffer.f6475;
                    while (true) {
                        if (j2 >= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                            break;
                        }
                        j2 += (long) (segment.f6484 - segment.f6482);
                        if (j2 >= j) {
                            j2 = j;
                            break;
                        }
                        segment = segment.f6479;
                    }
                    AsyncTimeout.this.m7202();
                    try {
                        sink.a_(buffer, j2);
                        j -= j2;
                        AsyncTimeout.this.m7207(true);
                    } catch (IOException e) {
                        throw AsyncTimeout.this.m7201(e);
                    } catch (Throwable th) {
                        AsyncTimeout.this.m7207(false);
                        throw th;
                    }
                }
            }

            public void close() throws IOException {
                AsyncTimeout.this.m7202();
                try {
                    sink.close();
                    AsyncTimeout.this.m7207(true);
                } catch (IOException e) {
                    throw AsyncTimeout.this.m7201(e);
                } catch (Throwable th) {
                    AsyncTimeout.this.m7207(false);
                    throw th;
                }
            }

            public void flush() throws IOException {
                AsyncTimeout.this.m7202();
                try {
                    sink.flush();
                    AsyncTimeout.this.m7207(true);
                } catch (IOException e) {
                    throw AsyncTimeout.this.m7201(e);
                } catch (Throwable th) {
                    AsyncTimeout.this.m7207(false);
                    throw th;
                }
            }

            public String toString() {
                return "AsyncTimeout.sink(" + sink + ")";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Timeout m20431() {
                return AsyncTimeout.this;
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Source m7205(final Source source) {
        return new Source() {
            public void close() throws IOException {
                try {
                    source.close();
                    AsyncTimeout.this.m7207(true);
                } catch (IOException e) {
                    throw AsyncTimeout.this.m7201(e);
                } catch (Throwable th) {
                    AsyncTimeout.this.m7207(false);
                    throw th;
                }
            }

            public String toString() {
                return "AsyncTimeout.source(" + source + ")";
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public long m20432(Buffer buffer, long j) throws IOException {
                AsyncTimeout.this.m7202();
                try {
                    long r2 = source.m20568(buffer, j);
                    AsyncTimeout.this.m7207(true);
                    return r2;
                } catch (IOException e) {
                    throw AsyncTimeout.this.m7201(e);
                } catch (Throwable th) {
                    AsyncTimeout.this.m7207(false);
                    throw th;
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Timeout m20433() {
                return AsyncTimeout.this;
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m7206() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m7207(boolean z) throws IOException {
        if (z_() && z) {
            throw m7203((IOException) null);
        }
    }
}
