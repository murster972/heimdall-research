package okio;

import android.support.v4.media.session.PlaybackStateCompat;
import javax.annotation.Nullable;

final class SegmentPool {

    /* renamed from: 靐  reason: contains not printable characters */
    static long f6486;
    @Nullable

    /* renamed from: 龘  reason: contains not printable characters */
    static Segment f6487;

    private SegmentPool() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Segment m7290() {
        synchronized (SegmentPool.class) {
            if (f6487 == null) {
                return new Segment();
            }
            Segment segment = f6487;
            f6487 = segment.f6479;
            segment.f6479 = null;
            f6486 -= PlaybackStateCompat.ACTION_PLAY_FROM_URI;
            return segment;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m7291(Segment segment) {
        if (segment.f6479 != null || segment.f6480 != null) {
            throw new IllegalArgumentException();
        } else if (!segment.f6483) {
            synchronized (SegmentPool.class) {
                if (f6486 + PlaybackStateCompat.ACTION_PLAY_FROM_URI <= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                    f6486 += PlaybackStateCompat.ACTION_PLAY_FROM_URI;
                    segment.f6479 = f6487;
                    segment.f6484 = 0;
                    segment.f6482 = 0;
                    f6487 = segment;
                }
            }
        }
    }
}
