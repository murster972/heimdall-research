package okio;

import java.io.IOException;
import java.nio.channels.WritableByteChannel;

public interface BufferedSink extends WritableByteChannel, Sink {
    void flush() throws IOException;

    /* renamed from: ʽ  reason: contains not printable characters */
    BufferedSink m20438(int i) throws IOException;

    /* renamed from: ʾ  reason: contains not printable characters */
    BufferedSink m20439(long j) throws IOException;

    /* renamed from: ˈ  reason: contains not printable characters */
    BufferedSink m20440(long j) throws IOException;

    /* renamed from: ˑ  reason: contains not printable characters */
    BufferedSink m20441(int i) throws IOException;

    /* renamed from: ـ  reason: contains not printable characters */
    BufferedSink m20442() throws IOException;

    /* renamed from: ٴ  reason: contains not printable characters */
    BufferedSink m20443(int i) throws IOException;

    /* renamed from: ᐧ  reason: contains not printable characters */
    BufferedSink m20444(int i) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    BufferedSink m20445(String str) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    BufferedSink m20446(ByteString byteString) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    Buffer m20447();

    /* renamed from: 齉  reason: contains not printable characters */
    BufferedSink m20448(byte[] bArr) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    BufferedSink m20449(byte[] bArr, int i, int i2) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    long m20450(Source source) throws IOException;
}
