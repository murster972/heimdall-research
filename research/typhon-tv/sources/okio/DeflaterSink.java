package okio;

import java.io.IOException;
import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

public final class DeflaterSink implements Sink {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Deflater f16433;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f16434;

    /* renamed from: 龘  reason: contains not printable characters */
    private final BufferedSink f16435;

    DeflaterSink(BufferedSink bufferedSink, Deflater deflater) {
        if (bufferedSink == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f16435 = bufferedSink;
            this.f16433 = deflater;
        }
    }

    @IgnoreJRERequirement
    /* renamed from: 龘  reason: contains not printable characters */
    private void m20474(boolean z) throws IOException {
        Segment r2;
        Buffer r0 = this.f16435.m20447();
        while (true) {
            r2 = r0.m7209(1);
            int deflate = z ? this.f16433.deflate(r2.f6485, r2.f6484, 8192 - r2.f6484, 2) : this.f16433.deflate(r2.f6485, r2.f6484, 8192 - r2.f6484);
            if (deflate > 0) {
                r2.f6484 += deflate;
                r0.f6474 += (long) deflate;
                this.f16435.m20442();
            } else if (this.f16433.needsInput()) {
                break;
            }
        }
        if (r2.f6482 == r2.f6484) {
            r0.f6475 = r2.m7284();
            SegmentPool.m7291(r2);
        }
    }

    public void a_(Buffer buffer, long j) throws IOException {
        Util.m20581(buffer.f6474, 0, j);
        while (j > 0) {
            Segment segment = buffer.f6475;
            int min = (int) Math.min(j, (long) (segment.f6484 - segment.f6482));
            this.f16433.setInput(segment.f6485, segment.f6482, min);
            m20474(false);
            buffer.f6474 -= (long) min;
            segment.f6482 += min;
            if (segment.f6482 == segment.f6484) {
                buffer.f6475 = segment.m7284();
                SegmentPool.m7291(segment);
            }
            j -= (long) min;
        }
    }

    public void close() throws IOException {
        if (!this.f16434) {
            Throwable th = null;
            try {
                m20475();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f16433.end();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            try {
                this.f16435.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.f16434 = true;
            if (th != null) {
                Util.m20582(th);
            }
        }
    }

    public void flush() throws IOException {
        m20474(true);
        this.f16435.flush();
    }

    public String toString() {
        return "DeflaterSink(" + this.f16435 + ")";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m20475() throws IOException {
        this.f16433.finish();
        m20474(false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20476() {
        return this.f16435.m20567();
    }
}
