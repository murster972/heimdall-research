package okio;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import java.nio.ByteBuffer;

final class RealBufferedSink implements BufferedSink {

    /* renamed from: 靐  reason: contains not printable characters */
    public final Sink f16459;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f16460;

    /* renamed from: 龘  reason: contains not printable characters */
    public final Buffer f16461 = new Buffer();

    RealBufferedSink(Sink sink) {
        if (sink == null) {
            throw new NullPointerException("sink == null");
        }
        this.f16459 = sink;
    }

    public void a_(Buffer buffer, long j) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.a_(buffer, j);
        m20526();
    }

    public void close() throws IOException {
        if (!this.f16460) {
            Throwable th = null;
            try {
                if (this.f16461.f6474 > 0) {
                    this.f16459.a_(this.f16461, this.f16461.f6474);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f16459.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.f16460 = true;
            if (th != null) {
                Util.m20582(th);
            }
        }
    }

    public void flush() throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        if (this.f16461.f6474 > 0) {
            this.f16459.a_(this.f16461, this.f16461.f6474);
        }
        this.f16459.flush();
    }

    public boolean isOpen() {
        return !this.f16460;
    }

    public String toString() {
        return "buffer(" + this.f16459 + ")";
    }

    public int write(ByteBuffer byteBuffer) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        int write = this.f16461.write(byteBuffer);
        m20526();
        return write;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public BufferedSink m20522(int i) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7215(i);
        return m20526();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public BufferedSink m20523(long j) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7218(j);
        return m20526();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public BufferedSink m20524(long j) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7221(j);
        return m20526();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public BufferedSink m20525(int i) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7229(i);
        return m20526();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public BufferedSink m20526() throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        long r0 = this.f16461.m7214();
        if (r0 > 0) {
            this.f16459.a_(this.f16461, r0);
        }
        return this;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public BufferedSink m20527(int i) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7234(i);
        return m20526();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public BufferedSink m20528(int i) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7238(i);
        return m20526();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public BufferedSink m20529(String str) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7246(str);
        return m20526();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public BufferedSink m20530(ByteString byteString) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7247(byteString);
        return m20526();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Buffer m20531() {
        return this.f16461;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public BufferedSink m20532(byte[] bArr) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7255(bArr);
        return m20526();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public BufferedSink m20533(byte[] bArr, int i, int i2) throws IOException {
        if (this.f16460) {
            throw new IllegalStateException("closed");
        }
        this.f16461.m7256(bArr, i, i2);
        return m20526();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20534(Source source) throws IOException {
        if (source == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long r0 = source.m20568(this.f16461, PlaybackStateCompat.ACTION_PLAY_FROM_URI);
            if (r0 == -1) {
                return j;
            }
            j += r0;
            m20526();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20535() {
        return this.f16459.m20567();
    }
}
