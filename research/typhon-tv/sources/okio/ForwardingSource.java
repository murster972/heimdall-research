package okio;

import java.io.IOException;

public abstract class ForwardingSource implements Source {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Source f16437;

    public ForwardingSource(Source source) {
        if (source == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f16437 = source;
    }

    public void close() throws IOException {
        this.f16437.close();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f16437.toString() + ")";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Source m20478() {
        return this.f16437;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20479(Buffer buffer, long j) throws IOException {
        return this.f16437.m20568(buffer, j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20480() {
        return this.f16437.m20569();
    }
}
