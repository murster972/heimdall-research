package okio;

import javax.annotation.Nullable;

final class Segment {

    /* renamed from: ʻ  reason: contains not printable characters */
    Segment f6479;

    /* renamed from: ʼ  reason: contains not printable characters */
    Segment f6480;

    /* renamed from: 连任  reason: contains not printable characters */
    boolean f6481;

    /* renamed from: 靐  reason: contains not printable characters */
    int f6482;

    /* renamed from: 麤  reason: contains not printable characters */
    boolean f6483;

    /* renamed from: 齉  reason: contains not printable characters */
    int f6484;

    /* renamed from: 龘  reason: contains not printable characters */
    final byte[] f6485;

    Segment() {
        this.f6485 = new byte[8192];
        this.f6481 = true;
        this.f6483 = false;
    }

    Segment(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        this.f6485 = bArr;
        this.f6482 = i;
        this.f6484 = i2;
        this.f6483 = z;
        this.f6481 = z2;
    }

    @Nullable
    /* renamed from: 靐  reason: contains not printable characters */
    public Segment m7284() {
        Segment segment = this.f6479 != this ? this.f6479 : null;
        this.f6480.f6479 = this.f6479;
        this.f6479.f6480 = this.f6480;
        this.f6479 = null;
        this.f6480 = null;
        return segment;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m7285() {
        if (this.f6480 == this) {
            throw new IllegalStateException();
        } else if (this.f6480.f6481) {
            int i = this.f6484 - this.f6482;
            if (i <= (8192 - this.f6480.f6484) + (this.f6480.f6483 ? 0 : this.f6480.f6482)) {
                m7289(this.f6480, i);
                m7284();
                SegmentPool.m7291(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Segment m7286() {
        this.f6483 = true;
        return new Segment(this.f6485, this.f6482, this.f6484, true, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Segment m7287(int i) {
        Segment r0;
        if (i <= 0 || i > this.f6484 - this.f6482) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            r0 = m7286();
        } else {
            r0 = SegmentPool.m7290();
            System.arraycopy(this.f6485, this.f6482, r0.f6485, 0, i);
        }
        r0.f6484 = r0.f6482 + i;
        this.f6482 += i;
        this.f6480.m7288(r0);
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Segment m7288(Segment segment) {
        segment.f6480 = this;
        segment.f6479 = this.f6479;
        this.f6479.f6480 = segment;
        this.f6479 = segment;
        return segment;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m7289(Segment segment, int i) {
        if (!segment.f6481) {
            throw new IllegalArgumentException();
        }
        if (segment.f6484 + i > 8192) {
            if (segment.f6483) {
                throw new IllegalArgumentException();
            } else if ((segment.f6484 + i) - segment.f6482 > 8192) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(segment.f6485, segment.f6482, segment.f6485, 0, segment.f6484 - segment.f6482);
                segment.f6484 -= segment.f6482;
                segment.f6482 = 0;
            }
        }
        System.arraycopy(this.f6485, this.f6482, segment.f6485, segment.f6484, i);
        segment.f6484 += i;
        this.f6482 += i;
    }
}
