package okio;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

public final class GzipSource implements Source {

    /* renamed from: 连任  reason: contains not printable characters */
    private final CRC32 f16444 = new CRC32();

    /* renamed from: 靐  reason: contains not printable characters */
    private final BufferedSource f16445;

    /* renamed from: 麤  reason: contains not printable characters */
    private final InflaterSource f16446;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Inflater f16447;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f16448 = 0;

    public GzipSource(Source source) {
        if (source == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f16447 = new Inflater(true);
        this.f16445 = Okio.m20507(source);
        this.f16446 = new InflaterSource(this.f16445, this.f16447);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m20492() throws IOException {
        this.f16445.m20470(10);
        byte r7 = this.f16445.m20466().m7252(3);
        boolean z = ((r7 >> 1) & 1) == 1;
        if (z) {
            m20495(this.f16445.m20466(), 0, 10);
        }
        m20494("ID1ID2", 8075, (int) this.f16445.m20462());
        this.f16445.m20461(8);
        if (((r7 >> 2) & 1) == 1) {
            this.f16445.m20470(2);
            if (z) {
                m20495(this.f16445.m20466(), 0, 2);
            }
            short r9 = this.f16445.m20466().m7222();
            this.f16445.m20470((long) r9);
            if (z) {
                m20495(this.f16445.m20466(), 0, (long) r9);
            }
            this.f16445.m20461((long) r9);
        }
        if (((r7 >> 3) & 1) == 1) {
            long r10 = this.f16445.m20467((byte) 0);
            if (r10 == -1) {
                throw new EOFException();
            }
            if (z) {
                m20495(this.f16445.m20466(), 0, 1 + r10);
            }
            this.f16445.m20461(1 + r10);
        }
        if (((r7 >> 4) & 1) == 1) {
            long r102 = this.f16445.m20467((byte) 0);
            if (r102 == -1) {
                throw new EOFException();
            }
            if (z) {
                m20495(this.f16445.m20466(), 0, 1 + r102);
            }
            this.f16445.m20461(1 + r102);
        }
        if (z) {
            m20494("FHCRC", (int) this.f16445.m20457(), (int) (short) ((int) this.f16444.getValue()));
            this.f16444.reset();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m20493() throws IOException {
        m20494("CRC", this.f16445.m20455(), (int) this.f16444.getValue());
        m20494("ISIZE", this.f16445.m20455(), (int) this.f16447.getBytesWritten());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20494(String str, int i, int i2) throws IOException {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m20495(Buffer buffer, long j, long j2) {
        Segment segment = buffer.f6475;
        while (j >= ((long) (segment.f6484 - segment.f6482))) {
            j -= (long) (segment.f6484 - segment.f6482);
            segment = segment.f6479;
        }
        while (j2 > 0) {
            int i = (int) (((long) segment.f6482) + j);
            int min = (int) Math.min((long) (segment.f6484 - i), j2);
            this.f16444.update(segment.f6485, i, min);
            j2 -= (long) min;
            j = 0;
            segment = segment.f6479;
        }
    }

    public void close() throws IOException {
        this.f16446.close();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20496(Buffer buffer, long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j == 0) {
            return 0;
        } else {
            if (this.f16448 == 0) {
                m20492();
                this.f16448 = 1;
            }
            if (this.f16448 == 1) {
                long j2 = buffer.f6474;
                long r4 = this.f16446.m20500(buffer, j);
                if (r4 != -1) {
                    m20495(buffer, j2, r4);
                    return r4;
                }
                this.f16448 = 2;
            }
            if (this.f16448 == 2) {
                m20493();
                this.f16448 = 3;
                if (!this.f16445.m20452()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20497() {
        return this.f16445.m20569();
    }
}
