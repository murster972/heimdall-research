package okio;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

final class SegmentedByteString extends ByteString {

    /* renamed from: 连任  reason: contains not printable characters */
    final transient int[] f16466;

    /* renamed from: 麤  reason: contains not printable characters */
    final transient byte[][] f16467;

    SegmentedByteString(Buffer buffer, int i) {
        super((byte[]) null);
        Util.m20581(buffer.f6474, 0, (long) i);
        int i2 = 0;
        int i3 = 0;
        Segment segment = buffer.f6475;
        while (i2 < i) {
            if (segment.f6484 == segment.f6482) {
                throw new AssertionError("s.limit == s.pos");
            }
            i2 += segment.f6484 - segment.f6482;
            i3++;
            segment = segment.f6479;
        }
        this.f16467 = new byte[i3][];
        this.f16466 = new int[(i3 * 2)];
        int i4 = 0;
        int i5 = 0;
        Segment segment2 = buffer.f6475;
        while (i4 < i) {
            this.f16467[i5] = segment2.f6485;
            i4 += segment2.f6484 - segment2.f6482;
            if (i4 > i) {
                i4 = i;
            }
            this.f16466[i5] = i4;
            this.f16466[this.f16467.length + i5] = segment2.f6482;
            segment2.f6483 = true;
            i5++;
            segment2 = segment2.f6479;
        }
    }

    private Object writeReplace() {
        return m20563();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private ByteString m20563() {
        return new ByteString(toByteArray());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m20564(int i) {
        int binarySearch = Arrays.binarySearch(this.f16466, 0, this.f16467.length, i + 1);
        return binarySearch >= 0 ? binarySearch : binarySearch ^ -1;
    }

    public ByteBuffer asByteBuffer() {
        return ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer();
    }

    public String base64() {
        return m20563().base64();
    }

    public String base64Url() {
        return m20563().base64Url();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof ByteString) && ((ByteString) obj).size() == size() && rangeEquals(0, (ByteString) obj, 0, size());
    }

    public byte getByte(int i) {
        Util.m20581((long) this.f16466[this.f16467.length - 1], (long) i, 1);
        int r6 = m20564(i);
        return this.f16467[r6][(i - (r6 == 0 ? 0 : this.f16466[r6 - 1])) + this.f16466[this.f16467.length + r6]];
    }

    public int hashCode() {
        int i = this.f6477;
        if (i != 0) {
            int i2 = i;
            return i;
        }
        int i3 = 1;
        int i4 = 0;
        int length = this.f16467.length;
        for (int i5 = 0; i5 < length; i5++) {
            byte[] bArr = this.f16467[i5];
            int i6 = this.f16466[length + i5];
            int i7 = this.f16466[i5];
            int i8 = i6 + (i7 - i4);
            for (int i9 = i6; i9 < i8; i9++) {
                i3 = (i3 * 31) + bArr[i9];
            }
            i4 = i7;
        }
        this.f6477 = i3;
        int i10 = i3;
        return i3;
    }

    public String hex() {
        return m20563().hex();
    }

    public ByteString hmacSha1(ByteString byteString) {
        return m20563().hmacSha1(byteString);
    }

    public ByteString hmacSha256(ByteString byteString) {
        return m20563().hmacSha256(byteString);
    }

    public int indexOf(byte[] bArr, int i) {
        return m20563().indexOf(bArr, i);
    }

    public int lastIndexOf(byte[] bArr, int i) {
        return m20563().lastIndexOf(bArr, i);
    }

    public ByteString md5() {
        return m20563().md5();
    }

    public boolean rangeEquals(int i, ByteString byteString, int i2, int i3) {
        if (i < 0 || i > size() - i3) {
            return false;
        }
        int r1 = m20564(i);
        while (i3 > 0) {
            int i4 = r1 == 0 ? 0 : this.f16466[r1 - 1];
            int min = Math.min(i3, (i4 + (this.f16466[r1] - i4)) - i);
            if (!byteString.rangeEquals(i2, this.f16467[r1], (i - i4) + this.f16466[this.f16467.length + r1], min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            r1++;
        }
        return true;
    }

    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        if (i < 0 || i > size() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int r1 = m20564(i);
        while (i3 > 0) {
            int i4 = r1 == 0 ? 0 : this.f16466[r1 - 1];
            int min = Math.min(i3, (i4 + (this.f16466[r1] - i4)) - i);
            if (!Util.m20583(this.f16467[r1], (i - i4) + this.f16466[this.f16467.length + r1], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            r1++;
        }
        return true;
    }

    public ByteString sha1() {
        return m20563().sha1();
    }

    public ByteString sha256() {
        return m20563().sha256();
    }

    public int size() {
        return this.f16466[this.f16467.length - 1];
    }

    public String string(Charset charset) {
        return m20563().string(charset);
    }

    public ByteString substring(int i) {
        return m20563().substring(i);
    }

    public ByteString substring(int i, int i2) {
        return m20563().substring(i, i2);
    }

    public ByteString toAsciiLowercase() {
        return m20563().toAsciiLowercase();
    }

    public ByteString toAsciiUppercase() {
        return m20563().toAsciiUppercase();
    }

    public byte[] toByteArray() {
        byte[] bArr = new byte[this.f16466[this.f16467.length - 1]];
        int i = 0;
        int length = this.f16467.length;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = this.f16466[length + i2];
            int i4 = this.f16466[i2];
            System.arraycopy(this.f16467[i2], i3, bArr, i, i4 - i);
            i = i4;
        }
        return bArr;
    }

    public String toString() {
        return m20563().toString();
    }

    public String utf8() {
        return m20563().utf8();
    }

    public void write(OutputStream outputStream) throws IOException {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        }
        int i = 0;
        int length = this.f16467.length;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = this.f16466[length + i2];
            int i4 = this.f16466[i2];
            outputStream.write(this.f16467[i2], i3, i4 - i);
            i = i4;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m20565(Buffer buffer) {
        int i = 0;
        int length = this.f16467.length;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = this.f16466[length + i2];
            int i4 = this.f16466[i2];
            Segment segment = new Segment(this.f16467[i2], i3, (i3 + i4) - i, true, false);
            if (buffer.f6475 == null) {
                segment.f6480 = segment;
                segment.f6479 = segment;
                buffer.f6475 = segment;
            } else {
                buffer.f6475.f6480.m7288(segment);
            }
            i = i4;
        }
        buffer.f6474 += (long) i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public byte[] m20566() {
        return toByteArray();
    }
}
