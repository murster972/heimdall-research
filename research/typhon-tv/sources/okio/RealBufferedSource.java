package okio;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

final class RealBufferedSource implements BufferedSource {

    /* renamed from: 靐  reason: contains not printable characters */
    public final Source f16462;

    /* renamed from: 齉  reason: contains not printable characters */
    boolean f16463;

    /* renamed from: 龘  reason: contains not printable characters */
    public final Buffer f16464 = new Buffer();

    RealBufferedSource(Source source) {
        if (source == null) {
            throw new NullPointerException("source == null");
        }
        this.f16462 = source;
    }

    public void close() throws IOException {
        if (!this.f16463) {
            this.f16463 = true;
            this.f16462.close();
            this.f16464.m7223();
        }
    }

    public boolean isOpen() {
        return !this.f16463;
    }

    public int read(ByteBuffer byteBuffer) throws IOException {
        if (this.f16464.f6474 == 0 && this.f16462.m20568(this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
            return -1;
        }
        return this.f16464.read(byteBuffer);
    }

    public String toString() {
        return "buffer(" + this.f16462 + ")";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m20536(long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("limit < 0: " + j);
        }
        long j2 = j == Long.MAX_VALUE ? Long.MAX_VALUE : j + 1;
        long r14 = m20553((byte) 10, 0, j2);
        if (r14 != -1) {
            return this.f16464.m7212(r14);
        }
        if (j2 < Long.MAX_VALUE && m20549(j2) && this.f16464.m7252(j2 - 1) == 13) {
            if (m20549(1 + j2) && this.f16464.m7252(j2) == 10) {
                return this.f16464.m7212(j2);
            }
        }
        Buffer buffer = new Buffer();
        this.f16464.m7269(buffer, 0, Math.min(32, this.f16464.m7242()));
        throw new EOFException("\\n not found: limit=" + Math.min(this.f16464.m7242(), j) + " content=" + buffer.m7277().hex() + 8230);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m20537() throws IOException {
        if (!this.f16463) {
            return this.f16464.m7210() && this.f16462.m20568(this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1;
        }
        throw new IllegalStateException("closed");
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public InputStream m20538() {
        return new InputStream() {
            public int available() throws IOException {
                if (!RealBufferedSource.this.f16463) {
                    return (int) Math.min(RealBufferedSource.this.f16464.f6474, 2147483647L);
                }
                throw new IOException("closed");
            }

            public void close() throws IOException {
                RealBufferedSource.this.close();
            }

            public int read() throws IOException {
                if (RealBufferedSource.this.f16463) {
                    throw new IOException("closed");
                } else if (RealBufferedSource.this.f16464.f6474 == 0 && RealBufferedSource.this.f16462.m20568(RealBufferedSource.this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return -1;
                } else {
                    return RealBufferedSource.this.f16464.m7228() & 255;
                }
            }

            public int read(byte[] bArr, int i, int i2) throws IOException {
                if (RealBufferedSource.this.f16463) {
                    throw new IOException("closed");
                }
                Util.m20581((long) bArr.length, (long) i, (long) i2);
                if (RealBufferedSource.this.f16464.f6474 == 0 && RealBufferedSource.this.f16462.m20568(RealBufferedSource.this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return -1;
                }
                return RealBufferedSource.this.f16464.m7257(bArr, i, i2);
            }

            public String toString() {
                return RealBufferedSource.this + ".inputStream()";
            }
        };
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public byte[] m20539(long j) throws IOException {
        m20558(j);
        return this.f16464.m7216(j);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m20540() throws IOException {
        m20558(4);
        return this.f16464.m7217();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0026  */
    /* renamed from: ʿ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m20541() throws java.io.IOException {
        /*
            r7 = this;
            r2 = 1
            r7.m20558((long) r2)
            r1 = 0
        L_0x0006:
            int r2 = r1 + 1
            long r2 = (long) r2
            boolean r2 = r7.m20549(r2)
            if (r2 == 0) goto L_0x0040
            okio.Buffer r2 = r7.f16464
            long r4 = (long) r1
            byte r0 = r2.m7252((long) r4)
            r2 = 48
            if (r0 < r2) goto L_0x001e
            r2 = 57
            if (r0 <= r2) goto L_0x003d
        L_0x001e:
            if (r1 != 0) goto L_0x0024
            r2 = 45
            if (r0 == r2) goto L_0x003d
        L_0x0024:
            if (r1 != 0) goto L_0x0040
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.String r3 = "Expected leading [0-9] or '-' character but was %#x"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r5 = 0
            java.lang.Byte r6 = java.lang.Byte.valueOf(r0)
            r4[r5] = r6
            java.lang.String r3 = java.lang.String.format(r3, r4)
            r2.<init>(r3)
            throw r2
        L_0x003d:
            int r1 = r1 + 1
            goto L_0x0006
        L_0x0040:
            okio.Buffer r2 = r7.f16464
            long r2 = r2.m7219()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.RealBufferedSource.m20541():long");
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public short m20542() throws IOException {
        m20558(2);
        return this.f16464.m7222();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public String m20543() throws IOException {
        this.f16464.m7262(this.f16462);
        return this.f16464.m7224();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public String m20544() throws IOException {
        return m20536(Long.MAX_VALUE);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public byte m20545() throws IOException {
        m20558(1);
        return this.f16464.m7228();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m20546(long j) throws IOException {
        if (this.f16463) {
            throw new IllegalStateException("closed");
        }
        while (j > 0) {
            if (this.f16464.f6474 == 0 && this.f16462.m20568(this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.f16464.m7242());
            this.f16464.m7230(min);
            j -= min;
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public short m20547() throws IOException {
        m20558(2);
        return this.f16464.m7235();
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int m20548() throws IOException {
        m20558(4);
        return this.f16464.m7236();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m20549(long j) throws IOException {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f16463) {
            throw new IllegalStateException("closed");
        } else {
            while (this.f16464.f6474 < j) {
                if (this.f16462.m20568(this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return false;
                }
            }
            return true;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public ByteString m20550(long j) throws IOException {
        m20558(j);
        return this.f16464.m7251(j);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Buffer m20551() {
        return this.f16464;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20552(byte b) throws IOException {
        return m20553(b, 0, Long.MAX_VALUE);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20553(byte b, long j, long j2) throws IOException {
        if (this.f16463) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || j2 < j) {
            throw new IllegalArgumentException(String.format("fromIndex=%s toIndex=%s", new Object[]{Long.valueOf(j), Long.valueOf(j2)}));
        } else {
            while (j < j2) {
                long r8 = this.f16464.m7259(b, j, j2);
                if (r8 != -1) {
                    return r8;
                }
                long j3 = this.f16464.f6474;
                if (j3 >= j2 || this.f16462.m20568(this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
                    return -1;
                }
                j = Math.max(j, j3);
            }
            return -1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20554(Buffer buffer, long j) throws IOException {
        if (buffer == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f16463) {
            throw new IllegalStateException("closed");
        } else if (this.f16464.f6474 == 0 && this.f16462.m20568(this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) == -1) {
            return -1;
        } else {
            return this.f16464.m7260(buffer, Math.min(j, this.f16464.f6474));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m20555(Sink sink) throws IOException {
        if (sink == null) {
            throw new IllegalArgumentException("sink == null");
        }
        long j = 0;
        while (this.f16462.m20568(this.f16464, PlaybackStateCompat.ACTION_PLAY_FROM_URI) != -1) {
            long r0 = this.f16464.m7214();
            if (r0 > 0) {
                j += r0;
                sink.a_(this.f16464, r0);
            }
        }
        if (this.f16464.m7242() <= 0) {
            return j;
        }
        long r2 = j + this.f16464.m7242();
        sink.a_(this.f16464, this.f16464.m7242());
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m20556(Charset charset) throws IOException {
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        }
        this.f16464.m7262(this.f16462);
        return this.f16464.m7264(charset);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Timeout m20557() {
        return this.f16462.m20569();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20558(long j) throws IOException {
        if (!m20549(j)) {
            throw new EOFException();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m20559(byte[] bArr) throws IOException {
        try {
            m20558((long) bArr.length);
            this.f16464.m7273(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (this.f16464.f6474 > 0) {
                int r2 = this.f16464.m7257(bArr, i, (int) this.f16464.f6474);
                if (r2 == -1) {
                    throw new AssertionError();
                }
                i += r2;
            }
            throw e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20560(long j, ByteString byteString) throws IOException {
        return m20561(j, byteString, 0, byteString.size());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m20561(long j, ByteString byteString, int i, int i2) throws IOException {
        if (this.f16463) {
            throw new IllegalStateException("closed");
        } else if (j < 0 || i < 0 || i2 < 0 || byteString.size() - i < i2) {
            return false;
        } else {
            for (int i3 = 0; i3 < i2; i3++) {
                long j2 = j + ((long) i3);
                if (!m20549(1 + j2) || this.f16464.m7252(j2) != byteString.getByte(i + i3)) {
                    return false;
                }
            }
            return true;
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public long m20562() throws IOException {
        m20558(1);
        int i = 0;
        while (true) {
            if (!m20549((long) (i + 1))) {
                break;
            }
            byte r0 = this.f16464.m7252((long) i);
            if ((r0 >= 48 && r0 <= 57) || ((r0 >= 97 && r0 <= 102) || (r0 >= 65 && r0 <= 70))) {
                i++;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", new Object[]{Byte.valueOf(r0)}));
            }
        }
        return this.f16464.m7276();
    }
}
