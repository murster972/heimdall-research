package okio;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

public interface Sink extends Closeable, Flushable {
    void a_(Buffer buffer, long j) throws IOException;

    void close() throws IOException;

    void flush() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    Timeout m20567();
}
