package okio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;

public interface BufferedSource extends ReadableByteChannel, Source {
    /* renamed from: ʻ  reason: contains not printable characters */
    String m20451(long j) throws IOException;

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean m20452() throws IOException;

    /* renamed from: ʼ  reason: contains not printable characters */
    InputStream m20453();

    /* renamed from: ʽ  reason: contains not printable characters */
    byte[] m20454(long j) throws IOException;

    /* renamed from: ʾ  reason: contains not printable characters */
    int m20455() throws IOException;

    /* renamed from: ʿ  reason: contains not printable characters */
    long m20456() throws IOException;

    /* renamed from: ˈ  reason: contains not printable characters */
    short m20457() throws IOException;

    /* renamed from: ˊ  reason: contains not printable characters */
    String m20458() throws IOException;

    /* renamed from: ˋ  reason: contains not printable characters */
    String m20459() throws IOException;

    /* renamed from: ˑ  reason: contains not printable characters */
    byte m20460() throws IOException;

    /* renamed from: ˑ  reason: contains not printable characters */
    void m20461(long j) throws IOException;

    /* renamed from: ٴ  reason: contains not printable characters */
    short m20462() throws IOException;

    /* renamed from: ᐧ  reason: contains not printable characters */
    int m20463() throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    boolean m20464(long j) throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    ByteString m20465(long j) throws IOException;

    /* renamed from: 齉  reason: contains not printable characters */
    Buffer m20466();

    /* renamed from: 龘  reason: contains not printable characters */
    long m20467(byte b) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    long m20468(Sink sink) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    String m20469(Charset charset) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m20470(long j) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m20471(byte[] bArr) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean m20472(long j, ByteString byteString) throws IOException;

    /* renamed from: ﹶ  reason: contains not printable characters */
    long m20473() throws IOException;
}
