package rx.internal.operators;

import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Action0;

public final class OnSubscribeTimerPeriodically implements Observable.OnSubscribe<Long> {

    /* renamed from: 靐  reason: contains not printable characters */
    final long f18926;

    /* renamed from: 麤  reason: contains not printable characters */
    final Scheduler f18927;

    /* renamed from: 齉  reason: contains not printable characters */
    final TimeUnit f18928;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f18929;

    public OnSubscribeTimerPeriodically(long initialDelay, long period, TimeUnit unit, Scheduler scheduler) {
        this.f18929 = initialDelay;
        this.f18926 = period;
        this.f18928 = unit;
        this.f18927 = scheduler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(final Subscriber<? super Long> child) {
        final Scheduler.Worker worker = this.f18927.createWorker();
        child.m24513((Subscription) worker);
        worker.m24504(new Action0() {

            /* renamed from: 龘  reason: contains not printable characters */
            long f18933;

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24645() {
                try {
                    Subscriber subscriber = child;
                    long j = this.f18933;
                    this.f18933 = 1 + j;
                    subscriber.onNext(Long.valueOf(j));
                } catch (Throwable th) {
                    Exceptions.m24533(e, (Observer<?>) child);
                    throw th;
                }
            }
        }, this.f18929, this.f18926, this.f18928);
    }
}
