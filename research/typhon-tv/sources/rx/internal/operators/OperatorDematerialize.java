package rx.internal.operators;

import rx.Notification;
import rx.Observable;
import rx.Subscriber;

public final class OperatorDematerialize<T> implements Observable.Operator<T, Notification<T>> {

    static final class Holder {

        /* renamed from: 龘  reason: contains not printable characters */
        static final OperatorDematerialize<Object> f18994 = new OperatorDematerialize<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static OperatorDematerialize m24669() {
        return Holder.f18994;
    }

    OperatorDematerialize() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super Notification<T>> call(final Subscriber<? super T> child) {
        return new Subscriber<Notification<T>>(child) {

            /* renamed from: 龘  reason: contains not printable characters */
            boolean f18992;

            /* renamed from: 龘  reason: contains not printable characters */
            public void onNext(Notification<T> t) {
                switch (AnonymousClass2.f18993[t.m24494().ordinal()]) {
                    case 1:
                        if (!this.f18992) {
                            child.onNext(t.m24501());
                            return;
                        }
                        return;
                    case 2:
                        onError(t.m24499());
                        return;
                    case 3:
                        onCompleted();
                        return;
                    default:
                        onError(new IllegalArgumentException("Unsupported notification type: " + t));
                        return;
                }
            }

            public void onError(Throwable e) {
                if (!this.f18992) {
                    this.f18992 = true;
                    child.onError(e);
                }
            }

            public void onCompleted() {
                if (!this.f18992) {
                    this.f18992 = true;
                    child.onCompleted();
                }
            }
        };
    }

    /* renamed from: rx.internal.operators.OperatorDematerialize$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {

        /* renamed from: 龘  reason: contains not printable characters */
        static final /* synthetic */ int[] f18993 = new int[Notification.Kind.values().length];

        static {
            try {
                f18993[Notification.Kind.OnNext.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f18993[Notification.Kind.OnError.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f18993[Notification.Kind.OnCompleted.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }
}
