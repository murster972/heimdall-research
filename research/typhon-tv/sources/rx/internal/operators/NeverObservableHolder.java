package rx.internal.operators;

import rx.Observable;
import rx.Subscriber;

public enum NeverObservableHolder implements Observable.OnSubscribe<Object> {
    INSTANCE;
    

    /* renamed from: 龘  reason: contains not printable characters */
    static final Observable<Object> f18798 = null;

    static {
        f18798 = Observable.m7346(INSTANCE);
    }

    public static <T> Observable<T> instance() {
        return f18798;
    }

    public void call(Subscriber<? super Object> subscriber) {
    }
}
