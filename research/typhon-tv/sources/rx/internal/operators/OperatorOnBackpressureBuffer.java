package rx.internal.operators;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import rx.BackpressureOverflow;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Action0;
import rx.internal.util.BackpressureDrainManager;

public class OperatorOnBackpressureBuffer<T> implements Observable.Operator<T, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Action0 f19041 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private final BackpressureOverflow.Strategy f19042 = BackpressureOverflow.f18746;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Long f19043 = null;

    static final class Holder {

        /* renamed from: 龘  reason: contains not printable characters */
        static final OperatorOnBackpressureBuffer<?> f19051 = new OperatorOnBackpressureBuffer<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> OperatorOnBackpressureBuffer<T> m24702() {
        return Holder.f19051;
    }

    OperatorOnBackpressureBuffer() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super T> child) {
        BufferSubscriber<T> parent = new BufferSubscriber<>(child, this.f19043, this.f19041, this.f19042);
        child.m24513((Subscription) parent);
        child.m24512(parent.m24706());
        return parent;
    }

    static final class BufferSubscriber<T> extends Subscriber<T> implements BackpressureDrainManager.BackpressureQueueCallback {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Action0 f19044;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final BackpressureOverflow.Strategy f19045;

        /* renamed from: 连任  reason: contains not printable characters */
        private final BackpressureDrainManager f19046;

        /* renamed from: 靐  reason: contains not printable characters */
        private final AtomicLong f19047;

        /* renamed from: 麤  reason: contains not printable characters */
        private final AtomicBoolean f19048 = new AtomicBoolean(false);

        /* renamed from: 齉  reason: contains not printable characters */
        private final Subscriber<? super T> f19049;

        /* renamed from: 龘  reason: contains not printable characters */
        private final ConcurrentLinkedQueue<Object> f19050 = new ConcurrentLinkedQueue<>();

        public BufferSubscriber(Subscriber<? super T> child, Long capacity, Action0 onOverflow, BackpressureOverflow.Strategy overflowStrategy) {
            this.f19049 = child;
            this.f19047 = capacity != null ? new AtomicLong(capacity.longValue()) : null;
            this.f19044 = onOverflow;
            this.f19046 = new BackpressureDrainManager(this);
            this.f19045 = overflowStrategy;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24705() {
            m24511(Long.MAX_VALUE);
        }

        public void onCompleted() {
            if (!this.f19048.get()) {
                this.f19046.terminateAndDrain();
            }
        }

        public void onError(Throwable e) {
            if (!this.f19048.get()) {
                this.f19046.terminateAndDrain(e);
            }
        }

        public void onNext(T t) {
            if (m24704()) {
                this.f19050.offer(NotificationLite.m24567(t));
                this.f19046.drain();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24710(Object value) {
            return NotificationLite.m24569(this.f19049, value);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24709(Throwable exception) {
            if (exception != null) {
                this.f19049.onError(exception);
            } else {
                this.f19049.onCompleted();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Object m24708() {
            return this.f19050.peek();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Object m24707() {
            Object value = this.f19050.poll();
            if (!(this.f19047 == null || value == null)) {
                this.f19047.incrementAndGet();
            }
            return value;
        }

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean m24704() {
            long currCapacity;
            if (this.f19047 == null) {
                return true;
            }
            do {
                currCapacity = this.f19047.get();
                if (currCapacity <= 0) {
                    boolean hasCapacity = false;
                    try {
                        hasCapacity = this.f19045.m24474() && m24707() != null;
                    } catch (MissingBackpressureException e) {
                        if (this.f19048.compareAndSet(false, true)) {
                            unsubscribe();
                            this.f19049.onError(e);
                        }
                    }
                    if (this.f19044 != null) {
                        try {
                            this.f19044.m24539();
                        } catch (Throwable e2) {
                            Exceptions.m24529(e2);
                            this.f19046.terminateAndDrain(e2);
                            return false;
                        }
                    }
                    if (!hasCapacity) {
                        return false;
                    }
                }
            } while (!this.f19047.compareAndSet(currCapacity, currCapacity - 1));
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 麤  reason: contains not printable characters */
        public Producer m24706() {
            return this.f19046;
        }
    }
}
