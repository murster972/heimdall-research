package rx.internal.operators;

import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;

public final class OperatorDelay<T> implements Observable.Operator<T, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final TimeUnit f18978;

    /* renamed from: 齉  reason: contains not printable characters */
    final Scheduler f18979;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f18980;

    public OperatorDelay(long delay, TimeUnit unit, Scheduler scheduler) {
        this.f18980 = delay;
        this.f18978 = unit;
        this.f18979 = scheduler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(final Subscriber<? super T> child) {
        final Scheduler.Worker worker = this.f18979.createWorker();
        child.m24513((Subscription) worker);
        return new Subscriber<T>(child) {

            /* renamed from: 龘  reason: contains not printable characters */
            boolean f18984;

            public void onCompleted() {
                worker.m24505(new Action0() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m24666() {
                        if (!AnonymousClass1.this.f18984) {
                            AnonymousClass1.this.f18984 = true;
                            child.onCompleted();
                        }
                    }
                }, OperatorDelay.this.f18980, OperatorDelay.this.f18978);
            }

            public void onError(final Throwable e) {
                worker.m24503(new Action0() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m24667() {
                        if (!AnonymousClass1.this.f18984) {
                            AnonymousClass1.this.f18984 = true;
                            child.onError(e);
                            worker.unsubscribe();
                        }
                    }
                });
            }

            public void onNext(final T t) {
                worker.m24505(new Action0() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m24668() {
                        if (!AnonymousClass1.this.f18984) {
                            child.onNext(t);
                        }
                    }
                }, OperatorDelay.this.f18980, OperatorDelay.this.f18978);
            }
        };
    }
}
