package rx.internal.operators;

import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.exceptions.OnErrorThrowable;
import rx.functions.Func1;
import rx.plugins.RxJavaHooks;

public final class OnSubscribeMap<T, R> implements Observable.OnSubscribe<R> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Func1<? super T, ? extends R> f18839;

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable<T> f18840;

    public OnSubscribeMap(Observable<T> source, Func1<? super T, ? extends R> transformer) {
        this.f18840 = source;
        this.f18839 = transformer;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super R> o) {
        MapSubscriber<T, R> parent = new MapSubscriber<>(o, this.f18839);
        o.m24513((Subscription) parent);
        this.f18840.m7416(parent);
    }

    static final class MapSubscriber<T, R> extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Func1<? super T, ? extends R> f18841;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f18842;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super R> f18843;

        public MapSubscriber(Subscriber<? super R> actual, Func1<? super T, ? extends R> mapper) {
            this.f18843 = actual;
            this.f18841 = mapper;
        }

        public void onNext(T t) {
            try {
                this.f18843.onNext(this.f18841.call(t));
            } catch (Throwable ex) {
                Exceptions.m24529(ex);
                unsubscribe();
                onError(OnErrorThrowable.addValueAsLastCause(ex, t));
            }
        }

        public void onError(Throwable e) {
            if (this.f18842) {
                RxJavaHooks.m25024(e);
                return;
            }
            this.f18842 = true;
            this.f18843.onError(e);
        }

        public void onCompleted() {
            if (!this.f18842) {
                this.f18843.onCompleted();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24602(Producer p) {
            this.f18843.m24512(p);
        }
    }
}
