package rx.internal.operators;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Observer;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.MissingBackpressureException;
import rx.internal.util.atomic.SpscAtomicArrayQueue;
import rx.internal.util.unsafe.SpscArrayQueue;
import rx.internal.util.unsafe.UnsafeAccess;

public final class OnSubscribePublishMulticast<T> extends AtomicInteger implements Observable.OnSubscribe<T>, Observer<T>, Subscription {
    private static final long serialVersionUID = -3741892510772238743L;

    /* renamed from: 靐  reason: contains not printable characters */
    static final PublishProducer<?>[] f18844 = new PublishProducer[0];

    /* renamed from: 龘  reason: contains not printable characters */
    static final PublishProducer<?>[] f18845 = new PublishProducer[0];
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final ParentSubscriber<T> parent;
    final int prefetch;
    volatile Producer producer;
    final Queue<T> queue;
    volatile PublishProducer<T>[] subscribers;

    public OnSubscribePublishMulticast(int prefetch2, boolean delayError2) {
        if (prefetch2 <= 0) {
            throw new IllegalArgumentException("prefetch > 0 required but it was " + prefetch2);
        }
        this.prefetch = prefetch2;
        this.delayError = delayError2;
        if (UnsafeAccess.m24998()) {
            this.queue = new SpscArrayQueue(prefetch2);
        } else {
            this.queue = new SpscAtomicArrayQueue(prefetch2);
        }
        this.subscribers = (PublishProducer[]) f18845;
        this.parent = new ParentSubscriber<>(this);
    }

    public void call(Subscriber<? super T> t) {
        PublishProducer<T> pp = new PublishProducer<>(t, this);
        t.m24513((Subscription) pp);
        t.m24512((Producer) pp);
        if (!m24607(pp)) {
            Throwable e = this.error;
            if (e != null) {
                t.onError(e);
            } else {
                t.onCompleted();
            }
        } else if (pp.isUnsubscribed()) {
            m24603(pp);
        } else {
            m24605();
        }
    }

    public void onNext(T t) {
        if (!this.queue.offer(t)) {
            this.parent.unsubscribe();
            this.error = new MissingBackpressureException("Queue full?!");
            this.done = true;
        }
        m24605();
    }

    public void onError(Throwable e) {
        this.error = e;
        this.done = true;
        m24605();
    }

    public void onCompleted() {
        this.done = true;
        m24605();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24606(Producer p) {
        this.producer = p;
        p.request((long) this.prefetch);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24605() {
        if (getAndIncrement() == 0) {
            Queue<T> q = this.queue;
            int missed = 0;
            do {
                long r = Long.MAX_VALUE;
                PublishProducer<T>[] a = this.subscribers;
                int n = a.length;
                for (PublishProducer<T> inner : a) {
                    r = Math.min(r, inner.get());
                }
                if (n != 0) {
                    long e = 0;
                    while (e != r) {
                        boolean d = this.done;
                        T v = q.poll();
                        boolean empty = v == null;
                        if (m24608(d, empty)) {
                            return;
                        }
                        if (empty) {
                            break;
                        }
                        for (PublishProducer<T> inner2 : a) {
                            inner2.actual.onNext(v);
                        }
                        e++;
                    }
                    if (e == r) {
                        if (m24608(this.done, q.isEmpty())) {
                            return;
                        }
                    }
                    if (e != 0) {
                        Producer p = this.producer;
                        if (p != null) {
                            p.request(e);
                        }
                        for (PublishProducer<T> inner3 : a) {
                            BackpressureUtils.m24549((AtomicLong) inner3, e);
                        }
                    }
                }
                missed = addAndGet(-missed);
            } while (missed != 0);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24608(boolean d, boolean empty) {
        int i = 0;
        if (d) {
            if (!this.delayError) {
                Throwable ex = this.error;
                if (ex != null) {
                    this.queue.clear();
                    PublishProducer<T>[] a = m24604();
                    int length = a.length;
                    while (i < length) {
                        a[i].actual.onError(ex);
                        i++;
                    }
                    return true;
                } else if (empty) {
                    PublishProducer<T>[] a2 = m24604();
                    int length2 = a2.length;
                    while (i < length2) {
                        a2[i].actual.onCompleted();
                        i++;
                    }
                    return true;
                }
            } else if (empty) {
                PublishProducer<T>[] a3 = m24604();
                Throwable ex2 = this.error;
                if (ex2 != null) {
                    int length3 = a3.length;
                    while (i < length3) {
                        a3[i].actual.onError(ex2);
                        i++;
                    }
                    return true;
                }
                int length4 = a3.length;
                while (i < length4) {
                    a3[i].actual.onCompleted();
                    i++;
                }
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public PublishProducer<T>[] m24604() {
        PublishProducer<T>[] a = this.subscribers;
        if (a != f18844) {
            synchronized (this) {
                a = this.subscribers;
                if (a != f18844) {
                    this.subscribers = (PublishProducer[]) f18844;
                }
            }
        }
        return a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24607(PublishProducer<T> inner) {
        boolean z = false;
        if (this.subscribers != f18844) {
            synchronized (this) {
                PublishProducer<T>[] a = this.subscribers;
                if (a != f18844) {
                    int n = a.length;
                    PublishProducer<T>[] b = new PublishProducer[(n + 1)];
                    System.arraycopy(a, 0, b, 0, n);
                    b[n] = inner;
                    this.subscribers = b;
                    z = true;
                }
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m24603(rx.internal.operators.OnSubscribePublishMulticast.PublishProducer<T> r8) {
        /*
            r7 = this;
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer<T>[] r0 = r7.subscribers
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer<?>[] r5 = f18844
            if (r0 == r5) goto L_0x000a
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer<?>[] r5 = f18845
            if (r0 != r5) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            monitor-enter(r7)
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer<T>[] r0 = r7.subscribers     // Catch:{ all -> 0x0018 }
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer<?>[] r5 = f18844     // Catch:{ all -> 0x0018 }
            if (r0 == r5) goto L_0x0016
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer<?>[] r5 = f18845     // Catch:{ all -> 0x0018 }
            if (r0 != r5) goto L_0x001b
        L_0x0016:
            monitor-exit(r7)     // Catch:{ all -> 0x0018 }
            goto L_0x000a
        L_0x0018:
            r5 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0018 }
            throw r5
        L_0x001b:
            r3 = -1
            int r4 = r0.length     // Catch:{ all -> 0x0018 }
            r2 = 0
        L_0x001e:
            if (r2 >= r4) goto L_0x0025
            r5 = r0[r2]     // Catch:{ all -> 0x0018 }
            if (r5 != r8) goto L_0x0029
            r3 = r2
        L_0x0025:
            if (r3 >= 0) goto L_0x002c
            monitor-exit(r7)     // Catch:{ all -> 0x0018 }
            goto L_0x000a
        L_0x0029:
            int r2 = r2 + 1
            goto L_0x001e
        L_0x002c:
            r5 = 1
            if (r4 != r5) goto L_0x0037
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer<?>[] r1 = f18845     // Catch:{ all -> 0x0018 }
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer[] r1 = (rx.internal.operators.OnSubscribePublishMulticast.PublishProducer[]) r1     // Catch:{ all -> 0x0018 }
        L_0x0033:
            r7.subscribers = r1     // Catch:{ all -> 0x0018 }
            monitor-exit(r7)     // Catch:{ all -> 0x0018 }
            goto L_0x000a
        L_0x0037:
            int r5 = r4 + -1
            rx.internal.operators.OnSubscribePublishMulticast$PublishProducer[] r1 = new rx.internal.operators.OnSubscribePublishMulticast.PublishProducer[r5]     // Catch:{ all -> 0x0018 }
            r5 = 0
            r6 = 0
            java.lang.System.arraycopy(r0, r5, r1, r6, r3)     // Catch:{ all -> 0x0018 }
            int r5 = r3 + 1
            int r6 = r4 - r3
            int r6 = r6 + -1
            java.lang.System.arraycopy(r0, r5, r1, r3, r6)     // Catch:{ all -> 0x0018 }
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OnSubscribePublishMulticast.m24603(rx.internal.operators.OnSubscribePublishMulticast$PublishProducer):void");
    }

    static final class ParentSubscriber<T> extends Subscriber<T> {

        /* renamed from: 龘  reason: contains not printable characters */
        final OnSubscribePublishMulticast<T> f18846;

        public ParentSubscriber(OnSubscribePublishMulticast<T> state) {
            this.f18846 = state;
        }

        public void onNext(T t) {
            this.f18846.onNext(t);
        }

        public void onError(Throwable e) {
            this.f18846.onError(e);
        }

        public void onCompleted() {
            this.f18846.onCompleted();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24609(Producer p) {
            this.f18846.m24606(p);
        }
    }

    public Subscriber<T> subscriber() {
        return this.parent;
    }

    public void unsubscribe() {
        this.parent.unsubscribe();
    }

    public boolean isUnsubscribed() {
        return this.parent.isUnsubscribed();
    }

    static final class PublishProducer<T> extends AtomicLong implements Producer, Subscription {
        private static final long serialVersionUID = 960704844171597367L;
        final Subscriber<? super T> actual;
        final AtomicBoolean once = new AtomicBoolean();
        final OnSubscribePublishMulticast<T> parent;

        public PublishProducer(Subscriber<? super T> actual2, OnSubscribePublishMulticast<T> parent2) {
            this.actual = actual2;
            this.parent = parent2;
        }

        public void request(long n) {
            if (n < 0) {
                throw new IllegalArgumentException("n >= 0 required but it was " + n);
            } else if (n != 0) {
                BackpressureUtils.m24552((AtomicLong) this, n);
                this.parent.m24605();
            }
        }

        public boolean isUnsubscribed() {
            return this.once.get();
        }

        public void unsubscribe() {
            if (this.once.compareAndSet(false, true)) {
                this.parent.m24603(this);
            }
        }
    }
}
