package rx.internal.operators;

import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Observer;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Func2;
import rx.functions.FuncN;
import rx.functions.Functions;
import rx.internal.util.RxRingBuffer;
import rx.subscriptions.CompositeSubscription;

public final class OperatorZip<R> implements Observable.Operator<R, Observable<?>[]> {

    /* renamed from: 龘  reason: contains not printable characters */
    final FuncN<? extends R> f19136;

    public OperatorZip(Func2 f) {
        this.f19136 = Functions.m24546(f);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super Observable[]> call(Subscriber<? super R> child) {
        Zip<R> zipper = new Zip<>(child, this.f19136);
        ZipProducer<R> producer = new ZipProducer<>(zipper);
        OperatorZip<R>.ZipSubscriber subscriber = new ZipSubscriber(child, zipper, producer);
        child.m24513((Subscription) subscriber);
        child.m24512((Producer) producer);
        return subscriber;
    }

    final class ZipSubscriber extends Subscriber<Observable[]> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Zip<R> f19141;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f19142;

        /* renamed from: 齉  reason: contains not printable characters */
        final ZipProducer<R> f19143;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super R> f19144;

        public ZipSubscriber(Subscriber<? super R> child, Zip<R> zipper, ZipProducer<R> producer) {
            this.f19144 = child;
            this.f19141 = zipper;
            this.f19143 = producer;
        }

        public void onCompleted() {
            if (!this.f19142) {
                this.f19144.onCompleted();
            }
        }

        public void onError(Throwable e) {
            this.f19144.onError(e);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void onNext(Observable[] observables) {
            if (observables == null || observables.length == 0) {
                this.f19144.onCompleted();
                return;
            }
            this.f19142 = true;
            this.f19141.m24784(observables, this.f19143);
        }
    }

    static final class ZipProducer<R> extends AtomicLong implements Producer {
        private static final long serialVersionUID = -1216676403723546796L;
        final Zip<R> zipper;

        public ZipProducer(Zip<R> zipper2) {
            this.zipper = zipper2;
        }

        public void request(long n) {
            BackpressureUtils.m24552((AtomicLong) this, n);
            this.zipper.m24783();
        }
    }

    static final class Zip<R> extends AtomicLong {
        private static final long serialVersionUID = 5995274816189928317L;

        /* renamed from: 龘  reason: contains not printable characters */
        static final int f19137 = ((int) (((double) RxRingBuffer.f19311) * 0.7d));
        final Observer<? super R> child;
        private final CompositeSubscription childSubscription = new CompositeSubscription();
        int emitted;
        private AtomicLong requested;
        private volatile Object[] subscribers;
        private final FuncN<? extends R> zipFunction;

        public Zip(Subscriber<? super R> child2, FuncN<? extends R> zipFunction2) {
            this.child = child2;
            this.zipFunction = zipFunction2;
            child2.m24513((Subscription) this.childSubscription);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24784(Observable[] os, AtomicLong requested2) {
            Object[] subscribers2 = new Object[os.length];
            for (int i = 0; i < os.length; i++) {
                Zip<R>.InnerSubscriber io2 = new InnerSubscriber();
                subscribers2[i] = io2;
                this.childSubscription.m25086((Subscription) io2);
            }
            this.requested = requested2;
            this.subscribers = subscribers2;
            for (int i2 = 0; i2 < os.length; i2++) {
                os[i2].m7416((InnerSubscriber) subscribers2[i2]);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24783() {
            Object[] subscribers2 = this.subscribers;
            if (subscribers2 != null && getAndIncrement() == 0) {
                int length = subscribers2.length;
                Observer<? super R> child2 = this.child;
                AtomicLong requested2 = this.requested;
                while (true) {
                    Object[] vs = new Object[length];
                    boolean allHaveValues = true;
                    for (int i = 0; i < length; i++) {
                        RxRingBuffer buffer = ((InnerSubscriber) subscribers2[i]).f19139;
                        Object n = buffer.m24902();
                        if (n == null) {
                            allHaveValues = false;
                        } else if (buffer.m24904(n)) {
                            child2.onCompleted();
                            this.childSubscription.unsubscribe();
                            return;
                        } else {
                            vs[i] = buffer.m24906(n);
                        }
                    }
                    if (allHaveValues && requested2.get() > 0) {
                        try {
                            child2.onNext(this.zipFunction.m24545(vs));
                            requested2.decrementAndGet();
                            this.emitted++;
                            for (Object obj : subscribers2) {
                                RxRingBuffer buffer2 = ((InnerSubscriber) obj).f19139;
                                buffer2.m24901();
                                if (buffer2.m24904(buffer2.m24902())) {
                                    child2.onCompleted();
                                    this.childSubscription.unsubscribe();
                                    return;
                                }
                            }
                            if (this.emitted > f19137) {
                                for (Object obj2 : subscribers2) {
                                    ((InnerSubscriber) obj2).m24786((long) this.emitted);
                                }
                                this.emitted = 0;
                            }
                        } catch (Throwable e) {
                            Exceptions.m24534(e, child2, vs);
                            return;
                        }
                    } else if (decrementAndGet() <= 0) {
                        return;
                    }
                }
            }
        }

        final class InnerSubscriber extends Subscriber {

            /* renamed from: 龘  reason: contains not printable characters */
            final RxRingBuffer f19139 = RxRingBuffer.m24899();

            InnerSubscriber() {
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m24785() {
                m24511((long) RxRingBuffer.f19311);
            }

            /* renamed from: 靐  reason: contains not printable characters */
            public void m24786(long n) {
                m24511(n);
            }

            public void onCompleted() {
                this.f19139.m24905();
                Zip.this.m24783();
            }

            public void onError(Throwable e) {
                Zip.this.child.onError(e);
            }

            public void onNext(Object t) {
                try {
                    this.f19139.m24908(t);
                } catch (MissingBackpressureException e) {
                    onError(e);
                }
                Zip.this.m24783();
            }
        }
    }
}
