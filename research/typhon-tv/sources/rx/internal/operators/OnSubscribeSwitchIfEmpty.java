package rx.internal.operators;

import java.util.concurrent.atomic.AtomicInteger;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.internal.producers.ProducerArbiter;
import rx.subscriptions.SerialSubscription;

public final class OnSubscribeSwitchIfEmpty<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Observable<? extends T> f18890;

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable<? extends T> f18891;

    public OnSubscribeSwitchIfEmpty(Observable<? extends T> source, Observable<? extends T> alternate) {
        this.f18891 = source;
        this.f18890 = alternate;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> child) {
        SerialSubscription serial = new SerialSubscription();
        ProducerArbiter arbiter = new ProducerArbiter();
        ParentSubscriber<T> parent = new ParentSubscriber<>(child, serial, arbiter, this.f18890);
        serial.m25088(parent);
        child.m24513((Subscription) serial);
        child.m24512((Producer) arbiter);
        parent.m24633(this.f18891);
    }

    static final class ParentSubscriber<T> extends Subscriber<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final ProducerArbiter f18894;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final Observable<? extends T> f18895;

        /* renamed from: 连任  reason: contains not printable characters */
        private final SerialSubscription f18896;

        /* renamed from: 靐  reason: contains not printable characters */
        volatile boolean f18897;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Subscriber<? super T> f18898;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f18899 = true;

        /* renamed from: 龘  reason: contains not printable characters */
        final AtomicInteger f18900;

        ParentSubscriber(Subscriber<? super T> child, SerialSubscription serial, ProducerArbiter arbiter, Observable<? extends T> alternate) {
            this.f18898 = child;
            this.f18896 = serial;
            this.f18894 = arbiter;
            this.f18895 = alternate;
            this.f18900 = new AtomicInteger();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24634(Producer producer) {
            this.f18894.m24796(producer);
        }

        public void onCompleted() {
            if (!this.f18899) {
                this.f18898.onCompleted();
            } else if (!this.f18898.isUnsubscribed()) {
                this.f18897 = false;
                m24633((Observable) null);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24633(Observable<? extends T> source) {
            if (this.f18900.getAndIncrement() == 0) {
                while (!this.f18898.isUnsubscribed()) {
                    if (!this.f18897) {
                        if (source == null) {
                            AlternateSubscriber<T> as = new AlternateSubscriber<>(this.f18898, this.f18894);
                            this.f18896.m25088(as);
                            this.f18897 = true;
                            this.f18895.m7416(as);
                        } else {
                            this.f18897 = true;
                            source.m7416((Subscriber<? super Object>) this);
                            source = null;
                        }
                    }
                    if (this.f18900.decrementAndGet() == 0) {
                        return;
                    }
                }
            }
        }

        public void onError(Throwable e) {
            this.f18898.onError(e);
        }

        public void onNext(T t) {
            this.f18899 = false;
            this.f18898.onNext(t);
            this.f18894.m24795(1);
        }
    }

    static final class AlternateSubscriber<T> extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Subscriber<? super T> f18892;

        /* renamed from: 龘  reason: contains not printable characters */
        private final ProducerArbiter f18893;

        AlternateSubscriber(Subscriber<? super T> child, ProducerArbiter arbiter) {
            this.f18892 = child;
            this.f18893 = arbiter;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24632(Producer producer) {
            this.f18893.m24796(producer);
        }

        public void onCompleted() {
            this.f18892.onCompleted();
        }

        public void onError(Throwable e) {
            this.f18892.onError(e);
        }

        public void onNext(T t) {
            this.f18892.onNext(t);
            this.f18893.m24795(1);
        }
    }
}
