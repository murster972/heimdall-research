package rx.internal.operators;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.CompositeException;
import rx.exceptions.MissingBackpressureException;
import rx.exceptions.OnErrorThrowable;
import rx.internal.util.RxRingBuffer;
import rx.internal.util.ScalarSynchronousObservable;
import rx.internal.util.atomic.SpscAtomicArrayQueue;
import rx.internal.util.atomic.SpscExactAtomicArrayQueue;
import rx.internal.util.atomic.SpscUnboundedAtomicArrayQueue;
import rx.internal.util.unsafe.Pow2;
import rx.internal.util.unsafe.SpscArrayQueue;
import rx.internal.util.unsafe.UnsafeAccess;
import rx.subscriptions.CompositeSubscription;

public final class OperatorMerge<T> implements Observable.Operator<T, Observable<? extends T>> {

    /* renamed from: 靐  reason: contains not printable characters */
    final int f18999;

    /* renamed from: 龘  reason: contains not printable characters */
    final boolean f19000;

    static final class HolderDelayErrors {

        /* renamed from: 龘  reason: contains not printable characters */
        static final OperatorMerge<Object> f19001 = new OperatorMerge<>(true, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    static final class HolderNoDelay {

        /* renamed from: 龘  reason: contains not printable characters */
        static final OperatorMerge<Object> f19002 = new OperatorMerge<>(false, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> OperatorMerge<T> m24675(boolean delayErrors) {
        if (delayErrors) {
            return HolderDelayErrors.f19001;
        }
        return HolderNoDelay.f19002;
    }

    OperatorMerge(boolean delayErrors, int maxConcurrent) {
        this.f19000 = delayErrors;
        this.f18999 = maxConcurrent;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<Observable<? extends T>> call(Subscriber<? super T> child) {
        MergeSubscriber<T> subscriber = new MergeSubscriber<>(child, this.f19000, this.f18999);
        MergeProducer<T> producer = new MergeProducer<>(subscriber);
        subscriber.f19023 = producer;
        child.m24513((Subscription) subscriber);
        child.m24512((Producer) producer);
        return subscriber;
    }

    static final class MergeProducer<T> extends AtomicLong implements Producer {
        private static final long serialVersionUID = -1214379189873595503L;
        final MergeSubscriber<T> subscriber;

        public MergeProducer(MergeSubscriber<T> subscriber2) {
            this.subscriber = subscriber2;
        }

        public void request(long n) {
            if (n > 0) {
                if (get() != Long.MAX_VALUE) {
                    BackpressureUtils.m24552((AtomicLong) this, n);
                    this.subscriber.m24683();
                }
            } else if (n < 0) {
                throw new IllegalArgumentException("n >= 0 required");
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m24679(int n) {
            return addAndGet((long) (-n));
        }
    }

    static final class MergeSubscriber<T> extends Subscriber<Observable<? extends T>> {

        /* renamed from: ﾞ  reason: contains not printable characters */
        static final InnerSubscriber<?>[] f19009 = new InnerSubscriber[0];

        /* renamed from: ʻ  reason: contains not printable characters */
        volatile CompositeSubscription f19010;

        /* renamed from: ʼ  reason: contains not printable characters */
        volatile ConcurrentLinkedQueue<Throwable> f19011;

        /* renamed from: ʽ  reason: contains not printable characters */
        volatile boolean f19012;

        /* renamed from: ʾ  reason: contains not printable characters */
        long f19013;

        /* renamed from: ʿ  reason: contains not printable characters */
        long f19014;

        /* renamed from: ˈ  reason: contains not printable characters */
        volatile InnerSubscriber<?>[] f19015 = f19009;

        /* renamed from: ˊ  reason: contains not printable characters */
        final int f19016;

        /* renamed from: ˋ  reason: contains not printable characters */
        int f19017;

        /* renamed from: ˑ  reason: contains not printable characters */
        boolean f19018;

        /* renamed from: ٴ  reason: contains not printable characters */
        boolean f19019;

        /* renamed from: ᐧ  reason: contains not printable characters */
        final Object f19020 = new Object();

        /* renamed from: 连任  reason: contains not printable characters */
        volatile Queue<Object> f19021;

        /* renamed from: 靐  reason: contains not printable characters */
        final boolean f19022;

        /* renamed from: 麤  reason: contains not printable characters */
        MergeProducer<T> f19023;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f19024;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f19025;

        /* renamed from: ﹶ  reason: contains not printable characters */
        int f19026;

        public MergeSubscriber(Subscriber<? super T> child, boolean delayErrors, int maxConcurrent) {
            this.f19025 = child;
            this.f19022 = delayErrors;
            this.f19024 = maxConcurrent;
            if (maxConcurrent == Integer.MAX_VALUE) {
                this.f19016 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
                m24511(Long.MAX_VALUE);
                return;
            }
            this.f19016 = Math.max(1, maxConcurrent >> 1);
            m24511((long) maxConcurrent);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Queue<Throwable> m24690() {
            ConcurrentLinkedQueue<Throwable> q = this.f19011;
            if (q == null) {
                synchronized (this) {
                    try {
                        q = this.f19011;
                        if (q == null) {
                            ConcurrentLinkedQueue<Throwable> q2 = new ConcurrentLinkedQueue<>();
                            try {
                                this.f19011 = q2;
                                q = q2;
                            } catch (Throwable th) {
                                th = th;
                                ConcurrentLinkedQueue<Throwable> concurrentLinkedQueue = q2;
                                throw th;
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        throw th;
                    }
                }
            }
            return q;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0014, code lost:
            if (r2 == false) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0016, code lost:
            m24513((rx.Subscription) r0);
         */
        /* renamed from: 齉  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public rx.subscriptions.CompositeSubscription m24689() {
            /*
                r4 = this;
                rx.subscriptions.CompositeSubscription r0 = r4.f19010
                if (r0 != 0) goto L_0x0019
                r2 = 0
                monitor-enter(r4)
                rx.subscriptions.CompositeSubscription r0 = r4.f19010     // Catch:{ all -> 0x001a }
                if (r0 != 0) goto L_0x0013
                rx.subscriptions.CompositeSubscription r1 = new rx.subscriptions.CompositeSubscription     // Catch:{ all -> 0x001a }
                r1.<init>()     // Catch:{ all -> 0x001a }
                r4.f19010 = r1     // Catch:{ all -> 0x001d }
                r2 = 1
                r0 = r1
            L_0x0013:
                monitor-exit(r4)     // Catch:{ all -> 0x001a }
                if (r2 == 0) goto L_0x0019
                r4.m24513((rx.Subscription) r0)
            L_0x0019:
                return r0
            L_0x001a:
                r3 = move-exception
            L_0x001b:
                monitor-exit(r4)     // Catch:{ all -> 0x001a }
                throw r3
            L_0x001d:
                r3 = move-exception
                r0 = r1
                goto L_0x001b
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorMerge.MergeSubscriber.m24689():rx.subscriptions.CompositeSubscription");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void onNext(Observable<? extends T> t) {
            if (t != null) {
                if (t == Observable.m7349()) {
                    m24688();
                } else if (t instanceof ScalarSynchronousObservable) {
                    m24691(((ScalarSynchronousObservable) t).m24912());
                } else {
                    long j = this.f19013;
                    this.f19013 = 1 + j;
                    InnerSubscriber<T> inner = new InnerSubscriber<>(this, j);
                    m24694(inner);
                    t.m7416((Subscriber<? super Object>) inner);
                    m24683();
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m24688() {
            int produced = this.f19017 + 1;
            if (produced == this.f19016) {
                this.f19017 = 0;
                m24684((long) produced);
                return;
            }
            this.f19017 = produced;
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        private void m24680() {
            List<Throwable> list = new ArrayList<>(this.f19011);
            if (list.size() == 1) {
                this.f19025.onError(list.get(0));
            } else {
                this.f19025.onError(new CompositeException((Collection<? extends Throwable>) list));
            }
        }

        public void onError(Throwable e) {
            m24690().offer(e);
            this.f19012 = true;
            m24683();
        }

        public void onCompleted() {
            this.f19012 = true;
            m24683();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24694(InnerSubscriber<T> inner) {
            m24689().m25086((Subscription) inner);
            synchronized (this.f19020) {
                InnerSubscriber<?>[] a = this.f19015;
                int n = a.length;
                InnerSubscriber<?>[] b = new InnerSubscriber[(n + 1)];
                System.arraycopy(a, 0, b, 0, n);
                b[n] = inner;
                this.f19015 = b;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24686(InnerSubscriber<T> inner) {
            RxRingBuffer q = inner.f19006;
            if (q != null) {
                q.m24907();
            }
            this.f19010.m25085(inner);
            synchronized (this.f19020) {
                InnerSubscriber<?>[] a = this.f19015;
                int n = a.length;
                int j = -1;
                int i = 0;
                while (true) {
                    if (i >= n) {
                        break;
                    } else if (inner.equals(a[i])) {
                        j = i;
                        break;
                    } else {
                        i++;
                    }
                }
                if (j >= 0) {
                    if (n == 1) {
                        this.f19015 = f19009;
                        return;
                    }
                    InnerSubscriber<?>[] b = new InnerSubscriber[(n - 1)];
                    System.arraycopy(a, 0, b, 0, j);
                    System.arraycopy(a, j + 1, b, j, (n - j) - 1);
                    this.f19015 = b;
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24695(InnerSubscriber<T> subscriber, T value) {
            boolean success = false;
            long r = this.f19023.get();
            if (r != 0) {
                synchronized (this) {
                    r = this.f19023.get();
                    if (!this.f19018 && r != 0) {
                        this.f19018 = true;
                        success = true;
                    }
                }
            }
            if (success) {
                RxRingBuffer subscriberQueue = subscriber.f19006;
                if (subscriberQueue == null || subscriberQueue.m24903()) {
                    m24696(subscriber, value, r);
                    return;
                }
                m24687(subscriber, value);
                m24681();
                return;
            }
            m24687(subscriber, value);
            m24683();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24687(InnerSubscriber<T> subscriber, T value) {
            RxRingBuffer q = subscriber.f19006;
            if (q == null) {
                q = RxRingBuffer.m24900();
                subscriber.m24513((Subscription) q);
                subscriber.f19006 = q;
            }
            try {
                q.m24908(NotificationLite.m24567(value));
            } catch (MissingBackpressureException ex) {
                subscriber.unsubscribe();
                subscriber.onError(ex);
            } catch (IllegalStateException ex2) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.unsubscribe();
                    subscriber.onError(ex2);
                }
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
            if (1 != 0) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
            monitor-enter(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r5.f19018 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x002a, code lost:
            monitor-exit(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x005e, code lost:
            if (1 != 0) goto L_0x0065;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x0060, code lost:
            monitor-enter(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
            r5.f19018 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x0064, code lost:
            monitor-exit(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x0065, code lost:
            m24681();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
            return;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m24696(rx.internal.operators.OperatorMerge.InnerSubscriber<T> r6, T r7, long r8) {
            /*
                r5 = this;
                r0 = 0
                rx.Subscriber<? super T> r2 = r5.f19025     // Catch:{ Throwable -> 0x002c }
                r2.onNext(r7)     // Catch:{ Throwable -> 0x002c }
            L_0x0006:
                r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r2 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
                if (r2 == 0) goto L_0x0015
                rx.internal.operators.OperatorMerge$MergeProducer<T> r2 = r5.f19023     // Catch:{ all -> 0x004e }
                r3 = 1
                r2.m24679(r3)     // Catch:{ all -> 0x004e }
            L_0x0015:
                r2 = 1
                r6.m24678(r2)     // Catch:{ all -> 0x004e }
                monitor-enter(r5)     // Catch:{ all -> 0x004e }
                r0 = 1
                boolean r2 = r5.f19019     // Catch:{ all -> 0x0069 }
                if (r2 != 0) goto L_0x005a
                r2 = 0
                r5.f19018 = r2     // Catch:{ all -> 0x0069 }
                monitor-exit(r5)     // Catch:{ all -> 0x0069 }
                if (r0 != 0) goto L_0x002b
                monitor-enter(r5)
                r2 = 0
                r5.f19018 = r2     // Catch:{ all -> 0x0057 }
                monitor-exit(r5)     // Catch:{ all -> 0x0057 }
            L_0x002b:
                return
            L_0x002c:
                r1 = move-exception
                boolean r2 = r5.f19022     // Catch:{ all -> 0x004e }
                if (r2 != 0) goto L_0x0046
                rx.exceptions.Exceptions.m24529(r1)     // Catch:{ all -> 0x004e }
                r0 = 1
                r6.unsubscribe()     // Catch:{ all -> 0x004e }
                r6.onError(r1)     // Catch:{ all -> 0x004e }
                if (r0 != 0) goto L_0x002b
                monitor-enter(r5)
                r2 = 0
                r5.f19018 = r2     // Catch:{ all -> 0x0043 }
                monitor-exit(r5)     // Catch:{ all -> 0x0043 }
                goto L_0x002b
            L_0x0043:
                r2 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0043 }
                throw r2
            L_0x0046:
                java.util.Queue r2 = r5.m24690()     // Catch:{ all -> 0x004e }
                r2.offer(r1)     // Catch:{ all -> 0x004e }
                goto L_0x0006
            L_0x004e:
                r2 = move-exception
                if (r0 != 0) goto L_0x0056
                monitor-enter(r5)
                r3 = 0
                r5.f19018 = r3     // Catch:{ all -> 0x006f }
                monitor-exit(r5)     // Catch:{ all -> 0x006f }
            L_0x0056:
                throw r2
            L_0x0057:
                r2 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0057 }
                throw r2
            L_0x005a:
                r2 = 0
                r5.f19019 = r2     // Catch:{ all -> 0x0069 }
                monitor-exit(r5)     // Catch:{ all -> 0x0069 }
                if (r0 != 0) goto L_0x0065
                monitor-enter(r5)
                r2 = 0
                r5.f19018 = r2     // Catch:{ all -> 0x006c }
                monitor-exit(r5)     // Catch:{ all -> 0x006c }
            L_0x0065:
                r5.m24681()
                goto L_0x002b
            L_0x0069:
                r2 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0069 }
                throw r2     // Catch:{ all -> 0x004e }
            L_0x006c:
                r2 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x006c }
                throw r2
            L_0x006f:
                r2 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x006f }
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorMerge.MergeSubscriber.m24696(rx.internal.operators.OperatorMerge$InnerSubscriber, java.lang.Object, long):void");
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24684(long n) {
            m24511(n);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24691(T value) {
            boolean success = false;
            long r = this.f19023.get();
            if (r != 0) {
                synchronized (this) {
                    r = this.f19023.get();
                    if (!this.f19018 && r != 0) {
                        this.f19018 = true;
                        success = true;
                    }
                }
            }
            if (success) {
                Queue<Object> mainQueue = this.f19021;
                if (mainQueue == null || mainQueue.isEmpty()) {
                    m24692(value, r);
                    return;
                }
                m24685(value);
                m24681();
                return;
            }
            m24685(value);
            m24683();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24685(T value) {
            Queue<Object> q = this.f19021;
            if (q == null) {
                int mc = this.f19024;
                if (mc == Integer.MAX_VALUE) {
                    q = new SpscUnboundedAtomicArrayQueue<>(RxRingBuffer.f19311);
                } else if (!Pow2.m24970(mc)) {
                    q = new SpscExactAtomicArrayQueue<>(mc);
                } else if (UnsafeAccess.m24998()) {
                    q = new SpscArrayQueue<>(mc);
                } else {
                    q = new SpscAtomicArrayQueue<>(mc);
                }
                this.f19021 = q;
            }
            if (!q.offer(NotificationLite.m24567(value))) {
                unsubscribe();
                onError(OnErrorThrowable.addValueAsLastCause(new MissingBackpressureException(), value));
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x002e, code lost:
            if (1 != 0) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0030, code lost:
            monitor-enter(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            r6.f19018 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0034, code lost:
            monitor-exit(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x006b, code lost:
            if (1 != 0) goto L_0x0072;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x006d, code lost:
            monitor-enter(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
            r6.f19018 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x0071, code lost:
            monitor-exit(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x0072, code lost:
            m24681();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
            return;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m24692(T r7, long r8) {
            /*
                r6 = this;
                r1 = 0
                rx.Subscriber<? super T> r3 = r6.f19025     // Catch:{ Throwable -> 0x0036 }
                r3.onNext(r7)     // Catch:{ Throwable -> 0x0036 }
            L_0x0006:
                r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r3 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
                if (r3 == 0) goto L_0x0015
                rx.internal.operators.OperatorMerge$MergeProducer<T> r3 = r6.f19023     // Catch:{ all -> 0x0058 }
                r4 = 1
                r3.m24679(r4)     // Catch:{ all -> 0x0058 }
            L_0x0015:
                int r3 = r6.f19017     // Catch:{ all -> 0x0058 }
                int r0 = r3 + 1
                int r3 = r6.f19016     // Catch:{ all -> 0x0058 }
                if (r0 != r3) goto L_0x0061
                r3 = 0
                r6.f19017 = r3     // Catch:{ all -> 0x0058 }
                long r4 = (long) r0     // Catch:{ all -> 0x0058 }
                r6.m24684((long) r4)     // Catch:{ all -> 0x0058 }
            L_0x0024:
                monitor-enter(r6)     // Catch:{ all -> 0x0058 }
                r1 = 1
                boolean r3 = r6.f19019     // Catch:{ all -> 0x0076 }
                if (r3 != 0) goto L_0x0067
                r3 = 0
                r6.f19018 = r3     // Catch:{ all -> 0x0076 }
                monitor-exit(r6)     // Catch:{ all -> 0x0076 }
                if (r1 != 0) goto L_0x0035
                monitor-enter(r6)
                r3 = 0
                r6.f19018 = r3     // Catch:{ all -> 0x0064 }
                monitor-exit(r6)     // Catch:{ all -> 0x0064 }
            L_0x0035:
                return
            L_0x0036:
                r2 = move-exception
                boolean r3 = r6.f19022     // Catch:{ all -> 0x0058 }
                if (r3 != 0) goto L_0x0050
                rx.exceptions.Exceptions.m24529(r2)     // Catch:{ all -> 0x0058 }
                r1 = 1
                r6.unsubscribe()     // Catch:{ all -> 0x0058 }
                r6.onError(r2)     // Catch:{ all -> 0x0058 }
                if (r1 != 0) goto L_0x0035
                monitor-enter(r6)
                r3 = 0
                r6.f19018 = r3     // Catch:{ all -> 0x004d }
                monitor-exit(r6)     // Catch:{ all -> 0x004d }
                goto L_0x0035
            L_0x004d:
                r3 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x004d }
                throw r3
            L_0x0050:
                java.util.Queue r3 = r6.m24690()     // Catch:{ all -> 0x0058 }
                r3.offer(r2)     // Catch:{ all -> 0x0058 }
                goto L_0x0006
            L_0x0058:
                r3 = move-exception
                if (r1 != 0) goto L_0x0060
                monitor-enter(r6)
                r4 = 0
                r6.f19018 = r4     // Catch:{ all -> 0x007c }
                monitor-exit(r6)     // Catch:{ all -> 0x007c }
            L_0x0060:
                throw r3
            L_0x0061:
                r6.f19017 = r0     // Catch:{ all -> 0x0058 }
                goto L_0x0024
            L_0x0064:
                r3 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0064 }
                throw r3
            L_0x0067:
                r3 = 0
                r6.f19019 = r3     // Catch:{ all -> 0x0076 }
                monitor-exit(r6)     // Catch:{ all -> 0x0076 }
                if (r1 != 0) goto L_0x0072
                monitor-enter(r6)
                r3 = 0
                r6.f19018 = r3     // Catch:{ all -> 0x0079 }
                monitor-exit(r6)     // Catch:{ all -> 0x0079 }
            L_0x0072:
                r6.m24681()
                goto L_0x0035
            L_0x0076:
                r3 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0076 }
                throw r3     // Catch:{ all -> 0x0058 }
            L_0x0079:
                r3 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0079 }
                throw r3
            L_0x007c:
                r3 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x007c }
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorMerge.MergeSubscriber.m24692(java.lang.Object, long):void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public void m24683() {
            synchronized (this) {
                if (this.f19018) {
                    this.f19019 = true;
                    return;
                }
                this.f19018 = true;
                m24681();
            }
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:230:0x027f, code lost:
            if (1 != 0) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:231:0x0281, code lost:
            monitor-enter(r32);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:234:?, code lost:
            r32.f19018 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:235:0x028a, code lost:
            monitor-exit(r32);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:306:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:307:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* renamed from: ʻ  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m24681() {
            /*
                r32 = this;
                r23 = 0
                r0 = r32
                rx.Subscriber<? super T> r4 = r0.f19025     // Catch:{ all -> 0x00fc }
            L_0x0006:
                boolean r30 = r32.m24682()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x001e
                r23 = 1
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x001b }
                monitor-exit(r32)     // Catch:{ all -> 0x001b }
            L_0x001a:
                return
            L_0x001b:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x001b }
                throw r30
            L_0x001e:
                r0 = r32
                java.util.Queue<java.lang.Object> r0 = r0.f19021     // Catch:{ all -> 0x00fc }
                r26 = r0
                r0 = r32
                rx.internal.operators.OperatorMerge$MergeProducer<T> r0 = r0.f19023     // Catch:{ all -> 0x00fc }
                r30 = r0
                long r20 = r30.get()     // Catch:{ all -> 0x00fc }
                r30 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r30 = (r20 > r30 ? 1 : (r20 == r30 ? 0 : -1))
                if (r30 != 0) goto L_0x0063
                r28 = 1
            L_0x0039:
                r19 = 0
                if (r26 == 0) goto L_0x0079
            L_0x003d:
                r22 = 0
                r16 = 0
            L_0x0041:
                r30 = 0
                int r30 = (r20 > r30 ? 1 : (r20 == r30 ? 0 : -1))
                if (r30 <= 0) goto L_0x0068
                java.lang.Object r16 = r26.poll()     // Catch:{ all -> 0x00fc }
                boolean r30 = r32.m24682()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x0066
                r23 = 1
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x0060 }
                monitor-exit(r32)     // Catch:{ all -> 0x0060 }
                goto L_0x001a
            L_0x0060:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x0060 }
                throw r30
            L_0x0063:
                r28 = 0
                goto L_0x0039
            L_0x0066:
                if (r16 != 0) goto L_0x00b6
            L_0x0068:
                if (r22 <= 0) goto L_0x0071
                if (r28 == 0) goto L_0x010a
                r20 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            L_0x0071:
                r30 = 0
                int r30 = (r20 > r30 ? 1 : (r20 == r30 ? 0 : -1))
                if (r30 == 0) goto L_0x0079
                if (r16 != 0) goto L_0x003d
            L_0x0079:
                r0 = r32
                boolean r5 = r0.f19012     // Catch:{ all -> 0x00fc }
                r0 = r32
                java.util.Queue<java.lang.Object> r0 = r0.f19021     // Catch:{ all -> 0x00fc }
                r26 = r0
                r0 = r32
                rx.internal.operators.OperatorMerge$InnerSubscriber<?>[] r9 = r0.f19015     // Catch:{ all -> 0x00fc }
                int r15 = r9.length     // Catch:{ all -> 0x00fc }
                if (r5 == 0) goto L_0x011e
                if (r26 == 0) goto L_0x0092
                boolean r30 = r26.isEmpty()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x011e
            L_0x0092:
                if (r15 != 0) goto L_0x011e
                r0 = r32
                java.util.concurrent.ConcurrentLinkedQueue<java.lang.Throwable> r6 = r0.f19011     // Catch:{ all -> 0x00fc }
                if (r6 == 0) goto L_0x00a0
                boolean r30 = r6.isEmpty()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x011a
            L_0x00a0:
                r4.onCompleted()     // Catch:{ all -> 0x00fc }
            L_0x00a3:
                r23 = 1
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x00b3 }
                monitor-exit(r32)     // Catch:{ all -> 0x00b3 }
                goto L_0x001a
            L_0x00b3:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x00b3 }
                throw r30
            L_0x00b6:
                java.lang.Object r29 = rx.internal.operators.NotificationLite.m24564(r16)     // Catch:{ all -> 0x00fc }
                r0 = r29
                r4.onNext(r0)     // Catch:{ Throwable -> 0x00c9 }
            L_0x00bf:
                int r19 = r19 + 1
                int r22 = r22 + 1
                r30 = 1
                long r20 = r20 - r30
                goto L_0x0041
            L_0x00c9:
                r27 = move-exception
                r0 = r32
                boolean r0 = r0.f19022     // Catch:{ all -> 0x00fc }
                r30 = r0
                if (r30 != 0) goto L_0x00f0
                rx.exceptions.Exceptions.m24529(r27)     // Catch:{ all -> 0x00fc }
                r23 = 1
                r32.unsubscribe()     // Catch:{ all -> 0x00fc }
                r0 = r27
                r4.onError(r0)     // Catch:{ all -> 0x00fc }
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x00ed }
                monitor-exit(r32)     // Catch:{ all -> 0x00ed }
                goto L_0x001a
            L_0x00ed:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x00ed }
                throw r30
            L_0x00f0:
                java.util.Queue r30 = r32.m24690()     // Catch:{ all -> 0x00fc }
                r0 = r30
                r1 = r27
                r0.offer(r1)     // Catch:{ all -> 0x00fc }
                goto L_0x00bf
            L_0x00fc:
                r30 = move-exception
                if (r23 != 0) goto L_0x0109
                monitor-enter(r32)
                r31 = 0
                r0 = r31
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x02a7 }
                monitor-exit(r32)     // Catch:{ all -> 0x02a7 }
            L_0x0109:
                throw r30
            L_0x010a:
                r0 = r32
                rx.internal.operators.OperatorMerge$MergeProducer<T> r0 = r0.f19023     // Catch:{ all -> 0x00fc }
                r30 = r0
                r0 = r30
                r1 = r22
                long r20 = r0.m24679(r1)     // Catch:{ all -> 0x00fc }
                goto L_0x0071
            L_0x011a:
                r32.m24680()     // Catch:{ all -> 0x00fc }
                goto L_0x00a3
            L_0x011e:
                r10 = 0
                if (r15 <= 0) goto L_0x025b
                r0 = r32
                long r0 = r0.f19014     // Catch:{ all -> 0x00fc }
                r24 = r0
                r0 = r32
                int r8 = r0.f19026     // Catch:{ all -> 0x00fc }
                if (r15 <= r8) goto L_0x0139
                r30 = r9[r8]     // Catch:{ all -> 0x00fc }
                r0 = r30
                long r0 = r0.f19005     // Catch:{ all -> 0x00fc }
                r30 = r0
                int r30 = (r30 > r24 ? 1 : (r30 == r24 ? 0 : -1))
                if (r30 == 0) goto L_0x015f
            L_0x0139:
                if (r15 > r8) goto L_0x013c
                r8 = 0
            L_0x013c:
                r14 = r8
                r7 = 0
            L_0x013e:
                if (r7 >= r15) goto L_0x014c
                r30 = r9[r14]     // Catch:{ all -> 0x00fc }
                r0 = r30
                long r0 = r0.f19005     // Catch:{ all -> 0x00fc }
                r30 = r0
                int r30 = (r30 > r24 ? 1 : (r30 == r24 ? 0 : -1))
                if (r30 != 0) goto L_0x017c
            L_0x014c:
                r8 = r14
                r0 = r32
                r0.f19026 = r14     // Catch:{ all -> 0x00fc }
                r30 = r9[r14]     // Catch:{ all -> 0x00fc }
                r0 = r30
                long r0 = r0.f19005     // Catch:{ all -> 0x00fc }
                r30 = r0
                r0 = r30
                r2 = r32
                r2.f19014 = r0     // Catch:{ all -> 0x00fc }
            L_0x015f:
                r14 = r8
                r7 = 0
            L_0x0161:
                if (r7 >= r15) goto L_0x0249
                boolean r30 = r32.m24682()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x0184
                r23 = 1
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x0179 }
                monitor-exit(r32)     // Catch:{ all -> 0x0179 }
                goto L_0x001a
            L_0x0179:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x0179 }
                throw r30
            L_0x017c:
                int r14 = r14 + 1
                if (r14 != r15) goto L_0x0181
                r14 = 0
            L_0x0181:
                int r7 = r7 + 1
                goto L_0x013e
            L_0x0184:
                r13 = r9[r14]     // Catch:{ all -> 0x00fc }
                r16 = 0
            L_0x0188:
                r17 = 0
            L_0x018a:
                r30 = 0
                int r30 = (r20 > r30 ? 1 : (r20 == r30 ? 0 : -1))
                if (r30 <= 0) goto L_0x01af
                boolean r30 = r32.m24682()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x01a9
                r23 = 1
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x01a6 }
                monitor-exit(r32)     // Catch:{ all -> 0x01a6 }
                goto L_0x001a
            L_0x01a6:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x01a6 }
                throw r30
            L_0x01a9:
                rx.internal.util.RxRingBuffer r0 = r13.f19006     // Catch:{ all -> 0x00fc }
                r18 = r0
                if (r18 != 0) goto L_0x01ff
            L_0x01af:
                if (r17 <= 0) goto L_0x01cb
                if (r28 != 0) goto L_0x023a
                r0 = r32
                rx.internal.operators.OperatorMerge$MergeProducer<T> r0 = r0.f19023     // Catch:{ all -> 0x00fc }
                r30 = r0
                r0 = r30
                r1 = r17
                long r20 = r0.m24679(r1)     // Catch:{ all -> 0x00fc }
            L_0x01c1:
                r0 = r17
                long r0 = (long) r0     // Catch:{ all -> 0x00fc }
                r30 = r0
                r0 = r30
                r13.m24678(r0)     // Catch:{ all -> 0x00fc }
            L_0x01cb:
                r30 = 0
                int r30 = (r20 > r30 ? 1 : (r20 == r30 ? 0 : -1))
                if (r30 == 0) goto L_0x01d3
                if (r16 != 0) goto L_0x0188
            L_0x01d3:
                boolean r11 = r13.f19007     // Catch:{ all -> 0x00fc }
                rx.internal.util.RxRingBuffer r12 = r13.f19006     // Catch:{ all -> 0x00fc }
                if (r11 == 0) goto L_0x0243
                if (r12 == 0) goto L_0x01e1
                boolean r30 = r12.m24903()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x0243
            L_0x01e1:
                r0 = r32
                r0.m24686(r13)     // Catch:{ all -> 0x00fc }
                boolean r30 = r32.m24682()     // Catch:{ all -> 0x00fc }
                if (r30 == 0) goto L_0x0240
                r23 = 1
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x01fc }
                monitor-exit(r32)     // Catch:{ all -> 0x01fc }
                goto L_0x001a
            L_0x01fc:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x01fc }
                throw r30
            L_0x01ff:
                java.lang.Object r16 = r18.m24901()     // Catch:{ all -> 0x00fc }
                if (r16 == 0) goto L_0x01af
                java.lang.Object r29 = rx.internal.operators.NotificationLite.m24564(r16)     // Catch:{ all -> 0x00fc }
                r0 = r29
                r4.onNext(r0)     // Catch:{ Throwable -> 0x0216 }
                r30 = 1
                long r20 = r20 - r30
                int r17 = r17 + 1
                goto L_0x018a
            L_0x0216:
                r27 = move-exception
                r23 = 1
                rx.exceptions.Exceptions.m24529(r27)     // Catch:{ all -> 0x00fc }
                r0 = r27
                r4.onError(r0)     // Catch:{ all -> 0x0235 }
                r32.unsubscribe()     // Catch:{ all -> 0x00fc }
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x0232 }
                monitor-exit(r32)     // Catch:{ all -> 0x0232 }
                goto L_0x001a
            L_0x0232:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x0232 }
                throw r30
            L_0x0235:
                r30 = move-exception
                r32.unsubscribe()     // Catch:{ all -> 0x00fc }
                throw r30     // Catch:{ all -> 0x00fc }
            L_0x023a:
                r20 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                goto L_0x01c1
            L_0x0240:
                int r19 = r19 + 1
                r10 = 1
            L_0x0243:
                r30 = 0
                int r30 = (r20 > r30 ? 1 : (r20 == r30 ? 0 : -1))
                if (r30 != 0) goto L_0x0290
            L_0x0249:
                r0 = r32
                r0.f19026 = r14     // Catch:{ all -> 0x00fc }
                r30 = r9[r14]     // Catch:{ all -> 0x00fc }
                r0 = r30
                long r0 = r0.f19005     // Catch:{ all -> 0x00fc }
                r30 = r0
                r0 = r30
                r2 = r32
                r2.f19014 = r0     // Catch:{ all -> 0x00fc }
            L_0x025b:
                if (r19 <= 0) goto L_0x0269
                r0 = r19
                long r0 = (long) r0     // Catch:{ all -> 0x00fc }
                r30 = r0
                r0 = r32
                r1 = r30
                r0.m24511((long) r1)     // Catch:{ all -> 0x00fc }
            L_0x0269:
                if (r10 != 0) goto L_0x0006
                monitor-enter(r32)     // Catch:{ all -> 0x00fc }
                r0 = r32
                boolean r0 = r0.f19019     // Catch:{ all -> 0x02a4 }
                r30 = r0
                if (r30 != 0) goto L_0x0299
                r23 = 1
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x02a4 }
                monitor-exit(r32)     // Catch:{ all -> 0x02a4 }
                if (r23 != 0) goto L_0x001a
                monitor-enter(r32)
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19018 = r0     // Catch:{ all -> 0x028d }
                monitor-exit(r32)     // Catch:{ all -> 0x028d }
                goto L_0x001a
            L_0x028d:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x028d }
                throw r30
            L_0x0290:
                int r14 = r14 + 1
                if (r14 != r15) goto L_0x0295
                r14 = 0
            L_0x0295:
                int r7 = r7 + 1
                goto L_0x0161
            L_0x0299:
                r30 = 0
                r0 = r30
                r1 = r32
                r1.f19019 = r0     // Catch:{ all -> 0x02a4 }
                monitor-exit(r32)     // Catch:{ all -> 0x02a4 }
                goto L_0x0006
            L_0x02a4:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x02a4 }
                throw r30     // Catch:{ all -> 0x00fc }
            L_0x02a7:
                r30 = move-exception
                monitor-exit(r32)     // Catch:{ all -> 0x02a7 }
                throw r30
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorMerge.MergeSubscriber.m24681():void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʼ  reason: contains not printable characters */
        public boolean m24682() {
            if (this.f19025.isUnsubscribed()) {
                return true;
            }
            Queue<Throwable> e = this.f19011;
            if (this.f19022 || e == null || e.isEmpty()) {
                return false;
            }
            try {
                m24680();
                return true;
            } finally {
                unsubscribe();
            }
        }
    }

    static final class InnerSubscriber<T> extends Subscriber<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        static final int f19003 = (RxRingBuffer.f19311 / 4);

        /* renamed from: 连任  reason: contains not printable characters */
        int f19004;

        /* renamed from: 靐  reason: contains not printable characters */
        final long f19005;

        /* renamed from: 麤  reason: contains not printable characters */
        volatile RxRingBuffer f19006;

        /* renamed from: 齉  reason: contains not printable characters */
        volatile boolean f19007;

        /* renamed from: 龘  reason: contains not printable characters */
        final MergeSubscriber<T> f19008;

        public InnerSubscriber(MergeSubscriber<T> parent, long id) {
            this.f19008 = parent;
            this.f19005 = id;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24677() {
            this.f19004 = RxRingBuffer.f19311;
            m24511((long) RxRingBuffer.f19311);
        }

        public void onNext(T t) {
            this.f19008.m24695(this, t);
        }

        public void onError(Throwable e) {
            this.f19008.m24690().offer(e);
            this.f19007 = true;
            this.f19008.m24683();
        }

        public void onCompleted() {
            this.f19007 = true;
            this.f19008.m24683();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24678(long n) {
            int r = this.f19004 - ((int) n);
            if (r > f19003) {
                this.f19004 = r;
                return;
            }
            this.f19004 = RxRingBuffer.f19311;
            int k = RxRingBuffer.f19311 - r;
            if (k > 0) {
                m24511((long) k);
            }
        }
    }
}
