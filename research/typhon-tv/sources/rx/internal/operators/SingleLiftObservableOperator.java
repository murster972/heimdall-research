package rx.internal.operators;

import rx.Observable;
import rx.Producer;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.internal.operators.SingleFromObservable;
import rx.internal.producers.SingleProducer;
import rx.plugins.RxJavaHooks;

public final class SingleLiftObservableOperator<T, R> implements Single.OnSubscribe<R> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Observable.Operator<? extends R, ? super T> f19149;

    /* renamed from: 龘  reason: contains not printable characters */
    final Single.OnSubscribe<T> f19150;

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(SingleSubscriber<? super R> t) {
        Subscriber<R> outputAsSubscriber = new SingleFromObservable.WrapSingleIntoSubscriber<>(t);
        t.m24508((Subscription) outputAsSubscriber);
        try {
            Subscriber<? super T> inputAsSubscriber = (Subscriber) RxJavaHooks.m25006(this.f19149).call(outputAsSubscriber);
            SingleSubscriber<? super T> input = m24789(inputAsSubscriber);
            inputAsSubscriber.m24510();
            this.f19150.call(input);
        } catch (Throwable ex) {
            Exceptions.m24535(ex, (SingleSubscriber<?>) t);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> SingleSubscriber<T> m24789(Subscriber<T> subscriber) {
        WrapSubscriberIntoSingle<T> parent = new WrapSubscriberIntoSingle<>(subscriber);
        subscriber.m24513((Subscription) parent);
        return parent;
    }

    static final class WrapSubscriberIntoSingle<T> extends SingleSubscriber<T> {

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f19151;

        WrapSubscriberIntoSingle(Subscriber<? super T> actual) {
            this.f19151 = actual;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24791(T value) {
            this.f19151.m24512((Producer) new SingleProducer(this.f19151, value));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24792(Throwable error) {
            this.f19151.onError(error);
        }
    }
}
