package rx.internal.operators;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.observers.SerializedSubscriber;

public final class OperatorTakeUntil<T, E> implements Observable.Operator<T, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Observable<? extends E> f19125;

    public OperatorTakeUntil(Observable<? extends E> other) {
        this.f19125 = other;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super T> child) {
        final Subscriber<T> serial = new SerializedSubscriber<>(child, false);
        final Subscriber<T> main = new Subscriber<T>(false, serial) {
            public void onNext(T t) {
                serial.onNext(t);
            }

            public void onError(Throwable e) {
                try {
                    serial.onError(e);
                } finally {
                    serial.unsubscribe();
                }
            }

            public void onCompleted() {
                try {
                    serial.onCompleted();
                } finally {
                    serial.unsubscribe();
                }
            }
        };
        Subscriber<E> so = new Subscriber<E>() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m24778() {
                m24511(Long.MAX_VALUE);
            }

            public void onCompleted() {
                main.onCompleted();
            }

            public void onError(Throwable e) {
                main.onError(e);
            }

            public void onNext(E e) {
                onCompleted();
            }
        };
        serial.m24513((Subscription) main);
        serial.m24513((Subscription) so);
        child.m24513((Subscription) serial);
        this.f19125.m7416(so);
        return main;
    }
}
