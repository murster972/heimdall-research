package rx.internal.operators;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;
import rx.Subscriber;
import rx.functions.Func1;
import rx.internal.util.UtilityFunctions;

public final class BackpressureUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    public static long m24552(AtomicLong requested, long n) {
        long current;
        do {
            current = requested.get();
        } while (!requested.compareAndSet(current, m24548(current, n)));
        return current;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m24551(long a, long b) {
        long u = a * b;
        if (((a | b) >>> 31) == 0 || b == 0 || u / b == a) {
            return u;
        }
        return Long.MAX_VALUE;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static long m24548(long a, long b) {
        long u = a + b;
        if (u < 0) {
            return Long.MAX_VALUE;
        }
        return u;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> void m24553(AtomicLong requested, Queue<T> queue, Subscriber<? super T> actual) {
        m24554(requested, queue, actual, UtilityFunctions.m24924());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> boolean m24556(AtomicLong requested, long n, Queue<T> queue, Subscriber<? super T> actual) {
        return m24557(requested, n, queue, actual, UtilityFunctions.m24924());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T, R> void m24554(AtomicLong requested, Queue<T> queue, Subscriber<? super R> actual, Func1<? super T, ? extends R> exitTransform) {
        long r;
        do {
            r = requested.get();
            if ((r & Long.MIN_VALUE) != 0) {
                return;
            }
        } while (!requested.compareAndSet(r, r | Long.MIN_VALUE));
        if (r != 0) {
            m24550(requested, queue, actual, exitTransform);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T, R> boolean m24557(AtomicLong requested, long n, Queue<T> queue, Subscriber<? super R> actual, Func1<? super T, ? extends R> exitTransform) {
        long r;
        long c;
        if (n < 0) {
            throw new IllegalArgumentException("n >= 0 required but it was " + n);
        } else if (n != 0) {
            do {
                r = requested.get();
                c = r & Long.MIN_VALUE;
            } while (!requested.compareAndSet(r, m24548(r & Long.MAX_VALUE, n) | c));
            if (r != Long.MIN_VALUE) {
                return c == 0;
            }
            m24550(requested, queue, actual, exitTransform);
            return false;
        } else if ((requested.get() & Long.MIN_VALUE) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static <T, R> void m24550(AtomicLong requested, Queue<T> queue, Subscriber<? super R> subscriber, Func1<? super T, ? extends R> exitTransform) {
        long r = requested.get();
        if (r == Long.MAX_VALUE) {
            while (!subscriber.isUnsubscribed()) {
                T v = queue.poll();
                if (v == null) {
                    subscriber.onCompleted();
                    return;
                }
                subscriber.onNext(exitTransform.call(v));
            }
            return;
        }
        long e = Long.MIN_VALUE;
        while (true) {
            if (e == r) {
                if (e == r) {
                    if (subscriber.isUnsubscribed()) {
                        return;
                    }
                    if (queue.isEmpty()) {
                        subscriber.onCompleted();
                        return;
                    }
                }
                r = requested.get();
                if (r == e) {
                    r = requested.addAndGet(-(e & Long.MAX_VALUE));
                    if (r != Long.MIN_VALUE) {
                        e = Long.MIN_VALUE;
                    } else {
                        return;
                    }
                } else {
                    continue;
                }
            } else if (!subscriber.isUnsubscribed()) {
                T v2 = queue.poll();
                if (v2 == null) {
                    subscriber.onCompleted();
                    return;
                } else {
                    subscriber.onNext(exitTransform.call(v2));
                    e++;
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static long m24549(AtomicLong requested, long n) {
        long current;
        long next;
        do {
            current = requested.get();
            if (current == Long.MAX_VALUE) {
                return Long.MAX_VALUE;
            }
            next = current - n;
            if (next < 0) {
                throw new IllegalStateException("More produced than requested: " + next);
            }
        } while (!requested.compareAndSet(current, next));
        return next;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m24555(long n) {
        if (n >= 0) {
            return n != 0;
        }
        throw new IllegalArgumentException("n >= 0 required but it was " + n);
    }
}
