package rx.internal.operators;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action0;
import rx.subjects.Subject;
import rx.subscriptions.Subscriptions;

public final class BufferUntilSubscriber<T> extends Subject<T, T> {

    /* renamed from: 齉  reason: contains not printable characters */
    static final Observer f18790 = new Observer() {
        public void onCompleted() {
        }

        public void onError(Throwable e) {
        }

        public void onNext(Object t) {
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    final State<T> f18791;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f18792;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static <T> BufferUntilSubscriber<T> m24558() {
        return new BufferUntilSubscriber<>(new State<>());
    }

    static final class State<T> extends AtomicReference<Observer<? super T>> {
        private static final long serialVersionUID = 8026705089538090368L;
        final ConcurrentLinkedQueue<Object> buffer = new ConcurrentLinkedQueue<>();
        boolean emitting;
        final Object guard = new Object();

        State() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24562(Observer<? super T> expected, Observer<? super T> next) {
            return compareAndSet(expected, next);
        }
    }

    static final class OnSubscribeAction<T> implements Observable.OnSubscribe<T> {

        /* renamed from: 龘  reason: contains not printable characters */
        final State<T> f18793;

        public OnSubscribeAction(State<T> state) {
            this.f18793 = state;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void call(Subscriber<? super T> s) {
            if (this.f18793.m24562((Observer) null, s)) {
                s.m24513(Subscriptions.m25090(new Action0() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m24561() {
                        OnSubscribeAction.this.f18793.set(BufferUntilSubscriber.f18790);
                    }
                }));
                boolean win = false;
                synchronized (this.f18793.guard) {
                    if (!this.f18793.emitting) {
                        this.f18793.emitting = true;
                        win = true;
                    }
                }
                if (win) {
                    while (true) {
                        Object o = this.f18793.buffer.poll();
                        if (o != null) {
                            NotificationLite.m24569((Observer) this.f18793.get(), o);
                        } else {
                            synchronized (this.f18793.guard) {
                                if (this.f18793.buffer.isEmpty()) {
                                    this.f18793.emitting = false;
                                    return;
                                }
                            }
                        }
                    }
                }
            } else {
                s.onError(new IllegalStateException("Only one subscriber allowed!"));
            }
        }
    }

    private BufferUntilSubscriber(State<T> state) {
        super(new OnSubscribeAction(state));
        this.f18791 = state;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m24559(Object v) {
        synchronized (this.f18791.guard) {
            this.f18791.buffer.add(v);
            if (this.f18791.get() != null && !this.f18791.emitting) {
                this.f18792 = true;
                this.f18791.emitting = true;
            }
        }
        if (this.f18792) {
            while (true) {
                Object o = this.f18791.buffer.poll();
                if (o != null) {
                    NotificationLite.m24569((Observer) this.f18791.get(), o);
                } else {
                    return;
                }
            }
        }
    }

    public void onCompleted() {
        if (this.f18792) {
            ((Observer) this.f18791.get()).onCompleted();
        } else {
            m24559(NotificationLite.m24566());
        }
    }

    public void onError(Throwable e) {
        if (this.f18792) {
            ((Observer) this.f18791.get()).onError(e);
        } else {
            m24559(NotificationLite.m24568(e));
        }
    }

    public void onNext(T t) {
        if (this.f18792) {
            ((Observer) this.f18791.get()).onNext(t);
        } else {
            m24559(NotificationLite.m24567(t));
        }
    }
}
