package rx.internal.operators;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.internal.util.OpenHashSet;
import rx.observables.ConnectableObservable;
import rx.schedulers.Timestamped;
import rx.subscriptions.Subscriptions;

public final class OperatorReplay<T> extends ConnectableObservable<T> implements Subscription {

    /* renamed from: 连任  reason: contains not printable characters */
    static final Func0 f19062 = new Func0() {
        public Object call() {
            return new UnboundedReplayBuffer(16);
        }
    };

    /* renamed from: 靐  reason: contains not printable characters */
    final Observable<? extends T> f19063;

    /* renamed from: 麤  reason: contains not printable characters */
    final Func0<? extends ReplayBuffer<T>> f19064;

    /* renamed from: 齉  reason: contains not printable characters */
    final AtomicReference<ReplaySubscriber<T>> f19065;

    interface ReplayBuffer<T> {
        /* renamed from: 齉  reason: contains not printable characters */
        void m24742();

        /* renamed from: 龘  reason: contains not printable characters */
        void m24743(T t);

        /* renamed from: 龘  reason: contains not printable characters */
        void m24744(Throwable th);

        /* renamed from: 龘  reason: contains not printable characters */
        void m24745(InnerProducer<T> innerProducer);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static <T> ConnectableObservable<T> m24718(Observable<? extends T> source) {
        return m24722(source, f19062);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> ConnectableObservable<T> m24719(Observable<? extends T> source, final int bufferSize) {
        if (bufferSize == Integer.MAX_VALUE) {
            return m24718(source);
        }
        return m24722(source, new Func0<ReplayBuffer<T>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public ReplayBuffer<T> call() {
                return new SizeBoundReplayBuffer(bufferSize);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> ConnectableObservable<T> m24720(Observable<? extends T> source, long maxAge, TimeUnit unit, Scheduler scheduler) {
        return m24721(source, maxAge, unit, scheduler, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> ConnectableObservable<T> m24721(Observable<? extends T> source, long maxAge, TimeUnit unit, final Scheduler scheduler, final int bufferSize) {
        final long maxAgeInMillis = unit.toMillis(maxAge);
        return m24722(source, new Func0<ReplayBuffer<T>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public ReplayBuffer<T> call() {
                return new SizeAndTimeBoundReplayBuffer(bufferSize, maxAgeInMillis, scheduler);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> ConnectableObservable<T> m24722(Observable<? extends T> source, final Func0<? extends ReplayBuffer<T>> bufferFactory) {
        final AtomicReference<ReplaySubscriber<T>> curr = new AtomicReference<>();
        return new OperatorReplay(new Observable.OnSubscribe<T>() {
            /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
            /* renamed from: 龘  reason: contains not printable characters */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void call(rx.Subscriber<? super T> r5) {
                /*
                    r4 = this;
                L_0x0000:
                    java.util.concurrent.atomic.AtomicReference r3 = r0
                    java.lang.Object r1 = r3.get()
                    rx.internal.operators.OperatorReplay$ReplaySubscriber r1 = (rx.internal.operators.OperatorReplay.ReplaySubscriber) r1
                    if (r1 != 0) goto L_0x0023
                    rx.internal.operators.OperatorReplay$ReplaySubscriber r2 = new rx.internal.operators.OperatorReplay$ReplaySubscriber
                    rx.functions.Func0 r3 = r4
                    java.lang.Object r3 = r3.call()
                    rx.internal.operators.OperatorReplay$ReplayBuffer r3 = (rx.internal.operators.OperatorReplay.ReplayBuffer) r3
                    r2.<init>(r3)
                    r2.m24750()
                    java.util.concurrent.atomic.AtomicReference r3 = r0
                    boolean r3 = r3.compareAndSet(r1, r2)
                    if (r3 == 0) goto L_0x0000
                    r1 = r2
                L_0x0023:
                    rx.internal.operators.OperatorReplay$InnerProducer r0 = new rx.internal.operators.OperatorReplay$InnerProducer
                    r0.<init>(r1, r5)
                    r1.m24753(r0)
                    r5.m24513((rx.Subscription) r0)
                    rx.internal.operators.OperatorReplay$ReplayBuffer<T> r3 = r1.f19086
                    r3.m24745(r0)
                    r5.m24512((rx.Producer) r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorReplay.AnonymousClass7.call(rx.Subscriber):void");
            }
        }, source, curr, bufferFactory);
    }

    private OperatorReplay(Observable.OnSubscribe<T> onSubscribe, Observable<? extends T> source, AtomicReference<ReplaySubscriber<T>> current, Func0<? extends ReplayBuffer<T>> bufferFactory) {
        super(onSubscribe);
        this.f19063 = source;
        this.f19065 = current;
        this.f19064 = bufferFactory;
    }

    public void unsubscribe() {
        this.f19065.lazySet((Object) null);
    }

    public boolean isUnsubscribed() {
        ReplaySubscriber<T> ps = this.f19065.get();
        return ps == null || ps.isUnsubscribed();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m24723(Action1<? super Subscription> connection) {
        ReplaySubscriber<T> ps;
        boolean doConnect = true;
        while (true) {
            ps = this.f19065.get();
            if (ps != null && !ps.isUnsubscribed()) {
                break;
            }
            ReplaySubscriber<T> u = new ReplaySubscriber<>((ReplayBuffer) this.f19064.call());
            u.m24750();
            if (this.f19065.compareAndSet(ps, u)) {
                ps = u;
                break;
            }
        }
        if (ps.f19082.get() || !ps.f19082.compareAndSet(false, true)) {
            doConnect = false;
        }
        connection.call(ps);
        if (doConnect) {
            this.f19063.m7416(ps);
        }
    }

    static final class ReplaySubscriber<T> extends Subscriber<T> implements Subscription {

        /* renamed from: 麤  reason: contains not printable characters */
        static final InnerProducer[] f19072 = new InnerProducer[0];

        /* renamed from: 齉  reason: contains not printable characters */
        static final InnerProducer[] f19073 = new InnerProducer[0];

        /* renamed from: ʻ  reason: contains not printable characters */
        final OpenHashSet<InnerProducer<T>> f19074 = new OpenHashSet<>();

        /* renamed from: ʼ  reason: contains not printable characters */
        InnerProducer<T>[] f19075 = f19073;

        /* renamed from: ʽ  reason: contains not printable characters */
        volatile long f19076;

        /* renamed from: ʾ  reason: contains not printable characters */
        long f19077;

        /* renamed from: ʿ  reason: contains not printable characters */
        long f19078;

        /* renamed from: ˈ  reason: contains not printable characters */
        boolean f19079;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f19080;

        /* renamed from: ˑ  reason: contains not printable characters */
        long f19081;

        /* renamed from: ٴ  reason: contains not printable characters */
        final AtomicBoolean f19082 = new AtomicBoolean();

        /* renamed from: ᐧ  reason: contains not printable characters */
        boolean f19083;

        /* renamed from: 连任  reason: contains not printable characters */
        volatile boolean f19084;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f19085;

        /* renamed from: 龘  reason: contains not printable characters */
        final ReplayBuffer<T> f19086;

        /* renamed from: ﹶ  reason: contains not printable characters */
        volatile Producer f19087;

        /* renamed from: ﾞ  reason: contains not printable characters */
        List<InnerProducer<T>> f19088;

        public ReplaySubscriber(ReplayBuffer<T> buffer) {
            this.f19086 = buffer;
            m24511(0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24750() {
            m24513(Subscriptions.m25090(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24754() {
                    if (!ReplaySubscriber.this.f19084) {
                        synchronized (ReplaySubscriber.this.f19074) {
                            if (!ReplaySubscriber.this.f19084) {
                                ReplaySubscriber.this.f19074.m24893();
                                ReplaySubscriber.this.f19076++;
                                ReplaySubscriber.this.f19084 = true;
                            }
                        }
                    }
                }
            }));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24753(InnerProducer<T> producer) {
            if (producer == null) {
                throw new NullPointerException();
            } else if (this.f19084) {
                return false;
            } else {
                synchronized (this.f19074) {
                    if (this.f19084) {
                        return false;
                    }
                    this.f19074.m24895(producer);
                    this.f19076++;
                    return true;
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24746(InnerProducer<T> producer) {
            if (!this.f19084) {
                synchronized (this.f19074) {
                    if (!this.f19084) {
                        this.f19074.m24890(producer);
                        if (this.f19074.m24892()) {
                            this.f19075 = f19073;
                        }
                        this.f19076++;
                    }
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24752(Producer p) {
            if (this.f19087 != null) {
                throw new IllegalStateException("Only a single producer can be set on a Subscriber.");
            }
            this.f19087 = p;
            m24748((InnerProducer) null);
            m24747();
        }

        public void onNext(T t) {
            if (!this.f19085) {
                this.f19086.m24743(t);
                m24747();
            }
        }

        public void onError(Throwable e) {
            if (!this.f19085) {
                this.f19085 = true;
                try {
                    this.f19086.m24744(e);
                    m24747();
                } finally {
                    unsubscribe();
                }
            }
        }

        public void onCompleted() {
            if (!this.f19085) {
                this.f19085 = true;
                try {
                    this.f19086.m24742();
                    m24747();
                } finally {
                    unsubscribe();
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x002d, code lost:
            r6 = r14.f19077;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x002f, code lost:
            if (r15 == null) goto L_0x0051;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0031, code lost:
            r2 = java.lang.Math.max(r6, r15.totalRequested.get());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x003b, code lost:
            m24751(r2, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0042, code lost:
            if (isUnsubscribed() != false) goto L_0x0007;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0044, code lost:
            monitor-enter(r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0047, code lost:
            if (r14.f19079 != false) goto L_0x006b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0049, code lost:
            r14.f19083 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x004c, code lost:
            monitor-exit(r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0051, code lost:
            r2 = r6;
            r0 = m24749();
            r10 = r0.length;
            r8 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0058, code lost:
            if (r8 >= r10) goto L_0x003b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x005a, code lost:
            r5 = r0[r8];
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x005c, code lost:
            if (r5 == null) goto L_0x0068;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x005e, code lost:
            r2 = java.lang.Math.max(r2, r5.totalRequested.get());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0068, code lost:
            r8 = r8 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
            r14.f19079 = false;
            r4 = r14.f19088;
            r14.f19088 = null;
            r1 = r14.f19080;
            r14.f19080 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x0078, code lost:
            monitor-exit(r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x0079, code lost:
            r6 = r14.f19077;
            r2 = r6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x007c, code lost:
            if (r4 == null) goto L_0x0099;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x007e, code lost:
            r8 = r4.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x0086, code lost:
            if (r8.hasNext() == false) goto L_0x0099;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0088, code lost:
            r2 = java.lang.Math.max(r2, r8.next().totalRequested.get());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x0099, code lost:
            if (r1 == false) goto L_0x00b4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x009b, code lost:
            r0 = m24749();
            r10 = r0.length;
            r8 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x00a1, code lost:
            if (r8 >= r10) goto L_0x00b4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x00a3, code lost:
            r5 = r0[r8];
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x00a5, code lost:
            if (r5 == null) goto L_0x00b1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x00a7, code lost:
            r2 = java.lang.Math.max(r2, r5.totalRequested.get());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x00b1, code lost:
            r8 = r8 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x00b4, code lost:
            m24751(r2, r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
            return;
         */
        /* renamed from: 齉  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m24748(rx.internal.operators.OperatorReplay.InnerProducer<T> r15) {
            /*
                r14 = this;
                r9 = 0
                boolean r8 = r14.isUnsubscribed()
                if (r8 == 0) goto L_0x0008
            L_0x0007:
                return
            L_0x0008:
                monitor-enter(r14)
                boolean r8 = r14.f19083     // Catch:{ all -> 0x0022 }
                if (r8 == 0) goto L_0x0029
                if (r15 == 0) goto L_0x0025
                java.util.List<rx.internal.operators.OperatorReplay$InnerProducer<T>> r4 = r14.f19088     // Catch:{ all -> 0x0022 }
                if (r4 != 0) goto L_0x001a
                java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x0022 }
                r4.<init>()     // Catch:{ all -> 0x0022 }
                r14.f19088 = r4     // Catch:{ all -> 0x0022 }
            L_0x001a:
                r4.add(r15)     // Catch:{ all -> 0x0022 }
            L_0x001d:
                r8 = 1
                r14.f19079 = r8     // Catch:{ all -> 0x0022 }
                monitor-exit(r14)     // Catch:{ all -> 0x0022 }
                goto L_0x0007
            L_0x0022:
                r8 = move-exception
                monitor-exit(r14)     // Catch:{ all -> 0x0022 }
                throw r8
            L_0x0025:
                r8 = 1
                r14.f19080 = r8     // Catch:{ all -> 0x0022 }
                goto L_0x001d
            L_0x0029:
                r8 = 1
                r14.f19083 = r8     // Catch:{ all -> 0x0022 }
                monitor-exit(r14)     // Catch:{ all -> 0x0022 }
                long r6 = r14.f19077
                if (r15 == 0) goto L_0x0051
                java.util.concurrent.atomic.AtomicLong r8 = r15.totalRequested
                long r10 = r8.get()
                long r2 = java.lang.Math.max(r6, r10)
            L_0x003b:
                r14.m24751(r2, r6)
            L_0x003e:
                boolean r8 = r14.isUnsubscribed()
                if (r8 != 0) goto L_0x0007
                monitor-enter(r14)
                boolean r8 = r14.f19079     // Catch:{ all -> 0x004e }
                if (r8 != 0) goto L_0x006b
                r8 = 0
                r14.f19083 = r8     // Catch:{ all -> 0x004e }
                monitor-exit(r14)     // Catch:{ all -> 0x004e }
                goto L_0x0007
            L_0x004e:
                r8 = move-exception
                monitor-exit(r14)     // Catch:{ all -> 0x004e }
                throw r8
            L_0x0051:
                r2 = r6
                rx.internal.operators.OperatorReplay$InnerProducer[] r0 = r14.m24749()
                int r10 = r0.length
                r8 = r9
            L_0x0058:
                if (r8 >= r10) goto L_0x003b
                r5 = r0[r8]
                if (r5 == 0) goto L_0x0068
                java.util.concurrent.atomic.AtomicLong r11 = r5.totalRequested
                long r12 = r11.get()
                long r2 = java.lang.Math.max(r2, r12)
            L_0x0068:
                int r8 = r8 + 1
                goto L_0x0058
            L_0x006b:
                r8 = 0
                r14.f19079 = r8     // Catch:{ all -> 0x004e }
                java.util.List<rx.internal.operators.OperatorReplay$InnerProducer<T>> r4 = r14.f19088     // Catch:{ all -> 0x004e }
                r8 = 0
                r14.f19088 = r8     // Catch:{ all -> 0x004e }
                boolean r1 = r14.f19080     // Catch:{ all -> 0x004e }
                r8 = 0
                r14.f19080 = r8     // Catch:{ all -> 0x004e }
                monitor-exit(r14)     // Catch:{ all -> 0x004e }
                long r6 = r14.f19077
                r2 = r6
                if (r4 == 0) goto L_0x0099
                java.util.Iterator r8 = r4.iterator()
            L_0x0082:
                boolean r10 = r8.hasNext()
                if (r10 == 0) goto L_0x0099
                java.lang.Object r5 = r8.next()
                rx.internal.operators.OperatorReplay$InnerProducer r5 = (rx.internal.operators.OperatorReplay.InnerProducer) r5
                java.util.concurrent.atomic.AtomicLong r10 = r5.totalRequested
                long r10 = r10.get()
                long r2 = java.lang.Math.max(r2, r10)
                goto L_0x0082
            L_0x0099:
                if (r1 == 0) goto L_0x00b4
                rx.internal.operators.OperatorReplay$InnerProducer[] r0 = r14.m24749()
                int r10 = r0.length
                r8 = r9
            L_0x00a1:
                if (r8 >= r10) goto L_0x00b4
                r5 = r0[r8]
                if (r5 == 0) goto L_0x00b1
                java.util.concurrent.atomic.AtomicLong r11 = r5.totalRequested
                long r12 = r11.get()
                long r2 = java.lang.Math.max(r2, r12)
            L_0x00b1:
                int r8 = r8 + 1
                goto L_0x00a1
            L_0x00b4:
                r14.m24751(r2, r6)
                goto L_0x003e
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorReplay.ReplaySubscriber.m24748(rx.internal.operators.OperatorReplay$InnerProducer):void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public InnerProducer<T>[] m24749() {
            InnerProducer<T>[] result;
            synchronized (this.f19074) {
                Object[] a = this.f19074.m24891();
                int n = a.length;
                result = new InnerProducer[n];
                System.arraycopy(a, 0, result, 0, n);
            }
            return result;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24751(long maxTotalRequests, long previousTotalRequests) {
            long ur = this.f19078;
            Producer p = this.f19087;
            long diff = maxTotalRequests - previousTotalRequests;
            if (diff != 0) {
                this.f19077 = maxTotalRequests;
                if (p == null) {
                    long u = ur + diff;
                    if (u < 0) {
                        u = Long.MAX_VALUE;
                    }
                    this.f19078 = u;
                } else if (ur != 0) {
                    this.f19078 = 0;
                    p.request(ur + diff);
                } else {
                    p.request(diff);
                }
            } else if (ur != 0 && p != null) {
                this.f19078 = 0;
                p.request(ur);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m24747() {
            InnerProducer<T>[] pc = this.f19075;
            if (this.f19081 != this.f19076) {
                synchronized (this.f19074) {
                    pc = this.f19075;
                    Object[] a = this.f19074.m24891();
                    int n = a.length;
                    if (pc.length != n) {
                        pc = new InnerProducer[n];
                        this.f19075 = pc;
                    }
                    System.arraycopy(a, 0, pc, 0, n);
                    this.f19081 = this.f19076;
                }
            }
            ReplayBuffer<T> b = this.f19086;
            for (InnerProducer<T> rp : pc) {
                if (rp != null) {
                    b.m24745(rp);
                }
            }
        }
    }

    static final class InnerProducer<T> extends AtomicLong implements Producer, Subscription {
        private static final long serialVersionUID = -4453897557930727610L;
        Subscriber<? super T> child;
        boolean emitting;
        Object index;
        boolean missed;
        final ReplaySubscriber<T> parent;
        final AtomicLong totalRequested = new AtomicLong();

        public InnerProducer(ReplaySubscriber<T> parent2, Subscriber<? super T> child2) {
            this.parent = parent2;
            this.child = child2;
        }

        public void request(long n) {
            long r;
            long u;
            if (n >= 0) {
                do {
                    r = get();
                    if (r == Long.MIN_VALUE) {
                        return;
                    }
                    if (r < 0 || n != 0) {
                        u = r + n;
                        if (u < 0) {
                            u = Long.MAX_VALUE;
                        }
                    } else {
                        return;
                    }
                } while (!compareAndSet(r, u));
                m24741(n);
                this.parent.m24748(this);
                this.parent.f19086.m24745(this);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24741(long n) {
            long r;
            long u;
            do {
                r = this.totalRequested.get();
                u = r + n;
                if (u < 0) {
                    u = Long.MAX_VALUE;
                }
            } while (!this.totalRequested.compareAndSet(r, u));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m24739(long n) {
            long r;
            long u;
            if (n <= 0) {
                throw new IllegalArgumentException("Cant produce zero or less");
            }
            do {
                r = get();
                if (r == Long.MIN_VALUE) {
                    return Long.MIN_VALUE;
                }
                u = r - n;
                if (u < 0) {
                    throw new IllegalStateException("More produced (" + n + ") than requested (" + r + ")");
                }
            } while (!compareAndSet(r, u));
            return u;
        }

        public boolean isUnsubscribed() {
            return get() == Long.MIN_VALUE;
        }

        public void unsubscribe() {
            if (get() != Long.MIN_VALUE && getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE) {
                this.parent.m24746(this);
                this.parent.m24748(this);
                this.child = null;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public <U> U m24740() {
            return this.index;
        }
    }

    static final class UnboundedReplayBuffer<T> extends ArrayList<Object> implements ReplayBuffer<T> {
        private static final long serialVersionUID = 7063189396499112664L;
        volatile int size;

        public UnboundedReplayBuffer(int capacityHint) {
            super(capacityHint);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24762(T value) {
            add(NotificationLite.m24567(value));
            this.size++;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24763(Throwable e) {
            add(NotificationLite.m24568(e));
            this.size++;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m24761() {
            add(NotificationLite.m24566());
            this.size++;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
            if (r15.isUnsubscribed() != false) goto L_0x000a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
            r7 = r14.size;
            r2 = (java.lang.Integer) r15.m24740();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
            if (r2 == null) goto L_0x004c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
            r1 = r2.intValue();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0023, code lost:
            r0 = r15.child;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
            if (r0 == null) goto L_0x000a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0027, code lost:
            r8 = r15.get();
            r4 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x002f, code lost:
            if (r4 == r8) goto L_0x006d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
            if (r1 >= r7) goto L_0x006d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
            r6 = get(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x003b, code lost:
            if (rx.internal.operators.NotificationLite.m24569(r0, r6) != false) goto L_0x000a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0041, code lost:
            if (r15.isUnsubscribed() != false) goto L_0x000a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0043, code lost:
            r1 = r1 + 1;
            r4 = r4 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x004c, code lost:
            r1 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x004e, code lost:
            r3 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x004f, code lost:
            rx.exceptions.Exceptions.m24529(r3);
            r15.unsubscribe();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0059, code lost:
            if (rx.internal.operators.NotificationLite.m24565(r6) != false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x0061, code lost:
            r0.onError(rx.exceptions.OnErrorThrowable.addValueAsLastCause(r3, rx.internal.operators.NotificationLite.m24564(r6)));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0071, code lost:
            if (r4 == 0) goto L_0x0085;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0073, code lost:
            r15.index = java.lang.Integer.valueOf(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0080, code lost:
            if (r8 == Long.MAX_VALUE) goto L_0x0085;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0082, code lost:
            r15.m24739(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0085, code lost:
            monitor-enter(r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x0088, code lost:
            if (r15.missed != false) goto L_0x0093;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x008a, code lost:
            r15.emitting = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x008d, code lost:
            monitor-exit(r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
            r15.missed = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x0096, code lost:
            monitor-exit(r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m24764(rx.internal.operators.OperatorReplay.InnerProducer<T> r15) {
            /*
                r14 = this;
                r10 = 0
                monitor-enter(r15)
                boolean r11 = r15.emitting     // Catch:{ all -> 0x0049 }
                if (r11 == 0) goto L_0x000b
                r10 = 1
                r15.missed = r10     // Catch:{ all -> 0x0049 }
                monitor-exit(r15)     // Catch:{ all -> 0x0049 }
            L_0x000a:
                return
            L_0x000b:
                r11 = 1
                r15.emitting = r11     // Catch:{ all -> 0x0049 }
                monitor-exit(r15)     // Catch:{ all -> 0x0049 }
            L_0x000f:
                boolean r11 = r15.isUnsubscribed()
                if (r11 != 0) goto L_0x000a
                int r7 = r14.size
                java.lang.Object r2 = r15.m24740()
                java.lang.Integer r2 = (java.lang.Integer) r2
                if (r2 == 0) goto L_0x004c
                int r1 = r2.intValue()
            L_0x0023:
                rx.Subscriber<? super T> r0 = r15.child
                if (r0 == 0) goto L_0x000a
                long r8 = r15.get()
                r4 = 0
            L_0x002d:
                int r11 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
                if (r11 == 0) goto L_0x006d
                if (r1 >= r7) goto L_0x006d
                java.lang.Object r6 = r14.get(r1)
                boolean r11 = rx.internal.operators.NotificationLite.m24569(r0, r6)     // Catch:{ Throwable -> 0x004e }
                if (r11 != 0) goto L_0x000a
                boolean r11 = r15.isUnsubscribed()
                if (r11 != 0) goto L_0x000a
                int r1 = r1 + 1
                r12 = 1
                long r4 = r4 + r12
                goto L_0x002d
            L_0x0049:
                r10 = move-exception
                monitor-exit(r15)     // Catch:{ all -> 0x0049 }
                throw r10
            L_0x004c:
                r1 = r10
                goto L_0x0023
            L_0x004e:
                r3 = move-exception
                rx.exceptions.Exceptions.m24529(r3)
                r15.unsubscribe()
                boolean r10 = rx.internal.operators.NotificationLite.m24565(r6)
                if (r10 != 0) goto L_0x000a
                boolean r10 = rx.internal.operators.NotificationLite.m24563(r6)
                if (r10 != 0) goto L_0x000a
                java.lang.Object r10 = rx.internal.operators.NotificationLite.m24564(r6)
                java.lang.Throwable r10 = rx.exceptions.OnErrorThrowable.addValueAsLastCause(r3, r10)
                r0.onError(r10)
                goto L_0x000a
            L_0x006d:
                r12 = 0
                int r11 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
                if (r11 == 0) goto L_0x0085
                java.lang.Integer r11 = java.lang.Integer.valueOf(r1)
                r15.index = r11
                r12 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r11 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
                if (r11 == 0) goto L_0x0085
                r15.m24739(r4)
            L_0x0085:
                monitor-enter(r15)
                boolean r11 = r15.missed     // Catch:{ all -> 0x0090 }
                if (r11 != 0) goto L_0x0093
                r10 = 0
                r15.emitting = r10     // Catch:{ all -> 0x0090 }
                monitor-exit(r15)     // Catch:{ all -> 0x0090 }
                goto L_0x000a
            L_0x0090:
                r10 = move-exception
                monitor-exit(r15)     // Catch:{ all -> 0x0090 }
                throw r10
            L_0x0093:
                r11 = 0
                r15.missed = r11     // Catch:{ all -> 0x0090 }
                monitor-exit(r15)     // Catch:{ all -> 0x0090 }
                goto L_0x000f
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorReplay.UnboundedReplayBuffer.m24764(rx.internal.operators.OperatorReplay$InnerProducer):void");
        }
    }

    static final class Node extends AtomicReference<Node> {
        private static final long serialVersionUID = 245354315435971818L;
        final long index;
        final Object value;

        public Node(Object value2, long index2) {
            this.value = value2;
            this.index = index2;
        }
    }

    static class BoundedReplayBuffer<T> extends AtomicReference<Node> implements ReplayBuffer<T> {
        private static final long serialVersionUID = 2346567790059478686L;
        long index;
        int size;
        Node tail;

        public BoundedReplayBuffer() {
            Node n = new Node((Object) null, 0);
            this.tail = n;
            set(n);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m24738(Node n) {
            this.tail.set(n);
            this.tail = n;
            this.size++;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m24734() {
            Node next = (Node) ((Node) get()).get();
            if (next == null) {
                throw new IllegalStateException("Empty list!");
            }
            this.size--;
            m24730(next);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m24730(Node n) {
            set(n);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Node m24729() {
            return (Node) get();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m24735(T value) {
            Object o = m24728(NotificationLite.m24567(value));
            long j = this.index + 1;
            this.index = j;
            m24738(new Node(o, j));
            m24731();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public final void m24736(Throwable e) {
            Object o = m24728(NotificationLite.m24568(e));
            long j = this.index + 1;
            this.index = j;
            m24738(new Node(o, j));
            m24727();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public final void m24733() {
            Object o = m24728(NotificationLite.m24566());
            long j = this.index + 1;
            this.index = j;
            m24738(new Node(o, j));
            m24727();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
            if (r14.isUnsubscribed() != false) goto L_0x000a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
            r4 = (rx.internal.operators.OperatorReplay.Node) r14.m24740();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x001b, code lost:
            if (r4 != null) goto L_0x0028;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
            r4 = m24729();
            r14.index = r4;
            r14.m24741(r4.index);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
            if (r14.isUnsubscribed() != false) goto L_0x000a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
            r0 = r14.child;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0030, code lost:
            if (r0 == null) goto L_0x000a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0032, code lost:
            r6 = r14.get();
            r2 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
            if (r2 == r6) goto L_0x0083;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x003c, code lost:
            r8 = (rx.internal.operators.OperatorReplay.Node) r4.get();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
            if (r8 == null) goto L_0x0083;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0044, code lost:
            r5 = m24732(r8.value);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x004e, code lost:
            if (rx.internal.operators.NotificationLite.m24569(r0, r5) == false) goto L_0x0078;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0050, code lost:
            r14.index = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0054, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0055, code lost:
            r14.index = null;
            rx.exceptions.Exceptions.m24529(r1);
            r14.unsubscribe();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0061, code lost:
            if (rx.internal.operators.NotificationLite.m24565(r5) != false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0069, code lost:
            r0.onError(rx.exceptions.OnErrorThrowable.addValueAsLastCause(r1, rx.internal.operators.NotificationLite.m24564(r5)));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0078, code lost:
            r2 = r2 + 1;
            r4 = r8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0080, code lost:
            if (r14.isUnsubscribed() == false) goto L_0x0038;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0087, code lost:
            if (r2 == 0) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0089, code lost:
            r14.index = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0092, code lost:
            if (r6 == Long.MAX_VALUE) goto L_0x0097;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0094, code lost:
            r14.m24739(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0097, code lost:
            monitor-enter(r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x009a, code lost:
            if (r14.missed != false) goto L_0x00a5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x009c, code lost:
            r14.emitting = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x009f, code lost:
            monitor-exit(r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
            r14.missed = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x00a8, code lost:
            monitor-exit(r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void m24737(rx.internal.operators.OperatorReplay.InnerProducer<T> r14) {
            /*
                r13 = this;
                r12 = 0
                monitor-enter(r14)
                boolean r9 = r14.emitting     // Catch:{ all -> 0x0075 }
                if (r9 == 0) goto L_0x000b
                r9 = 1
                r14.missed = r9     // Catch:{ all -> 0x0075 }
                monitor-exit(r14)     // Catch:{ all -> 0x0075 }
            L_0x000a:
                return
            L_0x000b:
                r9 = 1
                r14.emitting = r9     // Catch:{ all -> 0x0075 }
                monitor-exit(r14)     // Catch:{ all -> 0x0075 }
            L_0x000f:
                boolean r9 = r14.isUnsubscribed()
                if (r9 != 0) goto L_0x000a
                java.lang.Object r4 = r14.m24740()
                rx.internal.operators.OperatorReplay$Node r4 = (rx.internal.operators.OperatorReplay.Node) r4
                if (r4 != 0) goto L_0x0028
                rx.internal.operators.OperatorReplay$Node r4 = r13.m24729()
                r14.index = r4
                long r10 = r4.index
                r14.m24741(r10)
            L_0x0028:
                boolean r9 = r14.isUnsubscribed()
                if (r9 != 0) goto L_0x000a
                rx.Subscriber<? super T> r0 = r14.child
                if (r0 == 0) goto L_0x000a
                long r6 = r14.get()
                r2 = 0
            L_0x0038:
                int r9 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
                if (r9 == 0) goto L_0x0083
                java.lang.Object r8 = r4.get()
                rx.internal.operators.OperatorReplay$Node r8 = (rx.internal.operators.OperatorReplay.Node) r8
                if (r8 == 0) goto L_0x0083
                java.lang.Object r9 = r8.value
                java.lang.Object r5 = r13.m24732(r9)
                boolean r9 = rx.internal.operators.NotificationLite.m24569(r0, r5)     // Catch:{ Throwable -> 0x0054 }
                if (r9 == 0) goto L_0x0078
                r9 = 0
                r14.index = r9     // Catch:{ Throwable -> 0x0054 }
                goto L_0x000a
            L_0x0054:
                r1 = move-exception
                r14.index = r12
                rx.exceptions.Exceptions.m24529(r1)
                r14.unsubscribe()
                boolean r9 = rx.internal.operators.NotificationLite.m24565(r5)
                if (r9 != 0) goto L_0x000a
                boolean r9 = rx.internal.operators.NotificationLite.m24563(r5)
                if (r9 != 0) goto L_0x000a
                java.lang.Object r9 = rx.internal.operators.NotificationLite.m24564(r5)
                java.lang.Throwable r9 = rx.exceptions.OnErrorThrowable.addValueAsLastCause(r1, r9)
                r0.onError(r9)
                goto L_0x000a
            L_0x0075:
                r9 = move-exception
                monitor-exit(r14)     // Catch:{ all -> 0x0075 }
                throw r9
            L_0x0078:
                r10 = 1
                long r2 = r2 + r10
                r4 = r8
                boolean r9 = r14.isUnsubscribed()
                if (r9 == 0) goto L_0x0038
                goto L_0x000a
            L_0x0083:
                r10 = 0
                int r9 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
                if (r9 == 0) goto L_0x0097
                r14.index = r4
                r10 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r9 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
                if (r9 == 0) goto L_0x0097
                r14.m24739(r2)
            L_0x0097:
                monitor-enter(r14)
                boolean r9 = r14.missed     // Catch:{ all -> 0x00a2 }
                if (r9 != 0) goto L_0x00a5
                r9 = 0
                r14.emitting = r9     // Catch:{ all -> 0x00a2 }
                monitor-exit(r14)     // Catch:{ all -> 0x00a2 }
                goto L_0x000a
            L_0x00a2:
                r9 = move-exception
                monitor-exit(r14)     // Catch:{ all -> 0x00a2 }
                throw r9
            L_0x00a5:
                r9 = 0
                r14.missed = r9     // Catch:{ all -> 0x00a2 }
                monitor-exit(r14)     // Catch:{ all -> 0x00a2 }
                goto L_0x000f
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorReplay.BoundedReplayBuffer.m24737(rx.internal.operators.OperatorReplay$InnerProducer):void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Object m24728(Object value) {
            return value;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public Object m24732(Object value) {
            return value;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m24731() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public void m24727() {
        }
    }

    static final class SizeBoundReplayBuffer<T> extends BoundedReplayBuffer<T> {
        private static final long serialVersionUID = -5898283885385201806L;
        final int limit;

        public SizeBoundReplayBuffer(int limit2) {
            this.limit = limit2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m24760() {
            if (this.size > this.limit) {
                m24734();
            }
        }
    }

    static final class SizeAndTimeBoundReplayBuffer<T> extends BoundedReplayBuffer<T> {
        private static final long serialVersionUID = 3457957419649567404L;
        final int limit;
        final long maxAgeInMillis;
        final Scheduler scheduler;

        public SizeAndTimeBoundReplayBuffer(int limit2, long maxAgeInMillis2, Scheduler scheduler2) {
            this.scheduler = scheduler2;
            this.limit = limit2;
            this.maxAgeInMillis = maxAgeInMillis2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Object m24756(Object value) {
            return new Timestamped(this.scheduler.now(), value);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public Object m24759(Object value) {
            return ((Timestamped) value).m25056();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Node m24757() {
            long timeLimit = this.scheduler.now() - this.maxAgeInMillis;
            Node prev = (Node) get();
            for (Node next = (Node) prev.get(); next != null; next = (Node) next.get()) {
                Object o = next.value;
                Object v = m24759(o);
                if (NotificationLite.m24563(v) || NotificationLite.m24565(v) || ((Timestamped) o).m25057() > timeLimit) {
                    break;
                }
                prev = next;
            }
            return prev;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m24758() {
            long timeLimit = this.scheduler.now() - this.maxAgeInMillis;
            Node prev = (Node) get();
            Node next = (Node) prev.get();
            int e = 0;
            while (next != null) {
                if (this.size <= this.limit) {
                    if (((Timestamped) next.value).m25057() > timeLimit) {
                        break;
                    }
                    e++;
                    this.size--;
                    prev = next;
                    next = (Node) next.get();
                } else {
                    e++;
                    this.size--;
                    prev = next;
                    next = (Node) next.get();
                }
            }
            if (e != 0) {
                m24730(prev);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 连任  reason: contains not printable characters */
        public void m24755() {
            long timeLimit = this.scheduler.now() - this.maxAgeInMillis;
            Node prev = (Node) get();
            Node next = (Node) prev.get();
            int e = 0;
            while (next != null && this.size > 1 && ((Timestamped) next.value).m25057() <= timeLimit) {
                e++;
                this.size--;
                prev = next;
                next = (Node) next.get();
            }
            if (e != 0) {
                m24730(prev);
            }
        }
    }
}
