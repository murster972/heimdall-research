package rx.internal.operators;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Action0;
import rx.internal.schedulers.ImmediateScheduler;
import rx.internal.schedulers.TrampolineScheduler;
import rx.internal.util.RxRingBuffer;
import rx.internal.util.atomic.SpscAtomicArrayQueue;
import rx.internal.util.unsafe.SpscArrayQueue;
import rx.internal.util.unsafe.UnsafeAccess;
import rx.plugins.RxJavaHooks;

public final class OperatorObserveOn<T> implements Observable.Operator<T, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final boolean f19027;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f19028;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Scheduler f19029;

    public OperatorObserveOn(Scheduler scheduler, boolean delayError, int bufferSize) {
        this.f19029 = scheduler;
        this.f19027 = delayError;
        this.f19028 = bufferSize <= 0 ? RxRingBuffer.f19311 : bufferSize;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super T> child) {
        if ((this.f19029 instanceof ImmediateScheduler) || (this.f19029 instanceof TrampolineScheduler)) {
            return child;
        }
        ObserveOnSubscriber<T> parent = new ObserveOnSubscriber<>(this.f19029, child, this.f19027, this.f19028);
        parent.m24699();
        return parent;
    }

    static final class ObserveOnSubscriber<T> extends Subscriber<T> implements Action0 {

        /* renamed from: ʻ  reason: contains not printable characters */
        volatile boolean f19030;

        /* renamed from: ʼ  reason: contains not printable characters */
        final AtomicLong f19031 = new AtomicLong();

        /* renamed from: ʽ  reason: contains not printable characters */
        final AtomicLong f19032 = new AtomicLong();

        /* renamed from: ˑ  reason: contains not printable characters */
        Throwable f19033;

        /* renamed from: ٴ  reason: contains not printable characters */
        long f19034;

        /* renamed from: 连任  reason: contains not printable characters */
        final int f19035;

        /* renamed from: 靐  reason: contains not printable characters */
        final Scheduler.Worker f19036;

        /* renamed from: 麤  reason: contains not printable characters */
        final Queue<Object> f19037;

        /* renamed from: 齉  reason: contains not printable characters */
        final boolean f19038;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f19039;

        public ObserveOnSubscriber(Scheduler scheduler, Subscriber<? super T> child, boolean delayError, int bufferSize) {
            this.f19039 = child;
            this.f19036 = scheduler.createWorker();
            this.f19038 = delayError;
            int calculatedSize = bufferSize > 0 ? bufferSize : RxRingBuffer.f19311;
            this.f19035 = calculatedSize - (calculatedSize >> 2);
            if (UnsafeAccess.m24998()) {
                this.f19037 = new SpscArrayQueue(calculatedSize);
            } else {
                this.f19037 = new SpscAtomicArrayQueue(calculatedSize);
            }
            m24511((long) calculatedSize);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m24699() {
            Subscriber<? super T> localChild = this.f19039;
            localChild.m24512((Producer) new Producer() {
                public void request(long n) {
                    if (n > 0) {
                        BackpressureUtils.m24552(ObserveOnSubscriber.this.f19031, n);
                        ObserveOnSubscriber.this.m24698();
                    }
                }
            });
            localChild.m24513((Subscription) this.f19036);
            localChild.m24513((Subscription) this);
        }

        public void onNext(T t) {
            if (!isUnsubscribed() && !this.f19030) {
                if (!this.f19037.offer(NotificationLite.m24567(t))) {
                    onError(new MissingBackpressureException());
                } else {
                    m24698();
                }
            }
        }

        public void onCompleted() {
            if (!isUnsubscribed() && !this.f19030) {
                this.f19030 = true;
                m24698();
            }
        }

        public void onError(Throwable e) {
            if (isUnsubscribed() || this.f19030) {
                RxJavaHooks.m25024(e);
                return;
            }
            this.f19033 = e;
            this.f19030 = true;
            m24698();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m24698() {
            if (this.f19032.getAndIncrement() == 0) {
                this.f19036.m24503(this);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24700() {
            long missed = 1;
            long currentEmission = this.f19034;
            Queue<Object> q = this.f19037;
            Subscriber<? super T> localChild = this.f19039;
            do {
                long requestAmount = this.f19031.get();
                while (requestAmount != currentEmission) {
                    boolean done = this.f19030;
                    Object v = q.poll();
                    boolean empty = v == null;
                    if (!m24701(done, empty, localChild, q)) {
                        if (empty) {
                            break;
                        }
                        localChild.onNext(NotificationLite.m24564(v));
                        currentEmission++;
                        if (currentEmission == ((long) this.f19035)) {
                            requestAmount = BackpressureUtils.m24549(this.f19031, currentEmission);
                            m24511(currentEmission);
                            currentEmission = 0;
                        }
                    } else {
                        return;
                    }
                }
                if (requestAmount != currentEmission || !m24701(this.f19030, q.isEmpty(), localChild, q)) {
                    this.f19034 = currentEmission;
                    missed = this.f19032.addAndGet(-missed);
                } else {
                    return;
                }
            } while (missed != 0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24701(boolean done, boolean isEmpty, Subscriber<? super T> a, Queue<Object> q) {
            if (a.isUnsubscribed()) {
                q.clear();
                return true;
            }
            if (done) {
                if (!this.f19038) {
                    Throwable e = this.f19033;
                    if (e != null) {
                        q.clear();
                        try {
                            a.onError(e);
                            return true;
                        } finally {
                            this.f19036.unsubscribe();
                        }
                    } else if (isEmpty) {
                        try {
                            a.onCompleted();
                            return true;
                        } finally {
                            this.f19036.unsubscribe();
                        }
                    }
                } else if (isEmpty) {
                    Throwable e2 = this.f19033;
                    if (e2 != null) {
                        try {
                            a.onError(e2);
                        } catch (Throwable th) {
                            this.f19036.unsubscribe();
                            throw th;
                        }
                    } else {
                        a.onCompleted();
                    }
                    this.f19036.unsubscribe();
                }
            }
            return false;
        }
    }
}
