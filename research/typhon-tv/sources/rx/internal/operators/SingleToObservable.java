package rx.internal.operators;

import rx.Observable;
import rx.Single;
import rx.Subscriber;
import rx.Subscription;
import rx.internal.operators.SingleLiftObservableOperator;

public final class SingleToObservable<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final Single.OnSubscribe<T> f19152;

    public SingleToObservable(Single.OnSubscribe<T> source) {
        this.f19152 = source;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> t) {
        SingleLiftObservableOperator.WrapSubscriberIntoSingle<T> parent = new SingleLiftObservableOperator.WrapSubscriberIntoSingle<>(t);
        t.m24513((Subscription) parent);
        this.f19152.call(parent);
    }
}
