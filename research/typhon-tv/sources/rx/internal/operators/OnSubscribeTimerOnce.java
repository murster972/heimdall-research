package rx.internal.operators;

import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Action0;

public final class OnSubscribeTimerOnce implements Observable.OnSubscribe<Long> {

    /* renamed from: 靐  reason: contains not printable characters */
    final TimeUnit f18921;

    /* renamed from: 齉  reason: contains not printable characters */
    final Scheduler f18922;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f18923;

    public OnSubscribeTimerOnce(long time, TimeUnit unit, Scheduler scheduler) {
        this.f18923 = time;
        this.f18921 = unit;
        this.f18922 = scheduler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(final Subscriber<? super Long> child) {
        Scheduler.Worker worker = this.f18922.createWorker();
        child.m24513((Subscription) worker);
        worker.m24505(new Action0() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m24643() {
                try {
                    child.onNext(0L);
                    child.onCompleted();
                } catch (Throwable t) {
                    Exceptions.m24533(t, (Observer<?>) child);
                }
            }
        }, this.f18923, this.f18921);
    }
}
