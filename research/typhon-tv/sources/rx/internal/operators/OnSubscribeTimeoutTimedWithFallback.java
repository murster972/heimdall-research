package rx.internal.operators;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.internal.producers.ProducerArbiter;
import rx.internal.subscriptions.SequentialSubscription;
import rx.plugins.RxJavaHooks;

public final class OnSubscribeTimeoutTimedWithFallback<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 连任  reason: contains not printable characters */
    final Observable<? extends T> f18902;

    /* renamed from: 靐  reason: contains not printable characters */
    final long f18903;

    /* renamed from: 麤  reason: contains not printable characters */
    final Scheduler f18904;

    /* renamed from: 齉  reason: contains not printable characters */
    final TimeUnit f18905;

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable<T> f18906;

    public OnSubscribeTimeoutTimedWithFallback(Observable<T> source, long timeout, TimeUnit unit, Scheduler scheduler, Observable<? extends T> fallback) {
        this.f18906 = source;
        this.f18903 = timeout;
        this.f18905 = unit;
        this.f18904 = scheduler;
        this.f18902 = fallback;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> t) {
        TimeoutMainSubscriber<T> parent = new TimeoutMainSubscriber<>(t, this.f18903, this.f18905, this.f18904.createWorker(), this.f18902);
        t.m24513((Subscription) parent.f18912);
        t.m24512((Producer) parent.f18909);
        parent.m24638(0);
        this.f18906.m7386(parent);
    }

    static final class TimeoutMainSubscriber<T> extends Subscriber<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final ProducerArbiter f18909 = new ProducerArbiter();

        /* renamed from: ʼ  reason: contains not printable characters */
        final AtomicLong f18910 = new AtomicLong();

        /* renamed from: ʽ  reason: contains not printable characters */
        final SequentialSubscription f18911 = new SequentialSubscription();

        /* renamed from: ˑ  reason: contains not printable characters */
        final SequentialSubscription f18912 = new SequentialSubscription(this);

        /* renamed from: ٴ  reason: contains not printable characters */
        long f18913;

        /* renamed from: 连任  reason: contains not printable characters */
        final Observable<? extends T> f18914;

        /* renamed from: 靐  reason: contains not printable characters */
        final long f18915;

        /* renamed from: 麤  reason: contains not printable characters */
        final Scheduler.Worker f18916;

        /* renamed from: 齉  reason: contains not printable characters */
        final TimeUnit f18917;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f18918;

        TimeoutMainSubscriber(Subscriber<? super T> actual, long timeout, TimeUnit unit, Scheduler.Worker worker, Observable<? extends T> fallback) {
            this.f18918 = actual;
            this.f18915 = timeout;
            this.f18917 = unit;
            this.f18916 = worker;
            this.f18914 = fallback;
            m24513((Subscription) worker);
            m24513((Subscription) this.f18911);
        }

        public void onNext(T t) {
            long idx = this.f18910.get();
            if (idx != Long.MAX_VALUE && this.f18910.compareAndSet(idx, idx + 1)) {
                Subscription s = (Subscription) this.f18911.get();
                if (s != null) {
                    s.unsubscribe();
                }
                this.f18913++;
                this.f18918.onNext(t);
                m24638(idx + 1);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24638(long nextIdx) {
            this.f18911.replace(this.f18916.m24505(new TimeoutTask(nextIdx), this.f18915, this.f18917));
        }

        public void onError(Throwable e) {
            if (this.f18910.getAndSet(Long.MAX_VALUE) != Long.MAX_VALUE) {
                this.f18911.unsubscribe();
                this.f18918.onError(e);
                this.f18916.unsubscribe();
                return;
            }
            RxJavaHooks.m25024(e);
        }

        public void onCompleted() {
            if (this.f18910.getAndSet(Long.MAX_VALUE) != Long.MAX_VALUE) {
                this.f18911.unsubscribe();
                this.f18918.onCompleted();
                this.f18916.unsubscribe();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24640(Producer p) {
            this.f18909.m24796(p);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m24639(long idx) {
            if (this.f18910.compareAndSet(idx, Long.MAX_VALUE)) {
                unsubscribe();
                if (this.f18914 == null) {
                    this.f18918.onError(new TimeoutException());
                    return;
                }
                long c = this.f18913;
                if (c != 0) {
                    this.f18909.m24795(c);
                }
                FallbackSubscriber<T> fallbackSubscriber = new FallbackSubscriber<>(this.f18918, this.f18909);
                if (this.f18912.replace(fallbackSubscriber)) {
                    this.f18914.m7386(fallbackSubscriber);
                }
            }
        }

        final class TimeoutTask implements Action0 {

            /* renamed from: 龘  reason: contains not printable characters */
            final long f18920;

            TimeoutTask(long idx) {
                this.f18920 = idx;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24641() {
                TimeoutMainSubscriber.this.m24639(this.f18920);
            }
        }
    }

    static final class FallbackSubscriber<T> extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final ProducerArbiter f18907;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f18908;

        FallbackSubscriber(Subscriber<? super T> actual, ProducerArbiter arbiter) {
            this.f18908 = actual;
            this.f18907 = arbiter;
        }

        public void onNext(T t) {
            this.f18908.onNext(t);
        }

        public void onError(Throwable e) {
            this.f18908.onError(e);
        }

        public void onCompleted() {
            this.f18908.onCompleted();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24637(Producer p) {
            this.f18907.m24796(p);
        }
    }
}
