package rx.internal.operators;

import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Func1;
import rx.internal.producers.SingleDelayedProducer;
import rx.plugins.RxJavaHooks;

public final class OperatorAny<T> implements Observable.Operator<Boolean, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final boolean f18934;

    /* renamed from: 龘  reason: contains not printable characters */
    final Func1<? super T, Boolean> f18935;

    public OperatorAny(Func1<? super T, Boolean> predicate, boolean returnOnEmpty) {
        this.f18935 = predicate;
        this.f18934 = returnOnEmpty;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(final Subscriber<? super Boolean> child) {
        final SingleDelayedProducer<Boolean> producer = new SingleDelayedProducer<>(child);
        Subscriber<T> s = new Subscriber<T>() {

            /* renamed from: 靐  reason: contains not printable characters */
            boolean f18937;

            /* renamed from: 龘  reason: contains not printable characters */
            boolean f18940;

            public void onNext(T t) {
                if (!this.f18937) {
                    this.f18940 = true;
                    try {
                        if (OperatorAny.this.f18935.call(t).booleanValue()) {
                            this.f18937 = true;
                            producer.setValue(Boolean.valueOf(!OperatorAny.this.f18934));
                            unsubscribe();
                        }
                    } catch (Throwable e) {
                        Exceptions.m24534(e, this, t);
                    }
                }
            }

            public void onError(Throwable e) {
                if (!this.f18937) {
                    this.f18937 = true;
                    child.onError(e);
                    return;
                }
                RxJavaHooks.m25024(e);
            }

            public void onCompleted() {
                if (!this.f18937) {
                    this.f18937 = true;
                    if (this.f18940) {
                        producer.setValue(false);
                    } else {
                        producer.setValue(Boolean.valueOf(OperatorAny.this.f18934));
                    }
                }
            }
        };
        child.m24513((Subscription) s);
        child.m24512((Producer) producer);
        return s;
    }
}
