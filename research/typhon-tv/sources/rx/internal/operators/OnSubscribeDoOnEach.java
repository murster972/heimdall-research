package rx.internal.operators;

import java.util.Arrays;
import java.util.Collection;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.exceptions.CompositeException;
import rx.exceptions.Exceptions;
import rx.plugins.RxJavaHooks;

public class OnSubscribeDoOnEach<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Observable<T> f18825;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Observer<? super T> f18826;

    public OnSubscribeDoOnEach(Observable<T> source, Observer<? super T> doOnEachObserver) {
        this.f18825 = source;
        this.f18826 = doOnEachObserver;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> subscriber) {
        this.f18825.m7416(new DoOnEachSubscriber(subscriber, this.f18826));
    }

    private static final class DoOnEachSubscriber<T> extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Observer<? super T> f18827;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f18828;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Subscriber<? super T> f18829;

        DoOnEachSubscriber(Subscriber<? super T> subscriber, Observer<? super T> doOnEachObserver) {
            super(subscriber);
            this.f18829 = subscriber;
            this.f18827 = doOnEachObserver;
        }

        public void onCompleted() {
            if (!this.f18828) {
                try {
                    this.f18827.onCompleted();
                    this.f18828 = true;
                    this.f18829.onCompleted();
                } catch (Throwable e) {
                    Exceptions.m24533(e, (Observer<?>) this);
                }
            }
        }

        public void onError(Throwable e) {
            if (this.f18828) {
                RxJavaHooks.m25024(e);
                return;
            }
            this.f18828 = true;
            try {
                this.f18827.onError(e);
                this.f18829.onError(e);
            } catch (Throwable e2) {
                Exceptions.m24529(e2);
                this.f18829.onError(new CompositeException((Collection<? extends Throwable>) Arrays.asList(new Throwable[]{e, e2})));
            }
        }

        public void onNext(T value) {
            if (!this.f18828) {
                try {
                    this.f18827.onNext(value);
                    this.f18829.onNext(value);
                } catch (Throwable e) {
                    Exceptions.m24534(e, this, value);
                }
            }
        }
    }
}
