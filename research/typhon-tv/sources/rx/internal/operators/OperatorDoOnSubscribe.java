package rx.internal.operators;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.observers.Subscribers;

public class OperatorDoOnSubscribe<T> implements Observable.Operator<T, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Action0 f18998;

    public OperatorDoOnSubscribe(Action0 subscribe) {
        this.f18998 = subscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super T> child) {
        this.f18998.m24539();
        return Subscribers.m25005(child);
    }
}
