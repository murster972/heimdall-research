package rx.internal.operators;

import java.io.Serializable;
import rx.Observer;

public final class NotificationLite {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f18799 = new Serializable() {
        private static final long serialVersionUID = 2;

        public String toString() {
            return "Notification=>NULL";
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Object f18800 = new Serializable() {
        private static final long serialVersionUID = 1;

        public String toString() {
            return "Notification=>Completed";
        }
    };

    static final class OnErrorSentinel implements Serializable {
        private static final long serialVersionUID = 3;
        final Throwable e;

        public OnErrorSentinel(Throwable e2) {
            this.e = e2;
        }

        public String toString() {
            return "Notification=>Error:" + this.e;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Object m24567(T t) {
        if (t == null) {
            return f18799;
        }
        return t;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m24566() {
        return f18800;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Object m24568(Throwable e) {
        return new OnErrorSentinel(e);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> boolean m24569(Observer<? super T> o, Object n) {
        if (n == f18800) {
            o.onCompleted();
            return true;
        } else if (n == f18799) {
            o.onNext(null);
            return false;
        } else if (n == null) {
            throw new IllegalArgumentException("The lite notification can not be null");
        } else if (n.getClass() == OnErrorSentinel.class) {
            o.onError(((OnErrorSentinel) n).e);
            return true;
        } else {
            o.onNext(n);
            return false;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m24563(Object n) {
        return n == f18800;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m24565(Object n) {
        return n instanceof OnErrorSentinel;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static <T> T m24564(Object n) {
        if (n == f18799) {
            return null;
        }
        return n;
    }
}
