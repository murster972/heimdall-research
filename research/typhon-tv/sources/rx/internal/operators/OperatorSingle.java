package rx.internal.operators;

import java.util.NoSuchElementException;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.internal.producers.SingleProducer;
import rx.plugins.RxJavaHooks;

public final class OperatorSingle<T> implements Observable.Operator<T, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final T f19096;

    /* renamed from: 龘  reason: contains not printable characters */
    private final boolean f19097;

    static final class Holder {

        /* renamed from: 龘  reason: contains not printable characters */
        static final OperatorSingle<?> f19098 = new OperatorSingle<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> OperatorSingle<T> m24769() {
        return Holder.f19098;
    }

    OperatorSingle() {
        this(false, (Object) null);
    }

    private OperatorSingle(boolean hasDefaultValue, T defaultValue) {
        this.f19097 = hasDefaultValue;
        this.f19096 = defaultValue;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super T> child) {
        ParentSubscriber<T> parent = new ParentSubscriber<>(child, this.f19097, this.f19096);
        child.m24513((Subscription) parent);
        return parent;
    }

    static final class ParentSubscriber<T> extends Subscriber<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f19099;

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f19100;

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f19101;

        /* renamed from: 麤  reason: contains not printable characters */
        private T f19102;

        /* renamed from: 齉  reason: contains not printable characters */
        private final T f19103;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Subscriber<? super T> f19104;

        ParentSubscriber(Subscriber<? super T> child, boolean hasDefaultValue, T defaultValue) {
            this.f19104 = child;
            this.f19101 = hasDefaultValue;
            this.f19103 = defaultValue;
            m24511(2);
        }

        public void onNext(T value) {
            if (this.f19099) {
                return;
            }
            if (this.f19100) {
                this.f19099 = true;
                this.f19104.onError(new IllegalArgumentException("Sequence contains too many elements"));
                unsubscribe();
                return;
            }
            this.f19102 = value;
            this.f19100 = true;
        }

        public void onCompleted() {
            if (this.f19099) {
                return;
            }
            if (this.f19100) {
                this.f19104.m24512((Producer) new SingleProducer(this.f19104, this.f19102));
            } else if (this.f19101) {
                this.f19104.m24512((Producer) new SingleProducer(this.f19104, this.f19103));
            } else {
                this.f19104.onError(new NoSuchElementException("Sequence contains no elements"));
            }
        }

        public void onError(Throwable e) {
            if (this.f19099) {
                RxJavaHooks.m25024(e);
            } else {
                this.f19104.onError(e);
            }
        }
    }
}
