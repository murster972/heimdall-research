package rx.internal.operators;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.observers.SerializedSubscriber;

public final class OperatorSampleWithTime<T> implements Observable.Operator<T, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final TimeUnit f19090;

    /* renamed from: 齉  reason: contains not printable characters */
    final Scheduler f19091;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f19092;

    public OperatorSampleWithTime(long time, TimeUnit unit, Scheduler scheduler) {
        this.f19092 = time;
        this.f19090 = unit;
        this.f19091 = scheduler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super T> child) {
        SerializedSubscriber<T> s = new SerializedSubscriber<>(child);
        Scheduler.Worker worker = this.f19091.createWorker();
        child.m24513((Subscription) worker);
        SamplerSubscriber<T> sampler = new SamplerSubscriber<>(s);
        child.m24513((Subscription) sampler);
        worker.m24504(sampler, this.f19092, this.f19092, this.f19090);
        return sampler;
    }

    static final class SamplerSubscriber<T> extends Subscriber<T> implements Action0 {

        /* renamed from: 齉  reason: contains not printable characters */
        private static final Object f19093 = new Object();

        /* renamed from: 靐  reason: contains not printable characters */
        private final Subscriber<? super T> f19094;

        /* renamed from: 龘  reason: contains not printable characters */
        final AtomicReference<Object> f19095 = new AtomicReference<>(f19093);

        public SamplerSubscriber(Subscriber<? super T> subscriber) {
            this.f19094 = subscriber;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24767() {
            m24511(Long.MAX_VALUE);
        }

        public void onNext(T t) {
            this.f19095.set(t);
        }

        public void onError(Throwable e) {
            this.f19094.onError(e);
            unsubscribe();
        }

        public void onCompleted() {
            m24766();
            this.f19094.onCompleted();
            unsubscribe();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24768() {
            m24766();
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private void m24766() {
            T localValue = this.f19095.getAndSet(f19093);
            if (localValue != f19093) {
                try {
                    this.f19094.onNext(localValue);
                } catch (Throwable e) {
                    Exceptions.m24533(e, (Observer<?>) this);
                }
            }
        }
    }
}
