package rx.internal.operators;

import rx.Observable;
import rx.Subscriber;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.plugins.RxJavaHooks;

public final class OperatorDoAfterTerminate<T> implements Observable.Operator<T, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final Action0 f18995;

    public OperatorDoAfterTerminate(Action0 action) {
        if (action == null) {
            throw new NullPointerException("Action can not be null");
        }
        this.f18995 = action;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(final Subscriber<? super T> child) {
        return new Subscriber<T>(child) {
            public void onNext(T t) {
                child.onNext(t);
            }

            public void onError(Throwable e) {
                try {
                    child.onError(e);
                } finally {
                    m24673();
                }
            }

            public void onCompleted() {
                try {
                    child.onCompleted();
                } finally {
                    m24673();
                }
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public void m24673() {
                try {
                    OperatorDoAfterTerminate.this.f18995.m24539();
                } catch (Throwable ex) {
                    Exceptions.m24529(ex);
                    RxJavaHooks.m25024(ex);
                }
            }
        };
    }
}
