package rx.internal.operators;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.MissingBackpressureException;

public final class OperatorBufferWithSize<T> implements Observable.Operator<List<T>, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final int f18941;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f18942;

    public OperatorBufferWithSize(int count, int skip) {
        if (count <= 0) {
            throw new IllegalArgumentException("count must be greater than 0");
        } else if (skip <= 0) {
            throw new IllegalArgumentException("skip must be greater than 0");
        } else {
            this.f18942 = count;
            this.f18941 = skip;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super List<T>> child) {
        if (this.f18941 == this.f18942) {
            BufferExact<T> parent = new BufferExact<>(child, this.f18942);
            child.m24513((Subscription) parent);
            child.m24512(parent.m24649());
            return parent;
        } else if (this.f18941 > this.f18942) {
            BufferSkip<T> parent2 = new BufferSkip<>(child, this.f18942, this.f18941);
            child.m24513((Subscription) parent2);
            child.m24512(parent2.m24655());
            return parent2;
        } else {
            BufferOverlap<T> parent3 = new BufferOverlap<>(child, this.f18942, this.f18941);
            child.m24513((Subscription) parent3);
            child.m24512(parent3.m24652());
            return parent3;
        }
    }

    static final class BufferExact<T> extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final int f18943;

        /* renamed from: 齉  reason: contains not printable characters */
        List<T> f18944;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super List<T>> f18945;

        public BufferExact(Subscriber<? super List<T>> actual, int count) {
            this.f18945 = actual;
            this.f18943 = count;
            m24511(0);
        }

        public void onNext(T t) {
            List<T> b = this.f18944;
            if (b == null) {
                b = new ArrayList<>(this.f18943);
                this.f18944 = b;
            }
            b.add(t);
            if (b.size() == this.f18943) {
                this.f18944 = null;
                this.f18945.onNext(b);
            }
        }

        public void onError(Throwable e) {
            this.f18944 = null;
            this.f18945.onError(e);
        }

        public void onCompleted() {
            List<T> b = this.f18944;
            if (b != null) {
                this.f18945.onNext(b);
            }
            this.f18945.onCompleted();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Producer m24649() {
            return new Producer() {
                public void request(long n) {
                    if (n < 0) {
                        throw new IllegalArgumentException("n >= required but it was " + n);
                    } else if (n != 0) {
                        BufferExact.this.m24511(BackpressureUtils.m24551(n, (long) BufferExact.this.f18943));
                    }
                }
            };
        }
    }

    static final class BufferSkip<T> extends Subscriber<T> {

        /* renamed from: 连任  reason: contains not printable characters */
        List<T> f18954;

        /* renamed from: 靐  reason: contains not printable characters */
        final int f18955;

        /* renamed from: 麤  reason: contains not printable characters */
        long f18956;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f18957;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super List<T>> f18958;

        public BufferSkip(Subscriber<? super List<T>> actual, int count, int skip) {
            this.f18958 = actual;
            this.f18955 = count;
            this.f18957 = skip;
            m24511(0);
        }

        public void onNext(T t) {
            long i = this.f18956;
            List<T> b = this.f18954;
            if (i == 0) {
                b = new ArrayList<>(this.f18955);
                this.f18954 = b;
            }
            long i2 = i + 1;
            if (i2 == ((long) this.f18957)) {
                this.f18956 = 0;
            } else {
                this.f18956 = i2;
            }
            if (b != null) {
                b.add(t);
                if (b.size() == this.f18955) {
                    this.f18954 = null;
                    this.f18958.onNext(b);
                }
            }
        }

        public void onError(Throwable e) {
            this.f18954 = null;
            this.f18958.onError(e);
        }

        public void onCompleted() {
            List<T> b = this.f18954;
            if (b != null) {
                this.f18954 = null;
                this.f18958.onNext(b);
            }
            this.f18958.onCompleted();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Producer m24655() {
            return new BufferSkipProducer();
        }

        final class BufferSkipProducer extends AtomicBoolean implements Producer {
            private static final long serialVersionUID = 3428177408082367154L;

            BufferSkipProducer() {
            }

            public void request(long n) {
                if (n < 0) {
                    throw new IllegalArgumentException("n >= 0 required but it was " + n);
                } else if (n != 0) {
                    BufferSkip<T> parent = BufferSkip.this;
                    if (get() || !compareAndSet(false, true)) {
                        parent.m24511(BackpressureUtils.m24551(n, (long) parent.f18957));
                    } else {
                        parent.m24511(BackpressureUtils.m24548(BackpressureUtils.m24551(n, (long) parent.f18955), BackpressureUtils.m24551((long) (parent.f18957 - parent.f18955), n - 1)));
                    }
                }
            }
        }
    }

    static final class BufferOverlap<T> extends Subscriber<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final AtomicLong f18947 = new AtomicLong();

        /* renamed from: ʼ  reason: contains not printable characters */
        long f18948;

        /* renamed from: 连任  reason: contains not printable characters */
        final ArrayDeque<List<T>> f18949 = new ArrayDeque<>();

        /* renamed from: 靐  reason: contains not printable characters */
        final int f18950;

        /* renamed from: 麤  reason: contains not printable characters */
        long f18951;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f18952;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super List<T>> f18953;

        public BufferOverlap(Subscriber<? super List<T>> actual, int count, int skip) {
            this.f18953 = actual;
            this.f18950 = count;
            this.f18952 = skip;
            m24511(0);
        }

        public void onNext(T t) {
            long i = this.f18951;
            if (i == 0) {
                this.f18949.offer(new ArrayList<>(this.f18950));
            }
            long i2 = i + 1;
            if (i2 == ((long) this.f18952)) {
                this.f18951 = 0;
            } else {
                this.f18951 = i2;
            }
            Iterator<List<T>> it2 = this.f18949.iterator();
            while (it2.hasNext()) {
                it2.next().add(t);
            }
            List<T> b = this.f18949.peek();
            if (b != null && b.size() == this.f18950) {
                this.f18949.poll();
                this.f18948++;
                this.f18953.onNext(b);
            }
        }

        public void onError(Throwable e) {
            this.f18949.clear();
            this.f18953.onError(e);
        }

        public void onCompleted() {
            long p = this.f18948;
            if (p != 0) {
                if (p > this.f18947.get()) {
                    this.f18953.onError(new MissingBackpressureException("More produced than requested? " + p));
                    return;
                }
                this.f18947.addAndGet(-p);
            }
            BackpressureUtils.m24553(this.f18947, this.f18949, this.f18953);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Producer m24652() {
            return new BufferOverlapProducer();
        }

        final class BufferOverlapProducer extends AtomicBoolean implements Producer {
            private static final long serialVersionUID = -4015894850868853147L;

            BufferOverlapProducer() {
            }

            public void request(long n) {
                BufferOverlap<T> parent = BufferOverlap.this;
                if (BackpressureUtils.m24556(parent.f18947, n, parent.f18949, parent.f18953) && n != 0) {
                    if (get() || !compareAndSet(false, true)) {
                        parent.m24511(BackpressureUtils.m24551((long) parent.f18952, n));
                    } else {
                        parent.m24511(BackpressureUtils.m24548(BackpressureUtils.m24551((long) parent.f18952, n - 1), (long) parent.f18950));
                    }
                }
            }
        }
    }
}
