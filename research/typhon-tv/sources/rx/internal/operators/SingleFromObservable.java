package rx.internal.operators;

import java.util.NoSuchElementException;
import rx.Observable;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.Subscription;
import rx.plugins.RxJavaHooks;

public final class SingleFromObservable<T> implements Single.OnSubscribe<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable.OnSubscribe<T> f19145;

    public SingleFromObservable(Observable.OnSubscribe<T> source) {
        this.f19145 = source;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(SingleSubscriber<? super T> t) {
        WrapSingleIntoSubscriber<T> parent = new WrapSingleIntoSubscriber<>(t);
        t.m24508((Subscription) parent);
        this.f19145.call(parent);
    }

    static final class WrapSingleIntoSubscriber<T> extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        T f19146;

        /* renamed from: 齉  reason: contains not printable characters */
        int f19147;

        /* renamed from: 龘  reason: contains not printable characters */
        final SingleSubscriber<? super T> f19148;

        WrapSingleIntoSubscriber(SingleSubscriber<? super T> actual) {
            this.f19148 = actual;
        }

        public void onNext(T t) {
            int s = this.f19147;
            if (s == 0) {
                this.f19147 = 1;
                this.f19146 = t;
            } else if (s == 1) {
                this.f19147 = 2;
                this.f19148.m24507((Throwable) new IndexOutOfBoundsException("The upstream produced more than one value"));
            }
        }

        public void onError(Throwable e) {
            if (this.f19147 == 2) {
                RxJavaHooks.m25024(e);
                return;
            }
            this.f19146 = null;
            this.f19148.m24507(e);
        }

        public void onCompleted() {
            int s = this.f19147;
            if (s == 0) {
                this.f19148.m24507((Throwable) new NoSuchElementException());
            } else if (s == 1) {
                this.f19147 = 2;
                T v = this.f19146;
                this.f19146 = null;
                this.f19148.m24506(v);
            }
        }
    }
}
