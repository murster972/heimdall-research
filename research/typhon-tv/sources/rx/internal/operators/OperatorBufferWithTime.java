package rx.internal.operators;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.observers.SerializedSubscriber;

public final class OperatorBufferWithTime<T> implements Observable.Operator<List<T>, T> {

    /* renamed from: 连任  reason: contains not printable characters */
    final Scheduler f18959;

    /* renamed from: 靐  reason: contains not printable characters */
    final long f18960;

    /* renamed from: 麤  reason: contains not printable characters */
    final int f18961;

    /* renamed from: 齉  reason: contains not printable characters */
    final TimeUnit f18962;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f18963;

    public OperatorBufferWithTime(long timespan, long timeshift, TimeUnit unit, int count, Scheduler scheduler) {
        this.f18963 = timespan;
        this.f18960 = timeshift;
        this.f18962 = unit;
        this.f18961 = count;
        this.f18959 = scheduler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(Subscriber<? super List<T>> child) {
        Scheduler.Worker inner = this.f18959.createWorker();
        SerializedSubscriber<List<T>> serialized = new SerializedSubscriber<>(child);
        if (this.f18963 == this.f18960) {
            OperatorBufferWithTime<T>.ExactSubscriber parent = new ExactSubscriber(serialized, inner);
            parent.m24513((Subscription) inner);
            child.m24513((Subscription) parent);
            parent.m24658();
            return parent;
        }
        OperatorBufferWithTime<T>.InexactSubscriber parent2 = new InexactSubscriber(serialized, inner);
        parent2.m24513((Subscription) inner);
        child.m24513((Subscription) parent2);
        parent2.m24660();
        parent2.m24661();
        return parent2;
    }

    final class InexactSubscriber extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Scheduler.Worker f18971;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f18972;

        /* renamed from: 齉  reason: contains not printable characters */
        final List<List<T>> f18973 = new LinkedList();

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super List<T>> f18974;

        public InexactSubscriber(Subscriber<? super List<T>> child, Scheduler.Worker inner) {
            this.f18974 = child;
            this.f18971 = inner;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0038, code lost:
            if (r3 == null) goto L_0x0053;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x003a, code lost:
            r4 = r3.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
            if (r4.hasNext() == false) goto L_0x0053;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
            r6.f18974.onNext((java.util.List) r4.next());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0053, code lost:
            r2 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onNext(T r7) {
            /*
                r6 = this;
                r2 = 0
                monitor-enter(r6)
                boolean r4 = r6.f18972     // Catch:{ all -> 0x0050 }
                if (r4 == 0) goto L_0x0008
                monitor-exit(r6)     // Catch:{ all -> 0x0050 }
            L_0x0007:
                return
            L_0x0008:
                java.util.List<java.util.List<T>> r4 = r6.f18973     // Catch:{ all -> 0x0050 }
                java.util.Iterator r1 = r4.iterator()     // Catch:{ all -> 0x0050 }
                r3 = r2
            L_0x000f:
                boolean r4 = r1.hasNext()     // Catch:{ all -> 0x0055 }
                if (r4 == 0) goto L_0x0037
                java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0055 }
                java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x0055 }
                r0.add(r7)     // Catch:{ all -> 0x0055 }
                int r4 = r0.size()     // Catch:{ all -> 0x0055 }
                rx.internal.operators.OperatorBufferWithTime r5 = rx.internal.operators.OperatorBufferWithTime.this     // Catch:{ all -> 0x0055 }
                int r5 = r5.f18961     // Catch:{ all -> 0x0055 }
                if (r4 != r5) goto L_0x005a
                r1.remove()     // Catch:{ all -> 0x0055 }
                if (r3 != 0) goto L_0x0058
                java.util.LinkedList r2 = new java.util.LinkedList     // Catch:{ all -> 0x0055 }
                r2.<init>()     // Catch:{ all -> 0x0055 }
            L_0x0032:
                r2.add(r0)     // Catch:{ all -> 0x0050 }
            L_0x0035:
                r3 = r2
                goto L_0x000f
            L_0x0037:
                monitor-exit(r6)     // Catch:{ all -> 0x0055 }
                if (r3 == 0) goto L_0x0053
                java.util.Iterator r4 = r3.iterator()
            L_0x003e:
                boolean r5 = r4.hasNext()
                if (r5 == 0) goto L_0x0053
                java.lang.Object r0 = r4.next()
                java.util.List r0 = (java.util.List) r0
                rx.Subscriber<? super java.util.List<T>> r5 = r6.f18974
                r5.onNext(r0)
                goto L_0x003e
            L_0x0050:
                r4 = move-exception
            L_0x0051:
                monitor-exit(r6)     // Catch:{ all -> 0x0050 }
                throw r4
            L_0x0053:
                r2 = r3
                goto L_0x0007
            L_0x0055:
                r4 = move-exception
                r2 = r3
                goto L_0x0051
            L_0x0058:
                r2 = r3
                goto L_0x0032
            L_0x005a:
                r2 = r3
                goto L_0x0035
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorBufferWithTime.InexactSubscriber.onNext(java.lang.Object):void");
        }

        public void onError(Throwable e) {
            synchronized (this) {
                if (!this.f18972) {
                    this.f18972 = true;
                    this.f18973.clear();
                    this.f18974.onError(e);
                    unsubscribe();
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
            if (r3.hasNext() == false) goto L_0x0037;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
            r5.f18974.onNext(r3.next());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0037, code lost:
            r5.f18974.onCompleted();
            unsubscribe();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
            r3 = r1.iterator();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onCompleted() {
            /*
                r5 = this;
                monitor-enter(r5)     // Catch:{ Throwable -> 0x002d }
                boolean r3 = r5.f18972     // Catch:{ all -> 0x0034 }
                if (r3 == 0) goto L_0x0007
                monitor-exit(r5)     // Catch:{ all -> 0x0034 }
            L_0x0006:
                return
            L_0x0007:
                r3 = 1
                r5.f18972 = r3     // Catch:{ all -> 0x0034 }
                java.util.LinkedList r1 = new java.util.LinkedList     // Catch:{ all -> 0x0034 }
                java.util.List<java.util.List<T>> r3 = r5.f18973     // Catch:{ all -> 0x0034 }
                r1.<init>(r3)     // Catch:{ all -> 0x0034 }
                java.util.List<java.util.List<T>> r3 = r5.f18973     // Catch:{ all -> 0x0034 }
                r3.clear()     // Catch:{ all -> 0x0034 }
                monitor-exit(r5)     // Catch:{ all -> 0x0034 }
                java.util.Iterator r3 = r1.iterator()     // Catch:{ Throwable -> 0x002d }
            L_0x001b:
                boolean r4 = r3.hasNext()     // Catch:{ Throwable -> 0x002d }
                if (r4 == 0) goto L_0x0037
                java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x002d }
                java.util.List r0 = (java.util.List) r0     // Catch:{ Throwable -> 0x002d }
                rx.Subscriber<? super java.util.List<T>> r4 = r5.f18974     // Catch:{ Throwable -> 0x002d }
                r4.onNext(r0)     // Catch:{ Throwable -> 0x002d }
                goto L_0x001b
            L_0x002d:
                r2 = move-exception
                rx.Subscriber<? super java.util.List<T>> r3 = r5.f18974
                rx.exceptions.Exceptions.m24533((java.lang.Throwable) r2, (rx.Observer<?>) r3)
                goto L_0x0006
            L_0x0034:
                r3 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0034 }
                throw r3     // Catch:{ Throwable -> 0x002d }
            L_0x0037:
                rx.Subscriber<? super java.util.List<T>> r3 = r5.f18974
                r3.onCompleted()
                r5.unsubscribe()
                goto L_0x0006
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorBufferWithTime.InexactSubscriber.onCompleted():void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24661() {
            this.f18971.m24504(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24663() {
                    InexactSubscriber.this.m24660();
                }
            }, OperatorBufferWithTime.this.f18960, OperatorBufferWithTime.this.f18960, OperatorBufferWithTime.this.f18962);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m24660() {
            final List<T> chunk = new ArrayList<>();
            synchronized (this) {
                if (!this.f18972) {
                    this.f18973.add(chunk);
                    this.f18971.m24505(new Action0() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m24664() {
                            InexactSubscriber.this.m24662(chunk);
                        }
                    }, OperatorBufferWithTime.this.f18963, OperatorBufferWithTime.this.f18962);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
            if (r1 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r5.f18974.onNext(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0029, code lost:
            r3 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
            rx.exceptions.Exceptions.m24533(r3, (rx.Observer<?>) r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m24662(java.util.List<T> r6) {
            /*
                r5 = this;
                r1 = 0
                monitor-enter(r5)
                boolean r4 = r5.f18972     // Catch:{ all -> 0x002e }
                if (r4 == 0) goto L_0x0008
                monitor-exit(r5)     // Catch:{ all -> 0x002e }
            L_0x0007:
                return
            L_0x0008:
                java.util.List<java.util.List<T>> r4 = r5.f18973     // Catch:{ all -> 0x002e }
                java.util.Iterator r2 = r4.iterator()     // Catch:{ all -> 0x002e }
            L_0x000e:
                boolean r4 = r2.hasNext()     // Catch:{ all -> 0x002e }
                if (r4 == 0) goto L_0x0020
                java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x002e }
                java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x002e }
                if (r0 != r6) goto L_0x000e
                r2.remove()     // Catch:{ all -> 0x002e }
                r1 = 1
            L_0x0020:
                monitor-exit(r5)     // Catch:{ all -> 0x002e }
                if (r1 == 0) goto L_0x0007
                rx.Subscriber<? super java.util.List<T>> r4 = r5.f18974     // Catch:{ Throwable -> 0x0029 }
                r4.onNext(r6)     // Catch:{ Throwable -> 0x0029 }
                goto L_0x0007
            L_0x0029:
                r3 = move-exception
                rx.exceptions.Exceptions.m24533((java.lang.Throwable) r3, (rx.Observer<?>) r5)
                goto L_0x0007
            L_0x002e:
                r4 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x002e }
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorBufferWithTime.InexactSubscriber.m24662(java.util.List):void");
        }
    }

    final class ExactSubscriber extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Scheduler.Worker f18965;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f18966;

        /* renamed from: 齉  reason: contains not printable characters */
        List<T> f18967 = new ArrayList();

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super List<T>> f18968;

        public ExactSubscriber(Subscriber<? super List<T>> child, Scheduler.Worker inner) {
            this.f18968 = child;
            this.f18965 = inner;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
            if (r0 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
            r3.f18968.onNext(r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onNext(T r4) {
            /*
                r3 = this;
                r0 = 0
                monitor-enter(r3)
                boolean r1 = r3.f18966     // Catch:{ all -> 0x002b }
                if (r1 == 0) goto L_0x0008
                monitor-exit(r3)     // Catch:{ all -> 0x002b }
            L_0x0007:
                return
            L_0x0008:
                java.util.List<T> r1 = r3.f18967     // Catch:{ all -> 0x002b }
                r1.add(r4)     // Catch:{ all -> 0x002b }
                java.util.List<T> r1 = r3.f18967     // Catch:{ all -> 0x002b }
                int r1 = r1.size()     // Catch:{ all -> 0x002b }
                rx.internal.operators.OperatorBufferWithTime r2 = rx.internal.operators.OperatorBufferWithTime.this     // Catch:{ all -> 0x002b }
                int r2 = r2.f18961     // Catch:{ all -> 0x002b }
                if (r1 != r2) goto L_0x0022
                java.util.List<T> r0 = r3.f18967     // Catch:{ all -> 0x002b }
                java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x002b }
                r1.<init>()     // Catch:{ all -> 0x002b }
                r3.f18967 = r1     // Catch:{ all -> 0x002b }
            L_0x0022:
                monitor-exit(r3)     // Catch:{ all -> 0x002b }
                if (r0 == 0) goto L_0x0007
                rx.Subscriber<? super java.util.List<T>> r1 = r3.f18968
                r1.onNext(r0)
                goto L_0x0007
            L_0x002b:
                r1 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x002b }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorBufferWithTime.ExactSubscriber.onNext(java.lang.Object):void");
        }

        public void onError(Throwable e) {
            synchronized (this) {
                if (!this.f18966) {
                    this.f18966 = true;
                    this.f18967 = null;
                    this.f18968.onError(e);
                    unsubscribe();
                }
            }
        }

        public void onCompleted() {
            try {
                this.f18965.unsubscribe();
                synchronized (this) {
                    if (!this.f18966) {
                        this.f18966 = true;
                        List<T> toEmit = this.f18967;
                        this.f18967 = null;
                        this.f18968.onNext(toEmit);
                        this.f18968.onCompleted();
                        unsubscribe();
                    }
                }
            } catch (Throwable t) {
                Exceptions.m24533(t, (Observer<?>) this.f18968);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24658() {
            this.f18965.m24504(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24659() {
                    ExactSubscriber.this.m24657();
                }
            }, OperatorBufferWithTime.this.f18963, OperatorBufferWithTime.this.f18963, OperatorBufferWithTime.this.f18962);
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* renamed from: 齉  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m24657() {
            /*
                r3 = this;
                monitor-enter(r3)
                boolean r2 = r3.f18966     // Catch:{ all -> 0x001c }
                if (r2 == 0) goto L_0x0007
                monitor-exit(r3)     // Catch:{ all -> 0x001c }
            L_0x0006:
                return
            L_0x0007:
                java.util.List<T> r1 = r3.f18967     // Catch:{ all -> 0x001c }
                java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x001c }
                r2.<init>()     // Catch:{ all -> 0x001c }
                r3.f18967 = r2     // Catch:{ all -> 0x001c }
                monitor-exit(r3)     // Catch:{ all -> 0x001c }
                rx.Subscriber<? super java.util.List<T>> r2 = r3.f18968     // Catch:{ Throwable -> 0x0017 }
                r2.onNext(r1)     // Catch:{ Throwable -> 0x0017 }
                goto L_0x0006
            L_0x0017:
                r0 = move-exception
                rx.exceptions.Exceptions.m24533((java.lang.Throwable) r0, (rx.Observer<?>) r3)
                goto L_0x0006
            L_0x001c:
                r2 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x001c }
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OperatorBufferWithTime.ExactSubscriber.m24657():void");
        }
    }
}
