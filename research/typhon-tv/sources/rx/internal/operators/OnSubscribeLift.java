package rx.internal.operators;

import rx.Observable;
import rx.Subscriber;
import rx.exceptions.Exceptions;
import rx.plugins.RxJavaHooks;

public final class OnSubscribeLift<T, R> implements Observable.OnSubscribe<R> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Observable.Operator<? extends R, ? super T> f18837;

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable.OnSubscribe<T> f18838;

    public OnSubscribeLift(Observable.OnSubscribe<T> parent, Observable.Operator<? extends R, ? super T> operator) {
        this.f18838 = parent;
        this.f18837 = operator;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super R> o) {
        Subscriber<? super T> st;
        try {
            st = (Subscriber) RxJavaHooks.m25018(this.f18837).call(o);
            st.m24510();
            this.f18838.call(st);
        } catch (Throwable e) {
            Exceptions.m24529(e);
            o.onError(e);
        }
    }
}
