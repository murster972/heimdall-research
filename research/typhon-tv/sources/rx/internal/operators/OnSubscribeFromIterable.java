package rx.internal.operators;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Observer;
import rx.Producer;
import rx.Subscriber;
import rx.exceptions.Exceptions;

public final class OnSubscribeFromIterable<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final Iterable<? extends T> f18836;

    public OnSubscribeFromIterable(Iterable<? extends T> iterable) {
        if (iterable == null) {
            throw new NullPointerException("iterable must not be null");
        }
        this.f18836 = iterable;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> o) {
        try {
            Iterator<? extends T> it2 = this.f18836.iterator();
            boolean b = it2.hasNext();
            if (o.isUnsubscribed()) {
                return;
            }
            if (!b) {
                o.onCompleted();
            } else {
                o.m24512((Producer) new IterableProducer(o, it2));
            }
        } catch (Throwable ex) {
            Exceptions.m24533(ex, (Observer<?>) o);
        }
    }

    static final class IterableProducer<T> extends AtomicLong implements Producer {
        private static final long serialVersionUID = -8730475647105475802L;

        /* renamed from: it  reason: collision with root package name */
        private final Iterator<? extends T> f21520it;
        private final Subscriber<? super T> o;

        IterableProducer(Subscriber<? super T> o2, Iterator<? extends T> it2) {
            this.o = o2;
            this.f21520it = it2;
        }

        public void request(long n) {
            if (get() != Long.MAX_VALUE) {
                if (n == Long.MAX_VALUE && compareAndSet(0, Long.MAX_VALUE)) {
                    m24598();
                } else if (n > 0 && BackpressureUtils.m24552((AtomicLong) this, n) == 0) {
                    m24599(n);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24599(long n) {
            Subscriber<? super T> o2 = this.o;
            Iterator<? extends T> it2 = this.f21520it;
            long r = n;
            long e = 0;
            while (true) {
                if (e == r) {
                    r = get();
                    if (e == r) {
                        r = BackpressureUtils.m24549((AtomicLong) this, e);
                        if (r != 0) {
                            e = 0;
                        } else {
                            return;
                        }
                    } else {
                        continue;
                    }
                } else if (!o2.isUnsubscribed()) {
                    try {
                        o2.onNext(it2.next());
                        if (!o2.isUnsubscribed()) {
                            try {
                                if (it2.hasNext()) {
                                    e++;
                                } else if (!o2.isUnsubscribed()) {
                                    o2.onCompleted();
                                    return;
                                } else {
                                    return;
                                }
                            } catch (Throwable ex) {
                                Exceptions.m24533(ex, (Observer<?>) o2);
                                return;
                            }
                        } else {
                            return;
                        }
                    } catch (Throwable ex2) {
                        Exceptions.m24533(ex2, (Observer<?>) o2);
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24598() {
            Subscriber<? super T> o2 = this.o;
            Iterator<? extends T> it2 = this.f21520it;
            while (!o2.isUnsubscribed()) {
                try {
                    o2.onNext(it2.next());
                    if (!o2.isUnsubscribed()) {
                        try {
                            if (!it2.hasNext()) {
                                if (!o2.isUnsubscribed()) {
                                    o2.onCompleted();
                                    return;
                                }
                                return;
                            }
                        } catch (Throwable ex) {
                            Exceptions.m24533(ex, (Observer<?>) o2);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable ex2) {
                    Exceptions.m24533(ex2, (Observer<?>) o2);
                    return;
                }
            }
        }
    }
}
