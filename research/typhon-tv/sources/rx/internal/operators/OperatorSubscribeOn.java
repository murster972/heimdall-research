package rx.internal.operators;

import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;

public final class OperatorSubscribeOn<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Observable<T> f19105;

    /* renamed from: 齉  reason: contains not printable characters */
    final boolean f19106;

    /* renamed from: 龘  reason: contains not printable characters */
    final Scheduler f19107;

    public OperatorSubscribeOn(Observable<T> source, Scheduler scheduler, boolean requestOn) {
        this.f19107 = scheduler;
        this.f19105 = source;
        this.f19106 = requestOn;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> subscriber) {
        Scheduler.Worker inner = this.f19107.createWorker();
        SubscribeOnSubscriber<T> parent = new SubscribeOnSubscriber<>(subscriber, this.f19106, inner, this.f19105);
        subscriber.m24513((Subscription) parent);
        subscriber.m24513((Subscription) inner);
        inner.m24503(parent);
    }

    static final class SubscribeOnSubscriber<T> extends Subscriber<T> implements Action0 {

        /* renamed from: 连任  reason: contains not printable characters */
        Thread f19108;

        /* renamed from: 靐  reason: contains not printable characters */
        final boolean f19109;

        /* renamed from: 麤  reason: contains not printable characters */
        Observable<T> f19110;

        /* renamed from: 齉  reason: contains not printable characters */
        final Scheduler.Worker f19111;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f19112;

        SubscribeOnSubscriber(Subscriber<? super T> actual, boolean requestOn, Scheduler.Worker worker, Observable<T> source) {
            this.f19112 = actual;
            this.f19109 = requestOn;
            this.f19111 = worker;
            this.f19110 = source;
        }

        public void onNext(T t) {
            this.f19112.onNext(t);
        }

        public void onError(Throwable e) {
            try {
                this.f19112.onError(e);
            } finally {
                this.f19111.unsubscribe();
            }
        }

        public void onCompleted() {
            try {
                this.f19112.onCompleted();
            } finally {
                this.f19111.unsubscribe();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24772() {
            Observable<T> src = this.f19110;
            this.f19110 = null;
            this.f19108 = Thread.currentThread();
            src.m7416((Subscriber<? super T>) this);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24773(final Producer p) {
            this.f19112.m24512((Producer) new Producer() {
                public void request(final long n) {
                    if (SubscribeOnSubscriber.this.f19108 == Thread.currentThread() || !SubscribeOnSubscriber.this.f19109) {
                        p.request(n);
                    } else {
                        SubscribeOnSubscriber.this.f19111.m24503(new Action0() {
                            /* renamed from: 龘  reason: contains not printable characters */
                            public void m24774() {
                                p.request(n);
                            }
                        });
                    }
                }
            });
        }
    }
}
