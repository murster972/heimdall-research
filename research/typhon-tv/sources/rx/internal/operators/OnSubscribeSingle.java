package rx.internal.operators;

import java.util.NoSuchElementException;
import rx.Observable;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.Subscription;

public class OnSubscribeSingle<T> implements Single.OnSubscribe<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Observable<T> f18884;

    public OnSubscribeSingle(Observable<T> observable) {
        this.f18884 = observable;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(final SingleSubscriber<? super T> child) {
        Subscriber<T> parent = new Subscriber<T>() {

            /* renamed from: 连任  reason: contains not printable characters */
            private T f18885;

            /* renamed from: 麤  reason: contains not printable characters */
            private boolean f18887;

            /* renamed from: 齉  reason: contains not printable characters */
            private boolean f18888;

            /* renamed from: 靐  reason: contains not printable characters */
            public void m24630() {
                m24511(2);
            }

            public void onCompleted() {
                if (!this.f18888) {
                    if (this.f18887) {
                        child.m24506(this.f18885);
                    } else {
                        child.m24507((Throwable) new NoSuchElementException("Observable emitted no items"));
                    }
                }
            }

            public void onError(Throwable e) {
                child.m24507(e);
                unsubscribe();
            }

            public void onNext(T t) {
                if (this.f18887) {
                    this.f18888 = true;
                    child.m24507((Throwable) new IllegalArgumentException("Observable emitted too many elements"));
                    unsubscribe();
                    return;
                }
                this.f18887 = true;
                this.f18885 = t;
            }
        };
        child.m24508((Subscription) parent);
        this.f18884.m7416(parent);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> OnSubscribeSingle<T> m24628(Observable<T> observable) {
        return new OnSubscribeSingle<>(observable);
    }
}
