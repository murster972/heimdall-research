package rx.internal.operators;

import rx.Observable;
import rx.Subscriber;

public final class OnSubscribeThrow<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Throwable f18901;

    public OnSubscribeThrow(Throwable exception) {
        this.f18901 = exception;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> observer) {
        observer.onError(this.f18901);
    }
}
