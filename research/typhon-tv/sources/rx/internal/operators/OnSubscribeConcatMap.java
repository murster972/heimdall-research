package rx.internal.operators;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.exceptions.MissingBackpressureException;
import rx.functions.Func1;
import rx.internal.producers.ProducerArbiter;
import rx.internal.util.ExceptionsUtils;
import rx.internal.util.ScalarSynchronousObservable;
import rx.internal.util.atomic.SpscAtomicArrayQueue;
import rx.internal.util.unsafe.SpscArrayQueue;
import rx.internal.util.unsafe.UnsafeAccess;
import rx.observers.SerializedSubscriber;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.SerialSubscription;

public final class OnSubscribeConcatMap<T, R> implements Observable.OnSubscribe<R> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Func1<? super T, ? extends Observable<? extends R>> f18801;

    /* renamed from: 麤  reason: contains not printable characters */
    final int f18802;

    /* renamed from: 齉  reason: contains not printable characters */
    final int f18803;

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable<? extends T> f18804;

    public OnSubscribeConcatMap(Observable<? extends T> source, Func1<? super T, ? extends Observable<? extends R>> mapper, int prefetch, int delayErrorMode) {
        this.f18804 = source;
        this.f18801 = mapper;
        this.f18803 = prefetch;
        this.f18802 = delayErrorMode;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super R> child) {
        Subscriber<? super R> s;
        if (this.f18802 == 0) {
            s = new SerializedSubscriber<>(child);
        } else {
            s = child;
        }
        final ConcatMapSubscriber<T, R> parent = new ConcatMapSubscriber<>(s, this.f18801, this.f18803, this.f18802);
        child.m24513((Subscription) parent);
        child.m24513((Subscription) parent.f18814);
        child.m24512((Producer) new Producer() {
            public void request(long n) {
                parent.m24572(n);
            }
        });
        if (!child.isUnsubscribed()) {
            this.f18804.m7416(parent);
        }
    }

    static final class ConcatMapSubscriber<T, R> extends Subscriber<T> {

        /* renamed from: ʻ  reason: contains not printable characters */
        final AtomicInteger f18812 = new AtomicInteger();

        /* renamed from: ʼ  reason: contains not printable characters */
        final AtomicReference<Throwable> f18813 = new AtomicReference<>();

        /* renamed from: ʽ  reason: contains not printable characters */
        final SerialSubscription f18814;

        /* renamed from: ˑ  reason: contains not printable characters */
        volatile boolean f18815;

        /* renamed from: ٴ  reason: contains not printable characters */
        volatile boolean f18816;

        /* renamed from: 连任  reason: contains not printable characters */
        final Queue<Object> f18817;

        /* renamed from: 靐  reason: contains not printable characters */
        final Func1<? super T, ? extends Observable<? extends R>> f18818;

        /* renamed from: 麤  reason: contains not printable characters */
        final ProducerArbiter f18819 = new ProducerArbiter();

        /* renamed from: 齉  reason: contains not printable characters */
        final int f18820;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super R> f18821;

        public ConcatMapSubscriber(Subscriber<? super R> actual, Func1<? super T, ? extends Observable<? extends R>> mapper, int prefetch, int delayErrorMode) {
            Queue<Object> q;
            this.f18821 = actual;
            this.f18818 = mapper;
            this.f18820 = delayErrorMode;
            if (UnsafeAccess.m24998()) {
                q = new SpscArrayQueue<>(prefetch);
            } else {
                q = new SpscAtomicArrayQueue<>(prefetch);
            }
            this.f18817 = q;
            this.f18814 = new SerialSubscription();
            m24511((long) prefetch);
        }

        public void onNext(T t) {
            if (!this.f18817.offer(NotificationLite.m24567(t))) {
                unsubscribe();
                onError(new MissingBackpressureException());
                return;
            }
            m24575();
        }

        public void onError(Throwable mainError) {
            if (ExceptionsUtils.addThrowable(this.f18813, mainError)) {
                this.f18815 = true;
                if (this.f18820 == 0) {
                    Throwable ex = ExceptionsUtils.terminate(this.f18813);
                    if (!ExceptionsUtils.isTerminated(ex)) {
                        this.f18821.onError(ex);
                    }
                    this.f18814.unsubscribe();
                    return;
                }
                m24575();
                return;
            }
            m24577(mainError);
        }

        public void onCompleted() {
            this.f18815 = true;
            m24575();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24572(long n) {
            if (n > 0) {
                this.f18819.request(n);
            } else if (n < 0) {
                throw new IllegalArgumentException("n >= 0 required but it was " + n);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24576(R value) {
            this.f18821.onNext(value);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24578(Throwable innerError, long produced) {
            if (!ExceptionsUtils.addThrowable(this.f18813, innerError)) {
                m24577(innerError);
            } else if (this.f18820 == 0) {
                Throwable ex = ExceptionsUtils.terminate(this.f18813);
                if (!ExceptionsUtils.isTerminated(ex)) {
                    this.f18821.onError(ex);
                }
                unsubscribe();
            } else {
                if (produced != 0) {
                    this.f18819.m24795(produced);
                }
                this.f18816 = false;
                m24575();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m24574(long produced) {
            if (produced != 0) {
                this.f18819.m24795(produced);
            }
            this.f18816 = false;
            m24575();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24577(Throwable e) {
            RxJavaHooks.m25024(e);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24575() {
            if (this.f18812.getAndIncrement() == 0) {
                int delayErrorMode = this.f18820;
                while (!this.f18821.isUnsubscribed()) {
                    if (!this.f18816) {
                        if (delayErrorMode != 1 || this.f18813.get() == null) {
                            boolean mainDone = this.f18815;
                            Object v = this.f18817.poll();
                            boolean empty = v == null;
                            if (mainDone && empty) {
                                Throwable ex = ExceptionsUtils.terminate(this.f18813);
                                if (ex == null) {
                                    this.f18821.onCompleted();
                                    return;
                                } else if (!ExceptionsUtils.isTerminated(ex)) {
                                    this.f18821.onError(ex);
                                    return;
                                } else {
                                    return;
                                }
                            } else if (!empty) {
                                try {
                                    Observable<? extends R> source = (Observable) this.f18818.call(NotificationLite.m24564(v));
                                    if (source == null) {
                                        m24573((Throwable) new NullPointerException("The source returned by the mapper was null"));
                                        return;
                                    } else if (source != Observable.m7349()) {
                                        if (source instanceof ScalarSynchronousObservable) {
                                            this.f18816 = true;
                                            this.f18819.m24796((Producer) new ConcatMapInnerScalarProducer(((ScalarSynchronousObservable) source).m24912(), this));
                                        } else {
                                            ConcatMapInnerSubscriber<T, R> innerSubscriber = new ConcatMapInnerSubscriber<>(this);
                                            this.f18814.m25088(innerSubscriber);
                                            if (!innerSubscriber.isUnsubscribed()) {
                                                this.f18816 = true;
                                                source.m7416((Subscriber<? super Object>) innerSubscriber);
                                            } else {
                                                return;
                                            }
                                        }
                                        m24511(1);
                                    } else {
                                        m24511(1);
                                    }
                                } catch (Throwable mapperError) {
                                    Exceptions.m24529(mapperError);
                                    m24573(mapperError);
                                    return;
                                }
                            }
                        } else {
                            Throwable ex2 = ExceptionsUtils.terminate(this.f18813);
                            if (!ExceptionsUtils.isTerminated(ex2)) {
                                this.f18821.onError(ex2);
                                return;
                            }
                            return;
                        }
                    }
                    if (this.f18812.decrementAndGet() == 0) {
                        return;
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24573(Throwable mapperError) {
            unsubscribe();
            if (ExceptionsUtils.addThrowable(this.f18813, mapperError)) {
                Throwable ex = ExceptionsUtils.terminate(this.f18813);
                if (!ExceptionsUtils.isTerminated(ex)) {
                    this.f18821.onError(ex);
                    return;
                }
                return;
            }
            m24577(mapperError);
        }
    }

    static final class ConcatMapInnerSubscriber<T, R> extends Subscriber<R> {

        /* renamed from: 靐  reason: contains not printable characters */
        long f18810;

        /* renamed from: 龘  reason: contains not printable characters */
        final ConcatMapSubscriber<T, R> f18811;

        public ConcatMapInnerSubscriber(ConcatMapSubscriber<T, R> parent) {
            this.f18811 = parent;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24571(Producer p) {
            this.f18811.f18819.m24796(p);
        }

        public void onNext(R t) {
            this.f18810++;
            this.f18811.m24576(t);
        }

        public void onError(Throwable e) {
            this.f18811.m24578(e, this.f18810);
        }

        public void onCompleted() {
            this.f18811.m24574(this.f18810);
        }
    }

    static final class ConcatMapInnerScalarProducer<T, R> implements Producer {

        /* renamed from: 靐  reason: contains not printable characters */
        final ConcatMapSubscriber<T, R> f18807;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f18808;

        /* renamed from: 龘  reason: contains not printable characters */
        final R f18809;

        public ConcatMapInnerScalarProducer(R value, ConcatMapSubscriber<T, R> parent) {
            this.f18809 = value;
            this.f18807 = parent;
        }

        public void request(long n) {
            if (!this.f18808 && n > 0) {
                this.f18808 = true;
                ConcatMapSubscriber<T, R> p = this.f18807;
                p.m24576(this.f18809);
                p.m24574(1);
            }
        }
    }
}
