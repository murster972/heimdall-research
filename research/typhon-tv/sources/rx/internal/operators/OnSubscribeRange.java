package rx.internal.operators;

import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;

public final class OnSubscribeRange implements Observable.OnSubscribe<Integer> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f18847;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f18848;

    public OnSubscribeRange(int start, int end) {
        this.f18848 = start;
        this.f18847 = end;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super Integer> childSubscriber) {
        childSubscriber.m24512((Producer) new RangeProducer(childSubscriber, this.f18848, this.f18847));
    }

    static final class RangeProducer extends AtomicLong implements Producer {
        private static final long serialVersionUID = 4114392207069098388L;
        private final Subscriber<? super Integer> childSubscriber;
        private long currentIndex;
        private final int endOfRange;

        RangeProducer(Subscriber<? super Integer> childSubscriber2, int startIndex, int endIndex) {
            this.childSubscriber = childSubscriber2;
            this.currentIndex = (long) startIndex;
            this.endOfRange = endIndex;
        }

        public void request(long requestedAmount) {
            if (get() != Long.MAX_VALUE) {
                if (requestedAmount == Long.MAX_VALUE && compareAndSet(0, Long.MAX_VALUE)) {
                    m24611();
                } else if (requestedAmount > 0 && BackpressureUtils.m24552((AtomicLong) this, requestedAmount) == 0) {
                    m24612(requestedAmount);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24612(long requestedAmount) {
            long emitted = 0;
            long endIndex = ((long) this.endOfRange) + 1;
            long index = this.currentIndex;
            Subscriber<? super Integer> childSubscriber2 = this.childSubscriber;
            while (true) {
                if (emitted == requestedAmount || index == endIndex) {
                    if (childSubscriber2.isUnsubscribed()) {
                        return;
                    }
                    if (index == endIndex) {
                        childSubscriber2.onCompleted();
                        return;
                    }
                    requestedAmount = get();
                    if (requestedAmount == emitted) {
                        this.currentIndex = index;
                        requestedAmount = addAndGet(-emitted);
                        if (requestedAmount != 0) {
                            emitted = 0;
                        } else {
                            return;
                        }
                    } else {
                        continue;
                    }
                } else if (!childSubscriber2.isUnsubscribed()) {
                    childSubscriber2.onNext(Integer.valueOf((int) index));
                    index++;
                    emitted++;
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24611() {
            long endIndex = ((long) this.endOfRange) + 1;
            Subscriber<? super Integer> childSubscriber2 = this.childSubscriber;
            long index = this.currentIndex;
            while (index != endIndex) {
                if (!childSubscriber2.isUnsubscribed()) {
                    childSubscriber2.onNext(Integer.valueOf((int) index));
                    index++;
                } else {
                    return;
                }
            }
            if (!childSubscriber2.isUnsubscribed()) {
                childSubscriber2.onCompleted();
            }
        }
    }
}
