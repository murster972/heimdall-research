package rx.internal.operators;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import rx.Notification;
import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.internal.producers.ProducerArbiter;
import rx.observers.Subscribers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;
import rx.subscriptions.SerialSubscription;

public final class OnSubscribeRedo<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 麤  reason: contains not printable characters */
    static final Func1<Observable<? extends Notification<?>>, Observable<?>> f18849 = new Func1<Observable<? extends Notification<?>>, Observable<?>>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public Observable<?> call(Observable<? extends Notification<?>> ts) {
            return ts.m7392((Func1<? super Object, ? extends R>) new Func1<Notification<?>, Notification<?>>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Notification<?> call(Notification<?> notification) {
                    return Notification.m24492(null);
                }
            });
        }
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Scheduler f18850;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Func1<? super Observable<? extends Notification<?>>, ? extends Observable<?>> f18851;

    /* renamed from: 靐  reason: contains not printable characters */
    final boolean f18852;

    /* renamed from: 齉  reason: contains not printable characters */
    final boolean f18853;

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable<T> f18854;

    public static final class RedoFinite implements Func1<Observable<? extends Notification<?>>, Observable<?>> {

        /* renamed from: 龘  reason: contains not printable characters */
        final long f18881;

        public RedoFinite(long count) {
            this.f18881 = count;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Observable<?> call(Observable<? extends Notification<?>> ts) {
            return ts.m7392((Func1<? super Object, ? extends R>) new Func1<Notification<?>, Notification<?>>() {

                /* renamed from: 龘  reason: contains not printable characters */
                int f18883;

                /* renamed from: 龘  reason: contains not printable characters */
                public Notification<?> call(Notification<?> terminalNotification) {
                    if (RedoFinite.this.f18881 == 0) {
                        return terminalNotification;
                    }
                    this.f18883++;
                    if (((long) this.f18883) <= RedoFinite.this.f18881) {
                        return Notification.m24492(Integer.valueOf(this.f18883));
                    }
                    return terminalNotification;
                }
            }).m7388();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m24613(Observable<T> source, long count) {
        if (count >= 0) {
            return count == 0 ? source : m24614(source, (Func1<? super Observable<? extends Notification<?>>, ? extends Observable<?>>) new RedoFinite(count));
        }
        throw new IllegalArgumentException("count >= 0 expected");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m24614(Observable<T> source, Func1<? super Observable<? extends Notification<?>>, ? extends Observable<?>> notificationHandler) {
        return Observable.m7346(new OnSubscribeRedo(source, notificationHandler, true, false, Schedulers.trampoline()));
    }

    private OnSubscribeRedo(Observable<T> source, Func1<? super Observable<? extends Notification<?>>, ? extends Observable<?>> f, boolean stopOnComplete, boolean stopOnError, Scheduler scheduler) {
        this.f18854 = source;
        this.f18851 = f;
        this.f18852 = stopOnComplete;
        this.f18853 = stopOnError;
        this.f18850 = scheduler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> child) {
        final AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        final AtomicLong consumerCapacity = new AtomicLong();
        final Scheduler.Worker worker = this.f18850.createWorker();
        child.m24513((Subscription) worker);
        final SerialSubscription sourceSubscriptions = new SerialSubscription();
        child.m24513((Subscription) sourceSubscriptions);
        final Subject<Notification<?>, Notification<?>> terminals = BehaviorSubject.m25058().m25066();
        terminals.m7386((Subscriber<? super Notification<?>>) Subscribers.m25003());
        final ProducerArbiter arbiter = new ProducerArbiter();
        final Subscriber<? super T> subscriber = child;
        Action0 subscribeToSource = new Action0() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m24618() {
                if (!subscriber.isUnsubscribed()) {
                    Subscriber<T> terminalDelegatingSubscriber = new Subscriber<T>() {

                        /* renamed from: 龘  reason: contains not printable characters */
                        boolean f18863;

                        public void onCompleted() {
                            if (!this.f18863) {
                                this.f18863 = true;
                                unsubscribe();
                                terminals.onNext(Notification.m24491());
                            }
                        }

                        public void onError(Throwable e) {
                            if (!this.f18863) {
                                this.f18863 = true;
                                unsubscribe();
                                terminals.onNext(Notification.m24493(e));
                            }
                        }

                        public void onNext(T v) {
                            if (!this.f18863) {
                                subscriber.onNext(v);
                                m24619();
                                arbiter.m24795(1);
                            }
                        }

                        /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
                        /* renamed from: 龘  reason: contains not printable characters */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        private void m24619() {
                            /*
                                r6 = this;
                            L_0x0000:
                                rx.internal.operators.OnSubscribeRedo$2 r2 = rx.internal.operators.OnSubscribeRedo.AnonymousClass2.this
                                java.util.concurrent.atomic.AtomicLong r2 = r7
                                long r0 = r2.get()
                                r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                                int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                                if (r2 == 0) goto L_0x001f
                                rx.internal.operators.OnSubscribeRedo$2 r2 = rx.internal.operators.OnSubscribeRedo.AnonymousClass2.this
                                java.util.concurrent.atomic.AtomicLong r2 = r7
                                r4 = 1
                                long r4 = r0 - r4
                                boolean r2 = r2.compareAndSet(r0, r4)
                                if (r2 == 0) goto L_0x0000
                            L_0x001f:
                                return
                            */
                            throw new UnsupportedOperationException("Method not decompiled: rx.internal.operators.OnSubscribeRedo.AnonymousClass2.AnonymousClass1.m24619():void");
                        }

                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m24620(Producer producer) {
                            arbiter.m24796(producer);
                        }
                    };
                    sourceSubscriptions.m25088(terminalDelegatingSubscriber);
                    OnSubscribeRedo.this.f18854.m7416(terminalDelegatingSubscriber);
                }
            }
        };
        final Observable<?> restarts = (Observable) this.f18851.call(terminals.m7404((Observable.Operator<? extends Notification<?>, ? super Notification<?>>) new Observable.Operator<Notification<?>, Notification<?>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Subscriber<? super Notification<?>> call(final Subscriber<? super Notification<?>> filteredTerminals) {
                return new Subscriber<Notification<?>>(filteredTerminals) {
                    public void onCompleted() {
                        filteredTerminals.onCompleted();
                    }

                    public void onError(Throwable e) {
                        filteredTerminals.onError(e);
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public void onNext(Notification<?> t) {
                        if (t.m24496() && OnSubscribeRedo.this.f18852) {
                            filteredTerminals.onCompleted();
                        } else if (!t.m24495() || !OnSubscribeRedo.this.f18853) {
                            filteredTerminals.onNext(t);
                        } else {
                            filteredTerminals.onError(t.m24499());
                        }
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m24623(Producer producer) {
                        producer.request(Long.MAX_VALUE);
                    }
                };
            }
        }));
        final Subscriber<? super T> subscriber2 = child;
        final AtomicLong atomicLong = consumerCapacity;
        final Action0 action0 = subscribeToSource;
        worker.m24503(new Action0() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m24624() {
                restarts.m7416(new Subscriber<Object>(subscriber2) {
                    public void onCompleted() {
                        subscriber2.onCompleted();
                    }

                    public void onError(Throwable e) {
                        subscriber2.onError(e);
                    }

                    public void onNext(Object t) {
                        if (subscriber2.isUnsubscribed()) {
                            return;
                        }
                        if (atomicLong.get() > 0) {
                            worker.m24503(action0);
                        } else {
                            atomicBoolean.compareAndSet(false, true);
                        }
                    }

                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m24625(Producer producer) {
                        producer.request(Long.MAX_VALUE);
                    }
                });
            }
        });
        final AtomicLong atomicLong2 = consumerCapacity;
        final ProducerArbiter producerArbiter = arbiter;
        final AtomicBoolean atomicBoolean2 = atomicBoolean;
        final Scheduler.Worker worker2 = worker;
        final Action0 action02 = subscribeToSource;
        child.m24512((Producer) new Producer() {
            public void request(long n) {
                if (n > 0) {
                    BackpressureUtils.m24552(atomicLong2, n);
                    producerArbiter.request(n);
                    if (atomicBoolean2.compareAndSet(true, false)) {
                        worker2.m24503(action02);
                    }
                }
            }
        });
    }
}
