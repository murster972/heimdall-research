package rx.internal.operators;

import rx.Observable;
import rx.Subscriber;

public enum EmptyObservableHolder implements Observable.OnSubscribe<Object> {
    INSTANCE;
    

    /* renamed from: 龘  reason: contains not printable characters */
    static final Observable<Object> f18796 = null;

    static {
        f18796 = Observable.m7346(INSTANCE);
    }

    public static <T> Observable<T> instance() {
        return f18796;
    }

    public void call(Subscriber<? super Object> child) {
        child.onCompleted();
    }
}
