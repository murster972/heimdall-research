package rx.internal.operators;

import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;

public final class OnSubscribeFromArray<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final T[] f18835;

    public OnSubscribeFromArray(T[] array) {
        this.f18835 = array;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> child) {
        child.m24512((Producer) new FromArrayProducer(child, this.f18835));
    }

    static final class FromArrayProducer<T> extends AtomicLong implements Producer {
        private static final long serialVersionUID = 3534218984725836979L;
        final T[] array;
        final Subscriber<? super T> child;
        int index;

        public FromArrayProducer(Subscriber<? super T> child2, T[] array2) {
            this.child = child2;
            this.array = array2;
        }

        public void request(long n) {
            if (n < 0) {
                throw new IllegalArgumentException("n >= 0 required but it was " + n);
            } else if (n == Long.MAX_VALUE) {
                if (BackpressureUtils.m24552((AtomicLong) this, n) == 0) {
                    m24595();
                }
            } else if (n != 0 && BackpressureUtils.m24552((AtomicLong) this, n) == 0) {
                m24596(n);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24595() {
            Subscriber<? super T> child2 = this.child;
            T[] tArr = this.array;
            int length = tArr.length;
            int i = 0;
            while (i < length) {
                T t = tArr[i];
                if (!child2.isUnsubscribed()) {
                    child2.onNext(t);
                    i++;
                } else {
                    return;
                }
            }
            if (!child2.isUnsubscribed()) {
                child2.onCompleted();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24596(long r) {
            Subscriber<? super T> child2 = this.child;
            T[] array2 = this.array;
            int n = array2.length;
            long e = 0;
            int i = this.index;
            while (true) {
                if (r == 0 || i == n) {
                    r = get() + e;
                    if (r == 0) {
                        this.index = i;
                        r = addAndGet(e);
                        if (r != 0) {
                            e = 0;
                        } else {
                            return;
                        }
                    } else {
                        continue;
                    }
                } else if (!child2.isUnsubscribed()) {
                    child2.onNext(array2[i]);
                    i++;
                    if (i != n) {
                        r--;
                        e--;
                    } else if (!child2.isUnsubscribed()) {
                        child2.onCompleted();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }
}
