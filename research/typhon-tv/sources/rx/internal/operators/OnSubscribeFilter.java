package rx.internal.operators;

import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.exceptions.OnErrorThrowable;
import rx.functions.Func1;
import rx.plugins.RxJavaHooks;

public final class OnSubscribeFilter<T> implements Observable.OnSubscribe<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Func1<? super T, Boolean> f18830;

    /* renamed from: 龘  reason: contains not printable characters */
    final Observable<T> f18831;

    public OnSubscribeFilter(Observable<T> source, Func1<? super T, Boolean> predicate) {
        this.f18831 = source;
        this.f18830 = predicate;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> child) {
        FilterSubscriber<T> parent = new FilterSubscriber<>(child, this.f18830);
        child.m24513((Subscription) parent);
        this.f18831.m7416(parent);
    }

    static final class FilterSubscriber<T> extends Subscriber<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Func1<? super T, Boolean> f18832;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f18833;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f18834;

        public FilterSubscriber(Subscriber<? super T> actual, Func1<? super T, Boolean> predicate) {
            this.f18834 = actual;
            this.f18832 = predicate;
            m24511(0);
        }

        public void onNext(T t) {
            try {
                if (this.f18832.call(t).booleanValue()) {
                    this.f18834.onNext(t);
                } else {
                    m24511(1);
                }
            } catch (Throwable ex) {
                Exceptions.m24529(ex);
                unsubscribe();
                onError(OnErrorThrowable.addValueAsLastCause(ex, t));
            }
        }

        public void onError(Throwable e) {
            if (this.f18833) {
                RxJavaHooks.m25024(e);
                return;
            }
            this.f18833 = true;
            this.f18834.onError(e);
        }

        public void onCompleted() {
            if (!this.f18833) {
                this.f18834.onCompleted();
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24593(Producer p) {
            super.m24512(p);
            this.f18834.m24512(p);
        }
    }
}
