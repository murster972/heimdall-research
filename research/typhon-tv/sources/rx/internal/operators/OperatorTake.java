package rx.internal.operators;

import java.util.concurrent.atomic.AtomicLong;
import rx.Observable;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.plugins.RxJavaHooks;

public final class OperatorTake<T> implements Observable.Operator<T, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final int f19117;

    public OperatorTake(int limit) {
        if (limit < 0) {
            throw new IllegalArgumentException("limit >= 0 required but it was " + limit);
        }
        this.f19117 = limit;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(final Subscriber<? super T> child) {
        Subscriber<T> parent = new Subscriber<T>() {

            /* renamed from: 靐  reason: contains not printable characters */
            boolean f19118;

            /* renamed from: 龘  reason: contains not printable characters */
            int f19121;

            public void onCompleted() {
                if (!this.f19118) {
                    this.f19118 = true;
                    child.onCompleted();
                }
            }

            public void onError(Throwable e) {
                if (!this.f19118) {
                    this.f19118 = true;
                    try {
                        child.onError(e);
                    } finally {
                        unsubscribe();
                    }
                } else {
                    RxJavaHooks.m25024(e);
                }
            }

            public void onNext(T i) {
                if (!isUnsubscribed()) {
                    int i2 = this.f19121;
                    this.f19121 = i2 + 1;
                    if (i2 < OperatorTake.this.f19117) {
                        boolean stop = this.f19121 == OperatorTake.this.f19117;
                        child.onNext(i);
                        if (stop && !this.f19118) {
                            this.f19118 = true;
                            try {
                                child.onCompleted();
                            } finally {
                                unsubscribe();
                            }
                        }
                    }
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24776(final Producer producer) {
                child.m24512((Producer) new Producer() {

                    /* renamed from: 龘  reason: contains not printable characters */
                    final AtomicLong f19124 = new AtomicLong(0);

                    public void request(long n) {
                        long r;
                        long c;
                        if (n > 0 && !AnonymousClass1.this.f19118) {
                            do {
                                r = this.f19124.get();
                                c = Math.min(n, ((long) OperatorTake.this.f19117) - r);
                                if (c == 0) {
                                    return;
                                }
                            } while (!this.f19124.compareAndSet(r, r + c));
                            producer.request(c);
                        }
                    }
                });
            }
        };
        if (this.f19117 == 0) {
            child.onCompleted();
            parent.unsubscribe();
        }
        child.m24513((Subscription) parent);
        return parent;
    }
}
