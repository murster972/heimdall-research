package rx.internal.operators;

import rx.Observable;
import rx.Observer;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Func1;
import rx.internal.producers.ProducerArbiter;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.SerialSubscription;

public final class OperatorOnErrorResumeNextViaFunction<T> implements Observable.Operator<T, T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final Func1<? super Throwable, ? extends Observable<? extends T>> f19052;

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> OperatorOnErrorResumeNextViaFunction<T> m24712(final Func1<? super Throwable, ? extends T> resumeFunction) {
        return new OperatorOnErrorResumeNextViaFunction<>(new Func1<Throwable, Observable<? extends T>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<? extends T> call(Throwable t) {
                return Observable.m7356(resumeFunction.call(t));
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> OperatorOnErrorResumeNextViaFunction<T> m24711(final Observable<? extends T> other) {
        return new OperatorOnErrorResumeNextViaFunction<>(new Func1<Throwable, Observable<? extends T>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable<? extends T> call(Throwable t) {
                return other;
            }
        });
    }

    public OperatorOnErrorResumeNextViaFunction(Func1<? super Throwable, ? extends Observable<? extends T>> f) {
        this.f19052 = f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(final Subscriber<? super T> child) {
        final ProducerArbiter pa = new ProducerArbiter();
        final SerialSubscription serial = new SerialSubscription();
        Subscriber<T> parent = new Subscriber<T>() {

            /* renamed from: ʻ  reason: contains not printable characters */
            private boolean f19055;

            /* renamed from: 龘  reason: contains not printable characters */
            long f19060;

            public void onCompleted() {
                if (!this.f19055) {
                    this.f19055 = true;
                    child.onCompleted();
                }
            }

            public void onError(Throwable e) {
                if (this.f19055) {
                    Exceptions.m24529(e);
                    RxJavaHooks.m25024(e);
                    return;
                }
                this.f19055 = true;
                try {
                    unsubscribe();
                    Subscriber<T> next = new Subscriber<T>() {
                        public void onNext(T t) {
                            child.onNext(t);
                        }

                        public void onError(Throwable e) {
                            child.onError(e);
                        }

                        public void onCompleted() {
                            child.onCompleted();
                        }

                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m24717(Producer producer) {
                            pa.m24796(producer);
                        }
                    };
                    serial.m25088(next);
                    long p = this.f19060;
                    if (p != 0) {
                        pa.m24795(p);
                    }
                    ((Observable) OperatorOnErrorResumeNextViaFunction.this.f19052.call(e)).m7416(next);
                } catch (Throwable e2) {
                    Exceptions.m24533(e2, (Observer<?>) child);
                }
            }

            public void onNext(T t) {
                if (!this.f19055) {
                    this.f19060++;
                    child.onNext(t);
                }
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24716(Producer producer) {
                pa.m24796(producer);
            }
        };
        serial.m25088(parent);
        child.m24513((Subscription) serial);
        child.m24512((Producer) pa);
        return parent;
    }
}
