package rx.internal.operators;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import rx.Observable;
import rx.Observer;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.internal.producers.SingleDelayedProducer;

public final class OperatorToObservableList<T> implements Observable.Operator<List<T>, T> {

    static final class Holder {

        /* renamed from: 龘  reason: contains not printable characters */
        static final OperatorToObservableList<Object> f19135 = new OperatorToObservableList<>();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> OperatorToObservableList<T> m24779() {
        return Holder.f19135;
    }

    OperatorToObservableList() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscriber<? super T> call(final Subscriber<? super List<T>> o) {
        final SingleDelayedProducer<List<T>> producer = new SingleDelayedProducer<>(o);
        Subscriber<T> result = new Subscriber<T>() {

            /* renamed from: 靐  reason: contains not printable characters */
            List<T> f19131 = new LinkedList();

            /* renamed from: 龘  reason: contains not printable characters */
            boolean f19134;

            /* renamed from: 靐  reason: contains not printable characters */
            public void m24781() {
                m24511(Long.MAX_VALUE);
            }

            public void onCompleted() {
                if (!this.f19134) {
                    this.f19134 = true;
                    try {
                        List<T> result = new ArrayList<>(this.f19131);
                        this.f19131 = null;
                        producer.setValue(result);
                    } catch (Throwable t) {
                        Exceptions.m24533(t, (Observer<?>) this);
                    }
                }
            }

            public void onError(Throwable e) {
                o.onError(e);
            }

            public void onNext(T value) {
                if (!this.f19134) {
                    this.f19131.add(value);
                }
            }
        };
        o.m24513((Subscription) result);
        o.m24512((Producer) producer);
        return result;
    }
}
