package rx.internal.producers;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import rx.Producer;
import rx.Subscriber;
import rx.exceptions.Exceptions;
import rx.internal.operators.BackpressureUtils;
import rx.internal.util.atomic.SpscLinkedAtomicQueue;
import rx.internal.util.unsafe.SpscLinkedQueue;
import rx.internal.util.unsafe.UnsafeAccess;

public final class QueuedValueProducer<T> extends AtomicLong implements Producer {
    private static final long serialVersionUID = 7277121710709137047L;

    /* renamed from: 龘  reason: contains not printable characters */
    static final Object f19161 = new Object();
    final Subscriber<? super T> child;
    final Queue<Object> queue;
    final AtomicInteger wip;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public QueuedValueProducer(Subscriber<? super T> child2) {
        this(child2, UnsafeAccess.m24998() ? new SpscLinkedQueue() : new SpscLinkedAtomicQueue());
    }

    public QueuedValueProducer(Subscriber<? super T> child2, Queue<Object> queue2) {
        this.child = child2;
        this.queue = queue2;
        this.wip = new AtomicInteger();
    }

    public void request(long n) {
        if (n < 0) {
            throw new IllegalArgumentException("n >= 0 required");
        } else if (n > 0) {
            BackpressureUtils.m24552((AtomicLong) this, n);
            m24799();
        }
    }

    public boolean offer(T value) {
        if (value == null) {
            if (!this.queue.offer(f19161)) {
                return false;
            }
        } else if (!this.queue.offer(value)) {
            return false;
        }
        m24799();
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24799() {
        T v;
        if (this.wip.getAndIncrement() == 0) {
            Subscriber<? super T> c = this.child;
            Queue<Object> q = this.queue;
            while (!c.isUnsubscribed()) {
                this.wip.lazySet(1);
                long r = get();
                long e = 0;
                while (r != 0 && (v = q.poll()) != null) {
                    try {
                        if (v == f19161) {
                            c.onNext(null);
                        } else {
                            c.onNext(v);
                        }
                        if (!c.isUnsubscribed()) {
                            r--;
                            e++;
                        } else {
                            return;
                        }
                    } catch (Throwable ex) {
                        if (v == f19161) {
                            v = null;
                        }
                        Exceptions.m24534(ex, c, v);
                        return;
                    }
                }
                if (!(e == 0 || get() == Long.MAX_VALUE)) {
                    addAndGet(-e);
                }
                if (this.wip.decrementAndGet() == 0) {
                    return;
                }
            }
        }
    }
}
