package rx.internal.producers;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import rx.Observer;
import rx.Producer;
import rx.Subscriber;
import rx.exceptions.Exceptions;
import rx.exceptions.MissingBackpressureException;
import rx.internal.operators.BackpressureUtils;
import rx.internal.util.atomic.SpscLinkedAtomicQueue;
import rx.internal.util.unsafe.SpscLinkedQueue;
import rx.internal.util.unsafe.UnsafeAccess;

public final class QueuedProducer<T> extends AtomicLong implements Observer<T>, Producer {
    private static final long serialVersionUID = 7277121710709137047L;

    /* renamed from: 龘  reason: contains not printable characters */
    static final Object f19160 = new Object();
    final Subscriber<? super T> child;
    volatile boolean done;
    Throwable error;
    final Queue<Object> queue;
    final AtomicInteger wip;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public QueuedProducer(Subscriber<? super T> child2) {
        this(child2, UnsafeAccess.m24998() ? new SpscLinkedQueue() : new SpscLinkedAtomicQueue());
    }

    public QueuedProducer(Subscriber<? super T> child2, Queue<Object> queue2) {
        this.child = child2;
        this.queue = queue2;
        this.wip = new AtomicInteger();
    }

    public void request(long n) {
        if (n < 0) {
            throw new IllegalArgumentException("n >= 0 required");
        } else if (n > 0) {
            BackpressureUtils.m24552((AtomicLong) this, n);
            m24797();
        }
    }

    public boolean offer(T value) {
        if (value == null) {
            if (!this.queue.offer(f19160)) {
                return false;
            }
        } else if (!this.queue.offer(value)) {
            return false;
        }
        m24797();
        return true;
    }

    public void onNext(T value) {
        if (!offer(value)) {
            onError(new MissingBackpressureException());
        }
    }

    public void onError(Throwable e) {
        this.error = e;
        this.done = true;
        m24797();
    }

    public void onCompleted() {
        this.done = true;
        m24797();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m24798(boolean isDone, boolean isEmpty) {
        if (this.child.isUnsubscribed()) {
            return true;
        }
        if (isDone) {
            Throwable e = this.error;
            if (e != null) {
                this.queue.clear();
                this.child.onError(e);
                return true;
            } else if (isEmpty) {
                this.child.onCompleted();
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24797() {
        if (this.wip.getAndIncrement() == 0) {
            Subscriber<? super T> c = this.child;
            Queue<Object> q = this.queue;
            while (!m24798(this.done, q.isEmpty())) {
                this.wip.lazySet(1);
                long r = get();
                long e = 0;
                while (r != 0) {
                    boolean d = this.done;
                    T v = q.poll();
                    if (m24798(d, v == null)) {
                        return;
                    }
                    if (v == null) {
                        break;
                    }
                    try {
                        if (v == f19160) {
                            c.onNext(null);
                        } else {
                            c.onNext(v);
                        }
                        r--;
                        e++;
                    } catch (Throwable ex) {
                        if (v == f19160) {
                            v = null;
                        }
                        Exceptions.m24534(ex, c, v);
                        return;
                    }
                }
                if (!(e == 0 || get() == Long.MAX_VALUE)) {
                    addAndGet(-e);
                }
                if (this.wip.decrementAndGet() == 0) {
                    return;
                }
            }
        }
    }
}
