package rx.internal.util.unsafe;

/* compiled from: SpscArrayQueue */
abstract class SpscArrayQueueColdField<E> extends ConcurrentCircularArrayQueue<E> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Integer f19367 = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096);

    /* renamed from: 麤  reason: contains not printable characters */
    protected final int f19368;

    public SpscArrayQueueColdField(int capacity) {
        super(capacity);
        this.f19368 = Math.min(capacity / 4, f19367.intValue());
    }
}
