package rx.internal.util.unsafe;

import java.util.Iterator;

public class SpscUnboundedArrayQueue<E> extends SpscUnboundedArrayQueueConsumerField<E> {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final long f19371;

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final Object f19372 = new Object();

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final long f19373;

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final long f19374 = ((long) UnsafeAccess.f19384.arrayBaseOffset(Object[].class));

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final int f19375;

    /* renamed from: 龘  reason: contains not printable characters */
    static final int f19376 = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();

    static {
        int scale = UnsafeAccess.f19384.arrayIndexScale(Object[].class);
        if (4 == scale) {
            f19375 = 2;
        } else if (8 == scale) {
            f19375 = 3;
        } else {
            throw new IllegalStateException("Unknown pointer size");
        }
        try {
            f19371 = UnsafeAccess.f19384.objectFieldOffset(SpscUnboundedArrayQueueProducerFields.class.getDeclaredField("producerIndex"));
            try {
                f19373 = UnsafeAccess.f19384.objectFieldOffset(SpscUnboundedArrayQueueConsumerField.class.getDeclaredField("consumerIndex"));
            } catch (NoSuchFieldException e) {
                InternalError ex = new InternalError();
                ex.initCause(e);
                throw ex;
            }
        } catch (NoSuchFieldException e2) {
            InternalError ex2 = new InternalError();
            ex2.initCause(e2);
            throw ex2;
        }
    }

    public SpscUnboundedArrayQueue(int bufferSize) {
        int p2capacity = Pow2.m24971(bufferSize);
        long mask = (long) (p2capacity - 1);
        E[] buffer = (Object[]) new Object[(p2capacity + 1)];
        this.f19380 = buffer;
        this.f19379 = mask;
        m24990(p2capacity);
        this.f19378 = buffer;
        this.f19377 = mask;
        this.f19381 = mask - 1;
        m24991(0);
    }

    public final Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }

    public final boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException("Null is not a valid element");
        }
        E[] buffer = this.f19380;
        long index = this.producerIndex;
        long mask = this.f19379;
        long offset = m24987(index, mask);
        if (index < this.f19381) {
            return m24995(buffer, e, index, offset);
        }
        int lookAheadStep = this.f19382;
        if (m24988(buffer, m24987(((long) lookAheadStep) + index, mask)) == null) {
            this.f19381 = (((long) lookAheadStep) + index) - 1;
            return m24995(buffer, e, index, offset);
        } else if (m24988(buffer, m24987(1 + index, mask)) != null) {
            return m24995(buffer, e, index, offset);
        } else {
            m24992(buffer, index, offset, e, mask);
            return true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m24995(E[] buffer, E e, long index, long offset) {
        m24993((Object[]) buffer, offset, (Object) e);
        m24991(1 + index);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24992(E[] oldBuffer, long currIndex, long offset, E e, long mask) {
        E[] newBuffer = (Object[]) new Object[oldBuffer.length];
        this.f19380 = newBuffer;
        this.f19381 = (currIndex + mask) - 1;
        m24993((Object[]) newBuffer, offset, (Object) e);
        m24994(oldBuffer, newBuffer);
        m24993((Object[]) oldBuffer, offset, f19372);
        m24991(currIndex + 1);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24994(E[] curr, E[] next) {
        m24993((Object[]) curr, m24985((long) (curr.length - 1)), (Object) next);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private E[] m24996(E[] curr) {
        return (Object[]) m24988(curr, m24985((long) (curr.length - 1)));
    }

    public final E poll() {
        E[] buffer = this.f19378;
        long index = this.consumerIndex;
        long mask = this.f19377;
        long offset = m24987(index, mask);
        Object e = m24988(buffer, offset);
        boolean isNextBuffer = e == f19372;
        if (e != null && !isNextBuffer) {
            m24993((Object[]) buffer, offset, (Object) null);
            m24984(1 + index);
            return e;
        } else if (!isNextBuffer) {
            return null;
        } else {
            return m24989((E[]) m24996(buffer), index, mask);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private E m24989(E[] nextBuffer, long index, long mask) {
        this.f19378 = nextBuffer;
        long offsetInNew = m24987(index, mask);
        E n = m24988(nextBuffer, offsetInNew);
        if (n == null) {
            return null;
        }
        m24993((Object[]) nextBuffer, offsetInNew, (Object) null);
        m24984(1 + index);
        return n;
    }

    public final E peek() {
        E[] buffer = this.f19378;
        long index = this.consumerIndex;
        long mask = this.f19377;
        Object e = m24988(buffer, m24987(index, mask));
        if (e != f19372) {
            return e;
        }
        return m24983(m24996(buffer), index, mask);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private E m24983(E[] nextBuffer, long index, long mask) {
        this.f19378 = nextBuffer;
        return m24988(nextBuffer, m24987(index, mask));
    }

    public final int size() {
        long before;
        long currentProducerIndex;
        long after = m24982();
        do {
            before = after;
            currentProducerIndex = m24986();
            after = m24982();
        } while (before != after);
        return (int) (currentProducerIndex - after);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24990(int capacity) {
        this.f19382 = Math.min(capacity / 4, f19376);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m24986() {
        return UnsafeAccess.f19384.getLongVolatile(this, f19371);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private long m24982() {
        return UnsafeAccess.f19384.getLongVolatile(this, f19373);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24991(long v) {
        UnsafeAccess.f19384.putOrderedLong(this, f19371, v);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m24984(long v) {
        UnsafeAccess.f19384.putOrderedLong(this, f19373, v);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m24987(long index, long mask) {
        return m24985(index & mask);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static long m24985(long index) {
        return f19374 + (index << f19375);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m24993(Object[] buffer, long offset, Object e) {
        UnsafeAccess.f19384.putOrderedObject(buffer, offset, e);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <E> Object m24988(E[] buffer, long offset) {
        return UnsafeAccess.f19384.getObjectVolatile(buffer, offset);
    }
}
