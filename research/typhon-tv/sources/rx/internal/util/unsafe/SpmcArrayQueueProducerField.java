package rx.internal.util.unsafe;

/* compiled from: SpmcArrayQueue */
abstract class SpmcArrayQueueProducerField<E> extends SpmcArrayQueueL1Pad<E> {

    /* renamed from: 连任  reason: contains not printable characters */
    protected static final long f19365 = UnsafeAccess.m24997(SpmcArrayQueueProducerField.class, "producerIndex");
    private volatile long producerIndex;

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final long m24974() {
        return this.producerIndex;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final void m24975(long v) {
        UnsafeAccess.f19384.putOrderedLong(this, f19365, v);
    }

    public SpmcArrayQueueProducerField(int capacity) {
        super(capacity);
    }
}
