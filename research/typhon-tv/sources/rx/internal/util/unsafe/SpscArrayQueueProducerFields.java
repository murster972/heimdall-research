package rx.internal.util.unsafe;

/* compiled from: SpscArrayQueue */
abstract class SpscArrayQueueProducerFields<E> extends SpscArrayQueueL1Pad<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected static final long f19370 = UnsafeAccess.m24997(SpscArrayQueueProducerFields.class, "producerIndex");
    protected long producerIndex;

    public SpscArrayQueueProducerFields(int capacity) {
        super(capacity);
    }
}
