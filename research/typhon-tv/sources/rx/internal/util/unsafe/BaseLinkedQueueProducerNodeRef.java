package rx.internal.util.unsafe;

import rx.internal.util.atomic.LinkedQueueNode;

/* compiled from: BaseLinkedQueue */
abstract class BaseLinkedQueueProducerNodeRef<E> extends BaseLinkedQueuePad0<E> {

    /* renamed from: 靐  reason: contains not printable characters */
    protected static final long f19358 = UnsafeAccess.m24997(BaseLinkedQueueProducerNodeRef.class, "producerNode");
    protected LinkedQueueNode<E> producerNode;

    BaseLinkedQueueProducerNodeRef() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m24962(LinkedQueueNode<E> node) {
        this.producerNode = node;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final LinkedQueueNode<E> m24961() {
        return (LinkedQueueNode) UnsafeAccess.f19384.getObjectVolatile(this, f19358);
    }
}
