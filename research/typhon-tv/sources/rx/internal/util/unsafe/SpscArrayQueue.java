package rx.internal.util.unsafe;

public final class SpscArrayQueue<E> extends SpscArrayQueueL3Pad<E> {
    public SpscArrayQueue(int capacity) {
        super(capacity);
    }

    public boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException("null elements not allowed");
        }
        E[] lElementBuffer = this.f19363;
        long index = this.producerIndex;
        long offset = m24966(index);
        if (m24964(lElementBuffer, offset) != null) {
            return false;
        }
        m24965(lElementBuffer, offset, e);
        m24980(1 + index);
        return true;
    }

    public E poll() {
        long index = this.consumerIndex;
        long offset = m24966(index);
        E[] lElementBuffer = this.f19363;
        E e = m24964(lElementBuffer, offset);
        if (e == null) {
            return null;
        }
        m24965(lElementBuffer, offset, null);
        m24979(1 + index);
        return e;
    }

    public E peek() {
        return m24963(m24966(this.consumerIndex));
    }

    public int size() {
        long before;
        long currentProducerIndex;
        long after = m24978();
        do {
            before = after;
            currentProducerIndex = m24981();
            after = m24978();
        } while (before != after);
        return (int) (currentProducerIndex - after);
    }

    public boolean isEmpty() {
        return m24981() == m24978();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m24980(long v) {
        UnsafeAccess.f19384.putOrderedLong(this, f19370, v);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m24979(long v) {
        UnsafeAccess.f19384.putOrderedLong(this, f19369, v);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m24981() {
        return UnsafeAccess.f19384.getLongVolatile(this, f19370);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private long m24978() {
        return UnsafeAccess.f19384.getLongVolatile(this, f19369);
    }
}
