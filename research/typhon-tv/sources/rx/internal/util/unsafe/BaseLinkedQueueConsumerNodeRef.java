package rx.internal.util.unsafe;

import rx.internal.util.atomic.LinkedQueueNode;

/* compiled from: BaseLinkedQueue */
abstract class BaseLinkedQueueConsumerNodeRef<E> extends BaseLinkedQueuePad1<E> {

    /* renamed from: 龘  reason: contains not printable characters */
    protected static final long f19357 = UnsafeAccess.m24997(BaseLinkedQueueConsumerNodeRef.class, "consumerNode");
    protected LinkedQueueNode<E> consumerNode;

    BaseLinkedQueueConsumerNodeRef() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24960(LinkedQueueNode<E> node) {
        this.consumerNode = node;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final LinkedQueueNode<E> m24959() {
        return (LinkedQueueNode) UnsafeAccess.f19384.getObjectVolatile(this, f19357);
    }
}
