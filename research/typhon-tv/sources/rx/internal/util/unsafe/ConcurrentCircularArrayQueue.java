package rx.internal.util.unsafe;

import java.util.Iterator;

public abstract class ConcurrentCircularArrayQueue<E> extends ConcurrentCircularArrayQueueL0Pad<E> {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final int f19359;

    /* renamed from: 麤  reason: contains not printable characters */
    private static final long f19360 = ((long) (UnsafeAccess.f19384.arrayBaseOffset(Object[].class) + (32 << (f19359 - f19361))));

    /* renamed from: 龘  reason: contains not printable characters */
    protected static final int f19361 = Integer.getInteger("sparse.shift", 0).intValue();

    /* renamed from: 靐  reason: contains not printable characters */
    protected final long f19362;

    /* renamed from: 齉  reason: contains not printable characters */
    protected final E[] f19363;

    static {
        int scale = UnsafeAccess.f19384.arrayIndexScale(Object[].class);
        if (4 == scale) {
            f19359 = f19361 + 2;
        } else if (8 == scale) {
            f19359 = f19361 + 3;
        } else {
            throw new IllegalStateException("Unknown pointer size");
        }
    }

    public ConcurrentCircularArrayQueue(int capacity) {
        int actualCapacity = Pow2.m24971(capacity);
        this.f19362 = (long) (actualCapacity - 1);
        this.f19363 = (Object[]) new Object[((actualCapacity << f19361) + 64)];
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final long m24966(long index) {
        return m24967(index, this.f19362);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final long m24967(long index, long mask) {
        return f19360 + ((index & mask) << f19359);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24969(E[] buffer, long offset, E e) {
        UnsafeAccess.f19384.putObject(buffer, offset, e);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m24965(E[] buffer, long offset, E e) {
        UnsafeAccess.f19384.putOrderedObject(buffer, offset, e);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final E m24968(E[] buffer, long offset) {
        return UnsafeAccess.f19384.getObject(buffer, offset);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final E m24963(long offset) {
        return m24964(this.f19363, offset);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final E m24964(E[] buffer, long offset) {
        return UnsafeAccess.f19384.getObjectVolatile(buffer, offset);
    }

    public Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }
}
