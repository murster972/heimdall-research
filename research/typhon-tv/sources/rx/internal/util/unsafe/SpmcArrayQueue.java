package rx.internal.util.unsafe;

public final class SpmcArrayQueue<E> extends SpmcArrayQueueL3Pad<E> {
    public SpmcArrayQueue(int capacity) {
        super(capacity);
    }

    public boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException("Null is not a valid element");
        }
        E[] lb = this.f19363;
        long lMask = this.f19362;
        long currProducerIndex = m24974();
        long offset = m24966(currProducerIndex);
        if (m24964(lb, offset) == null) {
            m24969(lb, offset, e);
            m24975(1 + currProducerIndex);
        } else if (currProducerIndex - m24973() > lMask) {
            return false;
        } else {
            do {
            } while (m24964(lb, offset) != null);
        }
        m24969(lb, offset, e);
        m24975(1 + currProducerIndex);
        return true;
    }

    public E poll() {
        long currentConsumerIndex;
        long currProducerIndexCache = m24977();
        do {
            currentConsumerIndex = m24973();
            if (currentConsumerIndex >= currProducerIndexCache) {
                long currProducerIndex = m24974();
                if (currentConsumerIndex >= currProducerIndex) {
                    return null;
                }
                m24976(currProducerIndex);
            }
        } while (!m24972(currentConsumerIndex, 1 + currentConsumerIndex));
        long offset = m24966(currentConsumerIndex);
        E[] lb = this.f19363;
        E r6 = m24968(lb, offset);
        m24965(lb, offset, null);
        return r6;
    }

    public E peek() {
        E e;
        long currProducerIndexCache = m24977();
        do {
            long currentConsumerIndex = m24973();
            if (currentConsumerIndex >= currProducerIndexCache) {
                long currProducerIndex = m24974();
                if (currentConsumerIndex >= currProducerIndex) {
                    return null;
                }
                m24976(currProducerIndex);
            }
            e = m24963(m24966(currentConsumerIndex));
        } while (e == null);
        return e;
    }

    public int size() {
        long before;
        long currentProducerIndex;
        long after = m24973();
        do {
            before = after;
            currentProducerIndex = m24974();
            after = m24973();
        } while (before != after);
        return (int) (currentProducerIndex - after);
    }

    public boolean isEmpty() {
        return m24973() == m24974();
    }
}
