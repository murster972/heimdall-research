package rx.internal.util.unsafe;

import sun.misc.Unsafe;

public final class UnsafeAccess {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final boolean f19383;

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Unsafe f19384;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: sun.misc.Unsafe} */
    /* JADX WARNING: Multi-variable type inference failed */
    static {
        /*
            r3 = 1
            java.lang.String r4 = "rx.unsafe-disable"
            java.lang.String r4 = java.lang.System.getProperty(r4)
            if (r4 == 0) goto L_0x0026
        L_0x000a:
            f19383 = r3
            r2 = 0
            java.lang.Class<sun.misc.Unsafe> r3 = sun.misc.Unsafe.class
            java.lang.String r4 = "theUnsafe"
            java.lang.reflect.Field r1 = r3.getDeclaredField(r4)     // Catch:{ Throwable -> 0x0028 }
            r3 = 1
            r1.setAccessible(r3)     // Catch:{ Throwable -> 0x0028 }
            r3 = 0
            java.lang.Object r3 = r1.get(r3)     // Catch:{ Throwable -> 0x0028 }
            r0 = r3
            sun.misc.Unsafe r0 = (sun.misc.Unsafe) r0     // Catch:{ Throwable -> 0x0028 }
            r2 = r0
        L_0x0023:
            f19384 = r2
            return
        L_0x0026:
            r3 = 0
            goto L_0x000a
        L_0x0028:
            r3 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.internal.util.unsafe.UnsafeAccess.<clinit>():void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m24998() {
        return f19384 != null && !f19383;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static long m24997(Class<?> clazz, String fieldName) {
        try {
            return f19384.objectFieldOffset(clazz.getDeclaredField(fieldName));
        } catch (NoSuchFieldException ex) {
            InternalError ie = new InternalError();
            ie.initCause(ex);
            throw ie;
        }
    }
}
