package rx.internal.util.unsafe;

public final class Pow2 {
    /* renamed from: 龘  reason: contains not printable characters */
    public static int m24971(int value) {
        return 1 << (32 - Integer.numberOfLeadingZeros(value - 1));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m24970(int value) {
        return ((value + -1) & value) == 0;
    }
}
