package rx.internal.util.unsafe;

/* compiled from: SpmcArrayQueue */
abstract class SpmcArrayQueueProducerIndexCacheField<E> extends SpmcArrayQueueMidPad<E> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private volatile long f19366;

    public SpmcArrayQueueProducerIndexCacheField(int capacity) {
        super(capacity);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final long m24977() {
        return this.f19366;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final void m24976(long v) {
        this.f19366 = v;
    }
}
