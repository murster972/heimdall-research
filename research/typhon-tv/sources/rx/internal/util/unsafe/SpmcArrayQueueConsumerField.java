package rx.internal.util.unsafe;

/* compiled from: SpmcArrayQueue */
abstract class SpmcArrayQueueConsumerField<E> extends SpmcArrayQueueL2Pad<E> {

    /* renamed from: 麤  reason: contains not printable characters */
    protected static final long f19364 = UnsafeAccess.m24997(SpmcArrayQueueConsumerField.class, "consumerIndex");
    private volatile long consumerIndex;

    public SpmcArrayQueueConsumerField(int capacity) {
        super(capacity);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final long m24973() {
        return this.consumerIndex;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final boolean m24972(long expect, long newValue) {
        return UnsafeAccess.f19384.compareAndSwapLong(this, f19364, expect, newValue);
    }
}
