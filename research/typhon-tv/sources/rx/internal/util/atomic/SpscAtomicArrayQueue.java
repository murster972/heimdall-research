package rx.internal.util.atomic;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class SpscAtomicArrayQueue<E> extends AtomicReferenceArrayQueue<E> {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Integer f19342 = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096);

    /* renamed from: ʻ  reason: contains not printable characters */
    final int f19343;

    /* renamed from: 连任  reason: contains not printable characters */
    final AtomicLong f19344 = new AtomicLong();

    /* renamed from: 麤  reason: contains not printable characters */
    long f19345;

    /* renamed from: 齉  reason: contains not printable characters */
    final AtomicLong f19346 = new AtomicLong();

    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    public SpscAtomicArrayQueue(int capacity) {
        super(capacity);
        this.f19343 = Math.min(capacity / 4, f19342.intValue());
    }

    public boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException("Null is not a valid element");
        }
        AtomicReferenceArray<E> buffer = this.f19339;
        int mask = this.f19338;
        long index = this.f19346.get();
        int offset = m24928(index, mask);
        if (index >= this.f19345) {
            int step = this.f19343;
            if (m24930(buffer, m24928(((long) step) + index, mask)) == null) {
                this.f19345 = ((long) step) + index;
            } else if (m24930(buffer, offset) != null) {
                return false;
            }
        }
        m24931(buffer, offset, e);
        m24939(1 + index);
        return true;
    }

    public E poll() {
        long index = this.f19344.get();
        int offset = m24927(index);
        AtomicReferenceArray<E> lElementBuffer = this.f19339;
        E e = m24930(lElementBuffer, offset);
        if (e == null) {
            return null;
        }
        m24931(lElementBuffer, offset, null);
        m24940(1 + index);
        return e;
    }

    public E peek() {
        return m24929(m24927(this.f19344.get()));
    }

    public int size() {
        long before;
        long currentProducerIndex;
        long after = m24941();
        do {
            before = after;
            currentProducerIndex = m24938();
            after = m24941();
        } while (before != after);
        return (int) (currentProducerIndex - after);
    }

    public boolean isEmpty() {
        return m24938() == m24941();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m24939(long newIndex) {
        this.f19346.lazySet(newIndex);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m24940(long newIndex) {
        this.f19344.lazySet(newIndex);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m24941() {
        return this.f19344.get();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private long m24938() {
        return this.f19346.get();
    }
}
