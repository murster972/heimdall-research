package rx.internal.util.atomic;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReferenceArray;
import rx.internal.util.unsafe.Pow2;

public final class SpscUnboundedAtomicArrayQueue<T> implements Queue<T> {

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final Object f19347 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    static final int f19348 = Integer.getInteger("jctools.spsc.max.lookahead.step", 4096).intValue();

    /* renamed from: ʻ  reason: contains not printable characters */
    AtomicReferenceArray<Object> f19349;

    /* renamed from: ʼ  reason: contains not printable characters */
    int f19350;

    /* renamed from: ʽ  reason: contains not printable characters */
    AtomicReferenceArray<Object> f19351;

    /* renamed from: ˑ  reason: contains not printable characters */
    final AtomicLong f19352 = new AtomicLong();

    /* renamed from: 连任  reason: contains not printable characters */
    int f19353;

    /* renamed from: 靐  reason: contains not printable characters */
    final AtomicLong f19354 = new AtomicLong();

    /* renamed from: 麤  reason: contains not printable characters */
    long f19355;

    /* renamed from: 齉  reason: contains not printable characters */
    int f19356;

    public SpscUnboundedAtomicArrayQueue(int bufferSize) {
        int p2capacity = Pow2.m24971(Math.max(8, bufferSize));
        int mask = p2capacity - 1;
        AtomicReferenceArray<Object> buffer = new AtomicReferenceArray<>(p2capacity + 1);
        this.f19349 = buffer;
        this.f19353 = mask;
        m24953(p2capacity);
        this.f19351 = buffer;
        this.f19350 = mask;
        this.f19355 = (long) (mask - 1);
        m24954(0);
    }

    public boolean offer(T e) {
        if (e == null) {
            throw new NullPointerException();
        }
        AtomicReferenceArray<Object> buffer = this.f19349;
        long index = m24947();
        int mask = this.f19353;
        int offset = m24948(index, mask);
        if (index < this.f19355) {
            return m24958(buffer, e, index, offset);
        }
        int lookAheadStep = this.f19356;
        if (m24950(buffer, m24948(((long) lookAheadStep) + index, mask)) == null) {
            this.f19355 = (((long) lookAheadStep) + index) - 1;
            return m24958(buffer, e, index, offset);
        } else if (m24950(buffer, m24948(1 + index, mask)) != null) {
            return m24958(buffer, e, index, offset);
        } else {
            m24956(buffer, index, offset, e, (long) mask);
            return true;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m24958(AtomicReferenceArray<Object> buffer, T e, long index, int offset) {
        m24954(1 + index);
        m24955(buffer, offset, (Object) e);
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24956(AtomicReferenceArray<Object> oldBuffer, long currIndex, int offset, T e, long mask) {
        AtomicReferenceArray<Object> newBuffer = new AtomicReferenceArray<>(oldBuffer.length());
        this.f19349 = newBuffer;
        this.f19355 = (currIndex + mask) - 1;
        m24954(currIndex + 1);
        m24955(newBuffer, offset, (Object) e);
        m24957(oldBuffer, newBuffer);
        m24955(oldBuffer, offset, f19347);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24957(AtomicReferenceArray<Object> curr, AtomicReferenceArray<Object> next) {
        m24955(curr, m24942(curr.length() - 1), (Object) next);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private AtomicReferenceArray<Object> m24952(AtomicReferenceArray<Object> curr) {
        return (AtomicReferenceArray) m24950(curr, m24942(curr.length() - 1));
    }

    public T poll() {
        AtomicReferenceArray<Object> buffer = this.f19351;
        long index = m24946();
        int mask = this.f19350;
        int offset = m24948(index, mask);
        Object e = m24950(buffer, offset);
        boolean isNextBuffer = e == f19347;
        if (e != null && !isNextBuffer) {
            m24945(1 + index);
            m24955(buffer, offset, (Object) null);
            return e;
        } else if (isNextBuffer) {
            return m24951(m24952(buffer), index, mask);
        } else {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private T m24951(AtomicReferenceArray<Object> nextBuffer, long index, int mask) {
        this.f19351 = nextBuffer;
        int offsetInNew = m24948(index, mask);
        T n = m24950(nextBuffer, offsetInNew);
        if (n == null) {
            return null;
        }
        m24945(1 + index);
        m24955(nextBuffer, offsetInNew, (Object) null);
        return n;
    }

    public T peek() {
        AtomicReferenceArray<Object> buffer = this.f19351;
        long index = m24946();
        int mask = this.f19350;
        Object e = m24950(buffer, m24948(index, mask));
        if (e == f19347) {
            return m24944(m24952(buffer), index, mask);
        }
        return e;
    }

    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private T m24944(AtomicReferenceArray<Object> nextBuffer, long index, int mask) {
        this.f19351 = nextBuffer;
        return m24950(nextBuffer, m24948(index, mask));
    }

    public int size() {
        long before;
        long currentProducerIndex;
        long after = m24943();
        do {
            before = after;
            currentProducerIndex = m24949();
            after = m24943();
        } while (before != after);
        return (int) (currentProducerIndex - after);
    }

    public boolean isEmpty() {
        return m24949() == m24943();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24953(int capacity) {
        this.f19356 = Math.min(capacity / 4, f19348);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m24949() {
        return this.f19354.get();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private long m24943() {
        return this.f19352.get();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private long m24947() {
        return this.f19354.get();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private long m24946() {
        return this.f19352.get();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24954(long v) {
        this.f19354.lazySet(v);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m24945(long v) {
        this.f19352.lazySet(v);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m24948(long index, int mask) {
        return m24942(((int) index) & mask);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static int m24942(int index) {
        return index;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m24955(AtomicReferenceArray<Object> buffer, int offset, Object e) {
        buffer.lazySet(offset, e);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <E> Object m24950(AtomicReferenceArray<Object> buffer, int offset) {
        return buffer.get(offset);
    }

    public Iterator<T> iterator() {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object o) {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public <E> E[] toArray(E[] eArr) {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    public boolean containsAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public boolean add(T t) {
        throw new UnsupportedOperationException();
    }

    public T remove() {
        throw new UnsupportedOperationException();
    }

    public T element() {
        throw new UnsupportedOperationException();
    }
}
