package rx.internal.util.atomic;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReferenceArray;
import rx.internal.util.unsafe.Pow2;

abstract class AtomicReferenceArrayQueue<E> extends AbstractQueue<E> {

    /* renamed from: 靐  reason: contains not printable characters */
    protected final int f19338;

    /* renamed from: 龘  reason: contains not printable characters */
    protected final AtomicReferenceArray<E> f19339;

    public AtomicReferenceArrayQueue(int capacity) {
        int actualCapacity = Pow2.m24971(capacity);
        this.f19338 = actualCapacity - 1;
        this.f19339 = new AtomicReferenceArray<>(actualCapacity);
    }

    public Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        while (true) {
            if (poll() == null && isEmpty()) {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m24928(long index, int mask) {
        return ((int) index) & mask;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final int m24927(long index) {
        return ((int) index) & this.f19338;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final E m24930(AtomicReferenceArray<E> buffer, int offset) {
        return buffer.get(offset);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24931(AtomicReferenceArray<E> buffer, int offset, E value) {
        buffer.lazySet(offset, value);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final E m24929(int offset) {
        return m24930(this.f19339, offset);
    }
}
