package rx.internal.util.atomic;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

abstract class BaseLinkedAtomicQueue<E> extends AbstractQueue<E> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final AtomicReference<LinkedQueueNode<E>> f19340 = new AtomicReference<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private final AtomicReference<LinkedQueueNode<E>> f19341 = new AtomicReference<>();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final LinkedQueueNode<E> m24936() {
        return this.f19341.get();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final LinkedQueueNode<E> m24932() {
        return this.f19341.get();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24937(LinkedQueueNode<E> node) {
        this.f19341.lazySet(node);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public final LinkedQueueNode<E> m24935() {
        return this.f19340.get();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public final LinkedQueueNode<E> m24934() {
        return this.f19340.get();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final void m24933(LinkedQueueNode<E> node) {
        this.f19340.lazySet(node);
    }

    public final Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }

    public final int size() {
        LinkedQueueNode<E> next;
        LinkedQueueNode<E> chaserNode = m24935();
        LinkedQueueNode<E> producerNode = m24936();
        int size = 0;
        while (chaserNode != producerNode && size < Integer.MAX_VALUE) {
            do {
                next = chaserNode.lvNext();
            } while (next == null);
            chaserNode = next;
            size++;
        }
        return size;
    }

    public final boolean isEmpty() {
        return m24935() == m24936();
    }
}
