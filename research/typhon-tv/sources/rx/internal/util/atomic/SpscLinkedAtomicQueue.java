package rx.internal.util.atomic;

public final class SpscLinkedAtomicQueue<E> extends BaseLinkedAtomicQueue<E> {
    public SpscLinkedAtomicQueue() {
        LinkedQueueNode<E> node = new LinkedQueueNode<>();
        m24937(node);
        m24933(node);
        node.soNext((LinkedQueueNode<E>) null);
    }

    public boolean offer(E nextValue) {
        if (nextValue == null) {
            throw new NullPointerException("null elements not allowed");
        }
        LinkedQueueNode<E> nextNode = new LinkedQueueNode<>(nextValue);
        m24932().soNext(nextNode);
        m24937(nextNode);
        return true;
    }

    public E poll() {
        LinkedQueueNode<E> nextNode = m24934().lvNext();
        if (nextNode == null) {
            return null;
        }
        E nextValue = nextNode.getAndNullValue();
        m24933(nextNode);
        return nextValue;
    }

    public E peek() {
        LinkedQueueNode<E> nextNode = m24934().lvNext();
        if (nextNode != null) {
            return nextNode.lpValue();
        }
        return null;
    }
}
