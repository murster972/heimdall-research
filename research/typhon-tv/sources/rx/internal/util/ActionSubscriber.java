package rx.internal.util;

import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;

public final class ActionSubscriber<T> extends Subscriber<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Action1<Throwable> f19276;

    /* renamed from: 齉  reason: contains not printable characters */
    final Action0 f19277;

    /* renamed from: 龘  reason: contains not printable characters */
    final Action1<? super T> f19278;

    public ActionSubscriber(Action1<? super T> onNext, Action1<Throwable> onError, Action0 onCompleted) {
        this.f19278 = onNext;
        this.f19276 = onError;
        this.f19277 = onCompleted;
    }

    public void onNext(T t) {
        this.f19278.call(t);
    }

    public void onError(Throwable e) {
        this.f19276.call(e);
    }

    public void onCompleted() {
        this.f19277.m24539();
    }
}
