package rx.internal.util;

import rx.Observer;
import rx.functions.Action0;
import rx.functions.Action1;

public final class ActionObserver<T> implements Observer<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final Action1<? super Throwable> f19273;

    /* renamed from: 齉  reason: contains not printable characters */
    final Action0 f19274;

    /* renamed from: 龘  reason: contains not printable characters */
    final Action1<? super T> f19275;

    public ActionObserver(Action1<? super T> onNext, Action1<? super Throwable> onError, Action0 onCompleted) {
        this.f19275 = onNext;
        this.f19273 = onError;
        this.f19274 = onCompleted;
    }

    public void onNext(T t) {
        this.f19275.call(t);
    }

    public void onError(Throwable e) {
        this.f19273.call(e);
    }

    public void onCompleted() {
        this.f19274.m24539();
    }
}
