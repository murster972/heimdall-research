package rx.internal.util;

import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.Notification;
import rx.Observable;
import rx.Scheduler;
import rx.exceptions.OnErrorNotImplementedException;
import rx.functions.Action1;
import rx.functions.Action2;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.internal.operators.OperatorAny;
import rx.observables.ConnectableObservable;

public enum InternalObservableUtils {
    ;
    
    public static final PlusOneFunc2 COUNTER = null;
    public static final Action1<Throwable> ERROR_NOT_IMPLEMENTED = null;
    public static final Observable.Operator<Boolean, Object> IS_EMPTY = null;
    public static final PlusOneLongFunc2 LONG_COUNTER = null;
    public static final ObjectEqualsFunc2 OBJECT_EQUALS = null;
    public static final ToArrayFunc1 TO_ARRAY = null;

    /* renamed from: 靐  reason: contains not printable characters */
    static final NotificationErrorExtractor f19281 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReturnsVoidFunc1 f19283 = null;

    static {
        LONG_COUNTER = new PlusOneLongFunc2();
        OBJECT_EQUALS = new ObjectEqualsFunc2();
        TO_ARRAY = new ToArrayFunc1();
        f19283 = new ReturnsVoidFunc1();
        COUNTER = new PlusOneFunc2();
        f19281 = new NotificationErrorExtractor();
        ERROR_NOT_IMPLEMENTED = new ErrorNotImplementedAction();
        IS_EMPTY = new OperatorAny(UtilityFunctions.m24925(), true);
    }

    static final class PlusOneFunc2 implements Func2<Integer, Object, Integer> {
        PlusOneFunc2() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Integer call(Integer count, Object o) {
            return Integer.valueOf(count.intValue() + 1);
        }
    }

    static final class PlusOneLongFunc2 implements Func2<Long, Object, Long> {
        PlusOneLongFunc2() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Long call(Long count, Object o) {
            return Long.valueOf(count.longValue() + 1);
        }
    }

    static final class ObjectEqualsFunc2 implements Func2<Object, Object, Boolean> {
        ObjectEqualsFunc2() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Boolean call(Object first, Object second) {
            return Boolean.valueOf(first == second || (first != null && first.equals(second)));
        }
    }

    static final class ToArrayFunc1 implements Func1<List<? extends Observable<?>>, Observable<?>[]> {
        ToArrayFunc1() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Observable<?>[] call(List<? extends Observable<?>> o) {
            return (Observable[]) o.toArray(new Observable[o.size()]);
        }
    }

    public static Func1<Object, Boolean> equalsWith(Object other) {
        return new EqualsWithFunc1(other);
    }

    static final class EqualsWithFunc1 implements Func1<Object, Boolean> {

        /* renamed from: 龘  reason: contains not printable characters */
        final Object f19285;

        public EqualsWithFunc1(Object other) {
            this.f19285 = other;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Boolean call(Object t) {
            return Boolean.valueOf(t == this.f19285 || (t != null && t.equals(this.f19285)));
        }
    }

    public static Func1<Object, Boolean> isInstanceOf(Class<?> clazz) {
        return new IsInstanceOfFunc1(clazz);
    }

    static final class IsInstanceOfFunc1 implements Func1<Object, Boolean> {

        /* renamed from: 龘  reason: contains not printable characters */
        final Class<?> f19286;

        public IsInstanceOfFunc1(Class<?> other) {
            this.f19286 = other;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Boolean call(Object t) {
            return Boolean.valueOf(this.f19286.isInstance(t));
        }
    }

    public static Func1<Observable<? extends Notification<?>>, Observable<?>> createRepeatDematerializer(Func1<? super Observable<? extends Void>, ? extends Observable<?>> notificationHandler) {
        return new RepeatNotificationDematerializer(notificationHandler);
    }

    static final class RepeatNotificationDematerializer implements Func1<Observable<? extends Notification<?>>, Observable<?>> {

        /* renamed from: 龘  reason: contains not printable characters */
        final Func1<? super Observable<? extends Void>, ? extends Observable<?>> f19287;

        public RepeatNotificationDematerializer(Func1<? super Observable<? extends Void>, ? extends Observable<?>> notificationHandler) {
            this.f19287 = notificationHandler;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Observable<?> call(Observable<? extends Notification<?>> notifications) {
            return (Observable) this.f19287.call(notifications.m7392((Func1<? super Object, ? extends R>) InternalObservableUtils.f19283));
        }
    }

    static final class ReturnsVoidFunc1 implements Func1<Object, Void> {
        ReturnsVoidFunc1() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Void call(Object t) {
            return null;
        }
    }

    public static <T, R> Func1<Observable<T>, Observable<R>> createReplaySelectorAndObserveOn(Func1<? super Observable<T>, ? extends Observable<R>> selector, Scheduler scheduler) {
        return new SelectorAndObserveOn(selector, scheduler);
    }

    static final class SelectorAndObserveOn<T, R> implements Func1<Observable<T>, Observable<R>> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Scheduler f19301;

        /* renamed from: 龘  reason: contains not printable characters */
        final Func1<? super Observable<T>, ? extends Observable<R>> f19302;

        public SelectorAndObserveOn(Func1<? super Observable<T>, ? extends Observable<R>> selector, Scheduler scheduler) {
            this.f19302 = selector;
            this.f19301 = scheduler;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Observable<R> call(Observable<T> t) {
            return ((Observable) this.f19302.call(t)).m7407(this.f19301);
        }
    }

    public static Func1<Observable<? extends Notification<?>>, Observable<?>> createRetryDematerializer(Func1<? super Observable<? extends Throwable>, ? extends Observable<?>> notificationHandler) {
        return new RetryNotificationDematerializer(notificationHandler);
    }

    static final class RetryNotificationDematerializer implements Func1<Observable<? extends Notification<?>>, Observable<?>> {

        /* renamed from: 龘  reason: contains not printable characters */
        final Func1<? super Observable<? extends Throwable>, ? extends Observable<?>> f19300;

        public RetryNotificationDematerializer(Func1<? super Observable<? extends Throwable>, ? extends Observable<?>> notificationHandler) {
            this.f19300 = notificationHandler;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Observable<?> call(Observable<? extends Notification<?>> notifications) {
            return (Observable) this.f19300.call(notifications.m7392((Func1<? super Object, ? extends R>) InternalObservableUtils.f19281));
        }
    }

    static final class NotificationErrorExtractor implements Func1<Notification<?>, Throwable> {
        NotificationErrorExtractor() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Throwable call(Notification<?> t) {
            return t.m24499();
        }
    }

    public static <T> Func0<ConnectableObservable<T>> createReplaySupplier(Observable<T> source) {
        return new ReplaySupplierNoParams(source);
    }

    static final class ReplaySupplierNoParams<T> implements Func0<ConnectableObservable<T>> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Observable<T> f19294;

        ReplaySupplierNoParams(Observable<T> source) {
            this.f19294 = source;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ConnectableObservable<T> call() {
            return this.f19294.m7370();
        }
    }

    public static <T> Func0<ConnectableObservable<T>> createReplaySupplier(Observable<T> source, int bufferSize) {
        return new ReplaySupplierBuffer(source, bufferSize);
    }

    static final class ReplaySupplierBuffer<T> implements Func0<ConnectableObservable<T>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f19288;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Observable<T> f19289;

        ReplaySupplierBuffer(Observable<T> source, int bufferSize) {
            this.f19289 = source;
            this.f19288 = bufferSize;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ConnectableObservable<T> call() {
            return this.f19289.m7387(this.f19288);
        }
    }

    public static <T> Func0<ConnectableObservable<T>> createReplaySupplier(Observable<T> source, long time, TimeUnit unit, Scheduler scheduler) {
        return new ReplaySupplierBufferTime(source, time, unit, scheduler);
    }

    static final class ReplaySupplierBufferTime<T> implements Func0<ConnectableObservable<T>> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Observable<T> f19290;

        /* renamed from: 麤  reason: contains not printable characters */
        private final Scheduler f19291;

        /* renamed from: 齉  reason: contains not printable characters */
        private final long f19292;

        /* renamed from: 龘  reason: contains not printable characters */
        private final TimeUnit f19293;

        ReplaySupplierBufferTime(Observable<T> source, long time, TimeUnit unit, Scheduler scheduler) {
            this.f19293 = unit;
            this.f19290 = source;
            this.f19292 = time;
            this.f19291 = scheduler;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ConnectableObservable<T> call() {
            return this.f19290.m7398(this.f19292, this.f19293, this.f19291);
        }
    }

    public static <T> Func0<ConnectableObservable<T>> createReplaySupplier(Observable<T> source, int bufferSize, long time, TimeUnit unit, Scheduler scheduler) {
        return new ReplaySupplierTime(source, bufferSize, time, unit, scheduler);
    }

    static final class ReplaySupplierTime<T> implements Func0<ConnectableObservable<T>> {

        /* renamed from: 连任  reason: contains not printable characters */
        private final Observable<T> f19295;

        /* renamed from: 靐  reason: contains not printable characters */
        private final TimeUnit f19296;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f19297;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Scheduler f19298;

        /* renamed from: 龘  reason: contains not printable characters */
        private final long f19299;

        ReplaySupplierTime(Observable<T> source, int bufferSize, long time, TimeUnit unit, Scheduler scheduler) {
            this.f19299 = time;
            this.f19296 = unit;
            this.f19298 = scheduler;
            this.f19297 = bufferSize;
            this.f19295 = source;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public ConnectableObservable<T> call() {
            return this.f19295.m7418(this.f19297, this.f19299, this.f19296, this.f19298);
        }
    }

    public static <T, R> Func2<R, T, R> createCollectorCaller(Action2<R, ? super T> collector) {
        return new CollectorCaller(collector);
    }

    static final class CollectorCaller<T, R> implements Func2<R, T, R> {

        /* renamed from: 龘  reason: contains not printable characters */
        final Action2<R, ? super T> f19284;

        public CollectorCaller(Action2<R, ? super T> collector) {
            this.f19284 = collector;
        }

        public R call(R state, T value) {
            this.f19284.m24540(state, value);
            return state;
        }
    }

    static final class ErrorNotImplementedAction implements Action1<Throwable> {
        ErrorNotImplementedAction() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void call(Throwable t) {
            throw new OnErrorNotImplementedException(t);
        }
    }
}
