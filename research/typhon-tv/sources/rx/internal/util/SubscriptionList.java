package rx.internal.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import rx.Subscription;
import rx.exceptions.Exceptions;

public final class SubscriptionList implements Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile boolean f19332;

    /* renamed from: 龘  reason: contains not printable characters */
    private List<Subscription> f19333;

    public SubscriptionList() {
    }

    public SubscriptionList(Subscription... subscriptions) {
        this.f19333 = new LinkedList(Arrays.asList(subscriptions));
    }

    public SubscriptionList(Subscription s) {
        this.f19333 = new LinkedList();
        this.f19333.add(s);
    }

    public boolean isUnsubscribed() {
        return this.f19332;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24923(Subscription s) {
        if (!s.isUnsubscribed()) {
            if (!this.f19332) {
                synchronized (this) {
                    if (!this.f19332) {
                        List<Subscription> subs = this.f19333;
                        if (subs == null) {
                            subs = new LinkedList<>();
                            this.f19333 = subs;
                        }
                        subs.add(s);
                        return;
                    }
                }
            }
            s.unsubscribe();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0014, code lost:
        if (r1 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0016, code lost:
        r4.unsubscribe();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m24922(rx.Subscription r4) {
        /*
            r3 = this;
            boolean r2 = r3.f19332
            if (r2 != 0) goto L_0x000e
            monitor-enter(r3)
            java.util.List<rx.Subscription> r0 = r3.f19333     // Catch:{ all -> 0x001a }
            boolean r2 = r3.f19332     // Catch:{ all -> 0x001a }
            if (r2 != 0) goto L_0x000d
            if (r0 != 0) goto L_0x000f
        L_0x000d:
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
        L_0x000e:
            return
        L_0x000f:
            boolean r1 = r0.remove(r4)     // Catch:{ all -> 0x001a }
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            if (r1 == 0) goto L_0x000e
            r4.unsubscribe()
            goto L_0x000e
        L_0x001a:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x001a }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.internal.util.SubscriptionList.m24922(rx.Subscription):void");
    }

    public void unsubscribe() {
        if (!this.f19332) {
            synchronized (this) {
                if (!this.f19332) {
                    this.f19332 = true;
                    List<Subscription> list = this.f19333;
                    this.f19333 = null;
                    m24921((Collection<Subscription>) list);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m24921(Collection<Subscription> subscriptions) {
        if (subscriptions != null) {
            List<Throwable> es = null;
            for (Subscription s : subscriptions) {
                try {
                    s.unsubscribe();
                } catch (Throwable e) {
                    if (es == null) {
                        es = new ArrayList<>();
                    }
                    es.add(e);
                }
            }
            Exceptions.m24536((List<? extends Throwable>) es);
        }
    }
}
