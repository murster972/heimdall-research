package rx.internal.util;

import rx.functions.Func1;

public final class UtilityFunctions {
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Func1<? super T, Boolean> m24925() {
        return AlwaysTrue.INSTANCE;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> Func1<T, T> m24924() {
        return Identity.INSTANCE;
    }

    enum AlwaysTrue implements Func1<Object, Boolean> {
        INSTANCE;

        /* renamed from: 龘  reason: contains not printable characters */
        public Boolean call(Object o) {
            return true;
        }
    }

    enum Identity implements Func1<Object, Object> {
        INSTANCE;

        public Object call(Object o) {
            return o;
        }
    }
}
