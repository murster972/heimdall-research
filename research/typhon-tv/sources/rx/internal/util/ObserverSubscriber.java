package rx.internal.util;

import rx.Observer;
import rx.Subscriber;

public final class ObserverSubscriber<T> extends Subscriber<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final Observer<? super T> f19303;

    public ObserverSubscriber(Observer<? super T> observer) {
        this.f19303 = observer;
    }

    public void onNext(T t) {
        this.f19303.onNext(t);
    }

    public void onError(Throwable e) {
        this.f19303.onError(e);
    }

    public void onCompleted() {
        this.f19303.onCompleted();
    }
}
