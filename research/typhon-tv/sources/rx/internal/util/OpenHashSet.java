package rx.internal.util;

import rx.internal.util.unsafe.Pow2;

public final class OpenHashSet<T> {

    /* renamed from: 连任  reason: contains not printable characters */
    T[] f19304;

    /* renamed from: 靐  reason: contains not printable characters */
    int f19305;

    /* renamed from: 麤  reason: contains not printable characters */
    int f19306;

    /* renamed from: 齉  reason: contains not printable characters */
    int f19307;

    /* renamed from: 龘  reason: contains not printable characters */
    final float f19308;

    public OpenHashSet() {
        this(16, 0.75f);
    }

    public OpenHashSet(int capacity, float loadFactor) {
        this.f19308 = loadFactor;
        int c = Pow2.m24971(capacity);
        this.f19305 = c - 1;
        this.f19306 = (int) (((float) c) * loadFactor);
        this.f19304 = (Object[]) new Object[c];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24895(T value) {
        T curr;
        T[] a = this.f19304;
        int m = this.f19305;
        int pos = m24888(value.hashCode()) & m;
        T curr2 = a[pos];
        if (curr2 != null) {
            if (curr2.equals(value)) {
                return false;
            }
            do {
                pos = (pos + 1) & m;
                curr = a[pos];
                if (curr == null) {
                }
            } while (!curr.equals(value));
            return false;
        }
        a[pos] = value;
        int i = this.f19307 + 1;
        this.f19307 = i;
        if (i >= this.f19306) {
            m24889();
        }
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m24890(T value) {
        T curr;
        T[] a = this.f19304;
        int m = this.f19305;
        int pos = m24888(value.hashCode()) & m;
        T curr2 = a[pos];
        if (curr2 == null) {
            return false;
        }
        if (curr2.equals(value)) {
            return m24894(pos, a, m);
        }
        do {
            pos = (pos + 1) & m;
            curr = a[pos];
            if (curr == null) {
                return false;
            }
        } while (!curr.equals(value));
        return m24894(pos, a, m);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24894(int pos, T[] a, int m) {
        T curr;
        this.f19307--;
        while (true) {
            int last = pos;
            while (true) {
                pos = (pos + 1) & m;
                curr = a[pos];
                if (curr != null) {
                    int slot = m24888(curr.hashCode()) & m;
                    if (last > pos) {
                        if (last >= slot && slot > pos) {
                            break;
                        }
                    } else if (last >= slot || slot > pos) {
                        break;
                    }
                } else {
                    a[last] = null;
                    return true;
                }
            }
            a[last] = curr;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24893() {
        this.f19307 = 0;
        this.f19304 = (Object[]) new Object[0];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m24889() {
        T[] a = this.f19304;
        int i = a.length;
        int newCap = i << 1;
        int m = newCap - 1;
        T[] b = (Object[]) new Object[newCap];
        int j = this.f19307;
        while (true) {
            int j2 = j;
            j = j2 - 1;
            if (j2 != 0) {
                do {
                    i--;
                } while (a[i] == null);
                int pos = m24888(a[i].hashCode()) & m;
                if (b[pos] != null) {
                    do {
                        pos = (pos + 1) & m;
                    } while (b[pos] != null);
                }
                b[pos] = a[i];
            } else {
                this.f19305 = m;
                this.f19306 = (int) (((float) newCap) * this.f19308);
                this.f19304 = b;
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m24888(int x) {
        int h = x * -1640531527;
        return (h >>> 16) ^ h;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m24892() {
        return this.f19307 == 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public T[] m24891() {
        return this.f19304;
    }
}
