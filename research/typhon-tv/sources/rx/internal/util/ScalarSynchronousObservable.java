package rx.internal.util;

import java.util.concurrent.atomic.AtomicBoolean;
import rx.Observable;
import rx.Producer;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.internal.producers.SingleProducer;
import rx.internal.schedulers.EventLoopsScheduler;
import rx.observers.Subscribers;
import rx.plugins.RxJavaHooks;

public final class ScalarSynchronousObservable<T> extends Observable<T> {

    /* renamed from: 齉  reason: contains not printable characters */
    static final boolean f19315 = Boolean.valueOf(System.getProperty("rx.just.strong-mode", "false")).booleanValue();

    /* renamed from: 靐  reason: contains not printable characters */
    final T f19316;

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> Producer m24910(Subscriber<? super T> s, T v) {
        if (f19315) {
            return new SingleProducer(s, v);
        }
        return new WeakSingleProducer(s, v);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static <T> ScalarSynchronousObservable<T> m24909(T t) {
        return new ScalarSynchronousObservable<>(t);
    }

    protected ScalarSynchronousObservable(T t) {
        super(RxJavaHooks.m25016(new JustOnSubscribe(t)));
        this.f19316 = t;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public T m24912() {
        return this.f19316;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Observable<T> m24913(final Scheduler scheduler) {
        Func1<Action0, Subscription> onSchedule;
        if (scheduler instanceof EventLoopsScheduler) {
            final EventLoopsScheduler els = (EventLoopsScheduler) scheduler;
            onSchedule = new Func1<Action0, Subscription>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Subscription call(Action0 a) {
                    return els.m24815(a);
                }
            };
        } else {
            onSchedule = new Func1<Action0, Subscription>() {
                /* renamed from: 龘  reason: contains not printable characters */
                public Subscription call(final Action0 a) {
                    final Scheduler.Worker w = scheduler.createWorker();
                    w.m24503(new Action0() {
                        /* renamed from: 龘  reason: contains not printable characters */
                        public void m24916() {
                            try {
                                a.m24539();
                            } finally {
                                w.unsubscribe();
                            }
                        }
                    });
                    return w;
                }
            };
        }
        return m7346(new ScalarAsyncOnSubscribe(this.f19316, onSchedule));
    }

    static final class JustOnSubscribe<T> implements Observable.OnSubscribe<T> {

        /* renamed from: 龘  reason: contains not printable characters */
        final T f19326;

        JustOnSubscribe(T value) {
            this.f19326 = value;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void call(Subscriber<? super T> s) {
            s.m24512(ScalarSynchronousObservable.m24910(s, this.f19326));
        }
    }

    static final class ScalarAsyncOnSubscribe<T> implements Observable.OnSubscribe<T> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Func1<Action0, Subscription> f19327;

        /* renamed from: 龘  reason: contains not printable characters */
        final T f19328;

        ScalarAsyncOnSubscribe(T value, Func1<Action0, Subscription> onSchedule) {
            this.f19328 = value;
            this.f19327 = onSchedule;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void call(Subscriber<? super T> s) {
            s.m24512((Producer) new ScalarAsyncProducer(s, this.f19328, this.f19327));
        }
    }

    static final class ScalarAsyncProducer<T> extends AtomicBoolean implements Producer, Action0 {
        private static final long serialVersionUID = -2466317989629281651L;
        final Subscriber<? super T> actual;
        final Func1<Action0, Subscription> onSchedule;
        final T value;

        public ScalarAsyncProducer(Subscriber<? super T> actual2, T value2, Func1<Action0, Subscription> onSchedule2) {
            this.actual = actual2;
            this.value = value2;
            this.onSchedule = onSchedule2;
        }

        public void request(long n) {
            if (n < 0) {
                throw new IllegalArgumentException("n >= 0 required but it was " + n);
            } else if (n != 0 && compareAndSet(false, true)) {
                this.actual.m24513(this.onSchedule.call(this));
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24920() {
            Subscriber<? super T> a = this.actual;
            if (!a.isUnsubscribed()) {
                T v = this.value;
                try {
                    a.onNext(v);
                    if (!a.isUnsubscribed()) {
                        a.onCompleted();
                    }
                } catch (Throwable e) {
                    Exceptions.m24534(e, a, v);
                }
            }
        }

        public String toString() {
            return "ScalarAsyncProducer[" + this.value + ", " + get() + "]";
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public <R> Observable<R> m24911(final Func1<? super T, ? extends Observable<? extends R>> func) {
        return m7346(new Observable.OnSubscribe<R>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super R> child) {
                Observable<? extends R> o = (Observable) func.call(ScalarSynchronousObservable.this.f19316);
                if (o instanceof ScalarSynchronousObservable) {
                    child.m24512(ScalarSynchronousObservable.m24910(child, ((ScalarSynchronousObservable) o).f19316));
                } else {
                    o.m7416((Subscriber<? super Object>) Subscribers.m25005(child));
                }
            }
        });
    }

    static final class WeakSingleProducer<T> implements Producer {

        /* renamed from: 靐  reason: contains not printable characters */
        final T f19329;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f19330;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f19331;

        public WeakSingleProducer(Subscriber<? super T> actual, T value) {
            this.f19331 = actual;
            this.f19329 = value;
        }

        public void request(long n) {
            if (!this.f19330) {
                if (n < 0) {
                    throw new IllegalStateException("n >= required but it was " + n);
                } else if (n != 0) {
                    this.f19330 = true;
                    Subscriber<? super T> a = this.f19331;
                    if (!a.isUnsubscribed()) {
                        T v = this.f19329;
                        try {
                            a.onNext(v);
                            if (!a.isUnsubscribed()) {
                                a.onCompleted();
                            }
                        } catch (Throwable e) {
                            Exceptions.m24534(e, a, v);
                        }
                    }
                }
            }
        }
    }
}
