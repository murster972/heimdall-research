package rx.internal.util;

import java.util.Queue;
import rx.Subscription;
import rx.exceptions.MissingBackpressureException;
import rx.internal.operators.NotificationLite;
import rx.internal.util.atomic.SpscAtomicArrayQueue;
import rx.internal.util.unsafe.SpmcArrayQueue;
import rx.internal.util.unsafe.SpscArrayQueue;
import rx.internal.util.unsafe.UnsafeAccess;

public class RxRingBuffer implements Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final int f19311;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f19312;

    /* renamed from: 齉  reason: contains not printable characters */
    private Queue<Object> f19313;

    /* renamed from: 龘  reason: contains not printable characters */
    public volatile Object f19314;

    static {
        int defaultSize = 128;
        if (PlatformDependent.m24898()) {
            defaultSize = 16;
        }
        String sizeFromProperty = System.getProperty("rx.ring-buffer.size");
        if (sizeFromProperty != null) {
            try {
                defaultSize = Integer.parseInt(sizeFromProperty);
            } catch (NumberFormatException e) {
                System.err.println("Failed to set 'rx.buffer.size' with value " + sizeFromProperty + " => " + e.getMessage());
            }
        }
        f19311 = defaultSize;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static RxRingBuffer m24900() {
        if (UnsafeAccess.m24998()) {
            return new RxRingBuffer(false, f19311);
        }
        return new RxRingBuffer();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static RxRingBuffer m24899() {
        if (UnsafeAccess.m24998()) {
            return new RxRingBuffer(true, f19311);
        }
        return new RxRingBuffer();
    }

    private RxRingBuffer(Queue<Object> queue, int size) {
        this.f19313 = queue;
        this.f19312 = size;
    }

    private RxRingBuffer(boolean spmc, int size) {
        this.f19313 = spmc ? new SpmcArrayQueue<>(size) : new SpscArrayQueue<>(size);
        this.f19312 = size;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public synchronized void m24907() {
    }

    public void unsubscribe() {
        m24907();
    }

    RxRingBuffer() {
        this((Queue<Object>) new SpscAtomicArrayQueue(f19311), f19311);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24908(Object o) throws MissingBackpressureException {
        boolean iae = false;
        boolean mbe = false;
        synchronized (this) {
            Queue<Object> q = this.f19313;
            if (q != null) {
                mbe = !q.offer(NotificationLite.m24567(o));
            } else {
                iae = true;
            }
        }
        if (iae) {
            throw new IllegalStateException("This instance has been unsubscribed and the queue is no longer usable.");
        } else if (mbe) {
            throw new MissingBackpressureException();
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m24905() {
        if (this.f19314 == null) {
            this.f19314 = NotificationLite.m24566();
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m24903() {
        Queue<Object> q = this.f19313;
        return q == null || q.isEmpty();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m24901() {
        Object o = null;
        synchronized (this) {
            Queue<Object> q = this.f19313;
            if (q != null) {
                o = q.poll();
                Object ts = this.f19314;
                if (o == null && ts != null && q.peek() == null) {
                    o = ts;
                    this.f19314 = null;
                }
            }
        }
        return o;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Object m24902() {
        Object o;
        synchronized (this) {
            Queue<Object> q = this.f19313;
            if (q == null) {
                o = null;
            } else {
                o = q.peek();
                Object ts = this.f19314;
                if (o == null && ts != null && q.peek() == null) {
                    o = ts;
                }
            }
        }
        return o;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m24904(Object o) {
        return NotificationLite.m24563(o);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public Object m24906(Object o) {
        return NotificationLite.m24564(o);
    }

    public boolean isUnsubscribed() {
        return this.f19313 == null;
    }
}
