package rx.internal.schedulers;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.internal.util.RxThreadFactory;
import rx.internal.util.SubscriptionList;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

public final class EventLoopsScheduler extends Scheduler implements SchedulerLifecycle {

    /* renamed from: 靐  reason: contains not printable characters */
    static final PoolWorker f19184 = new PoolWorker(RxThreadFactory.NONE);

    /* renamed from: 齉  reason: contains not printable characters */
    static final FixedSchedulerPool f19185 = new FixedSchedulerPool((ThreadFactory) null, 0);

    /* renamed from: 龘  reason: contains not printable characters */
    static final int f19186;

    /* renamed from: 连任  reason: contains not printable characters */
    final AtomicReference<FixedSchedulerPool> f19187 = new AtomicReference<>(f19185);

    /* renamed from: 麤  reason: contains not printable characters */
    final ThreadFactory f19188;

    static {
        int max;
        int maxThreads = Integer.getInteger("rx.scheduler.max-computation-threads", 0).intValue();
        int cpuCount = Runtime.getRuntime().availableProcessors();
        if (maxThreads <= 0 || maxThreads > cpuCount) {
            max = cpuCount;
        } else {
            max = maxThreads;
        }
        f19186 = max;
        f19184.unsubscribe();
    }

    static final class FixedSchedulerPool {

        /* renamed from: 靐  reason: contains not printable characters */
        final PoolWorker[] f19197;

        /* renamed from: 齉  reason: contains not printable characters */
        long f19198;

        /* renamed from: 龘  reason: contains not printable characters */
        final int f19199;

        FixedSchedulerPool(ThreadFactory threadFactory, int maxThreads) {
            this.f19199 = maxThreads;
            this.f19197 = new PoolWorker[maxThreads];
            for (int i = 0; i < maxThreads; i++) {
                this.f19197[i] = new PoolWorker(threadFactory);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PoolWorker m24822() {
            int c = this.f19199;
            if (c == 0) {
                return EventLoopsScheduler.f19184;
            }
            PoolWorker[] poolWorkerArr = this.f19197;
            long j = this.f19198;
            this.f19198 = 1 + j;
            return poolWorkerArr[(int) (j % ((long) c))];
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24821() {
            for (PoolWorker w : this.f19197) {
                w.unsubscribe();
            }
        }
    }

    public EventLoopsScheduler(ThreadFactory threadFactory) {
        this.f19188 = threadFactory;
        m24816();
    }

    public Scheduler.Worker createWorker() {
        return new EventLoopWorker(this.f19187.get().m24822());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24816() {
        FixedSchedulerPool update = new FixedSchedulerPool(this.f19188, f19186);
        if (!this.f19187.compareAndSet(f19185, update)) {
            update.m24821();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m24814() {
        FixedSchedulerPool curr;
        do {
            curr = this.f19187.get();
            if (curr == f19185) {
                return;
            }
        } while (!this.f19187.compareAndSet(curr, f19185));
        curr.m24821();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscription m24815(Action0 action) {
        return this.f19187.get().m24822().m24840(action, -1, TimeUnit.NANOSECONDS);
    }

    static final class EventLoopWorker extends Scheduler.Worker {

        /* renamed from: 靐  reason: contains not printable characters */
        private final CompositeSubscription f19189 = new CompositeSubscription();

        /* renamed from: 麤  reason: contains not printable characters */
        private final PoolWorker f19190;

        /* renamed from: 齉  reason: contains not printable characters */
        private final SubscriptionList f19191 = new SubscriptionList(this.f19192, this.f19189);

        /* renamed from: 龘  reason: contains not printable characters */
        private final SubscriptionList f19192 = new SubscriptionList();

        EventLoopWorker(PoolWorker poolWorker) {
            this.f19190 = poolWorker;
        }

        public void unsubscribe() {
            this.f19191.unsubscribe();
        }

        public boolean isUnsubscribed() {
            return this.f19191.isUnsubscribed();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24817(final Action0 action) {
            if (isUnsubscribed()) {
                return Subscriptions.m25089();
            }
            return this.f19190.m24843((Action0) new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24819() {
                    if (!EventLoopWorker.this.isUnsubscribed()) {
                        action.m24539();
                    }
                }
            }, 0, (TimeUnit) null, this.f19192);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24818(final Action0 action, long delayTime, TimeUnit unit) {
            if (isUnsubscribed()) {
                return Subscriptions.m25089();
            }
            return this.f19190.m24844((Action0) new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24820() {
                    if (!EventLoopWorker.this.isUnsubscribed()) {
                        action.m24539();
                    }
                }
            }, delayTime, unit, this.f19189);
        }
    }

    static final class PoolWorker extends NewThreadWorker {
        PoolWorker(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }
}
