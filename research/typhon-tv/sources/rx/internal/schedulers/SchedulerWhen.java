package rx.internal.schedulers;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import rx.Completable;
import rx.CompletableSubscriber;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.internal.operators.BufferUntilSubscriber;
import rx.observers.SerializedObserver;
import rx.subjects.PublishSubject;
import rx.subscriptions.Subscriptions;

public class SchedulerWhen extends Scheduler implements Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Subscription f19244 = Subscriptions.m25089();

    /* renamed from: 龘  reason: contains not printable characters */
    static final Subscription f19245 = new Subscription() {
        public void unsubscribe() {
        }

        public boolean isUnsubscribed() {
            return false;
        }
    };

    /* renamed from: 连任  reason: contains not printable characters */
    private final Subscription f19246;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Observer<Observable<Completable>> f19247;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Scheduler f19248;

    public SchedulerWhen(Func1<Observable<Observable<Completable>>, Completable> combine, Scheduler actualScheduler) {
        this.f19248 = actualScheduler;
        PublishSubject<Observable<Completable>> workerSubject = PublishSubject.m25061();
        this.f19247 = new SerializedObserver(workerSubject);
        this.f19246 = combine.call(workerSubject.m7367()).m24480();
    }

    public void unsubscribe() {
        this.f19246.unsubscribe();
    }

    public boolean isUnsubscribed() {
        return this.f19246.isUnsubscribed();
    }

    public Scheduler.Worker createWorker() {
        final Scheduler.Worker actualWorker = this.f19248.createWorker();
        BufferUntilSubscriber<ScheduledAction> actionSubject = BufferUntilSubscriber.m24558();
        final Observer<ScheduledAction> actionObserver = new SerializedObserver<>(actionSubject);
        Observable<R> r2 = actionSubject.m7392((Func1<? super ScheduledAction, ? extends R>) new Func1<ScheduledAction, Completable>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Completable call(final ScheduledAction action) {
                return Completable.m24478((Completable.OnSubscribe) new Completable.OnSubscribe() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void call(CompletableSubscriber actionCompletable) {
                        actionCompletable.m24490((Subscription) action);
                        action.m24858(actualWorker, actionCompletable);
                    }
                });
            }
        });
        Scheduler.Worker worker = new Scheduler.Worker() {

            /* renamed from: 麤  reason: contains not printable characters */
            private final AtomicBoolean f19254 = new AtomicBoolean();

            public void unsubscribe() {
                if (this.f19254.compareAndSet(false, true)) {
                    actualWorker.unsubscribe();
                    actionObserver.onCompleted();
                }
            }

            public boolean isUnsubscribed() {
                return this.f19254.get();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Subscription m24854(Action0 action, long delayTime, TimeUnit unit) {
                DelayedAction delayedAction = new DelayedAction(action, delayTime, unit);
                actionObserver.onNext(delayedAction);
                return delayedAction;
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Subscription m24853(Action0 action) {
                ImmediateAction immediateAction = new ImmediateAction(action);
                actionObserver.onNext(immediateAction);
                return immediateAction;
            }
        };
        this.f19247.onNext(r2);
        return worker;
    }

    static abstract class ScheduledAction extends AtomicReference<Subscription> implements Subscription {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract Subscription m24860(Scheduler.Worker worker, CompletableSubscriber completableSubscriber);

        public ScheduledAction() {
            super(SchedulerWhen.f19245);
        }

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24858(Scheduler.Worker actualWorker, CompletableSubscriber actionCompletable) {
            Subscription oldState = (Subscription) get();
            if (oldState != SchedulerWhen.f19244 && oldState == SchedulerWhen.f19245) {
                Subscription newState = m24860(actualWorker, actionCompletable);
                if (!compareAndSet(SchedulerWhen.f19245, newState)) {
                    newState.unsubscribe();
                }
            }
        }

        public boolean isUnsubscribed() {
            return ((Subscription) get()).isUnsubscribed();
        }

        public void unsubscribe() {
            Subscription oldState;
            Subscription newState = SchedulerWhen.f19244;
            do {
                oldState = (Subscription) get();
                if (oldState == SchedulerWhen.f19244) {
                    return;
                }
            } while (!compareAndSet(oldState, newState));
            if (oldState != SchedulerWhen.f19245) {
                oldState.unsubscribe();
            }
        }
    }

    static class ImmediateAction extends ScheduledAction {
        private final Action0 action;

        public ImmediateAction(Action0 action2) {
            this.action = action2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24856(Scheduler.Worker actualWorker, CompletableSubscriber actionCompletable) {
            return actualWorker.m24503(new OnCompletedAction(this.action, actionCompletable));
        }
    }

    static class DelayedAction extends ScheduledAction {
        private final Action0 action;
        private final long delayTime;
        private final TimeUnit unit;

        public DelayedAction(Action0 action2, long delayTime2, TimeUnit unit2) {
            this.action = action2;
            this.delayTime = delayTime2;
            this.unit = unit2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24855(Scheduler.Worker actualWorker, CompletableSubscriber actionCompletable) {
            return actualWorker.m24505(new OnCompletedAction(this.action, actionCompletable), this.delayTime, this.unit);
        }
    }

    static class OnCompletedAction implements Action0 {

        /* renamed from: 靐  reason: contains not printable characters */
        private Action0 f19257;

        /* renamed from: 龘  reason: contains not printable characters */
        private CompletableSubscriber f19258;

        public OnCompletedAction(Action0 action, CompletableSubscriber actionCompletable) {
            this.f19257 = action;
            this.f19258 = actionCompletable;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24857() {
            try {
                this.f19257.m24539();
            } finally {
                this.f19258.m24488();
            }
        }
    }
}
