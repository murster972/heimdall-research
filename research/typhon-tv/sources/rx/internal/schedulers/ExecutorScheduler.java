package rx.internal.schedulers;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.MultipleAssignmentSubscription;
import rx.subscriptions.Subscriptions;

public final class ExecutorScheduler extends Scheduler {

    /* renamed from: 龘  reason: contains not printable characters */
    final Executor f19200;

    public ExecutorScheduler(Executor executor) {
        this.f19200 = executor;
    }

    public Scheduler.Worker createWorker() {
        return new ExecutorSchedulerWorker(this.f19200);
    }

    static final class ExecutorSchedulerWorker extends Scheduler.Worker implements Runnable {

        /* renamed from: 连任  reason: contains not printable characters */
        final ScheduledExecutorService f19201 = GenericScheduledExecutorService.m24827();

        /* renamed from: 靐  reason: contains not printable characters */
        final CompositeSubscription f19202 = new CompositeSubscription();

        /* renamed from: 麤  reason: contains not printable characters */
        final AtomicInteger f19203 = new AtomicInteger();

        /* renamed from: 齉  reason: contains not printable characters */
        final ConcurrentLinkedQueue<ScheduledAction> f19204 = new ConcurrentLinkedQueue<>();

        /* renamed from: 龘  reason: contains not printable characters */
        final Executor f19205;

        public ExecutorSchedulerWorker(Executor executor) {
            this.f19205 = executor;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24823(Action0 action) {
            if (isUnsubscribed()) {
                return Subscriptions.m25089();
            }
            ScheduledAction ea = new ScheduledAction(RxJavaHooks.m25022(action), this.f19202);
            this.f19202.m25086((Subscription) ea);
            this.f19204.offer(ea);
            if (this.f19203.getAndIncrement() != 0) {
                return ea;
            }
            try {
                this.f19205.execute(this);
                return ea;
            } catch (RejectedExecutionException t) {
                this.f19202.m25085(ea);
                this.f19203.decrementAndGet();
                RxJavaHooks.m25024((Throwable) t);
                throw t;
            }
        }

        public void run() {
            while (!this.f19202.isUnsubscribed()) {
                ScheduledAction sa = this.f19204.poll();
                if (sa != null) {
                    if (!sa.isUnsubscribed()) {
                        if (!this.f19202.isUnsubscribed()) {
                            sa.run();
                        } else {
                            this.f19204.clear();
                            return;
                        }
                    }
                    if (this.f19203.decrementAndGet() == 0) {
                        return;
                    }
                } else {
                    return;
                }
            }
            this.f19204.clear();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24824(Action0 action, long delayTime, TimeUnit unit) {
            if (delayTime <= 0) {
                return m24823(action);
            }
            if (isUnsubscribed()) {
                return Subscriptions.m25089();
            }
            final Action0 decorated = RxJavaHooks.m25022(action);
            MultipleAssignmentSubscription first = new MultipleAssignmentSubscription();
            final MultipleAssignmentSubscription mas = new MultipleAssignmentSubscription();
            mas.m25087(first);
            this.f19202.m25086((Subscription) mas);
            final Subscription removeMas = Subscriptions.m25090(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24825() {
                    ExecutorSchedulerWorker.this.f19202.m25085(mas);
                }
            });
            ScheduledAction ea = new ScheduledAction(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24826() {
                    if (!mas.isUnsubscribed()) {
                        Subscription s2 = ExecutorSchedulerWorker.this.m24823(decorated);
                        mas.m25087(s2);
                        if (s2.getClass() == ScheduledAction.class) {
                            ((ScheduledAction) s2).add(removeMas);
                        }
                    }
                }
            });
            first.m25087(ea);
            try {
                ea.add(this.f19201.schedule(ea, delayTime, unit));
                return removeMas;
            } catch (RejectedExecutionException t) {
                RxJavaHooks.m25024((Throwable) t);
                throw t;
            }
        }

        public boolean isUnsubscribed() {
            return this.f19202.isUnsubscribed();
        }

        public void unsubscribe() {
            this.f19202.unsubscribe();
            this.f19204.clear();
        }
    }
}
