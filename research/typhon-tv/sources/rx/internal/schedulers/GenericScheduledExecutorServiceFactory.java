package rx.internal.schedulers;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import rx.functions.Func0;
import rx.internal.util.RxThreadFactory;
import rx.plugins.RxJavaHooks;

enum GenericScheduledExecutorServiceFactory {
    ;
    

    /* renamed from: 龘  reason: contains not printable characters */
    static final RxThreadFactory f19218 = null;

    static {
        f19218 = new RxThreadFactory("RxScheduledExecutorPool-");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static ThreadFactory m24832() {
        return f19218;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static ScheduledExecutorService m24830() {
        Func0<? extends ScheduledExecutorService> f = RxJavaHooks.m25013();
        if (f == null) {
            return m24831();
        }
        return (ScheduledExecutorService) f.call();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static ScheduledExecutorService m24831() {
        return Executors.newScheduledThreadPool(1, m24832());
    }
}
