package rx.internal.schedulers;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.internal.util.RxThreadFactory;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

public final class CachedThreadScheduler extends Scheduler implements SchedulerLifecycle {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final TimeUnit f19162 = TimeUnit.SECONDS;

    /* renamed from: 连任  reason: contains not printable characters */
    private static final long f19163 = ((long) Integer.getInteger("rx.io-scheduler.keepalive", 60).intValue());

    /* renamed from: 靐  reason: contains not printable characters */
    static final CachedWorkerPool f19164 = new CachedWorkerPool((ThreadFactory) null, 0, (TimeUnit) null);

    /* renamed from: 龘  reason: contains not printable characters */
    static final ThreadWorker f19165 = new ThreadWorker(RxThreadFactory.NONE);

    /* renamed from: 麤  reason: contains not printable characters */
    final AtomicReference<CachedWorkerPool> f19166 = new AtomicReference<>(f19164);

    /* renamed from: 齉  reason: contains not printable characters */
    final ThreadFactory f19167;

    static {
        f19165.unsubscribe();
        f19164.m24804();
    }

    static final class CachedWorkerPool {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final Future<?> f19168;

        /* renamed from: 连任  reason: contains not printable characters */
        private final ScheduledExecutorService f19169;

        /* renamed from: 靐  reason: contains not printable characters */
        private final long f19170;

        /* renamed from: 麤  reason: contains not printable characters */
        private final CompositeSubscription f19171;

        /* renamed from: 齉  reason: contains not printable characters */
        private final ConcurrentLinkedQueue<ThreadWorker> f19172;

        /* renamed from: 龘  reason: contains not printable characters */
        private final ThreadFactory f19173;

        CachedWorkerPool(final ThreadFactory threadFactory, long keepAliveTime, TimeUnit unit) {
            this.f19173 = threadFactory;
            this.f19170 = unit != null ? unit.toNanos(keepAliveTime) : 0;
            this.f19172 = new ConcurrentLinkedQueue<>();
            this.f19171 = new CompositeSubscription();
            ScheduledExecutorService evictor = null;
            Future<?> task = null;
            if (unit != null) {
                evictor = Executors.newScheduledThreadPool(1, new ThreadFactory() {
                    public Thread newThread(Runnable r) {
                        Thread thread = threadFactory.newThread(r);
                        thread.setName(thread.getName() + " (Evictor)");
                        return thread;
                    }
                });
                NewThreadWorker.m24835(evictor);
                task = evictor.scheduleWithFixedDelay(new Runnable() {
                    public void run() {
                        CachedWorkerPool.this.m24803();
                    }
                }, this.f19170, this.f19170, TimeUnit.NANOSECONDS);
            }
            this.f19169 = evictor;
            this.f19168 = task;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public ThreadWorker m24806() {
            if (this.f19171.isUnsubscribed()) {
                return CachedThreadScheduler.f19165;
            }
            while (!this.f19172.isEmpty()) {
                ThreadWorker threadWorker = this.f19172.poll();
                if (threadWorker != null) {
                    return threadWorker;
                }
            }
            ThreadWorker w = new ThreadWorker(this.f19173);
            this.f19171.m25086((Subscription) w);
            return w;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24807(ThreadWorker threadWorker) {
            threadWorker.m24813(m24805() + this.f19170);
            this.f19172.offer(threadWorker);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m24803() {
            if (!this.f19172.isEmpty()) {
                long currentTimestamp = m24805();
                Iterator<ThreadWorker> it2 = this.f19172.iterator();
                while (it2.hasNext()) {
                    ThreadWorker threadWorker = it2.next();
                    if (threadWorker.m24812() > currentTimestamp) {
                        return;
                    }
                    if (this.f19172.remove(threadWorker)) {
                        this.f19171.m25085(threadWorker);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public long m24805() {
            return System.nanoTime();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public void m24804() {
            try {
                if (this.f19168 != null) {
                    this.f19168.cancel(true);
                }
                if (this.f19169 != null) {
                    this.f19169.shutdownNow();
                }
            } finally {
                this.f19171.unsubscribe();
            }
        }
    }

    public CachedThreadScheduler(ThreadFactory threadFactory) {
        this.f19167 = threadFactory;
        m24802();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24802() {
        CachedWorkerPool update = new CachedWorkerPool(this.f19167, f19163, f19162);
        if (!this.f19166.compareAndSet(f19164, update)) {
            update.m24804();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m24801() {
        CachedWorkerPool curr;
        do {
            curr = this.f19166.get();
            if (curr == f19164) {
                return;
            }
        } while (!this.f19166.compareAndSet(curr, f19164));
        curr.m24804();
    }

    public Scheduler.Worker createWorker() {
        return new EventLoopWorker(this.f19166.get());
    }

    static final class EventLoopWorker extends Scheduler.Worker implements Action0 {

        /* renamed from: 靐  reason: contains not printable characters */
        private final CompositeSubscription f19177 = new CompositeSubscription();

        /* renamed from: 麤  reason: contains not printable characters */
        private final ThreadWorker f19178;

        /* renamed from: 齉  reason: contains not printable characters */
        private final CachedWorkerPool f19179;

        /* renamed from: 龘  reason: contains not printable characters */
        final AtomicBoolean f19180;

        EventLoopWorker(CachedWorkerPool pool) {
            this.f19179 = pool;
            this.f19180 = new AtomicBoolean();
            this.f19178 = pool.m24806();
        }

        public void unsubscribe() {
            if (this.f19180.compareAndSet(false, true)) {
                this.f19178.m24841((Action0) this);
            }
            this.f19177.unsubscribe();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24810() {
            this.f19179.m24807(this.f19178);
        }

        public boolean isUnsubscribed() {
            return this.f19177.isUnsubscribed();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24808(Action0 action) {
            return m24809(action, 0, (TimeUnit) null);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24809(final Action0 action, long delayTime, TimeUnit unit) {
            if (this.f19177.isUnsubscribed()) {
                return Subscriptions.m25089();
            }
            ScheduledAction s = this.f19178.m24840(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m24811() {
                    if (!EventLoopWorker.this.isUnsubscribed()) {
                        action.m24539();
                    }
                }
            }, delayTime, unit);
            this.f19177.m25086((Subscription) s);
            s.addParent(this.f19177);
            return s;
        }
    }

    static final class ThreadWorker extends NewThreadWorker {

        /* renamed from: 齉  reason: contains not printable characters */
        private long f19183 = 0;

        ThreadWorker(ThreadFactory threadFactory) {
            super(threadFactory);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m24812() {
            return this.f19183;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24813(long expirationTime) {
            this.f19183 = expirationTime;
        }
    }
}
