package rx.internal.schedulers;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.BooleanSubscription;
import rx.subscriptions.Subscriptions;

public final class TrampolineScheduler extends Scheduler {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final TrampolineScheduler f19262 = new TrampolineScheduler();

    public Scheduler.Worker createWorker() {
        return new InnerCurrentThreadScheduler();
    }

    private TrampolineScheduler() {
    }

    static final class InnerCurrentThreadScheduler extends Scheduler.Worker implements Subscription {

        /* renamed from: 靐  reason: contains not printable characters */
        final PriorityBlockingQueue<TimedAction> f19263 = new PriorityBlockingQueue<>();

        /* renamed from: 麤  reason: contains not printable characters */
        private final AtomicInteger f19264 = new AtomicInteger();

        /* renamed from: 齉  reason: contains not printable characters */
        private final BooleanSubscription f19265 = new BooleanSubscription();

        /* renamed from: 龘  reason: contains not printable characters */
        final AtomicInteger f19266 = new AtomicInteger();

        InnerCurrentThreadScheduler() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24864(Action0 action) {
            return m24863(action, m24502());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24865(Action0 action, long delayTime, TimeUnit unit) {
            long execTime = m24502() + unit.toMillis(delayTime);
            return m24863(new SleepingAction(action, this, execTime), execTime);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Subscription m24863(Action0 action, long execTime) {
            if (this.f19265.isUnsubscribed()) {
                return Subscriptions.m25089();
            }
            final TimedAction timedAction = new TimedAction(action, Long.valueOf(execTime), this.f19266.incrementAndGet());
            this.f19263.add(timedAction);
            if (this.f19264.getAndIncrement() != 0) {
                return Subscriptions.m25090(new Action0() {
                    /* renamed from: 龘  reason: contains not printable characters */
                    public void m24866() {
                        InnerCurrentThreadScheduler.this.f19263.remove(timedAction);
                    }
                });
            }
            do {
                TimedAction polled = this.f19263.poll();
                if (polled != null) {
                    polled.f19271.m24539();
                }
            } while (this.f19264.decrementAndGet() > 0);
            return Subscriptions.m25089();
        }

        public void unsubscribe() {
            this.f19265.unsubscribe();
        }

        public boolean isUnsubscribed() {
            return this.f19265.isUnsubscribed();
        }
    }

    static final class TimedAction implements Comparable<TimedAction> {

        /* renamed from: 靐  reason: contains not printable characters */
        final Long f19269;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f19270;

        /* renamed from: 龘  reason: contains not printable characters */
        final Action0 f19271;

        TimedAction(Action0 action, Long execTime, int count) {
            this.f19271 = action;
            this.f19269 = execTime;
            this.f19270 = count;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int compareTo(TimedAction that) {
            int result = this.f19269.compareTo(that.f19269);
            if (result == 0) {
                return TrampolineScheduler.m24862(this.f19270, that.f19270);
            }
            return result;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m24862(int x, int y) {
        if (x < y) {
            return -1;
        }
        return x == y ? 0 : 1;
    }
}
