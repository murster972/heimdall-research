package rx.internal.schedulers;

import java.util.concurrent.ThreadFactory;
import rx.Scheduler;

public final class NewThreadScheduler extends Scheduler {

    /* renamed from: 龘  reason: contains not printable characters */
    private final ThreadFactory f19222;

    public NewThreadScheduler(ThreadFactory threadFactory) {
        this.f19222 = threadFactory;
    }

    public Scheduler.Worker createWorker() {
        return new NewThreadWorker(this.f19222);
    }
}
