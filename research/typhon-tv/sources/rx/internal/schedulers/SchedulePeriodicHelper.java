package rx.internal.schedulers;

import java.util.concurrent.TimeUnit;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.internal.subscriptions.SequentialSubscription;

public final class SchedulePeriodicHelper {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final long f19231 = TimeUnit.MINUTES.toNanos(Long.getLong("rx.scheduler.drift-tolerance", 15).longValue());

    public interface NowNanoSupplier {
        /* renamed from: 龘  reason: contains not printable characters */
        long m24847();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Subscription m24845(Scheduler.Worker worker, Action0 action, long initialDelay, long period, TimeUnit unit, NowNanoSupplier nowNanoSupplier) {
        final long periodInNanos = unit.toNanos(period);
        final long firstNowNanos = nowNanoSupplier != null ? nowNanoSupplier.m24847() : TimeUnit.MILLISECONDS.toNanos(worker.m24502());
        final long firstStartInNanos = firstNowNanos + unit.toNanos(initialDelay);
        SequentialSubscription first = new SequentialSubscription();
        final SequentialSubscription mas = new SequentialSubscription(first);
        final Action0 action0 = action;
        final NowNanoSupplier nowNanoSupplier2 = nowNanoSupplier;
        final Scheduler.Worker worker2 = worker;
        first.replace(worker.m24505(new Action0() {

            /* renamed from: 靐  reason: contains not printable characters */
            long f19238 = firstNowNanos;

            /* renamed from: 齉  reason: contains not printable characters */
            long f19240 = firstStartInNanos;

            /* renamed from: 龘  reason: contains not printable characters */
            long f19241;

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24846() {
                long nowNanos;
                long nextTick;
                action0.m24539();
                if (!mas.isUnsubscribed()) {
                    if (nowNanoSupplier2 != null) {
                        nowNanos = nowNanoSupplier2.m24847();
                    } else {
                        nowNanos = TimeUnit.MILLISECONDS.toNanos(worker2.m24502());
                    }
                    if (SchedulePeriodicHelper.f19231 + nowNanos < this.f19238 || nowNanos >= this.f19238 + periodInNanos + SchedulePeriodicHelper.f19231) {
                        nextTick = nowNanos + periodInNanos;
                        long j = periodInNanos;
                        long j2 = this.f19241 + 1;
                        this.f19241 = j2;
                        this.f19240 = nextTick - (j * j2);
                    } else {
                        long j3 = this.f19240;
                        long j4 = this.f19241 + 1;
                        this.f19241 = j4;
                        nextTick = j3 + (j4 * periodInNanos);
                    }
                    this.f19238 = nowNanos;
                    mas.replace(worker2.m24505(this, nextTick - nowNanos, TimeUnit.NANOSECONDS));
                }
            }
        }, initialDelay, unit));
        return mas;
    }
}
