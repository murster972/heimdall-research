package rx.internal.schedulers;

import rx.Scheduler;
import rx.exceptions.Exceptions;
import rx.functions.Action0;

class SleepingAction implements Action0 {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Scheduler.Worker f19259;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f19260;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Action0 f19261;

    public SleepingAction(Action0 underlying, Scheduler.Worker scheduler, long execTime) {
        this.f19261 = underlying;
        this.f19259 = scheduler;
        this.f19260 = execTime;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24861() {
        if (!this.f19259.isUnsubscribed()) {
            long delay = this.f19260 - this.f19259.m24502();
            if (delay > 0) {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    Exceptions.m24531((Throwable) e);
                }
            }
            if (!this.f19259.isUnsubscribed()) {
                this.f19261.m24539();
            }
        }
    }
}
