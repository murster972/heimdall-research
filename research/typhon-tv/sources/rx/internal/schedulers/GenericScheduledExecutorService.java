package rx.internal.schedulers;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicReference;

public final class GenericScheduledExecutorService implements SchedulerLifecycle {

    /* renamed from: 连任  reason: contains not printable characters */
    private static int f19212;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ScheduledExecutorService[] f19213 = new ScheduledExecutorService[0];

    /* renamed from: 齉  reason: contains not printable characters */
    private static final ScheduledExecutorService f19214 = Executors.newScheduledThreadPool(0);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final GenericScheduledExecutorService f19215 = new GenericScheduledExecutorService();

    /* renamed from: 麤  reason: contains not printable characters */
    private final AtomicReference<ScheduledExecutorService[]> f19216 = new AtomicReference<>(f19213);

    static {
        f19214.shutdown();
    }

    private GenericScheduledExecutorService() {
        m24829();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24829() {
        int i = 0;
        int count = Runtime.getRuntime().availableProcessors();
        if (count > 4) {
            count /= 2;
        }
        if (count > 8) {
            count = 8;
        }
        ScheduledExecutorService[] execs = new ScheduledExecutorService[count];
        for (int i2 = 0; i2 < count; i2++) {
            execs[i2] = GenericScheduledExecutorServiceFactory.m24830();
        }
        if (this.f19216.compareAndSet(f19213, execs)) {
            int length = execs.length;
            while (i < length) {
                ScheduledExecutorService exec = execs[i];
                if (!NewThreadWorker.m24835(exec) && (exec instanceof ScheduledThreadPoolExecutor)) {
                    NewThreadWorker.m24839((ScheduledThreadPoolExecutor) exec);
                }
                i++;
            }
            return;
        }
        int length2 = execs.length;
        while (i < length2) {
            execs[i].shutdownNow();
            i++;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m24828() {
        ScheduledExecutorService[] execs;
        do {
            execs = this.f19216.get();
            if (execs == f19213) {
                return;
            }
        } while (!this.f19216.compareAndSet(execs, f19213));
        for (ScheduledExecutorService exec : execs) {
            NewThreadWorker.m24838(exec);
            exec.shutdownNow();
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static ScheduledExecutorService m24827() {
        ScheduledExecutorService[] execs = f19215.f19216.get();
        if (execs == f19213) {
            return f19214;
        }
        int r = f19212 + 1;
        if (r >= execs.length) {
            r = 0;
        }
        f19212 = r;
        return execs[r];
    }
}
