package rx.internal.schedulers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.internal.util.PlatformDependent;
import rx.internal.util.RxThreadFactory;
import rx.internal.util.SubscriptionList;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.CompositeSubscription;
import rx.subscriptions.Subscriptions;

public class NewThreadWorker extends Scheduler.Worker implements Subscription {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final AtomicReference<ScheduledExecutorService> f19223 = new AtomicReference<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private static volatile Object f19224;

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final Object f19225 = new Object();

    /* renamed from: 连任  reason: contains not printable characters */
    private static final ConcurrentHashMap<ScheduledThreadPoolExecutor, ScheduledThreadPoolExecutor> f19226 = new ConcurrentHashMap<>();

    /* renamed from: 靐  reason: contains not printable characters */
    public static final int f19227 = Integer.getInteger("rx.scheduler.jdk6.purge-frequency-millis", 1000).intValue();

    /* renamed from: 麤  reason: contains not printable characters */
    private static final boolean f19228;

    /* renamed from: 齉  reason: contains not printable characters */
    private final ScheduledExecutorService f19229;

    /* renamed from: 龘  reason: contains not printable characters */
    volatile boolean f19230;

    static {
        boolean purgeForce = Boolean.getBoolean("rx.scheduler.jdk6.purge-force");
        int androidApiVersion = PlatformDependent.m24896();
        f19228 = !purgeForce && (androidApiVersion == 0 || androidApiVersion >= 21);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24839(ScheduledThreadPoolExecutor service) {
        while (true) {
            if (f19223.get() != null) {
                break;
            }
            ScheduledExecutorService exec = Executors.newScheduledThreadPool(1, new RxThreadFactory("RxSchedulerPurge-"));
            if (f19223.compareAndSet((Object) null, exec)) {
                exec.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        NewThreadWorker.m24837();
                    }
                }, (long) f19227, (long) f19227, TimeUnit.MILLISECONDS);
                break;
            }
            exec.shutdownNow();
        }
        f19226.putIfAbsent(service, service);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24838(ScheduledExecutorService service) {
        f19226.remove(service);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static void m24837() {
        try {
            Iterator<ScheduledThreadPoolExecutor> it2 = f19226.keySet().iterator();
            while (it2.hasNext()) {
                ScheduledThreadPoolExecutor exec = it2.next();
                if (!exec.isShutdown()) {
                    exec.purge();
                } else {
                    it2.remove();
                }
            }
        } catch (Throwable t) {
            Exceptions.m24529(t);
            RxJavaHooks.m25024(t);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m24835(ScheduledExecutorService executor) {
        Method methodToCall;
        if (f19228) {
            if (executor instanceof ScheduledThreadPoolExecutor) {
                Object localSetRemoveOnCancelPolicyMethod = f19224;
                if (localSetRemoveOnCancelPolicyMethod == f19225) {
                    return false;
                }
                if (localSetRemoveOnCancelPolicyMethod == null) {
                    Method method = m24836(executor);
                    f19224 = method != null ? method : f19225;
                    methodToCall = method;
                } else {
                    methodToCall = localSetRemoveOnCancelPolicyMethod;
                }
            } else {
                methodToCall = m24836(executor);
            }
            if (methodToCall != null) {
                try {
                    methodToCall.invoke(executor, new Object[]{true});
                    return true;
                } catch (InvocationTargetException e) {
                    RxJavaHooks.m25024((Throwable) e);
                } catch (IllegalAccessException e2) {
                    RxJavaHooks.m25024((Throwable) e2);
                } catch (IllegalArgumentException e3) {
                    RxJavaHooks.m25024((Throwable) e3);
                }
            }
        }
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static Method m24836(ScheduledExecutorService executor) {
        for (Method method : executor.getClass().getMethods()) {
            if (method.getName().equals("setRemoveOnCancelPolicy")) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length == 1 && parameterTypes[0] == Boolean.TYPE) {
                    return method;
                }
            }
        }
        return null;
    }

    public NewThreadWorker(ThreadFactory threadFactory) {
        ScheduledExecutorService exec = Executors.newScheduledThreadPool(1, threadFactory);
        if (!m24835(exec) && (exec instanceof ScheduledThreadPoolExecutor)) {
            m24839((ScheduledThreadPoolExecutor) exec);
        }
        this.f19229 = exec;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscription m24841(Action0 action) {
        return m24842(action, 0, (TimeUnit) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Subscription m24842(Action0 action, long delayTime, TimeUnit unit) {
        if (this.f19230) {
            return Subscriptions.m25089();
        }
        return m24840(action, delayTime, unit);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public ScheduledAction m24840(Action0 action, long delayTime, TimeUnit unit) {
        Future<?> f;
        ScheduledAction run = new ScheduledAction(RxJavaHooks.m25022(action));
        if (delayTime <= 0) {
            f = this.f19229.submit(run);
        } else {
            f = this.f19229.schedule(run, delayTime, unit);
        }
        run.add(f);
        return run;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ScheduledAction m24844(Action0 action, long delayTime, TimeUnit unit, CompositeSubscription parent) {
        Future<?> f;
        ScheduledAction run = new ScheduledAction(RxJavaHooks.m25022(action), parent);
        parent.m25086((Subscription) run);
        if (delayTime <= 0) {
            f = this.f19229.submit(run);
        } else {
            f = this.f19229.schedule(run, delayTime, unit);
        }
        run.add(f);
        return run;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public ScheduledAction m24843(Action0 action, long delayTime, TimeUnit unit, SubscriptionList parent) {
        Future<?> f;
        ScheduledAction run = new ScheduledAction(RxJavaHooks.m25022(action), parent);
        parent.m24923((Subscription) run);
        if (delayTime <= 0) {
            f = this.f19229.submit(run);
        } else {
            f = this.f19229.schedule(run, delayTime, unit);
        }
        run.add(f);
        return run;
    }

    public void unsubscribe() {
        this.f19230 = true;
        this.f19229.shutdownNow();
        m24838(this.f19229);
    }

    public boolean isUnsubscribed() {
        return this.f19230;
    }
}
