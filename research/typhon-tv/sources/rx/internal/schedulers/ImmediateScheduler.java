package rx.internal.schedulers;

import java.util.concurrent.TimeUnit;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.BooleanSubscription;
import rx.subscriptions.Subscriptions;

public final class ImmediateScheduler extends Scheduler {

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ImmediateScheduler f19219 = new ImmediateScheduler();

    private ImmediateScheduler() {
    }

    public Scheduler.Worker createWorker() {
        return new InnerImmediateScheduler();
    }

    final class InnerImmediateScheduler extends Scheduler.Worker implements Subscription {

        /* renamed from: 龘  reason: contains not printable characters */
        final BooleanSubscription f19221 = new BooleanSubscription();

        InnerImmediateScheduler() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24834(Action0 action, long delayTime, TimeUnit unit) {
            return m24833(new SleepingAction(action, this, ImmediateScheduler.this.now() + unit.toMillis(delayTime)));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24833(Action0 action) {
            action.m24539();
            return Subscriptions.m25089();
        }

        public void unsubscribe() {
            this.f19221.unsubscribe();
        }

        public boolean isUnsubscribed() {
            return this.f19221.isUnsubscribed();
        }
    }
}
