package rx;

import java.util.concurrent.TimeUnit;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.internal.schedulers.SchedulePeriodicHelper;
import rx.internal.schedulers.SchedulerWhen;

public abstract class Scheduler {
    public abstract Worker createWorker();

    public static abstract class Worker implements Subscription {
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract Subscription m24503(Action0 action0);

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract Subscription m24505(Action0 action0, long j, TimeUnit timeUnit);

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24504(Action0 action, long initialDelay, long period, TimeUnit unit) {
            return SchedulePeriodicHelper.m24845(this, action, initialDelay, period, unit, (SchedulePeriodicHelper.NowNanoSupplier) null);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m24502() {
            return System.currentTimeMillis();
        }
    }

    public long now() {
        return System.currentTimeMillis();
    }

    public <S extends Scheduler & Subscription> S when(Func1<Observable<Observable<Completable>>, Completable> combine) {
        return new SchedulerWhen(combine, this);
    }
}
