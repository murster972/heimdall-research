package rx;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.List;
import java.util.concurrent.TimeUnit;
import rx.exceptions.Exceptions;
import rx.exceptions.OnErrorFailedException;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.internal.operators.EmptyObservableHolder;
import rx.internal.operators.OnSubscribeConcatMap;
import rx.internal.operators.OnSubscribeCreate;
import rx.internal.operators.OnSubscribeDoOnEach;
import rx.internal.operators.OnSubscribeFilter;
import rx.internal.operators.OnSubscribeFromArray;
import rx.internal.operators.OnSubscribeFromIterable;
import rx.internal.operators.OnSubscribeLift;
import rx.internal.operators.OnSubscribeMap;
import rx.internal.operators.OnSubscribeRange;
import rx.internal.operators.OnSubscribeRedo;
import rx.internal.operators.OnSubscribeSingle;
import rx.internal.operators.OnSubscribeSwitchIfEmpty;
import rx.internal.operators.OnSubscribeThrow;
import rx.internal.operators.OnSubscribeTimeoutTimedWithFallback;
import rx.internal.operators.OnSubscribeTimerOnce;
import rx.internal.operators.OnSubscribeTimerPeriodically;
import rx.internal.operators.OperatorBufferWithSize;
import rx.internal.operators.OperatorBufferWithTime;
import rx.internal.operators.OperatorDelay;
import rx.internal.operators.OperatorDematerialize;
import rx.internal.operators.OperatorDoAfterTerminate;
import rx.internal.operators.OperatorDoOnSubscribe;
import rx.internal.operators.OperatorMerge;
import rx.internal.operators.OperatorObserveOn;
import rx.internal.operators.OperatorOnBackpressureBuffer;
import rx.internal.operators.OperatorOnErrorResumeNextViaFunction;
import rx.internal.operators.OperatorReplay;
import rx.internal.operators.OperatorSampleWithTime;
import rx.internal.operators.OperatorSingle;
import rx.internal.operators.OperatorSubscribeOn;
import rx.internal.operators.OperatorTake;
import rx.internal.operators.OperatorTakeUntil;
import rx.internal.operators.OperatorToObservableList;
import rx.internal.operators.OperatorZip;
import rx.internal.util.ActionObserver;
import rx.internal.util.ActionSubscriber;
import rx.internal.util.InternalObservableUtils;
import rx.internal.util.ObserverSubscriber;
import rx.internal.util.RxRingBuffer;
import rx.internal.util.ScalarSynchronousObservable;
import rx.internal.util.UtilityFunctions;
import rx.observables.ConnectableObservable;
import rx.observers.SafeSubscriber;
import rx.plugins.RxJavaHooks;
import rx.schedulers.Schedulers;

public class Observable<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final OnSubscribe<T> f6506;

    public interface OnSubscribe<T> extends Action1<Subscriber<? super T>> {
    }

    public interface Operator<R, T> extends Func1<Subscriber<? super R>, Subscriber<? super T>> {
    }

    public interface Transformer<T, R> extends Func1<Observable<T>, Observable<R>> {
    }

    protected Observable(OnSubscribe<T> f) {
        this.f6506 = f;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7359(OnSubscribe<T> f) {
        return new Observable<>(RxJavaHooks.m25016(f));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> Observable<T> m7346(OnSubscribe<T> f) {
        return new Observable<>(RxJavaHooks.m25016(f));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <R> Observable<R> m7404(Operator<? extends R, ? super T> operator) {
        return m7346(new OnSubscribeLift(this.f6506, operator));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public <R> Observable<R> m7405(Transformer<? super T, ? extends R> transformer) {
        return (Observable) transformer.call(this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Single<T> m7414() {
        return new Single<>(OnSubscribeSingle.m24628(this));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Completable m7377() {
        return Completable.m24479((Observable<?>) this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7360(Observable<? extends Observable<? extends T>> observables) {
        return observables.m7413((Func1<? super Object, ? extends Observable<? extends R>>) UtilityFunctions.m24924());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static <T> Observable<T> m7349() {
        return EmptyObservableHolder.instance();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7358(Throwable exception) {
        return m7346(new OnSubscribeThrow(exception));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7355(Iterable<? extends T> iterable) {
        return m7346(new OnSubscribeFromIterable(iterable));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7364(T[] array) {
        int n = array.length;
        if (n == 0) {
            return m7349();
        }
        if (n == 1) {
            return m7356(array[0]);
        }
        return m7346(new OnSubscribeFromArray(array));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Observable<Long> m7353(long interval, TimeUnit unit) {
        return m7352(interval, interval, unit, Schedulers.computation());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Observable<Long> m7352(long initialDelay, long period, TimeUnit unit, Scheduler scheduler) {
        return m7346(new OnSubscribeTimerPeriodically(initialDelay, period, unit, scheduler));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7356(T value) {
        return ScalarSynchronousObservable.m24909(value);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7357(T t1, T t2) {
        return m7364((T[]) new Object[]{t1, t2});
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> Observable<T> m7347(Observable<? extends Observable<? extends T>> source) {
        if (source.getClass() == ScalarSynchronousObservable.class) {
            return ((ScalarSynchronousObservable) source).m24911(UtilityFunctions.m24924());
        }
        return source.m7404((Operator<? extends R, ? super Object>) OperatorMerge.m24675(false));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7361(Observable<? extends T> t1, Observable<? extends T> t2) {
        return m7365((Observable<? extends T>[]) new Observable[]{t1, t2});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7362(Observable<? extends T> t1, Observable<? extends T> t2, Observable<? extends T> t3) {
        return m7365((Observable<? extends T>[]) new Observable[]{t1, t2, t3});
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable<T> m7365(Observable<? extends T>[] sequences) {
        return m7347(m7364((T[]) sequences));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static <T> Observable<T> m7350(Observable<? extends Observable<? extends T>> source) {
        return source.m7404((Operator<? extends R, ? super Object>) OperatorMerge.m24675(true));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> Observable<T> m7345(Iterable<? extends Observable<? extends T>> sequences) {
        return m7350(m7355(sequences));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> Observable<T> m7348(Observable<? extends T> t1, Observable<? extends T> t2) {
        return m7350(m7357(t1, t2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Observable<Integer> m7351(int start, int count) {
        if (count < 0) {
            throw new IllegalArgumentException("Count can not be negative");
        } else if (count == 0) {
            return m7349();
        } else {
            if (start > (MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - count) + 1) {
                throw new IllegalArgumentException("start + count can not exceed Integer.MAX_VALUE");
            } else if (count == 1) {
                return m7356(Integer.valueOf(start));
            } else {
                return m7346(new OnSubscribeRange(start, (count - 1) + start));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Observable<Long> m7354(long delay, TimeUnit unit, Scheduler scheduler) {
        return m7346(new OnSubscribeTimerOnce(delay, unit, scheduler));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T1, T2, R> Observable<R> m7363(Observable<? extends T1> o1, Observable<? extends T2> o2, Func2<? super T1, ? super T2, ? extends R> zipFunction) {
        return m7356(new Observable[]{o1, o2}).m7404(new OperatorZip(zipFunction));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<List<T>> m7399(int count) {
        return m7378(count, count);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<List<T>> m7378(int count, int skip) {
        return m7404(new OperatorBufferWithSize(count, skip));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<List<T>> m7379(long timespan, TimeUnit unit) {
        return m7401(timespan, unit, (int) MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, Schedulers.computation());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<List<T>> m7401(long timespan, TimeUnit unit, int count, Scheduler scheduler) {
        return m7404(new OperatorBufferWithTime(timespan, timespan, unit, count, scheduler));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <R> Observable<R> m7413(Func1<? super T, ? extends Observable<? extends R>> func) {
        if (this instanceof ScalarSynchronousObservable) {
            return ((ScalarSynchronousObservable) this).m24911(func);
        }
        return m7346(new OnSubscribeConcatMap(this, func, 2, 0));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<T> m7381(T defaultValue) {
        return m7391(m7356(defaultValue));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Observable<T> m7391(Observable<? extends T> alternate) {
        if (alternate != null) {
            return m7346(new OnSubscribeSwitchIfEmpty(this, alternate));
        }
        throw new NullPointerException("alternate is null");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<T> m7380(long delay, TimeUnit unit, Scheduler scheduler) {
        return m7404(new OperatorDelay(delay, unit, scheduler));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final <T2> Observable<T2> m7388() {
        return m7404(OperatorDematerialize.m24669());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7411(Action0 onCompleted) {
        return m7346(new OnSubscribeDoOnEach(this, new ActionObserver<>(Actions.m24541(), Actions.m24541(), onCompleted)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7412(Action1<? super Throwable> onError) {
        return m7346(new OnSubscribeDoOnEach(this, new ActionObserver<>(Actions.m24541(), onError, Actions.m24541())));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<T> m7384(Action1<? super T> onNext) {
        return m7346(new OnSubscribeDoOnEach(this, new ActionObserver<>(onNext, Actions.m24541(), Actions.m24541())));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<T> m7383(Action0 subscribe) {
        return m7404(new OperatorDoOnSubscribe(subscribe));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<T> m7385(Func1<? super T, Boolean> predicate) {
        return m7346(new OnSubscribeFilter(this, predicate));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Observable<T> m7395(Action0 action) {
        return m7404(new OperatorDoAfterTerminate(action));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Observable<T> m7374() {
        return m7393(1).m7371();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final <R> Observable<R> m7396(Func1<? super T, ? extends Observable<? extends R>> func) {
        if (getClass() == ScalarSynchronousObservable.class) {
            return ((ScalarSynchronousObservable) this).m24911(func);
        }
        return m7347(m7392(func));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final <R> Observable<R> m7392(Func1<? super T, ? extends R> func) {
        return m7346(new OnSubscribeMap(this, func));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7407(Scheduler scheduler) {
        return m7408(scheduler, RxRingBuffer.f19311);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7408(Scheduler scheduler, int bufferSize) {
        return m7410(scheduler, false, bufferSize);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7410(Scheduler scheduler, boolean delayError, int bufferSize) {
        if (this instanceof ScalarSynchronousObservable) {
            return ((ScalarSynchronousObservable) this).m24913(scheduler);
        }
        return m7404(new OperatorObserveOn(scheduler, delayError, bufferSize));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Observable<T> m7367() {
        return m7404(OperatorOnBackpressureBuffer.m24702());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Observable<T> m7375(Observable<? extends T> resumeSequence) {
        return m7404(OperatorOnErrorResumeNextViaFunction.m24711(resumeSequence));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public final Observable<T> m7376(Func1<? super Throwable, ? extends T> resumeFunction) {
        return m7404(OperatorOnErrorResumeNextViaFunction.m24712(resumeFunction));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public final ConnectableObservable<T> m7370() {
        return OperatorReplay.m24718(this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final ConnectableObservable<T> m7387(int bufferSize) {
        return OperatorReplay.m24719(this, bufferSize);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final ConnectableObservable<T> m7418(int bufferSize, long time, TimeUnit unit, Scheduler scheduler) {
        if (bufferSize >= 0) {
            return OperatorReplay.m24721(this, time, unit, scheduler, bufferSize);
        }
        throw new IllegalArgumentException("bufferSize < 0");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final ConnectableObservable<T> m7398(long time, TimeUnit unit, Scheduler scheduler) {
        return OperatorReplay.m24720(this, time, unit, scheduler);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7400(long count) {
        return OnSubscribeRedo.m24613(this, count);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final Observable<T> m7369(Func1<? super Observable<? extends Throwable>, ? extends Observable<?>> notificationHandler) {
        return OnSubscribeRedo.m24614(this, (Func1<? super Observable<? extends Notification<?>>, ? extends Observable<?>>) InternalObservableUtils.createRetryDematerializer(notificationHandler));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Observable<T> m7394(long period, TimeUnit unit) {
        return m7390(period, unit, Schedulers.computation());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Observable<T> m7390(long period, TimeUnit unit, Scheduler scheduler) {
        return m7404(new OperatorSampleWithTime(period, unit, scheduler));
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Observable<T> m7371() {
        return m7404(OperatorSingle.m24769());
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public final Subscription m7372() {
        return m7386(new ActionSubscriber(Actions.m24541(), InternalObservableUtils.ERROR_NOT_IMPLEMENTED, Actions.m24541()));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Subscription m7397(Action1<? super T> onNext) {
        if (onNext != null) {
            return m7386(new ActionSubscriber(onNext, InternalObservableUtils.ERROR_NOT_IMPLEMENTED, Actions.m24541()));
        }
        throw new IllegalArgumentException("onNext can not be null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Subscription m7417(Action1<? super T> onNext, Action1<Throwable> onError) {
        if (onNext == null) {
            throw new IllegalArgumentException("onNext can not be null");
        } else if (onError != null) {
            return m7386(new ActionSubscriber(onNext, onError, Actions.m24541()));
        } else {
            throw new IllegalArgumentException("onError can not be null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Subscription m7415(Observer<? super T> observer) {
        if (observer instanceof Subscriber) {
            return m7386((Subscriber) observer);
        }
        if (observer != null) {
            return m7386(new ObserverSubscriber(observer));
        }
        throw new NullPointerException("observer is null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Subscription m7416(Subscriber<? super T> subscriber) {
        try {
            subscriber.m24510();
            RxJavaHooks.m25017(this, this.f6506).call(subscriber);
            return RxJavaHooks.m25021((Subscription) subscriber);
        } catch (Throwable e2) {
            Exceptions.m24529(e2);
            RuntimeException r = new OnErrorFailedException("Error occurred attempting to subscribe [" + e.getMessage() + "] and then again while trying to pass to onError.", e2);
            RxJavaHooks.m25011((Throwable) r);
            throw r;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Subscription m7386(Subscriber<? super T> subscriber) {
        return m7366(subscriber, this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> Subscription m7366(Subscriber<? super T> subscriber, Observable<T> observable) {
        if (subscriber == null) {
            throw new IllegalArgumentException("subscriber can not be null");
        } else if (observable.f6506 == null) {
            throw new IllegalStateException("onSubscribe function can not be null.");
        } else {
            subscriber.m24510();
            if (!(subscriber instanceof SafeSubscriber)) {
                subscriber = new SafeSubscriber<>(subscriber);
            }
            try {
                RxJavaHooks.m25017(observable, observable.f6506).call(subscriber);
                return RxJavaHooks.m25021((Subscription) subscriber);
            } catch (Throwable e2) {
                Exceptions.m24529(e2);
                RuntimeException r = new OnErrorFailedException("Error occurred attempting to subscribe [" + e.getMessage() + "] and then again while trying to pass to onError.", e2);
                RxJavaHooks.m25011((Throwable) r);
                throw r;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final Observable<T> m7382(Scheduler scheduler) {
        return m7409(scheduler, !(this.f6506 instanceof OnSubscribeCreate));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7409(Scheduler scheduler, boolean requestOn) {
        if (this instanceof ScalarSynchronousObservable) {
            return ((ScalarSynchronousObservable) this).m24913(scheduler);
        }
        return m7346(new OperatorSubscribeOn(this, scheduler, requestOn));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public final Observable<T> m7393(int count) {
        return m7404(new OperatorTake(count));
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public final <E> Observable<T> m7368(Observable<? extends E> other) {
        return m7404(new OperatorTakeUntil(other));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public final Observable<T> m7389(long timeout, TimeUnit timeUnit) {
        return m7403(timeout, timeUnit, (Observable) null, Schedulers.computation());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7402(long timeout, TimeUnit timeUnit, Observable<? extends T> other) {
        return m7403(timeout, timeUnit, other, Schedulers.computation());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Observable<T> m7403(long timeout, TimeUnit timeUnit, Observable<? extends T> other, Scheduler scheduler) {
        return m7346(new OnSubscribeTimeoutTimedWithFallback(this, timeout, timeUnit, scheduler, other));
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public final Observable<List<T>> m7373() {
        return m7404(OperatorToObservableList.m24779());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final <T2, R> Observable<R> m7406(Observable<? extends T2> other, Func2<? super T, ? super T2, ? extends R> zipFunction) {
        return m7363(this, other, zipFunction);
    }
}
