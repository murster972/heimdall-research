package rx.android.plugins;

import java.util.concurrent.atomic.AtomicReference;

public final class RxAndroidPlugins {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final RxAndroidPlugins f18773 = new RxAndroidPlugins();

    /* renamed from: 靐  reason: contains not printable characters */
    private final AtomicReference<RxAndroidSchedulersHook> f18774 = new AtomicReference<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static RxAndroidPlugins m24514() {
        return f18773;
    }

    RxAndroidPlugins() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public RxAndroidSchedulersHook m24515() {
        if (this.f18774.get() == null) {
            this.f18774.compareAndSet((Object) null, RxAndroidSchedulersHook.m24516());
        }
        return this.f18774.get();
    }
}
