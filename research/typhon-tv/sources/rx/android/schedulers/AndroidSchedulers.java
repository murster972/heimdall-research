package rx.android.schedulers;

import android.os.Looper;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;

public final class AndroidSchedulers {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final AtomicReference<AndroidSchedulers> f18776 = new AtomicReference<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Scheduler f18777;

    /* renamed from: 靐  reason: contains not printable characters */
    private static AndroidSchedulers m24519() {
        AndroidSchedulers current;
        do {
            current = f18776.get();
            if (current != null) {
                break;
            }
            current = new AndroidSchedulers();
        } while (!f18776.compareAndSet((Object) null, current));
        return current;
    }

    private AndroidSchedulers() {
        Scheduler main = RxAndroidPlugins.m24514().m24515().m24517();
        if (main != null) {
            this.f18777 = main;
        } else {
            this.f18777 = new LooperScheduler(Looper.getMainLooper());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Scheduler m24520() {
        return m24519().f18777;
    }
}
