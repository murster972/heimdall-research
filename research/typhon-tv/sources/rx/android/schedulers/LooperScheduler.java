package rx.android.schedulers;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.concurrent.TimeUnit;
import rx.Scheduler;
import rx.Subscription;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.exceptions.OnErrorNotImplementedException;
import rx.functions.Action0;
import rx.plugins.RxJavaPlugins;
import rx.subscriptions.Subscriptions;

class LooperScheduler extends Scheduler {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Handler f18778;

    LooperScheduler(Looper looper) {
        this.f18778 = new Handler(looper);
    }

    public Scheduler.Worker createWorker() {
        return new HandlerWorker(this.f18778);
    }

    static class HandlerWorker extends Scheduler.Worker {

        /* renamed from: 靐  reason: contains not printable characters */
        private final RxAndroidSchedulersHook f18779 = RxAndroidPlugins.m24514().m24515();

        /* renamed from: 齉  reason: contains not printable characters */
        private volatile boolean f18780;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Handler f18781;

        HandlerWorker(Handler handler) {
            this.f18781 = handler;
        }

        public void unsubscribe() {
            this.f18780 = true;
            this.f18781.removeCallbacksAndMessages(this);
        }

        public boolean isUnsubscribed() {
            return this.f18780;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24522(Action0 action, long delayTime, TimeUnit unit) {
            if (this.f18780) {
                return Subscriptions.m25089();
            }
            ScheduledAction scheduledAction = new ScheduledAction(this.f18779.m24518(action), this.f18781);
            Message message = Message.obtain(this.f18781, scheduledAction);
            message.obj = this;
            this.f18781.sendMessageDelayed(message, unit.toMillis(delayTime));
            if (!this.f18780) {
                return scheduledAction;
            }
            this.f18781.removeCallbacks(scheduledAction);
            return Subscriptions.m25089();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m24521(Action0 action) {
            return m24522(action, 0, TimeUnit.MILLISECONDS);
        }
    }

    static final class ScheduledAction implements Runnable, Subscription {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Handler f18782;

        /* renamed from: 齉  reason: contains not printable characters */
        private volatile boolean f18783;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Action0 f18784;

        ScheduledAction(Action0 action, Handler handler) {
            this.f18784 = action;
            this.f18782 = handler;
        }

        public void run() {
            IllegalStateException ie;
            try {
                this.f18784.m24539();
            } catch (Throwable e) {
                if (e instanceof OnErrorNotImplementedException) {
                    ie = new IllegalStateException("Exception thrown on Scheduler.Worker thread. Add `onError` handling.", e);
                } else {
                    ie = new IllegalStateException("Fatal Exception thrown on Scheduler.Worker thread.", e);
                }
                RxJavaPlugins.m7437().m7440().m7429((Throwable) ie);
                Thread thread = Thread.currentThread();
                thread.getUncaughtExceptionHandler().uncaughtException(thread, ie);
            }
        }

        public void unsubscribe() {
            this.f18783 = true;
            this.f18782.removeCallbacks(this);
        }

        public boolean isUnsubscribed() {
            return this.f18783;
        }
    }
}
