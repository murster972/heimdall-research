package rx.observers;

import java.util.Arrays;
import java.util.Collection;
import rx.Observer;
import rx.Subscriber;
import rx.exceptions.CompositeException;
import rx.exceptions.Exceptions;
import rx.exceptions.OnErrorFailedException;
import rx.exceptions.OnErrorNotImplementedException;
import rx.exceptions.UnsubscribeFailedException;
import rx.plugins.RxJavaHooks;
import rx.plugins.RxJavaPlugins;

public class SafeSubscriber<T> extends Subscriber<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Subscriber<? super T> f19386;

    /* renamed from: 龘  reason: contains not printable characters */
    boolean f19387;

    public SafeSubscriber(Subscriber<? super T> actual) {
        super(actual);
        this.f19386 = actual;
    }

    public void onCompleted() {
        if (!this.f19387) {
            this.f19387 = true;
            try {
                this.f19386.onCompleted();
                try {
                    unsubscribe();
                } catch (Throwable e) {
                    RxJavaHooks.m25024(e);
                    throw new UnsubscribeFailedException(e.getMessage(), e);
                }
            } catch (Throwable th) {
                try {
                    unsubscribe();
                    throw th;
                } catch (Throwable e2) {
                    RxJavaHooks.m25024(e2);
                    throw new UnsubscribeFailedException(e2.getMessage(), e2);
                }
            }
        }
    }

    public void onError(Throwable e) {
        Exceptions.m24529(e);
        if (!this.f19387) {
            this.f19387 = true;
            m25001(e);
        }
    }

    public void onNext(T t) {
        try {
            if (!this.f19387) {
                this.f19386.onNext(t);
            }
        } catch (Throwable e) {
            Exceptions.m24533(e, (Observer<?>) this);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25001(Throwable e) {
        RxJavaPlugins.m7437().m7440().m7429(e);
        try {
            this.f19386.onError(e);
            try {
                unsubscribe();
            } catch (Throwable unsubscribeException) {
                RxJavaHooks.m25024(unsubscribeException);
                throw new OnErrorFailedException(unsubscribeException);
            }
        } catch (OnErrorNotImplementedException e2) {
            unsubscribe();
            throw e2;
        } catch (Throwable unsubscribeException2) {
            RxJavaHooks.m25024(unsubscribeException2);
            throw new OnErrorFailedException("Error occurred when trying to propagate error to Observer.onError and during unsubscription.", new CompositeException((Collection<? extends Throwable>) Arrays.asList(new Throwable[]{e, e2, unsubscribeException2})));
        }
    }
}
