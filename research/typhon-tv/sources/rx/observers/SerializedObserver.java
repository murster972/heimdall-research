package rx.observers;

import rx.Observer;
import rx.exceptions.Exceptions;
import rx.internal.operators.NotificationLite;

public class SerializedObserver<T> implements Observer<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean f19388;

    /* renamed from: 麤  reason: contains not printable characters */
    private FastList f19389;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile boolean f19390;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Observer<? super T> f19391;

    static final class FastList {

        /* renamed from: 靐  reason: contains not printable characters */
        int f19392;

        /* renamed from: 龘  reason: contains not printable characters */
        Object[] f19393;

        FastList() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m25002(Object o) {
            int s = this.f19392;
            Object[] a = this.f19393;
            if (a == null) {
                a = new Object[16];
                this.f19393 = a;
            } else if (s == a.length) {
                Object[] array2 = new Object[((s >> 2) + s)];
                System.arraycopy(a, 0, array2, 0, s);
                a = array2;
                this.f19393 = a;
            }
            a[s] = o;
            this.f19392 = s + 1;
        }
    }

    public SerializedObserver(Observer<? super T> s) {
        this.f19391 = s;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r9.f19391.onNext(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0032, code lost:
        monitor-enter(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r1 = r9.f19389;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0035, code lost:
        if (r1 != null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0037, code lost:
        r9.f19388 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x003a, code lost:
        monitor-exit(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x003f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0040, code lost:
        r9.f19390 = true;
        rx.exceptions.Exceptions.m24534(r0, r9.f19391, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r9.f19389 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x004b, code lost:
        monitor-exit(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x004c, code lost:
        r5 = r1.f19393;
        r6 = r5.length;
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0050, code lost:
        if (r3 >= r6) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0052, code lost:
        r2 = r5[r3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0054, code lost:
        if (r2 == null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x005c, code lost:
        if (rx.internal.operators.NotificationLite.m24569(r9.f19391, r2) == false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x005e, code lost:
        r9.f19390 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0062, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0063, code lost:
        r9.f19390 = true;
        rx.exceptions.Exceptions.m24529(r0);
        r9.f19391.onError(rx.exceptions.OnErrorThrowable.addValueAsLastCause(r0, r10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0072, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onNext(T r10) {
        /*
            r9 = this;
            r4 = 0
            r8 = 1
            boolean r3 = r9.f19390
            if (r3 == 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            monitor-enter(r9)
            boolean r3 = r9.f19390     // Catch:{ all -> 0x000e }
            if (r3 == 0) goto L_0x0011
            monitor-exit(r9)     // Catch:{ all -> 0x000e }
            goto L_0x0006
        L_0x000e:
            r3 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x000e }
            throw r3
        L_0x0011:
            boolean r3 = r9.f19388     // Catch:{ all -> 0x000e }
            if (r3 == 0) goto L_0x0029
            rx.observers.SerializedObserver$FastList r1 = r9.f19389     // Catch:{ all -> 0x000e }
            if (r1 != 0) goto L_0x0020
            rx.observers.SerializedObserver$FastList r1 = new rx.observers.SerializedObserver$FastList     // Catch:{ all -> 0x000e }
            r1.<init>()     // Catch:{ all -> 0x000e }
            r9.f19389 = r1     // Catch:{ all -> 0x000e }
        L_0x0020:
            java.lang.Object r3 = rx.internal.operators.NotificationLite.m24567(r10)     // Catch:{ all -> 0x000e }
            r1.m25002(r3)     // Catch:{ all -> 0x000e }
            monitor-exit(r9)     // Catch:{ all -> 0x000e }
            goto L_0x0006
        L_0x0029:
            r3 = 1
            r9.f19388 = r3     // Catch:{ all -> 0x000e }
            monitor-exit(r9)     // Catch:{ all -> 0x000e }
            rx.Observer<? super T> r3 = r9.f19391     // Catch:{ Throwable -> 0x003f }
            r3.onNext(r10)     // Catch:{ Throwable -> 0x003f }
        L_0x0032:
            monitor-enter(r9)
            rx.observers.SerializedObserver$FastList r1 = r9.f19389     // Catch:{ all -> 0x003c }
            if (r1 != 0) goto L_0x0048
            r3 = 0
            r9.f19388 = r3     // Catch:{ all -> 0x003c }
            monitor-exit(r9)     // Catch:{ all -> 0x003c }
            goto L_0x0006
        L_0x003c:
            r3 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x003c }
            throw r3
        L_0x003f:
            r0 = move-exception
            r9.f19390 = r8
            rx.Observer<? super T> r3 = r9.f19391
            rx.exceptions.Exceptions.m24534(r0, r3, r10)
            goto L_0x0006
        L_0x0048:
            r3 = 0
            r9.f19389 = r3     // Catch:{ all -> 0x003c }
            monitor-exit(r9)     // Catch:{ all -> 0x003c }
            java.lang.Object[] r5 = r1.f19393
            int r6 = r5.length
            r3 = r4
        L_0x0050:
            if (r3 >= r6) goto L_0x0032
            r2 = r5[r3]
            if (r2 == 0) goto L_0x0032
            rx.Observer<? super T> r7 = r9.f19391     // Catch:{ Throwable -> 0x0062 }
            boolean r7 = rx.internal.operators.NotificationLite.m24569(r7, r2)     // Catch:{ Throwable -> 0x0062 }
            if (r7 == 0) goto L_0x0072
            r3 = 1
            r9.f19390 = r3     // Catch:{ Throwable -> 0x0062 }
            goto L_0x0006
        L_0x0062:
            r0 = move-exception
            r9.f19390 = r8
            rx.exceptions.Exceptions.m24529(r0)
            rx.Observer<? super T> r3 = r9.f19391
            java.lang.Throwable r4 = rx.exceptions.OnErrorThrowable.addValueAsLastCause(r0, r10)
            r3.onError(r4)
            goto L_0x0006
        L_0x0072:
            int r3 = r3 + 1
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.observers.SerializedObserver.onNext(java.lang.Object):void");
    }

    public void onError(Throwable e) {
        Exceptions.m24529(e);
        if (!this.f19390) {
            synchronized (this) {
                if (!this.f19390) {
                    this.f19390 = true;
                    if (this.f19388) {
                        FastList list = this.f19389;
                        if (list == null) {
                            list = new FastList();
                            this.f19389 = list;
                        }
                        list.m25002(NotificationLite.m24568(e));
                        return;
                    }
                    this.f19388 = true;
                    this.f19391.onError(e);
                }
            }
        }
    }

    public void onCompleted() {
        if (!this.f19390) {
            synchronized (this) {
                if (!this.f19390) {
                    this.f19390 = true;
                    if (this.f19388) {
                        FastList list = this.f19389;
                        if (list == null) {
                            list = new FastList();
                            this.f19389 = list;
                        }
                        list.m25002(NotificationLite.m24566());
                        return;
                    }
                    this.f19388 = true;
                    this.f19391.onCompleted();
                }
            }
        }
    }
}
