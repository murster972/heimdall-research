package rx.observers;

import rx.Observer;
import rx.Subscriber;

public class SerializedSubscriber<T> extends Subscriber<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private final Observer<T> f19394;

    public SerializedSubscriber(Subscriber<? super T> s) {
        this(s, true);
    }

    public SerializedSubscriber(Subscriber<? super T> s, boolean shareSubscriptions) {
        super(s, shareSubscriptions);
        this.f19394 = new SerializedObserver(s);
    }

    public void onCompleted() {
        this.f19394.onCompleted();
    }

    public void onError(Throwable e) {
        this.f19394.onError(e);
    }

    public void onNext(T t) {
        this.f19394.onNext(t);
    }
}
