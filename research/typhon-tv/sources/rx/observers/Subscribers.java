package rx.observers;

import rx.Observer;
import rx.Subscriber;

public final class Subscribers {
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Subscriber<T> m25003() {
        return m25004(Observers.m25000());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Subscriber<T> m25004(final Observer<? super T> o) {
        return new Subscriber<T>() {
            public void onCompleted() {
                o.onCompleted();
            }

            public void onError(Throwable e) {
                o.onError(e);
            }

            public void onNext(T t) {
                o.onNext(t);
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Subscriber<T> m25005(final Subscriber<? super T> subscriber) {
        return new Subscriber<T>(subscriber) {
            public void onCompleted() {
                subscriber.onCompleted();
            }

            public void onError(Throwable e) {
                subscriber.onError(e);
            }

            public void onNext(T t) {
                subscriber.onNext(t);
            }
        };
    }
}
