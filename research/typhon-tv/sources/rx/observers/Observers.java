package rx.observers;

import rx.Observer;
import rx.exceptions.OnErrorNotImplementedException;

public final class Observers {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Observer<Object> f19385 = new Observer<Object>() {
        public final void onCompleted() {
        }

        public final void onError(Throwable e) {
            throw new OnErrorNotImplementedException(e);
        }

        public final void onNext(Object args) {
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observer<T> m25000() {
        return f19385;
    }
}
