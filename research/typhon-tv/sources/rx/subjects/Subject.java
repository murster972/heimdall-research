package rx.subjects;

import rx.Observable;
import rx.Observer;

public abstract class Subject<T, R> extends Observable<R> implements Observer<T> {
    protected Subject(Observable.OnSubscribe<R> onSubscribe) {
        super(onSubscribe);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public final SerializedSubject<T, R> m25066() {
        if (getClass() == SerializedSubject.class) {
            return (SerializedSubject) this;
        }
        return new SerializedSubject<>(this);
    }
}
