package rx.subjects;

import rx.Observable;
import rx.Subscriber;
import rx.observers.SerializedObserver;

public class SerializedSubject<T, R> extends Subject<T, R> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final SerializedObserver<T> f19445;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Subject<T, R> f19446;

    public SerializedSubject(final Subject<T, R> actual) {
        super(new Observable.OnSubscribe<R>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Subscriber<? super R> child) {
                Subject.this.m7416(child);
            }
        });
        this.f19446 = actual;
        this.f19445 = new SerializedObserver<>(actual);
    }

    public void onCompleted() {
        this.f19445.onCompleted();
    }

    public void onError(Throwable e) {
        this.f19445.onError(e);
    }

    public void onNext(T t) {
        this.f19445.onNext(t);
    }
}
