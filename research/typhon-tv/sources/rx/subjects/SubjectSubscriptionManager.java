package rx.subjects;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Actions;
import rx.internal.operators.NotificationLite;
import rx.subscriptions.Subscriptions;

final class SubjectSubscriptionManager<T> extends AtomicReference<State<T>> implements Observable.OnSubscribe<T> {
    private static final long serialVersionUID = 6035251036011671568L;
    boolean active = true;
    volatile Object latest;
    Action1<SubjectObserver<T>> onAdded = Actions.m24541();
    Action1<SubjectObserver<T>> onStart = Actions.m24541();
    Action1<SubjectObserver<T>> onTerminated = Actions.m24541();

    public SubjectSubscriptionManager() {
        super(State.f19450);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void call(Subscriber<? super T> child) {
        SubjectObserver<T> bo = new SubjectObserver<>(child);
        m25073(child, bo);
        this.onStart.call(bo);
        if (!child.isUnsubscribed() && m25074(bo) && child.isUnsubscribed()) {
            m25067(bo);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25073(Subscriber<? super T> child, final SubjectObserver<T> bo) {
        child.m24513(Subscriptions.m25090(new Action0() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m25075() {
                SubjectSubscriptionManager.this.m25067(bo);
            }
        }));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m25071(Object value) {
        this.latest = value;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Object m25070() {
        return this.latest;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m25074(SubjectObserver<T> o) {
        State oldState;
        do {
            oldState = (State) get();
            if (oldState.f19454) {
                this.onTerminated.call(o);
                return false;
            }
        } while (!compareAndSet(oldState, oldState.m25077(o)));
        this.onAdded.call(o);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m25067(rx.subjects.SubjectSubscriptionManager.SubjectObserver<T> r4) {
        /*
            r3 = this;
        L_0x0000:
            java.lang.Object r1 = r3.get()
            rx.subjects.SubjectSubscriptionManager$State r1 = (rx.subjects.SubjectSubscriptionManager.State) r1
            boolean r2 = r1.f19454
            if (r2 == 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            rx.subjects.SubjectSubscriptionManager$State r0 = r1.m25076(r4)
            if (r0 == r1) goto L_0x000a
            boolean r2 = r3.compareAndSet(r1, r0)
            if (r2 == 0) goto L_0x0000
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.subjects.SubjectSubscriptionManager.m25067(rx.subjects.SubjectSubscriptionManager$SubjectObserver):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public SubjectObserver<T>[] m25068(Object n) {
        m25071(n);
        return ((State) get()).f19453;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public SubjectObserver<T>[] m25069(Object n) {
        m25071(n);
        this.active = false;
        if (((State) get()).f19454) {
            return State.f19452;
        }
        return ((State) getAndSet(State.f19451)).f19453;
    }

    protected static final class State<T> {

        /* renamed from: 连任  reason: contains not printable characters */
        static final State f19450 = new State(false, f19452);

        /* renamed from: 麤  reason: contains not printable characters */
        static final State f19451 = new State(true, f19452);

        /* renamed from: 齉  reason: contains not printable characters */
        static final SubjectObserver[] f19452 = new SubjectObserver[0];

        /* renamed from: 靐  reason: contains not printable characters */
        final SubjectObserver[] f19453;

        /* renamed from: 龘  reason: contains not printable characters */
        final boolean f19454;

        public State(boolean terminated, SubjectObserver[] observers) {
            this.f19454 = terminated;
            this.f19453 = observers;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public State m25077(SubjectObserver o) {
            int n = this.f19453.length;
            SubjectObserver[] b = new SubjectObserver[(n + 1)];
            System.arraycopy(this.f19453, 0, b, 0, n);
            b[n] = o;
            return new State(this.f19454, b);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public State m25076(SubjectObserver o) {
            int j;
            SubjectObserver[] a = this.f19453;
            int n = a.length;
            if (n == 1 && a[0] == o) {
                return f19450;
            }
            if (n == 0) {
                return this;
            }
            SubjectObserver[] b = new SubjectObserver[(n - 1)];
            int i = 0;
            int j2 = 0;
            while (i < n) {
                SubjectObserver ai = a[i];
                if (ai == o) {
                    j = j2;
                } else if (j2 == n - 1) {
                    return this;
                } else {
                    j = j2 + 1;
                    b[j2] = ai;
                }
                i++;
                j2 = j;
            }
            if (j2 == 0) {
                return f19450;
            }
            if (j2 < n - 1) {
                SubjectObserver[] c = new SubjectObserver[j2];
                System.arraycopy(b, 0, c, 0, j2);
                b = c;
            }
            return new State<>(this.f19454, b);
        }
    }

    protected static final class SubjectObserver<T> implements Observer<T> {

        /* renamed from: 连任  reason: contains not printable characters */
        boolean f19455;

        /* renamed from: 靐  reason: contains not printable characters */
        boolean f19456 = true;

        /* renamed from: 麤  reason: contains not printable characters */
        List<Object> f19457;

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f19458;

        /* renamed from: 龘  reason: contains not printable characters */
        final Subscriber<? super T> f19459;

        public SubjectObserver(Subscriber<? super T> actual) {
            this.f19459 = actual;
        }

        public void onNext(T t) {
            this.f19459.onNext(t);
        }

        public void onError(Throwable e) {
            this.f19459.onError(e);
        }

        public void onCompleted() {
            this.f19459.onCompleted();
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
            r1.f19455 = true;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m25080(java.lang.Object r2) {
            /*
                r1 = this;
                boolean r0 = r1.f19455
                if (r0 != 0) goto L_0x0022
                monitor-enter(r1)
                r0 = 0
                r1.f19456 = r0     // Catch:{ all -> 0x0028 }
                boolean r0 = r1.f19458     // Catch:{ all -> 0x0028 }
                if (r0 == 0) goto L_0x001e
                java.util.List<java.lang.Object> r0 = r1.f19457     // Catch:{ all -> 0x0028 }
                if (r0 != 0) goto L_0x0017
                java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0028 }
                r0.<init>()     // Catch:{ all -> 0x0028 }
                r1.f19457 = r0     // Catch:{ all -> 0x0028 }
            L_0x0017:
                java.util.List<java.lang.Object> r0 = r1.f19457     // Catch:{ all -> 0x0028 }
                r0.add(r2)     // Catch:{ all -> 0x0028 }
                monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            L_0x001d:
                return
            L_0x001e:
                monitor-exit(r1)     // Catch:{ all -> 0x0028 }
                r0 = 1
                r1.f19455 = r0
            L_0x0022:
                rx.Subscriber<? super T> r0 = r1.f19459
                rx.internal.operators.NotificationLite.m24569(r0, r2)
                goto L_0x001d
            L_0x0028:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0028 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.subjects.SubjectSubscriptionManager.SubjectObserver.m25080(java.lang.Object):void");
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0015, code lost:
            if (r3 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0017, code lost:
            m25081((java.util.List<java.lang.Object>) null, r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            return;
         */
        /* renamed from: 靐  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m25078(java.lang.Object r3) {
            /*
                r2 = this;
                r0 = 0
                monitor-enter(r2)
                boolean r1 = r2.f19456     // Catch:{ all -> 0x001c }
                if (r1 == 0) goto L_0x000a
                boolean r1 = r2.f19458     // Catch:{ all -> 0x001c }
                if (r1 == 0) goto L_0x000c
            L_0x000a:
                monitor-exit(r2)     // Catch:{ all -> 0x001c }
            L_0x000b:
                return
            L_0x000c:
                r1 = 0
                r2.f19456 = r1     // Catch:{ all -> 0x001c }
                if (r3 == 0) goto L_0x0012
                r0 = 1
            L_0x0012:
                r2.f19458 = r0     // Catch:{ all -> 0x001c }
                monitor-exit(r2)     // Catch:{ all -> 0x001c }
                if (r3 == 0) goto L_0x000b
                r0 = 0
                r2.m25081(r0, r3)
                goto L_0x000b
            L_0x001c:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x001c }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.subjects.SubjectSubscriptionManager.SubjectObserver.m25078(java.lang.Object):void");
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0032, code lost:
            if (1 != 0) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0034, code lost:
            monitor-enter(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            r5.f19458 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0038, code lost:
            monitor-exit(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
            return;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void m25081(java.util.List<java.lang.Object> r6, java.lang.Object r7) {
            /*
                r5 = this;
                r1 = 1
                r2 = 0
            L_0x0002:
                if (r6 == 0) goto L_0x001f
                java.util.Iterator r3 = r6.iterator()     // Catch:{ all -> 0x0016 }
            L_0x0008:
                boolean r4 = r3.hasNext()     // Catch:{ all -> 0x0016 }
                if (r4 == 0) goto L_0x001f
                java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x0016 }
                r5.m25079(r0)     // Catch:{ all -> 0x0016 }
                goto L_0x0008
            L_0x0016:
                r3 = move-exception
                if (r2 != 0) goto L_0x001e
                monitor-enter(r5)
                r4 = 0
                r5.f19458 = r4     // Catch:{ all -> 0x0042 }
                monitor-exit(r5)     // Catch:{ all -> 0x0042 }
            L_0x001e:
                throw r3
            L_0x001f:
                if (r1 == 0) goto L_0x0025
                r1 = 0
                r5.m25079(r7)     // Catch:{ all -> 0x0016 }
            L_0x0025:
                monitor-enter(r5)     // Catch:{ all -> 0x0016 }
                java.util.List<java.lang.Object> r6 = r5.f19457     // Catch:{ all -> 0x003c }
                r3 = 0
                r5.f19457 = r3     // Catch:{ all -> 0x003c }
                if (r6 != 0) goto L_0x003a
                r3 = 0
                r5.f19458 = r3     // Catch:{ all -> 0x003c }
                r2 = 1
                monitor-exit(r5)     // Catch:{ all -> 0x003c }
                if (r2 != 0) goto L_0x0039
                monitor-enter(r5)
                r3 = 0
                r5.f19458 = r3     // Catch:{ all -> 0x003f }
                monitor-exit(r5)     // Catch:{ all -> 0x003f }
            L_0x0039:
                return
            L_0x003a:
                monitor-exit(r5)     // Catch:{ all -> 0x003c }
                goto L_0x0002
            L_0x003c:
                r3 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x003c }
                throw r3     // Catch:{ all -> 0x0016 }
            L_0x003f:
                r3 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x003f }
                throw r3
            L_0x0042:
                r3 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0042 }
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: rx.subjects.SubjectSubscriptionManager.SubjectObserver.m25081(java.util.List, java.lang.Object):void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public void m25079(Object n) {
            if (n != null) {
                NotificationLite.m24569(this.f19459, n);
            }
        }
    }
}
