package rx.subjects;

import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.exceptions.Exceptions;
import rx.functions.Action1;
import rx.internal.operators.NotificationLite;
import rx.subjects.SubjectSubscriptionManager;

public final class BehaviorSubject<T> extends Subject<T, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object[] f19439 = new Object[0];

    /* renamed from: 齉  reason: contains not printable characters */
    private final SubjectSubscriptionManager<T> f19440;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static <T> BehaviorSubject<T> m25058() {
        return m25059((Object) null, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <T> BehaviorSubject<T> m25059(T defaultValue, boolean hasDefault) {
        final SubjectSubscriptionManager<T> state = new SubjectSubscriptionManager<>();
        if (hasDefault) {
            state.m25071(NotificationLite.m24567(defaultValue));
        }
        state.onAdded = new Action1<SubjectSubscriptionManager.SubjectObserver<T>>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(SubjectSubscriptionManager.SubjectObserver<T> o) {
                o.m25078(state.m25070());
            }
        };
        state.onTerminated = state.onAdded;
        return new BehaviorSubject<>(state, state);
    }

    protected BehaviorSubject(Observable.OnSubscribe<T> onSubscribe, SubjectSubscriptionManager<T> state) {
        super(onSubscribe);
        this.f19440 = state;
    }

    public void onCompleted() {
        if (this.f19440.m25070() == null || this.f19440.active) {
            Object n = NotificationLite.m24566();
            for (SubjectSubscriptionManager.SubjectObserver<T> bo : this.f19440.m25069(n)) {
                bo.m25080(n);
            }
        }
    }

    public void onError(Throwable e) {
        if (this.f19440.m25070() == null || this.f19440.active) {
            Object n = NotificationLite.m24568(e);
            List<Throwable> errors = null;
            for (SubjectSubscriptionManager.SubjectObserver<T> bo : this.f19440.m25069(n)) {
                try {
                    bo.m25080(n);
                } catch (Throwable e2) {
                    if (errors == null) {
                        errors = new ArrayList<>();
                    }
                    errors.add(e2);
                }
            }
            Exceptions.m24536((List<? extends Throwable>) errors);
        }
    }

    public void onNext(T v) {
        if (this.f19440.m25070() == null || this.f19440.active) {
            Object n = NotificationLite.m24567(v);
            for (SubjectSubscriptionManager.SubjectObserver<T> bo : this.f19440.m25068(n)) {
                bo.m25080(n);
            }
        }
    }
}
