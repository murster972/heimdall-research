package rx.subjects;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import rx.Observable;
import rx.Observer;
import rx.Producer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;
import rx.exceptions.MissingBackpressureException;
import rx.internal.operators.BackpressureUtils;

public final class PublishSubject<T> extends Subject<T, T> {

    /* renamed from: 靐  reason: contains not printable characters */
    final PublishSubjectState<T> f19442;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static <T> PublishSubject<T> m25061() {
        return new PublishSubject<>(new PublishSubjectState());
    }

    protected PublishSubject(PublishSubjectState<T> state) {
        super(state);
        this.f19442 = state;
    }

    public void onNext(T v) {
        this.f19442.onNext(v);
    }

    public void onError(Throwable e) {
        this.f19442.onError(e);
    }

    public void onCompleted() {
        this.f19442.onCompleted();
    }

    static final class PublishSubjectState<T> extends AtomicReference<PublishSubjectProducer<T>[]> implements Observable.OnSubscribe<T>, Observer<T> {
        private static final long serialVersionUID = -7568940796666027140L;

        /* renamed from: 靐  reason: contains not printable characters */
        static final PublishSubjectProducer[] f19443 = new PublishSubjectProducer[0];

        /* renamed from: 龘  reason: contains not printable characters */
        static final PublishSubjectProducer[] f19444 = new PublishSubjectProducer[0];
        Throwable error;

        public PublishSubjectState() {
            lazySet(f19444);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void call(Subscriber<? super T> t) {
            PublishSubjectProducer<T> pp = new PublishSubjectProducer<>(this, t);
            t.m24513((Subscription) pp);
            t.m24512((Producer) pp);
            if (!m25064(pp)) {
                Throwable ex = this.error;
                if (ex != null) {
                    t.onError(ex);
                } else {
                    t.onCompleted();
                }
            } else if (pp.isUnsubscribed()) {
                m25062(pp);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m25064(PublishSubjectProducer<T> inner) {
            PublishSubjectProducer<T>[] curr;
            PublishSubjectProducer<T>[] next;
            do {
                curr = (PublishSubjectProducer[]) get();
                if (curr == f19443) {
                    return false;
                }
                int n = curr.length;
                next = new PublishSubjectProducer[(n + 1)];
                System.arraycopy(curr, 0, next, 0, n);
                next[n] = inner;
            } while (!compareAndSet(curr, next));
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public void m25062(PublishSubjectProducer<T> inner) {
            PublishSubjectProducer<T>[] curr;
            PublishSubjectProducer<T>[] next;
            do {
                curr = (PublishSubjectProducer[]) get();
                if (curr != f19443 && curr != f19444) {
                    int n = curr.length;
                    int j = -1;
                    int i = 0;
                    while (true) {
                        if (i >= n) {
                            break;
                        } else if (curr[i] == inner) {
                            j = i;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (j < 0) {
                        return;
                    }
                    if (n == 1) {
                        next = f19444;
                    } else {
                        next = new PublishSubjectProducer[(n - 1)];
                        System.arraycopy(curr, 0, next, 0, j);
                        System.arraycopy(curr, j + 1, next, j, (n - j) - 1);
                    }
                } else {
                    return;
                }
            } while (!compareAndSet(curr, next));
        }

        public void onNext(T t) {
            for (PublishSubjectProducer<T> pp : (PublishSubjectProducer[]) get()) {
                pp.onNext(t);
            }
        }

        public void onError(Throwable e) {
            this.error = e;
            List<Throwable> errors = null;
            for (PublishSubjectProducer<T> pp : (PublishSubjectProducer[]) getAndSet(f19443)) {
                try {
                    pp.onError(e);
                } catch (Throwable ex) {
                    if (errors == null) {
                        errors = new ArrayList<>(1);
                    }
                    errors.add(ex);
                }
            }
            Exceptions.m24536((List<? extends Throwable>) errors);
        }

        public void onCompleted() {
            for (PublishSubjectProducer<T> pp : (PublishSubjectProducer[]) getAndSet(f19443)) {
                pp.onCompleted();
            }
        }
    }

    static final class PublishSubjectProducer<T> extends AtomicLong implements Observer<T>, Producer, Subscription {
        private static final long serialVersionUID = 6451806817170721536L;
        final Subscriber<? super T> actual;
        final PublishSubjectState<T> parent;
        long produced;

        public PublishSubjectProducer(PublishSubjectState<T> parent2, Subscriber<? super T> actual2) {
            this.parent = parent2;
            this.actual = actual2;
        }

        public void request(long n) {
            long r;
            if (BackpressureUtils.m24555(n)) {
                do {
                    r = get();
                    if (r == Long.MIN_VALUE || compareAndSet(r, BackpressureUtils.m24548(r, n))) {
                    }
                    r = get();
                    return;
                } while (compareAndSet(r, BackpressureUtils.m24548(r, n)));
            }
        }

        public boolean isUnsubscribed() {
            return get() == Long.MIN_VALUE;
        }

        public void unsubscribe() {
            if (getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE) {
                this.parent.m25062(this);
            }
        }

        public void onNext(T t) {
            long r = get();
            if (r != Long.MIN_VALUE) {
                long p = this.produced;
                if (r != p) {
                    this.produced = 1 + p;
                    this.actual.onNext(t);
                    return;
                }
                unsubscribe();
                this.actual.onError(new MissingBackpressureException("PublishSubject: could not emit value due to lack of requests"));
            }
        }

        public void onError(Throwable e) {
            if (get() != Long.MIN_VALUE) {
                this.actual.onError(e);
            }
        }

        public void onCompleted() {
            if (get() != Long.MIN_VALUE) {
                this.actual.onCompleted();
            }
        }
    }
}
