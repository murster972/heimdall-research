package rx.subscriptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import rx.Subscription;
import rx.exceptions.Exceptions;

public final class CompositeSubscription implements Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    private volatile boolean f19462;

    /* renamed from: 龘  reason: contains not printable characters */
    private Set<Subscription> f19463;

    public boolean isUnsubscribed() {
        return this.f19462;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25086(Subscription s) {
        if (!s.isUnsubscribed()) {
            if (!this.f19462) {
                synchronized (this) {
                    if (!this.f19462) {
                        if (this.f19463 == null) {
                            this.f19463 = new HashSet(4);
                        }
                        this.f19463.add(s);
                        return;
                    }
                }
            }
            s.unsubscribe();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0016, code lost:
        if (r0 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        r3.unsubscribe();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m25085(rx.Subscription r3) {
        /*
            r2 = this;
            boolean r1 = r2.f19462
            if (r1 != 0) goto L_0x000e
            monitor-enter(r2)
            boolean r1 = r2.f19462     // Catch:{ all -> 0x001c }
            if (r1 != 0) goto L_0x000d
            java.util.Set<rx.Subscription> r1 = r2.f19463     // Catch:{ all -> 0x001c }
            if (r1 != 0) goto L_0x000f
        L_0x000d:
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
        L_0x000e:
            return
        L_0x000f:
            java.util.Set<rx.Subscription> r1 = r2.f19463     // Catch:{ all -> 0x001c }
            boolean r0 = r1.remove(r3)     // Catch:{ all -> 0x001c }
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x000e
            r3.unsubscribe()
            goto L_0x000e
        L_0x001c:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: rx.subscriptions.CompositeSubscription.m25085(rx.Subscription):void");
    }

    public void unsubscribe() {
        if (!this.f19462) {
            synchronized (this) {
                if (!this.f19462) {
                    this.f19462 = true;
                    Collection<Subscription> unsubscribe = this.f19463;
                    this.f19463 = null;
                    m25084(unsubscribe);
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m25084(Collection<Subscription> subscriptions) {
        if (subscriptions != null) {
            List<Throwable> es = null;
            for (Subscription s : subscriptions) {
                try {
                    s.unsubscribe();
                } catch (Throwable e) {
                    if (es == null) {
                        es = new ArrayList<>();
                    }
                    es.add(e);
                }
            }
            Exceptions.m24536((List<? extends Throwable>) es);
        }
    }
}
