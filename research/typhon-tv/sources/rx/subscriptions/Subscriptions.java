package rx.subscriptions;

import rx.Subscription;
import rx.functions.Action0;

public final class Subscriptions {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Unsubscribed f19466 = new Unsubscribed();

    /* renamed from: 龘  reason: contains not printable characters */
    public static Subscription m25089() {
        return f19466;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Subscription m25090(Action0 unsubscribe) {
        return BooleanSubscription.m25082(unsubscribe);
    }

    static final class Unsubscribed implements Subscription {
        Unsubscribed() {
        }

        public void unsubscribe() {
        }

        public boolean isUnsubscribed() {
            return true;
        }
    }
}
