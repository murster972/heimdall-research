package rx.subscriptions;

import rx.Subscription;
import rx.internal.subscriptions.SequentialSubscription;

public final class SerialSubscription implements Subscription {

    /* renamed from: 龘  reason: contains not printable characters */
    final SequentialSubscription f19465 = new SequentialSubscription();

    public boolean isUnsubscribed() {
        return this.f19465.isUnsubscribed();
    }

    public void unsubscribe() {
        this.f19465.unsubscribe();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25088(Subscription s) {
        if (s == null) {
            throw new IllegalArgumentException("Subscription can not be null");
        }
        this.f19465.update(s);
    }
}
