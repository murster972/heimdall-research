package rx.subscriptions;

import rx.Subscription;
import rx.internal.subscriptions.SequentialSubscription;

public final class MultipleAssignmentSubscription implements Subscription {

    /* renamed from: 龘  reason: contains not printable characters */
    final SequentialSubscription f19464 = new SequentialSubscription();

    public boolean isUnsubscribed() {
        return this.f19464.isUnsubscribed();
    }

    public void unsubscribe() {
        this.f19464.unsubscribe();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m25087(Subscription s) {
        if (s == null) {
            throw new IllegalArgumentException("Subscription can not be null");
        }
        this.f19464.replace(s);
    }
}
