package rx.subscriptions;

import java.util.concurrent.atomic.AtomicReference;
import rx.Subscription;
import rx.functions.Action0;

public final class BooleanSubscription implements Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Action0 f19460 = new Action0() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void m25083() {
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    final AtomicReference<Action0> f19461;

    public BooleanSubscription() {
        this.f19461 = new AtomicReference<>();
    }

    private BooleanSubscription(Action0 action) {
        this.f19461 = new AtomicReference<>(action);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static BooleanSubscription m25082(Action0 onUnsubscribe) {
        return new BooleanSubscription(onUnsubscribe);
    }

    public boolean isUnsubscribed() {
        return this.f19461.get() == f19460;
    }

    public void unsubscribe() {
        Action0 action;
        if (this.f19461.get() != f19460 && (action = this.f19461.getAndSet(f19460)) != null && action != f19460) {
            action.m24539();
        }
    }
}
