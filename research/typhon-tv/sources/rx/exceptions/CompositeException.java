package rx.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class CompositeException extends RuntimeException {
    private static final long serialVersionUID = 3026362227162912146L;
    private Throwable cause;
    private final List<Throwable> exceptions;
    private final String message;

    @Deprecated
    public CompositeException(String messagePrefix, Collection<? extends Throwable> errors) {
        Set<Throwable> deDupedExceptions = new LinkedHashSet<>();
        List<Throwable> localExceptions = new ArrayList<>();
        if (errors != null) {
            for (Throwable ex : errors) {
                if (ex instanceof CompositeException) {
                    deDupedExceptions.addAll(((CompositeException) ex).getExceptions());
                } else if (ex != null) {
                    deDupedExceptions.add(ex);
                } else {
                    deDupedExceptions.add(new NullPointerException());
                }
            }
        } else {
            deDupedExceptions.add(new NullPointerException());
        }
        localExceptions.addAll(deDupedExceptions);
        this.exceptions = Collections.unmodifiableList(localExceptions);
        this.message = this.exceptions.size() + " exceptions occurred. ";
    }

    public CompositeException(Collection<? extends Throwable> errors) {
        this((String) null, errors);
    }

    public CompositeException(Throwable... errors) {
        Set<Throwable> deDupedExceptions = new LinkedHashSet<>();
        List<Throwable> localExceptions = new ArrayList<>();
        if (errors != null) {
            for (Throwable ex : errors) {
                if (ex instanceof CompositeException) {
                    deDupedExceptions.addAll(((CompositeException) ex).getExceptions());
                } else if (ex != null) {
                    deDupedExceptions.add(ex);
                } else {
                    deDupedExceptions.add(new NullPointerException());
                }
            }
        } else {
            deDupedExceptions.add(new NullPointerException());
        }
        localExceptions.addAll(deDupedExceptions);
        this.exceptions = Collections.unmodifiableList(localExceptions);
        this.message = this.exceptions.size() + " exceptions occurred. ";
    }

    public List<Throwable> getExceptions() {
        return this.exceptions;
    }

    public String getMessage() {
        return this.message;
    }

    public synchronized Throwable getCause() {
        if (this.cause == null) {
            Throwable localCause = new CompositeExceptionCausalChain();
            Set<Throwable> seenCauses = new HashSet<>();
            Throwable chain = localCause;
            Iterator<Throwable> it2 = this.exceptions.iterator();
            while (it2.hasNext()) {
                Throwable e = it2.next();
                if (!seenCauses.contains(e)) {
                    seenCauses.add(e);
                    for (Throwable child : m7420(e)) {
                        if (seenCauses.contains(child)) {
                            e = new RuntimeException("Duplicate found in causal chain so cropping to prevent loop ...");
                        } else {
                            seenCauses.add(child);
                        }
                    }
                    try {
                        chain.initCause(e);
                    } catch (Throwable th) {
                    }
                    chain = m7419(chain);
                }
            }
            this.cause = localCause;
        }
        return this.cause;
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream s) {
        m7422((PrintStreamOrWriter) new WrappedPrintStream(s));
    }

    public void printStackTrace(PrintWriter s) {
        m7422((PrintStreamOrWriter) new WrappedPrintWriter(s));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7422(PrintStreamOrWriter s) {
        StringBuilder b = new StringBuilder(128);
        b.append(this).append(10);
        for (StackTraceElement myStackElement : getStackTrace()) {
            b.append("\tat ").append(myStackElement).append(10);
        }
        int i = 1;
        for (Throwable ex : this.exceptions) {
            b.append("  ComposedException ").append(i).append(" :\n");
            m7421(b, ex, "\t");
            i++;
        }
        synchronized (s.m24523()) {
            s.m24524(b.toString());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m7421(StringBuilder b, Throwable ex, String prefix) {
        b.append(prefix).append(ex).append(10);
        for (StackTraceElement stackElement : ex.getStackTrace()) {
            b.append("\t\tat ").append(stackElement).append(10);
        }
        if (ex.getCause() != null) {
            b.append("\tCaused by: ");
            m7421(b, ex.getCause(), "");
        }
    }

    static abstract class PrintStreamOrWriter {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract Object m24523();

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m24524(Object obj);

        PrintStreamOrWriter() {
        }
    }

    static final class WrappedPrintStream extends PrintStreamOrWriter {

        /* renamed from: 龘  reason: contains not printable characters */
        private final PrintStream f18785;

        WrappedPrintStream(PrintStream printStream) {
            this.f18785 = printStream;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Object m24525() {
            return this.f18785;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24526(Object o) {
            this.f18785.println(o);
        }
    }

    static final class WrappedPrintWriter extends PrintStreamOrWriter {

        /* renamed from: 龘  reason: contains not printable characters */
        private final PrintWriter f18786;

        WrappedPrintWriter(PrintWriter printWriter) {
            this.f18786 = printWriter;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Object m24527() {
            return this.f18786;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24528(Object o) {
            this.f18786.println(o);
        }
    }

    static final class CompositeExceptionCausalChain extends RuntimeException {
        private static final long serialVersionUID = 3875212506787802066L;

        CompositeExceptionCausalChain() {
        }

        public String getMessage() {
            return "Chain of Causes for CompositeException In Order Received =>";
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private List<Throwable> m7420(Throwable ex) {
        List<Throwable> list = new ArrayList<>();
        Throwable root = ex.getCause();
        if (root != null && root != ex) {
            while (true) {
                list.add(root);
                Throwable cause2 = root.getCause();
                if (cause2 == null || cause2 == root) {
                    break;
                }
                root = root.getCause();
            }
        }
        return list;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Throwable m7419(Throwable e) {
        Throwable root = e.getCause();
        if (root == null || root == e) {
            Throwable th = root;
            return e;
        }
        while (true) {
            Throwable cause2 = root.getCause();
            if (cause2 == null || cause2 == root) {
                Throwable th2 = root;
            } else {
                root = root.getCause();
            }
        }
        Throwable th22 = root;
        return root;
    }
}
