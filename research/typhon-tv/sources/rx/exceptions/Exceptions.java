package rx.exceptions;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import rx.Observer;
import rx.SingleSubscriber;

public final class Exceptions {
    /* renamed from: 龘  reason: contains not printable characters */
    public static RuntimeException m24531(Throwable t) {
        if (t instanceof RuntimeException) {
            throw ((RuntimeException) t);
        } else if (t instanceof Error) {
            throw ((Error) t);
        } else {
            throw new RuntimeException(t);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m24529(Throwable t) {
        if (t instanceof OnErrorNotImplementedException) {
            throw ((OnErrorNotImplementedException) t);
        } else if (t instanceof OnErrorFailedException) {
            throw ((OnErrorFailedException) t);
        } else if (t instanceof OnCompletedFailedException) {
            throw ((OnCompletedFailedException) t);
        } else if (t instanceof VirtualMachineError) {
            throw ((VirtualMachineError) t);
        } else if (t instanceof ThreadDeath) {
            throw ((ThreadDeath) t);
        } else if (t instanceof LinkageError) {
            throw ((LinkageError) t);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24532(Throwable e, Throwable cause) {
        Set<Throwable> seenCauses = new HashSet<>();
        int i = 0;
        while (true) {
            if (e.getCause() != null) {
                int i2 = i + 1;
                if (i >= 25) {
                    int i3 = i2;
                    return;
                }
                e = e.getCause();
                if (seenCauses.contains(e.getCause())) {
                    int i4 = i2;
                    break;
                } else {
                    seenCauses.add(e.getCause());
                    i = i2;
                }
            }
        }
        try {
            e.initCause(cause);
        } catch (Throwable th) {
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Throwable m24530(Throwable e) {
        int i = 0;
        while (e.getCause() != null) {
            int i2 = i + 1;
            if (i >= 25) {
                int i3 = i2;
                return new RuntimeException("Stack too deep to get final cause");
            }
            e = e.getCause();
            i = i2;
        }
        return e;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24536(List<? extends Throwable> exceptions) {
        if (exceptions != null && !exceptions.isEmpty()) {
            if (exceptions.size() == 1) {
                Throwable t = (Throwable) exceptions.get(0);
                if (t instanceof RuntimeException) {
                    throw ((RuntimeException) t);
                } else if (t instanceof Error) {
                    throw ((Error) t);
                } else {
                    throw new RuntimeException(t);
                }
            } else {
                throw new CompositeException((Collection<? extends Throwable>) exceptions);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24534(Throwable t, Observer<?> o, Object value) {
        m24529(t);
        o.onError(OnErrorThrowable.addValueAsLastCause(t, value));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24533(Throwable t, Observer<?> o) {
        m24529(t);
        o.onError(t);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24535(Throwable throwable, SingleSubscriber<?> subscriber) {
        m24529(throwable);
        subscriber.m24507(throwable);
    }
}
