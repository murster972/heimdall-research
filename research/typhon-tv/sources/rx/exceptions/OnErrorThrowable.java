package rx.exceptions;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import rx.plugins.RxJavaPlugins;

public final class OnErrorThrowable extends RuntimeException {
    private static final long serialVersionUID = -569558213262703934L;
    private final boolean hasValue;
    private final Object value;

    private OnErrorThrowable(Throwable exception) {
        super(exception);
        this.hasValue = false;
        this.value = null;
    }

    private OnErrorThrowable(Throwable exception, Object value2) {
        super(exception);
        Object message;
        this.hasValue = true;
        if (value2 instanceof Serializable) {
            message = value2;
        } else {
            try {
                message = String.valueOf(value2);
            } catch (Throwable ex) {
                message = ex.getMessage();
            }
        }
        this.value = message;
    }

    public Object getValue() {
        return this.value;
    }

    public boolean isValueNull() {
        return this.hasValue;
    }

    public static OnErrorThrowable from(Throwable t) {
        if (t == null) {
            t = new NullPointerException();
        }
        Throwable cause = Exceptions.m24530(t);
        if (cause instanceof OnNextValue) {
            return new OnErrorThrowable(t, ((OnNextValue) cause).getValue());
        }
        return new OnErrorThrowable(t);
    }

    public static Throwable addValueAsLastCause(Throwable e, Object value2) {
        if (e == null) {
            e = new NullPointerException();
        }
        Throwable lastCause = Exceptions.m24530(e);
        if (!(lastCause instanceof OnNextValue) || ((OnNextValue) lastCause).getValue() != value2) {
            Exceptions.m24532(e, (Throwable) new OnNextValue(value2));
        }
        return e;
    }

    public static class OnNextValue extends RuntimeException {
        private static final long serialVersionUID = -3454462756050397899L;
        private final Object value;

        static final class Primitives {

            /* renamed from: 龘  reason: contains not printable characters */
            static final Set<Class<?>> f18787 = m24538();

            /* renamed from: 龘  reason: contains not printable characters */
            private static Set<Class<?>> m24538() {
                Set<Class<?>> set = new HashSet<>();
                set.add(Boolean.class);
                set.add(Character.class);
                set.add(Byte.class);
                set.add(Short.class);
                set.add(Integer.class);
                set.add(Long.class);
                set.add(Float.class);
                set.add(Double.class);
                return set;
            }
        }

        public OnNextValue(Object value2) {
            super("OnError while emitting onNext value: " + m24537(value2));
            Object message;
            if (value2 instanceof Serializable) {
                message = value2;
            } else {
                try {
                    message = String.valueOf(value2);
                } catch (Throwable ex) {
                    message = ex.getMessage();
                }
            }
            this.value = message;
        }

        public Object getValue() {
            return this.value;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static String m24537(Object value2) {
            if (value2 == null) {
                return "null";
            }
            if (Primitives.f18787.contains(value2.getClass())) {
                return value2.toString();
            }
            if (value2 instanceof String) {
                return (String) value2;
            }
            if (value2 instanceof Enum) {
                return ((Enum) value2).name();
            }
            String pluggedRendering = RxJavaPlugins.m7437().m7440().m7428(value2);
            if (pluggedRendering != null) {
                return pluggedRendering;
            }
            return value2.getClass().getName() + ".class";
        }
    }
}
