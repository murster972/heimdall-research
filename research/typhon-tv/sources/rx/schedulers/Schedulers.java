package rx.schedulers;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import rx.Scheduler;
import rx.internal.schedulers.ExecutorScheduler;
import rx.internal.schedulers.GenericScheduledExecutorService;
import rx.internal.schedulers.ImmediateScheduler;
import rx.internal.schedulers.SchedulerLifecycle;
import rx.internal.schedulers.TrampolineScheduler;
import rx.plugins.RxJavaHooks;
import rx.plugins.RxJavaPlugins;
import rx.plugins.RxJavaSchedulersHook;

public final class Schedulers {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final AtomicReference<Schedulers> f19420 = new AtomicReference<>();

    /* renamed from: 靐  reason: contains not printable characters */
    private final Scheduler f19421;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Scheduler f19422;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Scheduler f19423;

    /* renamed from: 齉  reason: contains not printable characters */
    private static Schedulers m25043() {
        Schedulers current;
        while (true) {
            current = f19420.get();
            if (current == null) {
                current = new Schedulers();
                if (f19420.compareAndSet((Object) null, current)) {
                    break;
                }
                current.m25044();
            } else {
                break;
            }
        }
        return current;
    }

    private Schedulers() {
        RxJavaSchedulersHook hook = RxJavaPlugins.m7437().m7438();
        Scheduler c = hook.m7452();
        if (c != null) {
            this.f19423 = c;
        } else {
            this.f19423 = RxJavaSchedulersHook.m7448();
        }
        Scheduler io2 = hook.m7451();
        if (io2 != null) {
            this.f19421 = io2;
        } else {
            this.f19421 = RxJavaSchedulersHook.m7444();
        }
        Scheduler nt = hook.m7450();
        if (nt != null) {
            this.f19422 = nt;
        } else {
            this.f19422 = RxJavaSchedulersHook.m7446();
        }
    }

    public static Scheduler immediate() {
        return ImmediateScheduler.f19219;
    }

    public static Scheduler trampoline() {
        return TrampolineScheduler.f19262;
    }

    public static Scheduler newThread() {
        return RxJavaHooks.m25012(m25043().f19422);
    }

    public static Scheduler computation() {
        return RxJavaHooks.m25019(m25043().f19423);
    }

    public static Scheduler io() {
        return RxJavaHooks.m25007(m25043().f19421);
    }

    public static TestScheduler test() {
        return new TestScheduler();
    }

    public static Scheduler from(Executor executor) {
        return new ExecutorScheduler(executor);
    }

    public static void reset() {
        Schedulers s = f19420.getAndSet((Object) null);
        if (s != null) {
            s.m25044();
        }
    }

    public static void start() {
        Schedulers s = m25043();
        s.m25045();
        synchronized (s) {
            GenericScheduledExecutorService.f19215.m24829();
        }
    }

    public static void shutdown() {
        Schedulers s = m25043();
        s.m25044();
        synchronized (s) {
            GenericScheduledExecutorService.f19215.m24828();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public synchronized void m25045() {
        if (this.f19423 instanceof SchedulerLifecycle) {
            ((SchedulerLifecycle) this.f19423).m24850();
        }
        if (this.f19421 instanceof SchedulerLifecycle) {
            ((SchedulerLifecycle) this.f19421).m24850();
        }
        if (this.f19422 instanceof SchedulerLifecycle) {
            ((SchedulerLifecycle) this.f19422).m24850();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public synchronized void m25044() {
        if (this.f19423 instanceof SchedulerLifecycle) {
            ((SchedulerLifecycle) this.f19423).m24849();
        }
        if (this.f19421 instanceof SchedulerLifecycle) {
            ((SchedulerLifecycle) this.f19421).m24849();
        }
        if (this.f19422 instanceof SchedulerLifecycle) {
            ((SchedulerLifecycle) this.f19422).m24849();
        }
    }
}
