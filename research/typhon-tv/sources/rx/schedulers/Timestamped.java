package rx.schedulers;

public final class Timestamped<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private final T f19437;

    /* renamed from: 龘  reason: contains not printable characters */
    private final long f19438;

    public Timestamped(long timestampMillis, T value) {
        this.f19437 = value;
        this.f19438 = timestampMillis;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m25057() {
        return this.f19438;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public T m25056() {
        return this.f19437;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Timestamped)) {
            return false;
        }
        Timestamped<?> other = (Timestamped) obj;
        if (this.f19438 == other.f19438) {
            if (this.f19437 == other.f19437) {
                return true;
            }
            if (this.f19437 != null && this.f19437.equals(other.f19437)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return (31 * (((int) (this.f19438 ^ (this.f19438 >>> 32))) + 31)) + (this.f19437 == null ? 0 : this.f19437.hashCode());
    }

    public String toString() {
        return String.format("Timestamped(timestampMillis = %d, value = %s)", new Object[]{Long.valueOf(this.f19438), this.f19437.toString()});
    }
}
