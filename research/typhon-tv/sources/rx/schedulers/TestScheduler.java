package rx.schedulers;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action0;
import rx.internal.schedulers.SchedulePeriodicHelper;
import rx.subscriptions.BooleanSubscription;
import rx.subscriptions.Subscriptions;

public class TestScheduler extends Scheduler {

    /* renamed from: 靐  reason: contains not printable characters */
    static long f19424;

    /* renamed from: 齉  reason: contains not printable characters */
    long f19425;

    /* renamed from: 龘  reason: contains not printable characters */
    final Queue<TimedAction> f19426 = new PriorityQueue(11, new CompareActionsByTime());

    static final class TimedAction {

        /* renamed from: 靐  reason: contains not printable characters */
        final Action0 f19433;
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public final long f19434;

        /* renamed from: 齉  reason: contains not printable characters */
        final Scheduler.Worker f19435;

        /* renamed from: 龘  reason: contains not printable characters */
        final long f19436;

        TimedAction(Scheduler.Worker scheduler, long time, Action0 action) {
            long j = TestScheduler.f19424;
            TestScheduler.f19424 = 1 + j;
            this.f19434 = j;
            this.f19436 = time;
            this.f19433 = action;
            this.f19435 = scheduler;
        }

        public String toString() {
            return String.format("TimedAction(time = %d, action = %s)", new Object[]{Long.valueOf(this.f19436), this.f19433.toString()});
        }
    }

    static final class CompareActionsByTime implements Comparator<TimedAction> {
        CompareActionsByTime() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(TimedAction action1, TimedAction action2) {
            if (action1.f19436 == action2.f19436) {
                if (action1.f19434 < action2.f19434) {
                    return -1;
                }
                return action1.f19434 > action2.f19434 ? 1 : 0;
            } else if (action1.f19436 >= action2.f19436) {
                return action1.f19436 > action2.f19436 ? 1 : 0;
            } else {
                return -1;
            }
        }
    }

    public long now() {
        return TimeUnit.NANOSECONDS.toMillis(this.f19425);
    }

    public void advanceTimeBy(long delayTime, TimeUnit unit) {
        advanceTimeTo(this.f19425 + unit.toNanos(delayTime), TimeUnit.NANOSECONDS);
    }

    public void advanceTimeTo(long delayTime, TimeUnit unit) {
        m25046(unit.toNanos(delayTime));
    }

    public void triggerActions() {
        m25046(this.f19425);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m25046(long targetTimeInNanos) {
        while (!this.f19426.isEmpty()) {
            TimedAction current = this.f19426.peek();
            if (current.f19436 > targetTimeInNanos) {
                break;
            }
            this.f19425 = current.f19436 == 0 ? this.f19425 : current.f19436;
            this.f19426.remove();
            if (!current.f19435.isUnsubscribed()) {
                current.f19433.m24539();
            }
        }
        this.f19425 = targetTimeInNanos;
    }

    public Scheduler.Worker createWorker() {
        return new InnerTestScheduler();
    }

    final class InnerTestScheduler extends Scheduler.Worker implements SchedulePeriodicHelper.NowNanoSupplier {

        /* renamed from: 靐  reason: contains not printable characters */
        private final BooleanSubscription f19427 = new BooleanSubscription();

        InnerTestScheduler() {
        }

        public void unsubscribe() {
            this.f19427.unsubscribe();
        }

        public boolean isUnsubscribed() {
            return this.f19427.isUnsubscribed();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m25052(Action0 action, long delayTime, TimeUnit unit) {
            final TimedAction timedAction = new TimedAction(this, TestScheduler.this.f19425 + unit.toNanos(delayTime), action);
            TestScheduler.this.f19426.add(timedAction);
            return Subscriptions.m25090(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m25053() {
                    TestScheduler.this.f19426.remove(timedAction);
                }
            });
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m25050(Action0 action) {
            final TimedAction timedAction = new TimedAction(this, 0, action);
            TestScheduler.this.f19426.add(timedAction);
            return Subscriptions.m25090(new Action0() {
                /* renamed from: 龘  reason: contains not printable characters */
                public void m25054() {
                    TestScheduler.this.f19426.remove(timedAction);
                }
            });
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Subscription m25051(Action0 action, long initialDelay, long period, TimeUnit unit) {
            return SchedulePeriodicHelper.m24845(this, action, initialDelay, period, unit, this);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m25048() {
            return TestScheduler.this.now();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m25049() {
            return TestScheduler.this.f19425;
        }
    }
}
