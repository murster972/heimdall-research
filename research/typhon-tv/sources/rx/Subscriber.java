package rx;

import rx.internal.util.SubscriptionList;

public abstract class Subscriber<T> implements Observer<T>, Subscription {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Subscriber<?> f18769;

    /* renamed from: 麤  reason: contains not printable characters */
    private long f18770;

    /* renamed from: 齉  reason: contains not printable characters */
    private Producer f18771;

    /* renamed from: 龘  reason: contains not printable characters */
    private final SubscriptionList f18772;

    protected Subscriber() {
        this((Subscriber<?>) null, false);
    }

    protected Subscriber(Subscriber<?> subscriber) {
        this(subscriber, true);
    }

    protected Subscriber(Subscriber<?> subscriber, boolean shareSubscriptions) {
        this.f18770 = Long.MIN_VALUE;
        this.f18769 = subscriber;
        this.f18772 = (!shareSubscriptions || subscriber == null) ? new SubscriptionList() : subscriber.f18772;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24513(Subscription s) {
        this.f18772.m24923(s);
    }

    public final void unsubscribe() {
        this.f18772.unsubscribe();
    }

    public final boolean isUnsubscribed() {
        return this.f18772.isUnsubscribed();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m24510() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24511(long n) {
        if (n < 0) {
            throw new IllegalArgumentException("number requested cannot be negative: " + n);
        }
        synchronized (this) {
            if (this.f18771 != null) {
                Producer producerToRequestFrom = this.f18771;
                producerToRequestFrom.request(n);
                return;
            }
            m24509(n);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m24509(long n) {
        if (this.f18770 == Long.MIN_VALUE) {
            this.f18770 = n;
            return;
        }
        long total = this.f18770 + n;
        if (total < 0) {
            this.f18770 = Long.MAX_VALUE;
        } else {
            this.f18770 = total;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m24512(Producer p) {
        long toRequest;
        boolean passToSubscriber = false;
        synchronized (this) {
            toRequest = this.f18770;
            this.f18771 = p;
            if (this.f18769 != null && toRequest == Long.MIN_VALUE) {
                passToSubscriber = true;
            }
        }
        if (passToSubscriber) {
            this.f18769.m24512(this.f18771);
        } else if (toRequest == Long.MIN_VALUE) {
            this.f18771.request(Long.MAX_VALUE);
        } else {
            this.f18771.request(toRequest);
        }
    }
}
