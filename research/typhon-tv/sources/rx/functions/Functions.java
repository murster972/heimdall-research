package rx.functions;

public final class Functions {
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T0, T1, R> FuncN<R> m24546(final Func2<? super T0, ? super T1, ? extends R> f) {
        return new FuncN<R>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public R m24547(Object... args) {
                if (args.length == 2) {
                    return f.call(args[0], args[1]);
                }
                throw new IllegalArgumentException("Func2 expecting 2 arguments.");
            }
        };
    }
}
