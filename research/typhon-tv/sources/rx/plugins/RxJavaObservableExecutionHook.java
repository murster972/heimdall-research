package rx.plugins;

import rx.Observable;
import rx.Subscription;

public abstract class RxJavaObservableExecutionHook {
    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Observable.OnSubscribe<T> m7431(Observable.OnSubscribe<T> f) {
        return f;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Observable.OnSubscribe<T> m7432(Observable<? extends T> observable, Observable.OnSubscribe<T> onSubscribe) {
        return onSubscribe;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Subscription m7434(Subscription subscription) {
        return subscription;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Throwable m7430(Throwable e) {
        return e;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T, R> Observable.Operator<? extends R, ? super T> m7433(Observable.Operator<? extends R, ? super T> lift) {
        return lift;
    }
}
