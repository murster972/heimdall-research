package rx.plugins;

import java.util.concurrent.ThreadFactory;
import rx.Scheduler;
import rx.functions.Action0;
import rx.internal.schedulers.CachedThreadScheduler;
import rx.internal.schedulers.EventLoopsScheduler;
import rx.internal.schedulers.NewThreadScheduler;
import rx.internal.util.RxThreadFactory;

public class RxJavaSchedulersHook {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final RxJavaSchedulersHook f6514 = new RxJavaSchedulersHook();

    /* renamed from: 龘  reason: contains not printable characters */
    public static Scheduler m7448() {
        return m7449((ThreadFactory) new RxThreadFactory("RxComputationScheduler-"));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Scheduler m7449(ThreadFactory threadFactory) {
        if (threadFactory != null) {
            return new EventLoopsScheduler(threadFactory);
        }
        throw new NullPointerException("threadFactory == null");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Scheduler m7444() {
        return m7445(new RxThreadFactory("RxIoScheduler-"));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Scheduler m7445(ThreadFactory threadFactory) {
        if (threadFactory != null) {
            return new CachedThreadScheduler(threadFactory);
        }
        throw new NullPointerException("threadFactory == null");
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Scheduler m7446() {
        return m7447(new RxThreadFactory("RxNewThreadScheduler-"));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Scheduler m7447(ThreadFactory threadFactory) {
        if (threadFactory != null) {
            return new NewThreadScheduler(threadFactory);
        }
        throw new NullPointerException("threadFactory == null");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Scheduler m7452() {
        return null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Scheduler m7451() {
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Scheduler m7450() {
        return null;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public Action0 m7453(Action0 action) {
        return action;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static RxJavaSchedulersHook m7443() {
        return f6514;
    }
}
