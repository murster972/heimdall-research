package rx.plugins;

import rx.Observable;
import rx.Single;
import rx.Subscription;

public abstract class RxJavaSingleExecutionHook {
    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Single.OnSubscribe<T> m7457(Single.OnSubscribe<T> f) {
        return f;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Observable.OnSubscribe<T> m7455(Single<? extends T> single, Observable.OnSubscribe<T> onSubscribe) {
        return onSubscribe;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Subscription m7458(Subscription subscription) {
        return subscription;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T> Throwable m7454(Throwable e) {
        return e;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public <T, R> Observable.Operator<? extends R, ? super T> m7456(Observable.Operator<? extends R, ? super T> lift) {
        return lift;
    }
}
