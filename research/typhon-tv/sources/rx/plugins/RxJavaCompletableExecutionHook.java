package rx.plugins;

import rx.Completable;

public abstract class RxJavaCompletableExecutionHook {
    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public Completable.OnSubscribe m7424(Completable.OnSubscribe f) {
        return f;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public Completable.OnSubscribe m7425(Completable completableInstance, Completable.OnSubscribe onSubscribe) {
        return onSubscribe;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public Throwable m7423(Throwable e) {
        return e;
    }

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public Completable.Operator m7426(Completable.Operator lift) {
        return lift;
    }
}
