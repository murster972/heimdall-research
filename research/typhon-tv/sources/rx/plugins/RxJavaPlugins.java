package rx.plugins;

import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

public class RxJavaPlugins {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final RxJavaPlugins f6507 = new RxJavaPlugins();

    /* renamed from: 龘  reason: contains not printable characters */
    static final RxJavaErrorHandler f6508 = new RxJavaErrorHandler() {
    };

    /* renamed from: ʻ  reason: contains not printable characters */
    private final AtomicReference<RxJavaCompletableExecutionHook> f6509 = new AtomicReference<>();

    /* renamed from: ʼ  reason: contains not printable characters */
    private final AtomicReference<RxJavaSchedulersHook> f6510 = new AtomicReference<>();

    /* renamed from: 连任  reason: contains not printable characters */
    private final AtomicReference<RxJavaSingleExecutionHook> f6511 = new AtomicReference<>();

    /* renamed from: 麤  reason: contains not printable characters */
    private final AtomicReference<RxJavaObservableExecutionHook> f6512 = new AtomicReference<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private final AtomicReference<RxJavaErrorHandler> f6513 = new AtomicReference<>();

    @Deprecated
    /* renamed from: 龘  reason: contains not printable characters */
    public static RxJavaPlugins m7437() {
        return f6507;
    }

    RxJavaPlugins() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public RxJavaErrorHandler m7440() {
        if (this.f6513.get() == null) {
            Object impl = m7436(RxJavaErrorHandler.class, m7435());
            if (impl == null) {
                this.f6513.compareAndSet((Object) null, f6508);
            } else {
                this.f6513.compareAndSet((Object) null, (RxJavaErrorHandler) impl);
            }
        }
        return this.f6513.get();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public RxJavaObservableExecutionHook m7442() {
        if (this.f6512.get() == null) {
            Object impl = m7436(RxJavaObservableExecutionHook.class, m7435());
            if (impl == null) {
                this.f6512.compareAndSet((Object) null, RxJavaObservableExecutionHookDefault.m25041());
            } else {
                this.f6512.compareAndSet((Object) null, (RxJavaObservableExecutionHook) impl);
            }
        }
        return this.f6512.get();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public RxJavaSingleExecutionHook m7441() {
        if (this.f6511.get() == null) {
            Object impl = m7436(RxJavaSingleExecutionHook.class, m7435());
            if (impl == null) {
                this.f6511.compareAndSet((Object) null, RxJavaSingleExecutionHookDefault.m25042());
            } else {
                this.f6511.compareAndSet((Object) null, (RxJavaSingleExecutionHook) impl);
            }
        }
        return this.f6511.get();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public RxJavaCompletableExecutionHook m7439() {
        if (this.f6509.get() == null) {
            Object impl = m7436(RxJavaCompletableExecutionHook.class, m7435());
            if (impl == null) {
                this.f6509.compareAndSet((Object) null, new RxJavaCompletableExecutionHook() {
                });
            } else {
                this.f6509.compareAndSet((Object) null, (RxJavaCompletableExecutionHook) impl);
            }
        }
        return this.f6509.get();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static Properties m7435() {
        try {
            return System.getProperties();
        } catch (SecurityException e) {
            return new Properties();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Object m7436(Class<?> pluginClass, Properties propsIn) {
        Properties props = (Properties) propsIn.clone();
        String classSimpleName = pluginClass.getSimpleName();
        String implementingClass = props.getProperty("rxjava.plugin." + classSimpleName + ".implementation");
        if (implementingClass == null) {
            try {
                Iterator it2 = props.entrySet().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Map.Entry<Object, Object> e = (Map.Entry) it2.next();
                    String key = e.getKey().toString();
                    if (key.startsWith("rxjava.plugin.") && key.endsWith(".class") && classSimpleName.equals(e.getValue().toString())) {
                        String implKey = "rxjava.plugin." + key.substring(0, key.length() - ".class".length()).substring("rxjava.plugin.".length()) + ".impl";
                        implementingClass = props.getProperty(implKey);
                        if (implementingClass == null) {
                            throw new IllegalStateException("Implementing class declaration for " + classSimpleName + " missing: " + implKey);
                        }
                    }
                }
            } catch (SecurityException ex) {
                ex.printStackTrace();
            }
        }
        if (implementingClass == null) {
            return null;
        }
        try {
            return Class.forName(implementingClass).asSubclass(pluginClass).newInstance();
        } catch (ClassCastException e2) {
            throw new IllegalStateException(classSimpleName + " implementation is not an instance of " + classSimpleName + ": " + implementingClass, e2);
        } catch (ClassNotFoundException e3) {
            throw new IllegalStateException(classSimpleName + " implementation class not found: " + implementingClass, e3);
        } catch (InstantiationException e4) {
            throw new IllegalStateException(classSimpleName + " implementation not able to be instantiated: " + implementingClass, e4);
        } catch (IllegalAccessException e5) {
            throw new IllegalStateException(classSimpleName + " implementation not able to be accessed: " + implementingClass, e5);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public RxJavaSchedulersHook m7438() {
        if (this.f6510.get() == null) {
            Object impl = m7436(RxJavaSchedulersHook.class, m7435());
            if (impl == null) {
                this.f6510.compareAndSet((Object) null, RxJavaSchedulersHook.m7443());
            } else {
                this.f6510.compareAndSet((Object) null, (RxJavaSchedulersHook) impl);
            }
        }
        return this.f6510.get();
    }
}
