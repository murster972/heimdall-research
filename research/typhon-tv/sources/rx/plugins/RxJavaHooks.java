package rx.plugins;

import java.util.concurrent.ScheduledExecutorService;
import rx.Completable;
import rx.Observable;
import rx.Scheduler;
import rx.Single;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.internal.operators.SingleFromObservable;
import rx.internal.operators.SingleToObservable;

public final class RxJavaHooks {

    /* renamed from: ʻ  reason: contains not printable characters */
    static volatile Func2<Single, Single.OnSubscribe, Single.OnSubscribe> f19397;

    /* renamed from: ʼ  reason: contains not printable characters */
    static volatile Func2<Completable, Completable.OnSubscribe, Completable.OnSubscribe> f19398;

    /* renamed from: ʽ  reason: contains not printable characters */
    static volatile Func1<Scheduler, Scheduler> f19399;

    /* renamed from: ʾ  reason: contains not printable characters */
    static volatile Func1<Subscription, Subscription> f19400;

    /* renamed from: ʿ  reason: contains not printable characters */
    static volatile Func0<? extends ScheduledExecutorService> f19401;

    /* renamed from: ˆ  reason: contains not printable characters */
    static volatile Func1<Completable.Operator, Completable.Operator> f19402;

    /* renamed from: ˈ  reason: contains not printable characters */
    static volatile Func1<Subscription, Subscription> f19403;

    /* renamed from: ˊ  reason: contains not printable characters */
    static volatile Func1<Throwable, Throwable> f19404;

    /* renamed from: ˋ  reason: contains not printable characters */
    static volatile Func1<Observable.Operator, Observable.Operator> f19405;

    /* renamed from: ˎ  reason: contains not printable characters */
    static volatile Func1<Observable.Operator, Observable.Operator> f19406;

    /* renamed from: ˑ  reason: contains not printable characters */
    static volatile Func1<Scheduler, Scheduler> f19407;

    /* renamed from: ٴ  reason: contains not printable characters */
    static volatile Func1<Scheduler, Scheduler> f19408;

    /* renamed from: ᐧ  reason: contains not printable characters */
    static volatile Func1<Action0, Action0> f19409;

    /* renamed from: 连任  reason: contains not printable characters */
    static volatile Func2<Observable, Observable.OnSubscribe, Observable.OnSubscribe> f19410;

    /* renamed from: 靐  reason: contains not printable characters */
    static volatile Func1<Observable.OnSubscribe, Observable.OnSubscribe> f19411;

    /* renamed from: 麤  reason: contains not printable characters */
    static volatile Func1<Completable.OnSubscribe, Completable.OnSubscribe> f19412;

    /* renamed from: 齉  reason: contains not printable characters */
    static volatile Func1<Single.OnSubscribe, Single.OnSubscribe> f19413;

    /* renamed from: 龘  reason: contains not printable characters */
    static volatile Action1<Throwable> f19414;

    /* renamed from: ﹶ  reason: contains not printable characters */
    static volatile Func1<Throwable, Throwable> f19415;

    /* renamed from: ﾞ  reason: contains not printable characters */
    static volatile Func1<Throwable, Throwable> f19416;

    static {
        m25023();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m25023() {
        f19414 = new Action1<Throwable>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(Throwable e) {
                RxJavaPlugins.m7437().m7440().m7429(e);
            }
        };
        f19410 = new Func2<Observable, Observable.OnSubscribe, Observable.OnSubscribe>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable.OnSubscribe call(Observable t1, Observable.OnSubscribe t2) {
                return RxJavaPlugins.m7437().m7442().m7432(t1, t2);
            }
        };
        f19403 = new Func1<Subscription, Subscription>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Subscription call(Subscription f) {
                return RxJavaPlugins.m7437().m7442().m7434(f);
            }
        };
        f19397 = new Func2<Single, Single.OnSubscribe, Single.OnSubscribe>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Single.OnSubscribe call(Single t1, Single.OnSubscribe t2) {
                RxJavaSingleExecutionHook hook = RxJavaPlugins.m7437().m7441();
                return hook == RxJavaSingleExecutionHookDefault.m25042() ? t2 : new SingleFromObservable(hook.m7455(t1, new SingleToObservable(t2)));
            }
        };
        f19400 = new Func1<Subscription, Subscription>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Subscription call(Subscription f) {
                return RxJavaPlugins.m7437().m7441().m7458(f);
            }
        };
        f19398 = new Func2<Completable, Completable.OnSubscribe, Completable.OnSubscribe>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Completable.OnSubscribe call(Completable t1, Completable.OnSubscribe t2) {
                return RxJavaPlugins.m7437().m7439().m7425(t1, t2);
            }
        };
        f19409 = new Func1<Action0, Action0>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Action0 call(Action0 a) {
                return RxJavaPlugins.m7437().m7438().m7453(a);
            }
        };
        f19415 = new Func1<Throwable, Throwable>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Throwable call(Throwable t) {
                return RxJavaPlugins.m7437().m7442().m7430(t);
            }
        };
        f19405 = new Func1<Observable.Operator, Observable.Operator>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable.Operator call(Observable.Operator t) {
                return RxJavaPlugins.m7437().m7442().m7433(t);
            }
        };
        f19416 = new Func1<Throwable, Throwable>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Throwable call(Throwable t) {
                return RxJavaPlugins.m7437().m7441().m7454(t);
            }
        };
        f19406 = new Func1<Observable.Operator, Observable.Operator>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable.Operator call(Observable.Operator t) {
                return RxJavaPlugins.m7437().m7441().m7456(t);
            }
        };
        f19404 = new Func1<Throwable, Throwable>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Throwable call(Throwable t) {
                return RxJavaPlugins.m7437().m7439().m7423(t);
            }
        };
        f19402 = new Func1<Completable.Operator, Completable.Operator>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Completable.Operator call(Completable.Operator t) {
                return RxJavaPlugins.m7437().m7439().m7426(t);
            }
        };
        m25008();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static void m25008() {
        f19411 = new Func1<Observable.OnSubscribe, Observable.OnSubscribe>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Observable.OnSubscribe call(Observable.OnSubscribe f) {
                return RxJavaPlugins.m7437().m7442().m7431(f);
            }
        };
        f19413 = new Func1<Single.OnSubscribe, Single.OnSubscribe>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Single.OnSubscribe call(Single.OnSubscribe f) {
                return RxJavaPlugins.m7437().m7441().m7457(f);
            }
        };
        f19412 = new Func1<Completable.OnSubscribe, Completable.OnSubscribe>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public Completable.OnSubscribe call(Completable.OnSubscribe f) {
                return RxJavaPlugins.m7437().m7439().m7424(f);
            }
        };
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m25024(Throwable ex) {
        Action1<Throwable> f = f19414;
        if (f != null) {
            try {
                f.call(ex);
                return;
            } catch (Throwable pluginException) {
                System.err.println("The onError handler threw an Exception. It shouldn't. => " + pluginException.getMessage());
                pluginException.printStackTrace();
                m25009(pluginException);
            }
        }
        m25009(ex);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static void m25009(Throwable ex) {
        Thread current = Thread.currentThread();
        current.getUncaughtExceptionHandler().uncaughtException(current, ex);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable.OnSubscribe<T> m25016(Observable.OnSubscribe<T> onSubscribe) {
        Func1<Observable.OnSubscribe, Observable.OnSubscribe> f = f19411;
        if (f != null) {
            return f.call(onSubscribe);
        }
        return onSubscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Single.OnSubscribe<T> m25020(Single.OnSubscribe<T> onSubscribe) {
        Func1<Single.OnSubscribe, Single.OnSubscribe> f = f19413;
        if (f != null) {
            return f.call(onSubscribe);
        }
        return onSubscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Completable.OnSubscribe m25014(Completable.OnSubscribe onSubscribe) {
        Func1<Completable.OnSubscribe, Completable.OnSubscribe> f = f19412;
        if (f != null) {
            return f.call(onSubscribe);
        }
        return onSubscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Scheduler m25019(Scheduler scheduler) {
        Func1<Scheduler, Scheduler> f = f19399;
        if (f != null) {
            return f.call(scheduler);
        }
        return scheduler;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Scheduler m25007(Scheduler scheduler) {
        Func1<Scheduler, Scheduler> f = f19407;
        if (f != null) {
            return f.call(scheduler);
        }
        return scheduler;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Scheduler m25012(Scheduler scheduler) {
        Func1<Scheduler, Scheduler> f = f19408;
        if (f != null) {
            return f.call(scheduler);
        }
        return scheduler;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Action0 m25022(Action0 action) {
        Func1<Action0, Action0> f = f19409;
        if (f != null) {
            return f.call(action);
        }
        return action;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Observable.OnSubscribe<T> m25017(Observable<T> instance, Observable.OnSubscribe<T> onSubscribe) {
        Func2<Observable, Observable.OnSubscribe, Observable.OnSubscribe> f = f19410;
        if (f != null) {
            return f.call(instance, onSubscribe);
        }
        return onSubscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Subscription m25021(Subscription subscription) {
        Func1<Subscription, Subscription> f = f19403;
        if (f != null) {
            return f.call(subscription);
        }
        return subscription;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Throwable m25011(Throwable error) {
        Func1<Throwable, Throwable> f = f19415;
        if (f != null) {
            return f.call(error);
        }
        return error;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T, R> Observable.Operator<R, T> m25018(Observable.Operator<R, T> operator) {
        Func1<Observable.Operator, Observable.Operator> f = f19405;
        if (f != null) {
            return f.call(operator);
        }
        return operator;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T, R> Observable.Operator<R, T> m25006(Observable.Operator<R, T> operator) {
        Func1<Observable.Operator, Observable.Operator> f = f19406;
        if (f != null) {
            return f.call(operator);
        }
        return operator;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Completable.OnSubscribe m25015(Completable instance, Completable.OnSubscribe onSubscribe) {
        Func2<Completable, Completable.OnSubscribe, Completable.OnSubscribe> f = f19398;
        if (f != null) {
            return f.call(instance, onSubscribe);
        }
        return onSubscribe;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static Throwable m25010(Throwable error) {
        Func1<Throwable, Throwable> f = f19404;
        if (f != null) {
            return f.call(error);
        }
        return error;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Func0<? extends ScheduledExecutorService> m25013() {
        return f19401;
    }
}
