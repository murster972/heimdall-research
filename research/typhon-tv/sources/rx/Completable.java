package rx;

import rx.exceptions.Exceptions;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.plugins.RxJavaHooks;
import rx.subscriptions.MultipleAssignmentSubscription;
import rx.subscriptions.Subscriptions;

public class Completable {

    /* renamed from: 靐  reason: contains not printable characters */
    static final Completable f18753 = new Completable(new OnSubscribe() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void call(CompletableSubscriber s) {
            s.m24490(Subscriptions.m25089());
        }
    }, false);

    /* renamed from: 龘  reason: contains not printable characters */
    static final Completable f18754 = new Completable(new OnSubscribe() {
        /* renamed from: 龘  reason: contains not printable characters */
        public void call(CompletableSubscriber s) {
            s.m24490(Subscriptions.m25089());
            s.m24488();
        }
    }, false);

    /* renamed from: 齉  reason: contains not printable characters */
    private final OnSubscribe f18755;

    public interface OnSubscribe extends Action1<CompletableSubscriber> {
    }

    public interface Operator extends Func1<CompletableSubscriber, CompletableSubscriber> {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Completable m24478(OnSubscribe onSubscribe) {
        m24477(onSubscribe);
        try {
            return new Completable(onSubscribe);
        } catch (NullPointerException ex) {
            throw ex;
        } catch (Throwable ex2) {
            RxJavaHooks.m25024(ex2);
            throw m24476(ex2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Completable m24479(final Observable<?> flowable) {
        m24477(flowable);
        return m24478((OnSubscribe) new OnSubscribe() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void call(final CompletableSubscriber cs) {
                Subscriber<Object> subscriber = new Subscriber<Object>() {
                    public void onCompleted() {
                        cs.m24488();
                    }

                    public void onError(Throwable t) {
                        cs.m24489(t);
                    }

                    public void onNext(Object t) {
                    }
                };
                cs.m24490((Subscription) subscriber);
                flowable.m7416(subscriber);
            }
        });
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static <T> T m24477(T o) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static NullPointerException m24476(Throwable ex) {
        NullPointerException npe = new NullPointerException("Actually not, but can't pass out an exception otherwise...");
        npe.initCause(ex);
        return npe;
    }

    protected Completable(OnSubscribe onSubscribe) {
        this.f18755 = RxJavaHooks.m25014(onSubscribe);
    }

    protected Completable(OnSubscribe onSubscribe, boolean useHook) {
        this.f18755 = useHook ? RxJavaHooks.m25014(onSubscribe) : onSubscribe;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final Subscription m24480() {
        final MultipleAssignmentSubscription mad = new MultipleAssignmentSubscription();
        m24481((CompletableSubscriber) new CompletableSubscriber() {
            /* renamed from: 龘  reason: contains not printable characters */
            public void m24485() {
                mad.unsubscribe();
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24486(Throwable e) {
                RxJavaHooks.m25024(e);
                mad.unsubscribe();
                Completable.m24475(e);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m24487(Subscription d) {
                mad.m25087(d);
            }
        });
        return mad;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static void m24475(Throwable e) {
        Thread thread = Thread.currentThread();
        thread.getUncaughtExceptionHandler().uncaughtException(thread, e);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24481(CompletableSubscriber s) {
        m24477(s);
        try {
            RxJavaHooks.m25015(this, this.f18755).call(s);
        } catch (NullPointerException ex) {
            throw ex;
        } catch (Throwable ex2) {
            Exceptions.m24529(ex2);
            Throwable ex3 = RxJavaHooks.m25010(ex2);
            RxJavaHooks.m25024(ex3);
            throw m24476(ex3);
        }
    }
}
