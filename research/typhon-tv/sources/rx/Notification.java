package rx;

public final class Notification<T> {

    /* renamed from: 麤  reason: contains not printable characters */
    private static final Notification<Void> f18762 = new Notification<>(Kind.OnCompleted, (Object) null, (Throwable) null);

    /* renamed from: 靐  reason: contains not printable characters */
    private final Throwable f18763;

    /* renamed from: 齉  reason: contains not printable characters */
    private final T f18764;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Kind f18765;

    public enum Kind {
        OnNext,
        OnError,
        OnCompleted
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Notification<T> m24492(T t) {
        return new Notification<>(Kind.OnNext, t, (Throwable) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Notification<T> m24493(Throwable e) {
        return new Notification<>(Kind.OnError, (Object) null, e);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Notification<T> m24491() {
        return f18762;
    }

    private Notification(Kind kind, T value, Throwable e) {
        this.f18764 = value;
        this.f18763 = e;
        this.f18765 = kind;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Throwable m24499() {
        return this.f18763;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public T m24501() {
        return this.f18764;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m24500() {
        return m24497() && this.f18764 != null;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m24498() {
        return m24495() && this.f18763 != null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Kind m24494() {
        return this.f18765;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m24495() {
        return m24494() == Kind.OnError;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m24496() {
        return m24494() == Kind.OnCompleted;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m24497() {
        return m24494() == Kind.OnNext;
    }

    public String toString() {
        StringBuilder str = new StringBuilder(64).append('[').append(super.toString()).append(' ').append(m24494());
        if (m24500()) {
            str.append(' ').append(m24501());
        }
        if (m24498()) {
            str.append(' ').append(m24499().getMessage());
        }
        str.append(']');
        return str.toString();
    }

    public int hashCode() {
        int hash = m24494().hashCode();
        if (m24500()) {
            hash = (hash * 31) + m24501().hashCode();
        }
        if (m24498()) {
            return (hash * 31) + m24499().hashCode();
        }
        return hash;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Notification<?> notification = (Notification) obj;
        if (notification.m24494() != m24494() || ((this.f18764 != notification.f18764 && (this.f18764 == null || !this.f18764.equals(notification.f18764))) || (this.f18763 != notification.f18763 && (this.f18763 == null || !this.f18763.equals(notification.f18763))))) {
            z = false;
        }
        return z;
    }
}
