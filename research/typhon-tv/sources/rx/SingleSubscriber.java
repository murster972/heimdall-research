package rx;

import rx.internal.util.SubscriptionList;

public abstract class SingleSubscriber<T> implements Subscription {

    /* renamed from: 龘  reason: contains not printable characters */
    private final SubscriptionList f18768 = new SubscriptionList();

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m24506(T t);

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m24507(Throwable th);

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m24508(Subscription s) {
        this.f18768.m24923(s);
    }

    public final void unsubscribe() {
        this.f18768.unsubscribe();
    }

    public final boolean isUnsubscribed() {
        return this.f18768.isUnsubscribed();
    }
}
