package rx;

import rx.functions.Action1;
import rx.plugins.RxJavaHooks;

public class Single<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    final OnSubscribe<T> f18767;

    public interface OnSubscribe<T> extends Action1<SingleSubscriber<? super T>> {
    }

    protected Single(OnSubscribe<T> f) {
        this.f18767 = RxJavaHooks.m25020(f);
    }
}
