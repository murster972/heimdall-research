package rx;

import rx.exceptions.MissingBackpressureException;

public final class BackpressureOverflow {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Strategy f18746 = f18749;

    /* renamed from: 麤  reason: contains not printable characters */
    public static final Strategy f18747 = DropLatest.f18750;

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Strategy f18748 = DropOldest.f18751;

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Strategy f18749 = Error.f18752;

    public interface Strategy {
        /* renamed from: 龘  reason: contains not printable characters */
        boolean m24474() throws MissingBackpressureException;
    }

    static final class DropOldest implements Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        static final DropOldest f18751 = new DropOldest();

        private DropOldest() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24472() {
            return true;
        }
    }

    static final class DropLatest implements Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        static final DropLatest f18750 = new DropLatest();

        private DropLatest() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24471() {
            return false;
        }
    }

    static final class Error implements Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        static final Error f18752 = new Error();

        private Error() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24473() throws MissingBackpressureException {
            throw new MissingBackpressureException("Overflowed buffer");
        }
    }
}
