package org2.slf4j;

public interface ILoggerFactory {
    Logger getLogger(String str);
}
