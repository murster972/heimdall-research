package org2.slf4j;

import org2.slf4j.helpers.BasicMarkerFactory;
import org2.slf4j.helpers.Util;
import org2.slf4j.impl.StaticMarkerBinder;

public class MarkerFactory {
    static IMarkerFactory markerFactory;

    static {
        try {
            markerFactory = StaticMarkerBinder.SINGLETON.getMarkerFactory();
        } catch (NoClassDefFoundError e) {
            markerFactory = new BasicMarkerFactory();
        } catch (Exception e2) {
            Util.m24271("Unexpected failure while binding MarkerFactory", e2);
        }
    }

    private MarkerFactory() {
    }

    public static Marker getDetachedMarker(String str) {
        return markerFactory.getDetachedMarker(str);
    }

    public static IMarkerFactory getIMarkerFactory() {
        return markerFactory;
    }

    public static Marker getMarker(String str) {
        return markerFactory.getMarker(str);
    }
}
