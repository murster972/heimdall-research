package org2.slf4j.helpers;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org2.slf4j.Marker;

public class BasicMarker implements Marker {
    private static final long serialVersionUID = 1803952589649545191L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static String f18576 = " ]";

    /* renamed from: 齉  reason: contains not printable characters */
    private static String f18577 = ", ";

    /* renamed from: 龘  reason: contains not printable characters */
    private static String f18578 = "[ ";
    private final String name;
    private List refereceList;

    BasicMarker(String str) {
        if (str == null) {
            throw new IllegalArgumentException("A merker name cannot be null");
        }
        this.name = str;
    }

    public synchronized void add(Marker marker) {
        if (marker == null) {
            throw new IllegalArgumentException("A null value cannot be added to a Marker as reference.");
        } else if (!contains(marker)) {
            if (!marker.contains((Marker) this)) {
                if (this.refereceList == null) {
                    this.refereceList = new Vector();
                }
                this.refereceList.add(marker);
            }
        }
    }

    public boolean contains(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Other cannot be null");
        } else if (this.name.equals(str)) {
            return true;
        } else {
            if (!hasReferences()) {
                return false;
            }
            for (int i = 0; i < this.refereceList.size(); i++) {
                if (((Marker) this.refereceList.get(i)).contains(str)) {
                    return true;
                }
            }
            return false;
        }
    }

    public boolean contains(Marker marker) {
        if (marker == null) {
            throw new IllegalArgumentException("Other cannot be null");
        } else if (equals(marker)) {
            return true;
        } else {
            if (!hasReferences()) {
                return false;
            }
            for (int i = 0; i < this.refereceList.size(); i++) {
                if (((Marker) this.refereceList.get(i)).contains(marker)) {
                    return true;
                }
            }
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof Marker)) {
            return false;
        }
        return this.name.equals(((Marker) obj).getName());
    }

    public String getName() {
        return this.name;
    }

    public boolean hasChildren() {
        return hasReferences();
    }

    public synchronized boolean hasReferences() {
        return this.refereceList != null && this.refereceList.size() > 0;
    }

    public int hashCode() {
        return this.name.hashCode();
    }

    public synchronized Iterator iterator() {
        return this.refereceList != null ? this.refereceList.iterator() : Collections.EMPTY_LIST.iterator();
    }

    public synchronized boolean remove(Marker marker) {
        boolean z;
        if (this.refereceList != null) {
            int size = this.refereceList.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    z = false;
                    break;
                } else if (marker.equals((Marker) this.refereceList.get(i))) {
                    this.refereceList.remove(i);
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
        } else {
            z = false;
        }
        return z;
    }

    public String toString() {
        if (!hasReferences()) {
            return getName();
        }
        Iterator it2 = iterator();
        StringBuffer stringBuffer = new StringBuffer(getName());
        stringBuffer.append(' ').append(f18578);
        while (it2.hasNext()) {
            stringBuffer.append(((Marker) it2.next()).getName());
            if (it2.hasNext()) {
                stringBuffer.append(f18577);
            }
        }
        stringBuffer.append(f18576);
        return stringBuffer.toString();
    }
}
