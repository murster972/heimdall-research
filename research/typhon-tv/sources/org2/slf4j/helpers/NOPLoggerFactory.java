package org2.slf4j.helpers;

import org2.slf4j.ILoggerFactory;
import org2.slf4j.Logger;

public class NOPLoggerFactory implements ILoggerFactory {
    public Logger getLogger(String str) {
        return NOPLogger.NOP_LOGGER;
    }
}
