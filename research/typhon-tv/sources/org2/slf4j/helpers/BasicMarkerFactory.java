package org2.slf4j.helpers;

import java.util.HashMap;
import java.util.Map;
import org2.slf4j.IMarkerFactory;
import org2.slf4j.Marker;

public class BasicMarkerFactory implements IMarkerFactory {

    /* renamed from: 龘  reason: contains not printable characters */
    Map f18579 = new HashMap();

    public boolean detachMarker(String str) {
        return (str == null || this.f18579.remove(str) == null) ? false : true;
    }

    public synchronized boolean exists(String str) {
        return str == null ? false : this.f18579.containsKey(str);
    }

    public Marker getDetachedMarker(String str) {
        return new BasicMarker(str);
    }

    public synchronized Marker getMarker(String str) {
        Marker marker;
        if (str == null) {
            throw new IllegalArgumentException("Marker name cannot be null");
        }
        marker = (Marker) this.f18579.get(str);
        if (marker == null) {
            marker = new BasicMarker(str);
            this.f18579.put(str, marker);
        }
        return marker;
    }
}
