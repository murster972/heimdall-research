package org2.slf4j;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import org2.slf4j.helpers.NOPLoggerFactory;
import org2.slf4j.helpers.SubstituteLoggerFactory;
import org2.slf4j.helpers.Util;
import org2.slf4j.impl.StaticLoggerBinder;

public final class LoggerFactory {
    private static final String[] API_COMPATIBILITY_LIST = {"1.6"};
    static final String CODES_PREFIX = "http://www.slf4j.org/codes.html";
    static final int FAILED_INITILIZATION = 2;
    static int INITIALIZATION_STATE = 0;
    static final String MULTIPLE_BINDINGS_URL = "http://www.slf4j.org/codes.html#multiple_bindings";
    static NOPLoggerFactory NOP_FALLBACK_FACTORY = new NOPLoggerFactory();
    static final int NOP_FALLBACK_INITILIZATION = 4;
    static final String NO_STATICLOGGERBINDER_URL = "http://www.slf4j.org/codes.html#StaticLoggerBinder";
    static final String NULL_LF_URL = "http://www.slf4j.org/codes.html#null_LF";
    static final int ONGOING_INITILIZATION = 1;
    private static String STATIC_LOGGER_BINDER_PATH = "org/slf4j/impl/StaticLoggerBinder.class";
    static final String SUBSTITUTE_LOGGER_URL = "http://www.slf4j.org/codes.html#substituteLogger";
    static final int SUCCESSFUL_INITILIZATION = 3;
    static SubstituteLoggerFactory TEMP_FACTORY = new SubstituteLoggerFactory();
    static final int UNINITIALIZED = 0;
    static final String UNSUCCESSFUL_INIT_MSG = "org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit";
    static final String UNSUCCESSFUL_INIT_URL = "http://www.slf4j.org/codes.html#unsuccessfulInit";
    static final String VERSION_MISMATCH = "http://www.slf4j.org/codes.html#version_mismatch";
    static Class class$org$slf4j$LoggerFactory;

    private LoggerFactory() {
    }

    private static final void bind() {
        try {
            StaticLoggerBinder.getSingleton();
            INITIALIZATION_STATE = 3;
            emitSubstituteLoggerWarning();
        } catch (NoClassDefFoundError e) {
            String message = e.getMessage();
            if (message == null || message.indexOf("org/slf4j/impl/StaticLoggerBinder") == -1) {
                failedBinding(e);
                throw e;
            }
            INITIALIZATION_STATE = 4;
            Util.m24270("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
            Util.m24270("Defaulting to no-operation (NOP) logger implementation");
            Util.m24270("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
        } catch (NoSuchMethodError e2) {
            String message2 = e2.getMessage();
            if (!(message2 == null || message2.indexOf("org.slf4j.impl.StaticLoggerBinder.getSingleton()") == -1)) {
                INITIALIZATION_STATE = 2;
                Util.m24270("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                Util.m24270("Your binding is version 1.5.5 or earlier.");
                Util.m24270("Upgrade your binding to version 1.6.x. or 2.0.x");
            }
            throw e2;
        } catch (Exception e3) {
            failedBinding(e3);
            throw new IllegalStateException("Unexpected initialization failure", e3);
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    private static final void emitSubstituteLoggerWarning() {
        List r2 = TEMP_FACTORY.m24269();
        if (r2.size() != 0) {
            Util.m24270("The following loggers will not work becasue they were created");
            Util.m24270("during the default configuration phase of the underlying logging system.");
            Util.m24270("See also http://www.slf4j.org/codes.html#substituteLogger");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < r2.size()) {
                    Util.m24270((String) r2.get(i2));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    static void failedBinding(Throwable th) {
        INITIALIZATION_STATE = 2;
        Util.m24271("Failed to instantiate SLF4J LoggerFactory", th);
    }

    public static ILoggerFactory getILoggerFactory() {
        if (INITIALIZATION_STATE == 0) {
            INITIALIZATION_STATE = 1;
            performInitialization();
        }
        switch (INITIALIZATION_STATE) {
            case 1:
                return TEMP_FACTORY;
            case 2:
                throw new IllegalStateException(UNSUCCESSFUL_INIT_MSG);
            case 3:
                return StaticLoggerBinder.getSingleton().getLoggerFactory();
            case 4:
                return NOP_FALLBACK_FACTORY;
            default:
                throw new IllegalStateException("Unreachable code");
        }
    }

    public static Logger getLogger(Class cls) {
        return getLogger(cls.getName());
    }

    public static Logger getLogger(String str) {
        return getILoggerFactory().getLogger(str);
    }

    private static final void performInitialization() {
        singleImplementationSanityCheck();
        bind();
        if (INITIALIZATION_STATE == 3) {
            versionSanityCheck();
        }
    }

    static void reset() {
        INITIALIZATION_STATE = 0;
        TEMP_FACTORY = new SubstituteLoggerFactory();
    }

    private static void singleImplementationSanityCheck() {
        Class cls;
        try {
            if (class$org$slf4j$LoggerFactory == null) {
                cls = class$("org.slf4j.LoggerFactory");
                class$org$slf4j$LoggerFactory = cls;
            } else {
                cls = class$org$slf4j$LoggerFactory;
            }
            ClassLoader classLoader = cls.getClassLoader();
            Enumeration<URL> systemResources = classLoader == null ? ClassLoader.getSystemResources(STATIC_LOGGER_BINDER_PATH) : classLoader.getResources(STATIC_LOGGER_BINDER_PATH);
            ArrayList arrayList = new ArrayList();
            while (systemResources.hasMoreElements()) {
                arrayList.add(systemResources.nextElement());
            }
            if (arrayList.size() > 1) {
                Util.m24270("Class path contains multiple SLF4J bindings.");
                for (int i = 0; i < arrayList.size(); i++) {
                    Util.m24270(new StringBuffer().append("Found binding in [").append(arrayList.get(i)).append("]").toString());
                }
                Util.m24270("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
            }
        } catch (IOException e) {
            Util.m24271("Error getting resources from path", e);
        }
    }

    private static final void versionSanityCheck() {
        boolean z = false;
        try {
            String str = StaticLoggerBinder.REQUESTED_API_VERSION;
            for (String startsWith : API_COMPATIBILITY_LIST) {
                if (str.startsWith(startsWith)) {
                    z = true;
                }
            }
            if (!z) {
                Util.m24270(new StringBuffer().append("The requested version ").append(str).append(" by your slf4j binding is not compatible with ").append(Arrays.asList(API_COMPATIBILITY_LIST).toString()).toString());
                Util.m24270("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError e) {
        } catch (Throwable th) {
            Util.m24271("Unexpected problem occured during version sanity check", th);
        }
    }
}
