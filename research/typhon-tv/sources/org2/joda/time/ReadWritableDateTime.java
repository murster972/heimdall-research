package org2.joda.time;

public interface ReadWritableDateTime extends ReadWritableInstant, ReadableDateTime {
}
