package org2.joda.time.base;

import java.io.Serializable;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.Interval;
import org2.joda.time.Period;
import org2.joda.time.PeriodType;
import org2.joda.time.ReadableDuration;
import org2.joda.time.ReadableInstant;
import org2.joda.time.convert.ConverterManager;
import org2.joda.time.field.FieldUtils;

public abstract class BaseDuration extends AbstractDuration implements Serializable, ReadableDuration {
    private static final long serialVersionUID = 2581698638990L;
    private volatile long iMillis;

    protected BaseDuration(long j) {
        this.iMillis = j;
    }

    protected BaseDuration(long j, long j2) {
        this.iMillis = FieldUtils.m23027(j2, j);
    }

    protected BaseDuration(Object obj) {
        this.iMillis = ConverterManager.m22967().m22971(obj).m22979(obj);
    }

    protected BaseDuration(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        if (readableInstant == readableInstant2) {
            this.iMillis = 0;
            return;
        }
        this.iMillis = FieldUtils.m23027(DateTimeUtils.m22714(readableInstant2), DateTimeUtils.m22714(readableInstant));
    }

    public long getMillis() {
        return this.iMillis;
    }

    public Interval toIntervalFrom(ReadableInstant readableInstant) {
        return new Interval(readableInstant, (ReadableDuration) this);
    }

    public Interval toIntervalTo(ReadableInstant readableInstant) {
        return new Interval((ReadableDuration) this, readableInstant);
    }

    public Period toPeriod(Chronology chronology) {
        return new Period(getMillis(), chronology);
    }

    public Period toPeriod(PeriodType periodType) {
        return new Period(getMillis(), periodType);
    }

    public Period toPeriod(PeriodType periodType, Chronology chronology) {
        return new Period(getMillis(), periodType, chronology);
    }

    public Period toPeriodFrom(ReadableInstant readableInstant) {
        return new Period(readableInstant, this);
    }

    public Period toPeriodFrom(ReadableInstant readableInstant, PeriodType periodType) {
        return new Period(readableInstant, this, periodType);
    }

    public Period toPeriodTo(ReadableInstant readableInstant) {
        return new Period(this, readableInstant);
    }

    public Period toPeriodTo(ReadableInstant readableInstant, PeriodType periodType) {
        return new Period(this, readableInstant, periodType);
    }
}
