package org2.joda.time.base;

import java.io.Serializable;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.MutableInterval;
import org2.joda.time.ReadWritableInterval;
import org2.joda.time.ReadableDuration;
import org2.joda.time.ReadableInstant;
import org2.joda.time.ReadableInterval;
import org2.joda.time.ReadablePeriod;
import org2.joda.time.chrono.ISOChronology;
import org2.joda.time.convert.ConverterManager;
import org2.joda.time.convert.IntervalConverter;
import org2.joda.time.field.FieldUtils;

public abstract class BaseInterval extends AbstractInterval implements Serializable, ReadableInterval {
    private static final long serialVersionUID = 576586928732749278L;
    private volatile Chronology iChronology;
    private volatile long iEndMillis;
    private volatile long iStartMillis;

    protected BaseInterval(long j, long j2, Chronology chronology) {
        this.iChronology = DateTimeUtils.m22716(chronology);
        m22746(j, j2);
        this.iStartMillis = j;
        this.iEndMillis = j2;
    }

    protected BaseInterval(Object obj, Chronology chronology) {
        IntervalConverter r1 = ConverterManager.m22967().m22968(obj);
        if (r1.m22983(obj, chronology)) {
            ReadableInterval readableInterval = (ReadableInterval) obj;
            this.iChronology = chronology == null ? readableInterval.getChronology() : chronology;
            this.iStartMillis = readableInterval.getStartMillis();
            this.iEndMillis = readableInterval.getEndMillis();
        } else if (this instanceof ReadWritableInterval) {
            r1.m22984((ReadWritableInterval) this, obj, chronology);
        } else {
            MutableInterval mutableInterval = new MutableInterval();
            r1.m22984(mutableInterval, obj, chronology);
            this.iChronology = mutableInterval.getChronology();
            this.iStartMillis = mutableInterval.getStartMillis();
            this.iEndMillis = mutableInterval.getEndMillis();
        }
        m22746(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadableDuration readableDuration, ReadableInstant readableInstant) {
        this.iChronology = DateTimeUtils.m22709(readableInstant);
        this.iEndMillis = DateTimeUtils.m22714(readableInstant);
        this.iStartMillis = FieldUtils.m23036(this.iEndMillis, -DateTimeUtils.m22713(readableDuration));
        m22746(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadableInstant readableInstant, ReadableDuration readableDuration) {
        this.iChronology = DateTimeUtils.m22709(readableInstant);
        this.iStartMillis = DateTimeUtils.m22714(readableInstant);
        this.iEndMillis = FieldUtils.m23036(this.iStartMillis, DateTimeUtils.m22713(readableDuration));
        m22746(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        if (readableInstant == null && readableInstant2 == null) {
            long r0 = DateTimeUtils.m22712();
            this.iEndMillis = r0;
            this.iStartMillis = r0;
            this.iChronology = ISOChronology.getInstance();
            return;
        }
        this.iChronology = DateTimeUtils.m22709(readableInstant);
        this.iStartMillis = DateTimeUtils.m22714(readableInstant);
        this.iEndMillis = DateTimeUtils.m22714(readableInstant2);
        m22746(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadableInstant readableInstant, ReadablePeriod readablePeriod) {
        Chronology r0 = DateTimeUtils.m22709(readableInstant);
        this.iChronology = r0;
        this.iStartMillis = DateTimeUtils.m22714(readableInstant);
        if (readablePeriod == null) {
            this.iEndMillis = this.iStartMillis;
        } else {
            this.iEndMillis = r0.add(readablePeriod, this.iStartMillis, 1);
        }
        m22746(this.iStartMillis, this.iEndMillis);
    }

    protected BaseInterval(ReadablePeriod readablePeriod, ReadableInstant readableInstant) {
        Chronology r0 = DateTimeUtils.m22709(readableInstant);
        this.iChronology = r0;
        this.iEndMillis = DateTimeUtils.m22714(readableInstant);
        if (readablePeriod == null) {
            this.iStartMillis = this.iEndMillis;
        } else {
            this.iStartMillis = r0.add(readablePeriod, this.iEndMillis, -1);
        }
        m22746(this.iStartMillis, this.iEndMillis);
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public long getEndMillis() {
        return this.iEndMillis;
    }

    public long getStartMillis() {
        return this.iStartMillis;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22754(long j, long j2, Chronology chronology) {
        m22746(j, j2);
        this.iStartMillis = j;
        this.iEndMillis = j2;
        this.iChronology = DateTimeUtils.m22716(chronology);
    }
}
