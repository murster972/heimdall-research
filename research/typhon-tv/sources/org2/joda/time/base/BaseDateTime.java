package org2.joda.time.base;

import java.io.Serializable;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadableDateTime;
import org2.joda.time.chrono.ISOChronology;
import org2.joda.time.convert.ConverterManager;
import org2.joda.time.convert.InstantConverter;

public abstract class BaseDateTime extends AbstractDateTime implements Serializable, ReadableDateTime {
    private static final long serialVersionUID = -6728882245981L;
    private volatile Chronology iChronology;
    private volatile long iMillis;

    public BaseDateTime() {
        this(DateTimeUtils.m22712(), (Chronology) ISOChronology.getInstance());
    }

    public BaseDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this(i, i2, i3, i4, i5, i6, i7, (Chronology) ISOChronology.getInstance());
    }

    public BaseDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7, Chronology chronology) {
        this.iChronology = m22753(chronology);
        this.iMillis = m22752(this.iChronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7), this.iChronology);
        m22751();
    }

    public BaseDateTime(int i, int i2, int i3, int i4, int i5, int i6, int i7, DateTimeZone dateTimeZone) {
        this(i, i2, i3, i4, i5, i6, i7, (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public BaseDateTime(long j) {
        this(j, (Chronology) ISOChronology.getInstance());
    }

    public BaseDateTime(long j, Chronology chronology) {
        this.iChronology = m22753(chronology);
        this.iMillis = m22752(j, this.iChronology);
        m22751();
    }

    public BaseDateTime(long j, DateTimeZone dateTimeZone) {
        this(j, (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    public BaseDateTime(Object obj, Chronology chronology) {
        InstantConverter r0 = ConverterManager.m22967().m22972(obj);
        this.iChronology = m22753(r0.m22980(obj, chronology));
        this.iMillis = m22752(r0.m22981(obj, chronology), this.iChronology);
        m22751();
    }

    public BaseDateTime(Object obj, DateTimeZone dateTimeZone) {
        InstantConverter r0 = ConverterManager.m22967().m22972(obj);
        Chronology r1 = m22753(r0.m22982(obj, dateTimeZone));
        this.iChronology = r1;
        this.iMillis = m22752(r0.m22981(obj, r1), r1);
        m22751();
    }

    public BaseDateTime(Chronology chronology) {
        this(DateTimeUtils.m22712(), chronology);
    }

    public BaseDateTime(DateTimeZone dateTimeZone) {
        this(DateTimeUtils.m22712(), (Chronology) ISOChronology.getInstance(dateTimeZone));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22751() {
        if (this.iMillis == Long.MIN_VALUE || this.iMillis == Long.MAX_VALUE) {
            this.iChronology = this.iChronology.withUTC();
        }
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public long getMillis() {
        return this.iMillis;
    }

    /* access modifiers changed from: protected */
    public void setChronology(Chronology chronology) {
        this.iChronology = m22753(chronology);
    }

    /* access modifiers changed from: protected */
    public void setMillis(long j) {
        this.iMillis = m22752(j, this.iChronology);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22752(long j, Chronology chronology) {
        return j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m22753(Chronology chronology) {
        return DateTimeUtils.m22716(chronology);
    }
}
