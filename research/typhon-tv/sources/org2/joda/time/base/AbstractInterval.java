package org2.joda.time.base;

import org2.joda.time.DateTime;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.Duration;
import org2.joda.time.Interval;
import org2.joda.time.MutableInterval;
import org2.joda.time.Period;
import org2.joda.time.PeriodType;
import org2.joda.time.ReadableInstant;
import org2.joda.time.ReadableInterval;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.format.DateTimeFormatter;
import org2.joda.time.format.ISODateTimeFormat;

public abstract class AbstractInterval implements ReadableInterval {
    protected AbstractInterval() {
    }

    public boolean contains(long j) {
        return j >= getStartMillis() && j < getEndMillis();
    }

    public boolean contains(ReadableInstant readableInstant) {
        return readableInstant == null ? containsNow() : contains(readableInstant.getMillis());
    }

    public boolean contains(ReadableInterval readableInterval) {
        if (readableInterval == null) {
            return containsNow();
        }
        long startMillis = readableInterval.getStartMillis();
        long endMillis = readableInterval.getEndMillis();
        long startMillis2 = getStartMillis();
        long endMillis2 = getEndMillis();
        return startMillis2 <= startMillis && startMillis < endMillis2 && endMillis <= endMillis2;
    }

    public boolean containsNow() {
        return contains(DateTimeUtils.m22712());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReadableInterval)) {
            return false;
        }
        ReadableInterval readableInterval = (ReadableInterval) obj;
        return getStartMillis() == readableInterval.getStartMillis() && getEndMillis() == readableInterval.getEndMillis() && FieldUtils.m23040((Object) getChronology(), (Object) readableInterval.getChronology());
    }

    public DateTime getEnd() {
        return new DateTime(getEndMillis(), getChronology());
    }

    public DateTime getStart() {
        return new DateTime(getStartMillis(), getChronology());
    }

    public int hashCode() {
        long startMillis = getStartMillis();
        long endMillis = getEndMillis();
        return ((((((int) (startMillis ^ (startMillis >>> 32))) + 3007) * 31) + ((int) (endMillis ^ (endMillis >>> 32)))) * 31) + getChronology().hashCode();
    }

    public boolean isAfter(long j) {
        return getStartMillis() > j;
    }

    public boolean isAfter(ReadableInstant readableInstant) {
        return readableInstant == null ? isAfterNow() : isAfter(readableInstant.getMillis());
    }

    public boolean isAfter(ReadableInterval readableInterval) {
        return getStartMillis() >= (readableInterval == null ? DateTimeUtils.m22712() : readableInterval.getEndMillis());
    }

    public boolean isAfterNow() {
        return isAfter(DateTimeUtils.m22712());
    }

    public boolean isBefore(long j) {
        return getEndMillis() <= j;
    }

    public boolean isBefore(ReadableInstant readableInstant) {
        return readableInstant == null ? isBeforeNow() : isBefore(readableInstant.getMillis());
    }

    public boolean isBefore(ReadableInterval readableInterval) {
        return readableInterval == null ? isBeforeNow() : isBefore(readableInterval.getStartMillis());
    }

    public boolean isBeforeNow() {
        return isBefore(DateTimeUtils.m22712());
    }

    public boolean isEqual(ReadableInterval readableInterval) {
        return getStartMillis() == readableInterval.getStartMillis() && getEndMillis() == readableInterval.getEndMillis();
    }

    public boolean overlaps(ReadableInterval readableInterval) {
        long startMillis = getStartMillis();
        long endMillis = getEndMillis();
        if (readableInterval == null) {
            long r6 = DateTimeUtils.m22712();
            return startMillis < r6 && r6 < endMillis;
        }
        return startMillis < readableInterval.getEndMillis() && readableInterval.getStartMillis() < endMillis;
    }

    public Duration toDuration() {
        long durationMillis = toDurationMillis();
        return durationMillis == 0 ? Duration.ZERO : new Duration(durationMillis);
    }

    public long toDurationMillis() {
        return FieldUtils.m23027(getEndMillis(), getStartMillis());
    }

    public Interval toInterval() {
        return new Interval(getStartMillis(), getEndMillis(), getChronology());
    }

    public MutableInterval toMutableInterval() {
        return new MutableInterval(getStartMillis(), getEndMillis(), getChronology());
    }

    public Period toPeriod() {
        return new Period(getStartMillis(), getEndMillis(), getChronology());
    }

    public Period toPeriod(PeriodType periodType) {
        return new Period(getStartMillis(), getEndMillis(), periodType, getChronology());
    }

    public String toString() {
        DateTimeFormatter r0 = ISODateTimeFormat.m23265().m23069(getChronology());
        StringBuffer stringBuffer = new StringBuffer(48);
        r0.m23075(stringBuffer, getStartMillis());
        stringBuffer.append('/');
        r0.m23075(stringBuffer, getEndMillis());
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22746(long j, long j2) {
        if (j2 < j) {
            throw new IllegalArgumentException("The end instant must be greater than the start instant");
        }
    }
}
