package org2.joda.time.base;

import java.io.Serializable;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.Duration;
import org2.joda.time.DurationFieldType;
import org2.joda.time.MutablePeriod;
import org2.joda.time.PeriodType;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadableDuration;
import org2.joda.time.ReadableInstant;
import org2.joda.time.ReadablePartial;
import org2.joda.time.ReadablePeriod;
import org2.joda.time.chrono.ISOChronology;
import org2.joda.time.convert.ConverterManager;
import org2.joda.time.convert.PeriodConverter;
import org2.joda.time.field.FieldUtils;

public abstract class BasePeriod extends AbstractPeriod implements Serializable, ReadablePeriod {
    private static final long serialVersionUID = -2110953284060001145L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ReadablePeriod f17844 = new AbstractPeriod() {
        public PeriodType getPeriodType() {
            return PeriodType.time();
        }

        public int getValue(int i) {
            return 0;
        }
    };
    private final PeriodType iType;
    private final int[] iValues;

    protected BasePeriod(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, PeriodType periodType) {
        this.iType = m22762(periodType);
        this.iValues = m22758(i, i2, i3, i4, i5, i6, i7, i8);
    }

    protected BasePeriod(long j) {
        this.iType = PeriodType.standard();
        int[] iArr = ISOChronology.getInstanceUTC().get(f17844, j);
        this.iValues = new int[8];
        System.arraycopy(iArr, 0, this.iValues, 4, 4);
    }

    protected BasePeriod(long j, long j2, PeriodType periodType, Chronology chronology) {
        PeriodType r1 = m22762(periodType);
        Chronology r0 = DateTimeUtils.m22716(chronology);
        this.iType = r1;
        this.iValues = r0.get(this, j, j2);
    }

    protected BasePeriod(long j, PeriodType periodType, Chronology chronology) {
        PeriodType r0 = m22762(periodType);
        Chronology r1 = DateTimeUtils.m22716(chronology);
        this.iType = r0;
        this.iValues = r1.get((ReadablePeriod) this, j);
    }

    protected BasePeriod(Object obj, PeriodType periodType, Chronology chronology) {
        PeriodConverter r0 = ConverterManager.m22967().m22970(obj);
        PeriodType r1 = m22762(periodType == null ? r0.a_(obj) : periodType);
        this.iType = r1;
        if (this instanceof ReadWritablePeriod) {
            this.iValues = new int[size()];
            r0.m22995((ReadWritablePeriod) this, obj, DateTimeUtils.m22716(chronology));
            return;
        }
        this.iValues = new MutablePeriod(obj, r1, chronology).getValues();
    }

    protected BasePeriod(ReadableDuration readableDuration, ReadableInstant readableInstant, PeriodType periodType) {
        PeriodType r1 = m22762(periodType);
        long r2 = DateTimeUtils.m22713(readableDuration);
        long r4 = DateTimeUtils.m22714(readableInstant);
        long r22 = FieldUtils.m23027(r4, r2);
        Chronology r0 = DateTimeUtils.m22709(readableInstant);
        this.iType = r1;
        this.iValues = r0.get(this, r22, r4);
    }

    protected BasePeriod(ReadableInstant readableInstant, ReadableDuration readableDuration, PeriodType periodType) {
        PeriodType r1 = m22762(periodType);
        long r2 = DateTimeUtils.m22714(readableInstant);
        long r4 = FieldUtils.m23036(r2, DateTimeUtils.m22713(readableDuration));
        Chronology r0 = DateTimeUtils.m22709(readableInstant);
        this.iType = r1;
        this.iValues = r0.get(this, r2, r4);
    }

    protected BasePeriod(ReadableInstant readableInstant, ReadableInstant readableInstant2, PeriodType periodType) {
        PeriodType r1 = m22762(periodType);
        if (readableInstant == null && readableInstant2 == null) {
            this.iType = r1;
            this.iValues = new int[size()];
            return;
        }
        long r2 = DateTimeUtils.m22714(readableInstant);
        long r4 = DateTimeUtils.m22714(readableInstant2);
        Chronology r0 = DateTimeUtils.m22717(readableInstant, readableInstant2);
        this.iType = r1;
        this.iValues = r0.get(this, r2, r4);
    }

    protected BasePeriod(ReadablePartial readablePartial, ReadablePartial readablePartial2, PeriodType periodType) {
        if (readablePartial == null || readablePartial2 == null) {
            throw new IllegalArgumentException("ReadablePartial objects must not be null");
        } else if ((readablePartial instanceof BaseLocal) && (readablePartial2 instanceof BaseLocal) && readablePartial.getClass() == readablePartial2.getClass()) {
            PeriodType r1 = m22762(periodType);
            long r2 = ((BaseLocal) readablePartial).m22755();
            long r4 = ((BaseLocal) readablePartial2).m22755();
            Chronology r0 = DateTimeUtils.m22716(readablePartial.getChronology());
            this.iType = r1;
            this.iValues = r0.get(this, r2, r4);
        } else if (readablePartial.size() != readablePartial2.size()) {
            throw new IllegalArgumentException("ReadablePartial objects must have the same set of fields");
        } else {
            int size = readablePartial.size();
            for (int i = 0; i < size; i++) {
                if (readablePartial.getFieldType(i) != readablePartial2.getFieldType(i)) {
                    throw new IllegalArgumentException("ReadablePartial objects must have the same set of fields");
                }
            }
            if (!DateTimeUtils.m22722(readablePartial)) {
                throw new IllegalArgumentException("ReadablePartial objects must be contiguous");
            }
            this.iType = m22762(periodType);
            Chronology withUTC = DateTimeUtils.m22716(readablePartial.getChronology()).withUTC();
            this.iValues = withUTC.get(this, withUTC.set(readablePartial, 0), withUTC.set(readablePartial2, 0));
        }
    }

    protected BasePeriod(int[] iArr, PeriodType periodType) {
        this.iType = periodType;
        this.iValues = iArr;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m22756(ReadablePeriod readablePeriod) {
        int[] iArr = new int[size()];
        int size = readablePeriod.size();
        for (int i = 0; i < size; i++) {
            m22757(readablePeriod.getFieldType(i), iArr, readablePeriod.getValue(i));
        }
        m22765(iArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m22757(DurationFieldType durationFieldType, int[] iArr, int i) {
        int indexOf = indexOf(durationFieldType);
        if (indexOf != -1) {
            iArr[indexOf] = i;
        } else if (i != 0) {
            throw new IllegalArgumentException("Period does not support field '" + durationFieldType.getName() + "'");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int[] m22758(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int[] iArr = new int[size()];
        m22757(DurationFieldType.years(), iArr, i);
        m22757(DurationFieldType.months(), iArr, i2);
        m22757(DurationFieldType.weeks(), iArr, i3);
        m22757(DurationFieldType.days(), iArr, i4);
        m22757(DurationFieldType.hours(), iArr, i5);
        m22757(DurationFieldType.minutes(), iArr, i6);
        m22757(DurationFieldType.seconds(), iArr, i7);
        m22757(DurationFieldType.millis(), iArr, i8);
        return iArr;
    }

    public PeriodType getPeriodType() {
        return this.iType;
    }

    public int getValue(int i) {
        return this.iValues[i];
    }

    /* access modifiers changed from: protected */
    public void mergePeriod(ReadablePeriod readablePeriod) {
        if (readablePeriod != null) {
            m22765(m22767(getValues(), readablePeriod));
        }
    }

    /* access modifiers changed from: protected */
    public void setPeriod(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        m22765(m22758(i, i2, i3, i4, i5, i6, i7, i8));
    }

    /* access modifiers changed from: protected */
    public void setPeriod(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            m22765(new int[size()]);
        } else {
            m22756(readablePeriod);
        }
    }

    /* access modifiers changed from: protected */
    public void setValue(int i, int i2) {
        this.iValues[i] = i2;
    }

    public Duration toDurationFrom(ReadableInstant readableInstant) {
        long r0 = DateTimeUtils.m22714(readableInstant);
        return new Duration(r0, DateTimeUtils.m22709(readableInstant).add((ReadablePeriod) this, r0, 1));
    }

    public Duration toDurationTo(ReadableInstant readableInstant) {
        long r0 = DateTimeUtils.m22714(readableInstant);
        return new Duration(DateTimeUtils.m22709(readableInstant).add((ReadablePeriod) this, r0, -1), r0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22759(DurationFieldType durationFieldType, int i) {
        m22760(this.iValues, durationFieldType, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m22760(int[] iArr, DurationFieldType durationFieldType, int i) {
        int indexOf = indexOf(durationFieldType);
        if (indexOf != -1) {
            iArr[indexOf] = FieldUtils.m23031(iArr[indexOf], i);
        } else if (i != 0 || durationFieldType == null) {
            throw new IllegalArgumentException("Period does not support field '" + durationFieldType + "'");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public int[] m22761(int[] iArr, ReadablePeriod readablePeriod) {
        int size = readablePeriod.size();
        for (int i = 0; i < size; i++) {
            DurationFieldType fieldType = readablePeriod.getFieldType(i);
            int value = readablePeriod.getValue(i);
            if (value != 0) {
                int indexOf = indexOf(fieldType);
                if (indexOf == -1) {
                    throw new IllegalArgumentException("Period does not support field '" + fieldType.getName() + "'");
                }
                iArr[indexOf] = FieldUtils.m23031(getValue(indexOf), value);
            }
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodType m22762(PeriodType periodType) {
        return DateTimeUtils.m22720(periodType);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22763(DurationFieldType durationFieldType, int i) {
        m22766(this.iValues, durationFieldType, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22764(ReadablePeriod readablePeriod) {
        if (readablePeriod != null) {
            m22765(m22761(getValues(), readablePeriod));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22765(int[] iArr) {
        System.arraycopy(iArr, 0, this.iValues, 0, this.iValues.length);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22766(int[] iArr, DurationFieldType durationFieldType, int i) {
        int indexOf = indexOf(durationFieldType);
        if (indexOf != -1) {
            iArr[indexOf] = i;
        } else if (i != 0 || durationFieldType == null) {
            throw new IllegalArgumentException("Period does not support field '" + durationFieldType + "'");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m22767(int[] iArr, ReadablePeriod readablePeriod) {
        int size = readablePeriod.size();
        for (int i = 0; i < size; i++) {
            m22757(readablePeriod.getFieldType(i), iArr, readablePeriod.getValue(i));
        }
        return iArr;
    }
}
