package org2.joda.time.base;

import java.io.Serializable;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.ReadablePartial;
import org2.joda.time.convert.ConverterManager;
import org2.joda.time.convert.PartialConverter;
import org2.joda.time.format.DateTimeFormat;
import org2.joda.time.format.DateTimeFormatter;

public abstract class BasePartial extends AbstractPartial implements Serializable, ReadablePartial {
    private static final long serialVersionUID = 2353678632973660L;
    private final Chronology iChronology;
    private final int[] iValues;

    protected BasePartial() {
        this(DateTimeUtils.m22712(), (Chronology) null);
    }

    protected BasePartial(long j) {
        this(j, (Chronology) null);
    }

    protected BasePartial(long j, Chronology chronology) {
        Chronology r0 = DateTimeUtils.m22716(chronology);
        this.iChronology = r0.withUTC();
        this.iValues = r0.get((ReadablePartial) this, j);
    }

    protected BasePartial(Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter) {
        PartialConverter r0 = ConverterManager.m22967().m22969(obj);
        Chronology r1 = DateTimeUtils.m22716(r0.m22992(obj, chronology));
        this.iChronology = r1.withUTC();
        this.iValues = r0.m22994(this, obj, r1, dateTimeFormatter);
    }

    protected BasePartial(Chronology chronology) {
        this(DateTimeUtils.m22712(), chronology);
    }

    protected BasePartial(BasePartial basePartial, Chronology chronology) {
        this.iChronology = chronology.withUTC();
        this.iValues = basePartial.iValues;
    }

    protected BasePartial(BasePartial basePartial, int[] iArr) {
        this.iChronology = basePartial.iChronology;
        this.iValues = iArr;
    }

    protected BasePartial(int[] iArr, Chronology chronology) {
        Chronology r0 = DateTimeUtils.m22716(chronology);
        this.iChronology = r0.withUTC();
        r0.validate(this, iArr);
        this.iValues = iArr;
    }

    public Chronology getChronology() {
        return this.iChronology;
    }

    public int getValue(int i) {
        return this.iValues[i];
    }

    public int[] getValues() {
        return (int[]) this.iValues.clone();
    }

    public String toString(String str) {
        return str == null ? toString() : DateTimeFormat.m23049(str).m23067((ReadablePartial) this);
    }

    public String toString(String str, Locale locale) throws IllegalArgumentException {
        return str == null ? toString() : DateTimeFormat.m23049(str).m23068(locale).m23067((ReadablePartial) this);
    }
}
