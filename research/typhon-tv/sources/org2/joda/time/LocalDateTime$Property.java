package org2.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Locale;
import org2.joda.time.field.AbstractReadableInstantFieldProperty;

public final class LocalDateTime$Property extends AbstractReadableInstantFieldProperty {
    private static final long serialVersionUID = -358138762846288L;

    /* renamed from: 靐  reason: contains not printable characters */
    private transient DateTimeField f17812;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient LocalDateTime f17813;

    LocalDateTime$Property(LocalDateTime localDateTime, DateTimeField dateTimeField) {
        this.f17813 = localDateTime;
        this.f17812 = dateTimeField;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        this.f17813 = (LocalDateTime) objectInputStream.readObject();
        this.f17812 = ((DateTimeFieldType) objectInputStream.readObject()).getField(this.f17813.getChronology());
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.f17813);
        objectOutputStream.writeObject(this.f17812.getType());
    }

    public LocalDateTime addToCopy(int i) {
        return this.f17813.龘(this.f17812.add(this.f17813.龘(), i));
    }

    public LocalDateTime addToCopy(long j) {
        return this.f17813.龘(this.f17812.add(this.f17813.龘(), j));
    }

    public LocalDateTime addWrapFieldToCopy(int i) {
        return this.f17813.龘(this.f17812.addWrapField(this.f17813.龘(), i));
    }

    public DateTimeField getField() {
        return this.f17812;
    }

    public LocalDateTime getLocalDateTime() {
        return this.f17813;
    }

    public LocalDateTime roundCeilingCopy() {
        return this.f17813.龘(this.f17812.roundCeiling(this.f17813.龘()));
    }

    public LocalDateTime roundFloorCopy() {
        return this.f17813.龘(this.f17812.roundFloor(this.f17813.龘()));
    }

    public LocalDateTime roundHalfCeilingCopy() {
        return this.f17813.龘(this.f17812.roundHalfCeiling(this.f17813.龘()));
    }

    public LocalDateTime roundHalfEvenCopy() {
        return this.f17813.龘(this.f17812.roundHalfEven(this.f17813.龘()));
    }

    public LocalDateTime roundHalfFloorCopy() {
        return this.f17813.龘(this.f17812.roundHalfFloor(this.f17813.龘()));
    }

    public LocalDateTime setCopy(int i) {
        return this.f17813.龘(this.f17812.set(this.f17813.龘(), i));
    }

    public LocalDateTime setCopy(String str) {
        return setCopy(str, (Locale) null);
    }

    public LocalDateTime setCopy(String str, Locale locale) {
        return this.f17813.龘(this.f17812.set(this.f17813.龘(), str, locale));
    }

    public LocalDateTime withMaximumValue() {
        return setCopy(getMaximumValue());
    }

    public LocalDateTime withMinimumValue() {
        return setCopy(getMinimumValue());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m22732() {
        return this.f17813.getChronology();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22733() {
        return this.f17813.龘();
    }
}
