package org2.joda.time;

import java.io.Serializable;

public abstract class DurationFieldType implements Serializable {
    private static final long serialVersionUID = 8765135187319L;

    /* renamed from: ʻ  reason: contains not printable characters */
    static final DurationFieldType f17798 = new StandardDurationFieldType("weeks", (byte) 6);

    /* renamed from: ʼ  reason: contains not printable characters */
    static final DurationFieldType f17799 = new StandardDurationFieldType("days", (byte) 7);

    /* renamed from: ʽ  reason: contains not printable characters */
    static final DurationFieldType f17800 = new StandardDurationFieldType("halfdays", (byte) 8);

    /* renamed from: ˈ  reason: contains not printable characters */
    static final DurationFieldType f17801 = new StandardDurationFieldType("millis", (byte) 12);

    /* renamed from: ˑ  reason: contains not printable characters */
    static final DurationFieldType f17802 = new StandardDurationFieldType("hours", (byte) 9);

    /* renamed from: ٴ  reason: contains not printable characters */
    static final DurationFieldType f17803 = new StandardDurationFieldType("minutes", (byte) 10);

    /* renamed from: ᐧ  reason: contains not printable characters */
    static final DurationFieldType f17804 = new StandardDurationFieldType("seconds", (byte) 11);

    /* renamed from: 连任  reason: contains not printable characters */
    static final DurationFieldType f17805 = new StandardDurationFieldType("months", (byte) 5);

    /* renamed from: 靐  reason: contains not printable characters */
    static final DurationFieldType f17806 = new StandardDurationFieldType("centuries", (byte) 2);

    /* renamed from: 麤  reason: contains not printable characters */
    static final DurationFieldType f17807 = new StandardDurationFieldType("years", (byte) 4);

    /* renamed from: 齉  reason: contains not printable characters */
    static final DurationFieldType f17808 = new StandardDurationFieldType("weekyears", (byte) 3);

    /* renamed from: 龘  reason: contains not printable characters */
    static final DurationFieldType f17809 = new StandardDurationFieldType("eras", (byte) 1);
    private final String iName;

    private static class StandardDurationFieldType extends DurationFieldType {
        private static final long serialVersionUID = 31156755687123L;
        private final byte iOrdinal;

        StandardDurationFieldType(String str, byte b) {
            super(str);
            this.iOrdinal = b;
        }

        private Object readResolve() {
            switch (this.iOrdinal) {
                case 1:
                    return f17809;
                case 2:
                    return f17806;
                case 3:
                    return f17808;
                case 4:
                    return f17807;
                case 5:
                    return f17805;
                case 6:
                    return f17798;
                case 7:
                    return f17799;
                case 8:
                    return f17800;
                case 9:
                    return f17802;
                case 10:
                    return f17803;
                case 11:
                    return f17804;
                case 12:
                    return f17801;
                default:
                    return this;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof StandardDurationFieldType) {
                return this.iOrdinal == ((StandardDurationFieldType) obj).iOrdinal;
            }
            return false;
        }

        public DurationField getField(Chronology chronology) {
            Chronology r0 = DateTimeUtils.m22716(chronology);
            switch (this.iOrdinal) {
                case 1:
                    return r0.eras();
                case 2:
                    return r0.centuries();
                case 3:
                    return r0.weekyears();
                case 4:
                    return r0.years();
                case 5:
                    return r0.months();
                case 6:
                    return r0.weeks();
                case 7:
                    return r0.days();
                case 8:
                    return r0.halfdays();
                case 9:
                    return r0.hours();
                case 10:
                    return r0.minutes();
                case 11:
                    return r0.seconds();
                case 12:
                    return r0.millis();
                default:
                    throw new InternalError();
            }
        }

        public int hashCode() {
            return 1 << this.iOrdinal;
        }
    }

    protected DurationFieldType(String str) {
        this.iName = str;
    }

    public static DurationFieldType centuries() {
        return f17806;
    }

    public static DurationFieldType days() {
        return f17799;
    }

    public static DurationFieldType eras() {
        return f17809;
    }

    public static DurationFieldType halfdays() {
        return f17800;
    }

    public static DurationFieldType hours() {
        return f17802;
    }

    public static DurationFieldType millis() {
        return f17801;
    }

    public static DurationFieldType minutes() {
        return f17803;
    }

    public static DurationFieldType months() {
        return f17805;
    }

    public static DurationFieldType seconds() {
        return f17804;
    }

    public static DurationFieldType weeks() {
        return f17798;
    }

    public static DurationFieldType weekyears() {
        return f17808;
    }

    public static DurationFieldType years() {
        return f17807;
    }

    public abstract DurationField getField(Chronology chronology);

    public String getName() {
        return this.iName;
    }

    public boolean isSupported(Chronology chronology) {
        return getField(chronology).isSupported();
    }

    public String toString() {
        return getName();
    }
}
