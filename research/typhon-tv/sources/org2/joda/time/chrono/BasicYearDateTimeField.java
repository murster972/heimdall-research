package org2.joda.time.chrono;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.field.ImpreciseDateTimeField;

class BasicYearDateTimeField extends ImpreciseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    protected final BasicChronology f17949;

    BasicYearDateTimeField(BasicChronology basicChronology) {
        super(DateTimeFieldType.year(), basicChronology.m22792());
        this.f17949 = basicChronology;
    }

    public long add(long j, int i) {
        return i == 0 ? j : set(j, FieldUtils.m23031(get(j), i));
    }

    public long add(long j, long j2) {
        return add(j, FieldUtils.m23034(j2));
    }

    public long addWrapField(long j, int i) {
        return i == 0 ? j : set(j, FieldUtils.m23033(this.f17949.m22817(j), i, this.f17949.m22798(), this.f17949.m22783()));
    }

    public int get(long j) {
        return this.f17949.m22817(j);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return j < j2 ? -this.f17949.m22822(j2, j) : this.f17949.m22822(j, j2);
    }

    public int getLeapAmount(long j) {
        return this.f17949.m22801(get(j)) ? 1 : 0;
    }

    public DurationField getLeapDurationField() {
        return this.f17949.days();
    }

    public int getMaximumValue() {
        return this.f17949.m22783();
    }

    public int getMinimumValue() {
        return this.f17949.m22798();
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public boolean isLeap(long j) {
        return this.f17949.m22801(get(j));
    }

    public boolean isLenient() {
        return false;
    }

    public long remainder(long j) {
        return j - roundFloor(j);
    }

    public long roundCeiling(long j) {
        int i = get(j);
        return j != this.f17949.m22810(i) ? this.f17949.m22810(i + 1) : j;
    }

    public long roundFloor(long j) {
        return this.f17949.m22810(get(j));
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, this.f17949.m22798(), this.f17949.m22783());
        return this.f17949.m22786(j, i);
    }

    public long setExtended(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, this.f17949.m22798() - 1, this.f17949.m22783() + 1);
        return this.f17949.m22786(j, i);
    }
}
