package org2.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTime;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadableDateTime;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.SkipDateTimeField;

public final class EthiopicChronology extends BasicFixedMonthChronology {
    public static final int EE = 1;
    private static final long serialVersionUID = -5972804258688333942L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, EthiopicChronology[]> f17956 = new ConcurrentHashMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final EthiopicChronology f17957 = getInstance(DateTimeZone.UTC);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField f17958 = new BasicSingleEraDateTimeField("EE");

    EthiopicChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    public static EthiopicChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), 4);
    }

    public static EthiopicChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, 4);
    }

    public static EthiopicChronology getInstance(DateTimeZone dateTimeZone, int i) {
        EthiopicChronology[] ethiopicChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        EthiopicChronology[] ethiopicChronologyArr2 = f17956.get(dateTimeZone);
        if (ethiopicChronologyArr2 == null) {
            EthiopicChronology[] ethiopicChronologyArr3 = new EthiopicChronology[7];
            EthiopicChronology[] putIfAbsent = f17956.putIfAbsent(dateTimeZone, ethiopicChronologyArr3);
            ethiopicChronologyArr = putIfAbsent != null ? putIfAbsent : ethiopicChronologyArr3;
        } else {
            ethiopicChronologyArr = ethiopicChronologyArr2;
        }
        try {
            EthiopicChronology ethiopicChronology = ethiopicChronologyArr[i - 1];
            if (ethiopicChronology == null) {
                synchronized (ethiopicChronologyArr) {
                    ethiopicChronology = ethiopicChronologyArr[i - 1];
                    if (ethiopicChronology == null) {
                        if (dateTimeZone == DateTimeZone.UTC) {
                            EthiopicChronology ethiopicChronology2 = new EthiopicChronology((Chronology) null, (Object) null, i);
                            ethiopicChronology = new EthiopicChronology(LimitChronology.getInstance(ethiopicChronology2, new DateTime(1, 1, 1, 0, 0, 0, 0, ethiopicChronology2), (ReadableDateTime) null), (Object) null, i);
                        } else {
                            ethiopicChronology = new EthiopicChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, i), dateTimeZone), (Object) null, i);
                        }
                        ethiopicChronologyArr[i - 1] = ethiopicChronology;
                    }
                }
            }
            return ethiopicChronology;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
    }

    public static EthiopicChronology getInstanceUTC() {
        return f17957;
    }

    private Object readResolve() {
        Chronology r0 = m22774();
        return r0 == null ? getInstance(DateTimeZone.UTC, getMinimumDaysInFirstWeek()) : getInstance(r0.getZone(), getMinimumDaysInFirstWeek());
    }

    public Chronology withUTC() {
        return f17957;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22856() {
        return 292272984;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m22857(int i) {
        int i2;
        int i3 = i - 1963;
        if (i3 <= 0) {
            i2 = (i3 + 3) >> 2;
        } else {
            i2 = i3 >> 2;
            if (!m22832(i)) {
                i2++;
            }
        }
        return ((((long) i2) + (((long) i3) * 365)) * 86400000) + 21859200000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m22858(long j) {
        return dayOfMonth().get(j) == 6 && monthOfYear().isLeap(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m22859() {
        return 30962844000000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22860() {
        return -292269337;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22861(AssembledChronology.Fields fields) {
        if (m22774() == null) {
            super.m22823(fields);
            fields.f17915 = new SkipDateTimeField(this, fields.f17915);
            fields.f17905 = new SkipDateTimeField(this, fields.f17905);
            fields.f17885 = f17958;
            fields.f17912 = new BasicMonthOfYearDateTimeField(this, 13);
            fields.f17895 = fields.f17912.getDurationField();
        }
    }
}
