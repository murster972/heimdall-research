package org2.joda.time.chrono;

import java.text.DateFormatSymbols;
import java.util.Locale;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.IllegalFieldValueException;

class GJLocaleSymbols {

    /* renamed from: 龘  reason: contains not printable characters */
    private static ConcurrentMap<Locale, GJLocaleSymbols> f17974 = new ConcurrentHashMap();

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String[] f17975;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String[] f17976;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final TreeMap<String, Integer> f17977;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final int f17978;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final int f17979;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final int f17980;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final TreeMap<String, Integer> f17981;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final TreeMap<String, Integer> f17982;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f17983;

    /* renamed from: 连任  reason: contains not printable characters */
    private final String[] f17984;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String[] f17985;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String[] f17986;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String[] f17987;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final int f17988;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private final int f17989;

    private GJLocaleSymbols(Locale locale) {
        DateFormatSymbols r0 = DateTimeUtils.m22715(locale);
        this.f17985 = r0.getEras();
        this.f17987 = m22874(r0.getWeekdays());
        this.f17986 = m22874(r0.getShortWeekdays());
        this.f17984 = m22879(r0.getMonths());
        this.f17975 = m22879(r0.getShortMonths());
        this.f17976 = r0.getAmPmStrings();
        Integer[] numArr = new Integer[13];
        for (int i = 0; i < 13; i++) {
            numArr[i] = Integer.valueOf(i);
        }
        this.f17977 = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        m22878(this.f17977, this.f17985, numArr);
        if ("en".equals(locale.getLanguage())) {
            this.f17977.put("BCE", numArr[0]);
            this.f17977.put("CE", numArr[1]);
        }
        this.f17981 = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        m22878(this.f17981, this.f17987, numArr);
        m22878(this.f17981, this.f17986, numArr);
        m22877(this.f17981, 1, 7, numArr);
        this.f17982 = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        m22878(this.f17982, this.f17984, numArr);
        m22878(this.f17982, this.f17975, numArr);
        m22877(this.f17982, 1, 12, numArr);
        this.f17983 = m22875(this.f17985);
        this.f17980 = m22875(this.f17987);
        this.f17978 = m22875(this.f17986);
        this.f17979 = m22875(this.f17984);
        this.f17988 = m22875(this.f17975);
        this.f17989 = m22875(this.f17976);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static String[] m22874(String[] strArr) {
        String[] strArr2 = new String[8];
        int i = 1;
        while (i < 8) {
            strArr2[i] = strArr[i < 7 ? i + 1 : 1];
            i++;
        }
        return strArr2;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static int m22875(String[] strArr) {
        int i;
        int i2 = 0;
        int length = strArr.length;
        while (true) {
            int i3 = length - 1;
            if (i3 < 0) {
                return i2;
            }
            String str = strArr[i3];
            if (str == null || (i = str.length()) <= i2) {
                i = i2;
            }
            i2 = i;
            length = i3;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static GJLocaleSymbols m22876(Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        GJLocaleSymbols gJLocaleSymbols = (GJLocaleSymbols) f17974.get(locale);
        if (gJLocaleSymbols != null) {
            return gJLocaleSymbols;
        }
        GJLocaleSymbols gJLocaleSymbols2 = new GJLocaleSymbols(locale);
        GJLocaleSymbols putIfAbsent = f17974.putIfAbsent(locale, gJLocaleSymbols2);
        return putIfAbsent != null ? putIfAbsent : gJLocaleSymbols2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m22877(TreeMap<String, Integer> treeMap, int i, int i2, Integer[] numArr) {
        while (i <= i2) {
            treeMap.put(String.valueOf(i).intern(), numArr[i]);
            i++;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m22878(TreeMap<String, Integer> treeMap, String[] strArr, Integer[] numArr) {
        int length = strArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                String str = strArr[length];
                if (str != null) {
                    treeMap.put(str, numArr[length]);
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] m22879(String[] strArr) {
        String[] strArr2 = new String[13];
        for (int i = 1; i < 13; i++) {
            strArr2[i] = strArr[i - 1];
        }
        return strArr2;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22880() {
        return this.f17989;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m22881(int i) {
        return this.f17976[i];
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m22882() {
        return this.f17978;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m22883(int i) {
        return this.f17986[i];
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m22884() {
        return this.f17979;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m22885(String str) {
        Integer num = this.f17982.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalFieldValueException(DateTimeFieldType.monthOfYear(), str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m22886(int i) {
        return this.f17984[i];
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m22887() {
        return this.f17980;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m22888(String str) {
        String[] strArr = this.f17976;
        int length = strArr.length;
        do {
            length--;
            if (length < 0) {
                throw new IllegalFieldValueException(DateTimeFieldType.halfdayOfDay(), str);
            }
        } while (!strArr[length].equalsIgnoreCase(str));
        return length;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m22889(int i) {
        return this.f17987[i];
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m22890() {
        return this.f17988;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m22891(String str) {
        Integer num = this.f17981.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalFieldValueException(DateTimeFieldType.dayOfWeek(), str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m22892(int i) {
        return this.f17975[i];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22893() {
        return this.f17983;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22894(String str) {
        Integer num = this.f17977.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalFieldValueException(DateTimeFieldType.era(), str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m22895(int i) {
        return this.f17985[i];
    }
}
