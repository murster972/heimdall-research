package org2.joda.time.chrono;

import org2.joda.time.Chronology;

abstract class BasicGJChronology extends BasicChronology {
    private static final long serialVersionUID = 538276888268L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final int[] f17939 = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    /* renamed from: 麤  reason: contains not printable characters */
    private static final long[] f17940 = new long[12];

    /* renamed from: 齉  reason: contains not printable characters */
    private static final long[] f17941 = new long[12];

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int[] f17942 = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    static {
        long j = 0;
        long j2 = 0;
        for (int i = 0; i < 11; i++) {
            j2 += ((long) f17942[i]) * 86400000;
            f17941[i + 1] = j2;
            j += ((long) f17939[i]) * 86400000;
            f17940[i + 1] = j;
        }
    }

    BasicGJChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22840(int i) {
        return f17939[i - 1];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public long m22841(long j, int i) {
        int r1 = m22817(j);
        int r0 = m22813(j, r1);
        int r2 = m22791(j);
        if (r0 > 59) {
            if (m22801(r1)) {
                if (!m22801(i)) {
                    r0--;
                }
            } else if (m22801(i)) {
                r0++;
            }
        }
        return m22821(i, 1, r0) + ((long) r2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m22842(long j) {
        return dayOfMonth().get(j) == 29 && monthOfYear().isLeap(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22843(long j, int i) {
        if (i > 28 || i < 1) {
            return m22793(j);
        }
        return 28;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m22844(int i, int i2) {
        return m22801(i) ? f17939[i2 - 1] : f17942[i2 - 1];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m22845(int i, int i2) {
        return m22801(i) ? f17940[i2 - 1] : f17941[i2 - 1];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22846(long j, int i) {
        int r5 = (int) ((j - m22810(i)) >> 10);
        if (m22801(i)) {
            if (r5 < 15356250) {
                if (r5 < 7678125) {
                    if (r5 < 2615625) {
                        return 1;
                    }
                    return r5 < 5062500 ? 2 : 3;
                } else if (r5 < 10209375) {
                    return 4;
                } else {
                    return r5 < 12825000 ? 5 : 6;
                }
            } else if (r5 < 23118750) {
                if (r5 < 17971875) {
                    return 7;
                }
                return r5 < 20587500 ? 8 : 9;
            } else if (r5 < 25734375) {
                return 10;
            } else {
                return r5 < 28265625 ? 11 : 12;
            }
        } else if (r5 < 15271875) {
            if (r5 < 7593750) {
                if (r5 >= 2615625) {
                    return r5 < 4978125 ? 2 : 3;
                }
                return 1;
            } else if (r5 < 10125000) {
                return 4;
            } else {
                return r5 < 12740625 ? 5 : 6;
            }
        } else if (r5 < 23034375) {
            if (r5 < 17887500) {
                return 7;
            }
            return r5 < 20503125 ? 8 : 9;
        } else if (r5 < 25650000) {
            return 10;
        } else {
            return r5 < 28181250 ? 11 : 12;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0035  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long m22847(long r10, long r12) {
        /*
            r9 = this;
            int r6 = r9.m22817((long) r10)
            int r7 = r9.m22817((long) r12)
            long r0 = r9.m22810((int) r6)
            long r2 = r10 - r0
            long r0 = r9.m22810((int) r7)
            long r0 = r12 - r0
            r4 = 5097600000(0x12fd73400, double:2.518549036E-314)
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 < 0) goto L_0x004f
            boolean r4 = r9.m22801((int) r7)
            if (r4 == 0) goto L_0x0039
            boolean r4 = r9.m22801((int) r6)
            if (r4 != 0) goto L_0x004f
            r4 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 - r4
            r4 = r2
            r2 = r0
        L_0x002f:
            int r0 = r6 - r7
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x0037
            int r0 = r0 + -1
        L_0x0037:
            long r0 = (long) r0
            return r0
        L_0x0039:
            r4 = 5097600000(0x12fd73400, double:2.518549036E-314)
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 < 0) goto L_0x004f
            boolean r4 = r9.m22801((int) r6)
            if (r4 == 0) goto L_0x004f
            r4 = 86400000(0x5265c00, double:4.2687272E-316)
            long r2 = r2 - r4
            r4 = r2
            r2 = r0
            goto L_0x002f
        L_0x004f:
            r4 = r2
            r2 = r0
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.joda.time.chrono.BasicGJChronology.m22847(long, long):long");
    }
}
