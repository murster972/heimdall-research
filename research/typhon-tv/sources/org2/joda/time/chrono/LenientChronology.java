package org2.joda.time.chrono;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.LenientDateTimeField;

public final class LenientChronology extends AssembledChronology {
    private static final long serialVersionUID = -3148237568046877177L;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient Chronology f18002;

    private LenientChronology(Chronology chronology) {
        super(chronology, (Object) null);
    }

    public static LenientChronology getInstance(Chronology chronology) {
        if (chronology != null) {
            return new LenientChronology(chronology);
        }
        throw new IllegalArgumentException("Must supply a chronology");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeField m22939(DateTimeField dateTimeField) {
        return LenientDateTimeField.getInstance(dateTimeField, m22774());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LenientChronology)) {
            return false;
        }
        return m22774().equals(((LenientChronology) obj).m22774());
    }

    public int hashCode() {
        return 236548278 + (m22774().hashCode() * 7);
    }

    public String toString() {
        return "LenientChronology[" + m22774().toString() + ']';
    }

    public Chronology withUTC() {
        if (this.f18002 == null) {
            if (getZone() == DateTimeZone.UTC) {
                this.f18002 = this;
            } else {
                this.f18002 = getInstance(m22774().withUTC());
            }
        }
        return this.f18002;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == DateTimeZone.UTC ? withUTC() : dateTimeZone != getZone() ? getInstance(m22774().withZone(dateTimeZone)) : this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22940(AssembledChronology.Fields fields) {
        fields.f17915 = m22939(fields.f17915);
        fields.f17900 = m22939(fields.f17900);
        fields.f17902 = m22939(fields.f17902);
        fields.f17882 = m22939(fields.f17882);
        fields.f17885 = m22939(fields.f17885);
        fields.f17897 = m22939(fields.f17897);
        fields.f17901 = m22939(fields.f17901);
        fields.f17903 = m22939(fields.f17903);
        fields.f17912 = m22939(fields.f17912);
        fields.f17904 = m22939(fields.f17904);
        fields.f17905 = m22939(fields.f17905);
        fields.f17906 = m22939(fields.f17906);
        fields.f17886 = m22939(fields.f17886);
        fields.f17887 = m22939(fields.f17887);
        fields.f17913 = m22939(fields.f17913);
        fields.f17914 = m22939(fields.f17914);
        fields.f17891 = m22939(fields.f17891);
        fields.f17892 = m22939(fields.f17892);
        fields.f17893 = m22939(fields.f17893);
        fields.f17890 = m22939(fields.f17890);
        fields.f17888 = m22939(fields.f17888);
        fields.f17894 = m22939(fields.f17894);
        fields.f17896 = m22939(fields.f17896);
    }
}
