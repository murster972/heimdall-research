package org2.joda.time.chrono;

import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.ReadablePartial;
import org2.joda.time.field.PreciseDurationDateTimeField;

final class BasicWeekOfWeekyearDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BasicChronology f17947;

    BasicWeekOfWeekyearDateTimeField(BasicChronology basicChronology, DurationField durationField) {
        super(DateTimeFieldType.weekOfWeekyear(), durationField);
        this.f17947 = basicChronology;
    }

    public int get(long j) {
        return this.f17947.m22785(j);
    }

    public int getMaximumValue() {
        return 53;
    }

    public int getMaximumValue(long j) {
        return this.f17947.m22802(this.f17947.m22799(j));
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        if (!readablePartial.isSupported(DateTimeFieldType.weekyear())) {
            return 53;
        }
        return this.f17947.m22802(readablePartial.get(DateTimeFieldType.weekyear()));
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        int size = readablePartial.size();
        for (int i = 0; i < size; i++) {
            if (readablePartial.getFieldType(i) == DateTimeFieldType.weekyear()) {
                return this.f17947.m22802(iArr[i]);
            }
        }
        return 53;
    }

    public int getMinimumValue() {
        return 1;
    }

    public DurationField getRangeDurationField() {
        return this.f17947.weekyears();
    }

    public long remainder(long j) {
        return super.remainder(259200000 + j);
    }

    public long roundCeiling(long j) {
        return super.roundCeiling(j + 259200000) - 259200000;
    }

    public long roundFloor(long j) {
        return super.roundFloor(j + 259200000) - 259200000;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22848(long j, int i) {
        if (i > 52) {
            return getMaximumValue(j);
        }
        return 52;
    }
}
