package org2.joda.time.chrono;

import java.util.Locale;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.field.PreciseDurationDateTimeField;

final class GJDayOfWeekDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BasicChronology f17972;

    GJDayOfWeekDateTimeField(BasicChronology basicChronology, DurationField durationField) {
        super(DateTimeFieldType.dayOfWeek(), durationField);
        this.f17972 = basicChronology;
    }

    public int get(long j) {
        return this.f17972.m22788(j);
    }

    public String getAsShortText(int i, Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22883(i);
    }

    public String getAsText(int i, Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22889(i);
    }

    public int getMaximumShortTextLength(Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22882();
    }

    public int getMaximumTextLength(Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22887();
    }

    public int getMaximumValue() {
        return 7;
    }

    public int getMinimumValue() {
        return 1;
    }

    public DurationField getRangeDurationField() {
        return this.f17972.weeks();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22873(String str, Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22891(str);
    }
}
