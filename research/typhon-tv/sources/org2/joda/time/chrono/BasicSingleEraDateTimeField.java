package org2.joda.time.chrono;

import java.util.Locale;
import net.pubnative.library.request.PubnativeRequest;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.DurationFieldType;
import org2.joda.time.IllegalFieldValueException;
import org2.joda.time.field.BaseDateTimeField;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.field.UnsupportedDurationField;

final class BasicSingleEraDateTimeField extends BaseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final String f17946;

    BasicSingleEraDateTimeField(String str) {
        super(DateTimeFieldType.era());
        this.f17946 = str;
    }

    public int get(long j) {
        return 1;
    }

    public String getAsText(int i, Locale locale) {
        return this.f17946;
    }

    public DurationField getDurationField() {
        return UnsupportedDurationField.getInstance(DurationFieldType.eras());
    }

    public int getMaximumTextLength(Locale locale) {
        return this.f17946.length();
    }

    public int getMaximumValue() {
        return 1;
    }

    public int getMinimumValue() {
        return 1;
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public boolean isLenient() {
        return false;
    }

    public long roundCeiling(long j) {
        return Long.MAX_VALUE;
    }

    public long roundFloor(long j) {
        return Long.MIN_VALUE;
    }

    public long roundHalfCeiling(long j) {
        return Long.MIN_VALUE;
    }

    public long roundHalfEven(long j) {
        return Long.MIN_VALUE;
    }

    public long roundHalfFloor(long j) {
        return Long.MIN_VALUE;
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, 1, 1);
        return j;
    }

    public long set(long j, String str, Locale locale) {
        if (this.f17946.equals(str) || PubnativeRequest.LEGACY_ZONE_ID.equals(str)) {
            return j;
        }
        throw new IllegalFieldValueException(DateTimeFieldType.era(), str);
    }
}
