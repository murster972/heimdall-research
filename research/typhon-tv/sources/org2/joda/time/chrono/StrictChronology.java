package org2.joda.time.chrono;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.StrictDateTimeField;

public final class StrictChronology extends AssembledChronology {
    private static final long serialVersionUID = 6633006628097111960L;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient Chronology f18008;

    private StrictChronology(Chronology chronology) {
        super(chronology, (Object) null);
    }

    public static StrictChronology getInstance(Chronology chronology) {
        if (chronology != null) {
            return new StrictChronology(chronology);
        }
        throw new IllegalArgumentException("Must supply a chronology");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField m22945(DateTimeField dateTimeField) {
        return StrictDateTimeField.getInstance(dateTimeField);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StrictChronology)) {
            return false;
        }
        return m22774().equals(((StrictChronology) obj).m22774());
    }

    public int hashCode() {
        return 352831696 + (m22774().hashCode() * 7);
    }

    public String toString() {
        return "StrictChronology[" + m22774().toString() + ']';
    }

    public Chronology withUTC() {
        if (this.f18008 == null) {
            if (getZone() == DateTimeZone.UTC) {
                this.f18008 = this;
            } else {
                this.f18008 = getInstance(m22774().withUTC());
            }
        }
        return this.f18008;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == DateTimeZone.UTC ? withUTC() : dateTimeZone != getZone() ? getInstance(m22774().withZone(dateTimeZone)) : this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22946(AssembledChronology.Fields fields) {
        fields.f17915 = m22945(fields.f17915);
        fields.f17900 = m22945(fields.f17900);
        fields.f17902 = m22945(fields.f17902);
        fields.f17882 = m22945(fields.f17882);
        fields.f17885 = m22945(fields.f17885);
        fields.f17897 = m22945(fields.f17897);
        fields.f17901 = m22945(fields.f17901);
        fields.f17903 = m22945(fields.f17903);
        fields.f17912 = m22945(fields.f17912);
        fields.f17904 = m22945(fields.f17904);
        fields.f17905 = m22945(fields.f17905);
        fields.f17906 = m22945(fields.f17906);
        fields.f17886 = m22945(fields.f17886);
        fields.f17887 = m22945(fields.f17887);
        fields.f17913 = m22945(fields.f17913);
        fields.f17914 = m22945(fields.f17914);
        fields.f17891 = m22945(fields.f17891);
        fields.f17892 = m22945(fields.f17892);
        fields.f17893 = m22945(fields.f17893);
        fields.f17890 = m22945(fields.f17890);
        fields.f17888 = m22945(fields.f17888);
        fields.f17894 = m22945(fields.f17894);
        fields.f17896 = m22945(fields.f17896);
    }
}
