package org2.joda.time.chrono;

import java.io.IOException;
import java.io.ObjectInputStream;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.DurationField;

public abstract class AssembledChronology extends BaseChronology {
    private static final long serialVersionUID = -6728465968995518215L;
    private final Chronology iBase;
    private final Object iParam;

    /* renamed from: ʻ  reason: contains not printable characters */
    private transient DurationField f17845;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    private transient DateTimeField f17846;

    /* renamed from: ʼ  reason: contains not printable characters */
    private transient DurationField f17847;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    private transient int f17848;

    /* renamed from: ʽ  reason: contains not printable characters */
    private transient DurationField f17849;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    private transient DateTimeField f17850;

    /* renamed from: ʾ  reason: contains not printable characters */
    private transient DateTimeField f17851;

    /* renamed from: ʿ  reason: contains not printable characters */
    private transient DateTimeField f17852;

    /* renamed from: ˆ  reason: contains not printable characters */
    private transient DateTimeField f17853;

    /* renamed from: ˈ  reason: contains not printable characters */
    private transient DurationField f17854;

    /* renamed from: ˉ  reason: contains not printable characters */
    private transient DateTimeField f17855;

    /* renamed from: ˊ  reason: contains not printable characters */
    private transient DateTimeField f17856;

    /* renamed from: ˋ  reason: contains not printable characters */
    private transient DateTimeField f17857;

    /* renamed from: ˎ  reason: contains not printable characters */
    private transient DateTimeField f17858;

    /* renamed from: ˏ  reason: contains not printable characters */
    private transient DateTimeField f17859;

    /* renamed from: ˑ  reason: contains not printable characters */
    private transient DurationField f17860;

    /* renamed from: י  reason: contains not printable characters */
    private transient DateTimeField f17861;

    /* renamed from: ـ  reason: contains not printable characters */
    private transient DateTimeField f17862;

    /* renamed from: ٴ  reason: contains not printable characters */
    private transient DurationField f17863;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private transient DurationField f17864;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private transient DateTimeField f17865;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private transient DateTimeField f17866;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private transient DateTimeField f17867;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private transient DateTimeField f17868;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private transient DateTimeField f17869;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private transient DateTimeField f17870;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private transient DateTimeField f17871;

    /* renamed from: 连任  reason: contains not printable characters */
    private transient DurationField f17872;

    /* renamed from: 靐  reason: contains not printable characters */
    private transient DurationField f17873;

    /* renamed from: 麤  reason: contains not printable characters */
    private transient DurationField f17874;

    /* renamed from: 齉  reason: contains not printable characters */
    private transient DurationField f17875;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient DurationField f17876;

    /* renamed from: ﹳ  reason: contains not printable characters */
    private transient DateTimeField f17877;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private transient DateTimeField f17878;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private transient DateTimeField f17879;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private transient DateTimeField f17880;

    public static final class Fields {

        /* renamed from: ʻ  reason: contains not printable characters */
        public DurationField f17881;

        /* renamed from: ʻʻ  reason: contains not printable characters */
        public DateTimeField f17882;

        /* renamed from: ʼ  reason: contains not printable characters */
        public DurationField f17883;

        /* renamed from: ʽ  reason: contains not printable characters */
        public DurationField f17884;

        /* renamed from: ʽʽ  reason: contains not printable characters */
        public DateTimeField f17885;

        /* renamed from: ʾ  reason: contains not printable characters */
        public DateTimeField f17886;

        /* renamed from: ʿ  reason: contains not printable characters */
        public DateTimeField f17887;

        /* renamed from: ˆ  reason: contains not printable characters */
        public DateTimeField f17888;

        /* renamed from: ˈ  reason: contains not printable characters */
        public DurationField f17889;

        /* renamed from: ˉ  reason: contains not printable characters */
        public DateTimeField f17890;

        /* renamed from: ˊ  reason: contains not printable characters */
        public DateTimeField f17891;

        /* renamed from: ˋ  reason: contains not printable characters */
        public DateTimeField f17892;

        /* renamed from: ˎ  reason: contains not printable characters */
        public DateTimeField f17893;

        /* renamed from: ˏ  reason: contains not printable characters */
        public DateTimeField f17894;

        /* renamed from: ˑ  reason: contains not printable characters */
        public DurationField f17895;

        /* renamed from: י  reason: contains not printable characters */
        public DateTimeField f17896;

        /* renamed from: ـ  reason: contains not printable characters */
        public DateTimeField f17897;

        /* renamed from: ٴ  reason: contains not printable characters */
        public DurationField f17898;

        /* renamed from: ᐧ  reason: contains not printable characters */
        public DurationField f17899;

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        public DateTimeField f17900;

        /* renamed from: ᴵ  reason: contains not printable characters */
        public DateTimeField f17901;

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        public DateTimeField f17902;

        /* renamed from: ᵎ  reason: contains not printable characters */
        public DateTimeField f17903;

        /* renamed from: ᵔ  reason: contains not printable characters */
        public DateTimeField f17904;

        /* renamed from: ᵢ  reason: contains not printable characters */
        public DateTimeField f17905;

        /* renamed from: ⁱ  reason: contains not printable characters */
        public DateTimeField f17906;

        /* renamed from: 连任  reason: contains not printable characters */
        public DurationField f17907;

        /* renamed from: 靐  reason: contains not printable characters */
        public DurationField f17908;

        /* renamed from: 麤  reason: contains not printable characters */
        public DurationField f17909;

        /* renamed from: 齉  reason: contains not printable characters */
        public DurationField f17910;

        /* renamed from: 龘  reason: contains not printable characters */
        public DurationField f17911;

        /* renamed from: ﹳ  reason: contains not printable characters */
        public DateTimeField f17912;

        /* renamed from: ﹶ  reason: contains not printable characters */
        public DateTimeField f17913;

        /* renamed from: ﾞ  reason: contains not printable characters */
        public DateTimeField f17914;

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public DateTimeField f17915;

        Fields() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static boolean m22776(DateTimeField dateTimeField) {
            if (dateTimeField == null) {
                return false;
            }
            return dateTimeField.isSupported();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private static boolean m22777(DurationField durationField) {
            if (durationField == null) {
                return false;
            }
            return durationField.isSupported();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m22778(Chronology chronology) {
            DurationField millis = chronology.millis();
            if (m22777(millis)) {
                this.f17911 = millis;
            }
            DurationField seconds = chronology.seconds();
            if (m22777(seconds)) {
                this.f17908 = seconds;
            }
            DurationField minutes = chronology.minutes();
            if (m22777(minutes)) {
                this.f17910 = minutes;
            }
            DurationField hours = chronology.hours();
            if (m22777(hours)) {
                this.f17909 = hours;
            }
            DurationField halfdays = chronology.halfdays();
            if (m22777(halfdays)) {
                this.f17907 = halfdays;
            }
            DurationField days = chronology.days();
            if (m22777(days)) {
                this.f17881 = days;
            }
            DurationField weeks = chronology.weeks();
            if (m22777(weeks)) {
                this.f17883 = weeks;
            }
            DurationField weekyears = chronology.weekyears();
            if (m22777(weekyears)) {
                this.f17884 = weekyears;
            }
            DurationField months = chronology.months();
            if (m22777(months)) {
                this.f17895 = months;
            }
            DurationField years = chronology.years();
            if (m22777(years)) {
                this.f17898 = years;
            }
            DurationField centuries = chronology.centuries();
            if (m22777(centuries)) {
                this.f17899 = centuries;
            }
            DurationField eras = chronology.eras();
            if (m22777(eras)) {
                this.f17889 = eras;
            }
            DateTimeField millisOfSecond = chronology.millisOfSecond();
            if (m22776(millisOfSecond)) {
                this.f17886 = millisOfSecond;
            }
            DateTimeField millisOfDay = chronology.millisOfDay();
            if (m22776(millisOfDay)) {
                this.f17887 = millisOfDay;
            }
            DateTimeField secondOfMinute = chronology.secondOfMinute();
            if (m22776(secondOfMinute)) {
                this.f17913 = secondOfMinute;
            }
            DateTimeField secondOfDay = chronology.secondOfDay();
            if (m22776(secondOfDay)) {
                this.f17914 = secondOfDay;
            }
            DateTimeField minuteOfHour = chronology.minuteOfHour();
            if (m22776(minuteOfHour)) {
                this.f17891 = minuteOfHour;
            }
            DateTimeField minuteOfDay = chronology.minuteOfDay();
            if (m22776(minuteOfDay)) {
                this.f17892 = minuteOfDay;
            }
            DateTimeField hourOfDay = chronology.hourOfDay();
            if (m22776(hourOfDay)) {
                this.f17893 = hourOfDay;
            }
            DateTimeField clockhourOfDay = chronology.clockhourOfDay();
            if (m22776(clockhourOfDay)) {
                this.f17888 = clockhourOfDay;
            }
            DateTimeField hourOfHalfday = chronology.hourOfHalfday();
            if (m22776(hourOfHalfday)) {
                this.f17890 = hourOfHalfday;
            }
            DateTimeField clockhourOfHalfday = chronology.clockhourOfHalfday();
            if (m22776(clockhourOfHalfday)) {
                this.f17894 = clockhourOfHalfday;
            }
            DateTimeField halfdayOfDay = chronology.halfdayOfDay();
            if (m22776(halfdayOfDay)) {
                this.f17896 = halfdayOfDay;
            }
            DateTimeField dayOfWeek = chronology.dayOfWeek();
            if (m22776(dayOfWeek)) {
                this.f17897 = dayOfWeek;
            }
            DateTimeField dayOfMonth = chronology.dayOfMonth();
            if (m22776(dayOfMonth)) {
                this.f17901 = dayOfMonth;
            }
            DateTimeField dayOfYear = chronology.dayOfYear();
            if (m22776(dayOfYear)) {
                this.f17903 = dayOfYear;
            }
            DateTimeField weekOfWeekyear = chronology.weekOfWeekyear();
            if (m22776(weekOfWeekyear)) {
                this.f17904 = weekOfWeekyear;
            }
            DateTimeField weekyear = chronology.weekyear();
            if (m22776(weekyear)) {
                this.f17905 = weekyear;
            }
            DateTimeField weekyearOfCentury = chronology.weekyearOfCentury();
            if (m22776(weekyearOfCentury)) {
                this.f17906 = weekyearOfCentury;
            }
            DateTimeField monthOfYear = chronology.monthOfYear();
            if (m22776(monthOfYear)) {
                this.f17912 = monthOfYear;
            }
            DateTimeField year = chronology.year();
            if (m22776(year)) {
                this.f17915 = year;
            }
            DateTimeField yearOfEra = chronology.yearOfEra();
            if (m22776(yearOfEra)) {
                this.f17900 = yearOfEra;
            }
            DateTimeField yearOfCentury = chronology.yearOfCentury();
            if (m22776(yearOfCentury)) {
                this.f17902 = yearOfCentury;
            }
            DateTimeField centuryOfEra = chronology.centuryOfEra();
            if (m22776(centuryOfEra)) {
                this.f17882 = centuryOfEra;
            }
            DateTimeField era = chronology.era();
            if (m22776(era)) {
                this.f17885 = era;
            }
        }
    }

    protected AssembledChronology(Chronology chronology, Object obj) {
        this.iBase = chronology;
        this.iParam = obj;
        m22772();
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        m22772();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m22772() {
        int i = 0;
        Fields fields = new Fields();
        if (this.iBase != null) {
            fields.m22778(this.iBase);
        }
        m22775(fields);
        DurationField durationField = fields.f17911;
        if (durationField == null) {
            durationField = super.millis();
        }
        this.f17876 = durationField;
        DurationField durationField2 = fields.f17908;
        if (durationField2 == null) {
            durationField2 = super.seconds();
        }
        this.f17873 = durationField2;
        DurationField durationField3 = fields.f17910;
        if (durationField3 == null) {
            durationField3 = super.minutes();
        }
        this.f17875 = durationField3;
        DurationField durationField4 = fields.f17909;
        if (durationField4 == null) {
            durationField4 = super.hours();
        }
        this.f17874 = durationField4;
        DurationField durationField5 = fields.f17907;
        if (durationField5 == null) {
            durationField5 = super.halfdays();
        }
        this.f17872 = durationField5;
        DurationField durationField6 = fields.f17881;
        if (durationField6 == null) {
            durationField6 = super.days();
        }
        this.f17845 = durationField6;
        DurationField durationField7 = fields.f17883;
        if (durationField7 == null) {
            durationField7 = super.weeks();
        }
        this.f17847 = durationField7;
        DurationField durationField8 = fields.f17884;
        if (durationField8 == null) {
            durationField8 = super.weekyears();
        }
        this.f17849 = durationField8;
        DurationField durationField9 = fields.f17895;
        if (durationField9 == null) {
            durationField9 = super.months();
        }
        this.f17860 = durationField9;
        DurationField durationField10 = fields.f17898;
        if (durationField10 == null) {
            durationField10 = super.years();
        }
        this.f17863 = durationField10;
        DurationField durationField11 = fields.f17899;
        if (durationField11 == null) {
            durationField11 = super.centuries();
        }
        this.f17864 = durationField11;
        DurationField durationField12 = fields.f17889;
        if (durationField12 == null) {
            durationField12 = super.eras();
        }
        this.f17854 = durationField12;
        DateTimeField dateTimeField = fields.f17886;
        if (dateTimeField == null) {
            dateTimeField = super.millisOfSecond();
        }
        this.f17851 = dateTimeField;
        DateTimeField dateTimeField2 = fields.f17887;
        if (dateTimeField2 == null) {
            dateTimeField2 = super.millisOfDay();
        }
        this.f17852 = dateTimeField2;
        DateTimeField dateTimeField3 = fields.f17913;
        if (dateTimeField3 == null) {
            dateTimeField3 = super.secondOfMinute();
        }
        this.f17878 = dateTimeField3;
        DateTimeField dateTimeField4 = fields.f17914;
        if (dateTimeField4 == null) {
            dateTimeField4 = super.secondOfDay();
        }
        this.f17879 = dateTimeField4;
        DateTimeField dateTimeField5 = fields.f17891;
        if (dateTimeField5 == null) {
            dateTimeField5 = super.minuteOfHour();
        }
        this.f17856 = dateTimeField5;
        DateTimeField dateTimeField6 = fields.f17892;
        if (dateTimeField6 == null) {
            dateTimeField6 = super.minuteOfDay();
        }
        this.f17857 = dateTimeField6;
        DateTimeField dateTimeField7 = fields.f17893;
        if (dateTimeField7 == null) {
            dateTimeField7 = super.hourOfDay();
        }
        this.f17858 = dateTimeField7;
        DateTimeField dateTimeField8 = fields.f17888;
        if (dateTimeField8 == null) {
            dateTimeField8 = super.clockhourOfDay();
        }
        this.f17853 = dateTimeField8;
        DateTimeField dateTimeField9 = fields.f17890;
        if (dateTimeField9 == null) {
            dateTimeField9 = super.hourOfHalfday();
        }
        this.f17855 = dateTimeField9;
        DateTimeField dateTimeField10 = fields.f17894;
        if (dateTimeField10 == null) {
            dateTimeField10 = super.clockhourOfHalfday();
        }
        this.f17859 = dateTimeField10;
        DateTimeField dateTimeField11 = fields.f17896;
        if (dateTimeField11 == null) {
            dateTimeField11 = super.halfdayOfDay();
        }
        this.f17861 = dateTimeField11;
        DateTimeField dateTimeField12 = fields.f17897;
        if (dateTimeField12 == null) {
            dateTimeField12 = super.dayOfWeek();
        }
        this.f17862 = dateTimeField12;
        DateTimeField dateTimeField13 = fields.f17901;
        if (dateTimeField13 == null) {
            dateTimeField13 = super.dayOfMonth();
        }
        this.f17866 = dateTimeField13;
        DateTimeField dateTimeField14 = fields.f17903;
        if (dateTimeField14 == null) {
            dateTimeField14 = super.dayOfYear();
        }
        this.f17868 = dateTimeField14;
        DateTimeField dateTimeField15 = fields.f17904;
        if (dateTimeField15 == null) {
            dateTimeField15 = super.weekOfWeekyear();
        }
        this.f17869 = dateTimeField15;
        DateTimeField dateTimeField16 = fields.f17905;
        if (dateTimeField16 == null) {
            dateTimeField16 = super.weekyear();
        }
        this.f17870 = dateTimeField16;
        DateTimeField dateTimeField17 = fields.f17906;
        if (dateTimeField17 == null) {
            dateTimeField17 = super.weekyearOfCentury();
        }
        this.f17871 = dateTimeField17;
        DateTimeField dateTimeField18 = fields.f17912;
        if (dateTimeField18 == null) {
            dateTimeField18 = super.monthOfYear();
        }
        this.f17877 = dateTimeField18;
        DateTimeField dateTimeField19 = fields.f17915;
        if (dateTimeField19 == null) {
            dateTimeField19 = super.year();
        }
        this.f17880 = dateTimeField19;
        DateTimeField dateTimeField20 = fields.f17900;
        if (dateTimeField20 == null) {
            dateTimeField20 = super.yearOfEra();
        }
        this.f17865 = dateTimeField20;
        DateTimeField dateTimeField21 = fields.f17902;
        if (dateTimeField21 == null) {
            dateTimeField21 = super.yearOfCentury();
        }
        this.f17867 = dateTimeField21;
        DateTimeField dateTimeField22 = fields.f17882;
        if (dateTimeField22 == null) {
            dateTimeField22 = super.centuryOfEra();
        }
        this.f17846 = dateTimeField22;
        DateTimeField dateTimeField23 = fields.f17885;
        if (dateTimeField23 == null) {
            dateTimeField23 = super.era();
        }
        this.f17850 = dateTimeField23;
        if (this.iBase != null) {
            int i2 = ((this.f17858 == this.iBase.hourOfDay() && this.f17856 == this.iBase.minuteOfHour() && this.f17878 == this.iBase.secondOfMinute() && this.f17851 == this.iBase.millisOfSecond()) ? 1 : 0) | (this.f17852 == this.iBase.millisOfDay() ? 2 : 0);
            if (this.f17880 == this.iBase.year() && this.f17877 == this.iBase.monthOfYear() && this.f17866 == this.iBase.dayOfMonth()) {
                i = 4;
            }
            i |= i2;
        }
        this.f17848 = i;
    }

    public final DurationField centuries() {
        return this.f17864;
    }

    public final DateTimeField centuryOfEra() {
        return this.f17846;
    }

    public final DateTimeField clockhourOfDay() {
        return this.f17853;
    }

    public final DateTimeField clockhourOfHalfday() {
        return this.f17859;
    }

    public final DateTimeField dayOfMonth() {
        return this.f17866;
    }

    public final DateTimeField dayOfWeek() {
        return this.f17862;
    }

    public final DateTimeField dayOfYear() {
        return this.f17868;
    }

    public final DurationField days() {
        return this.f17845;
    }

    public final DateTimeField era() {
        return this.f17850;
    }

    public final DurationField eras() {
        return this.f17854;
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology chronology = this.iBase;
        return (chronology == null || (this.f17848 & 6) != 6) ? super.getDateTimeMillis(i, i2, i3, i4) : chronology.getDateTimeMillis(i, i2, i3, i4);
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        Chronology chronology = this.iBase;
        return (chronology == null || (this.f17848 & 5) != 5) ? super.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7) : chronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
    }

    public long getDateTimeMillis(long j, int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology chronology = this.iBase;
        return (chronology == null || (this.f17848 & 1) != 1) ? super.getDateTimeMillis(j, i, i2, i3, i4) : chronology.getDateTimeMillis(j, i, i2, i3, i4);
    }

    public DateTimeZone getZone() {
        Chronology chronology = this.iBase;
        if (chronology != null) {
            return chronology.getZone();
        }
        return null;
    }

    public final DateTimeField halfdayOfDay() {
        return this.f17861;
    }

    public final DurationField halfdays() {
        return this.f17872;
    }

    public final DateTimeField hourOfDay() {
        return this.f17858;
    }

    public final DateTimeField hourOfHalfday() {
        return this.f17855;
    }

    public final DurationField hours() {
        return this.f17874;
    }

    public final DurationField millis() {
        return this.f17876;
    }

    public final DateTimeField millisOfDay() {
        return this.f17852;
    }

    public final DateTimeField millisOfSecond() {
        return this.f17851;
    }

    public final DateTimeField minuteOfDay() {
        return this.f17857;
    }

    public final DateTimeField minuteOfHour() {
        return this.f17856;
    }

    public final DurationField minutes() {
        return this.f17875;
    }

    public final DateTimeField monthOfYear() {
        return this.f17877;
    }

    public final DurationField months() {
        return this.f17860;
    }

    public final DateTimeField secondOfDay() {
        return this.f17879;
    }

    public final DateTimeField secondOfMinute() {
        return this.f17878;
    }

    public final DurationField seconds() {
        return this.f17873;
    }

    public final DateTimeField weekOfWeekyear() {
        return this.f17869;
    }

    public final DurationField weeks() {
        return this.f17847;
    }

    public final DateTimeField weekyear() {
        return this.f17870;
    }

    public final DateTimeField weekyearOfCentury() {
        return this.f17871;
    }

    public final DurationField weekyears() {
        return this.f17849;
    }

    public final DateTimeField year() {
        return this.f17880;
    }

    public final DateTimeField yearOfCentury() {
        return this.f17867;
    }

    public final DateTimeField yearOfEra() {
        return this.f17865;
    }

    public final DurationField years() {
        return this.f17863;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public final Object m22773() {
        return this.iParam;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public final Chronology m22774() {
        return this.iBase;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m22775(Fields fields);
}
