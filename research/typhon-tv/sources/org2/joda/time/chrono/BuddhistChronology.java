package org2.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTime;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeZone;
import org2.joda.time.DurationFieldType;
import org2.joda.time.ReadableDateTime;
import org2.joda.time.ReadableInstant;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.DelegatedDateTimeField;
import org2.joda.time.field.DividedDateTimeField;
import org2.joda.time.field.OffsetDateTimeField;
import org2.joda.time.field.RemainderDateTimeField;
import org2.joda.time.field.SkipUndoDateTimeField;
import org2.joda.time.field.UnsupportedDurationField;

public final class BuddhistChronology extends AssembledChronology {
    public static final int BE = 1;
    private static final long serialVersionUID = -3474595157769370126L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, BuddhistChronology> f17950 = new ConcurrentHashMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final BuddhistChronology f17951 = getInstance(DateTimeZone.UTC);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField f17952 = new BasicSingleEraDateTimeField("BE");

    private BuddhistChronology(Chronology chronology, Object obj) {
        super(chronology, obj);
    }

    public static BuddhistChronology getInstance() {
        return getInstance(DateTimeZone.getDefault());
    }

    public static BuddhistChronology getInstance(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        BuddhistChronology buddhistChronology = f17950.get(dateTimeZone);
        if (buddhistChronology != null) {
            return buddhistChronology;
        }
        BuddhistChronology buddhistChronology2 = new BuddhistChronology(GJChronology.getInstance(dateTimeZone, (ReadableInstant) null), (Object) null);
        BuddhistChronology buddhistChronology3 = new BuddhistChronology(LimitChronology.getInstance(buddhistChronology2, new DateTime(1, 1, 1, 0, 0, 0, 0, buddhistChronology2), (ReadableDateTime) null), "");
        BuddhistChronology putIfAbsent = f17950.putIfAbsent(dateTimeZone, buddhistChronology3);
        return putIfAbsent != null ? putIfAbsent : buddhistChronology3;
    }

    public static BuddhistChronology getInstanceUTC() {
        return f17951;
    }

    private Object readResolve() {
        Chronology r0 = m22774();
        return r0 == null ? getInstanceUTC() : getInstance(r0.getZone());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BuddhistChronology) {
            return getZone().equals(((BuddhistChronology) obj).getZone());
        }
        return false;
    }

    public int hashCode() {
        return ("Buddhist".hashCode() * 11) + getZone().hashCode();
    }

    public String toString() {
        DateTimeZone zone = getZone();
        return zone != null ? "BuddhistChronology" + '[' + zone.getID() + ']' : "BuddhistChronology";
    }

    public Chronology withUTC() {
        return f17951;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22849(AssembledChronology.Fields fields) {
        if (m22773() == null) {
            fields.f17889 = UnsupportedDurationField.getInstance(DurationFieldType.eras());
            fields.f17915 = new OffsetDateTimeField(new SkipUndoDateTimeField(this, fields.f17915), 543);
            DateTimeField dateTimeField = fields.f17900;
            fields.f17900 = new DelegatedDateTimeField(fields.f17915, fields.f17889, DateTimeFieldType.yearOfEra());
            fields.f17905 = new OffsetDateTimeField(new SkipUndoDateTimeField(this, fields.f17905), 543);
            fields.f17882 = new DividedDateTimeField(new OffsetDateTimeField(fields.f17900, 99), fields.f17889, DateTimeFieldType.centuryOfEra(), 100);
            fields.f17899 = fields.f17882.getDurationField();
            fields.f17902 = new OffsetDateTimeField(new RemainderDateTimeField((DividedDateTimeField) fields.f17882), DateTimeFieldType.yearOfCentury(), 1);
            fields.f17906 = new OffsetDateTimeField(new RemainderDateTimeField(fields.f17905, fields.f17899, DateTimeFieldType.weekyearOfCentury(), 100), DateTimeFieldType.weekyearOfCentury(), 1);
            fields.f17885 = f17952;
        }
    }
}
