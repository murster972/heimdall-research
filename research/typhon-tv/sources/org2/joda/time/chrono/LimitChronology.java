package org2.joda.time.chrono;

import java.util.HashMap;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTime;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.DurationField;
import org2.joda.time.MutableDateTime;
import org2.joda.time.ReadableDateTime;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.DecoratedDateTimeField;
import org2.joda.time.field.DecoratedDurationField;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.format.DateTimeFormatter;
import org2.joda.time.format.ISODateTimeFormat;

public final class LimitChronology extends AssembledChronology {
    private static final long serialVersionUID = 7670866536893052522L;
    final DateTime iLowerLimit;
    final DateTime iUpperLimit;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient LimitChronology f18003;

    private class LimitDateTimeField extends DecoratedDateTimeField {

        /* renamed from: 靐  reason: contains not printable characters */
        private final DurationField f18004;

        /* renamed from: 麤  reason: contains not printable characters */
        private final DurationField f18005;

        /* renamed from: 齉  reason: contains not printable characters */
        private final DurationField f18006;

        LimitDateTimeField(DateTimeField dateTimeField, DurationField durationField, DurationField durationField2, DurationField durationField3) {
            super(dateTimeField, dateTimeField.getType());
            this.f18004 = durationField;
            this.f18006 = durationField2;
            this.f18005 = durationField3;
        }

        public long add(long j, int i) {
            LimitChronology.this.m22943(j, (String) null);
            long add = m23024().add(j, i);
            LimitChronology.this.m22943(add, "resulting");
            return add;
        }

        public long add(long j, long j2) {
            LimitChronology.this.m22943(j, (String) null);
            long add = m23024().add(j, j2);
            LimitChronology.this.m22943(add, "resulting");
            return add;
        }

        public long addWrapField(long j, int i) {
            LimitChronology.this.m22943(j, (String) null);
            long addWrapField = m23024().addWrapField(j, i);
            LimitChronology.this.m22943(addWrapField, "resulting");
            return addWrapField;
        }

        public int get(long j) {
            LimitChronology.this.m22943(j, (String) null);
            return m23024().get(j);
        }

        public String getAsShortText(long j, Locale locale) {
            LimitChronology.this.m22943(j, (String) null);
            return m23024().getAsShortText(j, locale);
        }

        public String getAsText(long j, Locale locale) {
            LimitChronology.this.m22943(j, (String) null);
            return m23024().getAsText(j, locale);
        }

        public int getDifference(long j, long j2) {
            LimitChronology.this.m22943(j, "minuend");
            LimitChronology.this.m22943(j2, "subtrahend");
            return m23024().getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            LimitChronology.this.m22943(j, "minuend");
            LimitChronology.this.m22943(j2, "subtrahend");
            return m23024().getDifferenceAsLong(j, j2);
        }

        public final DurationField getDurationField() {
            return this.f18004;
        }

        public int getLeapAmount(long j) {
            LimitChronology.this.m22943(j, (String) null);
            return m23024().getLeapAmount(j);
        }

        public final DurationField getLeapDurationField() {
            return this.f18005;
        }

        public int getMaximumShortTextLength(Locale locale) {
            return m23024().getMaximumShortTextLength(locale);
        }

        public int getMaximumTextLength(Locale locale) {
            return m23024().getMaximumTextLength(locale);
        }

        public int getMaximumValue(long j) {
            LimitChronology.this.m22943(j, (String) null);
            return m23024().getMaximumValue(j);
        }

        public int getMinimumValue(long j) {
            LimitChronology.this.m22943(j, (String) null);
            return m23024().getMinimumValue(j);
        }

        public final DurationField getRangeDurationField() {
            return this.f18006;
        }

        public boolean isLeap(long j) {
            LimitChronology.this.m22943(j, (String) null);
            return m23024().isLeap(j);
        }

        public long remainder(long j) {
            LimitChronology.this.m22943(j, (String) null);
            long remainder = m23024().remainder(j);
            LimitChronology.this.m22943(remainder, "resulting");
            return remainder;
        }

        public long roundCeiling(long j) {
            LimitChronology.this.m22943(j, (String) null);
            long roundCeiling = m23024().roundCeiling(j);
            LimitChronology.this.m22943(roundCeiling, "resulting");
            return roundCeiling;
        }

        public long roundFloor(long j) {
            LimitChronology.this.m22943(j, (String) null);
            long roundFloor = m23024().roundFloor(j);
            LimitChronology.this.m22943(roundFloor, "resulting");
            return roundFloor;
        }

        public long roundHalfCeiling(long j) {
            LimitChronology.this.m22943(j, (String) null);
            long roundHalfCeiling = m23024().roundHalfCeiling(j);
            LimitChronology.this.m22943(roundHalfCeiling, "resulting");
            return roundHalfCeiling;
        }

        public long roundHalfEven(long j) {
            LimitChronology.this.m22943(j, (String) null);
            long roundHalfEven = m23024().roundHalfEven(j);
            LimitChronology.this.m22943(roundHalfEven, "resulting");
            return roundHalfEven;
        }

        public long roundHalfFloor(long j) {
            LimitChronology.this.m22943(j, (String) null);
            long roundHalfFloor = m23024().roundHalfFloor(j);
            LimitChronology.this.m22943(roundHalfFloor, "resulting");
            return roundHalfFloor;
        }

        public long set(long j, int i) {
            LimitChronology.this.m22943(j, (String) null);
            long j2 = m23024().set(j, i);
            LimitChronology.this.m22943(j2, "resulting");
            return j2;
        }

        public long set(long j, String str, Locale locale) {
            LimitChronology.this.m22943(j, (String) null);
            long j2 = m23024().set(j, str, locale);
            LimitChronology.this.m22943(j2, "resulting");
            return j2;
        }
    }

    private class LimitDurationField extends DecoratedDurationField {
        private static final long serialVersionUID = 8049297699408782284L;

        LimitDurationField(DurationField durationField) {
            super(durationField, durationField.getType());
        }

        public long add(long j, int i) {
            LimitChronology.this.m22943(j, (String) null);
            long add = getWrappedField().add(j, i);
            LimitChronology.this.m22943(add, "resulting");
            return add;
        }

        public long add(long j, long j2) {
            LimitChronology.this.m22943(j, (String) null);
            long add = getWrappedField().add(j, j2);
            LimitChronology.this.m22943(add, "resulting");
            return add;
        }

        public int getDifference(long j, long j2) {
            LimitChronology.this.m22943(j, "minuend");
            LimitChronology.this.m22943(j2, "subtrahend");
            return getWrappedField().getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            LimitChronology.this.m22943(j, "minuend");
            LimitChronology.this.m22943(j2, "subtrahend");
            return getWrappedField().getDifferenceAsLong(j, j2);
        }

        public long getMillis(int i, long j) {
            LimitChronology.this.m22943(j, (String) null);
            return getWrappedField().getMillis(i, j);
        }

        public long getMillis(long j, long j2) {
            LimitChronology.this.m22943(j2, (String) null);
            return getWrappedField().getMillis(j, j2);
        }

        public int getValue(long j, long j2) {
            LimitChronology.this.m22943(j2, (String) null);
            return getWrappedField().getValue(j, j2);
        }

        public long getValueAsLong(long j, long j2) {
            LimitChronology.this.m22943(j2, (String) null);
            return getWrappedField().getValueAsLong(j, j2);
        }
    }

    private class LimitException extends IllegalArgumentException {
        private static final long serialVersionUID = -5924689995607498581L;
        private final boolean iIsLow;

        LimitException(String str, boolean z) {
            super(str);
            this.iIsLow = z;
        }

        public String getMessage() {
            StringBuffer stringBuffer = new StringBuffer(85);
            stringBuffer.append("The");
            String message = super.getMessage();
            if (message != null) {
                stringBuffer.append(' ');
                stringBuffer.append(message);
            }
            stringBuffer.append(" instant is ");
            DateTimeFormatter r1 = ISODateTimeFormat.m23265().m23069(LimitChronology.this.m22774());
            if (this.iIsLow) {
                stringBuffer.append("below the supported minimum of ");
                r1.m23075(stringBuffer, LimitChronology.this.getLowerLimit().getMillis());
            } else {
                stringBuffer.append("above the supported maximum of ");
                r1.m23075(stringBuffer, LimitChronology.this.getUpperLimit().getMillis());
            }
            stringBuffer.append(" (");
            stringBuffer.append(LimitChronology.this.m22774());
            stringBuffer.append(')');
            return stringBuffer.toString();
        }

        public String toString() {
            return "IllegalArgumentException: " + getMessage();
        }
    }

    private LimitChronology(Chronology chronology, DateTime dateTime, DateTime dateTime2) {
        super(chronology, (Object) null);
        this.iLowerLimit = dateTime;
        this.iUpperLimit = dateTime2;
    }

    public static LimitChronology getInstance(Chronology chronology, ReadableDateTime readableDateTime, ReadableDateTime readableDateTime2) {
        DateTime dateTime = null;
        if (chronology == null) {
            throw new IllegalArgumentException("Must supply a chronology");
        }
        ReadableDateTime dateTime2 = readableDateTime == null ? null : readableDateTime.toDateTime();
        if (readableDateTime2 != null) {
            dateTime = readableDateTime2.toDateTime();
        }
        if (dateTime2 == null || dateTime == null || dateTime2.isBefore(dateTime)) {
            return new LimitChronology(chronology, (DateTime) dateTime2, dateTime);
        }
        throw new IllegalArgumentException("The lower limit must be come before than the upper limit");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeField m22941(DateTimeField dateTimeField, HashMap<Object, Object> hashMap) {
        if (dateTimeField == null || !dateTimeField.isSupported()) {
            return dateTimeField;
        }
        if (hashMap.containsKey(dateTimeField)) {
            return (DateTimeField) hashMap.get(dateTimeField);
        }
        LimitDateTimeField limitDateTimeField = new LimitDateTimeField(dateTimeField, m22942(dateTimeField.getDurationField(), hashMap), m22942(dateTimeField.getRangeDurationField(), hashMap), m22942(dateTimeField.getLeapDurationField(), hashMap));
        hashMap.put(dateTimeField, limitDateTimeField);
        return limitDateTimeField;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DurationField m22942(DurationField durationField, HashMap<Object, Object> hashMap) {
        if (durationField == null || !durationField.isSupported()) {
            return durationField;
        }
        if (hashMap.containsKey(durationField)) {
            return (DurationField) hashMap.get(durationField);
        }
        LimitDurationField limitDurationField = new LimitDurationField(durationField);
        hashMap.put(durationField, limitDurationField);
        return limitDurationField;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LimitChronology)) {
            return false;
        }
        LimitChronology limitChronology = (LimitChronology) obj;
        return m22774().equals(limitChronology.m22774()) && FieldUtils.m23040((Object) getLowerLimit(), (Object) limitChronology.getLowerLimit()) && FieldUtils.m23040((Object) getUpperLimit(), (Object) limitChronology.getUpperLimit());
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        long dateTimeMillis = m22774().getDateTimeMillis(i, i2, i3, i4);
        m22943(dateTimeMillis, "resulting");
        return dateTimeMillis;
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        long dateTimeMillis = m22774().getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        m22943(dateTimeMillis, "resulting");
        return dateTimeMillis;
    }

    public long getDateTimeMillis(long j, int i, int i2, int i3, int i4) throws IllegalArgumentException {
        m22943(j, (String) null);
        long dateTimeMillis = m22774().getDateTimeMillis(j, i, i2, i3, i4);
        m22943(dateTimeMillis, "resulting");
        return dateTimeMillis;
    }

    public DateTime getLowerLimit() {
        return this.iLowerLimit;
    }

    public DateTime getUpperLimit() {
        return this.iUpperLimit;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (getLowerLimit() != null ? getLowerLimit().hashCode() : 0) + 317351877;
        if (getUpperLimit() != null) {
            i = getUpperLimit().hashCode();
        }
        return hashCode + i + (m22774().hashCode() * 7);
    }

    public String toString() {
        return "LimitChronology[" + m22774().toString() + ", " + (getLowerLimit() == null ? "NoLimit" : getLowerLimit().toString()) + ", " + (getUpperLimit() == null ? "NoLimit" : getUpperLimit().toString()) + ']';
    }

    public Chronology withUTC() {
        return withZone(DateTimeZone.UTC);
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        if (dateTimeZone == getZone()) {
            return this;
        }
        if (dateTimeZone == DateTimeZone.UTC && this.f18003 != null) {
            return this.f18003;
        }
        DateTime dateTime = this.iLowerLimit;
        if (dateTime != null) {
            MutableDateTime mutableDateTime = dateTime.toMutableDateTime();
            mutableDateTime.setZoneRetainFields(dateTimeZone);
            dateTime = mutableDateTime.toDateTime();
        }
        DateTime dateTime2 = this.iUpperLimit;
        if (dateTime2 != null) {
            MutableDateTime mutableDateTime2 = dateTime2.toMutableDateTime();
            mutableDateTime2.setZoneRetainFields(dateTimeZone);
            dateTime2 = mutableDateTime2.toDateTime();
        }
        LimitChronology instance = getInstance(m22774().withZone(dateTimeZone), dateTime, dateTime2);
        if (dateTimeZone == DateTimeZone.UTC) {
            this.f18003 = instance;
        }
        return instance;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22943(long j, String str) {
        DateTime dateTime = this.iLowerLimit;
        if (dateTime == null || j >= dateTime.getMillis()) {
            DateTime dateTime2 = this.iUpperLimit;
            if (dateTime2 != null && j >= dateTime2.getMillis()) {
                throw new LimitException(str, false);
            }
            return;
        }
        throw new LimitException(str, true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22944(AssembledChronology.Fields fields) {
        HashMap hashMap = new HashMap();
        fields.f17889 = m22942(fields.f17889, (HashMap<Object, Object>) hashMap);
        fields.f17899 = m22942(fields.f17899, (HashMap<Object, Object>) hashMap);
        fields.f17898 = m22942(fields.f17898, (HashMap<Object, Object>) hashMap);
        fields.f17895 = m22942(fields.f17895, (HashMap<Object, Object>) hashMap);
        fields.f17884 = m22942(fields.f17884, (HashMap<Object, Object>) hashMap);
        fields.f17883 = m22942(fields.f17883, (HashMap<Object, Object>) hashMap);
        fields.f17881 = m22942(fields.f17881, (HashMap<Object, Object>) hashMap);
        fields.f17907 = m22942(fields.f17907, (HashMap<Object, Object>) hashMap);
        fields.f17909 = m22942(fields.f17909, (HashMap<Object, Object>) hashMap);
        fields.f17910 = m22942(fields.f17910, (HashMap<Object, Object>) hashMap);
        fields.f17908 = m22942(fields.f17908, (HashMap<Object, Object>) hashMap);
        fields.f17911 = m22942(fields.f17911, (HashMap<Object, Object>) hashMap);
        fields.f17915 = m22941(fields.f17915, (HashMap<Object, Object>) hashMap);
        fields.f17900 = m22941(fields.f17900, (HashMap<Object, Object>) hashMap);
        fields.f17902 = m22941(fields.f17902, (HashMap<Object, Object>) hashMap);
        fields.f17882 = m22941(fields.f17882, (HashMap<Object, Object>) hashMap);
        fields.f17885 = m22941(fields.f17885, (HashMap<Object, Object>) hashMap);
        fields.f17897 = m22941(fields.f17897, (HashMap<Object, Object>) hashMap);
        fields.f17901 = m22941(fields.f17901, (HashMap<Object, Object>) hashMap);
        fields.f17903 = m22941(fields.f17903, (HashMap<Object, Object>) hashMap);
        fields.f17912 = m22941(fields.f17912, (HashMap<Object, Object>) hashMap);
        fields.f17904 = m22941(fields.f17904, (HashMap<Object, Object>) hashMap);
        fields.f17905 = m22941(fields.f17905, (HashMap<Object, Object>) hashMap);
        fields.f17906 = m22941(fields.f17906, (HashMap<Object, Object>) hashMap);
        fields.f17886 = m22941(fields.f17886, (HashMap<Object, Object>) hashMap);
        fields.f17887 = m22941(fields.f17887, (HashMap<Object, Object>) hashMap);
        fields.f17913 = m22941(fields.f17913, (HashMap<Object, Object>) hashMap);
        fields.f17914 = m22941(fields.f17914, (HashMap<Object, Object>) hashMap);
        fields.f17891 = m22941(fields.f17891, (HashMap<Object, Object>) hashMap);
        fields.f17892 = m22941(fields.f17892, (HashMap<Object, Object>) hashMap);
        fields.f17893 = m22941(fields.f17893, (HashMap<Object, Object>) hashMap);
        fields.f17890 = m22941(fields.f17890, (HashMap<Object, Object>) hashMap);
        fields.f17888 = m22941(fields.f17888, (HashMap<Object, Object>) hashMap);
        fields.f17894 = m22941(fields.f17894, (HashMap<Object, Object>) hashMap);
        fields.f17896 = m22941(fields.f17896, (HashMap<Object, Object>) hashMap);
    }
}
