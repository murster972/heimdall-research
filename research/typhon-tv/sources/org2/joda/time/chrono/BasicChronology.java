package org2.joda.time.chrono;

import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeZone;
import org2.joda.time.DurationField;
import org2.joda.time.DurationFieldType;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.DividedDateTimeField;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.field.MillisDurationField;
import org2.joda.time.field.OffsetDateTimeField;
import org2.joda.time.field.PreciseDateTimeField;
import org2.joda.time.field.PreciseDurationField;
import org2.joda.time.field.RemainderDateTimeField;
import org2.joda.time.field.ZeroIsMaxDateTimeField;

abstract class BasicChronology extends AssembledChronology {
    private static final long serialVersionUID = 8283225332206808863L;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final DurationField f17916 = new PreciseDurationField(DurationFieldType.days(), 86400000);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final DurationField f17917 = new PreciseDurationField(DurationFieldType.weeks(), 604800000);

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final DateTimeField f17918 = new PreciseDateTimeField(DateTimeFieldType.millisOfSecond(), f17931, f17928);

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final DateTimeField f17919 = new PreciseDateTimeField(DateTimeFieldType.minuteOfDay(), f17930, f17916);

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final DateTimeField f17920 = new PreciseDateTimeField(DateTimeFieldType.hourOfDay(), f17929, f17916);

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final DateTimeField f17921 = new PreciseDateTimeField(DateTimeFieldType.minuteOfHour(), f17930, f17929);

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final DateTimeField f17922 = new ZeroIsMaxDateTimeField(f17932, DateTimeFieldType.clockhourOfHalfday());

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final DateTimeField f17923 = new HalfdayField();

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final DateTimeField f17924 = new PreciseDateTimeField(DateTimeFieldType.millisOfDay(), f17931, f17916);

    /* renamed from: ٴ  reason: contains not printable characters */
    private static final DateTimeField f17925 = new PreciseDateTimeField(DateTimeFieldType.secondOfMinute(), f17928, f17930);

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final DateTimeField f17926 = new PreciseDateTimeField(DateTimeFieldType.secondOfDay(), f17928, f17916);
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final DurationField f17927 = new PreciseDurationField(DurationFieldType.halfdays(), 43200000);

    /* renamed from: 靐  reason: contains not printable characters */
    private static final DurationField f17928 = new PreciseDurationField(DurationFieldType.seconds(), 1000);

    /* renamed from: 麤  reason: contains not printable characters */
    private static final DurationField f17929 = new PreciseDurationField(DurationFieldType.hours(), 3600000);

    /* renamed from: 齉  reason: contains not printable characters */
    private static final DurationField f17930 = new PreciseDurationField(DurationFieldType.minutes(), 60000);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DurationField f17931 = MillisDurationField.INSTANCE;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final DateTimeField f17932 = new PreciseDateTimeField(DateTimeFieldType.hourOfHalfday(), f17929, f17927);

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final DateTimeField f17933 = new ZeroIsMaxDateTimeField(f17920, DateTimeFieldType.clockhourOfDay());
    private final int iMinDaysInFirstWeek;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final transient YearInfo[] f17934 = new YearInfo[1024];

    private static class HalfdayField extends PreciseDateTimeField {
        HalfdayField() {
            super(DateTimeFieldType.halfdayOfDay(), BasicChronology.f17927, BasicChronology.f17916);
        }

        public String getAsText(int i, Locale locale) {
            return GJLocaleSymbols.m22876(locale).m22881(i);
        }

        public int getMaximumTextLength(Locale locale) {
            return GJLocaleSymbols.m22876(locale).m22880();
        }

        public long set(long j, String str, Locale locale) {
            return set(j, GJLocaleSymbols.m22876(locale).m22888(str));
        }
    }

    private static class YearInfo {

        /* renamed from: 靐  reason: contains not printable characters */
        public final long f17935;

        /* renamed from: 龘  reason: contains not printable characters */
        public final int f17936;

        YearInfo(int i, long j) {
            this.f17936 = i;
            this.f17935 = j;
        }
    }

    BasicChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj);
        if (i < 1 || i > 7) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
        this.iMinDaysInFirstWeek = i;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private YearInfo m22781(int i) {
        YearInfo yearInfo = this.f17934[i & 1023];
        if (yearInfo != null && yearInfo.f17936 == i) {
            return yearInfo;
        }
        YearInfo yearInfo2 = new YearInfo(i, m22789(i));
        this.f17934[i & 1023] = yearInfo2;
        return yearInfo2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m22782(int i, int i2, int i3, int i4) {
        long j;
        long r2 = m22806(i, i2, i3);
        if (r2 == Long.MIN_VALUE) {
            i4 -= 86400000;
            j = m22806(i, i2, i3 + 1);
        } else {
            j = r2;
        }
        long j2 = ((long) i4) + j;
        if (j2 < 0 && j > 0) {
            return Long.MAX_VALUE;
        }
        if (j2 <= 0 || j >= 0) {
            return j2;
        }
        return Long.MIN_VALUE;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BasicChronology basicChronology = (BasicChronology) obj;
        return getMinimumDaysInFirstWeek() == basicChronology.getMinimumDaysInFirstWeek() && getZone().equals(basicChronology.getZone());
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology r0 = m22774();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4);
        }
        FieldUtils.m23039(DateTimeFieldType.millisOfDay(), i4, 0, 86399999);
        return m22782(i, i2, i3, i4);
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        Chronology r0 = m22774();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        }
        FieldUtils.m23039(DateTimeFieldType.hourOfDay(), i4, 0, 23);
        FieldUtils.m23039(DateTimeFieldType.minuteOfHour(), i5, 0, 59);
        FieldUtils.m23039(DateTimeFieldType.secondOfMinute(), i6, 0, 59);
        FieldUtils.m23039(DateTimeFieldType.millisOfSecond(), i7, 0, 999);
        return m22782(i, i2, i3, (int) ((long) ((3600000 * i4) + (60000 * i5) + (i6 * 1000) + i7)));
    }

    public int getMinimumDaysInFirstWeek() {
        return this.iMinDaysInFirstWeek;
    }

    public DateTimeZone getZone() {
        Chronology r0 = m22774();
        return r0 != null ? r0.getZone() : DateTimeZone.UTC;
    }

    public int hashCode() {
        return (getClass().getName().hashCode() * 11) + getZone().hashCode() + getMinimumDaysInFirstWeek();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(60);
        String name = getClass().getName();
        int lastIndexOf = name.lastIndexOf(46);
        if (lastIndexOf >= 0) {
            name = name.substring(lastIndexOf + 1);
        }
        sb.append(name);
        sb.append('[');
        DateTimeZone zone = getZone();
        if (zone != null) {
            sb.append(zone.getID());
        }
        if (getMinimumDaysInFirstWeek() != 4) {
            sb.append(",mdfw=");
            sb.append(getMinimumDaysInFirstWeek());
        }
        sb.append(']');
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m22783();

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract int m22784(int i);

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22785(long j) {
        return m22809(j, m22817(j));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract long m22786(long j, int i);

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m22787() {
        return 12;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m22788(long j) {
        long j2;
        if (j >= 0) {
            j2 = j / 86400000;
        } else {
            j2 = (j - 86399999) / 86400000;
            if (j2 < -3) {
                return ((int) ((j2 + 4) % 7)) + 7;
            }
        }
        return ((int) ((j2 + 3) % 7)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public abstract long m22789(int i);

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m22790(int i) {
        return m22787();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public int m22791(long j) {
        return j >= 0 ? (int) (j % 86400000) : 86399999 + ((int) ((1 + j) % 86400000));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public abstract long m22792();

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public int m22793(long j) {
        int r0 = m22817(j);
        return m22803(r0, m22818(j, r0));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public abstract long m22794();

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public abstract long m22795();

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m22796(long j) {
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public abstract long m22797();

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public abstract int m22798();

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22799(long j) {
        int r0 = m22817(j);
        int r1 = m22809(j, r0);
        return r1 == 1 ? m22817(604800000 + j) : r1 > 51 ? m22817(j - 1209600000) : r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22800(long j, int i) {
        return m22793(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public abstract boolean m22801(int i);

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m22802(int i) {
        return (int) ((m22814(i + 1) - m22814(i)) / 604800000);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract int m22803(int i, int i2);

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m22804(long j) {
        return m22818(j, m22817(j));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m22805(long j, int i) {
        return m22819(j, i, m22818(j, i));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public long m22806(int i, int i2, int i3) {
        FieldUtils.m23039(DateTimeFieldType.year(), i, m22798() - 1, m22783() + 1);
        FieldUtils.m23039(DateTimeFieldType.monthOfYear(), i2, 1, m22790(i));
        FieldUtils.m23039(DateTimeFieldType.dayOfMonth(), i3, 1, m22803(i, i2));
        long r0 = m22821(i, i2, i3);
        if (r0 < 0 && i == m22783() + 1) {
            return Long.MAX_VALUE;
        }
        if (r0 <= 0 || i != m22798() - 1) {
            return r0;
        }
        return Long.MIN_VALUE;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m22807() {
        return 31;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m22808(long j) {
        return m22813(j, m22817(j));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m22809(long j, int i) {
        long r0 = m22814(i);
        if (j < r0) {
            return m22802(i - 1);
        }
        if (j >= m22814(i + 1)) {
            return 1;
        }
        return ((int) ((j - r0) / 604800000)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public long m22810(int i) {
        return m22781(i).f17935;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m22811() {
        return 366;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m22812(long j) {
        int r0 = m22817(j);
        return m22819(j, r0, m22818(j, r0));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m22813(long j, int i) {
        return ((int) ((j - m22810(i)) / 86400000)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m22814(int i) {
        long r0 = m22810(i);
        int r2 = m22788(r0);
        return r2 > 8 - this.iMinDaysInFirstWeek ? r0 + (((long) (8 - r2)) * 86400000) : r0 - (((long) (r2 - 1)) * 86400000);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public abstract long m22815(int i, int i2);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22816(int i) {
        return m22801(i) ? 366 : 365;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22817(long j) {
        long j2 = 31536000000L;
        long r4 = m22794();
        long r0 = (j >> 1) + m22797();
        if (r0 < 0) {
            r0 = (r0 - r4) + 1;
        }
        int i = (int) (r0 / r4);
        long r42 = m22810(i);
        long j3 = j - r42;
        if (j3 < 0) {
            return i - 1;
        }
        if (j3 < 31536000000L) {
            return i;
        }
        if (m22801(i)) {
            j2 = 31622400000L;
        }
        return j2 + r42 <= j ? i + 1 : i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract int m22818(long j, int i);

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22819(long j, int i, int i2) {
        return ((int) ((j - (m22810(i) + m22815(i, i2))) / 86400000)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22820(int i, int i2) {
        return m22810(i) + m22815(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22821(int i, int i2, int i3) {
        return m22810(i) + m22815(i, i2) + (((long) (i3 - 1)) * 86400000);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract long m22822(long j, long j2);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22823(AssembledChronology.Fields fields) {
        fields.f17911 = f17931;
        fields.f17908 = f17928;
        fields.f17910 = f17930;
        fields.f17909 = f17929;
        fields.f17907 = f17927;
        fields.f17881 = f17916;
        fields.f17883 = f17917;
        fields.f17886 = f17918;
        fields.f17887 = f17924;
        fields.f17913 = f17925;
        fields.f17914 = f17926;
        fields.f17891 = f17921;
        fields.f17892 = f17919;
        fields.f17893 = f17920;
        fields.f17890 = f17932;
        fields.f17888 = f17933;
        fields.f17894 = f17922;
        fields.f17896 = f17923;
        fields.f17915 = new BasicYearDateTimeField(this);
        fields.f17900 = new GJYearOfEraDateTimeField(fields.f17915, this);
        fields.f17882 = new DividedDateTimeField(new OffsetDateTimeField(fields.f17900, 99), DateTimeFieldType.centuryOfEra(), 100);
        fields.f17899 = fields.f17882.getDurationField();
        fields.f17902 = new OffsetDateTimeField(new RemainderDateTimeField((DividedDateTimeField) fields.f17882), DateTimeFieldType.yearOfCentury(), 1);
        fields.f17885 = new GJEraDateTimeField(this);
        fields.f17897 = new GJDayOfWeekDateTimeField(this, fields.f17881);
        fields.f17901 = new BasicDayOfMonthDateTimeField(this, fields.f17881);
        fields.f17903 = new BasicDayOfYearDateTimeField(this, fields.f17881);
        fields.f17912 = new GJMonthOfYearDateTimeField(this);
        fields.f17905 = new BasicWeekyearDateTimeField(this);
        fields.f17904 = new BasicWeekOfWeekyearDateTimeField(this, fields.f17883);
        fields.f17906 = new OffsetDateTimeField(new RemainderDateTimeField(fields.f17905, fields.f17899, DateTimeFieldType.weekyearOfCentury(), 100), DateTimeFieldType.weekyearOfCentury(), 1);
        fields.f17898 = fields.f17915.getDurationField();
        fields.f17895 = fields.f17912.getDurationField();
        fields.f17884 = fields.f17905.getDurationField();
    }
}
