package org2.joda.time.chrono;

import java.util.Locale;

final class GJMonthOfYearDateTimeField extends BasicMonthOfYearDateTimeField {
    GJMonthOfYearDateTimeField(BasicChronology basicChronology) {
        super(basicChronology, 2);
    }

    public String getAsShortText(int i, Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22892(i);
    }

    public String getAsText(int i, Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22886(i);
    }

    public int getMaximumShortTextLength(Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22890();
    }

    public int getMaximumTextLength(Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22884();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22896(String str, Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22885(str);
    }
}
