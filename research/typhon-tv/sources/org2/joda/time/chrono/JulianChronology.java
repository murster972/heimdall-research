package org2.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeZone;
import org2.joda.time.IllegalFieldValueException;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.SkipDateTimeField;

public final class JulianChronology extends BasicGJChronology {
    private static final long serialVersionUID = -8731039522547897247L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, JulianChronology[]> f18000 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final JulianChronology f18001 = getInstance(DateTimeZone.UTC);

    JulianChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    public static JulianChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), 4);
    }

    public static JulianChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, 4);
    }

    public static JulianChronology getInstance(DateTimeZone dateTimeZone, int i) {
        JulianChronology[] julianChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        JulianChronology[] julianChronologyArr2 = f18000.get(dateTimeZone);
        if (julianChronologyArr2 == null) {
            julianChronologyArr = new JulianChronology[7];
            JulianChronology[] putIfAbsent = f18000.putIfAbsent(dateTimeZone, julianChronologyArr);
            if (putIfAbsent != null) {
                julianChronologyArr = putIfAbsent;
            }
        } else {
            julianChronologyArr = julianChronologyArr2;
        }
        try {
            JulianChronology julianChronology = julianChronologyArr[i - 1];
            if (julianChronology == null) {
                synchronized (julianChronologyArr) {
                    julianChronology = julianChronologyArr[i - 1];
                    if (julianChronology == null) {
                        julianChronology = dateTimeZone == DateTimeZone.UTC ? new JulianChronology((Chronology) null, (Object) null, i) : new JulianChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, i), dateTimeZone), (Object) null, i);
                        julianChronologyArr[i - 1] = julianChronology;
                    }
                }
            }
            return julianChronology;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
    }

    public static JulianChronology getInstanceUTC() {
        return f18001;
    }

    private Object readResolve() {
        Chronology r1 = m22774();
        int minimumDaysInFirstWeek = getMinimumDaysInFirstWeek();
        if (minimumDaysInFirstWeek == 0) {
            minimumDaysInFirstWeek = 4;
        }
        return r1 == null ? getInstance(DateTimeZone.UTC, minimumDaysInFirstWeek) : getInstance(r1.getZone(), minimumDaysInFirstWeek);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    static int m22928(int i) {
        if (i > 0) {
            return i;
        }
        if (i != 0) {
            return i + 1;
        }
        throw new IllegalFieldValueException(DateTimeFieldType.year(), (Number) Integer.valueOf(i), (Number) null, (Number) null);
    }

    public Chronology withUTC() {
        return f18001;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22929() {
        return 292272992;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m22930(int i) {
        int i2;
        int i3 = i - 1968;
        if (i3 <= 0) {
            i2 = (i3 + 3) >> 2;
        } else {
            i2 = i3 >> 2;
            if (!m22936(i)) {
                i2++;
            }
        }
        return ((((long) i2) + (((long) i3) * 365)) * 86400000) - 62035200000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m22931() {
        return 31557600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m22932() {
        return 15778800000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m22933() {
        return 2629800000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m22934() {
        return 31083663600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22935() {
        return -292269054;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22936(int i) {
        return (i & 3) == 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public long m22937(int i, int i2, int i3) throws IllegalArgumentException {
        return super.m22806(m22928(i), i2, i3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22938(AssembledChronology.Fields fields) {
        if (m22774() == null) {
            super.m22823(fields);
            fields.f17915 = new SkipDateTimeField(this, fields.f17915);
            fields.f17905 = new SkipDateTimeField(this, fields.f17905);
        }
    }
}
