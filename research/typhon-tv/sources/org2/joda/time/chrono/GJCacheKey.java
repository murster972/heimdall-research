package org2.joda.time.chrono;

import org2.joda.time.DateTimeZone;
import org2.joda.time.Instant;

class GJCacheKey {

    /* renamed from: 靐  reason: contains not printable characters */
    private final Instant f17959;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f17960;

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeZone f17961;

    GJCacheKey(DateTimeZone dateTimeZone, Instant instant, int i) {
        this.f17961 = dateTimeZone;
        this.f17959 = instant;
        this.f17960 = i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof GJCacheKey)) {
            return false;
        }
        GJCacheKey gJCacheKey = (GJCacheKey) obj;
        if (this.f17959 == null) {
            if (gJCacheKey.f17959 != null) {
                return false;
            }
        } else if (!this.f17959.equals(gJCacheKey.f17959)) {
            return false;
        }
        if (this.f17960 != gJCacheKey.f17960) {
            return false;
        }
        return this.f17961 == null ? gJCacheKey.f17961 == null : this.f17961.equals(gJCacheKey.f17961);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((this.f17959 == null ? 0 : this.f17959.hashCode()) + 31) * 31) + this.f17960) * 31;
        if (this.f17961 != null) {
            i = this.f17961.hashCode();
        }
        return hashCode + i;
    }
}
