package org2.joda.time.chrono;

import org2.joda.time.Chronology;

abstract class BasicFixedMonthChronology extends BasicChronology {
    private static final long serialVersionUID = 261387371998L;

    BasicFixedMonthChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22826(int i) {
        return i != 13 ? 30 : 6;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public long m22827(long j, int i) {
        int r0 = m22813(j, m22817(j));
        int r1 = m22791(j);
        if (r0 > 365 && !m22832(i)) {
            r0--;
        }
        return ((long) r1) + m22821(i, 1, r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public int m22828() {
        return 13;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m22829() {
        return 31557600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m22830() {
        return 15778800000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m22831() {
        return 2592000000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22832(int i) {
        return (i & 3) == 3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m22833(int i, int i2) {
        if (i2 != 13) {
            return 30;
        }
        return m22832(i) ? 6 : 5;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m22834(long j) {
        return ((m22808(j) - 1) / 30) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m22835() {
        return 30;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m22836(long j) {
        return ((m22808(j) - 1) % 30) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m22837(int i, int i2) {
        return ((long) (i2 - 1)) * 2592000000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22838(long j, int i) {
        return ((int) ((j - m22810(i)) / 2592000000L)) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22839(long j, long j2) {
        int r0 = m22817(j);
        int r1 = m22817(j2);
        int i = r0 - r1;
        if (j - m22810(r0) < j2 - m22810(r1)) {
            i--;
        }
        return (long) i;
    }
}
