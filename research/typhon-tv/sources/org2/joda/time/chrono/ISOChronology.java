package org2.joda.time.chrono;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeZone;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.DividedDateTimeField;
import org2.joda.time.field.RemainderDateTimeField;

public final class ISOChronology extends AssembledChronology {
    private static final long serialVersionUID = -6212696554273812441L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, ISOChronology> f17993 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ISOChronology f17994 = new ISOChronology(GregorianChronology.getInstanceUTC());

    private static final class Stub implements Serializable {
        private static final long serialVersionUID = -6212696554273812441L;

        /* renamed from: 龘  reason: contains not printable characters */
        private transient DateTimeZone f17995;

        Stub(DateTimeZone dateTimeZone) {
            this.f17995 = dateTimeZone;
        }

        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            this.f17995 = (DateTimeZone) objectInputStream.readObject();
        }

        private Object readResolve() {
            return ISOChronology.getInstance(this.f17995);
        }

        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeObject(this.f17995);
        }
    }

    static {
        f17993.put(DateTimeZone.UTC, f17994);
    }

    private ISOChronology(Chronology chronology) {
        super(chronology, (Object) null);
    }

    public static ISOChronology getInstance() {
        return getInstance(DateTimeZone.getDefault());
    }

    public static ISOChronology getInstance(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        ISOChronology iSOChronology = f17993.get(dateTimeZone);
        if (iSOChronology != null) {
            return iSOChronology;
        }
        ISOChronology iSOChronology2 = new ISOChronology(ZonedChronology.getInstance(f17994, dateTimeZone));
        ISOChronology putIfAbsent = f17993.putIfAbsent(dateTimeZone, iSOChronology2);
        return putIfAbsent != null ? putIfAbsent : iSOChronology2;
    }

    public static ISOChronology getInstanceUTC() {
        return f17994;
    }

    private Object writeReplace() {
        return new Stub(getZone());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ISOChronology) {
            return getZone().equals(((ISOChronology) obj).getZone());
        }
        return false;
    }

    public int hashCode() {
        return ("ISO".hashCode() * 11) + getZone().hashCode();
    }

    public String toString() {
        DateTimeZone zone = getZone();
        return zone != null ? "ISOChronology" + '[' + zone.getID() + ']' : "ISOChronology";
    }

    public Chronology withUTC() {
        return f17994;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22906(AssembledChronology.Fields fields) {
        if (m22774().getZone() == DateTimeZone.UTC) {
            fields.f17882 = new DividedDateTimeField(ISOYearOfEraDateTimeField.f17996, DateTimeFieldType.centuryOfEra(), 100);
            fields.f17899 = fields.f17882.getDurationField();
            fields.f17902 = new RemainderDateTimeField((DividedDateTimeField) fields.f17882, DateTimeFieldType.yearOfCentury());
            fields.f17906 = new RemainderDateTimeField((DividedDateTimeField) fields.f17882, fields.f17884, DateTimeFieldType.weekyearOfCentury());
        }
    }
}
