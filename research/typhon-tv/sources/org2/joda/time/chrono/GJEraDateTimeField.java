package org2.joda.time.chrono;

import java.util.Locale;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.DurationFieldType;
import org2.joda.time.field.BaseDateTimeField;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.field.UnsupportedDurationField;

final class GJEraDateTimeField extends BaseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f17973;

    GJEraDateTimeField(BasicChronology basicChronology) {
        super(DateTimeFieldType.era());
        this.f17973 = basicChronology;
    }

    public int get(long j) {
        return this.f17973.m22817(j) <= 0 ? 0 : 1;
    }

    public String getAsText(int i, Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22895(i);
    }

    public DurationField getDurationField() {
        return UnsupportedDurationField.getInstance(DurationFieldType.eras());
    }

    public int getMaximumTextLength(Locale locale) {
        return GJLocaleSymbols.m22876(locale).m22893();
    }

    public int getMaximumValue() {
        return 1;
    }

    public int getMinimumValue() {
        return 0;
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public boolean isLenient() {
        return false;
    }

    public long roundCeiling(long j) {
        if (get(j) == 0) {
            return this.f17973.m22786(0, 1);
        }
        return Long.MAX_VALUE;
    }

    public long roundFloor(long j) {
        if (get(j) == 1) {
            return this.f17973.m22786(0, 1);
        }
        return Long.MIN_VALUE;
    }

    public long roundHalfCeiling(long j) {
        return roundFloor(j);
    }

    public long roundHalfEven(long j) {
        return roundFloor(j);
    }

    public long roundHalfFloor(long j) {
        return roundFloor(j);
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, 0, 1);
        if (get(j) == i) {
            return j;
        }
        return this.f17973.m22786(j, -this.f17973.m22817(j));
    }

    public long set(long j, String str, Locale locale) {
        return set(j, GJLocaleSymbols.m22876(locale).m22894(str));
    }
}
