package org2.joda.time.chrono;

import java.util.HashMap;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.DurationField;
import org2.joda.time.IllegalFieldValueException;
import org2.joda.time.IllegalInstantException;
import org2.joda.time.ReadablePartial;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.BaseDateTimeField;
import org2.joda.time.field.BaseDurationField;

public final class ZonedChronology extends AssembledChronology {
    private static final long serialVersionUID = -1079258847191166848L;

    static final class ZonedDateTimeField extends BaseDateTimeField {

        /* renamed from: ʻ  reason: contains not printable characters */
        final DurationField f18009;

        /* renamed from: 连任  reason: contains not printable characters */
        final DurationField f18010;

        /* renamed from: 靐  reason: contains not printable characters */
        final DateTimeZone f18011;

        /* renamed from: 麤  reason: contains not printable characters */
        final boolean f18012;

        /* renamed from: 齉  reason: contains not printable characters */
        final DurationField f18013;

        /* renamed from: 龘  reason: contains not printable characters */
        final DateTimeField f18014;

        ZonedDateTimeField(DateTimeField dateTimeField, DateTimeZone dateTimeZone, DurationField durationField, DurationField durationField2, DurationField durationField3) {
            super(dateTimeField.getType());
            if (!dateTimeField.isSupported()) {
                throw new IllegalArgumentException();
            }
            this.f18014 = dateTimeField;
            this.f18011 = dateTimeZone;
            this.f18013 = durationField;
            this.f18012 = ZonedChronology.m22950(durationField);
            this.f18010 = durationField2;
            this.f18009 = durationField3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m22952(long j) {
            int offset = this.f18011.getOffset(j);
            if (((((long) offset) + j) ^ j) >= 0 || (((long) offset) ^ j) < 0) {
                return offset;
            }
            throw new ArithmeticException("Adding time zone offset caused overflow");
        }

        public long add(long j, int i) {
            if (this.f18012) {
                int r0 = m22952(j);
                return this.f18014.add(((long) r0) + j, i) - ((long) r0);
            }
            return this.f18011.convertLocalToUTC(this.f18014.add(this.f18011.convertUTCToLocal(j), i), false, j);
        }

        public long add(long j, long j2) {
            if (this.f18012) {
                int r0 = m22952(j);
                return this.f18014.add(((long) r0) + j, j2) - ((long) r0);
            }
            return this.f18011.convertLocalToUTC(this.f18014.add(this.f18011.convertUTCToLocal(j), j2), false, j);
        }

        public long addWrapField(long j, int i) {
            if (this.f18012) {
                int r0 = m22952(j);
                return this.f18014.addWrapField(((long) r0) + j, i) - ((long) r0);
            }
            return this.f18011.convertLocalToUTC(this.f18014.addWrapField(this.f18011.convertUTCToLocal(j), i), false, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ZonedDateTimeField)) {
                return false;
            }
            ZonedDateTimeField zonedDateTimeField = (ZonedDateTimeField) obj;
            return this.f18014.equals(zonedDateTimeField.f18014) && this.f18011.equals(zonedDateTimeField.f18011) && this.f18013.equals(zonedDateTimeField.f18013) && this.f18010.equals(zonedDateTimeField.f18010);
        }

        public int get(long j) {
            return this.f18014.get(this.f18011.convertUTCToLocal(j));
        }

        public String getAsShortText(int i, Locale locale) {
            return this.f18014.getAsShortText(i, locale);
        }

        public String getAsShortText(long j, Locale locale) {
            return this.f18014.getAsShortText(this.f18011.convertUTCToLocal(j), locale);
        }

        public String getAsText(int i, Locale locale) {
            return this.f18014.getAsText(i, locale);
        }

        public String getAsText(long j, Locale locale) {
            return this.f18014.getAsText(this.f18011.convertUTCToLocal(j), locale);
        }

        public int getDifference(long j, long j2) {
            int r1 = m22952(j2);
            return this.f18014.getDifference(((long) (this.f18012 ? r1 : m22952(j))) + j, ((long) r1) + j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            int r1 = m22952(j2);
            return this.f18014.getDifferenceAsLong(((long) (this.f18012 ? r1 : m22952(j))) + j, ((long) r1) + j2);
        }

        public final DurationField getDurationField() {
            return this.f18013;
        }

        public int getLeapAmount(long j) {
            return this.f18014.getLeapAmount(this.f18011.convertUTCToLocal(j));
        }

        public final DurationField getLeapDurationField() {
            return this.f18009;
        }

        public int getMaximumShortTextLength(Locale locale) {
            return this.f18014.getMaximumShortTextLength(locale);
        }

        public int getMaximumTextLength(Locale locale) {
            return this.f18014.getMaximumTextLength(locale);
        }

        public int getMaximumValue() {
            return this.f18014.getMaximumValue();
        }

        public int getMaximumValue(long j) {
            return this.f18014.getMaximumValue(this.f18011.convertUTCToLocal(j));
        }

        public int getMaximumValue(ReadablePartial readablePartial) {
            return this.f18014.getMaximumValue(readablePartial);
        }

        public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
            return this.f18014.getMaximumValue(readablePartial, iArr);
        }

        public int getMinimumValue() {
            return this.f18014.getMinimumValue();
        }

        public int getMinimumValue(long j) {
            return this.f18014.getMinimumValue(this.f18011.convertUTCToLocal(j));
        }

        public int getMinimumValue(ReadablePartial readablePartial) {
            return this.f18014.getMinimumValue(readablePartial);
        }

        public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
            return this.f18014.getMinimumValue(readablePartial, iArr);
        }

        public final DurationField getRangeDurationField() {
            return this.f18010;
        }

        public int hashCode() {
            return this.f18014.hashCode() ^ this.f18011.hashCode();
        }

        public boolean isLeap(long j) {
            return this.f18014.isLeap(this.f18011.convertUTCToLocal(j));
        }

        public boolean isLenient() {
            return this.f18014.isLenient();
        }

        public long remainder(long j) {
            return this.f18014.remainder(this.f18011.convertUTCToLocal(j));
        }

        public long roundCeiling(long j) {
            if (this.f18012) {
                int r0 = m22952(j);
                return this.f18014.roundCeiling(((long) r0) + j) - ((long) r0);
            }
            return this.f18011.convertLocalToUTC(this.f18014.roundCeiling(this.f18011.convertUTCToLocal(j)), false, j);
        }

        public long roundFloor(long j) {
            if (this.f18012) {
                int r0 = m22952(j);
                return this.f18014.roundFloor(((long) r0) + j) - ((long) r0);
            }
            return this.f18011.convertLocalToUTC(this.f18014.roundFloor(this.f18011.convertUTCToLocal(j)), false, j);
        }

        public long set(long j, int i) {
            long j2 = this.f18014.set(this.f18011.convertUTCToLocal(j), i);
            long convertLocalToUTC = this.f18011.convertLocalToUTC(j2, false, j);
            if (get(convertLocalToUTC) == i) {
                return convertLocalToUTC;
            }
            IllegalInstantException illegalInstantException = new IllegalInstantException(j2, this.f18011.getID());
            IllegalFieldValueException illegalFieldValueException = new IllegalFieldValueException(this.f18014.getType(), Integer.valueOf(i), illegalInstantException.getMessage());
            illegalFieldValueException.initCause(illegalInstantException);
            throw illegalFieldValueException;
        }

        public long set(long j, String str, Locale locale) {
            return this.f18011.convertLocalToUTC(this.f18014.set(this.f18011.convertUTCToLocal(j), str, locale), false, j);
        }
    }

    static class ZonedDurationField extends BaseDurationField {
        private static final long serialVersionUID = -485345310999208286L;
        final DurationField iField;
        final boolean iTimeField;
        final DateTimeZone iZone;

        ZonedDurationField(DurationField durationField, DateTimeZone dateTimeZone) {
            super(durationField.getType());
            if (!durationField.isSupported()) {
                throw new IllegalArgumentException();
            }
            this.iField = durationField;
            this.iTimeField = ZonedChronology.m22950(durationField);
            this.iZone = dateTimeZone;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private int m22953(long j) {
            int offsetFromLocal = this.iZone.getOffsetFromLocal(j);
            if (((j - ((long) offsetFromLocal)) ^ j) >= 0 || (((long) offsetFromLocal) ^ j) >= 0) {
                return offsetFromLocal;
            }
            throw new ArithmeticException("Subtracting time zone offset caused overflow");
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private long m22954(long j) {
            return this.iZone.convertUTCToLocal(j);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m22955(long j) {
            int offset = this.iZone.getOffset(j);
            if (((((long) offset) + j) ^ j) >= 0 || (((long) offset) ^ j) < 0) {
                return offset;
            }
            throw new ArithmeticException("Adding time zone offset caused overflow");
        }

        public long add(long j, int i) {
            int r0 = m22955(j);
            long add = this.iField.add(((long) r0) + j, i);
            if (!this.iTimeField) {
                r0 = m22953(add);
            }
            return add - ((long) r0);
        }

        public long add(long j, long j2) {
            int r0 = m22955(j);
            long add = this.iField.add(((long) r0) + j, j2);
            if (!this.iTimeField) {
                r0 = m22953(add);
            }
            return add - ((long) r0);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ZonedDurationField)) {
                return false;
            }
            ZonedDurationField zonedDurationField = (ZonedDurationField) obj;
            return this.iField.equals(zonedDurationField.iField) && this.iZone.equals(zonedDurationField.iZone);
        }

        public int getDifference(long j, long j2) {
            int r1 = m22955(j2);
            return this.iField.getDifference(((long) (this.iTimeField ? r1 : m22955(j))) + j, ((long) r1) + j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            int r1 = m22955(j2);
            return this.iField.getDifferenceAsLong(((long) (this.iTimeField ? r1 : m22955(j))) + j, ((long) r1) + j2);
        }

        public long getMillis(int i, long j) {
            return this.iField.getMillis(i, m22954(j));
        }

        public long getMillis(long j, long j2) {
            return this.iField.getMillis(j, m22954(j2));
        }

        public long getUnitMillis() {
            return this.iField.getUnitMillis();
        }

        public int getValue(long j, long j2) {
            return this.iField.getValue(j, m22954(j2));
        }

        public long getValueAsLong(long j, long j2) {
            return this.iField.getValueAsLong(j, m22954(j2));
        }

        public int hashCode() {
            return this.iField.hashCode() ^ this.iZone.hashCode();
        }

        public boolean isPrecise() {
            return this.iTimeField ? this.iField.isPrecise() : this.iField.isPrecise() && this.iZone.isFixed();
        }
    }

    private ZonedChronology(Chronology chronology, DateTimeZone dateTimeZone) {
        super(chronology, dateTimeZone);
    }

    public static ZonedChronology getInstance(Chronology chronology, DateTimeZone dateTimeZone) {
        if (chronology == null) {
            throw new IllegalArgumentException("Must supply a chronology");
        }
        Chronology withUTC = chronology.withUTC();
        if (withUTC == null) {
            throw new IllegalArgumentException("UTC chronology must not be null");
        } else if (dateTimeZone != null) {
            return new ZonedChronology(withUTC, dateTimeZone);
        } else {
            throw new IllegalArgumentException("DateTimeZone must not be null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private long m22947(long j) {
        if (j == Long.MAX_VALUE) {
            return Long.MAX_VALUE;
        }
        if (j == Long.MIN_VALUE) {
            return Long.MIN_VALUE;
        }
        DateTimeZone zone = getZone();
        int offsetFromLocal = zone.getOffsetFromLocal(j);
        long j2 = j - ((long) offsetFromLocal);
        if (j > 604800000 && j2 < 0) {
            return Long.MAX_VALUE;
        }
        if (j < -604800000 && j2 > 0) {
            return Long.MIN_VALUE;
        }
        if (offsetFromLocal == zone.getOffset(j2)) {
            return j2;
        }
        throw new IllegalInstantException(j, zone.getID());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeField m22948(DateTimeField dateTimeField, HashMap<Object, Object> hashMap) {
        if (dateTimeField == null || !dateTimeField.isSupported()) {
            return dateTimeField;
        }
        if (hashMap.containsKey(dateTimeField)) {
            return (DateTimeField) hashMap.get(dateTimeField);
        }
        ZonedDateTimeField zonedDateTimeField = new ZonedDateTimeField(dateTimeField, getZone(), m22949(dateTimeField.getDurationField(), hashMap), m22949(dateTimeField.getRangeDurationField(), hashMap), m22949(dateTimeField.getLeapDurationField(), hashMap));
        hashMap.put(dateTimeField, zonedDateTimeField);
        return zonedDateTimeField;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DurationField m22949(DurationField durationField, HashMap<Object, Object> hashMap) {
        if (durationField == null || !durationField.isSupported()) {
            return durationField;
        }
        if (hashMap.containsKey(durationField)) {
            return (DurationField) hashMap.get(durationField);
        }
        ZonedDurationField zonedDurationField = new ZonedDurationField(durationField, getZone());
        hashMap.put(durationField, zonedDurationField);
        return zonedDurationField;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m22950(DurationField durationField) {
        return durationField != null && durationField.getUnitMillis() < 43200000;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ZonedChronology)) {
            return false;
        }
        ZonedChronology zonedChronology = (ZonedChronology) obj;
        return m22774().equals(zonedChronology.m22774()) && getZone().equals(zonedChronology.getZone());
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        return m22947(m22774().getDateTimeMillis(i, i2, i3, i4));
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        return m22947(m22774().getDateTimeMillis(i, i2, i3, i4, i5, i6, i7));
    }

    public long getDateTimeMillis(long j, int i, int i2, int i3, int i4) throws IllegalArgumentException {
        return m22947(m22774().getDateTimeMillis(((long) getZone().getOffset(j)) + j, i, i2, i3, i4));
    }

    public DateTimeZone getZone() {
        return (DateTimeZone) m22773();
    }

    public int hashCode() {
        return 326565 + (getZone().hashCode() * 11) + (m22774().hashCode() * 7);
    }

    public String toString() {
        return "ZonedChronology[" + m22774() + ", " + getZone().getID() + ']';
    }

    public Chronology withUTC() {
        return m22774();
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == m22773() ? this : dateTimeZone == DateTimeZone.UTC ? m22774() : new ZonedChronology(m22774(), dateTimeZone);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22951(AssembledChronology.Fields fields) {
        HashMap hashMap = new HashMap();
        fields.f17889 = m22949(fields.f17889, (HashMap<Object, Object>) hashMap);
        fields.f17899 = m22949(fields.f17899, (HashMap<Object, Object>) hashMap);
        fields.f17898 = m22949(fields.f17898, (HashMap<Object, Object>) hashMap);
        fields.f17895 = m22949(fields.f17895, (HashMap<Object, Object>) hashMap);
        fields.f17884 = m22949(fields.f17884, (HashMap<Object, Object>) hashMap);
        fields.f17883 = m22949(fields.f17883, (HashMap<Object, Object>) hashMap);
        fields.f17881 = m22949(fields.f17881, (HashMap<Object, Object>) hashMap);
        fields.f17907 = m22949(fields.f17907, (HashMap<Object, Object>) hashMap);
        fields.f17909 = m22949(fields.f17909, (HashMap<Object, Object>) hashMap);
        fields.f17910 = m22949(fields.f17910, (HashMap<Object, Object>) hashMap);
        fields.f17908 = m22949(fields.f17908, (HashMap<Object, Object>) hashMap);
        fields.f17911 = m22949(fields.f17911, (HashMap<Object, Object>) hashMap);
        fields.f17915 = m22948(fields.f17915, (HashMap<Object, Object>) hashMap);
        fields.f17900 = m22948(fields.f17900, (HashMap<Object, Object>) hashMap);
        fields.f17902 = m22948(fields.f17902, (HashMap<Object, Object>) hashMap);
        fields.f17882 = m22948(fields.f17882, (HashMap<Object, Object>) hashMap);
        fields.f17885 = m22948(fields.f17885, (HashMap<Object, Object>) hashMap);
        fields.f17897 = m22948(fields.f17897, (HashMap<Object, Object>) hashMap);
        fields.f17901 = m22948(fields.f17901, (HashMap<Object, Object>) hashMap);
        fields.f17903 = m22948(fields.f17903, (HashMap<Object, Object>) hashMap);
        fields.f17912 = m22948(fields.f17912, (HashMap<Object, Object>) hashMap);
        fields.f17904 = m22948(fields.f17904, (HashMap<Object, Object>) hashMap);
        fields.f17905 = m22948(fields.f17905, (HashMap<Object, Object>) hashMap);
        fields.f17906 = m22948(fields.f17906, (HashMap<Object, Object>) hashMap);
        fields.f17886 = m22948(fields.f17886, (HashMap<Object, Object>) hashMap);
        fields.f17887 = m22948(fields.f17887, (HashMap<Object, Object>) hashMap);
        fields.f17913 = m22948(fields.f17913, (HashMap<Object, Object>) hashMap);
        fields.f17914 = m22948(fields.f17914, (HashMap<Object, Object>) hashMap);
        fields.f17891 = m22948(fields.f17891, (HashMap<Object, Object>) hashMap);
        fields.f17892 = m22948(fields.f17892, (HashMap<Object, Object>) hashMap);
        fields.f17893 = m22948(fields.f17893, (HashMap<Object, Object>) hashMap);
        fields.f17890 = m22948(fields.f17890, (HashMap<Object, Object>) hashMap);
        fields.f17888 = m22948(fields.f17888, (HashMap<Object, Object>) hashMap);
        fields.f17894 = m22948(fields.f17894, (HashMap<Object, Object>) hashMap);
        fields.f17896 = m22948(fields.f17896, (HashMap<Object, Object>) hashMap);
    }
}
