package org2.joda.time.chrono;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.DurationField;
import org2.joda.time.IllegalFieldValueException;
import org2.joda.time.Instant;
import org2.joda.time.LocalDate;
import org2.joda.time.ReadableInstant;
import org2.joda.time.ReadablePartial;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.BaseDateTimeField;
import org2.joda.time.field.DecoratedDurationField;
import org2.joda.time.format.ISODateTimeFormat;

public final class GJChronology extends AssembledChronology {
    private static final long serialVersionUID = -2545574827706931671L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<GJCacheKey, GJChronology> f17962 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    static final Instant f17963 = new Instant(-12219292800000L);
    private Instant iCutoverInstant;
    private long iCutoverMillis;
    /* access modifiers changed from: private */
    public long iGapDuration;
    /* access modifiers changed from: private */
    public GregorianChronology iGregorianChronology;
    private JulianChronology iJulianChronology;

    private class CutoverField extends BaseDateTimeField {

        /* renamed from: ʻ  reason: contains not printable characters */
        protected DurationField f17964;

        /* renamed from: 连任  reason: contains not printable characters */
        protected DurationField f17966;

        /* renamed from: 靐  reason: contains not printable characters */
        final DateTimeField f17967;

        /* renamed from: 麤  reason: contains not printable characters */
        final boolean f17968;

        /* renamed from: 齉  reason: contains not printable characters */
        final long f17969;

        /* renamed from: 龘  reason: contains not printable characters */
        final DateTimeField f17970;

        CutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, long j) {
            this(gJChronology, dateTimeField, dateTimeField2, j, false);
        }

        CutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, long j, boolean z) {
            this(dateTimeField, dateTimeField2, (DurationField) null, j, z);
        }

        CutoverField(DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, long j, boolean z) {
            super(dateTimeField2.getType());
            this.f17970 = dateTimeField;
            this.f17967 = dateTimeField2;
            this.f17969 = j;
            this.f17968 = z;
            this.f17966 = dateTimeField2.getDurationField();
            if (durationField == null && (durationField = dateTimeField2.getRangeDurationField()) == null) {
                durationField = dateTimeField.getRangeDurationField();
            }
            this.f17964 = durationField;
        }

        public long add(long j, int i) {
            return this.f17967.add(j, i);
        }

        public long add(long j, long j2) {
            return this.f17967.add(j, j2);
        }

        public int[] add(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
            if (i2 == 0) {
                return iArr;
            }
            if (!DateTimeUtils.m22722(readablePartial)) {
                return super.add(readablePartial, i, iArr, i2);
            }
            long j = 0;
            int size = readablePartial.size();
            for (int i3 = 0; i3 < size; i3++) {
                j = readablePartial.getFieldType(i3).getField(GJChronology.this).set(j, iArr[i3]);
            }
            return GJChronology.this.get(readablePartial, add(j, i2));
        }

        public int get(long j) {
            return j >= this.f17969 ? this.f17967.get(j) : this.f17970.get(j);
        }

        public String getAsShortText(int i, Locale locale) {
            return this.f17967.getAsShortText(i, locale);
        }

        public String getAsShortText(long j, Locale locale) {
            return j >= this.f17969 ? this.f17967.getAsShortText(j, locale) : this.f17970.getAsShortText(j, locale);
        }

        public String getAsText(int i, Locale locale) {
            return this.f17967.getAsText(i, locale);
        }

        public String getAsText(long j, Locale locale) {
            return j >= this.f17969 ? this.f17967.getAsText(j, locale) : this.f17970.getAsText(j, locale);
        }

        public int getDifference(long j, long j2) {
            return this.f17967.getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            return this.f17967.getDifferenceAsLong(j, j2);
        }

        public DurationField getDurationField() {
            return this.f17966;
        }

        public int getLeapAmount(long j) {
            return j >= this.f17969 ? this.f17967.getLeapAmount(j) : this.f17970.getLeapAmount(j);
        }

        public DurationField getLeapDurationField() {
            return this.f17967.getLeapDurationField();
        }

        public int getMaximumShortTextLength(Locale locale) {
            return Math.max(this.f17970.getMaximumShortTextLength(locale), this.f17967.getMaximumShortTextLength(locale));
        }

        public int getMaximumTextLength(Locale locale) {
            return Math.max(this.f17970.getMaximumTextLength(locale), this.f17967.getMaximumTextLength(locale));
        }

        public int getMaximumValue() {
            return this.f17967.getMaximumValue();
        }

        public int getMaximumValue(long j) {
            if (j >= this.f17969) {
                return this.f17967.getMaximumValue(j);
            }
            int maximumValue = this.f17970.getMaximumValue(j);
            return this.f17970.set(j, maximumValue) >= this.f17969 ? this.f17970.get(this.f17970.add(this.f17969, -1)) : maximumValue;
        }

        public int getMaximumValue(ReadablePartial readablePartial) {
            return getMaximumValue(GJChronology.getInstanceUTC().set(readablePartial, 0));
        }

        public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
            GJChronology instanceUTC = GJChronology.getInstanceUTC();
            int size = readablePartial.size();
            long j = 0;
            for (int i = 0; i < size; i++) {
                DateTimeField field = readablePartial.getFieldType(i).getField(instanceUTC);
                if (iArr[i] <= field.getMaximumValue(j)) {
                    j = field.set(j, iArr[i]);
                }
            }
            return getMaximumValue(j);
        }

        public int getMinimumValue() {
            return this.f17970.getMinimumValue();
        }

        public int getMinimumValue(long j) {
            if (j < this.f17969) {
                return this.f17970.getMinimumValue(j);
            }
            int minimumValue = this.f17967.getMinimumValue(j);
            return this.f17967.set(j, minimumValue) < this.f17969 ? this.f17967.get(this.f17969) : minimumValue;
        }

        public int getMinimumValue(ReadablePartial readablePartial) {
            return this.f17970.getMinimumValue(readablePartial);
        }

        public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
            return this.f17970.getMinimumValue(readablePartial, iArr);
        }

        public DurationField getRangeDurationField() {
            return this.f17964;
        }

        public boolean isLeap(long j) {
            return j >= this.f17969 ? this.f17967.isLeap(j) : this.f17970.isLeap(j);
        }

        public boolean isLenient() {
            return false;
        }

        public long roundCeiling(long j) {
            if (j >= this.f17969) {
                return this.f17967.roundCeiling(j);
            }
            long roundCeiling = this.f17970.roundCeiling(j);
            return (roundCeiling < this.f17969 || roundCeiling - GJChronology.this.iGapDuration < this.f17969) ? roundCeiling : m22872(roundCeiling);
        }

        public long roundFloor(long j) {
            if (j < this.f17969) {
                return this.f17970.roundFloor(j);
            }
            long roundFloor = this.f17967.roundFloor(j);
            return (roundFloor >= this.f17969 || GJChronology.this.iGapDuration + roundFloor >= this.f17969) ? roundFloor : m22871(roundFloor);
        }

        public long set(long j, int i) {
            long j2;
            if (j >= this.f17969) {
                j2 = this.f17967.set(j, i);
                if (j2 < this.f17969) {
                    if (GJChronology.this.iGapDuration + j2 < this.f17969) {
                        j2 = m22871(j2);
                    }
                    if (get(j2) != i) {
                        throw new IllegalFieldValueException(this.f17967.getType(), (Number) Integer.valueOf(i), (Number) null, (Number) null);
                    }
                }
            } else {
                j2 = this.f17970.set(j, i);
                if (j2 >= this.f17969) {
                    if (j2 - GJChronology.this.iGapDuration >= this.f17969) {
                        j2 = m22872(j2);
                    }
                    if (get(j2) != i) {
                        throw new IllegalFieldValueException(this.f17970.getType(), (Number) Integer.valueOf(i), (Number) null, (Number) null);
                    }
                }
            }
            return j2;
        }

        public long set(long j, String str, Locale locale) {
            if (j >= this.f17969) {
                long j2 = this.f17967.set(j, str, locale);
                return (j2 >= this.f17969 || GJChronology.this.iGapDuration + j2 >= this.f17969) ? j2 : m22871(j2);
            }
            long j3 = this.f17970.set(j, str, locale);
            return (j3 < this.f17969 || j3 - GJChronology.this.iGapDuration < this.f17969) ? j3 : m22872(j3);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public long m22871(long j) {
            return this.f17968 ? GJChronology.this.m22867(j) : GJChronology.this.m22866(j);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m22872(long j) {
            return this.f17968 ? GJChronology.this.m22868(j) : GJChronology.this.m22869(j);
        }
    }

    private final class ImpreciseCutoverField extends CutoverField {
        ImpreciseCutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, long j) {
            this(dateTimeField, dateTimeField2, (DurationField) null, j, false);
        }

        ImpreciseCutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, long j) {
            this(dateTimeField, dateTimeField2, durationField, j, false);
        }

        ImpreciseCutoverField(DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, long j, boolean z) {
            super(GJChronology.this, dateTimeField, dateTimeField2, j, z);
            this.f17966 = durationField == null ? new LinkedDurationField(this.f17966, this) : durationField;
        }

        ImpreciseCutoverField(GJChronology gJChronology, DateTimeField dateTimeField, DateTimeField dateTimeField2, DurationField durationField, DurationField durationField2, long j) {
            this(dateTimeField, dateTimeField2, durationField, j, false);
            this.f17964 = durationField2;
        }

        public long add(long j, int i) {
            if (j >= this.f17969) {
                long add = this.f17967.add(j, i);
                if (add >= this.f17969 || GJChronology.this.iGapDuration + add >= this.f17969) {
                    return add;
                }
                if (this.f17968) {
                    if (GJChronology.this.iGregorianChronology.weekyear().get(add) <= 0) {
                        add = GJChronology.this.iGregorianChronology.weekyear().add(add, -1);
                    }
                } else if (GJChronology.this.iGregorianChronology.year().get(add) <= 0) {
                    add = GJChronology.this.iGregorianChronology.year().add(add, -1);
                }
                return m22871(add);
            }
            long add2 = this.f17970.add(j, i);
            return (add2 < this.f17969 || add2 - GJChronology.this.iGapDuration < this.f17969) ? add2 : m22872(add2);
        }

        public long add(long j, long j2) {
            if (j >= this.f17969) {
                long add = this.f17967.add(j, j2);
                if (add >= this.f17969 || GJChronology.this.iGapDuration + add >= this.f17969) {
                    return add;
                }
                if (this.f17968) {
                    if (GJChronology.this.iGregorianChronology.weekyear().get(add) <= 0) {
                        add = GJChronology.this.iGregorianChronology.weekyear().add(add, -1);
                    }
                } else if (GJChronology.this.iGregorianChronology.year().get(add) <= 0) {
                    add = GJChronology.this.iGregorianChronology.year().add(add, -1);
                }
                return m22871(add);
            }
            long add2 = this.f17970.add(j, j2);
            return (add2 < this.f17969 || add2 - GJChronology.this.iGapDuration < this.f17969) ? add2 : m22872(add2);
        }

        public int getDifference(long j, long j2) {
            if (j >= this.f17969) {
                if (j2 >= this.f17969) {
                    return this.f17967.getDifference(j, j2);
                }
                return this.f17970.getDifference(m22871(j), j2);
            } else if (j2 < this.f17969) {
                return this.f17970.getDifference(j, j2);
            } else {
                return this.f17967.getDifference(m22872(j), j2);
            }
        }

        public long getDifferenceAsLong(long j, long j2) {
            if (j >= this.f17969) {
                if (j2 >= this.f17969) {
                    return this.f17967.getDifferenceAsLong(j, j2);
                }
                return this.f17970.getDifferenceAsLong(m22871(j), j2);
            } else if (j2 < this.f17969) {
                return this.f17970.getDifferenceAsLong(j, j2);
            } else {
                return this.f17967.getDifferenceAsLong(m22872(j), j2);
            }
        }

        public int getMaximumValue(long j) {
            return j >= this.f17969 ? this.f17967.getMaximumValue(j) : this.f17970.getMaximumValue(j);
        }

        public int getMinimumValue(long j) {
            return j >= this.f17969 ? this.f17967.getMinimumValue(j) : this.f17970.getMinimumValue(j);
        }
    }

    private static class LinkedDurationField extends DecoratedDurationField {
        private static final long serialVersionUID = 4097975388007713084L;
        private final ImpreciseCutoverField iField;

        LinkedDurationField(DurationField durationField, ImpreciseCutoverField impreciseCutoverField) {
            super(durationField, durationField.getType());
            this.iField = impreciseCutoverField;
        }

        public long add(long j, int i) {
            return this.iField.add(j, i);
        }

        public long add(long j, long j2) {
            return this.iField.add(j, j2);
        }

        public int getDifference(long j, long j2) {
            return this.iField.getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            return this.iField.getDifferenceAsLong(j, j2);
        }
    }

    private GJChronology(Chronology chronology, JulianChronology julianChronology, GregorianChronology gregorianChronology, Instant instant) {
        super(chronology, new Object[]{julianChronology, gregorianChronology, instant});
    }

    private GJChronology(JulianChronology julianChronology, GregorianChronology gregorianChronology, Instant instant) {
        super((Chronology) null, new Object[]{julianChronology, gregorianChronology, instant});
    }

    public static GJChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), (ReadableInstant) f17963, 4);
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, (ReadableInstant) f17963, 4);
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone, long j, int i) {
        return getInstance(dateTimeZone, j == f17963.getMillis() ? null : new Instant(j), i);
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone, ReadableInstant readableInstant) {
        return getInstance(dateTimeZone, readableInstant, 4);
    }

    public static GJChronology getInstance(DateTimeZone dateTimeZone, ReadableInstant readableInstant, int i) {
        ReadableInstant readableInstant2;
        GJChronology gJChronology;
        DateTimeZone r2 = DateTimeUtils.m22719(dateTimeZone);
        if (readableInstant == null) {
            readableInstant2 = f17963;
        } else {
            ReadableInstant instant = readableInstant.toInstant();
            if (new LocalDate(instant.getMillis(), GregorianChronology.getInstance(r2)).getYear() <= 0) {
                throw new IllegalArgumentException("Cutover too early. Must be on or after 0001-01-01.");
            }
            readableInstant2 = instant;
        }
        GJCacheKey gJCacheKey = new GJCacheKey(r2, readableInstant2, i);
        GJChronology gJChronology2 = f17962.get(gJCacheKey);
        if (gJChronology2 != null) {
            return gJChronology2;
        }
        if (r2 == DateTimeZone.UTC) {
            gJChronology = new GJChronology(JulianChronology.getInstance(r2, i), GregorianChronology.getInstance(r2, i), readableInstant2);
        } else {
            GJChronology instance = getInstance(DateTimeZone.UTC, readableInstant2, i);
            gJChronology = new GJChronology(ZonedChronology.getInstance(instance, r2), instance.iJulianChronology, instance.iGregorianChronology, instance.iCutoverInstant);
        }
        GJChronology putIfAbsent = f17962.putIfAbsent(gJCacheKey, gJChronology);
        return putIfAbsent != null ? putIfAbsent : gJChronology;
    }

    public static GJChronology getInstanceUTC() {
        return getInstance(DateTimeZone.UTC, (ReadableInstant) f17963, 4);
    }

    private Object readResolve() {
        return getInstance(getZone(), (ReadableInstant) this.iCutoverInstant, getMinimumDaysInFirstWeek());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static long m22862(long j, Chronology chronology, Chronology chronology2) {
        return chronology2.millisOfDay().set(chronology2.dayOfWeek().set(chronology2.weekOfWeekyear().set(chronology2.weekyear().set(0, chronology.weekyear().get(j)), chronology.weekOfWeekyear().get(j)), chronology.dayOfWeek().get(j)), chronology.millisOfDay().get(j));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static long m22864(long j, Chronology chronology, Chronology chronology2) {
        return chronology2.getDateTimeMillis(chronology.year().get(j), chronology.monthOfYear().get(j), chronology.dayOfMonth().get(j), chronology.millisOfDay().get(j));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GJChronology)) {
            return false;
        }
        GJChronology gJChronology = (GJChronology) obj;
        return this.iCutoverMillis == gJChronology.iCutoverMillis && getMinimumDaysInFirstWeek() == gJChronology.getMinimumDaysInFirstWeek() && getZone().equals(gJChronology.getZone());
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        Chronology r0 = m22774();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4);
        }
        long dateTimeMillis = this.iGregorianChronology.getDateTimeMillis(i, i2, i3, i4);
        if (dateTimeMillis >= this.iCutoverMillis) {
            return dateTimeMillis;
        }
        long dateTimeMillis2 = this.iJulianChronology.getDateTimeMillis(i, i2, i3, i4);
        if (dateTimeMillis2 < this.iCutoverMillis) {
            return dateTimeMillis2;
        }
        throw new IllegalArgumentException("Specified date does not exist");
    }

    public long getDateTimeMillis(int i, int i2, int i3, int i4, int i5, int i6, int i7) throws IllegalArgumentException {
        long dateTimeMillis;
        Chronology r0 = m22774();
        if (r0 != null) {
            return r0.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        }
        try {
            dateTimeMillis = this.iGregorianChronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        } catch (IllegalFieldValueException e) {
            IllegalFieldValueException illegalFieldValueException = e;
            if (i2 == 2 && i3 == 29) {
                dateTimeMillis = this.iGregorianChronology.getDateTimeMillis(i, i2, 28, i4, i5, i6, i7);
                if (dateTimeMillis >= this.iCutoverMillis) {
                    throw illegalFieldValueException;
                }
            } else {
                throw illegalFieldValueException;
            }
        }
        if (dateTimeMillis >= this.iCutoverMillis) {
            return dateTimeMillis;
        }
        long dateTimeMillis2 = this.iJulianChronology.getDateTimeMillis(i, i2, i3, i4, i5, i6, i7);
        if (dateTimeMillis2 < this.iCutoverMillis) {
            return dateTimeMillis2;
        }
        throw new IllegalArgumentException("Specified date does not exist");
    }

    public Instant getGregorianCutover() {
        return this.iCutoverInstant;
    }

    public int getMinimumDaysInFirstWeek() {
        return this.iGregorianChronology.getMinimumDaysInFirstWeek();
    }

    public DateTimeZone getZone() {
        Chronology r0 = m22774();
        return r0 != null ? r0.getZone() : DateTimeZone.UTC;
    }

    public int hashCode() {
        return ("GJ".hashCode() * 11) + getZone().hashCode() + getMinimumDaysInFirstWeek() + this.iCutoverInstant.hashCode();
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer(60);
        stringBuffer.append("GJChronology");
        stringBuffer.append('[');
        stringBuffer.append(getZone().getID());
        if (this.iCutoverMillis != f17963.getMillis()) {
            stringBuffer.append(",cutover=");
            (withUTC().dayOfYear().remainder(this.iCutoverMillis) == 0 ? ISODateTimeFormat.m23259() : ISODateTimeFormat.m23265()).m23069(withUTC()).m23075(stringBuffer, this.iCutoverMillis);
        }
        if (getMinimumDaysInFirstWeek() != 4) {
            stringBuffer.append(",mdfw=");
            stringBuffer.append(getMinimumDaysInFirstWeek());
        }
        stringBuffer.append(']');
        return stringBuffer.toString();
    }

    public Chronology withUTC() {
        return withZone(DateTimeZone.UTC);
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone, (ReadableInstant) this.iCutoverInstant, getMinimumDaysInFirstWeek());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public long m22866(long j) {
        return m22864(j, this.iGregorianChronology, this.iJulianChronology);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public long m22867(long j) {
        return m22862(j, this.iGregorianChronology, this.iJulianChronology);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m22868(long j) {
        return m22862(j, this.iJulianChronology, this.iGregorianChronology);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22869(long j) {
        return m22864(j, this.iJulianChronology, this.iGregorianChronology);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22870(AssembledChronology.Fields fields) {
        Object[] objArr = (Object[]) m22773();
        JulianChronology julianChronology = (JulianChronology) objArr[0];
        GregorianChronology gregorianChronology = (GregorianChronology) objArr[1];
        Instant instant = (Instant) objArr[2];
        this.iCutoverMillis = instant.getMillis();
        this.iJulianChronology = julianChronology;
        this.iGregorianChronology = gregorianChronology;
        this.iCutoverInstant = instant;
        if (m22774() == null) {
            if (julianChronology.getMinimumDaysInFirstWeek() != gregorianChronology.getMinimumDaysInFirstWeek()) {
                throw new IllegalArgumentException();
            }
            this.iGapDuration = this.iCutoverMillis - m22869(this.iCutoverMillis);
            fields.m22778((Chronology) gregorianChronology);
            if (gregorianChronology.millisOfDay().get(this.iCutoverMillis) == 0) {
                fields.f17886 = new CutoverField(this, julianChronology.millisOfSecond(), fields.f17886, this.iCutoverMillis);
                fields.f17887 = new CutoverField(this, julianChronology.millisOfDay(), fields.f17887, this.iCutoverMillis);
                fields.f17913 = new CutoverField(this, julianChronology.secondOfMinute(), fields.f17913, this.iCutoverMillis);
                fields.f17914 = new CutoverField(this, julianChronology.secondOfDay(), fields.f17914, this.iCutoverMillis);
                fields.f17891 = new CutoverField(this, julianChronology.minuteOfHour(), fields.f17891, this.iCutoverMillis);
                fields.f17892 = new CutoverField(this, julianChronology.minuteOfDay(), fields.f17892, this.iCutoverMillis);
                fields.f17893 = new CutoverField(this, julianChronology.hourOfDay(), fields.f17893, this.iCutoverMillis);
                fields.f17890 = new CutoverField(this, julianChronology.hourOfHalfday(), fields.f17890, this.iCutoverMillis);
                fields.f17888 = new CutoverField(this, julianChronology.clockhourOfDay(), fields.f17888, this.iCutoverMillis);
                fields.f17894 = new CutoverField(this, julianChronology.clockhourOfHalfday(), fields.f17894, this.iCutoverMillis);
                fields.f17896 = new CutoverField(this, julianChronology.halfdayOfDay(), fields.f17896, this.iCutoverMillis);
            }
            fields.f17885 = new CutoverField(this, julianChronology.era(), fields.f17885, this.iCutoverMillis);
            fields.f17915 = new ImpreciseCutoverField(this, julianChronology.year(), fields.f17915, this.iCutoverMillis);
            fields.f17898 = fields.f17915.getDurationField();
            fields.f17900 = new ImpreciseCutoverField(this, julianChronology.yearOfEra(), fields.f17900, fields.f17898, this.iCutoverMillis);
            fields.f17882 = new ImpreciseCutoverField(this, julianChronology.centuryOfEra(), fields.f17882, this.iCutoverMillis);
            fields.f17899 = fields.f17882.getDurationField();
            fields.f17902 = new ImpreciseCutoverField(this, julianChronology.yearOfCentury(), fields.f17902, fields.f17898, fields.f17899, this.iCutoverMillis);
            fields.f17912 = new ImpreciseCutoverField(this, julianChronology.monthOfYear(), fields.f17912, (DurationField) null, fields.f17898, this.iCutoverMillis);
            fields.f17895 = fields.f17912.getDurationField();
            fields.f17905 = new ImpreciseCutoverField(julianChronology.weekyear(), fields.f17905, (DurationField) null, this.iCutoverMillis, true);
            fields.f17884 = fields.f17905.getDurationField();
            fields.f17906 = new ImpreciseCutoverField(this, julianChronology.weekyearOfCentury(), fields.f17906, fields.f17884, fields.f17899, this.iCutoverMillis);
            fields.f17903 = new CutoverField(julianChronology.dayOfYear(), fields.f17903, fields.f17898, gregorianChronology.year().roundCeiling(this.iCutoverMillis), false);
            fields.f17904 = new CutoverField(julianChronology.weekOfWeekyear(), fields.f17904, fields.f17884, gregorianChronology.weekyear().roundCeiling(this.iCutoverMillis), true);
            CutoverField cutoverField = new CutoverField(this, julianChronology.dayOfMonth(), fields.f17901, this.iCutoverMillis);
            cutoverField.f17964 = fields.f17895;
            fields.f17901 = cutoverField;
        }
    }
}
