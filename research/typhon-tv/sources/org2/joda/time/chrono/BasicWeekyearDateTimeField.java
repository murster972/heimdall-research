package org2.joda.time.chrono;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.field.ImpreciseDateTimeField;

final class BasicWeekyearDateTimeField extends ImpreciseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f17948;

    BasicWeekyearDateTimeField(BasicChronology basicChronology) {
        super(DateTimeFieldType.weekyear(), basicChronology.m22792());
        this.f17948 = basicChronology;
    }

    public long add(long j, int i) {
        return i == 0 ? j : set(j, get(j) + i);
    }

    public long add(long j, long j2) {
        return add(j, FieldUtils.m23034(j2));
    }

    public long addWrapField(long j, int i) {
        return add(j, i);
    }

    public int get(long j) {
        return this.f17948.m22799(j);
    }

    public long getDifferenceAsLong(long j, long j2) {
        if (j < j2) {
            return (long) (-getDifference(j2, j));
        }
        int i = get(j);
        int i2 = get(j2);
        long remainder = remainder(j);
        long remainder2 = remainder(j2);
        long j3 = (remainder2 < 31449600000L || this.f17948.m22802(i) > 52) ? remainder2 : remainder2 - 604800000;
        int i3 = i - i2;
        if (remainder < j3) {
            i3--;
        }
        return (long) i3;
    }

    public int getLeapAmount(long j) {
        return this.f17948.m22802(this.f17948.m22799(j)) - 52;
    }

    public DurationField getLeapDurationField() {
        return this.f17948.weeks();
    }

    public int getMaximumValue() {
        return this.f17948.m22783();
    }

    public int getMinimumValue() {
        return this.f17948.m22798();
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public boolean isLeap(long j) {
        return this.f17948.m22802(this.f17948.m22799(j)) > 52;
    }

    public boolean isLenient() {
        return false;
    }

    public long remainder(long j) {
        return j - roundFloor(j);
    }

    public long roundFloor(long j) {
        long roundFloor = this.f17948.weekOfWeekyear().roundFloor(j);
        int r2 = this.f17948.m22785(roundFloor);
        return r2 > 1 ? roundFloor - (((long) (r2 - 1)) * 604800000) : roundFloor;
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, Math.abs(i), this.f17948.m22798(), this.f17948.m22783());
        int i2 = get(j);
        if (i2 == i) {
            return j;
        }
        int r4 = this.f17948.m22788(j);
        int r1 = this.f17948.m22802(i2);
        int r0 = this.f17948.m22802(i);
        if (r0 >= r1) {
            r0 = r1;
        }
        int r12 = this.f17948.m22785(j);
        if (r12 <= r0) {
            r0 = r12;
        }
        long r2 = this.f17948.m22786(j, i);
        int i3 = get(r2);
        if (i3 < i) {
            r2 += 604800000;
        } else if (i3 > i) {
            r2 -= 604800000;
        }
        return this.f17948.dayOfWeek().set((((long) (r0 - this.f17948.m22785(r2))) * 604800000) + r2, r4);
    }
}
