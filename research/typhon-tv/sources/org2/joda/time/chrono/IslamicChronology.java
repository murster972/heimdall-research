package org2.joda.time.chrono;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTime;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadableDateTime;
import org2.joda.time.chrono.AssembledChronology;

public final class IslamicChronology extends BasicChronology {
    public static final int AH = 1;
    public static final LeapYearPatternType LEAP_YEAR_15_BASED = new LeapYearPatternType(0, 623158436);
    public static final LeapYearPatternType LEAP_YEAR_16_BASED = new LeapYearPatternType(1, 623191204);
    public static final LeapYearPatternType LEAP_YEAR_HABASH_AL_HASIB = new LeapYearPatternType(3, 153692453);
    public static final LeapYearPatternType LEAP_YEAR_INDIAN = new LeapYearPatternType(2, 690562340);
    private static final long serialVersionUID = -3663823829888L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, IslamicChronology[]> f17997 = new ConcurrentHashMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final IslamicChronology f17998 = getInstance(DateTimeZone.UTC);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField f17999 = new BasicSingleEraDateTimeField("AH");
    private final LeapYearPatternType iLeapYears;

    public static class LeapYearPatternType implements Serializable {
        private static final long serialVersionUID = 26581275372698L;
        final byte index;
        final int pattern;

        LeapYearPatternType(int i, int i2) {
            this.index = (byte) i;
            this.pattern = i2;
        }

        private Object readResolve() {
            switch (this.index) {
                case 0:
                    return IslamicChronology.LEAP_YEAR_15_BASED;
                case 1:
                    return IslamicChronology.LEAP_YEAR_16_BASED;
                case 2:
                    return IslamicChronology.LEAP_YEAR_INDIAN;
                case 3:
                    return IslamicChronology.LEAP_YEAR_HABASH_AL_HASIB;
                default:
                    return this;
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof LeapYearPatternType) && this.index == ((LeapYearPatternType) obj).index;
        }

        public int hashCode() {
            return this.index;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22927(int i) {
            return ((1 << (i % 30)) & this.pattern) > 0;
        }
    }

    IslamicChronology(Chronology chronology, Object obj, LeapYearPatternType leapYearPatternType) {
        super(chronology, obj, 4);
        this.iLeapYears = leapYearPatternType;
    }

    public static IslamicChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), LEAP_YEAR_16_BASED);
    }

    public static IslamicChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, LEAP_YEAR_16_BASED);
    }

    public static IslamicChronology getInstance(DateTimeZone dateTimeZone, LeapYearPatternType leapYearPatternType) {
        IslamicChronology[] islamicChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        IslamicChronology[] islamicChronologyArr2 = f17997.get(dateTimeZone);
        if (islamicChronologyArr2 == null) {
            IslamicChronology[] islamicChronologyArr3 = new IslamicChronology[4];
            IslamicChronology[] putIfAbsent = f17997.putIfAbsent(dateTimeZone, islamicChronologyArr3);
            islamicChronologyArr = putIfAbsent != null ? putIfAbsent : islamicChronologyArr3;
        } else {
            islamicChronologyArr = islamicChronologyArr2;
        }
        IslamicChronology islamicChronology = islamicChronologyArr[leapYearPatternType.index];
        if (islamicChronology == null) {
            synchronized (islamicChronologyArr) {
                islamicChronology = islamicChronologyArr[leapYearPatternType.index];
                if (islamicChronology == null) {
                    if (dateTimeZone == DateTimeZone.UTC) {
                        IslamicChronology islamicChronology2 = new IslamicChronology((Chronology) null, (Object) null, leapYearPatternType);
                        islamicChronology = new IslamicChronology(LimitChronology.getInstance(islamicChronology2, new DateTime(1, 1, 1, 0, 0, 0, 0, islamicChronology2), (ReadableDateTime) null), (Object) null, leapYearPatternType);
                    } else {
                        islamicChronology = new IslamicChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, leapYearPatternType), dateTimeZone), (Object) null, leapYearPatternType);
                    }
                    islamicChronologyArr[leapYearPatternType.index] = islamicChronology;
                }
            }
        }
        return islamicChronology;
    }

    public static IslamicChronology getInstanceUTC() {
        return f17998;
    }

    private Object readResolve() {
        Chronology r0 = m22774();
        return r0 == null ? getInstanceUTC() : getInstance(r0.getZone());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof IslamicChronology)) {
            return false;
        }
        return getLeapYearPatternType().index == ((IslamicChronology) obj).getLeapYearPatternType().index && super.equals(obj);
    }

    public LeapYearPatternType getLeapYearPatternType() {
        return this.iLeapYears;
    }

    public int hashCode() {
        return (super.hashCode() * 13) + getLeapYearPatternType().hashCode();
    }

    public Chronology withUTC() {
        return f17998;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22907() {
        return 292271022;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22908(int i) {
        return (i == 12 || (i + -1) % 2 == 0) ? 30 : 29;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public long m22909(long j, int i) {
        int r0 = m22813(j, m22923(j));
        int r1 = m22791(j);
        if (r0 > 354 && !m22916(i)) {
            r0--;
        }
        return ((long) r1) + m22821(i, 1, r0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m22910(int i) {
        if (i > 292271022) {
            throw new ArithmeticException("Year is too large: " + i + " > " + 292271022);
        } else if (i < -292269337) {
            throw new ArithmeticException("Year is too small: " + i + " < " + -292269337);
        } else {
            int i2 = i - 1;
            int i3 = (i2 % 30) + 1;
            long j = (((long) (i2 / 30)) * 918518400000L) - 42521587200000L;
            for (int i4 = 1; i4 < i3; i4++) {
                j += m22916(i4) ? 30672000000L : 30585600000L;
            }
            return j;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m22911() {
        return 30617280288L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m22912() {
        return 15308640144L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m22913() {
        return 2551440384L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m22914() {
        return 21260793600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22915() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22916(int i) {
        return this.iLeapYears.m22927(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public int m22917(int i, int i2) {
        return ((i2 != 12 || !m22916(i)) && (i2 + -1) % 2 != 0) ? 29 : 30;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public int m22918() {
        return 30;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m22919() {
        return 355;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public int m22920(long j) {
        int r0 = m22808(j) - 1;
        if (r0 == 354) {
            return 30;
        }
        return ((r0 % 59) % 30) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public long m22921(int i, int i2) {
        int i3 = i2 - 1;
        return i3 % 2 == 1 ? (((long) (i3 / 2)) * 5097600000L) + 2592000000L : ((long) (i3 / 2)) * 5097600000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22922(int i) {
        return m22916(i) ? 355 : 354;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22923(long j) {
        long j2 = j - -42521587200000L;
        long j3 = j2 % 918518400000L;
        int i = (int) ((30 * (j2 / 918518400000L)) + 1);
        long j4 = m22916(i) ? 30672000000L : 30585600000L;
        while (j3 >= j4) {
            j3 -= j4;
            i++;
            j4 = m22916(i) ? 30672000000L : 30585600000L;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22924(long j, int i) {
        int r0 = (int) ((j - m22810(i)) / 86400000);
        if (r0 == 354) {
            return 12;
        }
        return ((r0 * 2) / 59) + 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22925(long j, long j2) {
        int r0 = m22923(j);
        int r1 = m22923(j2);
        int i = r0 - r1;
        if (j - m22810(r0) < j2 - m22810(r1)) {
            i--;
        }
        return (long) i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22926(AssembledChronology.Fields fields) {
        if (m22774() == null) {
            super.m22823(fields);
            fields.f17885 = f17999;
            fields.f17912 = new BasicMonthOfYearDateTimeField(this, 12);
            fields.f17895 = fields.f17912.getDurationField();
        }
    }
}
