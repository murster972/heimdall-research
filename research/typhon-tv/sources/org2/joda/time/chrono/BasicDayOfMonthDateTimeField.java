package org2.joda.time.chrono;

import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.ReadablePartial;
import org2.joda.time.field.PreciseDurationDateTimeField;

final class BasicDayOfMonthDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final BasicChronology f17937;

    BasicDayOfMonthDateTimeField(BasicChronology basicChronology, DurationField durationField) {
        super(DateTimeFieldType.dayOfMonth(), durationField);
        this.f17937 = basicChronology;
    }

    public int get(long j) {
        return this.f17937.m22812(j);
    }

    public int getMaximumValue() {
        return this.f17937.m22807();
    }

    public int getMaximumValue(long j) {
        return this.f17937.m22793(j);
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        if (!readablePartial.isSupported(DateTimeFieldType.monthOfYear())) {
            return getMaximumValue();
        }
        int i = readablePartial.get(DateTimeFieldType.monthOfYear());
        if (!readablePartial.isSupported(DateTimeFieldType.year())) {
            return this.f17937.m22784(i);
        }
        return this.f17937.m22803(readablePartial.get(DateTimeFieldType.year()), i);
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        int size = readablePartial.size();
        for (int i = 0; i < size; i++) {
            if (readablePartial.getFieldType(i) == DateTimeFieldType.monthOfYear()) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < size; i3++) {
                    if (readablePartial.getFieldType(i3) == DateTimeFieldType.year()) {
                        return this.f17937.m22803(iArr[i3], i2);
                    }
                }
                return this.f17937.m22784(i2);
            }
        }
        return getMaximumValue();
    }

    public int getMinimumValue() {
        return 1;
    }

    public DurationField getRangeDurationField() {
        return this.f17937.months();
    }

    public boolean isLeap(long j) {
        return this.f17937.m22796(j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22824(long j, int i) {
        return this.f17937.m22800(j, i);
    }
}
