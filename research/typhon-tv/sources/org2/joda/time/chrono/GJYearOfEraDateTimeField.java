package org2.joda.time.chrono;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.ReadablePartial;
import org2.joda.time.field.DecoratedDateTimeField;
import org2.joda.time.field.FieldUtils;

final class GJYearOfEraDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f17990;

    GJYearOfEraDateTimeField(DateTimeField dateTimeField, BasicChronology basicChronology) {
        super(dateTimeField, DateTimeFieldType.yearOfEra());
        this.f17990 = basicChronology;
    }

    public long add(long j, int i) {
        return m23024().add(j, i);
    }

    public long add(long j, long j2) {
        return m23024().add(j, j2);
    }

    public long addWrapField(long j, int i) {
        return m23024().addWrapField(j, i);
    }

    public int[] addWrapField(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        return m23024().addWrapField(readablePartial, i, iArr, i2);
    }

    public int get(long j) {
        int i = m23024().get(j);
        return i <= 0 ? 1 - i : i;
    }

    public int getDifference(long j, long j2) {
        return m23024().getDifference(j, j2);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return m23024().getDifferenceAsLong(j, j2);
    }

    public int getMaximumValue() {
        return m23024().getMaximumValue();
    }

    public int getMinimumValue() {
        return 1;
    }

    public DurationField getRangeDurationField() {
        return this.f17990.eras();
    }

    public long remainder(long j) {
        return m23024().remainder(j);
    }

    public long roundCeiling(long j) {
        return m23024().roundCeiling(j);
    }

    public long roundFloor(long j) {
        return m23024().roundFloor(j);
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, 1, getMaximumValue());
        if (this.f17990.m22817(j) <= 0) {
            i = 1 - i;
        }
        return super.set(j, i);
    }
}
