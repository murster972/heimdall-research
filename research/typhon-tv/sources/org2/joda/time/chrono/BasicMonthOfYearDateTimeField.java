package org2.joda.time.chrono;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DurationField;
import org2.joda.time.ReadablePartial;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.field.ImpreciseDateTimeField;

class BasicMonthOfYearDateTimeField extends ImpreciseDateTimeField {

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f17943;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f17944 = this.f17945.m22787();

    /* renamed from: 龘  reason: contains not printable characters */
    private final BasicChronology f17945;

    BasicMonthOfYearDateTimeField(BasicChronology basicChronology, int i) {
        super(DateTimeFieldType.monthOfYear(), basicChronology.m22795());
        this.f17945 = basicChronology;
        this.f17943 = i;
    }

    public long add(long j, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (i == 0) {
            return j;
        }
        long r4 = (long) this.f17945.m22791(j);
        int r2 = this.f17945.m22817(j);
        int r3 = this.f17945.m22818(j, r2);
        int i7 = (r3 - 1) + i;
        if (r3 <= 0 || i7 >= 0) {
            i2 = r2;
        } else {
            if (Math.signum((float) (this.f17944 + i)) == Math.signum((float) i)) {
                i5 = r2 - 1;
                i6 = this.f17944 + i;
            } else {
                i5 = r2 + 1;
                i6 = i - this.f17944;
            }
            int i8 = i6 + (r3 - 1);
            i2 = i5;
            i7 = i8;
        }
        if (i7 >= 0) {
            i3 = i2 + (i7 / this.f17944);
            i4 = (i7 % this.f17944) + 1;
        } else {
            i3 = (i2 + (i7 / this.f17944)) - 1;
            int abs = Math.abs(i7) % this.f17944;
            if (abs == 0) {
                abs = this.f17944;
            }
            i4 = (this.f17944 - abs) + 1;
            if (i4 == 1) {
                i3++;
            }
        }
        int r32 = this.f17945.m22819(j, r2, r3);
        int r22 = this.f17945.m22803(i3, i4);
        if (r32 <= r22) {
            r22 = r32;
        }
        return this.f17945.m22821(i3, i4, r22) + r4;
    }

    public long add(long j, long j2) {
        long j3;
        long j4;
        int i = (int) j2;
        if (((long) i) == j2) {
            return add(j, i);
        }
        long r4 = (long) this.f17945.m22791(j);
        int r6 = this.f17945.m22817(j);
        int r7 = this.f17945.m22818(j, r6);
        long j5 = ((long) (r7 - 1)) + j2;
        if (j5 >= 0) {
            j3 = ((long) r6) + (j5 / ((long) this.f17944));
            j4 = (j5 % ((long) this.f17944)) + 1;
        } else {
            j3 = (((long) r6) + (j5 / ((long) this.f17944))) - 1;
            int abs = (int) (Math.abs(j5) % ((long) this.f17944));
            if (abs == 0) {
                abs = this.f17944;
            }
            j4 = (long) ((this.f17944 - abs) + 1);
            if (j4 == 1) {
                j3++;
            }
        }
        if (j3 < ((long) this.f17945.m22798()) || j3 > ((long) this.f17945.m22783())) {
            throw new IllegalArgumentException("Magnitude of add amount is too large: " + j2);
        }
        int i2 = (int) j3;
        int i3 = (int) j4;
        int r1 = this.f17945.m22819(j, r6, r7);
        int r0 = this.f17945.m22803(i2, i3);
        if (r1 <= r0) {
            r0 = r1;
        }
        return this.f17945.m22821(i2, i3, r0) + r4;
    }

    public int[] add(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        if (i2 == 0) {
            return iArr;
        }
        if (readablePartial.size() > 0 && readablePartial.getFieldType(0).equals(DateTimeFieldType.monthOfYear()) && i == 0) {
            return set(readablePartial, 0, iArr, ((((iArr[0] - 1) + (i2 % 12)) + 12) % 12) + 1);
        }
        if (!DateTimeUtils.m22722(readablePartial)) {
            return super.add(readablePartial, i, iArr, i2);
        }
        long j = 0;
        int size = readablePartial.size();
        for (int i3 = 0; i3 < size; i3++) {
            j = readablePartial.getFieldType(i3).getField(this.f17945).set(j, iArr[i3]);
        }
        return this.f17945.get(readablePartial, add(j, i2));
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m23033(get(j), i, 1, this.f17944));
    }

    public int get(long j) {
        return this.f17945.m22804(j);
    }

    public long getDifferenceAsLong(long j, long j2) {
        if (j < j2) {
            return (long) (-getDifference(j2, j));
        }
        int r2 = this.f17945.m22817(j);
        int r3 = this.f17945.m22818(j, r2);
        int r4 = this.f17945.m22817(j2);
        int r5 = this.f17945.m22818(j2, r4);
        long j3 = ((((long) (r2 - r4)) * ((long) this.f17944)) + ((long) r3)) - ((long) r5);
        int r6 = this.f17945.m22819(j, r2, r3);
        if (r6 == this.f17945.m22803(r2, r3) && this.f17945.m22819(j2, r4, r5) > r6) {
            j2 = this.f17945.dayOfMonth().set(j2, r6);
        }
        return j - this.f17945.m22820(r2, r3) < j2 - this.f17945.m22820(r4, r5) ? j3 - 1 : j3;
    }

    public int getLeapAmount(long j) {
        return isLeap(j) ? 1 : 0;
    }

    public DurationField getLeapDurationField() {
        return this.f17945.days();
    }

    public int getMaximumValue() {
        return this.f17944;
    }

    public int getMinimumValue() {
        return 1;
    }

    public DurationField getRangeDurationField() {
        return this.f17945.years();
    }

    public boolean isLeap(long j) {
        int r1 = this.f17945.m22817(j);
        return this.f17945.m22801(r1) && this.f17945.m22818(j, r1) == this.f17943;
    }

    public boolean isLenient() {
        return false;
    }

    public long remainder(long j) {
        return j - roundFloor(j);
    }

    public long roundFloor(long j) {
        int r0 = this.f17945.m22817(j);
        return this.f17945.m22820(r0, this.f17945.m22818(j, r0));
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, 1, this.f17944);
        int r2 = this.f17945.m22817(j);
        int r1 = this.f17945.m22805(j, r2);
        int r0 = this.f17945.m22803(r2, i);
        if (r1 <= r0) {
            r0 = r1;
        }
        return this.f17945.m22821(r2, i, r0) + ((long) this.f17945.m22791(j));
    }
}
