package org2.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeZone;
import org2.joda.time.chrono.AssembledChronology;

public final class GregorianChronology extends BasicGJChronology {
    private static final long serialVersionUID = -861407383323710522L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, GregorianChronology[]> f17991 = new ConcurrentHashMap<>();

    /* renamed from: 龘  reason: contains not printable characters */
    private static final GregorianChronology f17992 = getInstance(DateTimeZone.UTC);

    private GregorianChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    public static GregorianChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), 4);
    }

    public static GregorianChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, 4);
    }

    public static GregorianChronology getInstance(DateTimeZone dateTimeZone, int i) {
        GregorianChronology[] gregorianChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        GregorianChronology[] gregorianChronologyArr2 = f17991.get(dateTimeZone);
        if (gregorianChronologyArr2 == null) {
            gregorianChronologyArr = new GregorianChronology[7];
            GregorianChronology[] putIfAbsent = f17991.putIfAbsent(dateTimeZone, gregorianChronologyArr);
            if (putIfAbsent != null) {
                gregorianChronologyArr = putIfAbsent;
            }
        } else {
            gregorianChronologyArr = gregorianChronologyArr2;
        }
        try {
            GregorianChronology gregorianChronology = gregorianChronologyArr[i - 1];
            if (gregorianChronology == null) {
                synchronized (gregorianChronologyArr) {
                    gregorianChronology = gregorianChronologyArr[i - 1];
                    if (gregorianChronology == null) {
                        gregorianChronology = dateTimeZone == DateTimeZone.UTC ? new GregorianChronology((Chronology) null, (Object) null, i) : new GregorianChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, i), dateTimeZone), (Object) null, i);
                        gregorianChronologyArr[i - 1] = gregorianChronology;
                    }
                }
            }
            return gregorianChronology;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
    }

    public static GregorianChronology getInstanceUTC() {
        return f17992;
    }

    private Object readResolve() {
        Chronology r1 = m22774();
        int minimumDaysInFirstWeek = getMinimumDaysInFirstWeek();
        if (minimumDaysInFirstWeek == 0) {
            minimumDaysInFirstWeek = 4;
        }
        return r1 == null ? getInstance(DateTimeZone.UTC, minimumDaysInFirstWeek) : getInstance(r1.getZone(), minimumDaysInFirstWeek);
    }

    public Chronology withUTC() {
        return f17992;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22897() {
        return 292278993;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m22898(int i) {
        int i2;
        int i3 = i / 100;
        if (i < 0) {
            i2 = (((i3 + 3) >> 2) + (((i + 3) >> 2) - i3)) - 1;
        } else {
            i2 = (i3 >> 2) + ((i >> 2) - i3);
            if (m22904(i)) {
                i2--;
            }
        }
        return (((long) (i2 - 719527)) + (((long) i) * 365)) * 86400000;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public long m22899() {
        return 31556952000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public long m22900() {
        return 15778476000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public long m22901() {
        return 2629746000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m22902() {
        return 31083597720000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22903() {
        return -292275054;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m22904(int i) {
        return (i & 3) == 0 && (i % 100 != 0 || i % 400 == 0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22905(AssembledChronology.Fields fields) {
        if (m22774() == null) {
            super.m22823(fields);
        }
    }
}
