package org2.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTime;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadableDateTime;
import org2.joda.time.chrono.AssembledChronology;
import org2.joda.time.field.SkipDateTimeField;

public final class CopticChronology extends BasicFixedMonthChronology {
    public static final int AM = 1;
    private static final long serialVersionUID = -5972804258688333942L;

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentHashMap<DateTimeZone, CopticChronology[]> f17953 = new ConcurrentHashMap<>();

    /* renamed from: 齉  reason: contains not printable characters */
    private static final CopticChronology f17954 = getInstance(DateTimeZone.UTC);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final DateTimeField f17955 = new BasicSingleEraDateTimeField("AM");

    CopticChronology(Chronology chronology, Object obj, int i) {
        super(chronology, obj, i);
    }

    public static CopticChronology getInstance() {
        return getInstance(DateTimeZone.getDefault(), 4);
    }

    public static CopticChronology getInstance(DateTimeZone dateTimeZone) {
        return getInstance(dateTimeZone, 4);
    }

    public static CopticChronology getInstance(DateTimeZone dateTimeZone, int i) {
        CopticChronology[] copticChronologyArr;
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        CopticChronology[] copticChronologyArr2 = f17953.get(dateTimeZone);
        if (copticChronologyArr2 == null) {
            CopticChronology[] copticChronologyArr3 = new CopticChronology[7];
            CopticChronology[] putIfAbsent = f17953.putIfAbsent(dateTimeZone, copticChronologyArr3);
            copticChronologyArr = putIfAbsent != null ? putIfAbsent : copticChronologyArr3;
        } else {
            copticChronologyArr = copticChronologyArr2;
        }
        try {
            CopticChronology copticChronology = copticChronologyArr[i - 1];
            if (copticChronology == null) {
                synchronized (copticChronologyArr) {
                    copticChronology = copticChronologyArr[i - 1];
                    if (copticChronology == null) {
                        if (dateTimeZone == DateTimeZone.UTC) {
                            CopticChronology copticChronology2 = new CopticChronology((Chronology) null, (Object) null, i);
                            copticChronology = new CopticChronology(LimitChronology.getInstance(copticChronology2, new DateTime(1, 1, 1, 0, 0, 0, 0, copticChronology2), (ReadableDateTime) null), (Object) null, i);
                        } else {
                            copticChronology = new CopticChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, i), dateTimeZone), (Object) null, i);
                        }
                        copticChronologyArr[i - 1] = copticChronology;
                    }
                }
            }
            return copticChronology;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Invalid min days in first week: " + i);
        }
    }

    public static CopticChronology getInstanceUTC() {
        return f17954;
    }

    private Object readResolve() {
        Chronology r1 = m22774();
        int minimumDaysInFirstWeek = getMinimumDaysInFirstWeek();
        if (minimumDaysInFirstWeek == 0) {
            minimumDaysInFirstWeek = 4;
        }
        return r1 == null ? getInstance(DateTimeZone.UTC, minimumDaysInFirstWeek) : getInstance(r1.getZone(), minimumDaysInFirstWeek);
    }

    public Chronology withUTC() {
        return f17954;
    }

    public Chronology withZone(DateTimeZone dateTimeZone) {
        if (dateTimeZone == null) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return dateTimeZone == getZone() ? this : getInstance(dateTimeZone);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public int m22850() {
        return 292272708;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public long m22851(int i) {
        int i2;
        int i3 = i - 1687;
        if (i3 <= 0) {
            i2 = (i3 + 3) >> 2;
        } else {
            i2 = i3 >> 2;
            if (!m22832(i)) {
                i2++;
            }
        }
        return ((((long) i2) + (((long) i3) * 365)) * 86400000) + 21859200000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m22852(long j) {
        return dayOfMonth().get(j) == 6 && monthOfYear().isLeap(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m22853() {
        return 26607895200000L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public int m22854() {
        return -292269337;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m22855(AssembledChronology.Fields fields) {
        if (m22774() == null) {
            super.m22823(fields);
            fields.f17915 = new SkipDateTimeField(this, fields.f17915);
            fields.f17905 = new SkipDateTimeField(this, fields.f17905);
            fields.f17885 = f17955;
            fields.f17912 = new BasicMonthOfYearDateTimeField(this, 13);
            fields.f17895 = fields.f17912.getDurationField();
        }
    }
}
