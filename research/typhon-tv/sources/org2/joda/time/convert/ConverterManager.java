package org2.joda.time.convert;

public final class ConverterManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private static ConverterManager f18016;

    /* renamed from: ʻ  reason: contains not printable characters */
    private ConverterSet f18017 = new ConverterSet(new Converter[]{ReadableIntervalConverter.f18031, StringConverter.f18034, NullConverter.f18028});

    /* renamed from: 连任  reason: contains not printable characters */
    private ConverterSet f18018 = new ConverterSet(new Converter[]{ReadableDurationConverter.f18029, ReadablePeriodConverter.f18033, ReadableIntervalConverter.f18031, StringConverter.f18034, NullConverter.f18028});

    /* renamed from: 靐  reason: contains not printable characters */
    private ConverterSet f18019 = new ConverterSet(new Converter[]{ReadableInstantConverter.f18030, StringConverter.f18034, CalendarConverter.f18015, DateConverter.f18026, LongConverter.f18027, NullConverter.f18028});

    /* renamed from: 麤  reason: contains not printable characters */
    private ConverterSet f18020 = new ConverterSet(new Converter[]{ReadableDurationConverter.f18029, ReadableIntervalConverter.f18031, StringConverter.f18034, LongConverter.f18027, NullConverter.f18028});

    /* renamed from: 齉  reason: contains not printable characters */
    private ConverterSet f18021 = new ConverterSet(new Converter[]{ReadablePartialConverter.f18032, ReadableInstantConverter.f18030, StringConverter.f18034, CalendarConverter.f18015, DateConverter.f18026, LongConverter.f18027, NullConverter.f18028});

    protected ConverterManager() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static ConverterManager m22967() {
        if (f18016 == null) {
            f18016 = new ConverterManager();
        }
        return f18016;
    }

    public String toString() {
        return "ConverterManager[" + this.f18019.m22974() + " instant," + this.f18021.m22974() + " partial," + this.f18020.m22974() + " duration," + this.f18018.m22974() + " period," + this.f18017.m22974() + " interval]";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public IntervalConverter m22968(Object obj) {
        IntervalConverter intervalConverter = (IntervalConverter) this.f18017.m22975(obj == null ? null : obj.getClass());
        if (intervalConverter != null) {
            return intervalConverter;
        }
        throw new IllegalArgumentException("No interval converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PartialConverter m22969(Object obj) {
        PartialConverter partialConverter = (PartialConverter) this.f18021.m22975(obj == null ? null : obj.getClass());
        if (partialConverter != null) {
            return partialConverter;
        }
        throw new IllegalArgumentException("No partial converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public PeriodConverter m22970(Object obj) {
        PeriodConverter periodConverter = (PeriodConverter) this.f18018.m22975(obj == null ? null : obj.getClass());
        if (periodConverter != null) {
            return periodConverter;
        }
        throw new IllegalArgumentException("No period converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DurationConverter m22971(Object obj) {
        DurationConverter durationConverter = (DurationConverter) this.f18020.m22975(obj == null ? null : obj.getClass());
        if (durationConverter != null) {
            return durationConverter;
        }
        throw new IllegalArgumentException("No duration converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public InstantConverter m22972(Object obj) {
        InstantConverter instantConverter = (InstantConverter) this.f18019.m22975(obj == null ? null : obj.getClass());
        if (instantConverter != null) {
            return instantConverter;
        }
        throw new IllegalArgumentException("No instant converter found for type: " + (obj == null ? "null" : obj.getClass().getName()));
    }
}
