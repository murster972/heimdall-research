package org2.joda.time.convert;

import org2.joda.time.Chronology;

class LongConverter extends AbstractConverter implements DurationConverter, InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final LongConverter f18027 = new LongConverter();

    protected LongConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m22985(Object obj) {
        return ((Long) obj).longValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m22986(Object obj, Chronology chronology) {
        return ((Long) obj).longValue();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m22987() {
        return Long.class;
    }
}
