package org2.joda.time.convert;

class ConverterSet {

    /* renamed from: 靐  reason: contains not printable characters */
    private Entry[] f18022 = new Entry[16];

    /* renamed from: 龘  reason: contains not printable characters */
    private final Converter[] f18023;

    static class Entry {

        /* renamed from: 靐  reason: contains not printable characters */
        final Converter f18024;

        /* renamed from: 龘  reason: contains not printable characters */
        final Class<?> f18025;

        Entry(Class<?> cls, Converter converter) {
            this.f18025 = cls;
            this.f18024 = converter;
        }
    }

    ConverterSet(Converter[] converterArr) {
        this.f18023 = converterArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Converter m22973(ConverterSet converterSet, Class<?> cls) {
        Converter[] converterArr = converterSet.f18023;
        int length = converterArr.length;
        int i = length;
        ConverterSet converterSet2 = converterSet;
        while (true) {
            int i2 = i - 1;
            if (i2 >= 0) {
                Converter converter = converterArr[i2];
                Class<?> r7 = converter.m22966();
                if (r7 == cls) {
                    return converter;
                }
                if (r7 == null || (cls != null && !r7.isAssignableFrom(cls))) {
                    converterSet2 = converterSet2.m22976(i2, (Converter[]) null);
                    converterArr = converterSet2.f18023;
                    length = converterArr.length;
                }
                i = i2;
            } else if (cls == null || length == 0) {
                return null;
            } else {
                if (length == 1) {
                    return converterArr[0];
                }
                Converter[] converterArr2 = converterArr;
                ConverterSet converterSet3 = converterSet2;
                int i3 = length;
                while (true) {
                    int i4 = length - 1;
                    if (i4 < 0) {
                        break;
                    }
                    Class<?> r72 = converterArr2[i4].m22966();
                    ConverterSet converterSet4 = converterSet3;
                    int i5 = i4;
                    int i6 = i3;
                    while (true) {
                        i6--;
                        if (i6 < 0) {
                            break;
                        } else if (i6 != i5 && converterArr2[i6].m22966().isAssignableFrom(r72)) {
                            converterSet4 = converterSet4.m22976(i6, (Converter[]) null);
                            converterArr2 = converterSet4.f18023;
                            i3 = converterArr2.length;
                            i5 = i3 - 1;
                        }
                    }
                    length = i5;
                    converterSet3 = converterSet4;
                }
                if (i3 == 1) {
                    return converterArr2[0];
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to find best converter for type \"");
                sb.append(cls.getName());
                sb.append("\" from remaining set: ");
                for (int i7 = 0; i7 < i3; i7++) {
                    Converter converter2 = converterArr2[i7];
                    Class<?> r5 = converter2.m22966();
                    sb.append(converter2.getClass().getName());
                    sb.append('[');
                    sb.append(r5 == null ? null : r5.getName());
                    sb.append("], ");
                }
                throw new IllegalStateException(sb.toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22974() {
        return this.f18023.length;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Converter m22975(Class<?> cls) throws IllegalStateException {
        Entry[] entryArr = this.f18022;
        int length = entryArr.length;
        int hashCode = cls == null ? 0 : cls.hashCode() & (length - 1);
        while (true) {
            Entry entry = entryArr[hashCode];
            if (entry == null) {
                Converter r3 = m22973(this, cls);
                Entry entry2 = new Entry(cls, r3);
                Entry[] entryArr2 = (Entry[]) entryArr.clone();
                entryArr2[hashCode] = entry2;
                for (int i = 0; i < length; i++) {
                    if (entryArr2[i] == null) {
                        this.f18022 = entryArr2;
                        return r3;
                    }
                }
                int i2 = length << 1;
                Entry[] entryArr3 = new Entry[i2];
                for (int i3 = 0; i3 < length; i3++) {
                    Entry entry3 = entryArr2[i3];
                    Class<?> cls2 = entry3.f18025;
                    int hashCode2 = cls2 == null ? 0 : cls2.hashCode() & (i2 - 1);
                    while (entryArr3[hashCode2] != null) {
                        int i4 = hashCode2 + 1;
                        if (i4 >= i2) {
                            i4 = 0;
                        }
                    }
                    entryArr3[hashCode2] = entry3;
                }
                this.f18022 = entryArr3;
                return r3;
            } else if (entry.f18025 == cls) {
                return entry.f18024;
            } else {
                int i5 = hashCode + 1;
                hashCode = i5 >= length ? 0 : i5;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ConverterSet m22976(int i, Converter[] converterArr) {
        int i2;
        Converter[] converterArr2 = this.f18023;
        int length = converterArr2.length;
        if (i >= length) {
            throw new IndexOutOfBoundsException();
        }
        if (converterArr != null) {
            converterArr[0] = converterArr2[i];
        }
        Converter[] converterArr3 = new Converter[(length - 1)];
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            if (i3 != i) {
                i2 = i4 + 1;
                converterArr3[i4] = converterArr2[i3];
            } else {
                i2 = i4;
            }
            i3++;
            i4 = i2;
        }
        return new ConverterSet(converterArr3);
    }
}
