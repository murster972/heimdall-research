package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTime;
import org2.joda.time.Period;
import org2.joda.time.ReadWritableInterval;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadablePartial;
import org2.joda.time.ReadablePeriod;
import org2.joda.time.field.FieldUtils;
import org2.joda.time.format.DateTimeFormatter;
import org2.joda.time.format.ISODateTimeFormat;
import org2.joda.time.format.ISOPeriodFormat;
import org2.joda.time.format.PeriodFormatter;

class StringConverter extends AbstractConverter implements DurationConverter, InstantConverter, IntervalConverter, PartialConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final StringConverter f18034 = new StringConverter();

    protected StringConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m23014(Object obj) {
        long parseLong;
        long j;
        int i = 1;
        String str = (String) obj;
        int length = str.length();
        if (length < 4 || !((str.charAt(0) == 'P' || str.charAt(0) == 'p') && ((str.charAt(1) == 'T' || str.charAt(1) == 't') && (str.charAt(length - 1) == 'S' || str.charAt(length - 1) == 's')))) {
            throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
        }
        String substring = str.substring(2, length - 1);
        boolean z = false;
        int i2 = -1;
        for (int i3 = 0; i3 < substring.length(); i3++) {
            if (substring.charAt(i3) < '0' || substring.charAt(i3) > '9') {
                if (i3 == 0 && substring.charAt(0) == '-') {
                    z = true;
                } else {
                    if (i3 > (z ? 1 : 0) && substring.charAt(i3) == '.' && i2 == -1) {
                        i2 = i3;
                    } else {
                        throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
                    }
                }
            }
        }
        if (!z) {
            i = 0;
        }
        if (i2 > 0) {
            long parseLong2 = Long.parseLong(substring.substring(i, i2));
            String substring2 = substring.substring(i2 + 1);
            if (substring2.length() != 3) {
                substring2 = (substring2 + "000").substring(0, 3);
            }
            long j2 = parseLong2;
            j = (long) Integer.parseInt(substring2);
            parseLong = j2;
        } else if (z) {
            parseLong = Long.parseLong(substring.substring(i, substring.length()));
            j = 0;
        } else {
            parseLong = Long.parseLong(substring);
            j = 0;
        }
        return z ? FieldUtils.m23036(FieldUtils.m23035(-parseLong, 1000), -j) : FieldUtils.m23036(FieldUtils.m23035(parseLong, 1000), j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m23015(Object obj, Chronology chronology) {
        return ISODateTimeFormat.m23270().m23069(chronology).m23065((String) obj);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m23016() {
        return String.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23017(ReadWritableInterval readWritableInterval, Object obj, Chronology chronology) {
        Chronology chronology2;
        long j;
        ReadablePeriod readablePeriod = null;
        String str = (String) obj;
        int indexOf = str.indexOf(47);
        if (indexOf < 0) {
            throw new IllegalArgumentException("Format requires a '/' separator: " + str);
        }
        String substring = str.substring(0, indexOf);
        if (substring.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        String substring2 = str.substring(indexOf + 1);
        if (substring2.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        DateTimeFormatter r6 = ISODateTimeFormat.m23270().m23069(chronology);
        PeriodFormatter r7 = ISOPeriodFormat.m23353();
        long j2 = 0;
        char charAt = substring.charAt(0);
        if (charAt == 'P' || charAt == 'p') {
            readablePeriod = r7.m23378(a_(substring)).m23377(substring);
            chronology2 = null;
        } else {
            DateTime r0 = r6.m23057(substring);
            j2 = r0.getMillis();
            chronology2 = r0.getChronology();
        }
        char charAt2 = substring2.charAt(0);
        if (charAt2 != 'P' && charAt2 != 'p') {
            DateTime r62 = r6.m23057(substring2);
            long millis = r62.getMillis();
            if (chronology2 == null) {
                chronology2 = r62.getChronology();
            }
            if (chronology == null) {
                chronology = chronology2;
            }
            if (readablePeriod != null) {
                j2 = chronology.add(readablePeriod, millis, -1);
                j = millis;
            } else {
                j = millis;
            }
        } else if (readablePeriod != null) {
            throw new IllegalArgumentException("Interval composed of two durations: " + str);
        } else {
            Period r1 = r7.m23378(a_(substring2)).m23377(substring2);
            if (chronology == null) {
                chronology = chronology2;
            }
            j = chronology.add((ReadablePeriod) r1, j2, 1);
        }
        readWritableInterval.setInterval(j2, j);
        readWritableInterval.setChronology(chronology);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23018(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        String str = (String) obj;
        PeriodFormatter r0 = ISOPeriodFormat.m23353();
        readWritablePeriod.clear();
        int r1 = r0.m23375(readWritablePeriod, str, 0);
        if (r1 < str.length()) {
            if (r1 < 0) {
                r0.m23378(readWritablePeriod.getPeriodType()).m23373(str);
            }
            throw new IllegalArgumentException("Invalid format: \"" + str + '\"');
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m23019(ReadablePartial readablePartial, Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter) {
        if (dateTimeFormatter.m23056() != null) {
            chronology = chronology.withZone(dateTimeFormatter.m23056());
        }
        return chronology.get(readablePartial, dateTimeFormatter.m23069(chronology).m23065((String) obj));
    }
}
