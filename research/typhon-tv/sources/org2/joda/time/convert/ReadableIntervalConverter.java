package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.ReadWritableInterval;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadableInterval;

class ReadableIntervalConverter extends AbstractConverter implements DurationConverter, IntervalConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadableIntervalConverter f18031 = new ReadableIntervalConverter();

    protected ReadableIntervalConverter() {
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23003(Object obj, Chronology chronology) {
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m23004(Object obj) {
        return ((ReadableInterval) obj).toDurationMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m23005() {
        return ReadableInterval.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23006(ReadWritableInterval readWritableInterval, Object obj, Chronology chronology) {
        ReadableInterval readableInterval = (ReadableInterval) obj;
        readWritableInterval.setInterval(readableInterval);
        if (chronology != null) {
            readWritableInterval.setChronology(chronology);
        } else {
            readWritableInterval.setChronology(readableInterval.getChronology());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23007(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        ReadableInterval readableInterval = (ReadableInterval) obj;
        int[] iArr = (chronology != null ? chronology : DateTimeUtils.m22718(readableInterval)).get(readWritablePeriod, readableInterval.getStartMillis(), readableInterval.getEndMillis());
        for (int i = 0; i < iArr.length; i++) {
            readWritablePeriod.setValue(i, iArr[i]);
        }
    }
}
