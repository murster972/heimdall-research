package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadableInstant;
import org2.joda.time.chrono.ISOChronology;

class ReadableInstantConverter extends AbstractConverter implements InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadableInstantConverter f18030 = new ReadableInstantConverter();

    protected ReadableInstantConverter() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m22999(Object obj, Chronology chronology) {
        return chronology == null ? DateTimeUtils.m22716(((ReadableInstant) obj).getChronology()) : chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m23000(Object obj, Chronology chronology) {
        return ((ReadableInstant) obj).getMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m23001() {
        return ReadableInstant.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m23002(Object obj, DateTimeZone dateTimeZone) {
        Chronology chronology = ((ReadableInstant) obj).getChronology();
        if (chronology == null) {
            return ISOChronology.getInstance(dateTimeZone);
        }
        if (chronology.getZone() == dateTimeZone) {
            return chronology;
        }
        Chronology withZone = chronology.withZone(dateTimeZone);
        return withZone == null ? ISOChronology.getInstance(dateTimeZone) : withZone;
    }
}
