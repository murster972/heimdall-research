package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadablePartial;

class ReadablePartialConverter extends AbstractConverter implements PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadablePartialConverter f18032 = new ReadablePartialConverter();

    protected ReadablePartialConverter() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m23008(Object obj, Chronology chronology) {
        return chronology == null ? DateTimeUtils.m22716(((ReadablePartial) obj).getChronology()) : chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m23009() {
        return ReadablePartial.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m23010(Object obj, DateTimeZone dateTimeZone) {
        return m23008(obj, (Chronology) null).withZone(dateTimeZone);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m23011(ReadablePartial readablePartial, Object obj, Chronology chronology) {
        ReadablePartial readablePartial2 = (ReadablePartial) obj;
        int size = readablePartial.size();
        int[] iArr = new int[size];
        for (int i = 0; i < size; i++) {
            iArr[i] = readablePartial2.get(readablePartial.getFieldType(i));
        }
        chronology.validate(readablePartial, iArr);
        return iArr;
    }
}
