package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadableDuration;
import org2.joda.time.ReadablePeriod;

class ReadableDurationConverter extends AbstractConverter implements DurationConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadableDurationConverter f18029 = new ReadableDurationConverter();

    protected ReadableDurationConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m22996(Object obj) {
        return ((ReadableDuration) obj).getMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m22997() {
        return ReadableDuration.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22998(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        int[] iArr = DateTimeUtils.m22716(chronology).get((ReadablePeriod) readWritablePeriod, ((ReadableDuration) obj).getMillis());
        for (int i = 0; i < iArr.length; i++) {
            readWritablePeriod.setValue(i, iArr[i]);
        }
    }
}
