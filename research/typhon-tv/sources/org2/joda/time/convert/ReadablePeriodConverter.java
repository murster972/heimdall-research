package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.PeriodType;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadablePeriod;

class ReadablePeriodConverter extends AbstractConverter implements PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final ReadablePeriodConverter f18033 = new ReadablePeriodConverter();

    protected ReadablePeriodConverter() {
    }

    public PeriodType a_(Object obj) {
        return ((ReadablePeriod) obj).getPeriodType();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m23012() {
        return ReadablePeriod.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23013(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        readWritablePeriod.setPeriod((ReadablePeriod) obj);
    }
}
