package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadablePartial;
import org2.joda.time.format.DateTimeFormatter;

public interface PartialConverter extends Converter {
    /* renamed from: 靐  reason: contains not printable characters */
    Chronology m22992(Object obj, Chronology chronology);

    /* renamed from: 龘  reason: contains not printable characters */
    Chronology m22993(Object obj, DateTimeZone dateTimeZone);

    /* renamed from: 龘  reason: contains not printable characters */
    int[] m22994(ReadablePartial readablePartial, Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter);
}
