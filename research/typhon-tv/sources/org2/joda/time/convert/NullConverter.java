package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.ReadWritableInterval;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadablePeriod;

class NullConverter extends AbstractConverter implements DurationConverter, InstantConverter, IntervalConverter, PartialConverter, PeriodConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final NullConverter f18028 = new NullConverter();

    protected NullConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m22988(Object obj) {
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m22989() {
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22990(ReadWritableInterval readWritableInterval, Object obj, Chronology chronology) {
        readWritableInterval.setChronology(chronology);
        long r0 = DateTimeUtils.m22712();
        readWritableInterval.setInterval(r0, r0);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22991(ReadWritablePeriod readWritablePeriod, Object obj, Chronology chronology) {
        readWritablePeriod.setPeriod((ReadablePeriod) null);
    }
}
