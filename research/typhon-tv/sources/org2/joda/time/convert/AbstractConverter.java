package org2.joda.time.convert;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.PeriodType;
import org2.joda.time.ReadablePartial;
import org2.joda.time.chrono.ISOChronology;
import org2.joda.time.format.DateTimeFormatter;

public abstract class AbstractConverter implements Converter {
    protected AbstractConverter() {
    }

    public PeriodType a_(Object obj) {
        return PeriodType.standard();
    }

    public String toString() {
        return "Converter[" + (m22966() == null ? "null" : m22966().getName()) + "]";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m22956(Object obj, Chronology chronology) {
        return DateTimeUtils.m22716(chronology);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m22957(Object obj, Chronology chronology) {
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m22958(Object obj, Chronology chronology) {
        return DateTimeUtils.m22712();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m22959(Object obj, DateTimeZone dateTimeZone) {
        return ISOChronology.getInstance(dateTimeZone);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m22960(ReadablePartial readablePartial, Object obj, Chronology chronology) {
        return chronology.get(readablePartial, m22958(obj, chronology));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m22961(ReadablePartial readablePartial, Object obj, Chronology chronology, DateTimeFormatter dateTimeFormatter) {
        return m22960(readablePartial, obj, chronology);
    }
}
