package org2.joda.time.convert;

import java.util.Date;
import org2.joda.time.Chronology;

final class DateConverter extends AbstractConverter implements InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final DateConverter f18026 = new DateConverter();

    protected DateConverter() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m22977(Object obj, Chronology chronology) {
        return ((Date) obj).getTime();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m22978() {
        return Date.class;
    }
}
