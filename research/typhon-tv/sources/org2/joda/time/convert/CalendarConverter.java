package org2.joda.time.convert;

import java.util.Calendar;
import java.util.GregorianCalendar;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeZone;
import org2.joda.time.chrono.BuddhistChronology;
import org2.joda.time.chrono.GJChronology;
import org2.joda.time.chrono.GregorianChronology;
import org2.joda.time.chrono.ISOChronology;
import org2.joda.time.chrono.JulianChronology;

final class CalendarConverter extends AbstractConverter implements InstantConverter, PartialConverter {

    /* renamed from: 龘  reason: contains not printable characters */
    static final CalendarConverter f18015 = new CalendarConverter();

    protected CalendarConverter() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m22962(Object obj, Chronology chronology) {
        DateTimeZone dateTimeZone;
        if (chronology != null) {
            return chronology;
        }
        Calendar calendar = (Calendar) obj;
        try {
            dateTimeZone = DateTimeZone.forTimeZone(calendar.getTimeZone());
        } catch (IllegalArgumentException e) {
            dateTimeZone = DateTimeZone.getDefault();
        }
        return m22965((Object) calendar, dateTimeZone);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m22963(Object obj, Chronology chronology) {
        return ((Calendar) obj).getTime().getTime();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Class<?> m22964() {
        return Calendar.class;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m22965(Object obj, DateTimeZone dateTimeZone) {
        if (obj.getClass().getName().endsWith(".BuddhistCalendar")) {
            return BuddhistChronology.getInstance(dateTimeZone);
        }
        if (!(obj instanceof GregorianCalendar)) {
            return ISOChronology.getInstance(dateTimeZone);
        }
        long time = ((GregorianCalendar) obj).getGregorianChange().getTime();
        return time == Long.MIN_VALUE ? GregorianChronology.getInstance(dateTimeZone) : time == Long.MAX_VALUE ? JulianChronology.getInstance(dateTimeZone) : GJChronology.getInstance(dateTimeZone, time, 4);
    }
}
