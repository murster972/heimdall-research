package org2.joda.time;

import java.io.Serializable;

public abstract class DateTimeFieldType implements Serializable {
    private static final long serialVersionUID = -42615285973990L;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final DateTimeFieldType f17767 = new StandardDateTimeFieldType("dayOfYear", (byte) 6, DurationFieldType.days(), DurationFieldType.years());
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final DateTimeFieldType f17768 = new StandardDateTimeFieldType("monthOfYear", (byte) 7, DurationFieldType.months(), DurationFieldType.years());
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final DateTimeFieldType f17769 = new StandardDateTimeFieldType("dayOfMonth", (byte) 8, DurationFieldType.days(), DurationFieldType.months());
    /* access modifiers changed from: private */

    /* renamed from: ʾ  reason: contains not printable characters */
    public static final DateTimeFieldType f17770 = new StandardDateTimeFieldType("halfdayOfDay", (byte) 13, DurationFieldType.halfdays(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public static final DateTimeFieldType f17771 = new StandardDateTimeFieldType("hourOfHalfday", (byte) 14, DurationFieldType.hours(), DurationFieldType.halfdays());
    /* access modifiers changed from: private */

    /* renamed from: ˆ  reason: contains not printable characters */
    public static final DateTimeFieldType f17772 = new StandardDateTimeFieldType("secondOfDay", (byte) 20, DurationFieldType.seconds(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final DateTimeFieldType f17773 = new StandardDateTimeFieldType("dayOfWeek", (byte) 12, DurationFieldType.days(), DurationFieldType.weeks());
    /* access modifiers changed from: private */

    /* renamed from: ˉ  reason: contains not printable characters */
    public static final DateTimeFieldType f17774 = new StandardDateTimeFieldType("secondOfMinute", (byte) 21, DurationFieldType.seconds(), DurationFieldType.minutes());
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public static final DateTimeFieldType f17775 = new StandardDateTimeFieldType("hourOfDay", (byte) 17, DurationFieldType.hours(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final DateTimeFieldType f17776 = new StandardDateTimeFieldType("minuteOfDay", (byte) 18, DurationFieldType.minutes(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final DateTimeFieldType f17777 = new StandardDateTimeFieldType("minuteOfHour", (byte) 19, DurationFieldType.minutes(), DurationFieldType.hours());
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public static final DateTimeFieldType f17778 = new StandardDateTimeFieldType("millisOfDay", (byte) 22, DurationFieldType.millis(), DurationFieldType.days());
    /* access modifiers changed from: private */

    /* renamed from: ˑ  reason: contains not printable characters */
    public static final DateTimeFieldType f17779 = new StandardDateTimeFieldType("weekyearOfCentury", (byte) 9, DurationFieldType.weekyears(), DurationFieldType.centuries());
    /* access modifiers changed from: private */

    /* renamed from: י  reason: contains not printable characters */
    public static final DateTimeFieldType f17780 = new StandardDateTimeFieldType("millisOfSecond", (byte) 23, DurationFieldType.millis(), DurationFieldType.seconds());
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public static final DateTimeFieldType f17781 = new StandardDateTimeFieldType("weekyear", (byte) 10, DurationFieldType.weekyears(), (DurationFieldType) null);
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static final DateTimeFieldType f17782 = new StandardDateTimeFieldType("weekOfWeekyear", (byte) 11, DurationFieldType.weeks(), DurationFieldType.weekyears());
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static final DateTimeFieldType f17783 = new StandardDateTimeFieldType("year", (byte) 5, DurationFieldType.years(), (DurationFieldType) null);
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static final DateTimeFieldType f17784 = new StandardDateTimeFieldType("yearOfEra", (byte) 2, DurationFieldType.years(), DurationFieldType.eras());
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static final DateTimeFieldType f17785 = new StandardDateTimeFieldType("yearOfCentury", (byte) 4, DurationFieldType.years(), DurationFieldType.centuries());
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final DateTimeFieldType f17786 = new StandardDateTimeFieldType("centuryOfEra", (byte) 3, DurationFieldType.centuries(), DurationFieldType.eras());
    /* access modifiers changed from: private */

    /* renamed from: 龘  reason: contains not printable characters */
    public static final DateTimeFieldType f17787 = new StandardDateTimeFieldType("era", (byte) 1, DurationFieldType.eras(), (DurationFieldType) null);
    /* access modifiers changed from: private */

    /* renamed from: ﹶ  reason: contains not printable characters */
    public static final DateTimeFieldType f17788 = new StandardDateTimeFieldType("clockhourOfHalfday", (byte) 15, DurationFieldType.hours(), DurationFieldType.halfdays());
    /* access modifiers changed from: private */

    /* renamed from: ﾞ  reason: contains not printable characters */
    public static final DateTimeFieldType f17789 = new StandardDateTimeFieldType("clockhourOfDay", (byte) 16, DurationFieldType.hours(), DurationFieldType.days());
    private final String iName;

    private static class StandardDateTimeFieldType extends DateTimeFieldType {
        private static final long serialVersionUID = -9937958251642L;
        private final byte iOrdinal;

        /* renamed from: 靐  reason: contains not printable characters */
        private final transient DurationFieldType f17790;

        /* renamed from: 龘  reason: contains not printable characters */
        private final transient DurationFieldType f17791;

        StandardDateTimeFieldType(String str, byte b, DurationFieldType durationFieldType, DurationFieldType durationFieldType2) {
            super(str);
            this.iOrdinal = b;
            this.f17791 = durationFieldType;
            this.f17790 = durationFieldType2;
        }

        private Object readResolve() {
            switch (this.iOrdinal) {
                case 1:
                    return DateTimeFieldType.f17787;
                case 2:
                    return DateTimeFieldType.f17784;
                case 3:
                    return DateTimeFieldType.f17786;
                case 4:
                    return DateTimeFieldType.f17785;
                case 5:
                    return DateTimeFieldType.f17783;
                case 6:
                    return DateTimeFieldType.f17767;
                case 7:
                    return DateTimeFieldType.f17768;
                case 8:
                    return DateTimeFieldType.f17769;
                case 9:
                    return DateTimeFieldType.f17779;
                case 10:
                    return DateTimeFieldType.f17781;
                case 11:
                    return DateTimeFieldType.f17782;
                case 12:
                    return DateTimeFieldType.f17773;
                case 13:
                    return DateTimeFieldType.f17770;
                case 14:
                    return DateTimeFieldType.f17771;
                case 15:
                    return DateTimeFieldType.f17788;
                case 16:
                    return DateTimeFieldType.f17789;
                case 17:
                    return DateTimeFieldType.f17775;
                case 18:
                    return DateTimeFieldType.f17776;
                case 19:
                    return DateTimeFieldType.f17777;
                case 20:
                    return DateTimeFieldType.f17772;
                case 21:
                    return DateTimeFieldType.f17774;
                case 22:
                    return DateTimeFieldType.f17778;
                case 23:
                    return DateTimeFieldType.f17780;
                default:
                    return this;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof StandardDateTimeFieldType) {
                return this.iOrdinal == ((StandardDateTimeFieldType) obj).iOrdinal;
            }
            return false;
        }

        public DurationFieldType getDurationType() {
            return this.f17791;
        }

        public DateTimeField getField(Chronology chronology) {
            Chronology r0 = DateTimeUtils.m22716(chronology);
            switch (this.iOrdinal) {
                case 1:
                    return r0.era();
                case 2:
                    return r0.yearOfEra();
                case 3:
                    return r0.centuryOfEra();
                case 4:
                    return r0.yearOfCentury();
                case 5:
                    return r0.year();
                case 6:
                    return r0.dayOfYear();
                case 7:
                    return r0.monthOfYear();
                case 8:
                    return r0.dayOfMonth();
                case 9:
                    return r0.weekyearOfCentury();
                case 10:
                    return r0.weekyear();
                case 11:
                    return r0.weekOfWeekyear();
                case 12:
                    return r0.dayOfWeek();
                case 13:
                    return r0.halfdayOfDay();
                case 14:
                    return r0.hourOfHalfday();
                case 15:
                    return r0.clockhourOfHalfday();
                case 16:
                    return r0.clockhourOfDay();
                case 17:
                    return r0.hourOfDay();
                case 18:
                    return r0.minuteOfDay();
                case 19:
                    return r0.minuteOfHour();
                case 20:
                    return r0.secondOfDay();
                case 21:
                    return r0.secondOfMinute();
                case 22:
                    return r0.millisOfDay();
                case 23:
                    return r0.millisOfSecond();
                default:
                    throw new InternalError();
            }
        }

        public DurationFieldType getRangeDurationType() {
            return this.f17790;
        }

        public int hashCode() {
            return 1 << this.iOrdinal;
        }
    }

    protected DateTimeFieldType(String str) {
        this.iName = str;
    }

    public static DateTimeFieldType centuryOfEra() {
        return f17786;
    }

    public static DateTimeFieldType clockhourOfDay() {
        return f17789;
    }

    public static DateTimeFieldType clockhourOfHalfday() {
        return f17788;
    }

    public static DateTimeFieldType dayOfMonth() {
        return f17769;
    }

    public static DateTimeFieldType dayOfWeek() {
        return f17773;
    }

    public static DateTimeFieldType dayOfYear() {
        return f17767;
    }

    public static DateTimeFieldType era() {
        return f17787;
    }

    public static DateTimeFieldType halfdayOfDay() {
        return f17770;
    }

    public static DateTimeFieldType hourOfDay() {
        return f17775;
    }

    public static DateTimeFieldType hourOfHalfday() {
        return f17771;
    }

    public static DateTimeFieldType millisOfDay() {
        return f17778;
    }

    public static DateTimeFieldType millisOfSecond() {
        return f17780;
    }

    public static DateTimeFieldType minuteOfDay() {
        return f17776;
    }

    public static DateTimeFieldType minuteOfHour() {
        return f17777;
    }

    public static DateTimeFieldType monthOfYear() {
        return f17768;
    }

    public static DateTimeFieldType secondOfDay() {
        return f17772;
    }

    public static DateTimeFieldType secondOfMinute() {
        return f17774;
    }

    public static DateTimeFieldType weekOfWeekyear() {
        return f17782;
    }

    public static DateTimeFieldType weekyear() {
        return f17781;
    }

    public static DateTimeFieldType weekyearOfCentury() {
        return f17779;
    }

    public static DateTimeFieldType year() {
        return f17783;
    }

    public static DateTimeFieldType yearOfCentury() {
        return f17785;
    }

    public static DateTimeFieldType yearOfEra() {
        return f17784;
    }

    public abstract DurationFieldType getDurationType();

    public abstract DateTimeField getField(Chronology chronology);

    public String getName() {
        return this.iName;
    }

    public abstract DurationFieldType getRangeDurationType();

    public boolean isSupported(Chronology chronology) {
        return getField(chronology).isSupported();
    }

    public String toString() {
        return getName();
    }
}
