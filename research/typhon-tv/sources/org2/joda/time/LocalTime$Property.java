package org2.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Locale;
import org2.joda.time.field.AbstractReadableInstantFieldProperty;

public final class LocalTime$Property extends AbstractReadableInstantFieldProperty {
    private static final long serialVersionUID = -325842547277223L;

    /* renamed from: 靐  reason: contains not printable characters */
    private transient DateTimeField f17814;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient LocalTime f17815;

    LocalTime$Property(LocalTime localTime, DateTimeField dateTimeField) {
        this.f17815 = localTime;
        this.f17814 = dateTimeField;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        this.f17815 = (LocalTime) objectInputStream.readObject();
        this.f17814 = ((DateTimeFieldType) objectInputStream.readObject()).getField(this.f17815.getChronology());
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.f17815);
        objectOutputStream.writeObject(this.f17814.getType());
    }

    public LocalTime addCopy(int i) {
        return this.f17815.龘(this.f17814.add(this.f17815.龘(), i));
    }

    public LocalTime addCopy(long j) {
        return this.f17815.龘(this.f17814.add(this.f17815.龘(), j));
    }

    public LocalTime addNoWrapToCopy(int i) {
        long add = this.f17814.add(this.f17815.龘(), i);
        if (((long) this.f17815.getChronology().millisOfDay().get(add)) == add) {
            return this.f17815.龘(add);
        }
        throw new IllegalArgumentException("The addition exceeded the boundaries of LocalTime");
    }

    public LocalTime addWrapFieldToCopy(int i) {
        return this.f17815.龘(this.f17814.addWrapField(this.f17815.龘(), i));
    }

    public DateTimeField getField() {
        return this.f17814;
    }

    public LocalTime getLocalTime() {
        return this.f17815;
    }

    public LocalTime roundCeilingCopy() {
        return this.f17815.龘(this.f17814.roundCeiling(this.f17815.龘()));
    }

    public LocalTime roundFloorCopy() {
        return this.f17815.龘(this.f17814.roundFloor(this.f17815.龘()));
    }

    public LocalTime roundHalfCeilingCopy() {
        return this.f17815.龘(this.f17814.roundHalfCeiling(this.f17815.龘()));
    }

    public LocalTime roundHalfEvenCopy() {
        return this.f17815.龘(this.f17814.roundHalfEven(this.f17815.龘()));
    }

    public LocalTime roundHalfFloorCopy() {
        return this.f17815.龘(this.f17814.roundHalfFloor(this.f17815.龘()));
    }

    public LocalTime setCopy(int i) {
        return this.f17815.龘(this.f17814.set(this.f17815.龘(), i));
    }

    public LocalTime setCopy(String str) {
        return setCopy(str, (Locale) null);
    }

    public LocalTime setCopy(String str, Locale locale) {
        return this.f17815.龘(this.f17814.set(this.f17815.龘(), str, locale));
    }

    public LocalTime withMaximumValue() {
        return setCopy(getMaximumValue());
    }

    public LocalTime withMinimumValue() {
        return setCopy(getMinimumValue());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m22734() {
        return this.f17815.getChronology();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22735() {
        return this.f17815.龘();
    }
}
