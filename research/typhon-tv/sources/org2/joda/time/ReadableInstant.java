package org2.joda.time;

public interface ReadableInstant extends Comparable<ReadableInstant> {
    int get(DateTimeFieldType dateTimeFieldType);

    Chronology getChronology();

    long getMillis();

    boolean isBefore(ReadableInstant readableInstant);

    Instant toInstant();
}
