package org2.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadablePartial;

class DateTimePrinterInternalPrinter implements InternalPrinter {

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimePrinter f18128;

    private DateTimePrinterInternalPrinter(DateTimePrinter dateTimePrinter) {
        this.f18128 = dateTimePrinter;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static InternalPrinter m23245(DateTimePrinter dateTimePrinter) {
        if (dateTimePrinter instanceof InternalPrinterDateTimePrinter) {
            return (InternalPrinter) dateTimePrinter;
        }
        if (dateTimePrinter == null) {
            return null;
        }
        return new DateTimePrinterInternalPrinter(dateTimePrinter);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23246() {
        return this.f18128.m23240();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23247(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
        if (appendable instanceof StringBuffer) {
            this.f18128.m23243((StringBuffer) appendable, j, chronology, i, dateTimeZone, locale);
        } else if (appendable instanceof Writer) {
            this.f18128.m23241((Writer) appendable, j, chronology, i, dateTimeZone, locale);
        } else {
            StringBuffer stringBuffer = new StringBuffer(m23246());
            this.f18128.m23243(stringBuffer, j, chronology, i, dateTimeZone, locale);
            appendable.append(stringBuffer);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23248(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        if (appendable instanceof StringBuffer) {
            this.f18128.m23244((StringBuffer) appendable, readablePartial, locale);
        } else if (appendable instanceof Writer) {
            this.f18128.m23242((Writer) appendable, readablePartial, locale);
        } else {
            StringBuffer stringBuffer = new StringBuffer(m23246());
            this.f18128.m23244(stringBuffer, readablePartial, locale);
            appendable.append(stringBuffer);
        }
    }
}
