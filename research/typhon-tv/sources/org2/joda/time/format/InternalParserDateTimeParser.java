package org2.joda.time.format;

class InternalParserDateTimeParser implements DateTimeParser, InternalParser {

    /* renamed from: 龘  reason: contains not printable characters */
    private final InternalParser f18190;

    private InternalParserDateTimeParser(InternalParser internalParser) {
        this.f18190 = internalParser;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static DateTimeParser m23356(InternalParser internalParser) {
        if (internalParser instanceof DateTimeParserInternalParser) {
            return ((DateTimeParserInternalParser) internalParser).m23239();
        }
        if (internalParser instanceof DateTimeParser) {
            return (DateTimeParser) internalParser;
        }
        if (internalParser == null) {
            return null;
        }
        return new InternalParserDateTimeParser(internalParser);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof InternalParserDateTimeParser) {
            return this.f18190.equals(((InternalParserDateTimeParser) obj).f18190);
        }
        return false;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m23357() {
        return this.f18190.m23354();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23358(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
        return this.f18190.m23355(dateTimeParserBucket, charSequence, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23359(DateTimeParserBucket dateTimeParserBucket, String str, int i) {
        return this.f18190.m23355(dateTimeParserBucket, str, i);
    }
}
