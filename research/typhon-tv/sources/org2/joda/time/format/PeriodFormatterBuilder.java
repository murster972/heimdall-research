package org2.joda.time.format;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;
import org2.joda.time.DurationFieldType;
import org2.joda.time.PeriodType;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadablePeriod;

public class PeriodFormatterBuilder {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final ConcurrentMap<String, Pattern> f18196 = new ConcurrentHashMap();

    /* renamed from: ʻ  reason: contains not printable characters */
    private PeriodFieldAffix f18197;

    /* renamed from: ʼ  reason: contains not printable characters */
    private List<Object> f18198;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f18199;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f18200;

    /* renamed from: ٴ  reason: contains not printable characters */
    private FieldFormatter[] f18201;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f18202;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f18203;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f18204;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f18205;

    static class Composite implements PeriodParser, PeriodPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final PeriodParser[] f18206;

        /* renamed from: 龘  reason: contains not printable characters */
        private final PeriodPrinter[] f18207;

        Composite(List<Object> list) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            m23400(list, (List<Object>) arrayList, (List<Object>) arrayList2);
            if (arrayList.size() <= 0) {
                this.f18207 = null;
            } else {
                this.f18207 = (PeriodPrinter[]) arrayList.toArray(new PeriodPrinter[arrayList.size()]);
            }
            if (arrayList2.size() <= 0) {
                this.f18206 = null;
            } else {
                this.f18206 = (PeriodParser[]) arrayList2.toArray(new PeriodParser[arrayList2.size()]);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m23400(List<Object> list, List<Object> list2, List<Object> list3) {
            int size = list.size();
            for (int i = 0; i < size; i += 2) {
                Object obj = list.get(i);
                if (obj instanceof PeriodPrinter) {
                    if (obj instanceof Composite) {
                        m23401(list2, (Object[]) ((Composite) obj).f18207);
                    } else {
                        list2.add(obj);
                    }
                }
                Object obj2 = list.get(i + 1);
                if (obj2 instanceof PeriodParser) {
                    if (obj2 instanceof Composite) {
                        m23401(list3, (Object[]) ((Composite) obj2).f18206);
                    } else {
                        list3.add(obj2);
                    }
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m23401(List<Object> list, Object[] objArr) {
            if (objArr != null) {
                for (Object add : objArr) {
                    list.add(add);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23402(ReadWritablePeriod readWritablePeriod, String str, int i, Locale locale) {
            PeriodParser[] periodParserArr = this.f18206;
            if (periodParserArr == null) {
                throw new UnsupportedOperationException();
            }
            int length = periodParserArr.length;
            for (int i2 = 0; i2 < length && i >= 0; i2++) {
                i = periodParserArr[i2].m23446(readWritablePeriod, str, i, locale);
            }
            return i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23403(ReadablePeriod readablePeriod, int i, Locale locale) {
            int i2 = 0;
            PeriodPrinter[] periodPrinterArr = this.f18207;
            int length = periodPrinterArr.length;
            while (i2 < i) {
                length--;
                if (length < 0) {
                    break;
                }
                i2 += periodPrinterArr[length].m23447(readablePeriod, (int) MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT, locale);
            }
            return i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23404(ReadablePeriod readablePeriod, Locale locale) {
            int i = 0;
            PeriodPrinter[] periodPrinterArr = this.f18207;
            int length = periodPrinterArr.length;
            while (true) {
                length--;
                if (length < 0) {
                    return i;
                }
                i += periodPrinterArr[length].m23448(readablePeriod, locale);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23405(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            for (PeriodPrinter r3 : this.f18207) {
                r3.m23449(stringBuffer, readablePeriod, locale);
            }
        }
    }

    static class CompositeAffix extends IgnorableAffix {

        /* renamed from: 靐  reason: contains not printable characters */
        private final PeriodFieldAffix f18208;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String[] f18209;

        /* renamed from: 龘  reason: contains not printable characters */
        private final PeriodFieldAffix f18210;

        CompositeAffix(PeriodFieldAffix periodFieldAffix, PeriodFieldAffix periodFieldAffix2) {
            this.f18210 = periodFieldAffix;
            this.f18208 = periodFieldAffix2;
            HashSet hashSet = new HashSet();
            for (String str : this.f18210.m23433()) {
                String[] r7 = this.f18208.m23433();
                int length = r7.length;
                for (int i = 0; i < length; i++) {
                    hashSet.add(str + r7[i]);
                }
            }
            this.f18209 = (String[]) hashSet.toArray(new String[hashSet.size()]);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23406(String str, int i) {
            int r1;
            int r0 = this.f18210.m23428(str, i);
            return (r0 < 0 || ((r1 = this.f18208.m23428(str, this.f18210.m23430(str, r0))) >= 0 && m23423(this.f18208.m23430(str, r1) - r0, str, i))) ? i ^ -1 : r0 > 0 ? r0 : r1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23407(int i) {
            return this.f18210.m23429(i) + this.f18208.m23429(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23408(String str, int i) {
            int r0 = this.f18210.m23430(str, i);
            if (r0 < 0) {
                return r0;
            }
            int r02 = this.f18208.m23430(str, r0);
            return (r02 < 0 || !m23423(m23408(str, r02) - r02, str, i)) ? r02 : i ^ -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23409(StringBuffer stringBuffer, int i) {
            this.f18210.m23431(stringBuffer, i);
            this.f18208.m23431(stringBuffer, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String[] m23410() {
            return (String[]) this.f18209.clone();
        }
    }

    static class FieldFormatter implements PeriodParser, PeriodPrinter {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final FieldFormatter[] f18211;

        /* renamed from: ʼ  reason: contains not printable characters */
        private final PeriodFieldAffix f18212;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final PeriodFieldAffix f18213;

        /* renamed from: 连任  reason: contains not printable characters */
        private final int f18214;

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f18215;

        /* renamed from: 麤  reason: contains not printable characters */
        private final boolean f18216;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f18217;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f18218;

        FieldFormatter(int i, int i2, int i3, boolean z, int i4, FieldFormatter[] fieldFormatterArr, PeriodFieldAffix periodFieldAffix, PeriodFieldAffix periodFieldAffix2) {
            this.f18218 = i;
            this.f18215 = i2;
            this.f18217 = i3;
            this.f18216 = z;
            this.f18214 = i4;
            this.f18211 = fieldFormatterArr;
            this.f18212 = periodFieldAffix;
            this.f18213 = periodFieldAffix2;
        }

        FieldFormatter(FieldFormatter fieldFormatter, PeriodFieldAffix periodFieldAffix) {
            this.f18218 = fieldFormatter.f18218;
            this.f18215 = fieldFormatter.f18215;
            this.f18217 = fieldFormatter.f18217;
            this.f18216 = fieldFormatter.f18216;
            this.f18214 = fieldFormatter.f18214;
            this.f18211 = fieldFormatter.f18211;
            this.f18212 = fieldFormatter.f18212;
            this.f18213 = fieldFormatter.f18213 != null ? new CompositeAffix(fieldFormatter.f18213, periodFieldAffix) : periodFieldAffix;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m23411(String str, int i, int i2) {
            int i3;
            boolean z = false;
            if (i2 >= 10) {
                return Integer.parseInt(str.substring(i, i + i2));
            }
            if (i2 <= 0) {
                return 0;
            }
            int i4 = i + 1;
            char charAt = str.charAt(i);
            int i5 = i2 - 1;
            if (charAt == '-') {
                i5--;
                if (i5 < 0) {
                    return 0;
                }
                z = true;
                i3 = i4 + 1;
                charAt = str.charAt(i4);
            } else {
                i3 = i4;
            }
            int i6 = charAt - '0';
            int i7 = i3;
            while (true) {
                int i8 = i5 - 1;
                if (i5 <= 0) {
                    break;
                }
                i6 = (((i6 << 1) + (i6 << 3)) + str.charAt(i7)) - 48;
                i7++;
                i5 = i8;
            }
            return z ? -i6 : i6;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m23412(ReadablePeriod readablePeriod) {
            int size = readablePeriod.size();
            for (int i = 0; i < size; i++) {
                if (readablePeriod.getValue(i) != 0) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m23413() {
            return this.f18214;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:105:0x0156, code lost:
            r4 = r5;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m23414(org2.joda.time.ReadWritablePeriod r10, java.lang.String r11, int r12, java.util.Locale r13) {
            /*
                r9 = this;
                int r0 = r9.f18215
                r1 = 4
                if (r0 != r1) goto L_0x0011
                r0 = 1
            L_0x0006:
                int r1 = r11.length()
                if (r12 < r1) goto L_0x0013
                if (r0 == 0) goto L_0x0010
                r12 = r12 ^ -1
            L_0x0010:
                return r12
            L_0x0011:
                r0 = 0
                goto L_0x0006
            L_0x0013:
                org2.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f18212
                if (r1 == 0) goto L_0x0020
                org2.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f18212
                int r12 = r1.m23430((java.lang.String) r11, (int) r12)
                if (r12 < 0) goto L_0x0085
                r0 = 1
            L_0x0020:
                r1 = -1
                org2.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r2 = r9.f18213
                if (r2 == 0) goto L_0x0159
                if (r0 != 0) goto L_0x0159
                org2.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f18213
                int r1 = r1.m23428(r11, r12)
                if (r1 < 0) goto L_0x008a
                r0 = 1
                r6 = r1
            L_0x0031:
                if (r0 != 0) goto L_0x003f
                org2.joda.time.PeriodType r0 = r10.getPeriodType()
                int r1 = r9.f18214
                boolean r0 = r9.m23421((org2.joda.time.PeriodType) r0, (int) r1)
                if (r0 == 0) goto L_0x0010
            L_0x003f:
                if (r6 <= 0) goto L_0x0091
                int r0 = r9.f18217
                int r1 = r6 - r12
                int r0 = java.lang.Math.min(r0, r1)
            L_0x0049:
                r1 = 0
                r3 = -1
                r2 = 0
                r5 = 0
                r4 = r0
                r0 = r1
                r1 = r12
            L_0x0050:
                if (r0 >= r4) goto L_0x0156
                int r7 = r1 + r0
                char r7 = r11.charAt(r7)
                if (r0 != 0) goto L_0x00b2
                r8 = 45
                if (r7 == r8) goto L_0x0062
                r8 = 43
                if (r7 != r8) goto L_0x00b2
            L_0x0062:
                boolean r8 = r9.f18216
                if (r8 != 0) goto L_0x00b2
                r5 = 45
                if (r7 != r5) goto L_0x009d
                r5 = 1
            L_0x006b:
                int r7 = r0 + 1
                if (r7 >= r4) goto L_0x0156
                int r7 = r1 + r0
                int r7 = r7 + 1
                char r7 = r11.charAt(r7)
                r8 = 48
                if (r7 < r8) goto L_0x0156
                r8 = 57
                if (r7 <= r8) goto L_0x009f
                r4 = r5
            L_0x0080:
                if (r2 != 0) goto L_0x00e6
                r12 = r1 ^ -1
                goto L_0x0010
            L_0x0085:
                if (r0 != 0) goto L_0x0010
                r12 = r12 ^ -1
                goto L_0x0010
            L_0x008a:
                if (r0 != 0) goto L_0x008f
                r12 = r1 ^ -1
                goto L_0x0010
            L_0x008f:
                r12 = r1
                goto L_0x0010
            L_0x0091:
                int r0 = r9.f18217
                int r1 = r11.length()
                int r1 = r1 - r12
                int r0 = java.lang.Math.min(r0, r1)
                goto L_0x0049
            L_0x009d:
                r5 = 0
                goto L_0x006b
            L_0x009f:
                if (r5 == 0) goto L_0x00af
                int r0 = r0 + 1
            L_0x00a3:
                int r4 = r4 + 1
                int r7 = r11.length()
                int r7 = r7 - r1
                int r4 = java.lang.Math.min(r4, r7)
                goto L_0x0050
            L_0x00af:
                int r1 = r1 + 1
                goto L_0x00a3
            L_0x00b2:
                r8 = 48
                if (r7 < r8) goto L_0x00be
                r8 = 57
                if (r7 > r8) goto L_0x00be
                r2 = 1
            L_0x00bb:
                int r0 = r0 + 1
                goto L_0x0050
            L_0x00be:
                r8 = 46
                if (r7 == r8) goto L_0x00c6
                r8 = 44
                if (r7 != r8) goto L_0x0156
            L_0x00c6:
                int r7 = r9.f18214
                r8 = 8
                if (r7 == r8) goto L_0x00d2
                int r7 = r9.f18214
                r8 = 9
                if (r7 != r8) goto L_0x0156
            L_0x00d2:
                if (r3 < 0) goto L_0x00d6
                r4 = r5
                goto L_0x0080
            L_0x00d6:
                int r3 = r1 + r0
                int r3 = r3 + 1
                int r4 = r4 + 1
                int r7 = r11.length()
                int r7 = r7 - r1
                int r4 = java.lang.Math.min(r4, r7)
                goto L_0x00bb
            L_0x00e6:
                if (r6 < 0) goto L_0x00ef
                int r2 = r1 + r0
                if (r2 == r6) goto L_0x00ef
                r12 = r1
                goto L_0x0010
            L_0x00ef:
                int r2 = r9.f18214
                r5 = 8
                if (r2 == r5) goto L_0x0114
                int r2 = r9.f18214
                r5 = 9
                if (r2 == r5) goto L_0x0114
                int r2 = r9.f18214
                int r3 = r9.m23411((java.lang.String) r11, (int) r1, (int) r0)
                r9.m23419((org2.joda.time.ReadWritablePeriod) r10, (int) r2, (int) r3)
            L_0x0104:
                int r0 = r0 + r1
                if (r0 < 0) goto L_0x0111
                org2.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f18213
                if (r1 == 0) goto L_0x0111
                org2.joda.time.format.PeriodFormatterBuilder$PeriodFieldAffix r1 = r9.f18213
                int r0 = r1.m23430((java.lang.String) r11, (int) r0)
            L_0x0111:
                r12 = r0
                goto L_0x0010
            L_0x0114:
                if (r3 >= 0) goto L_0x0124
                r2 = 6
                int r3 = r9.m23411((java.lang.String) r11, (int) r1, (int) r0)
                r9.m23419((org2.joda.time.ReadWritablePeriod) r10, (int) r2, (int) r3)
                r2 = 7
                r3 = 0
                r9.m23419((org2.joda.time.ReadWritablePeriod) r10, (int) r2, (int) r3)
                goto L_0x0104
            L_0x0124:
                int r2 = r3 - r1
                int r2 = r2 + -1
                int r5 = r9.m23411((java.lang.String) r11, (int) r1, (int) r2)
                r2 = 6
                r9.m23419((org2.joda.time.ReadWritablePeriod) r10, (int) r2, (int) r5)
                int r2 = r1 + r0
                int r2 = r2 - r3
                if (r2 > 0) goto L_0x013b
                r2 = 0
            L_0x0136:
                r3 = 7
                r9.m23419((org2.joda.time.ReadWritablePeriod) r10, (int) r3, (int) r2)
                goto L_0x0104
            L_0x013b:
                r6 = 3
                if (r2 < r6) goto L_0x0149
                r2 = 3
                int r2 = r9.m23411((java.lang.String) r11, (int) r3, (int) r2)
            L_0x0143:
                if (r4 != 0) goto L_0x0147
                if (r5 >= 0) goto L_0x0136
            L_0x0147:
                int r2 = -r2
                goto L_0x0136
            L_0x0149:
                int r3 = r9.m23411((java.lang.String) r11, (int) r3, (int) r2)
                r6 = 1
                if (r2 != r6) goto L_0x0153
                int r2 = r3 * 100
                goto L_0x0143
            L_0x0153:
                int r2 = r3 * 10
                goto L_0x0143
            L_0x0156:
                r4 = r5
                goto L_0x0080
            L_0x0159:
                r6 = r1
                goto L_0x0031
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.joda.time.format.PeriodFormatterBuilder.FieldFormatter.m23414(org2.joda.time.ReadWritablePeriod, java.lang.String, int, java.util.Locale):int");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23415(ReadablePeriod readablePeriod, int i, Locale locale) {
            if (i <= 0) {
                return 0;
            }
            return (this.f18215 == 4 || m23417(readablePeriod) != Long.MAX_VALUE) ? 1 : 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23416(ReadablePeriod readablePeriod, Locale locale) {
            long r2 = m23417(readablePeriod);
            if (r2 == Long.MAX_VALUE) {
                return 0;
            }
            int max = Math.max(FormatUtils.m23249(r2), this.f18218);
            if (this.f18214 >= 8) {
                max = (r2 < 0 ? Math.max(max, 5) : Math.max(max, 4)) + 1;
                if (this.f18214 == 9 && Math.abs(r2) % 1000 == 0) {
                    max -= 4;
                }
                r2 /= 1000;
            }
            int i = (int) r2;
            if (this.f18212 != null) {
                max += this.f18212.m23429(i);
            }
            return this.f18213 != null ? max + this.f18213.m23429(i) : max;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m23417(ReadablePeriod readablePeriod) {
            long j;
            PeriodType periodType = this.f18215 == 4 ? null : readablePeriod.getPeriodType();
            if (periodType != null && !m23421(periodType, this.f18214)) {
                return Long.MAX_VALUE;
            }
            switch (this.f18214) {
                case 0:
                    j = (long) readablePeriod.get(DurationFieldType.years());
                    break;
                case 1:
                    j = (long) readablePeriod.get(DurationFieldType.months());
                    break;
                case 2:
                    j = (long) readablePeriod.get(DurationFieldType.weeks());
                    break;
                case 3:
                    j = (long) readablePeriod.get(DurationFieldType.days());
                    break;
                case 4:
                    j = (long) readablePeriod.get(DurationFieldType.hours());
                    break;
                case 5:
                    j = (long) readablePeriod.get(DurationFieldType.minutes());
                    break;
                case 6:
                    j = (long) readablePeriod.get(DurationFieldType.seconds());
                    break;
                case 7:
                    j = (long) readablePeriod.get(DurationFieldType.millis());
                    break;
                case 8:
                case 9:
                    j = ((long) readablePeriod.get(DurationFieldType.millis())) + (((long) readablePeriod.get(DurationFieldType.seconds())) * 1000);
                    break;
                default:
                    return Long.MAX_VALUE;
            }
            if (j == 0) {
                switch (this.f18215) {
                    case 1:
                        if (!m23412(readablePeriod) || this.f18211[this.f18214] != this) {
                            return Long.MAX_VALUE;
                        }
                        int min = Math.min(this.f18214, 8) - 1;
                        while (min >= 0 && min <= 9) {
                            if (m23421(periodType, min) && this.f18211[min] != null) {
                                return Long.MAX_VALUE;
                            }
                            min--;
                        }
                        break;
                    case 2:
                        if (m23412(readablePeriod) && this.f18211[this.f18214] == this) {
                            for (int i = this.f18214 + 1; i <= 9; i++) {
                                if (m23421(periodType, i) && this.f18211[i] != null) {
                                    return Long.MAX_VALUE;
                                }
                            }
                            break;
                        } else {
                            return Long.MAX_VALUE;
                        }
                        break;
                    case 5:
                        return Long.MAX_VALUE;
                }
            }
            return j;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23418(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            long r2 = m23417(readablePeriod);
            if (r2 != Long.MAX_VALUE) {
                int i = (int) r2;
                if (this.f18214 >= 8) {
                    i = (int) (r2 / 1000);
                }
                if (this.f18212 != null) {
                    this.f18212.m23431(stringBuffer, i);
                }
                int length = stringBuffer.length();
                int i2 = this.f18218;
                if (i2 <= 1) {
                    FormatUtils.m23255(stringBuffer, i);
                } else {
                    FormatUtils.m23256(stringBuffer, i, i2);
                }
                if (this.f18214 >= 8) {
                    int abs = (int) (Math.abs(r2) % 1000);
                    if (this.f18214 == 8 || abs > 0) {
                        if (r2 < 0 && r2 > -1000) {
                            stringBuffer.insert(length, '-');
                        }
                        stringBuffer.append('.');
                        FormatUtils.m23256(stringBuffer, abs, 3);
                    }
                }
                if (this.f18213 != null) {
                    this.f18213.m23431(stringBuffer, i);
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m23419(ReadWritablePeriod readWritablePeriod, int i, int i2) {
            switch (i) {
                case 0:
                    readWritablePeriod.setYears(i2);
                    return;
                case 1:
                    readWritablePeriod.setMonths(i2);
                    return;
                case 2:
                    readWritablePeriod.setWeeks(i2);
                    return;
                case 3:
                    readWritablePeriod.setDays(i2);
                    return;
                case 4:
                    readWritablePeriod.setHours(i2);
                    return;
                case 5:
                    readWritablePeriod.setMinutes(i2);
                    return;
                case 6:
                    readWritablePeriod.setSeconds(i2);
                    return;
                case 7:
                    readWritablePeriod.setMillis(i2);
                    return;
                default:
                    return;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23420(FieldFormatter[] fieldFormatterArr) {
            HashSet hashSet = new HashSet();
            HashSet hashSet2 = new HashSet();
            for (FieldFormatter fieldFormatter : fieldFormatterArr) {
                if (fieldFormatter != null && !equals(fieldFormatter)) {
                    hashSet.add(fieldFormatter.f18212);
                    hashSet2.add(fieldFormatter.f18213);
                }
            }
            if (this.f18212 != null) {
                this.f18212.m23432((Set<PeriodFieldAffix>) hashSet);
            }
            if (this.f18213 != null) {
                this.f18213.m23432((Set<PeriodFieldAffix>) hashSet2);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23421(PeriodType periodType, int i) {
            switch (i) {
                case 0:
                    return periodType.isSupported(DurationFieldType.years());
                case 1:
                    return periodType.isSupported(DurationFieldType.months());
                case 2:
                    return periodType.isSupported(DurationFieldType.weeks());
                case 3:
                    return periodType.isSupported(DurationFieldType.days());
                case 4:
                    return periodType.isSupported(DurationFieldType.hours());
                case 5:
                    return periodType.isSupported(DurationFieldType.minutes());
                case 6:
                    return periodType.isSupported(DurationFieldType.seconds());
                case 7:
                    return periodType.isSupported(DurationFieldType.millis());
                case 8:
                case 9:
                    return periodType.isSupported(DurationFieldType.seconds()) || periodType.isSupported(DurationFieldType.millis());
                default:
                    return false;
            }
        }
    }

    static abstract class IgnorableAffix implements PeriodFieldAffix {

        /* renamed from: 龘  reason: contains not printable characters */
        private volatile String[] f18219;

        IgnorableAffix() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23422(Set<PeriodFieldAffix> set) {
            int i;
            if (this.f18219 == null) {
                int i2 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
                String str = null;
                String[] r5 = m23433();
                int length = r5.length;
                int i3 = 0;
                while (i3 < length) {
                    String str2 = r5[i3];
                    if (str2.length() < i2) {
                        i = str2.length();
                    } else {
                        str2 = str;
                        i = i2;
                    }
                    i3++;
                    i2 = i;
                    str = str2;
                }
                HashSet hashSet = new HashSet();
                for (PeriodFieldAffix next : set) {
                    if (next != null) {
                        for (String str3 : next.m23433()) {
                            if (str3.length() > i2 || (str3.equalsIgnoreCase(str) && !str3.equals(str))) {
                                hashSet.add(str3);
                            }
                        }
                    }
                }
                this.f18219 = (String[]) hashSet.toArray(new String[hashSet.size()]);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23423(int i, String str, int i2) {
            if (this.f18219 != null) {
                for (String str2 : this.f18219) {
                    int length = str2.length();
                    if ((i < length && str.regionMatches(true, i2, str2, 0, length)) || (i == length && str.regionMatches(false, i2, str2, 0, length))) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    static class Literal implements PeriodParser, PeriodPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        static final Literal f18220 = new Literal("");

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f18221;

        Literal(String str) {
            this.f18221 = str;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23424(ReadWritablePeriod readWritablePeriod, String str, int i, Locale locale) {
            return str.regionMatches(true, i, this.f18221, 0, this.f18221.length()) ? this.f18221.length() + i : i ^ -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23425(ReadablePeriod readablePeriod, int i, Locale locale) {
            return 0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23426(ReadablePeriod readablePeriod, Locale locale) {
            return this.f18221.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23427(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            stringBuffer.append(this.f18221);
        }
    }

    interface PeriodFieldAffix {
        /* renamed from: 靐  reason: contains not printable characters */
        int m23428(String str, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        int m23429(int i);

        /* renamed from: 龘  reason: contains not printable characters */
        int m23430(String str, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m23431(StringBuffer stringBuffer, int i);

        /* renamed from: 龘  reason: contains not printable characters */
        void m23432(Set<PeriodFieldAffix> set);

        /* renamed from: 龘  reason: contains not printable characters */
        String[] m23433();
    }

    static class Separator implements PeriodParser, PeriodPrinter {

        /* renamed from: ʻ  reason: contains not printable characters */
        private final PeriodPrinter f18222;
        /* access modifiers changed from: private */

        /* renamed from: ʼ  reason: contains not printable characters */
        public volatile PeriodPrinter f18223;

        /* renamed from: ʽ  reason: contains not printable characters */
        private final PeriodParser f18224;
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public volatile PeriodParser f18225;

        /* renamed from: 连任  reason: contains not printable characters */
        private final boolean f18226;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f18227;

        /* renamed from: 麤  reason: contains not printable characters */
        private final boolean f18228;

        /* renamed from: 齉  reason: contains not printable characters */
        private final String[] f18229;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18230;

        Separator(String str, String str2, String[] strArr, PeriodPrinter periodPrinter, PeriodParser periodParser, boolean z, boolean z2) {
            this.f18230 = str;
            this.f18227 = str2;
            if ((str2 == null || str.equals(str2)) && (strArr == null || strArr.length == 0)) {
                this.f18229 = new String[]{str};
            } else {
                TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
                treeSet.add(str);
                treeSet.add(str2);
                if (strArr != null) {
                    int length = strArr.length;
                    while (true) {
                        length--;
                        if (length < 0) {
                            break;
                        }
                        treeSet.add(strArr[length]);
                    }
                }
                ArrayList arrayList = new ArrayList(treeSet);
                Collections.reverse(arrayList);
                this.f18229 = (String[]) arrayList.toArray(new String[arrayList.size()]);
            }
            this.f18222 = periodPrinter;
            this.f18224 = periodParser;
            this.f18228 = z;
            this.f18226 = z2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0034  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x003f  */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m23436(org2.joda.time.ReadWritablePeriod r11, java.lang.String r12, int r13, java.util.Locale r14) {
            /*
                r10 = this;
                r1 = 1
                r4 = 0
                org2.joda.time.format.PeriodParser r0 = r10.f18224
                int r2 = r0.m23446(r11, r12, r13, r14)
                if (r2 >= 0) goto L_0x000b
            L_0x000a:
                return r2
            L_0x000b:
                r6 = -1
                if (r2 <= r13) goto L_0x0055
                java.lang.String[] r8 = r10.f18229
                int r9 = r8.length
                r7 = r4
            L_0x0012:
                if (r7 >= r9) goto L_0x0055
                r3 = r8[r7]
                if (r3 == 0) goto L_0x0029
                int r0 = r3.length()
                if (r0 == 0) goto L_0x0029
                int r5 = r3.length()
                r0 = r12
                boolean r0 = r0.regionMatches(r1, r2, r3, r4, r5)
                if (r0 == 0) goto L_0x003b
            L_0x0029:
                if (r3 != 0) goto L_0x0036
            L_0x002b:
                int r2 = r2 + r4
            L_0x002c:
                org2.joda.time.format.PeriodParser r0 = r10.f18225
                int r0 = r0.m23446(r11, r12, r2, r14)
                if (r0 >= 0) goto L_0x003f
                r2 = r0
                goto L_0x000a
            L_0x0036:
                int r4 = r3.length()
                goto L_0x002b
            L_0x003b:
                int r0 = r7 + 1
                r7 = r0
                goto L_0x0012
            L_0x003f:
                if (r1 == 0) goto L_0x0048
                if (r0 != r2) goto L_0x0048
                if (r4 <= 0) goto L_0x0048
                r2 = r2 ^ -1
                goto L_0x000a
            L_0x0048:
                if (r0 <= r2) goto L_0x0053
                if (r1 != 0) goto L_0x0053
                boolean r1 = r10.f18228
                if (r1 != 0) goto L_0x0053
                r2 = r2 ^ -1
                goto L_0x000a
            L_0x0053:
                r2 = r0
                goto L_0x000a
            L_0x0055:
                r1 = r4
                r4 = r6
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.joda.time.format.PeriodFormatterBuilder.Separator.m23436(org2.joda.time.ReadWritablePeriod, java.lang.String, int, java.util.Locale):int");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23437(ReadablePeriod readablePeriod, int i, Locale locale) {
            int r0 = this.f18222.m23447(readablePeriod, i, locale);
            return r0 < i ? r0 + this.f18223.m23447(readablePeriod, i, locale) : r0;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23438(ReadablePeriod readablePeriod, Locale locale) {
            int i;
            PeriodPrinter periodPrinter = this.f18222;
            PeriodPrinter periodPrinter2 = this.f18223;
            int r1 = periodPrinter.m23448(readablePeriod, locale) + periodPrinter2.m23448(readablePeriod, locale);
            if (!this.f18228) {
                return (!this.f18226 || periodPrinter2.m23447(readablePeriod, 1, locale) <= 0) ? r1 : r1 + this.f18230.length();
            }
            if (periodPrinter.m23447(readablePeriod, 1, locale) <= 0) {
                return r1;
            }
            if (!this.f18226) {
                return r1 + this.f18230.length();
            }
            int r0 = periodPrinter2.m23447(readablePeriod, 2, locale);
            if (r0 > 0) {
                i = (r0 > 1 ? this.f18230 : this.f18227).length() + r1;
            } else {
                i = r1;
            }
            return i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Separator m23439(PeriodPrinter periodPrinter, PeriodParser periodParser) {
            this.f18223 = periodPrinter;
            this.f18225 = periodParser;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23440(StringBuffer stringBuffer, ReadablePeriod readablePeriod, Locale locale) {
            PeriodPrinter periodPrinter = this.f18222;
            PeriodPrinter periodPrinter2 = this.f18223;
            periodPrinter.m23449(stringBuffer, readablePeriod, locale);
            if (this.f18228) {
                if (periodPrinter.m23447(readablePeriod, 1, locale) > 0) {
                    if (this.f18226) {
                        int r0 = periodPrinter2.m23447(readablePeriod, 2, locale);
                        if (r0 > 0) {
                            stringBuffer.append(r0 > 1 ? this.f18230 : this.f18227);
                        }
                    } else {
                        stringBuffer.append(this.f18230);
                    }
                }
            } else if (this.f18226 && periodPrinter2.m23447(readablePeriod, 1, locale) > 0) {
                stringBuffer.append(this.f18230);
            }
            periodPrinter2.m23449(stringBuffer, readablePeriod, locale);
        }
    }

    static class SimpleAffix extends IgnorableAffix {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18231;

        SimpleAffix(String str) {
            this.f18231 = str;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23441(String str, int i) {
            String str2 = this.f18231;
            int length = str2.length();
            int length2 = str.length();
            int i2 = i;
            while (i2 < length2) {
                if (str.regionMatches(true, i2, str2, 0, length) && !m23423(length, str, i2)) {
                    return i2;
                }
                switch (str.charAt(i2)) {
                    case '+':
                    case ',':
                    case '-':
                    case '.':
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        i2++;
                }
                return i ^ -1;
            }
            return i ^ -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23442(int i) {
            return this.f18231.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23443(String str, int i) {
            String str2 = this.f18231;
            int length = str2.length();
            return (!str.regionMatches(true, i, str2, 0, length) || m23423(length, str, i)) ? i ^ -1 : i + length;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23444(StringBuffer stringBuffer, int i) {
            stringBuffer.append(this.f18231);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String[] m23445() {
            return new String[]{this.f18231};
        }
    }

    public PeriodFormatterBuilder() {
        m23394();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m23380() throws IllegalStateException {
        if (this.f18197 != null) {
            throw new IllegalStateException("Prefix not followed by field");
        }
        this.f18197 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static PeriodFormatter m23381(List<Object> list, boolean z, boolean z2) {
        if (!z || !z2) {
            int size = list.size();
            if (size >= 2 && (list.get(0) instanceof Separator)) {
                Separator separator = (Separator) list.get(0);
                if (separator.f18225 == null && separator.f18223 == null) {
                    PeriodFormatter r1 = m23381(list.subList(2, size), z, z2);
                    Separator r12 = separator.m23439(r1.m23379(), r1.m23374());
                    return new PeriodFormatter(r12, r12);
                }
            }
            Object[] r3 = m23387(list);
            return z ? new PeriodFormatter((PeriodPrinter) null, (PeriodParser) r3[1]) : z2 ? new PeriodFormatter((PeriodPrinter) r3[0], (PeriodParser) null) : new PeriodFormatter((PeriodPrinter) r3[0], (PeriodParser) r3[1]);
        }
        throw new IllegalStateException("Builder has created neither a printer nor a parser");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodFormatterBuilder m23382(String str, String str2, String[] strArr, boolean z, boolean z2) {
        List<Object> list;
        if (str == null || str2 == null) {
            throw new IllegalArgumentException();
        }
        m23380();
        List<Object> list2 = this.f18198;
        if (list2.size() != 0) {
            Separator separator = null;
            int size = list2.size();
            while (true) {
                int i = size - 1;
                if (i < 0) {
                    list = list2;
                    break;
                } else if (list2.get(i) instanceof Separator) {
                    separator = (Separator) list2.get(i);
                    list = list2.subList(i + 1, list2.size());
                    break;
                } else {
                    size = i - 1;
                }
            }
            if (separator == null || list.size() != 0) {
                Object[] r1 = m23387(list);
                list.clear();
                Separator separator2 = new Separator(str, str2, strArr, (PeriodPrinter) r1[0], (PeriodParser) r1[1], z, z2);
                list.add(separator2);
                list.add(separator2);
            } else {
                throw new IllegalStateException("Cannot have two adjacent separators");
            }
        } else if (z2 && !z) {
            Separator separator3 = new Separator(str, str2, strArr, Literal.f18220, Literal.f18220, z, z2);
            m23384((PeriodPrinter) separator3, (PeriodParser) separator3);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodFormatterBuilder m23383(PeriodFieldAffix periodFieldAffix) {
        Object obj;
        Object obj2 = null;
        if (this.f18198.size() > 0) {
            obj2 = this.f18198.get(this.f18198.size() - 2);
            obj = this.f18198.get(this.f18198.size() - 1);
        } else {
            obj = null;
        }
        if (obj2 == null || obj == null || obj2 != obj || !(obj2 instanceof FieldFormatter)) {
            throw new IllegalStateException("No field to apply suffix to");
        }
        m23380();
        FieldFormatter fieldFormatter = new FieldFormatter((FieldFormatter) obj2, periodFieldAffix);
        this.f18198.set(this.f18198.size() - 2, fieldFormatter);
        this.f18198.set(this.f18198.size() - 1, fieldFormatter);
        this.f18201[fieldFormatter.m23413()] = fieldFormatter;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodFormatterBuilder m23384(PeriodPrinter periodPrinter, PeriodParser periodParser) {
        boolean z = true;
        this.f18198.add(periodPrinter);
        this.f18198.add(periodParser);
        this.f18199 = (periodPrinter == null) | this.f18199;
        boolean z2 = this.f18200;
        if (periodParser != null) {
            z = false;
        }
        this.f18200 = z2 | z;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23385(int i) {
        m23386(i, this.f18203);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23386(int i, int i2) {
        FieldFormatter fieldFormatter = new FieldFormatter(i2, this.f18205, this.f18204, this.f18202, i, this.f18201, this.f18197, (PeriodFieldAffix) null);
        m23384((PeriodPrinter) fieldFormatter, (PeriodParser) fieldFormatter);
        this.f18201[i] = fieldFormatter;
        this.f18197 = null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Object[] m23387(List<Object> list) {
        switch (list.size()) {
            case 0:
                return new Object[]{Literal.f18220, Literal.f18220};
            case 1:
                return new Object[]{list.get(0), list.get(1)};
            default:
                Composite composite = new Composite(list);
                return new Object[]{composite, composite};
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public PeriodFormatterBuilder m23388() {
        m23385(3);
        return this;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public PeriodFormatterBuilder m23389() {
        m23385(4);
        return this;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public PeriodFormatterBuilder m23390() {
        m23385(5);
        return this;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public PeriodFormatterBuilder m23391() {
        m23385(9);
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public PeriodFormatterBuilder m23392() {
        m23385(2);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PeriodFormatterBuilder m23393(String str) {
        if (str != null) {
            return m23383((PeriodFieldAffix) new SimpleAffix(str));
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m23394() {
        this.f18203 = 1;
        this.f18205 = 2;
        this.f18204 = 10;
        this.f18202 = false;
        this.f18197 = null;
        if (this.f18198 == null) {
            this.f18198 = new ArrayList();
        } else {
            this.f18198.clear();
        }
        this.f18199 = false;
        this.f18200 = false;
        this.f18201 = new FieldFormatter[10];
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public PeriodFormatterBuilder m23395() {
        m23385(1);
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PeriodFormatterBuilder m23396() {
        m23385(0);
        return this;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public PeriodFormatterBuilder m23397(String str) {
        return m23382(str, str, (String[]) null, false, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodFormatter m23398() {
        PeriodFormatter r1 = m23381(this.f18198, this.f18199, this.f18200);
        for (FieldFormatter fieldFormatter : this.f18201) {
            if (fieldFormatter != null) {
                fieldFormatter.m23420(this.f18201);
            }
        }
        this.f18201 = (FieldFormatter[]) this.f18201.clone();
        return r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodFormatterBuilder m23399(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Literal must not be null");
        }
        m23380();
        Literal literal = new Literal(str);
        m23384((PeriodPrinter) literal, (PeriodParser) literal);
        return this;
    }
}
