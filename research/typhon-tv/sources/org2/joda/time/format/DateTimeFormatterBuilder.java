package org2.joda.time.format;

import com.google.android.exoplayer2.C;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.MutableDateTime;
import org2.joda.time.MutableDateTime$Property;
import org2.joda.time.ReadablePartial;
import org2.joda.time.field.MillisDurationField;
import org2.joda.time.field.PreciseDateTimeField;

public class DateTimeFormatterBuilder {

    /* renamed from: 靐  reason: contains not printable characters */
    private Object f18068;

    /* renamed from: 龘  reason: contains not printable characters */
    private ArrayList<Object> f18069 = new ArrayList<>();

    static class CharacterLiteral implements InternalParser, InternalPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        private final char f18070;

        CharacterLiteral(char c) {
            this.f18070 = c;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23134() {
            return 1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23135() {
            return 1;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
            r0 = java.lang.Character.toUpperCase(r0);
            r1 = java.lang.Character.toUpperCase(r1);
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m23136(org2.joda.time.format.DateTimeParserBucket r3, java.lang.CharSequence r4, int r5) {
            /*
                r2 = this;
                int r0 = r4.length()
                if (r5 < r0) goto L_0x0009
                r0 = r5 ^ -1
            L_0x0008:
                return r0
            L_0x0009:
                char r0 = r4.charAt(r5)
                char r1 = r2.f18070
                if (r0 == r1) goto L_0x0028
                char r0 = java.lang.Character.toUpperCase(r0)
                char r1 = java.lang.Character.toUpperCase(r1)
                if (r0 == r1) goto L_0x0028
                char r0 = java.lang.Character.toLowerCase(r0)
                char r1 = java.lang.Character.toLowerCase(r1)
                if (r0 == r1) goto L_0x0028
                r0 = r5 ^ -1
                goto L_0x0008
            L_0x0028:
                int r0 = r5 + 1
                goto L_0x0008
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.joda.time.format.DateTimeFormatterBuilder.CharacterLiteral.m23136(org2.joda.time.format.DateTimeParserBucket, java.lang.CharSequence, int):int");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23137(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(this.f18070);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23138(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            appendable.append(this.f18070);
        }
    }

    static class Composite implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final InternalParser[] f18071;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f18072;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f18073;

        /* renamed from: 龘  reason: contains not printable characters */
        private final InternalPrinter[] f18074;

        Composite(List<Object> list) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            m23139(list, (List<Object>) arrayList, (List<Object>) arrayList2);
            if (arrayList.contains((Object) null) || arrayList.isEmpty()) {
                this.f18074 = null;
                this.f18073 = 0;
            } else {
                int size = arrayList.size();
                this.f18074 = new InternalPrinter[size];
                int i = 0;
                for (int i2 = 0; i2 < size; i2++) {
                    InternalPrinter internalPrinter = (InternalPrinter) arrayList.get(i2);
                    i += internalPrinter.m23360();
                    this.f18074[i2] = internalPrinter;
                }
                this.f18073 = i;
            }
            if (arrayList2.contains((Object) null) || arrayList2.isEmpty()) {
                this.f18071 = null;
                this.f18072 = 0;
                return;
            }
            int size2 = arrayList2.size();
            this.f18071 = new InternalParser[size2];
            int i3 = 0;
            for (int i4 = 0; i4 < size2; i4++) {
                InternalParser internalParser = (InternalParser) arrayList2.get(i4);
                i3 += internalParser.m23354();
                this.f18071[i4] = internalParser;
            }
            this.f18072 = i3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m23139(List<Object> list, List<Object> list2, List<Object> list3) {
            int size = list.size();
            for (int i = 0; i < size; i += 2) {
                Object obj = list.get(i);
                if (obj instanceof Composite) {
                    m23140(list2, ((Composite) obj).f18074);
                } else {
                    list2.add(obj);
                }
                Object obj2 = list.get(i + 1);
                if (obj2 instanceof Composite) {
                    m23140(list3, ((Composite) obj2).f18071);
                } else {
                    list3.add(obj2);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m23140(List<Object> list, Object[] objArr) {
            if (objArr != null) {
                for (Object add : objArr) {
                    list.add(add);
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23141() {
            return this.f18072;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public boolean m23142() {
            return this.f18071 != null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public boolean m23143() {
            return this.f18074 != null;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23144() {
            return this.f18073;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23145(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            InternalParser[] internalParserArr = this.f18071;
            if (internalParserArr == null) {
                throw new UnsupportedOperationException();
            }
            int length = internalParserArr.length;
            for (int i2 = 0; i2 < length && i >= 0; i2++) {
                i = internalParserArr[i2].m23355(dateTimeParserBucket, charSequence, i);
            }
            return i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23146(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            InternalPrinter[] internalPrinterArr = this.f18074;
            if (internalPrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            Locale locale2 = locale == null ? Locale.getDefault() : locale;
            for (InternalPrinter r0 : internalPrinterArr) {
                r0.m23361(appendable, j, chronology, i, dateTimeZone, locale2);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23147(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            InternalPrinter[] internalPrinterArr = this.f18074;
            if (internalPrinterArr == null) {
                throw new UnsupportedOperationException();
            }
            if (locale == null) {
                locale = Locale.getDefault();
            }
            for (InternalPrinter r3 : internalPrinterArr) {
                r3.m23362(appendable, readablePartial, locale);
            }
        }
    }

    static class FixedNumber extends PaddedNumber {
        protected FixedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            super(dateTimeFieldType, i, z, i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23148(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            int i2;
            char charAt;
            int r0 = super.m23159(dateTimeParserBucket, charSequence, i);
            if (r0 < 0 || r0 == (i2 = this.f18080 + i)) {
                return r0;
            }
            if (this.f18081 && ((charAt = charSequence.charAt(i)) == '-' || charAt == '+')) {
                i2++;
            }
            return r0 > i2 ? (i2 + 1) ^ -1 : r0 < i2 ? r0 ^ -1 : r0;
        }
    }

    static class Fraction implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        protected int f18075;

        /* renamed from: 齉  reason: contains not printable characters */
        private final DateTimeFieldType f18076;

        /* renamed from: 龘  reason: contains not printable characters */
        protected int f18077;

        protected Fraction(DateTimeFieldType dateTimeFieldType, int i, int i2) {
            this.f18076 = dateTimeFieldType;
            i2 = i2 > 18 ? 18 : i2;
            this.f18077 = i;
            this.f18075 = i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private long[] m23149(long j, DateTimeField dateTimeField) {
            int i;
            long j2;
            long unitMillis = dateTimeField.getDurationField().getUnitMillis();
            int i2 = this.f18075;
            while (true) {
                switch (i) {
                    case 1:
                        j2 = 10;
                        break;
                    case 2:
                        j2 = 100;
                        break;
                    case 3:
                        j2 = 1000;
                        break;
                    case 4:
                        j2 = 10000;
                        break;
                    case 5:
                        j2 = 100000;
                        break;
                    case 6:
                        j2 = C.MICROS_PER_SECOND;
                        break;
                    case 7:
                        j2 = 10000000;
                        break;
                    case 8:
                        j2 = 100000000;
                        break;
                    case 9:
                        j2 = C.NANOS_PER_SECOND;
                        break;
                    case 10:
                        j2 = 10000000000L;
                        break;
                    case 11:
                        j2 = 100000000000L;
                        break;
                    case 12:
                        j2 = 1000000000000L;
                        break;
                    case 13:
                        j2 = 10000000000000L;
                        break;
                    case 14:
                        j2 = 100000000000000L;
                        break;
                    case 15:
                        j2 = 1000000000000000L;
                        break;
                    case 16:
                        j2 = 10000000000000000L;
                        break;
                    case 17:
                        j2 = 100000000000000000L;
                        break;
                    case 18:
                        j2 = 1000000000000000000L;
                        break;
                    default:
                        j2 = 1;
                        break;
                }
                if ((unitMillis * j2) / j2 == unitMillis) {
                    return new long[]{(j2 * j) / unitMillis, (long) i};
                }
                i2 = i - 1;
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23150() {
            return this.f18075;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23151() {
            return this.f18075;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23152(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            DateTimeField field = this.f18076.getField(dateTimeParserBucket.m23224());
            int min = Math.min(this.f18075, charSequence.length() - i);
            long j = 0;
            long unitMillis = field.getDurationField().getUnitMillis() * 10;
            int i2 = 0;
            while (i2 < min) {
                char charAt = charSequence.charAt(i + i2);
                if (charAt < '0' || charAt > '9') {
                    break;
                }
                i2++;
                unitMillis /= 10;
                j += ((long) (charAt - '0')) * unitMillis;
            }
            long j2 = j / 10;
            if (i2 == 0) {
                return i ^ -1;
            }
            if (j2 > 2147483647L) {
                return i ^ -1;
            }
            dateTimeParserBucket.m23226((DateTimeField) new PreciseDateTimeField(DateTimeFieldType.millisOfSecond(), MillisDurationField.INSTANCE, field.getDurationField()), (int) j2);
            return i2 + i;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m23153(Appendable appendable, long j, Chronology chronology) throws IOException {
            DateTimeField field = this.f18076.getField(chronology);
            int i = this.f18077;
            try {
                long remainder = field.remainder(j);
                if (remainder == 0) {
                    while (true) {
                        i--;
                        if (i >= 0) {
                            appendable.append('0');
                        } else {
                            return;
                        }
                    }
                } else {
                    long[] r0 = m23149(remainder, field);
                    long j2 = r0[0];
                    int i2 = (int) r0[1];
                    String num = (2147483647L & j2) == j2 ? Integer.toString((int) j2) : Long.toString(j2);
                    int length = num.length();
                    while (length < i2) {
                        appendable.append('0');
                        i--;
                        i2--;
                    }
                    if (i < i2) {
                        while (i < i2 && length > 1 && num.charAt(length - 1) == '0') {
                            i2--;
                            length--;
                        }
                        if (length < num.length()) {
                            for (int i3 = 0; i3 < length; i3++) {
                                appendable.append(num.charAt(i3));
                            }
                            return;
                        }
                    }
                    appendable.append(num);
                }
            } catch (RuntimeException e) {
                DateTimeFormatterBuilder.m23084(appendable, i);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23154(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            m23153(appendable, j, chronology);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23155(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            m23153(appendable, readablePartial.getChronology().set(readablePartial, 0), readablePartial.getChronology());
        }
    }

    static class MatchingParser implements InternalParser {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f18078;

        /* renamed from: 龘  reason: contains not printable characters */
        private final InternalParser[] f18079;

        MatchingParser(InternalParser[] internalParserArr) {
            int i;
            this.f18079 = internalParserArr;
            int i2 = 0;
            int length = internalParserArr.length;
            while (true) {
                int i3 = length - 1;
                if (i3 >= 0) {
                    InternalParser internalParser = internalParserArr[i3];
                    if (internalParser == null || (i = internalParser.m23354()) <= i2) {
                        i = i2;
                    }
                    i2 = i;
                    length = i3;
                } else {
                    this.f18078 = i2;
                    return;
                }
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23156() {
            return this.f18078;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
            r11.m23230(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
            return r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            return r0 ^ -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0016, code lost:
            if (r4 > r13) goto L_0x001c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
            if (r4 != r13) goto L_0x0055;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
            if (r1 == false) goto L_0x0055;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
            if (r2 == null) goto L_0x0021;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m23157(org2.joda.time.format.DateTimeParserBucket r11, java.lang.CharSequence r12, int r13) {
            /*
                r10 = this;
                r5 = 0
                org2.joda.time.format.InternalParser[] r7 = r10.f18079
                int r8 = r7.length
                java.lang.Object r9 = r11.m23216()
                r2 = 0
                r6 = r5
                r0 = r13
                r4 = r13
            L_0x000c:
                if (r6 >= r8) goto L_0x005b
                r1 = r7[r6]
                if (r1 != 0) goto L_0x0023
                if (r4 > r13) goto L_0x0015
            L_0x0014:
                return r13
            L_0x0015:
                r1 = 1
            L_0x0016:
                if (r4 > r13) goto L_0x001c
                if (r4 != r13) goto L_0x0055
                if (r1 == 0) goto L_0x0055
            L_0x001c:
                if (r2 == 0) goto L_0x0021
                r11.m23230((java.lang.Object) r2)
            L_0x0021:
                r13 = r4
                goto L_0x0014
            L_0x0023:
                int r3 = r1.m23355(r11, r12, r13)
                if (r3 < r13) goto L_0x004b
                if (r3 <= r4) goto L_0x0058
                int r1 = r12.length()
                if (r3 >= r1) goto L_0x003b
                int r1 = r6 + 1
                if (r1 >= r8) goto L_0x003b
                int r1 = r6 + 1
                r1 = r7[r1]
                if (r1 != 0) goto L_0x003d
            L_0x003b:
                r13 = r3
                goto L_0x0014
            L_0x003d:
                java.lang.Object r1 = r11.m23216()
                r2 = r3
            L_0x0042:
                r11.m23230((java.lang.Object) r9)
                int r3 = r6 + 1
                r6 = r3
                r4 = r2
                r2 = r1
                goto L_0x000c
            L_0x004b:
                if (r3 >= 0) goto L_0x0058
                r1 = r3 ^ -1
                if (r1 <= r0) goto L_0x0058
                r0 = r1
                r1 = r2
                r2 = r4
                goto L_0x0042
            L_0x0055:
                r13 = r0 ^ -1
                goto L_0x0014
            L_0x0058:
                r1 = r2
                r2 = r4
                goto L_0x0042
            L_0x005b:
                r1 = r5
                goto L_0x0016
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.joda.time.format.DateTimeFormatterBuilder.MatchingParser.m23157(org2.joda.time.format.DateTimeParserBucket, java.lang.CharSequence, int):int");
        }
    }

    static abstract class NumberFormatter implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        protected final int f18080;

        /* renamed from: 齉  reason: contains not printable characters */
        protected final boolean f18081;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final DateTimeFieldType f18082;

        NumberFormatter(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            this.f18082 = dateTimeFieldType;
            this.f18080 = i;
            this.f18081 = z;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23158() {
            return this.f18080;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c1, code lost:
            r3 = r1;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int m23159(org2.joda.time.format.DateTimeParserBucket r13, java.lang.CharSequence r14, int r15) {
            /*
                r12 = this;
                r10 = 48
                r9 = 45
                r8 = 43
                r2 = 1
                r3 = 0
                int r0 = r12.f18080
                int r1 = r14.length()
                int r1 = r1 - r15
                int r0 = java.lang.Math.min(r0, r1)
                r4 = r3
                r1 = r3
                r5 = r0
                r0 = r3
            L_0x0017:
                if (r4 >= r5) goto L_0x00c1
                int r6 = r15 + r4
                char r6 = r14.charAt(r6)
                if (r4 != 0) goto L_0x0059
                if (r6 == r9) goto L_0x0025
                if (r6 != r8) goto L_0x0059
            L_0x0025:
                boolean r7 = r12.f18081
                if (r7 == 0) goto L_0x0059
                if (r6 != r9) goto L_0x0047
                r1 = r2
            L_0x002c:
                if (r6 != r8) goto L_0x0049
                r0 = r2
            L_0x002f:
                int r6 = r4 + 1
                if (r6 >= r5) goto L_0x00c1
                int r6 = r15 + r4
                int r6 = r6 + 1
                char r6 = r14.charAt(r6)
                if (r6 < r10) goto L_0x00c1
                r7 = 57
                if (r6 <= r7) goto L_0x004b
                r3 = r1
            L_0x0042:
                if (r4 != 0) goto L_0x0064
                r1 = r15 ^ -1
            L_0x0046:
                return r1
            L_0x0047:
                r1 = r3
                goto L_0x002c
            L_0x0049:
                r0 = r3
                goto L_0x002f
            L_0x004b:
                int r4 = r4 + 1
                int r5 = r5 + 1
                int r6 = r14.length()
                int r6 = r6 - r15
                int r5 = java.lang.Math.min(r5, r6)
                goto L_0x0017
            L_0x0059:
                if (r6 < r10) goto L_0x00c1
                r7 = 57
                if (r6 <= r7) goto L_0x0061
                r3 = r1
                goto L_0x0042
            L_0x0061:
                int r4 = r4 + 1
                goto L_0x0017
            L_0x0064:
                r1 = 9
                if (r4 < r1) goto L_0x008f
                if (r0 == 0) goto L_0x0080
                int r0 = r15 + 1
                int r1 = r15 + r4
                java.lang.CharSequence r0 = r14.subSequence(r0, r1)
                java.lang.String r0 = r0.toString()
                int r0 = java.lang.Integer.parseInt(r0)
            L_0x007a:
                org2.joda.time.DateTimeFieldType r2 = r12.f18082
                r13.m23227((org2.joda.time.DateTimeFieldType) r2, (int) r0)
                goto L_0x0046
            L_0x0080:
                int r1 = r15 + r4
                java.lang.CharSequence r0 = r14.subSequence(r15, r1)
                java.lang.String r0 = r0.toString()
                int r0 = java.lang.Integer.parseInt(r0)
                goto L_0x007a
            L_0x008f:
                if (r3 != 0) goto L_0x0093
                if (r0 == 0) goto L_0x00bf
            L_0x0093:
                int r0 = r15 + 1
                r1 = r0
            L_0x0096:
                int r0 = r1 + 1
                char r1 = r14.charAt(r1)     // Catch:{ StringIndexOutOfBoundsException -> 0x00b7 }
                int r2 = r1 + -48
                int r1 = r15 + r4
                r11 = r0
                r0 = r2
                r2 = r11
            L_0x00a3:
                if (r2 >= r1) goto L_0x00bb
                int r4 = r0 << 3
                int r0 = r0 << 1
                int r4 = r4 + r0
                int r0 = r2 + 1
                char r2 = r14.charAt(r2)
                int r2 = r2 + r4
                int r2 = r2 + -48
                r11 = r0
                r0 = r2
                r2 = r11
                goto L_0x00a3
            L_0x00b7:
                r0 = move-exception
                r1 = r15 ^ -1
                goto L_0x0046
            L_0x00bb:
                if (r3 == 0) goto L_0x007a
                int r0 = -r0
                goto L_0x007a
            L_0x00bf:
                r1 = r15
                goto L_0x0096
            L_0x00c1:
                r3 = r1
                goto L_0x0042
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.joda.time.format.DateTimeFormatterBuilder.NumberFormatter.m23159(org2.joda.time.format.DateTimeParserBucket, java.lang.CharSequence, int):int");
        }
    }

    static class PaddedNumber extends NumberFormatter {

        /* renamed from: 麤  reason: contains not printable characters */
        protected final int f18083;

        protected PaddedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z, int i2) {
            super(dateTimeFieldType, i, z);
            this.f18083 = i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23160() {
            return this.f18080;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23161(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                FormatUtils.m23253(appendable, this.f18082.getField(chronology).get(j), this.f18083);
            } catch (RuntimeException e) {
                DateTimeFormatterBuilder.m23084(appendable, this.f18083);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23162(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            if (readablePartial.isSupported(this.f18082)) {
                try {
                    FormatUtils.m23253(appendable, readablePartial.get(this.f18082), this.f18083);
                } catch (RuntimeException e) {
                    DateTimeFormatterBuilder.m23084(appendable, this.f18083);
                }
            } else {
                DateTimeFormatterBuilder.m23084(appendable, this.f18083);
            }
        }
    }

    static class StringLiteral implements InternalParser, InternalPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18084;

        StringLiteral(String str) {
            this.f18084 = str;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23163() {
            return this.f18084.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23164() {
            return this.f18084.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23165(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            return DateTimeFormatterBuilder.m23077(charSequence, i, this.f18084) ? this.f18084.length() + i : i ^ -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23166(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(this.f18084);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23167(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            appendable.append(this.f18084);
        }
    }

    static class TextField implements InternalParser, InternalPrinter {

        /* renamed from: 龘  reason: contains not printable characters */
        private static Map<Locale, Map<DateTimeFieldType, Object[]>> f18085 = new ConcurrentHashMap();

        /* renamed from: 靐  reason: contains not printable characters */
        private final DateTimeFieldType f18086;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f18087;

        TextField(DateTimeFieldType dateTimeFieldType, boolean z) {
            this.f18086 = dateTimeFieldType;
            this.f18087 = z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m23168(long j, Chronology chronology, Locale locale) {
            DateTimeField field = this.f18086.getField(chronology);
            return this.f18087 ? field.getAsShortText(j, locale) : field.getAsText(j, locale);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m23169(ReadablePartial readablePartial, Locale locale) {
            if (!readablePartial.isSupported(this.f18086)) {
                return "�";
            }
            DateTimeField field = this.f18086.getField(readablePartial.getChronology());
            return this.f18087 ? field.getAsShortText(readablePartial, locale) : field.getAsText(readablePartial, locale);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23170() {
            return m23171();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23171() {
            return this.f18087 ? 6 : 20;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23172(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            ConcurrentHashMap concurrentHashMap;
            Map map;
            int intValue;
            Locale r4 = dateTimeParserBucket.m23218();
            Map map2 = f18085.get(r4);
            if (map2 == null) {
                ConcurrentHashMap concurrentHashMap2 = new ConcurrentHashMap();
                f18085.put(r4, concurrentHashMap2);
                concurrentHashMap = concurrentHashMap2;
            } else {
                concurrentHashMap = map2;
            }
            Object[] objArr = (Object[]) concurrentHashMap.get(this.f18086);
            if (objArr == null) {
                ConcurrentHashMap concurrentHashMap3 = new ConcurrentHashMap(32);
                MutableDateTime$Property property = new MutableDateTime(0, DateTimeZone.UTC).property(this.f18086);
                int minimumValueOverall = property.getMinimumValueOverall();
                int maximumValueOverall = property.getMaximumValueOverall();
                if (maximumValueOverall - minimumValueOverall > 32) {
                    return i ^ -1;
                }
                intValue = property.getMaximumTextLength(r4);
                while (minimumValueOverall <= maximumValueOverall) {
                    property.set(minimumValueOverall);
                    concurrentHashMap3.put(property.getAsShortText(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsShortText(r4).toLowerCase(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsShortText(r4).toUpperCase(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsText(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsText(r4).toLowerCase(r4), Boolean.TRUE);
                    concurrentHashMap3.put(property.getAsText(r4).toUpperCase(r4), Boolean.TRUE);
                    minimumValueOverall++;
                }
                if ("en".equals(r4.getLanguage()) && this.f18086 == DateTimeFieldType.era()) {
                    concurrentHashMap3.put("BCE", Boolean.TRUE);
                    concurrentHashMap3.put("bce", Boolean.TRUE);
                    concurrentHashMap3.put("CE", Boolean.TRUE);
                    concurrentHashMap3.put("ce", Boolean.TRUE);
                    intValue = 3;
                }
                concurrentHashMap.put(this.f18086, new Object[]{concurrentHashMap3, Integer.valueOf(intValue)});
                map = concurrentHashMap3;
            } else {
                map = (Map) objArr[0];
                intValue = ((Integer) objArr[1]).intValue();
            }
            for (int min = Math.min(charSequence.length(), intValue + i); min > i; min--) {
                String obj = charSequence.subSequence(i, min).toString();
                if (map.containsKey(obj)) {
                    dateTimeParserBucket.m23228(this.f18086, obj, r4);
                    return min;
                }
            }
            return i ^ -1;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23173(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                appendable.append(m23168(j, chronology, locale));
            } catch (RuntimeException e) {
                appendable.append(65533);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23174(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            try {
                appendable.append(m23169(readablePartial, locale));
            } catch (RuntimeException e) {
                appendable.append(65533);
            }
        }
    }

    enum TimeZoneId implements InternalParser, InternalPrinter {
        INSTANCE;
        

        /* renamed from: ʻ  reason: contains not printable characters */
        private static final List<String> f18088 = null;

        /* renamed from: 连任  reason: contains not printable characters */
        private static final Map<String, List<String>> f18090 = null;

        /* renamed from: 靐  reason: contains not printable characters */
        static final int f18091 = 0;

        /* renamed from: 麤  reason: contains not printable characters */
        private static final List<String> f18092 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        static final int f18093 = 0;

        static {
            f18088 = new ArrayList();
            f18092 = new ArrayList(DateTimeZone.getAvailableIDs());
            Collections.sort(f18092);
            f18090 = new HashMap();
            int i = 0;
            int i2 = 0;
            for (String next : f18092) {
                int indexOf = next.indexOf(47);
                if (indexOf >= 0) {
                    if (indexOf < next.length()) {
                        indexOf++;
                    }
                    int max = Math.max(i, indexOf);
                    String substring = next.substring(0, indexOf + 1);
                    String substring2 = next.substring(indexOf);
                    if (!f18090.containsKey(substring)) {
                        f18090.put(substring, new ArrayList());
                    }
                    f18090.get(substring).add(substring2);
                    i = max;
                } else {
                    f18088.add(next);
                }
                i2 = Math.max(i2, next.length());
            }
            f18091 = i2;
            f18093 = i;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23175() {
            return f18091;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23176() {
            return f18091;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23177(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            int i2;
            List<String> list;
            List<String> list2 = f18088;
            int length = charSequence.length();
            int min = Math.min(length, f18093 + i);
            String str = "";
            int i3 = i;
            while (true) {
                if (i3 >= min) {
                    i2 = i;
                    list = list2;
                    break;
                } else if (charSequence.charAt(i3) == '/') {
                    str = charSequence.subSequence(i, i3 + 1).toString();
                    i2 = i + str.length();
                    List<String> list3 = f18090.get(i3 < length ? str + charSequence.charAt(i3 + 1) : str);
                    if (list3 == null) {
                        return i ^ -1;
                    }
                    list = list3;
                } else {
                    i3++;
                }
            }
            String str2 = null;
            int i4 = 0;
            while (i4 < list.size()) {
                String str3 = list.get(i4);
                if (!DateTimeFormatterBuilder.m23086(charSequence, i2, str3) || (str2 != null && str3.length() <= str2.length())) {
                    str3 = str2;
                }
                i4++;
                str2 = str3;
            }
            if (str2 == null) {
                return i ^ -1;
            }
            dateTimeParserBucket.m23229(DateTimeZone.forID(str + str2));
            return str2.length() + i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23178(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(dateTimeZone != null ? dateTimeZone.getID() : "");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23179(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        }
    }

    static class TimeZoneName implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f18095;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, DateTimeZone> f18096;

        TimeZoneName(int i, Map<String, DateTimeZone> map) {
            this.f18095 = i;
            this.f18096 = map;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private String m23180(long j, DateTimeZone dateTimeZone, Locale locale) {
            if (dateTimeZone == null) {
                return "";
            }
            switch (this.f18095) {
                case 0:
                    return dateTimeZone.getName(j, locale);
                case 1:
                    return dateTimeZone.getShortName(j, locale);
                default:
                    return "";
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23181() {
            return this.f18095 == 1 ? 4 : 20;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23182() {
            return this.f18095 == 1 ? 4 : 20;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23183(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            Map<String, DateTimeZone> map = this.f18096;
            Map<String, DateTimeZone> r1 = map != null ? map : DateTimeUtils.m22708();
            String str = null;
            for (String next : r1.keySet()) {
                if (!DateTimeFormatterBuilder.m23086(charSequence, i, next) || (str != null && next.length() <= str.length())) {
                    next = str;
                }
                str = next;
            }
            if (str == null) {
                return i ^ -1;
            }
            dateTimeParserBucket.m23229(r1.get(str));
            return str.length() + i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23184(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            appendable.append(m23180(j - ((long) i), dateTimeZone, locale));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23185(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        }
    }

    static class TimeZoneOffset implements InternalParser, InternalPrinter {

        /* renamed from: 连任  reason: contains not printable characters */
        private final int f18097;

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f18098;

        /* renamed from: 麤  reason: contains not printable characters */
        private final int f18099;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f18100;

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f18101;

        TimeZoneOffset(String str, String str2, boolean z, int i, int i2) {
            int i3 = 4;
            this.f18101 = str;
            this.f18098 = str2;
            this.f18100 = z;
            if (i <= 0 || i2 < i) {
                throw new IllegalArgumentException();
            }
            if (i > 4) {
                i2 = 4;
            } else {
                i3 = i;
            }
            this.f18099 = i3;
            this.f18097 = i2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m23186(CharSequence charSequence, int i, int i2) {
            int i3 = 0;
            for (int min = Math.min(charSequence.length() - i, i2); min > 0; min--) {
                char charAt = charSequence.charAt(i + i3);
                if (charAt < '0' || charAt > '9') {
                    break;
                }
                i3++;
            }
            return i3;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23187() {
            return m23188();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23188() {
            int i = (this.f18099 + 1) << 1;
            if (this.f18100) {
                i += this.f18099 - 1;
            }
            return (this.f18101 == null || this.f18101.length() <= i) ? i : this.f18101.length();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23189(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            boolean z;
            int i2;
            int i3;
            char charAt;
            boolean z2 = false;
            int length = charSequence.length() - i;
            if (this.f18098 != null) {
                if (this.f18098.length() == 0) {
                    if (length <= 0 || !((charAt = charSequence.charAt(i)) == '-' || charAt == '+')) {
                        dateTimeParserBucket.m23225((Integer) 0);
                        return i;
                    }
                } else if (DateTimeFormatterBuilder.m23077(charSequence, i, this.f18098)) {
                    dateTimeParserBucket.m23225((Integer) 0);
                    return i + this.f18098.length();
                }
            }
            if (length <= 1) {
                return i ^ -1;
            }
            char charAt2 = charSequence.charAt(i);
            if (charAt2 == '-') {
                z = true;
            } else if (charAt2 != '+') {
                return i ^ -1;
            } else {
                z = false;
            }
            int i4 = length - 1;
            int i5 = i + 1;
            if (m23186(charSequence, i5, 2) < 2) {
                return i5 ^ -1;
            }
            int r5 = FormatUtils.m23250(charSequence, i5);
            if (r5 > 23) {
                return i5 ^ -1;
            }
            int i6 = r5 * 3600000;
            int i7 = i4 - 2;
            int i8 = i5 + 2;
            if (i7 <= 0) {
                i2 = i6;
                i3 = i8;
            } else {
                char charAt3 = charSequence.charAt(i8);
                if (charAt3 == ':') {
                    i8++;
                    i7--;
                    z2 = true;
                } else if (charAt3 < '0' || charAt3 > '9') {
                    i2 = i6;
                    i3 = i8;
                }
                int r6 = m23186(charSequence, i8, 2);
                if (r6 == 0 && !z2) {
                    i2 = i6;
                    i3 = i8;
                } else if (r6 < 2) {
                    return i8 ^ -1;
                } else {
                    int r62 = FormatUtils.m23250(charSequence, i8);
                    if (r62 > 59) {
                        return i8 ^ -1;
                    }
                    int i9 = i6 + (r62 * 60000);
                    int i10 = i7 - 2;
                    int i11 = i8 + 2;
                    if (i10 <= 0) {
                        i2 = i9;
                        i3 = i11;
                    } else {
                        if (z2) {
                            if (charSequence.charAt(i11) != ':') {
                                i2 = i9;
                                i3 = i11;
                            } else {
                                i10--;
                                i11++;
                            }
                        }
                        int r63 = m23186(charSequence, i11, 2);
                        if (r63 == 0 && !z2) {
                            i2 = i9;
                            i3 = i11;
                        } else if (r63 < 2) {
                            return i11 ^ -1;
                        } else {
                            int r64 = FormatUtils.m23250(charSequence, i11);
                            if (r64 > 59) {
                                return i11 ^ -1;
                            }
                            int i12 = i9 + (r64 * 1000);
                            int i13 = i10 - 2;
                            int i14 = i11 + 2;
                            if (i13 <= 0) {
                                i2 = i12;
                                i3 = i14;
                            } else {
                                if (z2) {
                                    if (charSequence.charAt(i14) == '.' || charSequence.charAt(i14) == ',') {
                                        int i15 = i13 - 1;
                                        i14++;
                                    } else {
                                        i2 = i12;
                                        i3 = i14;
                                    }
                                }
                                int r65 = m23186(charSequence, i14, 3);
                                if (r65 == 0 && !z2) {
                                    i2 = i12;
                                    i3 = i14;
                                } else if (r65 < 1) {
                                    return i14 ^ -1;
                                } else {
                                    int i16 = i14 + 1;
                                    int charAt4 = ((charSequence.charAt(i14) - '0') * 100) + i12;
                                    if (r65 > 1) {
                                        int i17 = i16 + 1;
                                        i2 = ((charSequence.charAt(i16) - '0') * 10) + charAt4;
                                        if (r65 > 2) {
                                            i2 += charSequence.charAt(i17) - '0';
                                            i3 = i17 + 1;
                                        } else {
                                            i3 = i17;
                                        }
                                    } else {
                                        i2 = charAt4;
                                        i3 = i16;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            dateTimeParserBucket.m23225(Integer.valueOf(z ? -i2 : i2));
            return i3;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23190(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            if (dateTimeZone != null) {
                if (i != 0 || this.f18101 == null) {
                    if (i >= 0) {
                        appendable.append('+');
                    } else {
                        appendable.append('-');
                        i = -i;
                    }
                    int i2 = i / 3600000;
                    FormatUtils.m23253(appendable, i2, 2);
                    if (this.f18097 != 1) {
                        int i3 = i - (i2 * 3600000);
                        if (i3 != 0 || this.f18099 > 1) {
                            int i4 = i3 / 60000;
                            if (this.f18100) {
                                appendable.append(':');
                            }
                            FormatUtils.m23253(appendable, i4, 2);
                            if (this.f18097 != 2) {
                                int i5 = i3 - (i4 * 60000);
                                if (i5 != 0 || this.f18099 > 2) {
                                    int i6 = i5 / 1000;
                                    if (this.f18100) {
                                        appendable.append(':');
                                    }
                                    FormatUtils.m23253(appendable, i6, 2);
                                    if (this.f18097 != 3) {
                                        int i7 = i5 - (i6 * 1000);
                                        if (i7 != 0 || this.f18099 > 3) {
                                            if (this.f18100) {
                                                appendable.append('.');
                                            }
                                            FormatUtils.m23253(appendable, i7, 3);
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                }
                appendable.append(this.f18101);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23191(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        }
    }

    static class TwoDigitYear implements InternalParser, InternalPrinter {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f18102;

        /* renamed from: 齉  reason: contains not printable characters */
        private final boolean f18103;

        /* renamed from: 龘  reason: contains not printable characters */
        private final DateTimeFieldType f18104;

        TwoDigitYear(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            this.f18104 = dateTimeFieldType;
            this.f18102 = i;
            this.f18103 = z;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m23192(long j, Chronology chronology) {
            try {
                int i = this.f18104.getField(chronology).get(j);
                if (i < 0) {
                    i = -i;
                }
                return i % 100;
            } catch (RuntimeException e) {
                return -1;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private int m23193(ReadablePartial readablePartial) {
            if (readablePartial.isSupported(this.f18104)) {
                try {
                    int i = readablePartial.get(this.f18104);
                    if (i < 0) {
                        i = -i;
                    }
                    return i % 100;
                } catch (RuntimeException e) {
                }
            }
            return -1;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23194() {
            return this.f18103 ? 4 : 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23195() {
            return 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23196(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
            int i2;
            int i3;
            int i4 = 0;
            int length = charSequence.length() - i;
            if (this.f18103) {
                int i5 = 0;
                boolean z = false;
                boolean z2 = false;
                int i6 = length;
                while (i5 < i6) {
                    char charAt = charSequence.charAt(i + i5);
                    if (i5 != 0 || (charAt != '-' && charAt != '+')) {
                        if (charAt < '0' || charAt > '9') {
                            break;
                        }
                        i5++;
                    } else {
                        boolean z3 = charAt == '-';
                        if (z3) {
                            i5++;
                            z = z3;
                            z2 = true;
                        } else {
                            i++;
                            z2 = true;
                            i6--;
                            z = z3;
                        }
                    }
                }
                if (i5 == 0) {
                    return i ^ -1;
                }
                if (z2 || i5 != 2) {
                    if (i5 >= 9) {
                        i2 = i + i5;
                        i3 = Integer.parseInt(charSequence.subSequence(i, i2).toString());
                    } else {
                        int i7 = z ? i + 1 : i;
                        int i8 = i7 + 1;
                        try {
                            int charAt2 = charSequence.charAt(i7) - '0';
                            i2 = i + i5;
                            int i9 = i8;
                            i3 = charAt2;
                            for (int i10 = i9; i10 < i2; i10++) {
                                i3 = (charSequence.charAt(i10) + ((i3 << 3) + (i3 << 1))) - 48;
                            }
                            if (z) {
                                i3 = -i3;
                            }
                        } catch (StringIndexOutOfBoundsException e) {
                            return i ^ -1;
                        }
                    }
                    dateTimeParserBucket.m23227(this.f18104, i3);
                    return i2;
                }
            } else if (Math.min(2, length) < 2) {
                return i ^ -1;
            }
            char charAt3 = charSequence.charAt(i);
            if (charAt3 < '0' || charAt3 > '9') {
                return i ^ -1;
            }
            int i11 = charAt3 - '0';
            char charAt4 = charSequence.charAt(i + 1);
            if (charAt4 < '0' || charAt4 > '9') {
                return i ^ -1;
            }
            int i12 = (((i11 << 1) + (i11 << 3)) + charAt4) - 48;
            int i13 = this.f18102;
            if (dateTimeParserBucket.m23217() != null) {
                i13 = dateTimeParserBucket.m23217().intValue();
            }
            int i14 = i13 - 50;
            int i15 = i14 >= 0 ? i14 % 100 : ((i14 + 1) % 100) + 99;
            if (i12 < i15) {
                i4 = 100;
            }
            dateTimeParserBucket.m23227(this.f18104, ((i4 + i14) - i15) + i12);
            return i + 2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23197(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            int r0 = m23192(j, chronology);
            if (r0 < 0) {
                appendable.append(65533);
                appendable.append(65533);
                return;
            }
            FormatUtils.m23253(appendable, r0, 2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23198(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            int r0 = m23193(readablePartial);
            if (r0 < 0) {
                appendable.append(65533);
                appendable.append(65533);
                return;
            }
            FormatUtils.m23253(appendable, r0, 2);
        }
    }

    static class UnpaddedNumber extends NumberFormatter {
        protected UnpaddedNumber(DateTimeFieldType dateTimeFieldType, int i, boolean z) {
            super(dateTimeFieldType, i, z);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m23199() {
            return this.f18080;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23200(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
            try {
                FormatUtils.m23252(appendable, this.f18082.getField(chronology).get(j));
            } catch (RuntimeException e) {
                appendable.append(65533);
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23201(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
            if (readablePartial.isSupported(this.f18082)) {
                try {
                    FormatUtils.m23252(appendable, readablePartial.get(this.f18082));
                } catch (RuntimeException e) {
                    appendable.append(65533);
                }
            } else {
                appendable.append(65533);
            }
        }
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private Object m23076() {
        Object obj = this.f18068;
        if (obj == null) {
            if (this.f18069.size() == 2) {
                Object obj2 = this.f18069.get(0);
                Object obj3 = this.f18069.get(1);
                if (obj2 == null) {
                    obj = obj3;
                } else if (obj2 == obj3 || obj3 == null) {
                    obj = obj2;
                }
            }
            if (obj == null) {
                obj = new Composite(this.f18069);
            }
            this.f18068 = obj;
        }
        return obj;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static boolean m23077(CharSequence charSequence, int i, String str) {
        char upperCase;
        char upperCase2;
        int length = str.length();
        if (charSequence.length() - i < length) {
            return false;
        }
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = charSequence.charAt(i + i2);
            char charAt2 = str.charAt(i2);
            if (charAt != charAt2 && (upperCase = Character.toUpperCase(charAt)) != (upperCase2 = Character.toUpperCase(charAt2)) && Character.toLowerCase(upperCase) != Character.toLowerCase(upperCase2)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private boolean m23078(Object obj) {
        if (!(obj instanceof InternalPrinter)) {
            return false;
        }
        if (obj instanceof Composite) {
            return ((Composite) obj).m23143();
        }
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m23079(Object obj) {
        return m23078(obj) || m23081(obj);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m23080(DateTimeParser dateTimeParser) {
        if (dateTimeParser == null) {
            throw new IllegalArgumentException("No parser supplied");
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean m23081(Object obj) {
        if (!(obj instanceof InternalParser)) {
            return false;
        }
        if (obj instanceof Composite) {
            return ((Composite) obj).m23142();
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeFormatterBuilder m23082(Object obj) {
        this.f18068 = null;
        this.f18069.add(obj);
        this.f18069.add(obj);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private DateTimeFormatterBuilder m23083(InternalPrinter internalPrinter, InternalParser internalParser) {
        this.f18068 = null;
        this.f18069.add(internalPrinter);
        this.f18069.add(internalParser);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m23084(Appendable appendable, int i) throws IOException {
        while (true) {
            i--;
            if (i >= 0) {
                appendable.append(65533);
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23085(DateTimePrinter dateTimePrinter) {
        if (dateTimePrinter == null) {
            throw new IllegalArgumentException("No printer supplied");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m23086(CharSequence charSequence, int i, String str) {
        int length = str.length();
        if (charSequence.length() - i < length) {
            return false;
        }
        for (int i2 = 0; i2 < length; i2++) {
            if (charSequence.charAt(i + i2) != str.charAt(i2)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23087() {
        return m23108(DateTimeFieldType.dayOfWeek());
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23088(int i) {
        return m23130(DateTimeFieldType.hourOfHalfday(), i, 2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23089(int i, int i2) {
        return m23130(DateTimeFieldType.yearOfEra(), i, i2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23090() {
        return m23128(DateTimeFieldType.monthOfYear());
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23091(int i) {
        return m23130(DateTimeFieldType.clockhourOfHalfday(), i, 2);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23092(int i, int i2) {
        return m23109(DateTimeFieldType.centuryOfEra(), i, i2);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23093() {
        return m23108(DateTimeFieldType.monthOfYear());
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23094(int i) {
        return m23130(DateTimeFieldType.dayOfWeek(), i, 1);
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23095(int i) {
        return m23130(DateTimeFieldType.monthOfYear(), i, 2);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23096() {
        return m23128(DateTimeFieldType.era());
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23097(int i) {
        return m23130(DateTimeFieldType.dayOfMonth(), i, 2);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23098() {
        return m23083((InternalPrinter) new TimeZoneName(0, (Map<String, DateTimeZone>) null), (InternalParser) null);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23099(int i) {
        return m23130(DateTimeFieldType.dayOfYear(), i, 3);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23100() {
        return m23083((InternalPrinter) TimeZoneId.INSTANCE, (InternalParser) TimeZoneId.INSTANCE);
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23101(int i) {
        return m23130(DateTimeFieldType.weekOfWeekyear(), i, 2);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23102() {
        return m23128(DateTimeFieldType.dayOfWeek());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23103(int i) {
        return m23130(DateTimeFieldType.clockhourOfDay(), i, 2);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23104(int i, int i2) {
        return m23109(DateTimeFieldType.year(), i, i2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23105(int i) {
        return m23130(DateTimeFieldType.secondOfMinute(), i, 2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23106(int i, int i2) {
        return m23117(DateTimeFieldType.minuteOfDay(), i, i2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23107(int i, boolean z) {
        return m23082((Object) new TwoDigitYear(DateTimeFieldType.weekyear(), i, z));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23108(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return m23082((Object) new TextField(dateTimeFieldType, true));
        }
        throw new IllegalArgumentException("Field type must not be null");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23109(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i2 = i;
        }
        if (i >= 0 && i2 > 0) {
            return i <= 1 ? m23082((Object) new UnpaddedNumber(dateTimeFieldType, i2, true)) : m23082((Object) new PaddedNumber(dateTimeFieldType, i2, true, i));
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23110(DateTimeParser dateTimeParser) {
        m23080(dateTimeParser);
        return m23083((InternalPrinter) null, (InternalParser) new MatchingParser(new InternalParser[]{DateTimeParserInternalParser.m23236(dateTimeParser), null}));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeParser m23111() {
        Object r0 = m23076();
        if (m23081(r0)) {
            return InternalParserDateTimeParser.m23356((InternalParser) r0);
        }
        throw new UnsupportedOperationException("Parsing is not supported");
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23112() {
        return m23128(DateTimeFieldType.halfdayOfDay());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23113(int i) {
        return m23130(DateTimeFieldType.hourOfDay(), i, 2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23114(int i, int i2) {
        return m23109(DateTimeFieldType.weekyear(), i, i2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23115(int i) {
        return m23130(DateTimeFieldType.minuteOfHour(), i, 2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23116(int i, int i2) {
        return m23117(DateTimeFieldType.hourOfDay(), i, i2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23117(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i2 = i;
        }
        if (i >= 0 && i2 > 0) {
            return m23082((Object) new Fraction(dateTimeFieldType, i, i2));
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23118() {
        return m23079(m23076());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m23119() {
        Object r1 = m23076();
        InternalPrinter internalPrinter = m23078(r1) ? (InternalPrinter) r1 : null;
        InternalParser internalParser = m23081(r1) ? (InternalParser) r1 : null;
        if (internalPrinter != null || internalParser != null) {
            return new DateTimeFormatter(internalPrinter, internalParser);
        }
        throw new UnsupportedOperationException("Both printing and parsing not supported");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23120(char c) {
        return m23082((Object) new CharacterLiteral(c));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23121(int i) {
        return m23130(DateTimeFieldType.millisOfSecond(), i, 3);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23122(int i, int i2) {
        return m23117(DateTimeFieldType.secondOfDay(), i, i2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23123(int i, boolean z) {
        return m23082((Object) new TwoDigitYear(DateTimeFieldType.year(), i, z));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23124(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Literal must not be null");
        }
        switch (str.length()) {
            case 0:
                return this;
            case 1:
                return m23082((Object) new CharacterLiteral(str.charAt(0)));
            default:
                return m23082((Object) new StringLiteral(str));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23125(String str, String str2, boolean z, int i, int i2) {
        return m23082((Object) new TimeZoneOffset(str, str2, z, i, i2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23126(String str, boolean z, int i, int i2) {
        return m23082((Object) new TimeZoneOffset(str, str, z, i, i2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23127(Map<String, DateTimeZone> map) {
        TimeZoneName timeZoneName = new TimeZoneName(1, map);
        return m23083((InternalPrinter) timeZoneName, (InternalParser) timeZoneName);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23128(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType != null) {
            return m23082((Object) new TextField(dateTimeFieldType, false));
        }
        throw new IllegalArgumentException("Field type must not be null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23129(DateTimeFieldType dateTimeFieldType, int i) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        } else if (i > 0) {
            return m23082((Object) new FixedNumber(dateTimeFieldType, i, false));
        } else {
            throw new IllegalArgumentException("Illegal number of digits: " + i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23130(DateTimeFieldType dateTimeFieldType, int i, int i2) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("Field type must not be null");
        }
        if (i2 < i) {
            i2 = i;
        }
        if (i >= 0 && i2 > 0) {
            return i <= 1 ? m23082((Object) new UnpaddedNumber(dateTimeFieldType, i2, false)) : m23082((Object) new PaddedNumber(dateTimeFieldType, i2, false, i));
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23131(DateTimeFormatter dateTimeFormatter) {
        if (dateTimeFormatter != null) {
            return m23083(dateTimeFormatter.m23071(), dateTimeFormatter.m23064());
        }
        throw new IllegalArgumentException("No formatter supplied");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23132(DateTimeParser dateTimeParser) {
        m23080(dateTimeParser);
        return m23083((InternalPrinter) null, DateTimeParserInternalParser.m23236(dateTimeParser));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatterBuilder m23133(DateTimePrinter dateTimePrinter, DateTimeParser[] dateTimeParserArr) {
        int i = 0;
        if (dateTimePrinter != null) {
            m23085(dateTimePrinter);
        }
        if (dateTimeParserArr == null) {
            throw new IllegalArgumentException("No parsers supplied");
        }
        int length = dateTimeParserArr.length;
        if (length != 1) {
            InternalParser[] internalParserArr = new InternalParser[length];
            while (i < length - 1) {
                InternalParser r3 = DateTimeParserInternalParser.m23236(dateTimeParserArr[i]);
                internalParserArr[i] = r3;
                if (r3 == null) {
                    throw new IllegalArgumentException("Incomplete parser array");
                }
                i++;
            }
            internalParserArr[i] = DateTimeParserInternalParser.m23236(dateTimeParserArr[i]);
            return m23083(DateTimePrinterInternalPrinter.m23245(dateTimePrinter), (InternalParser) new MatchingParser(internalParserArr));
        } else if (dateTimeParserArr[0] != null) {
            return m23083(DateTimePrinterInternalPrinter.m23245(dateTimePrinter), DateTimeParserInternalParser.m23236(dateTimeParserArr[0]));
        } else {
            throw new IllegalArgumentException("No parser supplied");
        }
    }
}
