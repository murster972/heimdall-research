package org2.joda.time.format;

import java.util.Arrays;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.DurationField;
import org2.joda.time.DurationFieldType;
import org2.joda.time.IllegalFieldValueException;
import org2.joda.time.IllegalInstantException;

public class DateTimeParserBucket {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final Integer f18105;
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeZone f18106;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public Integer f18107;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Object f18108;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean f18109;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Integer f18110;
    /* access modifiers changed from: private */

    /* renamed from: ٴ  reason: contains not printable characters */
    public SavedField[] f18111;
    /* access modifiers changed from: private */

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int f18112;

    /* renamed from: 连任  reason: contains not printable characters */
    private final DateTimeZone f18113;

    /* renamed from: 靐  reason: contains not printable characters */
    private final long f18114;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f18115;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Locale f18116;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Chronology f18117;

    static class SavedField implements Comparable<SavedField> {

        /* renamed from: 靐  reason: contains not printable characters */
        int f18118;

        /* renamed from: 麤  reason: contains not printable characters */
        Locale f18119;

        /* renamed from: 齉  reason: contains not printable characters */
        String f18120;

        /* renamed from: 龘  reason: contains not printable characters */
        DateTimeField f18121;

        SavedField() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int compareTo(SavedField savedField) {
            DateTimeField dateTimeField = savedField.f18121;
            int r0 = DateTimeParserBucket.m23208(this.f18121.getRangeDurationField(), dateTimeField.getRangeDurationField());
            return r0 != 0 ? r0 : DateTimeParserBucket.m23208(this.f18121.getDurationField(), dateTimeField.getDurationField());
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m23232(long j, boolean z) {
            long extended = this.f18120 == null ? this.f18121.setExtended(j, this.f18118) : this.f18121.set(j, this.f18120, this.f18119);
            return z ? this.f18121.roundFloor(extended) : extended;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m23233(DateTimeField dateTimeField, int i) {
            this.f18121 = dateTimeField;
            this.f18118 = i;
            this.f18120 = null;
            this.f18119 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m23234(DateTimeField dateTimeField, String str, Locale locale) {
            this.f18121 = dateTimeField;
            this.f18118 = 0;
            this.f18120 = str;
            this.f18119 = locale;
        }
    }

    class SavedState {

        /* renamed from: 靐  reason: contains not printable characters */
        final Integer f18123;

        /* renamed from: 麤  reason: contains not printable characters */
        final int f18124;

        /* renamed from: 齉  reason: contains not printable characters */
        final SavedField[] f18125;

        /* renamed from: 龘  reason: contains not printable characters */
        final DateTimeZone f18126;

        SavedState() {
            this.f18126 = DateTimeParserBucket.this.f18106;
            this.f18123 = DateTimeParserBucket.this.f18107;
            this.f18125 = DateTimeParserBucket.this.f18111;
            this.f18124 = DateTimeParserBucket.this.f18112;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23235(DateTimeParserBucket dateTimeParserBucket) {
            if (dateTimeParserBucket != DateTimeParserBucket.this) {
                return false;
            }
            DateTimeZone unused = dateTimeParserBucket.f18106 = this.f18126;
            Integer unused2 = dateTimeParserBucket.f18107 = this.f18123;
            SavedField[] unused3 = dateTimeParserBucket.f18111 = this.f18125;
            if (this.f18124 < dateTimeParserBucket.f18112) {
                boolean unused4 = dateTimeParserBucket.f18109 = true;
            }
            int unused5 = dateTimeParserBucket.f18112 = this.f18124;
            return true;
        }
    }

    public DateTimeParserBucket(long j, Chronology chronology, Locale locale, Integer num, int i) {
        Chronology r0 = DateTimeUtils.m22716(chronology);
        this.f18114 = j;
        this.f18113 = r0.getZone();
        this.f18117 = r0.withUTC();
        this.f18116 = locale == null ? Locale.getDefault() : locale;
        this.f18115 = i;
        this.f18105 = num;
        this.f18106 = this.f18113;
        this.f18110 = this.f18105;
        this.f18111 = new SavedField[8];
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private SavedField m23204() {
        SavedField[] savedFieldArr;
        SavedField savedField;
        SavedField[] savedFieldArr2 = this.f18111;
        int i = this.f18112;
        if (i == savedFieldArr2.length || this.f18109) {
            savedFieldArr = new SavedField[(i == savedFieldArr2.length ? i * 2 : savedFieldArr2.length)];
            System.arraycopy(savedFieldArr2, 0, savedFieldArr, 0, i);
            this.f18111 = savedFieldArr;
            this.f18109 = false;
        } else {
            savedFieldArr = savedFieldArr2;
        }
        this.f18108 = null;
        SavedField savedField2 = savedFieldArr[i];
        if (savedField2 == null) {
            SavedField savedField3 = new SavedField();
            savedFieldArr[i] = savedField3;
            savedField = savedField3;
        } else {
            savedField = savedField2;
        }
        this.f18112 = i + 1;
        return savedField;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m23208(DurationField durationField, DurationField durationField2) {
        if (durationField == null || !durationField.isSupported()) {
            return (durationField2 == null || !durationField2.isSupported()) ? 0 : -1;
        }
        if (durationField2 == null || !durationField2.isSupported()) {
            return 1;
        }
        return -durationField.compareTo(durationField2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23213(SavedField[] savedFieldArr, int i) {
        if (i > 10) {
            Arrays.sort(savedFieldArr, 0, i);
            return;
        }
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = i2;
            while (i3 > 0 && savedFieldArr[i3 - 1].compareTo(savedFieldArr[i3]) > 0) {
                SavedField savedField = savedFieldArr[i3];
                savedFieldArr[i3] = savedFieldArr[i3 - 1];
                savedFieldArr[i3 - 1] = savedField;
                i3--;
            }
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Object m23216() {
        if (this.f18108 == null) {
            this.f18108 = new SavedState();
        }
        return this.f18108;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Integer m23217() {
        return this.f18110;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Locale m23218() {
        return this.f18116;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Integer m23219() {
        return this.f18107;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public DateTimeZone m23220() {
        return this.f18106;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m23221(InternalParser internalParser, CharSequence charSequence) {
        int r0 = internalParser.m23355(this, charSequence, 0);
        if (r0 < 0) {
            r0 ^= -1;
        } else if (r0 >= charSequence.length()) {
            return m23222(true, charSequence);
        }
        throw new IllegalArgumentException(FormatUtils.m23251(charSequence.toString(), r0));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m23222(boolean z, CharSequence charSequence) {
        SavedField[] savedFieldArr = this.f18111;
        int i = this.f18112;
        if (this.f18109) {
            savedFieldArr = (SavedField[]) this.f18111.clone();
            this.f18111 = savedFieldArr;
            this.f18109 = false;
        }
        m23213(savedFieldArr, i);
        if (i > 0) {
            DurationField field = DurationFieldType.months().getField(this.f18117);
            DurationField field2 = DurationFieldType.days().getField(this.f18117);
            DurationField durationField = savedFieldArr[0].f18121.getDurationField();
            if (m23208(durationField, field) >= 0 && m23208(durationField, field2) <= 0) {
                m23227(DateTimeFieldType.year(), this.f18115);
                return m23222(z, charSequence);
            }
        }
        long j = this.f18114;
        int i2 = 0;
        while (i2 < i) {
            try {
                j = savedFieldArr[i2].m23232(j, z);
                i2++;
            } catch (IllegalFieldValueException e) {
                if (charSequence != null) {
                    e.prependMessage("Cannot parse \"" + charSequence + '\"');
                }
                throw e;
            }
        }
        if (z) {
            int i3 = 0;
            while (i3 < i) {
                j = savedFieldArr[i3].m23232(j, i3 == i + -1);
                i3++;
            }
        }
        long j2 = j;
        if (this.f18107 != null) {
            return j2 - ((long) this.f18107.intValue());
        }
        if (this.f18106 == null) {
            return j2;
        }
        int offsetFromLocal = this.f18106.getOffsetFromLocal(j2);
        long j3 = j2 - ((long) offsetFromLocal);
        if (offsetFromLocal == this.f18106.getOffset(j3)) {
            return j3;
        }
        String str = "Illegal instant due to time zone offset transition (" + this.f18106 + ')';
        if (charSequence != null) {
            str = "Cannot parse \"" + charSequence + "\": " + str;
        }
        throw new IllegalInstantException(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m23223(boolean z, String str) {
        return m23222(z, (CharSequence) str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Chronology m23224() {
        return this.f18117;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23225(Integer num) {
        this.f18108 = null;
        this.f18107 = num;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23226(DateTimeField dateTimeField, int i) {
        m23204().m23233(dateTimeField, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23227(DateTimeFieldType dateTimeFieldType, int i) {
        m23204().m23233(dateTimeFieldType.getField(this.f18117), i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23228(DateTimeFieldType dateTimeFieldType, String str, Locale locale) {
        m23204().m23234(dateTimeFieldType.getField(this.f18117), str, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23229(DateTimeZone dateTimeZone) {
        this.f18108 = null;
        this.f18106 = dateTimeZone;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m23230(Object obj) {
        if (!(obj instanceof SavedState) || !((SavedState) obj).m23235(this)) {
            return false;
        }
        this.f18108 = obj;
        return true;
    }
}
