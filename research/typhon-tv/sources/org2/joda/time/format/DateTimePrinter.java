package org2.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadablePartial;

public interface DateTimePrinter {
    /* renamed from: 龘  reason: contains not printable characters */
    int m23240();

    /* renamed from: 龘  reason: contains not printable characters */
    void m23241(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m23242(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    void m23243(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale);

    /* renamed from: 龘  reason: contains not printable characters */
    void m23244(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale);
}
