package org2.joda.time.format;

import java.util.Collection;
import java.util.HashSet;
import org2.joda.time.DateTimeFieldType;

public class ISODateTimeFormat {

    static final class TyphoonApp {

        /* renamed from: ʻ  reason: contains not printable characters */
        private static final DateTimeFormatter f18130 = m23292();

        /* renamed from: ʻʻ  reason: contains not printable characters */
        private static final DateTimeFormatter f18131 = m23303();
        /* access modifiers changed from: private */

        /* renamed from: ʻʼ  reason: contains not printable characters */
        public static final DateTimeFormatter f18132 = m23308();
        /* access modifiers changed from: private */

        /* renamed from: ʻʽ  reason: contains not printable characters */
        public static final DateTimeFormatter f18133 = m23320();
        /* access modifiers changed from: private */

        /* renamed from: ʻʾ  reason: contains not printable characters */
        public static final DateTimeFormatter f18134 = m23324();
        /* access modifiers changed from: private */

        /* renamed from: ʻʿ  reason: contains not printable characters */
        public static final DateTimeFormatter f18135 = m23326();

        /* renamed from: ʼ  reason: contains not printable characters */
        private static final DateTimeFormatter f18136 = m23293();

        /* renamed from: ʼʼ  reason: contains not printable characters */
        private static final DateTimeFormatter f18137 = m23331();
        /* access modifiers changed from: private */

        /* renamed from: ʽ  reason: contains not printable characters */
        public static final DateTimeFormatter f18138 = m23295();

        /* renamed from: ʽʽ  reason: contains not printable characters */
        private static final DateTimeFormatter f18139 = m23352();

        /* renamed from: ʾ  reason: contains not printable characters */
        private static final DateTimeFormatter f18140 = m23294();

        /* renamed from: ʾʾ  reason: contains not printable characters */
        private static final DateTimeFormatter f18141 = m23301();
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public static final DateTimeFormatter f18142 = m23325();

        /* renamed from: ʿʿ  reason: contains not printable characters */
        private static final DateTimeFormatter f18143 = m23333();

        /* renamed from: ˆ  reason: contains not printable characters */
        private static final DateTimeFormatter f18144 = m23350();

        /* renamed from: ˆˆ  reason: contains not printable characters */
        private static final DateTimeFormatter f18145 = m23305();

        /* renamed from: ˈ  reason: contains not printable characters */
        private static final DateTimeFormatter f18146 = m23299();

        /* renamed from: ˈˈ  reason: contains not printable characters */
        private static final DateTimeFormatter f18147 = m23309();

        /* renamed from: ˉ  reason: contains not printable characters */
        private static final DateTimeFormatter f18148 = m23281();

        /* renamed from: ˉˉ  reason: contains not printable characters */
        private static final DateTimeFormatter f18149 = m23327();
        /* access modifiers changed from: private */

        /* renamed from: ˊ  reason: contains not printable characters */
        public static final DateTimeFormatter f18150 = m23341();

        /* renamed from: ˊˊ  reason: contains not printable characters */
        private static final DateTimeFormatter f18151 = m23311();

        /* renamed from: ˋ  reason: contains not printable characters */
        private static final DateTimeFormatter f18152 = m23348();

        /* renamed from: ˋˋ  reason: contains not printable characters */
        private static final DateTimeFormatter f18153 = m23313();

        /* renamed from: ˎ  reason: contains not printable characters */
        private static final DateTimeFormatter f18154 = m23329();

        /* renamed from: ˎˎ  reason: contains not printable characters */
        private static final DateTimeFormatter f18155 = m23315();

        /* renamed from: ˏ  reason: contains not printable characters */
        private static final DateTimeFormatter f18156 = m23282();

        /* renamed from: ˏˏ  reason: contains not printable characters */
        private static final DateTimeFormatter f18157 = m23317();

        /* renamed from: ˑ  reason: contains not printable characters */
        private static final DateTimeFormatter f18158 = m23296();

        /* renamed from: ˑˑ  reason: contains not printable characters */
        private static final DateTimeFormatter f18159 = m23321();

        /* renamed from: י  reason: contains not printable characters */
        private static final DateTimeFormatter f18160 = m23283();

        /* renamed from: יי  reason: contains not printable characters */
        private static final DateTimeFormatter f18161 = m23323();

        /* renamed from: ـ  reason: contains not printable characters */
        private static final DateTimeFormatter f18162 = m23284();

        /* renamed from: ــ  reason: contains not printable characters */
        private static final DateTimeFormatter f18163 = m23307();

        /* renamed from: ٴ  reason: contains not printable characters */
        private static final DateTimeFormatter f18164 = m23297();
        /* access modifiers changed from: private */

        /* renamed from: ٴٴ  reason: contains not printable characters */
        public static final DateTimeFormatter f18165 = m23314();

        /* renamed from: ᐧ  reason: contains not printable characters */
        private static final DateTimeFormatter f18166 = m23298();

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        private static final DateTimeFormatter f18167 = m23347();

        /* renamed from: ᴵ  reason: contains not printable characters */
        private static final DateTimeFormatter f18168 = m23285();

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        private static final DateTimeFormatter f18169 = m23280();

        /* renamed from: ᵎ  reason: contains not printable characters */
        private static final DateTimeFormatter f18170 = m23286();

        /* renamed from: ᵎᵎ  reason: contains not printable characters */
        private static final DateTimeFormatter f18171 = m23337();
        /* access modifiers changed from: private */

        /* renamed from: ᵔ  reason: contains not printable characters */
        public static final DateTimeFormatter f18172 = m23332();

        /* renamed from: ᵔᵔ  reason: contains not printable characters */
        private static final DateTimeFormatter f18173 = m23319();

        /* renamed from: ᵢ  reason: contains not printable characters */
        private static final DateTimeFormatter f18174 = m23334();

        /* renamed from: ᵢᵢ  reason: contains not printable characters */
        private static final DateTimeFormatter f18175 = m23316();
        /* access modifiers changed from: private */

        /* renamed from: ⁱ  reason: contains not printable characters */
        public static final DateTimeFormatter f18176 = m23336();

        /* renamed from: ⁱⁱ  reason: contains not printable characters */
        private static final DateTimeFormatter f18177 = m23312();

        /* renamed from: 连任  reason: contains not printable characters */
        private static final DateTimeFormatter f18178 = m23291();

        /* renamed from: 靐  reason: contains not printable characters */
        private static final DateTimeFormatter f18179 = m23288();
        /* access modifiers changed from: private */

        /* renamed from: 麤  reason: contains not printable characters */
        public static final DateTimeFormatter f18180 = m23290();

        /* renamed from: 齉  reason: contains not printable characters */
        private static final DateTimeFormatter f18181 = m23289();
        /* access modifiers changed from: private */

        /* renamed from: 龘  reason: contains not printable characters */
        public static final DateTimeFormatter f18182 = m23287();

        /* renamed from: ﹳ  reason: contains not printable characters */
        private static final DateTimeFormatter f18183 = m23338();

        /* renamed from: ﹳﹳ  reason: contains not printable characters */
        private static final DateTimeFormatter f18184 = m23351();
        /* access modifiers changed from: private */

        /* renamed from: ﹶ  reason: contains not printable characters */
        public static final DateTimeFormatter f18185 = m23335();
        /* access modifiers changed from: private */

        /* renamed from: ﹶﹶ  reason: contains not printable characters */
        public static final DateTimeFormatter f18186 = m23318();

        /* renamed from: ﾞ  reason: contains not printable characters */
        private static final DateTimeFormatter f18187 = m23339();
        /* access modifiers changed from: private */

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        public static final DateTimeFormatter f18188 = m23340();

        /* renamed from: ʻʻ  reason: contains not printable characters */
        private static DateTimeFormatter m23280() {
            return f18169 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23266()).m23131(m23336()).m23119() : f18169;
        }

        /* renamed from: ʻʼ  reason: contains not printable characters */
        private static DateTimeFormatter m23281() {
            return f18148 == null ? new DateTimeFormatterBuilder().m23131(m23295()).m23131(m23296()).m23131(m23297()).m23131(m23298()).m23119() : f18148;
        }

        /* renamed from: ʻʽ  reason: contains not printable characters */
        private static DateTimeFormatter m23282() {
            return f18156 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23259()).m23131(m23294()).m23131(ISODateTimeFormat.m23262()).m23119() : f18156;
        }

        /* renamed from: ʻʾ  reason: contains not printable characters */
        private static DateTimeFormatter m23283() {
            return f18160 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23259()).m23131(m23294()).m23131(m23348()).m23119() : f18160;
        }

        /* renamed from: ʻʿ  reason: contains not printable characters */
        private static DateTimeFormatter m23284() {
            return f18162 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23259()).m23131(m23294()).m23131(m23329()).m23119() : f18162;
        }

        /* renamed from: ʻˆ  reason: contains not printable characters */
        private static DateTimeFormatter m23285() {
            return f18168 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23259()).m23131(m23294()).m23131(m23350()).m23119() : f18168;
        }

        /* renamed from: ʻˈ  reason: contains not printable characters */
        private static DateTimeFormatter m23286() {
            return f18170 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23259()).m23131(m23294()).m23131(m23281()).m23119() : f18170;
        }

        /* renamed from: ʻˉ  reason: contains not printable characters */
        private static DateTimeFormatter m23287() {
            return f18182 == null ? new DateTimeFormatterBuilder().m23104(4, 9).m23119() : f18182;
        }

        /* renamed from: ʻˊ  reason: contains not printable characters */
        private static DateTimeFormatter m23288() {
            return f18179 == null ? new DateTimeFormatterBuilder().m23120('-').m23095(2).m23119() : f18179;
        }

        /* renamed from: ʻˋ  reason: contains not printable characters */
        private static DateTimeFormatter m23289() {
            return f18181 == null ? new DateTimeFormatterBuilder().m23120('-').m23097(2).m23119() : f18181;
        }

        /* renamed from: ʻˎ  reason: contains not printable characters */
        private static DateTimeFormatter m23290() {
            return f18180 == null ? new DateTimeFormatterBuilder().m23114(4, 9).m23119() : f18180;
        }

        /* renamed from: ʻˏ  reason: contains not printable characters */
        private static DateTimeFormatter m23291() {
            return f18178 == null ? new DateTimeFormatterBuilder().m23124("-W").m23101(2).m23119() : f18178;
        }

        /* renamed from: ʻˑ  reason: contains not printable characters */
        private static DateTimeFormatter m23292() {
            return f18130 == null ? new DateTimeFormatterBuilder().m23120('-').m23094(1).m23119() : f18130;
        }

        /* renamed from: ʻי  reason: contains not printable characters */
        private static DateTimeFormatter m23293() {
            return f18136 == null ? new DateTimeFormatterBuilder().m23120('-').m23099(3).m23119() : f18136;
        }

        /* renamed from: ʻـ  reason: contains not printable characters */
        private static DateTimeFormatter m23294() {
            return f18140 == null ? new DateTimeFormatterBuilder().m23120('T').m23119() : f18140;
        }

        /* renamed from: ʻٴ  reason: contains not printable characters */
        private static DateTimeFormatter m23295() {
            return f18138 == null ? new DateTimeFormatterBuilder().m23113(2).m23119() : f18138;
        }

        /* renamed from: ʻᐧ  reason: contains not printable characters */
        private static DateTimeFormatter m23296() {
            return f18158 == null ? new DateTimeFormatterBuilder().m23120(':').m23115(2).m23119() : f18158;
        }

        /* renamed from: ʻᴵ  reason: contains not printable characters */
        private static DateTimeFormatter m23297() {
            return f18164 == null ? new DateTimeFormatterBuilder().m23120(':').m23105(2).m23119() : f18164;
        }

        /* renamed from: ʻᵎ  reason: contains not printable characters */
        private static DateTimeFormatter m23298() {
            return f18166 == null ? new DateTimeFormatterBuilder().m23120('.').m23122(3, 9).m23119() : f18166;
        }

        /* renamed from: ʻᵔ  reason: contains not printable characters */
        private static DateTimeFormatter m23299() {
            return f18146 == null ? new DateTimeFormatterBuilder().m23126("Z", true, 2, 4).m23119() : f18146;
        }

        /* renamed from: ʼʼ  reason: contains not printable characters */
        private static DateTimeFormatter m23301() {
            return f18141 == null ? new DateTimeFormatterBuilder().m23104(4, 4).m23129(DateTimeFieldType.monthOfYear(), 2).m23129(DateTimeFieldType.dayOfMonth(), 2).m23119() : f18141;
        }

        /* renamed from: ʽʽ  reason: contains not printable characters */
        private static DateTimeFormatter m23303() {
            return f18131 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23266()).m23131(m23338()).m23119() : f18131;
        }

        /* renamed from: ʾʾ  reason: contains not printable characters */
        private static DateTimeFormatter m23305() {
            return f18145 == null ? new DateTimeFormatterBuilder().m23129(DateTimeFieldType.hourOfDay(), 2).m23129(DateTimeFieldType.minuteOfHour(), 2).m23129(DateTimeFieldType.secondOfMinute(), 2).m23126("Z", false, 2, 2).m23119() : f18145;
        }

        /* renamed from: ʿʿ  reason: contains not printable characters */
        private static DateTimeFormatter m23307() {
            return f18163 == null ? new DateTimeFormatterBuilder().m23129(DateTimeFieldType.hourOfDay(), 2).m23129(DateTimeFieldType.minuteOfHour(), 2).m23129(DateTimeFieldType.secondOfMinute(), 2).m23120('.').m23122(3, 9).m23126("Z", false, 2, 2).m23119() : f18163;
        }

        /* renamed from: ˆ  reason: contains not printable characters */
        private static DateTimeFormatter m23308() {
            return f18132 == null ? new DateTimeFormatterBuilder().m23110(m23294().m23060()).m23131(m23312()).m23119().m23055() : f18132;
        }

        /* renamed from: ˆˆ  reason: contains not printable characters */
        private static DateTimeFormatter m23309() {
            return f18147 == null ? new DateTimeFormatterBuilder().m23131(m23294()).m23131(m23305()).m23119() : f18147;
        }

        /* renamed from: ˈˈ  reason: contains not printable characters */
        private static DateTimeFormatter m23311() {
            return f18151 == null ? new DateTimeFormatterBuilder().m23131(m23301()).m23131(m23309()).m23119() : f18151;
        }

        /* renamed from: ˉ  reason: contains not printable characters */
        private static DateTimeFormatter m23312() {
            if (f18177 != null) {
                return f18177;
            }
            DateTimeParser r0 = new DateTimeFormatterBuilder().m23133((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m23120('.').m23111(), new DateTimeFormatterBuilder().m23120(',').m23111()}).m23111();
            return new DateTimeFormatterBuilder().m23131(m23295()).m23133((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m23131(m23296()).m23133((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m23131(m23297()).m23110(new DateTimeFormatterBuilder().m23132(r0).m23122(1, 9).m23111()).m23111(), new DateTimeFormatterBuilder().m23132(r0).m23106(1, 9).m23111(), null}).m23111(), new DateTimeFormatterBuilder().m23132(r0).m23116(1, 9).m23111(), null}).m23119();
        }

        /* renamed from: ˉˉ  reason: contains not printable characters */
        private static DateTimeFormatter m23313() {
            return f18153 == null ? new DateTimeFormatterBuilder().m23131(m23301()).m23131(m23327()).m23119() : f18153;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        private static DateTimeFormatter m23314() {
            return f18165 == null ? m23316().m23055() : f18165;
        }

        /* renamed from: ˊˊ  reason: contains not printable characters */
        private static DateTimeFormatter m23315() {
            return f18155 == null ? new DateTimeFormatterBuilder().m23131(m23317()).m23131(m23327()).m23119() : f18155;
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        private static DateTimeFormatter m23316() {
            if (f18175 != null) {
                return f18175;
            }
            return new DateTimeFormatterBuilder().m23133((DateTimePrinter) null, new DateTimeParser[]{new DateTimeFormatterBuilder().m23131(m23287()).m23110(new DateTimeFormatterBuilder().m23131(m23288()).m23110(m23289().m23060()).m23111()).m23111(), new DateTimeFormatterBuilder().m23131(m23290()).m23131(m23291()).m23110(m23292().m23060()).m23111(), new DateTimeFormatterBuilder().m23131(m23287()).m23131(m23293()).m23111()}).m23119();
        }

        /* renamed from: ˋˋ  reason: contains not printable characters */
        private static DateTimeFormatter m23317() {
            return f18157 == null ? new DateTimeFormatterBuilder().m23104(4, 4).m23129(DateTimeFieldType.dayOfYear(), 3).m23119() : f18157;
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        private static DateTimeFormatter m23318() {
            return f18186 == null ? new DateTimeFormatterBuilder().m23110(m23294().m23060()).m23131(m23312()).m23110(m23299().m23060()).m23119() : f18186;
        }

        /* renamed from: ˎˎ  reason: contains not printable characters */
        private static DateTimeFormatter m23319() {
            return f18173 == null ? new DateTimeFormatterBuilder().m23114(4, 4).m23120('W').m23129(DateTimeFieldType.weekOfWeekyear(), 2).m23129(DateTimeFieldType.dayOfWeek(), 1).m23119() : f18173;
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        private static DateTimeFormatter m23320() {
            if (f18133 != null) {
                return f18133;
            }
            DateTimeParser r0 = new DateTimeFormatterBuilder().m23120('T').m23131(m23312()).m23110(m23299().m23060()).m23111();
            return new DateTimeFormatterBuilder().m23133((DateTimePrinter) null, new DateTimeParser[]{r0, m23324().m23060()}).m23119();
        }

        /* renamed from: ˏˏ  reason: contains not printable characters */
        private static DateTimeFormatter m23321() {
            return f18159 == null ? new DateTimeFormatterBuilder().m23131(m23317()).m23131(m23309()).m23119() : f18159;
        }

        /* renamed from: ˑˑ  reason: contains not printable characters */
        private static DateTimeFormatter m23323() {
            return f18161 == null ? new DateTimeFormatterBuilder().m23131(m23319()).m23131(m23327()).m23119() : f18161;
        }

        /* renamed from: י  reason: contains not printable characters */
        private static DateTimeFormatter m23324() {
            if (f18134 != null) {
                return f18134;
            }
            return new DateTimeFormatterBuilder().m23131(m23316()).m23110(new DateTimeFormatterBuilder().m23120('T').m23110(m23312().m23060()).m23110(m23299().m23060()).m23111()).m23119();
        }

        /* renamed from: יי  reason: contains not printable characters */
        private static DateTimeFormatter m23325() {
            return f18142 == null ? new DateTimeFormatterBuilder().m23131(m23287()).m23131(m23288()).m23119() : f18142;
        }

        /* renamed from: ـ  reason: contains not printable characters */
        private static DateTimeFormatter m23326() {
            if (f18135 != null) {
                return f18135;
            }
            return new DateTimeFormatterBuilder().m23131(m23316()).m23110(new DateTimeFormatterBuilder().m23120('T').m23131(m23312()).m23111()).m23119().m23055();
        }

        /* renamed from: ــ  reason: contains not printable characters */
        private static DateTimeFormatter m23327() {
            return f18149 == null ? new DateTimeFormatterBuilder().m23131(m23294()).m23131(m23307()).m23119() : f18149;
        }

        /* renamed from: ٴٴ  reason: contains not printable characters */
        private static DateTimeFormatter m23329() {
            return f18154 == null ? new DateTimeFormatterBuilder().m23131(m23295()).m23131(m23296()).m23131(m23297()).m23119() : f18154;
        }

        /* renamed from: ᐧᐧ  reason: contains not printable characters */
        private static DateTimeFormatter m23331() {
            return f18137 == null ? new DateTimeFormatterBuilder().m23131(m23352()).m23131(m23336()).m23119() : f18137;
        }

        /* renamed from: ᴵ  reason: contains not printable characters */
        private static DateTimeFormatter m23332() {
            return f18172 == null ? new DateTimeFormatterBuilder().m23131(m23281()).m23131(m23299()).m23119() : f18172;
        }

        /* renamed from: ᴵᴵ  reason: contains not printable characters */
        private static DateTimeFormatter m23333() {
            return f18143 == null ? new DateTimeFormatterBuilder().m23131(m23352()).m23131(m23338()).m23119() : f18143;
        }

        /* renamed from: ᵎ  reason: contains not printable characters */
        private static DateTimeFormatter m23334() {
            return f18174 == null ? new DateTimeFormatterBuilder().m23131(m23329()).m23131(m23299()).m23119() : f18174;
        }

        /* renamed from: ᵎᵎ  reason: contains not printable characters */
        private static DateTimeFormatter m23335() {
            return f18185 == null ? new DateTimeFormatterBuilder().m23131(m23287()).m23131(m23288()).m23131(m23289()).m23119() : f18185;
        }

        /* renamed from: ᵔ  reason: contains not printable characters */
        private static DateTimeFormatter m23336() {
            return f18176 == null ? new DateTimeFormatterBuilder().m23131(m23294()).m23131(m23332()).m23119() : f18176;
        }

        /* renamed from: ᵔᵔ  reason: contains not printable characters */
        private static DateTimeFormatter m23337() {
            return f18171 == null ? new DateTimeFormatterBuilder().m23131(m23319()).m23131(m23309()).m23119() : f18171;
        }

        /* renamed from: ᵢ  reason: contains not printable characters */
        private static DateTimeFormatter m23338() {
            return f18183 == null ? new DateTimeFormatterBuilder().m23131(m23294()).m23131(m23334()).m23119() : f18183;
        }

        /* renamed from: ᵢᵢ  reason: contains not printable characters */
        private static DateTimeFormatter m23339() {
            return f18187 == null ? new DateTimeFormatterBuilder().m23131(m23290()).m23131(m23291()).m23119() : f18187;
        }

        /* renamed from: ⁱ  reason: contains not printable characters */
        private static DateTimeFormatter m23340() {
            return f18188 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23259()).m23131(m23336()).m23119() : f18188;
        }

        /* renamed from: ⁱⁱ  reason: contains not printable characters */
        private static DateTimeFormatter m23341() {
            return f18150 == null ? new DateTimeFormatterBuilder().m23131(m23290()).m23131(m23291()).m23131(m23292()).m23119() : f18150;
        }

        /* renamed from: ﹳ  reason: contains not printable characters */
        private static DateTimeFormatter m23347() {
            return f18167 == null ? new DateTimeFormatterBuilder().m23131(ISODateTimeFormat.m23259()).m23131(m23338()).m23119() : f18167;
        }

        /* renamed from: ﹳﹳ  reason: contains not printable characters */
        private static DateTimeFormatter m23348() {
            return f18152 == null ? new DateTimeFormatterBuilder().m23131(m23295()).m23131(m23296()).m23119() : f18152;
        }

        /* renamed from: ﹶﹶ  reason: contains not printable characters */
        private static DateTimeFormatter m23350() {
            return f18144 == null ? new DateTimeFormatterBuilder().m23131(m23295()).m23131(m23296()).m23131(m23297()).m23120('.').m23122(3, 3).m23119() : f18144;
        }

        /* renamed from: ﾞ  reason: contains not printable characters */
        private static DateTimeFormatter m23351() {
            if (f18184 != null) {
                return f18184;
            }
            return new DateTimeFormatterBuilder().m23131(m23316()).m23110(new DateTimeFormatterBuilder().m23120('T').m23131(m23299()).m23111()).m23119();
        }

        /* renamed from: ﾞﾞ  reason: contains not printable characters */
        private static DateTimeFormatter m23352() {
            return f18139 == null ? new DateTimeFormatterBuilder().m23131(m23287()).m23131(m23293()).m23119() : f18139;
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static DateTimeFormatter m23258() {
        return TyphoonApp.f18135;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public static DateTimeFormatter m23259() {
        return m23261();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public static DateTimeFormatter m23260() {
        return TyphoonApp.f18172;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public static DateTimeFormatter m23261() {
        return TyphoonApp.f18185;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public static DateTimeFormatter m23262() {
        return TyphoonApp.f18138;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public static DateTimeFormatter m23263() {
        return TyphoonApp.f18142;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public static DateTimeFormatter m23264() {
        return TyphoonApp.f18176;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public static DateTimeFormatter m23265() {
        return TyphoonApp.f18188;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public static DateTimeFormatter m23266() {
        return TyphoonApp.f18150;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public static DateTimeFormatter m23267() {
        return TyphoonApp.f18134;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static DateTimeFormatter m23268() {
        return TyphoonApp.f18186;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static boolean m23269(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        if (collection.remove(DateTimeFieldType.year())) {
            dateTimeFormatterBuilder.m23131(TyphoonApp.f18182);
            if (!collection.remove(DateTimeFieldType.dayOfYear())) {
                return true;
            }
            m23277(dateTimeFormatterBuilder, z);
            dateTimeFormatterBuilder.m23099(3);
            return false;
        } else if (!collection.remove(DateTimeFieldType.dayOfYear())) {
            return false;
        } else {
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23099(3);
            return false;
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static DateTimeFormatter m23270() {
        return TyphoonApp.f18133;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static DateTimeFormatter m23271() {
        return TyphoonApp.f18132;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static boolean m23272(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        if (collection.remove(DateTimeFieldType.weekyear())) {
            dateTimeFormatterBuilder.m23131(TyphoonApp.f18180);
            if (collection.remove(DateTimeFieldType.weekOfWeekyear())) {
                m23277(dateTimeFormatterBuilder, z);
                dateTimeFormatterBuilder.m23120('W');
                dateTimeFormatterBuilder.m23101(2);
                if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
                    return true;
                }
                m23277(dateTimeFormatterBuilder, z);
                dateTimeFormatterBuilder.m23094(1);
                return false;
            } else if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
                return true;
            } else {
                m23275(collection, z2);
                m23277(dateTimeFormatterBuilder, z);
                dateTimeFormatterBuilder.m23120('W');
                dateTimeFormatterBuilder.m23120('-');
                dateTimeFormatterBuilder.m23094(1);
                return false;
            }
        } else if (collection.remove(DateTimeFieldType.weekOfWeekyear())) {
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23120('W');
            dateTimeFormatterBuilder.m23101(2);
            if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
                return true;
            }
            m23277(dateTimeFormatterBuilder, z);
            dateTimeFormatterBuilder.m23094(1);
            return false;
        } else if (!collection.remove(DateTimeFieldType.dayOfWeek())) {
            return false;
        } else {
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23120('W');
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23094(1);
            return false;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeFormatter m23273() {
        return TyphoonApp.f18165;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeFormatter m23274(Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        boolean z3;
        if (collection == null || collection.size() == 0) {
            throw new IllegalArgumentException("The fields must not be null or empty");
        }
        HashSet hashSet = new HashSet(collection);
        int size = hashSet.size();
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        if (hashSet.contains(DateTimeFieldType.monthOfYear())) {
            z3 = m23278(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.dayOfYear())) {
            z3 = m23269(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.weekOfWeekyear())) {
            z3 = m23272(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.dayOfMonth())) {
            z3 = m23278(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.contains(DateTimeFieldType.dayOfWeek())) {
            z3 = m23272(dateTimeFormatterBuilder, hashSet, z, z2);
        } else if (hashSet.remove(DateTimeFieldType.year())) {
            dateTimeFormatterBuilder.m23131(TyphoonApp.f18182);
            z3 = true;
        } else if (hashSet.remove(DateTimeFieldType.weekyear())) {
            dateTimeFormatterBuilder.m23131(TyphoonApp.f18180);
            z3 = true;
        } else {
            z3 = false;
        }
        m23276(dateTimeFormatterBuilder, hashSet, z, z2, z3, hashSet.size() < size);
        if (!dateTimeFormatterBuilder.m23118()) {
            throw new IllegalArgumentException("No valid format for fields: " + collection);
        }
        try {
            collection.retainAll(hashSet);
        } catch (UnsupportedOperationException e) {
        }
        return dateTimeFormatterBuilder.m23119();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23275(Collection<DateTimeFieldType> collection, boolean z) {
        if (z) {
            throw new IllegalArgumentException("No valid ISO8601 format for fields: " + collection);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23276(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2, boolean z3, boolean z4) {
        boolean remove = collection.remove(DateTimeFieldType.hourOfDay());
        boolean remove2 = collection.remove(DateTimeFieldType.minuteOfHour());
        boolean remove3 = collection.remove(DateTimeFieldType.secondOfMinute());
        boolean remove4 = collection.remove(DateTimeFieldType.millisOfSecond());
        if (remove || remove2 || remove3 || remove4) {
            if (remove || remove2 || remove3 || remove4) {
                if (z2 && z3) {
                    throw new IllegalArgumentException("No valid ISO8601 format for fields because Date was reduced precision: " + collection);
                } else if (z4) {
                    dateTimeFormatterBuilder.m23120('T');
                }
            }
            if ((!remove || !remove2 || !remove3) && (!remove || remove3 || remove4)) {
                if (z2 && z4) {
                    throw new IllegalArgumentException("No valid ISO8601 format for fields because Time was truncated: " + collection);
                } else if ((remove || ((!remove2 || !remove3) && ((!remove2 || remove4) && !remove3))) && z2) {
                    throw new IllegalArgumentException("No valid ISO8601 format for fields: " + collection);
                }
            }
            if (remove) {
                dateTimeFormatterBuilder.m23113(2);
            } else if (remove2 || remove3 || remove4) {
                dateTimeFormatterBuilder.m23120('-');
            }
            if (z && remove && remove2) {
                dateTimeFormatterBuilder.m23120(':');
            }
            if (remove2) {
                dateTimeFormatterBuilder.m23115(2);
            } else if (remove3 || remove4) {
                dateTimeFormatterBuilder.m23120('-');
            }
            if (z && remove2 && remove3) {
                dateTimeFormatterBuilder.m23120(':');
            }
            if (remove3) {
                dateTimeFormatterBuilder.m23105(2);
            } else if (remove4) {
                dateTimeFormatterBuilder.m23120('-');
            }
            if (remove4) {
                dateTimeFormatterBuilder.m23120('.');
                dateTimeFormatterBuilder.m23121(3);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23277(DateTimeFormatterBuilder dateTimeFormatterBuilder, boolean z) {
        if (z) {
            dateTimeFormatterBuilder.m23120('-');
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m23278(DateTimeFormatterBuilder dateTimeFormatterBuilder, Collection<DateTimeFieldType> collection, boolean z, boolean z2) {
        if (collection.remove(DateTimeFieldType.year())) {
            dateTimeFormatterBuilder.m23131(TyphoonApp.f18182);
            if (collection.remove(DateTimeFieldType.monthOfYear())) {
                if (collection.remove(DateTimeFieldType.dayOfMonth())) {
                    m23277(dateTimeFormatterBuilder, z);
                    dateTimeFormatterBuilder.m23095(2);
                    m23277(dateTimeFormatterBuilder, z);
                    dateTimeFormatterBuilder.m23097(2);
                    return false;
                }
                dateTimeFormatterBuilder.m23120('-');
                dateTimeFormatterBuilder.m23095(2);
                return true;
            } else if (!collection.remove(DateTimeFieldType.dayOfMonth())) {
                return true;
            } else {
                m23275(collection, z2);
                dateTimeFormatterBuilder.m23120('-');
                dateTimeFormatterBuilder.m23120('-');
                dateTimeFormatterBuilder.m23097(2);
                return false;
            }
        } else if (collection.remove(DateTimeFieldType.monthOfYear())) {
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23095(2);
            if (!collection.remove(DateTimeFieldType.dayOfMonth())) {
                return true;
            }
            m23277(dateTimeFormatterBuilder, z);
            dateTimeFormatterBuilder.m23097(2);
            return false;
        } else if (!collection.remove(DateTimeFieldType.dayOfMonth())) {
            return false;
        } else {
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23120('-');
            dateTimeFormatterBuilder.m23097(2);
            return false;
        }
    }
}
