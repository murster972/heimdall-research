package org2.joda.time.format;

import java.util.Locale;
import org2.joda.time.MutablePeriod;
import org2.joda.time.Period;
import org2.joda.time.PeriodType;
import org2.joda.time.ReadWritablePeriod;
import org2.joda.time.ReadablePeriod;

public class PeriodFormatter {

    /* renamed from: 靐  reason: contains not printable characters */
    private final PeriodParser f18192;

    /* renamed from: 麤  reason: contains not printable characters */
    private final PeriodType f18193;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Locale f18194;

    /* renamed from: 龘  reason: contains not printable characters */
    private final PeriodPrinter f18195;

    public PeriodFormatter(PeriodPrinter periodPrinter, PeriodParser periodParser) {
        this.f18195 = periodPrinter;
        this.f18192 = periodParser;
        this.f18194 = null;
        this.f18193 = null;
    }

    PeriodFormatter(PeriodPrinter periodPrinter, PeriodParser periodParser, Locale locale, PeriodType periodType) {
        this.f18195 = periodPrinter;
        this.f18192 = periodParser;
        this.f18194 = locale;
        this.f18193 = periodType;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m23370(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            throw new IllegalArgumentException("Period must not be null");
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m23371() {
        if (this.f18192 == null) {
            throw new UnsupportedOperationException("Parsing not supported");
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m23372() {
        if (this.f18195 == null) {
            throw new UnsupportedOperationException("Printing not supported");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public MutablePeriod m23373(String str) {
        m23371();
        MutablePeriod mutablePeriod = new MutablePeriod(0, this.f18193);
        int r0 = m23374().m23446(mutablePeriod, str, 0, this.f18194);
        if (r0 < 0) {
            r0 ^= -1;
        } else if (r0 >= str.length()) {
            return mutablePeriod;
        }
        throw new IllegalArgumentException(FormatUtils.m23251(str, r0));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public PeriodParser m23374() {
        return this.f18192;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23375(ReadWritablePeriod readWritablePeriod, String str, int i) {
        m23371();
        m23370((ReadablePeriod) readWritablePeriod);
        return m23374().m23446(readWritablePeriod, str, i, this.f18194);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23376(ReadablePeriod readablePeriod) {
        m23372();
        m23370(readablePeriod);
        PeriodPrinter r0 = m23379();
        StringBuffer stringBuffer = new StringBuffer(r0.m23448(readablePeriod, this.f18194));
        r0.m23449(stringBuffer, readablePeriod, this.f18194);
        return stringBuffer.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Period m23377(String str) {
        m23371();
        return m23373(str).toPeriod();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodFormatter m23378(PeriodType periodType) {
        return periodType == this.f18193 ? this : new PeriodFormatter(this.f18195, this.f18192, this.f18194, periodType);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PeriodPrinter m23379() {
        return this.f18195;
    }
}
