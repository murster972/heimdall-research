package org2.joda.time.format;

import java.io.IOException;

public class FormatUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final double f18129 = Math.log(10.0d);

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m23249(long j) {
        if (j < 0) {
            if (j != Long.MIN_VALUE) {
                return m23249(-j) + 1;
            }
            return 20;
        } else if (j < 10) {
            return 1;
        } else {
            if (j < 100) {
                return 2;
            }
            if (j < 1000) {
                return 3;
            }
            if (j < 10000) {
                return 4;
            }
            return ((int) (Math.log((double) j) / f18129)) + 1;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m23250(CharSequence charSequence, int i) {
        int charAt = charSequence.charAt(i) - '0';
        return (((charAt << 1) + (charAt << 3)) + charSequence.charAt(i + 1)) - 48;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m23251(String str, int i) {
        int i2 = i + 32;
        String concat = str.length() <= i2 + 3 ? str : str.substring(0, i2).concat("...");
        return i <= 0 ? "Invalid format: \"" + concat + '\"' : i >= str.length() ? "Invalid format: \"" + concat + "\" is too short" : "Invalid format: \"" + concat + "\" is malformed at \"" + concat.substring(i) + '\"';
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23252(Appendable appendable, int i) throws IOException {
        if (i < 0) {
            appendable.append('-');
            if (i != Integer.MIN_VALUE) {
                i = -i;
            } else {
                appendable.append("2147483648");
                return;
            }
        }
        if (i < 10) {
            appendable.append((char) (i + 48));
        } else if (i < 100) {
            int i2 = ((i + 1) * 13421772) >> 27;
            appendable.append((char) (i2 + 48));
            appendable.append((char) (((i - (i2 << 3)) - (i2 << 1)) + 48));
        } else {
            appendable.append(Integer.toString(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23253(Appendable appendable, int i, int i2) throws IOException {
        if (i < 0) {
            appendable.append('-');
            if (i != Integer.MIN_VALUE) {
                i = -i;
            } else {
                while (i2 > 10) {
                    appendable.append('0');
                    i2--;
                }
                appendable.append("2147483648");
                return;
            }
        }
        if (i < 10) {
            while (i2 > 1) {
                appendable.append('0');
                i2--;
            }
            appendable.append((char) (i + 48));
        } else if (i < 100) {
            while (i2 > 2) {
                appendable.append('0');
                i2--;
            }
            int i3 = ((i + 1) * 13421772) >> 27;
            appendable.append((char) (i3 + 48));
            appendable.append((char) (((i - (i3 << 3)) - (i3 << 1)) + 48));
        } else {
            int log = i < 1000 ? 3 : i < 10000 ? 4 : ((int) (Math.log((double) i) / f18129)) + 1;
            while (i2 > log) {
                appendable.append('0');
                i2--;
            }
            appendable.append(Integer.toString(i));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23254(Appendable appendable, long j) throws IOException {
        int i = (int) j;
        if (((long) i) == j) {
            m23252(appendable, i);
        } else {
            appendable.append(Long.toString(j));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23255(StringBuffer stringBuffer, int i) {
        try {
            m23252((Appendable) stringBuffer, i);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23256(StringBuffer stringBuffer, int i, int i2) {
        try {
            m23253((Appendable) stringBuffer, i, i2);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23257(StringBuffer stringBuffer, long j) {
        try {
            m23254((Appendable) stringBuffer, j);
        } catch (IOException e) {
        }
    }
}
