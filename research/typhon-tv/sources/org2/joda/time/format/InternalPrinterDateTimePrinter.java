package org2.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeZone;
import org2.joda.time.ReadablePartial;

class InternalPrinterDateTimePrinter implements DateTimePrinter, InternalPrinter {

    /* renamed from: 龘  reason: contains not printable characters */
    private final InternalPrinter f18191;

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof InternalPrinterDateTimePrinter) {
            return this.f18191.equals(((InternalPrinterDateTimePrinter) obj).f18191);
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23363() {
        return this.f18191.m23360();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23364(Writer writer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
        this.f18191.m23361(writer, j, chronology, i, dateTimeZone, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23365(Writer writer, ReadablePartial readablePartial, Locale locale) throws IOException {
        this.f18191.m23362(writer, readablePartial, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23366(Appendable appendable, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) throws IOException {
        this.f18191.m23361(appendable, j, chronology, i, dateTimeZone, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23367(Appendable appendable, ReadablePartial readablePartial, Locale locale) throws IOException {
        this.f18191.m23362(appendable, readablePartial, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23368(StringBuffer stringBuffer, long j, Chronology chronology, int i, DateTimeZone dateTimeZone, Locale locale) {
        try {
            this.f18191.m23361(stringBuffer, j, chronology, i, dateTimeZone, locale);
        } catch (IOException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23369(StringBuffer stringBuffer, ReadablePartial readablePartial, Locale locale) {
        try {
            this.f18191.m23362(stringBuffer, readablePartial, locale);
        } catch (IOException e) {
        }
    }
}
