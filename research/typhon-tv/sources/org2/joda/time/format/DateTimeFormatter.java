package org2.joda.time.format;

import java.io.IOException;
import java.util.Locale;
import org2.joda.time.Chronology;
import org2.joda.time.DateTime;
import org2.joda.time.DateTimeUtils;
import org2.joda.time.DateTimeZone;
import org2.joda.time.LocalDate;
import org2.joda.time.LocalDateTime;
import org2.joda.time.LocalTime;
import org2.joda.time.ReadableInstant;
import org2.joda.time.ReadablePartial;

public class DateTimeFormatter {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final DateTimeZone f18060;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Integer f18061;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final int f18062;

    /* renamed from: 连任  reason: contains not printable characters */
    private final Chronology f18063;

    /* renamed from: 靐  reason: contains not printable characters */
    private final InternalParser f18064;

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f18065;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Locale f18066;

    /* renamed from: 龘  reason: contains not printable characters */
    private final InternalPrinter f18067;

    DateTimeFormatter(InternalPrinter internalPrinter, InternalParser internalParser) {
        this.f18067 = internalPrinter;
        this.f18064 = internalParser;
        this.f18066 = null;
        this.f18065 = false;
        this.f18063 = null;
        this.f18060 = null;
        this.f18061 = null;
        this.f18062 = 2000;
    }

    private DateTimeFormatter(InternalPrinter internalPrinter, InternalParser internalParser, Locale locale, boolean z, Chronology chronology, DateTimeZone dateTimeZone, Integer num, int i) {
        this.f18067 = internalPrinter;
        this.f18064 = internalParser;
        this.f18066 = locale;
        this.f18065 = z;
        this.f18063 = chronology;
        this.f18060 = dateTimeZone;
        this.f18061 = num;
        this.f18062 = i;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private InternalPrinter m23051() {
        InternalPrinter internalPrinter = this.f18067;
        if (internalPrinter != null) {
            return internalPrinter;
        }
        throw new UnsupportedOperationException("Printing not supported");
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private InternalParser m23052() {
        InternalParser internalParser = this.f18064;
        if (internalParser != null) {
            return internalParser;
        }
        throw new UnsupportedOperationException("Parsing not supported");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Chronology m23053(Chronology chronology) {
        Chronology r0 = DateTimeUtils.m22716(chronology);
        if (this.f18063 != null) {
            r0 = this.f18063;
        }
        return this.f18060 != null ? r0.withZone(this.f18060) : r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23054(Appendable appendable, long j, Chronology chronology) throws IOException {
        InternalPrinter r2 = m23051();
        Chronology r3 = m23053(chronology);
        DateTimeZone zone = r3.getZone();
        int offset = zone.getOffset(j);
        long j2 = ((long) offset) + j;
        if ((j ^ j2) < 0 && (((long) offset) ^ j) >= 0) {
            zone = DateTimeZone.UTC;
            offset = 0;
            j2 = j;
        }
        r2.m23361(appendable, j2, r3.withUTC(), offset, zone, this.f18066);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public DateTimeFormatter m23055() {
        return m23070(DateTimeZone.UTC);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public DateTimeZone m23056() {
        return this.f18060;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTime m23057(String str) {
        InternalParser r0 = m23052();
        Chronology r4 = m23053((Chronology) null);
        DateTimeParserBucket dateTimeParserBucket = new DateTimeParserBucket(0, r4, this.f18066, this.f18061, this.f18062);
        int r02 = r0.m23355(dateTimeParserBucket, str, 0);
        if (r02 < 0) {
            r02 ^= -1;
        } else if (r02 >= str.length()) {
            long r2 = dateTimeParserBucket.m23223(true, str);
            if (this.f18065 && dateTimeParserBucket.m23219() != null) {
                r4 = r4.withZone(DateTimeZone.forOffsetMillis(dateTimeParserBucket.m23219().intValue()));
            } else if (dateTimeParserBucket.m23220() != null) {
                r4 = r4.withZone(dateTimeParserBucket.m23220());
            }
            DateTime dateTime = new DateTime(r2, r4);
            return this.f18060 != null ? dateTime.withZone(this.f18060) : dateTime;
        }
        throw new IllegalArgumentException(FormatUtils.m23251(str, r02));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public DateTimeFormatter m23058() {
        return this.f18065 ? this : new DateTimeFormatter(this.f18067, this.f18064, this.f18066, true, this.f18063, (DateTimeZone) null, this.f18061, this.f18062);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public LocalDate m23059(String str) {
        return m23062(str).toLocalDate();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public DateTimeParser m23060() {
        return InternalParserDateTimeParser.m23356(this.f18064);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Locale m23061() {
        return this.f18066;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public LocalDateTime m23062(String str) {
        InternalParser r0 = m23052();
        Chronology withUTC = m23053((Chronology) null).withUTC();
        DateTimeParserBucket dateTimeParserBucket = new DateTimeParserBucket(0, withUTC, this.f18066, this.f18061, this.f18062);
        int r02 = r0.m23355(dateTimeParserBucket, str, 0);
        if (r02 < 0) {
            r02 ^= -1;
        } else if (r02 >= str.length()) {
            long r2 = dateTimeParserBucket.m23223(true, str);
            if (dateTimeParserBucket.m23219() != null) {
                withUTC = withUTC.withZone(DateTimeZone.forOffsetMillis(dateTimeParserBucket.m23219().intValue()));
            } else if (dateTimeParserBucket.m23220() != null) {
                withUTC = withUTC.withZone(dateTimeParserBucket.m23220());
            }
            return new LocalDateTime(r2, withUTC);
        }
        throw new IllegalArgumentException(FormatUtils.m23251(str, r02));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public LocalTime m23063(String str) {
        return m23062(str).toLocalTime();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public InternalParser m23064() {
        return this.f18064;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m23065(String str) {
        return new DateTimeParserBucket(0, m23053(this.f18063), this.f18066, this.f18061, this.f18062).m23221(m23052(), (CharSequence) str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23066(ReadableInstant readableInstant) {
        StringBuilder sb = new StringBuilder(m23051().m23360());
        try {
            m23073((Appendable) sb, readableInstant);
        } catch (IOException e) {
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23067(ReadablePartial readablePartial) {
        StringBuilder sb = new StringBuilder(m23051().m23360());
        try {
            m23074((Appendable) sb, readablePartial);
        } catch (IOException e) {
        }
        return sb.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m23068(Locale locale) {
        if (locale == m23061() || (locale != null && locale.equals(m23061()))) {
            return this;
        }
        return new DateTimeFormatter(this.f18067, this.f18064, locale, this.f18065, this.f18063, this.f18060, this.f18061, this.f18062);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m23069(Chronology chronology) {
        if (this.f18063 == chronology) {
            return this;
        }
        return new DateTimeFormatter(this.f18067, this.f18064, this.f18066, this.f18065, chronology, this.f18060, this.f18061, this.f18062);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeFormatter m23070(DateTimeZone dateTimeZone) {
        if (this.f18060 == dateTimeZone) {
            return this;
        }
        return new DateTimeFormatter(this.f18067, this.f18064, this.f18066, false, this.f18063, dateTimeZone, this.f18061, this.f18062);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public InternalPrinter m23071() {
        return this.f18067;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23072(Appendable appendable, long j) throws IOException {
        m23054(appendable, j, (Chronology) null);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23073(Appendable appendable, ReadableInstant readableInstant) throws IOException {
        m23054(appendable, DateTimeUtils.m22714(readableInstant), DateTimeUtils.m22709(readableInstant));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23074(Appendable appendable, ReadablePartial readablePartial) throws IOException {
        InternalPrinter r0 = m23051();
        if (readablePartial == null) {
            throw new IllegalArgumentException("The partial must not be null");
        }
        r0.m23362(appendable, readablePartial, this.f18066);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23075(StringBuffer stringBuffer, long j) {
        try {
            m23072((Appendable) stringBuffer, j);
        } catch (IOException e) {
        }
    }
}
