package org2.joda.time.format;

class DateTimeParserInternalParser implements InternalParser {

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeParser f18127;

    private DateTimeParserInternalParser(DateTimeParser dateTimeParser) {
        this.f18127 = dateTimeParser;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static InternalParser m23236(DateTimeParser dateTimeParser) {
        if (dateTimeParser instanceof InternalParserDateTimeParser) {
            return (InternalParser) dateTimeParser;
        }
        if (dateTimeParser == null) {
            return null;
        }
        return new DateTimeParserInternalParser(dateTimeParser);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m23237() {
        return this.f18127.m23202();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23238(DateTimeParserBucket dateTimeParserBucket, CharSequence charSequence, int i) {
        return this.f18127.m23203(dateTimeParserBucket, charSequence.toString(), i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeParser m23239() {
        return this.f18127;
    }
}
