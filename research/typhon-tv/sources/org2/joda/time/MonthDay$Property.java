package org2.joda.time;

import java.io.Serializable;
import java.util.Locale;
import org2.joda.time.field.AbstractPartialFieldProperty;

public class MonthDay$Property extends AbstractPartialFieldProperty implements Serializable {
    private static final long serialVersionUID = 5727734012190224363L;
    private final MonthDay iBase;
    private final int iFieldIndex;

    MonthDay$Property(MonthDay monthDay, int i) {
        this.iBase = monthDay;
        this.iFieldIndex = i;
    }

    public MonthDay addToCopy(int i) {
        return new MonthDay(this.iBase, getField().add(this.iBase, this.iFieldIndex, this.iBase.getValues(), i));
    }

    public MonthDay addWrapFieldToCopy(int i) {
        return new MonthDay(this.iBase, getField().addWrapField(this.iBase, this.iFieldIndex, this.iBase.getValues(), i));
    }

    public int get() {
        return this.iBase.getValue(this.iFieldIndex);
    }

    public DateTimeField getField() {
        return this.iBase.getField(this.iFieldIndex);
    }

    public MonthDay getMonthDay() {
        return this.iBase;
    }

    public MonthDay setCopy(int i) {
        return new MonthDay(this.iBase, getField().set(this.iBase, this.iFieldIndex, this.iBase.getValues(), i));
    }

    public MonthDay setCopy(String str) {
        return setCopy(str, (Locale) null);
    }

    public MonthDay setCopy(String str, Locale locale) {
        return new MonthDay(this.iBase, getField().set(this.iBase, this.iFieldIndex, this.iBase.getValues(), str, locale));
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public ReadablePartial m22736() {
        return this.iBase;
    }
}
