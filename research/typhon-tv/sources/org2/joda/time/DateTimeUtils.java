package org2.joda.time;

import java.text.DateFormatSymbols;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.time.TimeZones;
import org2.joda.time.chrono.ISOChronology;

public class DateTimeUtils {

    /* renamed from: 靐  reason: contains not printable characters */
    private static volatile MillisProvider f17792 = f17794;

    /* renamed from: 齉  reason: contains not printable characters */
    private static final AtomicReference<Map<String, DateTimeZone>> f17793 = new AtomicReference<>();

    /* renamed from: 龘  reason: contains not printable characters */
    public static final MillisProvider f17794 = new SystemMillisProvider();

    public interface MillisProvider {
        /* renamed from: 龘  reason: contains not printable characters */
        long m22723();
    }

    static class SystemMillisProvider implements MillisProvider {
        SystemMillisProvider() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m22724() {
            return System.currentTimeMillis();
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Map<String, DateTimeZone> m22708() {
        Map<String, DateTimeZone> map = f17793.get();
        if (map != null) {
            return map;
        }
        Map<String, DateTimeZone> r0 = m22711();
        return !f17793.compareAndSet((Object) null, r0) ? f17793.get() : r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final Chronology m22709(ReadableInstant readableInstant) {
        if (readableInstant == null) {
            return ISOChronology.getInstance();
        }
        Chronology chronology = readableInstant.getChronology();
        return chronology == null ? ISOChronology.getInstance() : chronology;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ReadableInterval m22710(ReadableInterval readableInterval) {
        if (readableInterval != null) {
            return readableInterval;
        }
        long r0 = m22712();
        return new Interval(r0, r0);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private static Map<String, DateTimeZone> m22711() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("UT", DateTimeZone.UTC);
        linkedHashMap.put("UTC", DateTimeZone.UTC);
        linkedHashMap.put(TimeZones.GMT_ID, DateTimeZone.UTC);
        m22721(linkedHashMap, "EST", "America/New_York");
        m22721(linkedHashMap, "EDT", "America/New_York");
        m22721(linkedHashMap, "CST", "America/Chicago");
        m22721(linkedHashMap, "CDT", "America/Chicago");
        m22721(linkedHashMap, "MST", "America/Denver");
        m22721(linkedHashMap, "MDT", "America/Denver");
        m22721(linkedHashMap, "PST", "America/Los_Angeles");
        m22721(linkedHashMap, "PDT", "America/Los_Angeles");
        return Collections.unmodifiableMap(linkedHashMap);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final long m22712() {
        return f17792.m22723();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final long m22713(ReadableDuration readableDuration) {
        if (readableDuration == null) {
            return 0;
        }
        return readableDuration.getMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final long m22714(ReadableInstant readableInstant) {
        return readableInstant == null ? m22712() : readableInstant.getMillis();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final DateFormatSymbols m22715(Locale locale) {
        try {
            return (DateFormatSymbols) DateFormatSymbols.class.getMethod("getInstance", new Class[]{Locale.class}).invoke((Object) null, new Object[]{locale});
        } catch (Exception e) {
            return new DateFormatSymbols(locale);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Chronology m22716(Chronology chronology) {
        return chronology == null ? ISOChronology.getInstance() : chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Chronology m22717(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        Chronology chronology = null;
        if (readableInstant != null) {
            chronology = readableInstant.getChronology();
        } else if (readableInstant2 != null) {
            chronology = readableInstant2.getChronology();
        }
        return chronology == null ? ISOChronology.getInstance() : chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final Chronology m22718(ReadableInterval readableInterval) {
        if (readableInterval == null) {
            return ISOChronology.getInstance();
        }
        Chronology chronology = readableInterval.getChronology();
        return chronology == null ? ISOChronology.getInstance() : chronology;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final DateTimeZone m22719(DateTimeZone dateTimeZone) {
        return dateTimeZone == null ? DateTimeZone.getDefault() : dateTimeZone;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final PeriodType m22720(PeriodType periodType) {
        return periodType == null ? PeriodType.standard() : periodType;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m22721(Map<String, DateTimeZone> map, String str, String str2) {
        try {
            map.put(str, DateTimeZone.forID(str2));
        } catch (RuntimeException e) {
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final boolean m22722(ReadablePartial readablePartial) {
        if (readablePartial == null) {
            throw new IllegalArgumentException("Partial must not be null");
        }
        DurationFieldType durationFieldType = null;
        for (int i = 0; i < readablePartial.size(); i++) {
            DateTimeField field = readablePartial.getField(i);
            if (i > 0 && (field.getRangeDurationField() == null || field.getRangeDurationField().getType() != durationFieldType)) {
                return false;
            }
            durationFieldType = field.getDurationField().getType();
        }
        return true;
    }
}
