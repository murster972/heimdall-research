package org2.joda.time;

import java.io.Serializable;
import org2.joda.time.base.BaseInterval;
import org2.joda.time.field.FieldUtils;

public class MutableInterval extends BaseInterval implements Serializable, Cloneable, ReadWritableInterval {
    private static final long serialVersionUID = -5982824024992428470L;

    public MutableInterval() {
        super(0, 0, (Chronology) null);
    }

    public MutableInterval(long j, long j2) {
        super(j, j2, (Chronology) null);
    }

    public MutableInterval(long j, long j2, Chronology chronology) {
        super(j, j2, chronology);
    }

    public MutableInterval(Object obj) {
        super(obj, (Chronology) null);
    }

    public MutableInterval(Object obj, Chronology chronology) {
        super(obj, chronology);
    }

    public MutableInterval(ReadableDuration readableDuration, ReadableInstant readableInstant) {
        super(readableDuration, readableInstant);
    }

    public MutableInterval(ReadableInstant readableInstant, ReadableDuration readableDuration) {
        super(readableInstant, readableDuration);
    }

    public MutableInterval(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        super(readableInstant, readableInstant2);
    }

    public MutableInterval(ReadableInstant readableInstant, ReadablePeriod readablePeriod) {
        super(readableInstant, readablePeriod);
    }

    public MutableInterval(ReadablePeriod readablePeriod, ReadableInstant readableInstant) {
        super(readablePeriod, readableInstant);
    }

    public static MutableInterval parse(String str) {
        return new MutableInterval(str);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError("Clone error");
        }
    }

    public MutableInterval copy() {
        return (MutableInterval) clone();
    }

    public void setChronology(Chronology chronology) {
        super.m22754(getStartMillis(), getEndMillis(), chronology);
    }

    public void setDurationAfterStart(long j) {
        setEndMillis(FieldUtils.m23036(getStartMillis(), j));
    }

    public void setDurationAfterStart(ReadableDuration readableDuration) {
        setEndMillis(FieldUtils.m23036(getStartMillis(), DateTimeUtils.m22713(readableDuration)));
    }

    public void setDurationBeforeEnd(long j) {
        setStartMillis(FieldUtils.m23036(getEndMillis(), -j));
    }

    public void setDurationBeforeEnd(ReadableDuration readableDuration) {
        setStartMillis(FieldUtils.m23036(getEndMillis(), -DateTimeUtils.m22713(readableDuration)));
    }

    public void setEnd(ReadableInstant readableInstant) {
        super.m22754(getStartMillis(), DateTimeUtils.m22714(readableInstant), getChronology());
    }

    public void setEndMillis(long j) {
        super.m22754(getStartMillis(), j, getChronology());
    }

    public void setInterval(long j, long j2) {
        super.m22754(j, j2, getChronology());
    }

    public void setInterval(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        if (readableInstant == null && readableInstant2 == null) {
            long r0 = DateTimeUtils.m22712();
            setInterval(r0, r0);
            return;
        }
        super.m22754(DateTimeUtils.m22714(readableInstant), DateTimeUtils.m22714(readableInstant2), DateTimeUtils.m22709(readableInstant));
    }

    public void setInterval(ReadableInterval readableInterval) {
        if (readableInterval == null) {
            throw new IllegalArgumentException("Interval must not be null");
        }
        super.m22754(readableInterval.getStartMillis(), readableInterval.getEndMillis(), readableInterval.getChronology());
    }

    public void setPeriodAfterStart(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            setEndMillis(getStartMillis());
        } else {
            setEndMillis(getChronology().add(readablePeriod, getStartMillis(), 1));
        }
    }

    public void setPeriodBeforeEnd(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            setStartMillis(getEndMillis());
        } else {
            setStartMillis(getChronology().add(readablePeriod, getEndMillis(), -1));
        }
    }

    public void setStart(ReadableInstant readableInstant) {
        super.m22754(DateTimeUtils.m22714(readableInstant), getEndMillis(), getChronology());
    }

    public void setStartMillis(long j) {
        super.m22754(j, getEndMillis(), getChronology());
    }
}
