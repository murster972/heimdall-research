package org2.joda.time.field;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.ReadablePartial;

public final class ZeroIsMaxDateTimeField extends DecoratedDateTimeField {
    public ZeroIsMaxDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType) {
        super(dateTimeField, dateTimeFieldType);
        if (dateTimeField.getMinimumValue() != 0) {
            throw new IllegalArgumentException("Wrapped field's minumum value must be zero");
        }
    }

    public long add(long j, int i) {
        return m23024().add(j, i);
    }

    public long add(long j, long j2) {
        return m23024().add(j, j2);
    }

    public long addWrapField(long j, int i) {
        return m23024().addWrapField(j, i);
    }

    public int[] addWrapField(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        return m23024().addWrapField(readablePartial, i, iArr, i2);
    }

    public int get(long j) {
        int i = m23024().get(j);
        return i == 0 ? getMaximumValue() : i;
    }

    public int getDifference(long j, long j2) {
        return m23024().getDifference(j, j2);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return m23024().getDifferenceAsLong(j, j2);
    }

    public int getLeapAmount(long j) {
        return m23024().getLeapAmount(j);
    }

    public DurationField getLeapDurationField() {
        return m23024().getLeapDurationField();
    }

    public int getMaximumValue() {
        return m23024().getMaximumValue() + 1;
    }

    public int getMaximumValue(long j) {
        return m23024().getMaximumValue(j) + 1;
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        return m23024().getMaximumValue(readablePartial) + 1;
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        return m23024().getMaximumValue(readablePartial, iArr) + 1;
    }

    public int getMinimumValue() {
        return 1;
    }

    public int getMinimumValue(long j) {
        return 1;
    }

    public int getMinimumValue(ReadablePartial readablePartial) {
        return 1;
    }

    public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
        return 1;
    }

    public boolean isLeap(long j) {
        return m23024().isLeap(j);
    }

    public long remainder(long j) {
        return m23024().remainder(j);
    }

    public long roundCeiling(long j) {
        return m23024().roundCeiling(j);
    }

    public long roundFloor(long j) {
        return m23024().roundFloor(j);
    }

    public long roundHalfCeiling(long j) {
        return m23024().roundHalfCeiling(j);
    }

    public long roundHalfEven(long j) {
        return m23024().roundHalfEven(j);
    }

    public long roundHalfFloor(long j) {
        return m23024().roundHalfFloor(j);
    }

    public long set(long j, int i) {
        int maximumValue = getMaximumValue();
        FieldUtils.m23038((DateTimeField) this, i, 1, maximumValue);
        if (i == maximumValue) {
            i = 0;
        }
        return m23024().set(j, i);
    }
}
