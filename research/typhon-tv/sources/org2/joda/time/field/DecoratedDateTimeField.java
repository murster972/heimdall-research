package org2.joda.time.field;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;

public abstract class DecoratedDateTimeField extends BaseDateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeField f18036;

    protected DecoratedDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType) {
        super(dateTimeFieldType);
        if (dateTimeField == null) {
            throw new IllegalArgumentException("The field must not be null");
        } else if (!dateTimeField.isSupported()) {
            throw new IllegalArgumentException("The field must be supported");
        } else {
            this.f18036 = dateTimeField;
        }
    }

    public int get(long j) {
        return this.f18036.get(j);
    }

    public DurationField getDurationField() {
        return this.f18036.getDurationField();
    }

    public int getMaximumValue() {
        return this.f18036.getMaximumValue();
    }

    public int getMinimumValue() {
        return this.f18036.getMinimumValue();
    }

    public DurationField getRangeDurationField() {
        return this.f18036.getRangeDurationField();
    }

    public boolean isLenient() {
        return this.f18036.isLenient();
    }

    public long roundFloor(long j) {
        return this.f18036.roundFloor(j);
    }

    public long set(long j, int i) {
        return this.f18036.set(j, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final DateTimeField m23024() {
        return this.f18036;
    }
}
