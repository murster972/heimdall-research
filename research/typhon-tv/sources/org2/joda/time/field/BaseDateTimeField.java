package org2.joda.time.field;

import java.util.Locale;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.IllegalFieldValueException;
import org2.joda.time.ReadablePartial;

public abstract class BaseDateTimeField extends DateTimeField {

    /* renamed from: 龘  reason: contains not printable characters */
    private final DateTimeFieldType f18035;

    protected BaseDateTimeField(DateTimeFieldType dateTimeFieldType) {
        if (dateTimeFieldType == null) {
            throw new IllegalArgumentException("The type must not be null");
        }
        this.f18035 = dateTimeFieldType;
    }

    public long add(long j, int i) {
        return getDurationField().add(j, i);
    }

    public long add(long j, long j2) {
        return getDurationField().add(j, j2);
    }

    public int[] add(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        if (i2 == 0) {
            return iArr;
        }
        DateTimeField dateTimeField = null;
        int i3 = i2;
        int[] iArr2 = iArr;
        while (true) {
            if (i3 <= 0) {
                break;
            }
            int maximumValue = getMaximumValue(readablePartial, iArr2);
            long j = (long) (iArr2[i] + i3);
            if (j <= ((long) maximumValue)) {
                iArr2[i] = (int) j;
                break;
            }
            if (dateTimeField == null) {
                if (i == 0) {
                    throw new IllegalArgumentException("Maximum value exceeded for add");
                }
                dateTimeField = readablePartial.getField(i - 1);
                if (getRangeDurationField().getType() != dateTimeField.getDurationField().getType()) {
                    throw new IllegalArgumentException("Fields invalid for add");
                }
            }
            i3 -= (maximumValue + 1) - iArr2[i];
            iArr2 = dateTimeField.add(readablePartial, i - 1, iArr2, 1);
            iArr2[i] = getMinimumValue(readablePartial, iArr2);
        }
        while (true) {
            if (i3 >= 0) {
                break;
            }
            int minimumValue = getMinimumValue(readablePartial, iArr2);
            long j2 = (long) (iArr2[i] + i3);
            if (j2 >= ((long) minimumValue)) {
                iArr2[i] = (int) j2;
                break;
            }
            if (dateTimeField == null) {
                if (i == 0) {
                    throw new IllegalArgumentException("Maximum value exceeded for add");
                }
                dateTimeField = readablePartial.getField(i - 1);
                if (getRangeDurationField().getType() != dateTimeField.getDurationField().getType()) {
                    throw new IllegalArgumentException("Fields invalid for add");
                }
            }
            i3 -= (minimumValue - 1) - iArr2[i];
            iArr2 = dateTimeField.add(readablePartial, i - 1, iArr2, -1);
            iArr2[i] = getMaximumValue(readablePartial, iArr2);
        }
        return set(readablePartial, i, iArr2, iArr2[i]);
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m23033(get(j), i, getMinimumValue(j), getMaximumValue(j)));
    }

    public int[] addWrapField(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        return set(readablePartial, i, iArr, FieldUtils.m23033(iArr[i], i2, getMinimumValue(readablePartial), getMaximumValue(readablePartial)));
    }

    public int[] addWrapPartial(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        int i3;
        if (i2 == 0) {
            return iArr;
        }
        DateTimeField dateTimeField = null;
        int i4 = i2;
        int[] iArr2 = iArr;
        while (true) {
            if (i4 <= 0) {
                break;
            }
            int maximumValue = getMaximumValue(readablePartial, iArr2);
            long j = (long) (iArr2[i] + i4);
            if (j <= ((long) maximumValue)) {
                iArr2[i] = (int) j;
                break;
            }
            if (dateTimeField == null) {
                if (i == 0) {
                    i4 -= (maximumValue + 1) - iArr2[i];
                    iArr2[i] = getMinimumValue(readablePartial, iArr2);
                } else {
                    dateTimeField = readablePartial.getField(i - 1);
                    if (getRangeDurationField().getType() != dateTimeField.getDurationField().getType()) {
                        throw new IllegalArgumentException("Fields invalid for add");
                    }
                }
            }
            i4 -= (maximumValue + 1) - iArr2[i];
            iArr2 = dateTimeField.addWrapPartial(readablePartial, i - 1, iArr2, 1);
            iArr2[i] = getMinimumValue(readablePartial, iArr2);
        }
        while (true) {
            if (i4 >= 0) {
                break;
            }
            int minimumValue = getMinimumValue(readablePartial, iArr2);
            long j2 = (long) (iArr2[i] + i4);
            if (j2 >= ((long) minimumValue)) {
                iArr2[i] = (int) j2;
                break;
            }
            if (dateTimeField == null) {
                if (i == 0) {
                    i3 = i4 - ((minimumValue - 1) - iArr2[i]);
                    iArr2[i] = getMaximumValue(readablePartial, iArr2);
                } else {
                    dateTimeField = readablePartial.getField(i - 1);
                    if (getRangeDurationField().getType() != dateTimeField.getDurationField().getType()) {
                        throw new IllegalArgumentException("Fields invalid for add");
                    }
                }
            }
            i3 = i4 - ((minimumValue - 1) - iArr2[i]);
            iArr2 = dateTimeField.addWrapPartial(readablePartial, i - 1, iArr2, -1);
            iArr2[i] = getMaximumValue(readablePartial, iArr2);
        }
        return set(readablePartial, i, iArr2, iArr2[i]);
    }

    public abstract int get(long j);

    public String getAsShortText(int i, Locale locale) {
        return getAsText(i, locale);
    }

    public final String getAsShortText(long j) {
        return getAsShortText(j, (Locale) null);
    }

    public String getAsShortText(long j, Locale locale) {
        return getAsShortText(get(j), locale);
    }

    public String getAsShortText(ReadablePartial readablePartial, int i, Locale locale) {
        return getAsShortText(i, locale);
    }

    public final String getAsShortText(ReadablePartial readablePartial, Locale locale) {
        return getAsShortText(readablePartial, readablePartial.get(getType()), locale);
    }

    public String getAsText(int i, Locale locale) {
        return Integer.toString(i);
    }

    public final String getAsText(long j) {
        return getAsText(j, (Locale) null);
    }

    public String getAsText(long j, Locale locale) {
        return getAsText(get(j), locale);
    }

    public String getAsText(ReadablePartial readablePartial, int i, Locale locale) {
        return getAsText(i, locale);
    }

    public final String getAsText(ReadablePartial readablePartial, Locale locale) {
        return getAsText(readablePartial, readablePartial.get(getType()), locale);
    }

    public int getDifference(long j, long j2) {
        return getDurationField().getDifference(j, j2);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return getDurationField().getDifferenceAsLong(j, j2);
    }

    public abstract DurationField getDurationField();

    public int getLeapAmount(long j) {
        return 0;
    }

    public DurationField getLeapDurationField() {
        return null;
    }

    public int getMaximumShortTextLength(Locale locale) {
        return getMaximumTextLength(locale);
    }

    public int getMaximumTextLength(Locale locale) {
        int maximumValue = getMaximumValue();
        if (maximumValue >= 0) {
            if (maximumValue < 10) {
                return 1;
            }
            if (maximumValue < 100) {
                return 2;
            }
            if (maximumValue < 1000) {
                return 3;
            }
        }
        return Integer.toString(maximumValue).length();
    }

    public abstract int getMaximumValue();

    public int getMaximumValue(long j) {
        return getMaximumValue();
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        return getMaximumValue();
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        return getMaximumValue(readablePartial);
    }

    public abstract int getMinimumValue();

    public int getMinimumValue(long j) {
        return getMinimumValue();
    }

    public int getMinimumValue(ReadablePartial readablePartial) {
        return getMinimumValue();
    }

    public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
        return getMinimumValue(readablePartial);
    }

    public final String getName() {
        return this.f18035.getName();
    }

    public abstract DurationField getRangeDurationField();

    public final DateTimeFieldType getType() {
        return this.f18035;
    }

    public boolean isLeap(long j) {
        return false;
    }

    public final boolean isSupported() {
        return true;
    }

    public long remainder(long j) {
        return j - roundFloor(j);
    }

    public long roundCeiling(long j) {
        long roundFloor = roundFloor(j);
        return roundFloor != j ? add(roundFloor, 1) : j;
    }

    public abstract long roundFloor(long j);

    public long roundHalfCeiling(long j) {
        long roundFloor = roundFloor(j);
        long roundCeiling = roundCeiling(j);
        return roundCeiling - j <= j - roundFloor ? roundCeiling : roundFloor;
    }

    public long roundHalfEven(long j) {
        long roundFloor = roundFloor(j);
        long roundCeiling = roundCeiling(j);
        long j2 = j - roundFloor;
        long j3 = roundCeiling - j;
        return j2 < j3 ? roundFloor : j3 < j2 ? roundCeiling : (get(roundCeiling) & 1) == 0 ? roundCeiling : roundFloor;
    }

    public long roundHalfFloor(long j) {
        long roundFloor = roundFloor(j);
        long roundCeiling = roundCeiling(j);
        return j - roundFloor <= roundCeiling - j ? roundFloor : roundCeiling;
    }

    public abstract long set(long j, int i);

    public final long set(long j, String str) {
        return set(j, str, (Locale) null);
    }

    public long set(long j, String str, Locale locale) {
        return set(j, m23023(str, locale));
    }

    public int[] set(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        FieldUtils.m23038((DateTimeField) this, i2, getMinimumValue(readablePartial, iArr), getMaximumValue(readablePartial, iArr));
        iArr[i] = i2;
        for (int i3 = i + 1; i3 < readablePartial.size(); i3++) {
            DateTimeField field = readablePartial.getField(i3);
            if (iArr[i3] > field.getMaximumValue(readablePartial, iArr)) {
                iArr[i3] = field.getMaximumValue(readablePartial, iArr);
            }
            if (iArr[i3] < field.getMinimumValue(readablePartial, iArr)) {
                iArr[i3] = field.getMinimumValue(readablePartial, iArr);
            }
        }
        return iArr;
    }

    public int[] set(ReadablePartial readablePartial, int i, int[] iArr, String str, Locale locale) {
        return set(readablePartial, i, iArr, m23023(str, locale));
    }

    public String toString() {
        return "DateTimeField[" + getName() + ']';
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m23023(String str, Locale locale) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw new IllegalFieldValueException(getType(), str);
        }
    }
}
