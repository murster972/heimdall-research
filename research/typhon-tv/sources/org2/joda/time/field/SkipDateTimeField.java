package org2.joda.time.field;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.IllegalFieldValueException;

public final class SkipDateTimeField extends DelegatedDateTimeField {
    private static final long serialVersionUID = -8869148464118507846L;
    private final Chronology iChronology;
    private final int iSkip;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient int f18054;

    public SkipDateTimeField(Chronology chronology, DateTimeField dateTimeField) {
        this(chronology, dateTimeField, 0);
    }

    public SkipDateTimeField(Chronology chronology, DateTimeField dateTimeField, int i) {
        super(dateTimeField);
        this.iChronology = chronology;
        int minimumValue = super.getMinimumValue();
        if (minimumValue < i) {
            this.f18054 = minimumValue - 1;
        } else if (minimumValue == i) {
            this.f18054 = i + 1;
        } else {
            this.f18054 = minimumValue;
        }
        this.iSkip = i;
    }

    private Object readResolve() {
        return getType().getField(this.iChronology);
    }

    public int get(long j) {
        int i = super.get(j);
        return i <= this.iSkip ? i - 1 : i;
    }

    public int getMinimumValue() {
        return this.f18054;
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, this.f18054, getMaximumValue());
        if (i <= this.iSkip) {
            if (i == this.iSkip) {
                throw new IllegalFieldValueException(DateTimeFieldType.year(), (Number) Integer.valueOf(i), (Number) null, (Number) null);
            }
            i++;
        }
        return super.set(j, i);
    }
}
