package org2.joda.time.field;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.ReadablePartial;

public final class UnsupportedDateTimeField extends DateTimeField implements Serializable {
    private static final long serialVersionUID = -1934618396111902255L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static HashMap<DateTimeFieldType, UnsupportedDateTimeField> f18056;
    private final DurationField iDurationField;
    private final DateTimeFieldType iType;

    private UnsupportedDateTimeField(DateTimeFieldType dateTimeFieldType, DurationField durationField) {
        if (dateTimeFieldType == null || durationField == null) {
            throw new IllegalArgumentException();
        }
        this.iType = dateTimeFieldType;
        this.iDurationField = durationField;
    }

    public static synchronized UnsupportedDateTimeField getInstance(DateTimeFieldType dateTimeFieldType, DurationField durationField) {
        UnsupportedDateTimeField unsupportedDateTimeField;
        synchronized (UnsupportedDateTimeField.class) {
            if (f18056 == null) {
                f18056 = new HashMap<>(7);
                unsupportedDateTimeField = null;
            } else {
                unsupportedDateTimeField = f18056.get(dateTimeFieldType);
                if (!(unsupportedDateTimeField == null || unsupportedDateTimeField.getDurationField() == durationField)) {
                    unsupportedDateTimeField = null;
                }
            }
            if (unsupportedDateTimeField == null) {
                unsupportedDateTimeField = new UnsupportedDateTimeField(dateTimeFieldType, durationField);
                f18056.put(dateTimeFieldType, unsupportedDateTimeField);
            }
        }
        return unsupportedDateTimeField;
    }

    private Object readResolve() {
        return getInstance(this.iType, this.iDurationField);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private UnsupportedOperationException m23044() {
        return new UnsupportedOperationException(this.iType + " field is unsupported");
    }

    public long add(long j, int i) {
        return getDurationField().add(j, i);
    }

    public long add(long j, long j2) {
        return getDurationField().add(j, j2);
    }

    public int[] add(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m23044();
    }

    public long addWrapField(long j, int i) {
        throw m23044();
    }

    public int[] addWrapField(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m23044();
    }

    public int[] addWrapPartial(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m23044();
    }

    public int get(long j) {
        throw m23044();
    }

    public String getAsShortText(int i, Locale locale) {
        throw m23044();
    }

    public String getAsShortText(long j) {
        throw m23044();
    }

    public String getAsShortText(long j, Locale locale) {
        throw m23044();
    }

    public String getAsShortText(ReadablePartial readablePartial, int i, Locale locale) {
        throw m23044();
    }

    public String getAsShortText(ReadablePartial readablePartial, Locale locale) {
        throw m23044();
    }

    public String getAsText(int i, Locale locale) {
        throw m23044();
    }

    public String getAsText(long j) {
        throw m23044();
    }

    public String getAsText(long j, Locale locale) {
        throw m23044();
    }

    public String getAsText(ReadablePartial readablePartial, int i, Locale locale) {
        throw m23044();
    }

    public String getAsText(ReadablePartial readablePartial, Locale locale) {
        throw m23044();
    }

    public int getDifference(long j, long j2) {
        return getDurationField().getDifference(j, j2);
    }

    public long getDifferenceAsLong(long j, long j2) {
        return getDurationField().getDifferenceAsLong(j, j2);
    }

    public DurationField getDurationField() {
        return this.iDurationField;
    }

    public int getLeapAmount(long j) {
        throw m23044();
    }

    public DurationField getLeapDurationField() {
        return null;
    }

    public int getMaximumShortTextLength(Locale locale) {
        throw m23044();
    }

    public int getMaximumTextLength(Locale locale) {
        throw m23044();
    }

    public int getMaximumValue() {
        throw m23044();
    }

    public int getMaximumValue(long j) {
        throw m23044();
    }

    public int getMaximumValue(ReadablePartial readablePartial) {
        throw m23044();
    }

    public int getMaximumValue(ReadablePartial readablePartial, int[] iArr) {
        throw m23044();
    }

    public int getMinimumValue() {
        throw m23044();
    }

    public int getMinimumValue(long j) {
        throw m23044();
    }

    public int getMinimumValue(ReadablePartial readablePartial) {
        throw m23044();
    }

    public int getMinimumValue(ReadablePartial readablePartial, int[] iArr) {
        throw m23044();
    }

    public String getName() {
        return this.iType.getName();
    }

    public DurationField getRangeDurationField() {
        return null;
    }

    public DateTimeFieldType getType() {
        return this.iType;
    }

    public boolean isLeap(long j) {
        throw m23044();
    }

    public boolean isLenient() {
        return false;
    }

    public boolean isSupported() {
        return false;
    }

    public long remainder(long j) {
        throw m23044();
    }

    public long roundCeiling(long j) {
        throw m23044();
    }

    public long roundFloor(long j) {
        throw m23044();
    }

    public long roundHalfCeiling(long j) {
        throw m23044();
    }

    public long roundHalfEven(long j) {
        throw m23044();
    }

    public long roundHalfFloor(long j) {
        throw m23044();
    }

    public long set(long j, int i) {
        throw m23044();
    }

    public long set(long j, String str) {
        throw m23044();
    }

    public long set(long j, String str, Locale locale) {
        throw m23044();
    }

    public int[] set(ReadablePartial readablePartial, int i, int[] iArr, int i2) {
        throw m23044();
    }

    public int[] set(ReadablePartial readablePartial, int i, int[] iArr, String str, Locale locale) {
        throw m23044();
    }

    public String toString() {
        return "UnsupportedDateTimeField";
    }
}
