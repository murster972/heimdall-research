package org2.joda.time.field;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;

public abstract class PreciseDurationDateTimeField extends BaseDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final DurationField f18049;

    /* renamed from: 龘  reason: contains not printable characters */
    final long f18050;

    public PreciseDurationDateTimeField(DateTimeFieldType dateTimeFieldType, DurationField durationField) {
        super(dateTimeFieldType);
        if (!durationField.isPrecise()) {
            throw new IllegalArgumentException("Unit duration field must be precise");
        }
        this.f18050 = durationField.getUnitMillis();
        if (this.f18050 < 1) {
            throw new IllegalArgumentException("The unit milliseconds must be at least 1");
        }
        this.f18049 = durationField;
    }

    public DurationField getDurationField() {
        return this.f18049;
    }

    public int getMinimumValue() {
        return 0;
    }

    public boolean isLenient() {
        return false;
    }

    public long remainder(long j) {
        return j >= 0 ? j % this.f18050 : (((j + 1) % this.f18050) + this.f18050) - 1;
    }

    public long roundCeiling(long j) {
        if (j <= 0) {
            return j - (j % this.f18050);
        }
        long j2 = j - 1;
        return (j2 - (j2 % this.f18050)) + this.f18050;
    }

    public long roundFloor(long j) {
        if (j >= 0) {
            return j - (j % this.f18050);
        }
        long j2 = 1 + j;
        return (j2 - (j2 % this.f18050)) - this.f18050;
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, getMinimumValue(), m23041(j, i));
        return (((long) (i - get(j))) * this.f18050) + j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m23041(long j, int i) {
        return getMaximumValue(j);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final long m23042() {
        return this.f18050;
    }
}
