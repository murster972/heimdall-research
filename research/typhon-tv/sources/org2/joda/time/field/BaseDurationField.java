package org2.joda.time.field;

import java.io.Serializable;
import org2.joda.time.DurationField;
import org2.joda.time.DurationFieldType;

public abstract class BaseDurationField extends DurationField implements Serializable {
    private static final long serialVersionUID = -2554245107589433218L;
    private final DurationFieldType iType;

    protected BaseDurationField(DurationFieldType durationFieldType) {
        if (durationFieldType == null) {
            throw new IllegalArgumentException("The type must not be null");
        }
        this.iType = durationFieldType;
    }

    public int compareTo(DurationField durationField) {
        long unitMillis = durationField.getUnitMillis();
        long unitMillis2 = getUnitMillis();
        if (unitMillis2 == unitMillis) {
            return 0;
        }
        return unitMillis2 < unitMillis ? -1 : 1;
    }

    public int getDifference(long j, long j2) {
        return FieldUtils.m23034(getDifferenceAsLong(j, j2));
    }

    public long getMillis(int i) {
        return ((long) i) * getUnitMillis();
    }

    public long getMillis(long j) {
        return FieldUtils.m23029(j, getUnitMillis());
    }

    public final String getName() {
        return this.iType.getName();
    }

    public final DurationFieldType getType() {
        return this.iType;
    }

    public int getValue(long j) {
        return FieldUtils.m23034(getValueAsLong(j));
    }

    public int getValue(long j, long j2) {
        return FieldUtils.m23034(getValueAsLong(j, j2));
    }

    public long getValueAsLong(long j) {
        return j / getUnitMillis();
    }

    public final boolean isSupported() {
        return true;
    }

    public String toString() {
        return "DurationField[" + getName() + ']';
    }
}
