package org2.joda.time.field;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;

public class RemainderDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    final DurationField f18051;

    /* renamed from: 齉  reason: contains not printable characters */
    final DurationField f18052;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f18053;

    public RemainderDateTimeField(DateTimeField dateTimeField, DurationField durationField, DateTimeFieldType dateTimeFieldType, int i) {
        super(dateTimeField, dateTimeFieldType);
        if (i < 2) {
            throw new IllegalArgumentException("The divisor must be at least 2");
        }
        this.f18052 = durationField;
        this.f18051 = dateTimeField.getDurationField();
        this.f18053 = i;
    }

    public RemainderDateTimeField(DividedDateTimeField dividedDateTimeField) {
        this(dividedDateTimeField, dividedDateTimeField.getType());
    }

    public RemainderDateTimeField(DividedDateTimeField dividedDateTimeField, DateTimeFieldType dateTimeFieldType) {
        this(dividedDateTimeField, dividedDateTimeField.m23024().getDurationField(), dateTimeFieldType);
    }

    public RemainderDateTimeField(DividedDateTimeField dividedDateTimeField, DurationField durationField, DateTimeFieldType dateTimeFieldType) {
        super(dividedDateTimeField.m23024(), dateTimeFieldType);
        this.f18053 = dividedDateTimeField.f18041;
        this.f18051 = durationField;
        this.f18052 = dividedDateTimeField.f18038;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m23043(int i) {
        return i >= 0 ? i / this.f18053 : ((i + 1) / this.f18053) - 1;
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m23033(get(j), i, 0, this.f18053 - 1));
    }

    public int get(long j) {
        int i = m23024().get(j);
        if (i >= 0) {
            return i % this.f18053;
        }
        return ((i + 1) % this.f18053) + (this.f18053 - 1);
    }

    public DurationField getDurationField() {
        return this.f18051;
    }

    public int getMaximumValue() {
        return this.f18053 - 1;
    }

    public int getMinimumValue() {
        return 0;
    }

    public DurationField getRangeDurationField() {
        return this.f18052;
    }

    public long remainder(long j) {
        return m23024().remainder(j);
    }

    public long roundCeiling(long j) {
        return m23024().roundCeiling(j);
    }

    public long roundFloor(long j) {
        return m23024().roundFloor(j);
    }

    public long roundHalfCeiling(long j) {
        return m23024().roundHalfCeiling(j);
    }

    public long roundHalfEven(long j) {
        return m23024().roundHalfEven(j);
    }

    public long roundHalfFloor(long j) {
        return m23024().roundHalfFloor(j);
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, 0, this.f18053 - 1);
        return m23024().set(j, (m23043(m23024().get(j)) * this.f18053) + i);
    }
}
