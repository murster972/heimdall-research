package org2.joda.time.field;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;

public class PreciseDateTimeField extends PreciseDurationDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f18047;

    /* renamed from: 齉  reason: contains not printable characters */
    private final DurationField f18048;

    public PreciseDateTimeField(DateTimeFieldType dateTimeFieldType, DurationField durationField, DurationField durationField2) {
        super(dateTimeFieldType, durationField);
        if (!durationField2.isPrecise()) {
            throw new IllegalArgumentException("Range duration field must be precise");
        }
        this.f18047 = (int) (durationField2.getUnitMillis() / m23042());
        if (this.f18047 < 2) {
            throw new IllegalArgumentException("The effective range must be at least 2");
        }
        this.f18048 = durationField2;
    }

    public long addWrapField(long j, int i) {
        int i2 = get(j);
        return (((long) (FieldUtils.m23033(i2, i, getMinimumValue(), getMaximumValue()) - i2)) * m23042()) + j;
    }

    public int get(long j) {
        return j >= 0 ? (int) ((j / m23042()) % ((long) this.f18047)) : (this.f18047 - 1) + ((int) (((1 + j) / m23042()) % ((long) this.f18047)));
    }

    public int getMaximumValue() {
        return this.f18047 - 1;
    }

    public DurationField getRangeDurationField() {
        return this.f18048;
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, getMinimumValue(), getMaximumValue());
        return (((long) (i - get(j))) * this.f18050) + j;
    }
}
