package org2.joda.time.field;

import java.util.Locale;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.ReadableInstant;
import org2.joda.time.ReadablePartial;

public abstract class AbstractPartialFieldProperty {
    protected AbstractPartialFieldProperty() {
    }

    public int compareTo(ReadableInstant readableInstant) {
        if (readableInstant == null) {
            throw new IllegalArgumentException("The instant must not be null");
        }
        int i = get();
        int i2 = readableInstant.get(getFieldType());
        if (i < i2) {
            return -1;
        }
        return i > i2 ? 1 : 0;
    }

    public int compareTo(ReadablePartial readablePartial) {
        if (readablePartial == null) {
            throw new IllegalArgumentException("The instant must not be null");
        }
        int i = get();
        int i2 = readablePartial.get(getFieldType());
        if (i < i2) {
            return -1;
        }
        return i > i2 ? 1 : 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AbstractPartialFieldProperty)) {
            return false;
        }
        AbstractPartialFieldProperty abstractPartialFieldProperty = (AbstractPartialFieldProperty) obj;
        return get() == abstractPartialFieldProperty.get() && getFieldType() == abstractPartialFieldProperty.getFieldType() && FieldUtils.m23040((Object) m23020().getChronology(), (Object) abstractPartialFieldProperty.m23020().getChronology());
    }

    public abstract int get();

    public String getAsShortText() {
        return getAsShortText((Locale) null);
    }

    public String getAsShortText(Locale locale) {
        return getField().getAsShortText(m23020(), get(), locale);
    }

    public String getAsString() {
        return Integer.toString(get());
    }

    public String getAsText() {
        return getAsText((Locale) null);
    }

    public String getAsText(Locale locale) {
        return getField().getAsText(m23020(), get(), locale);
    }

    public DurationField getDurationField() {
        return getField().getDurationField();
    }

    public abstract DateTimeField getField();

    public DateTimeFieldType getFieldType() {
        return getField().getType();
    }

    public int getMaximumShortTextLength(Locale locale) {
        return getField().getMaximumShortTextLength(locale);
    }

    public int getMaximumTextLength(Locale locale) {
        return getField().getMaximumTextLength(locale);
    }

    public int getMaximumValue() {
        return getField().getMaximumValue(m23020());
    }

    public int getMaximumValueOverall() {
        return getField().getMaximumValue();
    }

    public int getMinimumValue() {
        return getField().getMinimumValue(m23020());
    }

    public int getMinimumValueOverall() {
        return getField().getMinimumValue();
    }

    public String getName() {
        return getField().getName();
    }

    public DurationField getRangeDurationField() {
        return getField().getRangeDurationField();
    }

    public int hashCode() {
        return ((((get() + 247) * 13) + getFieldType().hashCode()) * 13) + m23020().getChronology().hashCode();
    }

    public String toString() {
        return "Property[" + getName() + "]";
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ReadablePartial m23020();
}
