package org2.joda.time.field;

import org2.joda.time.Chronology;
import org2.joda.time.DateTimeField;

public final class SkipUndoDateTimeField extends DelegatedDateTimeField {
    private static final long serialVersionUID = -5875876968979L;
    private final Chronology iChronology;
    private final int iSkip;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient int f18055;

    public SkipUndoDateTimeField(Chronology chronology, DateTimeField dateTimeField) {
        this(chronology, dateTimeField, 0);
    }

    public SkipUndoDateTimeField(Chronology chronology, DateTimeField dateTimeField, int i) {
        super(dateTimeField);
        this.iChronology = chronology;
        int minimumValue = super.getMinimumValue();
        if (minimumValue < i) {
            this.f18055 = minimumValue + 1;
        } else if (minimumValue == i + 1) {
            this.f18055 = i;
        } else {
            this.f18055 = minimumValue;
        }
        this.iSkip = i;
    }

    private Object readResolve() {
        return getType().getField(this.iChronology);
    }

    public int get(long j) {
        int i = super.get(j);
        return i < this.iSkip ? i + 1 : i;
    }

    public int getMinimumValue() {
        return this.f18055;
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, this.f18055, getMaximumValue());
        if (i <= this.iSkip) {
            i--;
        }
        return super.set(j, i);
    }
}
