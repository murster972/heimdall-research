package org2.joda.time.field;

import java.io.Serializable;
import java.util.HashMap;
import org2.joda.time.DurationField;
import org2.joda.time.DurationFieldType;

public final class UnsupportedDurationField extends DurationField implements Serializable {
    private static final long serialVersionUID = -6390301302770925357L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static HashMap<DurationFieldType, UnsupportedDurationField> f18057;
    private final DurationFieldType iType;

    private UnsupportedDurationField(DurationFieldType durationFieldType) {
        this.iType = durationFieldType;
    }

    public static synchronized UnsupportedDurationField getInstance(DurationFieldType durationFieldType) {
        UnsupportedDurationField unsupportedDurationField;
        synchronized (UnsupportedDurationField.class) {
            if (f18057 == null) {
                f18057 = new HashMap<>(7);
                unsupportedDurationField = null;
            } else {
                unsupportedDurationField = f18057.get(durationFieldType);
            }
            if (unsupportedDurationField == null) {
                unsupportedDurationField = new UnsupportedDurationField(durationFieldType);
                f18057.put(durationFieldType, unsupportedDurationField);
            }
        }
        return unsupportedDurationField;
    }

    private Object readResolve() {
        return getInstance(this.iType);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private UnsupportedOperationException m23045() {
        return new UnsupportedOperationException(this.iType + " field is unsupported");
    }

    public long add(long j, int i) {
        throw m23045();
    }

    public long add(long j, long j2) {
        throw m23045();
    }

    public int compareTo(DurationField durationField) {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UnsupportedDurationField)) {
            return false;
        }
        UnsupportedDurationField unsupportedDurationField = (UnsupportedDurationField) obj;
        return unsupportedDurationField.getName() == null ? getName() == null : unsupportedDurationField.getName().equals(getName());
    }

    public int getDifference(long j, long j2) {
        throw m23045();
    }

    public long getDifferenceAsLong(long j, long j2) {
        throw m23045();
    }

    public long getMillis(int i) {
        throw m23045();
    }

    public long getMillis(int i, long j) {
        throw m23045();
    }

    public long getMillis(long j) {
        throw m23045();
    }

    public long getMillis(long j, long j2) {
        throw m23045();
    }

    public String getName() {
        return this.iType.getName();
    }

    public final DurationFieldType getType() {
        return this.iType;
    }

    public long getUnitMillis() {
        return 0;
    }

    public int getValue(long j) {
        throw m23045();
    }

    public int getValue(long j, long j2) {
        throw m23045();
    }

    public long getValueAsLong(long j) {
        throw m23045();
    }

    public long getValueAsLong(long j, long j2) {
        throw m23045();
    }

    public int hashCode() {
        return getName().hashCode();
    }

    public boolean isPrecise() {
        return true;
    }

    public boolean isSupported() {
        return false;
    }

    public String toString() {
        return "UnsupportedDurationField[" + getName() + ']';
    }
}
