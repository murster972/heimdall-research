package org2.joda.time.field;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;

public class OffsetDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f18044;

    /* renamed from: 齉  reason: contains not printable characters */
    private final int f18045;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f18046;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public OffsetDateTimeField(DateTimeField dateTimeField, int i) {
        this(dateTimeField, dateTimeField == null ? null : dateTimeField.getType(), i, Integer.MIN_VALUE, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    public OffsetDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType, int i) {
        this(dateTimeField, dateTimeFieldType, i, Integer.MIN_VALUE, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
    }

    public OffsetDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType, int i, int i2, int i3) {
        super(dateTimeField, dateTimeFieldType);
        if (i == 0) {
            throw new IllegalArgumentException("The offset cannot be zero");
        }
        this.f18046 = i;
        if (i2 < dateTimeField.getMinimumValue() + i) {
            this.f18044 = dateTimeField.getMinimumValue() + i;
        } else {
            this.f18044 = i2;
        }
        if (i3 > dateTimeField.getMaximumValue() + i) {
            this.f18045 = dateTimeField.getMaximumValue() + i;
        } else {
            this.f18045 = i3;
        }
    }

    public long add(long j, int i) {
        long add = super.add(j, i);
        FieldUtils.m23038((DateTimeField) this, get(add), this.f18044, this.f18045);
        return add;
    }

    public long add(long j, long j2) {
        long add = super.add(j, j2);
        FieldUtils.m23038((DateTimeField) this, get(add), this.f18044, this.f18045);
        return add;
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m23033(get(j), i, this.f18044, this.f18045));
    }

    public int get(long j) {
        return super.get(j) + this.f18046;
    }

    public int getLeapAmount(long j) {
        return m23024().getLeapAmount(j);
    }

    public DurationField getLeapDurationField() {
        return m23024().getLeapDurationField();
    }

    public int getMaximumValue() {
        return this.f18045;
    }

    public int getMinimumValue() {
        return this.f18044;
    }

    public boolean isLeap(long j) {
        return m23024().isLeap(j);
    }

    public long remainder(long j) {
        return m23024().remainder(j);
    }

    public long roundCeiling(long j) {
        return m23024().roundCeiling(j);
    }

    public long roundFloor(long j) {
        return m23024().roundFloor(j);
    }

    public long roundHalfCeiling(long j) {
        return m23024().roundHalfCeiling(j);
    }

    public long roundHalfEven(long j) {
        return m23024().roundHalfEven(j);
    }

    public long roundHalfFloor(long j) {
        return m23024().roundHalfFloor(j);
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, this.f18044, this.f18045);
        return super.set(j, i - this.f18046);
    }
}
