package org2.joda.time.field;

import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;
import org2.joda.time.DurationFieldType;

public abstract class ImpreciseDateTimeField extends BaseDateTimeField {

    /* renamed from: 靐  reason: contains not printable characters */
    final long f18042;

    /* renamed from: 龘  reason: contains not printable characters */
    private final DurationField f18043;

    private final class LinkedDurationField extends BaseDurationField {
        private static final long serialVersionUID = -203813474600094134L;

        LinkedDurationField(DurationFieldType durationFieldType) {
            super(durationFieldType);
        }

        public long add(long j, int i) {
            return ImpreciseDateTimeField.this.add(j, i);
        }

        public long add(long j, long j2) {
            return ImpreciseDateTimeField.this.add(j, j2);
        }

        public int getDifference(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifference(j, j2);
        }

        public long getDifferenceAsLong(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifferenceAsLong(j, j2);
        }

        public long getMillis(int i, long j) {
            return ImpreciseDateTimeField.this.add(j, i) - j;
        }

        public long getMillis(long j, long j2) {
            return ImpreciseDateTimeField.this.add(j2, j) - j2;
        }

        public long getUnitMillis() {
            return ImpreciseDateTimeField.this.f18042;
        }

        public int getValue(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifference(j2 + j, j2);
        }

        public long getValueAsLong(long j, long j2) {
            return ImpreciseDateTimeField.this.getDifferenceAsLong(j2 + j, j2);
        }

        public boolean isPrecise() {
            return false;
        }
    }

    public ImpreciseDateTimeField(DateTimeFieldType dateTimeFieldType, long j) {
        super(dateTimeFieldType);
        this.f18042 = j;
        this.f18043 = new LinkedDurationField(dateTimeFieldType.getDurationType());
    }

    public abstract long add(long j, int i);

    public abstract long add(long j, long j2);

    public int getDifference(long j, long j2) {
        return FieldUtils.m23034(getDifferenceAsLong(j, j2));
    }

    public long getDifferenceAsLong(long j, long j2) {
        if (j < j2) {
            return -getDifferenceAsLong(j2, j);
        }
        long j3 = (j - j2) / this.f18042;
        if (add(j2, j3) < j) {
            do {
                j3++;
            } while (add(j2, j3) <= j);
            return j3 - 1;
        } else if (add(j2, j3) <= j) {
            return j3;
        } else {
            do {
                j3--;
            } while (add(j2, j3) > j);
            return j3;
        }
    }

    public final DurationField getDurationField() {
        return this.f18043;
    }
}
