package org2.joda.time.field;

import org2.joda.time.DateTimeField;
import org2.joda.time.DateTimeFieldType;
import org2.joda.time.DurationField;

public class DividedDateTimeField extends DecoratedDateTimeField {

    /* renamed from: 连任  reason: contains not printable characters */
    private final int f18037;

    /* renamed from: 靐  reason: contains not printable characters */
    final DurationField f18038;

    /* renamed from: 麤  reason: contains not printable characters */
    private final int f18039;

    /* renamed from: 齉  reason: contains not printable characters */
    final DurationField f18040;

    /* renamed from: 龘  reason: contains not printable characters */
    final int f18041;

    public DividedDateTimeField(DateTimeField dateTimeField, DateTimeFieldType dateTimeFieldType, int i) {
        this(dateTimeField, dateTimeField.getRangeDurationField(), dateTimeFieldType, i);
    }

    public DividedDateTimeField(DateTimeField dateTimeField, DurationField durationField, DateTimeFieldType dateTimeFieldType, int i) {
        super(dateTimeField, dateTimeFieldType);
        if (i < 2) {
            throw new IllegalArgumentException("The divisor must be at least 2");
        }
        DurationField durationField2 = dateTimeField.getDurationField();
        if (durationField2 == null) {
            this.f18038 = null;
        } else {
            this.f18038 = new ScaledDurationField(durationField2, dateTimeFieldType.getDurationType(), i);
        }
        this.f18040 = durationField;
        this.f18041 = i;
        int minimumValue = dateTimeField.getMinimumValue();
        int i2 = minimumValue >= 0 ? minimumValue / i : ((minimumValue + 1) / i) - 1;
        int maximumValue = dateTimeField.getMaximumValue();
        int i3 = maximumValue >= 0 ? maximumValue / i : ((maximumValue + 1) / i) - 1;
        this.f18039 = i2;
        this.f18037 = i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m23025(int i) {
        return i >= 0 ? i % this.f18041 : (this.f18041 - 1) + ((i + 1) % this.f18041);
    }

    public long add(long j, int i) {
        return m23024().add(j, this.f18041 * i);
    }

    public long add(long j, long j2) {
        return m23024().add(j, ((long) this.f18041) * j2);
    }

    public long addWrapField(long j, int i) {
        return set(j, FieldUtils.m23033(get(j), i, this.f18039, this.f18037));
    }

    public int get(long j) {
        int i = m23024().get(j);
        return i >= 0 ? i / this.f18041 : ((i + 1) / this.f18041) - 1;
    }

    public int getDifference(long j, long j2) {
        return m23024().getDifference(j, j2) / this.f18041;
    }

    public long getDifferenceAsLong(long j, long j2) {
        return m23024().getDifferenceAsLong(j, j2) / ((long) this.f18041);
    }

    public DurationField getDurationField() {
        return this.f18038;
    }

    public int getMaximumValue() {
        return this.f18037;
    }

    public int getMinimumValue() {
        return this.f18039;
    }

    public DurationField getRangeDurationField() {
        return this.f18040 != null ? this.f18040 : super.getRangeDurationField();
    }

    public long remainder(long j) {
        return set(j, get(m23024().remainder(j)));
    }

    public long roundFloor(long j) {
        DateTimeField r0 = m23024();
        return r0.roundFloor(r0.set(j, get(j) * this.f18041));
    }

    public long set(long j, int i) {
        FieldUtils.m23038((DateTimeField) this, i, this.f18039, this.f18037);
        return m23024().set(j, m23025(m23024().get(j)) + (this.f18041 * i));
    }
}
