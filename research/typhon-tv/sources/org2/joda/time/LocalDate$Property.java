package org2.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Locale;
import org2.joda.time.field.AbstractReadableInstantFieldProperty;

public final class LocalDate$Property extends AbstractReadableInstantFieldProperty {
    private static final long serialVersionUID = -3193829732634L;

    /* renamed from: 靐  reason: contains not printable characters */
    private transient DateTimeField f17810;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient LocalDate f17811;

    LocalDate$Property(LocalDate localDate, DateTimeField dateTimeField) {
        this.f17811 = localDate;
        this.f17810 = dateTimeField;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        this.f17811 = (LocalDate) objectInputStream.readObject();
        this.f17810 = ((DateTimeFieldType) objectInputStream.readObject()).getField(this.f17811.getChronology());
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.f17811);
        objectOutputStream.writeObject(this.f17810.getType());
    }

    public LocalDate addToCopy(int i) {
        return this.f17811.龘(this.f17810.add(this.f17811.龘(), i));
    }

    public LocalDate addWrapFieldToCopy(int i) {
        return this.f17811.龘(this.f17810.addWrapField(this.f17811.龘(), i));
    }

    public DateTimeField getField() {
        return this.f17810;
    }

    public LocalDate getLocalDate() {
        return this.f17811;
    }

    public LocalDate roundCeilingCopy() {
        return this.f17811.龘(this.f17810.roundCeiling(this.f17811.龘()));
    }

    public LocalDate roundFloorCopy() {
        return this.f17811.龘(this.f17810.roundFloor(this.f17811.龘()));
    }

    public LocalDate roundHalfCeilingCopy() {
        return this.f17811.龘(this.f17810.roundHalfCeiling(this.f17811.龘()));
    }

    public LocalDate roundHalfEvenCopy() {
        return this.f17811.龘(this.f17810.roundHalfEven(this.f17811.龘()));
    }

    public LocalDate roundHalfFloorCopy() {
        return this.f17811.龘(this.f17810.roundHalfFloor(this.f17811.龘()));
    }

    public LocalDate setCopy(int i) {
        return this.f17811.龘(this.f17810.set(this.f17811.龘(), i));
    }

    public LocalDate setCopy(String str) {
        return setCopy(str, (Locale) null);
    }

    public LocalDate setCopy(String str, Locale locale) {
        return this.f17811.龘(this.f17810.set(this.f17811.龘(), str, locale));
    }

    public LocalDate withMaximumValue() {
        return setCopy(getMaximumValue());
    }

    public LocalDate withMinimumValue() {
        return setCopy(getMinimumValue());
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public Chronology m22730() {
        return this.f17811.getChronology();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public long m22731() {
        return this.f17811.龘();
    }
}
