package org2.joda.time;

public interface ReadableDateTime extends ReadableInstant {
    DateTime toDateTime();
}
