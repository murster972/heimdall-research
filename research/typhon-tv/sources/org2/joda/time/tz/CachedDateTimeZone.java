package org2.joda.time.tz;

import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org2.joda.time.DateTimeZone;

public class CachedDateTimeZone extends DateTimeZone {
    private static final long serialVersionUID = 5472298452022250685L;

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int f18232;
    private final DateTimeZone iZone;

    /* renamed from: 靐  reason: contains not printable characters */
    private final transient Info[] f18233 = new Info[(f18232 + 1)];

    private static final class Info {

        /* renamed from: ʻ  reason: contains not printable characters */
        private int f18234 = Integer.MIN_VALUE;

        /* renamed from: 连任  reason: contains not printable characters */
        private int f18235 = Integer.MIN_VALUE;

        /* renamed from: 靐  reason: contains not printable characters */
        public final DateTimeZone f18236;

        /* renamed from: 麤  reason: contains not printable characters */
        private String f18237;

        /* renamed from: 齉  reason: contains not printable characters */
        Info f18238;

        /* renamed from: 龘  reason: contains not printable characters */
        public final long f18239;

        Info(DateTimeZone dateTimeZone, long j) {
            this.f18239 = j;
            this.f18236 = dateTimeZone;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23452(long j) {
            if (this.f18238 != null && j >= this.f18238.f18239) {
                return this.f18238.m23452(j);
            }
            if (this.f18235 == Integer.MIN_VALUE) {
                this.f18235 = this.f18236.getOffset(this.f18239);
            }
            return this.f18235;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public int m23453(long j) {
            if (this.f18238 != null && j >= this.f18238.f18239) {
                return this.f18238.m23453(j);
            }
            if (this.f18234 == Integer.MIN_VALUE) {
                this.f18234 = this.f18236.getStandardOffset(this.f18239);
            }
            return this.f18234;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m23454(long j) {
            if (this.f18238 != null && j >= this.f18238.f18239) {
                return this.f18238.m23454(j);
            }
            if (this.f18237 == null) {
                this.f18237 = this.f18236.getNameKey(this.f18239);
            }
            return this.f18237;
        }
    }

    static {
        Integer num;
        int i;
        try {
            num = Integer.getInteger("org.joda.time.tz.CachedDateTimeZone.size");
        } catch (SecurityException e) {
            num = null;
        }
        if (num == null) {
            i = 512;
        } else {
            int i2 = 0;
            for (int intValue = num.intValue() - 1; intValue > 0; intValue >>= 1) {
                i2++;
            }
            i = 1 << i2;
        }
        f18232 = i - 1;
    }

    private CachedDateTimeZone(DateTimeZone dateTimeZone) {
        super(dateTimeZone.getID());
        this.iZone = dateTimeZone;
    }

    public static CachedDateTimeZone forZone(DateTimeZone dateTimeZone) {
        return dateTimeZone instanceof CachedDateTimeZone ? (CachedDateTimeZone) dateTimeZone : new CachedDateTimeZone(dateTimeZone);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private Info m23450(long j) {
        long j2 = j & -4294967296L;
        Info info = new Info(this.iZone, j2);
        long j3 = j2 | InternalZipTyphoonApp.ZIP_64_LIMIT;
        Info info2 = info;
        while (true) {
            long nextTransition = this.iZone.nextTransition(j2);
            if (nextTransition == j2 || nextTransition > j3) {
                return info;
            }
            Info info3 = new Info(this.iZone, nextTransition);
            info2.f18238 = info3;
            info2 = info3;
            j2 = nextTransition;
        }
        return info;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Info m23451(long j) {
        int i = (int) (j >> 32);
        Info[] infoArr = this.f18233;
        int i2 = i & f18232;
        Info info = infoArr[i2];
        if (info != null && ((int) (info.f18239 >> 32)) == i) {
            return info;
        }
        Info r0 = m23450(j);
        infoArr[i2] = r0;
        return r0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof CachedDateTimeZone) {
            return this.iZone.equals(((CachedDateTimeZone) obj).iZone);
        }
        return false;
    }

    public String getNameKey(long j) {
        return m23451(j).m23454(j);
    }

    public int getOffset(long j) {
        return m23451(j).m23452(j);
    }

    public int getStandardOffset(long j) {
        return m23451(j).m23453(j);
    }

    public DateTimeZone getUncachedZone() {
        return this.iZone;
    }

    public int hashCode() {
        return this.iZone.hashCode();
    }

    public boolean isFixed() {
        return this.iZone.isFixed();
    }

    public long nextTransition(long j) {
        return this.iZone.nextTransition(j);
    }

    public long previousTransition(long j) {
        return this.iZone.previousTransition(j);
    }
}
