package org2.joda.time.tz;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org2.joda.time.DateTimeUtils;

public class DefaultNameProvider implements NameProvider {

    /* renamed from: 靐  reason: contains not printable characters */
    private HashMap<Locale, Map<String, Map<Boolean, Object>>> f18249 = m23475();

    /* renamed from: 龘  reason: contains not printable characters */
    private HashMap<Locale, Map<String, Map<String, Object>>> f18250 = m23475();

    /* renamed from: 齉  reason: contains not printable characters */
    private synchronized String[] m23473(Locale locale, String str, String str2) {
        String[] strArr;
        HashMap hashMap;
        String[] strArr2;
        String[] strArr3 = null;
        synchronized (this) {
            if (locale == null || str == null || str2 == null) {
                strArr = null;
            } else {
                Map map = this.f18250.get(locale);
                if (map == null) {
                    HashMap<Locale, Map<String, Map<String, Object>>> hashMap2 = this.f18250;
                    HashMap r0 = m23475();
                    hashMap2.put(locale, r0);
                    hashMap = r0;
                } else {
                    hashMap = map;
                }
                Map map2 = (Map) hashMap.get(str);
                if (map2 == null) {
                    map2 = m23475();
                    hashMap.put(str, map2);
                    String[][] zoneStrings = DateTimeUtils.m22715(Locale.ENGLISH).getZoneStrings();
                    int length = zoneStrings.length;
                    int i = 0;
                    while (true) {
                        if (i < length) {
                            String[] strArr4 = zoneStrings[i];
                            if (strArr4 != null && strArr4.length >= 5 && str.equals(strArr4[0])) {
                                strArr2 = strArr4;
                                break;
                            }
                            i++;
                        } else {
                            strArr2 = null;
                            break;
                        }
                    }
                    String[][] zoneStrings2 = DateTimeUtils.m22715(locale).getZoneStrings();
                    int length2 = zoneStrings2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 < length2) {
                            String[] strArr5 = zoneStrings2[i2];
                            if (strArr5 != null && strArr5.length >= 5 && str.equals(strArr5[0])) {
                                strArr3 = strArr5;
                                break;
                            }
                            i2++;
                        } else {
                            break;
                        }
                    }
                    if (!(strArr2 == null || strArr3 == null)) {
                        map2.put(strArr2[2], new String[]{strArr3[2], strArr3[1]});
                        if (strArr2[2].equals(strArr2[4])) {
                            map2.put(strArr2[4] + "-Summer", new String[]{strArr3[4], strArr3[3]});
                        } else {
                            map2.put(strArr2[4], new String[]{strArr3[4], strArr3[3]});
                        }
                    }
                }
                strArr = (String[]) map2.get(str2);
            }
        }
        return strArr;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private synchronized String[] m23474(Locale locale, String str, String str2, boolean z) {
        String[] strArr;
        HashMap hashMap;
        String[] strArr2;
        String[] strArr3 = null;
        synchronized (this) {
            if (locale == null || str == null || str2 == null) {
                strArr = null;
            } else {
                if (str.startsWith("Etc/")) {
                    str = str.substring(4);
                }
                Map map = this.f18249.get(locale);
                if (map == null) {
                    HashMap<Locale, Map<String, Map<Boolean, Object>>> hashMap2 = this.f18249;
                    HashMap r0 = m23475();
                    hashMap2.put(locale, r0);
                    hashMap = r0;
                } else {
                    hashMap = map;
                }
                Map map2 = (Map) hashMap.get(str);
                if (map2 == null) {
                    map2 = m23475();
                    hashMap.put(str, map2);
                    String[][] zoneStrings = DateTimeUtils.m22715(Locale.ENGLISH).getZoneStrings();
                    int length = zoneStrings.length;
                    int i = 0;
                    while (true) {
                        if (i < length) {
                            String[] strArr4 = zoneStrings[i];
                            if (strArr4 != null && strArr4.length >= 5 && str.equals(strArr4[0])) {
                                strArr2 = strArr4;
                                break;
                            }
                            i++;
                        } else {
                            strArr2 = null;
                            break;
                        }
                    }
                    String[][] zoneStrings2 = DateTimeUtils.m22715(locale).getZoneStrings();
                    int length2 = zoneStrings2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 < length2) {
                            String[] strArr5 = zoneStrings2[i2];
                            if (strArr5 != null && strArr5.length >= 5 && str.equals(strArr5[0])) {
                                strArr3 = strArr5;
                                break;
                            }
                            i2++;
                        } else {
                            break;
                        }
                    }
                    if (!(strArr2 == null || strArr3 == null)) {
                        map2.put(Boolean.TRUE, new String[]{strArr3[2], strArr3[1]});
                        map2.put(Boolean.FALSE, new String[]{strArr3[4], strArr3[3]});
                    }
                }
                strArr = (String[]) map2.get(Boolean.valueOf(z));
            }
        }
        return strArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private HashMap m23475() {
        return new HashMap(7);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m23476(Locale locale, String str, String str2) {
        String[] r0 = m23473(locale, str, str2);
        if (r0 == null) {
            return null;
        }
        return r0[1];
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m23477(Locale locale, String str, String str2, boolean z) {
        String[] r0 = m23474(locale, str, str2, z);
        if (r0 == null) {
            return null;
        }
        return r0[1];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23478(Locale locale, String str, String str2) {
        String[] r0 = m23473(locale, str, str2);
        if (r0 == null) {
            return null;
        }
        return r0[0];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23479(Locale locale, String str, String str2, boolean z) {
        String[] r0 = m23474(locale, str, str2, z);
        if (r0 == null) {
            return null;
        }
        return r0[0];
    }
}
