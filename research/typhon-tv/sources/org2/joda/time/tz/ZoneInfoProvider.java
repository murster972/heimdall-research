package org2.joda.time.tz;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org2.joda.time.DateTimeZone;

public class ZoneInfoProvider implements Provider {

    /* renamed from: 连任  reason: contains not printable characters */
    private final Set<String> f18252;

    /* renamed from: 靐  reason: contains not printable characters */
    private final String f18253;

    /* renamed from: 麤  reason: contains not printable characters */
    private final Map<String, Object> f18254;
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public final ClassLoader f18255;

    /* renamed from: 龘  reason: contains not printable characters */
    private final File f18256;

    public ZoneInfoProvider(File file) throws IOException {
        if (file == null) {
            throw new IllegalArgumentException("No file directory provided");
        } else if (!file.exists()) {
            throw new IOException("File directory doesn't exist: " + file);
        } else if (!file.isDirectory()) {
            throw new IOException("File doesn't refer to a directory: " + file);
        } else {
            this.f18256 = file;
            this.f18253 = null;
            this.f18255 = null;
            this.f18254 = m23489(m23486("ZoneInfoMap"));
            this.f18252 = Collections.unmodifiableSortedSet(new TreeSet(this.f18254.keySet()));
        }
    }

    public ZoneInfoProvider(String str) throws IOException {
        this(str, (ClassLoader) null, false);
    }

    private ZoneInfoProvider(String str, ClassLoader classLoader, boolean z) throws IOException {
        if (str == null) {
            throw new IllegalArgumentException("No resource path provided");
        }
        str = !str.endsWith(InternalZipTyphoonApp.ZIP_FILE_SEPARATOR) ? str + '/' : str;
        this.f18256 = null;
        this.f18253 = str;
        if (classLoader == null && !z) {
            classLoader = getClass().getClassLoader();
        }
        this.f18255 = classLoader;
        this.f18254 = m23489(m23486("ZoneInfoMap"));
        this.f18252 = Collections.unmodifiableSortedSet(new TreeSet(this.f18254.keySet()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private InputStream m23486(String str) throws IOException {
        if (this.f18256 != null) {
            return new FileInputStream(new File(this.f18256, str));
        }
        final String concat = this.f18253.concat(str);
        InputStream inputStream = (InputStream) AccessController.doPrivileged(new PrivilegedAction<InputStream>() {
            /* renamed from: 龘  reason: contains not printable characters */
            public InputStream run() {
                return ZoneInfoProvider.this.f18255 != null ? ZoneInfoProvider.this.f18255.getResourceAsStream(concat) : ClassLoader.getSystemResourceAsStream(concat);
            }
        });
        if (inputStream != null) {
            return inputStream;
        }
        throw new IOException(new StringBuilder(40).append("Resource not found: \"").append(concat).append("\" ClassLoader: ").append(this.f18255 != null ? this.f18255.toString() : "system").toString());
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x002e A[SYNTHETIC, Splitter:B:19:0x002e] */
    /* renamed from: 齉  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org2.joda.time.DateTimeZone m23487(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 0
            java.io.InputStream r2 = r5.m23486(r6)     // Catch:{ IOException -> 0x0019, all -> 0x002a }
            org2.joda.time.DateTimeZone r0 = org2.joda.time.tz.DateTimeZoneBuilder.m23457((java.io.InputStream) r2, (java.lang.String) r6)     // Catch:{ IOException -> 0x003a }
            java.util.Map<java.lang.String, java.lang.Object> r3 = r5.f18254     // Catch:{ IOException -> 0x003a }
            java.lang.ref.SoftReference r4 = new java.lang.ref.SoftReference     // Catch:{ IOException -> 0x003a }
            r4.<init>(r0)     // Catch:{ IOException -> 0x003a }
            r3.put(r6, r4)     // Catch:{ IOException -> 0x003a }
            if (r2 == 0) goto L_0x0018
            r2.close()     // Catch:{ IOException -> 0x0032 }
        L_0x0018:
            return r0
        L_0x0019:
            r0 = move-exception
            r2 = r1
        L_0x001b:
            r5.m23493((java.lang.Exception) r0)     // Catch:{ all -> 0x0038 }
            java.util.Map<java.lang.String, java.lang.Object> r0 = r5.f18254     // Catch:{ all -> 0x0038 }
            r0.remove(r6)     // Catch:{ all -> 0x0038 }
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x0034 }
        L_0x0028:
            r0 = r1
            goto L_0x0018
        L_0x002a:
            r0 = move-exception
            r2 = r1
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()     // Catch:{ IOException -> 0x0036 }
        L_0x0031:
            throw r0
        L_0x0032:
            r1 = move-exception
            goto L_0x0018
        L_0x0034:
            r0 = move-exception
            goto L_0x0028
        L_0x0036:
            r1 = move-exception
            goto L_0x0031
        L_0x0038:
            r0 = move-exception
            goto L_0x002c
        L_0x003a:
            r0 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.joda.time.tz.ZoneInfoProvider.m23487(java.lang.String):org2.joda.time.DateTimeZone");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Map<String, Object> m23489(InputStream inputStream) throws IOException {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        try {
            m23490(dataInputStream, concurrentHashMap);
            concurrentHashMap.put("UTC", new SoftReference(DateTimeZone.UTC));
            return concurrentHashMap;
        } finally {
            try {
                dataInputStream.close();
            } catch (IOException e) {
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23490(DataInputStream dataInputStream, Map<String, Object> map) throws IOException {
        int i = 0;
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        String[] strArr = new String[readUnsignedShort];
        for (int i2 = 0; i2 < readUnsignedShort; i2++) {
            strArr[i2] = dataInputStream.readUTF().intern();
        }
        int readUnsignedShort2 = dataInputStream.readUnsignedShort();
        while (i < readUnsignedShort2) {
            try {
                map.put(strArr[dataInputStream.readUnsignedShort()], strArr[dataInputStream.readUnsignedShort()]);
                i++;
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new IOException("Corrupt zone info map");
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<String> m23491() {
        return this.f18252;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeZone m23492(String str) {
        if (str == null) {
            return null;
        }
        Object obj = this.f18254.get(str);
        if (obj == null) {
            return null;
        }
        if (!(obj instanceof SoftReference)) {
            return str.equals(obj) ? m23487(str) : m23492((String) obj);
        }
        DateTimeZone dateTimeZone = (DateTimeZone) ((SoftReference) obj).get();
        return dateTimeZone == null ? m23487(str) : dateTimeZone;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23493(Exception exc) {
        exc.printStackTrace();
    }
}
