package org2.joda.time.tz;

import java.util.Collections;
import java.util.Set;
import org2.joda.time.DateTimeZone;

public final class UTCProvider implements Provider {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Set<String> f18251 = Collections.singleton("UTC");

    /* renamed from: 龘  reason: contains not printable characters */
    public Set<String> m23484() {
        return f18251;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public DateTimeZone m23485(String str) {
        if ("UTC".equalsIgnoreCase(str)) {
            return DateTimeZone.UTC;
        }
        return null;
    }
}
