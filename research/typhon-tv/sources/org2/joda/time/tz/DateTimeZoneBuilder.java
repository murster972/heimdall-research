package org2.joda.time.tz;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import org2.joda.time.Chronology;
import org2.joda.time.DateTimeZone;
import org2.joda.time.chrono.ISOChronology;

public class DateTimeZoneBuilder {

    private static final class DSTZone extends DateTimeZone {
        private static final long serialVersionUID = 6941492635554961361L;
        final Recurrence iEndRecurrence;
        final int iStandardOffset;
        final Recurrence iStartRecurrence;

        DSTZone(String str, int i, Recurrence recurrence, Recurrence recurrence2) {
            super(str);
            this.iStandardOffset = i;
            this.iStartRecurrence = recurrence;
            this.iEndRecurrence = recurrence2;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static DSTZone m23458(DataInput dataInput, String str) throws IOException {
            return new DSTZone(str, (int) DateTimeZoneBuilder.m23455(dataInput), Recurrence.m23468(dataInput), Recurrence.m23468(dataInput));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Recurrence m23459(long j) {
            long j2;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                j2 = recurrence.m23471(j, i, recurrence2.m23469());
            } catch (IllegalArgumentException e) {
                j2 = j;
            } catch (ArithmeticException e2) {
                j2 = j;
            }
            try {
                j = recurrence2.m23471(j, i, recurrence.m23469());
            } catch (ArithmeticException | IllegalArgumentException e3) {
            }
            return j2 > j ? recurrence : recurrence2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DSTZone)) {
                return false;
            }
            DSTZone dSTZone = (DSTZone) obj;
            return getID().equals(dSTZone.getID()) && this.iStandardOffset == dSTZone.iStandardOffset && this.iStartRecurrence.equals(dSTZone.iStartRecurrence) && this.iEndRecurrence.equals(dSTZone.iEndRecurrence);
        }

        public String getNameKey(long j) {
            return m23459(j).m23472();
        }

        public int getOffset(long j) {
            return this.iStandardOffset + m23459(j).m23469();
        }

        public int getStandardOffset(long j) {
            return this.iStandardOffset;
        }

        public boolean isFixed() {
            return false;
        }

        public long nextTransition(long j) {
            long j2;
            long j3;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                long r0 = recurrence.m23471(j, i, recurrence2.m23469());
                if (j > 0 && r0 < 0) {
                    r0 = j;
                }
                j2 = r0;
            } catch (IllegalArgumentException e) {
                j2 = j;
            } catch (ArithmeticException e2) {
                j2 = j;
            }
            try {
                long r02 = recurrence2.m23471(j, i, recurrence.m23469());
                if (j <= 0 || r02 >= 0) {
                    j = r02;
                }
                j3 = j;
            } catch (IllegalArgumentException e3) {
                j3 = j;
            } catch (ArithmeticException e4) {
                j3 = j;
            }
            return j2 > j3 ? j3 : j2;
        }

        public long previousTransition(long j) {
            long j2;
            long j3;
            long j4 = j + 1;
            int i = this.iStandardOffset;
            Recurrence recurrence = this.iStartRecurrence;
            Recurrence recurrence2 = this.iEndRecurrence;
            try {
                long r0 = recurrence.m23470(j4, i, recurrence2.m23469());
                if (j4 < 0 && r0 > 0) {
                    r0 = j4;
                }
                j2 = r0;
            } catch (IllegalArgumentException e) {
                j2 = j4;
            } catch (ArithmeticException e2) {
                j2 = j4;
            }
            try {
                long r02 = recurrence2.m23470(j4, i, recurrence.m23469());
                if (j4 >= 0 || r02 <= 0) {
                    j4 = r02;
                }
                j3 = j4;
            } catch (IllegalArgumentException e3) {
                j3 = j4;
            } catch (ArithmeticException e4) {
                j3 = j4;
            }
            if (j2 > j3) {
                j3 = j2;
            }
            return j3 - 1;
        }
    }

    private static final class OfYear {

        /* renamed from: ʻ  reason: contains not printable characters */
        final int f18240;

        /* renamed from: 连任  reason: contains not printable characters */
        final boolean f18241;

        /* renamed from: 靐  reason: contains not printable characters */
        final int f18242;

        /* renamed from: 麤  reason: contains not printable characters */
        final int f18243;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f18244;

        /* renamed from: 龘  reason: contains not printable characters */
        final char f18245;

        OfYear(char c, int i, int i2, int i3, boolean z, int i4) {
            if (c == 'u' || c == 'w' || c == 's') {
                this.f18245 = c;
                this.f18242 = i;
                this.f18244 = i2;
                this.f18243 = i3;
                this.f18241 = z;
                this.f18240 = i4;
                return;
            }
            throw new IllegalArgumentException("Unknown mode: " + c);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private long m23460(Chronology chronology, long j) {
            try {
                return m23462(chronology, j);
            } catch (IllegalArgumentException e) {
                if (this.f18242 == 2 && this.f18244 == 29) {
                    while (!chronology.year().isLeap(j)) {
                        j = chronology.year().add(j, -1);
                    }
                    return m23462(chronology, j);
                }
                throw e;
            }
        }

        /* renamed from: 麤  reason: contains not printable characters */
        private long m23461(Chronology chronology, long j) {
            int i = this.f18243 - chronology.dayOfWeek().get(j);
            if (i == 0) {
                return j;
            }
            if (this.f18241) {
                if (i < 0) {
                    i += 7;
                }
            } else if (i > 0) {
                i -= 7;
            }
            return chronology.dayOfWeek().add(j, i);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private long m23462(Chronology chronology, long j) {
            if (this.f18244 >= 0) {
                return chronology.dayOfMonth().set(j, this.f18244);
            }
            return chronology.dayOfMonth().add(chronology.monthOfYear().add(chronology.dayOfMonth().set(j, 1), 1), this.f18244);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private long m23463(Chronology chronology, long j) {
            try {
                return m23462(chronology, j);
            } catch (IllegalArgumentException e) {
                if (this.f18242 == 2 && this.f18244 == 29) {
                    while (!chronology.year().isLeap(j)) {
                        j = chronology.year().add(j, 1);
                    }
                    return m23462(chronology, j);
                }
                throw e;
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static OfYear m23464(DataInput dataInput) throws IOException {
            return new OfYear((char) dataInput.readUnsignedByte(), dataInput.readUnsignedByte(), dataInput.readByte(), dataInput.readUnsignedByte(), dataInput.readBoolean(), (int) DateTimeZoneBuilder.m23455(dataInput));
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OfYear)) {
                return false;
            }
            OfYear ofYear = (OfYear) obj;
            return this.f18245 == ofYear.f18245 && this.f18242 == ofYear.f18242 && this.f18244 == ofYear.f18244 && this.f18243 == ofYear.f18243 && this.f18241 == ofYear.f18241 && this.f18240 == ofYear.f18240;
        }

        public String toString() {
            return "[OfYear]\nMode: " + this.f18245 + 10 + "MonthOfYear: " + this.f18242 + 10 + "DayOfMonth: " + this.f18244 + 10 + "DayOfWeek: " + this.f18243 + 10 + "AdvanceDayOfWeek: " + this.f18241 + 10 + "MillisOfDay: " + this.f18240 + 10;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m23465(long j, int i, int i2) {
            if (this.f18245 == 'w') {
                i += i2;
            } else if (this.f18245 != 's') {
                i = 0;
            }
            long j2 = ((long) i) + j;
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long r0 = m23460(instanceUTC, instanceUTC.millisOfDay().add(instanceUTC.millisOfDay().set(instanceUTC.monthOfYear().set(j2, this.f18242), 0), this.f18240));
            if (this.f18243 != 0) {
                r0 = m23461(instanceUTC, r0);
                if (r0 >= j2) {
                    r0 = m23461(instanceUTC, m23460(instanceUTC, instanceUTC.monthOfYear().set(instanceUTC.year().add(r0, -1), this.f18242)));
                }
            } else if (r0 >= j2) {
                r0 = m23460(instanceUTC, instanceUTC.year().add(r0, -1));
            }
            return r0 - ((long) i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m23466(long j, int i, int i2) {
            if (this.f18245 == 'w') {
                i += i2;
            } else if (this.f18245 != 's') {
                i = 0;
            }
            long j2 = ((long) i) + j;
            ISOChronology instanceUTC = ISOChronology.getInstanceUTC();
            long r0 = m23463(instanceUTC, instanceUTC.millisOfDay().add(instanceUTC.millisOfDay().set(instanceUTC.monthOfYear().set(j2, this.f18242), 0), this.f18240));
            if (this.f18243 != 0) {
                r0 = m23461(instanceUTC, r0);
                if (r0 <= j2) {
                    r0 = m23461(instanceUTC, m23463(instanceUTC, instanceUTC.monthOfYear().set(instanceUTC.year().add(r0, 1), this.f18242)));
                }
            } else if (r0 <= j2) {
                r0 = m23463(instanceUTC, instanceUTC.year().add(r0, 1));
            }
            return r0 - ((long) i);
        }
    }

    private static final class PrecalculatedZone extends DateTimeZone {
        private static final long serialVersionUID = 7811976468055766265L;
        private final String[] iNameKeys;
        private final int[] iStandardOffsets;
        private final DSTZone iTailZone;
        private final long[] iTransitions;
        private final int[] iWallOffsets;

        private PrecalculatedZone(String str, long[] jArr, int[] iArr, int[] iArr2, String[] strArr, DSTZone dSTZone) {
            super(str);
            this.iTransitions = jArr;
            this.iWallOffsets = iArr;
            this.iStandardOffsets = iArr2;
            this.iNameKeys = strArr;
            this.iTailZone = dSTZone;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static PrecalculatedZone m23467(DataInput dataInput, String str) throws IOException {
            int readUnsignedShort;
            int readUnsignedShort2 = dataInput.readUnsignedShort();
            String[] strArr = new String[readUnsignedShort2];
            for (int i = 0; i < readUnsignedShort2; i++) {
                strArr[i] = dataInput.readUTF();
            }
            int readInt = dataInput.readInt();
            long[] jArr = new long[readInt];
            int[] iArr = new int[readInt];
            int[] iArr2 = new int[readInt];
            String[] strArr2 = new String[readInt];
            for (int i2 = 0; i2 < readInt; i2++) {
                jArr[i2] = DateTimeZoneBuilder.m23455(dataInput);
                iArr[i2] = (int) DateTimeZoneBuilder.m23455(dataInput);
                iArr2[i2] = (int) DateTimeZoneBuilder.m23455(dataInput);
                if (readUnsignedShort2 < 256) {
                    try {
                        readUnsignedShort = dataInput.readUnsignedByte();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        throw new IOException("Invalid encoding");
                    }
                } else {
                    readUnsignedShort = dataInput.readUnsignedShort();
                }
                strArr2[i2] = strArr[readUnsignedShort];
            }
            DSTZone dSTZone = null;
            if (dataInput.readBoolean()) {
                dSTZone = DSTZone.m23458(dataInput, str);
            }
            return new PrecalculatedZone(str, jArr, iArr, iArr2, strArr2, dSTZone);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PrecalculatedZone)) {
                return false;
            }
            PrecalculatedZone precalculatedZone = (PrecalculatedZone) obj;
            if (getID().equals(precalculatedZone.getID()) && Arrays.equals(this.iTransitions, precalculatedZone.iTransitions) && Arrays.equals(this.iNameKeys, precalculatedZone.iNameKeys) && Arrays.equals(this.iWallOffsets, precalculatedZone.iWallOffsets) && Arrays.equals(this.iStandardOffsets, precalculatedZone.iStandardOffsets)) {
                if (this.iTailZone == null) {
                    if (precalculatedZone.iTailZone == null) {
                        return true;
                    }
                } else if (this.iTailZone.equals(precalculatedZone.iTailZone)) {
                    return true;
                }
            }
            return false;
        }

        public String getNameKey(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iNameKeys[binarySearch];
            }
            int i = binarySearch ^ -1;
            return i < jArr.length ? i > 0 ? this.iNameKeys[i - 1] : "UTC" : this.iTailZone == null ? this.iNameKeys[i - 1] : this.iTailZone.getNameKey(j);
        }

        public int getOffset(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iWallOffsets[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i >= jArr.length) {
                return this.iTailZone == null ? this.iWallOffsets[i - 1] : this.iTailZone.getOffset(j);
            }
            if (i > 0) {
                return this.iWallOffsets[i - 1];
            }
            return 0;
        }

        public int getStandardOffset(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return this.iStandardOffsets[binarySearch];
            }
            int i = binarySearch ^ -1;
            if (i >= jArr.length) {
                return this.iTailZone == null ? this.iStandardOffsets[i - 1] : this.iTailZone.getStandardOffset(j);
            }
            if (i > 0) {
                return this.iStandardOffsets[i - 1];
            }
            return 0;
        }

        public boolean isFixed() {
            return false;
        }

        public long nextTransition(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            int i = binarySearch >= 0 ? binarySearch + 1 : binarySearch ^ -1;
            if (i < jArr.length) {
                return jArr[i];
            }
            if (this.iTailZone == null) {
                return j;
            }
            long j2 = jArr[jArr.length - 1];
            if (j < j2) {
                j = j2;
            }
            return this.iTailZone.nextTransition(j);
        }

        public long previousTransition(long j) {
            long[] jArr = this.iTransitions;
            int binarySearch = Arrays.binarySearch(jArr, j);
            if (binarySearch >= 0) {
                return j > Long.MIN_VALUE ? j - 1 : j;
            }
            int i = binarySearch ^ -1;
            if (i >= jArr.length) {
                if (this.iTailZone != null) {
                    long previousTransition = this.iTailZone.previousTransition(j);
                    if (previousTransition < j) {
                        return previousTransition;
                    }
                }
                long j2 = jArr[i - 1];
                return j2 > Long.MIN_VALUE ? j2 - 1 : j;
            } else if (i <= 0) {
                return j;
            } else {
                long j3 = jArr[i - 1];
                return j3 > Long.MIN_VALUE ? j3 - 1 : j;
            }
        }
    }

    private static final class Recurrence {

        /* renamed from: 靐  reason: contains not printable characters */
        final String f18246;

        /* renamed from: 齉  reason: contains not printable characters */
        final int f18247;

        /* renamed from: 龘  reason: contains not printable characters */
        final OfYear f18248;

        Recurrence(OfYear ofYear, String str, int i) {
            this.f18248 = ofYear;
            this.f18246 = str;
            this.f18247 = i;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Recurrence m23468(DataInput dataInput) throws IOException {
            return new Recurrence(OfYear.m23464(dataInput), dataInput.readUTF(), (int) DateTimeZoneBuilder.m23455(dataInput));
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Recurrence)) {
                return false;
            }
            Recurrence recurrence = (Recurrence) obj;
            return this.f18247 == recurrence.f18247 && this.f18246.equals(recurrence.f18246) && this.f18248.equals(recurrence.f18248);
        }

        public String toString() {
            return this.f18248 + " named " + this.f18246 + " at " + this.f18247;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m23469() {
            return this.f18247;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m23470(long j, int i, int i2) {
            return this.f18248.m23465(j, i, i2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m23471(long j, int i, int i2) {
            return this.f18248.m23466(j, i, i2);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public String m23472() {
            return this.f18246;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static long m23455(DataInput dataInput) throws IOException {
        int readUnsignedByte = dataInput.readUnsignedByte();
        switch (readUnsignedByte >> 6) {
            case 1:
                return ((long) (((readUnsignedByte << 26) >> 2) | (dataInput.readUnsignedByte() << 16) | (dataInput.readUnsignedByte() << 8) | dataInput.readUnsignedByte())) * 60000;
            case 2:
                return (((((long) readUnsignedByte) << 58) >> 26) | ((long) (dataInput.readUnsignedByte() << 24)) | ((long) (dataInput.readUnsignedByte() << 16)) | ((long) (dataInput.readUnsignedByte() << 8)) | ((long) dataInput.readUnsignedByte())) * 1000;
            case 3:
                return dataInput.readLong();
            default:
                return ((long) ((readUnsignedByte << 26) >> 26)) * 1800000;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeZone m23456(DataInput dataInput, String str) throws IOException {
        switch (dataInput.readUnsignedByte()) {
            case 67:
                return CachedDateTimeZone.forZone(PrecalculatedZone.m23467(dataInput, str));
            case 70:
                FixedDateTimeZone fixedDateTimeZone = new FixedDateTimeZone(str, dataInput.readUTF(), (int) m23455(dataInput), (int) m23455(dataInput));
                return fixedDateTimeZone.equals(DateTimeZone.UTC) ? DateTimeZone.UTC : fixedDateTimeZone;
            case 80:
                return PrecalculatedZone.m23467(dataInput, str);
            default:
                throw new IOException("Invalid encoding");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static DateTimeZone m23457(InputStream inputStream, String str) throws IOException {
        return inputStream instanceof DataInput ? m23456((DataInput) inputStream, str) : m23456((DataInput) new DataInputStream(inputStream), str);
    }
}
