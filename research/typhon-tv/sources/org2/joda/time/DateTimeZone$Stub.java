package org2.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;

final class DateTimeZone$Stub implements Serializable {
    private static final long serialVersionUID = -6471952376487863581L;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient String f17797;

    DateTimeZone$Stub(String str) {
        this.f17797 = str;
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        this.f17797 = objectInputStream.readUTF();
    }

    private Object readResolve() throws ObjectStreamException {
        return DateTimeZone.forID(this.f17797);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeUTF(this.f17797);
    }
}
