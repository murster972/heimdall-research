package org2.joda.time;

public interface ReadableDuration extends Comparable<ReadableDuration> {
    long getMillis();
}
