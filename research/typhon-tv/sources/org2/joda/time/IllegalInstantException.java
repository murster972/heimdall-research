package org2.joda.time;

import org2.joda.time.format.DateTimeFormat;

public class IllegalInstantException extends IllegalArgumentException {
    private static final long serialVersionUID = 2858712538216L;

    public IllegalInstantException(long j, String str) {
        super(m22729(j, str));
    }

    public IllegalInstantException(String str) {
        super(str);
    }

    public static boolean isIllegalInstant(Throwable th) {
        if (th instanceof IllegalInstantException) {
            return true;
        }
        if (th.getCause() == null || th.getCause() == th) {
            return false;
        }
        return isIllegalInstant(th.getCause());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m22729(long j, String str) {
        return "Illegal instant due to time zone offset transition (daylight savings time 'gap'): " + DateTimeFormat.m23049("yyyy-MM-dd'T'HH:mm:ss.SSS").m23066((ReadableInstant) new Instant(j)) + (str != null ? " (" + str + ")" : "");
    }
}
