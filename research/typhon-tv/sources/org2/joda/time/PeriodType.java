package org2.joda.time;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org2.joda.time.field.FieldUtils;

public class PeriodType implements Serializable {
    private static final long serialVersionUID = 2274324892792009998L;

    /* renamed from: ʻ  reason: contains not printable characters */
    static int f17817 = 5;

    /* renamed from: ʼ  reason: contains not printable characters */
    static int f17818 = 6;

    /* renamed from: ʽ  reason: contains not printable characters */
    static int f17819 = 7;

    /* renamed from: ʾ  reason: contains not printable characters */
    private static PeriodType f17820;

    /* renamed from: ʿ  reason: contains not printable characters */
    private static PeriodType f17821;

    /* renamed from: ˆ  reason: contains not printable characters */
    private static PeriodType f17822;

    /* renamed from: ˈ  reason: contains not printable characters */
    private static PeriodType f17823;

    /* renamed from: ˉ  reason: contains not printable characters */
    private static PeriodType f17824;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static PeriodType f17825;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static PeriodType f17826;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static PeriodType f17827;

    /* renamed from: ˏ  reason: contains not printable characters */
    private static PeriodType f17828;

    /* renamed from: ˑ  reason: contains not printable characters */
    private static final Map<PeriodType, Object> f17829 = new HashMap(32);

    /* renamed from: י  reason: contains not printable characters */
    private static PeriodType f17830;

    /* renamed from: ـ  reason: contains not printable characters */
    private static PeriodType f17831;

    /* renamed from: ٴ  reason: contains not printable characters */
    private static PeriodType f17832;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static PeriodType f17833;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private static PeriodType f17834;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private static PeriodType f17835;

    /* renamed from: 连任  reason: contains not printable characters */
    static int f17836 = 4;

    /* renamed from: 靐  reason: contains not printable characters */
    static int f17837 = 1;

    /* renamed from: 麤  reason: contains not printable characters */
    static int f17838 = 3;

    /* renamed from: 齉  reason: contains not printable characters */
    static int f17839 = 2;

    /* renamed from: 龘  reason: contains not printable characters */
    static int f17840 = 0;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static PeriodType f17841;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static PeriodType f17842;
    private final int[] iIndices;
    private final String iName;
    private final DurationFieldType[] iTypes;

    protected PeriodType(String str, DurationFieldType[] durationFieldTypeArr, int[] iArr) {
        this.iName = str;
        this.iTypes = durationFieldTypeArr;
        this.iIndices = iArr;
    }

    public static PeriodType dayTime() {
        PeriodType periodType = f17825;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("DayTime", new DurationFieldType[]{DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{-1, -1, -1, 0, 1, 2, 3, 4});
        f17825 = periodType2;
        return periodType2;
    }

    public static PeriodType days() {
        PeriodType periodType = f17828;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Days", new DurationFieldType[]{DurationFieldType.days()}, new int[]{-1, -1, -1, 0, -1, -1, -1, -1});
        f17828 = periodType2;
        return periodType2;
    }

    public static synchronized PeriodType forFields(DurationFieldType[] durationFieldTypeArr) {
        PeriodType periodType;
        synchronized (PeriodType.class) {
            if (durationFieldTypeArr != null) {
                if (durationFieldTypeArr.length != 0) {
                    for (DurationFieldType durationFieldType : durationFieldTypeArr) {
                        if (durationFieldType == null) {
                            throw new IllegalArgumentException("Types array must not contain null");
                        }
                    }
                    Map<PeriodType, Object> map = f17829;
                    if (map.isEmpty()) {
                        map.put(standard(), standard());
                        map.put(yearMonthDayTime(), yearMonthDayTime());
                        map.put(yearMonthDay(), yearMonthDay());
                        map.put(yearWeekDayTime(), yearWeekDayTime());
                        map.put(yearWeekDay(), yearWeekDay());
                        map.put(yearDayTime(), yearDayTime());
                        map.put(yearDay(), yearDay());
                        map.put(dayTime(), dayTime());
                        map.put(time(), time());
                        map.put(years(), years());
                        map.put(months(), months());
                        map.put(weeks(), weeks());
                        map.put(days(), days());
                        map.put(hours(), hours());
                        map.put(minutes(), minutes());
                        map.put(seconds(), seconds());
                        map.put(millis(), millis());
                    }
                    PeriodType periodType2 = new PeriodType((String) null, durationFieldTypeArr, (int[]) null);
                    Object obj = map.get(periodType2);
                    if (obj instanceof PeriodType) {
                        periodType = (PeriodType) obj;
                    } else if (obj != null) {
                        throw new IllegalArgumentException("PeriodType does not support fields: " + obj);
                    } else {
                        PeriodType standard = standard();
                        ArrayList arrayList = new ArrayList(Arrays.asList(durationFieldTypeArr));
                        if (!arrayList.remove(DurationFieldType.years())) {
                            standard = standard.withYearsRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.months())) {
                            standard = standard.withMonthsRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.weeks())) {
                            standard = standard.withWeeksRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.days())) {
                            standard = standard.withDaysRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.hours())) {
                            standard = standard.withHoursRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.minutes())) {
                            standard = standard.withMinutesRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.seconds())) {
                            standard = standard.withSecondsRemoved();
                        }
                        if (!arrayList.remove(DurationFieldType.millis())) {
                            standard = standard.withMillisRemoved();
                        }
                        if (arrayList.size() > 0) {
                            map.put(periodType2, arrayList);
                            throw new IllegalArgumentException("PeriodType does not support fields: " + arrayList);
                        }
                        PeriodType periodType3 = new PeriodType((String) null, standard.iTypes, (int[]) null);
                        periodType = (PeriodType) map.get(periodType3);
                        if (periodType != null) {
                            map.put(periodType3, periodType);
                        } else {
                            map.put(periodType3, standard);
                            periodType = standard;
                        }
                    }
                }
            }
            throw new IllegalArgumentException("Types array must not be null or empty");
        }
        return periodType;
    }

    public static PeriodType hours() {
        PeriodType periodType = f17830;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Hours", new DurationFieldType[]{DurationFieldType.hours()}, new int[]{-1, -1, -1, -1, 0, -1, -1, -1});
        f17830 = periodType2;
        return periodType2;
    }

    public static PeriodType millis() {
        PeriodType periodType = f17835;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Millis", new DurationFieldType[]{DurationFieldType.millis()}, new int[]{-1, -1, -1, -1, -1, -1, -1, 0});
        f17835 = periodType2;
        return periodType2;
    }

    public static PeriodType minutes() {
        PeriodType periodType = f17831;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Minutes", new DurationFieldType[]{DurationFieldType.minutes()}, new int[]{-1, -1, -1, -1, -1, 0, -1, -1});
        f17831 = periodType2;
        return periodType2;
    }

    public static PeriodType months() {
        PeriodType periodType = f17822;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Months", new DurationFieldType[]{DurationFieldType.months()}, new int[]{-1, 0, -1, -1, -1, -1, -1, -1});
        f17822 = periodType2;
        return periodType2;
    }

    public static PeriodType seconds() {
        PeriodType periodType = f17834;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Seconds", new DurationFieldType[]{DurationFieldType.seconds()}, new int[]{-1, -1, -1, -1, -1, -1, 0, -1});
        f17834 = periodType2;
        return periodType2;
    }

    public static PeriodType standard() {
        PeriodType periodType = f17832;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Standard", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.months(), DurationFieldType.weeks(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, 1, 2, 3, 4, 5, 6, 7});
        f17832 = periodType2;
        return periodType2;
    }

    public static PeriodType time() {
        PeriodType periodType = f17826;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Time", new DurationFieldType[]{DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{-1, -1, -1, -1, 0, 1, 2, 3});
        f17826 = periodType2;
        return periodType2;
    }

    public static PeriodType weeks() {
        PeriodType periodType = f17824;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Weeks", new DurationFieldType[]{DurationFieldType.weeks()}, new int[]{-1, -1, 0, -1, -1, -1, -1, -1});
        f17824 = periodType2;
        return periodType2;
    }

    public static PeriodType yearDay() {
        PeriodType periodType = f17842;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearDay", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.days()}, new int[]{0, -1, -1, 1, -1, -1, -1, -1});
        f17842 = periodType2;
        return periodType2;
    }

    public static PeriodType yearDayTime() {
        PeriodType periodType = f17841;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearDayTime", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, -1, -1, 1, 2, 3, 4, 5});
        f17841 = periodType2;
        return periodType2;
    }

    public static PeriodType yearMonthDay() {
        PeriodType periodType = f17823;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearMonthDay", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.months(), DurationFieldType.days()}, new int[]{0, 1, -1, 2, -1, -1, -1, -1});
        f17823 = periodType2;
        return periodType2;
    }

    public static PeriodType yearMonthDayTime() {
        PeriodType periodType = f17833;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearMonthDayTime", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.months(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, 1, -1, 2, 3, 4, 5, 6});
        f17833 = periodType2;
        return periodType2;
    }

    public static PeriodType yearWeekDay() {
        PeriodType periodType = f17821;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearWeekDay", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.weeks(), DurationFieldType.days()}, new int[]{0, -1, 1, 2, -1, -1, -1, -1});
        f17821 = periodType2;
        return periodType2;
    }

    public static PeriodType yearWeekDayTime() {
        PeriodType periodType = f17820;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("YearWeekDayTime", new DurationFieldType[]{DurationFieldType.years(), DurationFieldType.weeks(), DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes(), DurationFieldType.seconds(), DurationFieldType.millis()}, new int[]{0, -1, 1, 2, 3, 4, 5, 6});
        f17820 = periodType2;
        return periodType2;
    }

    public static PeriodType years() {
        PeriodType periodType = f17827;
        if (periodType != null) {
            return periodType;
        }
        PeriodType periodType2 = new PeriodType("Years", new DurationFieldType[]{DurationFieldType.years()}, new int[]{0, -1, -1, -1, -1, -1, -1, -1});
        f17827 = periodType2;
        return periodType2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private PeriodType m22741(int i, String str) {
        int i2 = this.iIndices[i];
        if (i2 == -1) {
            return this;
        }
        DurationFieldType[] durationFieldTypeArr = new DurationFieldType[(size() - 1)];
        for (int i3 = 0; i3 < this.iTypes.length; i3++) {
            if (i3 < i2) {
                durationFieldTypeArr[i3] = this.iTypes[i3];
            } else if (i3 > i2) {
                durationFieldTypeArr[i3 - 1] = this.iTypes[i3];
            }
        }
        int[] iArr = new int[8];
        for (int i4 = 0; i4 < iArr.length; i4++) {
            if (i4 < i) {
                iArr[i4] = this.iIndices[i4];
            } else if (i4 > i) {
                iArr[i4] = this.iIndices[i4] == -1 ? -1 : this.iIndices[i4] - 1;
            } else {
                iArr[i4] = -1;
            }
        }
        return new PeriodType(getName() + str, durationFieldTypeArr, iArr);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PeriodType)) {
            return false;
        }
        return Arrays.equals(this.iTypes, ((PeriodType) obj).iTypes);
    }

    public DurationFieldType getFieldType(int i) {
        return this.iTypes[i];
    }

    public String getName() {
        return this.iName;
    }

    public int hashCode() {
        int i = 0;
        for (DurationFieldType hashCode : this.iTypes) {
            i += hashCode.hashCode();
        }
        return i;
    }

    public int indexOf(DurationFieldType durationFieldType) {
        int size = size();
        for (int i = 0; i < size; i++) {
            if (this.iTypes[i] == durationFieldType) {
                return i;
            }
        }
        return -1;
    }

    public boolean isSupported(DurationFieldType durationFieldType) {
        return indexOf(durationFieldType) >= 0;
    }

    public int size() {
        return this.iTypes.length;
    }

    public String toString() {
        return "PeriodType[" + getName() + "]";
    }

    public PeriodType withDaysRemoved() {
        return m22741(3, "NoDays");
    }

    public PeriodType withHoursRemoved() {
        return m22741(4, "NoHours");
    }

    public PeriodType withMillisRemoved() {
        return m22741(7, "NoMillis");
    }

    public PeriodType withMinutesRemoved() {
        return m22741(5, "NoMinutes");
    }

    public PeriodType withMonthsRemoved() {
        return m22741(1, "NoMonths");
    }

    public PeriodType withSecondsRemoved() {
        return m22741(6, "NoSeconds");
    }

    public PeriodType withWeeksRemoved() {
        return m22741(2, "NoWeeks");
    }

    public PeriodType withYearsRemoved() {
        return m22741(0, "NoYears");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m22742(ReadablePeriod readablePeriod, int i, int[] iArr, int i2) {
        if (i2 == 0) {
            return false;
        }
        int i3 = this.iIndices[i];
        if (i3 == -1) {
            throw new UnsupportedOperationException("Field is not supported");
        }
        iArr[i3] = FieldUtils.m23031(iArr[i3], i2);
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m22743(ReadablePeriod readablePeriod, int i) {
        int i2 = this.iIndices[i];
        if (i2 == -1) {
            return 0;
        }
        return readablePeriod.getValue(i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22744(ReadablePeriod readablePeriod, int i, int[] iArr, int i2) {
        int i3 = this.iIndices[i];
        if (i3 == -1) {
            throw new UnsupportedOperationException("Field is not supported");
        }
        iArr[i3] = i2;
        return true;
    }
}
