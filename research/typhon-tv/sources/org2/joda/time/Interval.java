package org2.joda.time;

import java.io.Serializable;
import org2.joda.time.base.BaseInterval;
import org2.joda.time.chrono.ISOChronology;
import org2.joda.time.format.DateTimeFormatter;
import org2.joda.time.format.ISODateTimeFormat;
import org2.joda.time.format.ISOPeriodFormat;
import org2.joda.time.format.PeriodFormatter;

public final class Interval extends BaseInterval implements Serializable, ReadableInterval {
    private static final long serialVersionUID = 4922451897541386752L;

    public Interval(long j, long j2) {
        super(j, j2, (Chronology) null);
    }

    public Interval(long j, long j2, Chronology chronology) {
        super(j, j2, chronology);
    }

    public Interval(long j, long j2, DateTimeZone dateTimeZone) {
        super(j, j2, ISOChronology.getInstance(dateTimeZone));
    }

    public Interval(Object obj) {
        super(obj, (Chronology) null);
    }

    public Interval(Object obj, Chronology chronology) {
        super(obj, chronology);
    }

    public Interval(ReadableDuration readableDuration, ReadableInstant readableInstant) {
        super(readableDuration, readableInstant);
    }

    public Interval(ReadableInstant readableInstant, ReadableDuration readableDuration) {
        super(readableInstant, readableDuration);
    }

    public Interval(ReadableInstant readableInstant, ReadableInstant readableInstant2) {
        super(readableInstant, readableInstant2);
    }

    public Interval(ReadableInstant readableInstant, ReadablePeriod readablePeriod) {
        super(readableInstant, readablePeriod);
    }

    public Interval(ReadablePeriod readablePeriod, ReadableInstant readableInstant) {
        super(readablePeriod, readableInstant);
    }

    public static Interval parse(String str) {
        return new Interval(str);
    }

    public static Interval parseWithOffset(String str) {
        DateTime dateTime;
        ReadablePeriod readablePeriod = null;
        int indexOf = str.indexOf(47);
        if (indexOf < 0) {
            throw new IllegalArgumentException("Format requires a '/' separator: " + str);
        }
        String substring = str.substring(0, indexOf);
        if (substring.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        String substring2 = str.substring(indexOf + 1);
        if (substring2.length() <= 0) {
            throw new IllegalArgumentException("Format invalid: " + str);
        }
        DateTimeFormatter r4 = ISODateTimeFormat.m23270().m23058();
        PeriodFormatter r5 = ISOPeriodFormat.m23353();
        char charAt = substring.charAt(0);
        if (charAt == 'P' || charAt == 'p') {
            dateTime = null;
            readablePeriod = r5.m23378(PeriodType.standard()).m23377(substring);
        } else {
            dateTime = r4.m23057(substring);
        }
        char charAt2 = substring2.charAt(0);
        if (charAt2 != 'P' && charAt2 != 'p') {
            DateTime r2 = r4.m23057(substring2);
            return readablePeriod != null ? new Interval(readablePeriod, (ReadableInstant) r2) : new Interval((ReadableInstant) dateTime, (ReadableInstant) r2);
        } else if (readablePeriod == null) {
            return new Interval((ReadableInstant) dateTime, (ReadablePeriod) r5.m23378(PeriodType.standard()).m23377(substring2));
        } else {
            throw new IllegalArgumentException("Interval composed of two durations: " + str);
        }
    }

    public boolean abuts(ReadableInterval readableInterval) {
        if (readableInterval != null) {
            return readableInterval.getEndMillis() == getStartMillis() || getEndMillis() == readableInterval.getStartMillis();
        }
        long r2 = DateTimeUtils.m22712();
        return getStartMillis() == r2 || getEndMillis() == r2;
    }

    public Interval gap(ReadableInterval readableInterval) {
        ReadableInterval r0 = DateTimeUtils.m22710(readableInterval);
        long startMillis = r0.getStartMillis();
        long endMillis = r0.getEndMillis();
        long startMillis2 = getStartMillis();
        long endMillis2 = getEndMillis();
        if (startMillis2 > endMillis) {
            return new Interval(endMillis, startMillis2, getChronology());
        }
        if (startMillis <= endMillis2) {
            return null;
        }
        return new Interval(endMillis2, startMillis, getChronology());
    }

    public Interval overlap(ReadableInterval readableInterval) {
        ReadableInterval r0 = DateTimeUtils.m22710(readableInterval);
        if (!overlaps(r0)) {
            return null;
        }
        return new Interval(Math.max(getStartMillis(), r0.getStartMillis()), Math.min(getEndMillis(), r0.getEndMillis()), getChronology());
    }

    public Interval toInterval() {
        return this;
    }

    public Interval withChronology(Chronology chronology) {
        return getChronology() == chronology ? this : new Interval(getStartMillis(), getEndMillis(), chronology);
    }

    public Interval withDurationAfterStart(ReadableDuration readableDuration) {
        long r4 = DateTimeUtils.m22713(readableDuration);
        if (r4 == toDurationMillis()) {
            return this;
        }
        Chronology chronology = getChronology();
        long startMillis = getStartMillis();
        return new Interval(startMillis, chronology.add(startMillis, r4, 1), chronology);
    }

    public Interval withDurationBeforeEnd(ReadableDuration readableDuration) {
        long r4 = DateTimeUtils.m22713(readableDuration);
        if (r4 == toDurationMillis()) {
            return this;
        }
        Chronology chronology = getChronology();
        long endMillis = getEndMillis();
        return new Interval(chronology.add(endMillis, r4, -1), endMillis, chronology);
    }

    public Interval withEnd(ReadableInstant readableInstant) {
        return withEndMillis(DateTimeUtils.m22714(readableInstant));
    }

    public Interval withEndMillis(long j) {
        if (j == getEndMillis()) {
            return this;
        }
        return new Interval(getStartMillis(), j, getChronology());
    }

    public Interval withPeriodAfterStart(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            return withDurationAfterStart((ReadableDuration) null);
        }
        Chronology chronology = getChronology();
        long startMillis = getStartMillis();
        return new Interval(startMillis, chronology.add(readablePeriod, startMillis, 1), chronology);
    }

    public Interval withPeriodBeforeEnd(ReadablePeriod readablePeriod) {
        if (readablePeriod == null) {
            return withDurationBeforeEnd((ReadableDuration) null);
        }
        Chronology chronology = getChronology();
        long endMillis = getEndMillis();
        return new Interval(chronology.add(readablePeriod, endMillis, -1), endMillis, chronology);
    }

    public Interval withStart(ReadableInstant readableInstant) {
        return withStartMillis(DateTimeUtils.m22714(readableInstant));
    }

    public Interval withStartMillis(long j) {
        if (j == getStartMillis()) {
            return this;
        }
        return new Interval(j, getEndMillis(), getChronology());
    }
}
