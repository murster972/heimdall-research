package org2.mozilla.universalchardet.prober;

import org2.mozilla.universalchardet.prober.CharsetProber;

public class MBCSGroupProber extends CharsetProber {
    private int activeNum;
    private int bestGuess;
    private boolean[] isActive = new boolean[7];
    private CharsetProber[] probers = new CharsetProber[7];
    private CharsetProber.ProbingState state;

    public MBCSGroupProber() {
        this.probers[0] = new UTF8Prober();
        this.probers[1] = new SJISProber();
        this.probers[2] = new EUCJPProber();
        this.probers[3] = new GB18030Prober();
        this.probers[4] = new EUCKRProber();
        this.probers[5] = new Big5Prober();
        this.probers[6] = new EUCTWProber();
        reset();
    }

    public String getCharSetName() {
        if (this.bestGuess == -1) {
            getConfidence();
            if (this.bestGuess == -1) {
                this.bestGuess = 0;
            }
        }
        return this.probers[this.bestGuess].getCharSetName();
    }

    public float getConfidence() {
        float f = 0.0f;
        if (this.state == CharsetProber.ProbingState.FOUND_IT) {
            return 0.99f;
        }
        if (this.state == CharsetProber.ProbingState.NOT_ME) {
            return 0.01f;
        }
        for (int i = 0; i < this.probers.length; i++) {
            if (this.isActive[i]) {
                float confidence = this.probers[i].getConfidence();
                if (f < confidence) {
                    this.bestGuess = i;
                    f = confidence;
                }
            }
        }
        return f;
    }

    public CharsetProber.ProbingState getState() {
        return this.state;
    }

    public CharsetProber.ProbingState handleData(byte[] bArr, int i, int i2) {
        boolean z;
        int i3;
        byte[] bArr2 = new byte[i2];
        int i4 = i + i2;
        int i5 = 0;
        boolean z2 = true;
        while (i < i4) {
            if ((bArr[i] & 128) != 0) {
                i3 = i5 + 1;
                bArr2[i5] = bArr[i];
                z = true;
            } else if (z2) {
                i3 = i5 + 1;
                bArr2[i5] = bArr[i];
                z = false;
            } else {
                int i6 = i5;
                z = z2;
                i3 = i6;
            }
            i++;
            int i7 = i3;
            z2 = z;
            i5 = i7;
        }
        int i8 = 0;
        while (true) {
            if (i8 >= this.probers.length) {
                break;
            }
            if (this.isActive[i8]) {
                CharsetProber.ProbingState handleData = this.probers[i8].handleData(bArr2, 0, i5);
                if (handleData == CharsetProber.ProbingState.FOUND_IT) {
                    this.bestGuess = i8;
                    this.state = CharsetProber.ProbingState.FOUND_IT;
                    break;
                } else if (handleData == CharsetProber.ProbingState.NOT_ME) {
                    this.isActive[i8] = false;
                    this.activeNum--;
                    if (this.activeNum <= 0) {
                        this.state = CharsetProber.ProbingState.NOT_ME;
                        break;
                    }
                } else {
                    continue;
                }
            }
            i8++;
        }
        return this.state;
    }

    public void reset() {
        this.activeNum = 0;
        for (int i = 0; i < this.probers.length; i++) {
            this.probers[i].reset();
            this.isActive[i] = true;
            this.activeNum++;
        }
        this.bestGuess = -1;
        this.state = CharsetProber.ProbingState.DETECTING;
    }

    public void setOption() {
    }
}
