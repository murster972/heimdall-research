package org2.mozilla.universalchardet;

public interface CharsetListener {
    void report(String str);
}
