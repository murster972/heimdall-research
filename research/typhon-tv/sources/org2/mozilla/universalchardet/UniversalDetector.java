package org2.mozilla.universalchardet;

import java.io.FileInputStream;
import org2.mozilla.universalchardet.prober.CharsetProber;
import org2.mozilla.universalchardet.prober.EscCharsetProber;
import org2.mozilla.universalchardet.prober.Latin1Prober;
import org2.mozilla.universalchardet.prober.MBCSGroupProber;
import org2.mozilla.universalchardet.prober.SBCSGroupProber;

public class UniversalDetector {
    public static final float MINIMUM_THRESHOLD = 0.2f;
    public static final float SHORTCUT_THRESHOLD = 0.95f;
    private String detectedCharset;
    private boolean done;
    private CharsetProber escCharsetProber = null;
    private boolean gotData;
    private InputState inputState;
    private byte lastChar;
    private CharsetListener listener;
    private CharsetProber[] probers = new CharsetProber[3];
    private boolean start;

    public enum InputState {
        PURE_ASCII,
        ESC_ASCII,
        HIGHBYTE
    }

    public UniversalDetector(CharsetListener charsetListener) {
        this.listener = charsetListener;
        for (int i = 0; i < this.probers.length; i++) {
            this.probers[i] = null;
        }
        reset();
    }

    public static void main(String[] strArr) throws Exception {
        if (strArr.length != 1) {
            System.out.println("USAGE: java UniversalDetector filename");
            return;
        }
        UniversalDetector universalDetector = new UniversalDetector(new CharsetListener() {
            public void report(String str) {
                System.out.println("charset = " + str);
            }
        });
        byte[] bArr = new byte[4096];
        FileInputStream fileInputStream = new FileInputStream(strArr[0]);
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read <= 0 || universalDetector.isDone()) {
                universalDetector.dataEnd();
            } else {
                universalDetector.handleData(bArr, 0, read);
            }
        }
        universalDetector.dataEnd();
    }

    public void dataEnd() {
        if (this.gotData) {
            if (this.detectedCharset != null) {
                this.done = true;
                if (this.listener != null) {
                    this.listener.report(this.detectedCharset);
                }
            } else if (this.inputState == InputState.HIGHBYTE) {
                float f = 0.0f;
                int i = 0;
                for (int i2 = 0; i2 < this.probers.length; i2++) {
                    float confidence = this.probers[i2].getConfidence();
                    if (confidence > f) {
                        i = i2;
                        f = confidence;
                    }
                }
                if (f > 0.2f) {
                    this.detectedCharset = this.probers[i].getCharSetName();
                    if (this.listener != null) {
                        this.listener.report(this.detectedCharset);
                    }
                }
            } else {
                if (this.inputState == InputState.ESC_ASCII) {
                }
            }
        }
    }

    public String getDetectedCharset() {
        return this.detectedCharset;
    }

    public CharsetListener getListener() {
        return this.listener;
    }

    public void handleData(byte[] bArr, int i, int i2) {
        if (!this.done) {
            if (i2 > 0) {
                this.gotData = true;
            }
            if (this.start) {
                this.start = false;
                if (i2 > 3) {
                    byte b = bArr[i] & 255;
                    byte b2 = bArr[i + 1] & 255;
                    byte b3 = bArr[i + 2] & 255;
                    byte b4 = bArr[i + 3] & 255;
                    switch (b) {
                        case 0:
                            if (b2 != 0 || b3 != 254 || b4 != 255) {
                                if (b2 == 0 && b3 == 255 && b4 == 254) {
                                    this.detectedCharset = TyphoonApp.CHARSET_X_ISO_10646_UCS_4_2143;
                                    break;
                                }
                            } else {
                                this.detectedCharset = TyphoonApp.CHARSET_UTF_32BE;
                                break;
                            }
                        case 239:
                            if (b2 == 187 && b3 == 191) {
                                this.detectedCharset = TyphoonApp.CHARSET_UTF_8;
                                break;
                            }
                        case 254:
                            if (b2 != 255 || b3 != 0 || b4 != 0) {
                                if (b2 == 255) {
                                    this.detectedCharset = TyphoonApp.CHARSET_UTF_16BE;
                                    break;
                                }
                            } else {
                                this.detectedCharset = TyphoonApp.CHARSET_X_ISO_10646_UCS_4_3412;
                                break;
                            }
                            break;
                        case 255:
                            if (b2 != 254 || b3 != 0 || b4 != 0) {
                                if (b2 == 254) {
                                    this.detectedCharset = TyphoonApp.CHARSET_UTF_16LE;
                                    break;
                                }
                            } else {
                                this.detectedCharset = TyphoonApp.CHARSET_UTF_32LE;
                                break;
                            }
                            break;
                    }
                    if (this.detectedCharset != null) {
                        this.done = true;
                        return;
                    }
                }
            }
            int i3 = i + i2;
            for (int i4 = i; i4 < i3; i4++) {
                byte b5 = bArr[i4] & 255;
                if ((b5 & 128) == 0 || b5 == 160) {
                    if (this.inputState == InputState.PURE_ASCII && (b5 == 27 || (b5 == 123 && this.lastChar == 126))) {
                        this.inputState = InputState.ESC_ASCII;
                    }
                    this.lastChar = bArr[i4];
                } else if (this.inputState != InputState.HIGHBYTE) {
                    this.inputState = InputState.HIGHBYTE;
                    if (this.escCharsetProber != null) {
                        this.escCharsetProber = null;
                    }
                    if (this.probers[0] == null) {
                        this.probers[0] = new MBCSGroupProber();
                    }
                    if (this.probers[1] == null) {
                        this.probers[1] = new SBCSGroupProber();
                    }
                    if (this.probers[2] == null) {
                        this.probers[2] = new Latin1Prober();
                    }
                }
            }
            if (this.inputState == InputState.ESC_ASCII) {
                if (this.escCharsetProber == null) {
                    this.escCharsetProber = new EscCharsetProber();
                }
                if (this.escCharsetProber.handleData(bArr, i, i2) == CharsetProber.ProbingState.FOUND_IT) {
                    this.done = true;
                    this.detectedCharset = this.escCharsetProber.getCharSetName();
                }
            } else if (this.inputState == InputState.HIGHBYTE) {
                for (int i5 = 0; i5 < this.probers.length; i5++) {
                    if (this.probers[i5].handleData(bArr, i, i2) == CharsetProber.ProbingState.FOUND_IT) {
                        this.done = true;
                        this.detectedCharset = this.probers[i5].getCharSetName();
                        return;
                    }
                }
            }
        }
    }

    public boolean isDone() {
        return this.done;
    }

    public void reset() {
        this.done = false;
        this.start = true;
        this.detectedCharset = null;
        this.gotData = false;
        this.inputState = InputState.PURE_ASCII;
        this.lastChar = 0;
        if (this.escCharsetProber != null) {
            this.escCharsetProber.reset();
        }
        for (int i = 0; i < this.probers.length; i++) {
            if (this.probers[i] != null) {
                this.probers[i].reset();
            }
        }
    }

    public void setListener(CharsetListener charsetListener) {
        this.listener = charsetListener;
    }
}
