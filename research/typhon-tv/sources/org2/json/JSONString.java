package org2.json;

public interface JSONString {
    String toJSONString();
}
