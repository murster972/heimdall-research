package org2.json;

import java.util.HashMap;

public class XMLTokener extends JSONTokener {
    public static final HashMap entity = new HashMap(8);

    static {
        entity.put("amp", XML.AMP);
        entity.put("apos", XML.APOS);
        entity.put("gt", XML.GT);
        entity.put("lt", XML.LT);
        entity.put("quot", XML.QUOT);
    }

    public XMLTokener(String str) {
        super(str);
    }

    public String nextCDATA() throws JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            char next = next();
            if (end()) {
                throw syntaxError("Unclosed CDATA");
            }
            stringBuffer.append(next);
            int length = stringBuffer.length() - 3;
            if (length >= 0 && stringBuffer.charAt(length) == ']' && stringBuffer.charAt(length + 1) == ']' && stringBuffer.charAt(length + 2) == '>') {
                stringBuffer.setLength(length);
                return stringBuffer.toString();
            }
        }
    }

    public Object nextContent() throws JSONException {
        char next;
        do {
            next = next();
        } while (Character.isWhitespace(next));
        if (next == 0) {
            return null;
        }
        if (next == '<') {
            return XML.LT;
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (next != '<' && next != 0) {
            if (next == '&') {
                stringBuffer.append(nextEntity(next));
            } else {
                stringBuffer.append(next);
            }
            next = next();
        }
        back();
        return stringBuffer.toString().trim();
    }

    public Object nextEntity(char c) throws JSONException {
        char next;
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            next = next();
            if (!Character.isLetterOrDigit(next) && next != '#') {
                break;
            }
            stringBuffer.append(Character.toLowerCase(next));
        }
        if (next == ';') {
            String stringBuffer2 = stringBuffer.toString();
            Object obj = entity.get(stringBuffer2);
            return obj != null ? obj : new StringBuffer().append(c).append(stringBuffer2).append(";").toString();
        }
        throw syntaxError(new StringBuffer().append("Missing ';' in XML entity: &").append(stringBuffer).toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        r1 = next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        if (r1 != 0) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0041, code lost:
        throw syntaxError("Unterminated string");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
        if (r1 != r0) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0047, code lost:
        switch(r0) {
            case 0: goto L_0x004b;
            case 33: goto L_0x004b;
            case 34: goto L_0x004b;
            case 39: goto L_0x004b;
            case 47: goto L_0x004b;
            case 60: goto L_0x004b;
            case 61: goto L_0x004b;
            case 62: goto L_0x004b;
            case 63: goto L_0x004b;
            default: goto L_0x004a;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004b, code lost:
        back();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return java.lang.Boolean.TRUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return java.lang.Boolean.TRUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return java.lang.Boolean.TRUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        r0 = next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0015, code lost:
        if (java.lang.Character.isWhitespace(r0) == false) goto L_0x0047;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object nextMeta() throws org2.json.JSONException {
        /*
            r2 = this;
        L_0x0000:
            char r0 = r2.next()
            boolean r1 = java.lang.Character.isWhitespace(r0)
            if (r1 != 0) goto L_0x0000
            switch(r0) {
                case 0: goto L_0x001a;
                case 33: goto L_0x002e;
                case 34: goto L_0x0034;
                case 39: goto L_0x0034;
                case 47: goto L_0x0028;
                case 60: goto L_0x0022;
                case 61: goto L_0x002b;
                case 62: goto L_0x0025;
                case 63: goto L_0x0031;
                default: goto L_0x000d;
            }
        L_0x000d:
            char r0 = r2.next()
            boolean r1 = java.lang.Character.isWhitespace(r0)
            if (r1 == 0) goto L_0x0047
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
        L_0x0019:
            return r0
        L_0x001a:
            java.lang.String r0 = "Misshaped meta tag"
            org2.json.JSONException r0 = r2.syntaxError(r0)
            throw r0
        L_0x0022:
            java.lang.Character r0 = org2.json.XML.LT
            goto L_0x0019
        L_0x0025:
            java.lang.Character r0 = org2.json.XML.GT
            goto L_0x0019
        L_0x0028:
            java.lang.Character r0 = org2.json.XML.SLASH
            goto L_0x0019
        L_0x002b:
            java.lang.Character r0 = org2.json.XML.EQ
            goto L_0x0019
        L_0x002e:
            java.lang.Character r0 = org2.json.XML.BANG
            goto L_0x0019
        L_0x0031:
            java.lang.Character r0 = org2.json.XML.QUEST
            goto L_0x0019
        L_0x0034:
            char r1 = r2.next()
            if (r1 != 0) goto L_0x0042
            java.lang.String r0 = "Unterminated string"
            org2.json.JSONException r0 = r2.syntaxError(r0)
            throw r0
        L_0x0042:
            if (r1 != r0) goto L_0x0034
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            goto L_0x0019
        L_0x0047:
            switch(r0) {
                case 0: goto L_0x004b;
                case 33: goto L_0x004b;
                case 34: goto L_0x004b;
                case 39: goto L_0x004b;
                case 47: goto L_0x004b;
                case 60: goto L_0x004b;
                case 61: goto L_0x004b;
                case 62: goto L_0x004b;
                case 63: goto L_0x004b;
                default: goto L_0x004a;
            }
        L_0x004a:
            goto L_0x000d
        L_0x004b:
            r2.back()
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.json.XMLTokener.nextMeta():java.lang.Object");
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0012 A[LOOP_START, PHI: r0 
      PHI: (r0v13 char) = (r0v0 char), (r0v14 char) binds: [B:3:0x000d, B:27:0x006d] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object nextToken() throws org2.json.JSONException {
        /*
            r4 = this;
        L_0x0000:
            char r0 = r4.next()
            boolean r1 = java.lang.Character.isWhitespace(r0)
            if (r1 != 0) goto L_0x0000
            switch(r0) {
                case 0: goto L_0x0024;
                case 33: goto L_0x003d;
                case 34: goto L_0x0043;
                case 39: goto L_0x0043;
                case 47: goto L_0x0037;
                case 60: goto L_0x002c;
                case 61: goto L_0x003a;
                case 62: goto L_0x0034;
                case 63: goto L_0x0040;
                default: goto L_0x000d;
            }
        L_0x000d:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0012:
            r1.append(r0)
            char r0 = r4.next()
            boolean r2 = java.lang.Character.isWhitespace(r0)
            if (r2 == 0) goto L_0x006d
            java.lang.String r0 = r1.toString()
        L_0x0023:
            return r0
        L_0x0024:
            java.lang.String r0 = "Misshaped element"
            org2.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        L_0x002c:
            java.lang.String r0 = "Misplaced '<'"
            org2.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        L_0x0034:
            java.lang.Character r0 = org2.json.XML.GT
            goto L_0x0023
        L_0x0037:
            java.lang.Character r0 = org2.json.XML.SLASH
            goto L_0x0023
        L_0x003a:
            java.lang.Character r0 = org2.json.XML.EQ
            goto L_0x0023
        L_0x003d:
            java.lang.Character r0 = org2.json.XML.BANG
            goto L_0x0023
        L_0x0040:
            java.lang.Character r0 = org2.json.XML.QUEST
            goto L_0x0023
        L_0x0043:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0048:
            char r2 = r4.next()
            if (r2 != 0) goto L_0x0056
            java.lang.String r0 = "Unterminated string"
            org2.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        L_0x0056:
            if (r2 != r0) goto L_0x005d
            java.lang.String r0 = r1.toString()
            goto L_0x0023
        L_0x005d:
            r3 = 38
            if (r2 != r3) goto L_0x0069
            java.lang.Object r2 = r4.nextEntity(r2)
            r1.append(r2)
            goto L_0x0048
        L_0x0069:
            r1.append(r2)
            goto L_0x0048
        L_0x006d:
            switch(r0) {
                case 0: goto L_0x0071;
                case 33: goto L_0x0076;
                case 34: goto L_0x007e;
                case 39: goto L_0x007e;
                case 47: goto L_0x0076;
                case 60: goto L_0x007e;
                case 61: goto L_0x0076;
                case 62: goto L_0x0076;
                case 63: goto L_0x0076;
                case 91: goto L_0x0076;
                case 93: goto L_0x0076;
                default: goto L_0x0070;
            }
        L_0x0070:
            goto L_0x0012
        L_0x0071:
            java.lang.String r0 = r1.toString()
            goto L_0x0023
        L_0x0076:
            r4.back()
            java.lang.String r0 = r1.toString()
            goto L_0x0023
        L_0x007e:
            java.lang.String r0 = "Bad character in a name"
            org2.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.json.XMLTokener.nextToken():java.lang.Object");
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 129 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean skipPast(java.lang.String r10) throws org2.json.JSONException {
        /*
            r9 = this;
            r3 = 1
            r1 = 0
            int r5 = r10.length()
            char[] r6 = new char[r5]
            r0 = r1
        L_0x0009:
            if (r0 >= r5) goto L_0x0040
            char r2 = r9.next()
            if (r2 != 0) goto L_0x0012
        L_0x0011:
            return r1
        L_0x0012:
            r6[r0] = r2
            int r0 = r0 + 1
            goto L_0x0009
        L_0x0017:
            r6[r0] = r2
            int r0 = r0 + 1
            if (r0 < r5) goto L_0x001e
            int r0 = r0 - r5
        L_0x001e:
            r2 = r0
            r4 = r1
        L_0x0020:
            if (r4 >= r5) goto L_0x003e
            char r7 = r6[r2]
            char r8 = r10.charAt(r4)
            if (r7 == r8) goto L_0x002f
            r2 = r1
        L_0x002b:
            if (r2 == 0) goto L_0x0037
            r1 = r3
            goto L_0x0011
        L_0x002f:
            int r2 = r2 + 1
            if (r2 < r5) goto L_0x0034
            int r2 = r2 - r5
        L_0x0034:
            int r4 = r4 + 1
            goto L_0x0020
        L_0x0037:
            char r2 = r9.next()
            if (r2 != 0) goto L_0x0017
            goto L_0x0011
        L_0x003e:
            r2 = r3
            goto L_0x002b
        L_0x0040:
            r0 = r1
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.json.XMLTokener.skipPast(java.lang.String):boolean");
    }
}
