package org2.json;

import java.util.Iterator;

public class XML {
    public static final Character AMP = new Character('&');
    public static final Character APOS = new Character('\'');
    public static final Character BANG = new Character('!');
    public static final Character EQ = new Character('=');
    public static final Character GT = new Character('>');
    public static final Character LT = new Character('<');
    public static final Character QUEST = new Character('?');
    public static final Character QUOT = new Character('\"');
    public static final Character SLASH = new Character('/');

    public static String escape(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case '\"':
                    stringBuffer.append("&quot;");
                    break;
                case '&':
                    stringBuffer.append("&amp;");
                    break;
                case '\'':
                    stringBuffer.append("&apos;");
                    break;
                case '<':
                    stringBuffer.append("&lt;");
                    break;
                case '>':
                    stringBuffer.append("&gt;");
                    break;
                default:
                    stringBuffer.append(charAt);
                    break;
            }
        }
        return stringBuffer.toString();
    }

    public static void noSpace(String str) throws JSONException {
        int length = str.length();
        if (length == 0) {
            throw new JSONException("Empty string.");
        }
        for (int i = 0; i < length; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                throw new JSONException(new StringBuffer().append("'").append(str).append("' contains a space character.").toString());
            }
        }
    }

    private static boolean parse(XMLTokener xMLTokener, JSONObject jSONObject, String str) throws JSONException {
        Object nextToken = xMLTokener.nextToken();
        if (nextToken == BANG) {
            char next = xMLTokener.next();
            if (next == '-') {
                if (xMLTokener.next() == '-') {
                    xMLTokener.skipPast("-->");
                    return false;
                }
                xMLTokener.back();
            } else if (next == '[') {
                if (!"CDATA".equals(xMLTokener.nextToken()) || xMLTokener.next() != '[') {
                    throw xMLTokener.syntaxError("Expected 'CDATA['");
                }
                String nextCDATA = xMLTokener.nextCDATA();
                if (nextCDATA.length() > 0) {
                    jSONObject.accumulate("content", nextCDATA);
                }
                return false;
            }
            int i = 1;
            do {
                Object nextMeta = xMLTokener.nextMeta();
                if (nextMeta == null) {
                    throw xMLTokener.syntaxError("Missing '>' after '<!'.");
                } else if (nextMeta == LT) {
                    i++;
                    continue;
                } else if (nextMeta == GT) {
                    i--;
                    continue;
                } else {
                    continue;
                }
            } while (i > 0);
            return false;
        } else if (nextToken == QUEST) {
            xMLTokener.skipPast("?>");
            return false;
        } else if (nextToken == SLASH) {
            Object nextToken2 = xMLTokener.nextToken();
            if (str == null) {
                throw xMLTokener.syntaxError(new StringBuffer().append("Mismatched close tag ").append(nextToken2).toString());
            } else if (!nextToken2.equals(str)) {
                throw xMLTokener.syntaxError(new StringBuffer().append("Mismatched ").append(str).append(" and ").append(nextToken2).toString());
            } else if (xMLTokener.nextToken() == GT) {
                return true;
            } else {
                throw xMLTokener.syntaxError("Misshaped close tag");
            }
        } else if (nextToken instanceof Character) {
            throw xMLTokener.syntaxError("Misshaped tag");
        } else {
            String str2 = (String) nextToken;
            JSONObject jSONObject2 = new JSONObject();
            Object obj = null;
            while (true) {
                if (obj == null) {
                    obj = xMLTokener.nextToken();
                }
                if (obj instanceof String) {
                    String str3 = (String) obj;
                    Object nextToken3 = xMLTokener.nextToken();
                    if (nextToken3 == EQ) {
                        Object nextToken4 = xMLTokener.nextToken();
                        if (!(nextToken4 instanceof String)) {
                            throw xMLTokener.syntaxError("Missing value");
                        }
                        jSONObject2.accumulate(str3, stringToValue((String) nextToken4));
                        obj = null;
                    } else {
                        jSONObject2.accumulate(str3, "");
                        obj = nextToken3;
                    }
                } else if (obj == SLASH) {
                    if (xMLTokener.nextToken() != GT) {
                        throw xMLTokener.syntaxError("Misshaped tag");
                    }
                    if (jSONObject2.length() > 0) {
                        jSONObject.accumulate(str2, jSONObject2);
                    } else {
                        jSONObject.accumulate(str2, "");
                    }
                    return false;
                } else if (obj == GT) {
                    while (true) {
                        Object nextContent = xMLTokener.nextContent();
                        if (nextContent == null) {
                            if (str2 == null) {
                                return false;
                            }
                            throw xMLTokener.syntaxError(new StringBuffer().append("Unclosed tag ").append(str2).toString());
                        } else if (nextContent instanceof String) {
                            String str4 = (String) nextContent;
                            if (str4.length() > 0) {
                                jSONObject2.accumulate("content", stringToValue(str4));
                            }
                        } else if (nextContent == LT && parse(xMLTokener, jSONObject2, str2)) {
                            if (jSONObject2.length() == 0) {
                                jSONObject.accumulate(str2, "");
                            } else if (jSONObject2.length() != 1 || jSONObject2.opt("content") == null) {
                                jSONObject.accumulate(str2, jSONObject2);
                            } else {
                                jSONObject.accumulate(str2, jSONObject2.opt("content"));
                            }
                            return false;
                        }
                    }
                } else {
                    throw xMLTokener.syntaxError("Misshaped tag");
                }
            }
        }
    }

    public static Object stringToValue(String str) {
        int i = 1;
        boolean z = false;
        if ("".equals(str)) {
            return str;
        }
        if ("true".equalsIgnoreCase(str)) {
            return Boolean.TRUE;
        }
        if ("false".equalsIgnoreCase(str)) {
            return Boolean.FALSE;
        }
        if ("null".equalsIgnoreCase(str)) {
            return JSONObject.NULL;
        }
        if ("0".equals(str)) {
            return new Integer(0);
        }
        try {
            char charAt = str.charAt(0);
            if (charAt == '-') {
                charAt = str.charAt(1);
                z = true;
            }
            if (charAt == '0') {
                if (z) {
                    i = 2;
                }
                if (str.charAt(i) == '0') {
                    return str;
                }
            }
            if (charAt < '0' || charAt > '9') {
                return str;
            }
            if (str.indexOf(46) >= 0) {
                return Double.valueOf(str);
            }
            if (str.indexOf(101) >= 0 || str.indexOf(69) >= 0) {
                return str;
            }
            Long l = new Long(str);
            return l.longValue() == ((long) l.intValue()) ? new Integer(l.intValue()) : l;
        } catch (Exception e) {
            return str;
        }
    }

    public static JSONObject toJSONObject(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        XMLTokener xMLTokener = new XMLTokener(str);
        while (xMLTokener.more() && xMLTokener.skipPast("<")) {
            parse(xMLTokener, jSONObject, (String) null);
        }
        return jSONObject;
    }

    public static String toString(Object obj) throws JSONException {
        return toString(obj, (String) null);
    }

    public static String toString(Object obj, String str) throws JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        if (obj instanceof JSONObject) {
            if (str != null) {
                stringBuffer.append('<');
                stringBuffer.append(str);
                stringBuffer.append('>');
            }
            JSONObject jSONObject = (JSONObject) obj;
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String obj2 = keys.next().toString();
                Object opt = jSONObject.opt(obj2);
                if (opt == null) {
                    opt = "";
                }
                if (opt instanceof String) {
                    String str2 = (String) opt;
                }
                if ("content".equals(obj2)) {
                    if (opt instanceof JSONArray) {
                        JSONArray jSONArray = (JSONArray) opt;
                        int length = jSONArray.length();
                        for (int i = 0; i < length; i++) {
                            if (i > 0) {
                                stringBuffer.append(10);
                            }
                            stringBuffer.append(escape(jSONArray.get(i).toString()));
                        }
                    } else {
                        stringBuffer.append(escape(opt.toString()));
                    }
                } else if (opt instanceof JSONArray) {
                    JSONArray jSONArray2 = (JSONArray) opt;
                    int length2 = jSONArray2.length();
                    for (int i2 = 0; i2 < length2; i2++) {
                        Object obj3 = jSONArray2.get(i2);
                        if (obj3 instanceof JSONArray) {
                            stringBuffer.append('<');
                            stringBuffer.append(obj2);
                            stringBuffer.append('>');
                            stringBuffer.append(toString(obj3));
                            stringBuffer.append("</");
                            stringBuffer.append(obj2);
                            stringBuffer.append('>');
                        } else {
                            stringBuffer.append(toString(obj3, obj2));
                        }
                    }
                } else if ("".equals(opt)) {
                    stringBuffer.append('<');
                    stringBuffer.append(obj2);
                    stringBuffer.append("/>");
                } else {
                    stringBuffer.append(toString(opt, obj2));
                }
            }
            if (str != null) {
                stringBuffer.append("</");
                stringBuffer.append(str);
                stringBuffer.append('>');
            }
            return stringBuffer.toString();
        }
        Object jSONArray3 = obj.getClass().isArray() ? new JSONArray(obj) : obj;
        if (jSONArray3 instanceof JSONArray) {
            JSONArray jSONArray4 = (JSONArray) jSONArray3;
            int length3 = jSONArray4.length();
            for (int i3 = 0; i3 < length3; i3++) {
                stringBuffer.append(toString(jSONArray4.opt(i3), str == null ? "array" : str));
            }
            return stringBuffer.toString();
        }
        String escape = jSONArray3 == null ? "null" : escape(jSONArray3.toString());
        return str == null ? new StringBuffer().append("\"").append(escape).append("\"").toString() : escape.length() == 0 ? new StringBuffer().append("<").append(str).append("/>").toString() : new StringBuffer().append("<").append(str).append(">").append(escape).append("</").append(str).append(">").toString();
    }
}
