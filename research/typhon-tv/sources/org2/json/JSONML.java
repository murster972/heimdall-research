package org2.json;

import java.util.Iterator;

public class JSONML {
    private static Object parse(XMLTokener xMLTokener, boolean z, JSONArray jSONArray) throws JSONException {
        loop0:
        while (xMLTokener.more()) {
            Object nextContent = xMLTokener.nextContent();
            if (nextContent == XML.LT) {
                Object nextToken = xMLTokener.nextToken();
                if (nextToken instanceof Character) {
                    if (nextToken == XML.SLASH) {
                        Object nextToken2 = xMLTokener.nextToken();
                        if (!(nextToken2 instanceof String)) {
                            throw new JSONException(new StringBuffer().append("Expected a closing name instead of '").append(nextToken2).append("'.").toString());
                        } else if (xMLTokener.nextToken() == XML.GT) {
                            return nextToken2;
                        } else {
                            throw xMLTokener.syntaxError("Misshaped close tag");
                        }
                    } else if (nextToken == XML.BANG) {
                        char next = xMLTokener.next();
                        if (next == '-') {
                            if (xMLTokener.next() == '-') {
                                xMLTokener.skipPast("-->");
                            } else {
                                xMLTokener.back();
                            }
                        } else if (next != '[') {
                            int i = 1;
                            do {
                                Object nextMeta = xMLTokener.nextMeta();
                                if (nextMeta == null) {
                                    throw xMLTokener.syntaxError("Missing '>' after '<!'.");
                                } else if (nextMeta == XML.LT) {
                                    i++;
                                    continue;
                                } else if (nextMeta == XML.GT) {
                                    i--;
                                    continue;
                                } else {
                                    continue;
                                }
                            } while (i > 0);
                        } else if (!xMLTokener.nextToken().equals("CDATA") || xMLTokener.next() != '[') {
                            throw xMLTokener.syntaxError("Expected 'CDATA['");
                        } else if (jSONArray != null) {
                            jSONArray.put((Object) xMLTokener.nextCDATA());
                        }
                    } else if (nextToken == XML.QUEST) {
                        xMLTokener.skipPast("?>");
                    } else {
                        throw xMLTokener.syntaxError("Misshaped tag");
                    }
                } else if (!(nextToken instanceof String)) {
                    throw xMLTokener.syntaxError(new StringBuffer().append("Bad tagName '").append(nextToken).append("'.").toString());
                } else {
                    String str = (String) nextToken;
                    JSONArray jSONArray2 = new JSONArray();
                    JSONObject jSONObject = new JSONObject();
                    if (z) {
                        jSONArray2.put((Object) str);
                        if (jSONArray != null) {
                            jSONArray.put((Object) jSONArray2);
                        }
                    } else {
                        jSONObject.put("tagName", (Object) str);
                        if (jSONArray != null) {
                            jSONArray.put((Object) jSONObject);
                        }
                    }
                    Object obj = null;
                    while (true) {
                        if (obj == null) {
                            obj = xMLTokener.nextToken();
                        }
                        if (obj == null) {
                            throw xMLTokener.syntaxError("Misshaped tag");
                        } else if (!(obj instanceof String)) {
                            if (z && jSONObject.length() > 0) {
                                jSONArray2.put((Object) jSONObject);
                            }
                            if (obj == XML.SLASH) {
                                if (xMLTokener.nextToken() != XML.GT) {
                                    throw xMLTokener.syntaxError("Misshaped tag");
                                } else if (jSONArray == null) {
                                    return z ? jSONArray2 : jSONObject;
                                }
                            } else if (obj != XML.GT) {
                                throw xMLTokener.syntaxError("Misshaped tag");
                            } else {
                                String str2 = (String) parse(xMLTokener, z, jSONArray2);
                                if (str2 == null) {
                                    continue;
                                } else if (!str2.equals(str)) {
                                    throw xMLTokener.syntaxError(new StringBuffer().append("Mismatched '").append(str).append("' and '").append(str2).append("'").toString());
                                } else {
                                    if (!z && jSONArray2.length() > 0) {
                                        jSONObject.put("childNodes", (Object) jSONArray2);
                                    }
                                    if (jSONArray == null) {
                                        return z ? jSONArray2 : jSONObject;
                                    }
                                }
                            }
                        } else {
                            String str3 = (String) obj;
                            if (z || (!"tagName".equals(str3) && !"childNode".equals(str3))) {
                                Object nextToken3 = xMLTokener.nextToken();
                                if (nextToken3 == XML.EQ) {
                                    Object nextToken4 = xMLTokener.nextToken();
                                    if (!(nextToken4 instanceof String)) {
                                        throw xMLTokener.syntaxError("Missing value");
                                    }
                                    jSONObject.accumulate(str3, XML.stringToValue((String) nextToken4));
                                    obj = null;
                                } else {
                                    jSONObject.accumulate(str3, "");
                                    obj = nextToken3;
                                }
                            }
                        }
                    }
                    throw xMLTokener.syntaxError("Reserved attribute.");
                }
            } else if (jSONArray != null) {
                if (nextContent instanceof String) {
                    nextContent = XML.stringToValue((String) nextContent);
                }
                jSONArray.put(nextContent);
            }
        }
        throw xMLTokener.syntaxError("Bad XML");
    }

    public static JSONArray toJSONArray(String str) throws JSONException {
        return toJSONArray(new XMLTokener(str));
    }

    public static JSONArray toJSONArray(XMLTokener xMLTokener) throws JSONException {
        return (JSONArray) parse(xMLTokener, true, (JSONArray) null);
    }

    public static JSONObject toJSONObject(String str) throws JSONException {
        return toJSONObject(new XMLTokener(str));
    }

    public static JSONObject toJSONObject(XMLTokener xMLTokener) throws JSONException {
        return (JSONObject) parse(xMLTokener, false, (JSONArray) null);
    }

    public static String toString(JSONArray jSONArray) throws JSONException {
        int i;
        StringBuffer stringBuffer = new StringBuffer();
        String string = jSONArray.getString(0);
        XML.noSpace(string);
        String escape = XML.escape(string);
        stringBuffer.append('<');
        stringBuffer.append(escape);
        Object opt = jSONArray.opt(1);
        if (opt instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) opt;
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String obj = keys.next().toString();
                XML.noSpace(obj);
                String optString = jSONObject.optString(obj);
                if (optString != null) {
                    stringBuffer.append(' ');
                    stringBuffer.append(XML.escape(obj));
                    stringBuffer.append('=');
                    stringBuffer.append('\"');
                    stringBuffer.append(XML.escape(optString));
                    stringBuffer.append('\"');
                }
            }
            i = 2;
        } else {
            i = 1;
        }
        int length = jSONArray.length();
        if (i >= length) {
            stringBuffer.append('/');
            stringBuffer.append('>');
        } else {
            stringBuffer.append('>');
            int i2 = i;
            do {
                Object obj2 = jSONArray.get(i2);
                i2++;
                if (obj2 != null) {
                    if (obj2 instanceof String) {
                        stringBuffer.append(XML.escape(obj2.toString()));
                        continue;
                    } else if (obj2 instanceof JSONObject) {
                        stringBuffer.append(toString((JSONObject) obj2));
                        continue;
                    } else if (obj2 instanceof JSONArray) {
                        stringBuffer.append(toString((JSONArray) obj2));
                        continue;
                    } else {
                        continue;
                    }
                }
            } while (i2 < length);
            stringBuffer.append('<');
            stringBuffer.append('/');
            stringBuffer.append(escape);
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }

    public static String toString(JSONObject jSONObject) throws JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        String optString = jSONObject.optString("tagName");
        if (optString == null) {
            return XML.escape(jSONObject.toString());
        }
        XML.noSpace(optString);
        String escape = XML.escape(optString);
        stringBuffer.append('<');
        stringBuffer.append(escape);
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String obj = keys.next().toString();
            if (!"tagName".equals(obj) && !"childNodes".equals(obj)) {
                XML.noSpace(obj);
                String optString2 = jSONObject.optString(obj);
                if (optString2 != null) {
                    stringBuffer.append(' ');
                    stringBuffer.append(XML.escape(obj));
                    stringBuffer.append('=');
                    stringBuffer.append('\"');
                    stringBuffer.append(XML.escape(optString2));
                    stringBuffer.append('\"');
                }
            }
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("childNodes");
        if (optJSONArray == null) {
            stringBuffer.append('/');
            stringBuffer.append('>');
        } else {
            stringBuffer.append('>');
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                Object obj2 = optJSONArray.get(i);
                if (obj2 != null) {
                    if (obj2 instanceof String) {
                        stringBuffer.append(XML.escape(obj2.toString()));
                    } else if (obj2 instanceof JSONObject) {
                        stringBuffer.append(toString((JSONObject) obj2));
                    } else if (obj2 instanceof JSONArray) {
                        stringBuffer.append(toString((JSONArray) obj2));
                    } else {
                        stringBuffer.append(obj2.toString());
                    }
                }
            }
            stringBuffer.append('<');
            stringBuffer.append('/');
            stringBuffer.append(escape);
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }
}
