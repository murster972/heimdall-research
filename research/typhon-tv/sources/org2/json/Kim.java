package org2.json;

import java.util.Arrays;

public class Kim {
    private byte[] bytes;
    private int hashcode;
    public int length;
    private String string;

    public Kim(String str) throws JSONException {
        int i;
        int i2 = 0;
        this.bytes = null;
        this.hashcode = 0;
        this.length = 0;
        this.string = null;
        int length2 = str.length();
        this.hashcode = 0;
        this.length = 0;
        if (length2 > 0) {
            int i3 = 0;
            while (i3 < length2) {
                char charAt = str.charAt(i3);
                if (charAt <= 127) {
                    this.length++;
                } else if (charAt <= 16383) {
                    this.length += 2;
                } else {
                    if (charAt >= 55296 && charAt <= 57343) {
                        i3++;
                        char charAt2 = str.charAt(i3);
                        if (charAt > 56319 || charAt2 < 56320 || charAt2 > 57343) {
                            throw new JSONException("Bad UTF16");
                        }
                    }
                    this.length += 3;
                }
                i3++;
            }
            this.bytes = new byte[this.length];
            int i4 = 1;
            int i5 = 0;
            while (i2 < length2) {
                int charAt3 = str.charAt(i2);
                if (charAt3 <= 127) {
                    this.bytes[i5] = (byte) charAt3;
                    i = charAt3 + i4;
                    this.hashcode += i;
                } else if (charAt3 <= 16383) {
                    int i6 = (charAt3 >>> 7) | 128;
                    this.bytes[i5] = (byte) i6;
                    int i7 = i4 + i6;
                    this.hashcode += i7;
                    i5++;
                    int i8 = charAt3 & 127;
                    this.bytes[i5] = (byte) i8;
                    i = i8 + i7;
                    this.hashcode += i;
                } else {
                    if (charAt3 >= 55296 && charAt3 <= 56319) {
                        i2++;
                        charAt3 = (((charAt3 & 1023) << 10) | (str.charAt(i2) & 1023)) + 0;
                    }
                    int i9 = (charAt3 >>> 14) | 128;
                    this.bytes[i5] = (byte) i9;
                    int i10 = i4 + i9;
                    this.hashcode += i10;
                    int i11 = i5 + 1;
                    int i12 = ((charAt3 >>> 7) & 255) | 128;
                    this.bytes[i11] = (byte) i12;
                    int i13 = i10 + i12;
                    this.hashcode += i13;
                    i5 = i11 + 1;
                    int i14 = charAt3 & 127;
                    this.bytes[i5] = (byte) i14;
                    i = i14 + i13;
                    this.hashcode += i;
                }
                i2++;
                i5++;
                i4 = i;
            }
            this.hashcode += i4 << 16;
        }
    }

    public Kim(Kim kim, int i, int i2) {
        this(kim.bytes, i, i2);
    }

    public Kim(byte[] bArr, int i) {
        this(bArr, 0, i);
    }

    public Kim(byte[] bArr, int i, int i2) {
        this.bytes = null;
        this.hashcode = 0;
        this.length = 0;
        this.string = null;
        int i3 = 1;
        this.hashcode = 0;
        this.length = i2 - i;
        if (this.length > 0) {
            this.bytes = new byte[this.length];
            for (int i4 = 0; i4 < this.length; i4++) {
                byte b = bArr[i4 + i] & 255;
                i3 += b;
                this.hashcode += i3;
                this.bytes[i4] = (byte) b;
            }
            this.hashcode += i3 << 16;
        }
    }

    public static int characterSize(int i) throws JSONException {
        if (i < 0 || i > 1114111) {
            throw new JSONException(new StringBuffer().append("Bad character ").append(i).toString());
        } else if (i <= 127) {
            return 1;
        } else {
            return i <= 16383 ? 2 : 3;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005b, code lost:
        if (r0 > 57343) goto L_0x0008;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (r0 <= 127) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int characterAt(int r4) throws org2.json.JSONException {
        /*
            r3 = this;
            int r0 = r3.get(r4)
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 != 0) goto L_0x0009
        L_0x0008:
            return r0
        L_0x0009:
            int r1 = r4 + 1
            int r1 = r3.get(r1)
            r2 = r1 & 128(0x80, float:1.794E-43)
            if (r2 != 0) goto L_0x0036
            r0 = r0 & 127(0x7f, float:1.78E-43)
            int r0 = r0 << 7
            r0 = r0 | r1
            r1 = 127(0x7f, float:1.78E-43)
            if (r0 > r1) goto L_0x0008
        L_0x001c:
            org2.json.JSONException r0 = new org2.json.JSONException
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = "Bad character at "
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.StringBuffer r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r0.<init>((java.lang.String) r1)
            throw r0
        L_0x0036:
            int r2 = r4 + 2
            int r2 = r3.get(r2)
            r0 = r0 & 127(0x7f, float:1.78E-43)
            int r0 = r0 << 14
            r1 = r1 & 127(0x7f, float:1.78E-43)
            int r1 = r1 << 7
            r0 = r0 | r1
            r0 = r0 | r2
            r1 = r2 & 128(0x80, float:1.794E-43)
            if (r1 != 0) goto L_0x001c
            r1 = 16383(0x3fff, float:2.2957E-41)
            if (r0 <= r1) goto L_0x001c
            r1 = 1114111(0x10ffff, float:1.561202E-39)
            if (r0 > r1) goto L_0x001c
            r1 = 55296(0xd800, float:7.7486E-41)
            if (r0 < r1) goto L_0x0008
            r1 = 57343(0xdfff, float:8.0355E-41)
            if (r0 <= r1) goto L_0x001c
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.json.Kim.characterAt(int):int");
    }

    public int copy(byte[] bArr, int i) {
        System.arraycopy(this.bytes, 0, bArr, i, this.length);
        return this.length + i;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Kim)) {
            return false;
        }
        Kim kim = (Kim) obj;
        if (this == kim) {
            return true;
        }
        if (this.hashcode == kim.hashcode) {
            return Arrays.equals(this.bytes, kim.bytes);
        }
        return false;
    }

    public int get(int i) throws JSONException {
        if (i >= 0 && i <= this.length) {
            return this.bytes[i] & 255;
        }
        throw new JSONException(new StringBuffer().append("Bad character at ").append(i).toString());
    }

    public int hashCode() {
        return this.hashcode;
    }

    public String toString() throws JSONException {
        if (this.string == null) {
            char[] cArr = new char[this.length];
            int i = 0;
            int i2 = 0;
            while (i < this.length) {
                int characterAt = characterAt(i);
                if (characterAt < 65536) {
                    cArr[i2] = (char) characterAt;
                } else {
                    cArr[i2] = (char) (55296 | ((characterAt - 65536) >>> 10));
                    i2++;
                    cArr[i2] = (char) (56320 | (characterAt & 1023));
                }
                i2++;
                i += characterSize(characterAt);
            }
            this.string = new String(cArr, 0, i2);
        }
        return this.string;
    }
}
