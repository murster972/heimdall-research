package org2.json;

public class Cookie {
    public static String escape(String str) {
        String trim = str.trim();
        StringBuffer stringBuffer = new StringBuffer();
        int length = trim.length();
        for (int i = 0; i < length; i++) {
            char charAt = trim.charAt(i);
            if (charAt < ' ' || charAt == '+' || charAt == '%' || charAt == '=' || charAt == ';') {
                stringBuffer.append('%');
                stringBuffer.append(Character.forDigit((char) ((charAt >>> 4) & 15), 16));
                stringBuffer.append(Character.forDigit((char) (charAt & 15), 16));
            } else {
                stringBuffer.append(charAt);
            }
        }
        return stringBuffer.toString();
    }

    public static JSONObject toJSONObject(String str) throws JSONException {
        Object unescape;
        JSONObject jSONObject = new JSONObject();
        JSONTokener jSONTokener = new JSONTokener(str);
        jSONObject.put("name", (Object) jSONTokener.nextTo('='));
        jSONTokener.next('=');
        jSONObject.put("value", (Object) jSONTokener.nextTo(';'));
        jSONTokener.next();
        while (jSONTokener.more()) {
            String unescape2 = unescape(jSONTokener.nextTo("=;"));
            if (jSONTokener.next() == '=') {
                unescape = unescape(jSONTokener.nextTo(';'));
                jSONTokener.next();
            } else if (unescape2.equals("secure")) {
                unescape = Boolean.TRUE;
            } else {
                throw jSONTokener.syntaxError("Missing '=' in cookie parameter.");
            }
            jSONObject.put(unescape2, unescape);
        }
        return jSONObject;
    }

    public static String toString(JSONObject jSONObject) throws JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(escape(jSONObject.getString("name")));
        stringBuffer.append("=");
        stringBuffer.append(escape(jSONObject.getString("value")));
        if (jSONObject.has("expires")) {
            stringBuffer.append(";expires=");
            stringBuffer.append(jSONObject.getString("expires"));
        }
        if (jSONObject.has("domain")) {
            stringBuffer.append(";domain=");
            stringBuffer.append(escape(jSONObject.getString("domain")));
        }
        if (jSONObject.has("path")) {
            stringBuffer.append(";path=");
            stringBuffer.append(escape(jSONObject.getString("path")));
        }
        if (jSONObject.optBoolean("secure")) {
            stringBuffer.append(";secure");
        }
        return stringBuffer.toString();
    }

    public static String unescape(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt == '+') {
                charAt = ' ';
            } else if (charAt == '%' && i + 2 < length) {
                int dehexchar = JSONTokener.dehexchar(str.charAt(i + 1));
                int dehexchar2 = JSONTokener.dehexchar(str.charAt(i + 2));
                if (dehexchar >= 0 && dehexchar2 >= 0) {
                    charAt = (char) ((dehexchar * 16) + dehexchar2);
                    i += 2;
                }
            }
            stringBuffer.append(charAt);
            i++;
        }
        return stringBuffer.toString();
    }
}
