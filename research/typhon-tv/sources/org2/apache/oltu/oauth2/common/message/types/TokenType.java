package org2.apache.oltu.oauth2.common.message.types;

public enum TokenType {
    BEARER("Bearer"),
    MAC("MAC");
    
    private String tokenType;

    private TokenType(String str) {
        this.tokenType = str;
    }

    public String toString() {
        return this.tokenType;
    }
}
