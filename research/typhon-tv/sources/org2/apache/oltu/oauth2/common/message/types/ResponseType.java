package org2.apache.oltu.oauth2.common.message.types;

import org.apache.oltu.oauth2.common.OAuth;

public enum ResponseType {
    CODE(OAuth.OAUTH_CODE),
    TOKEN("token");
    
    private String code;

    private ResponseType(String str) {
        this.code = str;
    }

    public String toString() {
        return this.code;
    }
}
