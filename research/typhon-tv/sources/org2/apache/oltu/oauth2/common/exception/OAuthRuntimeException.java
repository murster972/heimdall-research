package org2.apache.oltu.oauth2.common.exception;

public class OAuthRuntimeException extends RuntimeException {
    public OAuthRuntimeException() {
    }

    public OAuthRuntimeException(String str) {
        super(str);
    }

    public OAuthRuntimeException(String str, Throwable th) {
        super(str, th);
    }

    public OAuthRuntimeException(Throwable th) {
        super(th);
    }
}
