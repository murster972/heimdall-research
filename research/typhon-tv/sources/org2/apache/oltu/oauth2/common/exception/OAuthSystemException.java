package org2.apache.oltu.oauth2.common.exception;

public class OAuthSystemException extends Exception {
    public OAuthSystemException() {
    }

    public OAuthSystemException(String str) {
        super(str);
    }

    public OAuthSystemException(String str, Throwable th) {
        super(str, th);
    }

    public OAuthSystemException(Throwable th) {
        super(th);
    }
}
