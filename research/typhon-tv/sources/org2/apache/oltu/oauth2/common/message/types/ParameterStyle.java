package org2.apache.oltu.oauth2.common.message.types;

public enum ParameterStyle {
    BODY(TtmlNode.TAG_BODY),
    QUERY("query"),
    HEADER("header");
    
    private String parameterStyle;

    private ParameterStyle(String str) {
        this.parameterStyle = str;
    }

    public String toString() {
        return this.parameterStyle;
    }
}
