package org2.apache.oltu.oauth2.common.exception;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org2.apache.oltu.oauth2.common.utils.OAuthUtils;

public class OAuthProblemException extends Exception {
    private String description;
    private String error;
    private Map<String, String> parameters;
    private String redirectUri;
    private int responseStatus;
    private String scope;
    private String state;
    private String uri;

    protected OAuthProblemException(String str) {
        this(str, "");
    }

    protected OAuthProblemException(String str, String str2) {
        super(str + StringUtils.SPACE + str2);
        this.parameters = new HashMap();
        this.description = str2;
        this.error = str;
    }

    public static OAuthProblemException error(String str) {
        return new OAuthProblemException(str);
    }

    public static OAuthProblemException error(String str, String str2) {
        return new OAuthProblemException(str, str2);
    }

    public OAuthProblemException description(String str) {
        this.description = str;
        return this;
    }

    public String get(String str) {
        return this.parameters.get(str);
    }

    public String getDescription() {
        return this.description;
    }

    public String getError() {
        return this.error;
    }

    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        if (!OAuthUtils.m22680(this.error)) {
            sb.append(this.error);
        }
        if (!OAuthUtils.m22680(this.description)) {
            sb.append(", ").append(this.description);
        }
        if (!OAuthUtils.m22680(this.uri)) {
            sb.append(", ").append(this.uri);
        }
        if (!OAuthUtils.m22680(this.state)) {
            sb.append(", ").append(this.state);
        }
        if (!OAuthUtils.m22680(this.scope)) {
            sb.append(", ").append(this.scope);
        }
        return sb.toString();
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public String getRedirectUri() {
        return this.redirectUri;
    }

    public int getResponseStatus() {
        if (this.responseStatus == 0) {
            return 400;
        }
        return this.responseStatus;
    }

    public String getScope() {
        return this.scope;
    }

    public String getState() {
        return this.state;
    }

    public String getUri() {
        return this.uri;
    }

    public OAuthProblemException responseStatus(int i) {
        this.responseStatus = i;
        return this;
    }

    public OAuthProblemException scope(String str) {
        this.scope = str;
        return this;
    }

    public OAuthProblemException setParameter(String str, String str2) {
        this.parameters.put(str, str2);
        return this;
    }

    public void setRedirectUri(String str) {
        this.redirectUri = str;
    }

    public OAuthProblemException state(String str) {
        this.state = str;
        return this;
    }

    public String toString() {
        return "OAuthProblemException{error='" + this.error + '\'' + ", description='" + this.description + '\'' + ", uri='" + this.uri + '\'' + ", state='" + this.state + '\'' + ", scope='" + this.scope + '\'' + ", redirectUri='" + this.redirectUri + '\'' + ", responseStatus=" + this.responseStatus + ", parameters=" + this.parameters + '}';
    }

    public OAuthProblemException uri(String str) {
        this.uri = str;
        return this;
    }
}
