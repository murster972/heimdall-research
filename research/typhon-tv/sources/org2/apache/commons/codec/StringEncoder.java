package org2.apache.commons.codec;

public interface StringEncoder extends Encoder {
    String encode(String str) throws EncoderException;
}
