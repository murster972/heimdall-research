package org2.apache.commons.codec.language;

import java.util.Locale;
import org2.apache.commons.codec.EncoderException;
import org2.apache.commons.codec.StringEncoder;

final class SoundexUtils {
    /* renamed from: 龘  reason: contains not printable characters */
    static int m22463(String str, String str2) {
        int i = 0;
        if (!(str == null || str2 == null)) {
            int min = Math.min(str.length(), str2.length());
            for (int i2 = 0; i2 < min; i2++) {
                if (str.charAt(i2) == str2.charAt(i2)) {
                    i++;
                }
            }
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m22464(StringEncoder stringEncoder, String str, String str2) throws EncoderException {
        return m22463(stringEncoder.encode(str), stringEncoder.encode(str2));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m22465(String str) {
        int i;
        if (str == null || str.length() == 0) {
            return str;
        }
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            if (Character.isLetter(str.charAt(i2))) {
                i = i3 + 1;
                cArr[i3] = str.charAt(i2);
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
        return i3 == length ? str.toUpperCase(Locale.ENGLISH) : new String(cArr, 0, i3).toUpperCase(Locale.ENGLISH);
    }
}
