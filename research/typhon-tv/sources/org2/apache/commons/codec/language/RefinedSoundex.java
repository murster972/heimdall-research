package org2.apache.commons.codec.language;

import org2.apache.commons.codec.EncoderException;
import org2.apache.commons.codec.StringEncoder;

public class RefinedSoundex implements StringEncoder {
    public static final RefinedSoundex US_ENGLISH = new RefinedSoundex();
    private static final char[] US_ENGLISH_MAPPING = "01360240043788015936020505".toCharArray();
    public static final String US_ENGLISH_MAPPING_STRING = "01360240043788015936020505";
    private final char[] soundexMapping;

    public RefinedSoundex() {
        this.soundexMapping = US_ENGLISH_MAPPING;
    }

    public RefinedSoundex(String str) {
        this.soundexMapping = str.toCharArray();
    }

    public RefinedSoundex(char[] cArr) {
        this.soundexMapping = new char[cArr.length];
        System.arraycopy(cArr, 0, this.soundexMapping, 0, cArr.length);
    }

    public int difference(String str, String str2) throws EncoderException {
        return SoundexUtils.m22464(this, str, str2);
    }

    public Object encode(Object obj) throws EncoderException {
        if (obj instanceof String) {
            return soundex((String) obj);
        }
        throw new EncoderException("Parameter supplied to RefinedSoundex encode is not of type java.lang.String");
    }

    public String encode(String str) {
        return soundex(str);
    }

    /* access modifiers changed from: package-private */
    public char getMappingCode(char c) {
        if (!Character.isLetter(c)) {
            return 0;
        }
        return this.soundexMapping[Character.toUpperCase(c) - 'A'];
    }

    public String soundex(String str) {
        if (str == null) {
            return null;
        }
        String r3 = SoundexUtils.m22465(str);
        if (r3.length() == 0) {
            return r3;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(r3.charAt(0));
        char c = '*';
        for (int i = 0; i < r3.length(); i++) {
            char mappingCode = getMappingCode(r3.charAt(i));
            if (mappingCode != c) {
                if (mappingCode != 0) {
                    sb.append(mappingCode);
                }
                c = mappingCode;
            }
        }
        return sb.toString();
    }
}
