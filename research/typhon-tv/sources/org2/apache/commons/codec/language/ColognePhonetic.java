package org2.apache.commons.codec.language;

import java.util.Locale;
import org2.apache.commons.codec.EncoderException;
import org2.apache.commons.codec.StringEncoder;

public class ColognePhonetic implements StringEncoder {
    private static final char[] AEIJOUY = {'A', 'E', 'I', 'J', 'O', 'U', 'Y'};
    private static final char[] AHKLOQRUX = {'A', 'H', 'K', 'L', 'O', 'Q', 'R', 'U', 'X'};
    private static final char[] AHOUKQX = {'A', 'H', 'O', 'U', 'K', 'Q', 'X'};
    private static final char[] CKQ = {'C', 'K', 'Q'};
    private static final char[] GKQ = {'G', 'K', 'Q'};
    private static final char[][] PREPROCESS_MAP = {new char[]{196, 'A'}, new char[]{220, 'U'}, new char[]{214, 'O'}, new char[]{223, 'S'}};
    private static final char[] SCZ = {'S', 'C', 'Z'};
    private static final char[] SZ = {'S', 'Z'};
    private static final char[] TDX = {'T', 'D', 'X'};
    private static final char[] WFPV = {'W', 'F', 'P', 'V'};

    private abstract class CologneBuffer {

        /* renamed from: 靐  reason: contains not printable characters */
        protected int f17568 = 0;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final char[] f17570;

        public CologneBuffer(int i) {
            this.f17570 = new char[i];
            this.f17568 = 0;
        }

        public CologneBuffer(char[] cArr) {
            this.f17570 = cArr;
            this.f17568 = cArr.length;
        }

        public String toString() {
            return new String(m22455(0, this.f17568));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m22454() {
            return this.f17568;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract char[] m22455(int i, int i2);
    }

    private class CologneInputBuffer extends CologneBuffer {
        public CologneInputBuffer(char[] cArr) {
            super(cArr);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public char m22456() {
            return this.f17570[m22458()];
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public char m22457() {
            this.f17568--;
            return m22456();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 齉  reason: contains not printable characters */
        public int m22458() {
            return this.f17570.length - this.f17568;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m22459(char c) {
            this.f17568++;
            this.f17570[m22458()] = c;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public char[] m22460(int i, int i2) {
            char[] cArr = new char[i2];
            System.arraycopy(this.f17570, (this.f17570.length - this.f17568) + i, cArr, 0, i2);
            return cArr;
        }
    }

    private class CologneOutputBuffer extends CologneBuffer {
        public CologneOutputBuffer(int i) {
            super(i);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m22461(char c) {
            this.f17570[this.f17568] = c;
            this.f17568++;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public char[] m22462(int i, int i2) {
            char[] cArr = new char[i2];
            System.arraycopy(this.f17570, i, cArr, 0, i2);
            return cArr;
        }
    }

    private static boolean arrayContains(char[] cArr, char c) {
        for (char c2 : cArr) {
            if (c2 == c) {
                return true;
            }
        }
        return false;
    }

    private String preprocess(String str) {
        char[] charArray = str.toUpperCase(Locale.GERMAN).toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] > 'Z') {
                char[][] cArr = PREPROCESS_MAP;
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    char[] cArr2 = cArr[i2];
                    if (charArray[i] == cArr2[0]) {
                        charArray[i] = cArr2[1];
                        break;
                    }
                    i2++;
                }
            }
        }
        return new String(charArray);
    }

    public String colognePhonetic(String str) {
        int i;
        char c;
        if (str == null) {
            return null;
        }
        String preprocess = preprocess(str);
        CologneOutputBuffer cologneOutputBuffer = new CologneOutputBuffer(preprocess.length() * 2);
        CologneInputBuffer cologneInputBuffer = new CologneInputBuffer(preprocess.toCharArray());
        int r2 = cologneInputBuffer.m22454();
        char c2 = '/';
        char c3 = '-';
        while (r2 > 0) {
            char r6 = cologneInputBuffer.m22457();
            r2 = cologneInputBuffer.m22454();
            char r0 = r2 > 0 ? cologneInputBuffer.m22456() : '-';
            if (arrayContains(AEIJOUY, r6)) {
                i = r2;
                c = '0';
            } else if (r6 == 'H' || r6 < 'A' || r6 > 'Z') {
                if (c2 != '/') {
                    i = r2;
                    c = '-';
                }
            } else if (r6 == 'B' || (r6 == 'P' && r0 != 'H')) {
                int i2 = r2;
                c = '1';
                i = i2;
            } else if ((r6 == 'D' || r6 == 'T') && !arrayContains(SCZ, r0)) {
                int i3 = r2;
                c = '2';
                i = i3;
            } else if (arrayContains(WFPV, r6)) {
                int i4 = r2;
                c = '3';
                i = i4;
            } else if (arrayContains(GKQ, r6)) {
                i = r2;
                c = '4';
            } else if (r6 == 'X' && !arrayContains(CKQ, c3)) {
                cologneInputBuffer.m22459('S');
                i = r2 + 1;
                c = '4';
            } else if (r6 == 'S' || r6 == 'Z') {
                i = r2;
                c = '8';
            } else if (r6 == 'C') {
                if (c2 == '/') {
                    if (arrayContains(AHKLOQRUX, r0)) {
                        i = r2;
                        c = '4';
                    } else {
                        i = r2;
                        c = '8';
                    }
                } else if (arrayContains(SZ, c3) || !arrayContains(AHOUKQX, r0)) {
                    i = r2;
                    c = '8';
                } else {
                    i = r2;
                    c = '4';
                }
            } else if (arrayContains(TDX, r6)) {
                i = r2;
                c = '8';
            } else if (r6 == 'R') {
                int i5 = r2;
                c = '7';
                i = i5;
            } else if (r6 == 'L') {
                int i6 = r2;
                c = '5';
                i = i6;
            } else if (r6 == 'M' || r6 == 'N') {
                int i7 = r2;
                c = '6';
                i = i7;
            } else {
                i = r2;
                c = r6;
            }
            if (c != '-' && ((c2 != c && (c != '0' || c2 == '/')) || c < '0' || c > '8')) {
                cologneOutputBuffer.m22461(c);
            }
            c2 = c;
            c3 = r6;
            r2 = i;
        }
        return cologneOutputBuffer.toString();
    }

    public Object encode(Object obj) throws EncoderException {
        if (obj instanceof String) {
            return encode((String) obj);
        }
        throw new EncoderException("This method's parameter was expected to be of the type " + String.class.getName() + ". But actually it was of the type " + obj.getClass().getName() + ".");
    }

    public String encode(String str) {
        return colognePhonetic(str);
    }

    public boolean isEncodeEqual(String str, String str2) {
        return colognePhonetic(str).equals(colognePhonetic(str2));
    }
}
