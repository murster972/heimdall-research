package org2.apache.commons.codec.language;

import java.util.Locale;
import org.apache.commons.lang3.StringUtils;
import org2.apache.commons.codec.EncoderException;
import org2.apache.commons.codec.StringEncoder;

public class DoubleMetaphone implements StringEncoder {
    private static final String[] ES_EP_EB_EL_EY_IB_IL_IN_IE_EI_ER = {"ES", "EP", "EB", "EL", "EY", "IB", "IL", "IN", "IE", "EI", "ER"};
    private static final String[] L_R_N_M_B_H_F_V_W_SPACE = {"L", "R", "N", "M", "B", "H", "F", "V", "W", StringUtils.SPACE};
    private static final String[] L_T_K_S_N_M_B_Z = {"L", "T", "K", "S", "N", "M", "B", "Z"};
    private static final String[] SILENT_START = {"GN", "KN", "PN", "WR", "PS"};
    private static final String VOWELS = "AEIOUY";
    private int maxCodeLen = 4;

    public class DoubleMetaphoneResult {
        private final StringBuilder alternate = new StringBuilder(DoubleMetaphone.this.getMaxCodeLen());
        private final int maxLength;
        private final StringBuilder primary = new StringBuilder(DoubleMetaphone.this.getMaxCodeLen());

        public DoubleMetaphoneResult(int i) {
            this.maxLength = i;
        }

        public void append(char c) {
            appendPrimary(c);
            appendAlternate(c);
        }

        public void append(char c, char c2) {
            appendPrimary(c);
            appendAlternate(c2);
        }

        public void append(String str) {
            appendPrimary(str);
            appendAlternate(str);
        }

        public void append(String str, String str2) {
            appendPrimary(str);
            appendAlternate(str2);
        }

        public void appendAlternate(char c) {
            if (this.alternate.length() < this.maxLength) {
                this.alternate.append(c);
            }
        }

        public void appendAlternate(String str) {
            int length = this.maxLength - this.alternate.length();
            if (str.length() <= length) {
                this.alternate.append(str);
            } else {
                this.alternate.append(str.substring(0, length));
            }
        }

        public void appendPrimary(char c) {
            if (this.primary.length() < this.maxLength) {
                this.primary.append(c);
            }
        }

        public void appendPrimary(String str) {
            int length = this.maxLength - this.primary.length();
            if (str.length() <= length) {
                this.primary.append(str);
            } else {
                this.primary.append(str.substring(0, length));
            }
        }

        public String getAlternate() {
            return this.alternate.toString();
        }

        public String getPrimary() {
            return this.primary.toString();
        }

        public boolean isComplete() {
            return this.primary.length() >= this.maxLength && this.alternate.length() >= this.maxLength;
        }
    }

    private String cleanInput(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() != 0) {
            return trim.toUpperCase(Locale.ENGLISH);
        }
        return null;
    }

    private boolean conditionC0(String str, int i) {
        if (contains(str, i, 4, "CHIA")) {
            return true;
        }
        if (i <= 1 || isVowel(charAt(str, i - 2))) {
            return false;
        }
        if (!contains(str, i - 1, 3, "ACH")) {
            return false;
        }
        char charAt = charAt(str, i + 2);
        if (charAt == 'I' || charAt == 'E') {
            if (!contains(str, i - 2, 6, "BACHER", "MACHER")) {
                return false;
            }
        }
        return true;
    }

    private boolean conditionCH0(String str, int i) {
        if (i != 0) {
            return false;
        }
        if (!contains(str, i + 1, 5, "HARAC", "HARIS")) {
            if (!contains(str, i + 1, 3, "HOR", "HYM", "HIA", "HEM")) {
                return false;
            }
        }
        return !contains(str, 0, 5, "CHORE");
    }

    private boolean conditionCH1(String str, int i) {
        if (!contains(str, 0, 4, "VAN ", "VON ")) {
            if (!contains(str, 0, 3, "SCH")) {
                if (!contains(str, i - 2, 6, "ORCHES", "ARCHIT", "ORCHID")) {
                    if (!contains(str, i + 2, 1, "T", "S")) {
                        if (!contains(str, i - 1, 1, "A", "O", "U", "E") && i != 0) {
                            return false;
                        }
                        if (!contains(str, i + 2, 1, L_R_N_M_B_H_F_V_W_SPACE) && i + 1 != str.length() - 1) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0055, code lost:
        if (contains(r8, r8.length() - 1, 1, "A", "O") != false) goto L_0x0057;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean conditionL0(java.lang.String r8, int r9) {
        /*
            r7 = this;
            r6 = 4
            r5 = 2
            r1 = 0
            r0 = 1
            int r2 = r8.length()
            int r2 = r2 + -3
            if (r9 != r2) goto L_0x0027
            int r2 = r9 + -1
            r3 = 3
            java.lang.String[] r3 = new java.lang.String[r3]
            java.lang.String r4 = "ILLO"
            r3[r1] = r4
            java.lang.String r4 = "ILLA"
            r3[r0] = r4
            java.lang.String r4 = "ALLE"
            r3[r5] = r4
            boolean r2 = contains(r8, r2, r6, r3)
            if (r2 == 0) goto L_0x0027
        L_0x0026:
            return r0
        L_0x0027:
            int r2 = r8.length()
            int r2 = r2 + -2
            java.lang.String[] r3 = new java.lang.String[r5]
            java.lang.String r4 = "AS"
            r3[r1] = r4
            java.lang.String r4 = "OS"
            r3[r0] = r4
            boolean r2 = contains(r8, r2, r5, r3)
            if (r2 != 0) goto L_0x0057
            int r2 = r8.length()
            int r2 = r2 + -1
            java.lang.String[] r3 = new java.lang.String[r5]
            java.lang.String r4 = "A"
            r3[r1] = r4
            java.lang.String r4 = "O"
            r3[r0] = r4
            boolean r2 = contains(r8, r2, r0, r3)
            if (r2 == 0) goto L_0x0066
        L_0x0057:
            int r2 = r9 + -1
            java.lang.String[] r3 = new java.lang.String[r0]
            java.lang.String r4 = "ALLE"
            r3[r1] = r4
            boolean r2 = contains(r8, r2, r6, r3)
            if (r2 != 0) goto L_0x0026
        L_0x0066:
            r0 = r1
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.language.DoubleMetaphone.conditionL0(java.lang.String, int):boolean");
    }

    private boolean conditionM0(String str, int i) {
        if (charAt(str, i + 1) == 'M') {
            return true;
        }
        if (contains(str, i - 1, 3, "UMB")) {
            if (i + 1 == str.length() - 1) {
                return true;
            }
            if (contains(str, i + 2, 2, "ER")) {
                return true;
            }
        }
        return false;
    }

    protected static boolean contains(String str, int i, int i2, String... strArr) {
        if (i < 0 || i + i2 > str.length()) {
            return false;
        }
        String substring = str.substring(i, i + i2);
        for (String equals : strArr) {
            if (substring.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    private int handleAEIOUY(DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (i == 0) {
            doubleMetaphoneResult.append('A');
        }
        return i + 1;
    }

    private int handleC(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (conditionC0(str, i)) {
            doubleMetaphoneResult.append('K');
            return i + 2;
        }
        if (i == 0) {
            if (contains(str, i, 6, "CAESAR")) {
                doubleMetaphoneResult.append('S');
                return i + 2;
            }
        }
        if (contains(str, i, 2, "CH")) {
            return handleCH(str, doubleMetaphoneResult, i);
        }
        if (contains(str, i, 2, "CZ")) {
            if (!contains(str, i - 2, 4, "WICZ")) {
                doubleMetaphoneResult.append('S', 'X');
                return i + 2;
            }
        }
        if (contains(str, i + 1, 3, "CIA")) {
            doubleMetaphoneResult.append('X');
            return i + 3;
        }
        if (contains(str, i, 2, "CC") && (i != 1 || charAt(str, 0) != 'M')) {
            return handleCC(str, doubleMetaphoneResult, i);
        }
        if (contains(str, i, 2, "CK", "CG", "CQ")) {
            doubleMetaphoneResult.append('K');
            return i + 2;
        }
        if (contains(str, i, 2, "CI", "CE", "CY")) {
            if (contains(str, i, 3, "CIO", "CIE", "CIA")) {
                doubleMetaphoneResult.append('S', 'X');
            } else {
                doubleMetaphoneResult.append('S');
            }
            return i + 2;
        }
        doubleMetaphoneResult.append('K');
        if (contains(str, i + 1, 2, " C", " Q", " G")) {
            return i + 3;
        }
        if (contains(str, i + 1, 1, "C", "K", "Q")) {
            if (!contains(str, i + 1, 2, "CE", "CI")) {
                return i + 2;
            }
        }
        return i + 1;
    }

    private int handleCC(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (contains(str, i + 2, 1, "I", "E", "H")) {
            if (!contains(str, i + 2, 2, "HU")) {
                if (!(i == 1 && charAt(str, i - 1) == 'A')) {
                    if (!contains(str, i - 1, 5, "UCCEE", "UCCES")) {
                        doubleMetaphoneResult.append('X');
                        return i + 3;
                    }
                }
                doubleMetaphoneResult.append("KS");
                return i + 3;
            }
        }
        doubleMetaphoneResult.append('K');
        return i + 2;
    }

    private int handleCH(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (i > 0) {
            if (contains(str, i, 4, "CHAE")) {
                doubleMetaphoneResult.append('K', 'X');
                return i + 2;
            }
        }
        if (conditionCH0(str, i)) {
            doubleMetaphoneResult.append('K');
            return i + 2;
        } else if (conditionCH1(str, i)) {
            doubleMetaphoneResult.append('K');
            return i + 2;
        } else {
            if (i > 0) {
                if (contains(str, 0, 2, "MC")) {
                    doubleMetaphoneResult.append('K');
                } else {
                    doubleMetaphoneResult.append('X', 'K');
                }
            } else {
                doubleMetaphoneResult.append('X');
            }
            return i + 2;
        }
    }

    private int handleD(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (contains(str, i, 2, "DG")) {
            if (contains(str, i + 2, 1, "I", "E", "Y")) {
                doubleMetaphoneResult.append('J');
                return i + 3;
            }
            doubleMetaphoneResult.append("TK");
            return i + 2;
        }
        if (contains(str, i, 2, "DT", "DD")) {
            doubleMetaphoneResult.append('T');
            return i + 2;
        }
        doubleMetaphoneResult.append('T');
        return i + 1;
    }

    private int handleG(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i, boolean z) {
        if (charAt(str, i + 1) == 'H') {
            return handleGH(str, doubleMetaphoneResult, i);
        }
        if (charAt(str, i + 1) == 'N') {
            if (i != 1 || !isVowel(charAt(str, 0)) || z) {
                if (contains(str, i + 2, 2, "EY") || charAt(str, i + 1) == 'Y' || z) {
                    doubleMetaphoneResult.append("KN");
                } else {
                    doubleMetaphoneResult.append("N", "KN");
                }
            } else {
                doubleMetaphoneResult.append("KN", "N");
            }
            return i + 2;
        }
        if (contains(str, i + 1, 2, "LI") && !z) {
            doubleMetaphoneResult.append("KL", "L");
            return i + 2;
        } else if (i != 0 || (charAt(str, i + 1) != 'Y' && !contains(str, i + 1, 2, ES_EP_EB_EL_EY_IB_IL_IN_IE_EI_ER))) {
            if (contains(str, i + 1, 2, "ER") || charAt(str, i + 1) == 'Y') {
                if (!contains(str, 0, 6, "DANGER", "RANGER", "MANGER")) {
                    if (!contains(str, i - 1, 1, "E", "I")) {
                        if (!contains(str, i - 1, 3, "RGY", "OGY")) {
                            doubleMetaphoneResult.append('K', 'J');
                            return i + 2;
                        }
                    }
                }
            }
            if (!contains(str, i + 1, 1, "E", "I", "Y")) {
                if (!contains(str, i - 1, 4, "AGGI", "OGGI")) {
                    if (charAt(str, i + 1) == 'G') {
                        int i2 = i + 2;
                        doubleMetaphoneResult.append('K');
                        return i2;
                    }
                    int i3 = i + 1;
                    doubleMetaphoneResult.append('K');
                    return i3;
                }
            }
            if (!contains(str, 0, 4, "VAN ", "VON ")) {
                if (!contains(str, 0, 3, "SCH")) {
                    if (!contains(str, i + 1, 2, "ET")) {
                        if (contains(str, i + 1, 3, "IER")) {
                            doubleMetaphoneResult.append('J');
                        } else {
                            doubleMetaphoneResult.append('J', 'K');
                        }
                        return i + 2;
                    }
                }
            }
            doubleMetaphoneResult.append('K');
            return i + 2;
        } else {
            doubleMetaphoneResult.append('K', 'J');
            return i + 2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004b, code lost:
        if (contains(r9, r11 - 2, 1, "B", "H", "D") == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0066, code lost:
        if (contains(r9, r11 - 3, 1, "B", "H", "D") == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x007c, code lost:
        if (contains(r9, r11 - 4, 1, "B", "H") != false) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return r11 + 2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int handleGH(java.lang.String r9, org2.apache.commons.codec.language.DoubleMetaphone.DoubleMetaphoneResult r10, int r11) {
        /*
            r8 = this;
            r7 = 75
            r6 = 3
            r5 = 0
            r3 = 2
            r4 = 1
            if (r11 <= 0) goto L_0x001a
            int r0 = r11 + -1
            char r0 = r8.charAt(r9, r0)
            boolean r0 = r8.isVowel(r0)
            if (r0 != 0) goto L_0x001a
            r10.append((char) r7)
            int r0 = r11 + 2
        L_0x0019:
            return r0
        L_0x001a:
            if (r11 != 0) goto L_0x0032
            int r0 = r11 + 2
            char r0 = r8.charAt(r9, r0)
            r1 = 73
            if (r0 != r1) goto L_0x002e
            r0 = 74
            r10.append((char) r0)
        L_0x002b:
            int r0 = r11 + 2
            goto L_0x0019
        L_0x002e:
            r10.append((char) r7)
            goto L_0x002b
        L_0x0032:
            if (r11 <= r4) goto L_0x004d
            int r0 = r11 + -2
            java.lang.String[] r1 = new java.lang.String[r6]
            java.lang.String r2 = "B"
            r1[r5] = r2
            java.lang.String r2 = "H"
            r1[r4] = r2
            java.lang.String r2 = "D"
            r1[r3] = r2
            boolean r0 = contains(r9, r0, r4, r1)
            if (r0 != 0) goto L_0x007e
        L_0x004d:
            if (r11 <= r3) goto L_0x0068
            int r0 = r11 + -3
            java.lang.String[] r1 = new java.lang.String[r6]
            java.lang.String r2 = "B"
            r1[r5] = r2
            java.lang.String r2 = "H"
            r1[r4] = r2
            java.lang.String r2 = "D"
            r1[r3] = r2
            boolean r0 = contains(r9, r0, r4, r1)
            if (r0 != 0) goto L_0x007e
        L_0x0068:
            if (r11 <= r6) goto L_0x0081
            int r0 = r11 + -4
            java.lang.String[] r1 = new java.lang.String[r3]
            java.lang.String r2 = "B"
            r1[r5] = r2
            java.lang.String r2 = "H"
            r1[r4] = r2
            boolean r0 = contains(r9, r0, r4, r1)
            if (r0 == 0) goto L_0x0081
        L_0x007e:
            int r0 = r11 + 2
            goto L_0x0019
        L_0x0081:
            if (r11 <= r3) goto L_0x00bb
            int r0 = r11 + -1
            char r0 = r8.charAt(r9, r0)
            r1 = 85
            if (r0 != r1) goto L_0x00bb
            int r0 = r11 + -3
            r1 = 5
            java.lang.String[] r1 = new java.lang.String[r1]
            java.lang.String r2 = "C"
            r1[r5] = r2
            java.lang.String r2 = "G"
            r1[r4] = r2
            java.lang.String r2 = "L"
            r1[r3] = r2
            java.lang.String r2 = "R"
            r1[r6] = r2
            r2 = 4
            java.lang.String r3 = "T"
            r1[r2] = r3
            boolean r0 = contains(r9, r0, r4, r1)
            if (r0 == 0) goto L_0x00bb
            r0 = 70
            r10.append((char) r0)
        L_0x00b7:
            int r0 = r11 + 2
            goto L_0x0019
        L_0x00bb:
            if (r11 <= 0) goto L_0x00b7
            int r0 = r11 + -1
            char r0 = r8.charAt(r9, r0)
            r1 = 73
            if (r0 == r1) goto L_0x00b7
            r10.append((char) r7)
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.language.DoubleMetaphone.handleGH(java.lang.String, org2.apache.commons.codec.language.DoubleMetaphone$DoubleMetaphoneResult, int):int");
    }

    private int handleH(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if ((i != 0 && !isVowel(charAt(str, i - 1))) || !isVowel(charAt(str, i + 1))) {
            return i + 1;
        }
        doubleMetaphoneResult.append('H');
        return i + 2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int handleJ(java.lang.String r8, org2.apache.commons.codec.language.DoubleMetaphone.DoubleMetaphoneResult r9, int r10, boolean r11) {
        /*
            r7 = this;
            r6 = 72
            r2 = 4
            r5 = 74
            r4 = 1
            r3 = 0
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.String r1 = "JOSE"
            r0[r3] = r1
            boolean r0 = contains(r8, r10, r2, r0)
            if (r0 != 0) goto L_0x0021
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.String r1 = "SAN "
            r0[r3] = r1
            boolean r0 = contains(r8, r3, r2, r0)
            if (r0 == 0) goto L_0x004a
        L_0x0021:
            if (r10 != 0) goto L_0x002d
            int r0 = r10 + 4
            char r0 = r7.charAt(r8, r0)
            r1 = 32
            if (r0 == r1) goto L_0x0040
        L_0x002d:
            int r0 = r8.length()
            if (r0 == r2) goto L_0x0040
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.String r1 = "SAN "
            r0[r3] = r1
            boolean r0 = contains(r8, r3, r2, r0)
            if (r0 == 0) goto L_0x0046
        L_0x0040:
            r9.append((char) r6)
        L_0x0043:
            int r0 = r10 + 1
        L_0x0045:
            return r0
        L_0x0046:
            r9.append((char) r5, (char) r6)
            goto L_0x0043
        L_0x004a:
            if (r10 != 0) goto L_0x0069
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.String r1 = "JOSE"
            r0[r3] = r1
            boolean r0 = contains(r8, r10, r2, r0)
            if (r0 != 0) goto L_0x0069
            r0 = 65
            r9.append((char) r5, (char) r0)
        L_0x005e:
            int r0 = r10 + 1
            char r0 = r7.charAt(r8, r0)
            if (r0 != r5) goto L_0x00c6
            int r0 = r10 + 2
            goto L_0x0045
        L_0x0069:
            int r0 = r10 + -1
            char r0 = r7.charAt(r8, r0)
            boolean r0 = r7.isVowel(r0)
            if (r0 == 0) goto L_0x008f
            if (r11 != 0) goto L_0x008f
            int r0 = r10 + 1
            char r0 = r7.charAt(r8, r0)
            r1 = 65
            if (r0 == r1) goto L_0x008b
            int r0 = r10 + 1
            char r0 = r7.charAt(r8, r0)
            r1 = 79
            if (r0 != r1) goto L_0x008f
        L_0x008b:
            r9.append((char) r5, (char) r6)
            goto L_0x005e
        L_0x008f:
            int r0 = r8.length()
            int r0 = r0 + -1
            if (r10 != r0) goto L_0x009d
            r0 = 32
            r9.append((char) r5, (char) r0)
            goto L_0x005e
        L_0x009d:
            int r0 = r10 + 1
            java.lang.String[] r1 = L_T_K_S_N_M_B_Z
            boolean r0 = contains(r8, r0, r4, r1)
            if (r0 != 0) goto L_0x005e
            int r0 = r10 + -1
            r1 = 3
            java.lang.String[] r1 = new java.lang.String[r1]
            java.lang.String r2 = "S"
            r1[r3] = r2
            java.lang.String r2 = "K"
            r1[r4] = r2
            r2 = 2
            java.lang.String r3 = "L"
            r1[r2] = r3
            boolean r0 = contains(r8, r0, r4, r1)
            if (r0 != 0) goto L_0x005e
            r9.append((char) r5)
            goto L_0x005e
        L_0x00c6:
            int r0 = r10 + 1
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.language.DoubleMetaphone.handleJ(java.lang.String, org2.apache.commons.codec.language.DoubleMetaphone$DoubleMetaphoneResult, int, boolean):int");
    }

    private int handleL(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (charAt(str, i + 1) == 'L') {
            if (conditionL0(str, i)) {
                doubleMetaphoneResult.appendPrimary('L');
            } else {
                doubleMetaphoneResult.append('L');
            }
            return i + 2;
        }
        int i2 = i + 1;
        doubleMetaphoneResult.append('L');
        return i2;
    }

    private int handleP(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (charAt(str, i + 1) == 'H') {
            doubleMetaphoneResult.append('F');
            return i + 2;
        }
        doubleMetaphoneResult.append('P');
        return contains(str, i + 1, 1, "P", "B") ? i + 2 : i + 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int handleR(java.lang.String r8, org2.apache.commons.codec.language.DoubleMetaphone.DoubleMetaphoneResult r9, int r10, boolean r11) {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
            r4 = 82
            r3 = 2
            int r0 = r8.length()
            int r0 = r0 + -1
            if (r10 != r0) goto L_0x0040
            if (r11 != 0) goto L_0x0040
            int r0 = r10 + -2
            java.lang.String[] r1 = new java.lang.String[r6]
            java.lang.String r2 = "IE"
            r1[r5] = r2
            boolean r0 = contains(r8, r0, r3, r1)
            if (r0 == 0) goto L_0x0040
            int r0 = r10 + -4
            java.lang.String[] r1 = new java.lang.String[r3]
            java.lang.String r2 = "ME"
            r1[r5] = r2
            java.lang.String r2 = "MA"
            r1[r6] = r2
            boolean r0 = contains(r8, r0, r3, r1)
            if (r0 != 0) goto L_0x0040
            r9.appendAlternate((char) r4)
        L_0x0035:
            int r0 = r10 + 1
            char r0 = r7.charAt(r8, r0)
            if (r0 != r4) goto L_0x0044
            int r0 = r10 + 2
        L_0x003f:
            return r0
        L_0x0040:
            r9.append((char) r4)
            goto L_0x0035
        L_0x0044:
            int r0 = r10 + 1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.language.DoubleMetaphone.handleR(java.lang.String, org2.apache.commons.codec.language.DoubleMetaphone$DoubleMetaphoneResult, int, boolean):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00bb, code lost:
        if (contains(r10, r12 + 1, 1, "M", "N", "L", "W") == false) goto L_0x00bd;
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0136  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int handleS(java.lang.String r10, org2.apache.commons.codec.language.DoubleMetaphone.DoubleMetaphoneResult r11, int r12, boolean r13) {
        /*
            r9 = this;
            r8 = 3
            r7 = 83
            r6 = 2
            r5 = 0
            r4 = 1
            int r0 = r12 + -1
            java.lang.String[] r1 = new java.lang.String[r6]
            java.lang.String r2 = "ISL"
            r1[r5] = r2
            java.lang.String r2 = "YSL"
            r1[r4] = r2
            boolean r0 = contains(r10, r0, r8, r1)
            if (r0 == 0) goto L_0x001d
            int r0 = r12 + 1
        L_0x001c:
            return r0
        L_0x001d:
            if (r12 != 0) goto L_0x0035
            r0 = 5
            java.lang.String[] r1 = new java.lang.String[r4]
            java.lang.String r2 = "SUGAR"
            r1[r5] = r2
            boolean r0 = contains(r10, r12, r0, r1)
            if (r0 == 0) goto L_0x0035
            r0 = 88
            r11.append((char) r0, (char) r7)
            int r0 = r12 + 1
            goto L_0x001c
        L_0x0035:
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.String r1 = "SH"
            r0[r5] = r1
            boolean r0 = contains(r10, r12, r6, r0)
            if (r0 == 0) goto L_0x006e
            int r0 = r12 + 1
            r1 = 4
            r2 = 4
            java.lang.String[] r2 = new java.lang.String[r2]
            java.lang.String r3 = "HEIM"
            r2[r5] = r3
            java.lang.String r3 = "HOEK"
            r2[r4] = r3
            java.lang.String r3 = "HOLM"
            r2[r6] = r3
            java.lang.String r3 = "HOLZ"
            r2[r8] = r3
            boolean r0 = contains(r10, r0, r1, r2)
            if (r0 == 0) goto L_0x0068
            r11.append((char) r7)
        L_0x0065:
            int r0 = r12 + 2
            goto L_0x001c
        L_0x0068:
            r0 = 88
            r11.append((char) r0)
            goto L_0x0065
        L_0x006e:
            java.lang.String[] r0 = new java.lang.String[r6]
            java.lang.String r1 = "SIO"
            r0[r5] = r1
            java.lang.String r1 = "SIA"
            r0[r4] = r1
            boolean r0 = contains(r10, r12, r8, r0)
            if (r0 != 0) goto L_0x008e
            r0 = 4
            java.lang.String[] r1 = new java.lang.String[r4]
            java.lang.String r2 = "SIAN"
            r1[r5] = r2
            boolean r0 = contains(r10, r12, r0, r1)
            if (r0 == 0) goto L_0x009c
        L_0x008e:
            if (r13 == 0) goto L_0x0096
            r11.append((char) r7)
        L_0x0093:
            int r0 = r12 + 3
            goto L_0x001c
        L_0x0096:
            r0 = 88
            r11.append((char) r7, (char) r0)
            goto L_0x0093
        L_0x009c:
            if (r12 != 0) goto L_0x00bd
            int r0 = r12 + 1
            r1 = 4
            java.lang.String[] r1 = new java.lang.String[r1]
            java.lang.String r2 = "M"
            r1[r5] = r2
            java.lang.String r2 = "N"
            r1[r4] = r2
            java.lang.String r2 = "L"
            r1[r6] = r2
            java.lang.String r2 = "W"
            r1[r8] = r2
            boolean r0 = contains(r10, r0, r4, r1)
            if (r0 != 0) goto L_0x00cc
        L_0x00bd:
            int r0 = r12 + 1
            java.lang.String[] r1 = new java.lang.String[r4]
            java.lang.String r2 = "Z"
            r1[r5] = r2
            boolean r0 = contains(r10, r0, r4, r1)
            if (r0 == 0) goto L_0x00e8
        L_0x00cc:
            r0 = 88
            r11.append((char) r7, (char) r0)
            int r0 = r12 + 1
            java.lang.String[] r1 = new java.lang.String[r4]
            java.lang.String r2 = "Z"
            r1[r5] = r2
            boolean r0 = contains(r10, r0, r4, r1)
            if (r0 == 0) goto L_0x00e4
            int r0 = r12 + 2
            goto L_0x001c
        L_0x00e4:
            int r0 = r12 + 1
            goto L_0x001c
        L_0x00e8:
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.String r1 = "SC"
            r0[r5] = r1
            boolean r0 = contains(r10, r12, r6, r0)
            if (r0 == 0) goto L_0x00fb
            int r0 = r9.handleSC(r10, r11, r12)
            goto L_0x001c
        L_0x00fb:
            int r0 = r10.length()
            int r0 = r0 + -1
            if (r12 != r0) goto L_0x0132
            int r0 = r12 + -2
            java.lang.String[] r1 = new java.lang.String[r6]
            java.lang.String r2 = "AI"
            r1[r5] = r2
            java.lang.String r2 = "OI"
            r1[r4] = r2
            boolean r0 = contains(r10, r0, r6, r1)
            if (r0 == 0) goto L_0x0132
            r11.appendAlternate((char) r7)
        L_0x011a:
            int r0 = r12 + 1
            java.lang.String[] r1 = new java.lang.String[r6]
            java.lang.String r2 = "S"
            r1[r5] = r2
            java.lang.String r2 = "Z"
            r1[r4] = r2
            boolean r0 = contains(r10, r0, r4, r1)
            if (r0 == 0) goto L_0x0136
            int r0 = r12 + 2
            goto L_0x001c
        L_0x0132:
            r11.append((char) r7)
            goto L_0x011a
        L_0x0136:
            int r0 = r12 + 1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.language.DoubleMetaphone.handleS(java.lang.String, org2.apache.commons.codec.language.DoubleMetaphone$DoubleMetaphoneResult, int, boolean):int");
    }

    private int handleSC(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (charAt(str, i + 2) == 'H') {
            if (contains(str, i + 3, 2, "OO", "ER", "EN", "UY", "ED", "EM")) {
                if (contains(str, i + 3, 2, "ER", "EN")) {
                    doubleMetaphoneResult.append("X", "SK");
                } else {
                    doubleMetaphoneResult.append("SK");
                }
            } else if (i != 0 || isVowel(charAt(str, 3)) || charAt(str, 3) == 'W') {
                doubleMetaphoneResult.append('X');
            } else {
                doubleMetaphoneResult.append('X', 'S');
            }
        } else {
            if (contains(str, i + 2, 1, "I", "E", "Y")) {
                doubleMetaphoneResult.append('S');
            } else {
                doubleMetaphoneResult.append("SK");
            }
        }
        return i + 3;
    }

    private int handleT(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i) {
        if (contains(str, i, 4, "TION")) {
            doubleMetaphoneResult.append('X');
            return i + 3;
        }
        if (contains(str, i, 3, "TIA", "TCH")) {
            doubleMetaphoneResult.append('X');
            return i + 3;
        }
        if (!contains(str, i, 2, "TH")) {
            if (!contains(str, i, 3, "TTH")) {
                doubleMetaphoneResult.append('T');
                return contains(str, i + 1, 1, "T", "D") ? i + 2 : i + 1;
            }
        }
        if (!contains(str, i + 2, 2, "OM", "AM")) {
            if (!contains(str, 0, 4, "VAN ", "VON ")) {
                if (!contains(str, 0, 3, "SCH")) {
                    doubleMetaphoneResult.append('0', 'T');
                    return i + 2;
                }
            }
        }
        doubleMetaphoneResult.append('T');
        return i + 2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0033, code lost:
        if (contains(r10, r12, 2, "WH") != false) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int handleW(java.lang.String r10, org2.apache.commons.codec.language.DoubleMetaphone.DoubleMetaphoneResult r11, int r12) {
        /*
            r9 = this;
            r8 = 4
            r7 = 3
            r6 = 2
            r5 = 1
            r4 = 0
            java.lang.String[] r0 = new java.lang.String[r5]
            java.lang.String r1 = "WR"
            r0[r4] = r1
            boolean r0 = contains(r10, r12, r6, r0)
            if (r0 == 0) goto L_0x001a
            r0 = 82
            r11.append((char) r0)
            int r0 = r12 + 2
        L_0x0019:
            return r0
        L_0x001a:
            if (r12 != 0) goto L_0x0051
            int r0 = r12 + 1
            char r0 = r9.charAt(r10, r0)
            boolean r0 = r9.isVowel(r0)
            if (r0 != 0) goto L_0x0035
            java.lang.String[] r0 = new java.lang.String[r5]
            java.lang.String r1 = "WH"
            r0[r4] = r1
            boolean r0 = contains(r10, r12, r6, r0)
            if (r0 == 0) goto L_0x0051
        L_0x0035:
            int r0 = r12 + 1
            char r0 = r9.charAt(r10, r0)
            boolean r0 = r9.isVowel(r0)
            if (r0 == 0) goto L_0x004b
            r0 = 65
            r1 = 70
            r11.append((char) r0, (char) r1)
        L_0x0048:
            int r0 = r12 + 1
            goto L_0x0019
        L_0x004b:
            r0 = 65
            r11.append((char) r0)
            goto L_0x0048
        L_0x0051:
            int r0 = r10.length()
            int r0 = r0 + -1
            if (r12 != r0) goto L_0x0065
            int r0 = r12 + -1
            char r0 = r9.charAt(r10, r0)
            boolean r0 = r9.isVowel(r0)
            if (r0 != 0) goto L_0x0091
        L_0x0065:
            int r0 = r12 + -1
            r1 = 5
            java.lang.String[] r2 = new java.lang.String[r8]
            java.lang.String r3 = "EWSKI"
            r2[r4] = r3
            java.lang.String r3 = "EWSKY"
            r2[r5] = r3
            java.lang.String r3 = "OWSKI"
            r2[r6] = r3
            java.lang.String r3 = "OWSKY"
            r2[r7] = r3
            boolean r0 = contains(r10, r0, r1, r2)
            if (r0 != 0) goto L_0x0091
            java.lang.String[] r0 = new java.lang.String[r5]
            java.lang.String r1 = "SCH"
            r0[r4] = r1
            boolean r0 = contains(r10, r4, r7, r0)
            if (r0 == 0) goto L_0x0099
        L_0x0091:
            r0 = 70
            r11.appendAlternate((char) r0)
            int r0 = r12 + 1
            goto L_0x0019
        L_0x0099:
            java.lang.String[] r0 = new java.lang.String[r6]
            java.lang.String r1 = "WICZ"
            r0[r4] = r1
            java.lang.String r1 = "WITZ"
            r0[r5] = r1
            boolean r0 = contains(r10, r12, r8, r0)
            if (r0 == 0) goto L_0x00b8
            java.lang.String r0 = "TS"
            java.lang.String r1 = "FX"
            r11.append((java.lang.String) r0, (java.lang.String) r1)
            int r0 = r12 + 4
            goto L_0x0019
        L_0x00b8:
            int r0 = r12 + 1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.language.DoubleMetaphone.handleW(java.lang.String, org2.apache.commons.codec.language.DoubleMetaphone$DoubleMetaphoneResult, int):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003c, code lost:
        if (contains(r8, r10 - 2, 2, "AU", "OU") == false) goto L_0x003e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int handleX(java.lang.String r8, org2.apache.commons.codec.language.DoubleMetaphone.DoubleMetaphoneResult r9, int r10) {
        /*
            r7 = this;
            r6 = 0
            r5 = 2
            r4 = 1
            if (r10 != 0) goto L_0x000d
            r0 = 83
            r9.append((char) r0)
            int r0 = r10 + 1
        L_0x000c:
            return r0
        L_0x000d:
            int r0 = r8.length()
            int r0 = r0 + -1
            if (r10 != r0) goto L_0x003e
            int r0 = r10 + -3
            r1 = 3
            java.lang.String[] r2 = new java.lang.String[r5]
            java.lang.String r3 = "IAU"
            r2[r6] = r3
            java.lang.String r3 = "EAU"
            r2[r4] = r3
            boolean r0 = contains(r8, r0, r1, r2)
            if (r0 != 0) goto L_0x0044
            int r0 = r10 + -2
            java.lang.String[] r1 = new java.lang.String[r5]
            java.lang.String r2 = "AU"
            r1[r6] = r2
            java.lang.String r2 = "OU"
            r1[r4] = r2
            boolean r0 = contains(r8, r0, r5, r1)
            if (r0 != 0) goto L_0x0044
        L_0x003e:
            java.lang.String r0 = "KS"
            r9.append((java.lang.String) r0)
        L_0x0044:
            int r0 = r10 + 1
            java.lang.String[] r1 = new java.lang.String[r5]
            java.lang.String r2 = "C"
            r1[r6] = r2
            java.lang.String r2 = "X"
            r1[r4] = r2
            boolean r0 = contains(r8, r0, r4, r1)
            if (r0 == 0) goto L_0x005b
            int r0 = r10 + 2
            goto L_0x000c
        L_0x005b:
            int r0 = r10 + 1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.language.DoubleMetaphone.handleX(java.lang.String, org2.apache.commons.codec.language.DoubleMetaphone$DoubleMetaphoneResult, int):int");
    }

    private int handleZ(String str, DoubleMetaphoneResult doubleMetaphoneResult, int i, boolean z) {
        if (charAt(str, i + 1) == 'H') {
            doubleMetaphoneResult.append('J');
            return i + 2;
        }
        if (contains(str, i + 1, 2, "ZO", "ZI", "ZA") || (z && i > 0 && charAt(str, i - 1) != 'T')) {
            doubleMetaphoneResult.append("S", "TS");
        } else {
            doubleMetaphoneResult.append('S');
        }
        return charAt(str, i + 1) == 'Z' ? i + 2 : i + 1;
    }

    private boolean isSilentStart(String str) {
        for (String startsWith : SILENT_START) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private boolean isSlavoGermanic(String str) {
        return str.indexOf(87) > -1 || str.indexOf(75) > -1 || str.indexOf("CZ") > -1 || str.indexOf("WITZ") > -1;
    }

    private boolean isVowel(char c) {
        return VOWELS.indexOf(c) != -1;
    }

    /* access modifiers changed from: protected */
    public char charAt(String str, int i) {
        if (i < 0 || i >= str.length()) {
            return 0;
        }
        return str.charAt(i);
    }

    public String doubleMetaphone(String str) {
        return doubleMetaphone(str, false);
    }

    public String doubleMetaphone(String str, boolean z) {
        String cleanInput = cleanInput(str);
        if (cleanInput == null) {
            return null;
        }
        boolean isSlavoGermanic = isSlavoGermanic(cleanInput);
        int i = isSilentStart(cleanInput) ? 1 : 0;
        DoubleMetaphoneResult doubleMetaphoneResult = new DoubleMetaphoneResult(getMaxCodeLen());
        while (!doubleMetaphoneResult.isComplete() && i <= cleanInput.length() - 1) {
            switch (cleanInput.charAt(i)) {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                case 'Y':
                    i = handleAEIOUY(doubleMetaphoneResult, i);
                    break;
                case 'B':
                    doubleMetaphoneResult.append('P');
                    if (charAt(cleanInput, i + 1) != 'B') {
                        i++;
                        break;
                    } else {
                        i += 2;
                        break;
                    }
                case 'C':
                    i = handleC(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'D':
                    i = handleD(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'F':
                    doubleMetaphoneResult.append('F');
                    if (charAt(cleanInput, i + 1) != 'F') {
                        i++;
                        break;
                    } else {
                        i += 2;
                        break;
                    }
                case 'G':
                    i = handleG(cleanInput, doubleMetaphoneResult, i, isSlavoGermanic);
                    break;
                case 'H':
                    i = handleH(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'J':
                    i = handleJ(cleanInput, doubleMetaphoneResult, i, isSlavoGermanic);
                    break;
                case 'K':
                    doubleMetaphoneResult.append('K');
                    if (charAt(cleanInput, i + 1) != 'K') {
                        i++;
                        break;
                    } else {
                        i += 2;
                        break;
                    }
                case 'L':
                    i = handleL(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'M':
                    doubleMetaphoneResult.append('M');
                    if (!conditionM0(cleanInput, i)) {
                        i++;
                        break;
                    } else {
                        i += 2;
                        break;
                    }
                case 'N':
                    doubleMetaphoneResult.append('N');
                    if (charAt(cleanInput, i + 1) != 'N') {
                        i++;
                        break;
                    } else {
                        i += 2;
                        break;
                    }
                case 'P':
                    i = handleP(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'Q':
                    doubleMetaphoneResult.append('K');
                    if (charAt(cleanInput, i + 1) != 'Q') {
                        i++;
                        break;
                    } else {
                        i += 2;
                        break;
                    }
                case 'R':
                    i = handleR(cleanInput, doubleMetaphoneResult, i, isSlavoGermanic);
                    break;
                case 'S':
                    i = handleS(cleanInput, doubleMetaphoneResult, i, isSlavoGermanic);
                    break;
                case 'T':
                    i = handleT(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'V':
                    doubleMetaphoneResult.append('F');
                    if (charAt(cleanInput, i + 1) != 'V') {
                        i++;
                        break;
                    } else {
                        i += 2;
                        break;
                    }
                case 'W':
                    i = handleW(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'X':
                    i = handleX(cleanInput, doubleMetaphoneResult, i);
                    break;
                case 'Z':
                    i = handleZ(cleanInput, doubleMetaphoneResult, i, isSlavoGermanic);
                    break;
                case 199:
                    doubleMetaphoneResult.append('S');
                    i++;
                    break;
                case 209:
                    doubleMetaphoneResult.append('N');
                    i++;
                    break;
                default:
                    i++;
                    break;
            }
        }
        return z ? doubleMetaphoneResult.getAlternate() : doubleMetaphoneResult.getPrimary();
    }

    public Object encode(Object obj) throws EncoderException {
        if (obj instanceof String) {
            return doubleMetaphone((String) obj);
        }
        throw new EncoderException("DoubleMetaphone encode parameter is not of type String");
    }

    public String encode(String str) {
        return doubleMetaphone(str);
    }

    public int getMaxCodeLen() {
        return this.maxCodeLen;
    }

    public boolean isDoubleMetaphoneEqual(String str, String str2) {
        return isDoubleMetaphoneEqual(str, str2, false);
    }

    public boolean isDoubleMetaphoneEqual(String str, String str2, boolean z) {
        return doubleMetaphone(str, z).equals(doubleMetaphone(str2, z));
    }

    public void setMaxCodeLen(int i) {
        this.maxCodeLen = i;
    }
}
