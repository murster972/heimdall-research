package org2.apache.commons.codec.language.bm;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import net.pubnative.library.request.PubnativeRequest;
import org.apache.commons.lang3.StringUtils;
import org2.apache.commons.codec.language.bm.Languages;
import org2.apache.commons.codec.language.bm.Rule;

public class PhoneticEngine {
    private static final int DEFAULT_MAX_PHONEMES = 20;
    private static final Map<NameType, Set<String>> NAME_PREFIXES = new EnumMap(NameType.class);
    private final boolean concat;
    private final Lang lang;
    private final int maxPhonemes;
    private final NameType nameType;
    private final RuleType ruleType;

    static final class PhonemeBuilder {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Set<Rule.Phoneme> f17579;

        private PhonemeBuilder(Set<Rule.Phoneme> set) {
            this.f17579 = set;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public static PhonemeBuilder m22469(Languages.LanguageSet languageSet) {
            return new PhonemeBuilder(Collections.singleton(new Rule.Phoneme("", languageSet)));
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public String m22470() {
            StringBuilder sb = new StringBuilder();
            for (Rule.Phoneme next : this.f17579) {
                if (sb.length() > 0) {
                    sb.append("|");
                }
                sb.append(next.getPhonemeText());
            }
            return sb.toString();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Set<Rule.Phoneme> m22471() {
            return this.f17579;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PhonemeBuilder m22472(CharSequence charSequence) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (Rule.Phoneme append : this.f17579) {
                linkedHashSet.add(append.append(charSequence));
            }
            return new PhonemeBuilder(linkedHashSet);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public PhonemeBuilder m22473(Rule.PhonemeExpr phonemeExpr, int i) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            loop0:
            for (Rule.Phoneme next : this.f17579) {
                Iterator<Rule.Phoneme> it2 = phonemeExpr.getPhonemes().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        Rule.Phoneme join = next.join(it2.next());
                        if (!join.getLanguages().isEmpty()) {
                            if (linkedHashSet.size() >= i) {
                                break loop0;
                            }
                            linkedHashSet.add(join);
                        }
                    }
                }
            }
            return new PhonemeBuilder(linkedHashSet);
        }
    }

    private static final class RulesApplication {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f17580;

        /* renamed from: 连任  reason: contains not printable characters */
        private final int f17581;

        /* renamed from: 靐  reason: contains not printable characters */
        private final CharSequence f17582;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f17583;

        /* renamed from: 齉  reason: contains not printable characters */
        private PhonemeBuilder f17584;

        /* renamed from: 龘  reason: contains not printable characters */
        private final List<Rule> f17585;

        public RulesApplication(List<Rule> list, CharSequence charSequence, PhonemeBuilder phonemeBuilder, int i, int i2) {
            if (list == null) {
                throw new NullPointerException("The finalRules argument must not be null");
            }
            this.f17585 = list;
            this.f17584 = phonemeBuilder;
            this.f17582 = charSequence;
            this.f17583 = i;
            this.f17581 = i2;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public PhonemeBuilder m22474() {
            return this.f17584;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean m22475() {
            return this.f17580;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public RulesApplication m22476() {
            int i;
            int i2 = 0;
            this.f17580 = false;
            Iterator<Rule> it2 = this.f17585.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    i = i2;
                    break;
                }
                Rule next = it2.next();
                i = next.getPattern().length();
                if (next.patternAndContextMatches(this.f17582, this.f17583)) {
                    this.f17584 = this.f17584.m22473(next.getPhoneme(), this.f17581);
                    this.f17580 = true;
                    break;
                }
                i2 = i;
            }
            if (!this.f17580) {
                i = 1;
            }
            this.f17583 += i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m22477() {
            return this.f17583;
        }
    }

    static {
        NAME_PREFIXES.put(NameType.ASHKENAZI, Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"bar", "ben", "da", "de", "van", "von"}))));
        NAME_PREFIXES.put(NameType.SEPHARDIC, Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{PubnativeRequest.Parameters.ASSET_LAYOUT, "el", "da", "dal", "de", "del", "dela", "de la", "della", "des", "di", "do", "dos", "du", "van", "von"}))));
        NAME_PREFIXES.put(NameType.GENERIC, Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"da", "dal", "de", "del", "dela", "de la", "della", "des", "di", "do", "dos", "du", "van", "von"}))));
    }

    public PhoneticEngine(NameType nameType2, RuleType ruleType2, boolean z) {
        this(nameType2, ruleType2, z, 20);
    }

    public PhoneticEngine(NameType nameType2, RuleType ruleType2, boolean z, int i) {
        if (ruleType2 == RuleType.RULES) {
            throw new IllegalArgumentException("ruleType must not be " + RuleType.RULES);
        }
        this.nameType = nameType2;
        this.ruleType = ruleType2;
        this.concat = z;
        this.lang = Lang.instance(nameType2);
        this.maxPhonemes = i;
    }

    private PhonemeBuilder applyFinalRules(PhonemeBuilder phonemeBuilder, List<Rule> list) {
        if (list == null) {
            throw new NullPointerException("finalRules can not be null");
        } else if (list.isEmpty()) {
            return phonemeBuilder;
        } else {
            TreeSet treeSet = new TreeSet(Rule.Phoneme.COMPARATOR);
            for (Rule.Phoneme next : phonemeBuilder.m22471()) {
                PhonemeBuilder r3 = PhonemeBuilder.m22469(next.getLanguages());
                CharSequence cacheSubSequence = cacheSubSequence(next.getPhonemeText());
                int i = 0;
                while (i < cacheSubSequence.length()) {
                    RulesApplication r0 = new RulesApplication(list, cacheSubSequence, r3, i, this.maxPhonemes).m22476();
                    boolean r1 = r0.m22475();
                    r3 = r0.m22474();
                    if (!r1) {
                        r3 = r3.m22472(cacheSubSequence.subSequence(i, i + 1));
                    }
                    i = r0.m22477();
                }
                treeSet.addAll(r3.m22471());
            }
            return new PhonemeBuilder(treeSet);
        }
    }

    private static CharSequence cacheSubSequence(final CharSequence charSequence) {
        final CharSequence[][] charSequenceArr = (CharSequence[][]) Array.newInstance(CharSequence.class, new int[]{charSequence.length(), charSequence.length()});
        return new CharSequence() {
            public char charAt(int i) {
                return charSequence.charAt(i);
            }

            public int length() {
                return charSequence.length();
            }

            public CharSequence subSequence(int i, int i2) {
                if (i == i2) {
                    return "";
                }
                CharSequence charSequence = charSequenceArr[i][i2 - 1];
                if (charSequence != null) {
                    return charSequence;
                }
                CharSequence subSequence = charSequence.subSequence(i, i2);
                charSequenceArr[i][i2 - 1] = subSequence;
                return subSequence;
            }
        };
    }

    private static String join(Iterable<String> iterable, String str) {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it2 = iterable.iterator();
        if (it2.hasNext()) {
            sb.append(it2.next());
        }
        while (it2.hasNext()) {
            sb.append(str).append(it2.next());
        }
        return sb.toString();
    }

    public String encode(String str) {
        return encode(str, this.lang.guessLanguages(str));
    }

    public String encode(String str, Languages.LanguageSet languageSet) {
        String str2;
        int i = 0;
        List<Rule> instance = Rule.getInstance(this.nameType, RuleType.RULES, languageSet);
        List<Rule> instance2 = Rule.getInstance(this.nameType, this.ruleType, "common");
        List<Rule> instance3 = Rule.getInstance(this.nameType, this.ruleType, languageSet);
        String trim = str.toLowerCase(Locale.ENGLISH).replace('-', ' ').trim();
        if (this.nameType == NameType.GENERIC) {
            if (trim.length() < 2 || !trim.substring(0, 2).equals("d'")) {
                for (String str3 : NAME_PREFIXES.get(this.nameType)) {
                    if (trim.startsWith(str3 + StringUtils.SPACE)) {
                        String substring = trim.substring(str3.length() + 1);
                        return "(" + encode(substring) + ")-(" + encode(str3 + substring) + ")";
                    }
                }
            } else {
                String substring2 = trim.substring(2);
                return "(" + encode(substring2) + ")-(" + encode("d" + substring2) + ")";
            }
        }
        List<String> asList = Arrays.asList(trim.split("\\s+"));
        ArrayList<String> arrayList = new ArrayList<>();
        switch (this.nameType) {
            case SEPHARDIC:
                for (String split : asList) {
                    String[] split2 = split.split("'");
                    arrayList.add(split2[split2.length - 1]);
                }
                arrayList.removeAll(NAME_PREFIXES.get(this.nameType));
                break;
            case ASHKENAZI:
                arrayList.addAll(asList);
                arrayList.removeAll(NAME_PREFIXES.get(this.nameType));
                break;
            case GENERIC:
                arrayList.addAll(asList);
                break;
            default:
                throw new IllegalStateException("Unreachable case: " + this.nameType);
        }
        if (this.concat) {
            str2 = join(arrayList, StringUtils.SPACE);
        } else if (arrayList.size() == 1) {
            str2 = (String) asList.iterator().next();
        } else {
            StringBuilder sb = new StringBuilder();
            for (String encode : arrayList) {
                sb.append("-").append(encode(encode));
            }
            return sb.substring(1);
        }
        PhonemeBuilder r3 = PhonemeBuilder.m22469(languageSet);
        CharSequence cacheSubSequence = cacheSubSequence(str2);
        while (i < cacheSubSequence.length()) {
            RulesApplication r0 = new RulesApplication(instance, cacheSubSequence, r3, i, this.maxPhonemes).m22476();
            i = r0.m22477();
            r3 = r0.m22474();
        }
        return applyFinalRules(applyFinalRules(r3, instance2), instance3).m22470();
    }

    public Lang getLang() {
        return this.lang;
    }

    public int getMaxPhonemes() {
        return this.maxPhonemes;
    }

    public NameType getNameType() {
        return this.nameType;
    }

    public RuleType getRuleType() {
        return this.ruleType;
    }

    public boolean isConcat() {
        return this.concat;
    }
}
