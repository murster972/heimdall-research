package org2.apache.commons.codec;

public interface Decoder {
    Object decode(Object obj) throws DecoderException;
}
