package org2.apache.commons.codec.binary;

import org2.apache.commons.codec.binary.BaseNCodec;

public class Base32 extends BaseNCodec {
    private static final int BITS_PER_ENCODED_BYTE = 5;
    private static final int BYTES_PER_ENCODED_BLOCK = 8;
    private static final int BYTES_PER_UNENCODED_BLOCK = 5;
    private static final byte[] CHUNK_SEPARATOR = {13, 10};
    private static final byte[] DECODE_TABLE = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 63, -1, -1, 26, 27, 28, 29, 30, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
    private static final byte[] ENCODE_TABLE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 50, 51, 52, 53, 54, 55};
    private static final byte[] HEX_DECODE_TABLE = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 63, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32};
    private static final byte[] HEX_ENCODE_TABLE = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86};
    private static final int MASK_5BITS = 31;
    private final int decodeSize;
    private final byte[] decodeTable;
    private final int encodeSize;
    private final byte[] encodeTable;
    private final byte[] lineSeparator;

    public Base32() {
        this(false);
    }

    public Base32(int i) {
        this(i, CHUNK_SEPARATOR);
    }

    public Base32(int i, byte[] bArr) {
        this(i, bArr, false);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Base32(int i, byte[] bArr, boolean z) {
        super(5, 8, i, bArr == null ? 0 : bArr.length);
        if (z) {
            this.encodeTable = HEX_ENCODE_TABLE;
            this.decodeTable = HEX_DECODE_TABLE;
        } else {
            this.encodeTable = ENCODE_TABLE;
            this.decodeTable = DECODE_TABLE;
        }
        if (i <= 0) {
            this.encodeSize = 8;
            this.lineSeparator = null;
        } else if (bArr == null) {
            throw new IllegalArgumentException("lineLength " + i + " > 0, but lineSeparator is null");
        } else if (containsAlphabetOrPad(bArr)) {
            throw new IllegalArgumentException("lineSeparator must not contain Base32 characters: [" + StringUtils.newStringUtf8(bArr) + "]");
        } else {
            this.encodeSize = bArr.length + 8;
            this.lineSeparator = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.lineSeparator, 0, bArr.length);
        }
        this.decodeSize = this.encodeSize - 1;
    }

    public Base32(boolean z) {
        this(0, (byte[]) null, z);
    }

    /* access modifiers changed from: package-private */
    public void decode(byte[] bArr, int i, int i2, BaseNCodec.Context context) {
        byte b;
        if (!context.f17560) {
            if (i2 < 0) {
                context.f17560 = true;
            }
            int i3 = 0;
            while (true) {
                if (i3 >= i2) {
                    break;
                }
                int i4 = i + 1;
                byte b2 = bArr[i];
                if (b2 == 61) {
                    context.f17560 = true;
                    break;
                }
                byte[] ensureBufferSize = ensureBufferSize(this.decodeSize, context);
                if (b2 >= 0 && b2 < this.decodeTable.length && (b = this.decodeTable[b2]) >= 0) {
                    context.f17562 = (context.f17562 + 1) % 8;
                    context.f17564 = (context.f17564 << 5) + ((long) b);
                    if (context.f17562 == 0) {
                        int i5 = context.f17565;
                        context.f17565 = i5 + 1;
                        ensureBufferSize[i5] = (byte) ((int) ((context.f17564 >> 32) & 255));
                        int i6 = context.f17565;
                        context.f17565 = i6 + 1;
                        ensureBufferSize[i6] = (byte) ((int) ((context.f17564 >> 24) & 255));
                        int i7 = context.f17565;
                        context.f17565 = i7 + 1;
                        ensureBufferSize[i7] = (byte) ((int) ((context.f17564 >> 16) & 255));
                        int i8 = context.f17565;
                        context.f17565 = i8 + 1;
                        ensureBufferSize[i8] = (byte) ((int) ((context.f17564 >> 8) & 255));
                        int i9 = context.f17565;
                        context.f17565 = i9 + 1;
                        ensureBufferSize[i9] = (byte) ((int) (context.f17564 & 255));
                    }
                }
                i3++;
                i = i4;
            }
            if (context.f17560 && context.f17562 >= 2) {
                byte[] ensureBufferSize2 = ensureBufferSize(this.decodeSize, context);
                switch (context.f17562) {
                    case 2:
                        int i10 = context.f17565;
                        context.f17565 = i10 + 1;
                        ensureBufferSize2[i10] = (byte) ((int) ((context.f17564 >> 2) & 255));
                        return;
                    case 3:
                        int i11 = context.f17565;
                        context.f17565 = i11 + 1;
                        ensureBufferSize2[i11] = (byte) ((int) ((context.f17564 >> 7) & 255));
                        return;
                    case 4:
                        context.f17564 >>= 4;
                        int i12 = context.f17565;
                        context.f17565 = i12 + 1;
                        ensureBufferSize2[i12] = (byte) ((int) ((context.f17564 >> 8) & 255));
                        int i13 = context.f17565;
                        context.f17565 = i13 + 1;
                        ensureBufferSize2[i13] = (byte) ((int) (context.f17564 & 255));
                        return;
                    case 5:
                        context.f17564 >>= 1;
                        int i14 = context.f17565;
                        context.f17565 = i14 + 1;
                        ensureBufferSize2[i14] = (byte) ((int) ((context.f17564 >> 16) & 255));
                        int i15 = context.f17565;
                        context.f17565 = i15 + 1;
                        ensureBufferSize2[i15] = (byte) ((int) ((context.f17564 >> 8) & 255));
                        int i16 = context.f17565;
                        context.f17565 = i16 + 1;
                        ensureBufferSize2[i16] = (byte) ((int) (context.f17564 & 255));
                        return;
                    case 6:
                        context.f17564 >>= 6;
                        int i17 = context.f17565;
                        context.f17565 = i17 + 1;
                        ensureBufferSize2[i17] = (byte) ((int) ((context.f17564 >> 16) & 255));
                        int i18 = context.f17565;
                        context.f17565 = i18 + 1;
                        ensureBufferSize2[i18] = (byte) ((int) ((context.f17564 >> 8) & 255));
                        int i19 = context.f17565;
                        context.f17565 = i19 + 1;
                        ensureBufferSize2[i19] = (byte) ((int) (context.f17564 & 255));
                        return;
                    case 7:
                        context.f17564 >>= 3;
                        int i20 = context.f17565;
                        context.f17565 = i20 + 1;
                        ensureBufferSize2[i20] = (byte) ((int) ((context.f17564 >> 24) & 255));
                        int i21 = context.f17565;
                        context.f17565 = i21 + 1;
                        ensureBufferSize2[i21] = (byte) ((int) ((context.f17564 >> 16) & 255));
                        int i22 = context.f17565;
                        context.f17565 = i22 + 1;
                        ensureBufferSize2[i22] = (byte) ((int) ((context.f17564 >> 8) & 255));
                        int i23 = context.f17565;
                        context.f17565 = i23 + 1;
                        ensureBufferSize2[i23] = (byte) ((int) (context.f17564 & 255));
                        return;
                    default:
                        throw new IllegalStateException("Impossible modulus " + context.f17562);
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v35, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v36, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void encode(byte[] r9, int r10, int r11, org2.apache.commons.codec.binary.BaseNCodec.Context r12) {
        /*
            r8 = this;
            boolean r0 = r12.f17560
            if (r0 == 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            if (r11 >= 0) goto L_0x0251
            r0 = 1
            r12.f17560 = r0
            int r0 = r12.f17562
            if (r0 != 0) goto L_0x0012
            int r0 = r8.lineLength
            if (r0 == 0) goto L_0x0004
        L_0x0012:
            int r0 = r8.encodeSize
            byte[] r0 = r8.ensureBufferSize(r0, r12)
            int r1 = r12.f17565
            int r2 = r12.f17562
            switch(r2) {
                case 0: goto L_0x009d;
                case 1: goto L_0x003b;
                case 2: goto L_0x00c3;
                case 3: goto L_0x013a;
                case 4: goto L_0x01bc;
                default: goto L_0x001f;
            }
        L_0x001f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Impossible modulus "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r12.f17562
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x003b:
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 3
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 2
            long r4 = r4 << r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
        L_0x009d:
            int r2 = r12.f17561
            int r3 = r12.f17565
            int r1 = r3 - r1
            int r1 = r1 + r2
            r12.f17561 = r1
            int r1 = r8.lineLength
            if (r1 <= 0) goto L_0x0004
            int r1 = r12.f17561
            if (r1 <= 0) goto L_0x0004
            byte[] r1 = r8.lineSeparator
            r2 = 0
            int r3 = r12.f17565
            byte[] r4 = r8.lineSeparator
            int r4 = r4.length
            java.lang.System.arraycopy(r1, r2, r0, r3, r4)
            int r0 = r12.f17565
            byte[] r1 = r8.lineSeparator
            int r1 = r1.length
            int r0 = r0 + r1
            r12.f17565 = r0
            goto L_0x0004
        L_0x00c3:
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 11
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 6
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 1
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 4
            long r4 = r4 << r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            goto L_0x009d
        L_0x013a:
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 19
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 14
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 9
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 4
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 1
            long r4 = r4 << r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            goto L_0x009d
        L_0x01bc:
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 27
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 22
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 17
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 12
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 7
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 2
            long r4 = r4 >> r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            byte[] r3 = r8.encodeTable
            long r4 = r12.f17564
            r6 = 3
            long r4 = r4 << r6
            int r4 = (int) r4
            r4 = r4 & 31
            byte r3 = r3[r4]
            r0[r2] = r3
            int r2 = r12.f17565
            int r3 = r2 + 1
            r12.f17565 = r3
            r3 = 61
            r0[r2] = r3
            goto L_0x009d
        L_0x0251:
            r0 = 0
            r1 = r0
        L_0x0253:
            if (r1 >= r11) goto L_0x0004
            int r0 = r8.encodeSize
            byte[] r3 = r8.ensureBufferSize(r0, r12)
            int r0 = r12.f17562
            int r0 = r0 + 1
            int r0 = r0 % 5
            r12.f17562 = r0
            int r2 = r10 + 1
            byte r0 = r9[r10]
            if (r0 >= 0) goto L_0x026b
            int r0 = r0 + 256
        L_0x026b:
            long r4 = r12.f17564
            r6 = 8
            long r4 = r4 << r6
            long r6 = (long) r0
            long r4 = r4 + r6
            r12.f17564 = r4
            int r0 = r12.f17562
            if (r0 != 0) goto L_0x033a
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            r5 = 35
            long r6 = r6 >> r5
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            r5 = 30
            long r6 = r6 >> r5
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            r5 = 25
            long r6 = r6 >> r5
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            r5 = 20
            long r6 = r6 >> r5
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            r5 = 15
            long r6 = r6 >> r5
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            r5 = 10
            long r6 = r6 >> r5
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            r5 = 5
            long r6 = r6 >> r5
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17565
            int r4 = r0 + 1
            r12.f17565 = r4
            byte[] r4 = r8.encodeTable
            long r6 = r12.f17564
            int r5 = (int) r6
            r5 = r5 & 31
            byte r4 = r4[r5]
            r3[r0] = r4
            int r0 = r12.f17561
            int r0 = r0 + 8
            r12.f17561 = r0
            int r0 = r8.lineLength
            if (r0 <= 0) goto L_0x033a
            int r0 = r8.lineLength
            int r4 = r12.f17561
            if (r0 > r4) goto L_0x033a
            byte[] r0 = r8.lineSeparator
            r4 = 0
            int r5 = r12.f17565
            byte[] r6 = r8.lineSeparator
            int r6 = r6.length
            java.lang.System.arraycopy(r0, r4, r3, r5, r6)
            int r0 = r12.f17565
            byte[] r3 = r8.lineSeparator
            int r3 = r3.length
            int r0 = r0 + r3
            r12.f17565 = r0
            r0 = 0
            r12.f17561 = r0
        L_0x033a:
            int r0 = r1 + 1
            r1 = r0
            r10 = r2
            goto L_0x0253
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.codec.binary.Base32.encode(byte[], int, int, org2.apache.commons.codec.binary.BaseNCodec$Context):void");
    }

    public boolean isInAlphabet(byte b) {
        return b >= 0 && b < this.decodeTable.length && this.decodeTable[b] != -1;
    }
}
