package org2.apache.commons.codec.binary;

import java.util.Arrays;
import org2.apache.commons.codec.BinaryDecoder;
import org2.apache.commons.codec.BinaryEncoder;
import org2.apache.commons.codec.DecoderException;
import org2.apache.commons.codec.EncoderException;

public abstract class BaseNCodec implements BinaryDecoder, BinaryEncoder {
    private static final int DEFAULT_BUFFER_RESIZE_FACTOR = 2;
    private static final int DEFAULT_BUFFER_SIZE = 8192;
    static final int EOF = -1;
    protected static final int MASK_8BITS = 255;
    public static final int MIME_CHUNK_SIZE = 76;
    protected static final byte PAD_DEFAULT = 61;
    public static final int PEM_CHUNK_SIZE = 64;
    protected final byte PAD = PAD_DEFAULT;
    private final int chunkSeparatorLength;
    private final int encodedBlockSize;
    protected final int lineLength;
    private final int unencodedBlockSize;

    static class Context {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f17560;

        /* renamed from: ʼ  reason: contains not printable characters */
        int f17561;

        /* renamed from: ʽ  reason: contains not printable characters */
        int f17562;

        /* renamed from: 连任  reason: contains not printable characters */
        int f17563;

        /* renamed from: 靐  reason: contains not printable characters */
        long f17564;

        /* renamed from: 麤  reason: contains not printable characters */
        int f17565;

        /* renamed from: 齉  reason: contains not printable characters */
        byte[] f17566;

        /* renamed from: 龘  reason: contains not printable characters */
        int f17567;

        Context() {
        }

        public String toString() {
            return String.format("%s[buffer=%s, currentLinePos=%s, eof=%s, ibitWorkArea=%s, lbitWorkArea=%s, modulus=%s, pos=%s, readPos=%s]", new Object[]{getClass().getSimpleName(), Arrays.toString(this.f17566), Integer.valueOf(this.f17561), Boolean.valueOf(this.f17560), Integer.valueOf(this.f17567), Long.valueOf(this.f17564), Integer.valueOf(this.f17562), Integer.valueOf(this.f17565), Integer.valueOf(this.f17563)});
        }
    }

    protected BaseNCodec(int i, int i2, int i3, int i4) {
        int i5 = 0;
        this.unencodedBlockSize = i;
        this.encodedBlockSize = i2;
        this.lineLength = i3 > 0 && i4 > 0 ? (i3 / i2) * i2 : i5;
        this.chunkSeparatorLength = i4;
    }

    protected static boolean isWhiteSpace(byte b) {
        switch (b) {
            case 9:
            case 10:
            case 13:
            case 32:
                return true;
            default:
                return false;
        }
    }

    private byte[] resizeBuffer(Context context) {
        if (context.f17566 == null) {
            context.f17566 = new byte[getDefaultBufferSize()];
            context.f17565 = 0;
            context.f17563 = 0;
        } else {
            byte[] bArr = new byte[(context.f17566.length * 2)];
            System.arraycopy(context.f17566, 0, bArr, 0, context.f17566.length);
            context.f17566 = bArr;
        }
        return context.f17566;
    }

    /* access modifiers changed from: package-private */
    public int available(Context context) {
        if (context.f17566 != null) {
            return context.f17565 - context.f17563;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public boolean containsAlphabetOrPad(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        for (byte b : bArr) {
            if (61 == b || isInAlphabet(b)) {
                return true;
            }
        }
        return false;
    }

    public Object decode(Object obj) throws DecoderException {
        if (obj instanceof byte[]) {
            return decode((byte[]) obj);
        }
        if (obj instanceof String) {
            return decode((String) obj);
        }
        throw new DecoderException("Parameter supplied to Base-N decode is not a byte[] or a String");
    }

    /* access modifiers changed from: package-private */
    public abstract void decode(byte[] bArr, int i, int i2, Context context);

    public byte[] decode(String str) {
        return decode(StringUtils.getBytesUtf8(str));
    }

    public byte[] decode(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        Context context = new Context();
        decode(bArr, 0, bArr.length, context);
        decode(bArr, 0, -1, context);
        byte[] bArr2 = new byte[context.f17565];
        readResults(bArr2, 0, bArr2.length, context);
        return bArr2;
    }

    public Object encode(Object obj) throws EncoderException {
        if (obj instanceof byte[]) {
            return encode((byte[]) obj);
        }
        throw new EncoderException("Parameter supplied to Base-N encode is not a byte[]");
    }

    /* access modifiers changed from: package-private */
    public abstract void encode(byte[] bArr, int i, int i2, Context context);

    public byte[] encode(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        Context context = new Context();
        encode(bArr, 0, bArr.length, context);
        encode(bArr, 0, -1, context);
        byte[] bArr2 = new byte[(context.f17565 - context.f17563)];
        readResults(bArr2, 0, bArr2.length, context);
        return bArr2;
    }

    public String encodeAsString(byte[] bArr) {
        return StringUtils.newStringUtf8(encode(bArr));
    }

    public String encodeToString(byte[] bArr) {
        return StringUtils.newStringUtf8(encode(bArr));
    }

    /* access modifiers changed from: protected */
    public byte[] ensureBufferSize(int i, Context context) {
        return (context.f17566 == null || context.f17566.length < context.f17565 + i) ? resizeBuffer(context) : context.f17566;
    }

    /* access modifiers changed from: protected */
    public int getDefaultBufferSize() {
        return 8192;
    }

    public long getEncodedLength(byte[] bArr) {
        long length = ((long) (((bArr.length + this.unencodedBlockSize) - 1) / this.unencodedBlockSize)) * ((long) this.encodedBlockSize);
        return this.lineLength > 0 ? length + ((((((long) this.lineLength) + length) - 1) / ((long) this.lineLength)) * ((long) this.chunkSeparatorLength)) : length;
    }

    /* access modifiers changed from: package-private */
    public boolean hasData(Context context) {
        return context.f17566 != null;
    }

    /* access modifiers changed from: protected */
    public abstract boolean isInAlphabet(byte b);

    public boolean isInAlphabet(String str) {
        return isInAlphabet(StringUtils.getBytesUtf8(str), true);
    }

    public boolean isInAlphabet(byte[] bArr, boolean z) {
        for (int i = 0; i < bArr.length; i++) {
            if (!isInAlphabet(bArr[i])) {
                if (!z) {
                    return false;
                }
                if (bArr[i] != 61 && !isWhiteSpace(bArr[i])) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public int readResults(byte[] bArr, int i, int i2, Context context) {
        if (context.f17566 == null) {
            return context.f17560 ? -1 : 0;
        }
        int min = Math.min(available(context), i2);
        System.arraycopy(context.f17566, context.f17563, bArr, i, min);
        context.f17563 += min;
        if (context.f17563 < context.f17565) {
            return min;
        }
        context.f17566 = null;
        return min;
    }
}
