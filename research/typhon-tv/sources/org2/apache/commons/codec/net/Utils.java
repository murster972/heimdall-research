package org2.apache.commons.codec.net;

import org2.apache.commons.codec.DecoderException;

class Utils {
    /* renamed from: 龘  reason: contains not printable characters */
    static int m22479(byte b) throws DecoderException {
        int digit = Character.digit((char) b, 16);
        if (digit != -1) {
            return digit;
        }
        throw new DecoderException("Invalid URL encoding: not a valid digit (radix 16): " + b);
    }
}
