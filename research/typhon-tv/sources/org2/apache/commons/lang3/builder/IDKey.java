package org2.apache.commons.lang3.builder;

final class IDKey {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f17623;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Object f17624;

    public IDKey(Object obj) {
        this.f17623 = System.identityHashCode(obj);
        this.f17624 = obj;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof IDKey)) {
            return false;
        }
        IDKey iDKey = (IDKey) obj;
        return this.f17623 == iDKey.f17623 && this.f17624 == iDKey.f17624;
    }

    public int hashCode() {
        return this.f17623;
    }
}
