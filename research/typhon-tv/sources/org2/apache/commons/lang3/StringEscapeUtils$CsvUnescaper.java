package org2.apache.commons.lang3;

import java.io.IOException;
import java.io.Writer;
import org.apache.commons.lang3.CharUtils;
import org2.apache.commons.lang3.text.translate.CharSequenceTranslator;

class StringEscapeUtils$CsvUnescaper extends CharSequenceTranslator {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final char[] f17621 = {',', '\"', CharUtils.CR, 10};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f17622 = String.valueOf('\"');

    StringEscapeUtils$CsvUnescaper() {
    }

    public int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
        if (i != 0) {
            throw new IllegalStateException("CsvUnescaper should never reach the [1] index");
        } else if (charSequence.charAt(0) == '\"' && charSequence.charAt(charSequence.length() - 1) == '\"') {
            String charSequence2 = charSequence.subSequence(1, charSequence.length() - 1).toString();
            if (StringUtils.containsAny(charSequence2, f17621)) {
                writer.write(StringUtils.replace(charSequence2, f17622 + f17622, f17622));
            } else {
                writer.write(charSequence.toString());
            }
            return Character.codePointCount(charSequence, 0, charSequence.length());
        } else {
            writer.write(charSequence.toString());
            return Character.codePointCount(charSequence, 0, charSequence.length());
        }
    }
}
