package org2.apache.commons.lang3;

import java.io.IOException;
import java.io.Writer;
import org.apache.commons.lang3.CharUtils;
import org2.apache.commons.lang3.text.translate.CharSequenceTranslator;

class StringEscapeUtils$CsvEscaper extends CharSequenceTranslator {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final char[] f17619 = {',', '\"', CharUtils.CR, 10};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String f17620 = String.valueOf('\"');

    StringEscapeUtils$CsvEscaper() {
    }

    public int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
        if (i != 0) {
            throw new IllegalStateException("CsvEscaper should never reach the [1] index");
        }
        if (StringUtils.containsNone(charSequence.toString(), f17619)) {
            writer.write(charSequence.toString());
        } else {
            writer.write(34);
            writer.write(StringUtils.replace(charSequence.toString(), f17620, f17620 + f17620));
            writer.write(34);
        }
        return Character.codePointCount(charSequence, 0, charSequence.length());
    }
}
