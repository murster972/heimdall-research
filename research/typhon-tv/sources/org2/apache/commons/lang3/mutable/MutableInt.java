package org2.apache.commons.lang3.mutable;

import org2.apache.commons.lang3.math.NumberUtils;

public class MutableInt extends Number implements Comparable<MutableInt>, Mutable<Number> {
    private static final long serialVersionUID = 512176391864L;
    private int value;

    public MutableInt() {
    }

    public MutableInt(int i) {
        this.value = i;
    }

    public MutableInt(Number number) {
        this.value = number.intValue();
    }

    public MutableInt(String str) throws NumberFormatException {
        this.value = Integer.parseInt(str);
    }

    public void add(int i) {
        this.value += i;
    }

    public void add(Number number) {
        this.value += number.intValue();
    }

    public int addAndGet(int i) {
        this.value += i;
        return this.value;
    }

    public int addAndGet(Number number) {
        this.value += number.intValue();
        return this.value;
    }

    public int compareTo(MutableInt mutableInt) {
        return NumberUtils.compare(this.value, mutableInt.value);
    }

    public void decrement() {
        this.value--;
    }

    public int decrementAndGet() {
        this.value--;
        return this.value;
    }

    public double doubleValue() {
        return (double) this.value;
    }

    public boolean equals(Object obj) {
        return (obj instanceof MutableInt) && this.value == ((MutableInt) obj).intValue();
    }

    public float floatValue() {
        return (float) this.value;
    }

    public int getAndAdd(int i) {
        int i2 = this.value;
        this.value += i;
        return i2;
    }

    public int getAndAdd(Number number) {
        int i = this.value;
        this.value += number.intValue();
        return i;
    }

    public int getAndDecrement() {
        this.value--;
        return this.value;
    }

    public int getAndIncrement() {
        int i = this.value;
        this.value++;
        return i;
    }

    public Integer getValue() {
        return Integer.valueOf(this.value);
    }

    public int hashCode() {
        return this.value;
    }

    public void increment() {
        this.value++;
    }

    public int incrementAndGet() {
        this.value++;
        return this.value;
    }

    public int intValue() {
        return this.value;
    }

    public long longValue() {
        return (long) this.value;
    }

    public void setValue(int i) {
        this.value = i;
    }

    public void setValue(Number number) {
        this.value = number.intValue();
    }

    public void subtract(int i) {
        this.value -= i;
    }

    public void subtract(Number number) {
        this.value -= number.intValue();
    }

    public Integer toInteger() {
        return Integer.valueOf(intValue());
    }

    public String toString() {
        return String.valueOf(this.value);
    }
}
