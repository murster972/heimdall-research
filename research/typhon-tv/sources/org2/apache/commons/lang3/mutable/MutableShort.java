package org2.apache.commons.lang3.mutable;

import org2.apache.commons.lang3.math.NumberUtils;

public class MutableShort extends Number implements Comparable<MutableShort>, Mutable<Number> {
    private static final long serialVersionUID = -2135791679;
    private short value;

    public MutableShort() {
    }

    public MutableShort(Number number) {
        this.value = number.shortValue();
    }

    public MutableShort(String str) throws NumberFormatException {
        this.value = Short.parseShort(str);
    }

    public MutableShort(short s) {
        this.value = s;
    }

    public void add(Number number) {
        this.value = (short) (this.value + number.shortValue());
    }

    public void add(short s) {
        this.value = (short) (this.value + s);
    }

    public short addAndGet(Number number) {
        this.value = (short) (this.value + number.shortValue());
        return this.value;
    }

    public short addAndGet(short s) {
        this.value = (short) (this.value + s);
        return this.value;
    }

    public int compareTo(MutableShort mutableShort) {
        return NumberUtils.compare(this.value, mutableShort.value);
    }

    public void decrement() {
        this.value = (short) (this.value - 1);
    }

    public short decrementAndGet() {
        this.value = (short) (this.value - 1);
        return this.value;
    }

    public double doubleValue() {
        return (double) this.value;
    }

    public boolean equals(Object obj) {
        return (obj instanceof MutableShort) && this.value == ((MutableShort) obj).shortValue();
    }

    public float floatValue() {
        return (float) this.value;
    }

    public short getAndAdd(Number number) {
        short s = this.value;
        this.value = (short) (this.value + number.shortValue());
        return s;
    }

    public short getAndAdd(short s) {
        short s2 = this.value;
        this.value = (short) (this.value + s);
        return s2;
    }

    public short getAndDecrement() {
        short s = this.value;
        this.value = (short) (this.value - 1);
        return s;
    }

    public short getAndIncrement() {
        short s = this.value;
        this.value = (short) (this.value + 1);
        return s;
    }

    public Short getValue() {
        return Short.valueOf(this.value);
    }

    public int hashCode() {
        return this.value;
    }

    public void increment() {
        this.value = (short) (this.value + 1);
    }

    public short incrementAndGet() {
        this.value = (short) (this.value + 1);
        return this.value;
    }

    public int intValue() {
        return this.value;
    }

    public long longValue() {
        return (long) this.value;
    }

    public void setValue(Number number) {
        this.value = number.shortValue();
    }

    public void setValue(short s) {
        this.value = s;
    }

    public short shortValue() {
        return this.value;
    }

    public void subtract(Number number) {
        this.value = (short) (this.value - number.shortValue());
    }

    public void subtract(short s) {
        this.value = (short) (this.value - s);
    }

    public Short toShort() {
        return Short.valueOf(shortValue());
    }

    public String toString() {
        return String.valueOf(this.value);
    }
}
