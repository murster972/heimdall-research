package org2.apache.commons.lang3.concurrent;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org2.apache.commons.lang3.concurrent.AbstractCircuitBreaker;

public class EventCountCircuitBreaker extends AbstractCircuitBreaker<Integer> {
    private static final Map<AbstractCircuitBreaker.State, StateStrategy> STRATEGY_MAP = createStrategyMap();
    private final AtomicReference<CheckIntervalData> checkIntervalData;
    private final long closingInterval;
    private final int closingThreshold;
    private final long openingInterval;
    private final int openingThreshold;

    private static class CheckIntervalData {

        /* renamed from: 靐  reason: contains not printable characters */
        private final long f17628;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f17629;

        public CheckIntervalData(int i, long j) {
            this.f17629 = i;
            this.f17628 = j;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m22534() {
            return this.f17628;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m22535() {
            return this.f17629;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public CheckIntervalData m22536(int i) {
            return i != 0 ? new CheckIntervalData(m22535() + i, m22534()) : this;
        }
    }

    private static abstract class StateStrategy {
        private StateStrategy() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract long m22537(EventCountCircuitBreaker eventCountCircuitBreaker);

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22538(EventCountCircuitBreaker eventCountCircuitBreaker, CheckIntervalData checkIntervalData, long j) {
            return j - checkIntervalData.m22534() > m22537(eventCountCircuitBreaker);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public abstract boolean m22539(EventCountCircuitBreaker eventCountCircuitBreaker, CheckIntervalData checkIntervalData, CheckIntervalData checkIntervalData2);
    }

    private static class StateStrategyClosed extends StateStrategy {
        private StateStrategyClosed() {
            super();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m22540(EventCountCircuitBreaker eventCountCircuitBreaker) {
            return eventCountCircuitBreaker.getOpeningInterval();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22541(EventCountCircuitBreaker eventCountCircuitBreaker, CheckIntervalData checkIntervalData, CheckIntervalData checkIntervalData2) {
            return checkIntervalData2.m22535() > eventCountCircuitBreaker.getOpeningThreshold();
        }
    }

    private static class StateStrategyOpen extends StateStrategy {
        private StateStrategyOpen() {
            super();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public long m22542(EventCountCircuitBreaker eventCountCircuitBreaker) {
            return eventCountCircuitBreaker.getClosingInterval();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22543(EventCountCircuitBreaker eventCountCircuitBreaker, CheckIntervalData checkIntervalData, CheckIntervalData checkIntervalData2) {
            return checkIntervalData2.m22534() != checkIntervalData.m22534() && checkIntervalData.m22535() < eventCountCircuitBreaker.getClosingThreshold();
        }
    }

    public EventCountCircuitBreaker(int i, long j, TimeUnit timeUnit) {
        this(i, j, timeUnit, i);
    }

    public EventCountCircuitBreaker(int i, long j, TimeUnit timeUnit, int i2) {
        this(i, j, timeUnit, i2, j, timeUnit);
    }

    public EventCountCircuitBreaker(int i, long j, TimeUnit timeUnit, int i2, long j2, TimeUnit timeUnit2) {
        this.checkIntervalData = new AtomicReference<>(new CheckIntervalData(0, 0));
        this.openingThreshold = i;
        this.openingInterval = timeUnit.toNanos(j);
        this.closingThreshold = i2;
        this.closingInterval = timeUnit2.toNanos(j2);
    }

    private void changeStateAndStartNewCheckInterval(AbstractCircuitBreaker.State state) {
        changeState(state);
        this.checkIntervalData.set(new CheckIntervalData(0, now()));
    }

    private static Map<AbstractCircuitBreaker.State, StateStrategy> createStrategyMap() {
        EnumMap enumMap = new EnumMap(AbstractCircuitBreaker.State.class);
        enumMap.put(AbstractCircuitBreaker.State.CLOSED, new StateStrategyClosed());
        enumMap.put(AbstractCircuitBreaker.State.OPEN, new StateStrategyOpen());
        return enumMap;
    }

    private CheckIntervalData nextCheckIntervalData(int i, CheckIntervalData checkIntervalData2, AbstractCircuitBreaker.State state, long j) {
        return stateStrategy(state).m22538(this, checkIntervalData2, j) ? new CheckIntervalData(i, j) : checkIntervalData2.m22536(i);
    }

    private boolean performStateCheck(int i) {
        AbstractCircuitBreaker.State state;
        CheckIntervalData checkIntervalData2;
        CheckIntervalData nextCheckIntervalData;
        do {
            long now = now();
            state = (AbstractCircuitBreaker.State) this.state.get();
            checkIntervalData2 = this.checkIntervalData.get();
            nextCheckIntervalData = nextCheckIntervalData(i, checkIntervalData2, state, now);
        } while (!updateCheckIntervalData(checkIntervalData2, nextCheckIntervalData));
        if (stateStrategy(state).m22539(this, checkIntervalData2, nextCheckIntervalData)) {
            state = state.oppositeState();
            changeStateAndStartNewCheckInterval(state);
        }
        return !isOpen(state);
    }

    private static StateStrategy stateStrategy(AbstractCircuitBreaker.State state) {
        return STRATEGY_MAP.get(state);
    }

    private boolean updateCheckIntervalData(CheckIntervalData checkIntervalData2, CheckIntervalData checkIntervalData3) {
        return checkIntervalData2 == checkIntervalData3 || this.checkIntervalData.compareAndSet(checkIntervalData2, checkIntervalData3);
    }

    public boolean checkState() {
        return performStateCheck(0);
    }

    public void close() {
        super.close();
        this.checkIntervalData.set(new CheckIntervalData(0, now()));
    }

    public long getClosingInterval() {
        return this.closingInterval;
    }

    public int getClosingThreshold() {
        return this.closingThreshold;
    }

    public long getOpeningInterval() {
        return this.openingInterval;
    }

    public int getOpeningThreshold() {
        return this.openingThreshold;
    }

    public boolean incrementAndCheckState() {
        return incrementAndCheckState((Integer) 1);
    }

    public boolean incrementAndCheckState(Integer num) throws CircuitBreakingException {
        return performStateCheck(1);
    }

    /* access modifiers changed from: package-private */
    public long now() {
        return System.nanoTime();
    }

    public void open() {
        super.open();
        this.checkIntervalData.set(new CheckIntervalData(0, now()));
    }
}
