package org2.apache.commons.lang3.event;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org2.apache.commons.lang3.reflect.MethodUtils;

public class EventUtils {

    private static class EventBindingInvocationHandler implements InvocationHandler {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f17631;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Set<String> f17632;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f17633;

        EventBindingInvocationHandler(Object obj, String str, String[] strArr) {
            this.f17633 = obj;
            this.f17631 = str;
            this.f17632 = new HashSet(Arrays.asList(strArr));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m22544(Method method) {
            return MethodUtils.getAccessibleMethod(this.f17633.getClass(), this.f17631, method.getParameterTypes()) != null;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            if (this.f17632.isEmpty() || this.f17632.contains(method.getName())) {
                return m22544(method) ? MethodUtils.invokeMethod(this.f17633, this.f17631, objArr) : MethodUtils.invokeMethod(this.f17633, this.f17631);
            }
            return null;
        }
    }

    public static <L> void addEventListener(Object obj, Class<L> cls, L l) {
        try {
            MethodUtils.invokeMethod(obj, "add" + cls.getSimpleName(), l);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Class " + obj.getClass().getName() + " does not have a public add" + cls.getSimpleName() + " method which takes a parameter of type " + cls.getName() + ".");
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("Class " + obj.getClass().getName() + " does not have an accessible add" + cls.getSimpleName() + " method which takes a parameter of type " + cls.getName() + ".");
        } catch (InvocationTargetException e3) {
            throw new RuntimeException("Unable to add listener.", e3.getCause());
        }
    }

    public static <L> void bindEventsToMethod(Object obj, String str, Object obj2, Class<L> cls, String... strArr) {
        addEventListener(obj2, cls, cls.cast(Proxy.newProxyInstance(obj.getClass().getClassLoader(), new Class[]{cls}, new EventBindingInvocationHandler(obj, str, strArr))));
    }
}
