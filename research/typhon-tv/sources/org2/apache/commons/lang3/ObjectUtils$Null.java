package org2.apache.commons.lang3;

import java.io.Serializable;

public class ObjectUtils$Null implements Serializable {
    private static final long serialVersionUID = 7092611880189329093L;

    ObjectUtils$Null() {
    }

    private Object readResolve() {
        return ObjectUtils.NULL;
    }
}
