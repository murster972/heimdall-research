package org2.apache.commons.lang3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeRequest;

public class SerializationUtils {

    static class ClassLoaderAwareObjectInputStream extends ObjectInputStream {

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Map<String, Class<?>> f17617 = new HashMap();

        /* renamed from: 靐  reason: contains not printable characters */
        private final ClassLoader f17618;

        static {
            f17617.put("byte", Byte.TYPE);
            f17617.put("short", Short.TYPE);
            f17617.put("int", Integer.TYPE);
            f17617.put(PubnativeRequest.Parameters.LONG, Long.TYPE);
            f17617.put("float", Float.TYPE);
            f17617.put("double", Double.TYPE);
            f17617.put("boolean", Boolean.TYPE);
            f17617.put("char", Character.TYPE);
            f17617.put("void", Void.TYPE);
        }

        public ClassLoaderAwareObjectInputStream(InputStream inputStream, ClassLoader classLoader) throws IOException {
            super(inputStream);
            this.f17618 = classLoader;
        }

        /* access modifiers changed from: protected */
        public Class<?> resolveClass(ObjectStreamClass objectStreamClass) throws IOException, ClassNotFoundException {
            String name = objectStreamClass.getName();
            try {
                return Class.forName(name, false, this.f17618);
            } catch (ClassNotFoundException e) {
                try {
                    return Class.forName(name, false, Thread.currentThread().getContextClassLoader());
                } catch (ClassNotFoundException e2) {
                    ClassNotFoundException classNotFoundException = e2;
                    Class<?> cls = f17617.get(name);
                    if (cls != null) {
                        return cls;
                    }
                    throw classNotFoundException;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0040 A[SYNTHETIC, Splitter:B:21:0x0040] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0034=Splitter:B:16:0x0034, B:26:0x0048=Splitter:B:26:0x0048} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T extends java.io.Serializable> T clone(T r5) {
        /*
            r0 = 0
            if (r5 != 0) goto L_0x0004
        L_0x0003:
            return r0
        L_0x0004:
            byte[] r1 = serialize(r5)
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            r2.<init>(r1)
            org2.apache.commons.lang3.SerializationUtils$ClassLoaderAwareObjectInputStream r1 = new org2.apache.commons.lang3.SerializationUtils$ClassLoaderAwareObjectInputStream     // Catch:{ ClassNotFoundException -> 0x0030, IOException -> 0x0044, all -> 0x005b }
            java.lang.Class r3 = r5.getClass()     // Catch:{ ClassNotFoundException -> 0x0030, IOException -> 0x0044, all -> 0x005b }
            java.lang.ClassLoader r3 = r3.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x0030, IOException -> 0x0044, all -> 0x005b }
            r1.<init>(r2, r3)     // Catch:{ ClassNotFoundException -> 0x0030, IOException -> 0x0044, all -> 0x005b }
            java.lang.Object r0 = r1.readObject()     // Catch:{ ClassNotFoundException -> 0x0062, IOException -> 0x0060 }
            java.io.Serializable r0 = (java.io.Serializable) r0     // Catch:{ ClassNotFoundException -> 0x0062, IOException -> 0x0060 }
            if (r1 == 0) goto L_0x0003
            r1.close()     // Catch:{ IOException -> 0x0026 }
            goto L_0x0003
        L_0x0026:
            r0 = move-exception
            org2.apache.commons.lang3.SerializationException r1 = new org2.apache.commons.lang3.SerializationException
            java.lang.String r2 = "IOException on closing cloned object data InputStream."
            r1.<init>(r2, r0)
            throw r1
        L_0x0030:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0034:
            org2.apache.commons.lang3.SerializationException r2 = new org2.apache.commons.lang3.SerializationException     // Catch:{ all -> 0x003d }
            java.lang.String r3 = "ClassNotFoundException while reading cloned object data"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x003d }
            throw r2     // Catch:{ all -> 0x003d }
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0043:
            throw r0
        L_0x0044:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0048:
            org2.apache.commons.lang3.SerializationException r2 = new org2.apache.commons.lang3.SerializationException     // Catch:{ all -> 0x003d }
            java.lang.String r3 = "IOException while reading cloned object data"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x003d }
            throw r2     // Catch:{ all -> 0x003d }
        L_0x0051:
            r0 = move-exception
            org2.apache.commons.lang3.SerializationException r1 = new org2.apache.commons.lang3.SerializationException
            java.lang.String r2 = "IOException on closing cloned object data InputStream."
            r1.<init>(r2, r0)
            throw r1
        L_0x005b:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x003e
        L_0x0060:
            r0 = move-exception
            goto L_0x0048
        L_0x0062:
            r0 = move-exception
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.lang3.SerializationUtils.clone(java.io.Serializable):java.io.Serializable");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0027 A[SYNTHETIC, Splitter:B:20:0x0027] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> T deserialize(java.io.InputStream r3) {
        /*
            if (r3 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "The InputStream must not be null"
            r0.<init>(r1)
            throw r0
        L_0x000b:
            r2 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ ClassNotFoundException -> 0x001b, IOException -> 0x002b }
            r1.<init>(r3)     // Catch:{ ClassNotFoundException -> 0x001b, IOException -> 0x002b }
            java.lang.Object r0 = r1.readObject()     // Catch:{ ClassNotFoundException -> 0x003b, IOException -> 0x0038 }
            if (r1 == 0) goto L_0x001a
            r1.close()     // Catch:{ IOException -> 0x0034 }
        L_0x001a:
            return r0
        L_0x001b:
            r0 = move-exception
            r1 = r2
        L_0x001d:
            org2.apache.commons.lang3.SerializationException r2 = new org2.apache.commons.lang3.SerializationException     // Catch:{ all -> 0x0023 }
            r2.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x0023 }
            throw r2     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            r2 = r1
        L_0x0025:
            if (r2 == 0) goto L_0x002a
            r2.close()     // Catch:{ IOException -> 0x0036 }
        L_0x002a:
            throw r0
        L_0x002b:
            r0 = move-exception
        L_0x002c:
            org2.apache.commons.lang3.SerializationException r1 = new org2.apache.commons.lang3.SerializationException     // Catch:{ all -> 0x0032 }
            r1.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x0032 }
            throw r1     // Catch:{ all -> 0x0032 }
        L_0x0032:
            r0 = move-exception
            goto L_0x0025
        L_0x0034:
            r1 = move-exception
            goto L_0x001a
        L_0x0036:
            r1 = move-exception
            goto L_0x002a
        L_0x0038:
            r0 = move-exception
            r2 = r1
            goto L_0x002c
        L_0x003b:
            r0 = move-exception
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.lang3.SerializationUtils.deserialize(java.io.InputStream):java.lang.Object");
    }

    public static <T> T deserialize(byte[] bArr) {
        if (bArr != null) {
            return deserialize((InputStream) new ByteArrayInputStream(bArr));
        }
        throw new IllegalArgumentException("The byte[] must not be null");
    }

    public static <T extends Serializable> T roundtrip(T t) {
        return (Serializable) deserialize(serialize(t));
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0025 A[SYNTHETIC, Splitter:B:18:0x0025] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void serialize(java.io.Serializable r3, java.io.OutputStream r4) {
        /*
            if (r4 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "The OutputStream must not be null"
            r0.<init>(r1)
            throw r0
        L_0x000b:
            r2 = 0
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x001a, all -> 0x002d }
            r1.<init>(r4)     // Catch:{ IOException -> 0x001a, all -> 0x002d }
            r1.writeObject(r3)     // Catch:{ IOException -> 0x0030 }
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0019:
            return
        L_0x001a:
            r0 = move-exception
            r1 = r2
        L_0x001c:
            org2.apache.commons.lang3.SerializationException r2 = new org2.apache.commons.lang3.SerializationException     // Catch:{ all -> 0x0022 }
            r2.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x0022 }
            throw r2     // Catch:{ all -> 0x0022 }
        L_0x0022:
            r0 = move-exception
        L_0x0023:
            if (r1 == 0) goto L_0x0028
            r1.close()     // Catch:{ IOException -> 0x002b }
        L_0x0028:
            throw r0
        L_0x0029:
            r0 = move-exception
            goto L_0x0019
        L_0x002b:
            r1 = move-exception
            goto L_0x0028
        L_0x002d:
            r0 = move-exception
            r1 = r2
            goto L_0x0023
        L_0x0030:
            r0 = move-exception
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.lang3.SerializationUtils.serialize(java.io.Serializable, java.io.OutputStream):void");
    }

    public static byte[] serialize(Serializable serializable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        serialize(serializable, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
