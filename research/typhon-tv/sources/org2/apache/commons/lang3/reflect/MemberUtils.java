package org2.apache.commons.lang3.reflect;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org2.apache.commons.lang3.ClassUtils;

abstract class MemberUtils {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Class<?>[] f17634 = {Byte.TYPE, Short.TYPE, Character.TYPE, Integer.TYPE, Long.TYPE, Float.TYPE, Double.TYPE};

    private static final class Executable {

        /* renamed from: 靐  reason: contains not printable characters */
        private final boolean f17635;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Class<?>[] f17636;

        private Executable(Constructor<?> constructor) {
            this.f17636 = constructor.getParameterTypes();
            this.f17635 = constructor.isVarArgs();
        }

        private Executable(Method method) {
            this.f17636 = method.getParameterTypes();
            this.f17635 = method.isVarArgs();
        }

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public static Executable m22557(Constructor<?> constructor) {
            return new Executable(constructor);
        }

        /* access modifiers changed from: private */
        /* renamed from: 靐  reason: contains not printable characters */
        public static Executable m22558(Method method) {
            return new Executable(method);
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m22561() {
            return this.f17635;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Class<?>[] m22562() {
            return this.f17636;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private static float m22545(Class<?> cls, Class<?> cls2) {
        float f = 0.0f;
        if (!cls.isPrimitive()) {
            f = 0.0f + 0.1f;
            cls = ClassUtils.wrapperToPrimitive(cls);
        }
        int i = 0;
        float f2 = f;
        Class<?> cls3 = cls;
        while (cls3 != cls2 && i < f17634.length) {
            if (cls3 == f17634[i]) {
                f2 += 0.1f;
                if (i < f17634.length - 1) {
                    cls3 = f17634[i + 1];
                }
            }
            i++;
        }
        return f2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static float m22546(Class<?> cls, Class<?> cls2) {
        if (cls2.isPrimitive()) {
            return m22545(cls, cls2);
        }
        float f = 0.0f;
        Class<? super Object> cls3 = cls;
        while (true) {
            if (cls3 != null && !cls2.equals(cls3)) {
                if (cls2.isInterface() && ClassUtils.isAssignable(cls3, cls2)) {
                    f += 0.25f;
                    break;
                }
                f += 1.0f;
                cls3 = cls3.getSuperclass();
            } else {
                break;
            }
        }
        return cls3 == null ? f + 1.5f : f;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static float m22547(Class<?>[] clsArr, Executable executable) {
        boolean z = true;
        Class[] r7 = executable.m22562();
        boolean r8 = executable.m22561();
        float f = 0.0f;
        long length = r8 ? (long) (r7.length - 1) : (long) r7.length;
        if (((long) clsArr.length) < length) {
            return Float.MAX_VALUE;
        }
        int i = 0;
        while (((long) i) < length) {
            float r6 = m22546(clsArr[i], (Class<?>) r7[i]) + f;
            i++;
            f = r6;
        }
        if (!r8) {
            return f;
        }
        boolean z2 = clsArr.length < r7.length;
        if (clsArr.length != r7.length || !clsArr[clsArr.length - 1].isArray()) {
            z = false;
        }
        Class<?> componentType = r7[r7.length - 1].getComponentType();
        if (z2) {
            return f + m22546(componentType, (Class<?>) Object.class) + 0.001f;
        }
        if (z) {
            return f + m22546(clsArr[clsArr.length - 1].getComponentType(), componentType) + 0.001f;
        }
        float f2 = f;
        for (int length2 = r7.length - 1; length2 < clsArr.length; length2++) {
            f2 += m22546(clsArr[length2], componentType) + 0.001f;
        }
        return f2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m22548(Constructor<?> constructor, Constructor<?> constructor2, Class<?>[] clsArr) {
        return m22550(Executable.m22557(constructor), Executable.m22557(constructor2), clsArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static int m22549(Method method, Method method2, Class<?>[] clsArr) {
        return m22550(Executable.m22558(method), Executable.m22558(method2), clsArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static int m22550(Executable executable, Executable executable2, Class<?>[] clsArr) {
        float r0 = m22547(clsArr, executable);
        float r1 = m22547(clsArr, executable2);
        if (r0 < r1) {
            return -1;
        }
        return r1 < r0 ? 1 : 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m22551(int i) {
        return (i & 7) == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m22552(AccessibleObject accessibleObject) {
        if (accessibleObject == null || accessibleObject.isAccessible()) {
            return false;
        }
        Member member = (Member) accessibleObject;
        if (!accessibleObject.isAccessible() && Modifier.isPublic(member.getModifiers()) && m22551(member.getDeclaringClass().getModifiers())) {
            try {
                accessibleObject.setAccessible(true);
                return true;
            } catch (SecurityException e) {
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m22553(Constructor<?> constructor, Class<?>[] clsArr) {
        return m22556(Executable.m22557(constructor), clsArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m22554(Member member) {
        return member != null && Modifier.isPublic(member.getModifiers()) && !member.isSynthetic();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m22555(Method method, Class<?>[] clsArr) {
        return m22556(Executable.m22558(method), clsArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m22556(Executable executable, Class<?>[] clsArr) {
        Class[] r3 = executable.m22562();
        if (!executable.m22561()) {
            return ClassUtils.isAssignable(clsArr, (Class<?>[]) r3, true);
        }
        int i = 0;
        while (i < r3.length - 1 && i < clsArr.length) {
            if (!ClassUtils.isAssignable(clsArr[i], (Class<?>) r3[i], true)) {
                return false;
            }
            i++;
        }
        Class<?> componentType = r3[r3.length - 1].getComponentType();
        while (i < clsArr.length) {
            if (!ClassUtils.isAssignable(clsArr[i], componentType, true)) {
                return false;
            }
            i++;
        }
        return true;
    }
}
