package org2.apache.commons.lang3.time;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtils {
    public static final long MILLIS_PER_DAY = 86400000;
    public static final long MILLIS_PER_HOUR = 3600000;
    public static final long MILLIS_PER_MINUTE = 60000;
    public static final long MILLIS_PER_SECOND = 1000;
    public static final int RANGE_MONTH_MONDAY = 6;
    public static final int RANGE_MONTH_SUNDAY = 5;
    public static final int RANGE_WEEK_CENTER = 4;
    public static final int RANGE_WEEK_MONDAY = 2;
    public static final int RANGE_WEEK_RELATIVE = 3;
    public static final int RANGE_WEEK_SUNDAY = 1;
    public static final int SEMI_MONTH = 1001;
    private static final int[][] fields = {new int[]{14}, new int[]{13}, new int[]{12}, new int[]{11, 10}, new int[]{5, 5, 9}, new int[]{2, 1001}, new int[]{1}, new int[]{0}};

    static class DateIterator implements Iterator<Calendar> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Calendar f17656;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Calendar f17657;

        DateIterator(Calendar calendar, Calendar calendar2) {
            this.f17657 = calendar2;
            this.f17656 = calendar;
            this.f17656.add(5, -1);
        }

        public boolean hasNext() {
            return this.f17656.before(this.f17657);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Calendar next() {
            if (this.f17656.equals(this.f17657)) {
                throw new NoSuchElementException();
            }
            this.f17656.add(5, 1);
            return (Calendar) this.f17656.clone();
        }
    }

    private enum ModifyType {
        TRUNCATE,
        ROUND,
        CEILING
    }

    private static Date add(Date date, int i, int i2) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(i, i2);
        return instance.getTime();
    }

    public static Date addDays(Date date, int i) {
        return add(date, 5, i);
    }

    public static Date addHours(Date date, int i) {
        return add(date, 11, i);
    }

    public static Date addMilliseconds(Date date, int i) {
        return add(date, 14, i);
    }

    public static Date addMinutes(Date date, int i) {
        return add(date, 12, i);
    }

    public static Date addMonths(Date date, int i) {
        return add(date, 2, i);
    }

    public static Date addSeconds(Date date, int i) {
        return add(date, 13, i);
    }

    public static Date addWeeks(Date date, int i) {
        return add(date, 3, i);
    }

    public static Date addYears(Date date, int i) {
        return add(date, 1, i);
    }

    public static Calendar ceiling(Calendar calendar, int i) {
        if (calendar == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar calendar2 = (Calendar) calendar.clone();
        modify(calendar2, i, ModifyType.CEILING);
        return calendar2;
    }

    public static Date ceiling(Object obj, int i) {
        if (obj == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (obj instanceof Date) {
            return ceiling((Date) obj, i);
        } else {
            if (obj instanceof Calendar) {
                return ceiling((Calendar) obj, i).getTime();
            }
            throw new ClassCastException("Could not find ceiling of for type: " + obj.getClass());
        }
    }

    public static Date ceiling(Date date, int i) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        modify(instance, i, ModifyType.CEILING);
        return instance.getTime();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0069, code lost:
        r0 = r0 + r7.convert((long) r5.get(12), java.util.concurrent.TimeUnit.MINUTES);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0077, code lost:
        r0 = r0 + r7.convert((long) r5.get(13), java.util.concurrent.TimeUnit.SECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return r0 + r7.convert((long) r5.get(14), java.util.concurrent.TimeUnit.MILLISECONDS);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long getFragment(java.util.Calendar r5, int r6, java.util.concurrent.TimeUnit r7) {
        /*
            if (r5 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "The date must not be null"
            r0.<init>(r1)
            throw r0
        L_0x000b:
            r0 = 0
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.DAYS
            if (r7 != r2) goto L_0x0039
            r2 = 0
        L_0x0012:
            switch(r6) {
                case 1: goto L_0x003b;
                case 2: goto L_0x004b;
                default: goto L_0x0015;
            }
        L_0x0015:
            switch(r6) {
                case 1: goto L_0x005b;
                case 2: goto L_0x005b;
                case 3: goto L_0x0018;
                case 4: goto L_0x0018;
                case 5: goto L_0x005b;
                case 6: goto L_0x005b;
                case 7: goto L_0x0018;
                case 8: goto L_0x0018;
                case 9: goto L_0x0018;
                case 10: goto L_0x0018;
                case 11: goto L_0x0069;
                case 12: goto L_0x0077;
                case 13: goto L_0x0085;
                case 14: goto L_0x0093;
                default: goto L_0x0018;
            }
        L_0x0018:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "The fragment "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = " is not supported"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0039:
            r2 = 1
            goto L_0x0012
        L_0x003b:
            r3 = 6
            int r3 = r5.get(r3)
            int r2 = r3 - r2
            long r2 = (long) r2
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.DAYS
            long r2 = r7.convert(r2, r4)
            long r0 = r0 + r2
            goto L_0x0015
        L_0x004b:
            r3 = 5
            int r3 = r5.get(r3)
            int r2 = r3 - r2
            long r2 = (long) r2
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.DAYS
            long r2 = r7.convert(r2, r4)
            long r0 = r0 + r2
            goto L_0x0015
        L_0x005b:
            r2 = 11
            int r2 = r5.get(r2)
            long r2 = (long) r2
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.HOURS
            long r2 = r7.convert(r2, r4)
            long r0 = r0 + r2
        L_0x0069:
            r2 = 12
            int r2 = r5.get(r2)
            long r2 = (long) r2
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MINUTES
            long r2 = r7.convert(r2, r4)
            long r0 = r0 + r2
        L_0x0077:
            r2 = 13
            int r2 = r5.get(r2)
            long r2 = (long) r2
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS
            long r2 = r7.convert(r2, r4)
            long r0 = r0 + r2
        L_0x0085:
            r2 = 14
            int r2 = r5.get(r2)
            long r2 = (long) r2
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r2 = r7.convert(r2, r4)
            long r0 = r0 + r2
        L_0x0093:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.lang3.time.DateUtils.getFragment(java.util.Calendar, int, java.util.concurrent.TimeUnit):long");
    }

    private static long getFragment(Date date, int i, TimeUnit timeUnit) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return getFragment(instance, i, timeUnit);
    }

    public static long getFragmentInDays(Calendar calendar, int i) {
        return getFragment(calendar, i, TimeUnit.DAYS);
    }

    public static long getFragmentInDays(Date date, int i) {
        return getFragment(date, i, TimeUnit.DAYS);
    }

    public static long getFragmentInHours(Calendar calendar, int i) {
        return getFragment(calendar, i, TimeUnit.HOURS);
    }

    public static long getFragmentInHours(Date date, int i) {
        return getFragment(date, i, TimeUnit.HOURS);
    }

    public static long getFragmentInMilliseconds(Calendar calendar, int i) {
        return getFragment(calendar, i, TimeUnit.MILLISECONDS);
    }

    public static long getFragmentInMilliseconds(Date date, int i) {
        return getFragment(date, i, TimeUnit.MILLISECONDS);
    }

    public static long getFragmentInMinutes(Calendar calendar, int i) {
        return getFragment(calendar, i, TimeUnit.MINUTES);
    }

    public static long getFragmentInMinutes(Date date, int i) {
        return getFragment(date, i, TimeUnit.MINUTES);
    }

    public static long getFragmentInSeconds(Calendar calendar, int i) {
        return getFragment(calendar, i, TimeUnit.SECONDS);
    }

    public static long getFragmentInSeconds(Date date, int i) {
        return getFragment(date, i, TimeUnit.SECONDS);
    }

    public static boolean isSameDay(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            return calendar.get(0) == calendar2.get(0) && calendar.get(1) == calendar2.get(1) && calendar.get(6) == calendar2.get(6);
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameDay(Date date, Date date2) {
        if (date == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        return isSameDay(instance, instance2);
    }

    public static boolean isSameInstant(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            return calendar.getTime().getTime() == calendar2.getTime().getTime();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameInstant(Date date, Date date2) {
        if (date != null && date2 != null) {
            return date.getTime() == date2.getTime();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameLocalTime(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            return calendar.get(14) == calendar2.get(14) && calendar.get(13) == calendar2.get(13) && calendar.get(12) == calendar2.get(12) && calendar.get(11) == calendar2.get(11) && calendar.get(6) == calendar2.get(6) && calendar.get(1) == calendar2.get(1) && calendar.get(0) == calendar2.get(0) && calendar.getClass() == calendar2.getClass();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static Iterator<?> iterator(Object obj, int i) {
        if (obj == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (obj instanceof Date) {
            return iterator((Date) obj, i);
        } else {
            if (obj instanceof Calendar) {
                return iterator((Calendar) obj, i);
            }
            throw new ClassCastException("Could not iterate based on " + obj);
        }
    }

    public static Iterator<Calendar> iterator(Calendar calendar, int i) {
        Calendar truncate;
        Calendar truncate2;
        int i2;
        int i3 = 2;
        if (calendar == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
                truncate = truncate(calendar, 5);
                truncate2 = truncate(calendar, 5);
                switch (i) {
                    case 1:
                        i2 = 7;
                        i3 = 1;
                        break;
                    case 2:
                        i2 = 1;
                        break;
                    case 3:
                        i3 = calendar.get(7);
                        i2 = i3 - 1;
                        break;
                    case 4:
                        i3 = calendar.get(7) - 3;
                        i2 = calendar.get(7) + 3;
                        break;
                    default:
                        i2 = 7;
                        i3 = 1;
                        break;
                }
            case 5:
            case 6:
                Calendar truncate3 = truncate(calendar, 2);
                Calendar calendar2 = (Calendar) truncate3.clone();
                calendar2.add(2, 1);
                calendar2.add(5, -1);
                if (i != 6) {
                    i3 = 1;
                    truncate = truncate3;
                    truncate2 = calendar2;
                    i2 = 7;
                    break;
                } else {
                    truncate = truncate3;
                    truncate2 = calendar2;
                    i2 = 1;
                    break;
                }
            default:
                throw new IllegalArgumentException("The range style " + i + " is not valid.");
        }
        if (i3 < 1) {
            i3 += 7;
        }
        if (i3 > 7) {
            i3 -= 7;
        }
        if (i2 < 1) {
            i2 += 7;
        }
        if (i2 > 7) {
            i2 -= 7;
        }
        while (truncate.get(7) != i3) {
            truncate.add(5, -1);
        }
        while (truncate2.get(7) != i2) {
            truncate2.add(5, 1);
        }
        return new DateIterator(truncate, truncate2);
    }

    public static Iterator<Calendar> iterator(Date date, int i) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return iterator(instance, i);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void modify(java.util.Calendar r11, int r12, org2.apache.commons.lang3.time.DateUtils.ModifyType r13) {
        /*
            r0 = 1
            int r0 = r11.get(r0)
            r1 = 280000000(0x10b07600, float:6.960157E-29)
            if (r0 <= r1) goto L_0x0013
            java.lang.ArithmeticException r0 = new java.lang.ArithmeticException
            java.lang.String r1 = "Calendar value too large for accurate calculations"
            r0.<init>(r1)
            throw r0
        L_0x0013:
            r0 = 14
            if (r12 != r0) goto L_0x0018
        L_0x0017:
            return
        L_0x0018:
            java.util.Date r4 = r11.getTime()
            long r2 = r4.getTime()
            r0 = 0
            r1 = 14
            int r1 = r11.get(r1)
            org2.apache.commons.lang3.time.DateUtils$ModifyType r5 = org2.apache.commons.lang3.time.DateUtils.ModifyType.TRUNCATE
            if (r5 == r13) goto L_0x002f
            r5 = 500(0x1f4, float:7.0E-43)
            if (r1 >= r5) goto L_0x0031
        L_0x002f:
            long r6 = (long) r1
            long r2 = r2 - r6
        L_0x0031:
            r1 = 13
            if (r12 != r1) goto L_0x0036
            r0 = 1
        L_0x0036:
            r1 = 13
            int r1 = r11.get(r1)
            if (r0 != 0) goto L_0x004b
            org2.apache.commons.lang3.time.DateUtils$ModifyType r5 = org2.apache.commons.lang3.time.DateUtils.ModifyType.TRUNCATE
            if (r5 == r13) goto L_0x0046
            r5 = 30
            if (r1 >= r5) goto L_0x004b
        L_0x0046:
            long r6 = (long) r1
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 * r8
            long r2 = r2 - r6
        L_0x004b:
            r1 = 12
            if (r12 != r1) goto L_0x0050
            r0 = 1
        L_0x0050:
            r1 = 12
            int r1 = r11.get(r1)
            if (r0 != 0) goto L_0x0184
            org2.apache.commons.lang3.time.DateUtils$ModifyType r0 = org2.apache.commons.lang3.time.DateUtils.ModifyType.TRUNCATE
            if (r0 == r13) goto L_0x0060
            r0 = 30
            if (r1 >= r0) goto L_0x0184
        L_0x0060:
            long r0 = (long) r1
            r6 = 60000(0xea60, double:2.9644E-319)
            long r0 = r0 * r6
            long r0 = r2 - r0
        L_0x0067:
            long r2 = r4.getTime()
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 == 0) goto L_0x0075
            r4.setTime(r0)
            r11.setTime(r4)
        L_0x0075:
            r2 = 0
            int[][] r4 = fields
            int r5 = r4.length
            r0 = 0
            r3 = r0
        L_0x007b:
            if (r3 >= r5) goto L_0x0163
            r6 = r4[r3]
            int r1 = r6.length
            r0 = 0
        L_0x0081:
            if (r0 >= r1) goto L_0x00e1
            r7 = r6[r0]
            if (r7 != r12) goto L_0x00de
            org2.apache.commons.lang3.time.DateUtils$ModifyType r0 = org2.apache.commons.lang3.time.DateUtils.ModifyType.CEILING
            if (r13 == r0) goto L_0x0091
            org2.apache.commons.lang3.time.DateUtils$ModifyType r0 = org2.apache.commons.lang3.time.DateUtils.ModifyType.ROUND
            if (r13 != r0) goto L_0x0017
            if (r2 == 0) goto L_0x0017
        L_0x0091:
            r0 = 1001(0x3e9, float:1.403E-42)
            if (r12 != r0) goto L_0x00b2
            r0 = 5
            int r0 = r11.get(r0)
            r1 = 1
            if (r0 != r1) goto L_0x00a5
            r0 = 5
            r1 = 15
            r11.add(r0, r1)
            goto L_0x0017
        L_0x00a5:
            r0 = 5
            r1 = -15
            r11.add(r0, r1)
            r0 = 2
            r1 = 1
            r11.add(r0, r1)
            goto L_0x0017
        L_0x00b2:
            r0 = 9
            if (r12 != r0) goto L_0x00d5
            r0 = 11
            int r0 = r11.get(r0)
            if (r0 != 0) goto L_0x00c7
            r0 = 11
            r1 = 12
            r11.add(r0, r1)
            goto L_0x0017
        L_0x00c7:
            r0 = 11
            r1 = -12
            r11.add(r0, r1)
            r0 = 5
            r1 = 1
            r11.add(r0, r1)
            goto L_0x0017
        L_0x00d5:
            r0 = 0
            r0 = r6[r0]
            r1 = 1
            r11.add(r0, r1)
            goto L_0x0017
        L_0x00de:
            int r0 = r0 + 1
            goto L_0x0081
        L_0x00e1:
            r1 = 0
            r0 = 0
            switch(r12) {
                case 9: goto L_0x0142;
                case 1001: goto L_0x0123;
                default: goto L_0x00e6;
            }
        L_0x00e6:
            r10 = r0
            r0 = r1
            r1 = r2
            r2 = r10
        L_0x00ea:
            if (r2 != 0) goto L_0x010c
            r0 = 0
            r0 = r6[r0]
            int r0 = r11.getActualMinimum(r0)
            r1 = 0
            r1 = r6[r1]
            int r2 = r11.getActualMaximum(r1)
            r1 = 0
            r1 = r6[r1]
            int r1 = r11.get(r1)
            int r1 = r1 - r0
            int r0 = r2 - r0
            int r0 = r0 / 2
            if (r1 <= r0) goto L_0x0161
            r0 = 1
        L_0x0109:
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x010c:
            if (r0 == 0) goto L_0x011d
            r2 = 0
            r2 = r6[r2]
            r7 = 0
            r6 = r6[r7]
            int r6 = r11.get(r6)
            int r0 = r6 - r0
            r11.set(r2, r0)
        L_0x011d:
            int r0 = r3 + 1
            r3 = r0
            r2 = r1
            goto L_0x007b
        L_0x0123:
            r7 = 0
            r7 = r6[r7]
            r8 = 5
            if (r7 != r8) goto L_0x00e6
            r0 = 5
            int r0 = r11.get(r0)
            int r2 = r0 + -1
            r0 = 15
            if (r2 < r0) goto L_0x0136
            int r2 = r2 + -15
        L_0x0136:
            r0 = 7
            if (r2 <= r0) goto L_0x0140
            r0 = 1
        L_0x013a:
            r1 = 1
            r10 = r1
            r1 = r0
            r0 = r2
            r2 = r10
            goto L_0x00ea
        L_0x0140:
            r0 = 0
            goto L_0x013a
        L_0x0142:
            r7 = 0
            r7 = r6[r7]
            r8 = 11
            if (r7 != r8) goto L_0x00e6
            r0 = 11
            int r2 = r11.get(r0)
            r0 = 12
            if (r2 < r0) goto L_0x0155
            int r2 = r2 + -12
        L_0x0155:
            r0 = 6
            if (r2 < r0) goto L_0x015f
            r0 = 1
        L_0x0159:
            r1 = 1
            r10 = r1
            r1 = r0
            r0 = r2
            r2 = r10
            goto L_0x00ea
        L_0x015f:
            r0 = 0
            goto L_0x0159
        L_0x0161:
            r0 = 0
            goto L_0x0109
        L_0x0163:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "The field "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " is not supported"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0184:
            r0 = r2
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.lang3.time.DateUtils.modify(java.util.Calendar, int, org2.apache.commons.lang3.time.DateUtils$ModifyType):void");
    }

    public static Date parseDate(String str, Locale locale, String... strArr) throws ParseException {
        return parseDateWithLeniency(str, locale, strArr, true);
    }

    public static Date parseDate(String str, String... strArr) throws ParseException {
        return parseDate(str, (Locale) null, strArr);
    }

    public static Date parseDateStrictly(String str, Locale locale, String... strArr) throws ParseException {
        return parseDateWithLeniency(str, locale, strArr, false);
    }

    public static Date parseDateStrictly(String str, String... strArr) throws ParseException {
        return parseDateStrictly(str, (Locale) null, strArr);
    }

    private static Date parseDateWithLeniency(String str, Locale locale, String[] strArr, boolean z) throws ParseException {
        if (str == null || strArr == null) {
            throw new IllegalArgumentException("Date and Patterns must not be null");
        }
        TimeZone timeZone = TimeZone.getDefault();
        if (locale == null) {
            locale = Locale.getDefault();
        }
        ParsePosition parsePosition = new ParsePosition(0);
        Calendar instance = Calendar.getInstance(timeZone, locale);
        instance.setLenient(z);
        for (String fastDateParser : strArr) {
            FastDateParser fastDateParser2 = new FastDateParser(fastDateParser, timeZone, locale);
            instance.clear();
            try {
                if (fastDateParser2.parse(str, parsePosition, instance) && parsePosition.getIndex() == str.length()) {
                    return instance.getTime();
                }
            } catch (IllegalArgumentException e) {
            }
            parsePosition.setIndex(0);
        }
        throw new ParseException("Unable to parse the date: " + str, -1);
    }

    public static Calendar round(Calendar calendar, int i) {
        if (calendar == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar calendar2 = (Calendar) calendar.clone();
        modify(calendar2, i, ModifyType.ROUND);
        return calendar2;
    }

    public static Date round(Object obj, int i) {
        if (obj == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (obj instanceof Date) {
            return round((Date) obj, i);
        } else {
            if (obj instanceof Calendar) {
                return round((Calendar) obj, i).getTime();
            }
            throw new ClassCastException("Could not round " + obj);
        }
    }

    public static Date round(Date date, int i) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        modify(instance, i, ModifyType.ROUND);
        return instance.getTime();
    }

    private static Date set(Date date, int i, int i2) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setLenient(false);
        instance.setTime(date);
        instance.set(i, i2);
        return instance.getTime();
    }

    public static Date setDays(Date date, int i) {
        return set(date, 5, i);
    }

    public static Date setHours(Date date, int i) {
        return set(date, 11, i);
    }

    public static Date setMilliseconds(Date date, int i) {
        return set(date, 14, i);
    }

    public static Date setMinutes(Date date, int i) {
        return set(date, 12, i);
    }

    public static Date setMonths(Date date, int i) {
        return set(date, 2, i);
    }

    public static Date setSeconds(Date date, int i) {
        return set(date, 13, i);
    }

    public static Date setYears(Date date, int i) {
        return set(date, 1, i);
    }

    public static Calendar toCalendar(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return instance;
    }

    public static Calendar toCalendar(Date date, TimeZone timeZone) {
        Calendar instance = Calendar.getInstance(timeZone);
        instance.setTime(date);
        return instance;
    }

    public static Calendar truncate(Calendar calendar, int i) {
        if (calendar == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar calendar2 = (Calendar) calendar.clone();
        modify(calendar2, i, ModifyType.TRUNCATE);
        return calendar2;
    }

    public static Date truncate(Object obj, int i) {
        if (obj == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (obj instanceof Date) {
            return truncate((Date) obj, i);
        } else {
            if (obj instanceof Calendar) {
                return truncate((Calendar) obj, i).getTime();
            }
            throw new ClassCastException("Could not truncate " + obj);
        }
    }

    public static Date truncate(Date date, int i) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        modify(instance, i, ModifyType.TRUNCATE);
        return instance.getTime();
    }

    public static int truncatedCompareTo(Calendar calendar, Calendar calendar2, int i) {
        return truncate(calendar, i).compareTo(truncate(calendar2, i));
    }

    public static int truncatedCompareTo(Date date, Date date2, int i) {
        return truncate(date, i).compareTo(truncate(date2, i));
    }

    public static boolean truncatedEquals(Calendar calendar, Calendar calendar2, int i) {
        return truncatedCompareTo(calendar, calendar2, i) == 0;
    }

    public static boolean truncatedEquals(Date date, Date date2, int i) {
        return truncatedCompareTo(date, date2, i) == 0;
    }
}
