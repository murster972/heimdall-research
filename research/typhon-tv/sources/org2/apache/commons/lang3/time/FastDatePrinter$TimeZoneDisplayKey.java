package org2.apache.commons.lang3.time;

import java.util.Locale;
import java.util.TimeZone;

class FastDatePrinter$TimeZoneDisplayKey {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f17694;

    /* renamed from: 齉  reason: contains not printable characters */
    private final Locale f17695;

    /* renamed from: 龘  reason: contains not printable characters */
    private final TimeZone f17696;

    FastDatePrinter$TimeZoneDisplayKey(TimeZone timeZone, boolean z, int i, Locale locale) {
        this.f17696 = timeZone;
        if (z) {
            this.f17694 = Integer.MIN_VALUE | i;
        } else {
            this.f17694 = i;
        }
        this.f17695 = locale;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FastDatePrinter$TimeZoneDisplayKey)) {
            return false;
        }
        FastDatePrinter$TimeZoneDisplayKey fastDatePrinter$TimeZoneDisplayKey = (FastDatePrinter$TimeZoneDisplayKey) obj;
        return this.f17696.equals(fastDatePrinter$TimeZoneDisplayKey.f17696) && this.f17694 == fastDatePrinter$TimeZoneDisplayKey.f17694 && this.f17695.equals(fastDatePrinter$TimeZoneDisplayKey.f17695);
    }

    public int hashCode() {
        return (((this.f17694 * 31) + this.f17695.hashCode()) * 31) + this.f17696.hashCode();
    }
}
