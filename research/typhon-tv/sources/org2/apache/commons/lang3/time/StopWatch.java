package org2.apache.commons.lang3.time;

import java.util.concurrent.TimeUnit;

public class StopWatch {
    private static final long NANO_2_MILLIS = 1000000;
    private State runningState = State.UNSTARTED;
    private SplitState splitState = SplitState.UNSPLIT;
    private long startTime;
    private long startTimeMillis;
    private long stopTime;

    private enum SplitState {
        SPLIT,
        UNSPLIT
    }

    private enum State {
        UNSTARTED {
            /* access modifiers changed from: package-private */
            /* renamed from: 靐  reason: contains not printable characters */
            public boolean m22657() {
                return true;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 齉  reason: contains not printable characters */
            public boolean m22658() {
                return false;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m22659() {
                return false;
            }
        },
        RUNNING {
            /* access modifiers changed from: package-private */
            /* renamed from: 靐  reason: contains not printable characters */
            public boolean m22660() {
                return false;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 齉  reason: contains not printable characters */
            public boolean m22661() {
                return false;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m22662() {
                return true;
            }
        },
        STOPPED {
            /* access modifiers changed from: package-private */
            /* renamed from: 靐  reason: contains not printable characters */
            public boolean m22663() {
                return true;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 齉  reason: contains not printable characters */
            public boolean m22664() {
                return false;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m22665() {
                return false;
            }
        },
        SUSPENDED {
            /* access modifiers changed from: package-private */
            /* renamed from: 靐  reason: contains not printable characters */
            public boolean m22666() {
                return false;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 齉  reason: contains not printable characters */
            public boolean m22667() {
                return true;
            }

            /* access modifiers changed from: package-private */
            /* renamed from: 龘  reason: contains not printable characters */
            public boolean m22668() {
                return true;
            }
        };

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public abstract boolean m22654();

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public abstract boolean m22655();

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract boolean m22656();
    }

    public static StopWatch createStarted() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        return stopWatch;
    }

    public long getNanoTime() {
        if (this.runningState == State.STOPPED || this.runningState == State.SUSPENDED) {
            return this.stopTime - this.startTime;
        }
        if (this.runningState == State.UNSTARTED) {
            return 0;
        }
        if (this.runningState == State.RUNNING) {
            return System.nanoTime() - this.startTime;
        }
        throw new RuntimeException("Illegal running state has occurred.");
    }

    public long getSplitNanoTime() {
        if (this.splitState == SplitState.SPLIT) {
            return this.stopTime - this.startTime;
        }
        throw new IllegalStateException("Stopwatch must be split to get the split time. ");
    }

    public long getSplitTime() {
        return getSplitNanoTime() / 1000000;
    }

    public long getStartTime() {
        if (this.runningState != State.UNSTARTED) {
            return this.startTimeMillis;
        }
        throw new IllegalStateException("Stopwatch has not been started");
    }

    public long getTime() {
        return getNanoTime() / 1000000;
    }

    public long getTime(TimeUnit timeUnit) {
        return timeUnit.convert(getNanoTime(), TimeUnit.NANOSECONDS);
    }

    public boolean isStarted() {
        return this.runningState.m22656();
    }

    public boolean isStopped() {
        return this.runningState.m22654();
    }

    public boolean isSuspended() {
        return this.runningState.m22655();
    }

    public void reset() {
        this.runningState = State.UNSTARTED;
        this.splitState = SplitState.UNSPLIT;
    }

    public void resume() {
        if (this.runningState != State.SUSPENDED) {
            throw new IllegalStateException("Stopwatch must be suspended to resume. ");
        }
        this.startTime += System.nanoTime() - this.stopTime;
        this.runningState = State.RUNNING;
    }

    public void split() {
        if (this.runningState != State.RUNNING) {
            throw new IllegalStateException("Stopwatch is not running. ");
        }
        this.stopTime = System.nanoTime();
        this.splitState = SplitState.SPLIT;
    }

    public void start() {
        if (this.runningState == State.STOPPED) {
            throw new IllegalStateException("Stopwatch must be reset before being restarted. ");
        } else if (this.runningState != State.UNSTARTED) {
            throw new IllegalStateException("Stopwatch already started. ");
        } else {
            this.startTime = System.nanoTime();
            this.startTimeMillis = System.currentTimeMillis();
            this.runningState = State.RUNNING;
        }
    }

    public void stop() {
        if (this.runningState == State.RUNNING || this.runningState == State.SUSPENDED) {
            if (this.runningState == State.RUNNING) {
                this.stopTime = System.nanoTime();
            }
            this.runningState = State.STOPPED;
            return;
        }
        throw new IllegalStateException("Stopwatch is not running. ");
    }

    public void suspend() {
        if (this.runningState != State.RUNNING) {
            throw new IllegalStateException("Stopwatch must be running to suspend. ");
        }
        this.stopTime = System.nanoTime();
        this.runningState = State.SUSPENDED;
    }

    public String toSplitString() {
        return DurationFormatUtils.formatDurationHMS(getSplitTime());
    }

    public String toString() {
        return DurationFormatUtils.formatDurationHMS(getTime());
    }

    public void unsplit() {
        if (this.splitState != SplitState.SPLIT) {
            throw new IllegalStateException("Stopwatch has not been split. ");
        }
        this.splitState = SplitState.UNSPLIT;
    }
}
