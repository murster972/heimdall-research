package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$UnpaddedNumberField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f17710;

    FastDatePrinter$UnpaddedNumberField(int i) {
        this.f17710 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22640() {
        return 4;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m22641(Appendable appendable, int i) throws IOException {
        if (i < 10) {
            appendable.append((char) (i + 48));
        } else if (i < 100) {
            FastDatePrinter.access$000(appendable, i);
        } else {
            FastDatePrinter.access$100(appendable, i, 1);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22642(Appendable appendable, Calendar calendar) throws IOException {
        m22641(appendable, calendar.get(this.f17710));
    }
}
