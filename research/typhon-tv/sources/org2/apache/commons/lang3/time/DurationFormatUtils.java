package org2.apache.commons.lang3.time;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org2.apache.commons.lang3.StringUtils;
import org2.apache.commons.lang3.Validate;

public class DurationFormatUtils {
    static final Object H = "H";
    public static final String ISO_EXTENDED_FORMAT_PATTERN = "'P'yyyy'Y'M'M'd'DT'H'H'm'M's.SSS'S'";
    static final Object M = "M";
    static final Object S = "S";
    static final Object d = "d";
    static final Object m = "m";
    static final Object s = "s";
    static final Object y = "y";

    static class Token {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f17662 = 1;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f17663;

        Token(Object obj) {
            this.f17663 = obj;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static boolean m22567(Token[] tokenArr, Object obj) {
            for (Token r3 : tokenArr) {
                if (r3.m22569() == obj) {
                    return true;
                }
            }
            return false;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Token)) {
                return false;
            }
            Token token = (Token) obj;
            if (this.f17663.getClass() == token.f17663.getClass() && this.f17662 == token.f17662) {
                return this.f17663 instanceof StringBuilder ? this.f17663.toString().equals(token.f17663.toString()) : this.f17663 instanceof Number ? this.f17663.equals(token.f17663) : this.f17663 == token.f17663;
            }
            return false;
        }

        public int hashCode() {
            return this.f17663.hashCode();
        }

        public String toString() {
            return StringUtils.repeat(this.f17663.toString(), this.f17662);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m22568() {
            return this.f17662;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public Object m22569() {
            return this.f17663;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22570() {
            this.f17662++;
        }
    }

    static String format(Token[] tokenArr, long j, long j2, long j3, long j4, long j5, long j6, long j7, boolean z) {
        StringBuilder sb = new StringBuilder();
        boolean z2 = false;
        for (Token token : tokenArr) {
            Object r8 = token.m22569();
            int r7 = token.m22568();
            if (r8 instanceof StringBuilder) {
                sb.append(r8.toString());
            } else if (r8.equals(y)) {
                sb.append(paddedValue(j, z, r7));
                z2 = false;
            } else if (r8.equals(M)) {
                sb.append(paddedValue(j2, z, r7));
                z2 = false;
            } else if (r8.equals(d)) {
                sb.append(paddedValue(j3, z, r7));
                z2 = false;
            } else if (r8.equals(H)) {
                sb.append(paddedValue(j4, z, r7));
                z2 = false;
            } else if (r8.equals(m)) {
                sb.append(paddedValue(j5, z, r7));
                z2 = false;
            } else if (r8.equals(s)) {
                sb.append(paddedValue(j6, z, r7));
                z2 = true;
            } else if (r8.equals(S)) {
                if (z2) {
                    sb.append(paddedValue(j7, true, z ? Math.max(3, r7) : 3));
                } else {
                    sb.append(paddedValue(j7, z, r7));
                }
                z2 = false;
            }
        }
        return sb.toString();
    }

    public static String formatDuration(long j, String str) {
        return formatDuration(j, str, true);
    }

    public static String formatDuration(long j, String str, boolean z) {
        long j2;
        Validate.inclusiveBetween(0, Long.MAX_VALUE, j, "durationMillis must not be negative");
        Token[] lexx = lexx(str);
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        long j6 = 0;
        if (Token.m22567(lexx, d)) {
            j3 = j / 86400000;
            j -= 86400000 * j3;
        }
        if (Token.m22567(lexx, H)) {
            j4 = j / 3600000;
            j -= 3600000 * j4;
        }
        if (Token.m22567(lexx, m)) {
            j5 = j / 60000;
            j -= 60000 * j5;
        }
        if (Token.m22567(lexx, s)) {
            j6 = j / 1000;
            j2 = j - (1000 * j6);
        } else {
            j2 = j;
        }
        return format(lexx, 0, 0, j3, j4, j5, j6, j2, z);
    }

    public static String formatDurationHMS(long j) {
        return formatDuration(j, "HH:mm:ss.SSS");
    }

    public static String formatDurationISO(long j) {
        return formatDuration(j, "'P'yyyy'Y'M'M'd'DT'H'H'm'M's.SSS'S'", false);
    }

    public static String formatDurationWords(long j, boolean z, boolean z2) {
        String formatDuration = formatDuration(j, "d' days 'H' hours 'm' minutes 's' seconds'");
        if (z) {
            String str = org.apache.commons.lang3.StringUtils.SPACE + formatDuration;
            formatDuration = StringUtils.replaceOnce(str, " 0 days", "");
            if (formatDuration.length() != str.length()) {
                String replaceOnce = StringUtils.replaceOnce(formatDuration, " 0 hours", "");
                if (replaceOnce.length() != formatDuration.length()) {
                    formatDuration = StringUtils.replaceOnce(replaceOnce, " 0 minutes", "");
                    if (formatDuration.length() != formatDuration.length()) {
                        formatDuration = StringUtils.replaceOnce(formatDuration, " 0 seconds", "");
                    }
                }
            } else {
                formatDuration = str;
            }
            if (formatDuration.length() != 0) {
                formatDuration = formatDuration.substring(1);
            }
        }
        if (z2) {
            String replaceOnce2 = StringUtils.replaceOnce(formatDuration, " 0 seconds", "");
            if (replaceOnce2.length() != formatDuration.length()) {
                formatDuration = StringUtils.replaceOnce(replaceOnce2, " 0 minutes", "");
                if (formatDuration.length() != replaceOnce2.length()) {
                    String replaceOnce3 = StringUtils.replaceOnce(formatDuration, " 0 hours", "");
                    if (replaceOnce3.length() != formatDuration.length()) {
                        formatDuration = StringUtils.replaceOnce(replaceOnce3, " 0 days", "");
                    }
                } else {
                    formatDuration = replaceOnce2;
                }
            }
        }
        return StringUtils.replaceOnce(StringUtils.replaceOnce(StringUtils.replaceOnce(StringUtils.replaceOnce(org.apache.commons.lang3.StringUtils.SPACE + formatDuration, " 1 seconds", " 1 second"), " 1 minutes", " 1 minute"), " 1 hours", " 1 hour"), " 1 days", " 1 day").trim();
    }

    public static String formatPeriod(long j, long j2, String str) {
        return formatPeriod(j, j2, str, true, TimeZone.getDefault());
    }

    public static String formatPeriod(long j, long j2, String str, boolean z, TimeZone timeZone) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        Validate.isTrue(j <= j2, "startMillis must not be greater than endMillis", new Object[0]);
        Token[] lexx = lexx(str);
        Calendar instance = Calendar.getInstance(timeZone);
        instance.setTime(new Date(j));
        Calendar instance2 = Calendar.getInstance(timeZone);
        instance2.setTime(new Date(j2));
        int i11 = instance2.get(14) - instance.get(14);
        int i12 = instance2.get(13) - instance.get(13);
        int i13 = instance2.get(12) - instance.get(12);
        int i14 = instance2.get(11) - instance.get(11);
        int i15 = instance2.get(5) - instance.get(5);
        int i16 = instance2.get(2) - instance.get(2);
        int i17 = instance2.get(1) - instance.get(1);
        while (i11 < 0) {
            i11 += 1000;
            i12--;
        }
        while (i12 < 0) {
            i12 += 60;
            i13--;
        }
        while (i13 < 0) {
            i13 += 60;
            i14--;
        }
        while (i14 < 0) {
            i14 += 24;
            i15--;
        }
        if (Token.m22567(lexx, M)) {
            int i18 = i16;
            i = i15;
            i2 = i18;
            while (i < 0) {
                i += instance.getActualMaximum(5);
                i2--;
                instance.add(2, 1);
            }
            while (i2 < 0) {
                i2 += 12;
                i17--;
            }
            if (!Token.m22567(lexx, y) && i17 != 0) {
                while (i17 != 0) {
                    i2 += i17 * 12;
                    i17 = 0;
                }
            }
        } else {
            if (!Token.m22567(lexx, y)) {
                int i19 = instance2.get(1);
                if (i16 < 0) {
                    i19--;
                }
                while (instance.get(1) != i19) {
                    int actualMaximum = i15 + (instance.getActualMaximum(6) - instance.get(6));
                    if ((instance instanceof GregorianCalendar) && instance.get(2) == 1 && instance.get(5) == 29) {
                        actualMaximum++;
                    }
                    instance.add(1, 1);
                    i15 = actualMaximum + instance.get(6);
                }
                i17 = 0;
            }
            while (instance.get(2) != instance2.get(2)) {
                i15 += instance.getActualMaximum(5);
                instance.add(2, 1);
            }
            int i20 = i15;
            int i21 = 0;
            while (i < 0) {
                i20 = i + instance.getActualMaximum(5);
                i21 = i2 - 1;
                instance.add(2, 1);
            }
        }
        int i22 = i2;
        int i23 = i;
        if (!Token.m22567(lexx, d)) {
            i3 = 0;
            i4 = i14 + (i23 * 24);
        } else {
            i3 = i23;
            i4 = i14;
        }
        if (!Token.m22567(lexx, H)) {
            i5 = 0;
            i6 = i13 + (i4 * 60);
        } else {
            i5 = i4;
            i6 = i13;
        }
        if (!Token.m22567(lexx, m)) {
            i7 = 0;
            i8 = i12 + (i6 * 60);
        } else {
            i7 = i6;
            i8 = i12;
        }
        if (!Token.m22567(lexx, s)) {
            i9 = 0;
            i10 = i11 + (i8 * 1000);
        } else {
            i9 = i8;
            i10 = i11;
        }
        return format(lexx, (long) i17, (long) i22, (long) i3, (long) i5, (long) i7, (long) i9, (long) i10, z);
    }

    public static String formatPeriodISO(long j, long j2) {
        return formatPeriod(j, j2, "'P'yyyy'Y'M'M'd'DT'H'H'm'M's.SSS'S'", false, TimeZone.getDefault());
    }

    static Token[] lexx(String str) {
        Object obj;
        ArrayList arrayList = new ArrayList(str.length());
        Token token = null;
        StringBuilder sb = null;
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (!z || charAt == '\'') {
                switch (charAt) {
                    case '\'':
                        if (!z) {
                            sb = new StringBuilder();
                            arrayList.add(new Token(sb));
                            z = true;
                            obj = null;
                            break;
                        } else {
                            obj = null;
                            sb = null;
                            z = false;
                            break;
                        }
                    case 'H':
                        obj = H;
                        break;
                    case 'M':
                        obj = M;
                        break;
                    case 'S':
                        obj = S;
                        break;
                    case 'd':
                        obj = d;
                        break;
                    case 'm':
                        obj = m;
                        break;
                    case 's':
                        obj = s;
                        break;
                    case 'y':
                        obj = y;
                        break;
                    default:
                        if (sb == null) {
                            sb = new StringBuilder();
                            arrayList.add(new Token(sb));
                        }
                        sb.append(charAt);
                        obj = null;
                        break;
                }
                if (obj != null) {
                    if (token == null || !token.m22569().equals(obj)) {
                        token = new Token(obj);
                        arrayList.add(token);
                    } else {
                        token.m22570();
                    }
                    sb = null;
                }
            } else {
                sb.append(charAt);
            }
        }
        if (!z) {
            return (Token[]) arrayList.toArray(new Token[arrayList.size()]);
        }
        throw new IllegalArgumentException("Unmatched quote in format: " + str);
    }

    private static String paddedValue(long j, boolean z, int i) {
        String l = Long.toString(j);
        return z ? StringUtils.leftPad(l, i, '0') : l;
    }
}
