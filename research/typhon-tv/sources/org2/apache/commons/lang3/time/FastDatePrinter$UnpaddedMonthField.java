package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$UnpaddedMonthField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    static final FastDatePrinter$UnpaddedMonthField f17709 = new FastDatePrinter$UnpaddedMonthField();

    FastDatePrinter$UnpaddedMonthField() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22637() {
        return 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m22638(Appendable appendable, int i) throws IOException {
        if (i < 10) {
            appendable.append((char) (i + 48));
        } else {
            FastDatePrinter.access$000(appendable, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22639(Appendable appendable, Calendar calendar) throws IOException {
        m22638(appendable, calendar.get(2) + 1);
    }
}
