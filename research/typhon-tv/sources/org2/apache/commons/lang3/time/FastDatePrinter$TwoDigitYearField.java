package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$TwoDigitYearField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    static final FastDatePrinter$TwoDigitYearField f17708 = new FastDatePrinter$TwoDigitYearField();

    FastDatePrinter$TwoDigitYearField() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22634() {
        return 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m22635(Appendable appendable, int i) throws IOException {
        FastDatePrinter.access$000(appendable, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22636(Appendable appendable, Calendar calendar) throws IOException {
        m22635(appendable, calendar.get(1) % 100);
    }
}
