package org2.apache.commons.lang3.time;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

abstract class FormatCache<F extends Format> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ConcurrentMap<MultipartKey, String> f17712 = new ConcurrentHashMap(7);

    /* renamed from: 龘  reason: contains not printable characters */
    private final ConcurrentMap<MultipartKey, F> f17713 = new ConcurrentHashMap(7);

    private static class MultipartKey {

        /* renamed from: 靐  reason: contains not printable characters */
        private int f17714;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object[] f17715;

        public MultipartKey(Object... objArr) {
            this.f17715 = objArr;
        }

        public boolean equals(Object obj) {
            return Arrays.equals(this.f17715, ((MultipartKey) obj).f17715);
        }

        public int hashCode() {
            int i = 0;
            if (this.f17714 == 0) {
                for (Object obj : this.f17715) {
                    if (obj != null) {
                        i = (i * 7) + obj.hashCode();
                    }
                }
                this.f17714 = i;
            }
            return this.f17714;
        }
    }

    FormatCache() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static String m22646(Integer num, Integer num2, Locale locale) {
        DateFormat dateInstance;
        MultipartKey multipartKey = new MultipartKey(num, num2, locale);
        String str = (String) f17712.get(multipartKey);
        if (str != null) {
            return str;
        }
        if (num == null) {
            try {
                dateInstance = DateFormat.getTimeInstance(num2.intValue(), locale);
            } catch (ClassCastException e) {
                throw new IllegalArgumentException("No date time pattern for locale: " + locale);
            }
        } else {
            dateInstance = num2 == null ? DateFormat.getDateInstance(num.intValue(), locale) : DateFormat.getDateTimeInstance(num.intValue(), num2.intValue(), locale);
        }
        String pattern = ((SimpleDateFormat) dateInstance).toPattern();
        String putIfAbsent = f17712.putIfAbsent(multipartKey, pattern);
        return putIfAbsent != null ? putIfAbsent : pattern;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private F m22647(Integer num, Integer num2, TimeZone timeZone, Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        return m22650(m22646(num, num2, locale), timeZone, locale);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public F m22648(int i, TimeZone timeZone, Locale locale) {
        return m22647((Integer) null, Integer.valueOf(i), timeZone, locale);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract F m22649(String str, TimeZone timeZone, Locale locale);

    /* renamed from: 齉  reason: contains not printable characters */
    public F m22650(String str, TimeZone timeZone, Locale locale) {
        if (str == null) {
            throw new NullPointerException("pattern must not be null");
        }
        if (timeZone == null) {
            timeZone = TimeZone.getDefault();
        }
        if (locale == null) {
            locale = Locale.getDefault();
        }
        MultipartKey multipartKey = new MultipartKey(str, timeZone, locale);
        F f = (Format) this.f17713.get(multipartKey);
        if (f != null) {
            return f;
        }
        F r1 = m22649(str, timeZone, locale);
        F f2 = (Format) this.f17713.putIfAbsent(multipartKey, r1);
        return f2 != null ? f2 : r1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public F m22651() {
        return m22652(3, 3, TimeZone.getDefault(), Locale.getDefault());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public F m22652(int i, int i2, TimeZone timeZone, Locale locale) {
        return m22647(Integer.valueOf(i), Integer.valueOf(i2), timeZone, locale);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public F m22653(int i, TimeZone timeZone, Locale locale) {
        return m22647(Integer.valueOf(i), (Integer) null, timeZone, locale);
    }
}
