package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$WeekYear implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    private final FastDatePrinter$NumberRule f17711;

    FastDatePrinter$WeekYear(FastDatePrinter$NumberRule fastDatePrinter$NumberRule) {
        this.f17711 = fastDatePrinter$NumberRule;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22643() {
        return this.f17711.m22612();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22644(Appendable appendable, int i) throws IOException {
        this.f17711.m22608(appendable, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22645(Appendable appendable, Calendar calendar) throws IOException {
        this.f17711.m22608(appendable, CalendarReflection.m22563(calendar));
    }
}
