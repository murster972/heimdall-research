package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.time.TimeZones;

public class FastDateParser implements Serializable, DateParser {
    private static final Strategy ABBREVIATED_YEAR_STRATEGY = new NumberStrategy(1) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m22574(FastDateParser fastDateParser, int i) {
            return i < 100 ? fastDateParser.adjustYear(i) : i;
        }
    };
    private static final Strategy DAY_OF_MONTH_STRATEGY = new NumberStrategy(5);
    private static final Strategy DAY_OF_WEEK_IN_MONTH_STRATEGY = new NumberStrategy(8);
    private static final Strategy DAY_OF_WEEK_STRATEGY = new NumberStrategy(7) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m22576(FastDateParser fastDateParser, int i) {
            if (i != 7) {
                return i + 1;
            }
            return 1;
        }
    };
    private static final Strategy DAY_OF_YEAR_STRATEGY = new NumberStrategy(6);
    private static final Strategy HOUR12_STRATEGY = new NumberStrategy(10) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m22578(FastDateParser fastDateParser, int i) {
            if (i == 12) {
                return 0;
            }
            return i;
        }
    };
    private static final Strategy HOUR24_OF_DAY_STRATEGY = new NumberStrategy(11) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m22577(FastDateParser fastDateParser, int i) {
            if (i == 24) {
                return 0;
            }
            return i;
        }
    };
    private static final Strategy HOUR_OF_DAY_STRATEGY = new NumberStrategy(11);
    private static final Strategy HOUR_STRATEGY = new NumberStrategy(10);
    static final Locale JAPANESE_IMPERIAL = new Locale("ja", "JP", "JP");
    private static final Strategy LITERAL_YEAR_STRATEGY = new NumberStrategy(1);
    /* access modifiers changed from: private */
    public static final Comparator<String> LONGER_FIRST_LOWERCASE = new Comparator<String>() {
        /* renamed from: 龘  reason: contains not printable characters */
        public int compare(String str, String str2) {
            return str2.compareTo(str);
        }
    };
    private static final Strategy MILLISECOND_STRATEGY = new NumberStrategy(14);
    private static final Strategy MINUTE_STRATEGY = new NumberStrategy(12);
    private static final Strategy NUMBER_MONTH_STRATEGY = new NumberStrategy(2) {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m22575(FastDateParser fastDateParser, int i) {
            return i - 1;
        }
    };
    private static final Strategy SECOND_STRATEGY = new NumberStrategy(13);
    private static final Strategy WEEK_OF_MONTH_STRATEGY = new NumberStrategy(4);
    private static final Strategy WEEK_OF_YEAR_STRATEGY = new NumberStrategy(3);
    private static final ConcurrentMap<Locale, Strategy>[] caches = new ConcurrentMap[17];
    private static final long serialVersionUID = 3;
    private final int century;
    private final Locale locale;
    private final String pattern;
    private transient List<StrategyAndWidth> patterns;
    private final int startYear;
    private final TimeZone timeZone;

    private static class CaseInsensitiveTextStrategy extends PatternStrategy {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f17664;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Map<String, Integer> f17665;

        /* renamed from: 龘  reason: contains not printable characters */
        final Locale f17666;

        CaseInsensitiveTextStrategy(int i, Calendar calendar, Locale locale) {
            super();
            this.f17664 = i;
            this.f17666 = locale;
            StringBuilder sb = new StringBuilder();
            sb.append("((?iu)");
            this.f17665 = FastDateParser.appendDisplayNames(calendar, locale, i, sb);
            sb.setLength(sb.length() - 1);
            sb.append(")");
            m22589(sb);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22579(FastDateParser fastDateParser, Calendar calendar, String str) {
            calendar.set(this.f17664, this.f17665.get(str.toLowerCase(this.f17666)).intValue());
        }
    }

    private static class CopyQuotedStrategy extends Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        private final String f17667;

        CopyQuotedStrategy(String str) {
            super();
            this.f17667 = str;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22580() {
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22581(FastDateParser fastDateParser, Calendar calendar, String str, ParsePosition parsePosition, int i) {
            int i2 = 0;
            while (i2 < this.f17667.length()) {
                int index = parsePosition.getIndex() + i2;
                if (index == str.length()) {
                    parsePosition.setErrorIndex(index);
                    return false;
                } else if (this.f17667.charAt(i2) != str.charAt(index)) {
                    parsePosition.setErrorIndex(index);
                    return false;
                } else {
                    i2++;
                }
            }
            parsePosition.setIndex(this.f17667.length() + parsePosition.getIndex());
            return true;
        }
    }

    private static class ISO8601TimeZoneStrategy extends PatternStrategy {

        /* renamed from: 靐  reason: contains not printable characters */
        private static final Strategy f17668 = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}\\d{2}))");
        /* access modifiers changed from: private */

        /* renamed from: 齉  reason: contains not printable characters */
        public static final Strategy f17669 = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}(?::)\\d{2}))");

        /* renamed from: 龘  reason: contains not printable characters */
        private static final Strategy f17670 = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}))");

        ISO8601TimeZoneStrategy(String str) {
            super();
            m22588(str);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Strategy m22583(int i) {
            switch (i) {
                case 1:
                    return f17670;
                case 2:
                    return f17668;
                case 3:
                    return f17669;
                default:
                    throw new IllegalArgumentException("invalid number of X");
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22584(FastDateParser fastDateParser, Calendar calendar, String str) {
            if (str.equals("Z")) {
                calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            } else {
                calendar.setTimeZone(TimeZone.getTimeZone(TimeZones.GMT_ID + str));
            }
        }
    }

    private static class NumberStrategy extends Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f17671;

        NumberStrategy(int i) {
            super();
            this.f17671 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m22585(FastDateParser fastDateParser, int i) {
            return i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22586() {
            return true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22587(FastDateParser fastDateParser, Calendar calendar, String str, ParsePosition parsePosition, int i) {
            int i2;
            int i3;
            int index = parsePosition.getIndex();
            int length = str.length();
            if (i == 0) {
                int i4 = index;
                while (i4 < length && Character.isWhitespace(str.charAt(i4))) {
                    i4++;
                }
                parsePosition.setIndex(i4);
                int i5 = length;
                i2 = i4;
                i3 = i5;
            } else {
                i3 = index + i;
                if (length > i3) {
                    i2 = index;
                } else {
                    i3 = length;
                    i2 = index;
                }
            }
            while (i2 < i3 && Character.isDigit(str.charAt(i2))) {
                i2++;
            }
            if (parsePosition.getIndex() == i2) {
                parsePosition.setErrorIndex(i2);
                return false;
            }
            int parseInt = Integer.parseInt(str.substring(parsePosition.getIndex(), i2));
            parsePosition.setIndex(i2);
            calendar.set(this.f17671, m22585(fastDateParser, parseInt));
            return true;
        }
    }

    private static abstract class PatternStrategy extends Strategy {

        /* renamed from: 龘  reason: contains not printable characters */
        private Pattern f17672;

        private PatternStrategy() {
            super();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22588(String str) {
            this.f17672 = Pattern.compile(str);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22589(StringBuilder sb) {
            m22588(sb.toString());
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract void m22590(FastDateParser fastDateParser, Calendar calendar, String str);

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22591() {
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22592(FastDateParser fastDateParser, Calendar calendar, String str, ParsePosition parsePosition, int i) {
            Matcher matcher = this.f17672.matcher(str.substring(parsePosition.getIndex()));
            if (!matcher.lookingAt()) {
                parsePosition.setErrorIndex(parsePosition.getIndex());
                return false;
            }
            parsePosition.setIndex(parsePosition.getIndex() + matcher.end(1));
            m22590(fastDateParser, calendar, matcher.group(1));
            return true;
        }
    }

    private static abstract class Strategy {
        private Strategy() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m22593() {
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract boolean m22594(FastDateParser fastDateParser, Calendar calendar, String str, ParsePosition parsePosition, int i);
    }

    private static class StrategyAndWidth {

        /* renamed from: 靐  reason: contains not printable characters */
        final int f17673;

        /* renamed from: 龘  reason: contains not printable characters */
        final Strategy f17674;

        StrategyAndWidth(Strategy strategy, int i) {
            this.f17674 = strategy;
            this.f17673 = i;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public int m22595(ListIterator<StrategyAndWidth> listIterator) {
            if (!this.f17674.m22593() || !listIterator.hasNext()) {
                return 0;
            }
            Strategy strategy = listIterator.next().f17674;
            listIterator.previous();
            if (strategy.m22593()) {
                return this.f17673;
            }
            return 0;
        }
    }

    private class StrategyParser {

        /* renamed from: 靐  reason: contains not printable characters */
        private final String f17675;

        /* renamed from: 麤  reason: contains not printable characters */
        private int f17676;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Calendar f17677;

        StrategyParser(String str, Calendar calendar) {
            this.f17675 = str;
            this.f17677 = calendar;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private StrategyAndWidth m22596() {
            StringBuilder sb = new StringBuilder();
            boolean z = false;
            while (this.f17676 < this.f17675.length()) {
                char charAt = this.f17675.charAt(this.f17676);
                if (!z && FastDateParser.isFormatLetter(charAt)) {
                    break;
                }
                if (charAt == '\'') {
                    int i = this.f17676 + 1;
                    this.f17676 = i;
                    if (i == this.f17675.length() || this.f17675.charAt(this.f17676) != '\'') {
                        z = !z;
                    }
                }
                this.f17676++;
                sb.append(charAt);
            }
            if (z) {
                throw new IllegalArgumentException("Unterminated quote");
            }
            String sb2 = sb.toString();
            return new StrategyAndWidth(new CopyQuotedStrategy(sb2), sb2.length());
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private StrategyAndWidth m22597(char c) {
            int i = this.f17676;
            do {
                int i2 = this.f17676 + 1;
                this.f17676 = i2;
                if (i2 >= this.f17675.length() || this.f17675.charAt(this.f17676) != c) {
                    int i3 = this.f17676 - i;
                }
                int i22 = this.f17676 + 1;
                this.f17676 = i22;
                break;
            } while (this.f17675.charAt(this.f17676) != c);
            int i32 = this.f17676 - i;
            return new StrategyAndWidth(FastDateParser.this.getStrategy(c, i32, this.f17677), i32);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public StrategyAndWidth m22598() {
            if (this.f17676 >= this.f17675.length()) {
                return null;
            }
            char charAt = this.f17675.charAt(this.f17676);
            return FastDateParser.isFormatLetter(charAt) ? m22597(charAt) : m22596();
        }
    }

    static class TimeZoneStrategy extends PatternStrategy {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Map<String, TzInfo> f17679 = new HashMap();

        /* renamed from: 龘  reason: contains not printable characters */
        private final Locale f17680;

        private static class TzInfo {

            /* renamed from: 靐  reason: contains not printable characters */
            int f17681;

            /* renamed from: 龘  reason: contains not printable characters */
            TimeZone f17682;

            TzInfo(TimeZone timeZone, boolean z) {
                this.f17682 = timeZone;
                this.f17681 = z ? timeZone.getDSTSavings() : 0;
            }
        }

        TimeZoneStrategy(Locale locale) {
            super();
            this.f17680 = locale;
            StringBuilder sb = new StringBuilder();
            sb.append("((?iu)[+-]\\d{4}|GMT[+-]\\d{1,2}:\\d{2}");
            TreeSet<String> treeSet = new TreeSet<>(FastDateParser.LONGER_FIRST_LOWERCASE);
            for (String[] strArr : DateFormatSymbols.getInstance(locale).getZoneStrings()) {
                String str = strArr[0];
                if (!str.equalsIgnoreCase(TimeZones.GMT_ID)) {
                    TimeZone timeZone = TimeZone.getTimeZone(str);
                    TzInfo tzInfo = new TzInfo(timeZone, false);
                    TzInfo tzInfo2 = tzInfo;
                    for (int i = 1; i < strArr.length; i++) {
                        switch (i) {
                            case 3:
                                tzInfo2 = new TzInfo(timeZone, true);
                                break;
                            case 5:
                                tzInfo2 = tzInfo;
                                break;
                        }
                        String lowerCase = strArr[i].toLowerCase(locale);
                        if (treeSet.add(lowerCase)) {
                            this.f17679.put(lowerCase, tzInfo2);
                        }
                    }
                }
            }
            for (String access$800 : treeSet) {
                StringBuilder unused = FastDateParser.simpleQuote(sb.append('|'), access$800);
            }
            sb.append(")");
            m22589(sb);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m22599(FastDateParser fastDateParser, Calendar calendar, String str) {
            if (str.charAt(0) == '+' || str.charAt(0) == '-') {
                calendar.setTimeZone(TimeZone.getTimeZone(TimeZones.GMT_ID + str));
            } else if (str.regionMatches(true, 0, TimeZones.GMT_ID, 0, 3)) {
                calendar.setTimeZone(TimeZone.getTimeZone(str.toUpperCase()));
            } else {
                TzInfo tzInfo = this.f17679.get(str.toLowerCase(this.f17680));
                calendar.set(16, tzInfo.f17681);
                calendar.set(15, tzInfo.f17682.getRawOffset());
            }
        }
    }

    protected FastDateParser(String str, TimeZone timeZone2, Locale locale2) {
        this(str, timeZone2, locale2, (Date) null);
    }

    protected FastDateParser(String str, TimeZone timeZone2, Locale locale2, Date date) {
        int i;
        this.pattern = str;
        this.timeZone = timeZone2;
        this.locale = locale2;
        Calendar instance = Calendar.getInstance(timeZone2, locale2);
        if (date != null) {
            instance.setTime(date);
            i = instance.get(1);
        } else if (locale2.equals(JAPANESE_IMPERIAL)) {
            i = 0;
        } else {
            instance.setTime(new Date());
            i = instance.get(1) - 80;
        }
        this.century = (i / 100) * 100;
        this.startYear = i - this.century;
        init(instance);
    }

    /* access modifiers changed from: private */
    public int adjustYear(int i) {
        int i2 = this.century + i;
        return i >= this.startYear ? i2 : i2 + 100;
    }

    /* access modifiers changed from: private */
    public static Map<String, Integer> appendDisplayNames(Calendar calendar, Locale locale2, int i, StringBuilder sb) {
        HashMap hashMap = new HashMap();
        Map<String, Integer> displayNames = calendar.getDisplayNames(i, 0, locale2);
        TreeSet treeSet = new TreeSet(LONGER_FIRST_LOWERCASE);
        for (Map.Entry next : displayNames.entrySet()) {
            String lowerCase = ((String) next.getKey()).toLowerCase(locale2);
            if (treeSet.add(lowerCase)) {
                hashMap.put(lowerCase, next.getValue());
            }
        }
        Iterator it2 = treeSet.iterator();
        while (it2.hasNext()) {
            simpleQuote(sb, (String) it2.next()).append('|');
        }
        return hashMap;
    }

    private static ConcurrentMap<Locale, Strategy> getCache(int i) {
        ConcurrentMap<Locale, Strategy> concurrentMap;
        synchronized (caches) {
            if (caches[i] == null) {
                caches[i] = new ConcurrentHashMap(3);
            }
            concurrentMap = caches[i];
        }
        return concurrentMap;
    }

    private Strategy getLocaleSpecificStrategy(int i, Calendar calendar) {
        TimeZoneStrategy timeZoneStrategy;
        ConcurrentMap<Locale, Strategy> cache = getCache(i);
        Strategy strategy = (Strategy) cache.get(this.locale);
        if (strategy == null) {
            timeZoneStrategy = i == 15 ? new TimeZoneStrategy(this.locale) : new CaseInsensitiveTextStrategy(i, calendar, this.locale);
            Strategy putIfAbsent = cache.putIfAbsent(this.locale, timeZoneStrategy);
            if (putIfAbsent != null) {
                return putIfAbsent;
            }
        } else {
            timeZoneStrategy = strategy;
        }
        return timeZoneStrategy;
    }

    /* access modifiers changed from: private */
    public Strategy getStrategy(char c, int i, Calendar calendar) {
        switch (c) {
            case 'D':
                return DAY_OF_YEAR_STRATEGY;
            case 'E':
                return getLocaleSpecificStrategy(7, calendar);
            case 'F':
                return DAY_OF_WEEK_IN_MONTH_STRATEGY;
            case 'G':
                return getLocaleSpecificStrategy(0, calendar);
            case 'H':
                return HOUR_OF_DAY_STRATEGY;
            case 'K':
                return HOUR_STRATEGY;
            case 'M':
                return i >= 3 ? getLocaleSpecificStrategy(2, calendar) : NUMBER_MONTH_STRATEGY;
            case 'S':
                return MILLISECOND_STRATEGY;
            case 'W':
                return WEEK_OF_MONTH_STRATEGY;
            case 'X':
                return ISO8601TimeZoneStrategy.m22583(i);
            case 'Y':
            case 'y':
                return i > 2 ? LITERAL_YEAR_STRATEGY : ABBREVIATED_YEAR_STRATEGY;
            case 'Z':
                if (i == 2) {
                    return ISO8601TimeZoneStrategy.f17669;
                }
                break;
            case 'a':
                return getLocaleSpecificStrategy(9, calendar);
            case 'd':
                return DAY_OF_MONTH_STRATEGY;
            case 'h':
                return HOUR12_STRATEGY;
            case 'k':
                return HOUR24_OF_DAY_STRATEGY;
            case 'm':
                return MINUTE_STRATEGY;
            case 's':
                return SECOND_STRATEGY;
            case 'u':
                return DAY_OF_WEEK_STRATEGY;
            case 'w':
                return WEEK_OF_YEAR_STRATEGY;
            case 'z':
                break;
            default:
                throw new IllegalArgumentException("Format '" + c + "' not supported");
        }
        return getLocaleSpecificStrategy(15, calendar);
    }

    private void init(Calendar calendar) {
        this.patterns = new ArrayList();
        StrategyParser strategyParser = new StrategyParser(this.pattern, calendar);
        while (true) {
            StrategyAndWidth r1 = strategyParser.m22598();
            if (r1 != null) {
                this.patterns.add(r1);
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean isFormatLetter(char c) {
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        init(Calendar.getInstance(this.timeZone, this.locale));
    }

    /* access modifiers changed from: private */
    public static StringBuilder simpleQuote(StringBuilder sb, String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case '$':
                case '(':
                case ')':
                case '*':
                case '+':
                case '.':
                case '?':
                case '[':
                case '\\':
                case '^':
                case '{':
                case '|':
                    sb.append('\\');
                    break;
            }
            sb.append(charAt);
        }
        return sb;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FastDateParser)) {
            return false;
        }
        FastDateParser fastDateParser = (FastDateParser) obj;
        return this.pattern.equals(fastDateParser.pattern) && this.timeZone.equals(fastDateParser.timeZone) && this.locale.equals(fastDateParser.locale);
    }

    public Locale getLocale() {
        return this.locale;
    }

    public String getPattern() {
        return this.pattern;
    }

    public TimeZone getTimeZone() {
        return this.timeZone;
    }

    public int hashCode() {
        return this.pattern.hashCode() + ((this.timeZone.hashCode() + (this.locale.hashCode() * 13)) * 13);
    }

    public Date parse(String str) throws ParseException {
        ParsePosition parsePosition = new ParsePosition(0);
        Date parse = parse(str, parsePosition);
        if (parse != null) {
            return parse;
        }
        if (this.locale.equals(JAPANESE_IMPERIAL)) {
            throw new ParseException("(The " + this.locale + " locale does not support dates before 1868 AD)\nUnparseable date: \"" + str, parsePosition.getErrorIndex());
        }
        throw new ParseException("Unparseable date: " + str, parsePosition.getErrorIndex());
    }

    public Date parse(String str, ParsePosition parsePosition) {
        Calendar instance = Calendar.getInstance(this.timeZone, this.locale);
        instance.clear();
        if (parse(str, parsePosition, instance)) {
            return instance.getTime();
        }
        return null;
    }

    public boolean parse(String str, ParsePosition parsePosition, Calendar calendar) {
        ListIterator<StrategyAndWidth> listIterator = this.patterns.listIterator();
        while (listIterator.hasNext()) {
            StrategyAndWidth next = listIterator.next();
            if (!next.f17674.m22594(this, calendar, str, parsePosition, next.m22595(listIterator))) {
                return false;
            }
        }
        return true;
    }

    public Object parseObject(String str) throws ParseException {
        return parse(str);
    }

    public Object parseObject(String str, ParsePosition parsePosition) {
        return parse(str, parsePosition);
    }

    public String toString() {
        return "FastDateParser[" + this.pattern + "," + this.locale + "," + this.timeZone.getID() + "]";
    }
}
