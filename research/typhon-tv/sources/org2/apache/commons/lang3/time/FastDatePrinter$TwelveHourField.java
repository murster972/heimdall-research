package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$TwelveHourField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    private final FastDatePrinter$NumberRule f17704;

    FastDatePrinter$TwelveHourField(FastDatePrinter$NumberRule fastDatePrinter$NumberRule) {
        this.f17704 = fastDatePrinter$NumberRule;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22622() {
        return this.f17704.m22612();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22623(Appendable appendable, int i) throws IOException {
        this.f17704.m22608(appendable, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22624(Appendable appendable, Calendar calendar) throws IOException {
        int i = calendar.get(10);
        if (i == 0) {
            i = calendar.getLeastMaximum(10) + 1;
        }
        this.f17704.m22608(appendable, i);
    }
}
