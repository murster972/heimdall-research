package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$DayInWeekField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    private final FastDatePrinter$NumberRule f17684;

    FastDatePrinter$DayInWeekField(FastDatePrinter$NumberRule fastDatePrinter$NumberRule) {
        this.f17684 = fastDatePrinter$NumberRule;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22602() {
        return this.f17684.m22612();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22603(Appendable appendable, int i) throws IOException {
        this.f17684.m22608(appendable, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22604(Appendable appendable, Calendar calendar) throws IOException {
        int i = 7;
        int i2 = calendar.get(7);
        FastDatePrinter$NumberRule fastDatePrinter$NumberRule = this.f17684;
        if (i2 != 1) {
            i = i2 - 1;
        }
        fastDatePrinter$NumberRule.m22608(appendable, i);
    }
}
