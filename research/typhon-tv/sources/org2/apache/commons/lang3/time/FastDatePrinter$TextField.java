package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$TextField implements FastDatePrinter$Rule {

    /* renamed from: 靐  reason: contains not printable characters */
    private final String[] f17692;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f17693;

    FastDatePrinter$TextField(int i, String[] strArr) {
        this.f17693 = i;
        this.f17692 = strArr;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22616() {
        int i = 0;
        int length = this.f17692.length;
        while (true) {
            int i2 = length - 1;
            if (i2 < 0) {
                return i;
            }
            int length2 = this.f17692[i2].length();
            if (length2 <= i) {
                length2 = i;
            }
            i = length2;
            length = i2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22617(Appendable appendable, Calendar calendar) throws IOException {
        appendable.append(this.f17692[calendar.get(this.f17693)]);
    }
}
