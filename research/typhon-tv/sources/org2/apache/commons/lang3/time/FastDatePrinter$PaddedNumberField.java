package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$PaddedNumberField implements FastDatePrinter$NumberRule {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f17689;

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f17690;

    FastDatePrinter$PaddedNumberField(int i, int i2) {
        if (i2 < 3) {
            throw new IllegalArgumentException();
        }
        this.f17690 = i;
        this.f17689 = i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22609() {
        return this.f17689;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m22610(Appendable appendable, int i) throws IOException {
        FastDatePrinter.access$100(appendable, i, this.f17689);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22611(Appendable appendable, Calendar calendar) throws IOException {
        m22610(appendable, calendar.get(this.f17690));
    }
}
