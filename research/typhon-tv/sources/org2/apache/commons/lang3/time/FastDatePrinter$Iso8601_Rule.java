package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$Iso8601_Rule implements FastDatePrinter$Rule {

    /* renamed from: 靐  reason: contains not printable characters */
    static final FastDatePrinter$Iso8601_Rule f17685 = new FastDatePrinter$Iso8601_Rule(5);

    /* renamed from: 齉  reason: contains not printable characters */
    static final FastDatePrinter$Iso8601_Rule f17686 = new FastDatePrinter$Iso8601_Rule(6);

    /* renamed from: 龘  reason: contains not printable characters */
    static final FastDatePrinter$Iso8601_Rule f17687 = new FastDatePrinter$Iso8601_Rule(3);

    /* renamed from: 麤  reason: contains not printable characters */
    final int f17688;

    FastDatePrinter$Iso8601_Rule(int i) {
        this.f17688 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static FastDatePrinter$Iso8601_Rule m22605(int i) {
        switch (i) {
            case 1:
                return f17687;
            case 2:
                return f17685;
            case 3:
                return f17686;
            default:
                throw new IllegalArgumentException("invalid number of X");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22606() {
        return this.f17688;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22607(Appendable appendable, Calendar calendar) throws IOException {
        int i = calendar.get(15) + calendar.get(16);
        if (i == 0) {
            appendable.append("Z");
            return;
        }
        if (i < 0) {
            appendable.append('-');
            i = -i;
        } else {
            appendable.append('+');
        }
        int i2 = i / 3600000;
        FastDatePrinter.access$000(appendable, i2);
        if (this.f17688 >= 5) {
            if (this.f17688 == 6) {
                appendable.append(':');
            }
            FastDatePrinter.access$000(appendable, (i / 60000) - (i2 * 60));
        }
    }
}
