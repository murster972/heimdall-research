package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$TwoDigitNumberField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    private final int f17707;

    FastDatePrinter$TwoDigitNumberField(int i) {
        this.f17707 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22631() {
        return 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m22632(Appendable appendable, int i) throws IOException {
        if (i < 100) {
            FastDatePrinter.access$000(appendable, i);
        } else {
            FastDatePrinter.access$100(appendable, i, 2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22633(Appendable appendable, Calendar calendar) throws IOException {
        m22632(appendable, calendar.get(this.f17707));
    }
}
