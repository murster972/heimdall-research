package org2.apache.commons.lang3.time;

import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org2.apache.commons.lang3.exception.ExceptionUtils;

class CalendarReflection {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Method f17654 = m22564("getWeekYear", new Class[0]);

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Method f17655 = m22564("isWeekDateSupported", new Class[0]);

    /* renamed from: 靐  reason: contains not printable characters */
    public static int m22563(Calendar calendar) {
        try {
            if (m22565(calendar)) {
                return ((Integer) f17654.invoke(calendar, new Object[0])).intValue();
            }
            int i = calendar.get(1);
            if (f17655 != null || !(calendar instanceof GregorianCalendar)) {
                return i;
            }
            switch (calendar.get(2)) {
                case 0:
                    return calendar.get(3) >= 52 ? i - 1 : i;
                case 11:
                    return calendar.get(3) == 1 ? i + 1 : i;
                default:
                    return i;
            }
        } catch (Exception e) {
            return ((Integer) ExceptionUtils.rethrow(e)).intValue();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static Method m22564(String str, Class<?>... clsArr) {
        try {
            return Calendar.class.getMethod(str, clsArr);
        } catch (Exception e) {
            return null;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m22565(Calendar calendar) {
        try {
            return f17655 != null && ((Boolean) f17655.invoke(calendar, new Object[0])).booleanValue();
        } catch (Exception e) {
            return ((Boolean) ExceptionUtils.rethrow(e)).booleanValue();
        }
    }
}
