package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$TwoDigitMonthField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    static final FastDatePrinter$TwoDigitMonthField f17706 = new FastDatePrinter$TwoDigitMonthField();

    FastDatePrinter$TwoDigitMonthField() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22628() {
        return 2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m22629(Appendable appendable, int i) throws IOException {
        FastDatePrinter.access$000(appendable, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22630(Appendable appendable, Calendar calendar) throws IOException {
        m22629(appendable, calendar.get(2) + 1);
    }
}
