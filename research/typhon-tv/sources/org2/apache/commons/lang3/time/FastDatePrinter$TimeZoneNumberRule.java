package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$TimeZoneNumberRule implements FastDatePrinter$Rule {

    /* renamed from: 靐  reason: contains not printable characters */
    static final FastDatePrinter$TimeZoneNumberRule f17701 = new FastDatePrinter$TimeZoneNumberRule(false);

    /* renamed from: 龘  reason: contains not printable characters */
    static final FastDatePrinter$TimeZoneNumberRule f17702 = new FastDatePrinter$TimeZoneNumberRule(true);

    /* renamed from: 齉  reason: contains not printable characters */
    final boolean f17703;

    FastDatePrinter$TimeZoneNumberRule(boolean z) {
        this.f17703 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22620() {
        return 5;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22621(Appendable appendable, Calendar calendar) throws IOException {
        int i = calendar.get(15) + calendar.get(16);
        if (i < 0) {
            appendable.append('-');
            i = -i;
        } else {
            appendable.append('+');
        }
        int i2 = i / 3600000;
        FastDatePrinter.access$000(appendable, i2);
        if (this.f17703) {
            appendable.append(':');
        }
        FastDatePrinter.access$000(appendable, (i / 60000) - (i2 * 60));
    }
}
