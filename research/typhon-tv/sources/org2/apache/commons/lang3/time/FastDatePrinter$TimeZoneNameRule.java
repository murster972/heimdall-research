package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

class FastDatePrinter$TimeZoneNameRule implements FastDatePrinter$Rule {

    /* renamed from: 靐  reason: contains not printable characters */
    private final int f17697;

    /* renamed from: 麤  reason: contains not printable characters */
    private final String f17698;

    /* renamed from: 齉  reason: contains not printable characters */
    private final String f17699;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Locale f17700;

    FastDatePrinter$TimeZoneNameRule(TimeZone timeZone, Locale locale, int i) {
        this.f17700 = locale;
        this.f17697 = i;
        this.f17699 = FastDatePrinter.getTimeZoneDisplay(timeZone, false, i, locale);
        this.f17698 = FastDatePrinter.getTimeZoneDisplay(timeZone, true, i, locale);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22618() {
        return Math.max(this.f17699.length(), this.f17698.length());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22619(Appendable appendable, Calendar calendar) throws IOException {
        TimeZone timeZone = calendar.getTimeZone();
        if (calendar.get(16) != 0) {
            appendable.append(FastDatePrinter.getTimeZoneDisplay(timeZone, true, this.f17697, this.f17700));
        } else {
            appendable.append(FastDatePrinter.getTimeZoneDisplay(timeZone, false, this.f17697, this.f17700));
        }
    }
}
