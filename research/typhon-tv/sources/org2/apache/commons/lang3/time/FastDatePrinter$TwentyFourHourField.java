package org2.apache.commons.lang3.time;

import java.io.IOException;
import java.util.Calendar;

class FastDatePrinter$TwentyFourHourField implements FastDatePrinter$NumberRule {

    /* renamed from: 龘  reason: contains not printable characters */
    private final FastDatePrinter$NumberRule f17705;

    FastDatePrinter$TwentyFourHourField(FastDatePrinter$NumberRule fastDatePrinter$NumberRule) {
        this.f17705 = fastDatePrinter$NumberRule;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m22625() {
        return this.f17705.m22612();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22626(Appendable appendable, int i) throws IOException {
        this.f17705.m22608(appendable, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m22627(Appendable appendable, Calendar calendar) throws IOException {
        int i = calendar.get(11);
        if (i == 0) {
            i = calendar.getMaximum(11) + 1;
        }
        this.f17705.m22608(appendable, i);
    }
}
