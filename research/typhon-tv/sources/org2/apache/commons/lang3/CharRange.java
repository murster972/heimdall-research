package org2.apache.commons.lang3;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

final class CharRange implements Serializable, Iterable<Character> {
    private static final long serialVersionUID = 8270183163158333422L;
    /* access modifiers changed from: private */
    public final char end;
    /* access modifiers changed from: private */
    public final boolean negated;
    /* access modifiers changed from: private */
    public final char start;

    /* renamed from: 龘  reason: contains not printable characters */
    private transient String f17601;

    private static class CharacterIterator implements Iterator<Character> {

        /* renamed from: 靐  reason: contains not printable characters */
        private final CharRange f17602;

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean f17603;

        /* renamed from: 龘  reason: contains not printable characters */
        private char f17604;

        private CharacterIterator(CharRange charRange) {
            this.f17602 = charRange;
            this.f17603 = true;
            if (!this.f17602.negated) {
                this.f17604 = this.f17602.start;
            } else if (this.f17602.start != 0) {
                this.f17604 = 0;
            } else if (this.f17602.end == 65535) {
                this.f17603 = false;
            } else {
                this.f17604 = (char) (this.f17602.end + 1);
            }
        }

        /* renamed from: 靐  reason: contains not printable characters */
        private void m22490() {
            if (this.f17602.negated) {
                if (this.f17604 == 65535) {
                    this.f17603 = false;
                } else if (this.f17604 + 1 != this.f17602.start) {
                    this.f17604 = (char) (this.f17604 + 1);
                } else if (this.f17602.end == 65535) {
                    this.f17603 = false;
                } else {
                    this.f17604 = (char) (this.f17602.end + 1);
                }
            } else if (this.f17604 < this.f17602.end) {
                this.f17604 = (char) (this.f17604 + 1);
            } else {
                this.f17603 = false;
            }
        }

        public boolean hasNext() {
            return this.f17603;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Character next() {
            if (!this.f17603) {
                throw new NoSuchElementException();
            }
            char c = this.f17604;
            m22490();
            return Character.valueOf(c);
        }
    }

    private CharRange(char c, char c2, boolean z) {
        if (c <= c2) {
            char c3 = c2;
            c2 = c;
            c = c3;
        }
        this.start = c2;
        this.end = c;
        this.negated = z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static CharRange m22482(char c) {
        return new CharRange(c, c, true);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static CharRange m22483(char c, char c2) {
        return new CharRange(c, c2, true);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static CharRange m22485(char c) {
        return new CharRange(c, c, false);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static CharRange m22486(char c, char c2) {
        return new CharRange(c, c2, false);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CharRange)) {
            return false;
        }
        CharRange charRange = (CharRange) obj;
        return this.start == charRange.start && this.end == charRange.end && this.negated == charRange.negated;
    }

    public int hashCode() {
        return (this.negated ? 1 : 0) + (this.end * 7) + this.start + 'S';
    }

    public Iterator<Character> iterator() {
        return new CharacterIterator();
    }

    public String toString() {
        if (this.f17601 == null) {
            StringBuilder sb = new StringBuilder(4);
            if (m22489()) {
                sb.append('^');
            }
            sb.append(this.start);
            if (this.start != this.end) {
                sb.append('-');
                sb.append(this.end);
            }
            this.f17601 = sb.toString();
        }
        return this.f17601;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m22488(char c) {
        return (c >= this.start && c <= this.end) != this.negated;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m22489() {
        return this.negated;
    }
}
