package org2.apache.commons.logging.impl;

import java.lang.reflect.InvocationTargetException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org2.apache.commons.logging.LogFactory;

public class ServletContextCleaner implements ServletContextListener {
    private static final Class[] RELEASE_SIGNATURE;
    static Class class$java$lang$ClassLoader;

    static {
        Class cls;
        Class[] clsArr = new Class[1];
        if (class$java$lang$ClassLoader == null) {
            cls = class$("java.lang.ClassLoader");
            class$java$lang$ClassLoader = cls;
        } else {
            cls = class$java$lang$ClassLoader;
        }
        clsArr[0] = cls;
        RELEASE_SIGNATURE = clsArr;
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        Object[] objArr = {contextClassLoader};
        ClassLoader classLoader = contextClassLoader;
        while (classLoader != null) {
            try {
                Class<?> loadClass = classLoader.loadClass("org.apache.commons.logging.LogFactory");
                loadClass.getMethod("release", RELEASE_SIGNATURE).invoke((Object) null, objArr);
                classLoader = loadClass.getClassLoader().getParent();
            } catch (ClassNotFoundException e) {
                classLoader = null;
            } catch (NoSuchMethodException e2) {
                System.err.println("LogFactory instance found which does not support release method!");
                classLoader = null;
            } catch (IllegalAccessException e3) {
                System.err.println("LogFactory instance found which is not accessable!");
                classLoader = null;
            } catch (InvocationTargetException e4) {
                System.err.println("LogFactory instance release method failed!");
                classLoader = null;
            }
        }
        LogFactory.release(contextClassLoader);
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
    }
}
