package org2.apache.commons.logging.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Hashtable;
import org2.apache.commons.logging.Log;
import org2.apache.commons.logging.LogConfigurationException;
import org2.apache.commons.logging.LogFactory;

public class LogFactoryImpl extends LogFactory {
    public static final String ALLOW_FLAWED_CONTEXT_PROPERTY = "org.apache.commons.logging.Log.allowFlawedContext";
    public static final String ALLOW_FLAWED_DISCOVERY_PROPERTY = "org.apache.commons.logging.Log.allowFlawedDiscovery";
    public static final String ALLOW_FLAWED_HIERARCHY_PROPERTY = "org.apache.commons.logging.Log.allowFlawedHierarchy";
    private static final String LOGGING_IMPL_JDK14_LOGGER = "org.apache.commons.logging.impl.Jdk14Logger";
    private static final String LOGGING_IMPL_LOG4J_LOGGER = "org.apache.commons.logging.impl.Log4JLogger";
    private static final String LOGGING_IMPL_LUMBERJACK_LOGGER = "org.apache.commons.logging.impl.Jdk13LumberjackLogger";
    private static final String LOGGING_IMPL_SIMPLE_LOGGER = "org.apache.commons.logging.impl.SimpleLog";
    public static final String LOG_PROPERTY = "org.apache.commons.logging.Log";
    protected static final String LOG_PROPERTY_OLD = "org.apache.commons.logging.log";
    private static final String PKG_IMPL = "org.apache.commons.logging.impl.";
    private static final int PKG_LEN = PKG_IMPL.length();
    static Class class$java$lang$String;
    static Class class$org$apache$commons$logging$Log;
    static Class class$org$apache$commons$logging$LogFactory;
    static Class class$org$apache$commons$logging$impl$LogFactoryImpl;
    private static final String[] classesToDiscover = {LOGGING_IMPL_LOG4J_LOGGER, LOGGING_IMPL_JDK14_LOGGER, LOGGING_IMPL_LUMBERJACK_LOGGER, LOGGING_IMPL_SIMPLE_LOGGER};
    private boolean allowFlawedContext;
    private boolean allowFlawedDiscovery;
    private boolean allowFlawedHierarchy;
    protected Hashtable attributes = new Hashtable();
    private String diagnosticPrefix;
    protected Hashtable instances = new Hashtable();
    private String logClassName;
    protected Constructor logConstructor = null;
    protected Class[] logConstructorSignature;
    protected Method logMethod;
    protected Class[] logMethodSignature;
    private boolean useTCCL = true;

    public LogFactoryImpl() {
        Class cls;
        Class cls2;
        Class[] clsArr = new Class[1];
        if (class$java$lang$String == null) {
            cls = class$("java.lang.String");
            class$java$lang$String = cls;
        } else {
            cls = class$java$lang$String;
        }
        clsArr[0] = cls;
        this.logConstructorSignature = clsArr;
        this.logMethod = null;
        Class[] clsArr2 = new Class[1];
        if (class$org$apache$commons$logging$LogFactory == null) {
            cls2 = class$("org.apache.commons.logging.LogFactory");
            class$org$apache$commons$logging$LogFactory = cls2;
        } else {
            cls2 = class$org$apache$commons$logging$LogFactory;
        }
        clsArr2[0] = cls2;
        this.logMethodSignature = clsArr2;
        initDiagnostics();
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Instance created.");
        }
    }

    static ClassLoader access$000() throws LogConfigurationException {
        return directGetContextClassLoader();
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x016a, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x016b, code lost:
        r8 = r4;
        r4 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0224, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0225, code lost:
        r5 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0263, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0264, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0265, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0266, code lost:
        r5 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x02b6, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x02b7, code lost:
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x02bc, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x02bd, code lost:
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x02c4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x02c5, code lost:
        r2 = r4;
        r4 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0263 A[ExcHandler: LogConfigurationException (r0v7 'e' org2.apache.commons.logging.LogConfigurationException A[CUSTOM_DECLARE]), Splitter:B:5:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0270 A[LOOP:0: B:4:0x0032->B:59:0x0270, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0220 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org2.apache.commons.logging.Log createLogFromClass(java.lang.String r10, java.lang.String r11, boolean r12) throws org2.apache.commons.logging.LogConfigurationException {
        /*
            r9 = this;
            r2 = 1
            r3 = 0
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0026
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Attempting to instantiate '"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.StringBuffer r0 = r0.append(r10)
            java.lang.String r1 = "'"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r9.logDiagnostic(r0)
        L_0x0026:
            java.lang.Object[] r6 = new java.lang.Object[r2]
            r0 = 0
            r6[r0] = r11
            java.lang.ClassLoader r0 = r9.getBaseClassLoader()
            r1 = r0
            r2 = r3
            r0 = r3
        L_0x0032:
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String r5 = "Trying to load '"
            java.lang.StringBuffer r4 = r4.append(r5)
            java.lang.StringBuffer r4 = r4.append(r10)
            java.lang.String r5 = "' from classloader "
            java.lang.StringBuffer r4 = r4.append(r5)
            java.lang.String r5 = objectId(r1)
            java.lang.StringBuffer r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r9.logDiagnostic(r4)
            boolean r4 = isDiagnosticsEnabled()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            if (r4 == 0) goto L_0x00ab
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r4.<init>()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r5 = 46
            r7 = 47
            java.lang.String r5 = r10.replace(r5, r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r5 = ".class"
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r5 = r4.toString()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            if (r1 == 0) goto L_0x0125
            java.net.URL r4 = r1.getResource(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
        L_0x0080:
            if (r4 != 0) goto L_0x013f
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r4.<init>()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "Class '"
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r4 = r4.append(r10)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "' ["
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r5 = "] cannot be found."
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r4 = r4.toString()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r9.logDiagnostic(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
        L_0x00ab:
            r4 = 1
            java.lang.Class r4 = java.lang.Class.forName(r10, r4, r1)     // Catch:{ ClassNotFoundException -> 0x01ab }
        L_0x00b0:
            java.lang.Class[] r5 = r9.logConstructorSignature     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.reflect.Constructor r5 = r4.getConstructor(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.Object r0 = r5.newInstance(r6)     // Catch:{ NoClassDefFoundError -> 0x02c0, ExceptionInInitializerError -> 0x02b9, LogConfigurationException -> 0x0263, Throwable -> 0x02b4 }
            boolean r7 = r0 instanceof org2.apache.commons.logging.Log     // Catch:{ NoClassDefFoundError -> 0x02c0, ExceptionInInitializerError -> 0x02b9, LogConfigurationException -> 0x0263, Throwable -> 0x02b4 }
            if (r7 == 0) goto L_0x021a
            org2.apache.commons.logging.Log r0 = (org2.apache.commons.logging.Log) r0     // Catch:{ NoClassDefFoundError -> 0x02c4, ExceptionInInitializerError -> 0x02bc, LogConfigurationException -> 0x0263, Throwable -> 0x02b6 }
            r2 = r4
            r4 = r0
        L_0x00c2:
            if (r2 == 0) goto L_0x0124
            if (r12 == 0) goto L_0x0124
            r9.logClassName = r10
            r9.logConstructor = r5
            java.lang.String r0 = "setLogFactory"
            java.lang.Class[] r5 = r9.logMethodSignature     // Catch:{ Throwable -> 0x0278 }
            java.lang.reflect.Method r0 = r2.getMethod(r0, r5)     // Catch:{ Throwable -> 0x0278 }
            r9.logMethod = r0     // Catch:{ Throwable -> 0x0278 }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x0278 }
            r0.<init>()     // Catch:{ Throwable -> 0x0278 }
            java.lang.String r5 = "Found method setLogFactory(LogFactory) in '"
            java.lang.StringBuffer r0 = r0.append(r5)     // Catch:{ Throwable -> 0x0278 }
            java.lang.StringBuffer r0 = r0.append(r10)     // Catch:{ Throwable -> 0x0278 }
            java.lang.String r5 = "'"
            java.lang.StringBuffer r0 = r0.append(r5)     // Catch:{ Throwable -> 0x0278 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0278 }
            r9.logDiagnostic(r0)     // Catch:{ Throwable -> 0x0278 }
        L_0x00f3:
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Log adapter '"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.StringBuffer r0 = r0.append(r10)
            java.lang.String r1 = "' from classloader "
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.ClassLoader r1 = r2.getClassLoader()
            java.lang.String r1 = objectId(r1)
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = " has been selected for use."
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r9.logDiagnostic(r0)
        L_0x0124:
            return r4
        L_0x0125:
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r4.<init>()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = ".class"
            java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r4 = r4.toString()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.net.URL r4 = java.lang.ClassLoader.getSystemResource(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            goto L_0x0080
        L_0x013f:
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r5.<init>()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "Class '"
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r5 = r5.append(r10)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "' was found at '"
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r4 = r5.append(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r5 = "'"
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r4 = r4.toString()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r9.logDiagnostic(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            goto L_0x00ab
        L_0x016a:
            r4 = move-exception
            r8 = r4
            r4 = r0
            r0 = r8
        L_0x016e:
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r6 = "The log adapter '"
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.StringBuffer r5 = r5.append(r10)
            java.lang.String r6 = "' is missing dependencies when loaded via classloader "
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r6 = objectId(r1)
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r6 = ": "
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r0 = r0.trim()
            java.lang.StringBuffer r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            r9.logDiagnostic(r0)
            r5 = r4
            r4 = r3
            goto L_0x00c2
        L_0x01ab:
            r4 = move-exception
            java.lang.String r4 = r4.getMessage()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r5.<init>()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "The log adapter '"
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r5 = r5.append(r10)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "' is not available via classloader "
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = objectId(r1)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = ": "
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r4 = r4.trim()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r4 = r5.append(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r4 = r4.toString()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r9.logDiagnostic(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.Class r4 = java.lang.Class.forName(r10)     // Catch:{ ClassNotFoundException -> 0x01eb }
            goto L_0x00b0
        L_0x01eb:
            r4 = move-exception
            java.lang.String r4 = r4.getMessage()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r5.<init>()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "The log adapter '"
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r5 = r5.append(r10)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r7 = "' is not available via the LogFactoryImpl class classloader: "
            java.lang.StringBuffer r5 = r5.append(r7)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r4 = r4.trim()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.StringBuffer r4 = r5.append(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            java.lang.String r4 = r4.toString()     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r9.logDiagnostic(r4)     // Catch:{ NoClassDefFoundError -> 0x016a, ExceptionInInitializerError -> 0x0224, LogConfigurationException -> 0x0263, Throwable -> 0x0265 }
            r5 = r0
            r4 = r3
            goto L_0x00c2
        L_0x021a:
            r9.handleFlawedHierarchy(r1, r4)     // Catch:{ NoClassDefFoundError -> 0x02c0, ExceptionInInitializerError -> 0x02b9, LogConfigurationException -> 0x0263, Throwable -> 0x02b4 }
            r4 = r5
        L_0x021e:
            if (r1 != 0) goto L_0x0270
            r5 = r4
            r4 = r3
            goto L_0x00c2
        L_0x0224:
            r4 = move-exception
            r5 = r0
            r0 = r4
        L_0x0227:
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String r6 = "The log adapter '"
            java.lang.StringBuffer r4 = r4.append(r6)
            java.lang.StringBuffer r4 = r4.append(r10)
            java.lang.String r6 = "' is unable to initialize itself when loaded via classloader "
            java.lang.StringBuffer r4 = r4.append(r6)
            java.lang.String r6 = objectId(r1)
            java.lang.StringBuffer r4 = r4.append(r6)
            java.lang.String r6 = ": "
            java.lang.StringBuffer r4 = r4.append(r6)
            java.lang.String r0 = r0.trim()
            java.lang.StringBuffer r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            r9.logDiagnostic(r0)
            r4 = r3
            goto L_0x00c2
        L_0x0263:
            r0 = move-exception
            throw r0
        L_0x0265:
            r4 = move-exception
            r5 = r0
            r0 = r4
        L_0x0268:
            handleThrowable(r0)
            r9.handleFlawedDiscovery(r10, r1, r0)
            r4 = r5
            goto L_0x021e
        L_0x0270:
            java.lang.ClassLoader r0 = r9.getParentClassLoader(r1)
            r1 = r0
            r0 = r4
            goto L_0x0032
        L_0x0278:
            r0 = move-exception
            handleThrowable(r0)
            r9.logMethod = r3
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r3 = "[INFO] '"
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.StringBuffer r0 = r0.append(r10)
            java.lang.String r3 = "' from classloader "
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r1 = objectId(r1)
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = " does not declare optional method "
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = "setLogFactory(LogFactory)"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r9.logDiagnostic(r0)
            goto L_0x00f3
        L_0x02b4:
            r0 = move-exception
            goto L_0x0268
        L_0x02b6:
            r0 = move-exception
            r2 = r4
            goto L_0x0268
        L_0x02b9:
            r0 = move-exception
            goto L_0x0227
        L_0x02bc:
            r0 = move-exception
            r2 = r4
            goto L_0x0227
        L_0x02c0:
            r0 = move-exception
            r4 = r5
            goto L_0x016e
        L_0x02c4:
            r0 = move-exception
            r2 = r4
            r4 = r5
            goto L_0x016e
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.impl.LogFactoryImpl.createLogFromClass(java.lang.String, java.lang.String, boolean):org2.apache.commons.logging.Log");
    }

    private Log discoverLogImplementation(String str) throws LogConfigurationException {
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Discovering a Log implementation...");
        }
        initConfiguration();
        Log log = null;
        String findUserSpecifiedLogClassName = findUserSpecifiedLogClassName();
        if (findUserSpecifiedLogClassName != null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("Attempting to load user-specified log class '").append(findUserSpecifiedLogClassName).append("'...").toString());
            }
            log = createLogFromClass(findUserSpecifiedLogClassName, str, true);
            if (log == null) {
                StringBuffer stringBuffer = new StringBuffer("User-specified log class '");
                stringBuffer.append(findUserSpecifiedLogClassName);
                stringBuffer.append("' cannot be found or is not useable.");
                informUponSimilarName(stringBuffer, findUserSpecifiedLogClassName, LOGGING_IMPL_LOG4J_LOGGER);
                informUponSimilarName(stringBuffer, findUserSpecifiedLogClassName, LOGGING_IMPL_JDK14_LOGGER);
                informUponSimilarName(stringBuffer, findUserSpecifiedLogClassName, LOGGING_IMPL_LUMBERJACK_LOGGER);
                informUponSimilarName(stringBuffer, findUserSpecifiedLogClassName, LOGGING_IMPL_SIMPLE_LOGGER);
                throw new LogConfigurationException(stringBuffer.toString());
            }
        } else {
            if (isDiagnosticsEnabled()) {
                logDiagnostic("No user-specified Log implementation; performing discovery using the standard supported logging implementations...");
            }
            for (int i = 0; i < classesToDiscover.length && log == null; i++) {
                log = createLogFromClass(classesToDiscover[i], str, true);
            }
            if (log == null) {
                throw new LogConfigurationException("No suitable Log implementation");
            }
        }
        return log;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String findUserSpecifiedLogClassName() {
        /*
            r4 = this;
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x000c
            java.lang.String r0 = "Trying to get log class from attribute 'org.apache.commons.logging.Log'"
            r4.logDiagnostic(r0)
        L_0x000c:
            java.lang.String r0 = "org.apache.commons.logging.Log"
            java.lang.Object r0 = r4.getAttribute(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x00a7
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0023
            java.lang.String r0 = "Trying to get log class from attribute 'org.apache.commons.logging.log'"
            r4.logDiagnostic(r0)
        L_0x0023:
            java.lang.String r0 = "org.apache.commons.logging.log"
            java.lang.Object r0 = r4.getAttribute(r0)
            java.lang.String r0 = (java.lang.String) r0
            r1 = r0
        L_0x002d:
            if (r1 != 0) goto L_0x0043
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x003b
            java.lang.String r0 = "Trying to get log class from system property 'org.apache.commons.logging.Log'"
            r4.logDiagnostic(r0)
        L_0x003b:
            java.lang.String r0 = "org.apache.commons.logging.Log"
            r2 = 0
            java.lang.String r1 = getSystemProperty(r0, r2)     // Catch:{ SecurityException -> 0x0060 }
        L_0x0043:
            if (r1 != 0) goto L_0x00a5
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0051
            java.lang.String r0 = "Trying to get log class from system property 'org.apache.commons.logging.log'"
            r4.logDiagnostic(r0)
        L_0x0051:
            java.lang.String r0 = "org.apache.commons.logging.log"
            r2 = 0
            java.lang.String r0 = getSystemProperty(r0, r2)     // Catch:{ SecurityException -> 0x0083 }
        L_0x0059:
            if (r0 == 0) goto L_0x005f
            java.lang.String r0 = r0.trim()
        L_0x005f:
            return r0
        L_0x0060:
            r0 = move-exception
            boolean r2 = isDiagnosticsEnabled()
            if (r2 == 0) goto L_0x0043
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r3 = "No access allowed to system property 'org.apache.commons.logging.Log' - "
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r4.logDiagnostic(r0)
            goto L_0x0043
        L_0x0083:
            r0 = move-exception
            boolean r2 = isDiagnosticsEnabled()
            if (r2 == 0) goto L_0x00a5
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r3 = "No access allowed to system property 'org.apache.commons.logging.log' - "
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r4.logDiagnostic(r0)
        L_0x00a5:
            r0 = r1
            goto L_0x0059
        L_0x00a7:
            r1 = r0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.impl.LogFactoryImpl.findUserSpecifiedLogClassName():java.lang.String");
    }

    private ClassLoader getBaseClassLoader() throws LogConfigurationException {
        Class cls;
        if (class$org$apache$commons$logging$impl$LogFactoryImpl == null) {
            cls = class$("org.apache.commons.logging.impl.LogFactoryImpl");
            class$org$apache$commons$logging$impl$LogFactoryImpl = cls;
        } else {
            cls = class$org$apache$commons$logging$impl$LogFactoryImpl;
        }
        ClassLoader classLoader = getClassLoader(cls);
        if (!this.useTCCL) {
            return classLoader;
        }
        ClassLoader contextClassLoaderInternal = getContextClassLoaderInternal();
        ClassLoader lowestClassLoader = getLowestClassLoader(contextClassLoaderInternal, classLoader);
        if (lowestClassLoader == null) {
            if (this.allowFlawedContext) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic("[WARNING] the context classloader is not part of a parent-child relationship with the classloader that loaded LogFactoryImpl.");
                }
                return contextClassLoaderInternal;
            }
            throw new LogConfigurationException("Bad classloader hierarchy; LogFactoryImpl was loaded via a classloader that is not related to the current context classloader.");
        } else if (lowestClassLoader == contextClassLoaderInternal) {
            return lowestClassLoader;
        } else {
            if (!this.allowFlawedContext) {
                throw new LogConfigurationException("Bad classloader hierarchy; LogFactoryImpl was loaded via a classloader that is not related to the current context classloader.");
            } else if (!isDiagnosticsEnabled()) {
                return lowestClassLoader;
            } else {
                logDiagnostic("Warning: the context classloader is an ancestor of the classloader that loaded LogFactoryImpl; it should be the same or a descendant. The application using commons-logging should ensure the context classloader is used correctly.");
                return lowestClassLoader;
            }
        }
    }

    private boolean getBooleanConfiguration(String str, boolean z) {
        String configurationValue = getConfigurationValue(str);
        return configurationValue == null ? z : Boolean.valueOf(configurationValue).booleanValue();
    }

    protected static ClassLoader getClassLoader(Class cls) {
        return LogFactory.getClassLoader(cls);
    }

    private String getConfigurationValue(String str) {
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("[ENV] Trying to get configuration for item ").append(str).toString());
        }
        Object attribute = getAttribute(str);
        if (attribute != null) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("[ENV] Found LogFactory attribute [").append(attribute).append("] for ").append(str).toString());
            }
            return attribute.toString();
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("[ENV] No LogFactory attribute found for ").append(str).toString());
        }
        try {
            String systemProperty = getSystemProperty(str, (String) null);
            if (systemProperty == null) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("[ENV] No system property found for property ").append(str).toString());
                }
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("[ENV] No configuration defined for item ").append(str).toString());
                }
                return null;
            } else if (!isDiagnosticsEnabled()) {
                return systemProperty;
            } else {
                logDiagnostic(new StringBuffer().append("[ENV] Found system property [").append(systemProperty).append("] for ").append(str).toString());
                return systemProperty;
            }
        } catch (SecurityException e) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("[ENV] Security prevented reading system property ").append(str).toString());
            }
        }
    }

    protected static ClassLoader getContextClassLoader() throws LogConfigurationException {
        return LogFactory.getContextClassLoader();
    }

    private static ClassLoader getContextClassLoaderInternal() throws LogConfigurationException {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                return LogFactoryImpl.access$000();
            }
        });
    }

    private ClassLoader getLowestClassLoader(ClassLoader classLoader, ClassLoader classLoader2) {
        if (classLoader == null) {
            return classLoader2;
        }
        if (classLoader2 == null) {
            return classLoader;
        }
        ClassLoader classLoader3 = classLoader;
        while (classLoader3 != null) {
            if (classLoader3 == classLoader2) {
                return classLoader;
            }
            classLoader3 = getParentClassLoader(classLoader3);
        }
        ClassLoader classLoader4 = classLoader2;
        while (classLoader4 != null) {
            if (classLoader4 == classLoader) {
                return classLoader2;
            }
            classLoader4 = getParentClassLoader(classLoader4);
        }
        return null;
    }

    private ClassLoader getParentClassLoader(ClassLoader classLoader) {
        try {
            return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction(this, classLoader) {

                /* renamed from: 靐  reason: contains not printable characters */
                private final LogFactoryImpl f17735;

                /* renamed from: 龘  reason: contains not printable characters */
                private final ClassLoader f17736;

                {
                    this.f17735 = r1;
                    this.f17736 = r2;
                }

                public Object run() {
                    return this.f17736.getParent();
                }
            });
        } catch (SecurityException e) {
            logDiagnostic("[SECURITY] Unable to obtain parent classloader");
            return null;
        }
    }

    private static String getSystemProperty(String str, String str2) throws SecurityException {
        return (String) AccessController.doPrivileged(new PrivilegedAction(str, str2) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f17733;

            /* renamed from: 龘  reason: contains not printable characters */
            private final String f17734;

            {
                this.f17734 = r1;
                this.f17733 = r2;
            }

            public Object run() {
                return System.getProperty(this.f17734, this.f17733);
            }
        });
    }

    private void handleFlawedDiscovery(String str, ClassLoader classLoader, Throwable th) {
        Throwable targetException;
        Throwable exception;
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Could not instantiate Log '").append(str).append("' -- ").append(th.getClass().getName()).append(": ").append(th.getLocalizedMessage()).toString());
            if ((th instanceof InvocationTargetException) && (targetException = ((InvocationTargetException) th).getTargetException()) != null) {
                logDiagnostic(new StringBuffer().append("... InvocationTargetException: ").append(targetException.getClass().getName()).append(": ").append(targetException.getLocalizedMessage()).toString());
                if ((targetException instanceof ExceptionInInitializerError) && (exception = ((ExceptionInInitializerError) targetException).getException()) != null) {
                    StringWriter stringWriter = new StringWriter();
                    exception.printStackTrace(new PrintWriter(stringWriter, true));
                    logDiagnostic(new StringBuffer().append("... ExceptionInInitializerError: ").append(stringWriter.toString()).toString());
                }
            }
        }
        if (!this.allowFlawedDiscovery) {
            throw new LogConfigurationException(th);
        }
    }

    private void handleFlawedHierarchy(ClassLoader classLoader, Class cls) throws LogConfigurationException {
        Class cls2;
        Class cls3;
        Class cls4;
        Class cls5;
        boolean z = false;
        if (class$org$apache$commons$logging$Log == null) {
            cls2 = class$("org.apache.commons.logging.Log");
            class$org$apache$commons$logging$Log = cls2;
        } else {
            cls2 = class$org$apache$commons$logging$Log;
        }
        String name = cls2.getName();
        Class[] interfaces = cls.getInterfaces();
        int i = 0;
        while (true) {
            if (i >= interfaces.length) {
                break;
            } else if (name.equals(interfaces[i].getName())) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            if (isDiagnosticsEnabled()) {
                try {
                    if (class$org$apache$commons$logging$Log == null) {
                        cls5 = class$("org.apache.commons.logging.Log");
                        class$org$apache$commons$logging$Log = cls5;
                    } else {
                        cls5 = class$org$apache$commons$logging$Log;
                    }
                    logDiagnostic(new StringBuffer().append("Class '").append(cls.getName()).append("' was found in classloader ").append(objectId(classLoader)).append(". It is bound to a Log interface which is not").append(" the one loaded from classloader ").append(objectId(getClassLoader(cls5))).toString());
                } catch (Throwable th) {
                    handleThrowable(th);
                    logDiagnostic(new StringBuffer().append("Error while trying to output diagnostics about bad class '").append(cls).append("'").toString());
                }
            }
            if (!this.allowFlawedHierarchy) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Terminating logging for this context ");
                stringBuffer.append("due to bad log hierarchy. ");
                stringBuffer.append("You have more than one version of '");
                if (class$org$apache$commons$logging$Log == null) {
                    cls4 = class$("org.apache.commons.logging.Log");
                    class$org$apache$commons$logging$Log = cls4;
                } else {
                    cls4 = class$org$apache$commons$logging$Log;
                }
                stringBuffer.append(cls4.getName());
                stringBuffer.append("' visible.");
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(stringBuffer.toString());
                }
                throw new LogConfigurationException(stringBuffer.toString());
            } else if (isDiagnosticsEnabled()) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("Warning: bad log hierarchy. ");
                stringBuffer2.append("You have more than one version of '");
                if (class$org$apache$commons$logging$Log == null) {
                    cls3 = class$("org.apache.commons.logging.Log");
                    class$org$apache$commons$logging$Log = cls3;
                } else {
                    cls3 = class$org$apache$commons$logging$Log;
                }
                stringBuffer2.append(cls3.getName());
                stringBuffer2.append("' visible.");
                logDiagnostic(stringBuffer2.toString());
            }
        } else if (!this.allowFlawedDiscovery) {
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append("Terminating logging for this context. ");
            stringBuffer3.append("Log class '");
            stringBuffer3.append(cls.getName());
            stringBuffer3.append("' does not implement the Log interface.");
            if (isDiagnosticsEnabled()) {
                logDiagnostic(stringBuffer3.toString());
            }
            throw new LogConfigurationException(stringBuffer3.toString());
        } else if (isDiagnosticsEnabled()) {
            StringBuffer stringBuffer4 = new StringBuffer();
            stringBuffer4.append("[WARNING] Log class '");
            stringBuffer4.append(cls.getName());
            stringBuffer4.append("' does not implement the Log interface.");
            logDiagnostic(stringBuffer4.toString());
        }
    }

    private void informUponSimilarName(StringBuffer stringBuffer, String str, String str2) {
        if (!str.equals(str2)) {
            if (str.regionMatches(true, 0, str2, 0, PKG_LEN + 5)) {
                stringBuffer.append(" Did you mean '");
                stringBuffer.append(str2);
                stringBuffer.append("'?");
            }
        }
    }

    private void initConfiguration() {
        this.allowFlawedContext = getBooleanConfiguration("org.apache.commons.logging.Log.allowFlawedContext", true);
        this.allowFlawedDiscovery = getBooleanConfiguration("org.apache.commons.logging.Log.allowFlawedDiscovery", true);
        this.allowFlawedHierarchy = getBooleanConfiguration("org.apache.commons.logging.Log.allowFlawedHierarchy", true);
    }

    private void initDiagnostics() {
        String str;
        ClassLoader classLoader = getClassLoader(getClass());
        if (classLoader == null) {
            str = "BOOTLOADER";
        } else {
            try {
                str = objectId(classLoader);
            } catch (SecurityException e) {
                str = "UNKNOWN";
            }
        }
        this.diagnosticPrefix = new StringBuffer().append("[LogFactoryImpl@").append(System.identityHashCode(this)).append(" from ").append(str).append("] ").toString();
    }

    protected static boolean isDiagnosticsEnabled() {
        return LogFactory.isDiagnosticsEnabled();
    }

    private boolean isLogLibraryAvailable(String str, String str2) {
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Checking for '").append(str).append("'.").toString());
        }
        try {
            if (createLogFromClass(str2, getClass().getName(), false) != null) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic(new StringBuffer().append("Found '").append(str).append("'.").toString());
                }
                return true;
            } else if (!isDiagnosticsEnabled()) {
                return false;
            } else {
                logDiagnostic(new StringBuffer().append("Did not find '").append(str).append("'.").toString());
                return false;
            }
        } catch (LogConfigurationException e) {
            if (!isDiagnosticsEnabled()) {
                return false;
            }
            logDiagnostic(new StringBuffer().append("Logging system '").append(str).append("' is available but not useable.").toString());
            return false;
        }
    }

    public Object getAttribute(String str) {
        return this.attributes.get(str);
    }

    public String[] getAttributeNames() {
        return (String[]) this.attributes.keySet().toArray(new String[this.attributes.size()]);
    }

    public Log getInstance(Class cls) throws LogConfigurationException {
        return getInstance(cls.getName());
    }

    public Log getInstance(String str) throws LogConfigurationException {
        Log log = (Log) this.instances.get(str);
        if (log != null) {
            return log;
        }
        Log newInstance = newInstance(str);
        this.instances.put(str, newInstance);
        return newInstance;
    }

    /* access modifiers changed from: protected */
    public String getLogClassName() {
        if (this.logClassName == null) {
            discoverLogImplementation(getClass().getName());
        }
        return this.logClassName;
    }

    /* access modifiers changed from: protected */
    public Constructor getLogConstructor() throws LogConfigurationException {
        if (this.logConstructor == null) {
            discoverLogImplementation(getClass().getName());
        }
        return this.logConstructor;
    }

    /* access modifiers changed from: protected */
    public boolean isJdk13LumberjackAvailable() {
        return isLogLibraryAvailable("Jdk13Lumberjack", LOGGING_IMPL_LUMBERJACK_LOGGER);
    }

    /* access modifiers changed from: protected */
    public boolean isJdk14Available() {
        return isLogLibraryAvailable("Jdk14", LOGGING_IMPL_JDK14_LOGGER);
    }

    /* access modifiers changed from: protected */
    public boolean isLog4JAvailable() {
        return isLogLibraryAvailable("Log4J", LOGGING_IMPL_LOG4J_LOGGER);
    }

    /* access modifiers changed from: protected */
    public void logDiagnostic(String str) {
        if (isDiagnosticsEnabled()) {
            logRawDiagnostic(new StringBuffer().append(this.diagnosticPrefix).append(str).toString());
        }
    }

    /* JADX WARNING: type inference failed for: r1v1, types: [java.lang.Throwable] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org2.apache.commons.logging.Log newInstance(java.lang.String r4) throws org2.apache.commons.logging.LogConfigurationException {
        /*
            r3 = this;
            java.lang.reflect.Constructor r0 = r3.logConstructor     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            if (r0 != 0) goto L_0x0018
            org2.apache.commons.logging.Log r0 = r3.discoverLogImplementation(r4)     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
        L_0x0008:
            java.lang.reflect.Method r1 = r3.logMethod     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            if (r1 == 0) goto L_0x0017
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            r2 = 0
            r1[r2] = r3     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            java.lang.reflect.Method r2 = r3.logMethod     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            r2.invoke(r0, r1)     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
        L_0x0017:
            return r0
        L_0x0018:
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            r1 = 0
            r0[r1] = r4     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            java.lang.reflect.Constructor r1 = r3.logConstructor     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            org2.apache.commons.logging.Log r0 = (org2.apache.commons.logging.Log) r0     // Catch:{ LogConfigurationException -> 0x0027, InvocationTargetException -> 0x0029, Throwable -> 0x0038 }
            goto L_0x0008
        L_0x0027:
            r0 = move-exception
            throw r0
        L_0x0029:
            r0 = move-exception
            java.lang.Throwable r1 = r0.getTargetException()
            org2.apache.commons.logging.LogConfigurationException r2 = new org2.apache.commons.logging.LogConfigurationException
            if (r1 != 0) goto L_0x0036
        L_0x0032:
            r2.<init>((java.lang.Throwable) r0)
            throw r2
        L_0x0036:
            r0 = r1
            goto L_0x0032
        L_0x0038:
            r0 = move-exception
            handleThrowable(r0)
            org2.apache.commons.logging.LogConfigurationException r1 = new org2.apache.commons.logging.LogConfigurationException
            r1.<init>((java.lang.Throwable) r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.impl.LogFactoryImpl.newInstance(java.lang.String):org2.apache.commons.logging.Log");
    }

    public void release() {
        logDiagnostic("Releasing all known loggers");
        this.instances.clear();
    }

    public void removeAttribute(String str) {
        this.attributes.remove(str);
    }

    public void setAttribute(String str, Object obj) {
        if (this.logConstructor != null) {
            logDiagnostic("setAttribute: call too late; configuration already performed.");
        }
        if (obj == null) {
            this.attributes.remove(str);
        } else {
            this.attributes.put(str, obj);
        }
        if (str.equals("use_tccl")) {
            this.useTCCL = obj != null && Boolean.valueOf(obj.toString()).booleanValue();
        }
    }
}
