package org2.apache.commons.logging.impl;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

public final class WeakHashtable extends Hashtable {
    private static final int MAX_CHANGES_BEFORE_PURGE = 100;
    private static final int PARTIAL_PURGE_COUNT = 10;
    private static final long serialVersionUID = -1546036869799732453L;
    private int changeCount = 0;
    private final ReferenceQueue queue = new ReferenceQueue();

    private static final class Entry implements Map.Entry {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Object f17740;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Object f17741;

        private Entry(Object obj, Object obj2) {
            this.f17741 = obj;
            this.f17740 = obj2;
        }

        Entry(Object obj, Object obj2, AnonymousClass1 r3) {
            this(obj, obj2);
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0021 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                r0 = 0
                if (r4 == 0) goto L_0x0022
                boolean r1 = r4 instanceof java.util.Map.Entry
                if (r1 == 0) goto L_0x0022
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                java.lang.Object r1 = r3.getKey()
                if (r1 != 0) goto L_0x0023
                java.lang.Object r1 = r4.getKey()
                if (r1 != 0) goto L_0x0022
            L_0x0015:
                java.lang.Object r1 = r3.getValue()
                if (r1 != 0) goto L_0x0032
                java.lang.Object r1 = r4.getValue()
                if (r1 != 0) goto L_0x0022
            L_0x0021:
                r0 = 1
            L_0x0022:
                return r0
            L_0x0023:
                java.lang.Object r1 = r3.getKey()
                java.lang.Object r2 = r4.getKey()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x0022
                goto L_0x0015
            L_0x0032:
                java.lang.Object r1 = r3.getValue()
                java.lang.Object r2 = r4.getValue()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x0022
                goto L_0x0021
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.impl.WeakHashtable.Entry.equals(java.lang.Object):boolean");
        }

        public Object getKey() {
            return this.f17741;
        }

        public Object getValue() {
            return this.f17740;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = getKey() == null ? 0 : getKey().hashCode();
            if (getValue() != null) {
                i = getValue().hashCode();
            }
            return hashCode ^ i;
        }

        public Object setValue(Object obj) {
            throw new UnsupportedOperationException("Entry.setValue is not supported.");
        }
    }

    private static final class Referenced {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f17742;

        /* renamed from: 龘  reason: contains not printable characters */
        private final WeakReference f17743;

        private Referenced(Object obj) {
            this.f17743 = new WeakReference(obj);
            this.f17742 = obj.hashCode();
        }

        private Referenced(Object obj, ReferenceQueue referenceQueue) {
            this.f17743 = new WeakKey(obj, referenceQueue, this, (AnonymousClass1) null);
            this.f17742 = obj.hashCode();
        }

        Referenced(Object obj, ReferenceQueue referenceQueue, AnonymousClass1 r3) {
            this(obj, referenceQueue);
        }

        Referenced(Object obj, AnonymousClass1 r2) {
            this(obj);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Object m22669() {
            return this.f17743.get();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Object m22670(Referenced referenced) {
            return referenced.m22669();
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Referenced)) {
                return false;
            }
            Referenced referenced = (Referenced) obj;
            Object r2 = m22669();
            Object r3 = referenced.m22669();
            if (r2 != null) {
                return r2.equals(r3);
            }
            return (r3 == null) && hashCode() == referenced.hashCode();
        }

        public int hashCode() {
            return this.f17742;
        }
    }

    private static final class WeakKey extends WeakReference {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Referenced f17744;

        private WeakKey(Object obj, ReferenceQueue referenceQueue, Referenced referenced) {
            super(obj, referenceQueue);
            this.f17744 = referenced;
        }

        WeakKey(Object obj, ReferenceQueue referenceQueue, Referenced referenced, AnonymousClass1 r4) {
            this(obj, referenceQueue, referenced);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private Referenced m22671() {
            return this.f17744;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        static Referenced m22672(WeakKey weakKey) {
            return weakKey.m22671();
        }
    }

    private void purge() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.queue) {
            while (true) {
                WeakKey weakKey = (WeakKey) this.queue.poll();
                if (weakKey == null) {
                    break;
                }
                arrayList.add(WeakKey.m22672(weakKey));
            }
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            super.remove(arrayList.get(i));
        }
    }

    private void purgeOne() {
        synchronized (this.queue) {
            WeakKey weakKey = (WeakKey) this.queue.poll();
            if (weakKey != null) {
                super.remove(WeakKey.m22672(weakKey));
            }
        }
    }

    public boolean containsKey(Object obj) {
        return super.containsKey(new Referenced(obj, (AnonymousClass1) null));
    }

    public Enumeration elements() {
        purge();
        return super.elements();
    }

    public Set entrySet() {
        purge();
        Set<Map.Entry> entrySet = super.entrySet();
        HashSet hashSet = new HashSet();
        for (Map.Entry entry : entrySet) {
            Object r1 = Referenced.m22670((Referenced) entry.getKey());
            Object value = entry.getValue();
            if (r1 != null) {
                hashSet.add(new Entry(r1, value, (AnonymousClass1) null));
            }
        }
        return hashSet;
    }

    public Object get(Object obj) {
        return super.get(new Referenced(obj, (AnonymousClass1) null));
    }

    public boolean isEmpty() {
        purge();
        return super.isEmpty();
    }

    public Set keySet() {
        purge();
        Set<Referenced> keySet = super.keySet();
        HashSet hashSet = new HashSet();
        for (Referenced r0 : keySet) {
            Object r02 = Referenced.m22670(r0);
            if (r02 != null) {
                hashSet.add(r02);
            }
        }
        return hashSet;
    }

    public Enumeration keys() {
        purge();
        return new Enumeration(this, super.keys()) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final WeakHashtable f17738;

            /* renamed from: 龘  reason: contains not printable characters */
            private final Enumeration f17739;

            {
                this.f17738 = r1;
                this.f17739 = r2;
            }

            public boolean hasMoreElements() {
                return this.f17739.hasMoreElements();
            }

            public Object nextElement() {
                return Referenced.m22670((Referenced) this.f17739.nextElement());
            }
        };
    }

    public synchronized Object put(Object obj, Object obj2) {
        if (obj == null) {
            throw new NullPointerException("Null keys are not allowed");
        } else if (obj2 == null) {
            throw new NullPointerException("Null values are not allowed");
        } else {
            int i = this.changeCount;
            this.changeCount = i + 1;
            if (i > 100) {
                purge();
                this.changeCount = 0;
            } else if (this.changeCount % 10 == 0) {
                purgeOne();
            }
        }
        return super.put(new Referenced(obj, this.queue, (AnonymousClass1) null), obj2);
    }

    public void putAll(Map map) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                put(entry.getKey(), entry.getValue());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void rehash() {
        purge();
        super.rehash();
    }

    public synchronized Object remove(Object obj) {
        int i = this.changeCount;
        this.changeCount = i + 1;
        if (i > 100) {
            purge();
            this.changeCount = 0;
        } else if (this.changeCount % 10 == 0) {
            purgeOne();
        }
        return super.remove(new Referenced(obj, (AnonymousClass1) null));
    }

    public int size() {
        purge();
        return super.size();
    }

    public String toString() {
        purge();
        return super.toString();
    }

    public Collection values() {
        purge();
        return super.values();
    }
}
