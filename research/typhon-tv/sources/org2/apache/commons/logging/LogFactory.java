package org2.apache.commons.logging;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public abstract class LogFactory {
    public static final String DIAGNOSTICS_DEST_PROPERTY = "org.apache.commons.logging.diagnostics.dest";
    public static final String FACTORY_DEFAULT = "org.apache.commons.logging.impl.LogFactoryImpl";
    public static final String FACTORY_PROPERTIES = "commons-logging.properties";
    public static final String FACTORY_PROPERTY = "org.apache.commons.logging.LogFactory";
    public static final String HASHTABLE_IMPLEMENTATION_PROPERTY = "org.apache.commons.logging.LogFactory.HashtableImpl";
    public static final String PRIORITY_KEY = "priority";
    protected static final String SERVICE_ID = "META-INF/services/org.apache.commons.logging.LogFactory";
    public static final String TCCL_KEY = "use_tccl";
    private static final String WEAK_HASHTABLE_CLASSNAME = "org.apache.commons.logging.impl.WeakHashtable";
    static Class class$org$apache$commons$logging$LogFactory;
    private static final String diagnosticPrefix;
    private static PrintStream diagnosticsStream;
    protected static Hashtable factories;
    protected static volatile LogFactory nullClassLoaderFactory = null;
    private static final ClassLoader thisClassLoader;

    static {
        Class cls;
        String str;
        Class cls2;
        diagnosticsStream = null;
        factories = null;
        if (class$org$apache$commons$logging$LogFactory == null) {
            cls = class$("org.apache.commons.logging.LogFactory");
            class$org$apache$commons$logging$LogFactory = cls;
        } else {
            cls = class$org$apache$commons$logging$LogFactory;
        }
        thisClassLoader = getClassLoader(cls);
        try {
            str = thisClassLoader == null ? "BOOTLOADER" : objectId(thisClassLoader);
        } catch (SecurityException e) {
            str = "UNKNOWN";
        }
        diagnosticPrefix = new StringBuffer().append("[LogFactory from ").append(str).append("] ").toString();
        diagnosticsStream = initDiagnostics();
        if (class$org$apache$commons$logging$LogFactory == null) {
            cls2 = class$("org.apache.commons.logging.LogFactory");
            class$org$apache$commons$logging$LogFactory = cls2;
        } else {
            cls2 = class$org$apache$commons$logging$LogFactory;
        }
        logClassLoaderEnvironment(cls2);
        factories = createFactoryStore();
        if (isDiagnosticsEnabled()) {
            logDiagnostic("BOOTSTRAP COMPLETED");
        }
    }

    protected LogFactory() {
    }

    static void access$000(String str) {
        logDiagnostic(str);
    }

    private static void cacheFactory(ClassLoader classLoader, LogFactory logFactory) {
        if (logFactory == null) {
            return;
        }
        if (classLoader == null) {
            nullClassLoaderFactory = logFactory;
        } else {
            factories.put(classLoader, logFactory);
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00b5, code lost:
        if (r5 == thisClassLoader) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00bb, code lost:
        if (isDiagnosticsEnabled() != false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00bd, code lost:
        logDiagnostic(new java.lang.StringBuffer().append("Unable to locate any class called '").append(r4).append("' via classloader ").append(objectId(r5)).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e3, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0115, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0118, code lost:
        if (r5 == thisClassLoader) goto L_0x011a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x011e, code lost:
        if (isDiagnosticsEnabled() != false) goto L_0x0120;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0120, code lost:
        logDiagnostic(new java.lang.StringBuffer().append("Class '").append(r4).append("' cannot be loaded").append(" via classloader ").append(objectId(r5)).append(" - it depends on some other class that cannot be found.").toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0154, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0158, code lost:
        if (r5 == thisClassLoader) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x015a, code lost:
        r2 = implementsLogFactory((java.lang.Class) null);
        r3 = new java.lang.StringBuffer();
        r3.append("The application has specified that a custom LogFactory implementation ");
        r3.append("should be used but Class '");
        r3.append(r4);
        r3.append("' cannot be converted to '");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x017a, code lost:
        if (class$org$apache$commons$logging$LogFactory == null) goto L_0x017c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x017c, code lost:
        r0 = class$("org.apache.commons.logging.LogFactory");
        class$org$apache$commons$logging$LogFactory = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0185, code lost:
        r3.append(r0.getName());
        r3.append("'. ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0192, code lost:
        if (r2 != false) goto L_0x0194;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0194, code lost:
        r3.append("The conflict is caused by the presence of multiple LogFactory classes ");
        r3.append("in incompatible classloaders. ");
        r3.append("Background can be found in http://commons.apache.org/logging/tech.html. ");
        r3.append("If you have not explicitly specified a custom LogFactory then it is likely ");
        r3.append("that the container has set one without your knowledge. ");
        r3.append("In this case, consider using the commons-logging-adapters.jar file or ");
        r3.append("specifying the standard LogFactory from the command line. ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01be, code lost:
        r3.append("Help can be found @http://commons.apache.org/logging/troubleshooting.html.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01c8, code lost:
        if (isDiagnosticsEnabled() != false) goto L_0x01ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01ca, code lost:
        logDiagnostic(r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01da, code lost:
        throw new java.lang.ClassCastException(r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01db, code lost:
        r0 = class$org$apache$commons$logging$LogFactory;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01de, code lost:
        r3.append("Please check the custom implementation. ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0224, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0225, code lost:
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b2 A[ExcHandler: ClassNotFoundException (r0v45 'e' java.lang.ClassNotFoundException A[CUSTOM_DECLARE]), Splitter:B:2:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0115 A[ExcHandler: NoClassDefFoundError (r0v33 'e' java.lang.NoClassDefFoundError A[CUSTOM_DECLARE]), Splitter:B:2:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0155 A[Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155, Exception -> 0x00e4 }, ExcHandler: ClassCastException (e java.lang.ClassCastException), Splitter:B:2:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static java.lang.Object createFactory(java.lang.String r4, java.lang.ClassLoader r5) {
        /*
            r1 = 0
            if (r5 == 0) goto L_0x01e5
            java.lang.Class r1 = r5.loadClass(r4)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155, Exception -> 0x0224 }
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            if (r0 != 0) goto L_0x0051
            java.lang.String r0 = "org.apache.commons.logging.LogFactory"
            java.lang.Class r0 = class$(r0)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            class$org$apache$commons$logging$LogFactory = r0     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
        L_0x0014:
            boolean r0 = r0.isAssignableFrom(r1)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            if (r0 == 0) goto L_0x0054
            boolean r0 = isDiagnosticsEnabled()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            if (r0 == 0) goto L_0x004a
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            r0.<init>()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = "Loaded class "
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = r1.getName()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = " from classloader "
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = objectId(r5)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            logDiagnostic(r0)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
        L_0x004a:
            java.lang.Object r0 = r1.newInstance()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            org2.apache.commons.logging.LogFactory r0 = (org2.apache.commons.logging.LogFactory) r0     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
        L_0x0050:
            return r0
        L_0x0051:
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            goto L_0x0014
        L_0x0054:
            boolean r0 = isDiagnosticsEnabled()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            if (r0 == 0) goto L_0x004a
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            r0.<init>()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = "Factory class "
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = r1.getName()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = " loaded from classloader "
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.ClassLoader r2 = r1.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = objectId(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = " does not extend '"
            java.lang.StringBuffer r2 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            if (r0 != 0) goto L_0x0112
            java.lang.String r0 = "org.apache.commons.logging.LogFactory"
            java.lang.Class r0 = class$(r0)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            class$org$apache$commons$logging$LogFactory = r0     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
        L_0x0095:
            java.lang.String r0 = r0.getName()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r2 = "' as loaded by this classloader."
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            logDiagnostic(r0)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            java.lang.String r0 = "[BAD CL TREE] "
            logHierarchy(r0, r5)     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            goto L_0x004a
        L_0x00b2:
            r0 = move-exception
            java.lang.ClassLoader r2 = thisClassLoader     // Catch:{ Exception -> 0x00e4 }
            if (r5 != r2) goto L_0x01e5
            boolean r2 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x00e4 }
            if (r2 == 0) goto L_0x00e3
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00e4 }
            r2.<init>()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = "Unable to locate any class called '"
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuffer r2 = r2.append(r4)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = "' via classloader "
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = objectId(r5)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00e4 }
            logDiagnostic(r2)     // Catch:{ Exception -> 0x00e4 }
        L_0x00e3:
            throw r0     // Catch:{ Exception -> 0x00e4 }
        L_0x00e4:
            r0 = move-exception
            r2 = r1
            r1 = r0
        L_0x00e7:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x00f3
            java.lang.String r0 = "Unable to create LogFactory instance."
            logDiagnostic(r0)
        L_0x00f3:
            if (r2 == 0) goto L_0x021d
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory
            if (r0 != 0) goto L_0x0219
            java.lang.String r0 = "org.apache.commons.logging.LogFactory"
            java.lang.Class r0 = class$(r0)
            class$org$apache$commons$logging$LogFactory = r0
        L_0x0102:
            boolean r0 = r0.isAssignableFrom(r2)
            if (r0 != 0) goto L_0x021d
            org2.apache.commons.logging.LogConfigurationException r0 = new org2.apache.commons.logging.LogConfigurationException
            java.lang.String r2 = "The chosen LogFactory implementation does not extend LogFactory. Please check your configuration."
            r0.<init>(r2, r1)
            goto L_0x0050
        L_0x0112:
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory     // Catch:{ ClassNotFoundException -> 0x00b2, NoClassDefFoundError -> 0x0115, ClassCastException -> 0x0155 }
            goto L_0x0095
        L_0x0115:
            r0 = move-exception
            java.lang.ClassLoader r2 = thisClassLoader     // Catch:{ Exception -> 0x00e4 }
            if (r5 != r2) goto L_0x01e5
            boolean r2 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x00e4 }
            if (r2 == 0) goto L_0x0154
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00e4 }
            r2.<init>()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = "Class '"
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuffer r2 = r2.append(r4)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = "' cannot be loaded"
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = " via classloader "
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = objectId(r5)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r3 = " - it depends on some other class that cannot be found."
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00e4 }
            logDiagnostic(r2)     // Catch:{ Exception -> 0x00e4 }
        L_0x0154:
            throw r0     // Catch:{ Exception -> 0x00e4 }
        L_0x0155:
            r0 = move-exception
            java.lang.ClassLoader r0 = thisClassLoader     // Catch:{ Exception -> 0x00e4 }
            if (r5 != r0) goto L_0x01e5
            boolean r2 = implementsLogFactory(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00e4 }
            r3.<init>()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "The application has specified that a custom LogFactory implementation "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "should be used but Class '"
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            r3.append(r4)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "' cannot be converted to '"
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory     // Catch:{ Exception -> 0x00e4 }
            if (r0 != 0) goto L_0x01db
            java.lang.String r0 = "org.apache.commons.logging.LogFactory"
            java.lang.Class r0 = class$(r0)     // Catch:{ Exception -> 0x00e4 }
            class$org$apache$commons$logging$LogFactory = r0     // Catch:{ Exception -> 0x00e4 }
        L_0x0185:
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x00e4 }
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "'. "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            if (r2 == 0) goto L_0x01de
            java.lang.String r0 = "The conflict is caused by the presence of multiple LogFactory classes "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "in incompatible classloaders. "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "Background can be found in http://commons.apache.org/logging/tech.html. "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "If you have not explicitly specified a custom LogFactory then it is likely "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "that the container has set one without your knowledge. "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "In this case, consider using the commons-logging-adapters.jar file or "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = "specifying the standard LogFactory from the command line. "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
        L_0x01be:
            java.lang.String r0 = "Help can be found @http://commons.apache.org/logging/troubleshooting.html."
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            boolean r0 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x00e4 }
            if (r0 == 0) goto L_0x01d1
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x00e4 }
            logDiagnostic(r0)     // Catch:{ Exception -> 0x00e4 }
        L_0x01d1:
            java.lang.ClassCastException r0 = new java.lang.ClassCastException     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x00e4 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00e4 }
            throw r0     // Catch:{ Exception -> 0x00e4 }
        L_0x01db:
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory     // Catch:{ Exception -> 0x00e4 }
            goto L_0x0185
        L_0x01de:
            java.lang.String r0 = "Please check the custom implementation. "
            r3.append(r0)     // Catch:{ Exception -> 0x00e4 }
            goto L_0x01be
        L_0x01e5:
            boolean r0 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x00e4 }
            if (r0 == 0) goto L_0x020d
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00e4 }
            r0.<init>()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r2 = "Unable to load factory class via classloader "
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r2 = objectId(r5)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r2 = " - trying the classloader associated with this LogFactory."
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00e4 }
            logDiagnostic(r0)     // Catch:{ Exception -> 0x00e4 }
        L_0x020d:
            java.lang.Class r1 = java.lang.Class.forName(r4)     // Catch:{ Exception -> 0x00e4 }
            java.lang.Object r0 = r1.newInstance()     // Catch:{ Exception -> 0x00e4 }
            org2.apache.commons.logging.LogFactory r0 = (org2.apache.commons.logging.LogFactory) r0     // Catch:{ Exception -> 0x00e4 }
            goto L_0x0050
        L_0x0219:
            java.lang.Class r0 = class$org$apache$commons$logging$LogFactory
            goto L_0x0102
        L_0x021d:
            org2.apache.commons.logging.LogConfigurationException r0 = new org2.apache.commons.logging.LogConfigurationException
            r0.<init>((java.lang.Throwable) r1)
            goto L_0x0050
        L_0x0224:
            r0 = move-exception
            r2 = r1
            r1 = r0
            goto L_0x00e7
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.LogFactory.createFactory(java.lang.String, java.lang.ClassLoader):java.lang.Object");
    }

    private static final Hashtable createFactoryStore() {
        String str;
        Hashtable hashtable;
        try {
            str = getSystemProperty("org.apache.commons.logging.LogFactory.HashtableImpl", (String) null);
        } catch (SecurityException e) {
            str = null;
        }
        String str2 = str == null ? WEAK_HASHTABLE_CLASSNAME : str;
        try {
            hashtable = (Hashtable) Class.forName(str2).newInstance();
        } catch (Throwable th) {
            handleThrowable(th);
            if (!WEAK_HASHTABLE_CLASSNAME.equals(str2)) {
                if (isDiagnosticsEnabled()) {
                    logDiagnostic("[ERROR] LogFactory: Load of custom hashtable failed");
                    hashtable = null;
                } else {
                    System.err.println("[ERROR] LogFactory: Load of custom hashtable failed");
                }
            }
            hashtable = null;
        }
        return hashtable == null ? new Hashtable() : hashtable;
    }

    protected static ClassLoader directGetContextClassLoader() throws LogConfigurationException {
        try {
            return Thread.currentThread().getContextClassLoader();
        } catch (SecurityException e) {
            return null;
        }
    }

    private static LogFactory getCachedFactory(ClassLoader classLoader) {
        return classLoader == null ? nullClassLoaderFactory : (LogFactory) factories.get(classLoader);
    }

    protected static ClassLoader getClassLoader(Class cls) {
        try {
            return cls.getClassLoader();
        } catch (SecurityException e) {
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("Unable to get classloader for class '").append(cls).append("' due to security restrictions - ").append(e.getMessage()).toString());
            }
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0123  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final java.util.Properties getConfigurationFile(java.lang.ClassLoader r13, java.lang.String r14) {
        /*
            r7 = 0
            r8 = 0
            java.util.Enumeration r10 = getResources(r13, r14)     // Catch:{ SecurityException -> 0x010f }
            if (r10 != 0) goto L_0x017b
        L_0x0009:
            return r7
        L_0x000a:
            boolean r0 = r10.hasMoreElements()     // Catch:{ SecurityException -> 0x0173 }
            if (r0 == 0) goto L_0x011d
            java.lang.Object r0 = r10.nextElement()     // Catch:{ SecurityException -> 0x0173 }
            java.net.URL r0 = (java.net.URL) r0     // Catch:{ SecurityException -> 0x0173 }
            java.util.Properties r6 = getProperties(r0)     // Catch:{ SecurityException -> 0x0173 }
            if (r6 == 0) goto L_0x010a
            if (r7 != 0) goto L_0x005f
            java.lang.String r1 = "priority"
            java.lang.String r1 = r6.getProperty(r1)     // Catch:{ SecurityException -> 0x016f }
            if (r1 == 0) goto L_0x0178
            double r2 = java.lang.Double.parseDouble(r1)     // Catch:{ SecurityException -> 0x016f }
        L_0x002b:
            boolean r1 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x016f }
            if (r1 == 0) goto L_0x005a
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x016f }
            r1.<init>()     // Catch:{ SecurityException -> 0x016f }
            java.lang.String r4 = "[LOOKUP] Properties file found at '"
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ SecurityException -> 0x016f }
            java.lang.StringBuffer r1 = r1.append(r0)     // Catch:{ SecurityException -> 0x016f }
            java.lang.String r4 = "'"
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ SecurityException -> 0x016f }
            java.lang.String r4 = " with priority "
            java.lang.StringBuffer r1 = r1.append(r4)     // Catch:{ SecurityException -> 0x016f }
            java.lang.StringBuffer r1 = r1.append(r2)     // Catch:{ SecurityException -> 0x016f }
            java.lang.String r1 = r1.toString()     // Catch:{ SecurityException -> 0x016f }
            logDiagnostic(r1)     // Catch:{ SecurityException -> 0x016f }
        L_0x005a:
            r1 = r6
        L_0x005b:
            r4 = r2
            r7 = r1
            r1 = r0
            goto L_0x000a
        L_0x005f:
            java.lang.String r2 = "priority"
            java.lang.String r2 = r6.getProperty(r2)     // Catch:{ SecurityException -> 0x0173 }
            if (r2 == 0) goto L_0x0175
            double r2 = java.lang.Double.parseDouble(r2)     // Catch:{ SecurityException -> 0x0173 }
        L_0x006c:
            int r11 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r11 <= 0) goto L_0x00be
            boolean r11 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0173 }
            if (r11 == 0) goto L_0x00bc
            java.lang.StringBuffer r11 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x0173 }
            r11.<init>()     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r12 = "[LOOKUP] Properties file at '"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r11 = r11.append(r0)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r12 = "'"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r12 = " with priority "
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r11 = r11.append(r2)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r12 = " overrides file at '"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r11 = r11.append(r1)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r12 = "'"
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r12 = " with priority "
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r4 = r11.append(r4)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r4 = r4.toString()     // Catch:{ SecurityException -> 0x0173 }
            logDiagnostic(r4)     // Catch:{ SecurityException -> 0x0173 }
        L_0x00bc:
            r1 = r6
            goto L_0x005b
        L_0x00be:
            boolean r6 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0173 }
            if (r6 == 0) goto L_0x010a
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x0173 }
            r6.<init>()     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r11 = "[LOOKUP] Properties file at '"
            java.lang.StringBuffer r6 = r6.append(r11)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r0 = r6.append(r0)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r6 = "'"
            java.lang.StringBuffer r0 = r0.append(r6)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r6 = " with priority "
            java.lang.StringBuffer r0 = r0.append(r6)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r2 = " does not override file at '"
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r2 = "'"
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r2 = " with priority "
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.StringBuffer r0 = r0.append(r4)     // Catch:{ SecurityException -> 0x0173 }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x0173 }
            logDiagnostic(r0)     // Catch:{ SecurityException -> 0x0173 }
        L_0x010a:
            r0 = r1
            r2 = r4
            r1 = r7
            goto L_0x005b
        L_0x010f:
            r0 = move-exception
            r1 = r7
        L_0x0111:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x011d
            java.lang.String r0 = "SecurityException thrown while trying to find/read config files."
            logDiagnostic(r0)
        L_0x011d:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0009
            if (r7 != 0) goto L_0x0145
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "[LOOKUP] No properties file of name '"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.StringBuffer r0 = r0.append(r14)
            java.lang.String r1 = "' found."
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            logDiagnostic(r0)
            goto L_0x0009
        L_0x0145:
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r2 = "[LOOKUP] Properties file of name '"
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.StringBuffer r0 = r0.append(r14)
            java.lang.String r2 = "' found at '"
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.StringBuffer r0 = r0.append(r1)
            r1 = 34
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            logDiagnostic(r0)
            goto L_0x0009
        L_0x016f:
            r1 = move-exception
            r1 = r0
            r7 = r6
            goto L_0x0111
        L_0x0173:
            r0 = move-exception
            goto L_0x0111
        L_0x0175:
            r2 = r8
            goto L_0x006c
        L_0x0178:
            r2 = r8
            goto L_0x002b
        L_0x017b:
            r1 = r7
            r4 = r8
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.LogFactory.getConfigurationFile(java.lang.ClassLoader, java.lang.String):java.util.Properties");
    }

    protected static ClassLoader getContextClassLoader() throws LogConfigurationException {
        return directGetContextClassLoader();
    }

    private static ClassLoader getContextClassLoaderInternal() throws LogConfigurationException {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                return LogFactory.directGetContextClassLoader();
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0049, code lost:
        r0 = r4.getProperty("use_tccl");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org2.apache.commons.logging.LogFactory getFactory() throws org2.apache.commons.logging.LogConfigurationException {
        /*
            java.lang.ClassLoader r1 = getContextClassLoaderInternal()
            if (r1 != 0) goto L_0x0012
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0012
            java.lang.String r0 = "Context classloader is null."
            logDiagnostic(r0)
        L_0x0012:
            org2.apache.commons.logging.LogFactory r2 = getCachedFactory(r1)
            if (r2 == 0) goto L_0x0019
        L_0x0018:
            return r2
        L_0x0019:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0040
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r3 = "[LOOKUP] LogFactory implementation requested for the first time for context classloader "
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r3 = objectId(r1)
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            logDiagnostic(r0)
            java.lang.String r0 = "[LOOKUP] "
            logHierarchy(r0, r1)
        L_0x0040:
            java.lang.String r0 = "commons-logging.properties"
            java.util.Properties r4 = getConfigurationFile(r1, r0)
            if (r4 == 0) goto L_0x0259
            java.lang.String r0 = "use_tccl"
            java.lang.String r0 = r4.getProperty(r0)
            if (r0 == 0) goto L_0x0259
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0259
            java.lang.ClassLoader r0 = thisClassLoader
        L_0x005e:
            boolean r3 = isDiagnosticsEnabled()
            if (r3 == 0) goto L_0x006a
            java.lang.String r3 = "[LOOKUP] Looking for system property [org.apache.commons.logging.LogFactory] to define the LogFactory subclass to use..."
            logDiagnostic(r3)
        L_0x006a:
            java.lang.String r3 = "org.apache.commons.logging.LogFactory"
            r5 = 0
            java.lang.String r3 = getSystemProperty(r3, r5)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            if (r3 == 0) goto L_0x0187
            boolean r5 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            if (r5 == 0) goto L_0x009f
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            r5.<init>()     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            java.lang.String r6 = "[LOOKUP] Creating an instance of LogFactory class '"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            java.lang.StringBuffer r5 = r5.append(r3)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            java.lang.String r6 = "' as specified by system property "
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            java.lang.String r6 = "org.apache.commons.logging.LogFactory"
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            java.lang.String r5 = r5.toString()     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            logDiagnostic(r5)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
        L_0x009f:
            org2.apache.commons.logging.LogFactory r2 = newFactory(r3, r0, r1)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
        L_0x00a3:
            if (r2 != 0) goto L_0x010f
            boolean r3 = isDiagnosticsEnabled()
            if (r3 == 0) goto L_0x00b1
            java.lang.String r3 = "[LOOKUP] Looking for a resource file of name [META-INF/services/org.apache.commons.logging.LogFactory] to define the LogFactory subclass to use..."
            logDiagnostic(r3)
        L_0x00b1:
            java.lang.String r3 = "META-INF/services/org.apache.commons.logging.LogFactory"
            java.io.InputStream r5 = getResourceAsStream(r1, r3)     // Catch:{ Exception -> 0x01ff }
            if (r5 == 0) goto L_0x022e
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x01f2 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x01f2 }
            java.lang.String r7 = "UTF-8"
            r6.<init>(r5, r7)     // Catch:{ UnsupportedEncodingException -> 0x01f2 }
            r3.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x01f2 }
        L_0x00c7:
            java.lang.String r5 = r3.readLine()     // Catch:{ Exception -> 0x01ff }
            r3.close()     // Catch:{ Exception -> 0x01ff }
            if (r5 == 0) goto L_0x010f
            java.lang.String r3 = ""
            boolean r3 = r3.equals(r5)     // Catch:{ Exception -> 0x01ff }
            if (r3 != 0) goto L_0x010f
            boolean r3 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x01ff }
            if (r3 == 0) goto L_0x010b
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x01ff }
            r3.<init>()     // Catch:{ Exception -> 0x01ff }
            java.lang.String r6 = "[LOOKUP]  Creating an instance of LogFactory class "
            java.lang.StringBuffer r3 = r3.append(r6)     // Catch:{ Exception -> 0x01ff }
            java.lang.StringBuffer r3 = r3.append(r5)     // Catch:{ Exception -> 0x01ff }
            java.lang.String r6 = " as specified by file '"
            java.lang.StringBuffer r3 = r3.append(r6)     // Catch:{ Exception -> 0x01ff }
            java.lang.String r6 = "META-INF/services/org.apache.commons.logging.LogFactory"
            java.lang.StringBuffer r3 = r3.append(r6)     // Catch:{ Exception -> 0x01ff }
            java.lang.String r6 = "' which was present in the path of the context classloader."
            java.lang.StringBuffer r3 = r3.append(r6)     // Catch:{ Exception -> 0x01ff }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01ff }
            logDiagnostic(r3)     // Catch:{ Exception -> 0x01ff }
        L_0x010b:
            org2.apache.commons.logging.LogFactory r2 = newFactory(r5, r0, r1)     // Catch:{ Exception -> 0x01ff }
        L_0x010f:
            if (r2 != 0) goto L_0x0151
            if (r4 == 0) goto L_0x024b
            boolean r3 = isDiagnosticsEnabled()
            if (r3 == 0) goto L_0x011f
            java.lang.String r3 = "[LOOKUP] Looking in properties file for entry with key 'org.apache.commons.logging.LogFactory' to define the LogFactory subclass to use..."
            logDiagnostic(r3)
        L_0x011f:
            java.lang.String r3 = "org.apache.commons.logging.LogFactory"
            java.lang.String r3 = r4.getProperty(r3)
            if (r3 == 0) goto L_0x023c
            boolean r2 = isDiagnosticsEnabled()
            if (r2 == 0) goto L_0x014c
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r5 = "[LOOKUP] Properties file specifies LogFactory subclass '"
            java.lang.StringBuffer r2 = r2.append(r5)
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r5 = "'"
            java.lang.StringBuffer r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            logDiagnostic(r2)
        L_0x014c:
            org2.apache.commons.logging.LogFactory r0 = newFactory(r3, r0, r1)
        L_0x0150:
            r2 = r0
        L_0x0151:
            if (r2 != 0) goto L_0x0168
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x015f
            java.lang.String r0 = "[LOOKUP] Loading the default LogFactory implementation 'org.apache.commons.logging.impl.LogFactoryImpl' via the same classloader that loaded this LogFactory class (ie not looking in the context classloader)."
            logDiagnostic(r0)
        L_0x015f:
            java.lang.String r0 = "org.apache.commons.logging.impl.LogFactoryImpl"
            java.lang.ClassLoader r2 = thisClassLoader
            org2.apache.commons.logging.LogFactory r2 = newFactory(r0, r2, r1)
        L_0x0168:
            if (r2 == 0) goto L_0x0018
            cacheFactory(r1, r2)
            if (r4 == 0) goto L_0x0018
            java.util.Enumeration r1 = r4.propertyNames()
        L_0x0173:
            boolean r0 = r1.hasMoreElements()
            if (r0 == 0) goto L_0x0018
            java.lang.Object r0 = r1.nextElement()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r3 = r4.getProperty(r0)
            r2.setAttribute(r0, r3)
            goto L_0x0173
        L_0x0187:
            boolean r3 = isDiagnosticsEnabled()     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            if (r3 == 0) goto L_0x00a3
            java.lang.String r3 = "[LOOKUP] No system property [org.apache.commons.logging.LogFactory] defined."
            logDiagnostic(r3)     // Catch:{ SecurityException -> 0x0195, RuntimeException -> 0x01c4 }
            goto L_0x00a3
        L_0x0195:
            r3 = move-exception
            boolean r5 = isDiagnosticsEnabled()
            if (r5 == 0) goto L_0x00a3
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r6 = "[LOOKUP] A security exception occurred while trying to create an instance of the custom factory class: ["
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r3 = r3.getMessage()
            java.lang.String r3 = trim(r3)
            java.lang.StringBuffer r3 = r5.append(r3)
            java.lang.String r5 = "]. Trying alternative implementations..."
            java.lang.StringBuffer r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            logDiagnostic(r3)
            goto L_0x00a3
        L_0x01c4:
            r0 = move-exception
            boolean r1 = isDiagnosticsEnabled()
            if (r1 == 0) goto L_0x01f1
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = "[LOOKUP] An exception occurred while trying to create an instance of the custom factory class: ["
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = r0.getMessage()
            java.lang.String r2 = trim(r2)
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r2 = "] as specified by a system property."
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            logDiagnostic(r1)
        L_0x01f1:
            throw r0
        L_0x01f2:
            r3 = move-exception
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01ff }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01ff }
            r6.<init>(r5)     // Catch:{ Exception -> 0x01ff }
            r3.<init>(r6)     // Catch:{ Exception -> 0x01ff }
            goto L_0x00c7
        L_0x01ff:
            r3 = move-exception
            boolean r5 = isDiagnosticsEnabled()
            if (r5 == 0) goto L_0x010f
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r6 = "[LOOKUP] A security exception occurred while trying to create an instance of the custom factory class: ["
            java.lang.StringBuffer r5 = r5.append(r6)
            java.lang.String r3 = r3.getMessage()
            java.lang.String r3 = trim(r3)
            java.lang.StringBuffer r3 = r5.append(r3)
            java.lang.String r5 = "]. Trying alternative implementations..."
            java.lang.StringBuffer r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            logDiagnostic(r3)
            goto L_0x010f
        L_0x022e:
            boolean r3 = isDiagnosticsEnabled()     // Catch:{ Exception -> 0x01ff }
            if (r3 == 0) goto L_0x010f
            java.lang.String r3 = "[LOOKUP] No resource file with name 'META-INF/services/org.apache.commons.logging.LogFactory' found."
            logDiagnostic(r3)     // Catch:{ Exception -> 0x01ff }
            goto L_0x010f
        L_0x023c:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0248
            java.lang.String r0 = "[LOOKUP] Properties file has no entry specifying LogFactory subclass."
            logDiagnostic(r0)
        L_0x0248:
            r0 = r2
            goto L_0x0150
        L_0x024b:
            boolean r0 = isDiagnosticsEnabled()
            if (r0 == 0) goto L_0x0151
            java.lang.String r0 = "[LOOKUP] No properties file available to determine LogFactory subclass from.."
            logDiagnostic(r0)
            goto L_0x0151
        L_0x0259:
            r0 = r1
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.LogFactory.getFactory():org2.apache.commons.logging.LogFactory");
    }

    public static Log getLog(Class cls) throws LogConfigurationException {
        return getFactory().getInstance(cls);
    }

    public static Log getLog(String str) throws LogConfigurationException {
        return getFactory().getInstance(str);
    }

    private static Properties getProperties(URL url) {
        return (Properties) AccessController.doPrivileged(new PrivilegedAction(url) {

            /* renamed from: 龘  reason: contains not printable characters */
            private final URL f17730;

            {
                this.f17730 = r1;
            }

            /* JADX WARNING: Removed duplicated region for block: B:38:0x00b8 A[SYNTHETIC, Splitter:B:38:0x00b8] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object run() {
                /*
                    r4 = this;
                    r1 = 0
                    java.net.URL r0 = r4.f17730     // Catch:{ IOException -> 0x006c, all -> 0x00b4 }
                    java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x006c, all -> 0x00b4 }
                    r2 = 0
                    r0.setUseCaches(r2)     // Catch:{ IOException -> 0x006c, all -> 0x00b4 }
                    java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x006c, all -> 0x00b4 }
                    if (r2 == 0) goto L_0x0044
                    java.util.Properties r0 = new java.util.Properties     // Catch:{ IOException -> 0x00e3, all -> 0x00dd }
                    r0.<init>()     // Catch:{ IOException -> 0x00e3, all -> 0x00dd }
                    r0.load(r2)     // Catch:{ IOException -> 0x00e3, all -> 0x00dd }
                    r2.close()     // Catch:{ IOException -> 0x00e3, all -> 0x00dd }
                    r2 = 0
                    if (r1 == 0) goto L_0x0022
                    r2.close()     // Catch:{ IOException -> 0x0023 }
                L_0x0022:
                    return r0
                L_0x0023:
                    r1 = move-exception
                    boolean r1 = org2.apache.commons.logging.LogFactory.isDiagnosticsEnabled()
                    if (r1 == 0) goto L_0x0022
                    java.lang.StringBuffer r1 = new java.lang.StringBuffer
                    r1.<init>()
                    java.lang.String r2 = "Unable to close stream for URL "
                    java.lang.StringBuffer r1 = r1.append(r2)
                    java.net.URL r2 = r4.f17730
                    java.lang.StringBuffer r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    org2.apache.commons.logging.LogFactory.access$000(r1)
                    goto L_0x0022
                L_0x0044:
                    if (r2 == 0) goto L_0x0049
                    r2.close()     // Catch:{ IOException -> 0x004b }
                L_0x0049:
                    r0 = r1
                    goto L_0x0022
                L_0x004b:
                    r0 = move-exception
                    boolean r0 = org2.apache.commons.logging.LogFactory.isDiagnosticsEnabled()
                    if (r0 == 0) goto L_0x0049
                    java.lang.StringBuffer r0 = new java.lang.StringBuffer
                    r0.<init>()
                    java.lang.String r2 = "Unable to close stream for URL "
                    java.lang.StringBuffer r0 = r0.append(r2)
                    java.net.URL r2 = r4.f17730
                    java.lang.StringBuffer r0 = r0.append(r2)
                    java.lang.String r0 = r0.toString()
                    org2.apache.commons.logging.LogFactory.access$000(r0)
                    goto L_0x0049
                L_0x006c:
                    r0 = move-exception
                    r0 = r1
                L_0x006e:
                    boolean r2 = org2.apache.commons.logging.LogFactory.isDiagnosticsEnabled()     // Catch:{ all -> 0x00df }
                    if (r2 == 0) goto L_0x008d
                    java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x00df }
                    r2.<init>()     // Catch:{ all -> 0x00df }
                    java.lang.String r3 = "Unable to read URL "
                    java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ all -> 0x00df }
                    java.net.URL r3 = r4.f17730     // Catch:{ all -> 0x00df }
                    java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ all -> 0x00df }
                    java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00df }
                    org2.apache.commons.logging.LogFactory.access$000(r2)     // Catch:{ all -> 0x00df }
                L_0x008d:
                    if (r0 == 0) goto L_0x0049
                    r0.close()     // Catch:{ IOException -> 0x0093 }
                    goto L_0x0049
                L_0x0093:
                    r0 = move-exception
                    boolean r0 = org2.apache.commons.logging.LogFactory.isDiagnosticsEnabled()
                    if (r0 == 0) goto L_0x0049
                    java.lang.StringBuffer r0 = new java.lang.StringBuffer
                    r0.<init>()
                    java.lang.String r2 = "Unable to close stream for URL "
                    java.lang.StringBuffer r0 = r0.append(r2)
                    java.net.URL r2 = r4.f17730
                    java.lang.StringBuffer r0 = r0.append(r2)
                    java.lang.String r0 = r0.toString()
                    org2.apache.commons.logging.LogFactory.access$000(r0)
                    goto L_0x0049
                L_0x00b4:
                    r0 = move-exception
                    r2 = r1
                L_0x00b6:
                    if (r2 == 0) goto L_0x00bb
                    r2.close()     // Catch:{ IOException -> 0x00bc }
                L_0x00bb:
                    throw r0
                L_0x00bc:
                    r1 = move-exception
                    boolean r1 = org2.apache.commons.logging.LogFactory.isDiagnosticsEnabled()
                    if (r1 == 0) goto L_0x00bb
                    java.lang.StringBuffer r1 = new java.lang.StringBuffer
                    r1.<init>()
                    java.lang.String r2 = "Unable to close stream for URL "
                    java.lang.StringBuffer r1 = r1.append(r2)
                    java.net.URL r2 = r4.f17730
                    java.lang.StringBuffer r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    org2.apache.commons.logging.LogFactory.access$000(r1)
                    goto L_0x00bb
                L_0x00dd:
                    r0 = move-exception
                    goto L_0x00b6
                L_0x00df:
                    r1 = move-exception
                    r2 = r0
                    r0 = r1
                    goto L_0x00b6
                L_0x00e3:
                    r0 = move-exception
                    r0 = r2
                    goto L_0x006e
                */
                throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.logging.LogFactory.AnonymousClass5.run():java.lang.Object");
            }
        });
    }

    private static InputStream getResourceAsStream(ClassLoader classLoader, String str) {
        return (InputStream) AccessController.doPrivileged(new PrivilegedAction(classLoader, str) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f17726;

            /* renamed from: 龘  reason: contains not printable characters */
            private final ClassLoader f17727;

            {
                this.f17727 = r1;
                this.f17726 = r2;
            }

            public Object run() {
                return this.f17727 != null ? this.f17727.getResourceAsStream(this.f17726) : ClassLoader.getSystemResourceAsStream(this.f17726);
            }
        });
    }

    private static Enumeration getResources(ClassLoader classLoader, String str) {
        return (Enumeration) AccessController.doPrivileged(new PrivilegedAction(classLoader, str) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f17728;

            /* renamed from: 龘  reason: contains not printable characters */
            private final ClassLoader f17729;

            {
                this.f17729 = r1;
                this.f17728 = r2;
            }

            public Object run() {
                try {
                    return this.f17729 != null ? this.f17729.getResources(this.f17728) : ClassLoader.getSystemResources(this.f17728);
                } catch (IOException e) {
                    if (!LogFactory.isDiagnosticsEnabled()) {
                        return null;
                    }
                    LogFactory.access$000(new StringBuffer().append("Exception while trying to find configuration file ").append(this.f17728).append(":").append(e.getMessage()).toString());
                    return null;
                } catch (NoSuchMethodError e2) {
                    return null;
                }
            }
        });
    }

    private static String getSystemProperty(String str, String str2) throws SecurityException {
        return (String) AccessController.doPrivileged(new PrivilegedAction(str, str2) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final String f17731;

            /* renamed from: 龘  reason: contains not printable characters */
            private final String f17732;

            {
                this.f17732 = r1;
                this.f17731 = r2;
            }

            public Object run() {
                return System.getProperty(this.f17732, this.f17731);
            }
        });
    }

    protected static void handleThrowable(Throwable th) {
        if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        }
    }

    private static boolean implementsLogFactory(Class cls) {
        boolean z = false;
        if (cls != null) {
            try {
                ClassLoader classLoader = cls.getClassLoader();
                if (classLoader == null) {
                    logDiagnostic("[CUSTOM LOG FACTORY] was loaded by the boot classloader");
                } else {
                    logHierarchy("[CUSTOM LOG FACTORY] ", classLoader);
                    z = Class.forName("org.apache.commons.logging.LogFactory", false, classLoader).isAssignableFrom(cls);
                    if (z) {
                        logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] ").append(cls.getName()).append(" implements LogFactory but was loaded by an incompatible classloader.").toString());
                    } else {
                        logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] ").append(cls.getName()).append(" does not implement LogFactory.").toString());
                    }
                }
            } catch (SecurityException e) {
                logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] SecurityException thrown whilst trying to determine whether the compatibility was caused by a classloader conflict: ").append(e.getMessage()).toString());
            } catch (LinkageError e2) {
                logDiagnostic(new StringBuffer().append("[CUSTOM LOG FACTORY] LinkageError thrown whilst trying to determine whether the compatibility was caused by a classloader conflict: ").append(e2.getMessage()).toString());
            } catch (ClassNotFoundException e3) {
                logDiagnostic("[CUSTOM LOG FACTORY] LogFactory class cannot be loaded by classloader which loaded the custom LogFactory implementation. Is the custom factory in the right classloader?");
            }
        }
        return z;
    }

    private static PrintStream initDiagnostics() {
        try {
            String systemProperty = getSystemProperty("org.apache.commons.logging.diagnostics.dest", (String) null);
            if (systemProperty == null) {
                return null;
            }
            if (systemProperty.equals("STDOUT")) {
                return System.out;
            }
            if (systemProperty.equals("STDERR")) {
                return System.err;
            }
            try {
                return new PrintStream(new FileOutputStream(systemProperty, true));
            } catch (IOException e) {
                return null;
            }
        } catch (SecurityException e2) {
            return null;
        }
    }

    protected static boolean isDiagnosticsEnabled() {
        return diagnosticsStream != null;
    }

    private static void logClassLoaderEnvironment(Class cls) {
        if (isDiagnosticsEnabled()) {
            try {
                logDiagnostic(new StringBuffer().append("[ENV] Extension directories (java.ext.dir): ").append(System.getProperty("java.ext.dir")).toString());
                logDiagnostic(new StringBuffer().append("[ENV] Application classpath (java.class.path): ").append(System.getProperty("java.class.path")).toString());
            } catch (SecurityException e) {
                logDiagnostic("[ENV] Security setting prevent interrogation of system classpaths.");
            }
            String name = cls.getName();
            try {
                ClassLoader classLoader = getClassLoader(cls);
                logDiagnostic(new StringBuffer().append("[ENV] Class ").append(name).append(" was loaded via classloader ").append(objectId(classLoader)).toString());
                logHierarchy(new StringBuffer().append("[ENV] Ancestry of classloader which loaded ").append(name).append(" is ").toString(), classLoader);
            } catch (SecurityException e2) {
                logDiagnostic(new StringBuffer().append("[ENV] Security forbids determining the classloader for ").append(name).toString());
            }
        }
    }

    private static final void logDiagnostic(String str) {
        if (diagnosticsStream != null) {
            diagnosticsStream.print(diagnosticPrefix);
            diagnosticsStream.println(str);
            diagnosticsStream.flush();
        }
    }

    private static void logHierarchy(String str, ClassLoader classLoader) {
        if (isDiagnosticsEnabled()) {
            if (classLoader != null) {
                logDiagnostic(new StringBuffer().append(str).append(objectId(classLoader)).append(" == '").append(classLoader.toString()).append("'").toString());
            }
            try {
                ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
                if (classLoader != null) {
                    StringBuffer stringBuffer = new StringBuffer(new StringBuffer().append(str).append("ClassLoader tree:").toString());
                    do {
                        stringBuffer.append(objectId(classLoader));
                        if (classLoader == systemClassLoader) {
                            stringBuffer.append(" (SYSTEM) ");
                        }
                        try {
                            classLoader = classLoader.getParent();
                            stringBuffer.append(" --> ");
                        } catch (SecurityException e) {
                            stringBuffer.append(" --> SECRET");
                        }
                    } while (classLoader != null);
                    stringBuffer.append("BOOT");
                    logDiagnostic(stringBuffer.toString());
                }
            } catch (SecurityException e2) {
                logDiagnostic(new StringBuffer().append(str).append("Security forbids determining the system classloader.").toString());
            }
        }
    }

    protected static final void logRawDiagnostic(String str) {
        if (diagnosticsStream != null) {
            diagnosticsStream.println(str);
            diagnosticsStream.flush();
        }
    }

    protected static LogFactory newFactory(String str, ClassLoader classLoader) {
        return newFactory(str, classLoader, (ClassLoader) null);
    }

    protected static LogFactory newFactory(String str, ClassLoader classLoader, ClassLoader classLoader2) throws LogConfigurationException {
        Object doPrivileged = AccessController.doPrivileged(new PrivilegedAction(str, classLoader) {

            /* renamed from: 靐  reason: contains not printable characters */
            private final ClassLoader f17724;

            /* renamed from: 龘  reason: contains not printable characters */
            private final String f17725;

            {
                this.f17725 = r1;
                this.f17724 = r2;
            }

            public Object run() {
                return LogFactory.createFactory(this.f17725, this.f17724);
            }
        });
        if (doPrivileged instanceof LogConfigurationException) {
            LogConfigurationException logConfigurationException = (LogConfigurationException) doPrivileged;
            if (isDiagnosticsEnabled()) {
                logDiagnostic(new StringBuffer().append("An error occurred while loading the factory class:").append(logConfigurationException.getMessage()).toString());
            }
            throw logConfigurationException;
        }
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Created object ").append(objectId(doPrivileged)).append(" to manage classloader ").append(objectId(classLoader2)).toString());
        }
        return (LogFactory) doPrivileged;
    }

    public static String objectId(Object obj) {
        return obj == null ? "null" : new StringBuffer().append(obj.getClass().getName()).append("@").append(System.identityHashCode(obj)).toString();
    }

    public static void release(ClassLoader classLoader) {
        if (isDiagnosticsEnabled()) {
            logDiagnostic(new StringBuffer().append("Releasing factory for classloader ").append(objectId(classLoader)).toString());
        }
        Hashtable hashtable = factories;
        synchronized (hashtable) {
            if (classLoader != null) {
                LogFactory logFactory = (LogFactory) hashtable.get(classLoader);
                if (logFactory != null) {
                    logFactory.release();
                    hashtable.remove(classLoader);
                }
            } else if (nullClassLoaderFactory != null) {
                nullClassLoaderFactory.release();
                nullClassLoaderFactory = null;
            }
        }
    }

    public static void releaseAll() {
        if (isDiagnosticsEnabled()) {
            logDiagnostic("Releasing factory for all classloaders.");
        }
        Hashtable hashtable = factories;
        synchronized (hashtable) {
            Enumeration elements = hashtable.elements();
            while (elements.hasMoreElements()) {
                ((LogFactory) elements.nextElement()).release();
            }
            hashtable.clear();
            if (nullClassLoaderFactory != null) {
                nullClassLoaderFactory.release();
                nullClassLoaderFactory = null;
            }
        }
    }

    private static String trim(String str) {
        if (str == null) {
            return null;
        }
        return str.trim();
    }

    public abstract Object getAttribute(String str);

    public abstract String[] getAttributeNames();

    public abstract Log getInstance(Class cls) throws LogConfigurationException;

    public abstract Log getInstance(String str) throws LogConfigurationException;

    public abstract void release();

    public abstract void removeAttribute(String str);

    public abstract void setAttribute(String str, Object obj);
}
