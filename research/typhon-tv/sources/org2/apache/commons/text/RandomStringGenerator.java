package org2.apache.commons.text;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import org2.apache.commons.lang3.Validate;

public final class RandomStringGenerator {
    private final Set<CharacterPredicate> inclusivePredicates;
    private final int maximumCodePoint;
    private final int minimumCodePoint;
    private final TextRandomProvider random;

    public static class Builder implements Builder<RandomStringGenerator> {
        public static final int DEFAULT_LENGTH = 0;
        public static final int DEFAULT_MAXIMUM_CODE_POINT = 1114111;
        public static final int DEFAULT_MINIMUM_CODE_POINT = 0;
        private Set<CharacterPredicate> inclusivePredicates;
        private int maximumCodePoint = 1114111;
        private int minimumCodePoint = 0;
        private TextRandomProvider random;

        public RandomStringGenerator build() {
            return new RandomStringGenerator(this.minimumCodePoint, this.maximumCodePoint, this.inclusivePredicates, this.random);
        }

        public Builder filteredBy(CharacterPredicate... characterPredicateArr) {
            if (characterPredicateArr == null || characterPredicateArr.length == 0) {
                this.inclusivePredicates = null;
            } else {
                if (this.inclusivePredicates == null) {
                    this.inclusivePredicates = new HashSet();
                } else {
                    this.inclusivePredicates.clear();
                }
                for (CharacterPredicate add : characterPredicateArr) {
                    this.inclusivePredicates.add(add);
                }
            }
            return this;
        }

        public Builder usingRandom(TextRandomProvider textRandomProvider) {
            this.random = textRandomProvider;
            return this;
        }

        public Builder withinRange(int i, int i2) {
            boolean z = true;
            Validate.isTrue(i <= i2, "Minimum code point %d is larger than maximum code point %d", Integer.valueOf(i), Integer.valueOf(i2));
            Validate.isTrue(i >= 0, "Minimum code point %d is negative", (long) i);
            if (i2 > 1114111) {
                z = false;
            }
            Validate.isTrue(z, "Value %d is larger than Character.MAX_CODE_POINT.", (long) i2);
            this.minimumCodePoint = i;
            this.maximumCodePoint = i2;
            return this;
        }
    }

    private RandomStringGenerator(int i, int i2, Set<CharacterPredicate> set, TextRandomProvider textRandomProvider) {
        this.minimumCodePoint = i;
        this.maximumCodePoint = i2;
        this.inclusivePredicates = set;
        this.random = textRandomProvider;
    }

    private int generateRandomNumber(int i, int i2) {
        return this.random != null ? this.random.nextInt((i2 - i) + 1) + i : ThreadLocalRandom.current().nextInt(i, i2 + 1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0044, code lost:
        if (r0 == false) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String generate(int r9) {
        /*
            r8 = this;
            r1 = 1
            r2 = 0
            if (r9 != 0) goto L_0x0008
            java.lang.String r0 = ""
        L_0x0007:
            return r0
        L_0x0008:
            if (r9 <= 0) goto L_0x0051
            r0 = r1
        L_0x000b:
            java.lang.String r3 = "Length %d is smaller than zero."
            long r4 = (long) r9
            org2.apache.commons.lang3.Validate.isTrue((boolean) r0, (java.lang.String) r3, (long) r4)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r9)
            long r4 = (long) r9
        L_0x0018:
            int r0 = r8.minimumCodePoint
            int r6 = r8.maximumCodePoint
            int r6 = r8.generateRandomNumber(r0, r6)
            int r0 = java.lang.Character.getType(r6)
            switch(r0) {
                case 0: goto L_0x0046;
                case 18: goto L_0x0046;
                case 19: goto L_0x0046;
                default: goto L_0x0027;
            }
        L_0x0027:
            java.util.Set<org2.apache.commons.text.CharacterPredicate> r0 = r8.inclusivePredicates
            if (r0 == 0) goto L_0x0053
            java.util.Set<org2.apache.commons.text.CharacterPredicate> r0 = r8.inclusivePredicates
            java.util.Iterator r7 = r0.iterator()
        L_0x0031:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x005a
            java.lang.Object r0 = r7.next()
            org2.apache.commons.text.CharacterPredicate r0 = (org2.apache.commons.text.CharacterPredicate) r0
            boolean r0 = r0.test(r6)
            if (r0 == 0) goto L_0x0031
            r0 = r1
        L_0x0044:
            if (r0 != 0) goto L_0x0053
        L_0x0046:
            r6 = 0
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x0018
            java.lang.String r0 = r3.toString()
            goto L_0x0007
        L_0x0051:
            r0 = r2
            goto L_0x000b
        L_0x0053:
            r3.appendCodePoint(r6)
            r6 = 1
            long r4 = r4 - r6
            goto L_0x0046
        L_0x005a:
            r0 = r2
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.text.RandomStringGenerator.generate(int):java.lang.String");
    }
}
