package org2.apache.commons.text;

import java.util.Map;

public abstract class StrLookup<V> {
    private static final StrLookup<String> NONE_LOOKUP = new MapStrLookup((Map) null);
    private static final StrLookup<String> SYSTEM_PROPERTIES_LOOKUP = new SystemPropertiesStrLookup();

    static class MapStrLookup<V> extends StrLookup<V> {

        /* renamed from: 龘  reason: contains not printable characters */
        private final Map<String, V> f17750;

        MapStrLookup(Map<String, V> map) {
            this.f17750 = map;
        }

        public String lookup(String str) {
            V v;
            if (this.f17750 == null || (v = this.f17750.get(str)) == null) {
                return null;
            }
            return v.toString();
        }
    }

    private static class SystemPropertiesStrLookup extends StrLookup<String> {
        private SystemPropertiesStrLookup() {
        }

        public String lookup(String str) {
            if (str.length() <= 0) {
                return null;
            }
            try {
                return System.getProperty(str);
            } catch (SecurityException e) {
                return null;
            }
        }
    }

    protected StrLookup() {
    }

    public static <V> StrLookup<V> mapLookup(Map<String, V> map) {
        return new MapStrLookup(map);
    }

    public static StrLookup<?> noneLookup() {
        return NONE_LOOKUP;
    }

    public static StrLookup<String> systemPropertiesLookup() {
        return SYSTEM_PROPERTIES_LOOKUP;
    }

    public abstract String lookup(String str);
}
