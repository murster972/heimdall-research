package org2.apache.commons.text;

public interface TextRandomProvider {
    int nextInt(int i);
}
