package org2.apache.commons.text;

public enum CharacterPredicates implements CharacterPredicate {
    LETTERS {
        public boolean test(int i) {
            return Character.isLetter(i);
        }
    },
    DIGITS {
        public boolean test(int i) {
            return Character.isDigit(i);
        }
    }
}
