package org2.apache.commons.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org2.apache.commons.lang3.ArrayUtils;
import org2.apache.commons.lang3.StringUtils;
import org2.apache.commons.lang3.Validate;

public class WordUtils {
    public static String abbreviate(String str, int i, int i2, String str2) {
        boolean z = true;
        Validate.isTrue(i2 >= -1, "upper value cannot be less than -1", new Object[0]);
        if (i2 < i && i2 != -1) {
            z = false;
        }
        Validate.isTrue(z, "upper value is less than lower value", new Object[0]);
        if (StringUtils.isEmpty(str)) {
            return str;
        }
        if (i > str.length()) {
            i = str.length();
        }
        if (i2 == -1 || i2 > str.length()) {
            i2 = str.length();
        }
        StringBuilder sb = new StringBuilder();
        int indexOf = StringUtils.indexOf(str, org.apache.commons.lang3.StringUtils.SPACE, i);
        if (indexOf == -1) {
            sb.append(str.substring(0, i2));
            if (i2 != str.length()) {
                sb.append(StringUtils.defaultString(str2));
            }
        } else if (indexOf > i2) {
            sb.append(str.substring(0, i2));
            sb.append(StringUtils.defaultString(str2));
        } else {
            sb.append(str.substring(0, indexOf));
            sb.append(StringUtils.defaultString(str2));
        }
        return sb.toString();
    }

    public static String capitalize(String str) {
        return capitalize(str, (char[]) null);
    }

    public static String capitalize(String str, char... cArr) {
        int i;
        int length = cArr == null ? -1 : cArr.length;
        if (StringUtils.isEmpty(str) || length == 0) {
            return str;
        }
        int length2 = str.length();
        int[] iArr = new int[length2];
        int i2 = 0;
        boolean z = true;
        int i3 = 0;
        while (i2 < length2) {
            int codePointAt = str.codePointAt(i2);
            if (isDelimiter(codePointAt, cArr)) {
                iArr[i3] = codePointAt;
                i2 += Character.charCount(codePointAt);
                i = i3 + 1;
                z = true;
            } else if (z) {
                int titleCase = Character.toTitleCase(codePointAt);
                iArr[i3] = titleCase;
                i2 += Character.charCount(titleCase);
                i = i3 + 1;
                z = false;
            } else {
                i = i3 + 1;
                iArr[i3] = codePointAt;
                i2 += Character.charCount(codePointAt);
            }
            i3 = i;
        }
        return new String(iArr, 0, i3);
    }

    public static String capitalizeFully(String str) {
        return capitalizeFully(str, (char[]) null);
    }

    public static String capitalizeFully(String str, char... cArr) {
        return (StringUtils.isEmpty(str) || (cArr == null ? -1 : cArr.length) == 0) ? str : capitalize(str.toLowerCase(), cArr);
    }

    public static boolean containsAllWords(CharSequence charSequence, CharSequence... charSequenceArr) {
        if (StringUtils.isEmpty(charSequence) || ArrayUtils.isEmpty(charSequenceArr)) {
            return false;
        }
        for (CharSequence charSequence2 : charSequenceArr) {
            if (StringUtils.isBlank(charSequence2) || !Pattern.compile(".*\\b" + charSequence2 + "\\b.*").matcher(charSequence).matches()) {
                return false;
            }
        }
        return true;
    }

    public static String initials(String str) {
        return initials(str, (char[]) null);
    }

    public static String initials(String str, char... cArr) {
        if (StringUtils.isEmpty(str)) {
            return str;
        }
        if (cArr != null && cArr.length == 0) {
            return "";
        }
        int length = str.length();
        char[] cArr2 = new char[((length / 2) + 1)];
        boolean z = true;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (isDelimiter(charAt, cArr)) {
                z = true;
            } else if (z) {
                cArr2[i] = charAt;
                i++;
                z = false;
            }
        }
        return new String(cArr2, 0, i);
    }

    public static boolean isDelimiter(char c, char[] cArr) {
        if (cArr == null) {
            return Character.isWhitespace(c);
        }
        for (char c2 : cArr) {
            if (c == c2) {
                return true;
            }
        }
        return false;
    }

    public static boolean isDelimiter(int i, char[] cArr) {
        if (cArr == null) {
            return Character.isWhitespace(i);
        }
        for (int i2 = 0; i2 < cArr.length; i2++) {
            if (Character.codePointAt(cArr, i2) == i) {
                return true;
            }
        }
        return false;
    }

    public static String swapCase(String str) {
        if (StringUtils.isEmpty(str)) {
            return str;
        }
        int length = str.length();
        int[] iArr = new int[length];
        boolean z = true;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            if (Character.isUpperCase(codePointAt)) {
                codePointAt = Character.toLowerCase(codePointAt);
                z = false;
            } else if (Character.isTitleCase(codePointAt)) {
                codePointAt = Character.toLowerCase(codePointAt);
                z = false;
            } else if (!Character.isLowerCase(codePointAt)) {
                z = Character.isWhitespace(codePointAt);
            } else if (z) {
                codePointAt = Character.toTitleCase(codePointAt);
                z = false;
            } else {
                codePointAt = Character.toUpperCase(codePointAt);
            }
            iArr[i2] = codePointAt;
            i = Character.charCount(codePointAt) + i;
            i2++;
        }
        return new String(iArr, 0, i2);
    }

    public static String uncapitalize(String str) {
        return uncapitalize(str, (char[]) null);
    }

    public static String uncapitalize(String str, char... cArr) {
        int i;
        int length = cArr == null ? -1 : cArr.length;
        if (StringUtils.isEmpty(str) || length == 0) {
            return str;
        }
        int length2 = str.length();
        int[] iArr = new int[length2];
        int i2 = 0;
        boolean z = true;
        int i3 = 0;
        while (i2 < length2) {
            int codePointAt = str.codePointAt(i2);
            if (isDelimiter(codePointAt, cArr)) {
                iArr[i3] = codePointAt;
                i2 += Character.charCount(codePointAt);
                i = i3 + 1;
                z = true;
            } else if (z) {
                int lowerCase = Character.toLowerCase(codePointAt);
                iArr[i3] = lowerCase;
                i2 += Character.charCount(lowerCase);
                i = i3 + 1;
                z = false;
            } else {
                i = i3 + 1;
                iArr[i3] = codePointAt;
                i2 += Character.charCount(codePointAt);
            }
            i3 = i;
        }
        return new String(iArr, 0, i3);
    }

    public static String wrap(String str, int i) {
        return wrap(str, i, (String) null, false);
    }

    public static String wrap(String str, int i, String str2, boolean z) {
        return wrap(str, i, str2, z, org.apache.commons.lang3.StringUtils.SPACE);
    }

    public static String wrap(String str, int i, String str2, boolean z, String str3) {
        int i2;
        if (str == null) {
            return null;
        }
        if (str2 == null) {
            str2 = System.lineSeparator();
        }
        if (i < 1) {
            i = 1;
        }
        if (StringUtils.isBlank(str3)) {
            str3 = org.apache.commons.lang3.StringUtils.SPACE;
        }
        Pattern compile = Pattern.compile(str3);
        int length = str.length();
        StringBuilder sb = new StringBuilder(length + 32);
        int i3 = 0;
        while (i3 < length) {
            int i4 = -1;
            Matcher matcher = compile.matcher(str.substring(i3, Math.min(i3 + i + 1, length)));
            if (matcher.find()) {
                if (matcher.start() == 0) {
                    i3 = matcher.end() + i3;
                } else {
                    i4 = matcher.start() + i3;
                }
            }
            if (length - i3 <= i) {
                break;
            }
            while (matcher.find()) {
                i4 = matcher.start() + i3;
            }
            if (i4 >= i3) {
                sb.append(str.substring(i3, i4));
                sb.append(str2);
                i2 = i4 + 1;
            } else if (z) {
                sb.append(str.substring(i3, i + i3));
                sb.append(str2);
                i2 = i3 + i;
            } else {
                Matcher matcher2 = compile.matcher(str.substring(i3 + i));
                if (matcher2.find()) {
                    i4 = matcher2.start() + i3 + i;
                }
                if (i4 >= 0) {
                    sb.append(str.substring(i3, i4));
                    sb.append(str2);
                    i2 = i4 + 1;
                } else {
                    sb.append(str.substring(i3));
                    i2 = length;
                }
            }
            i3 = i2;
        }
        sb.append(str.substring(i3));
        return sb.toString();
    }
}
