package org2.apache.commons.text.similarity;

public interface SimilarityScore<R> {
    R apply(CharSequence charSequence, CharSequence charSequence2);
}
