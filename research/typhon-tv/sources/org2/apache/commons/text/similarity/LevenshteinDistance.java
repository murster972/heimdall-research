package org2.apache.commons.text.similarity;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.Arrays;

public class LevenshteinDistance implements EditDistance<Integer> {
    private static final LevenshteinDistance DEFAULT_INSTANCE = new LevenshteinDistance();
    private final Integer threshold;

    public LevenshteinDistance() {
        this((Integer) null);
    }

    public LevenshteinDistance(Integer num) {
        if (num == null || num.intValue() >= 0) {
            this.threshold = num;
            return;
        }
        throw new IllegalArgumentException("Threshold must not be negative");
    }

    public static LevenshteinDistance getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    private static int limitedCompare(CharSequence charSequence, CharSequence charSequence2, int i) {
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (i < 0) {
            throw new IllegalArgumentException("Threshold must not be negative");
        } else {
            int length = charSequence.length();
            int length2 = charSequence2.length();
            if (length == 0) {
                if (length2 <= i) {
                    return length2;
                }
                return -1;
            } else if (length2 != 0) {
                if (length > length2) {
                    length = length2;
                    length2 = charSequence.length();
                } else {
                    CharSequence charSequence3 = charSequence2;
                    charSequence2 = charSequence;
                    charSequence = charSequence3;
                }
                int[] iArr = new int[(length + 1)];
                int[] iArr2 = new int[(length + 1)];
                int min = Math.min(length, i) + 1;
                for (int i2 = 0; i2 < min; i2++) {
                    iArr[i2] = i2;
                }
                Arrays.fill(iArr, min, iArr.length, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                Arrays.fill(iArr2, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
                int i3 = 1;
                int[] iArr3 = iArr2;
                while (i3 <= length2) {
                    char charAt = charSequence.charAt(i3 - 1);
                    iArr3[0] = i3;
                    int max = Math.max(1, i3 - i);
                    int min2 = i3 > MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - i ? length : Math.min(length, i3 + i);
                    if (max > min2) {
                        return -1;
                    }
                    if (max > 1) {
                        iArr3[max - 1] = Integer.MAX_VALUE;
                    }
                    while (max <= min2) {
                        if (charSequence2.charAt(max - 1) == charAt) {
                            iArr3[max] = iArr[max - 1];
                        } else {
                            iArr3[max] = Math.min(Math.min(iArr3[max - 1], iArr[max]), iArr[max - 1]) + 1;
                        }
                        max++;
                    }
                    i3++;
                    int[] iArr4 = iArr;
                    iArr = iArr3;
                    iArr3 = iArr4;
                }
                if (iArr[length] <= i) {
                    return iArr[length];
                }
                return -1;
            } else if (length <= i) {
                return length;
            } else {
                return -1;
            }
        }
    }

    private static int unlimitedCompare(CharSequence charSequence, CharSequence charSequence2) {
        int i;
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        int length = charSequence.length();
        int length2 = charSequence2.length();
        if (length == 0) {
            return length2;
        }
        if (length2 == 0) {
            return length;
        }
        if (length > length2) {
            i = charSequence.length();
        } else {
            int i2 = length2;
            length2 = length;
            i = i2;
            CharSequence charSequence3 = charSequence2;
            charSequence2 = charSequence;
            charSequence = charSequence3;
        }
        int[] iArr = new int[(length2 + 1)];
        for (int i3 = 0; i3 <= length2; i3++) {
            iArr[i3] = i3;
        }
        for (int i4 = 1; i4 <= i; i4++) {
            int i5 = iArr[0];
            char charAt = charSequence.charAt(i4 - 1);
            iArr[0] = i4;
            int i6 = i5;
            for (int i7 = 1; i7 <= length2; i7++) {
                int i8 = iArr[i7];
                iArr[i7] = Math.min(Math.min(iArr[i7 - 1] + 1, iArr[i7] + 1), (charSequence2.charAt(i7 + -1) == charAt ? 0 : 1) + i6);
                i6 = i8;
            }
        }
        return iArr[length2];
    }

    public Integer apply(CharSequence charSequence, CharSequence charSequence2) {
        return this.threshold != null ? Integer.valueOf(limitedCompare(charSequence, charSequence2, this.threshold.intValue())) : Integer.valueOf(unlimitedCompare(charSequence, charSequence2));
    }

    public Integer getThreshold() {
        return this.threshold;
    }
}
