package org2.apache.commons.text.similarity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class CosineSimilarity {
    private double dot(Map<CharSequence, Integer> map, Map<CharSequence, Integer> map2, Set<CharSequence> set) {
        long j = 0;
        Iterator<CharSequence> it2 = set.iterator();
        while (true) {
            long j2 = j;
            if (!it2.hasNext()) {
                return (double) j2;
            }
            CharSequence next = it2.next();
            j = ((long) (map2.get(next).intValue() * map.get(next).intValue())) + j2;
        }
    }

    private Set<CharSequence> getIntersection(Map<CharSequence, Integer> map, Map<CharSequence, Integer> map2) {
        HashSet hashSet = new HashSet(map.keySet());
        hashSet.retainAll(map2.keySet());
        return hashSet;
    }

    public Double cosineSimilarity(Map<CharSequence, Integer> map, Map<CharSequence, Integer> map2) {
        double d;
        double d2;
        if (map == null || map2 == null) {
            throw new IllegalArgumentException("Vectors must not be null");
        }
        double dot = dot(map, map2, getIntersection(map, map2));
        double d3 = 0.0d;
        Iterator<Integer> it2 = map.values().iterator();
        while (true) {
            d = d3;
            if (!it2.hasNext()) {
                break;
            }
            d3 = Math.pow((double) it2.next().intValue(), 2.0d) + d;
        }
        double d4 = 0.0d;
        Iterator<Integer> it3 = map2.values().iterator();
        while (true) {
            d2 = d4;
            if (!it3.hasNext()) {
                break;
            }
            d4 = Math.pow((double) it3.next().intValue(), 2.0d) + d2;
        }
        return Double.valueOf((d <= 0.0d || d2 <= 0.0d) ? 0.0d : dot / (Math.sqrt(d) * Math.sqrt(d2)));
    }
}
