package org2.apache.commons.text.similarity;

import java.util.HashMap;
import java.util.Map;

final class Counter {
    /* renamed from: 龘  reason: contains not printable characters */
    public static Map<CharSequence, Integer> m22676(CharSequence[] charSequenceArr) {
        HashMap hashMap = new HashMap();
        for (CharSequence charSequence : charSequenceArr) {
            if (hashMap.containsKey(charSequence)) {
                hashMap.put(charSequence, Integer.valueOf(((Integer) hashMap.get(charSequence)).intValue() + 1));
            } else {
                hashMap.put(charSequence, 1);
            }
        }
        return hashMap;
    }
}
