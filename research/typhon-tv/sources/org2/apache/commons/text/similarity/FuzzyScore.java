package org2.apache.commons.text.similarity;

import java.util.Locale;

public class FuzzyScore {
    private final Locale locale;

    public FuzzyScore(Locale locale2) {
        if (locale2 == null) {
            throw new IllegalArgumentException("Locale must not be null");
        }
        this.locale = locale2;
    }

    public Integer fuzzyScore(CharSequence charSequence, CharSequence charSequence2) {
        boolean z;
        int i;
        int i2;
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        String lowerCase = charSequence.toString().toLowerCase(this.locale);
        String lowerCase2 = charSequence2.toString().toLowerCase(this.locale);
        int i3 = Integer.MIN_VALUE;
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 0; i6 < lowerCase2.length(); i6++) {
            char charAt = lowerCase2.charAt(i6);
            boolean z2 = false;
            while (i4 < lowerCase.length() && !z2) {
                if (charAt == lowerCase.charAt(i4)) {
                    int i7 = i5 + 1;
                    int i8 = i3 + 1 == i4 ? i7 + 2 : i7;
                    i2 = i4;
                    z = true;
                    i = i8;
                } else {
                    z = z2;
                    i = i5;
                    i2 = i3;
                }
                i4++;
                i3 = i2;
                i5 = i;
                z2 = z;
            }
        }
        return Integer.valueOf(i5);
    }

    public Locale getLocale() {
        return this.locale;
    }
}
