package org2.apache.commons.text.similarity;

public class HammingDistance implements EditDistance<Integer> {
    public Integer apply(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (charSequence.length() != charSequence2.length()) {
            throw new IllegalArgumentException("Strings must have the same length");
        } else {
            int i = 0;
            for (int i2 = 0; i2 < charSequence.length(); i2++) {
                if (charSequence.charAt(i2) != charSequence2.charAt(i2)) {
                    i++;
                }
            }
            return Integer.valueOf(i);
        }
    }
}
