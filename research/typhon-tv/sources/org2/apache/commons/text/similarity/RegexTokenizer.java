package org2.apache.commons.text.similarity;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org2.apache.commons.lang3.StringUtils;
import org2.apache.commons.lang3.Validate;

class RegexTokenizer implements Tokenizer<CharSequence> {
    RegexTokenizer() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public CharSequence[] m22677(CharSequence charSequence) {
        Validate.isTrue(StringUtils.isNotBlank(charSequence), "Invalid text", new Object[0]);
        Matcher matcher = Pattern.compile("(\\w)+").matcher(charSequence.toString());
        ArrayList arrayList = new ArrayList();
        while (matcher.find()) {
            arrayList.add(matcher.group(0));
        }
        return (CharSequence[]) arrayList.toArray(new String[0]);
    }
}
