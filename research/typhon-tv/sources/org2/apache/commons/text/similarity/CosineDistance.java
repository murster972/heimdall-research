package org2.apache.commons.text.similarity;

public class CosineDistance implements EditDistance<Double> {
    private final CosineSimilarity cosineSimilarity = new CosineSimilarity();
    private final Tokenizer<CharSequence> tokenizer = new RegexTokenizer();

    public Double apply(CharSequence charSequence, CharSequence charSequence2) {
        return Double.valueOf(1.0d - this.cosineSimilarity.cosineSimilarity(Counter.m22676((CharSequence[]) this.tokenizer.m22679(charSequence)), Counter.m22676((CharSequence[]) this.tokenizer.m22679(charSequence2))).doubleValue());
    }
}
