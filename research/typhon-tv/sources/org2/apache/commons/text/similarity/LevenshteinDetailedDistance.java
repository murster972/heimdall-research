package org2.apache.commons.text.similarity;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.lang.reflect.Array;
import java.util.Arrays;

public class LevenshteinDetailedDistance implements EditDistance<LevenshteinResults> {
    private static final LevenshteinDetailedDistance DEFAULT_INSTANCE = new LevenshteinDetailedDistance();
    private final Integer threshold;

    public LevenshteinDetailedDistance() {
        this((Integer) null);
    }

    public LevenshteinDetailedDistance(Integer num) {
        if (num == null || num.intValue() >= 0) {
            this.threshold = num;
            return;
        }
        throw new IllegalArgumentException("Threshold must not be negative");
    }

    private static LevenshteinResults findDetailedResults(CharSequence charSequence, CharSequence charSequence2, int[][] iArr, boolean z) {
        int i = 0;
        int i2 = 0;
        int length = charSequence2.length();
        int length2 = charSequence.length();
        int i3 = 0;
        while (length >= 0 && length2 >= 0) {
            int i4 = length2 == 0 ? -1 : iArr[length][length2 - 1];
            int i5 = length == 0 ? -1 : iArr[length - 1][length2];
            int i6 = (length <= 0 || length2 <= 0) ? -1 : iArr[length - 1][length2 - 1];
            if (i4 == -1 && i5 == -1 && i6 == -1) {
                break;
            }
            int i7 = iArr[length][length2];
            if (length2 <= 0 || length <= 0 || charSequence.charAt(length2 - 1) != charSequence2.charAt(length - 1)) {
                boolean z2 = false;
                boolean z3 = false;
                if ((i7 - 1 == i4 && i7 <= i6 && i7 <= i5) || (i6 == -1 && i5 == -1)) {
                    length2--;
                    if (z) {
                        i2++;
                        z3 = true;
                    } else {
                        i++;
                        z2 = true;
                    }
                } else if ((i7 - 1 == i5 && i7 <= i6 && i7 <= i4) || (i6 == -1 && i4 == -1)) {
                    length--;
                    if (z) {
                        i++;
                        z2 = true;
                    } else {
                        i2++;
                        z3 = true;
                    }
                }
                if (!z3 && !z2) {
                    length2--;
                    length--;
                    i3++;
                }
            } else {
                length2--;
                length--;
            }
        }
        return new LevenshteinResults(Integer.valueOf(i2 + i + i3), Integer.valueOf(i2), Integer.valueOf(i), Integer.valueOf(i3));
    }

    public static LevenshteinDetailedDistance getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    private static LevenshteinResults limitedCompare(CharSequence charSequence, CharSequence charSequence2, int i) {
        int i2;
        boolean z;
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        } else if (i < 0) {
            throw new IllegalArgumentException("Threshold must not be negative");
        } else {
            int length = charSequence.length();
            int length2 = charSequence2.length();
            if (length == 0) {
                return length2 <= i ? new LevenshteinResults(Integer.valueOf(length2), Integer.valueOf(length2), 0, 0) : new LevenshteinResults(-1, 0, 0, 0);
            }
            if (length2 == 0) {
                return length <= i ? new LevenshteinResults(Integer.valueOf(length), 0, Integer.valueOf(length), 0) : new LevenshteinResults(-1, 0, 0, 0);
            }
            if (length > length2) {
                i2 = length2;
                length2 = charSequence.length();
                z = true;
            } else {
                i2 = length;
                z = false;
                CharSequence charSequence3 = charSequence2;
                charSequence2 = charSequence;
                charSequence = charSequence3;
            }
            int[] iArr = new int[(i2 + 1)];
            int[] iArr2 = new int[(i2 + 1)];
            int[][] iArr3 = (int[][]) Array.newInstance(Integer.TYPE, new int[]{length2 + 1, i2 + 1});
            for (int i3 = 0; i3 <= i2; i3++) {
                iArr3[0][i3] = i3;
            }
            for (int i4 = 0; i4 <= length2; i4++) {
                iArr3[i4][0] = i4;
            }
            int min = Math.min(i2, i) + 1;
            for (int i5 = 0; i5 < min; i5++) {
                iArr[i5] = i5;
            }
            Arrays.fill(iArr, min, iArr.length, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
            Arrays.fill(iArr2, MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT);
            int i6 = 1;
            int[] iArr4 = iArr2;
            while (i6 <= length2) {
                char charAt = charSequence.charAt(i6 - 1);
                iArr4[0] = i6;
                int max = Math.max(1, i6 - i);
                int min2 = i6 > MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT - i ? i2 : Math.min(i2, i6 + i);
                if (max > min2) {
                    return new LevenshteinResults(-1, 0, 0, 0);
                }
                if (max > 1) {
                    iArr4[max - 1] = Integer.MAX_VALUE;
                }
                while (max <= min2) {
                    if (charSequence2.charAt(max - 1) == charAt) {
                        iArr4[max] = iArr[max - 1];
                    } else {
                        iArr4[max] = Math.min(Math.min(iArr4[max - 1], iArr[max]), iArr[max - 1]) + 1;
                    }
                    iArr3[i6][max] = iArr4[max];
                    max++;
                }
                i6++;
                int[] iArr5 = iArr;
                iArr = iArr4;
                iArr4 = iArr5;
            }
            return iArr[i2] <= i ? findDetailedResults(charSequence2, charSequence, iArr3, z) : new LevenshteinResults(-1, 0, 0, 0);
        }
    }

    private static LevenshteinResults unlimitedCompare(CharSequence charSequence, CharSequence charSequence2) {
        int i;
        boolean z;
        if (charSequence == null || charSequence2 == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }
        int length = charSequence.length();
        int length2 = charSequence2.length();
        if (length == 0) {
            return new LevenshteinResults(Integer.valueOf(length2), Integer.valueOf(length2), 0, 0);
        }
        if (length2 == 0) {
            return new LevenshteinResults(Integer.valueOf(length), 0, Integer.valueOf(length), 0);
        }
        if (length > length2) {
            i = length2;
            length2 = charSequence.length();
            z = true;
        } else {
            i = length;
            z = false;
            CharSequence charSequence3 = charSequence2;
            charSequence2 = charSequence;
            charSequence = charSequence3;
        }
        int[] iArr = new int[(i + 1)];
        int[] iArr2 = new int[(i + 1)];
        int[][] iArr3 = (int[][]) Array.newInstance(Integer.TYPE, new int[]{length2 + 1, i + 1});
        for (int i2 = 0; i2 <= i; i2++) {
            iArr3[0][i2] = i2;
        }
        for (int i3 = 0; i3 <= length2; i3++) {
            iArr3[i3][0] = i3;
        }
        for (int i4 = 0; i4 <= i; i4++) {
            iArr[i4] = i4;
        }
        int i5 = 1;
        int[] iArr4 = iArr2;
        while (i5 <= length2) {
            char charAt = charSequence.charAt(i5 - 1);
            iArr4[0] = i5;
            for (int i6 = 1; i6 <= i; i6++) {
                iArr4[i6] = Math.min(Math.min(iArr4[i6 - 1] + 1, iArr[i6] + 1), (charSequence2.charAt(i6 + -1) == charAt ? 0 : 1) + iArr[i6 - 1]);
                iArr3[i5][i6] = iArr4[i6];
            }
            i5++;
            int[] iArr5 = iArr;
            iArr = iArr4;
            iArr4 = iArr5;
        }
        return findDetailedResults(charSequence2, charSequence, iArr3, z);
    }

    public LevenshteinResults apply(CharSequence charSequence, CharSequence charSequence2) {
        return this.threshold != null ? limitedCompare(charSequence, charSequence2, this.threshold.intValue()) : unlimitedCompare(charSequence, charSequence2);
    }

    public Integer getThreshold() {
        return this.threshold;
    }
}
