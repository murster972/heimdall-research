package org2.apache.commons.text;

import java.util.Arrays;

public abstract class StrMatcher {
    private static final StrMatcher COMMA_MATCHER = new CharMatcher(',');
    private static final StrMatcher DOUBLE_QUOTE_MATCHER = new CharMatcher('\"');
    private static final StrMatcher NONE_MATCHER = new NoMatcher();
    private static final StrMatcher QUOTE_MATCHER = new CharSetMatcher("'\"".toCharArray());
    private static final StrMatcher SINGLE_QUOTE_MATCHER = new CharMatcher('\'');
    private static final StrMatcher SPACE_MATCHER = new CharMatcher(' ');
    private static final StrMatcher SPLIT_MATCHER = new CharSetMatcher(" \t\n\r\f".toCharArray());
    private static final StrMatcher TAB_MATCHER = new CharMatcher(9);
    private static final StrMatcher TRIM_MATCHER = new TrimMatcher();

    static final class CharMatcher extends StrMatcher {

        /* renamed from: 龘  reason: contains not printable characters */
        private final char f17751;

        CharMatcher(char c) {
            this.f17751 = c;
        }

        public int isMatch(char[] cArr, int i, int i2, int i3) {
            return this.f17751 == cArr[i] ? 1 : 0;
        }
    }

    static final class CharSetMatcher extends StrMatcher {

        /* renamed from: 龘  reason: contains not printable characters */
        private final char[] f17752;

        CharSetMatcher(char[] cArr) {
            this.f17752 = (char[]) cArr.clone();
            Arrays.sort(this.f17752);
        }

        public int isMatch(char[] cArr, int i, int i2, int i3) {
            return Arrays.binarySearch(this.f17752, cArr[i]) >= 0 ? 1 : 0;
        }
    }

    static final class NoMatcher extends StrMatcher {
        NoMatcher() {
        }

        public int isMatch(char[] cArr, int i, int i2, int i3) {
            return 0;
        }
    }

    static final class StringMatcher extends StrMatcher {

        /* renamed from: 龘  reason: contains not printable characters */
        private final char[] f17753;

        StringMatcher(String str) {
            this.f17753 = str.toCharArray();
        }

        public int isMatch(char[] cArr, int i, int i2, int i3) {
            int length = this.f17753.length;
            if (i + length > i3) {
                return 0;
            }
            int i4 = 0;
            while (i4 < this.f17753.length) {
                if (this.f17753[i4] != cArr[i]) {
                    return 0;
                }
                i4++;
                i++;
            }
            return length;
        }

        public String toString() {
            return super.toString() + ' ' + Arrays.toString(this.f17753);
        }
    }

    static final class TrimMatcher extends StrMatcher {
        TrimMatcher() {
        }

        public int isMatch(char[] cArr, int i, int i2, int i3) {
            return cArr[i] <= ' ' ? 1 : 0;
        }
    }

    protected StrMatcher() {
    }

    public static StrMatcher charMatcher(char c) {
        return new CharMatcher(c);
    }

    public static StrMatcher charSetMatcher(String str) {
        return (str == null || str.length() == 0) ? NONE_MATCHER : str.length() == 1 ? new CharMatcher(str.charAt(0)) : new CharSetMatcher(str.toCharArray());
    }

    public static StrMatcher charSetMatcher(char... cArr) {
        return (cArr == null || cArr.length == 0) ? NONE_MATCHER : cArr.length == 1 ? new CharMatcher(cArr[0]) : new CharSetMatcher(cArr);
    }

    public static StrMatcher commaMatcher() {
        return COMMA_MATCHER;
    }

    public static StrMatcher doubleQuoteMatcher() {
        return DOUBLE_QUOTE_MATCHER;
    }

    public static StrMatcher noneMatcher() {
        return NONE_MATCHER;
    }

    public static StrMatcher quoteMatcher() {
        return QUOTE_MATCHER;
    }

    public static StrMatcher singleQuoteMatcher() {
        return SINGLE_QUOTE_MATCHER;
    }

    public static StrMatcher spaceMatcher() {
        return SPACE_MATCHER;
    }

    public static StrMatcher splitMatcher() {
        return SPLIT_MATCHER;
    }

    public static StrMatcher stringMatcher(String str) {
        return (str == null || str.length() == 0) ? NONE_MATCHER : new StringMatcher(str);
    }

    public static StrMatcher tabMatcher() {
        return TAB_MATCHER;
    }

    public static StrMatcher trimMatcher() {
        return TRIM_MATCHER;
    }

    public int isMatch(char[] cArr, int i) {
        return isMatch(cArr, i, 0, cArr.length);
    }

    public abstract int isMatch(char[] cArr, int i, int i2, int i3);
}
