package org2.apache.commons.text;

public interface Builder<T> {
    T build();
}
