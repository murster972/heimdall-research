package org2.apache.commons.text.translate;

import java.io.IOException;
import java.io.Writer;
import org.apache.commons.lang3.CharUtils;
import org2.apache.commons.lang3.StringUtils;

public final class CsvTranslators {
    private static final char CSV_DELIMITER = ',';
    /* access modifiers changed from: private */
    public static final String CSV_ESCAPED_QUOTE_STR = (CSV_QUOTE_STR + CSV_QUOTE_STR);
    private static final char CSV_QUOTE = '\"';
    /* access modifiers changed from: private */
    public static final String CSV_QUOTE_STR = String.valueOf(CSV_QUOTE);
    /* access modifiers changed from: private */
    public static final char[] CSV_SEARCH_CHARS = {CSV_DELIMITER, CSV_QUOTE, CharUtils.CR, 10};

    public static class CsvEscaper extends SinglePassTranslator {
        public /* bridge */ /* synthetic */ int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
            return super.translate(charSequence, i, writer);
        }

        /* access modifiers changed from: package-private */
        public void translateWhole(CharSequence charSequence, Writer writer) throws IOException {
            String charSequence2 = charSequence.toString();
            if (StringUtils.containsNone(charSequence2, CsvTranslators.CSV_SEARCH_CHARS)) {
                writer.write(charSequence2);
                return;
            }
            writer.write(34);
            writer.write(StringUtils.replace(charSequence2, CsvTranslators.CSV_QUOTE_STR, CsvTranslators.CSV_ESCAPED_QUOTE_STR));
            writer.write(34);
        }
    }

    public static class CsvUnescaper extends SinglePassTranslator {
        public /* bridge */ /* synthetic */ int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
            return super.translate(charSequence, i, writer);
        }

        /* access modifiers changed from: package-private */
        public void translateWhole(CharSequence charSequence, Writer writer) throws IOException {
            if (charSequence.charAt(0) == '\"' && charSequence.charAt(charSequence.length() - 1) == '\"') {
                String charSequence2 = charSequence.subSequence(1, charSequence.length() - 1).toString();
                if (StringUtils.containsAny(charSequence2, CsvTranslators.CSV_SEARCH_CHARS)) {
                    writer.write(StringUtils.replace(charSequence2, CsvTranslators.CSV_ESCAPED_QUOTE_STR, CsvTranslators.CSV_QUOTE_STR));
                } else {
                    writer.write(charSequence.toString());
                }
            } else {
                writer.write(charSequence.toString());
            }
        }
    }

    private CsvTranslators() {
    }
}
