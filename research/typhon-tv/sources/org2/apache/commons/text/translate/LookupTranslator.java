package org2.apache.commons.text.translate;

import java.io.IOException;
import java.io.Writer;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class LookupTranslator extends CharSequenceTranslator {
    private final int longest;
    private final Map<String, String> lookupMap;
    private final HashSet<Character> prefixSet;
    private final int shortest;

    public LookupTranslator(Map<CharSequence, CharSequence> map) {
        if (map == null) {
            throw new InvalidParameterException("lookupMap cannot be null");
        }
        this.lookupMap = new HashMap();
        this.prefixSet = new HashSet<>();
        int i = 0;
        int i2 = Integer.MAX_VALUE;
        for (Map.Entry next : map.entrySet()) {
            this.lookupMap.put(((CharSequence) next.getKey()).toString(), ((CharSequence) next.getValue()).toString());
            this.prefixSet.add(Character.valueOf(((CharSequence) next.getKey()).charAt(0)));
            int length = ((CharSequence) next.getKey()).length();
            int i3 = length < i2 ? length : i2;
            if (length <= i) {
                length = i;
            }
            i = length;
            i2 = i3;
        }
        this.shortest = i2;
        this.longest = i;
    }

    public int translate(CharSequence charSequence, int i, Writer writer) throws IOException {
        if (this.prefixSet.contains(Character.valueOf(charSequence.charAt(i)))) {
            int i2 = this.longest;
            if (this.longest + i > charSequence.length()) {
                i2 = charSequence.length() - i;
            }
            while (true) {
                int i3 = i2;
                if (i3 < this.shortest) {
                    break;
                }
                String str = this.lookupMap.get(charSequence.subSequence(i, i + i3).toString());
                if (str != null) {
                    writer.write(str);
                    return i3;
                }
                i2 = i3 - 1;
            }
        }
        return 0;
    }
}
