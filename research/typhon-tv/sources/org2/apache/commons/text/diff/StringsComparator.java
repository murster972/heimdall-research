package org2.apache.commons.text.diff;

public class StringsComparator {
    private final String left;
    private final String right;
    private final int[] vDown;
    private final int[] vUp;

    private static class Snake {

        /* renamed from: 靐  reason: contains not printable characters */
        private final int f17754;

        /* renamed from: 齉  reason: contains not printable characters */
        private final int f17755;

        /* renamed from: 龘  reason: contains not printable characters */
        private final int f17756;

        public Snake(int i, int i2, int i3) {
            this.f17756 = i;
            this.f17754 = i2;
            this.f17755 = i3;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public int m22673() {
            return this.f17754;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public int m22674() {
            return this.f17755;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public int m22675() {
            return this.f17756;
        }
    }

    public StringsComparator(String str, String str2) {
        this.left = str;
        this.right = str2;
        int length = str.length() + str2.length() + 2;
        this.vDown = new int[length];
        this.vUp = new int[length];
    }

    private void buildScript(int i, int i2, int i3, int i4, EditScript<Character> editScript) {
        Snake middleSnake = getMiddleSnake(i, i2, i3, i4);
        if (middleSnake == null || ((middleSnake.m22675() == i2 && middleSnake.m22674() == i2 - i4) || (middleSnake.m22673() == i && middleSnake.m22674() == i - i3))) {
            int i5 = i3;
            int i6 = i;
            while (true) {
                if (i6 >= i2 && i5 >= i4) {
                    return;
                }
                if (i6 < i2 && i5 < i4 && this.left.charAt(i6) == this.right.charAt(i5)) {
                    editScript.append((KeepCommand<Character>) new KeepCommand(Character.valueOf(this.left.charAt(i6))));
                    i6++;
                    i5++;
                } else if (i2 - i > i4 - i3) {
                    editScript.append((DeleteCommand<Character>) new DeleteCommand(Character.valueOf(this.left.charAt(i6))));
                    i6++;
                } else {
                    editScript.append((InsertCommand<Character>) new InsertCommand(Character.valueOf(this.right.charAt(i5))));
                    i5++;
                }
            }
        } else {
            buildScript(i, middleSnake.m22675(), i3, middleSnake.m22675() - middleSnake.m22674(), editScript);
            for (int r0 = middleSnake.m22675(); r0 < middleSnake.m22673(); r0++) {
                editScript.append((KeepCommand<Character>) new KeepCommand(Character.valueOf(this.left.charAt(r0))));
            }
            buildScript(middleSnake.m22673(), i2, middleSnake.m22673() - middleSnake.m22674(), i4, editScript);
        }
    }

    private Snake buildSnake(int i, int i2, int i3, int i4) {
        int i5 = i;
        while (i5 - i2 < i4 && i5 < i3 && this.left.charAt(i5) == this.right.charAt(i5 - i2)) {
            i5++;
        }
        return new Snake(i, i5, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x012a, code lost:
        r4 = r4 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org2.apache.commons.text.diff.StringsComparator.Snake getMiddleSnake(int r10, int r11, int r12, int r13) {
        /*
            r9 = this;
            int r0 = r11 - r10
            int r1 = r13 - r12
            if (r0 == 0) goto L_0x0008
            if (r1 != 0) goto L_0x000a
        L_0x0008:
            r0 = 0
        L_0x0009:
            return r0
        L_0x000a:
            int r5 = r0 - r1
            int r0 = r0 + r1
            int r1 = r0 % 2
            if (r1 != 0) goto L_0x006b
        L_0x0011:
            int r6 = r0 / 2
            int[] r0 = r9.vDown
            int r1 = r6 + 1
            r0[r1] = r10
            int[] r0 = r9.vUp
            int r1 = r6 + 1
            int r2 = r11 + 1
            r0[r1] = r2
            r0 = 0
            r4 = r0
        L_0x0023:
            if (r4 > r6) goto L_0x012f
            int r0 = -r4
            r2 = r0
        L_0x0027:
            if (r2 > r4) goto L_0x00a6
            int r3 = r2 + r6
            int r0 = -r4
            if (r2 == r0) goto L_0x003e
            if (r2 == r4) goto L_0x006e
            int[] r0 = r9.vDown
            int r1 = r3 + -1
            r0 = r0[r1]
            int[] r1 = r9.vDown
            int r7 = r3 + 1
            r1 = r1[r7]
            if (r0 >= r1) goto L_0x006e
        L_0x003e:
            int[] r0 = r9.vDown
            int[] r1 = r9.vDown
            int r7 = r3 + 1
            r1 = r1[r7]
            r0[r3] = r1
        L_0x0048:
            int[] r0 = r9.vDown
            r1 = r0[r3]
            int r0 = r1 - r10
            int r0 = r0 + r12
            int r0 = r0 - r2
        L_0x0050:
            if (r1 >= r11) goto L_0x007b
            if (r0 >= r13) goto L_0x007b
            java.lang.String r7 = r9.left
            char r7 = r7.charAt(r1)
            java.lang.String r8 = r9.right
            char r8 = r8.charAt(r0)
            if (r7 != r8) goto L_0x007b
            int[] r7 = r9.vDown
            int r1 = r1 + 1
            r7[r3] = r1
            int r0 = r0 + 1
            goto L_0x0050
        L_0x006b:
            int r0 = r0 + 1
            goto L_0x0011
        L_0x006e:
            int[] r0 = r9.vDown
            int[] r1 = r9.vDown
            int r7 = r3 + -1
            r1 = r1[r7]
            int r1 = r1 + 1
            r0[r3] = r1
            goto L_0x0048
        L_0x007b:
            int r0 = r5 % 2
            if (r0 == 0) goto L_0x00a2
            int r0 = r5 - r4
            if (r0 > r2) goto L_0x00a2
            int r0 = r5 + r4
            if (r2 > r0) goto L_0x00a2
            int[] r0 = r9.vUp
            int r1 = r3 - r5
            r0 = r0[r1]
            int[] r1 = r9.vDown
            r1 = r1[r3]
            if (r0 > r1) goto L_0x00a2
            int[] r0 = r9.vUp
            int r1 = r3 - r5
            r0 = r0[r1]
            int r1 = r2 + r10
            int r1 = r1 - r12
            org2.apache.commons.text.diff.StringsComparator$Snake r0 = r9.buildSnake(r0, r1, r11, r13)
            goto L_0x0009
        L_0x00a2:
            int r0 = r2 + 2
            r2 = r0
            goto L_0x0027
        L_0x00a6:
            int r0 = r5 - r4
            r3 = r0
        L_0x00a9:
            int r0 = r5 + r4
            if (r3 > r0) goto L_0x012a
            int r0 = r3 + r6
            int r7 = r0 - r5
            int r0 = r5 - r4
            if (r3 == r0) goto L_0x00c7
            int r0 = r5 + r4
            if (r3 == r0) goto L_0x00f9
            int[] r0 = r9.vUp
            int r1 = r7 + 1
            r0 = r0[r1]
            int[] r1 = r9.vUp
            int r2 = r7 + -1
            r1 = r1[r2]
            if (r0 > r1) goto L_0x00f9
        L_0x00c7:
            int[] r0 = r9.vUp
            int[] r1 = r9.vUp
            int r2 = r7 + 1
            r1 = r1[r2]
            int r1 = r1 + -1
            r0[r7] = r1
        L_0x00d3:
            int[] r0 = r9.vUp
            r0 = r0[r7]
            int r1 = r0 + -1
            int r0 = r1 - r10
            int r0 = r0 + r12
            int r0 = r0 - r3
        L_0x00dd:
            if (r1 < r10) goto L_0x0104
            if (r0 < r12) goto L_0x0104
            java.lang.String r2 = r9.left
            char r2 = r2.charAt(r1)
            java.lang.String r8 = r9.right
            char r8 = r8.charAt(r0)
            if (r2 != r8) goto L_0x0104
            int[] r8 = r9.vUp
            int r2 = r1 + -1
            r8[r7] = r1
            int r0 = r0 + -1
            r1 = r2
            goto L_0x00dd
        L_0x00f9:
            int[] r0 = r9.vUp
            int[] r1 = r9.vUp
            int r2 = r7 + -1
            r1 = r1[r2]
            r0[r7] = r1
            goto L_0x00d3
        L_0x0104:
            int r0 = r5 % 2
            if (r0 != 0) goto L_0x0126
            int r0 = -r4
            if (r0 > r3) goto L_0x0126
            if (r3 > r4) goto L_0x0126
            int[] r0 = r9.vUp
            r0 = r0[r7]
            int[] r1 = r9.vDown
            int r2 = r7 + r5
            r1 = r1[r2]
            if (r0 > r1) goto L_0x0126
            int[] r0 = r9.vUp
            r0 = r0[r7]
            int r1 = r3 + r10
            int r1 = r1 - r12
            org2.apache.commons.text.diff.StringsComparator$Snake r0 = r9.buildSnake(r0, r1, r11, r13)
            goto L_0x0009
        L_0x0126:
            int r0 = r3 + 2
            r3 = r0
            goto L_0x00a9
        L_0x012a:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0023
        L_0x012f:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Internal Error"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.text.diff.StringsComparator.getMiddleSnake(int, int, int, int):org2.apache.commons.text.diff.StringsComparator$Snake");
    }

    public EditScript<Character> getScript() {
        EditScript<Character> editScript = new EditScript<>();
        buildScript(0, this.left.length(), 0, this.right.length(), editScript);
        return editScript;
    }
}
