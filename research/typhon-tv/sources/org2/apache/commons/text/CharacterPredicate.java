package org2.apache.commons.text;

public interface CharacterPredicate {
    boolean test(int i);
}
