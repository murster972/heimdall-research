package org2.apache.commons.text;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;

public final class AlphabetConverter {
    private static final String ARROW = " -> ";
    private final int encodedLetterLength;
    private final Map<String, String> encodedToOriginal;
    private final Map<Integer, String> originalToEncoded;

    private AlphabetConverter(Map<Integer, String> map, Map<String, String> map2, int i) {
        this.originalToEncoded = map;
        this.encodedToOriginal = map2;
        this.encodedLetterLength = i;
    }

    /* JADX WARNING: type inference failed for: r11v0, types: [java.util.Iterator, java.util.Iterator<java.lang.Integer>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addSingleEncoding(int r8, java.lang.String r9, java.util.Collection<java.lang.Integer> r10, java.util.Iterator<java.lang.Integer> r11, java.util.Map<java.lang.Integer, java.lang.String> r12) {
        /*
            r7 = this;
            if (r8 <= 0) goto L_0x0049
            java.util.Iterator r6 = r10.iterator()
        L_0x0006:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x006d
            java.lang.Object r0 = r6.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            boolean r1 = r11.hasNext()
            if (r1 == 0) goto L_0x006d
            int r1 = r7.encodedLetterLength
            if (r8 != r1) goto L_0x002a
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            boolean r1 = r12.containsKey(r1)
            if (r1 != 0) goto L_0x0006
        L_0x002a:
            int r1 = r8 + -1
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.String r0 = codePointToString(r0)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = r0.toString()
            r0 = r7
            r3 = r10
            r4 = r11
            r5 = r12
            r0.addSingleEncoding(r1, r2, r3, r4, r5)
            goto L_0x0006
        L_0x0049:
            java.lang.Object r0 = r11.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
        L_0x004f:
            boolean r1 = r12.containsKey(r0)
            if (r1 == 0) goto L_0x0075
            int r1 = r0.intValue()
            java.lang.String r1 = codePointToString(r1)
            java.util.Map<java.lang.Integer, java.lang.String> r2 = r7.originalToEncoded
            r2.put(r0, r1)
            java.util.Map<java.lang.String, java.lang.String> r0 = r7.encodedToOriginal
            r0.put(r1, r1)
            boolean r0 = r11.hasNext()
            if (r0 != 0) goto L_0x006e
        L_0x006d:
            return
        L_0x006e:
            java.lang.Object r0 = r11.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            goto L_0x004f
        L_0x0075:
            int r1 = r0.intValue()
            java.lang.String r1 = codePointToString(r1)
            java.util.Map<java.lang.Integer, java.lang.String> r2 = r7.originalToEncoded
            r2.put(r0, r9)
            java.util.Map<java.lang.String, java.lang.String> r0 = r7.encodedToOriginal
            r0.put(r9, r1)
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.apache.commons.text.AlphabetConverter.addSingleEncoding(int, java.lang.String, java.util.Collection, java.util.Iterator, java.util.Map):void");
    }

    private static String codePointToString(int i) {
        return Character.charCount(i) == 1 ? String.valueOf((char) i) : new String(Character.toChars(i));
    }

    private static Integer[] convertCharsToIntegers(Character[] chArr) {
        if (chArr == null || chArr.length == 0) {
            return new Integer[0];
        }
        Integer[] numArr = new Integer[chArr.length];
        for (int i = 0; i < chArr.length; i++) {
            numArr[i] = Integer.valueOf(chArr[i].charValue());
        }
        return numArr;
    }

    public static AlphabetConverter createConverter(Integer[] numArr, Integer[] numArr2, Integer[] numArr3) {
        Integer num;
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>(Arrays.asList(numArr));
        LinkedHashSet linkedHashSet2 = new LinkedHashSet(Arrays.asList(numArr2));
        LinkedHashSet<Integer> linkedHashSet3 = new LinkedHashSet<>(Arrays.asList(numArr3));
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        HashMap hashMap = new HashMap();
        for (Integer intValue : linkedHashSet3) {
            int intValue2 = intValue.intValue();
            if (!linkedHashSet.contains(Integer.valueOf(intValue2))) {
                throw new IllegalArgumentException("Can not use 'do not encode' list because original alphabet does not contain '" + codePointToString(intValue2) + "'");
            } else if (!linkedHashSet2.contains(Integer.valueOf(intValue2))) {
                throw new IllegalArgumentException("Can not use 'do not encode' list because encoding alphabet does not contain '" + codePointToString(intValue2) + "'");
            } else {
                hashMap.put(Integer.valueOf(intValue2), codePointToString(intValue2));
            }
        }
        if (linkedHashSet2.size() >= linkedHashSet.size()) {
            Iterator it2 = linkedHashSet2.iterator();
            for (Integer intValue3 : linkedHashSet) {
                int intValue4 = intValue3.intValue();
                String codePointToString = codePointToString(intValue4);
                if (hashMap.containsKey(Integer.valueOf(intValue4))) {
                    linkedHashMap.put(Integer.valueOf(intValue4), codePointToString);
                    linkedHashMap2.put(codePointToString, codePointToString);
                } else {
                    Object next = it2.next();
                    while (true) {
                        num = (Integer) next;
                        if (!linkedHashSet3.contains(num)) {
                            break;
                        }
                        next = it2.next();
                    }
                    String codePointToString2 = codePointToString(num.intValue());
                    linkedHashMap.put(Integer.valueOf(intValue4), codePointToString2);
                    linkedHashMap2.put(codePointToString2, codePointToString);
                }
            }
            return new AlphabetConverter(linkedHashMap, linkedHashMap2, 1);
        } else if (linkedHashSet2.size() - linkedHashSet3.size() < 2) {
            throw new IllegalArgumentException("Must have at least two encoding characters (excluding those in the 'do not encode' list), but has " + (linkedHashSet2.size() - linkedHashSet3.size()));
        } else {
            int size = (linkedHashSet.size() - linkedHashSet3.size()) / (linkedHashSet2.size() - linkedHashSet3.size());
            int i = 1;
            while (size / linkedHashSet2.size() >= 1) {
                size /= linkedHashSet2.size();
                i++;
            }
            int i2 = i + 1;
            AlphabetConverter alphabetConverter = new AlphabetConverter(linkedHashMap, linkedHashMap2, i2);
            alphabetConverter.addSingleEncoding(i2, "", linkedHashSet2, linkedHashSet.iterator(), hashMap);
            return alphabetConverter;
        }
    }

    public static AlphabetConverter createConverterFromChars(Character[] chArr, Character[] chArr2, Character[] chArr3) {
        return createConverter(convertCharsToIntegers(chArr), convertCharsToIntegers(chArr2), convertCharsToIntegers(chArr3));
    }

    public static AlphabetConverter createConverterFromMap(Map<Integer, String> map) {
        Map<Integer, String> unmodifiableMap = Collections.unmodifiableMap(map);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        int i = 1;
        Iterator<Map.Entry<Integer, String>> it2 = unmodifiableMap.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it2.hasNext()) {
                return new AlphabetConverter(unmodifiableMap, linkedHashMap, i2);
            }
            Map.Entry next = it2.next();
            linkedHashMap.put(next.getValue(), codePointToString(((Integer) next.getKey()).intValue()));
            i = ((String) next.getValue()).length() > i2 ? ((String) next.getValue()).length() : i2;
        }
    }

    public String decode(String str) throws UnsupportedEncodingException {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= str.length()) {
                return sb.toString();
            }
            Integer valueOf = Integer.valueOf(str.codePointAt(i2));
            String codePointToString = codePointToString(valueOf.intValue());
            if (codePointToString.equals(this.originalToEncoded.get(valueOf))) {
                sb.append(codePointToString);
                i = i2 + 1;
            } else if (this.encodedLetterLength + i2 > str.length()) {
                throw new UnsupportedEncodingException("Unexpected end of string while decoding " + str);
            } else {
                String substring = str.substring(i2, this.encodedLetterLength + i2);
                String str2 = this.encodedToOriginal.get(substring);
                if (str2 == null) {
                    throw new UnsupportedEncodingException("Unexpected string without decoding (" + substring + ") in " + str);
                }
                sb.append(str2);
                i = this.encodedLetterLength + i2;
            }
        }
    }

    public String encode(String str) throws UnsupportedEncodingException {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= str.length()) {
                return sb.toString();
            }
            int codePointAt = str.codePointAt(i2);
            String str2 = this.originalToEncoded.get(Integer.valueOf(codePointAt));
            if (str2 == null) {
                throw new UnsupportedEncodingException("Couldn't find encoding for '" + codePointToString(codePointAt) + "' in " + str);
            }
            sb.append(str2);
            i = Character.charCount(codePointAt) + i2;
        }
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AlphabetConverter)) {
            return false;
        }
        AlphabetConverter alphabetConverter = (AlphabetConverter) obj;
        if (!this.originalToEncoded.equals(alphabetConverter.originalToEncoded) || !this.encodedToOriginal.equals(alphabetConverter.encodedToOriginal) || this.encodedLetterLength != alphabetConverter.encodedLetterLength) {
            z = false;
        }
        return z;
    }

    public int getEncodedCharLength() {
        return this.encodedLetterLength;
    }

    public Map<Integer, String> getOriginalToEncoded() {
        return Collections.unmodifiableMap(this.originalToEncoded);
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.originalToEncoded, this.encodedToOriginal, Integer.valueOf(this.encodedLetterLength)});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : this.originalToEncoded.entrySet()) {
            sb.append(codePointToString(((Integer) next.getKey()).intValue())).append(ARROW).append((String) next.getValue()).append(System.lineSeparator());
        }
        return sb.toString();
    }
}
