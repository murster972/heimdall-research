package org2.jsoup.helper;

public final class Validate {
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m23512(String str) {
        throw new IllegalArgumentException(str);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m23513(boolean z) {
        if (z) {
            throw new IllegalArgumentException("Must be false");
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m23514(boolean z, String str) {
        if (z) {
            throw new IllegalArgumentException(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23515(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Object must not be null");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23516(Object obj, String str) {
        if (obj == null) {
            throw new IllegalArgumentException(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23517(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("String must not be empty");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23518(String str, String str2) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException(str2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23519(boolean z) {
        if (!z) {
            throw new IllegalArgumentException("Must be true");
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23520(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23521(Object[] objArr) {
        m23522(objArr, "Array must not contain any null objects");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23522(Object[] objArr, String str) {
        for (Object obj : objArr) {
            if (obj == null) {
                throw new IllegalArgumentException(str);
            }
        }
    }
}
