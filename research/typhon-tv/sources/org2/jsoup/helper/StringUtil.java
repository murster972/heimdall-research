package org2.jsoup.helper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import org.apache.commons.lang3.StringUtils;

public final class StringUtil {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final ThreadLocal<StringBuilder> f18260 = new ThreadLocal<StringBuilder>() {
        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public StringBuilder initialValue() {
            return new StringBuilder(8192);
        }
    };

    /* renamed from: 龘  reason: contains not printable characters */
    static final String[] f18261 = {"", StringUtils.SPACE, "  ", "   ", "    ", "     ", "      ", "       ", "        ", "         ", "          ", "           ", "            ", "             ", "              ", "               ", "                ", "                 ", "                  ", "                   ", "                    "};

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m23497(int i) {
        return i == 32 || i == 9 || i == 10 || i == 12 || i == 13;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m23498(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isDigit(str.codePointAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m23499(String str, String[] strArr) {
        return Arrays.binarySearch(strArr, str) >= 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m23500(String str) {
        StringBuilder r0 = m23506();
        m23508(r0, str, false);
        return r0.toString();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static boolean m23501(int i) {
        return i == 32 || i == 9 || i == 10 || i == 12 || i == 13 || i == 160;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m23502(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("width must be > 0");
        } else if (i < f18261.length) {
            return f18261[i];
        } else {
            char[] cArr = new char[i];
            for (int i2 = 0; i2 < i; i2++) {
                cArr[i2] = ' ';
            }
            return String.valueOf(cArr);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m23503(String str, String str2) {
        try {
            try {
                return m23507(new URL(str), str2).toExternalForm();
            } catch (MalformedURLException e) {
                return "";
            }
        } catch (MalformedURLException e2) {
            return new URL(str2).toExternalForm();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m23504(Collection collection, String str) {
        return m23505(collection.iterator(), str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m23505(Iterator it2, String str) {
        if (!it2.hasNext()) {
            return "";
        }
        String obj = it2.next().toString();
        if (!it2.hasNext()) {
            return obj;
        }
        StringBuilder append = new StringBuilder(64).append(obj);
        while (it2.hasNext()) {
            append.append(str);
            append.append(it2.next());
        }
        return append.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static StringBuilder m23506() {
        StringBuilder sb = f18260.get();
        if (sb.length() > 8192) {
            StringBuilder sb2 = new StringBuilder(8192);
            f18260.set(sb2);
            return sb2;
        }
        sb.delete(0, sb.length());
        return sb;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static URL m23507(URL url, String str) throws MalformedURLException {
        if (str.startsWith("?")) {
            str = url.getPath() + str;
        }
        if (str.indexOf(46) == 0 && url.getFile().indexOf(47) != 0) {
            url = new URL(url.getProtocol(), url.getHost(), url.getPort(), InternalZipTyphoonApp.ZIP_FILE_SEPARATOR + url.getFile());
        }
        return new URL(url, str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m23508(StringBuilder sb, String str, boolean z) {
        int length = str.length();
        int i = 0;
        boolean z2 = false;
        boolean z3 = false;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            if (!m23501(codePointAt)) {
                sb.appendCodePoint(codePointAt);
                z2 = true;
                z3 = false;
            } else if ((!z || z2) && !z3) {
                sb.append(' ');
                z3 = true;
            }
            i += Character.charCount(codePointAt);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m23509(String str) {
        if (str == null || str.length() == 0) {
            return true;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!m23497(str.codePointAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static boolean m23510(String str, String... strArr) {
        for (String equals : strArr) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }
}
