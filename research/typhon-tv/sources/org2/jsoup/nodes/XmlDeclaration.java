package org2.jsoup.nodes;

import java.io.IOException;
import java.util.Iterator;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Document;

public class XmlDeclaration extends LeafNode {

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f18312;

    public XmlDeclaration(String str, boolean z) {
        Validate.m23515((Object) str);
        this.f18305 = str;
        this.f18312 = z;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23791(Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        Iterator<Attribute> it2 = m23714().iterator();
        while (it2.hasNext()) {
            Attribute next = it2.next();
            if (!next.getKey().equals(m23798())) {
                appendable.append(' ');
                next.m23533(appendable, outputSettings);
            }
        }
    }

    public String toString() {
        return m23727();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23792(String str) {
        return super.m23717(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23793(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23794() {
        return super.m23718();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23795(String str) {
        return super.m23719(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m23796() {
        return super.m23720();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m23797(String str) {
        return super.m23721(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23798() {
        return "#declaration";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23799(String str) {
        return super.m23722(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23800(String str, String str2) {
        return super.m23723(str, str2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23801(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        appendable.append("<").append(this.f18312 ? "!" : "?").append(m23716());
        m23791(appendable, outputSettings);
        appendable.append(this.f18312 ? "!" : "?").append(">");
    }
}
