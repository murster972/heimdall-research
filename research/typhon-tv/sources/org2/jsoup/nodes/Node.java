package org2.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org2.jsoup.SerializationException;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Document;
import org2.jsoup.parser.Parser;
import org2.jsoup.select.Elements;
import org2.jsoup.select.NodeTraversor;
import org2.jsoup.select.NodeVisitor;

public abstract class Node implements Cloneable {

    /* renamed from: 靐  reason: contains not printable characters */
    Node f18306;

    /* renamed from: 齉  reason: contains not printable characters */
    int f18307;

    private static class OuterHtmlVisitor implements NodeVisitor {

        /* renamed from: 靐  reason: contains not printable characters */
        private Document.OutputSettings f18310;

        /* renamed from: 龘  reason: contains not printable characters */
        private Appendable f18311;

        OuterHtmlVisitor(Appendable appendable, Document.OutputSettings outputSettings) {
            this.f18311 = appendable;
            this.f18310 = outputSettings;
            outputSettings.m23597();
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m23776(Node node, int i) {
            if (!node.m23765().equals("#text")) {
                try {
                    node.m23758(this.f18311, i, this.f18310);
                } catch (IOException e) {
                    throw new SerializationException((Throwable) e);
                }
            }
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m23777(Node node, int i) {
            try {
                node.m23771(this.f18311, i, this.f18310);
            } catch (IOException e) {
                throw new SerializationException((Throwable) e);
            }
        }
    }

    protected Node() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Element m23724(Element element) {
        Elements r0 = element.m23643();
        return r0.size() > 0 ? m23724((Element) r0.get(0)) : element;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23725(int i) {
        List<Node> r1 = m23737();
        while (i < r1.size()) {
            r1.get(i).m23762(i);
            i++;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23726(int i, String str) {
        Validate.m23515((Object) str);
        Validate.m23515((Object) this.f18306);
        List<Node> r0 = Parser.m23965(str, m23735() instanceof Element ? (Element) m23735() : null, m23759());
        this.f18306.m23769(i, (Node[]) r0.toArray(new Node[r0.size()]));
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public String toString() {
        return m23727();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m23727() {
        StringBuilder sb = new StringBuilder(128);
        m23770((Appendable) sb);
        return sb.toString();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Node m23728(Node node) {
        Validate.m23515((Object) node);
        Validate.m23515((Object) this.f18306);
        this.f18306.m23769(this.f18307, node);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public abstract void m23729(String str);

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m23730(Node node) {
        Validate.m23515((Object) node);
        if (this.f18306 != null) {
            this.f18306.m23731(this);
        }
        this.f18306 = node;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m23731(Node node) {
        Validate.m23519(node.f18306 == this);
        int i = node.f18307;
        m23737().remove(i);
        m23725(i);
        node.f18306 = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public abstract boolean m23732();

    /* renamed from: ʾʾ  reason: contains not printable characters */
    public boolean m23733() {
        return this.f18306 != null;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public abstract Attributes m23734();

    /* renamed from: ʿʿ  reason: contains not printable characters */
    public Node m23735() {
        return this.f18306;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˆˆ  reason: contains not printable characters */
    public Node[] m23736() {
        return (Node[]) m23737().toArray(new Node[m23761()]);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public abstract List<Node> m23737();

    /* renamed from: ˈˈ  reason: contains not printable characters */
    public Node m23738() {
        while (this.f18306 != null) {
            this = this.f18306;
        }
        return this;
    }

    /* renamed from: ˉˉ  reason: contains not printable characters */
    public final Node m23739() {
        return this.f18306;
    }

    /* renamed from: ˊˊ  reason: contains not printable characters */
    public void m23740() {
        Validate.m23515((Object) this.f18306);
        this.f18306.m23731(this);
    }

    /* renamed from: ˋˋ  reason: contains not printable characters */
    public Document m23741() {
        Node r0 = m23738();
        if (r0 instanceof Document) {
            return (Document) r0;
        }
        return null;
    }

    /* renamed from: ˎˎ  reason: contains not printable characters */
    public List<Node> m23742() {
        if (this.f18306 == null) {
            return Collections.emptyList();
        }
        List<Node> r0 = this.f18306.m23737();
        ArrayList arrayList = new ArrayList(r0.size() - 1);
        for (Node next : r0) {
            if (next != this) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public Node m23743(String str) {
        Validate.m23517(str);
        List<Node> r3 = Parser.m23965(str, m23735() instanceof Element ? (Element) m23735() : null, m23759());
        Node node = r3.get(0);
        if (node == null || !(node instanceof Element)) {
            return null;
        }
        Element element = (Element) node;
        Element r1 = m23724(element);
        this.f18306.m23772(this, (Node) element);
        r1.m23773(this);
        if (r3.size() <= 0) {
            return this;
        }
        for (int i = 0; i < r3.size(); i++) {
            Node node2 = r3.get(i);
            node2.f18306.m23731(node2);
            element.m23679(node2);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m23744() {
    }

    /* renamed from: ˏˏ  reason: contains not printable characters */
    public Node m23745() {
        Validate.m23515((Object) this.f18306);
        List<Node> r0 = m23737();
        Node node = r0.size() > 0 ? r0.get(0) : null;
        this.f18306.m23769(this.f18307, m23736());
        m23740();
        return node;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m23746(Node node) {
        node.m23730(this);
    }

    /* renamed from: ˑˑ  reason: contains not printable characters */
    public Node m23747() {
        if (this.f18306 == null) {
            return null;
        }
        List<Node> r1 = this.f18306.m23737();
        int i = this.f18307 + 1;
        if (r1.size() > i) {
            return r1.get(i);
        }
        return null;
    }

    /* renamed from: י  reason: contains not printable characters */
    public Node m23748(String str) {
        m23726(this.f18307 + 1, str);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: יי  reason: contains not printable characters */
    public Document.OutputSettings m23749() {
        Document r0 = m23741();
        return r0 != null ? r0.m23585() : new Document("").m23585();
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public Node m23750(String str) {
        m23726(this.f18307, str);
        return this;
    }

    /* renamed from: ــ  reason: contains not printable characters */
    public List<Node> m23751() {
        return Collections.unmodifiableList(m23737());
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Node clone() {
        Node r3 = m23755((Node) null);
        LinkedList linkedList = new LinkedList();
        linkedList.add(r3);
        while (!linkedList.isEmpty()) {
            Node node = (Node) linkedList.remove();
            int r5 = node.m23761();
            for (int i = 0; i < r5; i++) {
                List<Node> r6 = node.m23737();
                Node r1 = r6.get(i).m23755(node);
                r6.set(i, r1);
                linkedList.add(r1);
            }
        }
        return r3;
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m23753(final String str) {
        Validate.m23515((Object) str);
        m23768((NodeVisitor) new NodeVisitor() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m23774(Node node, int i) {
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m23775(Node node, int i) {
                node.m23729(str);
            }
        });
    }

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    public int m23754() {
        return this.f18307;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public Node m23755(Node node) {
        try {
            Node node2 = (Node) super.clone();
            node2.f18306 = node;
            node2.f18307 = node == null ? 0 : this.f18307;
            return node2;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Node m23756(int i) {
        return m23737().get(i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Node m23757(String str) {
        Validate.m23515((Object) str);
        m23734().m23542(str);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract void m23758(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException;

    /* renamed from: 麤  reason: contains not printable characters */
    public abstract String m23759();

    /* renamed from: 麤  reason: contains not printable characters */
    public String m23760(String str) {
        Validate.m23515((Object) str);
        if (!m23732()) {
            return "";
        }
        String r0 = m23734().m23549(str);
        return r0.length() <= 0 ? str.startsWith("abs:") ? m23766(str.substring("abs:".length())) : "" : r0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public abstract int m23761();

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m23762(int i) {
        this.f18307 = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m23763(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        appendable.append(10).append(StringUtil.m23502(outputSettings.m23594() * i));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23764(String str) {
        Validate.m23515((Object) str);
        if (str.startsWith("abs:")) {
            String substring = str.substring("abs:".length());
            if (m23734().m23544(substring) && !m23766(substring).equals("")) {
                return true;
            }
        }
        return m23734().m23544(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract String m23765();

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23766(String str) {
        Validate.m23517(str);
        return !m23764(str) ? "" : StringUtil.m23503(m23759(), m23760(str));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Node m23767(String str, String str2) {
        m23734().m23548(str, str2);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Node m23768(NodeVisitor nodeVisitor) {
        Validate.m23515((Object) nodeVisitor);
        NodeTraversor.m24227(nodeVisitor, this);
        return this;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23769(int i, Node... nodeArr) {
        Validate.m23521((Object[]) nodeArr);
        List<Node> r1 = m23737();
        for (Node r3 : nodeArr) {
            m23746(r3);
        }
        r1.addAll(i, Arrays.asList(nodeArr));
        m23725(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23770(Appendable appendable) {
        NodeTraversor.m24227((NodeVisitor) new OuterHtmlVisitor(appendable, m23749()), this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m23771(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException;

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23772(Node node, Node node2) {
        Validate.m23519(node.f18306 == this);
        Validate.m23515((Object) node2);
        if (node2.f18306 != null) {
            node2.f18306.m23731(node2);
        }
        int i = node.f18307;
        m23737().set(i, node2);
        node2.f18306 = this;
        node2.m23762(i);
        node.f18306 = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23773(Node... nodeArr) {
        List<Node> r1 = m23737();
        for (Node node : nodeArr) {
            m23746(node);
            r1.add(node);
            node.m23762(r1.size() - 1);
        }
    }
}
