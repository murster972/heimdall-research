package org2.jsoup.nodes;

import java.io.IOException;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.nodes.Document;

public class TextNode extends LeafNode {
    public TextNode(String str) {
        this.f18305 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m23778(StringBuilder sb) {
        return sb.length() != 0 && sb.charAt(sb.length() + -1) == ' ';
    }

    public String toString() {
        return m23727();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m23779() {
        return StringUtil.m23509(m23716());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m23780() {
        return m23716();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23781(String str) {
        return super.m23717(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23782(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23783() {
        return super.m23718();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23784(String str) {
        return super.m23719(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m23785() {
        return super.m23720();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m23786(String str) {
        return super.m23721(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23787() {
        return "#text";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23788(String str) {
        return super.m23722(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23789(String str, String str2) {
        return super.m23723(str, str2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23790(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (outputSettings.m23598() && ((m23754() == 0 && (this.f18306 instanceof Element) && ((Element) this.f18306).m23686().m23978() && !m23779()) || (outputSettings.m23596() && m23742().size() > 0 && !m23779()))) {
            m23763(appendable, i, outputSettings);
        }
        Entities.m23694(appendable, m23716(), outputSettings, false, outputSettings.m23598() && (m23735() instanceof Element) && !Element.m23619(m23735()), false);
    }
}
