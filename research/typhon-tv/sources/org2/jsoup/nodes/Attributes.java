package org2.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org2.jsoup.SerializationException;
import org2.jsoup.helper.Validate;
import org2.jsoup.internal.Normalizer;
import org2.jsoup.nodes.Document;

public class Attributes implements Cloneable, Iterable<Attribute> {

    /* renamed from: 齉  reason: contains not printable characters */
    private static final String[] f18266 = new String[0];

    /* renamed from: 靐  reason: contains not printable characters */
    String[] f18267 = f18266;
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public int f18268 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    String[] f18269 = f18266;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m23534(String str) {
        Validate.m23515((Object) str);
        for (int i = 0; i < this.f18268; i++) {
            if (str.equalsIgnoreCase(this.f18269[i])) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    static String m23536(String str) {
        return str == null ? "" : str;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23537(int i) {
        Validate.m23513(i >= this.f18268);
        int i2 = (this.f18268 - i) - 1;
        if (i2 > 0) {
            System.arraycopy(this.f18269, i + 1, this.f18269, i, i2);
            System.arraycopy(this.f18267, i + 1, this.f18267, i, i2);
        }
        this.f18268--;
        this.f18269[this.f18268] = null;
        this.f18267[this.f18268] = null;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m23538(String str, String str2) {
        m23539(this.f18268 + 1);
        this.f18269[this.f18268] = str;
        this.f18267[this.f18268] = str2;
        this.f18268++;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23539(int i) {
        Validate.m23519(i >= this.f18268);
        int length = this.f18269.length;
        if (length < i) {
            int i2 = length >= 4 ? this.f18268 * 2 : 4;
            if (i <= i2) {
                i = i2;
            }
            this.f18269 = m23541(this.f18269, i);
            this.f18267 = m23541(this.f18267, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String[] m23541(String[] strArr, int i) {
        String[] strArr2 = new String[i];
        System.arraycopy(strArr, 0, strArr2, 0, Math.min(strArr.length, i));
        return strArr2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Attributes attributes = (Attributes) obj;
        if (this.f18268 != attributes.f18268 || !Arrays.equals(this.f18269, attributes.f18269)) {
            return false;
        }
        return Arrays.equals(this.f18267, attributes.f18267);
    }

    public int hashCode() {
        return (((this.f18268 * 31) + Arrays.hashCode(this.f18269)) * 31) + Arrays.hashCode(this.f18267);
    }

    public Iterator<Attribute> iterator() {
        return new Iterator<Attribute>() {

            /* renamed from: 龘  reason: contains not printable characters */
            int f18271 = 0;

            public boolean hasNext() {
                return this.f18271 < Attributes.this.f18268;
            }

            public void remove() {
                Attributes attributes = Attributes.this;
                int i = this.f18271 - 1;
                this.f18271 = i;
                attributes.m23537(i);
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public Attribute next() {
                Attribute attribute = new Attribute(Attributes.this.f18269[this.f18271], Attributes.this.f18267[this.f18271], Attributes.this);
                this.f18271++;
                return attribute;
            }
        };
    }

    public String toString() {
        return m23551();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m23542(String str) {
        int r0 = m23534(str);
        if (r0 != -1) {
            m23537(r0);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m23543(String str) {
        return m23554(str) != -1;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m23544(String str) {
        return m23534(str) != -1;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m23545() {
        for (int i = 0; i < this.f18268; i++) {
            this.f18269[i] = Normalizer.m23524(this.f18269[i]);
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m23546(String str) {
        int r0 = m23554(str);
        if (r0 != -1) {
            m23537(r0);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<Attribute> m23547() {
        ArrayList arrayList = new ArrayList(this.f18268);
        for (int i = 0; i < this.f18268; i++) {
            arrayList.add(this.f18267[i] == null ? new BooleanAttribute(this.f18269[i]) : new Attribute(this.f18269[i], this.f18267[i], this));
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23548(String str, String str2) {
        int r0 = m23534(str);
        if (r0 != -1) {
            this.f18267[r0] = str2;
            if (!this.f18269[r0].equals(str)) {
                this.f18269[r0] = str;
                return;
            }
            return;
        }
        m23538(str, str2);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m23549(String str) {
        int r0 = m23534(str);
        return r0 == -1 ? "" : m23536(this.f18267[r0]);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Attributes clone() {
        try {
            Attributes attributes = (Attributes) super.clone();
            attributes.f18268 = this.f18268;
            this.f18269 = m23541(this.f18269, this.f18268);
            this.f18267 = m23541(this.f18267, this.f18268);
            return attributes;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m23551() {
        StringBuilder sb = new StringBuilder();
        try {
            m23557((Appendable) sb, new Document("").m23585());
            return sb.toString();
        } catch (IOException e) {
            throw new SerializationException((Throwable) e);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m23552(String str) {
        int r0 = m23554(str);
        return r0 == -1 ? "" : m23536(this.f18267[r0]);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23553() {
        return this.f18268;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m23554(String str) {
        Validate.m23515((Object) str);
        for (int i = 0; i < this.f18268; i++) {
            if (str.equals(this.f18269[i])) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Attributes m23555(String str, String str2) {
        int r0 = m23554(str);
        if (r0 != -1) {
            this.f18267[r0] = str2;
        } else {
            m23538(str, str2);
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Attributes m23556(Attribute attribute) {
        Validate.m23515((Object) attribute);
        m23555(attribute.getKey(), attribute.getValue());
        attribute.f18265 = this;
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public final void m23557(Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        int i = this.f18268;
        for (int i2 = 0; i2 < i; i2++) {
            String str = this.f18269[i2];
            String str2 = this.f18267[i2];
            appendable.append(' ').append(str);
            if (outputSettings.m23599() != Document.OutputSettings.Syntax.html || (str2 != null && (!str2.equals(str) || !Attribute.m23525(str)))) {
                appendable.append("=\"");
                if (str2 == null) {
                    str2 = "";
                }
                Entities.m23694(appendable, str2, outputSettings, true, false, false);
                appendable.append('\"');
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m23558(Attributes attributes) {
        if (attributes.m23553() != 0) {
            m23539(this.f18268 + attributes.f18268);
            Iterator<Attribute> it2 = attributes.iterator();
            while (it2.hasNext()) {
                m23556(it2.next());
            }
        }
    }
}
