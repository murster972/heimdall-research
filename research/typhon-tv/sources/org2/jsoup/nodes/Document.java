package org2.jsoup.nodes;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import net.lingala.zip4j.util.InternalZipTyphoonApp;
import net.pubnative.library.request.PubnativeAsset;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.nodes.Entities;
import org2.jsoup.parser.ParseSettings;
import org2.jsoup.parser.Tag;

public class Document extends Element {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f18272;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f18273 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private QuirksMode f18274 = QuirksMode.noQuirks;

    /* renamed from: 麤  reason: contains not printable characters */
    private OutputSettings f18275 = new OutputSettings();

    public static class OutputSettings implements Cloneable {

        /* renamed from: ʻ  reason: contains not printable characters */
        private boolean f18276 = false;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int f18277 = 1;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Syntax f18278 = Syntax.html;

        /* renamed from: 连任  reason: contains not printable characters */
        private boolean f18279 = true;

        /* renamed from: 靐  reason: contains not printable characters */
        Entities.CoreCharset f18280;

        /* renamed from: 麤  reason: contains not printable characters */
        private Charset f18281;

        /* renamed from: 齉  reason: contains not printable characters */
        private Entities.EscapeMode f18282 = Entities.EscapeMode.base;

        /* renamed from: 龘  reason: contains not printable characters */
        CharsetEncoder f18283;

        public enum Syntax {
            html,
            xml
        }

        public OutputSettings() {
            m23601(Charset.forName(InternalZipTyphoonApp.CHARSET_UTF8));
        }

        /* renamed from: ʻ  reason: contains not printable characters */
        public int m23594() {
            return this.f18277;
        }

        /* renamed from: ʼ  reason: contains not printable characters */
        public OutputSettings clone() {
            try {
                OutputSettings outputSettings = (OutputSettings) super.clone();
                outputSettings.m23600(this.f18281.name());
                outputSettings.f18282 = Entities.EscapeMode.valueOf(this.f18282.name());
                return outputSettings;
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        }

        /* renamed from: 连任  reason: contains not printable characters */
        public boolean m23596() {
            return this.f18276;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public CharsetEncoder m23597() {
            this.f18283 = this.f18281.newEncoder();
            this.f18280 = Entities.CoreCharset.m23699(this.f18283.charset().name());
            return this.f18283;
        }

        /* renamed from: 麤  reason: contains not printable characters */
        public boolean m23598() {
            return this.f18279;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Syntax m23599() {
            return this.f18278;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputSettings m23600(String str) {
            m23601(Charset.forName(str));
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputSettings m23601(Charset charset) {
            this.f18281 = charset;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public OutputSettings m23602(Syntax syntax) {
            this.f18278 = syntax;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Entities.EscapeMode m23603() {
            return this.f18282;
        }
    }

    public enum QuirksMode {
        noQuirks,
        quirks,
        limitedQuirks
    }

    public Document(String str) {
        super(Tag.m23969("#root", ParseSettings.f18388), str);
        this.f18272 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Element m23582(String str, Node node) {
        if (node.m23765().equals(str)) {
            return (Element) node;
        }
        int r2 = node.m23761();
        for (int i = 0; i < r2; i++) {
            Element r0 = m23582(str, node.m23756(i));
            if (r0 != null) {
                return r0;
            }
        }
        return null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m23583() {
        return super.m23630();
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Document m23588() {
        Document document = (Document) super.m23660();
        document.f18275 = this.f18275.clone();
        return document;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public OutputSettings m23585() {
        return this.f18275;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public QuirksMode m23586() {
        return this.f18274;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m23589() {
        Element first = m23685(PubnativeAsset.TITLE).first();
        return first != null ? StringUtil.m23500(first.m23667()).trim() : "";
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Element m23590(String str) {
        m23591().m23668(str);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Element m23591() {
        return m23582(TtmlNode.TAG_BODY, this);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23592() {
        return "#document";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Document m23593(QuirksMode quirksMode) {
        this.f18274 = quirksMode;
        return this;
    }
}
