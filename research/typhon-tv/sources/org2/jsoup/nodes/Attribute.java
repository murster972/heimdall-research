package org2.jsoup.nodes;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import org2.jsoup.SerializationException;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Document;

public class Attribute implements Cloneable, Map.Entry<String, String> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f18262 = {"allowfullscreen", "async", "autofocus", "checked", "compact", "declare", "default", "defer", "disabled", "formnovalidate", "hidden", "inert", "ismap", "itemscope", "multiple", "muted", "nohref", "noresize", "noshade", "novalidate", "nowrap", "open", "readonly", "required", "reversed", "seamless", "selected", "sortable", "truespeed", "typemustmatch"};

    /* renamed from: 麤  reason: contains not printable characters */
    private String f18263;

    /* renamed from: 齉  reason: contains not printable characters */
    private String f18264;

    /* renamed from: 龘  reason: contains not printable characters */
    Attributes f18265;

    public Attribute(String str, String str2) {
        this(str, str2, (Attributes) null);
    }

    public Attribute(String str, String str2, Attributes attributes) {
        Validate.m23515((Object) str);
        this.f18264 = str.trim();
        Validate.m23517(str);
        this.f18263 = str2;
        this.f18265 = attributes;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    protected static boolean m23525(String str) {
        return Arrays.binarySearch(f18262, str) >= 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static void m23526(String str, String str2, Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        appendable.append(str);
        if (!m23527(str, str2, outputSettings)) {
            appendable.append("=\"");
            Entities.m23694(appendable, Attributes.m23536(str2), outputSettings, true, false, false);
            appendable.append('\"');
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    protected static boolean m23527(String str, String str2, Document.OutputSettings outputSettings) {
        return (str2 == null || "".equals(str2) || str2.equalsIgnoreCase(str)) && outputSettings.m23599() == Document.OutputSettings.Syntax.html && m23525(str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Attribute attribute = (Attribute) obj;
        if (this.f18264 == null ? attribute.f18264 != null : !this.f18264.equals(attribute.f18264)) {
            return false;
        }
        return this.f18263 != null ? this.f18263.equals(attribute.f18263) : attribute.f18263 == null;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.f18264 != null ? this.f18264.hashCode() : 0) * 31;
        if (this.f18263 != null) {
            i = this.f18263.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return m23530();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String getValue() {
        return this.f18263;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public Attribute clone() {
        try {
            return (Attribute) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public String m23530() {
        StringBuilder sb = new StringBuilder();
        try {
            m23533(sb, new Document("").m23585());
            return sb.toString();
        } catch (IOException e) {
            throw new SerializationException((Throwable) e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String getKey() {
        return this.f18264;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String setValue(String str) {
        int r1;
        String r0 = this.f18265.m23552(this.f18264);
        if (!(this.f18265 == null || (r1 = this.f18265.m23554(this.f18264)) == -1)) {
            this.f18265.f18267[r1] = str;
        }
        this.f18263 = str;
        return r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23533(Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        m23526(this.f18264, this.f18263, appendable, outputSettings);
    }
}
