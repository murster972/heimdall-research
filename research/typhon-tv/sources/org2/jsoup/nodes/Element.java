package org2.jsoup.nodes;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org2.jsoup.helper.ChangeNotifyingArrayList;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.helper.Validate;
import org2.jsoup.internal.Normalizer;
import org2.jsoup.nodes.Document;
import org2.jsoup.parser.ParseSettings;
import org2.jsoup.parser.Parser;
import org2.jsoup.parser.Tag;
import org2.jsoup.select.Collector;
import org2.jsoup.select.Elements;
import org2.jsoup.select.Evaluator;
import org2.jsoup.select.NodeTraversor;
import org2.jsoup.select.NodeVisitor;
import org2.jsoup.select.Selector;

public class Element extends Node {

    /* renamed from: 连任  reason: contains not printable characters */
    private static final Pattern f18286 = Pattern.compile("\\s+");

    /* renamed from: 麤  reason: contains not printable characters */
    private static final List<Node> f18287 = Collections.emptyList();
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public Tag f18288;

    /* renamed from: ʼ  reason: contains not printable characters */
    private WeakReference<List<Element>> f18289;

    /* renamed from: ʽ  reason: contains not printable characters */
    private Attributes f18290;

    /* renamed from: ˑ  reason: contains not printable characters */
    private String f18291;

    /* renamed from: 龘  reason: contains not printable characters */
    List<Node> f18292;

    private static final class NodeList extends ChangeNotifyingArrayList<Node> {
        private final Element owner;

        NodeList(Element element, int i) {
            super(i);
            this.owner = element;
        }

        public void onContentsChanged() {
            this.owner.m23651();
        }
    }

    public Element(Tag tag, String str) {
        this(tag, str, (Attributes) null);
    }

    public Element(Tag tag, String str, Attributes attributes) {
        Validate.m23515((Object) tag);
        Validate.m23515((Object) str);
        this.f18292 = f18287;
        this.f18291 = str;
        this.f18290 = attributes;
        this.f18288 = tag;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private List<Element> m23616() {
        List<Element> list;
        if (this.f18289 != null && (list = (List) this.f18289.get()) != null) {
            return list;
        }
        int size = this.f18292.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            Node node = this.f18292.get(i);
            if (node instanceof Element) {
                arrayList.add((Element) node);
            }
        }
        this.f18289 = new WeakReference<>(arrayList);
        return arrayList;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m23617(StringBuilder sb) {
        for (Node r0 : this.f18292) {
            r0.m23770((Appendable) sb);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m23618(StringBuilder sb, TextNode textNode) {
        String r0 = textNode.m23780();
        if (m23619(textNode.f18306)) {
            sb.append(r0);
        } else {
            StringUtil.m23508(sb, r0, TextNode.m23778(sb));
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    static boolean m23619(Node node) {
        if (node == null || !(node instanceof Element)) {
            return false;
        }
        Element element = (Element) node;
        return element.f18288.m23972() || (element.m23637() != null && element.m23637().f18288.m23972());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static <E extends Element> int m23620(Element element, List<E> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == element) {
                return i;
            }
        }
        return 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23622(StringBuilder sb) {
        for (Node next : this.f18292) {
            if (next instanceof TextNode) {
                m23618(sb, (TextNode) next);
            } else if (next instanceof Element) {
                m23624((Element) next, sb);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23624(Element element, StringBuilder sb) {
        if (element.f18288.m23979().equals(TtmlNode.TAG_BR) && !TextNode.m23778(sb)) {
            sb.append(StringUtils.SPACE);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23625(Element element, Elements elements) {
        Element r0 = element.m23637();
        if (r0 != null && !r0.m23684().equals("#root")) {
            elements.add(r0);
            m23625(r0, elements);
        }
    }

    public String toString() {
        return m23727();
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m23627(String str) {
        this.f18291 = str;
    }

    /* renamed from: ʻʻ  reason: contains not printable characters */
    public Set<String> m23628() {
        LinkedHashSet linkedHashSet = new LinkedHashSet(Arrays.asList(f18286.split(m23663())));
        linkedHashSet.remove("");
        return linkedHashSet;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Element m23629(String str) {
        Validate.m23518(str, "Tag name must not be empty.");
        this.f18288 = Tag.m23969(str, ParseSettings.f18387);
        return this;
    }

    /* renamed from: ʼʼ  reason: contains not printable characters */
    public String m23630() {
        StringBuilder r0 = StringUtil.m23506();
        m23617(r0);
        return m23749().m23598() ? r0.toString().trim() : r0.toString();
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Elements m23631(String str) {
        return Selector.m24253(str, this);
    }

    /* renamed from: ʽʽ  reason: contains not printable characters */
    public String m23632() {
        return m23684().equals("textarea") ? m23667() : m23760("value");
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public Element m23654(String str) {
        return (Element) super.m23748(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m23634() {
        return this.f18290 != null;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Attributes m23635() {
        if (!m23634()) {
            this.f18290 = new Attributes();
        }
        return this.f18290;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public Element m23650(String str) {
        return (Element) super.m23743(str);
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Element m23638(String str) {
        if (m23684().equals("textarea")) {
            m23668(str);
        } else {
            m23680("value", str);
        }
        return this;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public Elements m23639() {
        Elements elements = new Elements();
        m23625(this, elements);
        return elements;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public List<Node> m23640() {
        if (this.f18292 == f18287) {
            this.f18292 = new NodeList(this, 4);
        }
        return this.f18292;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public Element m23655(String str) {
        return (Element) super.m23750(str);
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Element m23642(String str) {
        m23653();
        m23658(str);
        return this;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public Elements m23643() {
        return new Elements(m23616());
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public Element m23644(String str) {
        Validate.m23515((Object) str);
        Set<String> r0 = m23628();
        r0.add(str);
        m23678(r0);
        return this;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m23645() {
        return this.f18288.m23976();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public String m23646() {
        return m23635().m23549("id");
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public Element m23647(String str) {
        Validate.m23515((Object) str);
        Set<String> r0 = m23628();
        r0.remove(str);
        m23678(r0);
        return this;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final Element m23637() {
        return (Element) this.f18306;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public Element m23649(String str) {
        Validate.m23515((Object) str);
        Set<String> r0 = m23628();
        if (r0.contains(str)) {
            r0.remove(str);
        } else {
            r0.add(str);
        }
        m23678(r0);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m23651() {
        super.m23744();
        this.f18289 = null;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public Element m23652(String str) {
        return Selector.m24251(str, this);
    }

    /* renamed from: י  reason: contains not printable characters */
    public Element m23653() {
        this.f18292.clear();
        return this;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public Elements m23656() {
        if (this.f18306 == null) {
            return new Elements(0);
        }
        List<Element> r0 = m23637().m23616();
        Elements elements = new Elements(r0.size() - 1);
        for (Element next : r0) {
            if (next != this) {
                elements.add(next);
            }
        }
        return elements;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Element m23660() {
        return (Element) super.clone();
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public Element m23658(String str) {
        Validate.m23515((Object) str);
        List<Node> r0 = Parser.m23965(str, this, m23673());
        m23773((Node[]) r0.toArray(new Node[r0.size()]));
        return this;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public Element m23659(String str) {
        Validate.m23515((Object) str);
        List<Node> r0 = Parser.m23965(str, this, m23673());
        m23769(0, (Node[]) r0.toArray(new Node[r0.size()]));
        return this;
    }

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    public String m23661() {
        StringBuilder sb = new StringBuilder();
        for (Node next : this.f18292) {
            if (next instanceof DataNode) {
                sb.append(((DataNode) next).m23571());
            } else if (next instanceof Comment) {
                sb.append(((Comment) next).m23560());
            } else if (next instanceof Element) {
                sb.append(((Element) next).m23661());
            }
        }
        return sb.toString();
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public Element m23662() {
        if (this.f18306 == null) {
            return null;
        }
        List<Element> r1 = m23637().m23616();
        Integer valueOf = Integer.valueOf(m23620(this, r1));
        Validate.m23515((Object) valueOf);
        if (r1.size() > valueOf.intValue() + 1) {
            return r1.get(valueOf.intValue() + 1);
        }
        return null;
    }

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    public String m23663() {
        return m23760("class").trim();
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public Element m23664() {
        if (this.f18306 == null) {
            return null;
        }
        List<Element> r1 = m23637().m23616();
        Integer valueOf = Integer.valueOf(m23620(this, r1));
        Validate.m23515((Object) valueOf);
        if (valueOf.intValue() > 0) {
            return r1.get(valueOf.intValue() - 1);
        }
        return null;
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public int m23665() {
        if (m23637() == null) {
            return 0;
        }
        return m23620(this, m23637().m23616());
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public Elements m23666() {
        return Collector.m24171(new Evaluator.AllElements(), this);
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public String m23667() {
        final StringBuilder sb = new StringBuilder();
        NodeTraversor.m24227((NodeVisitor) new NodeVisitor() {
            /* renamed from: 靐  reason: contains not printable characters */
            public void m23689(Node node, int i) {
            }

            /* renamed from: 龘  reason: contains not printable characters */
            public void m23690(Node node, int i) {
                if (node instanceof TextNode) {
                    Element.m23618(sb, (TextNode) node);
                } else if (node instanceof Element) {
                    Element element = (Element) node;
                    if (sb.length() <= 0) {
                        return;
                    }
                    if ((element.m23645() || element.f18288.m23979().equals(TtmlNode.TAG_BR)) && !TextNode.m23778(sb)) {
                        sb.append(' ');
                    }
                }
            }
        }, (Node) this);
        return sb.toString().trim();
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public Element m23668(String str) {
        Validate.m23515((Object) str);
        m23653();
        m23679((Node) new TextNode(str));
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Element m23680(String str, String str2) {
        super.m23767(str, str2);
        return this;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Element m23626(Node node) {
        return (Element) super.m23728(node);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23672(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (!this.f18292.isEmpty() || !this.f18288.m23975()) {
            if (outputSettings.m23598() && !this.f18292.isEmpty() && (this.f18288.m23978() || (outputSettings.m23596() && (this.f18292.size() > 1 || (this.f18292.size() == 1 && !(this.f18292.get(0) instanceof TextNode)))))) {
                m23763(appendable, i, outputSettings);
            }
            appendable.append("</").append(m23684()).append('>');
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m23673() {
        return this.f18291;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public Element m23669(Node node) {
        Element element = (Element) super.m23755(node);
        element.f18290 = this.f18290 != null ? this.f18290.clone() : null;
        element.f18291 = this.f18291;
        element.f18292 = new NodeList(element, this.f18292.size());
        element.f18292.addAll(this.f18292);
        return element;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m23675() {
        return this.f18292.size();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23676() {
        return this.f18288.m23979();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Element m23677(int i) {
        return m23616().get(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Element m23678(Set<String> set) {
        Validate.m23515((Object) set);
        if (set.isEmpty()) {
            m23635().m23546("class");
        } else {
            m23635().m23555("class", StringUtil.m23504((Collection) set, StringUtils.SPACE));
        }
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Element m23679(Node node) {
        Validate.m23515((Object) node);
        m23746(node);
        m23640();
        this.f18292.add(node);
        node.m23762(this.f18292.size() - 1);
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23681(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (outputSettings.m23598() && (this.f18288.m23978() || ((m23637() != null && m23637().m23686().m23978()) || outputSettings.m23596()))) {
            if (!(appendable instanceof StringBuilder)) {
                m23763(appendable, i, outputSettings);
            } else if (((StringBuilder) appendable).length() > 0) {
                m23763(appendable, i, outputSettings);
            }
        }
        appendable.append('<').append(m23684());
        if (this.f18290 != null) {
            this.f18290.m23557(appendable, outputSettings);
        }
        if (!this.f18292.isEmpty() || !this.f18288.m23975()) {
            appendable.append('>');
        } else if (outputSettings.m23599() != Document.OutputSettings.Syntax.html || !this.f18288.m23977()) {
            appendable.append(" />");
        } else {
            appendable.append('>');
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m23682(Evaluator evaluator) {
        return evaluator.m24184((Element) m23738(), this);
    }

    /* renamed from: ﹳ  reason: contains not printable characters */
    public String m23683() {
        StringBuilder sb = new StringBuilder();
        m23622(sb);
        return sb.toString().trim();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public String m23684() {
        return this.f18288.m23979();
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public Elements m23685(String str) {
        Validate.m23517(str);
        return Collector.m24171(new Evaluator.Tag(Normalizer.m23523(str)), this);
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public Tag m23686() {
        return this.f18288;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m23687(String str) {
        String r0 = m23635().m23549("class");
        int length = r0.length();
        int length2 = str.length();
        if (length == 0 || length < length2) {
            return false;
        }
        if (length == length2) {
            return str.equalsIgnoreCase(r0);
        }
        int i = 0;
        boolean z = false;
        for (int i2 = 0; i2 < length; i2++) {
            if (Character.isWhitespace(r0.charAt(i2))) {
                if (!z) {
                    continue;
                } else if (i2 - i == length2 && r0.regionMatches(true, i, str, 0, length2)) {
                    return true;
                } else {
                    z = false;
                }
            } else if (!z) {
                i = i2;
                z = true;
            }
        }
        if (!z || length - i != length2) {
            return false;
        }
        return r0.regionMatches(true, i, str, 0, length2);
    }

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    public boolean m23688() {
        for (Node next : this.f18292) {
            if (next instanceof TextNode) {
                if (!((TextNode) next).m23779()) {
                    return true;
                }
            } else if ((next instanceof Element) && ((Element) next).m23688()) {
                return true;
            }
        }
        return false;
    }
}
