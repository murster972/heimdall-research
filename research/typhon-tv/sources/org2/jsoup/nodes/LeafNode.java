package org2.jsoup.nodes;

import java.util.List;
import org2.jsoup.helper.Validate;

abstract class LeafNode extends Node {

    /* renamed from: 龘  reason: contains not printable characters */
    Object f18305;

    LeafNode() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m23711() {
        if (!m23713()) {
            Object obj = this.f18305;
            Attributes attributes = new Attributes();
            this.f18305 = attributes;
            if (obj != null) {
                attributes.m23555(m23765(), (String) obj);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m23712(String str) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public final boolean m23713() {
        return this.f18305 instanceof Attributes;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public final Attributes m23714() {
        m23711();
        return (Attributes) this.f18305;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public List<Node> m23715() {
        throw new UnsupportedOperationException("Leaf Nodes do not have child nodes.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public String m23716() {
        return m23719(m23765());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public Node m23717(String str) {
        m23711();
        return super.m23757(str);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m23718() {
        return m23733() ? m23735().m23759() : "";
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m23719(String str) {
        Validate.m23515((Object) str);
        return !m23713() ? str.equals(m23765()) ? (String) this.f18305 : "" : super.m23760(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m23720() {
        return 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23721(String str) {
        m23711();
        return super.m23764(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23722(String str) {
        m23711();
        return super.m23766(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Node m23723(String str, String str2) {
        if (m23713() || !str.equals(m23765())) {
            m23711();
            super.m23767(str, str2);
        } else {
            this.f18305 = str2;
        }
        return this;
    }
}
