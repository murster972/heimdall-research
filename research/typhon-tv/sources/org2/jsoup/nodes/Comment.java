package org2.jsoup.nodes;

import java.io.IOException;
import org2.jsoup.nodes.Document;

public class Comment extends LeafNode {
    public Comment(String str) {
        this.f18305 = str;
    }

    public String toString() {
        return m23727();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m23560() {
        return m23716();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23561(String str) {
        return super.m23717(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23562(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23563() {
        return super.m23718();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23564(String str) {
        return super.m23719(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m23565() {
        return super.m23720();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m23566(String str) {
        return super.m23721(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23567() {
        return "#comment";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23568(String str) {
        return super.m23722(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23569(String str, String str2) {
        return super.m23723(str, str2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23570(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (outputSettings.m23598()) {
            m23763(appendable, i, outputSettings);
        }
        appendable.append("<!--").append(m23560()).append("-->");
    }
}
