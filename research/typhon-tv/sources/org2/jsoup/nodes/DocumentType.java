package org2.jsoup.nodes;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Document;

public class DocumentType extends LeafNode {
    public DocumentType(String str, String str2, String str3) {
        Validate.m23515((Object) str);
        Validate.m23515((Object) str2);
        Validate.m23515((Object) str3);
        m23614("name", str);
        m23614("publicId", str2);
        if (m23604("publicId")) {
            m23614("pubSysKey", "PUBLIC");
        }
        m23614("systemId", str3);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean m23604(String str) {
        return !StringUtil.m23509(m23609(str));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m23605(String str) {
        if (str != null) {
            m23614("pubSysKey", str);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23606(String str) {
        return super.m23717(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23607(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23608() {
        return super.m23718();
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23609(String str) {
        return super.m23719(str);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ int m23610() {
        return super.m23720();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m23611(String str) {
        return super.m23721(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23612() {
        return "#doctype";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ String m23613(String str) {
        return super.m23722(str);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ Node m23614(String str, String str2) {
        return super.m23723(str, str2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23615(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (outputSettings.m23599() != Document.OutputSettings.Syntax.html || m23604("publicId") || m23604("systemId")) {
            appendable.append("<!DOCTYPE");
        } else {
            appendable.append("<!doctype");
        }
        if (m23604("name")) {
            appendable.append(StringUtils.SPACE).append(m23609("name"));
        }
        if (m23604("pubSysKey")) {
            appendable.append(StringUtils.SPACE).append(m23609("pubSysKey"));
        }
        if (m23604("publicId")) {
            appendable.append(" \"").append(m23609("publicId")).append('\"');
        }
        if (m23604("systemId")) {
            appendable.append(" \"").append(m23609("systemId")).append('\"');
        }
        appendable.append('>');
    }
}
