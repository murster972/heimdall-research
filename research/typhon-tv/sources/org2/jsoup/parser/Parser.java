package org2.jsoup.parser;

import java.io.StringReader;
import java.util.List;
import org2.jsoup.nodes.Document;
import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.Node;

public class Parser {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f18391 = 0;

    /* renamed from: 麤  reason: contains not printable characters */
    private ParseSettings f18392;

    /* renamed from: 齉  reason: contains not printable characters */
    private ParseErrorList f18393;

    /* renamed from: 龘  reason: contains not printable characters */
    private TreeBuilder f18394;

    public Parser(TreeBuilder treeBuilder) {
        this.f18394 = treeBuilder;
        this.f18392 = treeBuilder.m24156();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Document m23962(String str, String str2) {
        HtmlTreeBuilder htmlTreeBuilder = new HtmlTreeBuilder();
        return htmlTreeBuilder.m24155(new StringReader(str), str2, ParseErrorList.noTracking(), htmlTreeBuilder.m24156());
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Parser m23963() {
        return new Parser(new HtmlTreeBuilder());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static Parser m23964() {
        return new Parser(new XmlTreeBuilder());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static List<Node> m23965(String str, Element element, String str2) {
        HtmlTreeBuilder htmlTreeBuilder = new HtmlTreeBuilder();
        return htmlTreeBuilder.m23891(str, element, str2, ParseErrorList.noTracking(), htmlTreeBuilder.m23895());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Document m23966(String str, String str2) {
        this.f18393 = m23968() ? ParseErrorList.tracking(this.f18391) : ParseErrorList.noTracking();
        return this.f18394.m24155(new StringReader(str), str2, this.f18393, this.f18392);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Parser m23967(ParseSettings parseSettings) {
        this.f18392 = parseSettings;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m23968() {
        return this.f18391 > 0;
    }
}
