package org2.jsoup.parser;

import com.mopub.common.AdType;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.pubnative.library.request.PubnativeAsset;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Attributes;
import org2.jsoup.nodes.Comment;
import org2.jsoup.nodes.DataNode;
import org2.jsoup.nodes.Document;
import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.FormElement;
import org2.jsoup.nodes.Node;
import org2.jsoup.nodes.TextNode;
import org2.jsoup.parser.Token;
import org2.jsoup.select.Elements;

public class HtmlTreeBuilder extends TreeBuilder {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final String[] f18321 = {"dd", "dt", "li", "optgroup", "option", TtmlNode.TAG_P, "rp", "rt"};

    /* renamed from: ʼ  reason: contains not printable characters */
    static final String[] f18322 = {"address", "applet", "area", "article", "aside", "base", "basefont", "bgsound", "blockquote", TtmlNode.TAG_BODY, TtmlNode.TAG_BR, "button", "caption", TtmlNode.CENTER, "col", "colgroup", "command", "dd", "details", "dir", TtmlNode.TAG_DIV, "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", TtmlNode.TAG_HEAD, "header", "hgroup", "hr", AdType.HTML, "iframe", "img", "input", "isindex", "li", "link", "listing", "marquee", "menu", "meta", "nav", "noembed", "noframes", "noscript", "object", "ol", TtmlNode.TAG_P, "param", "plaintext", "pre", "script", "section", "select", TtmlNode.TAG_STYLE, "summary", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", PubnativeAsset.TITLE, "tr", "ul", "wbr", "xmp"};

    /* renamed from: ʽ  reason: contains not printable characters */
    static final /* synthetic */ boolean f18323 = (!HtmlTreeBuilder.class.desiredAssertionStatus());

    /* renamed from: 连任  reason: contains not printable characters */
    static final String[] f18324 = {"optgroup", "option"};

    /* renamed from: 靐  reason: contains not printable characters */
    static final String[] f18325 = {"ol", "ul"};

    /* renamed from: 麤  reason: contains not printable characters */
    static final String[] f18326 = {AdType.HTML, "table"};

    /* renamed from: 齉  reason: contains not printable characters */
    static final String[] f18327 = {"button"};

    /* renamed from: 龘  reason: contains not printable characters */
    static final String[] f18328 = {"applet", "caption", AdType.HTML, "marquee", "object", "table", "td", "th"};

    /* renamed from: ˆ  reason: contains not printable characters */
    private Element f18329;

    /* renamed from: ˉ  reason: contains not printable characters */
    private FormElement f18330;

    /* renamed from: ˊ  reason: contains not printable characters */
    private HtmlTreeBuilderState f18331;

    /* renamed from: ˋ  reason: contains not printable characters */
    private HtmlTreeBuilderState f18332;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f18333;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Element f18334;

    /* renamed from: י  reason: contains not printable characters */
    private ArrayList<Element> f18335;

    /* renamed from: ـ  reason: contains not printable characters */
    private List<String> f18336;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private Token.EndTag f18337;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private boolean f18338;

    /* renamed from: ᵔ  reason: contains not printable characters */
    private boolean f18339;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private boolean f18340;

    /* renamed from: ⁱ  reason: contains not printable characters */
    private String[] f18341 = {null};

    HtmlTreeBuilder() {
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m23836(Node node) {
        if (this.f18532.size() == 0) {
            this.f18535.m23679(node);
        } else if (m23911()) {
            m23900(node);
        } else {
            m24154().m23679(node);
        }
        if ((node instanceof Element) && ((Element) node).m23686().m23973() && this.f18330 != null) {
            this.f18330.m23710((Element) node);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean m23837(Element element, Element element2) {
        return element.m23676().equals(element2.m23676()) && element.m23635().equals(element2.m23635());
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m23838(String... strArr) {
        int size = this.f18532.size() - 1;
        while (size >= 0) {
            Element element = (Element) this.f18532.get(size);
            if (!StringUtil.m23510(element.m23676(), strArr) && !element.m23676().equals(AdType.HTML)) {
                this.f18532.remove(size);
                size--;
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m23839(ArrayList<Element> arrayList, Element element, Element element2) {
        int lastIndexOf = arrayList.lastIndexOf(element);
        Validate.m23519(lastIndexOf != -1);
        arrayList.set(lastIndexOf, element2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m23840(String str, String[] strArr, String[] strArr2) {
        this.f18341[0] = str;
        return m23842(this.f18341, strArr, strArr2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m23841(ArrayList<Element> arrayList, Element element) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size) == element) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m23842(String[] strArr, String[] strArr2, String[] strArr3) {
        int i = 100;
        int size = this.f18532.size() - 1;
        if (size <= 100) {
            i = size;
        }
        for (int i2 = i; i2 >= 0; i2--) {
            String r0 = ((Element) this.f18532.get(i2)).m23676();
            if (StringUtil.m23499(r0, strArr)) {
                return true;
            }
            if (StringUtil.m23499(r0, strArr2)) {
                return false;
            }
            if (strArr3 != null && StringUtil.m23499(r0, strArr3)) {
                return false;
            }
        }
        Validate.m23512("Should not be reachable");
        return false;
    }

    public String toString() {
        return "TreeBuilder{currentToken=" + this.f18531 + ", state=" + this.f18331 + ", currentElement=" + m24154() + '}';
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Document m23843() {
        return this.f18535;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public Element m23844(Element element) {
        if (f18323 || m23886(element)) {
            for (int size = this.f18532.size() - 1; size >= 0; size--) {
                if (((Element) this.f18532.get(size)) == element) {
                    return (Element) this.f18532.get(size - 1);
                }
            }
            return null;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m23845(String str) {
        return m23907(str, f18325);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public String m23846() {
        return this.f18530;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m23847(Element element) {
        this.f18329 = element;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m23848(String str) {
        return m23907(str, f18327);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m23849() {
        return this.f18340;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m23850(String str) {
        return m23840(str, f18326, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m23851(Element element) {
        return StringUtil.m23499(element.m23676(), f18322);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public void m23852() {
        m23838("tr", "template");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public void m23853() {
        boolean z = false;
        int size = this.f18532.size() - 1;
        while (size >= 0) {
            Element element = (Element) this.f18532.get(size);
            if (size == 0) {
                z = true;
                element = this.f18334;
            }
            String r0 = element.m23676();
            if ("select".equals(r0)) {
                m23901(HtmlTreeBuilderState.InSelect);
                return;
            } else if ("td".equals(r0) || ("th".equals(r0) && !z)) {
                m23901(HtmlTreeBuilderState.InCell);
                return;
            } else if ("tr".equals(r0)) {
                m23901(HtmlTreeBuilderState.InRow);
                return;
            } else if ("tbody".equals(r0) || "thead".equals(r0) || "tfoot".equals(r0)) {
                m23901(HtmlTreeBuilderState.InTableBody);
                return;
            } else if ("caption".equals(r0)) {
                m23901(HtmlTreeBuilderState.InCaption);
                return;
            } else if ("colgroup".equals(r0)) {
                m23901(HtmlTreeBuilderState.InColumnGroup);
                return;
            } else if ("table".equals(r0)) {
                m23901(HtmlTreeBuilderState.InTable);
                return;
            } else if (TtmlNode.TAG_HEAD.equals(r0)) {
                m23901(HtmlTreeBuilderState.InBody);
                return;
            } else if (TtmlNode.TAG_BODY.equals(r0)) {
                m23901(HtmlTreeBuilderState.InBody);
                return;
            } else if ("frameset".equals(r0)) {
                m23901(HtmlTreeBuilderState.InFrameset);
                return;
            } else if (AdType.HTML.equals(r0)) {
                m23901(HtmlTreeBuilderState.BeforeHead);
                return;
            } else if (z) {
                m23901(HtmlTreeBuilderState.InBody);
                return;
            } else {
                size--;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˆ  reason: contains not printable characters */
    public void m23854() {
        m23867((String) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public void m23855() {
        m23838("tbody", "tfoot", "thead", "template");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˉ  reason: contains not printable characters */
    public Element m23856() {
        if (this.f18335.size() > 0) {
            return this.f18335.get(this.f18335.size() - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public FormElement m23857() {
        return this.f18330;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m23858() {
        this.f18336 = new ArrayList();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public List<String> m23859() {
        return this.f18336;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public Element m23860() {
        int size = this.f18335.size();
        if (size > 0) {
            return this.f18335.remove(size - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public Element m23861() {
        return (Element) this.f18532.remove(this.f18532.size() - 1);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m23862(Element element) {
        int i = 0;
        int size = this.f18335.size() - 1;
        while (true) {
            if (size >= 0) {
                Element element2 = this.f18335.get(size);
                if (element2 == null) {
                    break;
                }
                int i2 = m23837(element, element2) ? i + 1 : i;
                if (i2 == 3) {
                    this.f18335.remove(size);
                    break;
                } else {
                    size--;
                    i = i2;
                }
            } else {
                break;
            }
        }
        this.f18335.add(element);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public boolean m23863(String str) {
        for (int size = this.f18532.size() - 1; size >= 0; size--) {
            String r0 = ((Element) this.f18532.get(size)).m23676();
            if (r0.equals(str)) {
                return true;
            }
            if (!StringUtil.m23499(r0, f18324)) {
                return false;
            }
        }
        Validate.m23512("Should not be reachable");
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: י  reason: contains not printable characters */
    public void m23864() {
        boolean z;
        Element element;
        int i;
        Element r2 = m23856();
        if (r2 != null && !m23886(r2)) {
            int size = this.f18335.size();
            int i2 = size - 1;
            while (true) {
                if (i2 == 0) {
                    i = i2;
                    element = r2;
                    z = true;
                    break;
                }
                i2--;
                Element element2 = this.f18335.get(i2);
                if (element2 == null) {
                    z = false;
                    int i3 = i2;
                    element = element2;
                    i = i3;
                    break;
                } else if (m23886(element2)) {
                    z = false;
                    int i4 = i2;
                    element = element2;
                    i = i4;
                    break;
                } else {
                    r2 = element2;
                }
            }
            while (true) {
                if (!z) {
                    int i5 = i + 1;
                    int i6 = i5;
                    element = this.f18335.get(i5);
                    i = i6;
                }
                Validate.m23515((Object) element);
                Element r22 = m23892(element.m23676());
                r22.m23635().m23558(element.m23635());
                this.f18335.set(i, r22);
                if (i != size - 1) {
                    z = false;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:3:0x000c, LOOP_START, MTH_ENTER_BLOCK] */
    /* renamed from: ـ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m23865() {
        /*
            r1 = this;
        L_0x0000:
            java.util.ArrayList<org2.jsoup.nodes.Element> r0 = r1.f18335
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x000e
            org2.jsoup.nodes.Element r0 = r1.m23860()
            if (r0 != 0) goto L_0x0000
        L_0x000e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.HtmlTreeBuilder.m23865():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public ArrayList<Element> m23866() {
        return this.f18532;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m23867(String str) {
        while (str != null && !m24154().m23676().equals(str) && StringUtil.m23499(m24154().m23676(), f18321)) {
            m23861();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public void m23868(Element element) {
        for (int size = this.f18335.size() - 1; size >= 0; size--) {
            if (this.f18335.get(size) == element) {
                this.f18335.remove(size);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public Element m23869(String str) {
        int size = this.f18335.size() - 1;
        while (size >= 0) {
            Element element = this.f18335.get(size);
            if (element == null) {
                break;
            } else if (element.m23676().equals(str)) {
                return element;
            } else {
                size--;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m23870() {
        m23838("table");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m23871(Element element) {
        return m23841(this.f18335, element);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᴵ  reason: contains not printable characters */
    public void m23872() {
        this.f18335.add((Object) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m23873() {
        return this.f18338;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m23874(String str) {
        return m23907(str, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m23875(Element element) {
        for (int size = this.f18532.size() - 1; size >= 0; size--) {
            if (((Element) this.f18532.get(size)) == element) {
                this.f18532.remove(size);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Element m23876(String str) {
        for (int size = this.f18532.size() - 1; size >= 0; size--) {
            Element element = (Element) this.f18532.get(size);
            if (element.m23676().equals(str)) {
                return element;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Element m23877(Token.StartTag startTag) {
        Tag r0 = Tag.m23969(startTag.m24013(), this.f18539);
        Element element = new Element(r0, this.f18530, startTag.f18426);
        m23836((Node) element);
        if (startTag.m24015()) {
            if (!r0.m23971()) {
                r0.m23974();
            } else if (!r0.m23977()) {
                this.f18534.m24053("Tag cannot be self closing; not a void tag");
            }
        }
        return element;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public HtmlTreeBuilderState m23878() {
        return this.f18331;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23879(Element element) {
        m23836((Node) element);
        this.f18532.add(element);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23880(Element element, Element element2) {
        m23839((ArrayList<Element>) this.f18532, element, element2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23881(HtmlTreeBuilderState htmlTreeBuilderState) {
        if (this.f18538.m23959()) {
            this.f18538.add(new ParseError(this.f18533.m23829(), "Unexpected token [%s] when in state [%s]", this.f18531.m23993(), htmlTreeBuilderState));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m23882(boolean z) {
        this.f18339 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m23883(String[] strArr) {
        return m23842(strArr, f18328, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public HtmlTreeBuilderState m23884() {
        return this.f18332;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m23885(String str) {
        int size = this.f18532.size() - 1;
        while (size >= 0 && !((Element) this.f18532.get(size)).m23676().equals(str)) {
            this.f18532.remove(size);
            size--;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m23886(Element element) {
        return m23841((ArrayList<Element>) this.f18532, element);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m23887() {
        this.f18332 = this.f18331;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m23888(String str) {
        int size = this.f18532.size() - 1;
        while (size >= 0) {
            this.f18532.remove(size);
            if (!((Element) this.f18532.get(size)).m23676().equals(str)) {
                size--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m23889(Element element) {
        this.f18532.add(element);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m23890(Element element, Element element2) {
        m23839(this.f18335, element, element2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public List<Node> m23891(String str, Element element, String str2, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        this.f18331 = HtmlTreeBuilderState.Initial;
        m23896(new StringReader(str), str2, parseErrorList, parseSettings);
        this.f18334 = element;
        this.f18340 = true;
        Element element2 = null;
        if (element != null) {
            if (element.m23741() != null) {
                this.f18535.m23593(element.m23741().m23586());
            }
            String r0 = element.m23684();
            if (StringUtil.m23510(r0, PubnativeAsset.TITLE, "textarea")) {
                this.f18534.m24064(TokeniserState.Rcdata);
            } else {
                if (StringUtil.m23510(r0, "iframe", "noembed", "noframes", TtmlNode.TAG_STYLE, "xmp")) {
                    this.f18534.m24064(TokeniserState.Rawtext);
                } else if (r0.equals("script")) {
                    this.f18534.m24064(TokeniserState.ScriptData);
                } else if (r0.equals("noscript")) {
                    this.f18534.m24064(TokeniserState.Data);
                } else if (r0.equals("plaintext")) {
                    this.f18534.m24064(TokeniserState.Data);
                } else {
                    this.f18534.m24064(TokeniserState.Data);
                }
            }
            Element element3 = new Element(Tag.m23969(AdType.HTML, parseSettings), str2);
            this.f18535.m23679((Node) element3);
            this.f18532.add(element3);
            m23853();
            Elements r02 = element.m23639();
            r02.add(0, element);
            Iterator it2 = r02.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    element2 = element3;
                    break;
                }
                Element element4 = (Element) it2.next();
                if (element4 instanceof FormElement) {
                    this.f18330 = (FormElement) element4;
                    element2 = element3;
                    break;
                }
            }
        }
        m24153();
        return element != null ? element2.m23751() : this.f18535.m23751();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Element m23892(String str) {
        Element element = new Element(Tag.m23969(str, this.f18539), this.f18530);
        m23879(element);
        return element;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Element m23893(Token.StartTag startTag) {
        if (startTag.m24015()) {
            Element r0 = m23877(startTag);
            this.f18532.add(r0);
            this.f18534.m24064(TokeniserState.Data);
            this.f18534.m24063((Token) this.f18337.m24016().m24022(r0.m23684()));
            return r0;
        }
        Element element = new Element(Tag.m23969(startTag.m24013(), this.f18539), this.f18530, this.f18539.m23961(startTag.f18426));
        m23879(element);
        return element;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public FormElement m23894(Token.StartTag startTag, boolean z) {
        FormElement formElement = new FormElement(Tag.m23969(startTag.m24013(), this.f18539), this.f18530, startTag.f18426);
        m23899(formElement);
        m23836((Node) formElement);
        if (z) {
            this.f18532.add(formElement);
        }
        return formElement;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ParseSettings m23895() {
        return ParseSettings.f18388;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23896(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        super.m24157(reader, str, parseErrorList, parseSettings);
        this.f18331 = HtmlTreeBuilderState.Initial;
        this.f18332 = null;
        this.f18333 = false;
        this.f18329 = null;
        this.f18330 = null;
        this.f18334 = null;
        this.f18335 = new ArrayList<>();
        this.f18336 = new ArrayList();
        this.f18337 = new Token.EndTag();
        this.f18338 = true;
        this.f18339 = false;
        this.f18340 = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23897(Element element) {
        if (!this.f18333) {
            String r0 = element.m23766("href");
            if (r0.length() != 0) {
                this.f18530 = r0;
                this.f18333 = true;
                this.f18535.m23753(r0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23898(Element element, Element element2) {
        int lastIndexOf = this.f18532.lastIndexOf(element);
        Validate.m23519(lastIndexOf != -1);
        this.f18532.add(lastIndexOf + 1, element2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23899(FormElement formElement) {
        this.f18330 = formElement;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23900(Node node) {
        Element element;
        boolean z;
        Element r2 = m23876("table");
        if (r2 == null) {
            element = (Element) this.f18532.get(0);
            z = false;
        } else if (r2.m23637() != null) {
            element = r2.m23637();
            z = true;
        } else {
            element = m23844(r2);
            z = false;
        }
        if (z) {
            Validate.m23515((Object) r2);
            r2.m23626(node);
            return;
        }
        element.m23679(node);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23901(HtmlTreeBuilderState htmlTreeBuilderState) {
        this.f18331 = htmlTreeBuilderState;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23902(Token.Character character) {
        String r0 = m24154().m23684();
        m24154().m23679((r0.equals("script") || r0.equals(TtmlNode.TAG_STYLE)) ? new DataNode(character.m23994()) : new TextNode(character.m23994()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23903(Token.Comment comment) {
        m23836((Node) new Comment(comment.m23997()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23904(boolean z) {
        this.f18338 = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m23905(String... strArr) {
        int size = this.f18532.size() - 1;
        while (size >= 0) {
            this.f18532.remove(size);
            if (!StringUtil.m23499(((Element) this.f18532.get(size)).m23676(), strArr)) {
                size--;
            } else {
                return;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m23906(String str, Attributes attributes) {
        return super.m24158(str, attributes);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m23907(String str, String[] strArr) {
        return m23840(str, f18328, strArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m23908(Token token) {
        this.f18531 = token;
        return this.f18331.m23921(token, this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m23909(Token token, HtmlTreeBuilderState htmlTreeBuilderState) {
        this.f18531 = token;
        return htmlTreeBuilderState.m23921(token, this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public Element m23910() {
        return this.f18329;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m23911() {
        return this.f18339;
    }
}
