package org2.jsoup.parser;

import java.util.Arrays;
import org.apache.commons.lang3.CharUtils;
import org2.jsoup.parser.Token;

enum TokeniserState {
    Data {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24083(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23825()) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24061(characterReader.m23822());
                    return;
                case '&':
                    tokeniser.m24054(CharacterReferenceInData);
                    return;
                case '<':
                    tokeniser.m24054(TagOpen);
                    return;
                case 65535:
                    tokeniser.m24063((Token) new Token.EOF());
                    return;
                default:
                    tokeniser.m24062(characterReader.m23813());
                    return;
            }
        }
    },
    CharacterReferenceInData {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24095(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24072(tokeniser, Data);
        }
    },
    Rcdata {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24106(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23825()) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    characterReader.m23805();
                    tokeniser.m24061(65533);
                    return;
                case '&':
                    tokeniser.m24054(CharacterReferenceInRcdata);
                    return;
                case '<':
                    tokeniser.m24054(RcdataLessthanSign);
                    return;
                case 65535:
                    tokeniser.m24063((Token) new Token.EOF());
                    return;
                default:
                    tokeniser.m24062(characterReader.m23833('&', '<', 0));
                    return;
            }
        }
    },
    CharacterReferenceInRcdata {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24117(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24072(tokeniser, Rcdata);
        }
    },
    Rawtext {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24128(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24074(tokeniser, characterReader, this, RawtextLessthanSign);
        }
    },
    ScriptData {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24139(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24074(tokeniser, characterReader, this, ScriptDataLessthanSign);
        }
    },
    PLAINTEXT {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24148(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23825()) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    characterReader.m23805();
                    tokeniser.m24061(65533);
                    return;
                case 65535:
                    tokeniser.m24063((Token) new Token.EOF());
                    return;
                default:
                    tokeniser.m24062(characterReader.m23818(0));
                    return;
            }
        }
    },
    TagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24149(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23825()) {
                case '!':
                    tokeniser.m24054(MarkupDeclarationOpen);
                    return;
                case '/':
                    tokeniser.m24054(EndTagOpen);
                    return;
                case '?':
                    tokeniser.m24054(BogusComment);
                    return;
                default:
                    if (characterReader.m23835()) {
                        tokeniser.m24059(true);
                        tokeniser.m24064(TagName);
                        return;
                    }
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24061('<');
                    tokeniser.m24064(Data);
                    return;
            }
        }
    },
    EndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24150(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23820()) {
                tokeniser.m24056(this);
                tokeniser.m24062("</");
                tokeniser.m24064(Data);
            } else if (characterReader.m23835()) {
                tokeniser.m24059(false);
                tokeniser.m24064(TagName);
            } else if (characterReader.m23826('>')) {
                tokeniser.m24058((TokeniserState) this);
                tokeniser.m24054(Data);
            } else {
                tokeniser.m24058((TokeniserState) this);
                tokeniser.m24054(BogusComment);
            }
        }
    },
    TagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24084(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.f18451.m24018(characterReader.m23814());
            switch (characterReader.m23822()) {
                case 0:
                    tokeniser.f18451.m24018(TokeniserState.f18475);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BeforeAttributeName);
                    return;
                case '/':
                    tokeniser.m24064(SelfClosingStartTag);
                    return;
                case '>':
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    return;
            }
        }
    },
    RcdataLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24085(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23826('/')) {
                tokeniser.m24048();
                tokeniser.m24054(RCDATAEndTagOpen);
            } else if (!characterReader.m23835() || tokeniser.m24050() == null || characterReader.m23806("</" + tokeniser.m24050())) {
                tokeniser.m24062("<");
                tokeniser.m24064(Rcdata);
            } else {
                tokeniser.f18451 = tokeniser.m24059(false).m24022(tokeniser.m24050());
                tokeniser.m24052();
                characterReader.m23816();
                tokeniser.m24064(Data);
            }
        }
    },
    RCDATAEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24086(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23835()) {
                tokeniser.m24059(false);
                tokeniser.f18451.m24023(characterReader.m23825());
                tokeniser.f18454.append(characterReader.m23825());
                tokeniser.m24054(RCDATAEndTagName);
                return;
            }
            tokeniser.m24062("</");
            tokeniser.m24064(Rcdata);
        }
    },
    RCDATAEndTagName {
        /* renamed from: 靐  reason: contains not printable characters */
        private void m24087(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.m24062("</" + tokeniser.f18454.toString());
            characterReader.m23816();
            tokeniser.m24064(Rcdata);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24088(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23835()) {
                String r0 = characterReader.m23811();
                tokeniser.f18451.m24018(r0);
                tokeniser.f18454.append(r0);
                return;
            }
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    if (tokeniser.m24049()) {
                        tokeniser.m24064(BeforeAttributeName);
                        return;
                    } else {
                        m24087(tokeniser, characterReader);
                        return;
                    }
                case '/':
                    if (tokeniser.m24049()) {
                        tokeniser.m24064(SelfClosingStartTag);
                        return;
                    } else {
                        m24087(tokeniser, characterReader);
                        return;
                    }
                case '>':
                    if (tokeniser.m24049()) {
                        tokeniser.m24052();
                        tokeniser.m24064(Data);
                        return;
                    }
                    m24087(tokeniser, characterReader);
                    return;
                default:
                    m24087(tokeniser, characterReader);
                    return;
            }
        }
    },
    RawtextLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24089(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23826('/')) {
                tokeniser.m24048();
                tokeniser.m24054(RawtextEndTagOpen);
                return;
            }
            tokeniser.m24061('<');
            tokeniser.m24064(Rawtext);
        }
    },
    RawtextEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24090(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24068(tokeniser, characterReader, RawtextEndTagName, Rawtext);
        }
    },
    RawtextEndTagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24091(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24070(tokeniser, characterReader, Rawtext);
        }
    },
    ScriptDataLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24092(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case '!':
                    tokeniser.m24062("<!");
                    tokeniser.m24064(ScriptDataEscapeStart);
                    return;
                case '/':
                    tokeniser.m24048();
                    tokeniser.m24064(ScriptDataEndTagOpen);
                    return;
                default:
                    tokeniser.m24062("<");
                    characterReader.m23816();
                    tokeniser.m24064(ScriptData);
                    return;
            }
        }
    },
    ScriptDataEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24093(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24068(tokeniser, characterReader, ScriptDataEndTagName, ScriptData);
        }
    },
    ScriptDataEndTagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24094(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24070(tokeniser, characterReader, ScriptData);
        }
    },
    ScriptDataEscapeStart {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24096(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23826('-')) {
                tokeniser.m24061('-');
                tokeniser.m24054(ScriptDataEscapeStartDash);
                return;
            }
            tokeniser.m24064(ScriptData);
        }
    },
    ScriptDataEscapeStartDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24097(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23826('-')) {
                tokeniser.m24061('-');
                tokeniser.m24054(ScriptDataEscapedDashDash);
                return;
            }
            tokeniser.m24064(ScriptData);
        }
    },
    ScriptDataEscaped {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24098(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23820()) {
                tokeniser.m24056(this);
                tokeniser.m24064(Data);
                return;
            }
            switch (characterReader.m23825()) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    characterReader.m23805();
                    tokeniser.m24061(65533);
                    return;
                case '-':
                    tokeniser.m24061('-');
                    tokeniser.m24054(ScriptDataEscapedDash);
                    return;
                case '<':
                    tokeniser.m24054(ScriptDataEscapedLessthanSign);
                    return;
                default:
                    tokeniser.m24062(characterReader.m23833('-', '<', 0));
                    return;
            }
        }
    },
    ScriptDataEscapedDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24099(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23820()) {
                tokeniser.m24056(this);
                tokeniser.m24064(Data);
                return;
            }
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24061(65533);
                    tokeniser.m24064(ScriptDataEscaped);
                    return;
                case '-':
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataEscapedDashDash);
                    return;
                case '<':
                    tokeniser.m24064(ScriptDataEscapedLessthanSign);
                    return;
                default:
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataEscaped);
                    return;
            }
        }
    },
    ScriptDataEscapedDashDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24100(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23820()) {
                tokeniser.m24056(this);
                tokeniser.m24064(Data);
                return;
            }
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24061(65533);
                    tokeniser.m24064(ScriptDataEscaped);
                    return;
                case '-':
                    tokeniser.m24061(r0);
                    return;
                case '<':
                    tokeniser.m24064(ScriptDataEscapedLessthanSign);
                    return;
                case '>':
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptData);
                    return;
                default:
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataEscaped);
                    return;
            }
        }
    },
    ScriptDataEscapedLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24101(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23835()) {
                tokeniser.m24048();
                tokeniser.f18454.append(characterReader.m23825());
                tokeniser.m24062("<" + characterReader.m23825());
                tokeniser.m24054(ScriptDataDoubleEscapeStart);
            } else if (characterReader.m23826('/')) {
                tokeniser.m24048();
                tokeniser.m24054(ScriptDataEscapedEndTagOpen);
            } else {
                tokeniser.m24061('<');
                tokeniser.m24064(ScriptDataEscaped);
            }
        }
    },
    ScriptDataEscapedEndTagOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24102(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23835()) {
                tokeniser.m24059(false);
                tokeniser.f18451.m24023(characterReader.m23825());
                tokeniser.f18454.append(characterReader.m23825());
                tokeniser.m24054(ScriptDataEscapedEndTagName);
                return;
            }
            tokeniser.m24062("</");
            tokeniser.m24064(ScriptDataEscaped);
        }
    },
    ScriptDataEscapedEndTagName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24103(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24070(tokeniser, characterReader, ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscapeStart {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24104(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24067(tokeniser, characterReader, ScriptDataDoubleEscaped, ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscaped {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24105(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23825();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    characterReader.m23805();
                    tokeniser.m24061(65533);
                    return;
                case '-':
                    tokeniser.m24061(r0);
                    tokeniser.m24054(ScriptDataDoubleEscapedDash);
                    return;
                case '<':
                    tokeniser.m24061(r0);
                    tokeniser.m24054(ScriptDataDoubleEscapedLessthanSign);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24062(characterReader.m23833('-', '<', 0));
                    return;
            }
        }
    },
    ScriptDataDoubleEscapedDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24107(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24061(65533);
                    tokeniser.m24064(ScriptDataDoubleEscaped);
                    return;
                case '-':
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataDoubleEscapedDashDash);
                    return;
                case '<':
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataDoubleEscapedLessthanSign);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataDoubleEscaped);
                    return;
            }
        }
    },
    ScriptDataDoubleEscapedDashDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24108(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24061(65533);
                    tokeniser.m24064(ScriptDataDoubleEscaped);
                    return;
                case '-':
                    tokeniser.m24061(r0);
                    return;
                case '<':
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataDoubleEscapedLessthanSign);
                    return;
                case '>':
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptData);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24061(r0);
                    tokeniser.m24064(ScriptDataDoubleEscaped);
                    return;
            }
        }
    },
    ScriptDataDoubleEscapedLessthanSign {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24109(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23826('/')) {
                tokeniser.m24061('/');
                tokeniser.m24048();
                tokeniser.m24054(ScriptDataDoubleEscapeEnd);
                return;
            }
            tokeniser.m24064(ScriptDataDoubleEscaped);
        }
    },
    ScriptDataDoubleEscapeEnd {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24110(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.m24067(tokeniser, characterReader, ScriptDataEscaped, ScriptDataDoubleEscaped);
        }
    },
    BeforeAttributeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24111(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24025();
                    characterReader.m23816();
                    tokeniser.m24064(AttributeName);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                case '\'':
                case '<':
                case '=':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24025();
                    tokeniser.f18451.m24017(r0);
                    tokeniser.m24064(AttributeName);
                    return;
                case '/':
                    tokeniser.m24064(SelfClosingStartTag);
                    return;
                case '>':
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18451.m24025();
                    characterReader.m23816();
                    tokeniser.m24064(AttributeName);
                    return;
            }
        }
    },
    AttributeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24112(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.f18451.m24021(characterReader.m23819(TokeniserState.f18473));
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24017(65533);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(AfterAttributeName);
                    return;
                case '\"':
                case '\'':
                case '<':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24017(r0);
                    return;
                case '/':
                    tokeniser.m24064(SelfClosingStartTag);
                    return;
                case '=':
                    tokeniser.m24064(BeforeAttributeValue);
                    return;
                case '>':
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    return;
            }
        }
    },
    AfterAttributeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24113(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24017(65533);
                    tokeniser.m24064(AttributeName);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                case '\'':
                case '<':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24025();
                    tokeniser.f18451.m24017(r0);
                    tokeniser.m24064(AttributeName);
                    return;
                case '/':
                    tokeniser.m24064(SelfClosingStartTag);
                    return;
                case '=':
                    tokeniser.m24064(BeforeAttributeValue);
                    return;
                case '>':
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18451.m24025();
                    characterReader.m23816();
                    tokeniser.m24064(AttributeName);
                    return;
            }
        }
    },
    BeforeAttributeValue {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24114(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24020(65533);
                    tokeniser.m24064(AttributeValue_unquoted);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    tokeniser.m24064(AttributeValue_doubleQuoted);
                    return;
                case '&':
                    characterReader.m23816();
                    tokeniser.m24064(AttributeValue_unquoted);
                    return;
                case '\'':
                    tokeniser.m24064(AttributeValue_singleQuoted);
                    return;
                case '<':
                case '=':
                case '`':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24020(r0);
                    tokeniser.m24064(AttributeValue_unquoted);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                default:
                    characterReader.m23816();
                    tokeniser.m24064(AttributeValue_unquoted);
                    return;
            }
        }
    },
    AttributeValue_doubleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24115(Tokeniser tokeniser, CharacterReader characterReader) {
            String r0 = characterReader.m23833(TokeniserState.f18472);
            if (r0.length() > 0) {
                tokeniser.f18451.m24019(r0);
            } else {
                tokeniser.f18451.m24012();
            }
            switch (characterReader.m23822()) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24020(65533);
                    return;
                case '\"':
                    tokeniser.m24064(AfterAttributeValue_quoted);
                    return;
                case '&':
                    int[] r02 = tokeniser.m24066('\"', true);
                    if (r02 != null) {
                        tokeniser.f18451.m24024(r02);
                        return;
                    } else {
                        tokeniser.f18451.m24020('&');
                        return;
                    }
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    return;
            }
        }
    },
    AttributeValue_singleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24116(Tokeniser tokeniser, CharacterReader characterReader) {
            String r0 = characterReader.m23833(TokeniserState.f18471);
            if (r0.length() > 0) {
                tokeniser.f18451.m24019(r0);
            } else {
                tokeniser.f18451.m24012();
            }
            switch (characterReader.m23822()) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24020(65533);
                    return;
                case '&':
                    int[] r02 = tokeniser.m24066('\'', true);
                    if (r02 != null) {
                        tokeniser.f18451.m24024(r02);
                        return;
                    } else {
                        tokeniser.f18451.m24020('&');
                        return;
                    }
                case '\'':
                    tokeniser.m24064(AfterAttributeValue_quoted);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    return;
            }
        }
    },
    AttributeValue_unquoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24118(Tokeniser tokeniser, CharacterReader characterReader) {
            String r0 = characterReader.m23819(TokeniserState.f18474);
            if (r0.length() > 0) {
                tokeniser.f18451.m24019(r0);
            }
            char r02 = characterReader.m23822();
            switch (r02) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24020(65533);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BeforeAttributeName);
                    return;
                case '\"':
                case '\'':
                case '<':
                case '=':
                case '`':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18451.m24020(r02);
                    return;
                case '&':
                    int[] r03 = tokeniser.m24066('>', true);
                    if (r03 != null) {
                        tokeniser.f18451.m24024(r03);
                        return;
                    } else {
                        tokeniser.f18451.m24020('&');
                        return;
                    }
                case '>':
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    return;
            }
        }
    },
    AfterAttributeValue_quoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24119(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BeforeAttributeName);
                    return;
                case '/':
                    tokeniser.m24064(SelfClosingStartTag);
                    return;
                case '>':
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    characterReader.m23816();
                    tokeniser.m24064(BeforeAttributeName);
                    return;
            }
        }
    },
    SelfClosingStartTag {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24120(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case '>':
                    tokeniser.f18451.f18428 = true;
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    characterReader.m23816();
                    tokeniser.m24064(BeforeAttributeName);
                    return;
            }
        }
    },
    BogusComment {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24121(Tokeniser tokeniser, CharacterReader characterReader) {
            characterReader.m23816();
            Token.Comment comment = new Token.Comment();
            comment.f18415 = true;
            comment.f18414.append(characterReader.m23818('>'));
            tokeniser.m24063((Token) comment);
            tokeniser.m24054(Data);
        }
    },
    MarkupDeclarationOpen {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24122(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23823("--")) {
                tokeniser.m24057();
                tokeniser.m24064(CommentStart);
            } else if (characterReader.m23817("DOCTYPE")) {
                tokeniser.m24064(Doctype);
            } else if (characterReader.m23823("[CDATA[")) {
                tokeniser.m24064(CdataSection);
            } else {
                tokeniser.m24058((TokeniserState) this);
                tokeniser.m24054(BogusComment);
            }
        }
    },
    CommentStart {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24123(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18441.f18414.append(65533);
                    tokeniser.m24064(Comment);
                    return;
                case '-':
                    tokeniser.m24064(CommentStartDash);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18441.f18414.append(r0);
                    tokeniser.m24064(Comment);
                    return;
            }
        }
    },
    CommentStartDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24124(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18441.f18414.append(65533);
                    tokeniser.m24064(Comment);
                    return;
                case '-':
                    tokeniser.m24064(CommentStartDash);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18441.f18414.append(r0);
                    tokeniser.m24064(Comment);
                    return;
            }
        }
    },
    Comment {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24125(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23825()) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    characterReader.m23805();
                    tokeniser.f18441.f18414.append(65533);
                    return;
                case '-':
                    tokeniser.m24054(CommentEndDash);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18441.f18414.append(characterReader.m23833('-', 0));
                    return;
            }
        }
    },
    CommentEndDash {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24126(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18441.f18414.append('-').append(65533);
                    tokeniser.m24064(Comment);
                    return;
                case '-':
                    tokeniser.m24064(CommentEnd);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18441.f18414.append('-').append(r0);
                    tokeniser.m24064(Comment);
                    return;
            }
        }
    },
    CommentEnd {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24127(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18441.f18414.append("--").append(65533);
                    tokeniser.m24064(Comment);
                    return;
                case '!':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(CommentEndBang);
                    return;
                case '-':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18441.f18414.append('-');
                    return;
                case '>':
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18441.f18414.append("--").append(r0);
                    tokeniser.m24064(Comment);
                    return;
            }
        }
    },
    CommentEndBang {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24129(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18441.f18414.append("--!").append(65533);
                    tokeniser.m24064(Comment);
                    return;
                case '-':
                    tokeniser.f18441.f18414.append("--!");
                    tokeniser.m24064(CommentEndDash);
                    return;
                case '>':
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24055();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18441.f18414.append("--!").append(r0);
                    tokeniser.m24064(Comment);
                    return;
            }
        }
    },
    Doctype {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24130(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BeforeDoctypeName);
                    return;
                case '>':
                    break;
                case 65535:
                    tokeniser.m24056(this);
                    break;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(BeforeDoctypeName);
                    return;
            }
            tokeniser.m24058((TokeniserState) this);
            tokeniser.m24051();
            tokeniser.f18440.f18416 = true;
            tokeniser.m24047();
            tokeniser.m24064(Data);
        }
    },
    BeforeDoctypeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24131(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23835()) {
                tokeniser.m24051();
                tokeniser.m24064(DoctypeName);
                return;
            }
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24051();
                    tokeniser.f18440.f18418.append(65533);
                    tokeniser.m24064(DoctypeName);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.m24051();
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24051();
                    tokeniser.f18440.f18418.append(r0);
                    tokeniser.m24064(DoctypeName);
                    return;
            }
        }
    },
    DoctypeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24132(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23835()) {
                tokeniser.f18440.f18418.append(characterReader.m23811());
                return;
            }
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18418.append(65533);
                    return;
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(AfterDoctypeName);
                    return;
                case '>':
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18440.f18418.append(r0);
                    return;
            }
        }
    },
    AfterDoctypeName {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24133(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.m23820()) {
                tokeniser.m24056(this);
                tokeniser.f18440.f18416 = true;
                tokeniser.m24047();
                tokeniser.m24064(Data);
            } else if (characterReader.m23828(9, 10, CharUtils.CR, 12, ' ')) {
                characterReader.m23805();
            } else if (characterReader.m23826('>')) {
                tokeniser.m24047();
                tokeniser.m24054(Data);
            } else if (characterReader.m23817("PUBLIC")) {
                tokeniser.f18440.f18420 = "PUBLIC";
                tokeniser.m24064(AfterDoctypePublicKeyword);
            } else if (characterReader.m23817("SYSTEM")) {
                tokeniser.f18440.f18420 = "SYSTEM";
                tokeniser.m24064(AfterDoctypeSystemKeyword);
            } else {
                tokeniser.m24058((TokeniserState) this);
                tokeniser.f18440.f18416 = true;
                tokeniser.m24054(BogusDoctype);
            }
        }
    },
    AfterDoctypePublicKeyword {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24134(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BeforeDoctypePublicIdentifier);
                    return;
                case '\"':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypePublicIdentifier_doubleQuoted);
                    return;
                case '\'':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypePublicIdentifier_singleQuoted);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24064(BogusDoctype);
                    return;
            }
        }
    },
    BeforeDoctypePublicIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24135(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    tokeniser.m24064(DoctypePublicIdentifier_doubleQuoted);
                    return;
                case '\'':
                    tokeniser.m24064(DoctypePublicIdentifier_singleQuoted);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24064(BogusDoctype);
                    return;
            }
        }
    },
    DoctypePublicIdentifier_doubleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24136(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18419.append(65533);
                    return;
                case '\"':
                    tokeniser.m24064(AfterDoctypePublicIdentifier);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18440.f18419.append(r0);
                    return;
            }
        }
    },
    DoctypePublicIdentifier_singleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24137(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18419.append(65533);
                    return;
                case '\'':
                    tokeniser.m24064(AfterDoctypePublicIdentifier);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18440.f18419.append(r0);
                    return;
            }
        }
    },
    AfterDoctypePublicIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24138(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BetweenDoctypePublicAndSystemIdentifiers);
                    return;
                case '\"':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24064(BogusDoctype);
                    return;
            }
        }
    },
    BetweenDoctypePublicAndSystemIdentifiers {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24140(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24064(BogusDoctype);
                    return;
            }
        }
    },
    AfterDoctypeSystemKeyword {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24141(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BeforeDoctypeSystemIdentifier);
                    return;
                case '\"':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    return;
            }
        }
    },
    BeforeDoctypeSystemIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24142(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '\"':
                    tokeniser.m24064(DoctypeSystemIdentifier_doubleQuoted);
                    return;
                case '\'':
                    tokeniser.m24064(DoctypeSystemIdentifier_singleQuoted);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24064(BogusDoctype);
                    return;
            }
        }
    },
    DoctypeSystemIdentifier_doubleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24143(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18417.append(65533);
                    return;
                case '\"':
                    tokeniser.m24064(AfterDoctypeSystemIdentifier);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18440.f18417.append(r0);
                    return;
            }
        }
    },
    DoctypeSystemIdentifier_singleQuoted {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24144(Tokeniser tokeniser, CharacterReader characterReader) {
            char r0 = characterReader.m23822();
            switch (r0) {
                case 0:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18417.append(65533);
                    return;
                case '\'':
                    tokeniser.m24064(AfterDoctypeSystemIdentifier);
                    return;
                case '>':
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.f18440.f18417.append(r0);
                    return;
            }
        }
    },
    AfterDoctypeSystemIdentifier {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24145(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    return;
                case '>':
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24056(this);
                    tokeniser.f18440.f18416 = true;
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    tokeniser.m24058((TokeniserState) this);
                    tokeniser.m24064(BogusDoctype);
                    return;
            }
        }
    },
    BogusDoctype {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24146(Tokeniser tokeniser, CharacterReader characterReader) {
            switch (characterReader.m23822()) {
                case '>':
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                case 65535:
                    tokeniser.m24047();
                    tokeniser.m24064(Data);
                    return;
                default:
                    return;
            }
        }
    },
    CdataSection {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public void m24147(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.m24062(characterReader.m23832("]]>"));
            characterReader.m23823("]]>");
            tokeniser.m24064(Data);
        }
    };
    
    /* access modifiers changed from: private */

    /* renamed from: ʻי  reason: contains not printable characters */
    public static final char[] f18471 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻـ  reason: contains not printable characters */
    public static final char[] f18472 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻٴ  reason: contains not printable characters */
    public static final char[] f18473 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻᐧ  reason: contains not printable characters */
    public static final char[] f18474 = null;
    /* access modifiers changed from: private */

    /* renamed from: ʻᴵ  reason: contains not printable characters */
    public static final String f18475 = null;

    static {
        f18471 = new char[]{'\'', '&', 0};
        f18472 = new char[]{'\"', '&', 0};
        f18473 = new char[]{9, 10, CharUtils.CR, 12, ' ', '/', '=', '>', 0, '\"', '\'', '<'};
        f18474 = new char[]{9, 10, CharUtils.CR, 12, ' ', '&', '>', 0, '\"', '\'', '<', '=', '`'};
        f18475 = String.valueOf(65533);
        Arrays.sort(f18471);
        Arrays.sort(f18472);
        Arrays.sort(f18473);
        Arrays.sort(f18474);
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static void m24067(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState, TokeniserState tokeniserState2) {
        if (characterReader.m23835()) {
            String r0 = characterReader.m23811();
            tokeniser.f18454.append(r0);
            tokeniser.m24062(r0);
            return;
        }
        char r02 = characterReader.m23822();
        switch (r02) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
            case '/':
            case '>':
                if (tokeniser.f18454.toString().equals("script")) {
                    tokeniser.m24064(tokeniserState);
                } else {
                    tokeniser.m24064(tokeniserState2);
                }
                tokeniser.m24061(r02);
                return;
            default:
                characterReader.m23816();
                tokeniser.m24064(tokeniserState2);
                return;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 连任  reason: contains not printable characters */
    public static void m24068(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState, TokeniserState tokeniserState2) {
        if (characterReader.m23835()) {
            tokeniser.m24059(false);
            tokeniser.m24064(tokeniserState);
            return;
        }
        tokeniser.m24062("</");
        tokeniser.m24064(tokeniserState2);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m24070(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState) {
        if (characterReader.m23835()) {
            String r0 = characterReader.m23811();
            tokeniser.f18451.m24018(r0);
            tokeniser.f18454.append(r0);
            return;
        }
        boolean z = false;
        if (tokeniser.m24049() && !characterReader.m23820()) {
            char r2 = characterReader.m23822();
            switch (r2) {
                case 9:
                case 10:
                case 12:
                case 13:
                case ' ':
                    tokeniser.m24064(BeforeAttributeName);
                    break;
                case '/':
                    tokeniser.m24064(SelfClosingStartTag);
                    break;
                case '>':
                    tokeniser.m24052();
                    tokeniser.m24064(Data);
                    break;
                default:
                    tokeniser.f18454.append(r2);
                    z = true;
                    break;
            }
        } else {
            z = true;
        }
        if (z) {
            tokeniser.m24062("</" + tokeniser.f18454.toString());
            tokeniser.m24064(tokeniserState);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m24072(Tokeniser tokeniser, TokeniserState tokeniserState) {
        int[] r0 = tokeniser.m24066((Character) null, false);
        if (r0 == null) {
            tokeniser.m24061('&');
        } else {
            tokeniser.m24065(r0);
        }
        tokeniser.m24064(tokeniserState);
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static void m24074(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState, TokeniserState tokeniserState2) {
        switch (characterReader.m23825()) {
            case 0:
                tokeniser.m24058(tokeniserState);
                characterReader.m23805();
                tokeniser.m24061(65533);
                return;
            case '<':
                tokeniser.m24054(tokeniserState2);
                return;
            case 65535:
                tokeniser.m24063((Token) new Token.EOF());
                return;
            default:
                tokeniser.m24062(characterReader.m23833('<', 0));
                return;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m24082(Tokeniser tokeniser, CharacterReader characterReader);
}
