package org2.jsoup.parser;

import java.io.Reader;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Attributes;
import org2.jsoup.nodes.Document;
import org2.jsoup.nodes.DocumentType;
import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.Node;
import org2.jsoup.nodes.TextNode;
import org2.jsoup.parser.Token;

public class XmlTreeBuilder extends TreeBuilder {
    /* renamed from: 龘  reason: contains not printable characters */
    private void m24160(Node node) {
        m24154().m23679(node);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24161(Token.EndTag endTag) {
        Element element;
        String r3 = endTag.m24013();
        int size = this.f18532.size() - 1;
        while (true) {
            if (size < 0) {
                element = null;
                break;
            }
            Element element2 = (Element) this.f18532.get(size);
            if (element2.m23676().equals(r3)) {
                element = element2;
                break;
            }
            size--;
        }
        if (element != null) {
            int size2 = this.f18532.size() - 1;
            while (size2 >= 0) {
                Element element3 = (Element) this.f18532.get(size2);
                this.f18532.remove(size2);
                if (element3 != element) {
                    size2--;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Element m24162(Token.StartTag startTag) {
        Tag r0 = Tag.m23969(startTag.m24013(), this.f18539);
        Element element = new Element(r0, this.f18530, this.f18539.m23961(startTag.f18426));
        m24160((Node) element);
        if (!startTag.m24015()) {
            this.f18532.add(element);
        } else if (!r0.m23971()) {
            r0.m23974();
        }
        return element;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public ParseSettings m24163() {
        return ParseSettings.f18387;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24164(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        super.m24157(reader, str, parseErrorList, parseSettings);
        this.f18532.add(this.f18535);
        this.f18535.m23585().m23602(Document.OutputSettings.Syntax.xml);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24165(Token.Character character) {
        m24160((Node) new TextNode(character.m23994()));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0028, code lost:
        if (r1.startsWith("?") != false) goto L_0x002a;
     */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m24166(org2.jsoup.parser.Token.Comment r6) {
        /*
            r5 = this;
            r3 = 1
            org2.jsoup.nodes.Comment r0 = new org2.jsoup.nodes.Comment
            java.lang.String r1 = r6.m23997()
            r0.<init>(r1)
            boolean r1 = r6.f18415
            if (r1 == 0) goto L_0x007f
            java.lang.String r1 = r0.m23560()
            int r2 = r1.length()
            if (r2 <= r3) goto L_0x007f
            java.lang.String r2 = "!"
            boolean r2 = r1.startsWith(r2)
            if (r2 != 0) goto L_0x002a
            java.lang.String r2 = "?"
            boolean r2 = r1.startsWith(r2)
            if (r2 == 0) goto L_0x007f
        L_0x002a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "<"
            java.lang.StringBuilder r0 = r0.append(r2)
            int r2 = r1.length()
            int r2 = r2 + -1
            java.lang.String r2 = r1.substring(r3, r2)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = ">"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r2 = r5.f18530
            org2.jsoup.parser.Parser r3 = org2.jsoup.parser.Parser.m23964()
            org2.jsoup.nodes.Document r0 = org2.jsoup.Jsoup.m23496(r0, r2, r3)
            r2 = 0
            org2.jsoup.nodes.Element r2 = r0.m23677((int) r2)
            org2.jsoup.nodes.XmlDeclaration r0 = new org2.jsoup.nodes.XmlDeclaration
            org2.jsoup.parser.ParseSettings r3 = r5.f18539
            java.lang.String r4 = r2.m23684()
            java.lang.String r3 = r3.m23960((java.lang.String) r4)
            java.lang.String r4 = "!"
            boolean r1 = r1.startsWith(r4)
            r0.<init>(r3, r1)
            org2.jsoup.nodes.Attributes r1 = r0.m23734()
            org2.jsoup.nodes.Attributes r2 = r2.m23635()
            r1.m23558((org2.jsoup.nodes.Attributes) r2)
        L_0x007f:
            r5.m24160((org2.jsoup.nodes.Node) r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.XmlTreeBuilder.m24166(org2.jsoup.parser.Token$Comment):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24167(Token.Doctype doctype) {
        DocumentType documentType = new DocumentType(this.f18539.m23960(doctype.m23999()), doctype.m24004(), doctype.m24000());
        documentType.m23605(doctype.m24003());
        m24160((Node) documentType);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public /* bridge */ /* synthetic */ boolean m24168(String str, Attributes attributes) {
        return super.m24158(str, attributes);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24169(Token token) {
        switch (token.f18412) {
            case StartTag:
                m24162(token.m23981());
                return true;
            case EndTag:
                m24161(token.m23983());
                return true;
            case Comment:
                m24166(token.m23987());
                return true;
            case Character:
                m24165(token.m23985());
                return true;
            case Doctype:
                m24167(token.m23991());
                return true;
            case EOF:
                return true;
            default:
                Validate.m23512("Unexpected token type: " + token.f18412);
                return true;
        }
    }
}
