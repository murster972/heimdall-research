package org2.jsoup.parser;

import java.io.Reader;
import java.util.ArrayList;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Attributes;
import org2.jsoup.nodes.Document;
import org2.jsoup.nodes.Element;
import org2.jsoup.parser.Token;

abstract class TreeBuilder {

    /* renamed from: ʾ  reason: contains not printable characters */
    protected String f18530;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected Token f18531;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected ArrayList<Element> f18532;

    /* renamed from: ˑ  reason: contains not printable characters */
    CharacterReader f18533;

    /* renamed from: ٴ  reason: contains not printable characters */
    Tokeniser f18534;

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected Document f18535;

    /* renamed from: 靐  reason: contains not printable characters */
    private Token.EndTag f18536 = new Token.EndTag();

    /* renamed from: 龘  reason: contains not printable characters */
    private Token.StartTag f18537 = new Token.StartTag();

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected ParseErrorList f18538;

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected ParseSettings f18539;

    TreeBuilder() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m24151(String str) {
        return this.f18531 == this.f18536 ? m24159(new Token.EndTag().m24022(str)) : m24159(this.f18536.m24016().m24022(str));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m24152(String str) {
        return this.f18531 == this.f18537 ? m24159(new Token.StartTag().m24022(str)) : m24159(this.f18537.m24007().m24022(str));
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᵎ  reason: contains not printable characters */
    public void m24153() {
        Token r0;
        do {
            r0 = this.f18534.m24060();
            m24159(r0);
            r0.m23990();
        } while (r0.f18412 != Token.TokenType.EOF);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᵔ  reason: contains not printable characters */
    public Element m24154() {
        int size = this.f18532.size();
        if (size > 0) {
            return this.f18532.get(size - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public Document m24155(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        m24157(reader, str, parseErrorList, parseSettings);
        m24153();
        return this.f18535;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract ParseSettings m24156();

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24157(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        Validate.m23516((Object) reader, "String input must not be null");
        Validate.m23516((Object) str, "BaseURI must not be null");
        this.f18535 = new Document(str);
        this.f18539 = parseSettings;
        this.f18533 = new CharacterReader(reader);
        this.f18538 = parseErrorList;
        this.f18531 = null;
        this.f18534 = new Tokeniser(this.f18533, parseErrorList);
        this.f18532 = new ArrayList<>(32);
        this.f18530 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24158(String str, Attributes attributes) {
        if (this.f18531 == this.f18537) {
            return m24159(new Token.StartTag().m24008(str, attributes));
        }
        this.f18537.m24007();
        this.f18537.m24008(str, attributes);
        return m24159(this.f18537);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m24159(Token token);
}
