package org2.jsoup.parser;

import android.support.v4.app.NotificationCompat;
import com.google.android.exoplayer2.util.MimeTypes;
import com.mopub.common.AdType;
import java.util.HashMap;
import java.util.Map;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.oltu.oauth2.common.OAuth;
import org2.jsoup.helper.Validate;

public class Tag {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static final String[] f18395 = {"meta", "link", "base", "frame", "img", TtmlNode.TAG_BR, "wbr", "embed", "hr", "input", "keygen", "col", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track"};

    /* renamed from: ʿ  reason: contains not printable characters */
    private static final String[] f18396 = {PubnativeAsset.TITLE, "a", TtmlNode.TAG_P, "h1", "h2", "h3", "h4", "h5", "h6", "pre", "address", "li", "th", "td", "script", TtmlNode.TAG_STYLE, "ins", "del", "s"};

    /* renamed from: ˈ  reason: contains not printable characters */
    private static final String[] f18397 = {"object", "base", "font", TtmlNode.TAG_TT, "i", "b", "u", "big", "small", "em", "strong", "dfn", OAuth.OAUTH_CODE, "samp", "kbd", "var", "cite", "abbr", "time", "acronym", "mark", "ruby", "rt", "rp", "a", "img", TtmlNode.TAG_BR, "wbr", "map", "q", "sub", "sup", "bdo", "iframe", "embed", TtmlNode.TAG_SPAN, "input", "select", "textarea", "label", "button", "optgroup", "option", "legend", "datalist", "keygen", "output", NotificationCompat.CATEGORY_PROGRESS, "meter", "area", "param", "source", "track", "summary", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track", "data", "bdi", "s"};

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final String[] f18398 = {"input", "keygen", "object", "select", "textarea"};

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static final String[] f18399 = {AdType.HTML, TtmlNode.TAG_HEAD, TtmlNode.TAG_BODY, "frameset", "script", "noscript", TtmlNode.TAG_STYLE, "meta", "link", PubnativeAsset.TITLE, "frame", "noframes", "section", "nav", "aside", "hgroup", "header", "footer", TtmlNode.TAG_P, "h1", "h2", "h3", "h4", "h5", "h6", "ul", "ol", "pre", TtmlNode.TAG_DIV, "blockquote", "hr", "address", "figure", "figcaption", "form", "fieldset", "ins", "del", "dl", "dt", "dd", "li", "table", "caption", "thead", "tfoot", "tbody", "colgroup", "col", "tr", "th", "td", "video", MimeTypes.BASE_TYPE_AUDIO, "canvas", "details", "menu", "plaintext", "template", "article", "main", "svg", "math"};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final Map<String, Tag> f18400 = new HashMap();

    /* renamed from: ﹶ  reason: contains not printable characters */
    private static final String[] f18401 = {"pre", "plaintext", PubnativeAsset.TITLE, "textarea"};

    /* renamed from: ﾞ  reason: contains not printable characters */
    private static final String[] f18402 = {"button", "fieldset", "input", "keygen", "object", "output", "select", "textarea"};

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f18403 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f18404 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f18405 = false;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f18406 = false;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f18407 = false;

    /* renamed from: 连任  reason: contains not printable characters */
    private boolean f18408 = true;

    /* renamed from: 靐  reason: contains not printable characters */
    private String f18409;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f18410 = true;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f18411 = true;

    static {
        for (String tag : f18399) {
            m23970(new Tag(tag));
        }
        for (String tag2 : f18397) {
            Tag tag3 = new Tag(tag2);
            tag3.f18411 = false;
            tag3.f18410 = false;
            m23970(tag3);
        }
        for (String str : f18395) {
            Tag tag4 = f18400.get(str);
            Validate.m23515((Object) tag4);
            tag4.f18408 = false;
            tag4.f18403 = true;
        }
        for (String str2 : f18396) {
            Tag tag5 = f18400.get(str2);
            Validate.m23515((Object) tag5);
            tag5.f18410 = false;
        }
        for (String str3 : f18401) {
            Tag tag6 = f18400.get(str3);
            Validate.m23515((Object) tag6);
            tag6.f18405 = true;
        }
        for (String str4 : f18402) {
            Tag tag7 = f18400.get(str4);
            Validate.m23515((Object) tag7);
            tag7.f18406 = true;
        }
        for (String str5 : f18398) {
            Tag tag8 = f18400.get(str5);
            Validate.m23515((Object) tag8);
            tag8.f18407 = true;
        }
    }

    private Tag(String str) {
        this.f18409 = str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Tag m23969(String str, ParseSettings parseSettings) {
        Validate.m23515((Object) str);
        Tag tag = f18400.get(str);
        if (tag != null) {
            return tag;
        }
        String r1 = parseSettings.m23960(str);
        Validate.m23517(r1);
        Tag tag2 = f18400.get(r1);
        if (tag2 != null) {
            return tag2;
        }
        Tag tag3 = new Tag(r1);
        tag3.f18411 = false;
        return tag3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static void m23970(Tag tag) {
        f18400.put(tag.f18409, tag);
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Tag)) {
            return false;
        }
        Tag tag = (Tag) obj;
        if (!this.f18409.equals(tag.f18409) || this.f18408 != tag.f18408 || this.f18403 != tag.f18403 || this.f18410 != tag.f18410 || this.f18411 != tag.f18411 || this.f18405 != tag.f18405 || this.f18404 != tag.f18404 || this.f18406 != tag.f18406) {
            return false;
        }
        if (this.f18407 != tag.f18407) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 1;
        int hashCode = ((this.f18406 ? 1 : 0) + (((this.f18405 ? 1 : 0) + (((this.f18404 ? 1 : 0) + (((this.f18403 ? 1 : 0) + (((this.f18408 ? 1 : 0) + (((this.f18410 ? 1 : 0) + (((this.f18411 ? 1 : 0) + (this.f18409.hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (!this.f18407) {
            i = 0;
        }
        return hashCode + i;
    }

    public String toString() {
        return this.f18409;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m23971() {
        return f18400.containsKey(this.f18409);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public boolean m23972() {
        return this.f18405;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m23973() {
        return this.f18406;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public Tag m23974() {
        this.f18404 = true;
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m23975() {
        return this.f18403 || this.f18404;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m23976() {
        return this.f18411;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m23977() {
        return this.f18403;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23978() {
        return this.f18410;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23979() {
        return this.f18409;
    }
}
