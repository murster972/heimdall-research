package org2.jsoup.parser;

import org2.jsoup.internal.Normalizer;
import org2.jsoup.nodes.Attributes;

public class ParseSettings {

    /* renamed from: 靐  reason: contains not printable characters */
    public static final ParseSettings f18387 = new ParseSettings(true, true);

    /* renamed from: 龘  reason: contains not printable characters */
    public static final ParseSettings f18388 = new ParseSettings(false, false);

    /* renamed from: 麤  reason: contains not printable characters */
    private final boolean f18389;

    /* renamed from: 齉  reason: contains not printable characters */
    private final boolean f18390;

    public ParseSettings(boolean z, boolean z2) {
        this.f18390 = z;
        this.f18389 = z2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m23960(String str) {
        String trim = str.trim();
        return !this.f18390 ? Normalizer.m23524(trim) : trim;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Attributes m23961(Attributes attributes) {
        if (!this.f18389) {
            attributes.m23545();
        }
        return attributes;
    }
}
