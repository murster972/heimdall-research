package org2.jsoup.parser;

import com.mopub.common.AdType;
import com.mopub.mobileads.VastExtensionXmlManager;
import java.util.ArrayList;
import net.pubnative.library.request.PubnativeAsset;
import org.apache.oltu.oauth2.common.OAuth;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.nodes.Document;
import org2.jsoup.nodes.DocumentType;
import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.Node;
import org2.jsoup.parser.Token;

enum HtmlTreeBuilderState {
    Initial {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23922(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.m23914(token)) {
                return true;
            }
            if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
                return true;
            } else if (token.m23992()) {
                Token.Doctype r1 = token.m23991();
                DocumentType documentType = new DocumentType(htmlTreeBuilder.f18539.m23960(r1.m23999()), r1.m24004(), r1.m24000());
                documentType.m23605(r1.m24003());
                htmlTreeBuilder.m23843().m23679((Node) documentType);
                if (r1.m24001()) {
                    htmlTreeBuilder.m23843().m23593(Document.QuirksMode.quirks);
                }
                htmlTreeBuilder.m23901(BeforeHtml);
                return true;
            } else {
                htmlTreeBuilder.m23901(BeforeHtml);
                return htmlTreeBuilder.m23908(token);
            }
        }
    },
    BeforeHtml {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m23941(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.m23892(AdType.HTML);
            htmlTreeBuilder.m23901(BeforeHead);
            return htmlTreeBuilder.m23908(token);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23942(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23992()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return false;
            }
            if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
            } else if (HtmlTreeBuilderState.m23914(token)) {
                return true;
            } else {
                if (!token.m23989() || !token.m23981().m24014().equals(AdType.HTML)) {
                    if (token.m23982()) {
                        if (StringUtil.m23510(token.m23983().m24014(), TtmlNode.TAG_HEAD, TtmlNode.TAG_BODY, AdType.HTML, TtmlNode.TAG_BR)) {
                            return m23941(token, htmlTreeBuilder);
                        }
                    }
                    if (!token.m23982()) {
                        return m23941(token, htmlTreeBuilder);
                    }
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                }
                htmlTreeBuilder.m23893(token.m23981());
                htmlTreeBuilder.m23901(BeforeHead);
            }
            return true;
        }
    },
    BeforeHead {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23947(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.m23914(token)) {
                return true;
            }
            if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
                return true;
            } else if (token.m23992()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return false;
            } else if (token.m23989() && token.m23981().m24014().equals(AdType.HTML)) {
                return InBody.m23921(token, htmlTreeBuilder);
            } else {
                if (!token.m23989() || !token.m23981().m24014().equals(TtmlNode.TAG_HEAD)) {
                    if (token.m23982()) {
                        if (StringUtil.m23510(token.m23983().m24014(), TtmlNode.TAG_HEAD, TtmlNode.TAG_BODY, AdType.HTML, TtmlNode.TAG_BR)) {
                            htmlTreeBuilder.m24152(TtmlNode.TAG_HEAD);
                            return htmlTreeBuilder.m23908(token);
                        }
                    }
                    if (token.m23982()) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                    htmlTreeBuilder.m24152(TtmlNode.TAG_HEAD);
                    return htmlTreeBuilder.m23908(token);
                }
                htmlTreeBuilder.m23847(htmlTreeBuilder.m23893(token.m23981()));
                htmlTreeBuilder.m23901(InHead);
                return true;
            }
        }
    },
    InHead {
        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m23948(Token token, TreeBuilder treeBuilder) {
            treeBuilder.m24151(TtmlNode.TAG_HEAD);
            return treeBuilder.m24159(token);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23949(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.m23914(token)) {
                htmlTreeBuilder.m23902(token.m23985());
                return true;
            }
            switch (token.f18412) {
                case Comment:
                    htmlTreeBuilder.m23903(token.m23987());
                    return true;
                case Doctype:
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                case StartTag:
                    Token.StartTag r2 = token.m23981();
                    String r3 = r2.m24014();
                    if (r3.equals(AdType.HTML)) {
                        return InBody.m23921(token, htmlTreeBuilder);
                    }
                    if (StringUtil.m23510(r3, "base", "basefont", "bgsound", "command", "link")) {
                        Element r1 = htmlTreeBuilder.m23877(r2);
                        if (!r3.equals("base") || !r1.m23764("href")) {
                            return true;
                        }
                        htmlTreeBuilder.m23897(r1);
                        return true;
                    } else if (r3.equals("meta")) {
                        htmlTreeBuilder.m23877(r2);
                        return true;
                    } else if (r3.equals(PubnativeAsset.TITLE)) {
                        HtmlTreeBuilderState.m23916(r2, htmlTreeBuilder);
                        return true;
                    } else {
                        if (StringUtil.m23510(r3, "noframes", TtmlNode.TAG_STYLE)) {
                            HtmlTreeBuilderState.m23915(r2, htmlTreeBuilder);
                            return true;
                        } else if (r3.equals("noscript")) {
                            htmlTreeBuilder.m23893(r2);
                            htmlTreeBuilder.m23901(InHeadNoscript);
                            return true;
                        } else if (r3.equals("script")) {
                            htmlTreeBuilder.f18534.m24064(TokeniserState.ScriptData);
                            htmlTreeBuilder.m23887();
                            htmlTreeBuilder.m23901(Text);
                            htmlTreeBuilder.m23893(r2);
                            return true;
                        } else if (!r3.equals(TtmlNode.TAG_HEAD)) {
                            return m23948(token, (TreeBuilder) htmlTreeBuilder);
                        } else {
                            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                            return false;
                        }
                    }
                case EndTag:
                    String r22 = token.m23983().m24014();
                    if (r22.equals(TtmlNode.TAG_HEAD)) {
                        htmlTreeBuilder.m23861();
                        htmlTreeBuilder.m23901(AfterHead);
                        return true;
                    }
                    if (StringUtil.m23510(r22, TtmlNode.TAG_BODY, AdType.HTML, TtmlNode.TAG_BR)) {
                        return m23948(token, (TreeBuilder) htmlTreeBuilder);
                    }
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                default:
                    return m23948(token, (TreeBuilder) htmlTreeBuilder);
            }
        }
    },
    InHeadNoscript {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m23950(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
            htmlTreeBuilder.m23902(new Token.Character().m23996(token.toString()));
            return true;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x008e, code lost:
            if (org2.jsoup.helper.StringUtil.m23510(r8.m23981().m24014(), "basefont", "bgsound", "link", "meta", "noframes", com.google.android.exoplayer2.text.ttml.TtmlNode.TAG_STYLE) != false) goto L_0x0090;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d3, code lost:
            if (org2.jsoup.helper.StringUtil.m23510(r8.m23981().m24014(), com.google.android.exoplayer2.text.ttml.TtmlNode.TAG_HEAD, "noscript") == false) goto L_0x00d5;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m23951(org2.jsoup.parser.Token r8, org2.jsoup.parser.HtmlTreeBuilder r9) {
            /*
                r7 = this;
                r6 = 2
                r1 = 1
                r0 = 0
                boolean r2 = r8.m23992()
                if (r2 == 0) goto L_0x000e
                r9.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r7)
            L_0x000c:
                r0 = r1
            L_0x000d:
                return r0
            L_0x000e:
                boolean r2 = r8.m23989()
                if (r2 == 0) goto L_0x002c
                org2.jsoup.parser.Token$StartTag r2 = r8.m23981()
                java.lang.String r2 = r2.m24014()
                java.lang.String r3 = "html"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x002c
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InBody
                boolean r0 = r9.m23909((org2.jsoup.parser.Token) r8, (org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x002c:
                boolean r2 = r8.m23982()
                if (r2 == 0) goto L_0x004c
                org2.jsoup.parser.Token$EndTag r2 = r8.m23983()
                java.lang.String r2 = r2.m24014()
                java.lang.String r3 = "noscript"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x004c
                r9.m23861()
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InHead
                r9.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000c
            L_0x004c:
                boolean r2 = org2.jsoup.parser.HtmlTreeBuilderState.m23914((org2.jsoup.parser.Token) r8)
                if (r2 != 0) goto L_0x0090
                boolean r2 = r8.m23986()
                if (r2 != 0) goto L_0x0090
                boolean r2 = r8.m23989()
                if (r2 == 0) goto L_0x0098
                org2.jsoup.parser.Token$StartTag r2 = r8.m23981()
                java.lang.String r2 = r2.m24014()
                r3 = 6
                java.lang.String[] r3 = new java.lang.String[r3]
                java.lang.String r4 = "basefont"
                r3[r0] = r4
                java.lang.String r4 = "bgsound"
                r3[r1] = r4
                java.lang.String r4 = "link"
                r3[r6] = r4
                r4 = 3
                java.lang.String r5 = "meta"
                r3[r4] = r5
                r4 = 4
                java.lang.String r5 = "noframes"
                r3[r4] = r5
                r4 = 5
                java.lang.String r5 = "style"
                r3[r4] = r5
                boolean r2 = org2.jsoup.helper.StringUtil.m23510((java.lang.String) r2, (java.lang.String[]) r3)
                if (r2 == 0) goto L_0x0098
            L_0x0090:
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InHead
                boolean r0 = r9.m23909((org2.jsoup.parser.Token) r8, (org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x0098:
                boolean r2 = r8.m23982()
                if (r2 == 0) goto L_0x00b5
                org2.jsoup.parser.Token$EndTag r2 = r8.m23983()
                java.lang.String r2 = r2.m24014()
                java.lang.String r3 = "br"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x00b5
                boolean r0 = r7.m23950(r8, r9)
                goto L_0x000d
            L_0x00b5:
                boolean r2 = r8.m23989()
                if (r2 == 0) goto L_0x00d5
                org2.jsoup.parser.Token$StartTag r2 = r8.m23981()
                java.lang.String r2 = r2.m24014()
                java.lang.String[] r3 = new java.lang.String[r6]
                java.lang.String r4 = "head"
                r3[r0] = r4
                java.lang.String r4 = "noscript"
                r3[r1] = r4
                boolean r1 = org2.jsoup.helper.StringUtil.m23510((java.lang.String) r2, (java.lang.String[]) r3)
                if (r1 != 0) goto L_0x00db
            L_0x00d5:
                boolean r1 = r8.m23982()
                if (r1 == 0) goto L_0x00e0
            L_0x00db:
                r9.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r7)
                goto L_0x000d
            L_0x00e0:
                boolean r0 = r7.m23950(r8, r9)
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.HtmlTreeBuilderState.AnonymousClass5.m23951(org2.jsoup.parser.Token, org2.jsoup.parser.HtmlTreeBuilder):boolean");
        }
    },
    AfterHead {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m23952(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.m24152(TtmlNode.TAG_BODY);
            htmlTreeBuilder.m23904(true);
            return htmlTreeBuilder.m23908(token);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23953(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.m23914(token)) {
                htmlTreeBuilder.m23902(token.m23985());
            } else if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
            } else if (token.m23992()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
            } else if (token.m23989()) {
                Token.StartTag r2 = token.m23981();
                String r3 = r2.m24014();
                if (r3.equals(AdType.HTML)) {
                    return htmlTreeBuilder.m23909(token, InBody);
                }
                if (r3.equals(TtmlNode.TAG_BODY)) {
                    htmlTreeBuilder.m23893(r2);
                    htmlTreeBuilder.m23904(false);
                    htmlTreeBuilder.m23901(InBody);
                } else if (r3.equals("frameset")) {
                    htmlTreeBuilder.m23893(r2);
                    htmlTreeBuilder.m23901(InFrameset);
                } else {
                    if (StringUtil.m23510(r3, "base", "basefont", "bgsound", "link", "meta", "noframes", "script", TtmlNode.TAG_STYLE, PubnativeAsset.TITLE)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        Element r0 = htmlTreeBuilder.m23910();
                        htmlTreeBuilder.m23889(r0);
                        htmlTreeBuilder.m23909(token, InHead);
                        htmlTreeBuilder.m23875(r0);
                    } else if (r3.equals(TtmlNode.TAG_HEAD)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    } else {
                        m23952(token, htmlTreeBuilder);
                    }
                }
            } else if (token.m23982()) {
                if (StringUtil.m23510(token.m23983().m24014(), TtmlNode.TAG_BODY, AdType.HTML)) {
                    m23952(token, htmlTreeBuilder);
                } else {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                }
            } else {
                m23952(token, htmlTreeBuilder);
            }
            return true;
        }
    },
    InBody {
        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m23954(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            String r2 = htmlTreeBuilder.f18539.m23960(token.m23983().m24013());
            ArrayList<Element> r3 = htmlTreeBuilder.m23866();
            int size = r3.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                Element element = r3.get(size);
                if (element.m23676().equals(r2)) {
                    htmlTreeBuilder.m23867(r2);
                    if (!r2.equals(htmlTreeBuilder.m24154().m23676())) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    }
                    htmlTreeBuilder.m23888(r2);
                } else if (htmlTreeBuilder.m23851(element)) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    size--;
                }
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:303:0x0725  */
        /* JADX WARNING: Removed duplicated region for block: B:309:0x075d A[LOOP:9: B:308:0x075b->B:309:0x075d, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:316:0x078e  */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m23955(org2.jsoup.parser.Token r13, org2.jsoup.parser.HtmlTreeBuilder r14) {
            /*
                r12 = this;
                int[] r0 = org2.jsoup.parser.HtmlTreeBuilderState.AnonymousClass24.f18367
                org2.jsoup.parser.Token$TokenType r1 = r13.f18412
                int r1 = r1.ordinal()
                r0 = r0[r1]
                switch(r0) {
                    case 1: goto L_0x0044;
                    case 2: goto L_0x004c;
                    case 3: goto L_0x0051;
                    case 4: goto L_0x0666;
                    case 5: goto L_0x000f;
                    default: goto L_0x000d;
                }
            L_0x000d:
                r0 = 1
            L_0x000e:
                return r0
            L_0x000f:
                org2.jsoup.parser.Token$Character r0 = r13.m23985()
                java.lang.String r1 = r0.m23994()
                java.lang.String r2 = org2.jsoup.parser.HtmlTreeBuilderState.f18356
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x0026
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x0026:
                boolean r1 = r14.m23873()
                if (r1 == 0) goto L_0x0039
                boolean r1 = org2.jsoup.parser.HtmlTreeBuilderState.m23914((org2.jsoup.parser.Token) r0)
                if (r1 == 0) goto L_0x0039
                r14.m23864()
                r14.m23902((org2.jsoup.parser.Token.Character) r0)
                goto L_0x000d
            L_0x0039:
                r14.m23864()
                r14.m23902((org2.jsoup.parser.Token.Character) r0)
                r0 = 0
                r14.m23904((boolean) r0)
                goto L_0x000d
            L_0x0044:
                org2.jsoup.parser.Token$Comment r0 = r13.m23987()
                r14.m23903((org2.jsoup.parser.Token.Comment) r0)
                goto L_0x000d
            L_0x004c:
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x0051:
                org2.jsoup.parser.Token$StartTag r2 = r13.m23981()
                java.lang.String r0 = r2.m24014()
                java.lang.String r1 = "a"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x008e
                java.lang.String r0 = "a"
                org2.jsoup.nodes.Element r0 = r14.m23869((java.lang.String) r0)
                if (r0 == 0) goto L_0x0083
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.lang.String r0 = "a"
                r14.m24151(r0)
                java.lang.String r0 = "a"
                org2.jsoup.nodes.Element r0 = r14.m23876((java.lang.String) r0)
                if (r0 == 0) goto L_0x0083
                r14.m23868((org2.jsoup.nodes.Element) r0)
                r14.m23875((org2.jsoup.nodes.Element) r0)
            L_0x0083:
                r14.m23864()
                org2.jsoup.nodes.Element r0 = r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r14.m23862((org2.jsoup.nodes.Element) r0)
                goto L_0x000d
            L_0x008e:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18375
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x00a2
                r14.m23864()
                r14.m23877((org2.jsoup.parser.Token.StartTag) r2)
                r0 = 0
                r14.m23904((boolean) r0)
                goto L_0x000d
            L_0x00a2:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18379
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x00be
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x00b9
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x00b9:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x00be:
                java.lang.String r1 = "span"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x00cf
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x00cf:
                java.lang.String r1 = "li"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x012c
                r0 = 0
                r14.m23904((boolean) r0)
                java.util.ArrayList r3 = r14.m23866()
                int r0 = r3.size()
                int r0 = r0 + -1
                r1 = r0
            L_0x00e7:
                if (r1 <= 0) goto L_0x0102
                java.lang.Object r0 = r3.get(r1)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                java.lang.String r4 = r0.m23676()
                java.lang.String r5 = "li"
                boolean r4 = r4.equals(r5)
                if (r4 == 0) goto L_0x0116
                java.lang.String r0 = "li"
                r14.m24151(r0)
            L_0x0102:
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x0111
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x0111:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x0116:
                boolean r4 = r14.m23851((org2.jsoup.nodes.Element) r0)
                if (r4 == 0) goto L_0x0128
                java.lang.String r0 = r0.m23676()
                java.lang.String[] r4 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18378
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r0, r4)
                if (r0 == 0) goto L_0x0102
            L_0x0128:
                int r0 = r1 + -1
                r1 = r0
                goto L_0x00e7
            L_0x012c:
                java.lang.String r1 = "html"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0169
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.util.ArrayList r0 = r14.m23866()
                r1 = 0
                java.lang.Object r0 = r0.get(r1)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                org2.jsoup.nodes.Attributes r1 = r2.m24011()
                java.util.Iterator r2 = r1.iterator()
            L_0x014b:
                boolean r1 = r2.hasNext()
                if (r1 == 0) goto L_0x000d
                java.lang.Object r1 = r2.next()
                org2.jsoup.nodes.Attribute r1 = (org2.jsoup.nodes.Attribute) r1
                java.lang.String r3 = r1.getKey()
                boolean r3 = r0.m23764((java.lang.String) r3)
                if (r3 != 0) goto L_0x014b
                org2.jsoup.nodes.Attributes r3 = r0.m23635()
                r3.m23556((org2.jsoup.nodes.Attribute) r1)
                goto L_0x014b
            L_0x0169:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18382
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x0179
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InHead
                boolean r0 = r14.m23909((org2.jsoup.parser.Token) r13, (org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000e
            L_0x0179:
                java.lang.String r1 = "body"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x01df
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.util.ArrayList r1 = r14.m23866()
                int r0 = r1.size()
                r3 = 1
                if (r0 == r3) goto L_0x01ab
                int r0 = r1.size()
                r3 = 2
                if (r0 <= r3) goto L_0x01ae
                r0 = 1
                java.lang.Object r0 = r1.get(r0)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                java.lang.String r0 = r0.m23676()
                java.lang.String r3 = "body"
                boolean r0 = r0.equals(r3)
                if (r0 != 0) goto L_0x01ae
            L_0x01ab:
                r0 = 0
                goto L_0x000e
            L_0x01ae:
                r0 = 0
                r14.m23904((boolean) r0)
                r0 = 1
                java.lang.Object r0 = r1.get(r0)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                org2.jsoup.nodes.Attributes r1 = r2.m24011()
                java.util.Iterator r2 = r1.iterator()
            L_0x01c1:
                boolean r1 = r2.hasNext()
                if (r1 == 0) goto L_0x000d
                java.lang.Object r1 = r2.next()
                org2.jsoup.nodes.Attribute r1 = (org2.jsoup.nodes.Attribute) r1
                java.lang.String r3 = r1.getKey()
                boolean r3 = r0.m23764((java.lang.String) r3)
                if (r3 != 0) goto L_0x01c1
                org2.jsoup.nodes.Attributes r3 = r0.m23635()
                r3.m23556((org2.jsoup.nodes.Attribute) r1)
                goto L_0x01c1
            L_0x01df:
                java.lang.String r1 = "frameset"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0248
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.util.ArrayList r1 = r14.m23866()
                int r0 = r1.size()
                r3 = 1
                if (r0 == r3) goto L_0x0211
                int r0 = r1.size()
                r3 = 2
                if (r0 <= r3) goto L_0x0214
                r0 = 1
                java.lang.Object r0 = r1.get(r0)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                java.lang.String r0 = r0.m23676()
                java.lang.String r3 = "body"
                boolean r0 = r0.equals(r3)
                if (r0 != 0) goto L_0x0214
            L_0x0211:
                r0 = 0
                goto L_0x000e
            L_0x0214:
                boolean r0 = r14.m23873()
                if (r0 != 0) goto L_0x021d
                r0 = 0
                goto L_0x000e
            L_0x021d:
                r0 = 1
                java.lang.Object r0 = r1.get(r0)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                org2.jsoup.nodes.Element r3 = r0.m23637()
                if (r3 == 0) goto L_0x022d
                r0.m23740()
            L_0x022d:
                int r0 = r1.size()
                r3 = 1
                if (r0 <= r3) goto L_0x023e
                int r0 = r1.size()
                int r0 = r0 + -1
                r1.remove(r0)
                goto L_0x022d
            L_0x023e:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InFrameset
                r14.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x0248:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18381
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x027a
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x025f
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x025f:
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18381
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r0 == 0) goto L_0x0275
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r14.m23861()
            L_0x0275:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x027a:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18380
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x029a
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x0291
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x0291:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r0 = 0
                r14.m23904((boolean) r0)
                goto L_0x000d
            L_0x029a:
                java.lang.String r1 = "form"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x02c4
                org2.jsoup.nodes.FormElement r0 = r14.m23857()
                if (r0 == 0) goto L_0x02af
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x02af:
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x02be
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x02be:
                r0 = 1
                r14.m23894((org2.jsoup.parser.Token.StartTag) r2, (boolean) r0)
                goto L_0x000d
            L_0x02c4:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18368
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x0320
                r0 = 0
                r14.m23904((boolean) r0)
                java.util.ArrayList r3 = r14.m23866()
                int r0 = r3.size()
                int r0 = r0 + -1
                r1 = r0
            L_0x02db:
                if (r1 <= 0) goto L_0x02f6
                java.lang.Object r0 = r3.get(r1)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                java.lang.String r4 = r0.m23676()
                java.lang.String[] r5 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18368
                boolean r4 = org2.jsoup.helper.StringUtil.m23499(r4, r5)
                if (r4 == 0) goto L_0x030a
                java.lang.String r0 = r0.m23676()
                r14.m24151(r0)
            L_0x02f6:
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x0305
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x0305:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x030a:
                boolean r4 = r14.m23851((org2.jsoup.nodes.Element) r0)
                if (r4 == 0) goto L_0x031c
                java.lang.String r0 = r0.m23676()
                java.lang.String[] r4 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18378
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r0, r4)
                if (r0 == 0) goto L_0x02f6
            L_0x031c:
                int r0 = r1 + -1
                r1 = r0
                goto L_0x02db
            L_0x0320:
                java.lang.String r1 = "plaintext"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0344
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x0338
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x0338:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                org2.jsoup.parser.Tokeniser r0 = r14.f18534
                org2.jsoup.parser.TokeniserState r1 = org2.jsoup.parser.TokeniserState.PLAINTEXT
                r0.m24064((org2.jsoup.parser.TokeniserState) r1)
                goto L_0x000d
            L_0x0344:
                java.lang.String r1 = "button"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0370
                java.lang.String r0 = "button"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x0364
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.lang.String r0 = "button"
                r14.m24151(r0)
                r14.m23908((org2.jsoup.parser.Token) r2)
                goto L_0x000d
            L_0x0364:
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r0 = 0
                r14.m23904((boolean) r0)
                goto L_0x000d
            L_0x0370:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18369
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x0384
                r14.m23864()
                org2.jsoup.nodes.Element r0 = r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r14.m23862((org2.jsoup.nodes.Element) r0)
                goto L_0x000d
            L_0x0384:
                java.lang.String r1 = "nobr"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x03ae
                r14.m23864()
                java.lang.String r0 = "nobr"
                boolean r0 = r14.m23874((java.lang.String) r0)
                if (r0 == 0) goto L_0x03a5
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.lang.String r0 = "nobr"
                r14.m24151(r0)
                r14.m23864()
            L_0x03a5:
                org2.jsoup.nodes.Element r0 = r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r14.m23862((org2.jsoup.nodes.Element) r0)
                goto L_0x000d
            L_0x03ae:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18370
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x03c5
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r14.m23872()
                r0 = 0
                r14.m23904((boolean) r0)
                goto L_0x000d
            L_0x03c5:
                java.lang.String r1 = "table"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x03f7
                org2.jsoup.nodes.Document r0 = r14.m23843()
                org2.jsoup.nodes.Document$QuirksMode r0 = r0.m23586()
                org2.jsoup.nodes.Document$QuirksMode r1 = org2.jsoup.nodes.Document.QuirksMode.quirks
                if (r0 == r1) goto L_0x03e9
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x03e9
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x03e9:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r0 = 0
                r14.m23904((boolean) r0)
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InTable
                r14.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x03f7:
                java.lang.String r1 = "input"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x041d
                r14.m23864()
                org2.jsoup.nodes.Element r0 = r14.m23877((org2.jsoup.parser.Token.StartTag) r2)
                java.lang.String r1 = "type"
                java.lang.String r0 = r0.m23760(r1)
                java.lang.String r1 = "hidden"
                boolean r0 = r0.equalsIgnoreCase(r1)
                if (r0 != 0) goto L_0x000d
                r0 = 0
                r14.m23904((boolean) r0)
                goto L_0x000d
            L_0x041d:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18376
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x042a
                r14.m23877((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x042a:
                java.lang.String r1 = "hr"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x044b
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x0442
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x0442:
                r14.m23877((org2.jsoup.parser.Token.StartTag) r2)
                r0 = 0
                r14.m23904((boolean) r0)
                goto L_0x000d
            L_0x044b:
                java.lang.String r1 = "image"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x046f
                java.lang.String r0 = "svg"
                org2.jsoup.nodes.Element r0 = r14.m23876((java.lang.String) r0)
                if (r0 != 0) goto L_0x046a
                java.lang.String r0 = "img"
                org2.jsoup.parser.Token$Tag r0 = r2.m24022((java.lang.String) r0)
                boolean r0 = r14.m23908((org2.jsoup.parser.Token) r0)
                goto L_0x000e
            L_0x046a:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x046f:
                java.lang.String r1 = "isindex"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0522
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                org2.jsoup.nodes.FormElement r0 = r14.m23857()
                if (r0 == 0) goto L_0x0484
                r0 = 0
                goto L_0x000e
            L_0x0484:
                java.lang.String r0 = "form"
                r14.m24152(r0)
                org2.jsoup.nodes.Attributes r0 = r2.f18426
                java.lang.String r1 = "action"
                boolean r0 = r0.m23543(r1)
                if (r0 == 0) goto L_0x04a8
                org2.jsoup.nodes.FormElement r0 = r14.m23857()
                java.lang.String r1 = "action"
                org2.jsoup.nodes.Attributes r3 = r2.f18426
                java.lang.String r4 = "action"
                java.lang.String r3 = r3.m23552(r4)
                r0.m23680((java.lang.String) r1, (java.lang.String) r3)
            L_0x04a8:
                java.lang.String r0 = "hr"
                r14.m24152(r0)
                java.lang.String r0 = "label"
                r14.m24152(r0)
                org2.jsoup.nodes.Attributes r0 = r2.f18426
                java.lang.String r1 = "prompt"
                boolean r0 = r0.m23543(r1)
                if (r0 == 0) goto L_0x04fb
                org2.jsoup.nodes.Attributes r0 = r2.f18426
                java.lang.String r1 = "prompt"
                java.lang.String r0 = r0.m23552(r1)
            L_0x04c8:
                org2.jsoup.parser.Token$Character r1 = new org2.jsoup.parser.Token$Character
                r1.<init>()
                org2.jsoup.parser.Token$Character r0 = r1.m23996(r0)
                r14.m23908((org2.jsoup.parser.Token) r0)
                org2.jsoup.nodes.Attributes r1 = new org2.jsoup.nodes.Attributes
                r1.<init>()
                org2.jsoup.nodes.Attributes r0 = r2.f18426
                java.util.Iterator r2 = r0.iterator()
            L_0x04df:
                boolean r0 = r2.hasNext()
                if (r0 == 0) goto L_0x04ff
                java.lang.Object r0 = r2.next()
                org2.jsoup.nodes.Attribute r0 = (org2.jsoup.nodes.Attribute) r0
                java.lang.String r3 = r0.getKey()
                java.lang.String[] r4 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18377
                boolean r3 = org2.jsoup.helper.StringUtil.m23499(r3, r4)
                if (r3 != 0) goto L_0x04df
                r1.m23556((org2.jsoup.nodes.Attribute) r0)
                goto L_0x04df
            L_0x04fb:
                java.lang.String r0 = "This is a searchable index. Enter search keywords: "
                goto L_0x04c8
            L_0x04ff:
                java.lang.String r0 = "name"
                java.lang.String r2 = "isindex"
                r1.m23555((java.lang.String) r0, (java.lang.String) r2)
                java.lang.String r0 = "input"
                r14.m23906((java.lang.String) r0, (org2.jsoup.nodes.Attributes) r1)
                java.lang.String r0 = "label"
                r14.m24151(r0)
                java.lang.String r0 = "hr"
                r14.m24152(r0)
                java.lang.String r0 = "form"
                r14.m24151(r0)
                goto L_0x000d
            L_0x0522:
                java.lang.String r1 = "textarea"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0543
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                org2.jsoup.parser.Tokeniser r0 = r14.f18534
                org2.jsoup.parser.TokeniserState r1 = org2.jsoup.parser.TokeniserState.Rcdata
                r0.m24064((org2.jsoup.parser.TokeniserState) r1)
                r14.m23887()
                r0 = 0
                r14.m23904((boolean) r0)
                org2.jsoup.parser.HtmlTreeBuilderState r0 = Text
                r14.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x0543:
                java.lang.String r1 = "xmp"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0567
                java.lang.String r0 = "p"
                boolean r0 = r14.m23848((java.lang.String) r0)
                if (r0 == 0) goto L_0x055b
                java.lang.String r0 = "p"
                r14.m24151(r0)
            L_0x055b:
                r14.m23864()
                r0 = 0
                r14.m23904((boolean) r0)
                org2.jsoup.parser.HtmlTreeBuilderState.m23915(r2, r14)
                goto L_0x000d
            L_0x0567:
                java.lang.String r1 = "iframe"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0579
                r0 = 0
                r14.m23904((boolean) r0)
                org2.jsoup.parser.HtmlTreeBuilderState.m23915(r2, r14)
                goto L_0x000d
            L_0x0579:
                java.lang.String r1 = "noembed"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0587
                org2.jsoup.parser.HtmlTreeBuilderState.m23915(r2, r14)
                goto L_0x000d
            L_0x0587:
                java.lang.String r1 = "select"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x05d4
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                r0 = 0
                r14.m23904((boolean) r0)
                org2.jsoup.parser.HtmlTreeBuilderState r0 = r14.m23878()
                org2.jsoup.parser.HtmlTreeBuilderState r1 = InTable
                boolean r1 = r0.equals(r1)
                if (r1 != 0) goto L_0x05c6
                org2.jsoup.parser.HtmlTreeBuilderState r1 = InCaption
                boolean r1 = r0.equals(r1)
                if (r1 != 0) goto L_0x05c6
                org2.jsoup.parser.HtmlTreeBuilderState r1 = InTableBody
                boolean r1 = r0.equals(r1)
                if (r1 != 0) goto L_0x05c6
                org2.jsoup.parser.HtmlTreeBuilderState r1 = InRow
                boolean r1 = r0.equals(r1)
                if (r1 != 0) goto L_0x05c6
                org2.jsoup.parser.HtmlTreeBuilderState r1 = InCell
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x05cd
            L_0x05c6:
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InSelectInTable
                r14.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x05cd:
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InSelect
                r14.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x05d4:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18373
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x05fb
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                java.lang.String r1 = "option"
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x05f3
                java.lang.String r0 = "option"
                r14.m24151(r0)
            L_0x05f3:
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x05fb:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18371
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r1 == 0) goto L_0x062e
                java.lang.String r0 = "ruby"
                boolean r0 = r14.m23874((java.lang.String) r0)
                if (r0 == 0) goto L_0x000d
                r14.m23854()
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                java.lang.String r1 = "ruby"
                boolean r0 = r0.equals(r1)
                if (r0 != 0) goto L_0x0629
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.lang.String r0 = "ruby"
                r14.m23885((java.lang.String) r0)
            L_0x0629:
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x062e:
                java.lang.String r1 = "math"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x063f
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x063f:
                java.lang.String r1 = "svg"
                boolean r1 = r0.equals(r1)
                if (r1 == 0) goto L_0x0650
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x0650:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18372
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r0, r1)
                if (r0 == 0) goto L_0x065e
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x065e:
                r14.m23864()
                r14.m23893((org2.jsoup.parser.Token.StartTag) r2)
                goto L_0x000d
            L_0x0666:
                org2.jsoup.parser.Token$EndTag r0 = r13.m23983()
                java.lang.String r7 = r0.m24014()
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18384
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r7, r1)
                if (r1 == 0) goto L_0x07ac
                r0 = 0
                r6 = r0
            L_0x0678:
                r0 = 8
                if (r6 >= r0) goto L_0x000d
                org2.jsoup.nodes.Element r8 = r14.m23869((java.lang.String) r7)
                if (r8 != 0) goto L_0x0688
                boolean r0 = r12.m23954(r13, r14)
                goto L_0x000e
            L_0x0688:
                boolean r0 = r14.m23886((org2.jsoup.nodes.Element) r8)
                if (r0 != 0) goto L_0x0697
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r14.m23868((org2.jsoup.nodes.Element) r8)
                r0 = 1
                goto L_0x000e
            L_0x0697:
                java.lang.String r0 = r8.m23676()
                boolean r0 = r14.m23874((java.lang.String) r0)
                if (r0 != 0) goto L_0x06a7
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x06a7:
                org2.jsoup.nodes.Element r0 = r14.m24154()
                if (r0 == r8) goto L_0x06b0
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x06b0:
                r5 = 0
                r2 = 0
                r1 = 0
                java.util.ArrayList r4 = r14.m23866()
                int r9 = r4.size()
                r0 = 0
                r3 = r0
            L_0x06bd:
                if (r3 >= r9) goto L_0x06e6
                r0 = 64
                if (r3 >= r0) goto L_0x06e6
                java.lang.Object r0 = r4.get(r3)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                if (r0 != r8) goto L_0x06dd
                int r0 = r3 + -1
                java.lang.Object r0 = r4.get(r0)
                org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
                r1 = 1
                r11 = r1
                r1 = r0
                r0 = r11
            L_0x06d7:
                int r2 = r3 + 1
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x06bd
            L_0x06dd:
                if (r1 == 0) goto L_0x097a
                boolean r10 = r14.m23851((org2.jsoup.nodes.Element) r0)
                if (r10 == 0) goto L_0x097a
                r5 = r0
            L_0x06e6:
                if (r5 != 0) goto L_0x06f5
                java.lang.String r0 = r8.m23676()
                r14.m23888((java.lang.String) r0)
                r14.m23868((org2.jsoup.nodes.Element) r8)
                r0 = 1
                goto L_0x000e
            L_0x06f5:
                r0 = 0
                r4 = r0
                r1 = r5
                r0 = r5
            L_0x06f9:
                r3 = 3
                if (r4 >= r3) goto L_0x0719
                boolean r3 = r14.m23886((org2.jsoup.nodes.Element) r0)
                if (r3 == 0) goto L_0x0706
                org2.jsoup.nodes.Element r0 = r14.m23844((org2.jsoup.nodes.Element) r0)
            L_0x0706:
                boolean r3 = r14.m23871((org2.jsoup.nodes.Element) r0)
                if (r3 != 0) goto L_0x0717
                r14.m23875((org2.jsoup.nodes.Element) r0)
                r3 = r0
                r0 = r1
            L_0x0711:
                int r1 = r4 + 1
                r4 = r1
                r1 = r0
                r0 = r3
                goto L_0x06f9
            L_0x0717:
                if (r0 != r8) goto L_0x0765
            L_0x0719:
                java.lang.String r0 = r2.m23676()
                java.lang.String[] r3 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18374
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r0, r3)
                if (r0 == 0) goto L_0x078e
                org2.jsoup.nodes.Element r0 = r1.m23637()
                if (r0 == 0) goto L_0x072e
                r1.m23740()
            L_0x072e:
                r14.m23900((org2.jsoup.nodes.Node) r1)
            L_0x0731:
                org2.jsoup.nodes.Element r2 = new org2.jsoup.nodes.Element
                org2.jsoup.parser.Tag r0 = r8.m23686()
                java.lang.String r1 = r14.m23846()
                r2.<init>(r0, r1)
                org2.jsoup.nodes.Attributes r0 = r2.m23635()
                org2.jsoup.nodes.Attributes r1 = r8.m23635()
                r0.m23558((org2.jsoup.nodes.Attributes) r1)
                java.util.List r0 = r5.m23751()
                int r1 = r5.m23675()
                org2.jsoup.nodes.Node[] r1 = new org2.jsoup.nodes.Node[r1]
                java.lang.Object[] r0 = r0.toArray(r1)
                org2.jsoup.nodes.Node[] r0 = (org2.jsoup.nodes.Node[]) r0
                int r3 = r0.length
                r1 = 0
            L_0x075b:
                if (r1 >= r3) goto L_0x079b
                r4 = r0[r1]
                r2.m23679((org2.jsoup.nodes.Node) r4)
                int r1 = r1 + 1
                goto L_0x075b
            L_0x0765:
                org2.jsoup.nodes.Element r3 = new org2.jsoup.nodes.Element
                java.lang.String r9 = r0.m23676()
                org2.jsoup.parser.ParseSettings r10 = org2.jsoup.parser.ParseSettings.f18387
                org2.jsoup.parser.Tag r9 = org2.jsoup.parser.Tag.m23969(r9, r10)
                java.lang.String r10 = r14.m23846()
                r3.<init>(r9, r10)
                r14.m23890(r0, r3)
                r14.m23880(r0, r3)
                if (r1 != r5) goto L_0x0780
            L_0x0780:
                org2.jsoup.nodes.Element r0 = r1.m23637()
                if (r0 == 0) goto L_0x0789
                r1.m23740()
            L_0x0789:
                r3.m23679((org2.jsoup.nodes.Node) r1)
                r0 = r3
                goto L_0x0711
            L_0x078e:
                org2.jsoup.nodes.Element r0 = r1.m23637()
                if (r0 == 0) goto L_0x0797
                r1.m23740()
            L_0x0797:
                r2.m23679((org2.jsoup.nodes.Node) r1)
                goto L_0x0731
            L_0x079b:
                r5.m23679((org2.jsoup.nodes.Node) r2)
                r14.m23868((org2.jsoup.nodes.Element) r8)
                r14.m23875((org2.jsoup.nodes.Element) r8)
                r14.m23898((org2.jsoup.nodes.Element) r5, (org2.jsoup.nodes.Element) r2)
                int r0 = r6 + 1
                r6 = r0
                goto L_0x0678
            L_0x07ac:
                java.lang.String[] r1 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18383
                boolean r1 = org2.jsoup.helper.StringUtil.m23499(r7, r1)
                if (r1 == 0) goto L_0x07d9
                boolean r0 = r14.m23874((java.lang.String) r7)
                if (r0 != 0) goto L_0x07c0
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x07c0:
                r14.m23854()
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                boolean r0 = r0.equals(r7)
                if (r0 != 0) goto L_0x07d4
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x07d4:
                r14.m23888((java.lang.String) r7)
                goto L_0x000d
            L_0x07d9:
                java.lang.String r1 = "span"
                boolean r1 = r7.equals(r1)
                if (r1 == 0) goto L_0x07e8
                boolean r0 = r12.m23954(r13, r14)
                goto L_0x000e
            L_0x07e8:
                java.lang.String r1 = "li"
                boolean r1 = r7.equals(r1)
                if (r1 == 0) goto L_0x0816
                boolean r0 = r14.m23845((java.lang.String) r7)
                if (r0 != 0) goto L_0x07fd
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x07fd:
                r14.m23867((java.lang.String) r7)
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                boolean r0 = r0.equals(r7)
                if (r0 != 0) goto L_0x0811
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x0811:
                r14.m23888((java.lang.String) r7)
                goto L_0x000d
            L_0x0816:
                java.lang.String r1 = "body"
                boolean r1 = r7.equals(r1)
                if (r1 == 0) goto L_0x0835
                java.lang.String r0 = "body"
                boolean r0 = r14.m23874((java.lang.String) r0)
                if (r0 != 0) goto L_0x082e
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x082e:
                org2.jsoup.parser.HtmlTreeBuilderState r0 = AfterBody
                r14.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000d
            L_0x0835:
                java.lang.String r1 = "html"
                boolean r1 = r7.equals(r1)
                if (r1 == 0) goto L_0x084d
                java.lang.String r1 = "body"
                boolean r1 = r14.m24151(r1)
                if (r1 == 0) goto L_0x000d
                boolean r0 = r14.m23908((org2.jsoup.parser.Token) r0)
                goto L_0x000e
            L_0x084d:
                java.lang.String r1 = "form"
                boolean r1 = r7.equals(r1)
                if (r1 == 0) goto L_0x0885
                org2.jsoup.nodes.FormElement r0 = r14.m23857()
                r1 = 0
                r14.m23899((org2.jsoup.nodes.FormElement) r1)
                if (r0 == 0) goto L_0x0866
                boolean r1 = r14.m23874((java.lang.String) r7)
                if (r1 != 0) goto L_0x086c
            L_0x0866:
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x086c:
                r14.m23854()
                org2.jsoup.nodes.Element r1 = r14.m24154()
                java.lang.String r1 = r1.m23676()
                boolean r1 = r1.equals(r7)
                if (r1 != 0) goto L_0x0880
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x0880:
                r14.m23875((org2.jsoup.nodes.Element) r0)
                goto L_0x000d
            L_0x0885:
                java.lang.String r1 = "p"
                boolean r1 = r7.equals(r1)
                if (r1 == 0) goto L_0x08b9
                boolean r1 = r14.m23848((java.lang.String) r7)
                if (r1 != 0) goto L_0x08a0
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r14.m24152(r7)
                boolean r0 = r14.m23908((org2.jsoup.parser.Token) r0)
                goto L_0x000e
            L_0x08a0:
                r14.m23867((java.lang.String) r7)
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                boolean r0 = r0.equals(r7)
                if (r0 != 0) goto L_0x08b4
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x08b4:
                r14.m23888((java.lang.String) r7)
                goto L_0x000d
            L_0x08b9:
                java.lang.String[] r0 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18368
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r7, r0)
                if (r0 == 0) goto L_0x08e6
                boolean r0 = r14.m23874((java.lang.String) r7)
                if (r0 != 0) goto L_0x08cd
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x08cd:
                r14.m23867((java.lang.String) r7)
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                boolean r0 = r0.equals(r7)
                if (r0 != 0) goto L_0x08e1
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x08e1:
                r14.m23888((java.lang.String) r7)
                goto L_0x000d
            L_0x08e6:
                java.lang.String[] r0 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18381
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r7, r0)
                if (r0 == 0) goto L_0x0917
                java.lang.String[] r0 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18381
                boolean r0 = r14.m23883((java.lang.String[]) r0)
                if (r0 != 0) goto L_0x08fc
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x08fc:
                r14.m23867((java.lang.String) r7)
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                boolean r0 = r0.equals(r7)
                if (r0 != 0) goto L_0x0910
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x0910:
                java.lang.String[] r0 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18381
                r14.m23905((java.lang.String[]) r0)
                goto L_0x000d
            L_0x0917:
                java.lang.String r0 = "sarcasm"
                boolean r0 = r7.equals(r0)
                if (r0 == 0) goto L_0x0926
                boolean r0 = r12.m23954(r13, r14)
                goto L_0x000e
            L_0x0926:
                java.lang.String[] r0 = org2.jsoup.parser.HtmlTreeBuilderState.TyphoonApp.f18370
                boolean r0 = org2.jsoup.helper.StringUtil.m23499(r7, r0)
                if (r0 == 0) goto L_0x095f
                java.lang.String r0 = "name"
                boolean r0 = r14.m23874((java.lang.String) r0)
                if (r0 != 0) goto L_0x000d
                boolean r0 = r14.m23874((java.lang.String) r7)
                if (r0 != 0) goto L_0x0943
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                r0 = 0
                goto L_0x000e
            L_0x0943:
                r14.m23854()
                org2.jsoup.nodes.Element r0 = r14.m24154()
                java.lang.String r0 = r0.m23676()
                boolean r0 = r0.equals(r7)
                if (r0 != 0) goto L_0x0957
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
            L_0x0957:
                r14.m23888((java.lang.String) r7)
                r14.m23865()
                goto L_0x000d
            L_0x095f:
                java.lang.String r0 = "br"
                boolean r0 = r7.equals(r0)
                if (r0 == 0) goto L_0x0974
                r14.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r12)
                java.lang.String r0 = "br"
                r14.m24152(r0)
                r0 = 0
                goto L_0x000e
            L_0x0974:
                boolean r0 = r12.m23954(r13, r14)
                goto L_0x000e
            L_0x097a:
                r0 = r1
                r1 = r2
                goto L_0x06d7
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.HtmlTreeBuilderState.AnonymousClass7.m23955(org2.jsoup.parser.Token, org2.jsoup.parser.HtmlTreeBuilder):boolean");
        }
    },
    Text {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23956(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23988()) {
                htmlTreeBuilder.m23902(token.m23985());
            } else if (token.m23984()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                htmlTreeBuilder.m23861();
                htmlTreeBuilder.m23901(htmlTreeBuilder.m23884());
                return htmlTreeBuilder.m23908(token);
            } else if (token.m23982()) {
                htmlTreeBuilder.m23861();
                htmlTreeBuilder.m23901(htmlTreeBuilder.m23884());
            }
            return true;
        }
    },
    InTable {
        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public boolean m23957(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
            if (!StringUtil.m23510(htmlTreeBuilder.m24154().m23676(), "table", "tbody", "tfoot", "thead", "tr")) {
                return htmlTreeBuilder.m23909(token, InBody);
            }
            htmlTreeBuilder.m23882(true);
            boolean r0 = htmlTreeBuilder.m23909(token, InBody);
            htmlTreeBuilder.m23882(false);
            return r0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23958(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23988()) {
                htmlTreeBuilder.m23858();
                htmlTreeBuilder.m23887();
                htmlTreeBuilder.m23901(InTableText);
                return htmlTreeBuilder.m23908(token);
            } else if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
                return true;
            } else if (token.m23992()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return false;
            } else if (token.m23989()) {
                Token.StartTag r2 = token.m23981();
                String r3 = r2.m24014();
                if (r3.equals("caption")) {
                    htmlTreeBuilder.m23870();
                    htmlTreeBuilder.m23872();
                    htmlTreeBuilder.m23893(r2);
                    htmlTreeBuilder.m23901(InCaption);
                    return true;
                } else if (r3.equals("colgroup")) {
                    htmlTreeBuilder.m23870();
                    htmlTreeBuilder.m23893(r2);
                    htmlTreeBuilder.m23901(InColumnGroup);
                    return true;
                } else if (r3.equals("col")) {
                    htmlTreeBuilder.m24152("colgroup");
                    return htmlTreeBuilder.m23908(token);
                } else {
                    if (StringUtil.m23510(r3, "tbody", "tfoot", "thead")) {
                        htmlTreeBuilder.m23870();
                        htmlTreeBuilder.m23893(r2);
                        htmlTreeBuilder.m23901(InTableBody);
                        return true;
                    }
                    if (StringUtil.m23510(r3, "td", "th", "tr")) {
                        htmlTreeBuilder.m24152("tbody");
                        return htmlTreeBuilder.m23908(token);
                    } else if (r3.equals("table")) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        if (htmlTreeBuilder.m24151("table")) {
                            return htmlTreeBuilder.m23908(token);
                        }
                        return true;
                    } else {
                        if (StringUtil.m23510(r3, TtmlNode.TAG_STYLE, "script")) {
                            return htmlTreeBuilder.m23909(token, InHead);
                        }
                        if (r3.equals("input")) {
                            if (!r2.f18426.m23552(VastExtensionXmlManager.TYPE).equalsIgnoreCase("hidden")) {
                                return m23957(token, htmlTreeBuilder);
                            }
                            htmlTreeBuilder.m23877(r2);
                            return true;
                        } else if (!r3.equals("form")) {
                            return m23957(token, htmlTreeBuilder);
                        } else {
                            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                            if (htmlTreeBuilder.m23857() != null) {
                                return false;
                            }
                            htmlTreeBuilder.m23894(r2, false);
                            return true;
                        }
                    }
                }
            } else if (token.m23982()) {
                String r22 = token.m23983().m24014();
                if (!r22.equals("table")) {
                    if (!StringUtil.m23510(r22, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML, "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        return m23957(token, htmlTreeBuilder);
                    }
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                } else if (!htmlTreeBuilder.m23850(r22)) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    htmlTreeBuilder.m23888("table");
                    htmlTreeBuilder.m23853();
                    return true;
                }
            } else if (!token.m23984()) {
                return m23957(token, htmlTreeBuilder);
            } else {
                if (!htmlTreeBuilder.m24154().m23676().equals(AdType.HTML)) {
                    return true;
                }
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return true;
            }
        }
    },
    InTableText {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23923(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            switch (AnonymousClass24.f18367[token.f18412.ordinal()]) {
                case 5:
                    Token.Character r0 = token.m23985();
                    if (r0.m23994().equals(HtmlTreeBuilderState.f18356)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                    htmlTreeBuilder.m23859().add(r0.m23994());
                    return true;
                default:
                    if (htmlTreeBuilder.m23859().size() > 0) {
                        for (String next : htmlTreeBuilder.m23859()) {
                            if (!HtmlTreeBuilderState.m23913(next)) {
                                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                                if (StringUtil.m23510(htmlTreeBuilder.m24154().m23676(), "table", "tbody", "tfoot", "thead", "tr")) {
                                    htmlTreeBuilder.m23882(true);
                                    htmlTreeBuilder.m23909((Token) new Token.Character().m23996(next), InBody);
                                    htmlTreeBuilder.m23882(false);
                                } else {
                                    htmlTreeBuilder.m23909((Token) new Token.Character().m23996(next), InBody);
                                }
                            } else {
                                htmlTreeBuilder.m23902(new Token.Character().m23996(next));
                            }
                        }
                        htmlTreeBuilder.m23858();
                    }
                    htmlTreeBuilder.m23901(htmlTreeBuilder.m23884());
                    return htmlTreeBuilder.m23908(token);
            }
        }
    },
    InCaption {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x009d, code lost:
            if (org2.jsoup.helper.StringUtil.m23510(r10.m23981().m24014(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr") == false) goto L_0x009f;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m23924(org2.jsoup.parser.Token r10, org2.jsoup.parser.HtmlTreeBuilder r11) {
            /*
                r9 = this;
                r8 = 4
                r7 = 3
                r6 = 2
                r1 = 1
                r0 = 0
                boolean r2 = r10.m23982()
                if (r2 == 0) goto L_0x0055
                org2.jsoup.parser.Token$EndTag r2 = r10.m23983()
                java.lang.String r2 = r2.m24014()
                java.lang.String r3 = "caption"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x0055
                org2.jsoup.parser.Token$EndTag r2 = r10.m23983()
                java.lang.String r2 = r2.m24014()
                boolean r2 = r11.m23850((java.lang.String) r2)
                if (r2 != 0) goto L_0x002e
                r11.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r9)
            L_0x002d:
                return r0
            L_0x002e:
                r11.m23854()
                org2.jsoup.nodes.Element r0 = r11.m24154()
                java.lang.String r0 = r0.m23676()
                java.lang.String r2 = "caption"
                boolean r0 = r0.equals(r2)
                if (r0 != 0) goto L_0x0045
                r11.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r9)
            L_0x0045:
                java.lang.String r0 = "caption"
                r11.m23888((java.lang.String) r0)
                r11.m23865()
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InTable
                r11.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r0)
            L_0x0053:
                r0 = r1
                goto L_0x002d
            L_0x0055:
                boolean r2 = r10.m23989()
                if (r2 == 0) goto L_0x009f
                org2.jsoup.parser.Token$StartTag r2 = r10.m23981()
                java.lang.String r2 = r2.m24014()
                r3 = 9
                java.lang.String[] r3 = new java.lang.String[r3]
                java.lang.String r4 = "caption"
                r3[r0] = r4
                java.lang.String r4 = "col"
                r3[r1] = r4
                java.lang.String r4 = "colgroup"
                r3[r6] = r4
                java.lang.String r4 = "tbody"
                r3[r7] = r4
                java.lang.String r4 = "td"
                r3[r8] = r4
                r4 = 5
                java.lang.String r5 = "tfoot"
                r3[r4] = r5
                r4 = 6
                java.lang.String r5 = "th"
                r3[r4] = r5
                r4 = 7
                java.lang.String r5 = "thead"
                r3[r4] = r5
                r4 = 8
                java.lang.String r5 = "tr"
                r3[r4] = r5
                boolean r2 = org2.jsoup.helper.StringUtil.m23510((java.lang.String) r2, (java.lang.String[]) r3)
                if (r2 != 0) goto L_0x00b6
            L_0x009f:
                boolean r2 = r10.m23982()
                if (r2 == 0) goto L_0x00c8
                org2.jsoup.parser.Token$EndTag r2 = r10.m23983()
                java.lang.String r2 = r2.m24014()
                java.lang.String r3 = "table"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x00c8
            L_0x00b6:
                r11.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r9)
                java.lang.String r0 = "caption"
                boolean r0 = r11.m24151(r0)
                if (r0 == 0) goto L_0x0053
                boolean r0 = r11.m23908((org2.jsoup.parser.Token) r10)
                goto L_0x002d
            L_0x00c8:
                boolean r2 = r10.m23982()
                if (r2 == 0) goto L_0x011e
                org2.jsoup.parser.Token$EndTag r2 = r10.m23983()
                java.lang.String r2 = r2.m24014()
                r3 = 10
                java.lang.String[] r3 = new java.lang.String[r3]
                java.lang.String r4 = "body"
                r3[r0] = r4
                java.lang.String r4 = "col"
                r3[r1] = r4
                java.lang.String r1 = "colgroup"
                r3[r6] = r1
                java.lang.String r1 = "html"
                r3[r7] = r1
                java.lang.String r1 = "tbody"
                r3[r8] = r1
                r1 = 5
                java.lang.String r4 = "td"
                r3[r1] = r4
                r1 = 6
                java.lang.String r4 = "tfoot"
                r3[r1] = r4
                r1 = 7
                java.lang.String r4 = "th"
                r3[r1] = r4
                r1 = 8
                java.lang.String r4 = "thead"
                r3[r1] = r4
                r1 = 9
                java.lang.String r4 = "tr"
                r3[r1] = r4
                boolean r1 = org2.jsoup.helper.StringUtil.m23510((java.lang.String) r2, (java.lang.String[]) r3)
                if (r1 == 0) goto L_0x011e
                r11.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r9)
                goto L_0x002d
            L_0x011e:
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InBody
                boolean r0 = r11.m23909((org2.jsoup.parser.Token) r10, (org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.HtmlTreeBuilderState.AnonymousClass11.m23924(org2.jsoup.parser.Token, org2.jsoup.parser.HtmlTreeBuilder):boolean");
        }
    },
    InColumnGroup {
        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m23925(Token token, TreeBuilder treeBuilder) {
            if (treeBuilder.m24151("colgroup")) {
                return treeBuilder.m24159(token);
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x004e, code lost:
            if (r4.equals(com.mopub.common.AdType.HTML) != false) goto L_0x003f;
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m23926(org2.jsoup.parser.Token r7, org2.jsoup.parser.HtmlTreeBuilder r8) {
            /*
                r6 = this;
                r1 = 0
                r0 = 1
                boolean r2 = org2.jsoup.parser.HtmlTreeBuilderState.m23914((org2.jsoup.parser.Token) r7)
                if (r2 == 0) goto L_0x0010
                org2.jsoup.parser.Token$Character r1 = r7.m23985()
                r8.m23902((org2.jsoup.parser.Token.Character) r1)
            L_0x000f:
                return r0
            L_0x0010:
                int[] r2 = org2.jsoup.parser.HtmlTreeBuilderState.AnonymousClass24.f18367
                org2.jsoup.parser.Token$TokenType r3 = r7.f18412
                int r3 = r3.ordinal()
                r2 = r2[r3]
                switch(r2) {
                    case 1: goto L_0x0022;
                    case 2: goto L_0x002a;
                    case 3: goto L_0x002e;
                    case 4: goto L_0x0067;
                    case 5: goto L_0x001d;
                    case 6: goto L_0x009c;
                    default: goto L_0x001d;
                }
            L_0x001d:
                boolean r0 = r6.m23925((org2.jsoup.parser.Token) r7, (org2.jsoup.parser.TreeBuilder) r8)
                goto L_0x000f
            L_0x0022:
                org2.jsoup.parser.Token$Comment r1 = r7.m23987()
                r8.m23903((org2.jsoup.parser.Token.Comment) r1)
                goto L_0x000f
            L_0x002a:
                r8.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r6)
                goto L_0x000f
            L_0x002e:
                org2.jsoup.parser.Token$StartTag r3 = r7.m23981()
                java.lang.String r4 = r3.m24014()
                r2 = -1
                int r5 = r4.hashCode()
                switch(r5) {
                    case 98688: goto L_0x0051;
                    case 3213227: goto L_0x0047;
                    default: goto L_0x003e;
                }
            L_0x003e:
                r1 = r2
            L_0x003f:
                switch(r1) {
                    case 0: goto L_0x005c;
                    case 1: goto L_0x0063;
                    default: goto L_0x0042;
                }
            L_0x0042:
                boolean r0 = r6.m23925((org2.jsoup.parser.Token) r7, (org2.jsoup.parser.TreeBuilder) r8)
                goto L_0x000f
            L_0x0047:
                java.lang.String r5 = "html"
                boolean r4 = r4.equals(r5)
                if (r4 == 0) goto L_0x003e
                goto L_0x003f
            L_0x0051:
                java.lang.String r1 = "col"
                boolean r1 = r4.equals(r1)
                if (r1 == 0) goto L_0x003e
                r1 = r0
                goto L_0x003f
            L_0x005c:
                org2.jsoup.parser.HtmlTreeBuilderState r0 = InBody
                boolean r0 = r8.m23909((org2.jsoup.parser.Token) r7, (org2.jsoup.parser.HtmlTreeBuilderState) r0)
                goto L_0x000f
            L_0x0063:
                r8.m23877((org2.jsoup.parser.Token.StartTag) r3)
                goto L_0x000f
            L_0x0067:
                org2.jsoup.parser.Token$EndTag r2 = r7.m23983()
                java.lang.String r2 = r2.f18429
                java.lang.String r3 = "colgroup"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x0096
                org2.jsoup.nodes.Element r2 = r8.m24154()
                java.lang.String r2 = r2.m23676()
                java.lang.String r3 = "html"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x008c
                r8.m23881((org2.jsoup.parser.HtmlTreeBuilderState) r6)
                r0 = r1
                goto L_0x000f
            L_0x008c:
                r8.m23861()
                org2.jsoup.parser.HtmlTreeBuilderState r1 = InTable
                r8.m23901((org2.jsoup.parser.HtmlTreeBuilderState) r1)
                goto L_0x000f
            L_0x0096:
                boolean r0 = r6.m23925((org2.jsoup.parser.Token) r7, (org2.jsoup.parser.TreeBuilder) r8)
                goto L_0x000f
            L_0x009c:
                org2.jsoup.nodes.Element r1 = r8.m24154()
                java.lang.String r1 = r1.m23676()
                java.lang.String r2 = "html"
                boolean r1 = r1.equals(r2)
                if (r1 != 0) goto L_0x000f
                boolean r0 = r6.m23925((org2.jsoup.parser.Token) r7, (org2.jsoup.parser.TreeBuilder) r8)
                goto L_0x000f
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.HtmlTreeBuilderState.AnonymousClass12.m23926(org2.jsoup.parser.Token, org2.jsoup.parser.HtmlTreeBuilder):boolean");
        }
    },
    InTableBody {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m23927(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (htmlTreeBuilder.m23850("tbody") || htmlTreeBuilder.m23850("thead") || htmlTreeBuilder.m23874("tfoot")) {
                htmlTreeBuilder.m23855();
                htmlTreeBuilder.m24151(htmlTreeBuilder.m24154().m23676());
                return htmlTreeBuilder.m23908(token);
            }
            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
            return false;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        private boolean m23928(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return htmlTreeBuilder.m23909(token, InTable);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23929(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            switch (AnonymousClass24.f18367[token.f18412.ordinal()]) {
                case 3:
                    Token.StartTag r2 = token.m23981();
                    String r3 = r2.m24014();
                    if (r3.equals("template")) {
                        htmlTreeBuilder.m23893(r2);
                        break;
                    } else if (r3.equals("tr")) {
                        htmlTreeBuilder.m23855();
                        htmlTreeBuilder.m23893(r2);
                        htmlTreeBuilder.m23901(InRow);
                        break;
                    } else {
                        if (StringUtil.m23510(r3, "th", "td")) {
                            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                            htmlTreeBuilder.m24152("tr");
                            return htmlTreeBuilder.m23908((Token) r2);
                        }
                        return StringUtil.m23510(r3, "caption", "col", "colgroup", "tbody", "tfoot", "thead") ? m23927(token, htmlTreeBuilder) : m23928(token, htmlTreeBuilder);
                    }
                case 4:
                    String r22 = token.m23983().m24014();
                    if (StringUtil.m23510(r22, "tbody", "tfoot", "thead")) {
                        if (htmlTreeBuilder.m23850(r22)) {
                            htmlTreeBuilder.m23855();
                            htmlTreeBuilder.m23861();
                            htmlTreeBuilder.m23901(InTable);
                            break;
                        } else {
                            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                            return false;
                        }
                    } else if (r22.equals("table")) {
                        return m23927(token, htmlTreeBuilder);
                    } else {
                        if (!StringUtil.m23510(r22, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML, "td", "th", "tr")) {
                            return m23928(token, htmlTreeBuilder);
                        }
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                default:
                    return m23928(token, htmlTreeBuilder);
            }
            return true;
        }
    },
    InRow {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m23930(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return htmlTreeBuilder.m23909(token, InTable);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private boolean m23931(Token token, TreeBuilder treeBuilder) {
            if (treeBuilder.m24151("tr")) {
                return treeBuilder.m24159(token);
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23932(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23989()) {
                Token.StartTag r2 = token.m23981();
                String r3 = r2.m24014();
                if (r3.equals("template")) {
                    htmlTreeBuilder.m23893(r2);
                } else {
                    if (StringUtil.m23510(r3, "th", "td")) {
                        htmlTreeBuilder.m23852();
                        htmlTreeBuilder.m23893(r2);
                        htmlTreeBuilder.m23901(InCell);
                        htmlTreeBuilder.m23872();
                    } else {
                        return StringUtil.m23510(r3, "caption", "col", "colgroup", "tbody", "tfoot", "thead", "tr") ? m23931(token, (TreeBuilder) htmlTreeBuilder) : m23930(token, htmlTreeBuilder);
                    }
                }
            } else if (!token.m23982()) {
                return m23930(token, htmlTreeBuilder);
            } else {
                String r22 = token.m23983().m24014();
                if (r22.equals("tr")) {
                    if (!htmlTreeBuilder.m23850(r22)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                    htmlTreeBuilder.m23852();
                    htmlTreeBuilder.m23861();
                    htmlTreeBuilder.m23901(InTableBody);
                } else if (r22.equals("table")) {
                    return m23931(token, (TreeBuilder) htmlTreeBuilder);
                } else {
                    if (!StringUtil.m23510(r22, "tbody", "tfoot", "thead")) {
                        if (!StringUtil.m23510(r22, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML, "td", "th")) {
                            return m23930(token, htmlTreeBuilder);
                        }
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    } else if (!htmlTreeBuilder.m23850(r22)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    } else {
                        htmlTreeBuilder.m24151("tr");
                        return htmlTreeBuilder.m23908(token);
                    }
                }
            }
            return true;
        }
    },
    InCell {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m23933(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return htmlTreeBuilder.m23909(token, InBody);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        private void m23934(HtmlTreeBuilder htmlTreeBuilder) {
            if (htmlTreeBuilder.m23850("td")) {
                htmlTreeBuilder.m24151("td");
            } else {
                htmlTreeBuilder.m24151("th");
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23935(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23982()) {
                String r2 = token.m23983().m24014();
                if (!StringUtil.m23510(r2, "td", "th")) {
                    if (StringUtil.m23510(r2, TtmlNode.TAG_BODY, "caption", "col", "colgroup", AdType.HTML)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                    if (!StringUtil.m23510(r2, "table", "tbody", "tfoot", "thead", "tr")) {
                        return m23933(token, htmlTreeBuilder);
                    }
                    if (!htmlTreeBuilder.m23850(r2)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                    m23934(htmlTreeBuilder);
                    return htmlTreeBuilder.m23908(token);
                } else if (!htmlTreeBuilder.m23850(r2)) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    htmlTreeBuilder.m23901(InRow);
                    return false;
                } else {
                    htmlTreeBuilder.m23854();
                    if (!htmlTreeBuilder.m24154().m23676().equals(r2)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    }
                    htmlTreeBuilder.m23888(r2);
                    htmlTreeBuilder.m23865();
                    htmlTreeBuilder.m23901(InRow);
                    return true;
                }
            } else {
                if (token.m23989()) {
                    if (StringUtil.m23510(token.m23981().m24014(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        if (htmlTreeBuilder.m23850("td") || htmlTreeBuilder.m23850("th")) {
                            m23934(htmlTreeBuilder);
                            return htmlTreeBuilder.m23908(token);
                        }
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                }
                return m23933(token, htmlTreeBuilder);
            }
        }
    },
    InSelect {
        /* renamed from: 靐  reason: contains not printable characters */
        private boolean m23936(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23937(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            switch (AnonymousClass24.f18367[token.f18412.ordinal()]) {
                case 1:
                    htmlTreeBuilder.m23903(token.m23987());
                    break;
                case 2:
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                case 3:
                    Token.StartTag r2 = token.m23981();
                    String r4 = r2.m24014();
                    if (r4.equals(AdType.HTML)) {
                        return htmlTreeBuilder.m23909((Token) r2, InBody);
                    }
                    if (r4.equals("option")) {
                        if (htmlTreeBuilder.m24154().m23676().equals("option")) {
                            htmlTreeBuilder.m24151("option");
                        }
                        htmlTreeBuilder.m23893(r2);
                        break;
                    } else if (r4.equals("optgroup")) {
                        if (htmlTreeBuilder.m24154().m23676().equals("option")) {
                            htmlTreeBuilder.m24151("option");
                        } else if (htmlTreeBuilder.m24154().m23676().equals("optgroup")) {
                            htmlTreeBuilder.m24151("optgroup");
                        }
                        htmlTreeBuilder.m23893(r2);
                        break;
                    } else if (r4.equals("select")) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return htmlTreeBuilder.m24151("select");
                    } else {
                        if (!StringUtil.m23510(r4, "input", "keygen", "textarea")) {
                            return r4.equals("script") ? htmlTreeBuilder.m23909(token, InHead) : m23936(token, htmlTreeBuilder);
                        }
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        if (!htmlTreeBuilder.m23863("select")) {
                            return false;
                        }
                        htmlTreeBuilder.m24151("select");
                        return htmlTreeBuilder.m23908((Token) r2);
                    }
                case 4:
                    String r42 = token.m23983().m24014();
                    char c = 65535;
                    switch (r42.hashCode()) {
                        case -1010136971:
                            if (r42.equals("option")) {
                                c = 1;
                                break;
                            }
                            break;
                        case -906021636:
                            if (r42.equals("select")) {
                                c = 2;
                                break;
                            }
                            break;
                        case -80773204:
                            if (r42.equals("optgroup")) {
                                c = 0;
                                break;
                            }
                            break;
                    }
                    switch (c) {
                        case 0:
                            if (htmlTreeBuilder.m24154().m23676().equals("option") && htmlTreeBuilder.m23844(htmlTreeBuilder.m24154()) != null && htmlTreeBuilder.m23844(htmlTreeBuilder.m24154()).m23676().equals("optgroup")) {
                                htmlTreeBuilder.m24151("option");
                            }
                            if (!htmlTreeBuilder.m24154().m23676().equals("optgroup")) {
                                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                                break;
                            } else {
                                htmlTreeBuilder.m23861();
                                break;
                            }
                        case 1:
                            if (!htmlTreeBuilder.m24154().m23676().equals("option")) {
                                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                                break;
                            } else {
                                htmlTreeBuilder.m23861();
                                break;
                            }
                        case 2:
                            if (htmlTreeBuilder.m23863(r42)) {
                                htmlTreeBuilder.m23888(r42);
                                htmlTreeBuilder.m23853();
                                break;
                            } else {
                                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                                return false;
                            }
                        default:
                            return m23936(token, htmlTreeBuilder);
                    }
                case 5:
                    Token.Character r22 = token.m23985();
                    if (!r22.m23994().equals(HtmlTreeBuilderState.f18356)) {
                        htmlTreeBuilder.m23902(r22);
                        break;
                    } else {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                case 6:
                    if (!htmlTreeBuilder.m24154().m23676().equals(AdType.HTML)) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        break;
                    }
                    break;
                default:
                    return m23936(token, htmlTreeBuilder);
            }
            return true;
        }
    },
    InSelectInTable {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23938(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23989()) {
                if (StringUtil.m23510(token.m23981().m24014(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    htmlTreeBuilder.m24151("select");
                    return htmlTreeBuilder.m23908(token);
                }
            }
            if (token.m23982()) {
                if (StringUtil.m23510(token.m23983().m24014(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    if (!htmlTreeBuilder.m23850(token.m23983().m24014())) {
                        return false;
                    }
                    htmlTreeBuilder.m24151("select");
                    return htmlTreeBuilder.m23908(token);
                }
            }
            return htmlTreeBuilder.m23909(token, InSelect);
        }
    },
    AfterBody {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23939(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.m23914(token)) {
                return htmlTreeBuilder.m23909(token, InBody);
            }
            if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
            } else if (token.m23992()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return false;
            } else if (token.m23989() && token.m23981().m24014().equals(AdType.HTML)) {
                return htmlTreeBuilder.m23909(token, InBody);
            } else {
                if (!token.m23982() || !token.m23983().m24014().equals(AdType.HTML)) {
                    if (!token.m23984()) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        htmlTreeBuilder.m23901(InBody);
                        return htmlTreeBuilder.m23908(token);
                    }
                } else if (htmlTreeBuilder.m23849()) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    htmlTreeBuilder.m23901(AfterAfterBody);
                }
            }
            return true;
        }
    },
    InFrameset {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23940(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.m23914(token)) {
                htmlTreeBuilder.m23902(token.m23985());
            } else if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
            } else if (token.m23992()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return false;
            } else if (token.m23989()) {
                Token.StartTag r3 = token.m23981();
                String r4 = r3.m24014();
                char c = 65535;
                switch (r4.hashCode()) {
                    case -1644953643:
                        if (r4.equals("frameset")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 3213227:
                        if (r4.equals(AdType.HTML)) {
                            c = 0;
                            break;
                        }
                        break;
                    case 97692013:
                        if (r4.equals("frame")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1192721831:
                        if (r4.equals("noframes")) {
                            c = 3;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        return htmlTreeBuilder.m23909((Token) r3, InBody);
                    case 1:
                        htmlTreeBuilder.m23893(r3);
                        break;
                    case 2:
                        htmlTreeBuilder.m23877(r3);
                        break;
                    case 3:
                        return htmlTreeBuilder.m23909((Token) r3, InHead);
                    default:
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                }
            } else if (!token.m23982() || !token.m23983().m24014().equals("frameset")) {
                if (!token.m23984()) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                } else if (!htmlTreeBuilder.m24154().m23676().equals(AdType.HTML)) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return true;
                }
            } else if (htmlTreeBuilder.m24154().m23676().equals(AdType.HTML)) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return false;
            } else {
                htmlTreeBuilder.m23861();
                if (!htmlTreeBuilder.m23849() && !htmlTreeBuilder.m24154().m23676().equals("frameset")) {
                    htmlTreeBuilder.m23901(AfterFrameset);
                }
            }
            return true;
        }
    },
    AfterFrameset {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23943(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.m23914(token)) {
                htmlTreeBuilder.m23902(token.m23985());
            } else if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
            } else if (token.m23992()) {
                htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                return false;
            } else if (token.m23989() && token.m23981().m24014().equals(AdType.HTML)) {
                return htmlTreeBuilder.m23909(token, InBody);
            } else {
                if (token.m23982() && token.m23983().m24014().equals(AdType.HTML)) {
                    htmlTreeBuilder.m23901(AfterAfterFrameset);
                } else if (token.m23989() && token.m23981().m24014().equals("noframes")) {
                    return htmlTreeBuilder.m23909(token, InHead);
                } else {
                    if (!token.m23984()) {
                        htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                        return false;
                    }
                }
            }
            return true;
        }
    },
    AfterAfterBody {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23944(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
            } else if (token.m23992() || HtmlTreeBuilderState.m23914(token) || (token.m23989() && token.m23981().m24014().equals(AdType.HTML))) {
                return htmlTreeBuilder.m23909(token, InBody);
            } else {
                if (!token.m23984()) {
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    htmlTreeBuilder.m23901(InBody);
                    return htmlTreeBuilder.m23908(token);
                }
            }
            return true;
        }
    },
    AfterAfterFrameset {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23945(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.m23986()) {
                htmlTreeBuilder.m23903(token.m23987());
            } else if (token.m23992() || HtmlTreeBuilderState.m23914(token) || (token.m23989() && token.m23981().m24014().equals(AdType.HTML))) {
                return htmlTreeBuilder.m23909(token, InBody);
            } else {
                if (!token.m23984()) {
                    if (token.m23989() && token.m23981().m24014().equals("noframes")) {
                        return htmlTreeBuilder.m23909(token, InHead);
                    }
                    htmlTreeBuilder.m23881((HtmlTreeBuilderState) this);
                    return false;
                }
            }
            return true;
        }
    },
    ForeignContent {
        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m23946(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return true;
        }
    };
    
    /* access modifiers changed from: private */

    /* renamed from: ـ  reason: contains not printable characters */
    public static String f18356;

    static final class TyphoonApp {

        /* renamed from: ʻ  reason: contains not printable characters */
        static final String[] f18368 = null;

        /* renamed from: ʼ  reason: contains not printable characters */
        static final String[] f18369 = null;

        /* renamed from: ʽ  reason: contains not printable characters */
        static final String[] f18370 = null;

        /* renamed from: ʾ  reason: contains not printable characters */
        static final String[] f18371 = null;

        /* renamed from: ʿ  reason: contains not printable characters */
        static final String[] f18372 = null;

        /* renamed from: ˈ  reason: contains not printable characters */
        static final String[] f18373 = null;

        /* renamed from: ˊ  reason: contains not printable characters */
        static final String[] f18374 = null;

        /* renamed from: ˑ  reason: contains not printable characters */
        static final String[] f18375 = null;

        /* renamed from: ٴ  reason: contains not printable characters */
        static final String[] f18376 = null;

        /* renamed from: ᐧ  reason: contains not printable characters */
        static final String[] f18377 = null;

        /* renamed from: 连任  reason: contains not printable characters */
        static final String[] f18378 = null;

        /* renamed from: 靐  reason: contains not printable characters */
        static final String[] f18379 = null;

        /* renamed from: 麤  reason: contains not printable characters */
        static final String[] f18380 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        static final String[] f18381 = null;

        /* renamed from: 龘  reason: contains not printable characters */
        static final String[] f18382 = null;

        /* renamed from: ﹶ  reason: contains not printable characters */
        static final String[] f18383 = null;

        /* renamed from: ﾞ  reason: contains not printable characters */
        static final String[] f18384 = null;

        static {
            f18382 = new String[]{"base", "basefont", "bgsound", "command", "link", "meta", "noframes", "script", TtmlNode.TAG_STYLE, PubnativeAsset.TITLE};
            f18379 = new String[]{"address", "article", "aside", "blockquote", TtmlNode.CENTER, "details", "dir", TtmlNode.TAG_DIV, "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "menu", "nav", "ol", TtmlNode.TAG_P, "section", "summary", "ul"};
            f18381 = new String[]{"h1", "h2", "h3", "h4", "h5", "h6"};
            f18380 = new String[]{"listing", "pre"};
            f18378 = new String[]{"address", TtmlNode.TAG_DIV, TtmlNode.TAG_P};
            f18368 = new String[]{"dd", "dt"};
            f18369 = new String[]{"b", "big", OAuth.OAUTH_CODE, "em", "font", "i", "s", "small", "strike", "strong", TtmlNode.TAG_TT, "u"};
            f18370 = new String[]{"applet", "marquee", "object"};
            f18375 = new String[]{"area", TtmlNode.TAG_BR, "embed", "img", "keygen", "wbr"};
            f18376 = new String[]{"param", "source", "track"};
            f18377 = new String[]{"action", "name", "prompt"};
            f18373 = new String[]{"optgroup", "option"};
            f18371 = new String[]{"rp", "rt"};
            f18372 = new String[]{"caption", "col", "colgroup", "frame", TtmlNode.TAG_HEAD, "tbody", "td", "tfoot", "th", "thead", "tr"};
            f18383 = new String[]{"address", "article", "aside", "blockquote", "button", TtmlNode.CENTER, "details", "dir", TtmlNode.TAG_DIV, "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "listing", "menu", "nav", "ol", "pre", "section", "summary", "ul"};
            f18384 = new String[]{"a", "b", "big", OAuth.OAUTH_CODE, "em", "font", "i", "nobr", "s", "small", "strike", "strong", TtmlNode.TAG_TT, "u"};
            f18374 = new String[]{"table", "tbody", "tfoot", "thead", "tr"};
        }
    }

    static {
        f18356 = String.valueOf(0);
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m23913(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!StringUtil.m23497((int) str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m23914(Token token) {
        if (token.m23988()) {
            return m23913(token.m23985().m23994());
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: 麤  reason: contains not printable characters */
    public static void m23915(Token.StartTag startTag, HtmlTreeBuilder htmlTreeBuilder) {
        htmlTreeBuilder.f18534.m24064(TokeniserState.Rawtext);
        htmlTreeBuilder.m23887();
        htmlTreeBuilder.m23901(Text);
        htmlTreeBuilder.m23893(startTag);
    }

    /* access modifiers changed from: private */
    /* renamed from: 齉  reason: contains not printable characters */
    public static void m23916(Token.StartTag startTag, HtmlTreeBuilder htmlTreeBuilder) {
        htmlTreeBuilder.f18534.m24064(TokeniserState.Rcdata);
        htmlTreeBuilder.m23887();
        htmlTreeBuilder.m23901(Text);
        htmlTreeBuilder.m23893(startTag);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m23921(Token token, HtmlTreeBuilder htmlTreeBuilder);
}
