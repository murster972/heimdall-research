package org2.jsoup.parser;

public class ParseError {

    /* renamed from: 靐  reason: contains not printable characters */
    private String f18385;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f18386;

    ParseError(int i, String str) {
        this.f18386 = i;
        this.f18385 = str;
    }

    ParseError(int i, String str, Object... objArr) {
        this.f18385 = String.format(str, objArr);
        this.f18386 = i;
    }

    public String toString() {
        return this.f18386 + ": " + this.f18385;
    }
}
