package org2.jsoup.parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Locale;
import org2.jsoup.UncheckedIOException;
import org2.jsoup.helper.Validate;

public final class CharacterReader {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f18313;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f18314;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String[] f18315;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f18316;

    /* renamed from: 靐  reason: contains not printable characters */
    private final Reader f18317;

    /* renamed from: 麤  reason: contains not printable characters */
    private int f18318;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f18319;

    /* renamed from: 龘  reason: contains not printable characters */
    private final char[] f18320;

    public CharacterReader(Reader reader) {
        this(reader, 32768);
    }

    public CharacterReader(Reader reader, int i) {
        this.f18315 = new String[512];
        Validate.m23515((Object) reader);
        Validate.m23519(reader.markSupported());
        this.f18317 = reader;
        this.f18320 = new char[(i > 32768 ? 32768 : i)];
        m23802();
    }

    public CharacterReader(String str) {
        this(new StringReader(str), str.length());
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m23802() {
        int i = 24576;
        if (this.f18316 >= this.f18318) {
            try {
                this.f18313 += this.f18316;
                this.f18317.skip((long) this.f18316);
                this.f18317.mark(32768);
                this.f18319 = this.f18317.read(this.f18320);
                this.f18317.reset();
                this.f18316 = 0;
                this.f18314 = 0;
                if (this.f18319 <= 24576) {
                    i = this.f18319;
                }
                this.f18318 = i;
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m23803(char[] cArr, String[] strArr, int i, int i2) {
        int i3 = 0;
        if (i2 > 12) {
            return new String(cArr, i, i2);
        }
        int i4 = i;
        int i5 = 0;
        while (i3 < i2) {
            i3++;
            i5 = cArr[i4] + (i5 * 31);
            i4++;
        }
        int length = i5 & (strArr.length - 1);
        String str = strArr[length];
        if (str == null) {
            String str2 = new String(cArr, i, i2);
            strArr[length] = str2;
            return str2;
        } else if (m23804(cArr, i, i2, str)) {
            return str;
        } else {
            String str3 = new String(cArr, i, i2);
            strArr[length] = str3;
            return str3;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static boolean m23804(char[] cArr, int i, int i2, String str) {
        if (i2 != str.length()) {
            return false;
        }
        int i3 = 0;
        while (true) {
            int i4 = i2 - 1;
            if (i2 == 0) {
                return true;
            }
            int i5 = i + 1;
            int i6 = i3 + 1;
            if (cArr[i] != str.charAt(i3)) {
                return false;
            }
            i3 = i6;
            i = i5;
            i2 = i4;
        }
    }

    public String toString() {
        return new String(this.f18320, this.f18316, this.f18319 - this.f18316);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m23805() {
        this.f18316++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public boolean m23806(String str) {
        return m23831((CharSequence) str.toLowerCase(Locale.ENGLISH)) > -1 || m23831((CharSequence) str.toUpperCase(Locale.ENGLISH)) > -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m23807() {
        this.f18314 = this.f18316;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m23808() {
        this.f18316 = this.f18314;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public String m23809() {
        m23802();
        int i = this.f18316;
        while (this.f18316 < this.f18319 && (((r1 = this.f18320[this.f18316]) >= 'A' && r1 <= 'Z') || ((r1 >= 'a' && r1 <= 'z') || Character.isLetter(r1)))) {
            this.f18316++;
        }
        while (!m23820() && (r1 = this.f18320[this.f18316]) >= '0' && r1 <= '9') {
            this.f18316++;
        }
        return m23803(this.f18320, this.f18315, i, this.f18316 - i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʿ  reason: contains not printable characters */
    public String m23810() {
        m23802();
        int i = this.f18316;
        while (this.f18316 < this.f18319 && (((r1 = this.f18320[this.f18316]) >= '0' && r1 <= '9') || ((r1 >= 'A' && r1 <= 'F') || (r1 >= 'a' && r1 <= 'f')))) {
            this.f18316++;
        }
        return m23803(this.f18320, this.f18315, i, this.f18316 - i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public String m23811() {
        m23802();
        int i = this.f18316;
        while (this.f18316 < this.f18319 && (((r1 = this.f18320[this.f18316]) >= 'A' && r1 <= 'Z') || ((r1 >= 'a' && r1 <= 'z') || Character.isLetter(r1)))) {
            this.f18316++;
        }
        return m23803(this.f18320, this.f18315, i, this.f18316 - i);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0008, code lost:
        r1 = r3.f18320[r3.f18316];
     */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m23812() {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r3.m23820()
            if (r1 == 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            char[] r1 = r3.f18320
            int r2 = r3.f18316
            char r1 = r1[r2]
            r2 = 48
            if (r1 < r2) goto L_0x0007
            r2 = 57
            if (r1 > r2) goto L_0x0007
            r0 = 1
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.CharacterReader.m23812():boolean");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public String m23813() {
        m23802();
        int i = this.f18316;
        int i2 = this.f18319;
        char[] cArr = this.f18320;
        while (this.f18316 < i2 && (r3 = cArr[this.f18316]) != '&' && r3 != '<' && r3 != 0) {
            this.f18316++;
        }
        return this.f18316 > i ? m23803(this.f18320, this.f18315, i, this.f18316 - i) : "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public String m23814() {
        m23802();
        int i = this.f18316;
        int i2 = this.f18319;
        char[] cArr = this.f18320;
        while (this.f18316 < i2 && (r3 = cArr[this.f18316]) != 9 && r3 != 10 && r3 != 13 && r3 != 12 && r3 != ' ' && r3 != '/' && r3 != '>' && r3 != 0) {
            this.f18316++;
        }
        return this.f18316 > i ? m23803(this.f18320, this.f18315, i, this.f18316 - i) : "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public String m23815() {
        m23802();
        String r0 = m23803(this.f18320, this.f18315, this.f18316, this.f18319 - this.f18316);
        this.f18316 = this.f18319;
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m23816() {
        this.f18316--;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m23817(String str) {
        if (!m23827(str)) {
            return false;
        }
        this.f18316 += str.length();
        return true;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m23818(char c) {
        int r1 = m23830(c);
        if (r1 == -1) {
            return m23815();
        }
        String r0 = m23803(this.f18320, this.f18315, this.f18316, r1);
        this.f18316 = r1 + this.f18316;
        return r0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public String m23819(char... cArr) {
        m23802();
        int i = this.f18316;
        int i2 = this.f18319;
        char[] cArr2 = this.f18320;
        while (this.f18316 < i2 && Arrays.binarySearch(cArr, cArr2[this.f18316]) < 0) {
            this.f18316++;
        }
        return this.f18316 > i ? m23803(this.f18320, this.f18315, i, this.f18316 - i) : "";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m23820() {
        return this.f18316 >= this.f18319;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m23821(String str) {
        m23802();
        int length = str.length();
        if (length > this.f18319 - this.f18316) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (str.charAt(i) != this.f18320[this.f18316 + i]) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public char m23822() {
        m23802();
        char c = m23820() ? 65535 : this.f18320[this.f18316];
        this.f18316++;
        return c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m23823(String str) {
        m23802();
        if (!m23821(str)) {
            return false;
        }
        this.f18316 += str.length();
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m23824(char[] cArr) {
        m23802();
        return !m23820() && Arrays.binarySearch(cArr, this.f18320[this.f18316]) >= 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public char m23825() {
        m23802();
        if (m23820()) {
            return 65535;
        }
        return this.f18320[this.f18316];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23826(char c) {
        return !m23820() && this.f18320[this.f18316] == c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23827(String str) {
        m23802();
        int length = str.length();
        if (length > this.f18319 - this.f18316) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (Character.toUpperCase(str.charAt(i)) != Character.toUpperCase(this.f18320[this.f18316 + i])) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m23828(char... cArr) {
        if (m23820()) {
            return false;
        }
        m23802();
        char c = this.f18320[this.f18316];
        for (char c2 : cArr) {
            if (c2 == c) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m23829() {
        return this.f18313 + this.f18316;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m23830(char c) {
        m23802();
        for (int i = this.f18316; i < this.f18319; i++) {
            if (c == this.f18320[i]) {
                return i - this.f18316;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m23831(CharSequence charSequence) {
        m23802();
        char charAt = charSequence.charAt(0);
        int i = this.f18316;
        while (i < this.f18319) {
            if (charAt != this.f18320[i]) {
                do {
                    i++;
                    if (i >= this.f18319) {
                        break;
                    }
                } while (charAt == this.f18320[i]);
            }
            int i2 = i + 1;
            int length = (charSequence.length() + i2) - 1;
            if (i < this.f18319 && length <= this.f18319) {
                int i3 = 1;
                while (i2 < length && charSequence.charAt(i3) == this.f18320[i2]) {
                    i2++;
                    i3++;
                }
                if (i2 == length) {
                    return i - this.f18316;
                }
            }
            i++;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m23832(String str) {
        int r1 = m23831((CharSequence) str);
        if (r1 == -1) {
            return m23815();
        }
        String r0 = m23803(this.f18320, this.f18315, this.f18316, r1);
        this.f18316 = r1 + this.f18316;
        return r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public String m23833(char... cArr) {
        m23802();
        int i = this.f18316;
        int i2 = this.f18319;
        char[] cArr2 = this.f18320;
        loop0:
        while (this.f18316 < i2) {
            for (char c : cArr) {
                if (cArr2[this.f18316] == c) {
                    break loop0;
                }
            }
            this.f18316++;
        }
        return this.f18316 > i ? m23803(this.f18320, this.f18315, i, this.f18316 - i) : "";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﹶ  reason: contains not printable characters */
    public String m23834() {
        m23802();
        int i = this.f18316;
        while (this.f18316 < this.f18319 && (r1 = this.f18320[this.f18316]) >= '0' && r1 <= '9') {
            this.f18316++;
        }
        return m23803(this.f18320, this.f18315, i, this.f18316 - i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m23835() {
        if (m23820()) {
            return false;
        }
        char c = this.f18320[this.f18316];
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || Character.isLetter(c);
    }
}
