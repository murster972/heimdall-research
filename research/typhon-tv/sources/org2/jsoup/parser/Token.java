package org2.jsoup.parser;

import org.apache.commons.lang3.StringUtils;
import org2.jsoup.helper.Validate;
import org2.jsoup.internal.Normalizer;
import org2.jsoup.nodes.Attributes;

abstract class Token {

    /* renamed from: 龘  reason: contains not printable characters */
    TokenType f18412;

    static final class Character extends Token {

        /* renamed from: 靐  reason: contains not printable characters */
        private String f18413;

        Character() {
            super();
            this.f18412 = TokenType.Character;
        }

        public String toString() {
            return m23994();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public String m23994() {
            return this.f18413;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m23995() {
            this.f18413 = null;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public Character m23996(String str) {
            this.f18413 = str;
            return this;
        }
    }

    static final class Comment extends Token {

        /* renamed from: 靐  reason: contains not printable characters */
        final StringBuilder f18414 = new StringBuilder();

        /* renamed from: 齉  reason: contains not printable characters */
        boolean f18415 = false;

        Comment() {
            super();
            this.f18412 = TokenType.Comment;
        }

        public String toString() {
            return "<!--" + m23997() + "-->";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public String m23997() {
            return this.f18414.toString();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m23998() {
            m23980(this.f18414);
            this.f18415 = false;
            return this;
        }
    }

    static final class Doctype extends Token {

        /* renamed from: ʻ  reason: contains not printable characters */
        boolean f18416 = false;

        /* renamed from: 连任  reason: contains not printable characters */
        final StringBuilder f18417 = new StringBuilder();

        /* renamed from: 靐  reason: contains not printable characters */
        final StringBuilder f18418 = new StringBuilder();

        /* renamed from: 麤  reason: contains not printable characters */
        final StringBuilder f18419 = new StringBuilder();

        /* renamed from: 齉  reason: contains not printable characters */
        String f18420 = null;

        Doctype() {
            super();
            this.f18412 = TokenType.Doctype;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public String m23999() {
            return this.f18418.toString();
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public String m24000() {
            return this.f18417.toString();
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public boolean m24001() {
            return this.f18416;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m24002() {
            m23980(this.f18418);
            this.f18420 = null;
            m23980(this.f18419);
            m23980(this.f18417);
            this.f18416 = false;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public String m24003() {
            return this.f18420;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﾞ  reason: contains not printable characters */
        public String m24004() {
            return this.f18419.toString();
        }
    }

    static final class EOF extends Token {
        EOF() {
            super();
            this.f18412 = TokenType.EOF;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public Token m24005() {
            return this;
        }
    }

    static final class EndTag extends Tag {
        EndTag() {
            this.f18412 = TokenType.EndTag;
        }

        public String toString() {
            return "</" + m24013() + ">";
        }
    }

    static final class StartTag extends Tag {
        StartTag() {
            this.f18426 = new Attributes();
            this.f18412 = TokenType.StartTag;
        }

        public String toString() {
            return (this.f18426 == null || this.f18426.m23553() <= 0) ? "<" + m24013() + ">" : "<" + m24013() + StringUtils.SPACE + this.f18426.toString() + ">";
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public Tag m24007() {
            super.m24016();
            this.f18426 = new Attributes();
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public StartTag m24008(String str, Attributes attributes) {
            this.f18427 = str;
            this.f18426 = attributes;
            this.f18429 = Normalizer.m23524(this.f18427);
            return this;
        }
    }

    static abstract class Tag extends Token {

        /* renamed from: ʻ  reason: contains not printable characters */
        private String f18421;

        /* renamed from: ʼ  reason: contains not printable characters */
        private StringBuilder f18422 = new StringBuilder();

        /* renamed from: ʽ  reason: contains not printable characters */
        private String f18423;

        /* renamed from: ˑ  reason: contains not printable characters */
        private boolean f18424 = false;

        /* renamed from: ٴ  reason: contains not printable characters */
        private boolean f18425 = false;

        /* renamed from: 连任  reason: contains not printable characters */
        Attributes f18426;

        /* renamed from: 靐  reason: contains not printable characters */
        protected String f18427;

        /* renamed from: 麤  reason: contains not printable characters */
        boolean f18428 = false;

        /* renamed from: 齉  reason: contains not printable characters */
        protected String f18429;

        Tag() {
            super();
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        private void m24009() {
            this.f18425 = true;
            if (this.f18423 != null) {
                this.f18422.append(this.f18423);
                this.f18423 = null;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ʿ  reason: contains not printable characters */
        public Tag m24016() {
            this.f18427 = null;
            this.f18429 = null;
            this.f18421 = null;
            m23980(this.f18422);
            this.f18423 = null;
            this.f18424 = false;
            this.f18425 = false;
            this.f18428 = false;
            this.f18426 = null;
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˆ  reason: contains not printable characters */
        public final Attributes m24011() {
            return this.f18426;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˉ  reason: contains not printable characters */
        public final void m24012() {
            this.f18424 = true;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final String m24013() {
            Validate.m23513(this.f18427 == null || this.f18427.length() == 0);
            return this.f18427;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m24014() {
            return this.f18429;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final boolean m24015() {
            return this.f18428;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m24017(char c) {
            m24021(String.valueOf(c));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 靐  reason: contains not printable characters */
        public final void m24018(String str) {
            if (this.f18427 != null) {
                str = this.f18427.concat(str);
            }
            this.f18427 = str;
            this.f18429 = Normalizer.m23524(this.f18427);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 麤  reason: contains not printable characters */
        public final void m24019(String str) {
            m24009();
            if (this.f18422.length() == 0) {
                this.f18423 = str;
            } else {
                this.f18422.append(str);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public final void m24020(char c) {
            m24009();
            this.f18422.append(c);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 齉  reason: contains not printable characters */
        public final void m24021(String str) {
            if (this.f18421 != null) {
                str = this.f18421.concat(str);
            }
            this.f18421 = str;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final Tag m24022(String str) {
            this.f18427 = str;
            this.f18429 = Normalizer.m23524(str);
            return this;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m24023(char c) {
            m24018(String.valueOf(c));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: 龘  reason: contains not printable characters */
        public final void m24024(int[] iArr) {
            m24009();
            for (int appendCodePoint : iArr) {
                this.f18422.appendCodePoint(appendCodePoint);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﹶ  reason: contains not printable characters */
        public final void m24025() {
            if (this.f18426 == null) {
                this.f18426 = new Attributes();
            }
            if (this.f18421 != null) {
                this.f18421 = this.f18421.trim();
                if (this.f18421.length() > 0) {
                    this.f18426.m23555(this.f18421, this.f18425 ? this.f18422.length() > 0 ? this.f18422.toString() : this.f18423 : this.f18424 ? "" : null);
                }
            }
            this.f18421 = null;
            this.f18424 = false;
            this.f18425 = false;
            m23980(this.f18422);
            this.f18423 = null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ﾞ  reason: contains not printable characters */
        public final void m24026() {
            if (this.f18421 != null) {
                m24025();
            }
        }
    }

    enum TokenType {
        Doctype,
        StartTag,
        EndTag,
        Comment,
        Character,
        EOF
    }

    private Token() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static void m23980(StringBuilder sb) {
        if (sb != null) {
            sb.delete(0, sb.length());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final StartTag m23981() {
        return (StartTag) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final boolean m23982() {
        return this.f18412 == TokenType.EndTag;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final EndTag m23983() {
        return (EndTag) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʾ  reason: contains not printable characters */
    public final boolean m23984() {
        return this.f18412 == TokenType.EOF;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public final Character m23985() {
        return (Character) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public final boolean m23986() {
        return this.f18412 == TokenType.Comment;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ٴ  reason: contains not printable characters */
    public final Comment m23987() {
        return (Comment) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public final boolean m23988() {
        return this.f18412 == TokenType.Character;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public final boolean m23989() {
        return this.f18412 == TokenType.StartTag;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public abstract Token m23990();

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public final Doctype m23991() {
        return (Doctype) this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public final boolean m23992() {
        return this.f18412 == TokenType.Doctype;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public String m23993() {
        return getClass().getSimpleName();
    }
}
