package org2.jsoup.parser;

import io.fabric.sdk.android.services.events.EventsFilesManager;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.helper.Validate;

public class TokenQueue {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f18437 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    private String f18438;

    public TokenQueue(String str) {
        Validate.m23515((Object) str);
        this.f18438 = str;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public static String m24027(String str) {
        int i = 0;
        StringBuilder r3 = StringUtil.m23506();
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        char c = 0;
        while (i < length) {
            char c2 = charArray[i];
            if (c2 != '\\') {
                r3.append(c2);
            } else if (c != 0 && c == '\\') {
                r3.append(c2);
            }
            i++;
            c = c2;
        }
        return r3.toString();
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private int m24028() {
        return this.f18438.length() - this.f18437;
    }

    public String toString() {
        return this.f18438.substring(this.f18437);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public String m24029() {
        int i = this.f18437;
        while (!m24042()) {
            if (!m24040()) {
                if (!m24045("*|", "|", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, "-")) {
                    break;
                }
            }
            this.f18437++;
        }
        return this.f18438.substring(i, this.f18437);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public String m24030() {
        int i = this.f18437;
        while (!m24042() && (m24040() || m24044('-', '_'))) {
            this.f18437++;
        }
        return this.f18438.substring(i, this.f18437);
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public String m24031() {
        String substring = this.f18438.substring(this.f18437, this.f18438.length());
        this.f18437 = this.f18438.length();
        return substring;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public String m24032(String str) {
        String r0 = m24038(str);
        m24036(str);
        return r0;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m24033() {
        boolean z = false;
        while (m24035()) {
            this.f18437++;
            z = true;
        }
        return z;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public String m24034(String... strArr) {
        int i = this.f18437;
        while (!m24042() && !m24045(strArr)) {
            this.f18437++;
        }
        return this.f18438.substring(i, this.f18437);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m24035() {
        return !m24042() && StringUtil.m23497((int) this.f18438.charAt(this.f18437));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m24036(String str) {
        if (!m24043(str)) {
            return false;
        }
        this.f18437 += str.length();
        return true;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public char m24037() {
        String str = this.f18438;
        int i = this.f18437;
        this.f18437 = i + 1;
        return str.charAt(i);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public String m24038(String str) {
        int indexOf = this.f18438.indexOf(str, this.f18437);
        if (indexOf == -1) {
            return m24031();
        }
        String substring = this.f18438.substring(this.f18437, indexOf);
        this.f18437 += substring.length();
        return substring;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m24039(String str) {
        if (!m24043(str)) {
            throw new IllegalStateException("Queue did not match expected sequence");
        }
        int length = str.length();
        if (length > m24028()) {
            throw new IllegalStateException("Queue not long enough to consume sequence");
        }
        this.f18437 = length + this.f18437;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m24040() {
        return !m24042() && Character.isLetterOrDigit(this.f18438.charAt(this.f18437));
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x000d A[EDGE_INSN: B:37:0x000d->B:3:0x000d ?: BREAK  , SYNTHETIC] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String m24041(char r10, char r11) {
        /*
            r9 = this;
            r5 = -1
            r1 = 0
            r0 = r1
            r2 = r1
            r3 = r1
            r4 = r5
            r6 = r5
        L_0x0007:
            boolean r7 = r9.m24042()
            if (r7 == 0) goto L_0x0036
        L_0x000d:
            if (r4 < 0) goto L_0x0094
            java.lang.String r0 = r9.f18438
            java.lang.String r0 = r0.substring(r6, r4)
        L_0x0015:
            if (r3 <= 0) goto L_0x0035
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Did not find balanced marker at '"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r2 = "'"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            org2.jsoup.helper.Validate.m23512((java.lang.String) r1)
        L_0x0035:
            return r0
        L_0x0036:
            char r7 = r9.m24037()
            java.lang.Character r7 = java.lang.Character.valueOf(r7)
            if (r2 == 0) goto L_0x0044
            r8 = 92
            if (r2 == r8) goto L_0x007c
        L_0x0044:
            r8 = 39
            java.lang.Character r8 = java.lang.Character.valueOf(r8)
            boolean r8 = r7.equals(r8)
            if (r8 != 0) goto L_0x005c
            r8 = 34
            java.lang.Character r8 = java.lang.Character.valueOf(r8)
            boolean r8 = r7.equals(r8)
            if (r8 == 0) goto L_0x0065
        L_0x005c:
            char r8 = r7.charValue()
            if (r8 == r10) goto L_0x0065
            if (r0 != 0) goto L_0x006a
            r0 = 1
        L_0x0065:
            if (r0 == 0) goto L_0x006c
        L_0x0067:
            if (r3 > 0) goto L_0x0007
            goto L_0x000d
        L_0x006a:
            r0 = r1
            goto L_0x0065
        L_0x006c:
            java.lang.Character r8 = java.lang.Character.valueOf(r10)
            boolean r8 = r7.equals(r8)
            if (r8 == 0) goto L_0x0087
            int r3 = r3 + 1
            if (r6 != r5) goto L_0x007c
            int r6 = r9.f18437
        L_0x007c:
            if (r3 <= 0) goto L_0x0082
            if (r2 == 0) goto L_0x0082
            int r4 = r9.f18437
        L_0x0082:
            char r2 = r7.charValue()
            goto L_0x0067
        L_0x0087:
            java.lang.Character r8 = java.lang.Character.valueOf(r11)
            boolean r8 = r7.equals(r8)
            if (r8 == 0) goto L_0x007c
            int r3 = r3 + -1
            goto L_0x007c
        L_0x0094:
            java.lang.String r0 = ""
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.parser.TokenQueue.m24041(char, char):java.lang.String");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24042() {
        return m24028() == 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24043(String str) {
        return this.f18438.regionMatches(true, this.f18437, str, 0, str.length());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24044(char... cArr) {
        if (m24042()) {
            return false;
        }
        for (char c : cArr) {
            if (this.f18438.charAt(this.f18437) == c) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m24045(String... strArr) {
        for (String r3 : strArr) {
            if (m24043(r3)) {
                return true;
            }
        }
        return false;
    }
}
