package org2.jsoup.parser;

import java.util.Arrays;
import org.apache.commons.lang3.CharUtils;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Entities;
import org2.jsoup.parser.Token;

final class Tokeniser {

    /* renamed from: ʽ  reason: contains not printable characters */
    private static final char[] f18439 = {9, 10, CharUtils.CR, 12, ' ', '<', '&'};

    /* renamed from: ʻ  reason: contains not printable characters */
    Token.Doctype f18440 = new Token.Doctype();

    /* renamed from: ʼ  reason: contains not printable characters */
    Token.Comment f18441 = new Token.Comment();

    /* renamed from: ʾ  reason: contains not printable characters */
    private boolean f18442 = false;

    /* renamed from: ʿ  reason: contains not printable characters */
    private String f18443 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private Token f18444;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int[] f18445 = new int[1];

    /* renamed from: ˋ  reason: contains not printable characters */
    private final int[] f18446 = new int[2];

    /* renamed from: ˑ  reason: contains not printable characters */
    private final CharacterReader f18447;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final ParseErrorList f18448;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private TokeniserState f18449 = TokeniserState.Data;

    /* renamed from: 连任  reason: contains not printable characters */
    Token.Character f18450 = new Token.Character();

    /* renamed from: 靐  reason: contains not printable characters */
    Token.Tag f18451;

    /* renamed from: 麤  reason: contains not printable characters */
    Token.EndTag f18452 = new Token.EndTag();

    /* renamed from: 齉  reason: contains not printable characters */
    Token.StartTag f18453 = new Token.StartTag();

    /* renamed from: 龘  reason: contains not printable characters */
    StringBuilder f18454 = new StringBuilder(1024);

    /* renamed from: ﹶ  reason: contains not printable characters */
    private StringBuilder f18455 = new StringBuilder(1024);

    /* renamed from: ﾞ  reason: contains not printable characters */
    private String f18456;

    static {
        Arrays.sort(f18439);
    }

    Tokeniser(CharacterReader characterReader, ParseErrorList parseErrorList) {
        this.f18447 = characterReader;
        this.f18448 = parseErrorList;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m24046(String str) {
        if (this.f18448.m23959()) {
            this.f18448.add(new ParseError(this.f18447.m23829(), "Invalid character reference: %s", str));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m24047() {
        m24063((Token) this.f18440);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m24048() {
        Token.m23980(this.f18454);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public boolean m24049() {
        return this.f18456 != null && this.f18451.m24013().equalsIgnoreCase(this.f18456);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public String m24050() {
        return this.f18456;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m24051() {
        this.f18440.m24002();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m24052() {
        this.f18451.m24026();
        m24063((Token) this.f18451);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m24053(String str) {
        if (this.f18448.m23959()) {
            this.f18448.add(new ParseError(this.f18447.m23829(), str));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m24054(TokeniserState tokeniserState) {
        this.f18447.m23805();
        this.f18449 = tokeniserState;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m24055() {
        m24063((Token) this.f18441);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m24056(TokeniserState tokeniserState) {
        if (this.f18448.m23959()) {
            this.f18448.add(new ParseError(this.f18447.m23829(), "Unexpectedly reached end of file (EOF) in input state [%s]", tokeniserState));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m24057() {
        this.f18441.m23998();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m24058(TokeniserState tokeniserState) {
        if (this.f18448.m23959()) {
            this.f18448.add(new ParseError(this.f18447.m23829(), "Unexpected character '%s' in input state [%s]", Character.valueOf(this.f18447.m23825()), tokeniserState));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Token.Tag m24059(boolean z) {
        this.f18451 = z ? this.f18453.m24007() : this.f18452.m24016();
        return this.f18451;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Token m24060() {
        while (!this.f18442) {
            this.f18449.m24082(this, this.f18447);
        }
        if (this.f18455.length() > 0) {
            String sb = this.f18455.toString();
            this.f18455.delete(0, this.f18455.length());
            this.f18443 = null;
            return this.f18450.m23996(sb);
        } else if (this.f18443 != null) {
            Token.Character r0 = this.f18450.m23996(this.f18443);
            this.f18443 = null;
            return r0;
        } else {
            this.f18442 = false;
            return this.f18444;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24061(char c) {
        m24062(String.valueOf(c));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24062(String str) {
        if (this.f18443 == null) {
            this.f18443 = str;
            return;
        }
        if (this.f18455.length() == 0) {
            this.f18455.append(this.f18443);
        }
        this.f18455.append(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24063(Token token) {
        Validate.m23514(this.f18442, "There is an unread token pending!");
        this.f18444 = token;
        this.f18442 = true;
        if (token.f18412 == Token.TokenType.StartTag) {
            this.f18456 = ((Token.StartTag) token).f18427;
        } else if (token.f18412 == Token.TokenType.EndTag && ((Token.EndTag) token).f18426 != null) {
            m24053("Attributes incorrectly present on end tag");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24064(TokeniserState tokeniserState) {
        this.f18449 = tokeniserState;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24065(int[] iArr) {
        m24062(new String(iArr, 0, iArr.length));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public int[] m24066(Character ch, boolean z) {
        int i;
        if (this.f18447.m23820()) {
            return null;
        }
        if (ch != null && ch.charValue() == this.f18447.m23825()) {
            return null;
        }
        if (this.f18447.m23824(f18439)) {
            return null;
        }
        int[] iArr = this.f18445;
        this.f18447.m23807();
        if (this.f18447.m23823("#")) {
            boolean r4 = this.f18447.m23817("X");
            String r0 = r4 ? this.f18447.m23810() : this.f18447.m23834();
            if (r0.length() == 0) {
                m24046("numeric reference with no numerals");
                this.f18447.m23808();
                return null;
            }
            if (!this.f18447.m23823(";")) {
                m24046("missing semicolon");
            }
            try {
                i = Integer.valueOf(r0, r4 ? 16 : 10).intValue();
            } catch (NumberFormatException e) {
                i = -1;
            }
            if (i == -1 || ((i >= 55296 && i <= 57343) || i > 1114111)) {
                m24046("character outside of valid range");
                iArr[0] = 65533;
                return iArr;
            }
            iArr[0] = i;
            return iArr;
        }
        String r5 = this.f18447.m23809();
        boolean r6 = this.f18447.m23826(';');
        if (!(Entities.m23692(r5) || (Entities.m23697(r5) && r6))) {
            this.f18447.m23808();
            if (r6) {
                m24046(String.format("invalid named referenece '%s'", new Object[]{r5}));
            }
            return null;
        } else if (!z || (!this.f18447.m23835() && !this.f18447.m23812() && !this.f18447.m23828('=', '-', '_'))) {
            if (!this.f18447.m23823(";")) {
                m24046("missing semicolon");
            }
            int r02 = Entities.m23693(r5, this.f18446);
            if (r02 == 1) {
                iArr[0] = this.f18446[0];
                return iArr;
            } else if (r02 == 2) {
                return this.f18446;
            } else {
                Validate.m23512("Unexpected characters returned for " + r5);
                return this.f18446;
            }
        } else {
            this.f18447.m23808();
            return null;
        }
    }
}
