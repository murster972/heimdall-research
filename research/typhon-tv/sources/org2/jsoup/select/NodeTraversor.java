package org2.jsoup.select;

import java.util.Iterator;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.Node;
import org2.jsoup.select.NodeFilter;

public class NodeTraversor {
    /* renamed from: 龘  reason: contains not printable characters */
    public static NodeFilter.FilterResult m24225(NodeFilter nodeFilter, Node node) {
        int i = 0;
        Node node2 = node;
        while (node2 != null) {
            NodeFilter.FilterResult r0 = nodeFilter.m24224(node2, i);
            if (r0 == NodeFilter.FilterResult.STOP) {
                return r0;
            }
            if (r0 != NodeFilter.FilterResult.CONTINUE || node2.m23761() <= 0) {
                while (node2.m23747() == null && i > 0) {
                    if ((r0 == NodeFilter.FilterResult.CONTINUE || r0 == NodeFilter.FilterResult.SKIP_CHILDREN) && (r0 = nodeFilter.m24223(node2, i)) == NodeFilter.FilterResult.STOP) {
                        return r0;
                    }
                    Node r3 = node2.m23739();
                    i--;
                    if (r0 == NodeFilter.FilterResult.REMOVE) {
                        node2.m23740();
                    }
                    r0 = NodeFilter.FilterResult.CONTINUE;
                    node2 = r3;
                }
                if ((r0 == NodeFilter.FilterResult.CONTINUE || r0 == NodeFilter.FilterResult.SKIP_CHILDREN) && (r0 = nodeFilter.m24223(node2, i)) == NodeFilter.FilterResult.STOP) {
                    return r0;
                }
                if (node2 == node) {
                    return r0;
                }
                Node r32 = node2.m23747();
                if (r0 == NodeFilter.FilterResult.REMOVE) {
                    node2.m23740();
                }
                node2 = r32;
            } else {
                node2 = node2.m23756(0);
                i++;
            }
        }
        return NodeFilter.FilterResult.CONTINUE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:1:0x000a A[LOOP:0: B:1:0x000a->B:4:0x001c, LOOP_START] */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m24226(org2.jsoup.select.NodeFilter r3, org2.jsoup.select.Elements r4) {
        /*
            org2.jsoup.helper.Validate.m23515((java.lang.Object) r3)
            org2.jsoup.helper.Validate.m23515((java.lang.Object) r4)
            java.util.Iterator r1 = r4.iterator()
        L_0x000a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x001e
            java.lang.Object r0 = r1.next()
            org2.jsoup.nodes.Element r0 = (org2.jsoup.nodes.Element) r0
            org2.jsoup.select.NodeFilter$FilterResult r0 = m24225((org2.jsoup.select.NodeFilter) r3, (org2.jsoup.nodes.Node) r0)
            org2.jsoup.select.NodeFilter$FilterResult r2 = org2.jsoup.select.NodeFilter.FilterResult.STOP
            if (r0 != r2) goto L_0x000a
        L_0x001e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.select.NodeTraversor.m24226(org2.jsoup.select.NodeFilter, org2.jsoup.select.Elements):void");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24227(NodeVisitor nodeVisitor, Node node) {
        int i = 0;
        Node node2 = node;
        while (node2 != null) {
            nodeVisitor.m24230(node2, i);
            if (node2.m23761() > 0) {
                node2 = node2.m23756(0);
                i++;
            } else {
                while (node2.m23747() == null && i > 0) {
                    nodeVisitor.m24229(node2, i);
                    node2 = node2.m23739();
                    i--;
                }
                nodeVisitor.m24229(node2, i);
                if (node2 != node) {
                    node2 = node2.m23747();
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m24228(NodeVisitor nodeVisitor, Elements elements) {
        Validate.m23515((Object) nodeVisitor);
        Validate.m23515((Object) elements);
        Iterator it2 = elements.iterator();
        while (it2.hasNext()) {
            m24227(nodeVisitor, (Node) (Element) it2.next());
        }
    }
}
