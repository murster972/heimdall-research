package org2.jsoup.select;

import java.util.Iterator;
import java.util.regex.Pattern;
import org2.jsoup.helper.Validate;
import org2.jsoup.internal.Normalizer;
import org2.jsoup.nodes.Comment;
import org2.jsoup.nodes.Document;
import org2.jsoup.nodes.DocumentType;
import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.Node;
import org2.jsoup.nodes.XmlDeclaration;

public abstract class Evaluator {

    public static final class AllElements extends Evaluator {
        public String toString() {
            return "*";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24185(Element element, Element element2) {
            return true;
        }
    }

    public static final class Attribute extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18549;

        public Attribute(String str) {
            this.f18549 = str;
        }

        public String toString() {
            return String.format("[%s]", new Object[]{this.f18549});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24186(Element element, Element element2) {
            return element2.m23764(this.f18549);
        }
    }

    public static abstract class AttributeKeyPair extends Evaluator {

        /* renamed from: 靐  reason: contains not printable characters */
        String f18550;

        /* renamed from: 龘  reason: contains not printable characters */
        String f18551;

        public AttributeKeyPair(String str, String str2) {
            Validate.m23517(str);
            Validate.m23517(str2);
            this.f18551 = Normalizer.m23523(str);
            if ((str2.startsWith("\"") && str2.endsWith("\"")) || (str2.startsWith("'") && str2.endsWith("'"))) {
                str2 = str2.substring(1, str2.length() - 1);
            }
            this.f18550 = Normalizer.m23523(str2);
        }
    }

    public static final class AttributeStarting extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18552;

        public AttributeStarting(String str) {
            Validate.m23517(str);
            this.f18552 = Normalizer.m23524(str);
        }

        public String toString() {
            return String.format("[^%s]", new Object[]{this.f18552});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24187(Element element, Element element2) {
            for (org2.jsoup.nodes.Attribute r0 : element2.m23635().m23547()) {
                if (Normalizer.m23524(r0.getKey()).startsWith(this.f18552)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static final class AttributeWithValue extends AttributeKeyPair {
        public AttributeWithValue(String str, String str2) {
            super(str, str2);
        }

        public String toString() {
            return String.format("[%s=%s]", new Object[]{this.f18551, this.f18550});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24188(Element element, Element element2) {
            return element2.m23764(this.f18551) && this.f18550.equalsIgnoreCase(element2.m23760(this.f18551).trim());
        }
    }

    public static final class AttributeWithValueContaining extends AttributeKeyPair {
        public AttributeWithValueContaining(String str, String str2) {
            super(str, str2);
        }

        public String toString() {
            return String.format("[%s*=%s]", new Object[]{this.f18551, this.f18550});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24189(Element element, Element element2) {
            return element2.m23764(this.f18551) && Normalizer.m23524(element2.m23760(this.f18551)).contains(this.f18550);
        }
    }

    public static final class AttributeWithValueEnding extends AttributeKeyPair {
        public AttributeWithValueEnding(String str, String str2) {
            super(str, str2);
        }

        public String toString() {
            return String.format("[%s$=%s]", new Object[]{this.f18551, this.f18550});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24190(Element element, Element element2) {
            return element2.m23764(this.f18551) && Normalizer.m23524(element2.m23760(this.f18551)).endsWith(this.f18550);
        }
    }

    public static final class AttributeWithValueMatching extends Evaluator {

        /* renamed from: 靐  reason: contains not printable characters */
        Pattern f18553;

        /* renamed from: 龘  reason: contains not printable characters */
        String f18554;

        public AttributeWithValueMatching(String str, Pattern pattern) {
            this.f18554 = Normalizer.m23523(str);
            this.f18553 = pattern;
        }

        public String toString() {
            return String.format("[%s~=%s]", new Object[]{this.f18554, this.f18553.toString()});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24191(Element element, Element element2) {
            return element2.m23764(this.f18554) && this.f18553.matcher(element2.m23760(this.f18554)).find();
        }
    }

    public static final class AttributeWithValueNot extends AttributeKeyPair {
        public AttributeWithValueNot(String str, String str2) {
            super(str, str2);
        }

        public String toString() {
            return String.format("[%s!=%s]", new Object[]{this.f18551, this.f18550});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24192(Element element, Element element2) {
            return !this.f18550.equalsIgnoreCase(element2.m23760(this.f18551));
        }
    }

    public static final class AttributeWithValueStarting extends AttributeKeyPair {
        public AttributeWithValueStarting(String str, String str2) {
            super(str, str2);
        }

        public String toString() {
            return String.format("[%s^=%s]", new Object[]{this.f18551, this.f18550});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24193(Element element, Element element2) {
            return element2.m23764(this.f18551) && Normalizer.m23524(element2.m23760(this.f18551)).startsWith(this.f18550);
        }
    }

    public static final class Class extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18555;

        public Class(String str) {
            this.f18555 = str;
        }

        public String toString() {
            return String.format(".%s", new Object[]{this.f18555});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24194(Element element, Element element2) {
            return element2.m23687(this.f18555);
        }
    }

    public static final class ContainsData extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18556;

        public ContainsData(String str) {
            this.f18556 = Normalizer.m23524(str);
        }

        public String toString() {
            return String.format(":containsData(%s)", new Object[]{this.f18556});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24195(Element element, Element element2) {
            return Normalizer.m23524(element2.m23661()).contains(this.f18556);
        }
    }

    public static final class ContainsOwnText extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18557;

        public ContainsOwnText(String str) {
            this.f18557 = Normalizer.m23524(str);
        }

        public String toString() {
            return String.format(":containsOwn(%s)", new Object[]{this.f18557});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24196(Element element, Element element2) {
            return Normalizer.m23524(element2.m23683()).contains(this.f18557);
        }
    }

    public static final class ContainsText extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18558;

        public ContainsText(String str) {
            this.f18558 = Normalizer.m23524(str);
        }

        public String toString() {
            return String.format(":contains(%s)", new Object[]{this.f18558});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24197(Element element, Element element2) {
            return Normalizer.m23524(element2.m23667()).contains(this.f18558);
        }
    }

    public static abstract class CssNthEvaluator extends Evaluator {

        /* renamed from: 靐  reason: contains not printable characters */
        protected final int f18559;

        /* renamed from: 龘  reason: contains not printable characters */
        protected final int f18560;

        public CssNthEvaluator(int i, int i2) {
            this.f18560 = i;
            this.f18559 = i2;
        }

        public String toString() {
            if (this.f18560 == 0) {
                return String.format(":%s(%d)", new Object[]{m24199(), Integer.valueOf(this.f18559)});
            } else if (this.f18559 == 0) {
                return String.format(":%s(%dn)", new Object[]{m24199(), Integer.valueOf(this.f18560)});
            } else {
                return String.format(":%s(%dn%+d)", new Object[]{m24199(), Integer.valueOf(this.f18560), Integer.valueOf(this.f18559)});
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public abstract int m24198(Element element, Element element2);

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public abstract String m24199();

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24200(Element element, Element element2) {
            Element r2 = element2.m23637();
            if (r2 == null || (r2 instanceof Document)) {
                return false;
            }
            int r22 = m24198(element, element2);
            return this.f18560 == 0 ? r22 == this.f18559 : (r22 - this.f18559) * this.f18560 >= 0 && (r22 - this.f18559) % this.f18560 == 0;
        }
    }

    public static final class Id extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18561;

        public Id(String str) {
            this.f18561 = str;
        }

        public String toString() {
            return String.format("#%s", new Object[]{this.f18561});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24201(Element element, Element element2) {
            return this.f18561.equals(element2.m23646());
        }
    }

    public static final class IndexEquals extends IndexEvaluator {
        public IndexEquals(int i) {
            super(i);
        }

        public String toString() {
            return String.format(":eq(%d)", new Object[]{Integer.valueOf(this.f18562)});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24202(Element element, Element element2) {
            return element2.m23665() == this.f18562;
        }
    }

    public static abstract class IndexEvaluator extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        int f18562;

        public IndexEvaluator(int i) {
            this.f18562 = i;
        }
    }

    public static final class IndexGreaterThan extends IndexEvaluator {
        public IndexGreaterThan(int i) {
            super(i);
        }

        public String toString() {
            return String.format(":gt(%d)", new Object[]{Integer.valueOf(this.f18562)});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24203(Element element, Element element2) {
            return element2.m23665() > this.f18562;
        }
    }

    public static final class IndexLessThan extends IndexEvaluator {
        public IndexLessThan(int i) {
            super(i);
        }

        public String toString() {
            return String.format(":lt(%d)", new Object[]{Integer.valueOf(this.f18562)});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24204(Element element, Element element2) {
            return element != element2 && element2.m23665() < this.f18562;
        }
    }

    public static final class IsEmpty extends Evaluator {
        public String toString() {
            return ":empty";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24205(Element element, Element element2) {
            for (Node next : element2.m23751()) {
                if (!(next instanceof Comment) && !(next instanceof XmlDeclaration) && !(next instanceof DocumentType)) {
                    return false;
                }
            }
            return true;
        }
    }

    public static final class IsFirstChild extends Evaluator {
        public String toString() {
            return ":first-child";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24206(Element element, Element element2) {
            Element r0 = element2.m23637();
            return r0 != null && !(r0 instanceof Document) && element2.m23665() == 0;
        }
    }

    public static final class IsFirstOfType extends IsNthOfType {
        public IsFirstOfType() {
            super(0, 1);
        }

        public String toString() {
            return ":first-of-type";
        }
    }

    public static final class IsLastChild extends Evaluator {
        public String toString() {
            return ":last-child";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24207(Element element, Element element2) {
            Element r0 = element2.m23637();
            return r0 != null && !(r0 instanceof Document) && element2.m23665() == r0.m23643().size() + -1;
        }
    }

    public static final class IsLastOfType extends IsNthLastOfType {
        public IsLastOfType() {
            super(0, 1);
        }

        public String toString() {
            return ":last-of-type";
        }
    }

    public static final class IsNthChild extends CssNthEvaluator {
        public IsNthChild(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m24208(Element element, Element element2) {
            return element2.m23665() + 1;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m24209() {
            return "nth-child";
        }
    }

    public static final class IsNthLastChild extends CssNthEvaluator {
        public IsNthLastChild(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m24210(Element element, Element element2) {
            return element2.m23637().m23643().size() - element2.m23665();
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m24211() {
            return "nth-last-child";
        }
    }

    public static class IsNthLastOfType extends CssNthEvaluator {
        public IsNthLastOfType(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m24212(Element element, Element element2) {
            Elements r3 = element2.m23637().m23643();
            int r0 = element2.m23665();
            int i = 0;
            while (true) {
                int i2 = r0;
                if (i2 >= r3.size()) {
                    return i;
                }
                if (((Element) r3.get(i2)).m23686().equals(element2.m23686())) {
                    i++;
                }
                r0 = i2 + 1;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m24213() {
            return "nth-last-of-type";
        }
    }

    public static class IsNthOfType extends CssNthEvaluator {
        public IsNthOfType(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: 靐  reason: contains not printable characters */
        public int m24214(Element element, Element element2) {
            Iterator it2 = element2.m23637().m23643().iterator();
            int i = 0;
            while (it2.hasNext()) {
                Element element3 = (Element) it2.next();
                if (element3.m23686().equals(element2.m23686())) {
                    i++;
                    continue;
                }
                if (element3 == element2) {
                    break;
                }
            }
            return i;
        }

        /* access modifiers changed from: protected */
        /* renamed from: 龘  reason: contains not printable characters */
        public String m24215() {
            return "nth-of-type";
        }
    }

    public static final class IsOnlyChild extends Evaluator {
        public String toString() {
            return ":only-child";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24216(Element element, Element element2) {
            Element r0 = element2.m23637();
            return r0 != null && !(r0 instanceof Document) && element2.m23656().size() == 0;
        }
    }

    public static final class IsOnlyOfType extends Evaluator {
        public String toString() {
            return ":only-of-type";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24217(Element element, Element element2) {
            Element r0 = element2.m23637();
            if (r0 == null || (r0 instanceof Document)) {
                return false;
            }
            Iterator it2 = r0.m23643().iterator();
            int i = 0;
            while (it2.hasNext()) {
                i = ((Element) it2.next()).m23686().equals(element2.m23686()) ? i + 1 : i;
            }
            return i == 1;
        }
    }

    public static final class IsRoot extends Evaluator {
        public String toString() {
            return ":root";
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24218(Element element, Element element2) {
            if (element instanceof Document) {
                element = element.m23677(0);
            }
            return element2 == element;
        }
    }

    public static final class Matches extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private Pattern f18563;

        public Matches(Pattern pattern) {
            this.f18563 = pattern;
        }

        public String toString() {
            return String.format(":matches(%s)", new Object[]{this.f18563});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24219(Element element, Element element2) {
            return this.f18563.matcher(element2.m23667()).find();
        }
    }

    public static final class MatchesOwn extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private Pattern f18564;

        public MatchesOwn(Pattern pattern) {
            this.f18564 = pattern;
        }

        public String toString() {
            return String.format(":matchesOwn(%s)", new Object[]{this.f18564});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24220(Element element, Element element2) {
            return this.f18564.matcher(element2.m23683()).find();
        }
    }

    public static final class Tag extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18565;

        public Tag(String str) {
            this.f18565 = str;
        }

        public String toString() {
            return String.format("%s", new Object[]{this.f18565});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24221(Element element, Element element2) {
            return element2.m23684().equalsIgnoreCase(this.f18565);
        }
    }

    public static final class TagEndsWith extends Evaluator {

        /* renamed from: 龘  reason: contains not printable characters */
        private String f18566;

        public TagEndsWith(String str) {
            this.f18566 = str;
        }

        public String toString() {
            return String.format("%s", new Object[]{this.f18566});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24222(Element element, Element element2) {
            return element2.m23684().endsWith(this.f18566);
        }
    }

    protected Evaluator() {
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public abstract boolean m24184(Element element, Element element2);
}
