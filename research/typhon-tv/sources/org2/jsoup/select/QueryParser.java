package org2.jsoup.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.helper.Validate;
import org2.jsoup.internal.Normalizer;
import org2.jsoup.parser.TokenQueue;
import org2.jsoup.select.CombiningEvaluator;
import org2.jsoup.select.Evaluator;
import org2.jsoup.select.Selector;
import org2.jsoup.select.StructuralEvaluator;

public class QueryParser {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final Pattern f18568 = Pattern.compile("(([+-])?(\\d+)?)n(\\s*([+-])?\\s*\\d+)?", 2);

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final Pattern f18569 = Pattern.compile("([+-])?(\\d+)");

    /* renamed from: 靐  reason: contains not printable characters */
    private static final String[] f18570 = {"=", "!=", "^=", "$=", "*=", "~="};

    /* renamed from: 龘  reason: contains not printable characters */
    private static final String[] f18571 = {",", ">", "+", "~", StringUtils.SPACE};

    /* renamed from: 连任  reason: contains not printable characters */
    private List<Evaluator> f18572 = new ArrayList();

    /* renamed from: 麤  reason: contains not printable characters */
    private String f18573;

    /* renamed from: 齉  reason: contains not printable characters */
    private TokenQueue f18574;

    private QueryParser(String str) {
        this.f18573 = str;
        this.f18574 = new TokenQueue(str);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m24231() {
        String r0 = this.f18574.m24029();
        Validate.m23517(r0);
        if (r0.startsWith("*|")) {
            this.f18572.add(new CombiningEvaluator.Or(new Evaluator.Tag(Normalizer.m23523(r0)), new Evaluator.TagEndsWith(Normalizer.m23523(r0.replace("*|", ":")))));
            return;
        }
        if (r0.contains("|")) {
            r0 = r0.replace("|", ":");
        }
        this.f18572.add(new Evaluator.Tag(r0.trim()));
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m24232() {
        TokenQueue tokenQueue = new TokenQueue(this.f18574.m24041('[', ']'));
        String r1 = tokenQueue.m24034(f18570);
        Validate.m23517(r1);
        tokenQueue.m24033();
        if (tokenQueue.m24042()) {
            if (r1.startsWith("^")) {
                this.f18572.add(new Evaluator.AttributeStarting(r1.substring(1)));
            } else {
                this.f18572.add(new Evaluator.Attribute(r1));
            }
        } else if (tokenQueue.m24036("=")) {
            this.f18572.add(new Evaluator.AttributeWithValue(r1, tokenQueue.m24031()));
        } else if (tokenQueue.m24036("!=")) {
            this.f18572.add(new Evaluator.AttributeWithValueNot(r1, tokenQueue.m24031()));
        } else if (tokenQueue.m24036("^=")) {
            this.f18572.add(new Evaluator.AttributeWithValueStarting(r1, tokenQueue.m24031()));
        } else if (tokenQueue.m24036("$=")) {
            this.f18572.add(new Evaluator.AttributeWithValueEnding(r1, tokenQueue.m24031()));
        } else if (tokenQueue.m24036("*=")) {
            this.f18572.add(new Evaluator.AttributeWithValueContaining(r1, tokenQueue.m24031()));
        } else if (tokenQueue.m24036("~=")) {
            this.f18572.add(new Evaluator.AttributeWithValueMatching(r1, Pattern.compile(tokenQueue.m24031())));
        } else {
            throw new Selector.SelectorParseException("Could not parse attribute query '%s': unexpected token at '%s'", this.f18573, tokenQueue.m24031());
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private void m24233() {
        this.f18572.add(new Evaluator.AllElements());
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    private void m24234() {
        this.f18574.m24039(":has");
        String r0 = this.f18574.m24041('(', ')');
        Validate.m23518(r0, ":has(el) subselect must not be empty");
        this.f18572.add(new StructuralEvaluator.Has(m24245(r0)));
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    private void m24235() {
        this.f18574.m24039(":containsData");
        String r0 = TokenQueue.m24027(this.f18574.m24041('(', ')'));
        Validate.m23518(r0, ":containsData(text) query must not be empty");
        this.f18572.add(new Evaluator.ContainsData(r0));
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private int m24236() {
        String trim = this.f18574.m24032(")").trim();
        Validate.m23520(StringUtil.m23498(trim), "Index must be numeric");
        return Integer.parseInt(trim);
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m24237() {
        this.f18572.add(new Evaluator.IndexLessThan(m24236()));
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private void m24238() {
        this.f18572.add(new Evaluator.IndexGreaterThan(m24236()));
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m24239() {
        this.f18572.add(new Evaluator.IndexEquals(m24236()));
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m24240() {
        String r0 = this.f18574.m24030();
        Validate.m23517(r0);
        this.f18572.add(new Evaluator.Class(r0.trim()));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private String m24241() {
        StringBuilder sb = new StringBuilder();
        while (!this.f18574.m24042()) {
            if (this.f18574.m24043("(")) {
                sb.append("(").append(this.f18574.m24041('(', ')')).append(")");
            } else if (this.f18574.m24043("[")) {
                sb.append("[").append(this.f18574.m24041('[', ']')).append("]");
            } else if (this.f18574.m24045(f18571)) {
                break;
            } else {
                sb.append(this.f18574.m24037());
            }
        }
        return sb.toString();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m24242(boolean z) {
        this.f18574.m24039(z ? ":matchesOwn" : ":matches");
        String r0 = this.f18574.m24041('(', ')');
        Validate.m23518(r0, ":matches(regex) query must not be empty");
        if (z) {
            this.f18572.add(new Evaluator.MatchesOwn(Pattern.compile(r0)));
        } else {
            this.f18572.add(new Evaluator.Matches(Pattern.compile(r0)));
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m24243() {
        String r0 = this.f18574.m24030();
        Validate.m23517(r0);
        this.f18572.add(new Evaluator.Id(r0));
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m24244() {
        if (this.f18574.m24036("#")) {
            m24243();
        } else if (this.f18574.m24036(".")) {
            m24240();
        } else if (this.f18574.m24040() || this.f18574.m24043("*|")) {
            m24231();
        } else if (this.f18574.m24043("[")) {
            m24232();
        } else if (this.f18574.m24036("*")) {
            m24233();
        } else if (this.f18574.m24036(":lt(")) {
            m24237();
        } else if (this.f18574.m24036(":gt(")) {
            m24238();
        } else if (this.f18574.m24036(":eq(")) {
            m24239();
        } else if (this.f18574.m24043(":has(")) {
            m24234();
        } else if (this.f18574.m24043(":contains(")) {
            m24247(false);
        } else if (this.f18574.m24043(":containsOwn(")) {
            m24247(true);
        } else if (this.f18574.m24043(":containsData(")) {
            m24235();
        } else if (this.f18574.m24043(":matches(")) {
            m24242(false);
        } else if (this.f18574.m24043(":matchesOwn(")) {
            m24242(true);
        } else if (this.f18574.m24043(":not(")) {
            m24249();
        } else if (this.f18574.m24036(":nth-child(")) {
            m24248(false, false);
        } else if (this.f18574.m24036(":nth-last-child(")) {
            m24248(true, false);
        } else if (this.f18574.m24036(":nth-of-type(")) {
            m24248(false, true);
        } else if (this.f18574.m24036(":nth-last-of-type(")) {
            m24248(true, true);
        } else if (this.f18574.m24036(":first-child")) {
            this.f18572.add(new Evaluator.IsFirstChild());
        } else if (this.f18574.m24036(":last-child")) {
            this.f18572.add(new Evaluator.IsLastChild());
        } else if (this.f18574.m24036(":first-of-type")) {
            this.f18572.add(new Evaluator.IsFirstOfType());
        } else if (this.f18574.m24036(":last-of-type")) {
            this.f18572.add(new Evaluator.IsLastOfType());
        } else if (this.f18574.m24036(":only-child")) {
            this.f18572.add(new Evaluator.IsOnlyChild());
        } else if (this.f18574.m24036(":only-of-type")) {
            this.f18572.add(new Evaluator.IsOnlyOfType());
        } else if (this.f18574.m24036(":empty")) {
            this.f18572.add(new Evaluator.IsEmpty());
        } else if (this.f18574.m24036(":root")) {
            this.f18572.add(new Evaluator.IsRoot());
        } else {
            throw new Selector.SelectorParseException("Could not parse query '%s': unexpected token at '%s'", this.f18573, this.f18574.m24031());
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Evaluator m24245(String str) {
        try {
            return new QueryParser(str).m24250();
        } catch (IllegalArgumentException e) {
            throw new Selector.SelectorParseException(e.getMessage(), new Object[0]);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24246(char c) {
        Evaluator and;
        boolean z;
        Evaluator evaluator;
        CombiningEvaluator.Or or;
        CombiningEvaluator.And and2;
        this.f18574.m24033();
        Evaluator r6 = m24245(m24241());
        if (this.f18572.size() == 1) {
            and = this.f18572.get(0);
            if (!(and instanceof CombiningEvaluator.Or) || c == ',') {
                z = false;
                evaluator = and;
            } else {
                z = true;
                evaluator = and;
                and = ((CombiningEvaluator.Or) and).m24178();
            }
        } else {
            and = new CombiningEvaluator.And((Collection<Evaluator>) this.f18572);
            z = false;
            evaluator = and;
        }
        this.f18572.clear();
        if (c == '>') {
            and2 = new CombiningEvaluator.And(r6, new StructuralEvaluator.ImmediateParent(and));
        } else if (c == ' ') {
            and2 = new CombiningEvaluator.And(r6, new StructuralEvaluator.Parent(and));
        } else if (c == '+') {
            and2 = new CombiningEvaluator.And(r6, new StructuralEvaluator.ImmediatePreviousSibling(and));
        } else if (c == '~') {
            and2 = new CombiningEvaluator.And(r6, new StructuralEvaluator.PreviousSibling(and));
        } else if (c == ',') {
            if (and instanceof CombiningEvaluator.Or) {
                or = (CombiningEvaluator.Or) and;
                or.m24181(r6);
            } else {
                CombiningEvaluator.Or or2 = new CombiningEvaluator.Or();
                or2.m24181(and);
                or2.m24181(r6);
                or = or2;
            }
            and2 = or;
        } else {
            throw new Selector.SelectorParseException("Unknown combinator: " + c, new Object[0]);
        }
        if (z) {
            ((CombiningEvaluator.Or) evaluator).m24179(and2);
        } else {
            evaluator = and2;
        }
        this.f18572.add(evaluator);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24247(boolean z) {
        this.f18574.m24039(z ? ":containsOwn" : ":contains");
        String r0 = TokenQueue.m24027(this.f18574.m24041('(', ')'));
        Validate.m23518(r0, ":contains(text) query must not be empty");
        if (z) {
            this.f18572.add(new Evaluator.ContainsOwnText(r0));
        } else {
            this.f18572.add(new Evaluator.ContainsText(r0));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m24248(boolean z, boolean z2) {
        int i = 1;
        int i2 = 0;
        String r3 = Normalizer.m23523(this.f18574.m24032(")"));
        Matcher matcher = f18568.matcher(r3);
        Matcher matcher2 = f18569.matcher(r3);
        if ("odd".equals(r3)) {
            i2 = 1;
            i = 2;
        } else if ("even".equals(r3)) {
            i = 2;
        } else if (matcher.matches()) {
            if (matcher.group(3) != null) {
                i = Integer.parseInt(matcher.group(1).replaceFirst("^\\+", ""));
            }
            if (matcher.group(4) != null) {
                i2 = Integer.parseInt(matcher.group(4).replaceFirst("^\\+", ""));
            }
        } else if (matcher2.matches()) {
            i = 0;
            i2 = Integer.parseInt(matcher2.group().replaceFirst("^\\+", ""));
        } else {
            throw new Selector.SelectorParseException("Could not parse nth-index '%s': unexpected format", r3);
        }
        if (z2) {
            if (z) {
                this.f18572.add(new Evaluator.IsNthLastOfType(i, i2));
            } else {
                this.f18572.add(new Evaluator.IsNthOfType(i, i2));
            }
        } else if (z) {
            this.f18572.add(new Evaluator.IsNthLastChild(i, i2));
        } else {
            this.f18572.add(new Evaluator.IsNthChild(i, i2));
        }
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    private void m24249() {
        this.f18574.m24039(":not");
        String r0 = this.f18574.m24041('(', ')');
        Validate.m23518(r0, ":not(selector) subselect must not be empty");
        this.f18572.add(new StructuralEvaluator.Not(m24245(r0)));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Evaluator m24250() {
        this.f18574.m24033();
        if (this.f18574.m24045(f18571)) {
            this.f18572.add(new StructuralEvaluator.Root());
            m24246(this.f18574.m24037());
        } else {
            m24244();
        }
        while (!this.f18574.m24042()) {
            boolean r0 = this.f18574.m24033();
            if (this.f18574.m24045(f18571)) {
                m24246(this.f18574.m24037());
            } else if (r0) {
                m24246(' ');
            } else {
                m24244();
            }
        }
        return this.f18572.size() == 1 ? this.f18572.get(0) : new CombiningEvaluator.And((Collection<Evaluator>) this.f18572);
    }
}
