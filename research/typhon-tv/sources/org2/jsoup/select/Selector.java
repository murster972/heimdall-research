package org2.jsoup.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Element;

public class Selector {

    public static class SelectorParseException extends IllegalStateException {
        public SelectorParseException(String str, Object... objArr) {
            super(String.format(str, objArr));
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Element m24251(String str, Element element) {
        Validate.m23517(str);
        return Collector.m24170(QueryParser.m24245(str), element);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m24252(String str, Iterable<Element> iterable) {
        Validate.m23517(str);
        Validate.m23515((Object) iterable);
        Evaluator r1 = QueryParser.m24245(str);
        ArrayList arrayList = new ArrayList();
        IdentityHashMap identityHashMap = new IdentityHashMap();
        for (Element r0 : iterable) {
            Iterator it2 = m24255(r1, r0).iterator();
            while (it2.hasNext()) {
                Element element = (Element) it2.next();
                if (!identityHashMap.containsKey(element)) {
                    arrayList.add(element);
                    identityHashMap.put(element, Boolean.TRUE);
                }
            }
        }
        return new Elements((List<Element>) arrayList);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m24253(String str, Element element) {
        Validate.m23517(str);
        return m24255(QueryParser.m24245(str), element);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    static Elements m24254(Collection<Element> collection, Collection<Element> collection2) {
        boolean z;
        Elements elements = new Elements();
        for (Element next : collection) {
            Iterator<Element> it2 = collection2.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (next.equals(it2.next())) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                elements.add(next);
            }
        }
        return elements;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m24255(Evaluator evaluator, Element element) {
        Validate.m23515((Object) evaluator);
        Validate.m23515((Object) element);
        return Collector.m24171(evaluator, element);
    }
}
