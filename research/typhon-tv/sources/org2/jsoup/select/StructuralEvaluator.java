package org2.jsoup.select;

import java.util.Iterator;
import org2.jsoup.nodes.Element;

abstract class StructuralEvaluator extends Evaluator {

    /* renamed from: 龘  reason: contains not printable characters */
    Evaluator f18575;

    static class Has extends StructuralEvaluator {
        public Has(Evaluator evaluator) {
            this.f18575 = evaluator;
        }

        public String toString() {
            return String.format(":has(%s)", new Object[]{this.f18575});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24256(Element element, Element element2) {
            Iterator it2 = element2.m23666().iterator();
            while (it2.hasNext()) {
                Element element3 = (Element) it2.next();
                if (element3 != element2 && this.f18575.m24184(element, element3)) {
                    return true;
                }
            }
            return false;
        }
    }

    static class ImmediateParent extends StructuralEvaluator {
        public ImmediateParent(Evaluator evaluator) {
            this.f18575 = evaluator;
        }

        public String toString() {
            return String.format(":ImmediateParent%s", new Object[]{this.f18575});
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
            r1 = r5.m23637();
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m24257(org2.jsoup.nodes.Element r4, org2.jsoup.nodes.Element r5) {
            /*
                r3 = this;
                r0 = 0
                if (r4 != r5) goto L_0x0004
            L_0x0003:
                return r0
            L_0x0004:
                org2.jsoup.nodes.Element r1 = r5.m23637()
                if (r1 == 0) goto L_0x0003
                org2.jsoup.select.Evaluator r2 = r3.f18575
                boolean r1 = r2.m24184(r4, r1)
                if (r1 == 0) goto L_0x0003
                r0 = 1
                goto L_0x0003
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.select.StructuralEvaluator.ImmediateParent.m24257(org2.jsoup.nodes.Element, org2.jsoup.nodes.Element):boolean");
        }
    }

    static class ImmediatePreviousSibling extends StructuralEvaluator {
        public ImmediatePreviousSibling(Evaluator evaluator) {
            this.f18575 = evaluator;
        }

        public String toString() {
            return String.format(":prev%s", new Object[]{this.f18575});
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
            r1 = r5.m23664();
         */
        /* renamed from: 龘  reason: contains not printable characters */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean m24258(org2.jsoup.nodes.Element r4, org2.jsoup.nodes.Element r5) {
            /*
                r3 = this;
                r0 = 0
                if (r4 != r5) goto L_0x0004
            L_0x0003:
                return r0
            L_0x0004:
                org2.jsoup.nodes.Element r1 = r5.m23664()
                if (r1 == 0) goto L_0x0003
                org2.jsoup.select.Evaluator r2 = r3.f18575
                boolean r1 = r2.m24184(r4, r1)
                if (r1 == 0) goto L_0x0003
                r0 = 1
                goto L_0x0003
            */
            throw new UnsupportedOperationException("Method not decompiled: org2.jsoup.select.StructuralEvaluator.ImmediatePreviousSibling.m24258(org2.jsoup.nodes.Element, org2.jsoup.nodes.Element):boolean");
        }
    }

    static class Not extends StructuralEvaluator {
        public Not(Evaluator evaluator) {
            this.f18575 = evaluator;
        }

        public String toString() {
            return String.format(":not%s", new Object[]{this.f18575});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24259(Element element, Element element2) {
            return !this.f18575.m24184(element, element2);
        }
    }

    static class Parent extends StructuralEvaluator {
        public Parent(Evaluator evaluator) {
            this.f18575 = evaluator;
        }

        public String toString() {
            return String.format(":parent%s", new Object[]{this.f18575});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24260(Element element, Element element2) {
            if (element == element2) {
                return false;
            }
            for (Element r0 = element2.m23637(); !this.f18575.m24184(element, r0); r0 = r0.m23637()) {
                if (r0 == element) {
                    return false;
                }
            }
            return true;
        }
    }

    static class PreviousSibling extends StructuralEvaluator {
        public PreviousSibling(Evaluator evaluator) {
            this.f18575 = evaluator;
        }

        public String toString() {
            return String.format(":prev*%s", new Object[]{this.f18575});
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24261(Element element, Element element2) {
            if (element == element2) {
                return false;
            }
            for (Element r1 = element2.m23664(); r1 != null; r1 = r1.m23664()) {
                if (this.f18575.m24184(element, r1)) {
                    return true;
                }
            }
            return false;
        }
    }

    static class Root extends Evaluator {
        Root() {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24262(Element element, Element element2) {
            return element == element2;
        }
    }

    StructuralEvaluator() {
    }
}
