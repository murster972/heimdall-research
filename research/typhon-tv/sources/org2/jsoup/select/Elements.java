package org2.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org2.jsoup.helper.Validate;
import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.FormElement;

public class Elements extends ArrayList<Element> {
    public Elements() {
    }

    public Elements(int i) {
        super(i);
    }

    public Elements(Collection<Element> collection) {
        super(collection);
    }

    public Elements(List<Element> list) {
        super(list);
    }

    public Elements(Element... elementArr) {
        super(Arrays.asList(elementArr));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Elements m24183(String str, boolean z, boolean z2) {
        Elements elements = new Elements();
        Evaluator r1 = str != null ? QueryParser.m24245(str) : null;
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            do {
                element = z ? element.m23662() : element.m23664();
                if (element == null) {
                    break;
                } else if (r1 == null) {
                    elements.add(element);
                    continue;
                } else if (element.m23682(r1)) {
                    elements.add(element);
                    continue;
                } else {
                    continue;
                }
            } while (z2);
        }
        return elements;
    }

    public Elements addClass(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23644(str);
        }
        return this;
    }

    public Elements after(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23654(str);
        }
        return this;
    }

    public Elements append(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23658(str);
        }
        return this;
    }

    public String attr(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (element.m23764(str)) {
                return element.m23760(str);
            }
        }
        return "";
    }

    public Elements attr(String str, String str2) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23680(str, str2);
        }
        return this;
    }

    public Elements before(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23655(str);
        }
        return this;
    }

    public Elements clone() {
        Elements elements = new Elements(size());
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            elements.add(((Element) it2.next()).m23660());
        }
        return elements;
    }

    public List<String> eachAttr(String str) {
        ArrayList arrayList = new ArrayList(size());
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (element.m23764(str)) {
                arrayList.add(element.m23760(str));
            }
        }
        return arrayList;
    }

    public List<String> eachText() {
        ArrayList arrayList = new ArrayList(size());
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (element.m23688()) {
                arrayList.add(element.m23667());
            }
        }
        return arrayList;
    }

    public Elements empty() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23653();
        }
        return this;
    }

    public Elements eq(int i) {
        if (size() <= i) {
            return new Elements();
        }
        return new Elements((Element) get(i));
    }

    public Elements filter(NodeFilter nodeFilter) {
        NodeTraversor.m24226(nodeFilter, this);
        return this;
    }

    public Element first() {
        if (isEmpty()) {
            return null;
        }
        return (Element) get(0);
    }

    public List<FormElement> forms() {
        ArrayList arrayList = new ArrayList();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (element instanceof FormElement) {
                arrayList.add((FormElement) element);
            }
        }
        return arrayList;
    }

    public boolean hasAttr(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m23764(str)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasClass(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m23687(str)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasText() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m23688()) {
                return true;
            }
        }
        return false;
    }

    public String html() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(StringUtils.LF);
            }
            sb.append(element.m23630());
        }
        return sb.toString();
    }

    public Elements html(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23642(str);
        }
        return this;
    }

    public boolean is(String str) {
        Evaluator r1 = QueryParser.m24245(str);
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            if (((Element) it2.next()).m23682(r1)) {
                return true;
            }
        }
        return false;
    }

    public Element last() {
        if (isEmpty()) {
            return null;
        }
        return (Element) get(size() - 1);
    }

    public Elements next() {
        return m24183((String) null, true, false);
    }

    public Elements next(String str) {
        return m24183(str, true, false);
    }

    public Elements nextAll() {
        return m24183((String) null, true, true);
    }

    public Elements nextAll(String str) {
        return m24183(str, true, true);
    }

    public Elements not(String str) {
        return Selector.m24254((Collection<Element>) this, (Collection<Element>) Selector.m24252(str, (Iterable<Element>) this));
    }

    public String outerHtml() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(StringUtils.LF);
            }
            sb.append(element.m23727());
        }
        return sb.toString();
    }

    public Elements parents() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            linkedHashSet.addAll(((Element) it2.next()).m23639());
        }
        return new Elements((Collection<Element>) linkedHashSet);
    }

    public Elements prepend(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23659(str);
        }
        return this;
    }

    public Elements prev() {
        return m24183((String) null, false, false);
    }

    public Elements prev(String str) {
        return m24183(str, false, false);
    }

    public Elements prevAll() {
        return m24183((String) null, false, true);
    }

    public Elements prevAll(String str) {
        return m24183(str, false, true);
    }

    public Elements remove() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23740();
        }
        return this;
    }

    public Elements removeAttr(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23757(str);
        }
        return this;
    }

    public Elements removeClass(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23647(str);
        }
        return this;
    }

    public Elements select(String str) {
        return Selector.m24252(str, (Iterable<Element>) this);
    }

    public Elements tagName(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23629(str);
        }
        return this;
    }

    public String text() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(StringUtils.SPACE);
            }
            sb.append(element.m23667());
        }
        return sb.toString();
    }

    public String toString() {
        return outerHtml();
    }

    public Elements toggleClass(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23649(str);
        }
        return this;
    }

    public Elements traverse(NodeVisitor nodeVisitor) {
        NodeTraversor.m24228(nodeVisitor, this);
        return this;
    }

    public Elements unwrap() {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23745();
        }
        return this;
    }

    public String val() {
        return size() > 0 ? first().m23632() : "";
    }

    public Elements val(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23638(str);
        }
        return this;
    }

    public Elements wrap(String str) {
        Validate.m23517(str);
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            ((Element) it2.next()).m23650(str);
        }
        return this;
    }
}
