package org2.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.apache.commons.lang3.StringUtils;
import org2.jsoup.helper.StringUtil;
import org2.jsoup.nodes.Element;

abstract class CombiningEvaluator extends Evaluator {

    /* renamed from: 靐  reason: contains not printable characters */
    int f18547;

    /* renamed from: 龘  reason: contains not printable characters */
    final ArrayList<Evaluator> f18548;

    static final class And extends CombiningEvaluator {
        And(Collection<Evaluator> collection) {
            super(collection);
        }

        And(Evaluator... evaluatorArr) {
            this((Collection<Evaluator>) Arrays.asList(evaluatorArr));
        }

        public String toString() {
            return StringUtil.m23504((Collection) this.f18548, StringUtils.SPACE);
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24180(Element element, Element element2) {
            for (int i = 0; i < this.f18547; i++) {
                if (!((Evaluator) this.f18548.get(i)).m24184(element, element2)) {
                    return false;
                }
            }
            return true;
        }
    }

    static final class Or extends CombiningEvaluator {
        Or() {
        }

        Or(Collection<Evaluator> collection) {
            if (this.f18547 > 1) {
                this.f18548.add(new And(collection));
            } else {
                this.f18548.addAll(collection);
            }
            m24177();
        }

        Or(Evaluator... evaluatorArr) {
            this((Collection<Evaluator>) Arrays.asList(evaluatorArr));
        }

        public String toString() {
            return StringUtil.m23504((Collection) this.f18548, ", ");
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24181(Evaluator evaluator) {
            this.f18548.add(evaluator);
            m24177();
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public boolean m24182(Element element, Element element2) {
            for (int i = 0; i < this.f18547; i++) {
                if (((Evaluator) this.f18548.get(i)).m24184(element, element2)) {
                    return true;
                }
            }
            return false;
        }
    }

    CombiningEvaluator() {
        this.f18547 = 0;
        this.f18548 = new ArrayList<>();
    }

    CombiningEvaluator(Collection<Evaluator> collection) {
        this();
        this.f18548.addAll(collection);
        m24177();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m24177() {
        this.f18547 = this.f18548.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public Evaluator m24178() {
        if (this.f18547 > 0) {
            return this.f18548.get(this.f18547 - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m24179(Evaluator evaluator) {
        this.f18548.set(this.f18547 - 1, evaluator);
    }
}
