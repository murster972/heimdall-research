package org2.jsoup.select;

import org2.jsoup.nodes.Element;
import org2.jsoup.nodes.Node;
import org2.jsoup.select.NodeFilter;

public class Collector {

    private static class Accumulator implements NodeVisitor {

        /* renamed from: 靐  reason: contains not printable characters */
        private final Elements f18541;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Evaluator f18542;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Element f18543;

        Accumulator(Element element, Elements elements, Evaluator evaluator) {
            this.f18543 = element;
            this.f18541 = elements;
            this.f18542 = evaluator;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m24172(Node node, int i) {
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m24173(Node node, int i) {
            if (node instanceof Element) {
                Element element = (Element) node;
                if (this.f18542.m24184(this.f18543, element)) {
                    this.f18541.add(element);
                }
            }
        }
    }

    private static class FirstFinder implements NodeFilter {
        /* access modifiers changed from: private */

        /* renamed from: 靐  reason: contains not printable characters */
        public Element f18544 = null;

        /* renamed from: 齉  reason: contains not printable characters */
        private final Evaluator f18545;

        /* renamed from: 龘  reason: contains not printable characters */
        private final Element f18546;

        FirstFinder(Element element, Evaluator evaluator) {
            this.f18546 = element;
            this.f18545 = evaluator;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public NodeFilter.FilterResult m24175(Node node, int i) {
            return NodeFilter.FilterResult.CONTINUE;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public NodeFilter.FilterResult m24176(Node node, int i) {
            if (node instanceof Element) {
                Element element = (Element) node;
                if (this.f18545.m24184(this.f18546, element)) {
                    this.f18544 = element;
                    return NodeFilter.FilterResult.STOP;
                }
            }
            return NodeFilter.FilterResult.CONTINUE;
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static Element m24170(Evaluator evaluator, Element element) {
        FirstFinder firstFinder = new FirstFinder(element, evaluator);
        NodeTraversor.m24225((NodeFilter) firstFinder, (Node) element);
        return firstFinder.f18544;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static Elements m24171(Evaluator evaluator, Element element) {
        Elements elements = new Elements();
        NodeTraversor.m24227((NodeVisitor) new Accumulator(element, elements, evaluator), (Node) element);
        return elements;
    }
}
