package org2.jsoup.select;

import org2.jsoup.nodes.Node;

public interface NodeFilter {

    public enum FilterResult {
        CONTINUE,
        SKIP_CHILDREN,
        SKIP_ENTIRELY,
        REMOVE,
        STOP
    }

    /* renamed from: 靐  reason: contains not printable characters */
    FilterResult m24223(Node node, int i);

    /* renamed from: 龘  reason: contains not printable characters */
    FilterResult m24224(Node node, int i);
}
