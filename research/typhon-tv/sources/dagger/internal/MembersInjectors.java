package dagger.internal;

import dagger.MembersInjector;

public final class MembersInjectors {

    private enum NoOpMembersInjector implements MembersInjector<Object> {
        INSTANCE;

        public void injectMembers(Object obj) {
            Preconditions.m18237(obj);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> MembersInjector<T> m18235() {
        return NoOpMembersInjector.INSTANCE;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m18236(MembersInjector<T> membersInjector, T t) {
        membersInjector.injectMembers(t);
        return t;
    }
}
