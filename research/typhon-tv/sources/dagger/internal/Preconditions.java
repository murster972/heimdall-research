package dagger.internal;

public final class Preconditions {
    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m18237(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> T m18238(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }
}
