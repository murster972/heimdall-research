package dagger.internal;

import dagger.Lazy;
import javax.inject.Provider;

public final class DoubleCheck<T> implements Lazy<T>, Provider<T> {

    /* renamed from: 靐  reason: contains not printable characters */
    private static final Object f14454 = new Object();

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ boolean f14455 = (!DoubleCheck.class.desiredAssertionStatus());

    /* renamed from: 麤  reason: contains not printable characters */
    private volatile Object f14456 = f14454;

    /* renamed from: 齉  reason: contains not printable characters */
    private volatile Provider<T> f14457;

    private DoubleCheck(Provider<T> provider) {
        if (f14455 || provider != null) {
            this.f14457 = provider;
            return;
        }
        throw new AssertionError();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static <T> Lazy<T> m18233(Provider<T> provider) {
        return provider instanceof Lazy ? (Lazy) provider : new DoubleCheck((Provider) Preconditions.m18237(provider));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static <T> Provider<T> m18234(Provider<T> provider) {
        Preconditions.m18237(provider);
        return provider instanceof DoubleCheck ? provider : new DoubleCheck(provider);
    }

    public T get() {
        T t = this.f14456;
        if (t == f14454) {
            synchronized (this) {
                t = this.f14456;
                if (t == f14454) {
                    t = this.f14457.get();
                    T t2 = this.f14456;
                    if (t2 == f14454 || t2 == t) {
                        this.f14456 = t;
                        this.f14457 = null;
                    } else {
                        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + t2 + " & " + t);
                    }
                }
            }
        }
        return t;
    }
}
