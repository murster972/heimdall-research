package dagger.internal;

import javax.inject.Provider;

public final class DelegateFactory<T> implements Factory<T> {

    /* renamed from: 龘  reason: contains not printable characters */
    private Provider<T> f14453;

    public T get() {
        if (this.f14453 != null) {
            return this.f14453.get();
        }
        throw new IllegalStateException();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m18232(Provider<T> provider) {
        if (provider == null) {
            throw new IllegalArgumentException();
        } else if (this.f14453 != null) {
            throw new IllegalStateException();
        } else {
            this.f14453 = provider;
        }
    }
}
