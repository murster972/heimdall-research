package cat.ereza.customactivityoncrash.config;

import android.app.Activity;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import java.io.Serializable;
import java.lang.reflect.Modifier;

public class CaocConfig implements Serializable {
    public static final int BACKGROUND_MODE_CRASH = 2;
    public static final int BACKGROUND_MODE_SHOW_CUSTOM = 1;
    public static final int BACKGROUND_MODE_SILENT = 0;
    /* access modifiers changed from: private */
    public int backgroundMode = 1;
    /* access modifiers changed from: private */
    public boolean enabled = true;
    /* access modifiers changed from: private */
    public Class<? extends Activity> errorActivityClass = null;
    /* access modifiers changed from: private */
    public Integer errorDrawable = null;
    /* access modifiers changed from: private */
    public CustomActivityOnCrash.EventListener eventListener = null;
    /* access modifiers changed from: private */
    public boolean logErrorOnRestart = true;
    /* access modifiers changed from: private */
    public int minTimeBetweenCrashesMs = 3000;
    /* access modifiers changed from: private */
    public Class<? extends Activity> restartActivityClass = null;
    /* access modifiers changed from: private */
    public boolean showErrorDetails = true;
    /* access modifiers changed from: private */
    public boolean showRestartButton = true;
    /* access modifiers changed from: private */
    public boolean trackActivities = false;

    public static class Builder {

        /* renamed from: 龘  reason: contains not printable characters */
        private CaocConfig f1905;

        /* renamed from: 龘  reason: contains not printable characters */
        public static Builder m2291() {
            Builder builder = new Builder();
            CaocConfig r2 = CustomActivityOnCrash.m2254();
            CaocConfig caocConfig = new CaocConfig();
            int unused = caocConfig.backgroundMode = r2.backgroundMode;
            boolean unused2 = caocConfig.enabled = r2.enabled;
            boolean unused3 = caocConfig.showErrorDetails = r2.showErrorDetails;
            boolean unused4 = caocConfig.showRestartButton = r2.showRestartButton;
            boolean unused5 = caocConfig.logErrorOnRestart = r2.logErrorOnRestart;
            boolean unused6 = caocConfig.trackActivities = r2.trackActivities;
            int unused7 = caocConfig.minTimeBetweenCrashesMs = r2.minTimeBetweenCrashesMs;
            Integer unused8 = caocConfig.errorDrawable = r2.errorDrawable;
            Class unused9 = caocConfig.errorActivityClass = r2.errorActivityClass;
            Class unused10 = caocConfig.restartActivityClass = r2.restartActivityClass;
            CustomActivityOnCrash.EventListener unused11 = caocConfig.eventListener = r2.eventListener;
            builder.f1905 = caocConfig;
            return builder;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public Builder m2292(boolean z) {
            boolean unused = this.f1905.showErrorDetails = z;
            return this;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m2293() {
            CustomActivityOnCrash.m2264(this.f1905);
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public Builder m2294(boolean z) {
            boolean unused = this.f1905.showRestartButton = z;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m2295(int i) {
            int unused = this.f1905.backgroundMode = i;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m2296(CustomActivityOnCrash.EventListener eventListener) {
            if (eventListener == null || eventListener.getClass().getEnclosingClass() == null || Modifier.isStatic(eventListener.getClass().getModifiers())) {
                CustomActivityOnCrash.EventListener unused = this.f1905.eventListener = eventListener;
                return this;
            }
            throw new IllegalArgumentException("The event listener cannot be an inner or anonymous class, because it will need to be serialized. Change it to a class of its own, or make it a static inner class.");
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m2297(Integer num) {
            Integer unused = this.f1905.errorDrawable = num;
            return this;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public Builder m2298(boolean z) {
            boolean unused = this.f1905.enabled = z;
            return this;
        }
    }

    public int getBackgroundMode() {
        return this.backgroundMode;
    }

    public Class<? extends Activity> getErrorActivityClass() {
        return this.errorActivityClass;
    }

    public Integer getErrorDrawable() {
        return this.errorDrawable;
    }

    public CustomActivityOnCrash.EventListener getEventListener() {
        return this.eventListener;
    }

    public int getMinTimeBetweenCrashesMs() {
        return this.minTimeBetweenCrashesMs;
    }

    public Class<? extends Activity> getRestartActivityClass() {
        return this.restartActivityClass;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public boolean isLogErrorOnRestart() {
        return this.logErrorOnRestart;
    }

    public boolean isShowErrorDetails() {
        return this.showErrorDetails;
    }

    public boolean isShowRestartButton() {
        return this.showRestartButton;
    }

    public boolean isTrackActivities() {
        return this.trackActivities;
    }

    public void setBackgroundMode(int i) {
        this.backgroundMode = i;
    }

    public void setEnabled(boolean z) {
        this.enabled = z;
    }

    public void setErrorActivityClass(Class<? extends Activity> cls) {
        this.errorActivityClass = cls;
    }

    public void setErrorDrawable(Integer num) {
        this.errorDrawable = num;
    }

    public void setEventListener(CustomActivityOnCrash.EventListener eventListener2) {
        this.eventListener = eventListener2;
    }

    public void setLogErrorOnRestart(boolean z) {
        this.logErrorOnRestart = z;
    }

    public void setMinTimeBetweenCrashesMs(int i) {
        this.minTimeBetweenCrashesMs = i;
    }

    public void setRestartActivityClass(Class<? extends Activity> cls) {
        this.restartActivityClass = cls;
    }

    public void setShowErrorDetails(boolean z) {
        this.showErrorDetails = z;
    }

    public void setShowRestartButton(boolean z) {
        this.showRestartButton = z;
    }

    public void setTrackActivities(boolean z) {
        this.trackActivities = z;
    }
}
