package cat.ereza.customactivityoncrash.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.R;
import cat.ereza.customactivityoncrash.config.CaocConfig;

public final class DefaultErrorActivity extends AppCompatActivity {
    /* access modifiers changed from: private */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m2267() {
        String r2 = CustomActivityOnCrash.m2255((Context) this, getIntent());
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService("clipboard");
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(ClipData.newPlainText(getString(R.string.customactivityoncrash_error_activity_error_details_clipboard_label), r2));
            Toast.makeText(this, R.string.customactivityoncrash_error_activity_error_details_copied, 0).show();
        }
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"PrivateResource"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        TypedArray obtainStyledAttributes = obtainStyledAttributes(R.styleable.AppCompatTheme);
        if (!obtainStyledAttributes.hasValue(R.styleable.AppCompatTheme_windowActionBar)) {
            setTheme(R.style.Theme_AppCompat_Light_DarkActionBar);
        }
        obtainStyledAttributes.recycle();
        setContentView(R.layout.customactivityoncrash_default_error_activity);
        Button button = (Button) findViewById(R.id.customactivityoncrash_error_activity_restart_button);
        final CaocConfig r1 = CustomActivityOnCrash.m2244(getIntent());
        if (r1 == null) {
            finish();
            return;
        }
        if (!r1.isShowRestartButton() || r1.getRestartActivityClass() == null) {
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    CustomActivityOnCrash.m2245((Activity) DefaultErrorActivity.this, r1);
                }
            });
        } else {
            button.setText(R.string.customactivityoncrash_error_activity_restart_app);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    CustomActivityOnCrash.m2261((Activity) DefaultErrorActivity.this, r1);
                }
            });
        }
        Button button2 = (Button) findViewById(R.id.customactivityoncrash_error_activity_more_info_button);
        if (r1.isShowErrorDetails()) {
            button2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    TextView textView = (TextView) new AlertDialog.Builder(DefaultErrorActivity.this).m530(R.string.customactivityoncrash_error_activity_error_details_title).m523((CharSequence) CustomActivityOnCrash.m2255((Context) DefaultErrorActivity.this, DefaultErrorActivity.this.getIntent())).m531(R.string.customactivityoncrash_error_activity_error_details_close, (DialogInterface.OnClickListener) null).m527(R.string.customactivityoncrash_error_activity_error_details_copy, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            DefaultErrorActivity.this.m2267();
                        }
                    }).m528().findViewById(16908299);
                    if (textView != null) {
                        textView.setTextSize(0, DefaultErrorActivity.this.getResources().getDimension(R.dimen.customactivityoncrash_error_activity_error_details_text_size));
                    }
                }
            });
        } else {
            button2.setVisibility(8);
        }
        Integer errorDrawable = r1.getErrorDrawable();
        ImageView imageView = (ImageView) findViewById(R.id.customactivityoncrash_error_activity_image);
        if (errorDrawable != null) {
            imageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), errorDrawable.intValue(), getTheme()));
        }
    }
}
