package cat.ereza.customactivityoncrash;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.RestrictTo;
import android.util.Log;
import cat.ereza.customactivityoncrash.activity.DefaultErrorActivity;
import cat.ereza.customactivityoncrash.config.CaocConfig;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.Thread;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.Date;
import java.util.Deque;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipFile;
import org.apache.commons.lang3.StringUtils;

public final class CustomActivityOnCrash {
    /* access modifiers changed from: private */

    /* renamed from: 连任  reason: contains not printable characters */
    public static boolean f1891 = true;
    /* access modifiers changed from: private */

    /* renamed from: 靐  reason: contains not printable characters */
    public static CaocConfig f1892 = new CaocConfig();
    /* access modifiers changed from: private */

    /* renamed from: 麤  reason: contains not printable characters */
    public static WeakReference<Activity> f1893 = new WeakReference<>((Object) null);
    /* access modifiers changed from: private */

    /* renamed from: 齉  reason: contains not printable characters */
    public static final Deque<String> f1894 = new ArrayDeque(50);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: 龘  reason: contains not printable characters */
    public static Application f1895;

    public interface EventListener extends Serializable {
        void onCloseAppFromErrorActivity();

        void onLaunchErrorActivity();

        void onRestartAppFromErrorActivity();
    }

    /* access modifiers changed from: private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public static Class<? extends Activity> m2230(Context context) {
        Class<? extends Activity> r0 = m2232(context);
        return r0 == null ? m2234(context) : r0;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Class<? extends Activity> m2232(Context context) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent().setAction("cat.ereza.customactivityoncrash.RESTART").setPackage(context.getPackageName()), 64);
        if (queryIntentActivities != null && queryIntentActivities.size() > 0) {
            try {
                return Class.forName(queryIntentActivities.get(0).activityInfo.name);
            } catch (ClassNotFoundException e) {
                Log.e("CustomActivityOnCrash", "Failed when resolving the restart activity class via intent filter, stack trace follows!", e);
            }
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static Class<? extends Activity> m2234(Context context) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if (!(launchIntentForPackage == null || launchIntentForPackage.getComponent() == null)) {
            try {
                return Class.forName(launchIntentForPackage.getComponent().getClassName());
            } catch (ClassNotFoundException e) {
                Log.e("CustomActivityOnCrash", "Failed when resolving the restart activity class via getLaunchIntentForPackage, stack trace follows!", e);
            }
        }
        return null;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static String m2235() {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        return str2.startsWith(str) ? m2258(str2) : m2258(str) + StringUtils.SPACE + str2;
    }

    /* access modifiers changed from: private */
    /* renamed from: ˈ  reason: contains not printable characters */
    public static boolean m2236(Context context) {
        long r2 = m2240(context);
        long time = new Date().getTime();
        return r2 <= time && time - r2 < ((long) f1892.getMinTimeBetweenCrashesMs());
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public static Class<? extends Activity> m2237(Context context) {
        Class<? extends Activity> r0 = m2239(context);
        return r0 == null ? DefaultErrorActivity.class : r0;
    }

    /* access modifiers changed from: private */
    /* renamed from: ˑ  reason: contains not printable characters */
    public static void m2238() {
        Process.killProcess(Process.myPid());
        System.exit(10);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    private static Class<? extends Activity> m2239(Context context) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent().setAction("cat.ereza.customactivityoncrash.ERROR").setPackage(context.getPackageName()), 64);
        if (queryIntentActivities != null && queryIntentActivities.size() > 0) {
            try {
                return Class.forName(queryIntentActivities.get(0).activityInfo.name);
            } catch (ClassNotFoundException e) {
                Log.e("CustomActivityOnCrash", "Failed when resolving the error activity class via intent filter, stack trace follows!", e);
            }
        }
        return null;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private static long m2240(Context context) {
        return context.getSharedPreferences("custom_activity_on_crash", 0).getLong("last_crash_timestamp", -1);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private static String m2241(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            return "Unknown";
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static CaocConfig m2244(Intent intent) {
        CaocConfig caocConfig = (CaocConfig) intent.getSerializableExtra("cat.ereza.customactivityoncrash.EXTRA_CONFIG");
        if (caocConfig.isLogErrorOnRestart() && m2257(intent) != null) {
            Log.e("CustomActivityOnCrash", "The previous app process crashed. This is the stack trace of the crash:\n" + m2257(intent));
        }
        return caocConfig;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static void m2245(Activity activity, CaocConfig caocConfig) {
        if (caocConfig.getEventListener() != null) {
            caocConfig.getEventListener().onCloseAppFromErrorActivity();
        }
        activity.finish();
        m2238();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"ApplySharedPref"})
    /* renamed from: 靐  reason: contains not printable characters */
    public static void m2246(Context context, long j) {
        context.getSharedPreferences("custom_activity_on_crash", 0).edit().putLong("last_crash_timestamp", j).commit();
    }

    /* access modifiers changed from: private */
    /* renamed from: 靐  reason: contains not printable characters */
    public static boolean m2248(Throwable th, Class<? extends Activity> cls) {
        do {
            for (StackTraceElement stackTraceElement : th.getStackTrace()) {
                if ((stackTraceElement.getClassName().equals("android.app.ActivityThread") && stackTraceElement.getMethodName().equals("handleBindApplication")) || stackTraceElement.getClassName().equals(cls.getName())) {
                    return true;
                }
            }
            th = th.getCause();
        } while (th != null);
        return false;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static String m2253(Intent intent) {
        return intent.getStringExtra("cat.ereza.customactivityoncrash.EXTRA_ACTIVITY_LOG");
    }

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public static CaocConfig m2254() {
        return f1892;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m2255(Context context, Intent intent) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String r1 = m2256(context, (DateFormat) simpleDateFormat);
        String str = "" + "Build version: " + m2241(context) + " \n";
        if (r1 != null) {
            str = str + "Build date: " + r1 + " \n";
        }
        String str2 = (((str + "Current date: " + simpleDateFormat.format(date) + " \n") + "Device: " + m2235() + " \n \n") + "Stack trace:  \n") + m2257(intent);
        String r0 = m2253(intent);
        if (r0 == null) {
            return str2;
        }
        return (str2 + "\nUser actions: \n") + r0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m2256(Context context, DateFormat dateFormat) {
        long j;
        try {
            ZipFile zipFile = new ZipFile(context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir);
            j = zipFile.getEntry("classes.dex").getTime();
            zipFile.close();
        } catch (Exception e) {
            j = 0;
        }
        if (j > 312764400000L) {
            return dateFormat.format(new Date(j));
        }
        return null;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static String m2257(Intent intent) {
        return intent.getStringExtra("cat.ereza.customactivityoncrash.EXTRA_STACK_TRACE");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static String m2258(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        char charAt = str.charAt(0);
        return !Character.isUpperCase(charAt) ? Character.toUpperCase(charAt) + str.substring(1) : str;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2260(Activity activity, Intent intent, CaocConfig caocConfig) {
        intent.addFlags(270565376);
        if (intent.getComponent() != null) {
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");
        }
        if (caocConfig.getEventListener() != null) {
            caocConfig.getEventListener().onRestartAppFromErrorActivity();
        }
        activity.finish();
        activity.startActivity(intent);
        m2238();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2261(Activity activity, CaocConfig caocConfig) {
        m2260(activity, new Intent(activity, caocConfig.getRestartActivityClass()), caocConfig);
    }

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2262(Context context) {
        if (context == null) {
            try {
                Log.e("CustomActivityOnCrash", "Install failed: context is null!");
            } catch (Throwable th) {
                Log.e("CustomActivityOnCrash", "An unknown error occurred while installing CustomActivityOnCrash, it may not have been properly initialized. Please report this as a bug if needed.", th);
            }
        } else {
            final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtExceptionHandler == null || !defaultUncaughtExceptionHandler.getClass().getName().startsWith("cat.ereza.customactivityoncrash")) {
                if (defaultUncaughtExceptionHandler != null) {
                    if (!defaultUncaughtExceptionHandler.getClass().getName().startsWith("com.android.internal.os")) {
                        Log.e("CustomActivityOnCrash", "IMPORTANT WARNING! You already have an UncaughtExceptionHandler, are you sure this is correct? If you use a custom UncaughtExceptionHandler, you must initialize it AFTER CustomActivityOnCrash! Installing anyway, but your original handler will not be called.");
                    }
                }
                f1895 = (Application) context.getApplicationContext();
                Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    public void uncaughtException(Thread thread, Throwable th) {
                        if (CustomActivityOnCrash.f1892.isEnabled()) {
                            Log.e("CustomActivityOnCrash", "App has crashed, executing CustomActivityOnCrash's UncaughtExceptionHandler", th);
                            if (CustomActivityOnCrash.m2236(CustomActivityOnCrash.f1895)) {
                                Log.e("CustomActivityOnCrash", "App already crashed recently, not starting custom error activity because we could enter a restart loop. Are you sure that your app does not crash directly on init?", th);
                                if (defaultUncaughtExceptionHandler != null) {
                                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                                    return;
                                }
                            } else {
                                CustomActivityOnCrash.m2246((Context) CustomActivityOnCrash.f1895, new Date().getTime());
                                Class<? extends Activity> errorActivityClass = CustomActivityOnCrash.f1892.getErrorActivityClass();
                                if (errorActivityClass == null) {
                                    errorActivityClass = CustomActivityOnCrash.m2237(CustomActivityOnCrash.f1895);
                                }
                                if (CustomActivityOnCrash.m2248(th, errorActivityClass)) {
                                    Log.e("CustomActivityOnCrash", "Your application class or your error activity have crashed, the custom activity will not be launched!");
                                    if (defaultUncaughtExceptionHandler != null) {
                                        defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                                        return;
                                    }
                                } else if (CustomActivityOnCrash.f1892.getBackgroundMode() == 1 || !CustomActivityOnCrash.f1891) {
                                    Intent intent = new Intent(CustomActivityOnCrash.f1895, errorActivityClass);
                                    StringWriter stringWriter = new StringWriter();
                                    th.printStackTrace(new PrintWriter(stringWriter));
                                    String stringWriter2 = stringWriter.toString();
                                    if (stringWriter2.length() > 131071) {
                                        stringWriter2 = stringWriter2.substring(0, 131071 - " [stack trace too large]".length()) + " [stack trace too large]";
                                    }
                                    intent.putExtra("cat.ereza.customactivityoncrash.EXTRA_STACK_TRACE", stringWriter2);
                                    if (CustomActivityOnCrash.f1892.isTrackActivities()) {
                                        StringBuilder sb = new StringBuilder();
                                        while (!CustomActivityOnCrash.f1894.isEmpty()) {
                                            sb.append((String) CustomActivityOnCrash.f1894.poll());
                                        }
                                        intent.putExtra("cat.ereza.customactivityoncrash.EXTRA_ACTIVITY_LOG", sb.toString());
                                    }
                                    if (CustomActivityOnCrash.f1892.isShowRestartButton() && CustomActivityOnCrash.f1892.getRestartActivityClass() == null) {
                                        CustomActivityOnCrash.f1892.setRestartActivityClass(CustomActivityOnCrash.m2230(CustomActivityOnCrash.f1895));
                                    }
                                    intent.putExtra("cat.ereza.customactivityoncrash.EXTRA_CONFIG", CustomActivityOnCrash.f1892);
                                    intent.setFlags(268468224);
                                    if (CustomActivityOnCrash.f1892.getEventListener() != null) {
                                        CustomActivityOnCrash.f1892.getEventListener().onLaunchErrorActivity();
                                    }
                                    CustomActivityOnCrash.f1895.startActivity(intent);
                                } else if (CustomActivityOnCrash.f1892.getBackgroundMode() == 2 && defaultUncaughtExceptionHandler != null) {
                                    defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                                    return;
                                }
                            }
                            Activity activity = (Activity) CustomActivityOnCrash.f1893.get();
                            if (activity != null) {
                                activity.finish();
                                CustomActivityOnCrash.f1893.clear();
                            }
                            CustomActivityOnCrash.m2238();
                        } else if (defaultUncaughtExceptionHandler != null) {
                            defaultUncaughtExceptionHandler.uncaughtException(thread, th);
                        }
                    }
                });
                f1895.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {

                    /* renamed from: 靐  reason: contains not printable characters */
                    final DateFormat f1897 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

                    /* renamed from: 龘  reason: contains not printable characters */
                    int f1898 = 0;

                    public void onActivityCreated(Activity activity, Bundle bundle) {
                        if (activity.getClass() != CustomActivityOnCrash.f1892.getErrorActivityClass()) {
                            WeakReference unused = CustomActivityOnCrash.f1893 = new WeakReference(activity);
                        }
                        if (CustomActivityOnCrash.f1892.isTrackActivities()) {
                            CustomActivityOnCrash.f1894.add(this.f1897.format(new Date()) + ": " + activity.getClass().getSimpleName() + " created\n");
                        }
                    }

                    public void onActivityDestroyed(Activity activity) {
                        if (CustomActivityOnCrash.f1892.isTrackActivities()) {
                            CustomActivityOnCrash.f1894.add(this.f1897.format(new Date()) + ": " + activity.getClass().getSimpleName() + " destroyed\n");
                        }
                    }

                    public void onActivityPaused(Activity activity) {
                        if (CustomActivityOnCrash.f1892.isTrackActivities()) {
                            CustomActivityOnCrash.f1894.add(this.f1897.format(new Date()) + ": " + activity.getClass().getSimpleName() + " paused\n");
                        }
                    }

                    public void onActivityResumed(Activity activity) {
                        if (CustomActivityOnCrash.f1892.isTrackActivities()) {
                            CustomActivityOnCrash.f1894.add(this.f1897.format(new Date()) + ": " + activity.getClass().getSimpleName() + " resumed\n");
                        }
                    }

                    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    }

                    public void onActivityStarted(Activity activity) {
                        this.f1898++;
                        boolean unused = CustomActivityOnCrash.f1891 = this.f1898 == 0;
                    }

                    public void onActivityStopped(Activity activity) {
                        this.f1898--;
                        boolean unused = CustomActivityOnCrash.f1891 = this.f1898 == 0;
                    }
                });
            } else {
                Log.e("CustomActivityOnCrash", "CustomActivityOnCrash was already installed, doing nothing!");
            }
            Log.i("CustomActivityOnCrash", "CustomActivityOnCrash has been installed.");
        }
    }

    @RestrictTo
    /* renamed from: 龘  reason: contains not printable characters */
    public static void m2264(CaocConfig caocConfig) {
        f1892 = caocConfig;
    }
}
