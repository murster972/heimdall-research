package junrar.util;

public class VolumeHelper {
    /* renamed from: 龘  reason: contains not printable characters */
    public static String m19855(String str, boolean z) {
        if (!z) {
            int length = str.length();
            int i = length - 1;
            while (i >= 0 && !m19856(str.charAt(i))) {
                i--;
            }
            int i2 = i + 1;
            int i3 = i - 1;
            while (i3 >= 0 && m19856(str.charAt(i3))) {
                i3--;
            }
            if (i3 < 0) {
                return null;
            }
            int i4 = i3 + 1;
            StringBuilder sb = new StringBuilder(length);
            sb.append(str, 0, i4);
            char[] cArr = new char[((i - i4) + 1)];
            str.getChars(i4, i + 1, cArr, 0);
            int length2 = cArr.length - 1;
            while (length2 >= 0) {
                char c = (char) (cArr[length2] + 1);
                cArr[length2] = c;
                if (c != ':') {
                    break;
                }
                cArr[length2] = '0';
                length2--;
            }
            if (length2 < 0) {
                sb.append('1');
            }
            sb.append(cArr);
            sb.append(str, i2, length);
            return sb.toString();
        }
        int length3 = str.length();
        if (length3 <= 4 || str.charAt(length3 - 4) != '.') {
            return null;
        }
        StringBuilder sb2 = new StringBuilder();
        int i5 = length3 - 3;
        sb2.append(str, 0, i5);
        if (!m19856(str.charAt(i5 + 1)) || !m19856(str.charAt(i5 + 2))) {
            sb2.append("r00");
        } else {
            char[] cArr2 = new char[3];
            str.getChars(i5, length3, cArr2, 0);
            int length4 = cArr2.length - 1;
            while (true) {
                char c2 = (char) (cArr2[length4] + 1);
                cArr2[length4] = c2;
                if (c2 != ':') {
                    break;
                }
                cArr2[length4] = '0';
                length4--;
            }
            sb2.append(cArr2);
        }
        return sb2.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private static boolean m19856(char c) {
        return c >= '0' && c <= '9';
    }
}
