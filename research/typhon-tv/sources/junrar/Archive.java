package junrar;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import junrar.exception.RarException;
import junrar.impl.FileVolumeManager;
import junrar.io.IReadOnlyAccess;
import junrar.rarfile.AVHeader;
import junrar.rarfile.BaseBlock;
import junrar.rarfile.BlockHeader;
import junrar.rarfile.CommentHeader;
import junrar.rarfile.EAHeader;
import junrar.rarfile.EndArcHeader;
import junrar.rarfile.FileHeader;
import junrar.rarfile.MacInfoHeader;
import junrar.rarfile.MainHeader;
import junrar.rarfile.MarkHeader;
import junrar.rarfile.ProtectHeader;
import junrar.rarfile.SignHeader;
import junrar.rarfile.SubBlockHeader;
import junrar.rarfile.UnixOwnersHeader;
import junrar.rarfile.UnrarHeadertype;
import junrar.unpack.ComprDataIO;
import junrar.unpack.Unpack;

public class Archive implements Closeable {

    /* renamed from: 龘  reason: contains not printable characters */
    private static Logger f15496 = Logger.getLogger(Archive.class.getName());

    /* renamed from: ʻ  reason: contains not printable characters */
    private final List<BaseBlock> f15497;

    /* renamed from: ʼ  reason: contains not printable characters */
    private boolean f15498;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MarkHeader f15499;

    /* renamed from: ʾ  reason: contains not printable characters */
    private VolumeManager f15500;

    /* renamed from: ʿ  reason: contains not printable characters */
    private Volume f15501;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f15502;

    /* renamed from: ˑ  reason: contains not printable characters */
    private MainHeader f15503;

    /* renamed from: ٴ  reason: contains not printable characters */
    private Unpack f15504;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f15505;

    /* renamed from: 连任  reason: contains not printable characters */
    private final ComprDataIO f15506;

    /* renamed from: 靐  reason: contains not printable characters */
    private IReadOnlyAccess f15507;

    /* renamed from: 麤  reason: contains not printable characters */
    private final UnrarCallback f15508;

    /* renamed from: 齉  reason: contains not printable characters */
    private long f15509;

    public Archive(File file) throws RarException, IOException {
        this(new FileVolumeManager(file), (UnrarCallback) null);
    }

    public Archive(VolumeManager volumeManager, UnrarCallback unrarCallback) throws RarException, IOException {
        this.f15497 = new ArrayList();
        this.f15499 = null;
        this.f15503 = null;
        this.f15505 = 0;
        this.f15502 = 0;
        this.f15500 = volumeManager;
        this.f15508 = unrarCallback;
        m19468(this.f15500.m19476(this, (Volume) null));
        this.f15506 = new ComprDataIO(this);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19456(HeaderCallback headerCallback) throws IOException, RarException {
        EndArcHeader endArcHeader;
        this.f15499 = null;
        this.f15503 = null;
        this.f15497.clear();
        while (true) {
            byte[] bArr = new byte[7];
            long r26 = this.f15507.m19483();
            if (r26 < this.f15509 && this.f15507.m19484(bArr, 7) != 0) {
                BaseBlock baseBlock = new BaseBlock(bArr);
                baseBlock.m19508(r26);
                switch (baseBlock.m19502()) {
                    case MarkHeader:
                        this.f15499 = new MarkHeader(baseBlock);
                        if (this.f15499.m19550()) {
                            this.f15497.add(this.f15499);
                            break;
                        } else {
                            throw new RarException(RarException.RarExceptionType.badRarArchive);
                        }
                    case MainHeader:
                        int i = baseBlock.m19507() ? 7 : 6;
                        byte[] bArr2 = new byte[i];
                        this.f15507.m19484(bArr2, i);
                        MainHeader mainHeader = new MainHeader(baseBlock, bArr2);
                        this.f15497.add(mainHeader);
                        this.f15503 = mainHeader;
                        if (!this.f15503.m19547()) {
                            break;
                        } else {
                            throw new RarException(RarException.RarExceptionType.rarEncryptedException);
                        }
                    case SignHeader:
                        byte[] bArr3 = new byte[8];
                        this.f15507.m19484(bArr3, 8);
                        this.f15497.add(new SignHeader(baseBlock, bArr3));
                        break;
                    case AvHeader:
                        byte[] bArr4 = new byte[7];
                        this.f15507.m19484(bArr4, 7);
                        this.f15497.add(new AVHeader(baseBlock, bArr4));
                        break;
                    case CommHeader:
                        byte[] bArr5 = new byte[6];
                        this.f15507.m19484(bArr5, 6);
                        CommentHeader commentHeader = new CommentHeader(baseBlock, bArr5);
                        this.f15497.add(commentHeader);
                        this.f15507.m19486(commentHeader.m19504() + ((long) commentHeader.m19501()));
                        break;
                    case EndArcHeader:
                        int i2 = 0;
                        if (baseBlock.m19509()) {
                            i2 = 0 + 4;
                        }
                        if (baseBlock.m19505()) {
                            i2 += 2;
                        }
                        if (i2 > 0) {
                            byte[] bArr6 = new byte[i2];
                            this.f15507.m19484(bArr6, i2);
                            endArcHeader = new EndArcHeader(baseBlock, bArr6);
                        } else {
                            endArcHeader = new EndArcHeader(baseBlock, (byte[]) null);
                        }
                        this.f15497.add(endArcHeader);
                        return;
                    default:
                        byte[] bArr7 = new byte[4];
                        this.f15507.m19484(bArr7, 4);
                        BlockHeader blockHeader = new BlockHeader(baseBlock, bArr7);
                        switch (blockHeader.m19502()) {
                            case NewSubHeader:
                            case FileHeader:
                                int r33 = (blockHeader.m19501() - 7) - 4;
                                byte[] bArr8 = new byte[r33];
                                this.f15507.m19484(bArr8, r33);
                                FileHeader fileHeader = new FileHeader(blockHeader, bArr8);
                                this.f15497.add(fileHeader);
                                this.f15507.m19486(fileHeader.m19504() + ((long) fileHeader.m19501()) + fileHeader.m19521());
                                if (headerCallback == null) {
                                    break;
                                } else {
                                    headerCallback.m19471(fileHeader);
                                    break;
                                }
                            case ProtectHeader:
                                int r332 = (blockHeader.m19501() - 7) - 4;
                                byte[] bArr9 = new byte[r332];
                                this.f15507.m19484(bArr9, r332);
                                ProtectHeader protectHeader = new ProtectHeader(blockHeader, bArr9);
                                this.f15507.m19486(protectHeader.m19504() + ((long) protectHeader.m19501()) + protectHeader.m19512());
                                break;
                            case SubHeader:
                                byte[] bArr10 = new byte[3];
                                this.f15507.m19484(bArr10, 3);
                                SubBlockHeader subBlockHeader = new SubBlockHeader(blockHeader, bArr10);
                                subBlockHeader.m19556();
                                switch (subBlockHeader.m19555()) {
                                    case MAC_HEAD:
                                        byte[] bArr11 = new byte[8];
                                        this.f15507.m19484(bArr11, 8);
                                        MacInfoHeader macInfoHeader = new MacInfoHeader(subBlockHeader, bArr11);
                                        macInfoHeader.m19535();
                                        this.f15497.add(macInfoHeader);
                                        break;
                                    case EA_HEAD:
                                        byte[] bArr12 = new byte[10];
                                        this.f15507.m19484(bArr12, 10);
                                        EAHeader eAHeader = new EAHeader(subBlockHeader, bArr12);
                                        eAHeader.m19513();
                                        this.f15497.add(eAHeader);
                                        break;
                                    case UO_HEAD:
                                        int r333 = ((subBlockHeader.m19501() - 7) - 4) - 3;
                                        byte[] bArr13 = new byte[r333];
                                        this.f15507.m19484(bArr13, r333);
                                        UnixOwnersHeader unixOwnersHeader = new UnixOwnersHeader(subBlockHeader, bArr13);
                                        unixOwnersHeader.m19557();
                                        this.f15497.add(unixOwnersHeader);
                                        break;
                                }
                            default:
                                f15496.warning("Unknown Header");
                                throw new RarException(RarException.RarExceptionType.notRarArchive);
                        }
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19457(FileHeader fileHeader, OutputStream outputStream) throws RarException, IOException {
        this.f15506.m19564(outputStream);
        this.f15506.m19565(fileHeader, this.f15507.m19485());
        this.f15506.m19563(m19462() ? 0 : -1);
        if (this.f15504 == null) {
            this.f15504 = new Unpack(this.f15506);
        }
        if (!fileHeader.m19523()) {
            this.f15504.m19587((byte[]) null);
        }
        this.f15504.m19585(fileHeader.m19517());
        try {
            this.f15504.m19584(fileHeader.m19520(), fileHeader.m19523());
            FileHeader r11 = this.f15506.m19560();
            if ((r11.m19518() ? this.f15506.m19562() ^ -1 : this.f15506.m19558() ^ -1) != ((long) r11.m19515())) {
                throw new RarException(RarException.RarExceptionType.crcError);
            }
        } catch (Exception e) {
            this.f15504.m19581();
            if (e instanceof RarException) {
                throw ((RarException) e);
            }
            throw new RarException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19458(IReadOnlyAccess iReadOnlyAccess, long j) throws IOException {
        this.f15505 = 0;
        this.f15502 = 0;
        close();
        this.f15507 = iReadOnlyAccess;
        this.f15509 = j;
        this.f15498 = false;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19459(HeaderCallback headerCallback) {
        if (this.f15498) {
            return false;
        }
        this.f15498 = true;
        try {
            m19456(headerCallback);
        } catch (Exception e) {
            f15496.log(Level.WARNING, "exception in archive constructor maybe file is encrypted or currupt: " + toString(), e);
        }
        for (BaseBlock next : this.f15497) {
            if (next.m19502() == UnrarHeadertype.FileHeader) {
                this.f15505 += ((FileHeader) next).m19521();
            }
        }
        if (this.f15508 == null) {
            return true;
        }
        this.f15508.m19472(this.f15502, this.f15505);
        return true;
    }

    public void close() throws IOException {
        if (this.f15507 != null) {
            this.f15507.close();
            this.f15507 = null;
        }
        if (this.f15504 != null) {
            this.f15504.m19581();
        }
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f15500.toString() + ")";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public VolumeManager m19460() {
        return this.f15500;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public Volume m19461() {
        return this.f15501;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m19462() {
        m19459((HeaderCallback) null);
        return this.f15499.m19549();
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public UnrarCallback m19463() {
        return this.f15508;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public MainHeader m19464() {
        m19459((HeaderCallback) null);
        return this.f15503;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m19465() {
        m19459((HeaderCallback) null);
        if (this.f15503 != null) {
            return this.f15503.m19547();
        }
        throw new NullPointerException("mainheader is null");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<FileHeader> m19466() {
        ArrayList arrayList = new ArrayList();
        for (BaseBlock next : this.f15497) {
            if (next.m19502().equals(UnrarHeadertype.FileHeader)) {
                arrayList.add((FileHeader) next);
            }
        }
        return arrayList;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19467(int i) {
        if (i > 0) {
            this.f15502 += (long) i;
            if (this.f15508 != null) {
                this.f15508.m19472(this.f15502, this.f15505);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19468(Volume volume) throws IOException {
        this.f15501 = volume;
        m19458(volume.m19475(), volume.m19474());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19469(FileHeader fileHeader, OutputStream outputStream) throws RarException {
        if (!this.f15497.contains(fileHeader)) {
            throw new RarException(RarException.RarExceptionType.headerNotInArchive);
        }
        try {
            m19457(fileHeader, outputStream);
        } catch (Exception e) {
            if (e instanceof RarException) {
                throw ((RarException) e);
            }
            throw new RarException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19470(Volume volume, ComprDataIO comprDataIO) throws IOException {
        m19468(volume);
        List<FileHeader> r1 = m19466();
        if (r1.isEmpty()) {
            return false;
        }
        FileHeader fileHeader = r1.get(0);
        this.f15497.remove(fileHeader);
        comprDataIO.m19565(fileHeader, this.f15507.m19485());
        return true;
    }
}
