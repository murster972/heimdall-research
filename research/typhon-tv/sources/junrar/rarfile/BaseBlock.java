package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BaseBlock {

    /* renamed from: ʻ  reason: contains not printable characters */
    protected short f15526 = 0;

    /* renamed from: 连任  reason: contains not printable characters */
    protected short f15527 = 0;

    /* renamed from: 靐  reason: contains not printable characters */
    protected long f15528;

    /* renamed from: 麤  reason: contains not printable characters */
    protected byte f15529 = 0;

    /* renamed from: 齉  reason: contains not printable characters */
    protected short f15530 = 0;

    /* renamed from: 龘  reason: contains not printable characters */
    Log f15531 = LogFactory.getLog(BaseBlock.class.getName());

    public BaseBlock() {
    }

    public BaseBlock(BaseBlock baseBlock) {
        this.f15527 = baseBlock.m19499();
        this.f15530 = baseBlock.m19500();
        this.f15529 = baseBlock.m19502().getHeaderByte();
        this.f15526 = baseBlock.m19501();
        this.f15528 = baseBlock.m19504();
    }

    public BaseBlock(byte[] bArr) {
        this.f15530 = Raw.m19487(bArr, 0);
        int i = 0 + 2;
        this.f15529 = (byte) (this.f15529 | (bArr[i] & 255));
        int i2 = i + 1;
        this.f15527 = Raw.m19487(bArr, i2);
        this.f15526 = Raw.m19487(bArr, i2 + 2);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public short m19499() {
        return this.f15527;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public short m19500() {
        return this.f15530;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public short m19501() {
        return this.f15526;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public UnrarHeadertype m19502() {
        return UnrarHeadertype.findType(this.f15529);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19503() {
        StringBuilder sb = new StringBuilder();
        sb.append("HeaderType: " + m19502());
        sb.append("\nHeadCRC: " + Integer.toHexString(m19500()));
        sb.append("\nFlags: " + Integer.toHexString(m19499()));
        sb.append("\nHeaderSize: " + m19501());
        sb.append("\nPosition in file: " + m19504());
        this.f15531.info(sb.toString());
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public long m19504() {
        return this.f15528;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public boolean m19505() {
        return (this.f15527 & 8) != 0;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m19506() {
        if (UnrarHeadertype.SubHeader.equals(this.f15529)) {
            return true;
        }
        return UnrarHeadertype.NewSubHeader.equals(this.f15529) && (this.f15527 & 16) != 0;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m19507() {
        return (this.f15527 & 512) != 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19508(long j) {
        this.f15528 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19509() {
        return (this.f15527 & 2) != 0;
    }
}
