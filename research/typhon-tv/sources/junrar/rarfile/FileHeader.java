package junrar.rarfile;

import com.mopub.nativeads.MoPubNativeAdPositioning;
import java.util.Calendar;
import java.util.Date;
import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileHeader extends BlockHeader {

    /* renamed from: ʼ  reason: contains not printable characters */
    private final Log f15546 = LogFactory.getLog(FileHeader.class.getName());

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f15547;

    /* renamed from: ʾ  reason: contains not printable characters */
    private byte f15548;

    /* renamed from: ʿ  reason: contains not printable characters */
    private short f15549;

    /* renamed from: ˆ  reason: contains not printable characters */
    private byte[] f15550;

    /* renamed from: ˈ  reason: contains not printable characters */
    private byte f15551;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final byte[] f15552 = new byte[8];

    /* renamed from: ˊ  reason: contains not printable characters */
    private final byte[] f15553;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f15554;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f15555;

    /* renamed from: ˏ  reason: contains not printable characters */
    private Date f15556;

    /* renamed from: ˑ  reason: contains not printable characters */
    private final HostSystem f15557;

    /* renamed from: י  reason: contains not printable characters */
    private long f15558;

    /* renamed from: ـ  reason: contains not printable characters */
    private long f15559;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final int f15560;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final int f15561;

    /* renamed from: ᴵ  reason: contains not printable characters */
    private int f15562;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int f15563 = -1;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f15564;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f15565;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileHeader(BlockHeader blockHeader, byte[] bArr) {
        super(blockHeader);
        short s = 4096;
        this.f15547 = Raw.m19489(bArr, 0);
        this.f15557 = HostSystem.findHostSystem(bArr[4]);
        int i = 0 + 4 + 1;
        this.f15560 = Raw.m19490(bArr, i);
        int i2 = i + 4;
        this.f15561 = Raw.m19490(bArr, i2);
        this.f15551 = (byte) (this.f15551 | (bArr[13] & 255));
        this.f15548 = (byte) (this.f15548 | (bArr[14] & 255));
        int i3 = i2 + 4 + 1 + 1;
        this.f15549 = Raw.m19487(bArr, i3);
        int i4 = i3 + 2;
        this.f15562 = Raw.m19490(bArr, i4);
        int i5 = i4 + 4;
        if (m19529()) {
            this.f15564 = Raw.m19490(bArr, i5);
            int i6 = i5 + 4;
            this.f15565 = Raw.m19490(bArr, i6);
            i5 = i6 + 4;
        } else {
            this.f15564 = 0;
            this.f15565 = 0;
            if (this.f15547 == -1) {
                this.f15547 = -1;
                this.f15565 = MoPubNativeAdPositioning.MoPubClientPositioning.NO_REPEAT;
            }
        }
        this.f15558 |= (long) this.f15564;
        this.f15558 <<= 32;
        this.f15558 |= m19510();
        this.f15559 |= (long) this.f15565;
        this.f15559 <<= 32;
        this.f15559 += this.f15547;
        this.f15549 = this.f15549 <= 4096 ? this.f15549 : s;
        this.f15553 = new byte[this.f15549];
        for (int i7 = 0; i7 < this.f15549; i7++) {
            this.f15553[i7] = bArr[i5];
            i5++;
        }
        if (m19527()) {
            if (m19526()) {
                int i8 = 0;
                this.f15554 = "";
                this.f15555 = "";
                while (i8 < this.f15553.length && this.f15553[i8] != 0) {
                    i8++;
                }
                byte[] bArr2 = new byte[i8];
                System.arraycopy(this.f15553, 0, bArr2, 0, bArr2.length);
                this.f15554 = new String(bArr2);
                if (i8 != this.f15549) {
                    this.f15555 = FileNameDecoder.m19533(this.f15553, i8 + 1);
                }
            } else {
                this.f15554 = new String(this.f15553);
                this.f15555 = "";
            }
        }
        if (UnrarHeadertype.NewSubHeader.equals(this.f15529)) {
            int i9 = (this.f15526 - 32) - this.f15549;
            i9 = m19528() ? i9 - 8 : i9;
            if (i9 > 0) {
                this.f15550 = new byte[i9];
                for (int i10 = 0; i10 < i9; i10++) {
                    this.f15550[i10] = bArr[i5];
                    i5++;
                }
            }
            if (NewSubHeaderType.f15576.m19553(this.f15553)) {
                this.f15563 = this.f15550[8] + (this.f15550[9] << 8) + (this.f15550[10] << 16) + (this.f15550[11] << 24);
            }
        }
        if (m19528()) {
            for (int i11 = 0; i11 < 8; i11++) {
                this.f15552[i11] = bArr[i5];
                i5++;
            }
        }
        this.f15556 = m19514(this.f15561);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private Date m19514(int i) {
        Calendar instance = Calendar.getInstance();
        instance.set(5, 1);
        instance.set(1, (i >>> 25) + 1980);
        instance.set(2, ((i >>> 21) & 15) - 1);
        instance.set(5, (i >>> 16) & 31);
        instance.set(11, (i >>> 11) & 31);
        instance.set(12, (i >>> 5) & 63);
        instance.set(13, (i & 31) * 2);
        instance.set(14, 0);
        return instance.getTime();
    }

    public String toString() {
        return super.toString();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m19515() {
        return this.f15560;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public String m19516() {
        return this.f15554;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public long m19517() {
        return this.f15559;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m19518() {
        return (this.f15527 & 2) != 0;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public long m19519() {
        return this.f15547;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public byte m19520() {
        return this.f15551;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public long m19521() {
        return this.f15558;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m19522() {
        return (this.f15527 & 1) != 0;
    }

    /* renamed from: י  reason: contains not printable characters */
    public boolean m19523() {
        return (this.f15527 & 16) != 0;
    }

    /* renamed from: ـ  reason: contains not printable characters */
    public boolean m19524() {
        return (this.f15527 & 4) != 0;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19525() {
        super.m19511();
        StringBuilder sb = new StringBuilder();
        sb.append("unpSize: " + m19519());
        sb.append("\nHostOS: " + this.f15557.name());
        sb.append("\nMDate: " + this.f15556);
        sb.append("\nFileName: " + m19516());
        sb.append("\nunpMethod: " + Integer.toHexString(m19532()));
        sb.append("\nunpVersion: " + Integer.toHexString(m19520()));
        sb.append("\nfullpackedsize: " + m19521());
        sb.append("\nfullunpackedsize: " + m19517());
        sb.append("\nisEncrypted: " + m19524());
        sb.append("\nisfileHeader: " + m19527());
        sb.append("\nisSolid: " + m19523());
        sb.append("\nisSplitafter: " + m19518());
        sb.append("\nisSplitBefore:" + m19522());
        sb.append("\nunpSize: " + m19519());
        sb.append("\ndataSize: " + m19512());
        sb.append("\nisUnicode: " + m19526());
        sb.append("\nhasVolumeNumber: " + m19505());
        sb.append("\nhasArchiveDataCRC: " + m19509());
        sb.append("\nhasSalt: " + m19528());
        sb.append("\nhasEncryptVersions: " + m19507());
        sb.append("\nisSubBlock: " + m19506());
        this.f15546.info(sb.toString());
    }

    /* renamed from: ᴵ  reason: contains not printable characters */
    public boolean m19526() {
        return (this.f15527 & 512) != 0;
    }

    /* renamed from: ᵎ  reason: contains not printable characters */
    public boolean m19527() {
        return UnrarHeadertype.FileHeader.equals(this.f15529);
    }

    /* renamed from: ᵔ  reason: contains not printable characters */
    public boolean m19528() {
        return (this.f15527 & 1024) != 0;
    }

    /* renamed from: ᵢ  reason: contains not printable characters */
    public boolean m19529() {
        return (this.f15527 & 256) != 0;
    }

    /* renamed from: ⁱ  reason: contains not printable characters */
    public boolean m19530() {
        return (this.f15527 & 224) == 224;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public String m19531() {
        return this.f15555;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public byte m19532() {
        return this.f15548;
    }
}
