package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MarkHeader extends BaseBlock {

    /* renamed from: ʼ  reason: contains not printable characters */
    private Log f15574 = LogFactory.getLog(MarkHeader.class.getName());

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f15575 = false;

    public MarkHeader(BaseBlock baseBlock) {
        super(baseBlock);
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public boolean m19549() {
        return this.f15575;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public boolean m19550() {
        byte[] bArr = new byte[7];
        Raw.m19494(bArr, 0, this.f15530);
        bArr[2] = this.f15529;
        Raw.m19494(bArr, 3, this.f15527);
        Raw.m19494(bArr, 5, this.f15526);
        if (bArr[0] != 82) {
            return false;
        }
        if (bArr[1] == 69 && bArr[2] == 126 && bArr[3] == 94) {
            this.f15575 = true;
            return true;
        } else if (bArr[1] != 97 || bArr[2] != 114 || bArr[3] != 33 || bArr[4] != 26 || bArr[5] != 7 || bArr[6] != 0) {
            return false;
        } else {
            this.f15575 = false;
            return true;
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19551() {
        super.m19503();
        this.f15574.info("valid: " + m19552());
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m19552() {
        return m19500() == 24914 && m19502() == UnrarHeadertype.MarkHeader && m19499() == 6689 && m19501() == 7;
    }
}
