package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BlockHeader extends BaseBlock {

    /* renamed from: ʼ  reason: contains not printable characters */
    private Log f15532 = LogFactory.getLog(BlockHeader.class.getName());

    /* renamed from: ʽ  reason: contains not printable characters */
    private long f15533;

    /* renamed from: ˑ  reason: contains not printable characters */
    private long f15534;

    public BlockHeader() {
    }

    public BlockHeader(BaseBlock baseBlock, byte[] bArr) {
        super(baseBlock);
        this.f15534 = Raw.m19489(bArr, 0);
        this.f15533 = this.f15534;
    }

    public BlockHeader(BlockHeader blockHeader) {
        super((BaseBlock) blockHeader);
        this.f15534 = blockHeader.m19512();
        this.f15533 = this.f15534;
        this.f15528 = blockHeader.m19504();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public long m19510() {
        return this.f15534;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19511() {
        super.m19503();
        this.f15532.info("DataSize: " + m19512() + " packSize: " + m19510());
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public long m19512() {
        return this.f15533;
    }
}
