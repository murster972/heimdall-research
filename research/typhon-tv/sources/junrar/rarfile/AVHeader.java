package junrar.rarfile;

import junrar.io.Raw;

public class AVHeader extends BaseBlock {

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte f15522;

    /* renamed from: ʽ  reason: contains not printable characters */
    private byte f15523;

    /* renamed from: ˑ  reason: contains not printable characters */
    private byte f15524;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f15525;

    public AVHeader(BaseBlock baseBlock, byte[] bArr) {
        super(baseBlock);
        this.f15522 = (byte) (this.f15522 | (bArr[0] & 255));
        int i = 0 + 1;
        this.f15523 = (byte) (this.f15523 | (bArr[i] & 255));
        int i2 = i + 1;
        this.f15524 = (byte) (this.f15524 | (bArr[i2] & 255));
        this.f15525 = Raw.m19490(bArr, i2 + 1);
    }
}
