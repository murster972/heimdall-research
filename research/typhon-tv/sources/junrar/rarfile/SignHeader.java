package junrar.rarfile;

import junrar.io.Raw;

public class SignHeader extends BaseBlock {

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f15589 = 0;

    /* renamed from: ʽ  reason: contains not printable characters */
    private short f15590 = 0;

    /* renamed from: ˑ  reason: contains not printable characters */
    private short f15591 = 0;

    public SignHeader(BaseBlock baseBlock, byte[] bArr) {
        super(baseBlock);
        this.f15589 = Raw.m19490(bArr, 0);
        int i = 0 + 4;
        this.f15590 = Raw.m19487(bArr, i);
        this.f15591 = Raw.m19487(bArr, i + 2);
    }
}
