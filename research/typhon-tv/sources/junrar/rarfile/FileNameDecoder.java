package junrar.rarfile;

public class FileNameDecoder {
    /* renamed from: 靐  reason: contains not printable characters */
    public static String m19533(byte[] bArr, int i) {
        int i2;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int r7 = m19534(bArr, i);
        StringBuffer stringBuffer = new StringBuffer();
        int i6 = i + 1;
        while (i6 < bArr.length) {
            if (i5 == 0) {
                i2 = i6 + 1;
                i4 = m19534(bArr, i6);
                i5 = 8;
            } else {
                i2 = i6;
            }
            switch (i4 >> 6) {
                case 0:
                    i6 = i2 + 1;
                    stringBuffer.append((char) m19534(bArr, i2));
                    i3++;
                    break;
                case 1:
                    i6 = i2 + 1;
                    stringBuffer.append((char) (m19534(bArr, i2) + (r7 << 8)));
                    i3++;
                    break;
                case 2:
                    stringBuffer.append((char) ((m19534(bArr, i2 + 1) << 8) + m19534(bArr, i2)));
                    i3++;
                    i6 = i2 + 2;
                    break;
                case 3:
                    i6 = i2 + 1;
                    int r8 = m19534(bArr, i2);
                    if ((r8 & 128) != 0) {
                        int i7 = i6 + 1;
                        int r1 = m19534(bArr, i6);
                        int i8 = (r8 & 127) + 2;
                        while (i8 > 0 && i3 < bArr.length) {
                            stringBuffer.append((char) ((r7 << 8) + ((m19534(bArr, i3) + r1) & 255)));
                            i8--;
                            i3++;
                        }
                        i6 = i7;
                        break;
                    } else {
                        int i9 = r8 + 2;
                        while (i9 > 0 && i3 < bArr.length) {
                            stringBuffer.append((char) m19534(bArr, i3));
                            i9--;
                            i3++;
                        }
                    }
                    break;
                default:
                    i6 = i2;
                    break;
            }
            i4 = (i4 << 2) & 255;
            i5 -= 2;
        }
        return stringBuffer.toString();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m19534(byte[] bArr, int i) {
        return bArr[i] & 255;
    }
}
