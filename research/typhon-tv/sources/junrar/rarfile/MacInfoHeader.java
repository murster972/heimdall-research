package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MacInfoHeader extends SubBlockHeader {

    /* renamed from: ʼ  reason: contains not printable characters */
    private Log f15567 = LogFactory.getLog((Class) getClass());

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f15568;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f15569;

    public MacInfoHeader(SubBlockHeader subBlockHeader, byte[] bArr) {
        super(subBlockHeader);
        this.f15568 = Raw.m19490(bArr, 0);
        this.f15569 = Raw.m19490(bArr, 0 + 4);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19535() {
        super.m19556();
        this.f15567.info("filetype: " + this.f15568);
        this.f15567.info("creator :" + this.f15569);
    }
}
