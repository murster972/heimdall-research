package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UnixOwnersHeader extends SubBlockHeader {

    /* renamed from: ʼ  reason: contains not printable characters */
    private Log f15596 = LogFactory.getLog(UnixOwnersHeader.class);

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f15597;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f15598;

    /* renamed from: ٴ  reason: contains not printable characters */
    private String f15599;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private String f15600;

    public UnixOwnersHeader(SubBlockHeader subBlockHeader, byte[] bArr) {
        super(subBlockHeader);
        this.f15597 = Raw.m19487(bArr, 0) & 65535;
        int i = 0 + 2;
        this.f15598 = Raw.m19487(bArr, i) & 65535;
        int i2 = i + 2;
        if (this.f15597 + 4 < bArr.length) {
            byte[] bArr2 = new byte[this.f15597];
            System.arraycopy(bArr, i2, bArr2, 0, this.f15597);
            this.f15599 = new String(bArr2);
        }
        int i3 = this.f15597 + 4;
        if (this.f15598 + i3 < bArr.length) {
            byte[] bArr3 = new byte[this.f15598];
            System.arraycopy(bArr, i3, bArr3, 0, this.f15598);
            this.f15600 = new String(bArr3);
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19557() {
        super.m19556();
        this.f15596.info("ownerNameSize: " + this.f15597);
        this.f15596.info("owner: " + this.f15599);
        this.f15596.info("groupNameSize: " + this.f15598);
        this.f15596.info("group: " + this.f15600);
    }
}
