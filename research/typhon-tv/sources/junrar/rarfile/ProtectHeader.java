package junrar.rarfile;

import junrar.io.Raw;

public class ProtectHeader extends BlockHeader {

    /* renamed from: ʼ  reason: contains not printable characters */
    private byte f15585;

    /* renamed from: ʽ  reason: contains not printable characters */
    private short f15586;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f15587;

    /* renamed from: ٴ  reason: contains not printable characters */
    private byte f15588;

    public ProtectHeader(BlockHeader blockHeader, byte[] bArr) {
        super(blockHeader);
        this.f15585 = (byte) (this.f15585 | (bArr[0] & 255));
        this.f15586 = Raw.m19487(bArr, 0);
        int i = 0 + 2;
        this.f15587 = Raw.m19490(bArr, i);
        this.f15588 = (byte) (this.f15588 | (bArr[i + 4] & 255));
    }
}
