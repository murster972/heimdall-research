package junrar.rarfile;

import junrar.io.Raw;

public class EndArcHeader extends BaseBlock {

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f15544;

    /* renamed from: ʽ  reason: contains not printable characters */
    private short f15545;

    public EndArcHeader(BaseBlock baseBlock, byte[] bArr) {
        super(baseBlock);
        int i = 0;
        if (m19509()) {
            this.f15544 = Raw.m19490(bArr, 0);
            i = 0 + 4;
        }
        if (m19505()) {
            this.f15545 = Raw.m19487(bArr, i);
        }
    }
}
