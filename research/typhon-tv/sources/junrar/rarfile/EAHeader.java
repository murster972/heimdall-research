package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EAHeader extends SubBlockHeader {

    /* renamed from: ʼ  reason: contains not printable characters */
    private Log f15539 = LogFactory.getLog((Class) getClass());

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f15540;

    /* renamed from: ˑ  reason: contains not printable characters */
    private byte f15541;

    /* renamed from: ٴ  reason: contains not printable characters */
    private byte f15542;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f15543;

    public EAHeader(SubBlockHeader subBlockHeader, byte[] bArr) {
        super(subBlockHeader);
        this.f15540 = Raw.m19490(bArr, 0);
        int i = 0 + 4;
        this.f15541 = (byte) (this.f15541 | (bArr[i] & 255));
        int i2 = i + 1;
        this.f15542 = (byte) (this.f15542 | (bArr[i2] & 255));
        this.f15543 = Raw.m19490(bArr, i2 + 1);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19513() {
        super.m19556();
        this.f15539.info("unpSize: " + this.f15540);
        this.f15539.info("unpVersion: " + this.f15541);
        this.f15539.info("method: " + this.f15542);
        this.f15539.info("EACRC:" + this.f15543);
    }
}
