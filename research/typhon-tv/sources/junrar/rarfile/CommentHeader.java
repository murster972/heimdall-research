package junrar.rarfile;

import junrar.io.Raw;

public class CommentHeader extends BaseBlock {

    /* renamed from: ʼ  reason: contains not printable characters */
    private short f15535;

    /* renamed from: ʽ  reason: contains not printable characters */
    private byte f15536;

    /* renamed from: ˑ  reason: contains not printable characters */
    private byte f15537;

    /* renamed from: ٴ  reason: contains not printable characters */
    private short f15538;

    public CommentHeader(BaseBlock baseBlock, byte[] bArr) {
        super(baseBlock);
        this.f15535 = Raw.m19487(bArr, 0);
        int i = 0 + 2;
        this.f15536 = (byte) (this.f15536 | (bArr[i] & 255));
        int i2 = i + 1;
        this.f15537 = (byte) (this.f15537 | (bArr[i2] & 255));
        this.f15538 = Raw.m19487(bArr, i2 + 1);
    }
}
