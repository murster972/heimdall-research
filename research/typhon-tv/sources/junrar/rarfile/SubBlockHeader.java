package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SubBlockHeader extends BlockHeader {

    /* renamed from: ʼ  reason: contains not printable characters */
    private Log f15592 = LogFactory.getLog((Class) getClass());

    /* renamed from: ʽ  reason: contains not printable characters */
    private short f15593;

    /* renamed from: ˑ  reason: contains not printable characters */
    private byte f15594;

    public SubBlockHeader(BlockHeader blockHeader, byte[] bArr) {
        super(blockHeader);
        this.f15593 = Raw.m19487(bArr, 0);
        this.f15594 = (byte) (this.f15594 | (bArr[0 + 2] & 255));
    }

    public SubBlockHeader(SubBlockHeader subBlockHeader) {
        super(subBlockHeader);
        this.f15593 = subBlockHeader.m19555().getSubblocktype();
        this.f15594 = subBlockHeader.m19554();
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public byte m19554() {
        return this.f15594;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public SubBlockHeaderType m19555() {
        return SubBlockHeaderType.findSubblockHeaderType(this.f15593);
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19556() {
        super.m19511();
        this.f15592.info("subtype: " + m19555());
        this.f15592.info("level: " + this.f15594);
    }
}
