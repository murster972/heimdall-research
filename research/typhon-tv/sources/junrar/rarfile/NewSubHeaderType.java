package junrar.rarfile;

import java.util.Arrays;

public class NewSubHeaderType {

    /* renamed from: ʻ  reason: contains not printable characters */
    public static final NewSubHeaderType f15576 = new NewSubHeaderType(new byte[]{82, 82});

    /* renamed from: ʼ  reason: contains not printable characters */
    public static final NewSubHeaderType f15577 = new NewSubHeaderType(new byte[]{69, 65, 50});

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final NewSubHeaderType f15578 = new NewSubHeaderType(new byte[]{69, 65, 66, 69});

    /* renamed from: 连任  reason: contains not printable characters */
    public static final NewSubHeaderType f15579 = new NewSubHeaderType(new byte[]{65, 86});

    /* renamed from: 靐  reason: contains not printable characters */
    public static final NewSubHeaderType f15580 = new NewSubHeaderType(new byte[]{65, 67, 76});

    /* renamed from: 麤  reason: contains not printable characters */
    public static final NewSubHeaderType f15581 = new NewSubHeaderType(new byte[]{85, 79, 87});

    /* renamed from: 齉  reason: contains not printable characters */
    public static final NewSubHeaderType f15582 = new NewSubHeaderType(new byte[]{83, 84, 77});

    /* renamed from: 龘  reason: contains not printable characters */
    public static final NewSubHeaderType f15583 = new NewSubHeaderType(new byte[]{67, 77, 84});

    /* renamed from: ˑ  reason: contains not printable characters */
    private byte[] f15584;

    private NewSubHeaderType(byte[] bArr) {
        this.f15584 = bArr;
    }

    public String toString() {
        return new String(this.f15584);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19553(byte[] bArr) {
        return Arrays.equals(this.f15584, bArr);
    }
}
