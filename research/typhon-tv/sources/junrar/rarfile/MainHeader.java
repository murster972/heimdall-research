package junrar.rarfile;

import junrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MainHeader extends BaseBlock {

    /* renamed from: ʼ  reason: contains not printable characters */
    private Log f15570 = LogFactory.getLog(MainHeader.class.getName());

    /* renamed from: ʽ  reason: contains not printable characters */
    private short f15571;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f15572;

    /* renamed from: ٴ  reason: contains not printable characters */
    private byte f15573;

    public MainHeader(BaseBlock baseBlock, byte[] bArr) {
        super(baseBlock);
        this.f15571 = Raw.m19487(bArr, 0);
        int i = 0 + 2;
        this.f15572 = Raw.m19490(bArr, i);
        int i2 = i + 4;
        if (m19507()) {
            this.f15573 = (byte) (this.f15573 | (bArr[i2] & 255));
        }
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public short m19536() {
        return this.f15571;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m19537() {
        return this.f15572;
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    public boolean m19538() {
        return (this.f15527 & 64) != 0;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public byte m19539() {
        return this.f15573;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    public boolean m19540() {
        return (this.f15527 & 32) != 0;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean m19541() {
        return (this.f15527 & 256) != 0;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public boolean m19542() {
        return (this.f15527 & 8) != 0;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean m19543() {
        return (this.f15527 & 4) != 0;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean m19544() {
        return (this.f15527 & 16) != 0;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public void m19545() {
        super.m19503();
        StringBuilder sb = new StringBuilder();
        sb.append("posav: " + m19537());
        sb.append("\nhighposav: " + m19536());
        sb.append("\nhasencversion: " + m19507() + (m19507() ? Byte.valueOf(m19539()) : ""));
        sb.append("\nhasarchcmt: " + m19546());
        sb.append("\nisEncrypted: " + m19547());
        sb.append("\nisMultivolume: " + m19548());
        sb.append("\nisFirstvolume: " + m19541());
        sb.append("\nisSolid: " + m19542());
        sb.append("\nisLocked: " + m19543());
        sb.append("\nisProtected: " + m19538());
        sb.append("\nisAV: " + m19540());
        this.f15570.info(sb.toString());
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public boolean m19546() {
        return (this.f15527 & 2) != 0;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public boolean m19547() {
        return (this.f15527 & 128) != 0;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public boolean m19548() {
        return (this.f15527 & 1) != 0;
    }
}
