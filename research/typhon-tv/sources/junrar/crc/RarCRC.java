package junrar.crc;

public class RarCRC {

    /* renamed from: 龘  reason: contains not printable characters */
    private static final int[] f15512 = new int[256];

    static {
        for (int i = 0; i < 256; i++) {
            int i2 = i;
            for (int i3 = 0; i3 < 8; i3++) {
                i2 = (i2 & 1) != 0 ? (i2 >>> 1) ^ -306674912 : i2 >>> 1;
            }
            f15512[i] = i2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m19477(int i, byte[] bArr, int i2, int i3) {
        int min = Math.min(bArr.length - i2, i3);
        for (int i4 = 0; i4 < min; i4++) {
            i = f15512[(bArr[i2 + i4] ^ i) & 255] ^ (i >>> 8);
        }
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static short m19478(short s, byte[] bArr, int i) {
        int min = Math.min(bArr.length, i);
        for (int i2 = 0; i2 < min; i2++) {
            short s2 = (short) (((short) (((short) (bArr[i2] & 255)) + s)) & -1);
            s = (short) (((s2 << 1) | (s2 >>> 15)) & -1);
        }
        return s;
    }
}
