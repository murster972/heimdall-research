package junrar.unpack;

import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.view.InputDeviceCompat;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.IOException;
import java.util.Arrays;
import junrar.exception.RarException;
import junrar.unpack.decode.AudioVariables;
import junrar.unpack.decode.BitDecode;
import junrar.unpack.decode.Decode;
import junrar.unpack.decode.DistDecode;
import junrar.unpack.decode.LitDecode;
import junrar.unpack.decode.LowDistDecode;
import junrar.unpack.decode.MultDecode;
import junrar.unpack.decode.RepDecode;

public abstract class Unpack20 extends Unpack15 {

    /* renamed from: ʻʽ  reason: contains not printable characters */
    public static final int[] f15697 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 20, 24, 28, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, PsExtractor.AUDIO_STREAM, 224};

    /* renamed from: ʻʾ  reason: contains not printable characters */
    public static final byte[] f15698 = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5};

    /* renamed from: ʻʿ  reason: contains not printable characters */
    public static final int[] f15699 = {0, 1, 2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64, 96, 128, PsExtractor.AUDIO_STREAM, 256, 384, 512, 768, 1024, 1536, 2048, 3072, 4096, 6144, 8192, 12288, 16384, 24576, 32768, 49152, 65536, 98304, 131072, 196608, 262144, 327680, 393216, 458752, 524288, 589824, 655360, 720896, 786432, 851968, 917504, 983040};

    /* renamed from: ʻˆ  reason: contains not printable characters */
    public static final int[] f15700 = {0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16};

    /* renamed from: ʻˈ  reason: contains not printable characters */
    public static final int[] f15701 = {0, 4, 8, 16, 32, 64, 128, PsExtractor.AUDIO_STREAM};

    /* renamed from: ʻˉ  reason: contains not printable characters */
    public static final int[] f15702 = {2, 2, 3, 4, 5, 6, 6, 6};

    /* renamed from: ʻʼ  reason: contains not printable characters */
    protected BitDecode f15703 = new BitDecode();

    /* renamed from: ˎˎ  reason: contains not printable characters */
    protected byte[] f15704 = new byte[1028];

    /* renamed from: ˏˏ  reason: contains not printable characters */
    protected MultDecode[] f15705 = new MultDecode[4];

    /* renamed from: ˑˑ  reason: contains not printable characters */
    protected int f15706;

    /* renamed from: יי  reason: contains not printable characters */
    protected int f15707;

    /* renamed from: ٴٴ  reason: contains not printable characters */
    protected LowDistDecode f15708 = new LowDistDecode();

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    protected int f15709;

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    protected int f15710;

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    protected AudioVariables[] f15711 = new AudioVariables[4];

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    protected LitDecode f15712 = new LitDecode();

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    protected DistDecode f15713 = new DistDecode();

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    protected RepDecode f15714 = new RepDecode();

    /* access modifiers changed from: protected */
    /* renamed from: ٴ  reason: contains not printable characters */
    public boolean m19603() throws IOException, RarException {
        int i;
        int i2;
        int i3;
        byte[] bArr = new byte[19];
        byte[] bArr2 = new byte[1028];
        if (this.f15831 > this.f15660 - 25 && !m19598()) {
            return false;
        }
        int r0 = m19799();
        this.f15706 = 32768 & r0;
        if ((r0 & 16384) == 0) {
            Arrays.fill(this.f15704, (byte) 0);
        }
        m19804(2);
        if (this.f15706 != 0) {
            this.f15710 = ((r0 >>> 12) & 3) + 1;
            if (this.f15707 >= this.f15710) {
                this.f15707 = 0;
            }
            m19804(2);
            i = this.f15710 * InputDeviceCompat.SOURCE_KEYBOARD;
        } else {
            i = 374;
        }
        for (int i4 = 0; i4 < 19; i4++) {
            bArr[i4] = (byte) (m19799() >>> 12);
            m19804(4);
        }
        m19610(bArr, 0, this.f15703, 19);
        int i5 = 0;
        while (i5 < i) {
            if (this.f15831 > this.f15660 - 5 && !m19598()) {
                return false;
            }
            int r6 = m19609(this.f15703);
            if (r6 < 16) {
                bArr2[i5] = (byte) ((this.f15704[i5] + r6) & 15);
                i5++;
            } else if (r6 == 16) {
                int r4 = (m19799() >>> 14) + 3;
                m19804(2);
                while (true) {
                    int i6 = r4;
                    r4 = i6 - 1;
                    if (i6 <= 0 || i5 >= i) {
                        break;
                    }
                    bArr2[i5] = bArr2[i5 - 1];
                    i5++;
                }
            } else {
                if (r6 == 17) {
                    i2 = (m19799() >>> 13) + 3;
                    m19804(3);
                } else {
                    i2 = (m19799() >>> 9) + 11;
                    m19804(7);
                }
                while (true) {
                    i3 = i5;
                    int i7 = i2;
                    i2 = i7 - 1;
                    if (i7 <= 0 || i3 >= i) {
                        i5 = i3;
                    } else {
                        i5 = i3 + 1;
                        bArr2[i3] = 0;
                    }
                }
                i5 = i3;
            }
        }
        if (this.f15831 > this.f15660) {
            return true;
        }
        if (this.f15706 != 0) {
            for (int i8 = 0; i8 < this.f15710; i8++) {
                m19610(bArr2, i8 * InputDeviceCompat.SOURCE_KEYBOARD, this.f15705[i8], InputDeviceCompat.SOURCE_KEYBOARD);
            }
        } else {
            m19610(bArr2, 0, this.f15712, 298);
            m19610(bArr2, 298, this.f15713, 48);
            m19610(bArr2, 346, this.f15714, 28);
        }
        for (int i9 = 0; i9 < this.f15704.length; i9++) {
            this.f15704[i9] = bArr2[i9];
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ᐧ  reason: contains not printable characters */
    public void m19604() throws IOException, RarException {
        if (this.f15660 < this.f15831 + 5) {
            return;
        }
        if (this.f15706 != 0) {
            if (m19609(this.f15705[this.f15707]) == 256) {
                m19603();
            }
        } else if (m19609(this.f15712) == 269) {
            m19603();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m19605(boolean z) {
        if (!z) {
            this.f15707 = 0;
            this.f15709 = 0;
            this.f15710 = 1;
            Arrays.fill(this.f15711, new AudioVariables());
            Arrays.fill(this.f15704, (byte) 0);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public byte m19606(int i) {
        AudioVariables audioVariables = this.f15711[this.f15707];
        audioVariables.m19646(audioVariables.m19645() + 1);
        audioVariables.m19638(audioVariables.m19641());
        audioVariables.m19642(audioVariables.m19643());
        audioVariables.m19644(audioVariables.m19628() - audioVariables.m19639());
        audioVariables.m19640(audioVariables.m19628());
        int r0 = ((((((audioVariables.m19629() * 8) + (audioVariables.m19624() * audioVariables.m19639())) + ((audioVariables.m19626() * audioVariables.m19643()) + (audioVariables.m19631() * audioVariables.m19641()))) + ((audioVariables.m19633() * audioVariables.m19637()) + (audioVariables.m19635() * this.f15709))) >>> 3) & 255) - i;
        int i2 = ((byte) i) << 3;
        int[] r7 = audioVariables.m19623();
        r7[0] = r7[0] + Math.abs(i2);
        int[] r72 = audioVariables.m19623();
        r72[1] = r72[1] + Math.abs(i2 - audioVariables.m19639());
        int[] r73 = audioVariables.m19623();
        r73[2] = r73[2] + Math.abs(audioVariables.m19639() + i2);
        int[] r74 = audioVariables.m19623();
        r74[3] = r74[3] + Math.abs(i2 - audioVariables.m19643());
        int[] r75 = audioVariables.m19623();
        r75[4] = r75[4] + Math.abs(audioVariables.m19643() + i2);
        int[] r76 = audioVariables.m19623();
        r76[5] = r76[5] + Math.abs(i2 - audioVariables.m19641());
        int[] r77 = audioVariables.m19623();
        r77[6] = r77[6] + Math.abs(audioVariables.m19641() + i2);
        int[] r78 = audioVariables.m19623();
        r78[7] = r78[7] + Math.abs(i2 - audioVariables.m19637());
        int[] r79 = audioVariables.m19623();
        r79[8] = r79[8] + Math.abs(audioVariables.m19637() + i2);
        int[] r710 = audioVariables.m19623();
        r710[9] = r710[9] + Math.abs(i2 - this.f15709);
        int[] r711 = audioVariables.m19623();
        r711[10] = r711[10] + Math.abs(this.f15709 + i2);
        audioVariables.m19630((byte) (r0 - audioVariables.m19629()));
        this.f15709 = audioVariables.m19628();
        audioVariables.m19636(r0);
        if ((audioVariables.m19645() & 31) == 0) {
            int i3 = audioVariables.m19623()[0];
            int i4 = 0;
            audioVariables.m19623()[0] = 0;
            for (int i5 = 1; i5 < audioVariables.m19623().length; i5++) {
                if (audioVariables.m19623()[i5] < i3) {
                    i3 = audioVariables.m19623()[i5];
                    i4 = i5;
                }
                audioVariables.m19623()[i5] = 0;
            }
            switch (i4) {
                case 1:
                    if (audioVariables.m19624() >= -16) {
                        audioVariables.m19622(audioVariables.m19624() - 1);
                        break;
                    }
                    break;
                case 2:
                    if (audioVariables.m19624() < 16) {
                        audioVariables.m19622(audioVariables.m19624() + 1);
                        break;
                    }
                    break;
                case 3:
                    if (audioVariables.m19626() >= -16) {
                        audioVariables.m19625(audioVariables.m19626() - 1);
                        break;
                    }
                    break;
                case 4:
                    if (audioVariables.m19626() < 16) {
                        audioVariables.m19625(audioVariables.m19626() + 1);
                        break;
                    }
                    break;
                case 5:
                    if (audioVariables.m19631() >= -16) {
                        audioVariables.m19627(audioVariables.m19631() - 1);
                        break;
                    }
                    break;
                case 6:
                    if (audioVariables.m19631() < 16) {
                        audioVariables.m19627(audioVariables.m19631() + 1);
                        break;
                    }
                    break;
                case 7:
                    if (audioVariables.m19633() >= -16) {
                        audioVariables.m19632(audioVariables.m19633() - 1);
                        break;
                    }
                    break;
                case 8:
                    if (audioVariables.m19633() < 16) {
                        audioVariables.m19632(audioVariables.m19633() + 1);
                        break;
                    }
                    break;
                case 9:
                    if (audioVariables.m19635() >= -16) {
                        audioVariables.m19634(audioVariables.m19635() - 1);
                        break;
                    }
                    break;
                case 10:
                    if (audioVariables.m19635() < 16) {
                        audioVariables.m19634(audioVariables.m19635() + 1);
                        break;
                    }
                    break;
            }
        }
        return (byte) r0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m19607(int i, int i2) {
        int i3;
        int[] iArr = this.f15680;
        int i4 = this.f15664;
        this.f15664 = i4 + 1;
        iArr[i4 & 3] = i2;
        this.f15679 = i2;
        this.f15669 = i;
        this.f15662 -= (long) i;
        int i5 = this.f15681 - i2;
        if (i5 < 4194004 && this.f15681 < 4194004) {
            byte[] bArr = this.f15676;
            int i6 = this.f15681;
            this.f15681 = i6 + 1;
            int i7 = i5 + 1;
            bArr[i6] = this.f15676[i5];
            byte[] bArr2 = this.f15676;
            int i8 = this.f15681;
            this.f15681 = i8 + 1;
            int i9 = i7 + 1;
            bArr2[i8] = this.f15676[i7];
            while (true) {
                i3 = i9;
                if (i <= 2) {
                    break;
                }
                i--;
                byte[] bArr3 = this.f15676;
                int i10 = this.f15681;
                this.f15681 = i10 + 1;
                i9 = i3 + 1;
                bArr3[i10] = this.f15676[i3];
            }
        } else {
            while (true) {
                i3 = i5;
                int i11 = i;
                i = i11 - 1;
                if (i11 == 0) {
                    break;
                }
                i5 = i3 + 1;
                this.f15676[this.f15681] = this.f15676[i3 & 4194303];
                this.f15681 = (this.f15681 + 1) & 4194303;
            }
        }
        int i12 = i3;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m19608(boolean z) throws IOException, RarException {
        if (this.f15692) {
            this.f15681 = this.f15670;
        } else {
            m19601(z);
            if (!m19598()) {
                return;
            }
            if (z || m19603()) {
                this.f15662--;
            } else {
                return;
            }
        }
        while (this.f15662 >= 0) {
            this.f15681 &= 4194303;
            if (this.f15831 > this.f15660 - 30 && !m19598()) {
                break;
            }
            if (((this.f15670 - this.f15681) & 4194303) < 270 && this.f15670 != this.f15681) {
                m19593();
                if (this.f15692) {
                    return;
                }
            }
            if (this.f15706 != 0) {
                int r0 = m19609(this.f15705[this.f15707]);
                if (r0 != 256) {
                    byte[] bArr = this.f15676;
                    int i = this.f15681;
                    this.f15681 = i + 1;
                    bArr[i] = m19606(r0);
                    int i2 = this.f15707 + 1;
                    this.f15707 = i2;
                    if (i2 == this.f15710) {
                        this.f15707 = 0;
                    }
                    this.f15662--;
                } else if (!m19603()) {
                    break;
                }
            } else {
                int r6 = m19609(this.f15712);
                if (r6 < 256) {
                    byte[] bArr2 = this.f15676;
                    int i3 = this.f15681;
                    this.f15681 = i3 + 1;
                    bArr2[i3] = (byte) r6;
                    this.f15662--;
                } else if (r6 > 269) {
                    int i4 = r6 - 270;
                    int i5 = f15697[i4] + 3;
                    byte b = f15698[i4];
                    if (b > 0) {
                        i5 += m19799() >>> (16 - b);
                        m19804(b);
                    }
                    int r2 = m19609(this.f15713);
                    int i6 = f15699[r2] + 1;
                    int i7 = f15700[r2];
                    if (i7 > 0) {
                        i6 += m19799() >>> (16 - i7);
                        m19804(i7);
                    }
                    if (i6 >= 8192) {
                        i5++;
                        if (((long) i6) >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
                            i5++;
                        }
                    }
                    m19607(i5, i6);
                } else if (r6 == 269) {
                    if (!m19603()) {
                        break;
                    }
                } else if (r6 == 256) {
                    m19607(this.f15669, this.f15679);
                } else if (r6 < 261) {
                    int i8 = this.f15680[(this.f15664 - (r6 + InputDeviceCompat.SOURCE_ANY)) & 3];
                    int r5 = m19609(this.f15714);
                    int i9 = f15697[r5] + 2;
                    byte b2 = f15698[r5];
                    if (b2 > 0) {
                        i9 += m19799() >>> (16 - b2);
                        m19804(b2);
                    }
                    if (i8 >= 257) {
                        i9++;
                        if (i8 >= 8192) {
                            i9++;
                            if (i8 >= 262144) {
                                i9++;
                            }
                        }
                    }
                    m19607(i9, i8);
                } else if (r6 < 270) {
                    int i10 = r6 - 261;
                    int i11 = f15701[i10] + 1;
                    int i12 = f15702[i10];
                    if (i12 > 0) {
                        i11 += m19799() >>> (16 - i12);
                        m19804(i12);
                    }
                    m19607(2, i11);
                }
            }
        }
        m19604();
        m19593();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m19609(Decode decode) {
        long r2 = (long) (m19799() & 65534);
        int[] r4 = decode.m19651();
        int i = r2 < ((long) r4[8]) ? r2 < ((long) r4[4]) ? r2 < ((long) r4[2]) ? r2 < ((long) r4[1]) ? 1 : 2 : r2 < ((long) r4[3]) ? 3 : 4 : r2 < ((long) r4[6]) ? r2 < ((long) r4[5]) ? 5 : 6 : r2 < ((long) r4[7]) ? 7 : 8 : r2 < ((long) r4[12]) ? r2 < ((long) r4[10]) ? r2 < ((long) r4[9]) ? 9 : 10 : r2 < ((long) r4[11]) ? 11 : 12 : r2 < ((long) r4[14]) ? r2 < ((long) r4[13]) ? 13 : 14 : 15;
        m19804(i);
        int i2 = decode.m19649()[i] + ((((int) r2) - r4[i - 1]) >>> (16 - i));
        if (i2 >= decode.m19648()) {
            i2 = 0;
        }
        return decode.m19647()[i2];
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19610(byte[] bArr, int i, Decode decode, int i2) {
        int[] iArr = new int[16];
        int[] iArr2 = new int[16];
        Arrays.fill(iArr, 0);
        Arrays.fill(decode.m19647(), 0);
        for (int i3 = 0; i3 < i2; i3++) {
            byte b = bArr[i + i3] & 15;
            iArr[b] = iArr[b] + 1;
        }
        iArr[0] = 0;
        iArr2[0] = 0;
        decode.m19649()[0] = 0;
        decode.m19651()[0] = 0;
        long j = 0;
        for (int i4 = 1; i4 < 16; i4++) {
            j = 2 * (((long) iArr[i4]) + j);
            long j2 = j << (15 - i4);
            if (j2 > 65535) {
                j2 = 65535;
            }
            decode.m19651()[i4] = (int) j2;
            int[] r9 = decode.m19649();
            int i5 = decode.m19649()[i4 - 1] + iArr[i4 - 1];
            r9[i4] = i5;
            iArr2[i4] = i5;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            if (bArr[i + i6] != 0) {
                int[] r92 = decode.m19647();
                byte b2 = bArr[i + i6] & 15;
                int i7 = iArr2[b2];
                iArr2[b2] = i7 + 1;
                r92[i7] = i6;
            }
        }
        decode.m19650(i2);
    }
}
