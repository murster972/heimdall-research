package junrar.unpack;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import junrar.Archive;
import junrar.UnrarCallback;
import junrar.Volume;
import junrar.crc.RarCRC;
import junrar.exception.RarException;
import junrar.io.IReadOnlyAccess;
import junrar.io.ReadOnlyAccessInputStream;
import junrar.rarfile.FileHeader;

public class ComprDataIO {

    /* renamed from: ʻ  reason: contains not printable characters */
    private OutputStream f15602;

    /* renamed from: ʼ  reason: contains not printable characters */
    private FileHeader f15603;

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f15604;

    /* renamed from: ʾ  reason: contains not printable characters */
    private long f15605;

    /* renamed from: ʿ  reason: contains not printable characters */
    private long f15606;

    /* renamed from: ˆ  reason: contains not printable characters */
    private long f15607;

    /* renamed from: ˈ  reason: contains not printable characters */
    private long f15608;

    /* renamed from: ˉ  reason: contains not printable characters */
    private int f15609;

    /* renamed from: ˊ  reason: contains not printable characters */
    private long f15610;

    /* renamed from: ˋ  reason: contains not printable characters */
    private long f15611;

    /* renamed from: ˎ  reason: contains not printable characters */
    private long f15612;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int f15613;

    /* renamed from: ˑ  reason: contains not printable characters */
    private boolean f15614;

    /* renamed from: ٴ  reason: contains not printable characters */
    private boolean f15615;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private long f15616;

    /* renamed from: 连任  reason: contains not printable characters */
    private InputStream f15617;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f15618;

    /* renamed from: 麤  reason: contains not printable characters */
    private boolean f15619;

    /* renamed from: 齉  reason: contains not printable characters */
    private boolean f15620;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Archive f15621;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private long f15622;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private long f15623;

    public ComprDataIO(Archive archive) {
        this.f15621 = archive;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m19558() {
        return this.f15612;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19559(byte[] bArr, int i, int i2) throws IOException {
        if (!this.f15620) {
            this.f15602.write(bArr, i, i2);
        }
        this.f15622 += (long) i2;
        if (this.f15619) {
            return;
        }
        if (this.f15621.m19462()) {
            this.f15612 = (long) RarCRC.m19478((short) ((int) this.f15612), bArr, i2);
        } else {
            this.f15612 = (long) RarCRC.m19477((int) this.f15612, bArr, i, i2);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public FileHeader m19560() {
        return this.f15603;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19561(byte[] bArr, int i, int i2) throws IOException, RarException {
        int i3 = 0;
        int i4 = 0;
        while (i2 > 0) {
            i3 = this.f15617.read(bArr, i, ((long) i2) > this.f15618 ? (int) this.f15618 : i2);
            if (i3 >= 0) {
                if (this.f15603.m19518()) {
                    this.f15607 = (long) RarCRC.m19477((int) this.f15607, bArr, i, i3);
                }
                this.f15606 += (long) i3;
                i4 += i3;
                i += i3;
                i2 -= i3;
                this.f15618 -= (long) i3;
                this.f15621.m19467(i3);
                if (this.f15618 != 0 || !this.f15603.m19518()) {
                    break;
                }
                Volume r2 = this.f15621.m19460().m19476(this.f15621, this.f15621.m19461());
                if (r2 == null) {
                    this.f15615 = true;
                    int i5 = i3;
                    return -1;
                }
                FileHeader r1 = m19560();
                if (r1.m19520() < 20 || r1.m19515() == -1 || m19562() == ((long) (r1.m19515() ^ -1))) {
                    UnrarCallback r0 = this.f15621.m19463();
                    if (r0 != null && !r0.m19473(r2)) {
                        int i6 = i3;
                        return -1;
                    } else if (!this.f15621.m19470(r2, this)) {
                        int i7 = i3;
                        return -1;
                    }
                } else {
                    throw new RarException(RarException.RarExceptionType.crcError);
                }
            } else {
                throw new EOFException();
            }
        }
        if (i3 != -1) {
            i3 = i4;
        }
        int i8 = i3;
        return i3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m19562() {
        return this.f15607;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19563(long j) {
        this.f15612 = j;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19564(OutputStream outputStream) {
        this.f15602 = outputStream;
        this.f15618 = 0;
        this.f15620 = false;
        this.f15619 = false;
        this.f15604 = false;
        this.f15614 = false;
        this.f15615 = false;
        this.f15609 = 0;
        this.f15613 = 0;
        this.f15616 = 0;
        this.f15622 = 0;
        this.f15606 = 0;
        this.f15605 = 0;
        this.f15608 = 0;
        this.f15607 = -1;
        this.f15612 = -1;
        this.f15611 = -1;
        this.f15603 = null;
        this.f15610 = 0;
        this.f15623 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19565(FileHeader fileHeader, IReadOnlyAccess iReadOnlyAccess) throws IOException {
        long r2 = fileHeader.m19504() + ((long) fileHeader.m19501());
        this.f15618 = fileHeader.m19521();
        this.f15617 = new ReadOnlyAccessInputStream(iReadOnlyAccess, r2, this.f15618 + r2);
        this.f15603 = fileHeader;
        this.f15606 = 0;
        this.f15605 = 0;
        this.f15607 = -1;
    }
}
