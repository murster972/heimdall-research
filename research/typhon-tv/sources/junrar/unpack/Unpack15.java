package junrar.unpack;

import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.MotionEventCompat;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.io.IOException;
import java.util.Arrays;
import junrar.exception.RarException;
import junrar.unpack.vm.BitInput;

public abstract class Unpack15 extends BitInput {

    /* renamed from: ʻʼ  reason: contains not printable characters */
    private static int[] f15640 = {MotionEventCompat.ACTION_POINTER_INDEX_MASK, 65535, 65535, 65535, 65535, 65535};

    /* renamed from: ʻʽ  reason: contains not printable characters */
    private static int[] f15641 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 0, 0, 0};

    /* renamed from: ˈˈ  reason: contains not printable characters */
    static int[] f15642 = {0, 160, 208, 224, 240, 248, 252, 254, 255, PsExtractor.AUDIO_STREAM, 128, 144, 152, 156, 176};

    /* renamed from: ˉˉ  reason: contains not printable characters */
    static int[] f15643 = {1, 3, 4, 4, 5, 6, 7, 8, 8, 4, 4, 5, 6, 6, 4, 0};

    /* renamed from: ˊˊ  reason: contains not printable characters */
    static int[] f15644 = {0, 64, 96, 160, 208, 224, 240, 248, 252, PsExtractor.AUDIO_STREAM, 128, 144, 152, 156, 176};

    /* renamed from: ˋˋ  reason: contains not printable characters */
    static int[] f15645 = {2, 3, 3, 3, 4, 4, 5, 6, 6, 4, 4, 5, 6, 6, 4, 0};

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private static int[] f15646 = {40960, 49152, 53248, 57344, 59904, 60928, 61440, 61952, 62016, 65535};

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private static int[] f15647 = {0, 0, 0, 2, 3, 5, 7, 11, 16, 20, 24, 32, 32};

    /* renamed from: ˑˑ  reason: contains not printable characters */
    private static int[] f15648 = {0, 0, 0, 0, 5, 7, 9, 13, 18, 22, 26, 34, 36};

    /* renamed from: יי  reason: contains not printable characters */
    private static int[] f15649 = {0, 0, 0, 0, 0, 8, 16, 24, 33, 33, 33, 33, 33};

    /* renamed from: ٴٴ  reason: contains not printable characters */
    private static int[] f15650 = {2048, 9216, 60928, 65152, 65535, 65535, 65535};

    /* renamed from: ᵎᵎ  reason: contains not printable characters */
    private static int[] f15651 = {8192, 49152, 57344, 61440, 61952, 61952, 63456, 65535};

    /* renamed from: ᵔᵔ  reason: contains not printable characters */
    private static int[] f15652 = {32768, 49152, 57344, 61952, 61952, 61952, 61952, 61952, 65535};

    /* renamed from: ᵢᵢ  reason: contains not printable characters */
    private static int[] f15653 = {0, 0, 0, 0, 0, 0, 4, 44, 60, 76, 80, 80, 127};

    /* renamed from: ⁱⁱ  reason: contains not printable characters */
    private static int[] f15654 = {4096, 9216, 32768, 49152, 64000, 65535, 65535, 65535};

    /* renamed from: 龘  reason: contains not printable characters */
    private static int[] f15655 = {32768, 40960, 49152, 53248, 57344, 59904, 60928, 61440, 61952, 61952, 65535};

    /* renamed from: ﹳﹳ  reason: contains not printable characters */
    private static int[] f15656 = {0, 0, 0, 0, 0, 0, 2, 7, 53, 117, 233, 0, 0};

    /* renamed from: ﹶﹶ  reason: contains not printable characters */
    private static int[] f15657 = {0, 0, 0, 0, 0, 0, 0, 2, 16, 218, 251, 0, 0};

    /* renamed from: ʻ  reason: contains not printable characters */
    protected boolean f15658;

    /* renamed from: ʻʻ  reason: contains not printable characters */
    protected int f15659;

    /* renamed from: ʼ  reason: contains not printable characters */
    protected int f15660;

    /* renamed from: ʼʼ  reason: contains not printable characters */
    protected int f15661;

    /* renamed from: ʽ  reason: contains not printable characters */
    protected long f15662;

    /* renamed from: ʽʽ  reason: contains not printable characters */
    protected int f15663;

    /* renamed from: ʾ  reason: contains not printable characters */
    protected int f15664;

    /* renamed from: ʾʾ  reason: contains not printable characters */
    protected int f15665;

    /* renamed from: ʿ  reason: contains not printable characters */
    protected int[] f15666 = new int[256];

    /* renamed from: ʿʿ  reason: contains not printable characters */
    protected int f15667;

    /* renamed from: ˆ  reason: contains not printable characters */
    protected int[] f15668 = new int[256];

    /* renamed from: ˆˆ  reason: contains not printable characters */
    protected int f15669;

    /* renamed from: ˈ  reason: contains not printable characters */
    protected int f15670;

    /* renamed from: ˉ  reason: contains not printable characters */
    protected int[] f15671 = new int[256];

    /* renamed from: ˊ  reason: contains not printable characters */
    protected int[] f15672 = new int[256];

    /* renamed from: ˋ  reason: contains not printable characters */
    protected int[] f15673 = new int[256];

    /* renamed from: ˎ  reason: contains not printable characters */
    protected int[] f15674 = new int[256];

    /* renamed from: ˏ  reason: contains not printable characters */
    protected int[] f15675 = new int[256];

    /* renamed from: ˑ  reason: contains not printable characters */
    protected byte[] f15676;

    /* renamed from: י  reason: contains not printable characters */
    protected int[] f15677 = new int[256];

    /* renamed from: ـ  reason: contains not printable characters */
    protected int[] f15678 = new int[256];

    /* renamed from: ــ  reason: contains not printable characters */
    protected int f15679;

    /* renamed from: ٴ  reason: contains not printable characters */
    protected int[] f15680 = new int[4];

    /* renamed from: ᐧ  reason: contains not printable characters */
    protected int f15681;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    protected int f15682;

    /* renamed from: ᴵ  reason: contains not printable characters */
    protected int f15683;

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    protected int f15684;

    /* renamed from: ᵎ  reason: contains not printable characters */
    protected int f15685;

    /* renamed from: ᵔ  reason: contains not printable characters */
    protected int f15686;

    /* renamed from: ᵢ  reason: contains not printable characters */
    protected int f15687;

    /* renamed from: ⁱ  reason: contains not printable characters */
    protected int f15688;

    /* renamed from: 连任  reason: contains not printable characters */
    protected ComprDataIO f15689;

    /* renamed from: 靐  reason: contains not printable characters */
    protected int f15690;

    /* renamed from: 麤  reason: contains not printable characters */
    protected boolean f15691;

    /* renamed from: 齉  reason: contains not printable characters */
    protected boolean f15692;

    /* renamed from: ﹳ  reason: contains not printable characters */
    protected int f15693;

    /* renamed from: ﹶ  reason: contains not printable characters */
    protected int[] f15694 = new int[256];

    /* renamed from: ﾞ  reason: contains not printable characters */
    protected int[] f15695 = new int[256];

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    protected int f15696;

    /* renamed from: 靐  reason: contains not printable characters */
    private int m19588(int i) {
        return i == 3 ? this.f15696 + 3 : f15645[i];
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m19589(int i) {
        return i == 1 ? this.f15696 + 3 : f15643[i];
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʻ  reason: contains not printable characters */
    public void m19590() {
        int i = 4;
        int r0 = m19800();
        int r1 = (this.f15685 > 30207 ? m19599(r0, 8, f15640, f15641) : this.f15685 > 24063 ? m19599(r0, 6, f15650, f15657) : this.f15685 > 13823 ? m19599(r0, 5, f15654, f15656) : this.f15685 > 3583 ? m19599(r0, 5, f15651, f15653) : m19599(r0, 4, f15652, f15649)) & 255;
        if (this.f15684 != 0) {
            if (r1 == 0 && r0 > 4095) {
                r1 = 256;
            }
            r1--;
            if (r1 == -1) {
                int r02 = m19800();
                m19803(1);
                if ((32768 & r02) != 0) {
                    this.f15684 = 0;
                    this.f15682 = 0;
                    return;
                }
                if ((r02 & 16384) == 0) {
                    i = 3;
                }
                m19803(1);
                m19803(5);
                m19600((m19599(m19800(), 5, f15654, f15656) << 5) | (m19800() >>> 11), i);
                return;
            }
        } else {
            int i2 = this.f15682;
            this.f15682 = i2 + 1;
            if (i2 >= 16 && this.f15663 == 0) {
                this.f15684 = 1;
            }
        }
        this.f15685 += r1;
        this.f15685 -= this.f15685 >>> 8;
        this.f15661 += 16;
        if (this.f15661 > 255) {
            this.f15661 = 144;
            this.f15667 >>>= 1;
        }
        byte[] bArr = this.f15676;
        int i3 = this.f15681;
        this.f15681 = i3 + 1;
        bArr[i3] = (byte) (this.f15666[r1] >>> 8);
        this.f15662--;
        while (true) {
            int i4 = this.f15666[r1];
            int[] iArr = this.f15675;
            int i5 = i4 + 1;
            int i6 = i4 & 255;
            int i7 = iArr[i6];
            iArr[i6] = i7 + 1;
            if ((i5 & 255) > 161) {
                m19602(this.f15666, this.f15675);
            } else {
                this.f15666[r1] = this.f15666[i7];
                this.f15666[i7] = i5;
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʼ  reason: contains not printable characters */
    public void m19591() {
        int r2 = m19599(m19800(), 5, f15654, f15656);
        while (true) {
            int i = this.f15672[r2];
            this.f15683 = i >>> 8;
            int[] iArr = this.f15678;
            int i2 = i + 1;
            int i3 = i & 255;
            int i4 = iArr[i3];
            iArr[i3] = i4 + 1;
            if ((i2 & 255) != 0) {
                this.f15672[r2] = this.f15672[i4];
                this.f15672[i4] = i2;
                return;
            }
            m19602(this.f15672, this.f15678);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: ʽ  reason: contains not printable characters */
    public void m19592() {
        for (int i = 0; i < 256; i++) {
            int[] iArr = this.f15673;
            int[] iArr2 = this.f15674;
            this.f15668[i] = i;
            iArr2[i] = i;
            iArr[i] = i;
            this.f15671[i] = ((i ^ -1) + 1) & 255;
            int[] iArr3 = this.f15666;
            int i2 = i << 8;
            this.f15695[i] = i2;
            iArr3[i] = i2;
            this.f15694[i] = i;
            this.f15672[i] = (((i ^ -1) + 1) & 255) << 8;
        }
        Arrays.fill(this.f15675, 0);
        Arrays.fill(this.f15677, 0);
        Arrays.fill(this.f15678, 0);
        m19602(this.f15695, this.f15677);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ˑ  reason: contains not printable characters */
    public void m19593() throws IOException {
        if (this.f15681 != this.f15670) {
            this.f15658 = true;
        }
        if (this.f15681 < this.f15670) {
            this.f15689.m19559(this.f15676, this.f15670, (-this.f15670) & 4194303);
            this.f15689.m19559(this.f15676, 0, this.f15681);
            this.f15691 = true;
        } else {
            this.f15689.m19559(this.f15676, this.f15670, this.f15681 - this.f15670);
        }
        this.f15670 = this.f15681;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 连任  reason: contains not printable characters */
    public void m19594() {
        int i;
        int i2;
        int i3;
        this.f15682 = 0;
        this.f15667 += 16;
        if (this.f15667 > 255) {
            this.f15667 = 144;
            this.f15661 >>>= 1;
        }
        int i4 = this.f15688;
        int r0 = m19800();
        if (this.f15688 >= 122) {
            i = m19599(r0, 3, f15646, f15648);
        } else if (this.f15688 >= 64) {
            i = m19599(r0, 2, f15655, f15647);
        } else if (r0 < 256) {
            i = r0;
            m19803(16);
        } else {
            i = 0;
            while (((r0 << i) & 32768) == 0) {
                i++;
            }
            m19803(i + 1);
        }
        this.f15688 += i;
        this.f15688 -= this.f15688 >>> 5;
        int r02 = m19800();
        int r3 = this.f15686 > 10495 ? m19599(r02, 5, f15654, f15656) : this.f15686 > 1791 ? m19599(r02, 5, f15651, f15653) : m19599(r02, 4, f15652, f15649);
        this.f15686 += r3;
        this.f15686 -= this.f15686 >> 8;
        while (true) {
            int i5 = this.f15695[r3 & 255];
            int[] iArr = this.f15677;
            i2 = i5 + 1;
            int i6 = i5 & 255;
            i3 = iArr[i6];
            iArr[i6] = i3 + 1;
            if ((i2 & 255) != 0) {
                break;
            }
            m19602(this.f15695, this.f15677);
        }
        this.f15695[r3] = this.f15695[i3];
        this.f15695[i3] = i2;
        int r1 = ((65280 & i2) | (m19800() >>> 8)) >>> 1;
        m19803(7);
        int i7 = this.f15693;
        if (!(i == 1 || i == 4)) {
            if (i == 0 && r1 <= this.f15665) {
                this.f15693++;
                this.f15693 -= this.f15693 >> 8;
            } else if (this.f15693 > 0) {
                this.f15693--;
            }
        }
        int i8 = i + 3;
        if (r1 >= this.f15665) {
            i8++;
        }
        if (r1 <= 256) {
            i8 += 8;
        }
        if (i7 > 176 || (this.f15685 >= 10752 && i4 < 64)) {
            this.f15665 = 32512;
        } else {
            this.f15665 = 8193;
        }
        int[] iArr2 = this.f15680;
        int i9 = this.f15664;
        this.f15664 = i9 + 1;
        iArr2[i9] = r1;
        this.f15664 &= 3;
        this.f15669 = i8;
        this.f15679 = r1;
        m19600(r1, i8);
    }

    /* access modifiers changed from: protected */
    /* renamed from: 靐  reason: contains not printable characters */
    public void m19595(boolean z) throws IOException, RarException {
        if (this.f15692) {
            this.f15681 = this.f15670;
        } else {
            m19601(z);
            m19597(z);
            m19598();
            if (!z) {
                m19592();
                this.f15681 = 0;
            } else {
                this.f15681 = this.f15670;
            }
            this.f15662--;
        }
        if (this.f15662 >= 0) {
            m19591();
            this.f15663 = 8;
        }
        while (this.f15662 >= 0) {
            this.f15681 &= 4194303;
            if (this.f15831 > this.f15660 - 30 && !m19598()) {
                break;
            }
            if (((this.f15670 - this.f15681) & 4194303) < 270 && this.f15670 != this.f15681) {
                m19593();
                if (this.f15692) {
                    return;
                }
            }
            if (this.f15684 != 0) {
                m19590();
            } else {
                int i = this.f15663 - 1;
                this.f15663 = i;
                if (i < 0) {
                    m19591();
                    this.f15663 = 7;
                }
                if ((this.f15683 & 128) != 0) {
                    this.f15683 <<= 1;
                    if (this.f15667 > this.f15661) {
                        m19594();
                    } else {
                        m19590();
                    }
                } else {
                    this.f15683 <<= 1;
                    int i2 = this.f15663 - 1;
                    this.f15663 = i2;
                    if (i2 < 0) {
                        m19591();
                        this.f15663 = 7;
                    }
                    if ((this.f15683 & 128) != 0) {
                        this.f15683 <<= 1;
                        if (this.f15667 > this.f15661) {
                            m19590();
                        } else {
                            m19594();
                        }
                    } else {
                        this.f15683 <<= 1;
                        m19596();
                    }
                }
            }
        }
        m19593();
    }

    /* access modifiers changed from: protected */
    /* renamed from: 麤  reason: contains not printable characters */
    public void m19596() {
        int i;
        this.f15682 = 0;
        int r0 = m19800();
        if (this.f15659 == 2) {
            m19803(1);
            if (r0 >= 32768) {
                m19600(this.f15679, this.f15669);
                return;
            } else {
                r0 <<= 1;
                this.f15659 = 0;
            }
        }
        int i2 = r0 >>> 8;
        if (this.f15687 < 37) {
            i = 0;
            while (((f15642[i] ^ i2) & ((255 >>> m19589(i)) ^ -1)) != 0) {
                i++;
            }
            m19803(m19589(i));
        } else {
            int i3 = 0;
            while (((f15644[i] ^ i2) & ((255 >> m19588(i)) ^ -1)) != 0) {
                i3 = i + 1;
            }
            m19803(m19588(i));
        }
        if (i < 9) {
            this.f15659 = 0;
            this.f15687 += i;
            this.f15687 -= this.f15687 >> 4;
            int r2 = m19599(m19800(), 5, f15654, f15656) & 255;
            int i4 = this.f15694[r2];
            int i5 = r2 - 1;
            if (i5 != -1) {
                int[] iArr = this.f15674;
                iArr[i4] = iArr[i4] - 1;
                int i6 = this.f15694[i5];
                int[] iArr2 = this.f15674;
                iArr2[i6] = iArr2[i6] + 1;
                this.f15694[i5 + 1] = i6;
                this.f15694[i5] = i4;
            }
            int i7 = i + 2;
            int[] iArr3 = this.f15680;
            int i8 = this.f15664;
            this.f15664 = i8 + 1;
            int i9 = i4 + 1;
            iArr3[i8] = i9;
            this.f15664 &= 3;
            this.f15669 = i7;
            this.f15679 = i9;
            m19600(i9, i7);
        } else if (i == 9) {
            this.f15659++;
            m19600(this.f15679, this.f15669);
        } else if (i == 14) {
            this.f15659 = 0;
            int r4 = m19599(m19800(), 3, f15646, f15648) + 5;
            int r1 = (m19800() >> 1) | 32768;
            m19803(15);
            this.f15669 = r4;
            this.f15679 = r1;
            m19600(r1, r4);
        } else {
            this.f15659 = 0;
            int i10 = i;
            int i11 = this.f15680[(this.f15664 - (i - 9)) & 3];
            int r42 = m19599(m19800(), 2, f15655, f15647) + 2;
            if (r42 == 257 && i10 == 10) {
                this.f15696 ^= 1;
                return;
            }
            if (i11 > 256) {
                r42++;
            }
            if (i11 >= this.f15665) {
                r42++;
            }
            int[] iArr4 = this.f15680;
            int i12 = this.f15664;
            this.f15664 = i12 + 1;
            iArr4[i12] = i11;
            this.f15664 &= 3;
            this.f15669 = r42;
            this.f15679 = i11;
            m19600(i11, r42);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public void m19597(boolean z) {
        if (!z) {
            this.f15696 = 0;
            this.f15682 = 0;
            this.f15693 = 0;
            this.f15688 = 0;
            this.f15687 = 0;
            this.f15686 = 0;
            this.f15685 = 13568;
            this.f15665 = 8193;
            this.f15667 = 128;
            this.f15661 = 128;
        }
        this.f15663 = 0;
        this.f15683 = 0;
        this.f15684 = 0;
        this.f15659 = 0;
        this.f15660 = 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m19598() throws IOException, RarException {
        int i = this.f15660 - this.f15831;
        if (i < 0) {
            return false;
        }
        if (this.f15831 > 16384) {
            if (i > 0) {
                System.arraycopy(this.f15833, this.f15831, this.f15833, 0, i);
            }
            this.f15831 = 0;
            this.f15660 = i;
        } else {
            i = this.f15660;
        }
        int r1 = this.f15689.m19561(this.f15833, i, (32768 - i) & -16);
        if (r1 > 0) {
            this.f15660 += r1;
        }
        this.f15690 = this.f15660 - 30;
        return r1 != -1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public int m19599(int i, int i2, int[] iArr, int[] iArr2) {
        int i3 = i & 65520;
        int i4 = 0;
        while (iArr[i4] <= i3) {
            i2++;
            i4++;
        }
        m19803(i2);
        return ((i3 - (i4 != 0 ? iArr[i4 - 1] : 0)) >>> (16 - i2)) + iArr2[i2];
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19600(int i, int i2) {
        this.f15662 -= (long) i2;
        while (true) {
            int i3 = i2;
            i2 = i3 - 1;
            if (i3 != 0) {
                this.f15676[this.f15681] = this.f15676[(this.f15681 - i) & 4194303];
                this.f15681 = (this.f15681 + 1) & 4194303;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public abstract void m19601(boolean z);

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19602(int[] iArr, int[] iArr2) {
        int i = 0;
        for (int i2 = 7; i2 >= 0; i2--) {
            int i3 = 0;
            while (i3 < 32) {
                iArr[i] = (iArr[i] & InputDeviceCompat.SOURCE_ANY) | i2;
                i3++;
                i++;
            }
        }
        Arrays.fill(iArr2, 0);
        for (int i4 = 6; i4 >= 0; i4--) {
            iArr2[i4] = (7 - i4) * 32;
        }
    }
}
