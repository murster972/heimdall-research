package junrar.unpack;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import junrar.exception.RarException;
import junrar.unpack.ppm.BlockTypes;
import junrar.unpack.ppm.ModelPPM;
import junrar.unpack.ppm.SubAllocator;
import junrar.unpack.vm.BitInput;
import junrar.unpack.vm.RarVM;
import junrar.unpack.vm.VMPreparedProgram;

public final class Unpack extends Unpack20 {

    /* renamed from: 龘  reason: contains not printable characters */
    public static int[] f15624 = {4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 14, 0, 12};

    /* renamed from: ʻˏ  reason: contains not printable characters */
    private final ModelPPM f15625 = new ModelPPM();

    /* renamed from: ʻˑ  reason: contains not printable characters */
    private int f15626;

    /* renamed from: ʻי  reason: contains not printable characters */
    private RarVM f15627 = new RarVM();

    /* renamed from: ʻـ  reason: contains not printable characters */
    private List<UnpackFilter> f15628 = new ArrayList();

    /* renamed from: ʻٴ  reason: contains not printable characters */
    private List<UnpackFilter> f15629 = new ArrayList();

    /* renamed from: ʻᐧ  reason: contains not printable characters */
    private List<Integer> f15630 = new ArrayList();

    /* renamed from: ʻᴵ  reason: contains not printable characters */
    private int f15631;

    /* renamed from: ʻᵎ  reason: contains not printable characters */
    private boolean f15632;

    /* renamed from: ʻᵔ  reason: contains not printable characters */
    private byte[] f15633 = new byte[404];

    /* renamed from: ʻᵢ  reason: contains not printable characters */
    private BlockTypes f15634;

    /* renamed from: ʻⁱ  reason: contains not printable characters */
    private long f15635;

    /* renamed from: ʻﹳ  reason: contains not printable characters */
    private boolean f15636;

    /* renamed from: ʻﹶ  reason: contains not printable characters */
    private boolean f15637;

    /* renamed from: ʻﾞ  reason: contains not printable characters */
    private int f15638;

    /* renamed from: ʼʻ  reason: contains not printable characters */
    private int f15639;

    public Unpack(ComprDataIO comprDataIO) {
        this.f15689 = comprDataIO;
        this.f15676 = null;
        this.f15692 = false;
        this.f15691 = false;
        this.f15658 = false;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m19566(int i) {
        this.f15680[3] = this.f15680[2];
        this.f15680[2] = this.f15680[1];
        this.f15680[1] = this.f15680[0];
        this.f15680[0] = i;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m19567(boolean z) throws IOException, RarException {
        int[] iArr = new int[60];
        byte[] bArr = new byte[60];
        if (iArr[1] == 0) {
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            while (i4 < f15624.length) {
                int i5 = f15624[i4];
                int i6 = 0;
                while (i6 < i5) {
                    iArr[i3] = i;
                    bArr[i3] = (byte) i2;
                    i6++;
                    i3++;
                    i += 1 << i2;
                }
                i4++;
                i2++;
            }
        }
        this.f15636 = true;
        if (!this.f15692) {
            m19586(z);
            if (m19598()) {
                if ((!z || !this.f15632) && !m19568()) {
                    return;
                }
            } else {
                return;
            }
        }
        if (!this.f15637) {
            while (true) {
                this.f15681 &= 4194303;
                if (this.f15831 > this.f15690 && !m19598()) {
                    break;
                }
                if (((this.f15670 - this.f15681) & 4194303) < 260 && this.f15670 != this.f15681) {
                    m19570();
                    if (this.f15635 > this.f15662) {
                        return;
                    }
                    if (this.f15692) {
                        this.f15636 = false;
                        return;
                    }
                }
                if (this.f15634 == BlockTypes.BLOCK_PPM) {
                    int r6 = this.f15625.m19681();
                    if (r6 == -1) {
                        this.f15637 = true;
                        break;
                    }
                    if (r6 == this.f15626) {
                        int r18 = this.f15625.m19681();
                        if (r18 != 0) {
                            if (r18 != 2 && r18 != -1) {
                                if (r18 != 3) {
                                    if (r18 != 4) {
                                        if (r18 == 5) {
                                            int r15 = this.f15625.m19681();
                                            if (r15 == -1) {
                                                break;
                                            }
                                            m19574(r15 + 4, 1);
                                        }
                                    } else {
                                        int i7 = 0;
                                        int i8 = 0;
                                        boolean z2 = false;
                                        for (int i9 = 0; i9 < 4 && !z2; i9++) {
                                            int r21 = this.f15625.m19681();
                                            if (r21 == -1) {
                                                z2 = true;
                                            } else if (i9 == 3) {
                                                i8 = r21 & 255;
                                            } else {
                                                i7 = (i7 << 8) + (r21 & 255);
                                            }
                                        }
                                        if (z2) {
                                            break;
                                        }
                                        m19574(i8 + 32, i7 + 2);
                                    }
                                } else if (!m19573()) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if (!m19568()) {
                            break;
                        }
                    }
                    byte[] bArr2 = this.f15676;
                    int i10 = this.f15681;
                    this.f15681 = i10 + 1;
                    bArr2[i10] = (byte) r6;
                } else {
                    int r19 = m19609(this.f15712);
                    if (r19 < 256) {
                        byte[] bArr3 = this.f15676;
                        int i11 = this.f15681;
                        this.f15681 = i11 + 1;
                        bArr3[i11] = (byte) r19;
                    } else if (r19 >= 271) {
                        int i12 = r19 - 271;
                        int i13 = f15697[i12] + 3;
                        byte b = f15698[i12];
                        if (b > 0) {
                            i13 += m19799() >>> (16 - b);
                            m19804(b);
                        }
                        int r11 = m19609(this.f15713);
                        int i14 = iArr[r11] + 1;
                        byte b2 = bArr[r11];
                        if (b2 > 0) {
                            if (r11 > 9) {
                                if (b2 > 4) {
                                    i14 += (m19799() >>> (20 - b2)) << 4;
                                    m19804(b2 - 4);
                                }
                                if (this.f15639 > 0) {
                                    this.f15639--;
                                    i14 += this.f15638;
                                } else {
                                    int r17 = m19609(this.f15708);
                                    if (r17 == 16) {
                                        this.f15639 = 15;
                                        i14 += this.f15638;
                                    } else {
                                        i14 += r17;
                                        this.f15638 = r17;
                                    }
                                }
                            } else {
                                i14 += m19799() >>> (16 - b2);
                                m19804(b2);
                            }
                        }
                        if (i14 >= 8192) {
                            i13++;
                            if (((long) i14) >= PlaybackStateCompat.ACTION_SET_REPEAT_MODE) {
                                i13++;
                            }
                        }
                        m19566(i14);
                        m19575(i13, i14);
                        m19574(i13, i14);
                    } else if (r19 == 256) {
                        if (!m19572()) {
                            break;
                        }
                    } else if (r19 == 257) {
                        if (!m19569()) {
                            break;
                        }
                    } else if (r19 == 258) {
                        if (this.f15669 != 0) {
                            m19574(this.f15669, this.f15679);
                        }
                    } else if (r19 < 263) {
                        int i15 = r19 - 259;
                        int i16 = this.f15680[i15];
                        for (int i17 = i15; i17 > 0; i17--) {
                            this.f15680[i17] = this.f15680[i17 - 1];
                        }
                        this.f15680[0] = i16;
                        int r16 = m19609(this.f15714);
                        int i18 = f15697[r16] + 2;
                        byte b3 = f15698[r16];
                        if (b3 > 0) {
                            i18 += m19799() >>> (16 - b3);
                            m19804(b3);
                        }
                        m19575(i18, i16);
                        m19574(i18, i16);
                    } else if (r19 < 272) {
                        int i19 = r19 - 263;
                        int i20 = f15701[i19] + 1;
                        int i21 = f15702[i19];
                        if (i21 > 0) {
                            i20 += m19799() >>> (16 - i21);
                            m19804(i21);
                        }
                        m19566(i20);
                        m19575(2, i20);
                        m19574(2, i20);
                    }
                }
            }
            m19570();
        }
    }

    /* renamed from: ˆ  reason: contains not printable characters */
    private boolean m19568() throws IOException, RarException {
        int i;
        int i2;
        int i3;
        int i4;
        byte[] bArr = new byte[20];
        byte[] bArr2 = new byte[404];
        if (this.f15831 > this.f15660 - 25 && !m19598()) {
            return false;
        }
        m19803((8 - this.f15832) & 7);
        long r8 = (long) (m19800() & -1);
        if ((PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID & r8) != 0) {
            this.f15634 = BlockTypes.BLOCK_PPM;
            return this.f15625.m19689(this, this.f15626);
        }
        this.f15634 = BlockTypes.BLOCK_LZ;
        this.f15638 = 0;
        this.f15639 = 0;
        if ((16384 & r8) == 0) {
            Arrays.fill(this.f15633, (byte) 0);
        }
        m19803(2);
        int i5 = 0;
        while (i5 < 20) {
            int r13 = (m19800() >>> 12) & 255;
            m19803(4);
            if (r13 == 15) {
                int r15 = (m19800() >>> 12) & 255;
                m19803(4);
                if (r15 == 0) {
                    bArr[i5] = 15;
                } else {
                    int i6 = r15 + 2;
                    while (true) {
                        int i7 = i6;
                        i4 = i5;
                        i6 = i7 - 1;
                        if (i7 <= 0 || i4 >= bArr.length) {
                            i5 = i4 - 1;
                        } else {
                            i5 = i4 + 1;
                            bArr[i4] = 0;
                        }
                    }
                    i5 = i4 - 1;
                }
            } else {
                bArr[i5] = (byte) r13;
            }
            i5++;
        }
        m19610(bArr, 0, this.f15703, 20);
        int i8 = 0;
        while (i8 < 404) {
            if (this.f15831 > this.f15660 - 5 && !m19598()) {
                return false;
            }
            int r6 = m19609(this.f15703);
            if (r6 < 16) {
                bArr2[i8] = (byte) ((this.f15633[i8] + r6) & 15);
                i8++;
            } else if (r6 < 18) {
                if (r6 == 16) {
                    i3 = (m19800() >>> 13) + 3;
                    m19803(3);
                } else {
                    i3 = (m19800() >>> 9) + 11;
                    m19803(7);
                }
                while (true) {
                    int i9 = i3;
                    i3 = i9 - 1;
                    if (i9 <= 0 || i8 >= 404) {
                        break;
                    }
                    bArr2[i8] = bArr2[i8 - 1];
                    i8++;
                }
            } else {
                if (r6 == 18) {
                    i = (m19800() >>> 13) + 3;
                    m19803(3);
                } else {
                    i = (m19800() >>> 9) + 11;
                    m19803(7);
                }
                while (true) {
                    int i10 = i;
                    i2 = i8;
                    i = i10 - 1;
                    if (i10 <= 0 || i2 >= 404) {
                        i8 = i2;
                    } else {
                        i8 = i2 + 1;
                        bArr2[i2] = 0;
                    }
                }
                i8 = i2;
            }
        }
        this.f15632 = true;
        if (this.f15831 > this.f15660) {
            return false;
        }
        m19610(bArr2, 0, this.f15712, 299);
        m19610(bArr2, 299, this.f15713, 60);
        m19610(bArr2, 359, this.f15708, 17);
        m19610(bArr2, 376, this.f15714, 28);
        for (int i11 = 0; i11 < this.f15633.length; i11++) {
            this.f15633[i11] = bArr2[i11];
        }
        return true;
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private boolean m19569() throws IOException, RarException {
        int r0 = m19799() >> 8;
        m19804(8);
        int i = (r0 & 7) + 1;
        if (i == 7) {
            i = (m19799() >> 8) + 7;
            m19804(8);
        } else if (i == 8) {
            i = m19799();
            m19804(16);
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < i; i2++) {
            if (this.f15831 >= this.f15660 - 1 && !m19598() && i2 < i - 1) {
                return false;
            }
            arrayList.add(Byte.valueOf((byte) (m19799() >> 8)));
            m19804(8);
        }
        return m19579(r0, (List<Byte>) arrayList, i);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m19570() throws IOException {
        UnpackFilter unpackFilter;
        int i = this.f15670;
        int i2 = (this.f15681 - i) & 4194303;
        int i3 = 0;
        while (i3 < this.f15629.size()) {
            UnpackFilter unpackFilter2 = this.f15629.get(i3);
            if (unpackFilter2 != null) {
                if (unpackFilter2.m19616()) {
                    unpackFilter2.m19621(false);
                } else {
                    int r6 = unpackFilter2.m19613();
                    int r5 = unpackFilter2.m19619();
                    if (((r6 - i) & 4194303) >= i2) {
                        continue;
                    } else {
                        if (i != r6) {
                            m19576(i, r6);
                            i = r6;
                            i2 = (this.f15681 - i) & 4194303;
                        }
                        if (r5 <= i2) {
                            int i4 = (r6 + r5) & 4194303;
                            if (r6 < i4 || i4 == 0) {
                                this.f15627.m19821(0, this.f15676, r6, r5);
                            } else {
                                int i5 = 4194304 - r6;
                                this.f15627.m19821(0, this.f15676, r6, i5);
                                this.f15627.m19821(i5, this.f15676, 0, i4);
                            }
                            VMPreparedProgram r15 = this.f15628.get(unpackFilter2.m19612()).m19611();
                            VMPreparedProgram r16 = unpackFilter2.m19611();
                            if (r15.m19839().size() > 64) {
                                r16.m19839().setSize(r15.m19839().size());
                                for (int i6 = 0; i6 < r15.m19839().size() - 64; i6++) {
                                    r16.m19839().set(i6 + 64, r15.m19839().get(i6 + 64));
                                }
                            }
                            m19577(r16);
                            if (r16.m19839().size() > 64) {
                                if (r15.m19839().size() < r16.m19839().size()) {
                                    r15.m19839().setSize(r16.m19839().size());
                                }
                                for (int i7 = 0; i7 < r16.m19839().size() - 64; i7++) {
                                    r15.m19839().set(i7 + 64, r16.m19839().get(i7 + 64));
                                }
                            } else {
                                r15.m19839().clear();
                            }
                            int r8 = r16.m19845();
                            int r9 = r16.m19842();
                            byte[] bArr = new byte[r9];
                            for (int i8 = 0; i8 < r9; i8++) {
                                bArr[i8] = this.f15627.m19819()[r8 + i8];
                            }
                            this.f15629.set(i3, (Object) null);
                            while (i3 + 1 < this.f15629.size() && (unpackFilter = this.f15629.get(i3 + 1)) != null && unpackFilter.m19613() == r6 && unpackFilter.m19619() == r9 && !unpackFilter.m19616()) {
                                this.f15627.m19821(0, bArr, 0, r9);
                                VMPreparedProgram r22 = this.f15628.get(unpackFilter.m19612()).m19611();
                                VMPreparedProgram r14 = unpackFilter.m19611();
                                if (r22.m19839().size() > 64) {
                                    r14.m19839().setSize(r22.m19839().size());
                                    for (int i9 = 0; i9 < r22.m19839().size() - 64; i9++) {
                                        r14.m19839().set(i9 + 64, r22.m19839().get(i9 + 64));
                                    }
                                }
                                m19577(r14);
                                if (r14.m19839().size() > 64) {
                                    if (r22.m19839().size() < r14.m19839().size()) {
                                        r22.m19839().setSize(r14.m19839().size());
                                    }
                                    for (int i10 = 0; i10 < r14.m19839().size() - 64; i10++) {
                                        r22.m19839().set(i10 + 64, r14.m19839().get(i10 + 64));
                                    }
                                } else {
                                    r22.m19839().clear();
                                }
                                int r82 = r14.m19845();
                                r9 = r14.m19842();
                                bArr = new byte[r9];
                                for (int i11 = 0; i11 < r9; i11++) {
                                    bArr[i11] = r14.m19839().get(r82 + i11).byteValue();
                                }
                                i3++;
                                this.f15629.set(i3, (Object) null);
                            }
                            this.f15689.m19559(bArr, 0, r9);
                            this.f15658 = true;
                            this.f15635 += (long) r9;
                            i = i4;
                            i2 = (this.f15681 - i) & 4194303;
                        } else {
                            for (int i12 = i3; i12 < this.f15629.size(); i12++) {
                                UnpackFilter unpackFilter3 = this.f15629.get(i12);
                                if (unpackFilter3 != null && unpackFilter3.m19616()) {
                                    unpackFilter3.m19621(false);
                                }
                            }
                            this.f15670 = i;
                            return;
                        }
                    }
                }
            }
            i3++;
        }
        m19576(i, this.f15681);
        this.f15670 = this.f15681;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m19571() {
        this.f15630.clear();
        this.f15631 = 0;
        this.f15628.clear();
        this.f15629.clear();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean m19572() throws IOException, RarException {
        boolean z;
        int r0 = m19799();
        boolean z2 = false;
        if ((32768 & r0) != 0) {
            z = true;
            m19804(1);
        } else {
            z2 = true;
            z = (r0 & 16384) != 0;
            m19804(2);
        }
        this.f15632 = !z;
        return !z2 && (!z || m19568());
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean m19573() throws IOException, RarException {
        int r1;
        int r3 = this.f15625.m19681();
        if (r3 == -1) {
            return false;
        }
        int i = (r3 & 7) + 1;
        if (i == 7) {
            int r0 = this.f15625.m19681();
            if (r0 == -1) {
                return false;
            }
            i = r0 + 7;
        } else if (i == 8) {
            int r02 = this.f15625.m19681();
            if (r02 == -1 || (r1 = this.f15625.m19681()) == -1) {
                return false;
            }
            i = (r02 * 256) + r1;
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < i; i2++) {
            int r2 = this.f15625.m19681();
            if (r2 == -1) {
                return false;
            }
            arrayList.add(Byte.valueOf((byte) r2));
        }
        return m19579(r3, (List<Byte>) arrayList, i);
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private void m19574(int i, int i2) {
        int i3;
        int i4 = this.f15681 - i2;
        if (i4 >= 0 && i4 < 4194044 && this.f15681 < 4194044) {
            byte[] bArr = this.f15676;
            int i5 = this.f15681;
            this.f15681 = i5 + 1;
            i3 = i4 + 1;
            bArr[i5] = this.f15676[i4];
            while (true) {
                i--;
                if (i <= 0) {
                    break;
                }
                byte[] bArr2 = this.f15676;
                int i6 = this.f15681;
                this.f15681 = i6 + 1;
                bArr2[i6] = this.f15676[i3];
                i3++;
            }
        } else {
            while (true) {
                i3 = i4;
                int i7 = i;
                i = i7 - 1;
                if (i7 == 0) {
                    break;
                }
                i4 = i3 + 1;
                this.f15676[this.f15681] = this.f15676[i3 & 4194303];
                this.f15681 = (this.f15681 + 1) & 4194303;
            }
        }
        int i8 = i3;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private void m19575(int i, int i2) {
        this.f15679 = i2;
        this.f15669 = i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m19576(int i, int i2) throws IOException {
        if (i2 != i) {
            this.f15658 = true;
        }
        if (i2 < i) {
            m19578(this.f15676, i, (-i) & 4194303);
            m19578(this.f15676, 0, i2);
            this.f15691 = true;
            return;
        }
        m19578(this.f15676, i, i2 - i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19577(VMPreparedProgram vMPreparedProgram) {
        if (vMPreparedProgram.m19839().size() > 0) {
            vMPreparedProgram.m19840()[6] = (int) this.f15635;
            this.f15627.m19822(vMPreparedProgram.m19839(), 36, (int) this.f15635);
            this.f15627.m19822(vMPreparedProgram.m19839(), 40, (int) (this.f15635 >>> 32));
            this.f15627.m19823(vMPreparedProgram);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19578(byte[] bArr, int i, int i2) throws IOException {
        if (this.f15635 < this.f15662) {
            int i3 = i2;
            long j = this.f15662 - this.f15635;
            if (((long) i3) > j) {
                i3 = (int) j;
            }
            this.f15689.m19559(bArr, i, i3);
            this.f15635 += (long) i2;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19579(int i, List<Byte> list, int i2) {
        int i3;
        UnpackFilter unpackFilter;
        int r6;
        BitInput bitInput = new BitInput();
        bitInput.m19801();
        for (int i4 = 0; i4 < Math.min(32768, list.size()); i4++) {
            bitInput.m19805()[i4] = list.get(i4).byteValue();
        }
        this.f15627.m19820();
        if ((i & 128) != 0) {
            i3 = RarVM.m19808(bitInput);
            if (i3 == 0) {
                m19571();
            } else {
                i3--;
            }
        } else {
            i3 = this.f15631;
        }
        if (i3 > this.f15628.size() || i3 > this.f15630.size()) {
            return false;
        }
        this.f15631 = i3;
        boolean z = i3 == this.f15628.size();
        UnpackFilter unpackFilter2 = new UnpackFilter();
        if (!z) {
            unpackFilter = this.f15628.get(i3);
            unpackFilter2.m19615(i3);
            unpackFilter.m19618(unpackFilter.m19617() + 1);
        } else if (i3 > 1024) {
            return false;
        } else {
            unpackFilter = new UnpackFilter();
            this.f15628.add(unpackFilter);
            unpackFilter2.m19615(this.f15628.size() - 1);
            this.f15630.add(0);
            unpackFilter.m19618(0);
        }
        this.f15629.add(unpackFilter2);
        unpackFilter2.m19618(unpackFilter.m19617());
        int r4 = RarVM.m19808(bitInput);
        if ((i & 64) != 0) {
            r4 += 258;
        }
        unpackFilter2.m19614((this.f15681 + r4) & 4194303);
        if ((i & 32) != 0) {
            unpackFilter2.m19620(RarVM.m19808(bitInput));
        } else {
            unpackFilter2.m19620(i3 < this.f15630.size() ? this.f15630.get(i3).intValue() : 0);
        }
        unpackFilter2.m19621(this.f15670 != this.f15681 && ((this.f15670 - this.f15681) & 4194303) <= r4);
        this.f15630.set(i3, Integer.valueOf(unpackFilter2.m19619()));
        Arrays.fill(unpackFilter2.m19611().m19840(), 0);
        unpackFilter2.m19611().m19840()[3] = 245760;
        unpackFilter2.m19611().m19840()[4] = unpackFilter2.m19619();
        unpackFilter2.m19611().m19840()[5] = unpackFilter2.m19617();
        if ((i & 16) != 0) {
            int r10 = bitInput.m19800() >>> 9;
            bitInput.m19803(7);
            for (int i5 = 0; i5 < 7; i5++) {
                if (((1 << i5) & r10) != 0) {
                    unpackFilter2.m19611().m19840()[i5] = RarVM.m19808(bitInput);
                }
            }
        }
        if (z) {
            int r16 = RarVM.m19808(bitInput);
            if (r16 >= 65536 || r16 == 0) {
                return false;
            }
            byte[] bArr = new byte[r16];
            for (int i6 = 0; i6 < r16; i6++) {
                if (bitInput.m19802(3)) {
                    return false;
                }
                bArr[i6] = (byte) (bitInput.m19800() >> 8);
                bitInput.m19803(8);
            }
            this.f15627.m19824(bArr, r16, unpackFilter.m19611());
        }
        unpackFilter2.m19611().m19850(unpackFilter.m19611().m19843());
        unpackFilter2.m19611().m19849(unpackFilter.m19611().m19846());
        int size = unpackFilter.m19611().m19841().size();
        if (size > 0 && size < 8192) {
            unpackFilter2.m19611().m19851(unpackFilter.m19611().m19841());
        }
        if (unpackFilter2.m19611().m19839().size() < 64) {
            unpackFilter2.m19611().m19839().clear();
            unpackFilter2.m19611().m19839().setSize(64);
        }
        Vector<Byte> r17 = unpackFilter2.m19611().m19839();
        for (int i7 = 0; i7 < 7; i7++) {
            this.f15627.m19822(r17, i7 * 4, unpackFilter2.m19611().m19840()[i7]);
        }
        this.f15627.m19822(r17, 28, unpackFilter2.m19619());
        this.f15627.m19822(r17, 32, 0);
        this.f15627.m19822(r17, 36, 0);
        this.f15627.m19822(r17, 40, 0);
        this.f15627.m19822(r17, 44, unpackFilter2.m19617());
        for (int i8 = 0; i8 < 16; i8++) {
            r17.set(i8 + 48, (Byte) null);
        }
        if ((i & 8) != 0) {
            if (bitInput.m19802(3) || (r6 = RarVM.m19808(bitInput)) > 8128) {
                return false;
            }
            int size2 = unpackFilter2.m19611().m19839().size();
            if (size2 < r6 + 64) {
                unpackFilter2.m19611().m19839().setSize((r6 + 64) - size2);
            }
            Vector<Byte> r172 = unpackFilter2.m19611().m19839();
            for (int i9 = 0; i9 < r6; i9++) {
                if (bitInput.m19802(3)) {
                    return false;
                }
                r172.set(64 + i9, Byte.valueOf((byte) (bitInput.m19800() >>> 8)));
                bitInput.m19803(8);
            }
        }
        return true;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    private void m19580() throws IOException, RarException {
        byte[] bArr = new byte[65536];
        while (true) {
            int r1 = this.f15689.m19561(bArr, 0, (int) Math.min((long) bArr.length, this.f15662));
            if (r1 != 0 && r1 != -1) {
                if (((long) r1) >= this.f15662) {
                    r1 = (int) this.f15662;
                }
                this.f15689.m19559(bArr, 0, r1);
                if (this.f15662 >= 0) {
                    this.f15662 -= (long) r1;
                }
            } else {
                return;
            }
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19581() {
        SubAllocator r0;
        if (this.f15625 != null && (r0 = this.f15625.m19687()) != null) {
            r0.m19793();
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19582() throws IOException, RarException {
        if (this.f15831 > 32738) {
            m19598();
        }
        byte[] bArr = this.f15833;
        int i = this.f15831;
        this.f15831 = i + 1;
        return bArr[i] & 255;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19583(int i) {
        this.f15626 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19584(int i, boolean z) throws IOException, RarException {
        if (this.f15689.m19560().m19532() == 48) {
            m19580();
        }
        switch (i) {
            case 15:
                m19595(z);
                return;
            case 20:
            case 26:
                m19608(z);
                return;
            case 29:
            case 36:
                m19567(z);
                return;
            default:
                return;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19585(long j) {
        this.f15662 = j;
        this.f15636 = false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: 龘  reason: contains not printable characters */
    public void m19586(boolean z) {
        if (!z) {
            this.f15632 = false;
            Arrays.fill(this.f15680, 0);
            this.f15664 = 0;
            this.f15679 = 0;
            this.f15669 = 0;
            Arrays.fill(this.f15633, (byte) 0);
            this.f15681 = 0;
            this.f15670 = 0;
            this.f15626 = 2;
            m19571();
        }
        m19801();
        this.f15637 = false;
        this.f15635 = 0;
        this.f15660 = 0;
        this.f15690 = 0;
        m19605(z);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19587(byte[] bArr) {
        if (bArr == null) {
            this.f15676 = new byte[4194304];
        } else {
            this.f15676 = bArr;
        }
        this.f15831 = 0;
        m19586(false);
    }
}
