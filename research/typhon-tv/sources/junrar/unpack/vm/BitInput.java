package junrar.unpack.vm;

public class BitInput {

    /* renamed from: ʻˊ  reason: contains not printable characters */
    protected int f15831;

    /* renamed from: ʻˋ  reason: contains not printable characters */
    protected int f15832;

    /* renamed from: ʻˎ  reason: contains not printable characters */
    protected byte[] f15833 = new byte[32768];

    /* renamed from: ʾ  reason: contains not printable characters */
    public int m19799() {
        return (((((this.f15833[this.f15831] & 255) << 16) + ((this.f15833[this.f15831 + 1] & 255) << 8)) + (this.f15833[this.f15831 + 2] & 255)) >>> (8 - this.f15832)) & 65535;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int m19800() {
        return m19799();
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public void m19801() {
        this.f15831 = 0;
        this.f15832 = 0;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public boolean m19802(int i) {
        return this.f15831 + i >= 32768;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19803(int i) {
        m19804(i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19804(int i) {
        int i2 = i + this.f15832;
        this.f15831 += i2 >> 3;
        this.f15832 = i2 & 7;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public byte[] m19805() {
        return this.f15833;
    }
}
