package junrar.unpack.vm;

import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.view.InputDeviceCompat;
import java.util.List;
import java.util.Vector;
import junrar.crc.RarCRC;
import junrar.io.Raw;

public class RarVM extends BitInput {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f15834;

    /* renamed from: 连任  reason: contains not printable characters */
    private int f15835;

    /* renamed from: 靐  reason: contains not printable characters */
    private int[] f15836 = new int[8];

    /* renamed from: 麤  reason: contains not printable characters */
    private int f15837 = 25000000;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f15838;

    /* renamed from: 龘  reason: contains not printable characters */
    private byte[] f15839 = null;

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19806(VMPreparedProgram vMPreparedProgram) {
        List<VMPreparedCommand> r1 = vMPreparedProgram.m19843();
        for (VMPreparedCommand next : r1) {
            switch (next.m19826()) {
                case VM_MOV:
                    next.m19828(next.m19830() ? VMCommands.VM_MOVB : VMCommands.VM_MOVD);
                    break;
                case VM_CMP:
                    next.m19828(next.m19830() ? VMCommands.VM_CMPB : VMCommands.VM_CMPD);
                    break;
                default:
                    if ((VMCmdFlags.f15842[next.m19826().getVMCommand()] & 64) == 0) {
                        break;
                    } else {
                        boolean z = false;
                        int indexOf = r1.indexOf(next) + 1;
                        while (true) {
                            if (indexOf < r1.size()) {
                                byte b = VMCmdFlags.f15842[r1.get(indexOf).m19826().getVMCommand()];
                                if ((b & 56) != 0) {
                                    z = true;
                                } else if ((b & 64) == 0) {
                                    indexOf++;
                                }
                            }
                        }
                        if (z) {
                            break;
                        } else {
                            switch (next.m19826()) {
                                case VM_ADD:
                                    next.m19828(next.m19830() ? VMCommands.VM_ADDB : VMCommands.VM_ADDD);
                                    break;
                                case VM_SUB:
                                    next.m19828(next.m19830() ? VMCommands.VM_SUBB : VMCommands.VM_SUBD);
                                    break;
                                case VM_INC:
                                    next.m19828(next.m19830() ? VMCommands.VM_INCB : VMCommands.VM_INCD);
                                    break;
                                case VM_DEC:
                                    next.m19828(next.m19830() ? VMCommands.VM_DECB : VMCommands.VM_DECD);
                                    break;
                                case VM_NEG:
                                    next.m19828(next.m19830() ? VMCommands.VM_NEGB : VMCommands.VM_NEGD);
                                    break;
                            }
                        }
                    }
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m19807(int i, int i2, int i3) {
        int i4 = i2 / 8;
        int i5 = i4 + 1;
        int i6 = i5 + 1;
        return (-1 >>> (32 - i3)) & (((((this.f15839[i + i4] & 255) | ((this.f15839[i + i5] & 255) << 8)) | ((this.f15839[i + i6] & 255) << 16)) | ((this.f15839[i + (i6 + 1)] & 255) << 24)) >>> (i2 & 7));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static int m19808(BitInput bitInput) {
        int i;
        int r0 = bitInput.m19800();
        switch (49152 & r0) {
            case 0:
                bitInput.m19803(6);
                int i2 = r0;
                return (r0 >> 10) & 15;
            case 16384:
                if ((r0 & 15360) == 0) {
                    i = ((r0 >> 2) & 255) | InputDeviceCompat.SOURCE_ANY;
                    bitInput.m19803(14);
                } else {
                    i = (r0 >> 6) & 255;
                    bitInput.m19803(10);
                }
                int i3 = i;
                return i;
            case 32768:
                bitInput.m19803(2);
                int r02 = bitInput.m19800();
                bitInput.m19803(16);
                int i4 = r02;
                return r02;
            default:
                bitInput.m19803(2);
                bitInput.m19803(16);
                int r03 = (bitInput.m19800() << 16) | bitInput.m19800();
                bitInput.m19803(16);
                int i5 = r03;
                return r03;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m19809(VMPreparedOperand vMPreparedOperand) {
        if (vMPreparedOperand.m19834() == VMOpType.VM_OPREGMEM) {
            return Raw.m19490(this.f15839, (vMPreparedOperand.m19833() + vMPreparedOperand.m19836()) & 262143);
        }
        return Raw.m19490(this.f15839, vMPreparedOperand.m19833());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m19810(boolean z, byte[] bArr, int i) {
        return z ? m19818(bArr) ? bArr[i] : bArr[i] & 255 : m19818(bArr) ? Raw.m19490(bArr, i) : Raw.m19492(bArr, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private VMStandardFilters m19811(byte[] bArr, int i) {
        VMStandardFilterSignature[] vMStandardFilterSignatureArr = {new VMStandardFilterSignature(53, -1386780537, VMStandardFilters.VMSF_E8), new VMStandardFilterSignature(57, 1020781950, VMStandardFilters.VMSF_E8E9), new VMStandardFilterSignature(120, 929663295, VMStandardFilters.VMSF_ITANIUM), new VMStandardFilterSignature(29, 235276157, VMStandardFilters.VMSF_DELTA), new VMStandardFilterSignature(149, 472669640, VMStandardFilters.VMSF_RGB), new VMStandardFilterSignature(216, -1132075263, VMStandardFilters.VMSF_AUDIO), new VMStandardFilterSignature(40, 1186579808, VMStandardFilters.VMSF_UPCASE)};
        int r0 = RarCRC.m19477(-1, bArr, 0, bArr.length) ^ -1;
        for (int i2 = 0; i2 < vMStandardFilterSignatureArr.length; i2++) {
            if (vMStandardFilterSignatureArr[i2].m19854() == r0 && vMStandardFilterSignatureArr[i2].m19852() == bArr.length) {
                return vMStandardFilterSignatureArr[i2].m19853();
            }
        }
        return VMStandardFilters.VMSF_NONE;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19812(int i, int i2, int i3, int i4) {
        int i5 = i3 / 8;
        int i6 = i3 & 7;
        byte b = ((-1 >>> (32 - i4)) << i6) ^ -1;
        int i7 = i2 << i6;
        for (int i8 = 0; i8 < 4; i8++) {
            byte[] bArr = this.f15839;
            int i9 = i + i5 + i8;
            bArr[i9] = (byte) (bArr[i9] & b);
            byte[] bArr2 = this.f15839;
            int i10 = i + i5 + i8;
            bArr2[i10] = (byte) (bArr2[i10] | i7);
            b = (b >>> 8) | -16777216;
            i7 >>>= 8;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19813(VMPreparedOperand vMPreparedOperand, boolean z) {
        int r0 = m19800();
        if ((32768 & r0) != 0) {
            vMPreparedOperand.m19838(VMOpType.VM_OPREG);
            vMPreparedOperand.m19832((r0 >> 12) & 7);
            vMPreparedOperand.m19835(vMPreparedOperand.m19831());
            m19803(4);
        } else if ((49152 & r0) == 0) {
            vMPreparedOperand.m19838(VMOpType.VM_OPINT);
            if (z) {
                vMPreparedOperand.m19832((r0 >> 6) & 255);
                m19803(10);
                return;
            }
            m19803(2);
            vMPreparedOperand.m19832(m19808((BitInput) this));
        } else {
            vMPreparedOperand.m19838(VMOpType.VM_OPREGMEM);
            if ((r0 & 8192) == 0) {
                vMPreparedOperand.m19832((r0 >> 10) & 7);
                vMPreparedOperand.m19835(vMPreparedOperand.m19831());
                vMPreparedOperand.m19837(0);
                m19803(6);
                return;
            }
            if ((r0 & 4096) == 0) {
                vMPreparedOperand.m19832((r0 >> 9) & 7);
                vMPreparedOperand.m19835(vMPreparedOperand.m19831());
                m19803(7);
            } else {
                vMPreparedOperand.m19832(0);
                m19803(4);
            }
            vMPreparedOperand.m19837(m19808((BitInput) this));
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19814(VMStandardFilters vMStandardFilters) {
        long j;
        byte b;
        switch (vMStandardFilters) {
            case VMSF_E8:
            case VMSF_E8E9:
                int i = this.f15836[4];
                long j2 = (long) (this.f15836[6] & -1);
                if (i < 245760) {
                    byte b2 = (byte) (vMStandardFilters == VMStandardFilters.VMSF_E8E9 ? 233 : 232);
                    int i2 = 0;
                    while (true) {
                        int i3 = i2;
                        if (i3 < i - 4) {
                            i2 = i3 + 1;
                            byte b3 = this.f15839[i3];
                            if (b3 == -24 || b3 == b2) {
                                long j3 = ((long) i2) + j2;
                                long r6 = (long) m19810(false, this.f15839, i2);
                                if ((-2147483648L & r6) != 0) {
                                    if (((r6 + j3) & -2147483648L) == 0) {
                                        m19815(false, this.f15839, i2, ((int) r6) + 16777216);
                                    }
                                } else if (((r6 - ((long) 16777216)) & -2147483648L) != 0) {
                                    m19815(false, this.f15839, i2, (int) (r6 - j3));
                                }
                                i2 += 4;
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    return;
                }
                break;
            case VMSF_ITANIUM:
                int i4 = this.f15836[4];
                long j4 = (long) (this.f15836[6] & -1);
                if (i4 < 245760) {
                    int i5 = 0;
                    byte[] bArr = {4, 4, 6, 6, 0, 0, 7, 7, 4, 4, 0, 0, 4, 4, 0, 0};
                    long j5 = j4 >>> 4;
                    while (i5 < i4 - 21) {
                        int i6 = (this.f15839[i5] & 31) - 16;
                        if (i6 >= 0 && (b = bArr[i6]) != 0) {
                            for (int i7 = 0; i7 <= 2; i7++) {
                                if (((1 << i7) & b) != 0) {
                                    int i8 = (i7 * 41) + 5;
                                    if (m19807(i5, i8 + 37, 4) == 5) {
                                        m19812(i5, ((int) (((long) m19807(i5, i8 + 13, 20)) - j5)) & 1048575, i8 + 13, 20);
                                    }
                                }
                            }
                        }
                        i5 += 16;
                        j5++;
                    }
                    return;
                }
                return;
            case VMSF_DELTA:
                int i9 = this.f15836[4] & -1;
                int i10 = this.f15836[0] & -1;
                int i11 = 0;
                int i12 = (i9 * 2) & -1;
                m19815(false, this.f15839, 245792, i9);
                if (i9 < 122880) {
                    int i13 = 0;
                    while (i13 < i10) {
                        byte b4 = 0;
                        int i14 = i9 + i13;
                        while (true) {
                            int i15 = i11;
                            if (i14 < i12) {
                                byte[] bArr2 = this.f15839;
                                i11 = i15 + 1;
                                b4 = (byte) (b4 - this.f15839[i15]);
                                bArr2[i14] = b4;
                                i14 += i10;
                            } else {
                                i13++;
                                i11 = i15;
                            }
                        }
                    }
                    return;
                }
                return;
            case VMSF_RGB:
                int i16 = this.f15836[4];
                int i17 = this.f15836[0] - 3;
                int i18 = this.f15836[1];
                int i19 = 0;
                int i20 = i16;
                m19815(false, this.f15839, 245792, i16);
                if (i16 < 122880 && i18 >= 0) {
                    int i21 = 0;
                    while (i21 < 3) {
                        long j6 = 0;
                        int i22 = i21;
                        while (true) {
                            int i23 = i19;
                            if (i22 < i16) {
                                int i24 = i22 - i17;
                                if (i24 >= 3) {
                                    int i25 = i20 + i24;
                                    byte b5 = this.f15839[i25] & 255;
                                    byte b6 = this.f15839[i25 - 3] & 255;
                                    long j7 = (((long) b5) + j6) - ((long) b6);
                                    int abs = Math.abs((int) (j7 - j6));
                                    int abs2 = Math.abs((int) (j7 - ((long) b5)));
                                    int abs3 = Math.abs((int) (j7 - ((long) b6)));
                                    j = (abs > abs2 || abs > abs3) ? abs2 <= abs3 ? (long) b5 : (long) b6 : j6;
                                } else {
                                    j = j6;
                                }
                                i19 = i23 + 1;
                                j6 = (j - ((long) this.f15839[i23])) & 255 & 255;
                                this.f15839[i20 + i22] = (byte) ((int) (255 & j6));
                                i22 += 3;
                            } else {
                                i21++;
                                i19 = i23;
                            }
                        }
                    }
                    int i26 = i16 - 2;
                    for (int i27 = i18; i27 < i26; i27 += 3) {
                        byte b7 = this.f15839[i20 + i27 + 1];
                        byte[] bArr3 = this.f15839;
                        int i28 = i20 + i27;
                        bArr3[i28] = (byte) (bArr3[i28] + b7);
                        byte[] bArr4 = this.f15839;
                        int i29 = i20 + i27 + 2;
                        bArr4[i29] = (byte) (bArr4[i29] + b7);
                    }
                    return;
                }
                return;
            case VMSF_AUDIO:
                int i30 = this.f15836[4];
                int i31 = this.f15836[0];
                int i32 = 0;
                int i33 = i30;
                m19815(false, this.f15839, 245792, i30);
                if (i30 < 122880) {
                    int i34 = 0;
                    while (i34 < i31) {
                        long j8 = 0;
                        long j9 = 0;
                        long[] jArr = new long[7];
                        int i35 = 0;
                        int i36 = 0;
                        int i37 = 0;
                        int i38 = 0;
                        int i39 = 0;
                        int i40 = i34;
                        int i41 = 0;
                        while (true) {
                            int i42 = i32;
                            if (i40 < i30) {
                                int i43 = i36;
                                i36 = ((int) j9) - i35;
                                i35 = (int) j9;
                                i32 = i42 + 1;
                                long j10 = (long) (this.f15839[i42] & 255);
                                long j11 = (((((((8 * j8) + ((long) (i37 * i35))) + ((long) (i38 * i36))) + ((long) (i39 * i43))) >>> 3) & 255) - j10) & -1;
                                this.f15839[i33 + i40] = (byte) ((int) j11);
                                j9 = (long) ((byte) ((int) (j11 - j8)));
                                j8 = j11;
                                int i44 = ((byte) ((int) j10)) << 3;
                                jArr[0] = jArr[0] + ((long) Math.abs(i44));
                                jArr[1] = jArr[1] + ((long) Math.abs(i44 - i35));
                                jArr[2] = jArr[2] + ((long) Math.abs(i44 + i35));
                                jArr[3] = jArr[3] + ((long) Math.abs(i44 - i36));
                                jArr[4] = jArr[4] + ((long) Math.abs(i44 + i36));
                                jArr[5] = jArr[5] + ((long) Math.abs(i44 - i43));
                                jArr[6] = jArr[6] + ((long) Math.abs(i44 + i43));
                                if ((i41 & 31) == 0) {
                                    long j12 = jArr[0];
                                    long j13 = 0;
                                    jArr[0] = 0;
                                    for (int i45 = 1; i45 < jArr.length; i45++) {
                                        if (jArr[i45] < j12) {
                                            j12 = jArr[i45];
                                            j13 = (long) i45;
                                        }
                                        jArr[i45] = 0;
                                    }
                                    switch ((int) j13) {
                                        case 1:
                                            if (i37 < -16) {
                                                break;
                                            } else {
                                                i37--;
                                                break;
                                            }
                                        case 2:
                                            if (i37 >= 16) {
                                                break;
                                            } else {
                                                i37++;
                                                break;
                                            }
                                        case 3:
                                            if (i38 < -16) {
                                                break;
                                            } else {
                                                i38--;
                                                break;
                                            }
                                        case 4:
                                            if (i38 >= 16) {
                                                break;
                                            } else {
                                                i38++;
                                                break;
                                            }
                                        case 5:
                                            if (i39 < -16) {
                                                break;
                                            } else {
                                                i39--;
                                                break;
                                            }
                                        case 6:
                                            if (i39 >= 16) {
                                                break;
                                            } else {
                                                i39++;
                                                break;
                                            }
                                    }
                                }
                                i40 += i31;
                                i41++;
                            } else {
                                i34++;
                                i32 = i42;
                            }
                        }
                    }
                    return;
                }
                return;
            case VMSF_UPCASE:
                int i46 = this.f15836[4];
                int i47 = 0;
                int i48 = i46;
                if (i46 < 122880) {
                    while (true) {
                        int i49 = i48;
                        int i50 = i47;
                        if (i50 < i46) {
                            i47 = i50 + 1;
                            byte b8 = this.f15839[i50];
                            if (b8 == 2) {
                                int i51 = i47 + 1;
                                b8 = this.f15839[i47];
                                if (b8 != 2) {
                                    b8 = (byte) (b8 - 32);
                                    i47 = i51;
                                } else {
                                    i47 = i51;
                                }
                            }
                            i48 = i49 + 1;
                            this.f15839[i49] = b8;
                        } else {
                            m19815(false, this.f15839, 245788, i49 - i46);
                            m19815(false, this.f15839, 245792, i46);
                            return;
                        }
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private void m19815(boolean z, byte[] bArr, int i, int i2) {
        if (z) {
            if (m19818(bArr)) {
                bArr[i] = (byte) i2;
            } else {
                bArr[i] = (byte) ((bArr[i] & 0) | ((byte) (i2 & 255)));
            }
        } else if (m19818(bArr)) {
            Raw.m19491(bArr, i, i2);
        } else {
            Raw.m19493(bArr, i, i2);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19816(int i) {
        if (i >= this.f15835) {
            return true;
        }
        int i2 = this.f15837 - 1;
        this.f15837 = i2;
        if (i2 <= 0) {
            return false;
        }
        this.f15834 = i;
        return true;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m19817(java.util.List<junrar.unpack.vm.VMPreparedCommand> r23, int r24) {
        /*
            r22 = this;
            r14 = 25000000(0x17d7840, float:4.6555036E-38)
            r0 = r22
            r0.f15837 = r14
            r0 = r24
            r1 = r22
            r1.f15835 = r0
            r14 = 0
            r0 = r22
            r0.f15834 = r14
        L_0x0012:
            r0 = r22
            int r14 = r0.f15834
            r0 = r23
            java.lang.Object r6 = r0.get(r14)
            junrar.unpack.vm.VMPreparedCommand r6 = (junrar.unpack.vm.VMPreparedCommand) r6
            junrar.unpack.vm.VMPreparedOperand r14 = r6.m19825()
            r0 = r22
            int r9 = r0.m19809((junrar.unpack.vm.VMPreparedOperand) r14)
            junrar.unpack.vm.VMPreparedOperand r14 = r6.m19827()
            r0 = r22
            int r10 = r0.m19809((junrar.unpack.vm.VMPreparedOperand) r14)
            int[] r14 = junrar.unpack.vm.RarVM.AnonymousClass1.f15841
            junrar.unpack.vm.VMCommands r15 = r6.m19826()
            int r15 = r15.ordinal()
            r14 = r14[r15]
            switch(r14) {
                case 1: goto L_0x0056;
                case 2: goto L_0x007a;
                case 3: goto L_0x0099;
                case 4: goto L_0x00b8;
                case 5: goto L_0x00f7;
                case 6: goto L_0x0130;
                case 7: goto L_0x0169;
                case 8: goto L_0x01e9;
                case 9: goto L_0x0234;
                case 10: goto L_0x027f;
                case 11: goto L_0x02db;
                case 12: goto L_0x0326;
                case 13: goto L_0x0371;
                case 14: goto L_0x0390;
                case 15: goto L_0x03af;
                case 16: goto L_0x03ee;
                case 17: goto L_0x041c;
                case 18: goto L_0x044a;
                case 19: goto L_0x0481;
                case 20: goto L_0x04af;
                case 21: goto L_0x04dd;
                case 22: goto L_0x04ef;
                case 23: goto L_0x0534;
                case 24: goto L_0x0579;
                case 25: goto L_0x05be;
                case 26: goto L_0x05f6;
                case 27: goto L_0x0615;
                case 28: goto L_0x0634;
                case 29: goto L_0x0653;
                case 30: goto L_0x067a;
                case 31: goto L_0x06a1;
                case 32: goto L_0x06c0;
                case 33: goto L_0x06fc;
                case 34: goto L_0x0738;
                case 35: goto L_0x077a;
                case 36: goto L_0x07a1;
                case 37: goto L_0x07f7;
                case 38: goto L_0x0845;
                case 39: goto L_0x0893;
                case 40: goto L_0x08cc;
                case 41: goto L_0x08f1;
                case 42: goto L_0x0916;
                case 43: goto L_0x0951;
                case 44: goto L_0x0983;
                case 45: goto L_0x09b3;
                case 46: goto L_0x09e0;
                case 47: goto L_0x0a00;
                case 48: goto L_0x0a25;
                case 49: goto L_0x0a65;
                case 50: goto L_0x0aab;
                case 51: goto L_0x0ada;
                case 52: goto L_0x0b57;
                case 53: goto L_0x0bd4;
                case 54: goto L_0x0c0f;
                default: goto L_0x0041;
            }
        L_0x0041:
            r0 = r22
            int r14 = r0.f15834
            int r14 = r14 + 1
            r0 = r22
            r0.f15834 = r14
            r0 = r22
            int r14 = r0.f15837
            int r14 = r14 + -1
            r0 = r22
            r0.f15837 = r14
            goto L_0x0012
        L_0x0056:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            boolean r16 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x007a:
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x0099:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x00b8:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r10)
            int r11 = r12 - r14
            if (r11 != 0) goto L_0x00e4
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x00e4:
            if (r11 <= r12) goto L_0x00ed
            r14 = 1
        L_0x00e7:
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x00ed:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            r14 = r14 | 0
            goto L_0x00e7
        L_0x00f7:
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r10)
            int r11 = r12 - r14
            if (r11 != 0) goto L_0x011d
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x011d:
            if (r11 <= r12) goto L_0x0126
            r14 = 1
        L_0x0120:
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x0126:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            r14 = r14 | 0
            goto L_0x0120
        L_0x0130:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r10)
            int r11 = r12 - r14
            if (r11 != 0) goto L_0x0156
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x0156:
            if (r11 <= r12) goto L_0x015f
            r14 = 1
        L_0x0159:
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x015f:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            r14 = r14 | 0
            goto L_0x0159
        L_0x0169:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            long r14 = (long) r12
            boolean r16 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            long r14 = r14 + r16
            r16 = -1
            long r14 = r14 & r16
            int r11 = (int) r14
            boolean r14 = r6.m19830()
            if (r14 == 0) goto L_0x01ce
            r11 = r11 & 255(0xff, float:3.57E-43)
            if (r11 >= r12) goto L_0x01b6
            r14 = 1
        L_0x01a3:
            r0 = r22
            r0.f15838 = r14
        L_0x01a7:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x01b6:
            if (r11 != 0) goto L_0x01c1
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x01be:
            r14 = r14 | 0
            goto L_0x01a3
        L_0x01c1:
            r14 = r11 & 128(0x80, float:1.794E-43)
            if (r14 == 0) goto L_0x01cc
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            goto L_0x01be
        L_0x01cc:
            r14 = 0
            goto L_0x01be
        L_0x01ce:
            if (r11 >= r12) goto L_0x01d6
            r14 = 1
        L_0x01d1:
            r0 = r22
            r0.f15838 = r14
            goto L_0x01a7
        L_0x01d6:
            if (r11 != 0) goto L_0x01e1
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x01de:
            r14 = r14 | 0
            goto L_0x01d1
        L_0x01e1:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x01de
        L_0x01e9:
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = -1
            r20 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r21 = r0
            r0 = r22
            r1 = r20
            r2 = r21
            int r20 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r20
            long r0 = (long) r0
            r20 = r0
            long r18 = r18 + r20
            long r16 = r16 & r18
            r18 = -1
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x0234:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = -1
            r20 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r21 = r0
            r0 = r22
            r1 = r20
            r2 = r21
            int r20 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r20
            long r0 = (long) r0
            r20 = r0
            long r18 = r18 + r20
            long r16 = r16 & r18
            r18 = -1
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x027f:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            long r14 = (long) r12
            r16 = -1
            boolean r18 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r19 = r0
            r0 = r22
            r1 = r18
            r2 = r19
            int r18 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r18
            long r0 = (long) r0
            r18 = r0
            long r16 = r16 - r18
            long r14 = r14 & r16
            r16 = -1
            long r14 = r14 & r16
            int r11 = (int) r14
            if (r11 != 0) goto L_0x02cd
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x02ba:
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x02cd:
            if (r11 <= r12) goto L_0x02d1
            r14 = 1
            goto L_0x02ba
        L_0x02d1:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            r14 = r14 | 0
            goto L_0x02ba
        L_0x02db:
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = -1
            r20 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r21 = r0
            r0 = r22
            r1 = r20
            r2 = r21
            int r20 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r20
            long r0 = (long) r0
            r20 = r0
            long r18 = r18 - r20
            long r16 = r16 & r18
            r18 = -1
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x0326:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = -1
            r20 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r21 = r0
            r0 = r22
            r1 = r20
            r2 = r21
            int r20 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r20
            long r0 = (long) r0
            r20 = r0
            long r18 = r18 - r20
            long r16 = r16 & r18
            r18 = -1
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x0371:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FZ
            int r15 = r15.getFlag()
            r14 = r14 & r15
            if (r14 == 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x0390:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FZ
            int r15 = r15.getFlag()
            r14 = r14 & r15
            if (r14 != 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x03af:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            long r14 = (long) r14
            r16 = 0
            long r14 = r14 & r16
            int r11 = (int) r14
            boolean r14 = r6.m19830()
            if (r14 == 0) goto L_0x03cb
            r11 = r11 & 255(0xff, float:3.57E-43)
        L_0x03cb:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            if (r11 != 0) goto L_0x03e6
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x03e0:
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x03e6:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x03e0
        L_0x03ee:
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = 0
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x041c:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = 0
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x044a:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            long r14 = (long) r14
            r16 = -2
            long r14 = r14 & r16
            int r11 = (int) r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            if (r11 != 0) goto L_0x0479
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x0473:
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x0479:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x0473
        L_0x0481:
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = -2
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x04af:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            long r0 = (long) r0
            r16 = r0
            r18 = -2
            long r16 = r16 & r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x04dd:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x04ef:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r15 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r16 = r0
            r0 = r22
            r1 = r16
            int r15 = r0.m19810((boolean) r15, (byte[]) r1, (int) r10)
            r11 = r14 ^ r15
            if (r11 != 0) goto L_0x052c
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x0519:
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x052c:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x0519
        L_0x0534:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r15 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r16 = r0
            r0 = r22
            r1 = r16
            int r15 = r0.m19810((boolean) r15, (byte[]) r1, (int) r10)
            r11 = r14 & r15
            if (r11 != 0) goto L_0x0571
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x055e:
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x0571:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x055e
        L_0x0579:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r15 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r16 = r0
            r0 = r22
            r1 = r16
            int r15 = r0.m19810((boolean) r15, (byte[]) r1, (int) r10)
            r11 = r14 | r15
            if (r11 != 0) goto L_0x05b6
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x05a3:
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x05b6:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x05a3
        L_0x05be:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r15 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r16 = r0
            r0 = r22
            r1 = r16
            int r15 = r0.m19810((boolean) r15, (byte[]) r1, (int) r10)
            r11 = r14 & r15
            if (r11 != 0) goto L_0x05ee
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x05e8:
            r0 = r22
            r0.f15838 = r14
            goto L_0x0041
        L_0x05ee:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x05e8
        L_0x05f6:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FS
            int r15 = r15.getFlag()
            r14 = r14 & r15
            if (r14 == 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x0615:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FS
            int r15 = r15.getFlag()
            r14 = r14 & r15
            if (r14 != 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x0634:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FC
            int r15 = r15.getFlag()
            r14 = r14 & r15
            if (r14 == 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x0653:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FC
            int r15 = r15.getFlag()
            junrar.unpack.vm.VMFlags r16 = junrar.unpack.vm.VMFlags.VM_FZ
            int r16 = r16.getFlag()
            r15 = r15 | r16
            r14 = r14 & r15
            if (r14 == 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x067a:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FC
            int r15 = r15.getFlag()
            junrar.unpack.vm.VMFlags r16 = junrar.unpack.vm.VMFlags.VM_FZ
            int r16 = r16.getFlag()
            r15 = r15 | r16
            r14 = r14 & r15
            if (r14 != 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x06a1:
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FC
            int r15 = r15.getFlag()
            r14 = r14 & r15
            if (r14 != 0) goto L_0x0041
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x06c0:
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r16 = r14[r15]
            int r16 = r16 + -4
            r14[r15] = r16
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int[] r0 = r0.f15836
            r16 = r0
            r17 = 7
            r16 = r16[r17]
            r17 = 262143(0x3ffff, float:3.6734E-40)
            r16 = r16 & r17
            r17 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r18 = r0
            r0 = r22
            r1 = r17
            r2 = r18
            int r17 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r22
            r1 = r16
            r2 = r17
            r0.m19815((boolean) r14, (byte[]) r15, (int) r1, (int) r2)
            goto L_0x0041
        L_0x06fc:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            int[] r0 = r0.f15836
            r18 = r0
            r19 = 7
            r18 = r18[r19]
            r19 = 262143(0x3ffff, float:3.6734E-40)
            r18 = r18 & r19
            r0 = r22
            r1 = r16
            r2 = r17
            r3 = r18
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r3)
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r16 = r14[r15]
            int r16 = r16 + 4
            r14[r15] = r16
            goto L_0x0041
        L_0x0738:
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r16 = r14[r15]
            int r16 = r16 + -4
            r14[r15] = r16
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int[] r0 = r0.f15836
            r16 = r0
            r17 = 7
            r16 = r16[r17]
            r17 = 262143(0x3ffff, float:3.6734E-40)
            r16 = r16 & r17
            r0 = r22
            int r0 = r0.f15834
            r17 = r0
            int r17 = r17 + 1
            r0 = r22
            r1 = r16
            r2 = r17
            r0.m19815((boolean) r14, (byte[]) r15, (int) r1, (int) r2)
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            r0.m19816((int) r14)
            goto L_0x0012
        L_0x077a:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            boolean r16 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r16 = r16 ^ -1
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x07a1:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r13 = r0.m19810((boolean) r14, (byte[]) r15, (int) r10)
            int r11 = r12 << r13
            if (r11 != 0) goto L_0x07ec
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
            r15 = r14
        L_0x07c8:
            int r14 = r13 + -1
            int r14 = r12 << r14
            r16 = -2147483648(0xffffffff80000000, float:-0.0)
            r14 = r14 & r16
            if (r14 == 0) goto L_0x07f5
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FC
            int r14 = r14.getFlag()
        L_0x07d8:
            r14 = r14 | r15
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x07ec:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            r15 = r14
            goto L_0x07c8
        L_0x07f5:
            r14 = 0
            goto L_0x07d8
        L_0x07f7:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r13 = r0.m19810((boolean) r14, (byte[]) r15, (int) r10)
            int r11 = r12 >>> r13
            if (r11 != 0) goto L_0x083d
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x081d:
            int r15 = r13 + -1
            int r15 = r12 >>> r15
            junrar.unpack.vm.VMFlags r16 = junrar.unpack.vm.VMFlags.VM_FC
            int r16 = r16.getFlag()
            r15 = r15 & r16
            r14 = r14 | r15
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x083d:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x081d
        L_0x0845:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r13 = r0.m19810((boolean) r14, (byte[]) r15, (int) r10)
            int r11 = r12 >> r13
            if (r11 != 0) goto L_0x088b
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x086b:
            int r15 = r13 + -1
            int r15 = r12 >> r15
            junrar.unpack.vm.VMFlags r16 = junrar.unpack.vm.VMFlags.VM_FC
            int r16 = r16.getFlag()
            r15 = r15 & r16
            r14 = r14 | r15
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x088b:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x086b
        L_0x0893:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            int r11 = -r14
            if (r11 != 0) goto L_0x08bd
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x08aa:
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x08bd:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FC
            int r14 = r14.getFlag()
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FS
            int r15 = r15.getFlag()
            r15 = r15 & r11
            r14 = r14 | r15
            goto L_0x08aa
        L_0x08cc:
            r14 = 1
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            int r0 = -r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x08f1:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r9)
            r0 = r16
            int r0 = -r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x0916:
            r8 = 0
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r14 = r14[r15]
            int r5 = r14 + -4
        L_0x0920:
            r14 = 8
            if (r8 >= r14) goto L_0x0944
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 262143(0x3ffff, float:3.6734E-40)
            r16 = r16 & r5
            r0 = r22
            int[] r0 = r0.f15836
            r17 = r0
            r17 = r17[r8]
            r0 = r22
            r1 = r16
            r2 = r17
            r0.m19815((boolean) r14, (byte[]) r15, (int) r1, (int) r2)
            int r8 = r8 + 1
            int r5 = r5 + -4
            goto L_0x0920
        L_0x0944:
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r16 = r14[r15]
            int r16 = r16 + -32
            r14[r15] = r16
            goto L_0x0041
        L_0x0951:
            r8 = 0
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r5 = r14[r15]
        L_0x0959:
            r14 = 8
            if (r8 >= r14) goto L_0x0041
            r0 = r22
            int[] r14 = r0.f15836
            int r15 = 7 - r8
            r16 = 0
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r18 = 262143(0x3ffff, float:3.6734E-40)
            r18 = r18 & r5
            r0 = r22
            r1 = r16
            r2 = r17
            r3 = r18
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r3)
            r14[r15] = r16
            int r8 = r8 + 1
            int r5 = r5 + 4
            goto L_0x0959
        L_0x0983:
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r16 = r14[r15]
            int r16 = r16 + -4
            r14[r15] = r16
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int[] r0 = r0.f15836
            r16 = r0
            r17 = 7
            r16 = r16[r17]
            r17 = 262143(0x3ffff, float:3.6734E-40)
            r16 = r16 & r17
            r0 = r22
            int r0 = r0.f15838
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            r0.m19815((boolean) r14, (byte[]) r15, (int) r1, (int) r2)
            goto L_0x0041
        L_0x09b3:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int[] r0 = r0.f15836
            r16 = r0
            r17 = 7
            r16 = r16[r17]
            r17 = 262143(0x3ffff, float:3.6734E-40)
            r16 = r16 & r17
            r0 = r22
            r1 = r16
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r1)
            r0 = r22
            r0.f15838 = r14
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r16 = r14[r15]
            int r16 = r16 + 4
            r14[r15] = r16
            goto L_0x0041
        L_0x09e0:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x0a00:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r16 = 1
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r16
            byte r0 = (byte) r0
            r16 = r0
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            goto L_0x0041
        L_0x0a25:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            boolean r16 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r17 = r0
            r0 = r22
            r1 = r16
            r2 = r17
            int r16 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r22
            r1 = r16
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r1)
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r10, (int) r12)
            goto L_0x0041
        L_0x0a65:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            long r14 = (long) r14
            r16 = -1
            boolean r18 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r19 = r0
            r0 = r22
            r1 = r18
            r2 = r19
            int r18 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r18
            long r0 = (long) r0
            r18 = r0
            long r16 = r16 * r18
            long r14 = r14 & r16
            r16 = -1
            long r14 = r14 & r16
            r16 = -1
            long r14 = r14 & r16
            int r11 = (int) r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x0aab:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r7 = r0.m19810((boolean) r14, (byte[]) r15, (int) r10)
            if (r7 == 0) goto L_0x0041
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            int r11 = r14 / r7
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x0ada:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FC
            int r15 = r15.getFlag()
            r4 = r14 & r15
            long r14 = (long) r12
            r16 = -1
            boolean r18 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r19 = r0
            r0 = r22
            r1 = r18
            r2 = r19
            int r18 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r18
            long r0 = (long) r0
            r18 = r0
            long r16 = r16 + r18
            long r14 = r14 & r16
            r16 = -1
            long r0 = (long) r4
            r18 = r0
            long r16 = r16 + r18
            long r14 = r14 & r16
            r16 = -1
            long r14 = r14 & r16
            int r11 = (int) r14
            boolean r14 = r6.m19830()
            if (r14 == 0) goto L_0x0b2a
            r11 = r11 & 255(0xff, float:3.57E-43)
        L_0x0b2a:
            if (r11 < r12) goto L_0x0b30
            if (r11 != r12) goto L_0x0b44
            if (r4 == 0) goto L_0x0b44
        L_0x0b30:
            r14 = 1
        L_0x0b31:
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x0b44:
            if (r11 != 0) goto L_0x0b4f
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x0b4c:
            r14 = r14 | 0
            goto L_0x0b31
        L_0x0b4f:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x0b4c
        L_0x0b57:
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int r12 = r0.m19810((boolean) r14, (byte[]) r15, (int) r9)
            r0 = r22
            int r14 = r0.f15838
            junrar.unpack.vm.VMFlags r15 = junrar.unpack.vm.VMFlags.VM_FC
            int r15 = r15.getFlag()
            r4 = r14 & r15
            long r14 = (long) r12
            r16 = -1
            boolean r18 = r6.m19830()
            r0 = r22
            byte[] r0 = r0.f15839
            r19 = r0
            r0 = r22
            r1 = r18
            r2 = r19
            int r18 = r0.m19810((boolean) r1, (byte[]) r2, (int) r10)
            r0 = r18
            long r0 = (long) r0
            r18 = r0
            long r16 = r16 - r18
            long r14 = r14 & r16
            r16 = -1
            long r0 = (long) r4
            r18 = r0
            long r16 = r16 - r18
            long r14 = r14 & r16
            r16 = -1
            long r14 = r14 & r16
            int r11 = (int) r14
            boolean r14 = r6.m19830()
            if (r14 == 0) goto L_0x0ba7
            r11 = r11 & 255(0xff, float:3.57E-43)
        L_0x0ba7:
            if (r11 > r12) goto L_0x0bad
            if (r11 != r12) goto L_0x0bc1
            if (r4 == 0) goto L_0x0bc1
        L_0x0bad:
            r14 = 1
        L_0x0bae:
            r0 = r22
            r0.f15838 = r14
            boolean r14 = r6.m19830()
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            r0.m19815((boolean) r14, (byte[]) r15, (int) r9, (int) r11)
            goto L_0x0041
        L_0x0bc1:
            if (r11 != 0) goto L_0x0bcc
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FZ
            int r14 = r14.getFlag()
        L_0x0bc9:
            r14 = r14 | 0
            goto L_0x0bae
        L_0x0bcc:
            junrar.unpack.vm.VMFlags r14 = junrar.unpack.vm.VMFlags.VM_FS
            int r14 = r14.getFlag()
            r14 = r14 & r11
            goto L_0x0bc9
        L_0x0bd4:
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r14 = r14[r15]
            r15 = 262144(0x40000, float:3.67342E-40)
            if (r14 < r15) goto L_0x0be1
            r14 = 1
            return r14
        L_0x0be1:
            r14 = 0
            r0 = r22
            byte[] r15 = r0.f15839
            r0 = r22
            int[] r0 = r0.f15836
            r16 = r0
            r17 = 7
            r16 = r16[r17]
            r17 = 262143(0x3ffff, float:3.6734E-40)
            r16 = r16 & r17
            r0 = r22
            r1 = r16
            int r14 = r0.m19810((boolean) r14, (byte[]) r15, (int) r1)
            r0 = r22
            r0.m19816((int) r14)
            r0 = r22
            int[] r14 = r0.f15836
            r15 = 7
            r16 = r14[r15]
            int r16 = r16 + 4
            r14[r15] = r16
            goto L_0x0012
        L_0x0c0f:
            junrar.unpack.vm.VMPreparedOperand r14 = r6.m19825()
            int r14 = r14.m19831()
            junrar.unpack.vm.VMStandardFilters r14 = junrar.unpack.vm.VMStandardFilters.findFilter(r14)
            r0 = r22
            r0.m19814((junrar.unpack.vm.VMStandardFilters) r14)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: junrar.unpack.vm.RarVM.m19817(java.util.List, int):boolean");
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private boolean m19818(byte[] bArr) {
        return this.f15839 == bArr;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public byte[] m19819() {
        return this.f15839;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19820() {
        if (this.f15839 == null) {
            this.f15839 = new byte[262148];
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19821(int i, byte[] bArr, int i2, int i3) {
        if (i < 262144) {
            int i4 = 0;
            while (i4 < Math.min(bArr.length - i2, i3) && 262144 - i >= i4) {
                this.f15839[i + i4] = bArr[i2 + i4];
                i4++;
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19822(Vector<Byte> vector, int i, int i2) {
        vector.set(i + 0, Byte.valueOf((byte) (i2 & 255)));
        vector.set(i + 1, Byte.valueOf((byte) ((i2 >>> 8) & 255)));
        vector.set(i + 2, Byte.valueOf((byte) ((i2 >>> 16) & 255)));
        vector.set(i + 3, Byte.valueOf((byte) ((i2 >>> 24) & 255)));
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19823(VMPreparedProgram vMPreparedProgram) {
        for (int i = 0; i < vMPreparedProgram.m19840().length; i++) {
            this.f15836[i] = vMPreparedProgram.m19840()[i];
        }
        long min = (long) (Math.min(vMPreparedProgram.m19839().size(), 8192) & -1);
        if (min != 0) {
            for (int i2 = 0; ((long) i2) < min; i2++) {
                this.f15839[245760 + i2] = vMPreparedProgram.m19839().get(i2).byteValue();
            }
        }
        long min2 = Math.min((long) vMPreparedProgram.m19841().size(), PlaybackStateCompat.ACTION_PLAY_FROM_URI - min) & -1;
        if (min2 != 0) {
            for (int i3 = 0; ((long) i3) < min2; i3++) {
                this.f15839[245760 + ((int) min) + i3] = vMPreparedProgram.m19841().get(i3).byteValue();
            }
        }
        this.f15836[7] = 262144;
        this.f15838 = 0;
        List<VMPreparedCommand> r6 = vMPreparedProgram.m19848().size() != 0 ? vMPreparedProgram.m19848() : vMPreparedProgram.m19843();
        if (!m19817(r6, vMPreparedProgram.m19846())) {
            r6.get(0).m19828(VMCommands.VM_RET);
        }
        int r4 = m19810(false, this.f15839, 245792) & 262143;
        int r5 = m19810(false, this.f15839, 245788) & 262143;
        if (r4 + r5 >= 262144) {
            r4 = 0;
            r5 = 0;
        }
        vMPreparedProgram.m19844(r4);
        vMPreparedProgram.m19847(r5);
        vMPreparedProgram.m19839().clear();
        int min3 = Math.min(m19810(false, this.f15839, 245808), 8128);
        if (min3 != 0) {
            vMPreparedProgram.m19839().setSize(min3 + 64);
            for (int i4 = 0; i4 < min3 + 64; i4++) {
                vMPreparedProgram.m19839().set(i4, Byte.valueOf(this.f15839[245760 + i4]));
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19824(byte[] bArr, int i, VMPreparedProgram vMPreparedProgram) {
        int r8;
        m19801();
        int min = Math.min(32768, i);
        for (int i2 = 0; i2 < min; i2++) {
            byte[] bArr2 = this.f15833;
            bArr2[i2] = (byte) (bArr2[i2] | bArr[i2]);
        }
        byte b = 0;
        for (int i3 = 1; i3 < i; i3++) {
            b = (byte) (bArr[i3] ^ b);
        }
        m19803(8);
        vMPreparedProgram.m19849(0);
        if (b == bArr[0]) {
            VMStandardFilters r9 = m19811(bArr, i);
            if (r9 != VMStandardFilters.VMSF_NONE) {
                VMPreparedCommand vMPreparedCommand = new VMPreparedCommand();
                vMPreparedCommand.m19828(VMCommands.VM_STANDARD);
                vMPreparedCommand.m19825().m19832(r9.getFilter());
                vMPreparedCommand.m19825().m19838(VMOpType.VM_OPNONE);
                vMPreparedCommand.m19827().m19838(VMOpType.VM_OPNONE);
                i = 0;
                vMPreparedProgram.m19843().add(vMPreparedCommand);
                vMPreparedProgram.m19849(vMPreparedProgram.m19846() + 1);
            }
            int r5 = m19800();
            m19803(1);
            if ((32768 & r5) != 0) {
                long r6 = ((long) m19808((BitInput) this)) & 0;
                int i4 = 0;
                while (this.f15831 < i && ((long) i4) < r6) {
                    vMPreparedProgram.m19841().add(Byte.valueOf((byte) (m19800() >> 8)));
                    m19803(8);
                    i4++;
                }
            }
            while (this.f15831 < i) {
                VMPreparedCommand vMPreparedCommand2 = new VMPreparedCommand();
                int r4 = m19800();
                if ((32768 & r4) == 0) {
                    vMPreparedCommand2.m19828(VMCommands.findVMCommand(r4 >> 12));
                    m19803(4);
                } else {
                    vMPreparedCommand2.m19828(VMCommands.findVMCommand((r4 >> 10) - 24));
                    m19803(6);
                }
                if ((VMCmdFlags.f15842[vMPreparedCommand2.m19826().getVMCommand()] & 4) != 0) {
                    vMPreparedCommand2.m19829((m19800() >> 15) == 1);
                    m19803(1);
                } else {
                    vMPreparedCommand2.m19829(false);
                }
                vMPreparedCommand2.m19825().m19838(VMOpType.VM_OPNONE);
                vMPreparedCommand2.m19827().m19838(VMOpType.VM_OPNONE);
                byte b2 = VMCmdFlags.f15842[vMPreparedCommand2.m19826().getVMCommand()] & 3;
                if (b2 > 0) {
                    m19813(vMPreparedCommand2.m19825(), vMPreparedCommand2.m19830());
                    if (b2 == 2) {
                        m19813(vMPreparedCommand2.m19827(), vMPreparedCommand2.m19830());
                    } else if (vMPreparedCommand2.m19825().m19834() == VMOpType.VM_OPINT && (VMCmdFlags.f15842[vMPreparedCommand2.m19826().getVMCommand()] & 24) != 0) {
                        int r82 = vMPreparedCommand2.m19825().m19831();
                        if (r82 >= 256) {
                            r8 = r82 + InputDeviceCompat.SOURCE_ANY;
                        } else {
                            if (r82 >= 136) {
                                r82 -= 264;
                            } else if (r82 >= 16) {
                                r82 -= 8;
                            } else if (r82 >= 8) {
                                r82 -= 16;
                            }
                            r8 = r82 + vMPreparedProgram.m19846();
                        }
                        vMPreparedCommand2.m19825().m19832(r8);
                    }
                }
                vMPreparedProgram.m19849(vMPreparedProgram.m19846() + 1);
                vMPreparedProgram.m19843().add(vMPreparedCommand2);
            }
        }
        VMPreparedCommand vMPreparedCommand3 = new VMPreparedCommand();
        vMPreparedCommand3.m19828(VMCommands.VM_RET);
        vMPreparedCommand3.m19825().m19838(VMOpType.VM_OPNONE);
        vMPreparedCommand3.m19827().m19838(VMOpType.VM_OPNONE);
        vMPreparedProgram.m19843().add(vMPreparedCommand3);
        vMPreparedProgram.m19849(vMPreparedProgram.m19846() + 1);
        if (i != 0) {
            m19806(vMPreparedProgram);
        }
    }
}
