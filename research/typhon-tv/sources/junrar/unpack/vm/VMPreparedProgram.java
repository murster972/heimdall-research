package junrar.unpack.vm;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class VMPreparedProgram {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int[] f15854;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f15855;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f15856;

    /* renamed from: 连任  reason: contains not printable characters */
    private Vector<Byte> f15857;

    /* renamed from: 靐  reason: contains not printable characters */
    private List<VMPreparedCommand> f15858;

    /* renamed from: 麤  reason: contains not printable characters */
    private Vector<Byte> f15859;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f15860;

    /* renamed from: 龘  reason: contains not printable characters */
    private List<VMPreparedCommand> f15861;

    public VMPreparedProgram() {
        this.f15861 = new ArrayList();
        this.f15858 = new ArrayList();
        this.f15859 = new Vector<>();
        this.f15857 = new Vector<>();
        this.f15854 = new int[7];
        this.f15858 = null;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public Vector<Byte> m19839() {
        return this.f15859;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int[] m19840() {
        return this.f15854;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public Vector<Byte> m19841() {
        return this.f15857;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m19842() {
        return this.f15856;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public List<VMPreparedCommand> m19843() {
        return this.f15861;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19844(int i) {
        this.f15855 = i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m19845() {
        return this.f15855;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m19846() {
        return this.f15860;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19847(int i) {
        this.f15856 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public List<VMPreparedCommand> m19848() {
        return this.f15858;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19849(int i) {
        this.f15860 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19850(List<VMPreparedCommand> list) {
        this.f15858 = list;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19851(Vector<Byte> vector) {
        this.f15857 = vector;
    }
}
