package junrar.unpack.ppm;

import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;

public class SubAllocator {

    /* renamed from: 靐  reason: contains not printable characters */
    static final /* synthetic */ boolean f15810 = (!SubAllocator.class.desiredAssertionStatus());

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int f15811 = Math.max(PPMContext.f15776, 12);

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f15812;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f15813;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f15814;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int f15815;

    /* renamed from: ʿ  reason: contains not printable characters */
    private int f15816;

    /* renamed from: ˆ  reason: contains not printable characters */
    private RarMemBlock f15817 = null;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f15818;

    /* renamed from: ˉ  reason: contains not printable characters */
    private RarMemBlock f15819 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f15820;

    /* renamed from: ˋ  reason: contains not printable characters */
    private RarNode f15821 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private RarMemBlock f15822 = null;

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f15823;

    /* renamed from: ٴ  reason: contains not printable characters */
    private final RarNode[] f15824 = new RarNode[38];

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f15825;

    /* renamed from: 连任  reason: contains not printable characters */
    private int[] f15826 = new int[128];

    /* renamed from: 麤  reason: contains not printable characters */
    private int[] f15827 = new int[38];

    /* renamed from: 齉  reason: contains not printable characters */
    private int f15828;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private byte[] f15829;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f15830;

    public SubAllocator() {
        m19797();
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private int m19774(int i) {
        return f15811 * i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private int m19775(int i) {
        if (this.f15812 == 0) {
            this.f15812 = 255;
            m19777();
            if (this.f15824[i].m19741() != 0) {
                return m19778(i);
            }
        }
        int i2 = i;
        do {
            i2++;
            if (i2 == 38) {
                this.f15812--;
                int r0 = m19774(this.f15827[i]);
                int i3 = this.f15827[i] * 12;
                if (this.f15816 - this.f15825 <= i3) {
                    return 0;
                }
                this.f15816 -= i3;
                this.f15818 -= r0;
                return this.f15818;
            }
        } while (this.f15824[i2].m19741() == 0);
        int r2 = m19778(i2);
        m19779(r2, i2, i);
        return r2;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    private int m19776() {
        return this.f15824.length * 4;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    private void m19777() {
        RarMemBlock rarMemBlock = this.f15822;
        rarMemBlock.m19714(this.f15820);
        RarMemBlock rarMemBlock2 = this.f15817;
        RarMemBlock rarMemBlock3 = this.f15819;
        if (this.f15814 != this.f15823) {
            this.f15829[this.f15814] = 0;
        }
        rarMemBlock.m19737(rarMemBlock);
        rarMemBlock.m19733(rarMemBlock);
        for (int i = 0; i < 38; i++) {
            while (this.f15824[i].m19741() != 0) {
                rarMemBlock2.m19714(m19778(i));
                rarMemBlock2.m19740(rarMemBlock);
                rarMemBlock2.m19730(65535);
                rarMemBlock2.m19732(this.f15827[i]);
            }
        }
        rarMemBlock2.m19714(rarMemBlock.m19731());
        while (rarMemBlock2.m19713() != rarMemBlock.m19713()) {
            rarMemBlock3.m19714(m19780(rarMemBlock2.m19713(), rarMemBlock2.m19736()));
            while (rarMemBlock3.m19729() == 65535 && rarMemBlock2.m19736() + rarMemBlock3.m19736() < 65536) {
                rarMemBlock3.m19738();
                rarMemBlock2.m19732(rarMemBlock2.m19736() + rarMemBlock3.m19736());
                rarMemBlock3.m19714(m19780(rarMemBlock2.m19713(), rarMemBlock2.m19736()));
            }
            rarMemBlock2.m19714(rarMemBlock2.m19731());
        }
        rarMemBlock2.m19714(rarMemBlock.m19731());
        while (rarMemBlock2.m19713() != rarMemBlock.m19713()) {
            rarMemBlock2.m19738();
            int r5 = rarMemBlock2.m19736();
            while (r5 > 128) {
                m19781(rarMemBlock2.m19713(), 37);
                r5 -= 128;
                rarMemBlock2.m19714(m19780(rarMemBlock2.m19713(), 128));
            }
            int[] iArr = this.f15827;
            int i2 = this.f15826[r5 - 1];
            if (iArr[i2] != r5) {
                i2--;
                int i3 = r5 - this.f15827[i2];
                m19781(m19780(rarMemBlock2.m19713(), r5 - i3), i3 - 1);
            }
            m19781(rarMemBlock2.m19713(), i2);
            rarMemBlock2.m19714(rarMemBlock.m19731());
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private int m19778(int i) {
        int r0 = this.f15824[i].m19741();
        RarNode rarNode = this.f15821;
        rarNode.m19714(r0);
        this.f15824[i].m19742(rarNode.m19741());
        return r0;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private void m19779(int i, int i2, int i3) {
        int i4 = this.f15827[i2] - this.f15827[i3];
        int r1 = i + m19774(this.f15827[i3]);
        int[] iArr = this.f15827;
        int i5 = this.f15826[i4 - 1];
        if (iArr[i5] != i4) {
            int i6 = i5 - 1;
            m19781(r1, i6);
            int i7 = this.f15827[i6];
            r1 += m19774(i7);
            i4 -= i7;
        }
        m19781(r1, this.f15826[i4 - 1]);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    private int m19780(int i, int i2) {
        return m19774(i2) + i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private void m19781(int i, int i2) {
        RarNode rarNode = this.f15821;
        rarNode.m19714(i);
        rarNode.m19742(this.f15824[i2].m19741());
        this.f15824[i2].m19743(rarNode);
    }

    public String toString() {
        return "SubAllocator[" + "\n  subAllocatorSize=" + this.f15828 + "\n  glueCount=" + this.f15812 + "\n  heapStart=" + this.f15813 + "\n  loUnit=" + this.f15814 + "\n  hiUnit=" + this.f15823 + "\n  pText=" + this.f15825 + "\n  unitsStart=" + this.f15818 + "\n]";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m19782() {
        return this.f15816;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int m19783() {
        return this.f15815;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m19784() {
        return this.f15825;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public void m19785() {
        Arrays.fill(this.f15829, this.f15830, this.f15830 + m19776(), (byte) 0);
        this.f15825 = this.f15813;
        int i = ((this.f15828 / 8) / 12) * 7 * 12;
        int i2 = (i / 12) * f15811;
        int i3 = this.f15828 - i;
        int i4 = ((i3 / 12) * f15811) + (i3 % 12);
        this.f15823 = this.f15813 + this.f15828;
        int i5 = this.f15813 + i4;
        this.f15818 = i5;
        this.f15814 = i5;
        this.f15816 = this.f15813 + i3;
        this.f15823 = this.f15814 + i2;
        int i6 = 0;
        int i7 = 1;
        while (i6 < 4) {
            this.f15827[i6] = i7 & 255;
            i6++;
            i7++;
        }
        int i8 = i7 + 1;
        while (i6 < 8) {
            this.f15827[i6] = i8 & 255;
            i6++;
            i8 += 2;
        }
        int i9 = i8 + 1;
        while (i6 < 12) {
            this.f15827[i6] = i9 & 255;
            i6++;
            i9 += 3;
        }
        int i10 = i9 + 1;
        while (i6 < 38) {
            this.f15827[i6] = i10 & 255;
            i6++;
            i10 += 4;
        }
        this.f15812 = 0;
        int i11 = 0;
        for (int i12 = 0; i12 < 128; i12++) {
            i11 += this.f15827[i11] < i12 + 1 ? 1 : 0;
            this.f15826[i12] = i11 & 255;
        }
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public byte[] m19786() {
        return this.f15829;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m19787() {
        if (this.f15823 == this.f15814) {
            return this.f15824[0].m19741() != 0 ? m19778(0) : m19775(0);
        }
        int i = this.f15823 - f15811;
        this.f15823 = i;
        return i;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m19788(int i) {
        int i2 = this.f15826[i - 1];
        if (this.f15824[i2].m19741() != 0) {
            return m19778(i2);
        }
        int i3 = this.f15814;
        this.f15814 += m19774(this.f15827[i2]);
        if (this.f15814 <= this.f15823) {
            return i3;
        }
        this.f15814 -= m19774(this.f15827[i2]);
        return m19775(i2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19789() {
        this.f15825++;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19790(int i, int i2) {
        m19781(i, this.f15826[i2 - 1]);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m19791() {
        return this.f15828;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19792(int i) {
        m19794(m19784() - i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19793() {
        if (this.f15828 != 0) {
            this.f15828 = 0;
            this.f15829 = null;
            this.f15813 = 1;
            this.f15821 = null;
            this.f15822 = null;
            this.f15817 = null;
            this.f15819 = null;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19794(int i) {
        this.f15825 = i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19795(int i, int i2) {
        int i3 = this.f15826[i2 - 1];
        if (i3 == this.f15826[(i2 - 1) + 1]) {
            return i;
        }
        int r2 = m19788(i2 + 1);
        if (r2 != 0) {
            System.arraycopy(this.f15829, i, this.f15829, r2, m19774(i2));
            m19781(i, i3);
        }
        return r2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19796(int i, int i2, int i3) {
        int i4 = this.f15826[i2 - 1];
        int i5 = this.f15826[i3 - 1];
        if (i4 == i5) {
            return i;
        }
        if (this.f15824[i5].m19741() != 0) {
            int r2 = m19778(i5);
            System.arraycopy(this.f15829, i, this.f15829, r2, m19774(i3));
            m19781(i, i4);
            return r2;
        }
        m19779(i, i4, i5);
        return i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19797() {
        this.f15828 = 0;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19798(int i) {
        int i2 = i << 20;
        if (this.f15828 != i2) {
            m19793();
            int i3 = ((i2 / 12) * f15811) + f15811;
            int i4 = i3 + 1 + 152;
            this.f15820 = i4;
            int i5 = i4 + 12;
            this.f15829 = new byte[i5];
            this.f15813 = 1;
            this.f15815 = (this.f15813 + i3) - f15811;
            this.f15828 = i2;
            this.f15830 = this.f15813 + i3;
            if (f15810 || i5 - this.f15820 == 12) {
                int i6 = 0;
                int i7 = this.f15830;
                while (i6 < this.f15824.length) {
                    this.f15824[i6] = new RarNode(this.f15829);
                    this.f15824[i6].m19714(i7);
                    i6++;
                    i7 += 4;
                }
                this.f15821 = new RarNode(this.f15829);
                this.f15822 = new RarMemBlock(this.f15829);
                this.f15817 = new RarMemBlock(this.f15829);
                this.f15819 = new RarMemBlock(this.f15829);
            } else {
                throw new AssertionError(i5 + StringUtils.SPACE + this.f15820 + StringUtils.SPACE + 12);
            }
        }
        return true;
    }
}
