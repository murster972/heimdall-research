package junrar.unpack.ppm;

public abstract class Pointer {

    /* renamed from: 连任  reason: contains not printable characters */
    static final /* synthetic */ boolean f15788 = (!Pointer.class.desiredAssertionStatus());

    /* renamed from: 麤  reason: contains not printable characters */
    protected int f15789;

    /* renamed from: 齉  reason: contains not printable characters */
    protected byte[] f15790;

    public Pointer(byte[] bArr) {
        this.f15790 = bArr;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m19713() {
        if (f15788 || this.f15790 != null) {
            return this.f15789;
        }
        throw new AssertionError();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19714(int i) {
        if (!f15788 && this.f15790 == null) {
            throw new AssertionError();
        } else if (f15788 || (i >= 0 && i < this.f15790.length)) {
            this.f15789 = i;
        } else {
            throw new AssertionError(i);
        }
    }
}
