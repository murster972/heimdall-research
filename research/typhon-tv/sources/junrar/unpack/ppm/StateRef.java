package junrar.unpack.ppm;

public class StateRef {

    /* renamed from: 靐  reason: contains not printable characters */
    private int f15807;

    /* renamed from: 齉  reason: contains not printable characters */
    private int f15808;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f15809;

    public String toString() {
        return "State[" + "\n  symbol=" + m19770() + "\n  freq=" + m19765() + "\n  successor=" + m19768() + "\n]";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m19765() {
        return this.f15807;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19766(int i) {
        this.f15807 = i & 255;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19767(int i) {
        this.f15808 = i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m19768() {
        return this.f15808;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19769(int i) {
        this.f15807 = (this.f15807 - i) & 255;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19770() {
        return this.f15809;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19771(int i) {
        this.f15809 = i & 255;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19772(PPMContext pPMContext) {
        m19767(pPMContext.m19713());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19773(State state) {
        m19766(state.m19754());
        m19767(state.m19758());
        m19771(state.m19759());
    }
}
