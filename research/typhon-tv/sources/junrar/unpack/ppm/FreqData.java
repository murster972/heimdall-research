package junrar.unpack.ppm;

import junrar.io.Raw;

public class FreqData extends Pointer {
    public FreqData(byte[] bArr) {
        super(bArr);
    }

    public void a_(int i) {
        Raw.m19491(this.f15790, this.f15789 + 2, i);
    }

    public String toString() {
        return "FreqData[" + "\n  pos=" + this.f15789 + "\n  size=" + 6 + "\n  summFreq=" + m19654() + "\n  stats=" + m19652() + "\n]";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m19652() {
        return Raw.m19490(this.f15790, this.f15789 + 2);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19653(int i) {
        Raw.m19488(this.f15790, this.f15789, i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19654() {
        return Raw.m19487(this.f15790, this.f15789) & 65535;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public FreqData m19655(byte[] bArr) {
        this.f15790 = bArr;
        this.f15789 = 0;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19656(int i) {
        Raw.m19494(this.f15790, this.f15789, (short) i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19657(State state) {
        a_(state.m19713());
    }
}
