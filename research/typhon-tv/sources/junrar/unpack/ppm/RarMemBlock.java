package junrar.unpack.ppm;

import junrar.io.Raw;

public class RarMemBlock extends Pointer {

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f15799;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f15800;

    /* renamed from: 靐  reason: contains not printable characters */
    private int f15801;

    /* renamed from: 龘  reason: contains not printable characters */
    private int f15802;

    public RarMemBlock(byte[] bArr) {
        super(bArr);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m19729() {
        if (this.f15790 != null) {
            this.f15802 = Raw.m19487(this.f15790, this.f15789) & 65535;
        }
        return this.f15802;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m19730(int i) {
        this.f15802 = i;
        if (this.f15790 != null) {
            Raw.m19494(this.f15790, this.f15789, (short) i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m19731() {
        if (this.f15790 != null) {
            this.f15799 = Raw.m19490(this.f15790, this.f15789 + 4);
        }
        return this.f15799;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19732(int i) {
        this.f15801 = 65535 & i;
        if (this.f15790 != null) {
            Raw.m19494(this.f15790, this.f15789 + 2, (short) i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19733(RarMemBlock rarMemBlock) {
        m19739(rarMemBlock.m19713());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m19734() {
        if (this.f15790 != null) {
            this.f15800 = Raw.m19490(this.f15790, this.f15789 + 8);
        }
        return this.f15800;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19735(int i) {
        this.f15800 = i;
        if (this.f15790 != null) {
            Raw.m19491(this.f15790, this.f15789 + 8, i);
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m19736() {
        if (this.f15790 != null) {
            this.f15801 = Raw.m19487(this.f15790, this.f15789 + 2) & 65535;
        }
        return this.f15801;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19737(RarMemBlock rarMemBlock) {
        m19735(rarMemBlock.m19713());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19738() {
        RarMemBlock rarMemBlock = new RarMemBlock(this.f15790);
        rarMemBlock.m19714(m19734());
        rarMemBlock.m19739(m19731());
        rarMemBlock.m19714(m19731());
        rarMemBlock.m19735(m19734());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19739(int i) {
        this.f15799 = i;
        if (this.f15790 != null) {
            Raw.m19491(this.f15790, this.f15789 + 4, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19740(RarMemBlock rarMemBlock) {
        RarMemBlock rarMemBlock2 = new RarMemBlock(this.f15790);
        m19735(rarMemBlock.m19713());
        rarMemBlock2.m19714(m19734());
        m19739(rarMemBlock2.m19731());
        rarMemBlock2.m19733(this);
        rarMemBlock2.m19714(m19731());
        rarMemBlock2.m19737(this);
    }
}
