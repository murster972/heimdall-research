package junrar.unpack.ppm;

import junrar.io.Raw;

public class PPMContext extends Pointer {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static final int f15774 = Math.max(6, 6);

    /* renamed from: 靐  reason: contains not printable characters */
    public static final int[] f15775 = {25, 14, 9, 7, 5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 2, 2};

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int f15776 = ((f15774 + 2) + 4);

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f15777;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final FreqData f15778;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final State f15779 = new State((byte[]) null);

    /* renamed from: ʿ  reason: contains not printable characters */
    private final State f15780 = new State((byte[]) null);

    /* renamed from: ˈ  reason: contains not printable characters */
    private final State f15781 = new State((byte[]) null);

    /* renamed from: ˊ  reason: contains not printable characters */
    private final int[] f15782 = new int[256];

    /* renamed from: ˑ  reason: contains not printable characters */
    private final State f15783;

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f15784;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private final State f15785 = new State((byte[]) null);

    /* renamed from: ﹶ  reason: contains not printable characters */
    private final State f15786 = new State((byte[]) null);

    /* renamed from: ﾞ  reason: contains not printable characters */
    private PPMContext f15787 = null;

    public PPMContext(byte[] bArr) {
        super(bArr);
        this.f15783 = new State(bArr);
        this.f15778 = new FreqData(bArr);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    private PPMContext m19692(byte[] bArr) {
        if (this.f15787 == null) {
            this.f15787 = new PPMContext((byte[]) null);
        }
        return this.f15787.m19707(bArr);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    private SEE2Context m19693(ModelPPM modelPPM, int i) {
        int i2 = 1;
        int r2 = m19695();
        if (r2 != 256) {
            PPMContext r4 = m19692(modelPPM.m19674());
            r4.m19702(m19699());
            int i3 = modelPPM.m19691()[i - 1];
            int i4 = 0 + (i < r4.m19695() - r2 ? 1 : 0) + ((this.f15778.m19654() < r2 * 11 ? 1 : 0) * 2);
            if (modelPPM.m19668() <= i) {
                i2 = 0;
            }
            SEE2Context sEE2Context = modelPPM.m19686()[i3][i4 + (i2 * 4) + modelPPM.m19678()];
            modelPPM.m19670().m19720().m19725((long) sEE2Context.m19749());
            return sEE2Context;
        }
        SEE2Context r3 = modelPPM.m19683();
        modelPPM.m19670().m19720().m19725(1);
        return r3;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    private int m19694(ModelPPM modelPPM, State state) {
        PPMContext r1 = m19692(modelPPM.m19687().m19786());
        r1.m19702(m19699());
        return 0 + modelPPM.m19677() + modelPPM.m19690()[r1.m19695() - 1] + modelPPM.m19678() + (modelPPM.m19671()[state.m19759()] * 2) + ((modelPPM.m19676() >>> 26) & 32);
    }

    public String toString() {
        return "PPMContext[" + "\n  pos=" + this.f15789 + "\n  size=" + f15776 + "\n  numStats=" + m19695() + "\n  Suffix=" + m19699() + "\n  freqData=" + this.f15778 + "\n  oneState=" + this.f15783 + "\n]";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public final int m19695() {
        if (this.f15790 != null) {
            this.f15777 = Raw.m19487(this.f15790, this.f15789) & 65535;
        }
        return this.f15777;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19696(int i) {
        this.f15784 = i;
        if (this.f15790 != null) {
            Raw.m19491(this.f15790, this.f15789 + 8, i);
        }
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19697(ModelPPM modelPPM) {
        int i = 0;
        State r3 = this.f15785.m19760(modelPPM.m19674());
        r3.m19714(this.f15783.m19713());
        modelPPM.m19669(modelPPM.m19671()[modelPPM.m19673().m19759()]);
        int r1 = r3.m19754() - 1;
        int r2 = m19694(modelPPM, r3);
        int i2 = modelPPM.m19672()[r1][r2];
        if (modelPPM.m19670().m19719(14) < ((long) i2)) {
            modelPPM.m19673().m19714(r3.m19713());
            if (r3.m19754() < 128) {
                i = 1;
            }
            r3.m19757(i);
            modelPPM.m19670().m19720().m19723(0);
            modelPPM.m19670().m19720().m19728((long) i2);
            modelPPM.m19672()[r1][r2] = ((i2 + 128) - m19704(i2, 7, 2)) & 65535;
            modelPPM.m19684(1);
            modelPPM.m19666(1);
            return;
        }
        modelPPM.m19670().m19720().m19723((long) i2);
        int r0 = (i2 - m19704(i2, 7, 2)) & 65535;
        modelPPM.m19672()[r1][r2] = r0;
        modelPPM.m19670().m19720().m19728(16384);
        modelPPM.m19680(f15775[r0 >>> 10]);
        modelPPM.m19685(1);
        modelPPM.m19667()[r3.m19759()] = modelPPM.m19664();
        modelPPM.m19684(0);
        modelPPM.m19673().m19714(0);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19698(ModelPPM modelPPM, int i) {
        State r0 = this.f15786.m19760(modelPPM.m19674());
        r0.m19714(i);
        modelPPM.m19673().m19714(i);
        modelPPM.m19673().m19757(4);
        this.f15778.m19653(4);
        if (r0.m19754() > 124) {
            m19709(modelPPM);
        }
        modelPPM.m19682(1);
        modelPPM.m19665(modelPPM.m19679());
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public int m19699() {
        if (this.f15790 != null) {
            this.f15784 = Raw.m19490(this.f15790, this.f15789 + 8);
        }
        return this.f15784;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public boolean m19700(ModelPPM modelPPM) {
        int i = 0;
        RangeCoder r1 = modelPPM.m19670();
        r1.m19720().m19725((long) this.f15778.m19654());
        State state = new State(modelPPM.m19674());
        state.m19714(this.f15778.m19652());
        long r2 = (long) r1.m19716();
        if (r2 >= r1.m19720().m19724()) {
            return false;
        }
        int r0 = state.m19754();
        if (r2 < ((long) r0)) {
            r1.m19720().m19728((long) r0);
            if (((long) (r0 * 2)) > r1.m19720().m19724()) {
                i = 1;
            }
            modelPPM.m19684(i);
            modelPPM.m19666(modelPPM.m19677());
            int i2 = r0 + 4;
            modelPPM.m19673().m19714(state.m19713());
            modelPPM.m19673().m19755(i2);
            this.f15778.m19653(4);
            if (i2 > 124) {
                m19709(modelPPM);
            }
            r1.m19720().m19723(0);
            return true;
        } else if (modelPPM.m19673().m19713() == 0) {
            return false;
        } else {
            modelPPM.m19684(0);
            int r5 = m19695();
            int i3 = r5 - 1;
            do {
                r0 += state.m19752().m19754();
                if (((long) r0) <= r2) {
                    i3--;
                } else {
                    r1.m19720().m19723((long) (r0 - state.m19754()));
                    r1.m19720().m19728((long) r0);
                    m19710(modelPPM, state.m19713());
                    return true;
                }
            } while (i3 != 0);
            modelPPM.m19669(modelPPM.m19671()[modelPPM.m19673().m19759()]);
            r1.m19720().m19723((long) r0);
            modelPPM.m19667()[state.m19759()] = modelPPM.m19664();
            modelPPM.m19685(r5);
            int i4 = r5 - 1;
            modelPPM.m19673().m19714(0);
            do {
                modelPPM.m19667()[state.m19756().m19759()] = modelPPM.m19664();
                i4--;
            } while (i4 != 0);
            r1.m19720().m19728(r1.m19720().m19724());
            return true;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public State m19701() {
        return this.f15783;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19702(int i) {
        super.m19714(i);
        this.f15783.m19714(i + 2);
        this.f15778.m19714(i + 2);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public boolean m19703(ModelPPM modelPPM) {
        int i;
        int r4 = m19695() - modelPPM.m19668();
        SEE2Context r8 = m19693(modelPPM, r4);
        RangeCoder r0 = modelPPM.m19670();
        State r5 = this.f15785.m19760(modelPPM.m19674());
        State r9 = this.f15781.m19760(modelPPM.m19674());
        r5.m19714(this.f15778.m19652() - 6);
        int i2 = 0;
        int i3 = 0;
        while (true) {
            r5.m19752();
            if (modelPPM.m19667()[r5.m19759()] != modelPPM.m19664()) {
                i3 += r5.m19754();
                i = i2 + 1;
                this.f15782[i2] = r5.m19713();
                r4--;
                if (r4 == 0) {
                    break;
                }
                i2 = i;
            }
        }
        r0.m19720().m19727(i3);
        long r2 = (long) r0.m19716();
        if (r2 >= r0.m19720().m19724()) {
            int i4 = i;
            return false;
        }
        int i5 = 0;
        r5.m19714(this.f15782[0]);
        if (r2 < ((long) i3)) {
            int i6 = 0;
            while (true) {
                i6 += r5.m19754();
                if (((long) i6) > r2) {
                    break;
                }
                i5++;
                r5.m19714(this.f15782[i5]);
            }
            r0.m19720().m19728((long) i6);
            r0.m19720().m19723((long) (i6 - r5.m19754()));
            r8.m19744();
            m19698(modelPPM, r5.m19713());
        } else {
            r0.m19720().m19723((long) i3);
            r0.m19720().m19728(r0.m19720().m19724());
            int r42 = m19695() - modelPPM.m19668();
            int i7 = 0 - 1;
            do {
                i7++;
                r9.m19714(this.f15782[i7]);
                modelPPM.m19667()[r9.m19759()] = modelPPM.m19664();
                r42--;
            } while (r42 != 0);
            r8.m19746((int) r0.m19720().m19724());
            modelPPM.m19685(m19695());
        }
        return true;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19704(int i, int i2, int i3) {
        return ((1 << (i2 - i3)) + i) >>> i2;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19705(ModelPPM modelPPM, State state, StateRef stateRef) {
        PPMContext r0 = m19692(modelPPM.m19687().m19786());
        r0.m19702(modelPPM.m19687().m19787());
        if (r0 != null) {
            r0.m19708(1);
            r0.m19712(stateRef);
            r0.m19711(this);
            state.m19762(r0);
        }
        return r0.m19713();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public FreqData m19706() {
        return this.f15778;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public PPMContext m19707(byte[] bArr) {
        this.f15790 = bArr;
        this.f15789 = 0;
        this.f15783.m19760(bArr);
        this.f15778.m19655(bArr);
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public final void m19708(int i) {
        this.f15777 = 65535 & i;
        if (this.f15790 != null) {
            Raw.m19494(this.f15790, this.f15789, (short) i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19709(ModelPPM modelPPM) {
        int r2 = m19695();
        int r3 = m19695() - 1;
        State state = new State(modelPPM.m19674());
        State state2 = new State(modelPPM.m19674());
        State state3 = new State(modelPPM.m19674());
        state2.m19714(modelPPM.m19673().m19713());
        while (state2.m19713() != this.f15778.m19652()) {
            state3.m19714(state2.m19713() - 6);
            State.m19751(state2, state3);
            state2.m19756();
        }
        state3.m19714(this.f15778.m19652());
        state3.m19757(4);
        this.f15778.m19653(4);
        int r1 = this.f15778.m19654() - state2.m19754();
        int i = modelPPM.m19675() != 0 ? 1 : 0;
        state2.m19755((state2.m19754() + i) >>> 1);
        this.f15778.m19656(state2.m19754());
        do {
            state2.m19752();
            r1 -= state2.m19754();
            state2.m19755((state2.m19754() + i) >>> 1);
            this.f15778.m19653(state2.m19754());
            state3.m19714(state2.m19713() - 6);
            if (state2.m19754() > state3.m19754()) {
                state.m19714(state2.m19713());
                StateRef stateRef = new StateRef();
                stateRef.m19773(state);
                State state4 = new State(modelPPM.m19674());
                State state5 = new State(modelPPM.m19674());
                do {
                    state4.m19714(state.m19713() - 6);
                    state.m19763(state4);
                    state.m19756();
                    state5.m19714(state.m19713() - 6);
                    if (state.m19713() == this.f15778.m19652() || stateRef.m19765() <= state5.m19754()) {
                        state.m19764(stateRef);
                    }
                    state4.m19714(state.m19713() - 6);
                    state.m19763(state4);
                    state.m19756();
                    state5.m19714(state.m19713() - 6);
                    break;
                } while (stateRef.m19765() <= state5.m19754());
                state.m19764(stateRef);
            }
            r3--;
        } while (r3 != 0);
        if (state2.m19754() == 0) {
            do {
                r3++;
                state2.m19756();
            } while (state2.m19754() == 0);
            r1 += r3;
            m19708(m19695() - r3);
            if (m19695() == 1) {
                StateRef stateRef2 = new StateRef();
                state3.m19714(this.f15778.m19652());
                stateRef2.m19773(state3);
                do {
                    stateRef2.m19769(stateRef2.m19765() >>> 1);
                    r1 >>>= 1;
                } while (r1 > 1);
                modelPPM.m19687().m19790(this.f15778.m19652(), (r2 + 1) >>> 1);
                this.f15783.m19764(stateRef2);
                modelPPM.m19673().m19714(this.f15783.m19713());
                return;
            }
        }
        this.f15778.m19653(r1 - (r1 >>> 1));
        int i2 = (r2 + 1) >>> 1;
        int r5 = (m19695() + 1) >>> 1;
        if (i2 != r5) {
            this.f15778.a_(modelPPM.m19687().m19796(this.f15778.m19652(), i2, r5));
        }
        modelPPM.m19673().m19714(this.f15778.m19652());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19710(ModelPPM modelPPM, int i) {
        modelPPM.m19673().m19714(i);
        modelPPM.m19673().m19757(4);
        this.f15778.m19653(4);
        State r0 = this.f15779.m19760(modelPPM.m19674());
        State r1 = this.f15780.m19760(modelPPM.m19674());
        r0.m19714(i);
        r1.m19714(i - 6);
        if (r0.m19754() > r1.m19754()) {
            State.m19751(r0, r1);
            modelPPM.m19673().m19714(r1.m19713());
            if (r1.m19754() > 124) {
                m19709(modelPPM);
            }
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19711(PPMContext pPMContext) {
        m19696(pPMContext.m19713());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19712(StateRef stateRef) {
        this.f15783.m19764(stateRef);
    }
}
