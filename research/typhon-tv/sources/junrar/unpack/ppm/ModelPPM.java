package junrar.unpack.ppm;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import junrar.exception.RarException;
import junrar.unpack.Unpack;

public class ModelPPM {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static int[] f15741 = {15581, 7999, 22975, 18675, 25761, 23228, 26162, 24657};

    /* renamed from: ʻ  reason: contains not printable characters */
    private int f15742;

    /* renamed from: ʼ  reason: contains not printable characters */
    private int f15743;

    /* renamed from: ʽ  reason: contains not printable characters */
    private int f15744;

    /* renamed from: ʾ  reason: contains not printable characters */
    private int[] f15745 = new int[256];

    /* renamed from: ʿ  reason: contains not printable characters */
    private int[] f15746 = new int[256];

    /* renamed from: ˆ  reason: contains not printable characters */
    private RangeCoder f15747 = new RangeCoder();

    /* renamed from: ˈ  reason: contains not printable characters */
    private int[] f15748 = new int[256];

    /* renamed from: ˉ  reason: contains not printable characters */
    private SubAllocator f15749 = new SubAllocator();

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f15750;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f15751;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int[][] f15752 = ((int[][]) Array.newInstance(Integer.TYPE, new int[]{128, 64}));

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f15753;

    /* renamed from: י  reason: contains not printable characters */
    private final State f15754 = new State((byte[]) null);

    /* renamed from: ـ  reason: contains not printable characters */
    private final State f15755 = new State((byte[]) null);

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f15756;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private int f15757;

    /* renamed from: ᐧᐧ  reason: contains not printable characters */
    private final PPMContext f15758 = new PPMContext((byte[]) null);

    /* renamed from: ᴵ  reason: contains not printable characters */
    private final State f15759 = new State((byte[]) null);

    /* renamed from: ᴵᴵ  reason: contains not printable characters */
    private final int[] f15760 = new int[64];

    /* renamed from: ᵎ  reason: contains not printable characters */
    private final State f15761 = new State((byte[]) null);

    /* renamed from: ᵔ  reason: contains not printable characters */
    private final StateRef f15762 = new StateRef();

    /* renamed from: ᵢ  reason: contains not printable characters */
    private final StateRef f15763 = new StateRef();

    /* renamed from: ⁱ  reason: contains not printable characters */
    private final PPMContext f15764 = new PPMContext((byte[]) null);

    /* renamed from: 连任  reason: contains not printable characters */
    private State f15765;

    /* renamed from: 靐  reason: contains not printable characters */
    private SEE2Context f15766;

    /* renamed from: 麤  reason: contains not printable characters */
    private PPMContext f15767 = null;

    /* renamed from: 齉  reason: contains not printable characters */
    private PPMContext f15768 = null;

    /* renamed from: 龘  reason: contains not printable characters */
    private SEE2Context[][] f15769 = ((SEE2Context[][]) Array.newInstance(SEE2Context.class, new int[]{25, 16}));

    /* renamed from: ﹳ  reason: contains not printable characters */
    private final PPMContext f15770 = new PPMContext((byte[]) null);

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int[] f15771 = new int[256];

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f15772;

    /* renamed from: ﾞﾞ  reason: contains not printable characters */
    private final PPMContext f15773 = new PPMContext((byte[]) null);

    /* renamed from: ˆ  reason: contains not printable characters */
    private void m19658() {
        int i = 12;
        Arrays.fill(this.f15748, 0);
        this.f15749.m19785();
        if (this.f15753 < 12) {
            i = this.f15753;
        }
        this.f15757 = (-i) - 1;
        int r0 = this.f15749.m19787();
        this.f15768.m19702(r0);
        this.f15767.m19702(r0);
        this.f15768.m19696(0);
        this.f15744 = this.f15753;
        this.f15768.m19708(256);
        this.f15768.m19706().m19656(this.f15768.m19695() + 1);
        int r02 = this.f15749.m19788(128);
        this.f15765.m19714(r02);
        this.f15768.m19706().a_(r02);
        State state = new State(this.f15749.m19786());
        int r03 = this.f15768.m19706().m19652();
        this.f15756 = this.f15757;
        this.f15750 = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            state.m19714((i2 * 6) + r03);
            state.m19761(i2);
            state.m19755(1);
            state.m19753(0);
        }
        for (int i3 = 0; i3 < 128; i3++) {
            for (int i4 = 0; i4 < 8; i4++) {
                for (int i5 = 0; i5 < 64; i5 += 8) {
                    this.f15752[i3][i4 + i5] = 16384 - (f15741[i4] / (i3 + 2));
                }
            }
        }
        for (int i6 = 0; i6 < 25; i6++) {
            for (int i7 = 0; i7 < 16; i7++) {
                this.f15769[i6][i7].m19750((i6 * 5) + 10);
            }
        }
    }

    /* renamed from: ˉ  reason: contains not printable characters */
    private void m19659() {
        this.f15772 = 1;
        Arrays.fill(this.f15748, 0);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m19660() {
        m19658();
        this.f15772 = 0;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    private void m19661(int i) {
        this.f15772 = 1;
        this.f15753 = i;
        m19658();
        this.f15746[0] = 0;
        this.f15746[1] = 2;
        for (int i2 = 0; i2 < 9; i2++) {
            this.f15746[i2 + 2] = 4;
        }
        for (int i3 = 0; i3 < 245; i3++) {
            this.f15746[i3 + 11] = 6;
        }
        int i4 = 0;
        while (i4 < 3) {
            this.f15745[i4] = i4;
            i4++;
        }
        int i5 = i4;
        int i6 = 1;
        int i7 = 1;
        while (i4 < 256) {
            this.f15745[i4] = i5;
            i6--;
            if (i6 == 0) {
                i7++;
                i6 = i7;
                i5++;
            }
            i4++;
        }
        for (int i8 = 0; i8 < 64; i8++) {
            this.f15771[i8] = 0;
        }
        for (int i9 = 0; i9 < 192; i9++) {
            this.f15771[i9 + 64] = 8;
        }
        this.f15766.m19745(7);
    }

    /* renamed from: י  reason: contains not printable characters */
    private void m19662() {
        int i;
        StateRef stateRef = this.f15762;
        stateRef.m19773(this.f15765);
        State r6 = this.f15759.m19760(m19674());
        State r12 = this.f15761.m19760(m19674());
        PPMContext r7 = this.f15773.m19707(m19674());
        PPMContext r10 = this.f15758.m19707(m19674());
        r7.m19702(this.f15768.m19699());
        if (stateRef.m19765() < 31 && r7.m19713() != 0) {
            if (r7.m19695() != 1) {
                r6.m19714(r7.m19706().m19652());
                if (r6.m19759() != stateRef.m19770()) {
                    do {
                        r6.m19752();
                    } while (r6.m19759() != stateRef.m19770());
                    r12.m19714(r6.m19713() - 6);
                    if (r6.m19754() >= r12.m19754()) {
                        State.m19751(r6, r12);
                        r6.m19756();
                    }
                }
                if (r6.m19754() < 115) {
                    r6.m19757(2);
                    r7.m19706().m19653(2);
                }
            } else {
                r6.m19714(r7.m19701().m19713());
                if (r6.m19754() < 32) {
                    r6.m19757(1);
                }
            }
        }
        if (this.f15744 == 0) {
            this.f15765.m19753(m19663(true, r6));
            this.f15768.m19702(this.f15765.m19758());
            this.f15767.m19702(this.f15765.m19758());
            if (this.f15768.m19713() == 0) {
                m19660();
                return;
            }
            return;
        }
        this.f15749.m19786()[this.f15749.m19784()] = (byte) stateRef.m19770();
        this.f15749.m19789();
        r10.m19702(this.f15749.m19784());
        if (this.f15749.m19784() >= this.f15749.m19782()) {
            m19660();
            return;
        }
        if (stateRef.m19768() != 0) {
            if (stateRef.m19768() <= this.f15749.m19784()) {
                stateRef.m19767(m19663(false, r6));
                if (stateRef.m19768() == 0) {
                    m19660();
                    return;
                }
            }
            int i2 = this.f15744 - 1;
            this.f15744 = i2;
            if (i2 == 0) {
                r10.m19702(stateRef.m19768());
                if (this.f15767.m19713() != this.f15768.m19713()) {
                    this.f15749.m19792(1);
                }
            }
        } else {
            this.f15765.m19753(r10.m19713());
            stateRef.m19772(this.f15768);
        }
        int r4 = this.f15768.m19695();
        int r8 = (this.f15768.m19706().m19654() - r4) - (stateRef.m19765() - 1);
        r7.m19702(this.f15767.m19713());
        while (r7.m19713() != this.f15768.m19713()) {
            int r5 = r7.m19695();
            if (r5 != 1) {
                if ((r5 & 1) == 0) {
                    r7.m19706().a_(this.f15749.m19795(r7.m19706().m19652(), r5 >>> 1));
                    if (r7.m19706().m19652() == 0) {
                        m19660();
                        return;
                    }
                }
                r7.m19706().m19653((r5 * 2 < r4 ? 1 : 0) + (((r5 * 4 <= r4 ? 1 : 0) & (r7.m19706().m19654() <= r5 * 8 ? 1 : 0)) * 2));
            } else {
                r6.m19714(this.f15749.m19788(1));
                if (r6.m19713() == 0) {
                    m19660();
                    return;
                }
                r6.m19763(r7.m19701());
                r7.m19706().m19657(r6);
                if (r6.m19754() < 30) {
                    r6.m19757(r6.m19754());
                } else {
                    r6.m19755(120);
                }
                r7.m19706().m19656((r4 > 3 ? 1 : 0) + this.f15743 + r6.m19754());
            }
            int r2 = stateRef.m19765() * 2 * (r7.m19706().m19654() + 6);
            int r9 = r8 + r7.m19706().m19654();
            if (r2 < r9 * 6) {
                i = (r2 > r9 ? 1 : 0) + 1 + (r2 >= r9 * 4 ? 1 : 0);
                r7.m19706().m19653(3);
            } else {
                i = (r2 >= r9 * 9 ? 1 : 0) + 4 + (r2 >= r9 * 12 ? 1 : 0) + (r2 >= r9 * 15 ? 1 : 0);
                r7.m19706().m19653(i);
            }
            r6.m19714(r7.m19706().m19652() + (r5 * 6));
            r6.m19762(r10);
            r6.m19761(stateRef.m19770());
            r6.m19755(i);
            r7.m19708(r5 + 1);
            r7.m19702(r7.m19699());
        }
        int r1 = stateRef.m19768();
        this.f15767.m19702(r1);
        this.f15768.m19702(r1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d7, code lost:
        if (r4.m19699() != 0) goto L_0x00d9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c9  */
    /* renamed from: 龘  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m19663(boolean r14, junrar.unpack.ppm.State r15) {
        /*
            r13 = this;
            junrar.unpack.ppm.StateRef r10 = r13.f15763
            junrar.unpack.ppm.State r11 = r13.f15754
            byte[] r12 = r13.m19674()
            junrar.unpack.ppm.State r8 = r11.m19760((byte[]) r12)
            junrar.unpack.ppm.PPMContext r11 = r13.f15764
            byte[] r12 = r13.m19674()
            junrar.unpack.ppm.PPMContext r4 = r11.m19707((byte[]) r12)
            junrar.unpack.ppm.PPMContext r11 = r13.f15768
            int r11 = r11.m19713()
            r4.m19702((int) r11)
            junrar.unpack.ppm.PPMContext r11 = r13.f15770
            byte[] r12 = r13.m19674()
            junrar.unpack.ppm.PPMContext r9 = r11.m19707((byte[]) r12)
            junrar.unpack.ppm.State r11 = r13.f15765
            int r11 = r11.m19758()
            r9.m19702((int) r11)
            junrar.unpack.ppm.State r11 = r13.f15755
            byte[] r12 = r13.m19674()
            junrar.unpack.ppm.State r3 = r11.m19760((byte[]) r12)
            r5 = 0
            r2 = 0
            if (r14 != 0) goto L_0x0054
            int[] r11 = r13.f15760
            int r6 = r5 + 1
            junrar.unpack.ppm.State r12 = r13.f15765
            int r12 = r12.m19713()
            r11[r5] = r12
            int r11 = r4.m19699()
            if (r11 != 0) goto L_0x0185
            r2 = 1
            r5 = r6
        L_0x0054:
            if (r2 != 0) goto L_0x00b6
            r1 = 0
            int r11 = r15.m19713()
            if (r11 == 0) goto L_0x00d9
            int r11 = r15.m19713()
            r3.m19714(r11)
            int r11 = r4.m19699()
            r4.m19702((int) r11)
            r1 = 1
            r6 = r5
        L_0x006d:
            if (r1 != 0) goto L_0x00a3
            int r11 = r4.m19699()
            r4.m19702((int) r11)
            int r11 = r4.m19695()
            r12 = 1
            if (r11 == r12) goto L_0x00bd
            junrar.unpack.ppm.FreqData r11 = r4.m19706()
            int r11 = r11.m19652()
            r3.m19714(r11)
            int r11 = r3.m19759()
            junrar.unpack.ppm.State r12 = r13.f15765
            int r12 = r12.m19759()
            if (r11 == r12) goto L_0x00a3
        L_0x0094:
            r3.m19752()
            int r11 = r3.m19759()
            junrar.unpack.ppm.State r12 = r13.f15765
            int r12 = r12.m19759()
            if (r11 != r12) goto L_0x0094
        L_0x00a3:
            r1 = 0
            int r11 = r3.m19758()
            int r12 = r9.m19713()
            if (r11 == r12) goto L_0x00c9
            int r11 = r3.m19758()
            r4.m19702((int) r11)
            r5 = r6
        L_0x00b6:
            if (r5 != 0) goto L_0x00db
            int r11 = r4.m19713()
        L_0x00bc:
            return r11
        L_0x00bd:
            junrar.unpack.ppm.State r11 = r4.m19701()
            int r11 = r11.m19713()
            r3.m19714(r11)
            goto L_0x00a3
        L_0x00c9:
            int[] r11 = r13.f15760
            int r5 = r6 + 1
            int r12 = r3.m19713()
            r11[r6] = r12
            int r11 = r4.m19699()
            if (r11 == 0) goto L_0x00b6
        L_0x00d9:
            r6 = r5
            goto L_0x006d
        L_0x00db:
            byte[] r11 = r13.m19674()
            int r12 = r9.m19713()
            byte r11 = r11[r12]
            r10.m19771((int) r11)
            int r11 = r9.m19713()
            int r11 = r11 + 1
            r10.m19767(r11)
            int r11 = r4.m19695()
            r12 = 1
            if (r11 == r12) goto L_0x0171
            int r11 = r4.m19713()
            junrar.unpack.ppm.SubAllocator r12 = r13.f15749
            int r12 = r12.m19784()
            if (r11 > r12) goto L_0x0106
            r11 = 0
            goto L_0x00bc
        L_0x0106:
            junrar.unpack.ppm.FreqData r11 = r4.m19706()
            int r11 = r11.m19652()
            r3.m19714(r11)
            int r11 = r3.m19759()
            int r12 = r10.m19770()
            if (r11 == r12) goto L_0x0128
        L_0x011b:
            r3.m19752()
            int r11 = r3.m19759()
            int r12 = r10.m19770()
            if (r11 != r12) goto L_0x011b
        L_0x0128:
            int r11 = r3.m19754()
            int r0 = r11 + -1
            junrar.unpack.ppm.FreqData r11 = r4.m19706()
            int r11 = r11.m19654()
            int r12 = r4.m19695()
            int r11 = r11 - r12
            int r7 = r11 - r0
            int r11 = r0 * 2
            if (r11 > r7) goto L_0x0166
            int r11 = r0 * 5
            if (r11 <= r7) goto L_0x0164
            r11 = 1
        L_0x0146:
            int r11 = r11 + 1
            r10.m19766(r11)
        L_0x014b:
            int[] r11 = r13.f15760
            int r5 = r5 + -1
            r11 = r11[r5]
            r8.m19714(r11)
            int r11 = r4.m19705((junrar.unpack.ppm.ModelPPM) r13, (junrar.unpack.ppm.State) r8, (junrar.unpack.ppm.StateRef) r10)
            r4.m19702((int) r11)
            int r11 = r4.m19713()
            if (r11 != 0) goto L_0x017d
            r11 = 0
            goto L_0x00bc
        L_0x0164:
            r11 = 0
            goto L_0x0146
        L_0x0166:
            int r11 = r0 * 2
            int r12 = r7 * 3
            int r11 = r11 + r12
            int r11 = r11 + -1
            int r12 = r7 * 2
            int r11 = r11 / r12
            goto L_0x0146
        L_0x0171:
            junrar.unpack.ppm.State r11 = r4.m19701()
            int r11 = r11.m19754()
            r10.m19766(r11)
            goto L_0x014b
        L_0x017d:
            if (r5 != 0) goto L_0x014b
            int r11 = r4.m19713()
            goto L_0x00bc
        L_0x0185:
            r5 = r6
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: junrar.unpack.ppm.ModelPPM.m19663(boolean, junrar.unpack.ppm.State):int");
    }

    public String toString() {
        return "ModelPPM[" + "\n  numMasked=" + this.f15742 + "\n  initEsc=" + this.f15743 + "\n  orderFall=" + this.f15744 + "\n  maxOrder=" + this.f15753 + "\n  runLength=" + this.f15756 + "\n  initRL=" + this.f15757 + "\n  escCount=" + this.f15772 + "\n  prevSuccess=" + this.f15750 + "\n  foundState=" + this.f15765 + "\n  coder=" + this.f15747 + "\n  subAlloc=" + this.f15749 + "\n]";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public int m19664() {
        return this.f15772;
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public void m19665(int i) {
        this.f15756 = i;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public void m19666(int i) {
        m19665(m19676() + i);
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    public int[] m19667() {
        return this.f15748;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public int m19668() {
        return this.f15742;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    public void m19669(int i) {
        this.f15751 = i & 255;
    }

    /* renamed from: ʾ  reason: contains not printable characters */
    public RangeCoder m19670() {
        return this.f15747;
    }

    /* renamed from: ʿ  reason: contains not printable characters */
    public int[] m19671() {
        return this.f15771;
    }

    /* renamed from: ˈ  reason: contains not printable characters */
    public int[][] m19672() {
        return this.f15752;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public State m19673() {
        return this.f15765;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public byte[] m19674() {
        return this.f15749.m19786();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public int m19675() {
        return this.f15744;
    }

    /* renamed from: ˑ  reason: contains not printable characters */
    public int m19676() {
        return this.f15756;
    }

    /* renamed from: ٴ  reason: contains not printable characters */
    public int m19677() {
        return this.f15750;
    }

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int m19678() {
        return this.f15751;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public int m19679() {
        return this.f15757;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m19680(int i) {
        this.f15743 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0051, code lost:
        if (r4.f15768.m19700(r4) != false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a2, code lost:
        if (r4.f15768.m19703(r4) == false) goto L_0x001d;
     */
    /* renamed from: 靐  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int m19681() throws java.io.IOException, junrar.exception.RarException {
        /*
            r4 = this;
            r0 = -1
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            int r2 = r2.m19713()
            junrar.unpack.ppm.SubAllocator r3 = r4.f15749
            int r3 = r3.m19784()
            if (r2 <= r3) goto L_0x001d
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            int r2 = r2.m19713()
            junrar.unpack.ppm.SubAllocator r3 = r4.f15749
            int r3 = r3.m19783()
            if (r2 <= r3) goto L_0x001e
        L_0x001d:
            return r0
        L_0x001e:
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            int r2 = r2.m19695()
            r3 = 1
            if (r2 == r3) goto L_0x00aa
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            junrar.unpack.ppm.FreqData r2 = r2.m19706()
            int r2 = r2.m19652()
            junrar.unpack.ppm.SubAllocator r3 = r4.f15749
            int r3 = r3.m19784()
            if (r2 <= r3) goto L_0x001d
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            junrar.unpack.ppm.FreqData r2 = r2.m19706()
            int r2 = r2.m19652()
            junrar.unpack.ppm.SubAllocator r3 = r4.f15749
            int r3 = r3.m19783()
            if (r2 > r3) goto L_0x001d
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            boolean r2 = r2.m19700(r4)
            if (r2 == 0) goto L_0x001d
        L_0x0053:
            junrar.unpack.ppm.RangeCoder r2 = r4.f15747
            r2.m19718()
        L_0x0058:
            junrar.unpack.ppm.State r2 = r4.f15765
            int r2 = r2.m19713()
            if (r2 != 0) goto L_0x00b0
            junrar.unpack.ppm.RangeCoder r2 = r4.f15747
            r2.m19717()
        L_0x0065:
            int r2 = r4.f15744
            int r2 = r2 + 1
            r4.f15744 = r2
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            junrar.unpack.ppm.PPMContext r3 = r4.f15768
            int r3 = r3.m19699()
            r2.m19702((int) r3)
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            int r2 = r2.m19713()
            junrar.unpack.ppm.SubAllocator r3 = r4.f15749
            int r3 = r3.m19784()
            if (r2 <= r3) goto L_0x001d
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            int r2 = r2.m19713()
            junrar.unpack.ppm.SubAllocator r3 = r4.f15749
            int r3 = r3.m19783()
            if (r2 > r3) goto L_0x001d
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            int r2 = r2.m19695()
            int r3 = r4.f15742
            if (r2 == r3) goto L_0x0065
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            boolean r2 = r2.m19703((junrar.unpack.ppm.ModelPPM) r4)
            if (r2 == 0) goto L_0x001d
            junrar.unpack.ppm.RangeCoder r2 = r4.f15747
            r2.m19718()
            goto L_0x0058
        L_0x00aa:
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            r2.m19697((junrar.unpack.ppm.ModelPPM) r4)
            goto L_0x0053
        L_0x00b0:
            junrar.unpack.ppm.State r2 = r4.f15765
            int r0 = r2.m19759()
            int r2 = r4.f15744
            if (r2 != 0) goto L_0x00df
            junrar.unpack.ppm.State r2 = r4.f15765
            int r2 = r2.m19758()
            junrar.unpack.ppm.SubAllocator r3 = r4.f15749
            int r3 = r3.m19784()
            if (r2 <= r3) goto L_0x00df
            junrar.unpack.ppm.State r2 = r4.f15765
            int r1 = r2.m19758()
            junrar.unpack.ppm.PPMContext r2 = r4.f15768
            r2.m19702((int) r1)
            junrar.unpack.ppm.PPMContext r2 = r4.f15767
            r2.m19702((int) r1)
        L_0x00d8:
            junrar.unpack.ppm.RangeCoder r2 = r4.f15747
            r2.m19717()
            goto L_0x001d
        L_0x00df:
            r4.m19662()
            int r2 = r4.f15772
            if (r2 != 0) goto L_0x00d8
            r4.m19659()
            goto L_0x00d8
        */
        throw new UnsupportedOperationException("Method not decompiled: junrar.unpack.ppm.ModelPPM.m19681():int");
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19682(int i) {
        m19688(m19664() + i);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public SEE2Context m19683() {
        return this.f15766;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19684(int i) {
        this.f15750 = i & 255;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19685(int i) {
        this.f15742 = i;
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public SEE2Context[][] m19686() {
        return this.f15769;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SubAllocator m19687() {
        return this.f15749;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19688(int i) {
        this.f15772 = i & 255;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public boolean m19689(Unpack unpack, int i) throws IOException, RarException {
        boolean z = true;
        int r1 = unpack.m19582() & 255;
        boolean z2 = (r1 & 32) != 0;
        int i2 = 0;
        if (z2) {
            i2 = unpack.m19582();
            if (i2 > 1) {
                i2 = 1;
            }
        } else if (this.f15749.m19791() == 0) {
            return false;
        }
        if ((r1 & 64) != 0) {
            unpack.m19583(unpack.m19582());
        }
        this.f15747.m19721(unpack);
        if (z2) {
            int i3 = (r1 & 31) + 1;
            if (i3 > 16) {
                i3 = ((i3 - 16) * 3) + 16;
            }
            if (i3 == 1) {
                this.f15749.m19793();
                return false;
            }
            this.f15749.m19798(i2 + 1);
            this.f15768 = new PPMContext(m19674());
            this.f15767 = new PPMContext(m19674());
            this.f15765 = new State(m19674());
            this.f15766 = new SEE2Context();
            for (int i4 = 0; i4 < 25; i4++) {
                for (int i5 = 0; i5 < 16; i5++) {
                    this.f15769[i4][i5] = new SEE2Context();
                }
            }
            m19661(i3);
        }
        if (this.f15768.m19713() == 0) {
            z = false;
        }
        return z;
    }

    /* renamed from: ﹶ  reason: contains not printable characters */
    public int[] m19690() {
        return this.f15746;
    }

    /* renamed from: ﾞ  reason: contains not printable characters */
    public int[] m19691() {
        return this.f15745;
    }
}
