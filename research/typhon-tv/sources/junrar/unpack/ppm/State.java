package junrar.unpack.ppm;

import junrar.io.Raw;

public class State extends Pointer {
    public State(byte[] bArr) {
        super(bArr);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static void m19751(State state, State state2) {
        byte[] bArr = state.f15790;
        byte[] bArr2 = state2.f15790;
        int i = 0;
        int i2 = state.f15789;
        int i3 = state2.f15789;
        while (i < 6) {
            byte b = bArr[i2];
            bArr[i2] = bArr2[i3];
            bArr2[i3] = b;
            i++;
            i2++;
            i3++;
        }
    }

    public String toString() {
        return "State[" + "\n  pos=" + this.f15789 + "\n  size=" + 6 + "\n  symbol=" + m19759() + "\n  freq=" + m19754() + "\n  successor=" + m19758() + "\n]";
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    public State m19752() {
        m19714(this.f15789 + 6);
        return this;
    }

    /* renamed from: 连任  reason: contains not printable characters */
    public void m19753(int i) {
        Raw.m19491(this.f15790, this.f15789 + 2, i);
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m19754() {
        return this.f15790[this.f15789 + 1] & 255;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public void m19755(int i) {
        this.f15790[this.f15789 + 1] = (byte) i;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public State m19756() {
        m19714(this.f15789 - 6);
        return this;
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19757(int i) {
        byte[] bArr = this.f15790;
        int i2 = this.f15789 + 1;
        bArr[i2] = (byte) (bArr[i2] + i);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public int m19758() {
        return Raw.m19490(this.f15790, this.f15789 + 2);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19759() {
        return this.f15790[this.f15789] & 255;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public State m19760(byte[] bArr) {
        this.f15790 = bArr;
        this.f15789 = 0;
        return this;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19761(int i) {
        this.f15790[this.f15789] = (byte) i;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19762(PPMContext pPMContext) {
        m19753(pPMContext.m19713());
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19763(State state) {
        System.arraycopy(state.f15790, state.f15789, this.f15790, this.f15789, 6);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19764(StateRef stateRef) {
        m19761(stateRef.m19770());
        m19755(stateRef.m19765());
        m19753(stateRef.m19768());
    }
}
