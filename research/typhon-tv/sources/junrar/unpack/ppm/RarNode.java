package junrar.unpack.ppm;

import junrar.io.Raw;

public class RarNode extends Pointer {

    /* renamed from: 龘  reason: contains not printable characters */
    private int f15803;

    public RarNode(byte[] bArr) {
        super(bArr);
    }

    public String toString() {
        return "State[" + "\n  pos=" + this.f15789 + "\n  size=" + 4 + "\n  next=" + m19741() + "\n]";
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19741() {
        if (this.f15790 != null) {
            this.f15803 = Raw.m19490(this.f15790, this.f15789);
        }
        return this.f15803;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19742(int i) {
        this.f15803 = i;
        if (this.f15790 != null) {
            Raw.m19491(this.f15790, this.f15789, i);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19743(RarNode rarNode) {
        m19742(rarNode.m19713());
    }
}
