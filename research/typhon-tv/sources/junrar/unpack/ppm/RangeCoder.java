package junrar.unpack.ppm;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import junrar.exception.RarException;
import junrar.unpack.Unpack;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class RangeCoder {

    /* renamed from: 连任  reason: contains not printable characters */
    private Unpack f15791;

    /* renamed from: 靐  reason: contains not printable characters */
    private long f15792;

    /* renamed from: 麤  reason: contains not printable characters */
    private final SubRange f15793 = new SubRange();

    /* renamed from: 齉  reason: contains not printable characters */
    private long f15794;

    /* renamed from: 龘  reason: contains not printable characters */
    private long f15795;

    public static class SubRange {

        /* renamed from: 靐  reason: contains not printable characters */
        private long f15796;

        /* renamed from: 齉  reason: contains not printable characters */
        private long f15797;

        /* renamed from: 龘  reason: contains not printable characters */
        private long f15798;

        public String toString() {
            return "SubRange[" + "\n  lowCount=" + this.f15798 + "\n  highCount=" + this.f15796 + "\n  scale=" + this.f15797 + "]";
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public long m19722() {
            return this.f15798 & InternalZipTyphoonApp.ZIP_64_LIMIT;
        }

        /* renamed from: 靐  reason: contains not printable characters */
        public void m19723(long j) {
            this.f15798 = InternalZipTyphoonApp.ZIP_64_LIMIT & j;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public long m19724() {
            return this.f15797;
        }

        /* renamed from: 齉  reason: contains not printable characters */
        public void m19725(long j) {
            this.f15797 = InternalZipTyphoonApp.ZIP_64_LIMIT & j;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public long m19726() {
            return this.f15796;
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m19727(int i) {
            m19725(m19724() + ((long) i));
        }

        /* renamed from: 龘  reason: contains not printable characters */
        public void m19728(long j) {
            this.f15796 = InternalZipTyphoonApp.ZIP_64_LIMIT & j;
        }
    }

    /* renamed from: 连任  reason: contains not printable characters */
    private int m19715() throws IOException, RarException {
        return this.f15791.m19582();
    }

    public String toString() {
        return "RangeCoder[" + "\n  low=" + this.f15795 + "\n  code=" + this.f15792 + "\n  range=" + this.f15794 + "\n  subrange=" + this.f15793 + "]";
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public int m19716() {
        this.f15794 = (this.f15794 / this.f15793.m19724()) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        return (int) ((this.f15792 - this.f15795) / this.f15794);
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public void m19717() throws IOException, RarException {
        boolean z = false;
        while (true) {
            if ((this.f15795 ^ (this.f15795 + this.f15794)) >= 16777216) {
                z = this.f15794 < PlaybackStateCompat.ACTION_PREPARE_FROM_MEDIA_ID;
                if (!z) {
                    return;
                }
            }
            if (z) {
                this.f15794 = (-this.f15795) & 32767 & InternalZipTyphoonApp.ZIP_64_LIMIT;
                z = false;
            }
            this.f15792 = ((this.f15792 << 8) | ((long) m19715())) & InternalZipTyphoonApp.ZIP_64_LIMIT;
            this.f15794 = (this.f15794 << 8) & InternalZipTyphoonApp.ZIP_64_LIMIT;
            this.f15795 = (this.f15795 << 8) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        }
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public void m19718() {
        this.f15795 = (this.f15795 + (this.f15794 * this.f15793.m19722())) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        this.f15794 = (this.f15794 * (this.f15793.m19726() - this.f15793.m19722())) & InternalZipTyphoonApp.ZIP_64_LIMIT;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public long m19719(int i) {
        this.f15794 >>>= i;
        return ((this.f15792 - this.f15795) / this.f15794) & InternalZipTyphoonApp.ZIP_64_LIMIT;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public SubRange m19720() {
        return this.f15793;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19721(Unpack unpack) throws IOException, RarException {
        this.f15791 = unpack;
        this.f15792 = 0;
        this.f15795 = 0;
        this.f15794 = InternalZipTyphoonApp.ZIP_64_LIMIT;
        for (int i = 0; i < 4; i++) {
            this.f15792 = ((this.f15792 << 8) | ((long) m19715())) & InternalZipTyphoonApp.ZIP_64_LIMIT;
        }
    }
}
