package junrar.io;

import java.io.IOException;
import java.io.InputStream;

public class ReadOnlyAccessInputStream extends InputStream {

    /* renamed from: 靐  reason: contains not printable characters */
    private long f15519;

    /* renamed from: 齉  reason: contains not printable characters */
    private final long f15520;

    /* renamed from: 龘  reason: contains not printable characters */
    private IReadOnlyAccess f15521;

    public ReadOnlyAccessInputStream(IReadOnlyAccess iReadOnlyAccess, long j, long j2) throws IOException {
        this.f15521 = iReadOnlyAccess;
        this.f15519 = j;
        this.f15520 = j2;
        iReadOnlyAccess.m19486(this.f15519);
    }

    public int read() throws IOException {
        if (this.f15519 == this.f15520) {
            return -1;
        }
        int read = this.f15521.read();
        this.f15519++;
        return read;
    }

    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (i2 == 0) {
            return 0;
        }
        if (this.f15519 == this.f15520) {
            return -1;
        }
        int read = this.f15521.read(bArr, i, (int) Math.min((long) i2, this.f15520 - this.f15519));
        this.f15519 += (long) read;
        return read;
    }
}
