package junrar.io;

public class Raw {
    /* renamed from: 靐  reason: contains not printable characters */
    public static final short m19487(byte[] bArr, int i) {
        return (short) ((bArr[i] & 255) + ((short) (((short) ((bArr[i + 1] & 255) + 0)) << 8)));
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public static final void m19488(byte[] bArr, int i, int i2) {
        int i3 = ((bArr[i] & 255) + (i2 & 255)) >>> 8;
        bArr[i] = (byte) (bArr[i] + (i2 & 255));
        if (i3 > 0 || (65280 & i2) != 0) {
            int i4 = i + 1;
            bArr[i4] = (byte) (bArr[i4] + ((i2 >>> 8) & 255) + i3);
        }
    }

    /* renamed from: 麤  reason: contains not printable characters */
    public static final long m19489(byte[] bArr, int i) {
        return ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 1]) & 255) << 8) | (((long) bArr[i]) & 255);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static final int m19490(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | ((bArr[i + 2] & 255) << 16) | ((bArr[i + 1] & 255) << 8) | (bArr[i] & 255);
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public static final void m19491(byte[] bArr, int i, int i2) {
        bArr[i + 3] = (byte) (i2 >>> 24);
        bArr[i + 2] = (byte) (i2 >>> 16);
        bArr[i + 1] = (byte) (i2 >>> 8);
        bArr[i] = (byte) (i2 & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final int m19492(byte[] bArr, int i) {
        return ((((((0 | (bArr[i] & 255)) << 8) | (bArr[i + 1] & 255)) << 8) | (bArr[i + 2] & 255)) << 8) | (bArr[i + 3] & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m19493(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) ((i2 >>> 24) & 255);
        bArr[i + 1] = (byte) ((i2 >>> 16) & 255);
        bArr[i + 2] = (byte) ((i2 >>> 8) & 255);
        bArr[i + 3] = (byte) (i2 & 255);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public static final void m19494(byte[] bArr, int i, short s) {
        bArr[i + 1] = (byte) (s >>> 8);
        bArr[i] = (byte) (s & 255);
    }
}
