package junrar.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import net.lingala.zip4j.util.InternalZipTyphoonApp;

public class ReadOnlyAccessFile extends RandomAccessFile implements IReadOnlyAccess {

    /* renamed from: 龘  reason: contains not printable characters */
    static final /* synthetic */ boolean f15517 = (!ReadOnlyAccessFile.class.desiredAssertionStatus());

    /* renamed from: 靐  reason: contains not printable characters */
    private final File f15518;

    public ReadOnlyAccessFile(File file) throws FileNotFoundException {
        super(file, InternalZipTyphoonApp.READ_MODE);
        this.f15518 = file;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m19495() throws IOException {
        return getFilePointer();
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public int m19496(byte[] bArr, int i) throws IOException {
        if (f15517 || i > 0) {
            readFully(bArr, 0, i);
            return i;
        }
        throw new AssertionError(i);
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public IReadOnlyAccess m19497() {
        try {
            return new ReadOnlyAccessFile(this.f15518);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public void m19498(long j) throws IOException {
        seek(j);
    }
}
