package junrar.io;

import java.io.IOException;

public interface IReadOnlyAccess {
    void close() throws IOException;

    int read() throws IOException;

    int read(byte[] bArr, int i, int i2) throws IOException;

    /* renamed from: 靐  reason: contains not printable characters */
    long m19483() throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    int m19484(byte[] bArr, int i) throws IOException;

    /* renamed from: 龘  reason: contains not printable characters */
    IReadOnlyAccess m19485();

    /* renamed from: 龘  reason: contains not printable characters */
    void m19486(long j) throws IOException;
}
