package junrar.impl;

import java.io.File;
import java.io.IOException;
import junrar.Archive;
import junrar.Volume;
import junrar.io.IReadOnlyAccess;
import junrar.io.ReadOnlyAccessFile;

public class FileVolume implements Volume {

    /* renamed from: 靐  reason: contains not printable characters */
    private final File f15514;

    /* renamed from: 龘  reason: contains not printable characters */
    private final Archive f15515;

    public FileVolume(Archive archive, File file) {
        this.f15515 = archive;
        this.f15514 = file;
    }

    /* renamed from: 靐  reason: contains not printable characters */
    public long m19479() {
        return this.f15514.length();
    }

    /* renamed from: 齉  reason: contains not printable characters */
    public File m19480() {
        return this.f15514;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public IReadOnlyAccess m19481() throws IOException {
        return new ReadOnlyAccessFile(this.f15514);
    }
}
