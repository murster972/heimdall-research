package junrar.impl;

import java.io.File;
import java.io.IOException;
import junrar.Archive;
import junrar.Volume;
import junrar.VolumeManager;
import junrar.util.VolumeHelper;

public class FileVolumeManager implements VolumeManager {

    /* renamed from: 龘  reason: contains not printable characters */
    private final File f15516;

    public FileVolumeManager(File file) {
        this.f15516 = file;
    }

    /* renamed from: 龘  reason: contains not printable characters */
    public Volume m19482(Archive archive, Volume volume) throws IOException {
        if (volume == null) {
            return new FileVolume(archive, this.f15516);
        }
        return new FileVolume(archive, new File(VolumeHelper.m19855(((FileVolume) volume).m19480().getAbsolutePath(), !archive.m19464().m19544() || archive.m19462())));
    }
}
