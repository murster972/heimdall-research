package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class PLockerSK extends BaseProvider {
    private String c = Utils.getProvider(45);

    private int j(String str) {
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            i += Character.codePointAt(str, i2) + i2;
        }
        return i;
    }

    public String a() {
        return "PLockerSK";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, "-1");
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, movieInfo.session);
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, movieInfo.eps);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:205:0x04a7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0170, code lost:
        if (r11.contains("ts") != false) goto L_0x0177;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0240, code lost:
        if (r8.contains("503 Service Unavailable") == false) goto L_0x02af;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02c8 A[SYNTHETIC, Splitter:B:106:0x02c8] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x037b A[Catch:{ Exception -> 0x052d, all -> 0x04a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x037e A[Catch:{ Exception -> 0x052d, all -> 0x04a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x03c0 A[Catch:{ Exception -> 0x052d, all -> 0x04a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x0447 A[SYNTHETIC, Splitter:B:181:0x0447] */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x04ea  */
    /* JADX WARNING: Removed duplicated region for block: B:247:? A[ExcHandler: Exception (unused java.lang.Exception), PHI: r16 
      PHI: (r16v16 java.lang.String) = (r16v19 java.lang.String), (r16v19 java.lang.String), (r16v19 java.lang.String), (r16v19 java.lang.String), (r16v19 java.lang.String), (r16v19 java.lang.String), (r16v19 java.lang.String), (r16v19 java.lang.String), (r16v20 java.lang.String), (r16v20 java.lang.String), (r16v20 java.lang.String) binds: [B:159:0x0389, B:160:?, B:161:0x0397, B:201:0x049e, B:202:?, B:174:0x041f, B:175:?, B:176:0x0421, B:146:0x035e, B:147:?, B:148:0x0360] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:146:0x035e] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x019a A[Catch:{ all -> 0x04f6 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r28, java.lang.String r29, com.movie.data.model.MovieInfo r30, java.lang.String r31) {
        /*
            r27 = this;
            r1 = r27
            r2 = r28
            r3 = r29
            java.lang.String r4 = "&update=0"
            java.lang.String r5 = "&server="
            java.lang.String r6 = "&_="
            java.lang.String r7 = "/ajax/episode/info?ts="
            java.lang.String r8 = "id"
            java.lang.String r9 = "-"
            java.lang.String r10 = "&id="
            r0 = r30
            java.lang.String r0 = r0.session
            boolean r0 = r0.isEmpty()
            r11 = 1
            r12 = r0 ^ 1
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            r13 = 0
            java.util.Map[] r14 = new java.util.Map[r13]
            java.lang.String r0 = r0.a((java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r14)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r14 = "html[data-ts]"
            org.jsoup.nodes.Element r0 = r0.h(r14)
            if (r0 == 0) goto L_0x054e
            java.lang.String r14 = "data-ts"
            java.lang.String r14 = r0.b((java.lang.String) r14)
            java.lang.String r15 = "div[class=watch-page][data-id][data-type]"
            org.jsoup.nodes.Element r0 = r0.h(r15)
            java.lang.String r15 = "data-id"
            java.lang.String r0 = r0.b((java.lang.String) r15)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = r1.c
            r11.append(r13)
            java.lang.String r13 = "/ajax/film/servers?ts=%s&_=1201&id=%s"
            r11.append(r13)
            java.lang.String r11 = r11.toString()
            com.original.tase.helper.http.HttpHelper r13 = com.original.tase.helper.http.HttpHelper.e()
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r16 = r4
            r4 = 0
            r2[r4] = r14
            r17 = 1
            r2[r17] = r0
            java.lang.String r0 = java.lang.String.format(r11, r2)
            java.util.Map[] r2 = new java.util.Map[r4]
            java.lang.String r0 = r13.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r2)
            java.lang.String r2 = ""
            java.lang.String r4 = "\""
            java.lang.String r0 = r0.replace(r4, r2)
            java.lang.String r4 = "\\"
            java.lang.String r0 = r0.replace(r4, r2)
            org.jsoup.nodes.Document r4 = org.jsoup.Jsoup.b(r0)
            java.lang.String r0 = "ul[class*=episodes]"
            org.jsoup.select.Elements r0 = r4.g((java.lang.String) r0)
            java.util.Iterator r11 = r0.iterator()
        L_0x0091:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x054e
            java.lang.Object r0 = r11.next()
            r13 = r0
            org.jsoup.nodes.Element r13 = (org.jsoup.nodes.Element) r13
            java.lang.String r0 = "div[data-type][data-id]"
            org.jsoup.nodes.Element r0 = r4.h(r0)     // Catch:{ all -> 0x00ad }
            java.lang.String r0 = r0.b((java.lang.String) r15)     // Catch:{ all -> 0x00ad }
            r17 = r4
            r18 = r11
            goto L_0x00ba
        L_0x00ad:
            r0 = move-exception
            r17 = r4
            r18 = r11
            r4 = 0
            boolean[] r11 = new boolean[r4]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r11)
            java.lang.String r0 = "28"
        L_0x00ba:
            r4 = r0
            java.lang.String r0 = "a[data-id]"
            org.jsoup.select.Elements r0 = r13.g((java.lang.String) r0)
            java.util.Iterator r11 = r0.iterator()
        L_0x00c5:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x0542
            java.lang.Object r0 = r11.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            boolean r13 = r28.isDisposed()     // Catch:{ all -> 0x0513 }
            if (r13 != 0) goto L_0x0512
            java.lang.String r13 = r0.G()     // Catch:{ all -> 0x0513 }
            java.lang.String r13 = r13.trim()     // Catch:{ all -> 0x0513 }
            java.lang.String r13 = r13.toLowerCase()     // Catch:{ all -> 0x0513 }
            r19 = r11
            java.lang.String r11 = "eps"
            java.lang.String r11 = r13.replace(r11, r2)     // Catch:{ all -> 0x0501 }
            java.lang.String r13 = "ep"
            java.lang.String r11 = r11.replace(r13, r2)     // Catch:{ all -> 0x0501 }
            boolean r13 = r11.contains(r9)     // Catch:{ all -> 0x0501 }
            if (r13 == 0) goto L_0x0113
            java.lang.String[] r13 = r11.split(r9)     // Catch:{ all -> 0x00fe }
            r20 = r2
            goto L_0x011c
        L_0x00fe:
            r0 = move-exception
            r11 = r28
            r20 = r2
            r21 = r7
            r7 = r8
            r22 = r9
            r24 = r12
        L_0x010a:
            r23 = r15
            r26 = r16
            r2 = 0
            r16 = r4
            goto L_0x0528
        L_0x0113:
            r20 = r2
            r13 = 1
            java.lang.String[] r2 = new java.lang.String[r13]     // Catch:{ all -> 0x04fd }
            r13 = 0
            r2[r13] = r11     // Catch:{ all -> 0x04fd }
            r13 = r2
        L_0x011c:
            java.lang.String r2 = "ts"
            if (r12 != 0) goto L_0x0160
            int r11 = r13.length     // Catch:{ all -> 0x0155 }
            r22 = r9
            r9 = 0
        L_0x0124:
            if (r9 >= r11) goto L_0x0151
            r23 = r11
            r11 = r13[r9]     // Catch:{ all -> 0x014f }
            r24 = r12
            java.lang.String r12 = "(\\d+)"
            r25 = r13
            r13 = 1
            java.lang.String r12 = com.original.tase.utils.Regex.a((java.lang.String) r11, (java.lang.String) r12, (int) r13)     // Catch:{ all -> 0x0175 }
            boolean r26 = r12.isEmpty()     // Catch:{ all -> 0x0175 }
            if (r26 != 0) goto L_0x0146
            boolean r11 = r12.equals(r11)     // Catch:{ all -> 0x0175 }
            if (r11 == 0) goto L_0x0146
            java.lang.Integer r11 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x0175 }
            goto L_0x0173
        L_0x0146:
            int r9 = r9 + 1
            r11 = r23
            r12 = r24
            r13 = r25
            goto L_0x0124
        L_0x014f:
            r0 = move-exception
            goto L_0x0158
        L_0x0151:
            r24 = r12
            r11 = 0
            goto L_0x0173
        L_0x0155:
            r0 = move-exception
            r22 = r9
        L_0x0158:
            r24 = r12
        L_0x015a:
            r11 = r28
            r21 = r7
            r7 = r8
            goto L_0x010a
        L_0x0160:
            r22 = r9
            r24 = r12
            java.lang.String r9 = "cam"
            boolean r9 = r11.contains(r9)     // Catch:{ all -> 0x04f6 }
            if (r9 != 0) goto L_0x0177
            boolean r9 = r11.contains(r2)     // Catch:{ all -> 0x0175 }
            if (r9 == 0) goto L_0x0173
            goto L_0x0177
        L_0x0173:
            r13 = 0
            goto L_0x0178
        L_0x0175:
            r0 = move-exception
            goto L_0x015a
        L_0x0177:
            r13 = 1
        L_0x0178:
            java.lang.String r9 = r0.b((java.lang.String) r15)     // Catch:{ all -> 0x04f6 }
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap     // Catch:{ all -> 0x04f6 }
            r0.<init>()     // Catch:{ all -> 0x04f6 }
            r0.put(r8, r9)     // Catch:{ all -> 0x04f6 }
            java.lang.String r11 = "server"
            r0.put(r11, r4)     // Catch:{ all -> 0x04f6 }
            java.lang.String r11 = "update"
            java.lang.String r12 = "0"
            r0.put(r11, r12)     // Catch:{ all -> 0x04f6 }
            r0.put(r2, r14)     // Catch:{ all -> 0x04f6 }
            int r2 = r1.a(r0)     // Catch:{ all -> 0x04f6 }
            r11 = -1
            if (r2 == r11) goto L_0x04ea
            java.util.HashMap r12 = com.original.Constants.a()     // Catch:{ all -> 0x04f6 }
            java.lang.String r11 = "accept"
            r23 = r15
            java.lang.String r15 = "application/json, text/javascript, */*; q=0.01"
            r12.put(r11, r15)     // Catch:{ all -> 0x04e3 }
            java.lang.String r11 = "referer"
            r12.put(r11, r3)     // Catch:{ all -> 0x04e3 }
            java.lang.String r11 = "User-Agent"
            java.lang.String r15 = com.original.Constants.f5838a     // Catch:{ all -> 0x04e3 }
            r12.put(r11, r15)     // Catch:{ all -> 0x04e3 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x04e3 }
            r11.<init>()     // Catch:{ all -> 0x04e3 }
            java.lang.String r15 = r1.c     // Catch:{ all -> 0x04e3 }
            r11.append(r15)     // Catch:{ all -> 0x04e3 }
            r11.append(r7)     // Catch:{ all -> 0x04e3 }
            r25 = r8
            r15 = 0
            boolean[] r8 = new boolean[r15]     // Catch:{ all -> 0x04d9 }
            java.lang.String r8 = com.original.tase.utils.Utils.a((java.lang.String) r14, (boolean[]) r8)     // Catch:{ all -> 0x04d9 }
            r11.append(r8)     // Catch:{ all -> 0x04d9 }
            r11.append(r6)     // Catch:{ all -> 0x04d9 }
            java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ all -> 0x04d9 }
            boolean[] r8 = new boolean[r15]     // Catch:{ all -> 0x04d9 }
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r8)     // Catch:{ all -> 0x04d9 }
            r11.append(r2)     // Catch:{ all -> 0x04d9 }
            r11.append(r10)     // Catch:{ all -> 0x04d9 }
            boolean[] r2 = new boolean[r15]     // Catch:{ all -> 0x04d9 }
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r9, (boolean[]) r2)     // Catch:{ all -> 0x04d9 }
            r11.append(r2)     // Catch:{ all -> 0x04d9 }
            r11.append(r5)     // Catch:{ all -> 0x04d9 }
            boolean[] r2 = new boolean[r15]     // Catch:{ all -> 0x04d9 }
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r4, (boolean[]) r2)     // Catch:{ all -> 0x04d9 }
            r11.append(r2)     // Catch:{ all -> 0x04d9 }
            r2 = r16
            r11.append(r2)     // Catch:{ all -> 0x04cc }
            java.lang.String r8 = r11.toString()     // Catch:{ all -> 0x04cc }
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x04cc }
            java.lang.String r8 = r11.b((java.lang.String) r8, (java.lang.String) r3)     // Catch:{ all -> 0x04cc }
            java.lang.String r11 = r8.trim()     // Catch:{ all -> 0x04cc }
            java.lang.String r11 = r11.toLowerCase()     // Catch:{ all -> 0x04cc }
            java.lang.String r15 = "error"
            boolean r11 = r11.contains(r15)     // Catch:{ all -> 0x04cc }
            java.lang.String r15 = "token"
            if (r11 == 0) goto L_0x0238
            java.lang.String r11 = r8.trim()     // Catch:{ all -> 0x04cc }
            java.lang.String r11 = r11.toLowerCase()     // Catch:{ all -> 0x04cc }
            boolean r11 = r11.contains(r15)     // Catch:{ all -> 0x04cc }
            if (r11 == 0) goto L_0x0238
            java.lang.String r11 = r8.trim()     // Catch:{ all -> 0x04cc }
            java.lang.String r11 = r11.toLowerCase()     // Catch:{ all -> 0x04cc }
            r16 = r15
            java.lang.String r15 = "http"
            boolean r11 = r11.contains(r15)     // Catch:{ all -> 0x04cc }
            if (r11 == 0) goto L_0x0242
            goto L_0x023a
        L_0x0238:
            r16 = r15
        L_0x023a:
            java.lang.String r11 = "503 Service Unavailable"
            boolean r11 = r8.contains(r11)     // Catch:{ all -> 0x04cc }
            if (r11 == 0) goto L_0x02af
        L_0x0242:
            int r0 = r1.a(r0)     // Catch:{ all -> 0x02a4 }
            int r0 = r0 + 45
            r11 = -1
            if (r0 == r11) goto L_0x02af
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x02a4 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x02a4 }
            r11.<init>()     // Catch:{ all -> 0x02a4 }
            java.lang.String r15 = r1.c     // Catch:{ all -> 0x02a4 }
            r11.append(r15)     // Catch:{ all -> 0x02a4 }
            r11.append(r7)     // Catch:{ all -> 0x02a4 }
            r21 = r7
            r15 = 0
            boolean[] r7 = new boolean[r15]     // Catch:{ all -> 0x02a2 }
            java.lang.String r7 = com.original.tase.utils.Utils.a((java.lang.String) r14, (boolean[]) r7)     // Catch:{ all -> 0x02a2 }
            r11.append(r7)     // Catch:{ all -> 0x02a2 }
            r11.append(r6)     // Catch:{ all -> 0x02a2 }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x02a2 }
            boolean[] r7 = new boolean[r15]     // Catch:{ all -> 0x02a2 }
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r7)     // Catch:{ all -> 0x02a2 }
            r11.append(r0)     // Catch:{ all -> 0x02a2 }
            r11.append(r10)     // Catch:{ all -> 0x02a2 }
            boolean[] r0 = new boolean[r15]     // Catch:{ all -> 0x02a2 }
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r9, (boolean[]) r0)     // Catch:{ all -> 0x02a2 }
            r11.append(r0)     // Catch:{ all -> 0x02a2 }
            r11.append(r5)     // Catch:{ all -> 0x02a2 }
            boolean[] r0 = new boolean[r15]     // Catch:{ all -> 0x02a2 }
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r4, (boolean[]) r0)     // Catch:{ all -> 0x02a2 }
            r11.append(r0)     // Catch:{ all -> 0x02a2 }
            r11.append(r2)     // Catch:{ all -> 0x02a2 }
            java.lang.String r0 = r11.toString()     // Catch:{ all -> 0x02a2 }
            r7 = 1
            java.util.Map[] r11 = new java.util.Map[r7]     // Catch:{ all -> 0x02a2 }
            r7 = 0
            r11[r7] = r12     // Catch:{ all -> 0x02a2 }
            java.lang.String r8 = r8.a((java.lang.String) r0, (java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r11)     // Catch:{ all -> 0x02a2 }
            goto L_0x02b1
        L_0x02a2:
            r0 = move-exception
            goto L_0x02a7
        L_0x02a4:
            r0 = move-exception
            r21 = r7
        L_0x02a7:
            r11 = r28
            r26 = r2
            r16 = r4
            goto L_0x04d5
        L_0x02af:
            r21 = r7
        L_0x02b1:
            com.google.gson.JsonParser r0 = new com.google.gson.JsonParser     // Catch:{ all -> 0x04b5 }
            r0.<init>()     // Catch:{ all -> 0x04b5 }
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r8)     // Catch:{ all -> 0x04b5 }
            com.google.gson.JsonObject r7 = r0.f()     // Catch:{ all -> 0x04b5 }
            java.lang.String r0 = "target"
            com.google.gson.JsonElement r0 = r7.a((java.lang.String) r0)     // Catch:{ all -> 0x04b5 }
            java.lang.String r8 = "HD"
            if (r0 == 0) goto L_0x02ff
            boolean r11 = r0.k()     // Catch:{ all -> 0x04b5 }
            if (r11 != 0) goto L_0x02ff
            java.lang.String r0 = r0.i()     // Catch:{ all -> 0x04b5 }
            if (r0 == 0) goto L_0x02ff
            boolean r11 = r0.isEmpty()     // Catch:{ all -> 0x04b5 }
            if (r11 != 0) goto L_0x02ff
            java.lang.String r11 = "//"
            boolean r11 = r0.startsWith(r11)     // Catch:{ all -> 0x04b5 }
            if (r11 == 0) goto L_0x02f3
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x04b5 }
            r11.<init>()     // Catch:{ all -> 0x04b5 }
            java.lang.String r12 = "http:"
            r11.append(r12)     // Catch:{ all -> 0x04b5 }
            r11.append(r0)     // Catch:{ all -> 0x04b5 }
            java.lang.String r0 = r11.toString()     // Catch:{ all -> 0x04b5 }
        L_0x02f3:
            r11 = 1
            boolean[] r12 = new boolean[r11]     // Catch:{ all -> 0x04b5 }
            r11 = 0
            r12[r11] = r13     // Catch:{ all -> 0x04b5 }
            r11 = r28
            r1.a(r11, r0, r8, r12)     // Catch:{ all -> 0x04ab }
            goto L_0x0301
        L_0x02ff:
            r11 = r28
        L_0x0301:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04ad }
            r0.<init>()     // Catch:{ Exception -> 0x04ad }
            java.lang.String r12 = r1.c     // Catch:{ Exception -> 0x04ad }
            r0.append(r12)     // Catch:{ Exception -> 0x04ad }
            java.lang.String r12 = "/grabber-api/"
            r0.append(r12)     // Catch:{ Exception -> 0x04ad }
            java.lang.String r12 = r0.toString()     // Catch:{ Exception -> 0x04ad }
            java.lang.String r0 = "grabber"
            com.google.gson.JsonElement r0 = r7.a((java.lang.String) r0)     // Catch:{ all -> 0x0321 }
            java.lang.String r12 = r0.i()     // Catch:{ all -> 0x0321 }
            r26 = r2
            goto L_0x032a
        L_0x0321:
            r0 = move-exception
            r26 = r2
            r15 = 0
            boolean[] r2 = new boolean[r15]     // Catch:{ Exception -> 0x04af, all -> 0x04a9 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)     // Catch:{ Exception -> 0x04af, all -> 0x04a9 }
        L_0x032a:
            java.lang.String r0 = "params"
            com.google.gson.JsonElement r0 = r7.a((java.lang.String) r0)     // Catch:{ Exception -> 0x04af, all -> 0x04a9 }
            com.google.gson.JsonObject r2 = r0.f()     // Catch:{ Exception -> 0x04af, all -> 0x04a9 }
            r7 = r16
            com.google.gson.JsonElement r15 = r2.a((java.lang.String) r7)     // Catch:{ Exception -> 0x04af, all -> 0x04a9 }
            com.google.gson.JsonElement r0 = r2.a((java.lang.String) r7)     // Catch:{ all -> 0x0344 }
            java.lang.String r0 = r0.i()     // Catch:{ all -> 0x0344 }
            r3 = r0
            goto L_0x034d
        L_0x0344:
            r0 = move-exception
            r7 = 0
            boolean[] r3 = new boolean[r7]     // Catch:{ Exception -> 0x04af, all -> 0x04a9 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r3)     // Catch:{ Exception -> 0x04af, all -> 0x04a9 }
            r3 = r20
        L_0x034d:
            r7 = r25
            com.google.gson.JsonElement r0 = r2.a((java.lang.String) r7)     // Catch:{ all -> 0x035a }
            java.lang.String r9 = r0.i()     // Catch:{ all -> 0x035a }
            r16 = r4
            goto L_0x0363
        L_0x035a:
            r0 = move-exception
            r16 = r4
            r2 = 0
            boolean[] r4 = new boolean[r2]     // Catch:{ Exception -> 0x052d, all -> 0x04a7 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r4)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
        L_0x0363:
            if (r15 == 0) goto L_0x052d
            boolean r0 = r15.k()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            if (r0 != 0) goto L_0x052d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r0.<init>()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r0.append(r12)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r2 = "?"
            boolean r2 = r12.contains(r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            if (r2 == 0) goto L_0x037e
            java.lang.String r2 = "&"
            goto L_0x0380
        L_0x037e:
            java.lang.String r2 = "?"
        L_0x0380:
            r0.append(r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r2 = "ts="
            r0.append(r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r2 = 0
            boolean[] r4 = new boolean[r2]     // Catch:{ Exception -> 0x052d, all -> 0x04a7 }
            java.lang.String r4 = com.original.tase.utils.Utils.a((java.lang.String) r14, (boolean[]) r4)     // Catch:{ Exception -> 0x052d, all -> 0x04a7 }
            r0.append(r4)     // Catch:{ Exception -> 0x052d, all -> 0x04a7 }
            r0.append(r10)     // Catch:{ Exception -> 0x052d, all -> 0x04a7 }
            boolean[] r4 = new boolean[r2]     // Catch:{ Exception -> 0x052d, all -> 0x04a7 }
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r9, (boolean[]) r4)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r0.append(r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r2 = "&token="
            r0.append(r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r2 = r15.i()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r4 = 0
            boolean[] r9 = new boolean[r4]     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r9)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r0.append(r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r2 = "&mobile=0"
            r0.append(r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            boolean r2 = r3.isEmpty()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            if (r2 != 0) goto L_0x03db
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r2.<init>()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r2.append(r0)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r0 = "&option="
            r2.append(r0)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r4 = 0
            boolean[] r0 = new boolean[r4]     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r3, (boolean[]) r0)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r2.append(r0)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
        L_0x03db:
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x041c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x041c }
            r3.<init>()     // Catch:{ all -> 0x041c }
            r3.append(r0)     // Catch:{ all -> 0x041c }
            java.lang.String r4 = "&_"
            r3.append(r4)     // Catch:{ all -> 0x041c }
            java.net.URL r4 = new java.net.URL     // Catch:{ all -> 0x041c }
            r4.<init>(r0)     // Catch:{ all -> 0x041c }
            java.util.Map r0 = com.original.tase.utils.Utils.a((java.net.URL) r4)     // Catch:{ all -> 0x041c }
            int r0 = r1.a(r0)     // Catch:{ all -> 0x041c }
            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x041c }
            r4 = 0
            boolean[] r9 = new boolean[r4]     // Catch:{ all -> 0x041c }
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r9)     // Catch:{ all -> 0x041c }
            r3.append(r0)     // Catch:{ all -> 0x041c }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x041c }
            r3 = 1
            java.util.Map[] r4 = new java.util.Map[r3]     // Catch:{ all -> 0x041a }
            java.util.HashMap r9 = com.original.Constants.a()     // Catch:{ all -> 0x041a }
            r12 = 0
            r4[r12] = r9     // Catch:{ all -> 0x041a }
            java.lang.String r2 = r2.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r4)     // Catch:{ all -> 0x041a }
            goto L_0x0426
        L_0x041a:
            r0 = move-exception
            goto L_0x041e
        L_0x041c:
            r0 = move-exception
            r3 = 1
        L_0x041e:
            r2 = 0
            boolean[] r4 = new boolean[r2]     // Catch:{ Exception -> 0x052d, all -> 0x04a7 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r4)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r2 = r20
        L_0x0426:
            com.google.gson.JsonParser r0 = new com.google.gson.JsonParser     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            r0.<init>()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.lang.String r2 = "data"
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r2)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            com.google.gson.JsonArray r0 = r0.e()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
        L_0x0441:
            boolean r0 = r2.hasNext()     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            if (r0 == 0) goto L_0x052d
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x049c }
            com.google.gson.JsonElement r0 = (com.google.gson.JsonElement) r0     // Catch:{ all -> 0x049c }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ all -> 0x049c }
            java.lang.String r4 = "file"
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r4)     // Catch:{ all -> 0x049c }
            java.lang.String r0 = r0.i()     // Catch:{ all -> 0x049c }
            boolean r4 = com.original.tase.helper.GoogleVideoHelper.k(r0)     // Catch:{ all -> 0x049c }
            com.original.tase.model.media.MediaSource r9 = new com.original.tase.model.media.MediaSource     // Catch:{ all -> 0x049c }
            if (r13 == 0) goto L_0x0479
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x049c }
            r12.<init>()     // Catch:{ all -> 0x049c }
            java.lang.String r15 = r27.a()     // Catch:{ all -> 0x049c }
            r12.append(r15)     // Catch:{ all -> 0x049c }
            java.lang.String r15 = " (CAM)"
            r12.append(r15)     // Catch:{ all -> 0x049c }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x049c }
            goto L_0x047d
        L_0x0479:
            java.lang.String r12 = r27.a()     // Catch:{ all -> 0x049c }
        L_0x047d:
            if (r4 == 0) goto L_0x0482
            java.lang.String r15 = "GoogleVideo"
            goto L_0x0484
        L_0x0482:
            java.lang.String r15 = "CDN"
        L_0x0484:
            r3 = 0
            r9.<init>(r12, r15, r3)     // Catch:{ all -> 0x049a }
            r9.setStreamLink(r0)     // Catch:{ all -> 0x049c }
            if (r4 == 0) goto L_0x0492
            java.lang.String r0 = com.original.tase.helper.GoogleVideoHelper.h(r0)     // Catch:{ all -> 0x049c }
            goto L_0x0493
        L_0x0492:
            r0 = r8
        L_0x0493:
            r9.setQuality((java.lang.String) r0)     // Catch:{ all -> 0x049c }
            r11.onNext(r9)     // Catch:{ all -> 0x049c }
            goto L_0x04a3
        L_0x049a:
            r0 = move-exception
            goto L_0x049e
        L_0x049c:
            r0 = move-exception
            r3 = 0
        L_0x049e:
            boolean[] r4 = new boolean[r3]     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r4)     // Catch:{ Exception -> 0x052d, all -> 0x04a5 }
        L_0x04a3:
            r3 = 1
            goto L_0x0441
        L_0x04a5:
            r0 = move-exception
            goto L_0x04be
        L_0x04a7:
            r0 = move-exception
            goto L_0x04bf
        L_0x04a9:
            r0 = move-exception
            goto L_0x04ba
        L_0x04ab:
            r0 = move-exception
            goto L_0x04b8
        L_0x04ad:
            r26 = r2
        L_0x04af:
            r16 = r4
            r7 = r25
            goto L_0x052d
        L_0x04b5:
            r0 = move-exception
            r11 = r28
        L_0x04b8:
            r26 = r2
        L_0x04ba:
            r16 = r4
            r7 = r25
        L_0x04be:
            r2 = 0
        L_0x04bf:
            boolean[] r3 = new boolean[r2]     // Catch:{ all -> 0x04c9 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r3)     // Catch:{ all -> 0x04c6 }
            goto L_0x052d
        L_0x04c6:
            r0 = move-exception
            goto L_0x0527
        L_0x04c9:
            r0 = move-exception
            goto L_0x0528
        L_0x04cc:
            r0 = move-exception
            r11 = r28
            r26 = r2
            r16 = r4
            r21 = r7
        L_0x04d5:
            r7 = r25
            goto L_0x0527
        L_0x04d9:
            r0 = move-exception
            r11 = r28
            r21 = r7
            r26 = r16
            r7 = r25
            goto L_0x0525
        L_0x04e3:
            r0 = move-exception
            r11 = r28
            r21 = r7
            r7 = r8
            goto L_0x050f
        L_0x04ea:
            r11 = r28
            r21 = r7
            r7 = r8
            r23 = r15
            r26 = r16
            r16 = r4
            goto L_0x052d
        L_0x04f6:
            r0 = move-exception
            r11 = r28
            r21 = r7
            r7 = r8
            goto L_0x050d
        L_0x04fd:
            r0 = move-exception
            r11 = r28
            goto L_0x0506
        L_0x0501:
            r0 = move-exception
            r11 = r28
            r20 = r2
        L_0x0506:
            r21 = r7
            r7 = r8
            r22 = r9
            r24 = r12
        L_0x050d:
            r23 = r15
        L_0x050f:
            r26 = r16
            goto L_0x0525
        L_0x0512:
            return
        L_0x0513:
            r0 = move-exception
            r20 = r2
            r21 = r7
            r7 = r8
            r22 = r9
            r19 = r11
            r24 = r12
            r23 = r15
            r26 = r16
            r11 = r28
        L_0x0525:
            r16 = r4
        L_0x0527:
            r2 = 0
        L_0x0528:
            boolean[] r3 = new boolean[r2]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r3)
        L_0x052d:
            r3 = r29
            r8 = r7
            r4 = r16
            r11 = r19
            r2 = r20
            r7 = r21
            r9 = r22
            r15 = r23
            r12 = r24
            r16 = r26
            goto L_0x00c5
        L_0x0542:
            r11 = r28
            r20 = r2
            r3 = r29
            r4 = r17
            r11 = r18
            goto L_0x0091
        L_0x054e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.PLockerSK.a(io.reactivex.ObservableEmitter, java.lang.String, com.movie.data.model.MovieInfo, java.lang.String):void");
    }

    private int a(Map<String, String> map) {
        try {
            int j = j("XT4Az3r00D");
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getValue();
                String str2 = "XT4Az3r00D" + ((String) next.getKey());
                int i = 0;
                int i2 = 0;
                while (i < Math.max(str2.length(), str.length())) {
                    int codePointAt = i < str.length() ? i2 + Character.codePointAt(str, i) : i2 + 0;
                    i2 = i < str2.length() ? codePointAt + Character.codePointAt(str2, i) : codePointAt + 0;
                    i++;
                }
                j += j(Integer.toHexString(i2));
            }
            return j;
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return -1;
        }
    }

    private String a(MovieInfo movieInfo, String str) {
        movieInfo.session.isEmpty();
        String e = TitleHelper.e(movieInfo.name.replace("Marvel's ", "").replace("DC's ", ""));
        String str2 = this.c + "/search?keyword=" + com.original.tase.utils.Utils.a(e, new boolean[0]);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0])).g("div.item").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            Element h = element.h("div.inner[data-tip]");
            if (h != null) {
                String a2 = Regex.a(HttpHelper.e().b(this.c + "/" + h.b("data-tip"), str2), "<span>\\s*(\\d{4})\\s*</span>", 1, true);
                if (!(movieInfo.year.equals(a2) ^ true) || !(!movieInfo.sessionYear.equals(a2))) {
                }
            }
            element.h("div.quality").G();
            String b = element.h("a.name[href]").b("href");
            if (b.startsWith("/")) {
                return this.c + "" + b;
            } else if (b.startsWith(UriUtil.HTTP_SCHEME)) {
                return b;
            } else {
                return this.c + "/" + b;
            }
        }
        return "";
    }
}
