package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import okhttp3.internal.cache.DiskLruCache;

public class DDLValley extends BaseProvider {
    private HashMap<String, String> c = new HashMap<>();
    private String d = Utils.getProvider(88);

    public DDLValley() {
        this.c.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        this.c.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.9,en;q=0.8");
        this.c.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
    }

    public String a() {
        return "DDLValley";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo, "-1", "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo, movieInfo.session, movieInfo.eps);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:169:0x03fd A[Catch:{ Exception -> 0x0595 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r24, com.movie.data.model.MovieInfo r25, java.lang.String r26, java.lang.String r27) {
        /*
            r23 = this;
            r7 = r23
            r1 = r25
            java.lang.String r8 = "1080p"
            java.lang.String r2 = "title"
            java.lang.Integer r0 = r25.getType()
            int r0 = r0.intValue()
            r9 = 1
            r10 = 0
            if (r0 != r9) goto L_0x0016
            r11 = 1
            goto L_0x0017
        L_0x0016:
            r11 = 0
        L_0x0017:
            com.original.tase.helper.DirectoryIndexHelper r12 = new com.original.tase.helper.DirectoryIndexHelper
            r12.<init>()
            java.lang.String r13 = ""
            if (r11 == 0) goto L_0x0022
            r3 = r13
            goto L_0x004c
        L_0x0022:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "S"
            r0.append(r3)
            int r3 = java.lang.Integer.parseInt(r26)
            java.lang.String r3 = com.original.tase.utils.Utils.a((int) r3)
            r0.append(r3)
            java.lang.String r3 = "E"
            r0.append(r3)
            int r3 = java.lang.Integer.parseInt(r27)
            java.lang.String r3 = com.original.tase.utils.Utils.a((int) r3)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r3 = r0
        L_0x004c:
            java.lang.String r14 = r25.getName()
            if (r11 == 0) goto L_0x005c
            java.lang.Integer r0 = r25.getYear()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r15 = r0
            goto L_0x005d
        L_0x005c:
            r15 = r3
        L_0x005d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r4 = "'"
            java.lang.String r4 = r14.replace(r4, r13)
            r0.append(r4)
            java.lang.String r6 = " "
            r0.append(r6)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            java.lang.String r4 = "(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)"
            java.lang.String r0 = r0.replaceAll(r4, r6)
            java.lang.String r4 = "  "
            java.lang.String r0 = r0.replace(r4, r6)
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r5 = r7.d
            java.util.Map[] r9 = new java.util.Map[r10]
            r4.a((java.lang.String) r5, (java.util.Map<java.lang.String, java.lang.String>[]) r9)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r7.d
            r4.append(r5)
            java.lang.String r5 = "/?s="
            r4.append(r5)
            boolean[] r5 = new boolean[r10]
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r5)
            r4.append(r0)
            java.lang.String r4 = r4.toString()
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r9 = r7.d
            r5.append(r9)
            java.lang.String r9 = "/"
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            r18 = r12
            r10 = 1
            java.util.Map[] r12 = new java.util.Map[r10]
            java.util.HashMap<java.lang.String, java.lang.String> r10 = r7.c
            r17 = 0
            r12[r17] = r10
            java.lang.String r0 = r0.b(r4, r5, r12)
            java.lang.String r5 = "Attention Required! | Cloudflare"
            boolean r5 = r0.contains(r5)
            if (r5 == 0) goto L_0x00e4
            java.lang.String r5 = "Need Verify Recaptcha"
            com.original.tase.Logger.a((java.lang.String) r5, (java.lang.String) r4)
            java.lang.String r5 = com.utils.Getlink.Provider.BaseProvider.h(r4)
            com.utils.Utils.a((java.lang.String) r4, (java.lang.String) r5)
        L_0x00e4:
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r10 = "div.pb.fl"
            org.jsoup.nodes.Element r0 = r0.h(r10)
            java.lang.String r12 = "\\</[uibp]\\>"
            r19 = r8
            java.lang.String r8 = "\\<[uibp]\\>"
            r26 = r14
            java.lang.String r14 = "href"
            r27 = r6
            if (r0 == 0) goto L_0x0279
            java.lang.String r6 = "h2"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r6)
            java.util.Iterator r6 = r0.iterator()
        L_0x010b:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0279
            java.lang.Object r0 = r6.next()     // Catch:{ Exception -> 0x0264 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ Exception -> 0x0264 }
            r20 = r6
            java.lang.String r6 = "a[href]"
            org.jsoup.nodes.Element r0 = r0.h(r6)     // Catch:{ Exception -> 0x0260 }
            if (r0 == 0) goto L_0x025b
            java.lang.String r6 = r0.b((java.lang.String) r14)     // Catch:{ Exception -> 0x0260 }
            boolean r21 = r0.d((java.lang.String) r2)     // Catch:{ Exception -> 0x0260 }
            if (r21 == 0) goto L_0x0130
            java.lang.String r0 = r0.b((java.lang.String) r2)     // Catch:{ Exception -> 0x0260 }
            goto L_0x0134
        L_0x0130:
            java.lang.String r0 = r0.G()     // Catch:{ Exception -> 0x0260 }
        L_0x0134:
            java.lang.String r0 = r0.replaceAll(r8, r13)     // Catch:{ Exception -> 0x0260 }
            java.lang.String r0 = r0.replaceAll(r12, r13)     // Catch:{ Exception -> 0x0260 }
            r21 = r2
            java.lang.String r2 = r0.toLowerCase()     // Catch:{ Exception -> 0x0259 }
            if (r11 == 0) goto L_0x01a7
            r22 = r14
            java.lang.String r14 = " cam"
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = "cam "
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = "hdts "
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = " hdts"
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = " ts "
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = " telesync"
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = "telesync "
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = "hdtc "
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = " hdtc"
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = " tc "
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = " telecine"
            boolean r14 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r14 != 0) goto L_0x0271
            java.lang.String r14 = "telecine "
            boolean r2 = r2.contains(r14)     // Catch:{ Exception -> 0x0257 }
            if (r2 != 0) goto L_0x0271
            goto L_0x01a9
        L_0x01a7:
            r22 = r14
        L_0x01a9:
            java.lang.String r2 = "//"
            boolean r2 = r6.startsWith(r2)     // Catch:{ Exception -> 0x0257 }
            if (r2 == 0) goto L_0x01c3
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0257 }
            r2.<init>()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = "http:"
            r2.append(r14)     // Catch:{ Exception -> 0x0257 }
            r2.append(r6)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r6 = r2.toString()     // Catch:{ Exception -> 0x0257 }
            goto L_0x01da
        L_0x01c3:
            boolean r2 = r6.startsWith(r9)     // Catch:{ Exception -> 0x0257 }
            if (r2 == 0) goto L_0x01da
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0257 }
            r2.<init>()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = r7.d     // Catch:{ Exception -> 0x0257 }
            r2.append(r14)     // Catch:{ Exception -> 0x0257 }
            r2.append(r6)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r6 = r2.toString()     // Catch:{ Exception -> 0x0257 }
        L_0x01da:
            java.lang.String r2 = r0.toLowerCase()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = "goto"
            boolean r2 = r2.startsWith(r14)     // Catch:{ Exception -> 0x0257 }
            if (r2 == 0) goto L_0x01f3
            r2 = 4
            int r14 = r0.length()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r0 = r0.substring(r2, r14)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x0257 }
        L_0x01f3:
            if (r11 == 0) goto L_0x0220
            java.lang.String r0 = com.original.tase.helper.TitleHelper.f(r0)     // Catch:{ Exception -> 0x0257 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0257 }
            r2.<init>()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = r25.getName()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = com.original.tase.helper.TitleHelper.e(r14)     // Catch:{ Exception -> 0x0257 }
            r2.append(r14)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = r1.year     // Catch:{ Exception -> 0x0257 }
            r2.append(r14)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r2 = com.original.tase.helper.TitleHelper.f(r2)     // Catch:{ Exception -> 0x0257 }
            boolean r0 = r0.startsWith(r2)     // Catch:{ Exception -> 0x0257 }
            if (r0 == 0) goto L_0x0271
            r5.add(r6)     // Catch:{ Exception -> 0x0257 }
            goto L_0x0271
        L_0x0220:
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r2 = r1.year     // Catch:{ Exception -> 0x0257 }
            java.lang.String r0 = r0.replace(r2, r13)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r0 = com.original.tase.helper.TitleHelper.a(r0, r13)     // Catch:{ Exception -> 0x0257 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0257 }
            r2.<init>()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = r25.getName()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = r14.toLowerCase()     // Catch:{ Exception -> 0x0257 }
            r2.append(r14)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r14 = r3.toLowerCase()     // Catch:{ Exception -> 0x0257 }
            r2.append(r14)     // Catch:{ Exception -> 0x0257 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0257 }
            java.lang.String r2 = com.original.tase.helper.TitleHelper.a(r2, r13)     // Catch:{ Exception -> 0x0257 }
            boolean r0 = r0.startsWith(r2)     // Catch:{ Exception -> 0x0257 }
            if (r0 == 0) goto L_0x0271
            r5.add(r6)     // Catch:{ Exception -> 0x0257 }
            goto L_0x0271
        L_0x0257:
            r0 = move-exception
            goto L_0x026b
        L_0x0259:
            r0 = move-exception
            goto L_0x0269
        L_0x025b:
            r21 = r2
            r22 = r14
            goto L_0x0271
        L_0x0260:
            r0 = move-exception
            r21 = r2
            goto L_0x0269
        L_0x0264:
            r0 = move-exception
            r21 = r2
            r20 = r6
        L_0x0269:
            r22 = r14
        L_0x026b:
            r2 = 0
            boolean[] r6 = new boolean[r2]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r6)
        L_0x0271:
            r6 = r20
            r2 = r21
            r14 = r22
            goto L_0x010b
        L_0x0279:
            r22 = r14
            boolean r0 = r5.isEmpty()
            if (r0 == 0) goto L_0x028b
            java.lang.String r0 = r1.name
            java.lang.String r1 = r1.year
            java.lang.String r2 = r7.d
            java.util.List r5 = com.original.tase.search.SearchHelper.c(r0, r1, r15, r2, r13)
        L_0x028b:
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x05b5
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.Iterator r2 = r5.iterator()
        L_0x029a:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0359
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0348 }
            r5 = 1
            java.util.Map[] r6 = new java.util.Map[r5]     // Catch:{ Exception -> 0x0348 }
            java.util.HashMap<java.lang.String, java.lang.String> r5 = r7.c     // Catch:{ Exception -> 0x0348 }
            r14 = 0
            r6[r14] = r5     // Catch:{ Exception -> 0x0348 }
            java.lang.String r0 = r3.b(r0, r4, r6)     // Catch:{ Exception -> 0x0348 }
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)     // Catch:{ Exception -> 0x0348 }
            java.lang.String r3 = "div.cont.cl"
            org.jsoup.nodes.Element r3 = r0.h(r3)     // Catch:{ Exception -> 0x0348 }
            if (r3 == 0) goto L_0x0343
            org.jsoup.nodes.Element r0 = r0.h(r10)     // Catch:{ Exception -> 0x0348 }
            if (r0 == 0) goto L_0x0343
            java.lang.String r5 = "h1"
            org.jsoup.nodes.Element r0 = r0.h(r5)     // Catch:{ Exception -> 0x0348 }
            if (r0 == 0) goto L_0x0343
            java.lang.String r0 = r0.G()     // Catch:{ Exception -> 0x0348 }
            java.lang.String r0 = r0.replaceAll(r8, r13)     // Catch:{ Exception -> 0x0348 }
            java.lang.String r5 = r0.replaceAll(r12, r13)     // Catch:{ Exception -> 0x0348 }
            org.jsoup.select.Elements r0 = r3.t()     // Catch:{ Exception -> 0x0348 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x0348 }
        L_0x02e4:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0348 }
            if (r0 == 0) goto L_0x0343
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0348 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ Exception -> 0x0348 }
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x0331 }
            java.lang.String r14 = "span"
            boolean r6 = r6.contains(r14)     // Catch:{ Exception -> 0x0331 }
            if (r6 == 0) goto L_0x032c
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x0331 }
            java.lang.String r14 = "info2"
            boolean r6 = r6.contains(r14)     // Catch:{ Exception -> 0x0331 }
            if (r6 == 0) goto L_0x032c
            java.lang.String r6 = "a"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r6)     // Catch:{ Exception -> 0x0331 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x0331 }
        L_0x0312:
            boolean r6 = r0.hasNext()     // Catch:{ Exception -> 0x0331 }
            if (r6 == 0) goto L_0x032c
            java.lang.Object r6 = r0.next()     // Catch:{ Exception -> 0x0331 }
            org.jsoup.nodes.Element r6 = (org.jsoup.nodes.Element) r6     // Catch:{ Exception -> 0x0331 }
            r14 = r22
            java.lang.String r6 = r6.b((java.lang.String) r14)     // Catch:{ Exception -> 0x032a }
            r1.put(r6, r5)     // Catch:{ Exception -> 0x032a }
            r22 = r14
            goto L_0x0312
        L_0x032a:
            r0 = move-exception
            goto L_0x0334
        L_0x032c:
            r14 = r22
            r25 = r2
            goto L_0x033c
        L_0x0331:
            r0 = move-exception
            r14 = r22
        L_0x0334:
            r25 = r2
            r6 = 0
            boolean[] r2 = new boolean[r6]     // Catch:{ Exception -> 0x0341 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)     // Catch:{ Exception -> 0x0341 }
        L_0x033c:
            r2 = r25
            r22 = r14
            goto L_0x02e4
        L_0x0341:
            r0 = move-exception
            goto L_0x034d
        L_0x0343:
            r25 = r2
            r14 = r22
            goto L_0x0353
        L_0x0348:
            r0 = move-exception
            r25 = r2
            r14 = r22
        L_0x034d:
            r2 = 0
            boolean[] r3 = new boolean[r2]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r3)
        L_0x0353:
            r2 = r25
            r22 = r14
            goto L_0x029a
        L_0x0359:
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x05b5
            java.util.Set r0 = r1.entrySet()
            java.util.Iterator r8 = r0.iterator()
        L_0x0367:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x05b5
            java.lang.Object r0 = r8.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()     // Catch:{ Exception -> 0x05a0 }
            r3 = r1
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x05a0 }
            java.lang.Object r0 = r0.getValue()     // Catch:{ Exception -> 0x05a0 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x05a0 }
            java.lang.String r1 = ".7z"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x05a0 }
            if (r1 != 0) goto L_0x0598
            java.lang.String r1 = ".rar"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x05a0 }
            if (r1 != 0) goto L_0x0598
            java.lang.String r1 = ".zip"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x05a0 }
            if (r1 != 0) goto L_0x0598
            java.lang.String r1 = ".iso"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x05a0 }
            if (r1 != 0) goto L_0x0598
            java.lang.String r1 = ".avi"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x05a0 }
            if (r1 != 0) goto L_0x0598
            java.lang.String r1 = ".flv"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x05a0 }
            if (r1 != 0) goto L_0x0598
            java.lang.String r1 = "imdb."
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x05a0 }
            if (r1 != 0) goto L_0x0598
            if (r11 != 0) goto L_0x03e7
            boolean r1 = r3.contains(r9)     // Catch:{ Exception -> 0x05a0 }
            if (r1 == 0) goto L_0x03e7
            java.lang.String[] r1 = r3.split(r9)     // Catch:{ Exception -> 0x05a0 }
            int r2 = r1.length     // Catch:{ Exception -> 0x05a0 }
            if (r2 <= 0) goto L_0x03e7
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05a0 }
            r2.<init>()     // Catch:{ Exception -> 0x05a0 }
            java.lang.String r4 = "(720p|1080p)"
            java.lang.String r0 = r0.replaceAll(r4, r13)     // Catch:{ Exception -> 0x05a0 }
            r2.append(r0)     // Catch:{ Exception -> 0x05a0 }
            r6 = r27
            r2.append(r6)     // Catch:{ Exception -> 0x0595 }
            int r0 = r1.length     // Catch:{ Exception -> 0x0595 }
            r4 = 1
            int r0 = r0 - r4
            r0 = r1[r0]     // Catch:{ Exception -> 0x0595 }
            r2.append(r0)     // Catch:{ Exception -> 0x0595 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0595 }
            goto L_0x03e9
        L_0x03e7:
            r6 = r27
        L_0x03e9:
            java.lang.String r1 = com.original.tase.helper.TitleHelper.f(r26)     // Catch:{ Exception -> 0x0595 }
            java.lang.String r2 = "(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)"
            java.lang.String r2 = r0.replaceAll(r2, r13)     // Catch:{ Exception -> 0x0595 }
            java.lang.String r2 = com.original.tase.helper.TitleHelper.f(r2)     // Catch:{ Exception -> 0x0595 }
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x0595 }
            if (r1 == 0) goto L_0x0593
            java.lang.String r1 = "[\\.|\\(|\\[|\\s]([2-9]0\\d{2}|1[5-9]\\d{2})[\\.|\\)|\\]|\\s]"
            r10 = 1
            java.util.ArrayList r1 = com.original.tase.utils.Regex.b(r0, r1, r10)     // Catch:{ Exception -> 0x0595 }
            r2 = 0
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x0595 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ Exception -> 0x0595 }
            if (r11 != 0) goto L_0x042b
            java.lang.String r4 = "[\\.|\\(|\\[|\\s](S\\d*E\\d*)[\\.|\\)|\\]|\\s]"
            java.util.ArrayList r4 = com.original.tase.utils.Regex.b(r0, r4, r10)     // Catch:{ Exception -> 0x0595 }
            java.lang.Object r4 = r4.get(r2)     // Catch:{ Exception -> 0x0595 }
            java.util.Collection r4 = (java.util.Collection) r4     // Catch:{ Exception -> 0x0595 }
            r1.addAll(r4)     // Catch:{ Exception -> 0x0595 }
            java.lang.String r4 = "[\\.|\\(|\\[|\\s](S\\d*)[\\.|\\)|\\]|\\s]"
            java.util.ArrayList r4 = com.original.tase.utils.Regex.b(r0, r4, r10)     // Catch:{ Exception -> 0x0595 }
            java.lang.Object r4 = r4.get(r2)     // Catch:{ Exception -> 0x0595 }
            java.util.Collection r4 = (java.util.Collection) r4     // Catch:{ Exception -> 0x0595 }
            r1.addAll(r4)     // Catch:{ Exception -> 0x0595 }
        L_0x042b:
            int r2 = r1.size()     // Catch:{ Exception -> 0x0595 }
            if (r2 <= 0) goto L_0x0593
            java.util.Iterator r1 = r1.iterator()     // Catch:{ Exception -> 0x0595 }
        L_0x0435:
            boolean r2 = r1.hasNext()     // Catch:{ Exception -> 0x0595 }
            if (r2 == 0) goto L_0x044d
            java.lang.Object r2 = r1.next()     // Catch:{ Exception -> 0x0595 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0595 }
            java.lang.String r2 = r2.toUpperCase()     // Catch:{ Exception -> 0x0595 }
            boolean r2 = r2.equals(r15)     // Catch:{ Exception -> 0x0595 }
            if (r2 == 0) goto L_0x0435
            r1 = 1
            goto L_0x044e
        L_0x044d:
            r1 = 0
        L_0x044e:
            if (r1 == 0) goto L_0x0593
            java.lang.String r0 = r0.toUpperCase()     // Catch:{ Exception -> 0x0595 }
            java.lang.String r1 = "(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)"
            java.lang.String r0 = r0.replaceAll(r1, r13)     // Catch:{ Exception -> 0x0595 }
            java.lang.String r1 = "\\.|\\(|\\)|\\[|\\]|\\s|\\-"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x0595 }
            int r1 = r0.length     // Catch:{ Exception -> 0x0595 }
            java.lang.String r2 = "HQ"
            r5 = r2
            r4 = 0
        L_0x0465:
            if (r4 >= r1) goto L_0x0546
            r12 = r0[r4]     // Catch:{ Exception -> 0x053e }
            java.lang.String r12 = r12.toLowerCase()     // Catch:{ Exception -> 0x053e }
            java.lang.String r14 = "subs"
            boolean r14 = r12.endsWith(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "sub"
            boolean r14 = r12.endsWith(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "dubbed"
            boolean r14 = r12.endsWith(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "dub"
            boolean r14 = r12.endsWith(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "dvdscr"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "r5"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "r6"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "camrip"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "tsrip"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "hdcam"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "hdts"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "dvdcam"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "dvdts"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "cam"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "telesync"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 != 0) goto L_0x053a
            java.lang.String r14 = "ts"
            boolean r14 = r12.contains(r14)     // Catch:{ Exception -> 0x053e }
            if (r14 == 0) goto L_0x04ee
            goto L_0x053a
        L_0x04ee:
            r14 = r19
            boolean r16 = r12.contains(r14)     // Catch:{ Exception -> 0x058d }
            if (r16 != 0) goto L_0x0532
            java.lang.String r10 = "1080"
            boolean r10 = r12.equals(r10)     // Catch:{ Exception -> 0x058d }
            if (r10 == 0) goto L_0x04ff
            goto L_0x0532
        L_0x04ff:
            java.lang.String r10 = "720p"
            boolean r10 = r12.contains(r10)     // Catch:{ Exception -> 0x058d }
            if (r10 != 0) goto L_0x052f
            java.lang.String r10 = "720"
            boolean r10 = r12.equals(r10)     // Catch:{ Exception -> 0x058d }
            if (r10 != 0) goto L_0x052f
            java.lang.String r10 = "brrip"
            boolean r10 = r12.contains(r10)     // Catch:{ Exception -> 0x058d }
            if (r10 != 0) goto L_0x052f
            java.lang.String r10 = "bdrip"
            boolean r10 = r12.contains(r10)     // Catch:{ Exception -> 0x058d }
            if (r10 != 0) goto L_0x052f
            java.lang.String r10 = "hdrip"
            boolean r10 = r12.contains(r10)     // Catch:{ Exception -> 0x058d }
            if (r10 != 0) goto L_0x052f
            java.lang.String r10 = "web-dl"
            boolean r10 = r12.contains(r10)     // Catch:{ Exception -> 0x058d }
            if (r10 == 0) goto L_0x0533
        L_0x052f:
            java.lang.String r5 = "HD"
            goto L_0x0533
        L_0x0532:
            r5 = r14
        L_0x0533:
            int r4 = r4 + 1
            r19 = r14
            r10 = 1
            goto L_0x0465
        L_0x053a:
            r14 = r19
            r0 = 1
            goto L_0x0549
        L_0x053e:
            r0 = move-exception
            r14 = r19
        L_0x0541:
            r12 = r6
            r10 = r18
            goto L_0x05a7
        L_0x0546:
            r14 = r19
            r0 = 0
        L_0x0549:
            if (r0 != 0) goto L_0x058f
            java.lang.String r0 = r23.a()     // Catch:{ Exception -> 0x058d }
            if (r11 == 0) goto L_0x0558
            r10 = r18
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r1 = r10.a(r3)     // Catch:{ Exception -> 0x058a }
            goto L_0x055e
        L_0x0558:
            r10 = r18
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r1 = r10.b(r3)     // Catch:{ Exception -> 0x058a }
        L_0x055e:
            if (r1 == 0) goto L_0x0576
            java.lang.String r0 = r1.c()     // Catch:{ Exception -> 0x058a }
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x058a }
            if (r0 != 0) goto L_0x056e
            java.lang.String r5 = r1.c()     // Catch:{ Exception -> 0x058a }
        L_0x056e:
            java.lang.String r0 = r1.b()     // Catch:{ Exception -> 0x058a }
            java.lang.String r0 = r7.f(r0)     // Catch:{ Exception -> 0x058a }
        L_0x0576:
            r4 = r5
            r1 = 0
            r5 = r0
            boolean[] r0 = new boolean[r1]     // Catch:{ Exception -> 0x0587 }
            r1 = r23
            r2 = r24
            r12 = r6
            r6 = r0
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0585 }
            goto L_0x059e
        L_0x0585:
            r0 = move-exception
            goto L_0x05a7
        L_0x0587:
            r0 = move-exception
            r12 = r6
            goto L_0x05a8
        L_0x058a:
            r0 = move-exception
            r12 = r6
            goto L_0x05a7
        L_0x058d:
            r0 = move-exception
            goto L_0x0541
        L_0x058f:
            r12 = r6
            r10 = r18
            goto L_0x059e
        L_0x0593:
            r12 = r6
            goto L_0x059a
        L_0x0595:
            r0 = move-exception
            r12 = r6
            goto L_0x05a3
        L_0x0598:
            r12 = r27
        L_0x059a:
            r10 = r18
            r14 = r19
        L_0x059e:
            r1 = 0
            goto L_0x05ad
        L_0x05a0:
            r0 = move-exception
            r12 = r27
        L_0x05a3:
            r10 = r18
            r14 = r19
        L_0x05a7:
            r1 = 0
        L_0x05a8:
            boolean[] r2 = new boolean[r1]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)
        L_0x05ad:
            r18 = r10
            r27 = r12
            r19 = r14
            goto L_0x0367
        L_0x05b5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.DDLValley.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo, java.lang.String, java.lang.String):void");
    }
}
