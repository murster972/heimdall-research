package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class BestTvFix extends BaseProvider {
    private String c = Utils.getProvider(57);
    private String d = "HD";
    private String e = "";
    private HashMap f = new HashMap();

    public String a() {
        return "BestTvFix";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        String str2;
        if (!(movieInfo.getType().intValue() == 1)) {
            str = str + "/season/" + movieInfo.session + "/episode/" + movieInfo.eps;
        }
        this.f.remove("Origin");
        this.f.put("Referer", this.e);
        Iterator it2 = Regex.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{this.f}), "((?:embeds))\\[([0-9A-Za-z]+)\\]\\s*=\\s*[']([^'\"]+[^'\"]*)[']", 3, true).get(2).iterator();
        while (it2.hasNext()) {
            String str3 = (String) it2.next();
            try {
                str2 = new String(Base64.decode(str3, 0), "UTF-8");
            } catch (Throwable unused) {
                str2 = str3;
            }
            String a2 = Regex.a(str2, "(?:SRC|src)\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
            boolean k = GoogleVideoHelper.k(a2);
            if (k) {
                HashMap hashMap = new HashMap();
                hashMap.put("User-Agent", Constants.f5838a);
                MediaSource mediaSource = new MediaSource(a(), "GoogleVideo", false);
                mediaSource.setPlayHeader(hashMap);
                mediaSource.setStreamLink(a2);
                mediaSource.setQuality(k ? GoogleVideoHelper.h(a2) : "HD");
                observableEmitter.onNext(mediaSource);
            } else {
                a(observableEmitter, a2, this.d, false);
            }
        }
    }

    private String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        this.f.put("Host", this.c.replace("https://", "").replace("http://", ""));
        this.f.put("Origin", this.c);
        HashMap hashMap = this.f;
        hashMap.put("Referer", this.c + "/");
        this.f.put("User-Agent", Constants.f5838a);
        String replace = com.original.tase.utils.Utils.a(movieInfo.getName(), new boolean[0]).replace("%20", "+");
        this.e = this.c + "/search";
        HttpHelper e2 = HttpHelper.e();
        String str = this.e;
        String a2 = e2.a(str, "menu=search&query=" + replace, false, (Map<String, String>[]) new Map[]{this.f});
        Iterator it2 = Jsoup.b(a2).g("ul.ch-grid").b("h5.left").iterator();
        if (!z) {
            it2 = Jsoup.b(a2).g("ul.ch-grid").b("h5").iterator();
        }
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            String G = h.G();
            if (z) {
                if (G.equalsIgnoreCase(movieInfo.name + " (" + movieInfo.year + ")")) {
                    return b;
                }
            } else if (G.equalsIgnoreCase(movieInfo.name)) {
                return b;
            }
        }
        return "";
    }
}
