package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class OnePutlocker extends BaseProvider {
    private String c = (Utils.getProvider(93) + "/");

    public String a() {
        return "OnePutlocker";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    private String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        HttpHelper e = HttpHelper.e();
        String str = this.c;
        e.c(str, str);
        HashMap hashMap = new HashMap();
        hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        hashMap.put("Referer", this.c);
        String lowerCase = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+").toLowerCase();
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "search-putlocker-movies/" + lowerCase + ".html", (Map<String, String>[]) new Map[]{hashMap})).g("div[class=item]").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            String a2 = Regex.a(h.toString(), "<i>(.*)</i>", 1);
            if (!z) {
                if (!a2.equalsIgnoreCase(movieInfo.name + ": season " + movieInfo.session) || !h.toString().contains(movieInfo.sessionYear)) {
                    if (a2.equalsIgnoreCase(movieInfo.name + " (" + movieInfo.sessionYear + "): season " + movieInfo.session)) {
                    }
                }
                return b;
            } else if (a2.startsWith(movieInfo.name) && h.toString().contains(movieInfo.year)) {
                return b;
            }
        }
        return "";
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        String str2;
        String str3;
        boolean z;
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", str);
        hashMap.put("user-agent", Constants.f5838a);
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap});
        if (!(movieInfo.getType().intValue() == 1)) {
            Iterator it2 = Jsoup.b(a2).g("div[id=cont_player]").b("div[class=section-box]").b("a").iterator();
            while (true) {
                if (!it2.hasNext()) {
                    str3 = a2;
                    z = false;
                    break;
                }
                Element element = (Element) it2.next();
                String b = element.b("href");
                if (movieInfo.eps.equals(element.G())) {
                    str3 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[]{hashMap});
                    z = true;
                    break;
                }
            }
            if (z) {
                a2 = str3;
            } else {
                return;
            }
        }
        Iterator it3 = Jsoup.b(a2).g("div[id=cont_player]").b("p[class=server_play]").iterator();
        ArrayList arrayList = new ArrayList();
        String str4 = a2;
        int i = 0;
        while (it3.hasNext()) {
            String b2 = ((Element) it3.next()).h("a").b("href");
            if (!arrayList.contains(b2)) {
                arrayList.add(b2);
                if (!observableEmitter.isDisposed()) {
                    if (i != 0) {
                        str4 = HttpHelper.e().a(b2, (Map<String, String>[]) new Map[]{hashMap});
                    }
                    String a3 = Regex.a(Jsoup.b(str4).g("div[class=mediaplayer]").toString(), "Base64.decode\\(([^'']+[^'\"])\\)", 1);
                    try {
                        str2 = new String(Base64.decode(a3, 8), "UTF-8");
                    } catch (Exception e) {
                        Logger.a((Throwable) e, new boolean[0]);
                        try {
                            str2 = new String(Base64.decode(a3, 8));
                        } catch (Exception e2) {
                            Logger.a((Throwable) e2, new boolean[0]);
                            str2 = "";
                        }
                    }
                    if (!str2.isEmpty()) {
                        String a4 = Regex.a(str2, "<iframe[^>]+src=['\"]([^'\"]+[^'\"])[^'\"]", 1, 2);
                        MediaSource mediaSource = new MediaSource(a(), "HD", false);
                        mediaSource.setStreamLink(a4);
                        mediaSource.setQuality("HD");
                        observableEmitter.onNext(mediaSource);
                    }
                } else {
                    return;
                }
            }
            i++;
            if (i > 10) {
                return;
            }
        }
    }
}
