package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Fmovies extends BaseProvider {
    private String c = Utils.getProvider(48);

    public String a() {
        return "Fmovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        HashMap hashMap = new HashMap();
        String lowerCase = String.format(this.c + "/search-movies/%s.html", new Object[]{TitleHelper.a(movieInfo.name, "+")}).toLowerCase();
        hashMap.put("referer", this.c + "/");
        hashMap.put("Host", BaseProvider.h(this.c));
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(lowerCase, (Map<String, String>[]) new Map[]{hashMap})).g("ul.listcontent").b("li.item").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            Element h = element.h("div.thumb");
            Element h2 = element.h("a.title");
            String G = h.h("div.status.status-year").G();
            String b = h2.b("href");
            String trim = h2.G().trim();
            if (z) {
                String lowerCase2 = trim.toLowerCase();
                if (lowerCase2.equals(movieInfo.name.toLowerCase() + " (" + movieInfo.year + ")") || (trim.toLowerCase().equals(movieInfo.name.toLowerCase()) && movieInfo.year.equals(G.trim()))) {
                    return b;
                }
            } else {
                String trim2 = h.h("div.status").G().trim();
                if (trim.toLowerCase().equals(movieInfo.name.toLowerCase()) && !trim2.isEmpty()) {
                    if (trim2.equals("Season " + movieInfo.session)) {
                        if (b.endsWith("/")) {
                            return b + "episode-" + movieInfo.eps + ".html";
                        }
                        return b + "/episode-" + movieInfo.eps + ".html";
                    }
                }
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
        return new java.lang.String(android.util.Base64.decode(r1, 0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002b, code lost:
        return "";
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0021 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String j(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 1
            java.lang.String r1 = "decode\\(\"([^\"]+)\""
            java.lang.String r1 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r1, (int) r0)
            boolean r2 = r1.isEmpty()
            if (r2 == 0) goto L_0x0014
            java.lang.String r1 = "div\\sclass=\"player\">\\r?\\n.*href=[\"']([^\"']+[^\"'])[\"']"
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r1, (int) r0)
            return r5
        L_0x0014:
            r5 = 0
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0021 }
            byte[] r2 = android.util.Base64.decode(r1, r5)     // Catch:{ Exception -> 0x0021 }
            java.lang.String r3 = "UTF-8"
            r0.<init>(r2, r3)     // Catch:{ Exception -> 0x0021 }
            return r0
        L_0x0021:
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x002b }
            byte[] r5 = android.util.Base64.decode(r1, r5)     // Catch:{ Exception -> 0x002b }
            r0.<init>(r5)     // Catch:{ Exception -> 0x002b }
            return r0
        L_0x002b:
            java.lang.String r5 = ""
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Fmovies.j(java.lang.String):java.lang.String");
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        HttpHelper e = HttpHelper.e();
        String b = e.b(str, this.c + "/");
        String j = j(b);
        if (!j.isEmpty()) {
            a(observableEmitter, j, "HQ", new boolean[0]);
        }
        Iterator it2 = Jsoup.b(b).g("p.server_play").iterator();
        if (it2.hasNext()) {
            it2.next();
        }
        while (it2.hasNext()) {
            String j2 = j(HttpHelper.e().b(((Element) it2.next()).g("a").a("href"), str));
            if (!j2.isEmpty()) {
                Logger.a(j2);
                a(observableEmitter, j2, "HQ", new boolean[0]);
            }
        }
    }
}
