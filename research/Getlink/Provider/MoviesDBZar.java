package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class MoviesDBZar extends BaseProvider {
    private String[] c = Utils.getProvider(20).split(",");

    private void c(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        HashMap hashMap;
        int i;
        String str;
        String str2;
        Iterator it2;
        MovieInfo movieInfo2 = movieInfo;
        String str3 = "h4.box-title";
        String replace = com.original.tase.utils.Utils.a(movieInfo2.name.toLowerCase(), new boolean[0]).replace(" ", "+");
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        String[] strArr = this.c;
        int length = strArr.length;
        String str4 = "HD";
        int i2 = 0;
        while (i2 < length) {
            String str5 = strArr[i2];
            String j = j(str5);
            if (!j.isEmpty()) {
                str5 = j;
            }
            hashMap2.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
            hashMap2.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US,en;q=0.9,vi;q=0.8");
            String str6 = "";
            hashMap2.put("Origin", str5.replace("http:\\", str6).replace("https:\\", str6));
            hashMap2.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
            hashMap2.put("Referer", str5 + "/");
            String[] strArr2 = strArr;
            String str7 = replace;
            String a2 = HttpHelper.e().a(str5 + "/msearch.php?q=&searchquery=" + replace + "&q=M", (Map<String, String>[]) new Map[]{hashMap2});
            Iterator it3 = Jsoup.b(a2).g("div.post-details").iterator();
            while (true) {
                hashMap = hashMap2;
                i = length;
                if (!it3.hasNext()) {
                    str = str4;
                    break;
                }
                try {
                    Element element = (Element) it3.next();
                    str = str4;
                    try {
                        str6 = element.h("a").b("href");
                        it2 = it3;
                    } catch (Throwable th) {
                        th = th;
                        it2 = it3;
                        Logger.a(th, new boolean[0]);
                        hashMap2 = hashMap;
                        length = i;
                        str4 = str;
                        it3 = it2;
                    }
                    try {
                        str7 = element.h("a").b("title");
                        String a3 = Regex.a(element.toString(), "<span>(\\d+)</span>", 1);
                        if (str7.toLowerCase().startsWith(movieInfo2.name.toLowerCase()) && a3.equals(movieInfo2.year)) {
                            if (str6.startsWith("/")) {
                                str6 = str5 + str6;
                            }
                            if (str6.startsWith("movie.php")) {
                                str6 = str5 + "/" + str6;
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        Logger.a(th, new boolean[0]);
                        hashMap2 = hashMap;
                        length = i;
                        str4 = str;
                        it3 = it2;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    str = str4;
                    it2 = it3;
                    Logger.a(th, new boolean[0]);
                    hashMap2 = hashMap;
                    length = i;
                    str4 = str;
                    it3 = it2;
                }
                hashMap2 = hashMap;
                length = i;
                str4 = str;
                it3 = it2;
            }
            if (str6.isEmpty()) {
                Iterator it4 = Jsoup.b(a2).g("article.box.MovBOX").b("div.details").iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        str2 = str3;
                        break;
                    }
                    try {
                        Element element2 = (Element) it4.next();
                        str6 = element2.h(str3).h("a").b("href");
                        str7 = element2.h(str3).h("a").G();
                        String G = element2.h("span.review").h("a").G();
                        str2 = str3;
                        try {
                            str = Regex.a(element2.toString(), "<label class=\"month\">(\\w+).*</label>", 1);
                            if (str7.toLowerCase().equals(movieInfo2.name.toLowerCase()) && G.equals(movieInfo2.year)) {
                                if (str6.startsWith("/")) {
                                    str6 = str5 + str6;
                                }
                                if (str6.startsWith("movie.php")) {
                                    str6 = str5 + "/" + str6;
                                }
                            }
                        } catch (Throwable th4) {
                            th = th4;
                            Logger.a(th, new boolean[0]);
                            str3 = str2;
                        }
                    } catch (Throwable th5) {
                        th = th5;
                        str2 = str3;
                        Logger.a(th, new boolean[0]);
                        str3 = str2;
                    }
                    str3 = str2;
                }
                str4 = str;
                if (str6.isEmpty()) {
                    ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
                    replace = str7;
                    i2++;
                    strArr = strArr2;
                    hashMap2 = hashMap;
                    length = i;
                    str3 = str2;
                } else {
                    str = str4;
                }
            } else {
                str2 = str3;
            }
            String b = HttpHelper.e().b(str6, str5);
            String a4 = Regex.a(b, "<span class=\"MovieQuality\">(.*)</span>", 1);
            if (a4.isEmpty()) {
                a4 = str.isEmpty() ? "HQ" : str;
            }
            Iterator it5 = Regex.b(b, "['\"]([^'\"]+(mka|mkv|mp4|avi))['\"]", 1, true).get(0).iterator();
            hashMap3.put("Referer", str6);
            hashMap3.put("User-Agent", Constants.f5838a);
            ArrayList arrayList = new ArrayList();
            while (it5.hasNext()) {
                String str8 = (String) it5.next();
                if (str8.startsWith("//")) {
                    str8 = "http:" + str8;
                }
                if ((str8.contains("http://") || str8.contains("https://")) && !arrayList.contains(str8)) {
                    arrayList.add(str8);
                    MediaSource mediaSource = new MediaSource(a(), "CDN", c(a4));
                    mediaSource.setStreamLink(str8);
                    mediaSource.setPlayHeader(hashMap3);
                    mediaSource.setQuality(a4);
                    observableEmitter.onNext(mediaSource);
                } else {
                    ObservableEmitter<? super MediaSource> observableEmitter3 = observableEmitter;
                }
            }
            ObservableEmitter<? super MediaSource> observableEmitter4 = observableEmitter;
            replace = str7;
            str4 = str;
            i2++;
            strArr = strArr2;
            hashMap2 = hashMap;
            length = i;
            str3 = str2;
        }
    }

    private String j(String str) {
        return Regex.a(HttpHelper.e().b(str, (Map<String, String>[]) new Map[0]).getBody(), "url=(.*)[\"]", 1);
    }

    public String a() {
        return "MoviesDBZar";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        c(movieInfo, observableEmitter);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }
}
