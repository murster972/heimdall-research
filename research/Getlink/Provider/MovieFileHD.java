package com.utils.Getlink.Provider;

import com.jaunt.ResponseException;
import com.jaunt.UserAgent;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class MovieFileHD extends BaseProvider {
    private String[] c = Utils.getProvider(104).split(",");
    String d = this.c[0];

    private String b(MovieInfo movieInfo) {
        if (HttpHelper.e().a(this.d, (Map<String, String>[]) new Map[0]).contains("Just a moment...")) {
            try {
                UserAgent userAgent = new UserAgent();
                userAgent.a(this.d);
                Logger.a(a(), userAgent.k.toString());
            } catch (ResponseException unused) {
            }
        }
        HashMap hashMap = new HashMap();
        String str = this.d + "/search/keyword/" + com.original.tase.utils.Utils.a(movieInfo.getName().toLowerCase(), new boolean[0]).replace("+", "%20");
        hashMap.put("referer", this.d + "/");
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap});
        if (a2.contains("Just a moment...")) {
            try {
                UserAgent userAgent2 = new UserAgent();
                userAgent2.a(str);
                a2 = userAgent2.b();
            } catch (ResponseException unused2) {
            }
        }
        hashMap.put("referer", str);
        Iterator it2 = Jsoup.b(a2).g("div.thumbnail.text-center").iterator();
        new ArrayList();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String a3 = Regex.a(element.toString(), "<h(?:\\d)><a.*href=['\"]([^'\"]+[^'\"])['\"]>(.+)<\\/a>", 2);
            String a4 = Regex.a(element.toString(), "<h(?:\\d)><a.*href=['\"]([^'\"]+[^'\"])['\"]>(.+)<\\/a>", 1);
            String a5 = Regex.a(element.toString(), "<div.*>\\r?\\n[\\s\\S]+(\\d{4})\\r?\\n[\\s\\S]+<\\/div", 1);
            if (a3.toLowerCase().equalsIgnoreCase(movieInfo.name.toLowerCase()) && a5.equalsIgnoreCase(movieInfo.year)) {
                if (!a4.startsWith("/")) {
                    return a4;
                }
                return this.d + a4;
            }
        }
        return "";
    }

    public String a() {
        return "MovieFileHD";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        for (String str : this.c) {
            this.d = str;
            String b = b(movieInfo);
            if (!b.isEmpty()) {
                a(observableEmitter, movieInfo, b);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        HashMap<String, String> a2 = Constants.a();
        a2.put("origin", this.d);
        a2.put("referer", this.d + "/");
        String a3 = Regex.a(str, "movie_([0-9a-zA-Z]+)", 1);
        String a4 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
        if (a4.contains("Just a moment...")) {
            try {
                UserAgent userAgent = new UserAgent();
                userAgent.a(str);
                a4 = userAgent.b();
            } catch (ResponseException unused) {
            }
        }
        String format = String.format("pass=%s&param=%s&extra=%s&e2=1", new Object[]{a3, Jsoup.b(a4).h("div#divU").G(), Utils.o()});
        String a5 = HttpHelper.e().a(this.d + "/home/index/GetMInfoAjax", format, (Map<String, String>[]) new Map[]{a2});
        if (a5.contains("Just a moment...")) {
            try {
                UserAgent userAgent2 = new UserAgent();
                userAgent2.a(this.d + "/home/index/GetMInfoAjax", format, "x-requested-with:XMLHttpRequest", "referer:" + str + "");
                a5 = userAgent2.b();
            } catch (ResponseException unused2) {
            }
        }
        String a6 = Regex.a(a5, "\\\\\"val\\\\\":\\\\\"([^'\"]+[^'\"])\\\\\"", 1);
        if (!a6.isEmpty()) {
            boolean k = GoogleVideoHelper.k(a6);
            MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
            String replace = a6.replace("\\", "");
            mediaSource.setStreamLink(replace);
            if (k) {
                mediaSource.setPlayHeader(a2);
            }
            mediaSource.setQuality(k ? GoogleVideoHelper.h(replace) : "1080");
            observableEmitter.onNext(mediaSource);
        }
    }
}
