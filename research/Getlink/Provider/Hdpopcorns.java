package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.unity3d.ads.metadata.MediationMetaData;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Hdpopcorns extends BaseProvider {
    public String c = (Utils.getProvider(79) + "/");

    public String a() {
        return "Hdpopcorns";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("?s=");
        sb.append(com.original.tase.utils.Utils.a(movieInfo.name + " " + movieInfo.year, new boolean[0]).replace("%20", "+"));
        String sb2 = sb.toString();
        HttpHelper e = HttpHelper.e();
        Document b = Jsoup.b(e.a(this.c + sb2, (Map<String, String>[]) new Map[0]));
        if (b.h("div[class=no-results]") != null) {
            return "";
        }
        Element h = b.h("article[class=latestPost excerpt first]");
        String b2 = h.h("a").b("href");
        String lowerCase = h.h("a").b("title").replace("’", "'").replace("–", "-").toLowerCase();
        String lowerCase2 = movieInfo.name.replace("’", "'").replace("–", "-").toLowerCase();
        if (lowerCase.startsWith(lowerCase2 + " " + movieInfo.year)) {
            return b2;
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0])).h("form[action=/select-movie-quality.php]").g("input").iterator();
        String str2 = "";
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            str2 = str2 + element.b(MediationMetaData.KEY_NAME) + "=" + element.b("value") + "&";
        }
        String str3 = str2 + "x=&y=";
        com.original.tase.utils.Utils.a(str3, new boolean[0]);
        String a2 = HttpHelper.e().a(this.c + "select-movie-quality.php", str3, false, (Map<String, String>[]) new Map[0]);
        new ArrayList();
        Iterator it3 = Jsoup.b(a2).g("div[class=thecontent]").b("a[href]").iterator();
        while (it3.hasNext()) {
            String b = ((Element) it3.next()).b("href");
            if (b.contains(".mp4") || !b.contains("http:///downloads")) {
                HashMap hashMap = new HashMap();
                hashMap.put("Referer", "http://hdpopcorns.co/select-movie-quality.php");
                hashMap.put("User-agent", Constants.f5838a);
                String str4 = b.contains("720p") ? "720" : "HD";
                if (b.contains("1080p")) {
                    str4 = "1080";
                }
                MediaSource mediaSource = new MediaSource(a(), "HD SlowServer", false);
                mediaSource.setPlayHeader(hashMap);
                mediaSource.setStreamLink(b);
                mediaSource.setQuality(str4);
                observableEmitter.onNext(mediaSource);
            }
        }
    }
}
