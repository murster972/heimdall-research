package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class MovieGL extends BaseProvider {
    private String c = Utils.getProvider(55);
    private String d = "HD";

    public String a() {
        return "MovieGL";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b + "-season-" + movieInfo.session + "-episode-" + movieInfo.eps);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        String str2;
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("ul#stream_links").b("li").b("a[data-service][data-link]").iterator();
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).b("data-link");
            if (!b.isEmpty()) {
                Logger.a("http link ", b);
                try {
                    str2 = new String(Base64.decode(b, 10), "UTF-8");
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                    str2 = new String(Base64.decode(b, 0));
                }
                String replace = Regex.a(str2, "link[\"]\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1).replace("\\/", "/");
                if (!replace.isEmpty()) {
                    a(observableEmitter, replace, this.d, false);
                }
            }
        }
    }

    private String b(MovieInfo movieInfo) {
        int intValue = movieInfo.getType().intValue();
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        String replace = com.original.tase.utils.Utils.a(movieInfo.getName(), new boolean[0]).replace("%20", "+");
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/search?q=" + replace.toLowerCase(), (Map<String, String>[]) new Map[]{hashMap})).g("ul.main_list.list_of_catefories").b("li").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            String G = h.h("span.item_name").G();
            if (G.isEmpty()) {
                G = h.h("img").b("alt");
            }
            String G2 = h.h("span.item_ganre").G();
            if (G.equalsIgnoreCase(movieInfo.name) && G2.contains(movieInfo.year)) {
                if (!b.startsWith("/")) {
                    return b;
                }
                return this.c + b;
            }
        }
        return "";
    }
}
