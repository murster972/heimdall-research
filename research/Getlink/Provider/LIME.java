package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class LIME extends BaseProvider {
    public String c = Utils.getProvider(9);

    public String a() {
        return "LIME";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0227, code lost:
        r17 = r2;
        r2 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(com.movie.data.model.MovieInfo r19, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r20) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            java.lang.String r2 = "1080p"
            java.lang.Integer r3 = r19.getType()
            int r3 = r3.intValue()
            r4 = 0
            r5 = 1
            if (r3 != r5) goto L_0x0014
            r3 = 1
            goto L_0x0015
        L_0x0014:
            r3 = 0
        L_0x0015:
            if (r3 == 0) goto L_0x002b
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = " "
            r6.append(r7)
            java.lang.String r7 = r1.year
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            goto L_0x0058
        L_0x002b:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = " S"
            r6.append(r7)
            java.lang.String r7 = r1.session
            int r7 = java.lang.Integer.parseInt(r7)
            java.lang.String r7 = com.original.tase.utils.Utils.a((int) r7)
            r6.append(r7)
            java.lang.String r7 = "E"
            r6.append(r7)
            java.lang.String r7 = r1.eps
            int r7 = java.lang.Integer.parseInt(r7)
            java.lang.String r7 = com.original.tase.utils.Utils.a((int) r7)
            r6.append(r7)
            java.lang.String r6 = r6.toString()
        L_0x0058:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = r1.name
            r7.append(r8)
            r7.append(r6)
            java.lang.String r7 = r7.toString()
            boolean[] r8 = new boolean[r4]
            java.lang.String r7 = com.original.tase.utils.Utils.a((java.lang.String) r7, (boolean[]) r8)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = r0.c
            r8.append(r9)
            java.lang.String r9 = "/search/0/1/000/10/"
            r8.append(r9)
            r8.append(r7)
            java.lang.String r8 = r8.toString()
            java.util.HashMap r9 = new java.util.HashMap
            r9.<init>()
            java.lang.String r10 = "Accept"
            java.lang.String r11 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
            r9.put(r10, r11)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = r0.c
            r10.append(r11)
            java.lang.String r11 = "/search/"
            r10.append(r11)
            r10.append(r7)
            java.lang.String r7 = r10.toString()
            java.lang.String r10 = "Referer"
            r9.put(r10, r7)
            java.lang.String r7 = "Upgrade-Insecure-Requests"
            java.lang.String r10 = "1"
            r9.put(r7, r10)
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r10 = new java.util.Map[r5]
            r10[r4] = r9
            java.lang.String r7 = r7.a((java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r10)
            org.jsoup.nodes.Document r7 = org.jsoup.Jsoup.b(r7)
            java.lang.String r8 = "div#index"
            org.jsoup.select.Elements r7 = r7.g((java.lang.String) r8)
            java.lang.String r8 = "tbody"
            org.jsoup.select.Elements r7 = r7.b(r8)
            java.lang.String r8 = "tr"
            org.jsoup.select.Elements r7 = r7.b(r8)
            java.util.Iterator r7 = r7.iterator()
            com.original.tase.helper.DirectoryIndexHelper r8 = new com.original.tase.helper.DirectoryIndexHelper
            r8.<init>()
            java.util.HashMap r9 = new java.util.HashMap
            r9.<init>()
        L_0x00e3:
            boolean r10 = r7.hasNext()
            java.lang.String r11 = ""
            if (r10 == 0) goto L_0x013c
            java.lang.Object r10 = r7.next()
            org.jsoup.nodes.Element r10 = (org.jsoup.nodes.Element) r10
            java.lang.String r12 = "td"
            org.jsoup.select.Elements r12 = r10.g((java.lang.String) r12)     // Catch:{ all -> 0x013a }
            java.lang.String r13 = "a"
            org.jsoup.select.Elements r12 = r12.b(r13)     // Catch:{ all -> 0x013a }
            org.jsoup.nodes.Element r12 = r12.a()     // Catch:{ all -> 0x013a }
            java.lang.String r12 = r12.G()     // Catch:{ all -> 0x013a }
            java.lang.String r13 = r12.toLowerCase()     // Catch:{ all -> 0x013a }
            java.lang.String r13 = com.original.tase.helper.TitleHelper.a(r13, r11)     // Catch:{ all -> 0x013a }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x013a }
            r14.<init>()     // Catch:{ all -> 0x013a }
            java.lang.String r15 = r1.name     // Catch:{ all -> 0x013a }
            java.lang.String r15 = r15.toLowerCase()     // Catch:{ all -> 0x013a }
            r14.append(r15)     // Catch:{ all -> 0x013a }
            r14.append(r6)     // Catch:{ all -> 0x013a }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x013a }
            java.lang.String r11 = com.original.tase.helper.TitleHelper.a(r14, r11)     // Catch:{ all -> 0x013a }
            boolean r11 = r13.contains(r11)     // Catch:{ all -> 0x013a }
            if (r11 == 0) goto L_0x00e3
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x013a }
            java.lang.String r11 = "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?"
            java.lang.String r10 = com.original.tase.utils.Regex.a((java.lang.String) r10, (java.lang.String) r11, (int) r5)     // Catch:{ all -> 0x013a }
            r9.put(r10, r12)     // Catch:{ all -> 0x013a }
            goto L_0x00e3
        L_0x013a:
            goto L_0x00e3
        L_0x013c:
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Set r6 = r9.entrySet()
            java.util.Iterator r6 = r6.iterator()
            java.lang.String r7 = "HQ"
            r9 = r7
        L_0x0151:
            boolean r10 = r6.hasNext()
            if (r10 == 0) goto L_0x0297
            java.lang.Object r10 = r6.next()
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10
            java.lang.Object r12 = r10.getValue()     // Catch:{ all -> 0x028d }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x028d }
            java.lang.String r13 = r12.toUpperCase()     // Catch:{ all -> 0x028d }
            java.lang.String r14 = "(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)"
            java.lang.String r13 = r13.replaceAll(r14, r11)     // Catch:{ all -> 0x028d }
            java.lang.String r14 = "\\.|\\(|\\)|\\[|\\]|\\s|\\-"
            java.lang.String[] r13 = r13.split(r14)     // Catch:{ all -> 0x028d }
            int r14 = r13.length     // Catch:{ all -> 0x028d }
            r15 = r9
            r9 = 0
        L_0x0176:
            if (r9 >= r14) goto L_0x022e
            r16 = r13[r9]     // Catch:{ all -> 0x022b }
            java.lang.String r4 = r16.toLowerCase()     // Catch:{ all -> 0x022b }
            java.lang.String r5 = "dvdscr"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "camrip"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "tsrip"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "hdcam"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "hdtc"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "hdts"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "dvdcam"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "dvdts"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "cam"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "telesync"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 != 0) goto L_0x0227
            java.lang.String r5 = "ts"
            boolean r5 = r4.contains(r5)     // Catch:{ all -> 0x022b }
            if (r5 == 0) goto L_0x01d7
            goto L_0x0227
        L_0x01d7:
            boolean r5 = r4.contains(r2)     // Catch:{ all -> 0x022b }
            r17 = r2
            java.lang.String r2 = "720p"
            if (r5 != 0) goto L_0x021d
            java.lang.String r5 = "1080"
            boolean r5 = r4.equals(r5)     // Catch:{ all -> 0x0290 }
            if (r5 == 0) goto L_0x01ea
            goto L_0x021d
        L_0x01ea:
            boolean r5 = r4.contains(r2)     // Catch:{ all -> 0x0290 }
            if (r5 != 0) goto L_0x021b
            java.lang.String r5 = "720"
            boolean r5 = r4.equals(r5)     // Catch:{ all -> 0x0290 }
            if (r5 == 0) goto L_0x01f9
            goto L_0x021b
        L_0x01f9:
            java.lang.String r2 = "brrip"
            boolean r2 = r4.contains(r2)     // Catch:{ all -> 0x0290 }
            if (r2 != 0) goto L_0x0219
            java.lang.String r2 = "bdrip"
            boolean r2 = r4.contains(r2)     // Catch:{ all -> 0x0290 }
            if (r2 != 0) goto L_0x0219
            java.lang.String r2 = "hdrip"
            boolean r2 = r4.contains(r2)     // Catch:{ all -> 0x0290 }
            if (r2 != 0) goto L_0x0219
            java.lang.String r2 = "web-dl"
            boolean r2 = r4.contains(r2)     // Catch:{ all -> 0x0290 }
            if (r2 == 0) goto L_0x021f
        L_0x0219:
            java.lang.String r2 = "HD"
        L_0x021b:
            r15 = r2
            goto L_0x021f
        L_0x021d:
            r15 = r17
        L_0x021f:
            int r9 = r9 + 1
            r2 = r17
            r4 = 0
            r5 = 1
            goto L_0x0176
        L_0x0227:
            r17 = r2
            r2 = 1
            goto L_0x0231
        L_0x022b:
            r17 = r2
            goto L_0x0290
        L_0x022e:
            r17 = r2
            r2 = 0
        L_0x0231:
            java.lang.Object r4 = r10.getKey()     // Catch:{ all -> 0x0290 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ all -> 0x0290 }
            java.lang.String r5 = r18.a()     // Catch:{ all -> 0x0290 }
            if (r3 == 0) goto L_0x0242
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r9 = r8.a(r12)     // Catch:{ all -> 0x0290 }
            goto L_0x0246
        L_0x0242:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r9 = r8.b(r12)     // Catch:{ all -> 0x0290 }
        L_0x0246:
            if (r9 == 0) goto L_0x025d
            java.lang.String r5 = r9.c()     // Catch:{ all -> 0x0290 }
            boolean r10 = r5.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0290 }
            if (r10 != 0) goto L_0x0253
            r15 = r5
        L_0x0253:
            java.lang.String r5 = r9.b()     // Catch:{ all -> 0x0290 }
            r9 = 1
            java.lang.String r5 = r0.a((java.lang.String) r5, (boolean) r9)     // Catch:{ all -> 0x0290 }
            goto L_0x025e
        L_0x025d:
            r9 = 1
        L_0x025e:
            java.lang.String r10 = "(magnet:\\?xt=urn:btih:[^&.]+)"
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r4, (java.lang.String) r10, (int) r9)     // Catch:{ all -> 0x0290 }
            java.lang.String r4 = r4.toLowerCase()     // Catch:{ all -> 0x0290 }
            com.movie.data.model.realdebrid.MagnetObject r9 = new com.movie.data.model.realdebrid.MagnetObject     // Catch:{ all -> 0x0290 }
            if (r2 == 0) goto L_0x027e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0290 }
            r2.<init>()     // Catch:{ all -> 0x0290 }
            java.lang.String r10 = "CAM-"
            r2.append(r10)     // Catch:{ all -> 0x0290 }
            r2.append(r15)     // Catch:{ all -> 0x0290 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0290 }
            goto L_0x027f
        L_0x027e:
            r2 = r15
        L_0x027f:
            java.lang.String r10 = r18.a()     // Catch:{ all -> 0x0290 }
            r9.<init>(r5, r4, r2, r10)     // Catch:{ all -> 0x0290 }
            r9.setFileName(r12)     // Catch:{ all -> 0x0290 }
            r1.add(r9)     // Catch:{ all -> 0x0290 }
            goto L_0x0290
        L_0x028d:
            r17 = r2
            r15 = r9
        L_0x0290:
            r9 = r15
            r2 = r17
            r4 = 0
            r5 = 1
            goto L_0x0151
        L_0x0297:
            int r2 = r1.size()
            if (r2 <= 0) goto L_0x02ba
            com.original.tase.model.media.MediaSource r2 = new com.original.tase.model.media.MediaSource
            java.lang.String r3 = r18.a()
            java.lang.String r4 = "Torrent"
            r5 = 0
            r2.<init>(r3, r4, r5)
            r3 = 1
            r2.setTorrent(r3)
            r2.setMagnetObjects(r1)
            java.lang.String r1 = "magnet:LIME"
            r2.setStreamLink(r1)
            r1 = r20
            r1.onNext(r2)
        L_0x02ba:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.LIME.c(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }
}
