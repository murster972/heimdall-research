package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

public class BestFilms extends BaseProvider {
    private String c = Utils.getProvider(19);

    public String a() {
        return "BestFilms";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        ArrayList b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        ArrayList b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00eb, code lost:
        if (r15.contains("S" + com.original.tase.utils.Utils.a(java.lang.Integer.parseInt(r2.session)) + "E" + com.original.tase.utils.Utils.a(java.lang.Integer.parseInt(r2.eps))) == false) goto L_0x037b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r25, com.movie.data.model.MovieInfo r26, java.util.ArrayList<java.lang.String> r27) {
        /*
            r24 = this;
            r0 = r24
            r1 = r25
            r2 = r26
            java.lang.Integer r3 = r26.getType()
            int r3 = r3.intValue()
            r4 = 1
            r5 = 0
            if (r3 != r4) goto L_0x0014
            r3 = 1
            goto L_0x0015
        L_0x0014:
            r3 = 0
        L_0x0015:
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = r0.c
            java.lang.String r10 = "http"
            java.lang.String r11 = "https"
            java.lang.String r9 = r9.replace(r11, r10)
            r8.append(r9)
            java.lang.String r9 = "/"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            java.lang.String r9 = "Referer"
            r7.put(r9, r8)
            java.lang.String r8 = "Upgrade-Insecure-Requests"
            java.lang.String r9 = "1"
            r7.put(r8, r9)
            java.util.Iterator r8 = r27.iterator()
        L_0x004a:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L_0x038a
            java.lang.Object r9 = r8.next()
            java.lang.String r9 = (java.lang.String) r9
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r12 = new java.util.Map[r4]
            r12[r5] = r7
            java.lang.String r11 = r11.a((java.lang.String) r9, (java.util.Map<java.lang.String, java.lang.String>[]) r12)
            boolean r9 = r0.c(r9)
            org.jsoup.nodes.Document r11 = org.jsoup.Jsoup.b(r11)
            java.lang.String r12 = "div#arya-content-main"
            org.jsoup.select.Elements r11 = r11.g((java.lang.String) r12)
            java.lang.String r12 = "a"
            org.jsoup.select.Elements r11 = r11.b(r12)
            java.util.Iterator r11 = r11.iterator()
        L_0x007a:
            boolean r13 = r11.hasNext()
            if (r13 == 0) goto L_0x0384
            java.lang.Object r13 = r11.next()
            org.jsoup.nodes.Element r13 = (org.jsoup.nodes.Element) r13
            java.lang.String r14 = "href"
            java.lang.String r15 = r13.b((java.lang.String) r14)
            r13.G()
            java.lang.String r13 = "imdb"
            boolean r13 = r15.contains(r13)
            if (r13 != 0) goto L_0x037e
            java.lang.String r13 = "title"
            boolean r13 = r15.contains(r13)
            if (r13 != 0) goto L_0x037e
            java.lang.String r13 = "wiki"
            boolean r13 = r15.contains(r13)
            if (r13 != 0) goto L_0x037e
            java.lang.String r13 = "image"
            boolean r13 = r15.contains(r13)
            if (r13 != 0) goto L_0x037e
            java.lang.String r13 = r0.c
            boolean r13 = r15.contains(r13)
            if (r13 == 0) goto L_0x00b8
            goto L_0x007a
        L_0x00b8:
            if (r3 != 0) goto L_0x00ef
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r4 = "S"
            r13.append(r4)
            java.lang.String r4 = r2.session
            int r4 = java.lang.Integer.parseInt(r4)
            java.lang.String r4 = com.original.tase.utils.Utils.a((int) r4)
            r13.append(r4)
            java.lang.String r4 = "E"
            r13.append(r4)
            java.lang.String r4 = r2.eps
            int r4 = java.lang.Integer.parseInt(r4)
            java.lang.String r4 = com.original.tase.utils.Utils.a((int) r4)
            r13.append(r4)
            java.lang.String r4 = r13.toString()
            boolean r4 = r15.contains(r4)
            if (r4 != 0) goto L_0x00ef
            goto L_0x037b
        L_0x00ef:
            boolean r4 = r6.contains(r15)
            if (r4 != 0) goto L_0x0377
            r6.add(r15)
            com.original.tase.helper.DirectoryIndexHelper r4 = new com.original.tase.helper.DirectoryIndexHelper
            r4.<init>()
            boolean r13 = com.original.tase.helper.GoogleVideoHelper.j(r15)
            java.lang.String r5 = "Cookie"
            java.lang.String r16 = "1080p"
            java.lang.String r2 = "GoogleVideo"
            r17 = r6
            java.lang.String r6 = "User-Agent"
            if (r13 == 0) goto L_0x018b
            java.util.HashMap r13 = com.original.tase.helper.GoogleVideoHelper.g(r15)
            if (r13 == 0) goto L_0x018b
            boolean r18 = r13.isEmpty()
            if (r18 != 0) goto L_0x018b
            java.util.Set r13 = r13.entrySet()
            java.util.Iterator r13 = r13.iterator()
        L_0x0121:
            boolean r18 = r13.hasNext()
            if (r18 == 0) goto L_0x018b
            java.lang.Object r18 = r13.next()
            java.util.Map$Entry r18 = (java.util.Map.Entry) r18
            java.lang.Object r19 = r18.getKey()
            r20 = r7
            r7 = r19
            java.lang.String r7 = (java.lang.String) r7
            r27 = r8
            com.original.tase.model.media.MediaSource r8 = new com.original.tase.model.media.MediaSource
            r19 = r11
            java.lang.String r11 = r24.a()
            r21 = r13
            r13 = 0
            r8.<init>(r11, r2, r13)
            r8.setOriginalLink(r15)
            r8.setStreamLink(r7)
            java.lang.Object r7 = r18.getValue()
            java.lang.String r7 = (java.lang.String) r7
            boolean r7 = r7.isEmpty()
            if (r7 == 0) goto L_0x015c
            r7 = r16
            goto L_0x0162
        L_0x015c:
            java.lang.Object r7 = r18.getValue()
            java.lang.String r7 = (java.lang.String) r7
        L_0x0162:
            r8.setQuality((java.lang.String) r7)
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.String r11 = com.original.Constants.f5838a
            r7.put(r6, r11)
            java.lang.Object r11 = r18.getKey()
            java.lang.String r11 = (java.lang.String) r11
            java.lang.String r11 = com.original.tase.helper.GoogleVideoHelper.c(r15, r11)
            r7.put(r5, r11)
            r8.setPlayHeader(r7)
            r1.onNext(r8)
            r8 = r27
            r11 = r19
            r7 = r20
            r13 = r21
            goto L_0x0121
        L_0x018b:
            r20 = r7
            r27 = r8
            r19 = r11
            java.lang.String r7 = "links.trickaddict"
            boolean r7 = r15.contains(r7)
            java.lang.String r11 = "HQ"
            if (r7 == 0) goto L_0x02ed
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            r13 = 0
            java.util.Map[] r8 = new java.util.Map[r13]
            java.lang.String r7 = r7.a((java.lang.String) r15, (java.util.Map<java.lang.String, java.lang.String>[]) r8)
            java.lang.String r8 = "<input[^>]*name\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>"
            r0 = 1
            r13 = 34
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r8, (int) r0, (int) r13)
            r21 = r11
            java.lang.String r11 = "<input[^>]*value\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>"
            java.lang.String r11 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r11, (int) r0, (int) r13)
            boolean r0 = r8.isEmpty()
            if (r0 != 0) goto L_0x01dc
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r8)
            java.lang.String r8 = "="
            r7.append(r8)
            r7.append(r11)
            java.lang.String r7 = r7.toString()
            r8 = 0
            java.util.Map[] r11 = new java.util.Map[r8]
            java.lang.String r7 = r0.a((java.lang.String) r15, (java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r11)
        L_0x01dc:
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r7)
            java.lang.String r7 = "div.row"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r7)
            org.jsoup.select.Elements r0 = r0.b(r12)
            java.util.Iterator r0 = r0.iterator()
        L_0x01ee:
            boolean r7 = r0.hasNext()
            if (r7 == 0) goto L_0x02e7
            java.lang.Object r7 = r0.next()
            org.jsoup.nodes.Element r7 = (org.jsoup.nodes.Element) r7
            java.lang.String r8 = r7.b((java.lang.String) r14)
            r7.G()
            boolean r7 = r8.contains(r10)
            if (r7 == 0) goto L_0x02da
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.j(r8)
            if (r7 == 0) goto L_0x02ab
            java.util.HashMap r7 = com.original.tase.helper.GoogleVideoHelper.g(r8)
            if (r7 == 0) goto L_0x02a3
            boolean r11 = r7.isEmpty()
            if (r11 != 0) goto L_0x02a3
            java.util.Set r7 = r7.entrySet()
            java.util.Iterator r7 = r7.iterator()
        L_0x0221:
            boolean r11 = r7.hasNext()
            if (r11 == 0) goto L_0x02a3
            java.lang.Object r11 = r7.next()
            java.util.Map$Entry r11 = (java.util.Map.Entry) r11
            java.lang.Object r13 = r11.getKey()
            java.lang.String r13 = (java.lang.String) r13
            com.original.tase.model.media.MediaSource r15 = new com.original.tase.model.media.MediaSource
            if (r9 != 0) goto L_0x0245
            java.lang.String r18 = r24.a()
            r22 = r7
            r7 = 0
            r23 = r18
            r18 = r0
            r0 = r23
            goto L_0x025f
        L_0x0245:
            r18 = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r22 = r7
            java.lang.String r7 = "CAM"
            r0.append(r7)
            java.lang.String r7 = r24.a()
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            r7 = 0
        L_0x025f:
            r15.<init>(r0, r2, r7)
            r15.setOriginalLink(r8)
            r15.setStreamLink(r13)
            java.lang.Object r0 = r11.getValue()
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0277
            r0 = r16
            goto L_0x027d
        L_0x0277:
            java.lang.Object r0 = r11.getValue()
            java.lang.String r0 = (java.lang.String) r0
        L_0x027d:
            r15.setQuality((java.lang.String) r0)
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.lang.String r7 = com.original.Constants.f5838a
            r0.put(r6, r7)
            java.lang.Object r7 = r11.getKey()
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r7 = com.original.tase.helper.GoogleVideoHelper.c(r8, r7)
            r0.put(r5, r7)
            r15.setPlayHeader(r0)
            r1.onNext(r15)
            r0 = r18
            r7 = r22
            goto L_0x0221
        L_0x02a3:
            r18 = r0
            r7 = 0
            r15 = r24
            r11 = r21
            goto L_0x02e1
        L_0x02ab:
            r18 = r0
            if (r3 == 0) goto L_0x02b4
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r0 = r4.a(r8)
            goto L_0x02b8
        L_0x02b4:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r0 = r4.b(r8)
        L_0x02b8:
            if (r0 == 0) goto L_0x02cb
            java.lang.String r7 = r0.c()
            r11 = r21
            boolean r7 = r7.equalsIgnoreCase(r11)
            if (r7 != 0) goto L_0x02cd
            java.lang.String r0 = r0.c()
            goto L_0x02ce
        L_0x02cb:
            r11 = r21
        L_0x02cd:
            r0 = r11
        L_0x02ce:
            r7 = 1
            boolean[] r13 = new boolean[r7]
            r7 = 0
            r13[r7] = r7
            r15 = r24
            r15.a(r1, r8, r0, r13)
            goto L_0x02e1
        L_0x02da:
            r15 = r24
            r18 = r0
            r11 = r21
            r7 = 0
        L_0x02e1:
            r21 = r11
            r0 = r18
            goto L_0x01ee
        L_0x02e7:
            r7 = 0
            r8 = 1
            r0 = r24
            goto L_0x0369
        L_0x02ed:
            r7 = 0
            java.lang.String r2 = "racaty.com"
            boolean r2 = r15.contains(r2)
            if (r2 == 0) goto L_0x0314
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r4 = new java.util.Map[r7]
            java.lang.String r2 = r2.a((java.lang.String) r15, (java.util.Map<java.lang.String, java.lang.String>[]) r4)
            java.lang.String r4 = "<a[^>]*href\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>Continue"
            r5 = 34
            r8 = 1
            java.lang.String r2 = com.original.tase.utils.Regex.a((java.lang.String) r2, (java.lang.String) r4, (int) r8, (int) r5)
            boolean[] r4 = new boolean[r8]
            r4[r7] = r7
            java.lang.String r5 = "HD"
            r0.a(r1, r2, r5, r4)
        L_0x0312:
            r7 = 0
            goto L_0x0369
        L_0x0314:
            r8 = 1
            java.lang.String r2 = ".mkv"
            boolean r2 = r15.endsWith(r2)
            if (r2 != 0) goto L_0x032c
            java.lang.String r2 = ".mp4"
            boolean r2 = r15.endsWith(r2)
            if (r2 == 0) goto L_0x0326
            goto L_0x032c
        L_0x0326:
            java.lang.String r2 = "NEEDRESOLVER 2 "
            com.original.tase.Logger.a((java.lang.String) r2, (java.lang.String) r15)
            goto L_0x0312
        L_0x032c:
            if (r3 == 0) goto L_0x0333
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r2 = r4.a(r15)
            goto L_0x0337
        L_0x0333:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r2 = r4.b(r15)
        L_0x0337:
            if (r2 == 0) goto L_0x0347
            java.lang.String r4 = r2.c()
            boolean r4 = r4.equalsIgnoreCase(r11)
            if (r4 != 0) goto L_0x0347
            java.lang.String r11 = r2.c()
        L_0x0347:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r4 = com.original.Constants.f5838a
            r2.put(r6, r4)
            com.original.tase.model.media.MediaSource r4 = new com.original.tase.model.media.MediaSource
            java.lang.String r5 = r24.a()
            java.lang.String r6 = "CDN-FastServer"
            r7 = 0
            r4.<init>(r5, r6, r7)
            r4.setStreamLink(r15)
            r4.setPlayHeader(r2)
            r4.setQuality((java.lang.String) r11)
            r1.onNext(r4)
        L_0x0369:
            r2 = r26
            r8 = r27
            r6 = r17
            r11 = r19
            r7 = r20
            r4 = 1
            r5 = 0
            goto L_0x007a
        L_0x0377:
            r20 = r7
            r2 = r26
        L_0x037b:
            r4 = 1
            goto L_0x007a
        L_0x037e:
            r20 = r7
            r2 = r26
            goto L_0x007a
        L_0x0384:
            r20 = r7
            r2 = r26
            goto L_0x004a
        L_0x038a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.BestFilms.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo, java.util.ArrayList):void");
    }

    private ArrayList b(MovieInfo movieInfo) {
        String str;
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        if (movieInfo.getType().intValue() != 1) {
            z = false;
        }
        new DirectoryIndexHelper();
        String name = movieInfo.getName();
        if (z) {
            str = String.valueOf(movieInfo.getYear());
        } else {
            str = "S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.eps));
        }
        String replace = (name.replace("'", "") + " " + str).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", " ").replace("  ", " ");
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/?s=" + com.original.tase.utils.Utils.a(replace, new boolean[0]), (Map<String, String>[]) new Map[0])).g("div.arya-main-blog-body.left.relative").b("li").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("href");
            String G = element.h("h2").G();
            if (!z) {
                if (TitleHelper.a(G.toLowerCase(), "").startsWith(TitleHelper.a(movieInfo.getName().toLowerCase() + "season" + movieInfo.session, "")) && !arrayList.contains(b)) {
                    arrayList.add(b);
                }
            } else if (TitleHelper.a(G.toLowerCase(), "").startsWith(TitleHelper.a(movieInfo.getName().toLowerCase(), "")) && !arrayList.contains(b)) {
                arrayList.add(b);
            }
        }
        if (arrayList.isEmpty()) {
            Iterator it3 = Jsoup.a(HttpHelper.e().a(this.c + "/search/" + com.original.tase.utils.Utils.a(replace, new boolean[0]) + "/feed/rss2", (Map<String, String>[]) new Map[0]), "", Parser.b()).g("item").iterator();
            while (it3.hasNext()) {
                Element element2 = (Element) it3.next();
                Element h = element2.h("title");
                if (h != null) {
                    String replace2 = h.G().toLowerCase().replace("direct download", "").replace("free", "");
                    if (!z) {
                        if (TitleHelper.f(replace2).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName() + "season" + movieInfo.session)))) {
                            String a2 = Regex.a(element2.toString(), "http.*?(movieeater.org)[/].*?[/]", 0);
                            if (!arrayList.contains(a2)) {
                                arrayList.add(a2);
                            }
                        }
                    } else if (TitleHelper.f(replace2).contains(TitleHelper.f(TitleHelper.e(movieInfo.getName()))) && replace2.contains(movieInfo.year)) {
                        String a3 = Regex.a(element2.toString(), "http.*?(movieeater.org)[/].*?[/]", 0);
                        if (!arrayList.contains(a3)) {
                            arrayList.add(a3);
                        }
                    }
                }
            }
        }
        return arrayList;
    }
}
