package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.http.sucuri.SucuriCloudProxyHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class NovaMovie extends BaseProvider {
    private String c = (Utils.getProvider(81) + "/");
    private String d = "";

    public String a() {
        return "NovaMovie";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String c2 = c(movieInfo);
        if (c2.isEmpty()) {
            c2 = b(movieInfo);
            if (c2.isEmpty()) {
                return;
            }
        }
        a(observableEmitter, c2, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String c2 = c(movieInfo);
        if (c2.isEmpty()) {
            c2 = b(movieInfo);
            if (c2.isEmpty()) {
                return;
            }
        }
        a(observableEmitter, c2, movieInfo);
    }

    public String c(MovieInfo movieInfo) {
        boolean z = true;
        if (movieInfo.getType().intValue() != 1) {
            z = false;
        }
        HashMap hashMap = new HashMap();
        String str = this.c + "?s=" + TitleHelper.a(movieInfo.name, "+");
        hashMap.put("referer", this.c);
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(SucuriCloudProxyHelper.b(str, str)).g("div[class=ml-item]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.h("a").b("href");
            String b2 = element.h("a").b("oldtitle");
            if (z) {
                this.d = element.g("a").b("span[class=mli-quality]").c();
                if (b2.toLowerCase().equals(movieInfo.name.toLowerCase() + " (" + movieInfo.year + ")")) {
                    return b;
                }
            } else {
                this.d = "HD";
                if (b2.toLowerCase().equals(movieInfo.name.toLowerCase() + " season " + movieInfo.session)) {
                    return b;
                }
            }
        }
        return "";
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        boolean z;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.c);
        hashMap.put("user-agent", Constants.f5838a);
        int i = 0;
        boolean z2 = movieInfo.getType().intValue() == 1;
        String str2 = str;
        String a2 = HttpHelper.e().a(str2, (Map<String, String>[]) new Map[]{hashMap});
        Iterator it2 = null;
        if (!z2) {
            Iterator it3 = Jsoup.b(a2).g("div[class=tvseason]").b("a").iterator();
            while (true) {
                if (!it3.hasNext()) {
                    z = false;
                    break;
                }
                Element element = (Element) it3.next();
                String b = element.h("a").b("href");
                String G = element.h("a").G();
                String str3 = movieInfo.eps;
                this.d = "HD";
                if (movieInfo.getEps().intValue() < 10) {
                    str3 = "0" + movieInfo.getEps();
                }
                if (G.toLowerCase().equals("episode " + str3)) {
                    it2 = Jsoup.b(HttpHelper.e().a(b, (Map<String, String>[]) new Map[0])).g("div[class=movieplay playerload]").b("iframe").iterator();
                    str2 = b;
                    z = true;
                    break;
                }
                str2 = b;
            }
            if (!z) {
                return;
            }
        } else {
            Document b2 = Jsoup.b(a2);
            it2 = b2.g("div[class=movieplay]").b("iframe").iterator();
            String G2 = b2.h("span.quality").G();
            if (!G2.isEmpty()) {
                this.d = G2;
            }
        }
        while (it2.hasNext()) {
            Element element2 = (Element) it2.next();
            String b3 = element2.b("src");
            if (b3.isEmpty()) {
                b3 = element2.b("data-lazy-src");
            }
            boolean contains = this.d.toLowerCase().contains("cam");
            if (b3.contains("novamovie.net") || b3.contains("pkayprek")) {
                if (b3.startsWith("//")) {
                    b3 = "https:" + b3;
                }
                hashMap.put("referer", str2);
                HttpHelper e = HttpHelper.e();
                Map[] mapArr = new Map[1];
                mapArr[i] = hashMap;
                String a3 = e.a(b3, (Map<String, String>[]) mapArr);
                Iterator it4 = Regex.b(a3, "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(i).iterator();
                Iterator it5 = Regex.b(a3, "['\"]?label['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(i).iterator();
                while (it4.hasNext()) {
                    String str4 = (String) it4.next();
                    if (GoogleVideoHelper.d(str4) && !str4.contains(".srt")) {
                        HashMap hashMap2 = new HashMap();
                        hashMap2.put("User-Agent", Constants.f5838a);
                        MediaSource mediaSource = new MediaSource(a(), "GoogleVideo", contains);
                        mediaSource.setStreamLink(str4);
                        mediaSource.setPlayHeader(hashMap2);
                        mediaSource.setQuality((String) it5.next());
                        observableEmitter2.onNext(mediaSource);
                    }
                }
            } else {
                String str5 = this.d;
                boolean[] zArr = new boolean[1];
                zArr[i] = contains;
                a(observableEmitter2, b3, str5, zArr);
            }
            i = 0;
        }
    }

    public String b(MovieInfo movieInfo) {
        boolean z = true;
        if (movieInfo.getType().intValue() != 1) {
            z = false;
        }
        if (z) {
            return this.c + TitleHelper.a(movieInfo.name.toLowerCase() + " " + movieInfo.year, "-");
        }
        return this.c + "series/" + TitleHelper.a(movieInfo.name.toLowerCase() + " season " + movieInfo.session, "-");
    }
}
