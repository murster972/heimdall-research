package com.utils.Getlink.Provider;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.vungle.warren.model.CookieDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FliXanityBased extends BaseProvider {
    public static String c = "";

    static class LOGG implements console {
        LOGG() {
        }
    }

    interface console {
    }

    public String a() {
        return "FliXanityBased";
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.movie.data.model.MovieInfo r16, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r17) {
        /*
            r15 = this;
            java.lang.String r0 = r16.getName()
            java.lang.String r1 = "'"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)
            java.lang.String r0 = com.original.tase.helper.TitleHelper.g(r0)
            java.lang.String r1 = r16.getName()
            int r2 = r1.hashCode()
            r3 = 0
            r4 = 1
            switch(r2) {
                case -1438572772: goto L_0x0064;
                case -1296695595: goto L_0x005a;
                case -616025719: goto L_0x0050;
                case -519161968: goto L_0x0046;
                case -95314585: goto L_0x003c;
                case 268366428: goto L_0x0032;
                case 607165378: goto L_0x0028;
                case 704771935: goto L_0x001e;
                default: goto L_0x001d;
            }
        L_0x001d:
            goto L_0x006e
        L_0x001e:
            java.lang.String r2 = "Fast & Furious 6"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 5
            goto L_0x006f
        L_0x0028:
            java.lang.String r2 = "Now You See Me 2"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 2
            goto L_0x006f
        L_0x0032:
            java.lang.String r2 = "Self/less"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 3
            goto L_0x006f
        L_0x003c:
            java.lang.String r2 = "Star Wars: The Force Awakens"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 0
            goto L_0x006f
        L_0x0046:
            java.lang.String r2 = "Justice League"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 6
            goto L_0x006f
        L_0x0050:
            java.lang.String r2 = "Fast & Furious"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 4
            goto L_0x006f
        L_0x005a:
            java.lang.String r2 = "Mission: Impossible - Ghost Protocol"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 7
            goto L_0x006f
        L_0x0064:
            java.lang.String r2 = "Star Wars: The Last Jedi"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            r1 = 1
            goto L_0x006f
        L_0x006e:
            r1 = -1
        L_0x006f:
            switch(r1) {
                case 0: goto L_0x0086;
                case 1: goto L_0x0083;
                case 2: goto L_0x0080;
                case 3: goto L_0x007d;
                case 4: goto L_0x007a;
                case 5: goto L_0x0077;
                case 6: goto L_0x0074;
                case 7: goto L_0x0073;
                default: goto L_0x0072;
            }
        L_0x0072:
            goto L_0x0088
        L_0x0073:
            return
        L_0x0074:
            java.lang.String r0 = "the-justice-league-part-one"
            goto L_0x0088
        L_0x0077:
            java.lang.String r0 = "fast-furious-6"
            goto L_0x0088
        L_0x007a:
            java.lang.String r0 = "fast-furious"
            goto L_0x0088
        L_0x007d:
            java.lang.String r0 = "self-less"
            goto L_0x0088
        L_0x0080:
            java.lang.String r0 = "now-you-see-me-the-second-act"
            goto L_0x0088
        L_0x0083:
            java.lang.String r0 = "star-wars-episode-viii"
            goto L_0x0088
        L_0x0086:
            java.lang.String r0 = "star-wars-episode-vii-the-force-awakens"
        L_0x0088:
            java.lang.String r1 = "-"
            java.lang.String r0 = com.original.tase.helper.TitleHelper.a(r0, r1)
            r1 = r15
            java.util.ArrayList r2 = r15.a(r4)
            java.util.Iterator r2 = r2.iterator()
        L_0x0097:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x00de
            java.lang.Object r4 = r2.next()
            java.util.HashMap r4 = (java.util.HashMap) r4
            java.lang.String r5 = "url"
            java.lang.Object r5 = r4.get(r5)
            r7 = r5
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r5 = "urlLink"
            java.lang.Object r5 = r4.get(r5)
            java.lang.String r5 = (java.lang.String) r5
            java.lang.String r6 = "embedsPath"
            java.lang.Object r4 = r4.get(r6)
            r13 = r4
            java.lang.String r13 = (java.lang.String) r13
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r5)
            r4.append(r0)
            java.lang.String r8 = r4.toString()
            r10 = 1
            boolean[] r14 = new boolean[r3]
            java.lang.String r11 = "-1"
            java.lang.String r12 = "-1"
            r5 = r15
            r6 = r17
            r9 = r16
            boolean r4 = r5.a(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            if (r4 == 0) goto L_0x0097
        L_0x00de:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.FliXanityBased.a(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.movie.data.model.MovieInfo r16, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r17) {
        /*
            r15 = this;
            r10 = r16
            java.lang.String r0 = r16.getName()
            java.lang.String r1 = ""
            java.lang.String r2 = "DC's "
            java.lang.String r0 = r0.replace(r2, r1)
            java.lang.String r2 = "'"
            java.lang.String r0 = r0.replace(r2, r1)
            java.lang.String r0 = com.original.tase.helper.TitleHelper.g(r0)
            java.lang.String r1 = r16.getName()
            int r2 = r1.hashCode()
            r11 = 0
            switch(r2) {
                case -1989436052: goto L_0x0061;
                case -1753753522: goto L_0x0057;
                case -517469293: goto L_0x004d;
                case 91465286: goto L_0x0043;
                case 424581095: goto L_0x0039;
                case 1544707312: goto L_0x002f;
                case 1728305255: goto L_0x0025;
                default: goto L_0x0024;
            }
        L_0x0024:
            goto L_0x006b
        L_0x0025:
            java.lang.String r2 = "Marvel's Agents of S.H.I.E.L.D."
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006b
            r1 = 0
            goto L_0x006c
        L_0x002f:
            java.lang.String r2 = "Will & Grace"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006b
            r1 = 6
            goto L_0x006c
        L_0x0039:
            java.lang.String r2 = "Marvel's Iron Fist"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006b
            r1 = 1
            goto L_0x006c
        L_0x0043:
            java.lang.String r2 = "Marvel's The Punisher"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006b
            r1 = 4
            goto L_0x006c
        L_0x004d:
            java.lang.String r2 = "Marvel's Daredevil"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006b
            r1 = 3
            goto L_0x006c
        L_0x0057:
            java.lang.String r2 = "Marvel's Jessica Jones"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006b
            r1 = 5
            goto L_0x006c
        L_0x0061:
            java.lang.String r2 = "Marvel's The Defenders"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006b
            r1 = 2
            goto L_0x006c
        L_0x006b:
            r1 = -1
        L_0x006c:
            switch(r1) {
                case 0: goto L_0x0082;
                case 1: goto L_0x007f;
                case 2: goto L_0x007c;
                case 3: goto L_0x0079;
                case 4: goto L_0x0076;
                case 5: goto L_0x0073;
                case 6: goto L_0x0070;
                default: goto L_0x006f;
            }
        L_0x006f:
            goto L_0x0084
        L_0x0070:
            java.lang.String r0 = "will-and-grace"
            goto L_0x0084
        L_0x0073:
            java.lang.String r0 = "jessica-jones"
            goto L_0x0084
        L_0x0076:
            java.lang.String r0 = "the-punisher"
            goto L_0x0084
        L_0x0079:
            java.lang.String r0 = "marvels-daredevil"
            goto L_0x0084
        L_0x007c:
            java.lang.String r0 = "the-defenders"
            goto L_0x0084
        L_0x007f:
            java.lang.String r0 = "iron-fist"
            goto L_0x0084
        L_0x0082:
            java.lang.String r0 = "agents-of-shield"
        L_0x0084:
            java.lang.String r1 = "-"
            java.lang.String r12 = com.original.tase.helper.TitleHelper.a(r0, r1)
            r13 = r15
            java.util.ArrayList r0 = r15.a(r11)
            java.util.Iterator r14 = r0.iterator()
        L_0x0093:
            boolean r0 = r14.hasNext()
            if (r0 == 0) goto L_0x00ee
            java.lang.Object r0 = r14.next()
            java.util.HashMap r0 = (java.util.HashMap) r0
            java.lang.String r1 = "url"
            java.lang.Object r1 = r0.get(r1)
            r2 = r1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r1 = "urlLink"
            java.lang.Object r1 = r0.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r3 = "embedsPath"
            java.lang.Object r0 = r0.get(r3)
            r8 = r0
            java.lang.String r8 = (java.lang.String) r8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r1)
            r0.append(r12)
            java.lang.String r1 = "/season/"
            r0.append(r1)
            java.lang.String r1 = r10.session
            r0.append(r1)
            java.lang.String r1 = "/episode/"
            r0.append(r1)
            java.lang.String r1 = r10.eps
            r0.append(r1)
            java.lang.String r3 = r0.toString()
            r5 = 0
            java.lang.String r6 = r10.session
            java.lang.String r7 = r10.eps
            boolean[] r9 = new boolean[r11]
            r0 = r15
            r1 = r17
            r4 = r16
            boolean r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            if (r0 == 0) goto L_0x0093
        L_0x00ee:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.FliXanityBased.b(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    private ArrayList<HashMap<String, String>> a(boolean z) {
        Iterator<JsonElement> it2 = new JsonParser().a(HttpHelper.e().a("https://raw.githubusercontent.com/TeruSetephen/cinemaapk/master/provider/flixanityProviderUrlsv2.json", (Map<String, String>[]) new Map[0])).f().a("bases").e().iterator();
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        if (c.isEmpty()) {
            c = HttpHelper.e().a("https://raw.githubusercontent.com/TeruSetephen/cinemaapk/master/provider/flixanityProviderEnc.txt", (Map<String, String>[]) new Map[0]);
        }
        while (it2.hasNext()) {
            HashMap hashMap = new HashMap();
            JsonObject f = it2.next().f();
            String i = f.a(ReportDBAdapter.ReportColumns.COLUMN_URL).i();
            String i2 = f.a("embedsPath").i();
            String i3 = f.a("bdcCookie").i();
            hashMap.put("embedsPath", f.a("embedsPath").i());
            hashMap.put(ReportDBAdapter.ReportColumns.COLUMN_URL, i);
            if (z) {
                hashMap.put("urlLink", f.a("urlMovies").i());
            } else {
                hashMap.put("urlLink", f.a("urlSeries").i());
            }
            if (i2 != null && !i2.isEmpty() && i3 != null && !i3.isEmpty()) {
                HttpHelper.e().c(i, i3);
            }
            arrayList.add(hashMap);
        }
        return arrayList;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public boolean a(ObservableEmitter<? super MediaSource> observableEmitter, String str, String str2, MovieInfo movieInfo, boolean z, String str3, String str4, String str5, boolean... zArr) {
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put("accept-language", "en-US;q=0.9,en;q=0.8");
        String a2 = HttpHelper.e().a(str2, (Map<String, String>[]) new Map[]{hashMap});
        String a3 = Regex.a(a2, "var\\s*tok\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
        String a4 = Regex.a(a2, "elid\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
        if (a3.isEmpty()) {
            return false;
        }
        Duktape create = Duktape.create();
        try {
            create.set("console", console.class, new LOGG());
            String obj = create.evaluate(c).toString();
            create.close();
            HttpHelper e = HttpHelper.e();
            e.c(str2, a4 + "=" + obj);
            String str6 = !z ? "action=getEpisodeEmb&idEl=%s&token=%s&nopop=&elid=%s" : "action=getMovieEmb&idEl=%s&token=%s&nopop=&elid=%s";
            hashMap.put("origin", str);
            hashMap.put("accept", "application/x-www-form-urlencoded; charset=UTF-8");
            hashMap.put("referer", str2);
            hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, HttpHelper.e().a(str2));
            hashMap.put("x-requested-with", "XMLHttpRequest");
            HttpHelper e2 = HttpHelper.e();
            Iterator it2 = Regex.b(e2.a(str + "/ajax/" + str5, String.format(str6, new Object[]{a4, a3, obj}), (Map<String, String>[]) new Map[]{hashMap}).replace("\\\"", "\"").replace("\\/", "/"), "src\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0).iterator();
            HashMap hashMap2 = new HashMap();
            hashMap2.put("user-agent", Constants.f5838a);
            boolean z2 = false;
            while (it2.hasNext()) {
                String str7 = (String) it2.next();
                boolean k = GoogleVideoHelper.k(str7);
                MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                mediaSource.setStreamLink(str7);
                if (k) {
                    mediaSource.setPlayHeader(hashMap2);
                }
                mediaSource.setQuality(k ? GoogleVideoHelper.h(str7) : "HD");
                observableEmitter.onNext(mediaSource);
                z2 = true;
            }
            return z2;
        } catch (Throwable th) {
            create.close();
            throw th;
        }
    }
}
