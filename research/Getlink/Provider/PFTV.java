package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import org.jsoup.Jsoup;

public class PFTV extends BaseProvider {
    private String c = Utils.getProvider(62);

    public String a() {
        return "PFTV";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str = this.c + "/" + TitleHelper.g(TitleHelper.e(movieInfo.name)) + "-" + movieInfo.year + "/";
        String b = HttpHelper.e().b(str, this.c);
        if (!b.contains("callvalue")) {
            this.c = "https://projectfreetv.unblocked.mx";
            str = this.c + "/movies/" + TitleHelper.g(TitleHelper.e(movieInfo.name)) + "-" + movieInfo.year + "/";
            if (!b.contains("callvalue")) {
                this.c = "https://projectfreetv.bypassed.org";
                str = this.c + "/movies/" + TitleHelper.g(TitleHelper.e(movieInfo.name)) + "-" + movieInfo.year + "/";
                if (!b.contains("callvalue")) {
                    this.c = "https://projectfreetv.bypassed.bz";
                    str = this.c + "/movies/" + TitleHelper.g(TitleHelper.e(movieInfo.name)) + "-" + movieInfo.year + "/";
                }
            }
        }
        String a2 = Regex.a(b, "episode-date\\/\\d+\\/\"\\s+[0-9a-zA-Z-\"-=]+>\\d{4}", 1, 34);
        if (a2.isEmpty() || (com.original.tase.utils.Utils.b(a2) && a2.equals(movieInfo.year))) {
            a(observableEmitter, str);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str = this.c + "/episode/" + TitleHelper.g(TitleHelper.e(movieInfo.name.replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + movieInfo.session + "-episode-" + movieInfo.eps + "/";
        String b = HttpHelper.e().b(str, this.c);
        if (!b.contains("callvalue")) {
            this.c = "https://projectfreetv.unblocked.mx";
            str = this.c + "/episode/" + TitleHelper.g(TitleHelper.e(movieInfo.name.replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + movieInfo.eps + "/";
            b = HttpHelper.e().b(str, this.c);
            if (!b.contains("callvalue")) {
                this.c = "https://projectfreetv.bypassed.org";
                str = this.c + "/episode/" + TitleHelper.g(TitleHelper.e(movieInfo.name.replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + movieInfo.eps + "/";
                b = HttpHelper.e().b(str, this.c);
                if (!b.contains("callvalue")) {
                    this.c = "https://projectfreetv.bypassed.bz";
                    str = this.c + "/episode/" + TitleHelper.g(TitleHelper.e(movieInfo.name.replace("Marvel's ", "").replace("DC's ", ""))) + "-season-" + str + "-episode-" + movieInfo.eps + "/";
                    b = HttpHelper.e().b(str, this.c);
                }
            }
        }
        String a2 = Regex.a(b, "release-year\\/\\d+\\/\"\\s+[0-9a-zA-Z-\"-=]+>\\d{4}", 1, 34);
        if (a2.isEmpty() || (com.original.tase.utils.Utils.b(a2) && a2.equals(movieInfo.year))) {
            a(observableEmitter, str);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        try {
            String b = Jsoup.b(HttpHelper.e().b(str, this.c)).h("div.movieplay").h("iframe ").b("src");
            if (!b.isEmpty()) {
                a(observableEmitter, b, "HD", false);
            }
        } catch (Throwable unused) {
        }
    }
}
