package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class KickassTorrents extends BaseProvider {
    public String c = Utils.getProvider(4);

    public String a() {
        return "KickassTorrents";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:90:0x02ae, code lost:
        r16 = r2;
        r1 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(com.movie.data.model.MovieInfo r19, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r20) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            java.lang.String r2 = "1080p"
            java.lang.String r3 = "."
            java.lang.Integer r4 = r19.getType()
            int r4 = r4.intValue()
            r5 = 0
            r6 = 1
            if (r4 != r6) goto L_0x0016
            r4 = 1
            goto L_0x0017
        L_0x0016:
            r4 = 0
        L_0x0017:
            java.lang.String r7 = " "
            if (r4 == 0) goto L_0x002d
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r7)
            java.lang.String r9 = r1.year
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            goto L_0x005a
        L_0x002d:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = " S"
            r8.append(r9)
            java.lang.String r9 = r1.session
            int r9 = java.lang.Integer.parseInt(r9)
            java.lang.String r9 = com.original.tase.utils.Utils.a((int) r9)
            r8.append(r9)
            java.lang.String r9 = "E"
            r8.append(r9)
            java.lang.String r9 = r1.eps
            int r9 = java.lang.Integer.parseInt(r9)
            java.lang.String r9 = com.original.tase.utils.Utils.a((int) r9)
            r8.append(r9)
            java.lang.String r8 = r8.toString()
        L_0x005a:
            java.util.HashMap r9 = new java.util.HashMap
            r9.<init>()
            java.lang.String r10 = "accept"
            java.lang.String r11 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
            r9.put(r10, r11)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = r0.c
            r10.append(r11)
            java.lang.String r11 = "/"
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            java.lang.String r12 = "referer"
            r9.put(r12, r10)
            java.lang.String r10 = "upgrade-insecure-requests"
            java.lang.String r12 = "1"
            r9.put(r10, r12)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = r1.name
            r9.append(r10)
            r9.append(r8)
            java.lang.String r9 = r9.toString()
            boolean[] r10 = new boolean[r5]
            java.lang.String r9 = com.original.tase.utils.Utils.a((java.lang.String) r9, (boolean[]) r10)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r12 = r0.c
            r10.append(r12)
            java.lang.String r12 = "/usearch/"
            r10.append(r12)
            r10.append(r9)
            java.lang.String r9 = r10.toString()
            com.original.tase.helper.http.HttpHelper r10 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = r0.c
            r12.append(r13)
            r12.append(r11)
            java.lang.String r12 = r12.toString()
            java.lang.String r10 = r10.b((java.lang.String) r9, (java.lang.String) r12)
            org.jsoup.nodes.Document r10 = org.jsoup.Jsoup.b(r10)
            java.lang.String r12 = "div.torrentname"
            org.jsoup.select.Elements r10 = r10.g((java.lang.String) r12)
            java.util.Iterator r10 = r10.iterator()
            com.original.tase.helper.DirectoryIndexHelper r12 = new com.original.tase.helper.DirectoryIndexHelper
            r12.<init>()
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            java.util.HashMap r14 = new java.util.HashMap
            r14.<init>()
        L_0x00e8:
            boolean r15 = r10.hasNext()
            java.lang.String r5 = ""
            if (r15 == 0) goto L_0x019f
            java.lang.Object r15 = r10.next()
            org.jsoup.nodes.Element r15 = (org.jsoup.nodes.Element) r15
            java.lang.String r6 = "a"
            org.jsoup.nodes.Element r6 = r15.h(r6)
            java.lang.String r15 = "href"
            java.lang.String r6 = r6.b((java.lang.String) r15)
            java.lang.String r15 = r6.toLowerCase()
            java.lang.String r15 = com.original.tase.helper.TitleHelper.a(r15, r7)
            if (r4 == 0) goto L_0x014a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r16 = r10
            java.lang.String r10 = r1.name
            java.lang.String r10 = r10.toLowerCase()
            r5.append(r10)
            r5.append(r8)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = com.original.tase.helper.TitleHelper.a(r5, r7)
            boolean r5 = r15.contains(r5)
            if (r5 == 0) goto L_0x0147
            boolean r5 = r6.startsWith(r11)
            if (r5 == 0) goto L_0x0144
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r10 = r0.c
            r5.append(r10)
            r5.append(r6)
            java.lang.String r6 = r5.toString()
        L_0x0144:
            r14.put(r6, r15)
        L_0x0147:
            r17 = r13
            goto L_0x0197
        L_0x014a:
            r16 = r10
            java.lang.String r10 = r1.year
            java.lang.String r5 = r15.replace(r10, r5)
            java.lang.String r10 = "  "
            java.lang.String r5 = r5.replace(r10, r7)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r17 = r13
            java.lang.String r13 = r1.name
            java.lang.String r13 = r13.toLowerCase()
            r10.append(r13)
            java.lang.String r13 = r8.toLowerCase()
            r10.append(r13)
            java.lang.String r10 = r10.toString()
            java.lang.String r10 = com.original.tase.helper.TitleHelper.a(r10, r7)
            boolean r5 = r5.contains(r10)
            if (r5 == 0) goto L_0x0197
            boolean r5 = r6.startsWith(r11)
            if (r5 == 0) goto L_0x0194
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r10 = r0.c
            r5.append(r10)
            r5.append(r6)
            java.lang.String r6 = r5.toString()
        L_0x0194:
            r14.put(r6, r15)
        L_0x0197:
            r10 = r16
            r13 = r17
            r5 = 0
            r6 = 1
            goto L_0x00e8
        L_0x019f:
            r17 = r13
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.Set r1 = r14.entrySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x01ae:
            boolean r6 = r1.hasNext()
            if (r6 == 0) goto L_0x031b
            java.lang.Object r6 = r1.next()
            java.util.Map$Entry r6 = (java.util.Map.Entry) r6
            java.lang.Object r7 = r6.getKey()     // Catch:{ all -> 0x030d }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x030d }
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x030d }
            java.lang.String r7 = r8.b((java.lang.String) r7, (java.lang.String) r9)     // Catch:{ all -> 0x030d }
            java.lang.String r8 = "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?"
            r10 = 1
            java.lang.String r7 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r8, (int) r10)     // Catch:{ all -> 0x030d }
            boolean r8 = r7.isEmpty()     // Catch:{ all -> 0x030d }
            if (r8 == 0) goto L_0x01d6
            goto L_0x01ae
        L_0x01d6:
            java.lang.Object r6 = r6.getValue()     // Catch:{ all -> 0x030d }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x030d }
            java.lang.String r6 = com.original.tase.helper.TitleHelper.a(r6, r3)     // Catch:{ all -> 0x030d }
            java.lang.String r8 = ".."
            java.lang.String r6 = r6.replace(r8, r3)     // Catch:{ all -> 0x030d }
            java.lang.String r8 = r6.toUpperCase()     // Catch:{ all -> 0x030d }
            java.lang.String r10 = "(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)"
            java.lang.String r8 = r8.replaceAll(r10, r5)     // Catch:{ all -> 0x030d }
            java.lang.String r10 = "\\.|\\(|\\)|\\[|\\]|\\s|\\-"
            java.lang.String[] r8 = r8.split(r10)     // Catch:{ all -> 0x030d }
            int r10 = r8.length     // Catch:{ all -> 0x030d }
            java.lang.String r11 = "HQ"
            r14 = r11
            r13 = 0
        L_0x01fb:
            if (r13 >= r10) goto L_0x02b2
            r15 = r8[r13]     // Catch:{ all -> 0x030d }
            java.lang.String r15 = r15.toLowerCase()     // Catch:{ all -> 0x030d }
            r19 = r1
            java.lang.String r1 = "dvdscr"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "camrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "tsrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "hdcam"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "hdtc"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "hdts"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "dvdcam"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "dvdts"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "cam"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "telesync"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 != 0) goto L_0x02ae
            java.lang.String r1 = "ts"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030f }
            if (r1 == 0) goto L_0x025e
            goto L_0x02ae
        L_0x025e:
            boolean r1 = r15.contains(r2)     // Catch:{ all -> 0x030f }
            r16 = r2
            java.lang.String r2 = "720p"
            if (r1 != 0) goto L_0x02a4
            java.lang.String r1 = "1080"
            boolean r1 = r15.equals(r1)     // Catch:{ all -> 0x0311 }
            if (r1 == 0) goto L_0x0271
            goto L_0x02a4
        L_0x0271:
            boolean r1 = r15.contains(r2)     // Catch:{ all -> 0x0311 }
            if (r1 != 0) goto L_0x02a2
            java.lang.String r1 = "720"
            boolean r1 = r15.equals(r1)     // Catch:{ all -> 0x0311 }
            if (r1 == 0) goto L_0x0280
            goto L_0x02a2
        L_0x0280:
            java.lang.String r1 = "brrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0311 }
            if (r1 != 0) goto L_0x02a0
            java.lang.String r1 = "bdrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0311 }
            if (r1 != 0) goto L_0x02a0
            java.lang.String r1 = "hdrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0311 }
            if (r1 != 0) goto L_0x02a0
            java.lang.String r1 = "web-dl"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0311 }
            if (r1 == 0) goto L_0x02a6
        L_0x02a0:
            java.lang.String r2 = "HD"
        L_0x02a2:
            r14 = r2
            goto L_0x02a6
        L_0x02a4:
            r14 = r16
        L_0x02a6:
            int r13 = r13 + 1
            r1 = r19
            r2 = r16
            goto L_0x01fb
        L_0x02ae:
            r16 = r2
            r1 = 1
            goto L_0x02b7
        L_0x02b2:
            r19 = r1
            r16 = r2
            r1 = 0
        L_0x02b7:
            java.lang.String r2 = r18.a()     // Catch:{ all -> 0x0311 }
            if (r4 == 0) goto L_0x02c2
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r8 = r12.a(r6)     // Catch:{ all -> 0x0311 }
            goto L_0x02c6
        L_0x02c2:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r8 = r12.b(r6)     // Catch:{ all -> 0x0311 }
        L_0x02c6:
            if (r8 == 0) goto L_0x02dd
            java.lang.String r2 = r8.c()     // Catch:{ all -> 0x0311 }
            boolean r10 = r2.equalsIgnoreCase(r11)     // Catch:{ all -> 0x0311 }
            if (r10 != 0) goto L_0x02d3
            r14 = r2
        L_0x02d3:
            java.lang.String r2 = r8.b()     // Catch:{ all -> 0x0311 }
            r8 = 1
            java.lang.String r2 = r0.a((java.lang.String) r2, (boolean) r8)     // Catch:{ all -> 0x0311 }
            goto L_0x02de
        L_0x02dd:
            r8 = 1
        L_0x02de:
            java.lang.String r10 = "(magnet:\\?xt=urn:btih:[^&.]+)"
            java.lang.String r7 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r10, (int) r8)     // Catch:{ all -> 0x0311 }
            java.lang.String r7 = r7.toLowerCase()     // Catch:{ all -> 0x0311 }
            com.movie.data.model.realdebrid.MagnetObject r8 = new com.movie.data.model.realdebrid.MagnetObject     // Catch:{ all -> 0x0311 }
            if (r1 == 0) goto L_0x02fd
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0311 }
            r1.<init>()     // Catch:{ all -> 0x0311 }
            java.lang.String r10 = "CAM-"
            r1.append(r10)     // Catch:{ all -> 0x0311 }
            r1.append(r14)     // Catch:{ all -> 0x0311 }
            java.lang.String r14 = r1.toString()     // Catch:{ all -> 0x0311 }
        L_0x02fd:
            java.lang.String r1 = r18.a()     // Catch:{ all -> 0x0311 }
            r8.<init>(r2, r7, r14, r1)     // Catch:{ all -> 0x0311 }
            r8.setFileName(r6)     // Catch:{ all -> 0x0311 }
            r1 = r17
            r1.add(r8)     // Catch:{ all -> 0x0313 }
            goto L_0x0313
        L_0x030d:
            r19 = r1
        L_0x030f:
            r16 = r2
        L_0x0311:
            r1 = r17
        L_0x0313:
            r17 = r1
            r2 = r16
            r1 = r19
            goto L_0x01ae
        L_0x031b:
            r1 = r17
            int r2 = r1.size()
            if (r2 <= 0) goto L_0x033e
            com.original.tase.model.media.MediaSource r2 = new com.original.tase.model.media.MediaSource
            java.lang.String r3 = r18.a()
            r4 = 0
            r2.<init>(r3, r5, r4)
            r3 = 1
            r2.setTorrent(r3)
            r2.setMagnetObjects(r1)
            java.lang.String r1 = "magnet:KickassTorrents"
            r2.setStreamLink(r1)
            r1 = r20
            r1.onNext(r2)
        L_0x033e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.KickassTorrents.c(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }
}
