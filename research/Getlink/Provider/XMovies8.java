package com.utils.Getlink.Provider;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.ui.JavascriptBridge;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class XMovies8 extends BaseProvider {
    private String c = Utils.getProvider(60);
    String[] d = {"hserver", "vserver", "oserver"};

    public String a() {
        return "XMovies8";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, false);
        if (a2.isEmpty()) {
            a2 = this.c + "/movie/" + TitleHelper.g(movieInfo.name.replace("'", "-")) + "-" + movieInfo.year + "-1080p/watching.html";
        }
        a(observableEmitter, a2, movieInfo, false);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, true);
        if (a2.isEmpty()) {
            a2 = this.c + "/movie/" + TitleHelper.g(movieInfo.name.replace("'", "-")) + "-season-" + movieInfo.session + "-" + movieInfo.sessionYear + "-1080p/watching.html";
        }
        a(observableEmitter, a2, movieInfo, true);
    }

    private String a(MovieInfo movieInfo, boolean z) {
        String str;
        HttpHelper.e().c(this.c, "recaptcha=1");
        if (movieInfo.name.equals("Russian Lolita")) {
            movieInfo.name = "Russkaya Lolita";
        } else if (movieInfo.name.equals("House")) {
            movieInfo.name = "House M.D.";
        } else if (movieInfo.name.contains("Marvel's ")) {
            movieInfo.name = movieInfo.name.replace("Marvel's ", "");
        }
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", this.c + "/");
        hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        HttpHelper e = HttpHelper.e();
        StringBuilder sb = new StringBuilder();
        sb.append(this.c);
        sb.append("/movie/search/");
        StringBuilder sb2 = new StringBuilder();
        sb2.append(movieInfo.name);
        if (z) {
            str = " " + movieInfo.sessionYear;
        } else {
            str = "";
        }
        sb2.append(str.toLowerCase());
        sb.append(TitleHelper.a(sb2.toString(), "+"));
        Iterator it2 = Jsoup.b(e.a(sb.toString(), (Map<String, String>[]) new Map[]{hashMap})).g("div.ml-item").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a[href][title]");
            if (h != null) {
                String trim = h.b("href").trim();
                String b = h.b("title");
                String trim2 = Regex.a(b, "(.*?)\\s*\\((\\d{4})\\)", 1).trim();
                String trim3 = Regex.a(b, "(.*?)\\s*\\((\\d{4})\\)", 2).trim();
                h.G();
                if (Regex.a(b, "\\([^\\)]*(Trailer)[^\\)]*\\)", 1, 34).isEmpty()) {
                    trim2.isEmpty();
                    String trim4 = Regex.a(b.toLowerCase(), "Season\\s*(\\d+)", 1, 2).trim();
                    String d2 = TitleHelper.d(movieInfo.name);
                    if (!z) {
                        if (b.toLowerCase().equals(movieInfo.name.toLowerCase() + " (" + movieInfo.year + ")")) {
                            if (trim.endsWith("/")) {
                                return trim + "watching.html";
                            }
                            return trim + "/watching.html";
                        }
                    } else if (trim4.equals(movieInfo.session) && trim3.equals(movieInfo.sessionYear) && trim4.toLowerCase().contains(d2.toLowerCase())) {
                        if (trim.endsWith("/")) {
                            return trim + "watching.html";
                        }
                        return trim + "/watching.html";
                    }
                } else {
                    continue;
                }
            }
        }
        return "";
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo, boolean z) {
        String str2;
        String str3;
        String i;
        String str4;
        XMovies8 xMovies8 = this;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String str5 = str;
        MovieInfo movieInfo2 = movieInfo;
        if (!movieInfo2.eps.isEmpty() || !z) {
            HashMap hashMap = new HashMap();
            hashMap.put("Referer", str5);
            int i2 = 1;
            char c2 = 0;
            if (z) {
                String a2 = HttpHelper.e().a(str5, (Map<String, String>[]) new Map[]{hashMap});
                if (a2.isEmpty()) {
                    a2 = HttpHelper.e().a(str5, (Map<String, String>[]) new Map[]{hashMap});
                }
                Iterator it2 = Jsoup.b(a2).g("a[href*=\"episode_id\"]").iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Element element = (Element) it2.next();
                    if (element.toString().trim().replaceAll(" ", "").replaceAll(" ", "").toLowerCase().contains("episode" + movieInfo2.eps)) {
                        str5 = element.b("href");
                        break;
                    }
                }
                if (str5.isEmpty()) {
                    return;
                }
            }
            String str6 = str5;
            String a3 = HttpHelper.e().a(str6, (Map<String, String>[]) new Map[]{hashMap});
            if (a3.isEmpty()) {
                a3 = HttpHelper.e().a(str6, (Map<String, String>[]) new Map[]{hashMap});
            }
            String lowerCase = Jsoup.b(a3).h("span.quality").G().trim().toLowerCase();
            HashMap<String, String> a4 = Constants.a();
            a4.put("Referer", str6);
            a4.put(TheTvdb.HEADER_ACCEPT, "application/json, text/javascript, */*; q=0.01");
            a4.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US,en;q=0.5");
            a4.put("Origin", xMovies8.c);
            a4.put(TraktV2.HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded; charset=UTF-8");
            Iterator it3 = Regex.b(a3, "load_player\\(\\s*['\"]([^'\"]+)", 1).get(0).iterator();
            while (it3.hasNext()) {
                String str7 = (String) it3.next();
                if (!observableEmitter.isDisposed()) {
                    String[] strArr = xMovies8.d;
                    int length = strArr.length;
                    int i3 = 0;
                    while (i3 < length) {
                        String str8 = strArr[i3];
                        if (!observableEmitter.isDisposed()) {
                            try {
                                Object[] objArr = new Object[3];
                                objArr[c2] = str8;
                                objArr[i2] = str7;
                                objArr[2] = DateTimeHelper.c();
                                String format = String.format(xMovies8.c + "/ajax/v4_get_sources?s=%s&id=%s&_=%s", objArr);
                                HttpHelper e = HttpHelper.e();
                                Map[] mapArr = new Map[i2];
                                mapArr[c2] = a4;
                                String a5 = e.a(format, (Map<String, String>[]) mapArr);
                                if (!a5.isEmpty()) {
                                    JsonObject f = new JsonParser().a(a5).f();
                                    if (f.a(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS).a() && (((i = f.a("value").i()) != null && !i.isEmpty()) || (i = f.a((String) JavascriptBridge.MraidHandler.DOWNLOAD_ACTION).i()) == null || !i.isEmpty())) {
                                        String replace = i.replace("\\/", "/");
                                        if (replace.startsWith("//")) {
                                            replace = "https:" + replace;
                                        }
                                        if (GoogleVideoHelper.j(replace)) {
                                            HashMap<String, String> g = GoogleVideoHelper.g(replace);
                                            if (g != null && !g.isEmpty()) {
                                                for (Map.Entry next : g.entrySet()) {
                                                    str3 = lowerCase;
                                                    try {
                                                        MediaSource mediaSource = new MediaSource(a() + lowerCase, "GoogleVideo", false);
                                                        mediaSource.setOriginalLink(replace);
                                                        mediaSource.setStreamLink((String) next.getKey());
                                                        if (((String) next.getValue()).isEmpty()) {
                                                            str4 = " HD";
                                                        } else {
                                                            str4 = (String) next.getValue();
                                                        }
                                                        mediaSource.setQuality(str4);
                                                        HashMap hashMap2 = new HashMap();
                                                        hashMap2.put("User-Agent", Constants.f5838a);
                                                        hashMap2.put("Cookie", GoogleVideoHelper.c(replace, (String) next.getKey()));
                                                        mediaSource.setPlayHeader(hashMap2);
                                                        observableEmitter2.onNext(mediaSource);
                                                        lowerCase = str3;
                                                    } catch (Throwable th) {
                                                        th = th;
                                                        Logger.a(th, new boolean[0]);
                                                        i3++;
                                                        i2 = 1;
                                                        c2 = 0;
                                                        xMovies8 = this;
                                                        lowerCase = str2;
                                                    }
                                                }
                                            }
                                        } else {
                                            str2 = lowerCase;
                                            if (replace != null && !replace.contains("xmovies8")) {
                                                MediaSource mediaSource2 = new MediaSource(a(), "CDN-FastServer", false);
                                                mediaSource2.setQuality("HD");
                                                mediaSource2.setStreamLink(replace);
                                                HashMap hashMap3 = new HashMap();
                                                hashMap3.put("User-Agent", Constants.f5838a);
                                                hashMap3.put("referer", str6);
                                                mediaSource2.setPlayHeader(hashMap3);
                                                observableEmitter2.onNext(mediaSource2);
                                            }
                                            i3++;
                                            i2 = 1;
                                            c2 = 0;
                                            xMovies8 = this;
                                            lowerCase = str2;
                                        }
                                    }
                                }
                                str2 = lowerCase;
                            } catch (Throwable th2) {
                                th = th2;
                                str3 = lowerCase;
                                Logger.a(th, new boolean[0]);
                                i3++;
                                i2 = 1;
                                c2 = 0;
                                xMovies8 = this;
                                lowerCase = str2;
                            }
                            i3++;
                            i2 = 1;
                            c2 = 0;
                            xMovies8 = this;
                            lowerCase = str2;
                        } else {
                            return;
                        }
                    }
                    xMovies8 = this;
                } else {
                    return;
                }
            }
        }
    }
}
