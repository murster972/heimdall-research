package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class GoGoAnime extends BaseProvider {
    private String c = Utils.getProvider(91);
    private String d = (this.c + "/");
    private String e = this.d;

    public String a() {
        return "GoGoAnime";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        String str2;
        String str3;
        boolean z;
        boolean z2 = movieInfo.getType().intValue() == 1;
        HashMap hashMap = new HashMap();
        hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html, */*; q=0.01");
        hashMap.put("Origin", this.c);
        hashMap.put("referer", this.e);
        hashMap.put("user-agent", Constants.f5838a);
        Document b = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap}));
        Element h = b.h("div[class=anime_info_episodes_next]");
        String b2 = h.h("input[id=movie_id]").b("value");
        String b3 = h.h("input[id=default_ep]").b("value");
        String b4 = h.h("input[id=alias_anime]").b("value");
        Element h2 = b.h("div[class=anime_video_body]").h("ul[id=episode_page]").h("a");
        Document b5 = Jsoup.b(HttpHelper.e().a("https://ajax.apimovie.xyz/ajax/load-list-episode?ep_start=" + h2.b("ep_start") + "&ep_end=" + h2.b("ep_end") + "&id=" + b2 + "&default_ep=" + b3 + "&alias=" + b4 + "##forceNoCache##", (Map<String, String>[]) new Map[]{hashMap}));
        if (z2) {
            str2 = b5.h("ul[id=episode_related]").h("a[href]").b("href");
            if (str2.startsWith("/") || str2.startsWith(" /")) {
                str2 = this.c + str2.replace(" /", "/");
            }
        } else {
            Iterator it2 = b5.g("ul[id=episode_related]").b("a[href]").iterator();
            while (true) {
                if (!it2.hasNext()) {
                    str3 = str;
                    z = false;
                    break;
                }
                Element element = (Element) it2.next();
                str3 = element.b("href");
                if (element.h("div[class=name]").G().equalsIgnoreCase("EP " + movieInfo.eps)) {
                    if (str3.startsWith("/") || str3.startsWith(" /")) {
                        str3 = this.c + str3.replace(" /", "/");
                    }
                    z = true;
                } else {
                    str = str3;
                }
            }
            if (z) {
                str2 = str3;
            } else {
                return;
            }
        }
        Iterator it3 = Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[]{hashMap})).g("div[class=anime_muti_link]").b("li").iterator();
        while (it3.hasNext()) {
            String b6 = ((Element) it3.next()).h("a[data-video]").b("data-video");
            if (b6.startsWith("//")) {
                b6 = "https:" + b6;
            }
            a(observableEmitter, b6, "HD", false);
        }
    }

    public String b(MovieInfo movieInfo) {
        if (!a(movieInfo.genres)) {
            return "";
        }
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        String a2 = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]);
        this.e = this.d + "/search.html?keyword=" + a2;
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.d);
        hashMap.put("Cookie", HttpHelper.e().a(this.c));
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.e, (Map<String, String>[]) new Map[]{hashMap})).g("div[class=last_episodes]").b("li").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.h("a").b("href");
            String G = element.h("p[class=name]").h("a").G();
            String G2 = element.h("p[class=released]").G();
            String replaceAll = G.replaceAll("([ ]*Movie.*[0-9]+)", "");
            String replaceAll2 = G.replaceAll("([ ]*Movie.*[0-9].*(The)+)", ":");
            if ((replaceAll.equalsIgnoreCase(movieInfo.name) || replaceAll2.equalsIgnoreCase(movieInfo.name)) && G2.contains(movieInfo.year)) {
                if (!b.startsWith("/")) {
                    return b;
                }
                return this.c + b;
            }
        }
        return "";
    }
}
