package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import okhttp3.internal.cache.DiskLruCache;

public class TV21 extends BaseProvider {
    private String c = Utils.getProvider(70);

    private String b(MovieInfo movieInfo) {
        String replace = movieInfo.getName().replace("Marvel's ", "").replace("DC's ", "");
        String replace2 = this.c.replace("https://", "http://");
        String b = HttpHelper.e().b("https://google.ch/search?q=" + com.original.tase.utils.Utils.a(replace, new boolean[0]).replace("%20", "+") + "+" + movieInfo.getYear() + "+site:" + replace2, "https://google.ch");
        "kl=us-en&b=&q=" + com.original.tase.utils.Utils.a(replace + " " + movieInfo.getYear() + " site:" + replace2.replace("https://", "").replace("http://", ""), new boolean[0]);
        HashMap hashMap = new HashMap();
        hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US");
        String str = b;
        hashMap.put("Origin", "https://duckduckgo.com");
        hashMap.put("Referer", "https://duckduckgo.com/");
        hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        HttpHelper e = HttpHelper.e();
        StringBuilder sb = new StringBuilder();
        sb.append("https://www.bing.com/search?q=");
        Object obj = TheTvdb.HEADER_ACCEPT_LANGUAGE;
        sb.append(com.original.tase.utils.Utils.a(replace, new boolean[0]).replace("%20", "+"));
        sb.append("+");
        sb.append(movieInfo.getYear());
        sb.append("+site:");
        sb.append(replace2);
        String b2 = e.b(sb.toString(), "https://www.bing.com");
        "cmd=process_search&language=english&enginecount=1&pl=&abp=1&hmb=1&ff=&theme=&flag_ac=0&cat=web&ycc=0&t=air&nj=0&query=" + com.original.tase.utils.Utils.a(replace + " " + movieInfo.getYear() + " " + replace2.replace("https://", "").replace("http://", ""), new boolean[0]);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap2.put(obj, "en-US");
        hashMap2.put("Host", "www.startpage.com");
        hashMap2.put("Origin", "https://www.startpage.com");
        hashMap2.put("Referer", "https://www.startpage.com/do/asearch");
        hashMap2.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        hashMap2.put("User-Agent", Constants.f5838a);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(str);
        arrayList.add(b2.replaceAll("(</?\\w{1,7}>)", ""));
        String str2 = "/(" + TitleHelper.g(replace.replace("$", "\\$").replace("*", "\\*").replace("(", "\\(").replace(")", "\\)").replace("[", "\\[").replace("]", "\\]")) + "-" + movieInfo.getYear() + ")(?:/|\\?|$)";
        for (String b3 : arrayList) {
            Iterator it2 = Regex.b(b3, "href=['\"](.+?)['\"]", 1, true).get(0).iterator();
            while (true) {
                if (it2.hasNext()) {
                    String str3 = (String) it2.next();
                    try {
                        if (str3.startsWith(UriUtil.HTTP_SCHEME) && str3.contains("tv21") && !str3.contains("/tag/") && !str3.contains("//translate.") && !str3.contains("startpage.com") && str3.replace("https://", "http://").contains(replace2) && !Regex.a(str3.trim().toLowerCase(), str2, 1, 2).isEmpty()) {
                            return str3;
                        }
                    } catch (Exception e2) {
                        Logger.a((Throwable) e2, new boolean[0]);
                    }
                }
            }
        }
        return "";
    }

    public String a() {
        return "TV21";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:130:0x04d1  */
    /* JADX WARNING: Removed duplicated region for block: B:351:0x04ea A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r28, com.movie.data.model.MovieInfo r29) {
        /*
            r27 = this;
            r1 = r27
            r2 = r28
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r3 = r1.c
            java.lang.String r4 = com.original.Constants.f5838a
            r5 = 0
            java.util.Map[] r6 = new java.util.Map[r5]
            java.lang.String r7 = ""
            java.lang.String r0 = r0.a((java.lang.String) r3, (java.lang.String) r4, (java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r6)
            java.lang.String r3 = "Please complete the security check to access"
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x0030
            com.original.tase.RxBus r0 = com.original.tase.RxBus.b()
            com.original.tase.event.ReCaptchaRequiredEvent r2 = new com.original.tase.event.ReCaptchaRequiredEvent
            java.lang.String r3 = r27.a()
            java.lang.String r4 = r1.c
            r2.<init>(r3, r4)
            r0.a(r2)
            return
        L_0x0030:
            boolean r0 = com.original.tase.helper.http.sucuri.SucuriCloudProxyHelper.c(r0)
            if (r0 == 0) goto L_0x003b
            java.lang.String r0 = r1.c
            com.original.tase.helper.http.sucuri.SucuriCloudProxyHelper.b(r0, r0)
        L_0x003b:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r1.c
            r3.append(r4)
            java.lang.String r4 = "/?s="
            r3.append(r4)
            java.lang.String r6 = r29.getName()
            boolean[] r8 = new boolean[r5]
            java.lang.String r6 = com.original.tase.utils.Utils.a((java.lang.String) r6, (boolean[]) r8)
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            java.lang.String r6 = com.original.Constants.f5838a
            java.lang.String r8 = r1.c
            java.util.Map[] r9 = new java.util.Map[r5]
            java.lang.String r0 = r0.a((java.lang.String) r3, (java.lang.String) r6, (java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r9)
            boolean r3 = r0.isEmpty()
            if (r3 == 0) goto L_0x00a0
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = r1.c
            r3.append(r6)
            r3.append(r4)
            java.lang.String r4 = r29.getName()
            boolean[] r6 = new boolean[r5]
            java.lang.String r4 = com.original.tase.utils.Utils.a((java.lang.String) r4, (boolean[]) r6)
            r3.append(r4)
            java.lang.String r4 = "&search=advanced&post_type=&index=&orderby=&genre=&movieyear=&country=&quality="
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = com.original.Constants.f5838a
            java.lang.String r6 = r1.c
            java.util.Map[] r8 = new java.util.Map[r5]
            java.lang.String r0 = r0.a((java.lang.String) r3, (java.lang.String) r4, (java.lang.String) r6, (java.util.Map<java.lang.String, java.lang.String>[]) r8)
        L_0x00a0:
            boolean r3 = r0.isEmpty()
            java.lang.String r4 = "a[href]"
            r6 = 2
            r8 = 1
            if (r3 != 0) goto L_0x0132
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r3 = "article[id*=\"post-\"]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r3)
            java.util.Iterator r3 = r0.iterator()
        L_0x00b8:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0132
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x012b }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ Exception -> 0x012b }
            java.lang.String r9 = "h2.entry-title"
            org.jsoup.nodes.Element r0 = r0.h(r9)     // Catch:{ Exception -> 0x012b }
            if (r0 == 0) goto L_0x00b8
            org.jsoup.nodes.Element r0 = r0.h(r4)     // Catch:{ Exception -> 0x012b }
            if (r0 == 0) goto L_0x00b8
            java.lang.String r9 = "href"
            java.lang.String r9 = r0.b((java.lang.String) r9)     // Catch:{ Exception -> 0x012b }
            java.lang.String r0 = r0.G()     // Catch:{ Exception -> 0x012b }
            java.lang.String r10 = "(.*?)\\s*\\((\\d{4})\\)"
            java.lang.String r10 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r10, (int) r8)     // Catch:{ Exception -> 0x012b }
            java.lang.String r11 = "(.*?)\\s*\\((\\d{4})\\)"
            java.lang.String r11 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r11, (int) r6)     // Catch:{ Exception -> 0x012b }
            boolean r12 = r10.isEmpty()     // Catch:{ Exception -> 0x012b }
            if (r12 == 0) goto L_0x00ef
            goto L_0x00f0
        L_0x00ef:
            r0 = r10
        L_0x00f0:
            java.lang.String r0 = com.original.tase.helper.TitleHelper.f(r0)     // Catch:{ Exception -> 0x012b }
            java.lang.String r10 = r29.getName()     // Catch:{ Exception -> 0x012b }
            java.lang.String r10 = com.original.tase.helper.TitleHelper.f(r10)     // Catch:{ Exception -> 0x012b }
            boolean r0 = r0.equals(r10)     // Catch:{ Exception -> 0x012b }
            if (r0 == 0) goto L_0x00b8
            java.lang.String r0 = r11.trim()     // Catch:{ Exception -> 0x012b }
            boolean r0 = r0.isEmpty()     // Catch:{ Exception -> 0x012b }
            if (r0 != 0) goto L_0x0133
            java.lang.String r0 = r11.trim()     // Catch:{ Exception -> 0x012b }
            boolean r0 = com.original.tase.utils.Utils.b(r0)     // Catch:{ Exception -> 0x012b }
            if (r0 == 0) goto L_0x0133
            java.lang.Integer r0 = r29.getYear()     // Catch:{ Exception -> 0x012b }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x012b }
            if (r0 <= 0) goto L_0x0133
            r11.trim()     // Catch:{ Exception -> 0x012b }
            java.lang.Integer r0 = r29.getYear()     // Catch:{ Exception -> 0x012b }
            r0.intValue()     // Catch:{ Exception -> 0x012b }
            goto L_0x0133
        L_0x012b:
            r0 = move-exception
            boolean[] r9 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)
            goto L_0x00b8
        L_0x0132:
            r9 = r7
        L_0x0133:
            boolean r0 = r9.isEmpty()
            java.lang.String r3 = "http"
            java.lang.String r10 = "player"
            java.lang.String r11 = "http:"
            java.lang.String r12 = "//"
            java.lang.String r13 = "/"
            if (r0 == 0) goto L_0x0291
            r14 = r29
            java.lang.String r9 = r1.b(r14)
            boolean r0 = r9.isEmpty()
            if (r0 == 0) goto L_0x02d9
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r9 = r1.c
            r0.append(r9)
            r0.append(r13)
            java.lang.String r9 = r29.getName()
            java.lang.String r9 = com.original.tase.helper.TitleHelper.g(r9)
            r0.append(r9)
            java.lang.String r9 = "-"
            r0.append(r9)
            java.lang.Integer r15 = r29.getYear()
            r0.append(r15)
            r0.append(r13)
            java.lang.String r0 = r0.toString()
            com.original.tase.helper.http.HttpHelper r15 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r6 = com.original.Constants.f5838a
            java.lang.String r8 = r1.c
            java.util.Map[] r14 = new java.util.Map[r5]
            java.lang.String r6 = r15.a((java.lang.String) r0, (java.lang.String) r6, (java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r14)
            java.lang.String r6 = r6.toLowerCase()
            boolean r6 = r6.contains(r10)
            if (r6 != 0) goto L_0x028f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r6 = r1.c
            r0.append(r6)
            r0.append(r13)
            java.lang.String r6 = r29.getName()
            java.lang.String r6 = com.original.tase.helper.TitleHelper.g(r6)
            r0.append(r6)
            r0.append(r9)
            java.lang.Integer r6 = r29.getYear()
            int r6 = r6.intValue()
            r8 = 1
            int r6 = r6 + r8
            r0.append(r6)
            r0.append(r13)
            java.lang.String r0 = r0.toString()
            com.original.tase.helper.http.HttpHelper r6 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r8 = com.original.Constants.f5838a
            java.lang.String r14 = r1.c
            java.util.Map[] r15 = new java.util.Map[r5]
            java.lang.String r6 = r6.a((java.lang.String) r0, (java.lang.String) r8, (java.lang.String) r14, (java.util.Map<java.lang.String, java.lang.String>[]) r15)
            java.lang.String r6 = r6.toLowerCase()
            boolean r6 = r6.contains(r10)
            if (r6 != 0) goto L_0x028f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r6 = r1.c
            r0.append(r6)
            r0.append(r13)
            java.lang.String r6 = r29.getName()
            java.lang.String r6 = com.original.tase.helper.TitleHelper.g(r6)
            r0.append(r6)
            r0.append(r9)
            java.lang.Integer r6 = r29.getYear()
            int r6 = r6.intValue()
            r8 = 1
            int r6 = r6 - r8
            r0.append(r6)
            r0.append(r13)
            java.lang.String r9 = r0.toString()
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r6 = com.original.Constants.f5838a
            java.lang.String r8 = r1.c
            java.util.Map[] r14 = new java.util.Map[r5]
            java.lang.String r0 = r0.a((java.lang.String) r9, (java.lang.String) r6, (java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r14)
            java.lang.String r0 = r0.toLowerCase()
            boolean r0 = r0.contains(r10)
            if (r0 != 0) goto L_0x02d9
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = r1.c
            r0.append(r2)
            r0.append(r13)
            java.lang.String r2 = r29.getName()
            java.lang.String r2 = com.original.tase.helper.TitleHelper.g(r2)
            r0.append(r2)
            r0.append(r13)
            java.lang.String r0 = r0.toString()
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r3 = com.original.Constants.f5838a
            java.lang.String r4 = r1.c
            java.util.Map[] r5 = new java.util.Map[r5]
            java.lang.String r0 = r2.a((java.lang.String) r0, (java.lang.String) r3, (java.lang.String) r4, (java.util.Map<java.lang.String, java.lang.String>[]) r5)
            java.lang.String r2 = r0.toLowerCase()
            boolean r2 = r2.contains(r10)
            if (r2 == 0) goto L_0x028e
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r2 = "h1.entry-title"
            org.jsoup.nodes.Element r0 = r0.h(r2)
            if (r0 != 0) goto L_0x0264
            return
        L_0x0264:
            java.lang.String r0 = r0.G()
            java.lang.String r2 = " "
            java.lang.String r0 = r0.replace(r2, r7)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "("
            r2.append(r3)
            java.lang.Integer r3 = r29.getYear()
            r2.append(r3)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            boolean r0 = r0.contains(r2)
            if (r0 != 0) goto L_0x028e
        L_0x028e:
            return
        L_0x028f:
            r9 = r0
            goto L_0x02d9
        L_0x0291:
            boolean r0 = r9.startsWith(r12)
            if (r0 == 0) goto L_0x02a7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r11)
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            goto L_0x02d9
        L_0x02a7:
            boolean r0 = r9.startsWith(r13)
            if (r0 == 0) goto L_0x02bf
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r6 = r1.c
            r0.append(r6)
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            goto L_0x02d9
        L_0x02bf:
            boolean r0 = r9.startsWith(r3)
            if (r0 != 0) goto L_0x02d9
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r6 = r1.c
            r0.append(r6)
            r0.append(r13)
            r0.append(r9)
            java.lang.String r9 = r0.toString()
        L_0x02d9:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r6 = com.original.Constants.f5838a
            java.util.Map[] r8 = new java.util.Map[r5]
            java.lang.String r0 = r0.a((java.lang.String) r9, (java.lang.String) r6, (java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r8)
            boolean r6 = r0.isEmpty()
            if (r6 != 0) goto L_0x02f3
            java.lang.String r6 = "Sorry, you have been blocked"
            boolean r6 = r0.contains(r6)
            if (r6 == 0) goto L_0x0301
        L_0x02f3:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r6 = com.original.Constants.f5838a
            java.lang.String r8 = r1.c
            java.util.Map[] r14 = new java.util.Map[r5]
            java.lang.String r0 = r0.a((java.lang.String) r9, (java.lang.String) r6, (java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r14)
        L_0x0301:
            boolean r6 = r0.isEmpty()
            if (r6 == 0) goto L_0x0308
            return
        L_0x0308:
            r6 = 34
            java.lang.String r8 = "['\"]?post_id['\"]?\\s*:\\s*['\"]?([^'\"]+)['\"]?"
            r14 = 1
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r8, (int) r14, (int) r6)
            boolean r15 = r8.isEmpty()
            if (r15 == 0) goto L_0x0324
            java.lang.String r8 = "\\s+postid-([^\\s]+)\\s+"
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r8, (int) r14, (int) r6)
            boolean r14 = r8.isEmpty()
            if (r14 == 0) goto L_0x0324
            return
        L_0x0324:
            java.util.HashMap r14 = com.original.Constants.a()
            java.lang.String r15 = r1.c
            java.lang.String r6 = "Origin"
            r14.put(r6, r15)
            java.lang.String r6 = "Referer"
            r14.put(r6, r9)
            java.lang.String r6 = com.original.Constants.f5838a
            java.lang.String r15 = "User-Agent"
            r14.put(r15, r6)
            org.jsoup.nodes.Document r6 = org.jsoup.Jsoup.b(r0)
            java.lang.String r0 = "a[id*=\"player\"]"
            org.jsoup.select.Elements r0 = r6.g((java.lang.String) r0)
            java.util.Iterator r18 = r0.iterator()
            r0 = 1
        L_0x034a:
            boolean r19 = r18.hasNext()
            java.lang.String r5 = "HD"
            if (r19 == 0) goto L_0x0649
            java.lang.Object r19 = r18.next()
            org.jsoup.nodes.Element r19 = (org.jsoup.nodes.Element) r19
            java.lang.String r19 = r19.G()
            boolean r20 = r19.isEmpty()
            if (r20 != 0) goto L_0x062e
            r20 = r7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r10)
            int r21 = r0 + 1
            r7.append(r0)
            java.lang.String r0 = r7.toString()
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            r22 = r10
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r23 = r4
            java.lang.String r4 = r1.c
            r10.append(r4)
            java.lang.String r4 = "/wp-admin/admin-ajax.php"
            r10.append(r4)
            java.lang.String r4 = r10.toString()
            r24 = r6
            r10 = 2
            java.lang.Object[] r6 = new java.lang.Object[r10]
            r10 = 0
            r6[r10] = r0
            r10 = 1
            r6[r10] = r8
            java.lang.String r0 = "action=muvipro_player_content&tab=%s&post_id=%s"
            java.lang.String r0 = java.lang.String.format(r0, r6)
            java.util.Map[] r6 = new java.util.Map[r10]
            r17 = 0
            r6[r17] = r14
            java.lang.String r0 = r7.a((java.lang.String) r4, (java.lang.String) r0, (boolean) r10, (java.util.Map<java.lang.String, java.lang.String>[]) r6)
            java.lang.String r4 = "src\\s*=\\s*['\"]([^'\"]+)['\"]"
            r6 = 34
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r4, (int) r10, (int) r6)
            boolean r4 = r0.isEmpty()
            if (r4 == 0) goto L_0x03f1
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = r1.c
            r4.append(r6)
            java.lang.String r6 = "/wp-admin/admin-ajax.php"
            r4.append(r6)
            java.lang.String r4 = r4.toString()
            r6 = 2
            java.lang.Object[] r7 = new java.lang.Object[r6]
            r6 = 0
            r7[r6] = r19
            r10 = 1
            r7[r10] = r8
            java.lang.String r6 = "action=muvipro_player_content&tab=%s&post_id=%s"
            java.lang.String r6 = java.lang.String.format(r6, r7)
            java.util.Map[] r7 = new java.util.Map[r10]
            r17 = 0
            r7[r17] = r14
            java.lang.String r0 = r0.a((java.lang.String) r4, (java.lang.String) r6, (boolean) r10, (java.util.Map<java.lang.String, java.lang.String>[]) r7)
            java.lang.String r4 = "src\\s*=\\s*['\"]([^'\"]+)['\"]"
            r6 = 34
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r4, (int) r10, (int) r6)
        L_0x03f1:
            boolean r4 = r0.isEmpty()
            if (r4 != 0) goto L_0x0627
            boolean r4 = r0.startsWith(r12)
            if (r4 == 0) goto L_0x040e
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r11)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
        L_0x040c:
            r4 = r0
            goto L_0x0441
        L_0x040e:
            boolean r4 = r0.startsWith(r13)
            if (r4 == 0) goto L_0x0426
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = r1.c
            r4.append(r6)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            goto L_0x040c
        L_0x0426:
            boolean r4 = r0.startsWith(r3)
            if (r4 != 0) goto L_0x040c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = r1.c
            r4.append(r6)
            r4.append(r13)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            goto L_0x040c
        L_0x0441:
            java.lang.String r0 = r4.toLowerCase()
            java.lang.String r6 = "haxhits."
            boolean r0 = r0.contains(r6)
            if (r0 == 0) goto L_0x061c
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r6 = com.original.Constants.f5838a
            r7 = 0
            java.util.Map[] r10 = new java.util.Map[r7]
            java.lang.String r0 = r0.a((java.lang.String) r4, (java.lang.String) r6, (java.lang.String) r9, (java.util.Map<java.lang.String, java.lang.String>[]) r10)
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r6.add(r0)
            java.lang.String r10 = "atob"
            boolean r10 = r0.contains(r10)
            if (r10 == 0) goto L_0x04f3
            java.lang.String r10 = "atob\\s*\\(\\s*['\"]([^'\"]+)['\"]"
            r19 = r8
            r8 = 1
            java.util.ArrayList r10 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r10, (int) r8, (boolean) r8)
            java.lang.Object r8 = r10.get(r7)
            java.util.ArrayList r8 = (java.util.ArrayList) r8
            java.util.Iterator r7 = r8.iterator()
            r8 = r0
        L_0x047e:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x04ef
            java.lang.Object r0 = r7.next()
            r10 = r0
            java.lang.String r10 = (java.lang.String) r10
            boolean r0 = r10.isEmpty()
            if (r0 != 0) goto L_0x04e6
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x04a6 }
            r25 = r7
            r26 = r9
            r7 = 0
            byte[] r9 = android.util.Base64.decode(r10, r7)     // Catch:{ Exception -> 0x04a4 }
            java.lang.String r7 = "UTF-8"
            r0.<init>(r9, r7)     // Catch:{ Exception -> 0x04a2 }
            goto L_0x04cb
        L_0x04a2:
            r0 = move-exception
            goto L_0x04ab
        L_0x04a4:
            r0 = move-exception
            goto L_0x04ac
        L_0x04a6:
            r0 = move-exception
            r25 = r7
            r26 = r9
        L_0x04ab:
            r7 = 0
        L_0x04ac:
            boolean[] r9 = new boolean[r7]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x04bb }
            byte[] r9 = android.util.Base64.decode(r10, r7)     // Catch:{ Exception -> 0x04bb }
            r0.<init>(r9)     // Catch:{ Exception -> 0x04bb }
            goto L_0x04cb
        L_0x04bb:
            r0 = move-exception
            r7 = 0
            boolean[] r9 = new boolean[r7]     // Catch:{ Exception -> 0x04c3 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)     // Catch:{ Exception -> 0x04c3 }
            goto L_0x04c9
        L_0x04c3:
            r0 = move-exception
            boolean[] r9 = new boolean[r7]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)
        L_0x04c9:
            r0 = r20
        L_0x04cb:
            boolean r7 = r0.isEmpty()
            if (r7 != 0) goto L_0x04ea
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r8)
            java.lang.String r8 = "\n"
            r7.append(r8)
            r7.append(r0)
            java.lang.String r8 = r7.toString()
            goto L_0x04ea
        L_0x04e6:
            r25 = r7
            r26 = r9
        L_0x04ea:
            r7 = r25
            r9 = r26
            goto L_0x047e
        L_0x04ef:
            r26 = r9
            r0 = r8
            goto L_0x04f7
        L_0x04f3:
            r19 = r8
            r26 = r9
        L_0x04f7:
            boolean r7 = com.original.tase.helper.js.JsUnpacker.m30920(r0)
            if (r7 == 0) goto L_0x0504
            java.util.ArrayList r0 = com.original.tase.helper.js.JsUnpacker.m30916(r0)
            r6.addAll(r0)
        L_0x0504:
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.util.Iterator r8 = r6.iterator()
        L_0x050d:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x055d
            java.lang.Object r0 = r8.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r9 = "['\"]([^'\"]*//[^'\"]*haxhits\\.[^'\"]+gdata[^'\"]+)['\"]"
            r16 = r8
            r8 = 1
            r10 = 2
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r9, (int) r8, (int) r10)     // Catch:{ Exception -> 0x054e }
            r8 = 0
            java.lang.Object r0 = r0.get(r8)     // Catch:{ Exception -> 0x054c }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x054e }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x054e }
        L_0x052e:
            boolean r8 = r0.hasNext()     // Catch:{ Exception -> 0x054e }
            if (r8 == 0) goto L_0x055a
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x054e }
            java.lang.Object r9 = r0.next()     // Catch:{ Exception -> 0x054e }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ Exception -> 0x054e }
            java.lang.String r8 = r8.b((java.lang.String) r9, (java.lang.String) r4)     // Catch:{ Exception -> 0x054e }
            boolean r9 = r8.isEmpty()     // Catch:{ Exception -> 0x054e }
            if (r9 != 0) goto L_0x052e
            r7.add(r8)     // Catch:{ Exception -> 0x054e }
            goto L_0x052e
        L_0x054c:
            r0 = move-exception
            goto L_0x0555
        L_0x054e:
            r0 = move-exception
            goto L_0x0554
        L_0x0550:
            r0 = move-exception
            r16 = r8
            r10 = 2
        L_0x0554:
            r8 = 0
        L_0x0555:
            boolean[] r9 = new boolean[r8]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)
        L_0x055a:
            r8 = r16
            goto L_0x050d
        L_0x055d:
            r10 = 2
            r6.addAll(r7)
            java.util.ArrayList r0 = com.original.tase.utils.Utils.a(r6)
            java.util.Iterator r4 = r0.iterator()
        L_0x0569:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x062b
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r6 = "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*['\"]?type['\"]?\\s*:\\s*['\"]?video"
            r7 = 34
            r8 = 1
            java.util.ArrayList r6 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r6, (int) r8, (int) r7)     // Catch:{ Exception -> 0x060e }
            r9 = 0
            java.lang.Object r6 = r6.get(r9)     // Catch:{ Exception -> 0x060e }
            java.util.ArrayList r6 = (java.util.ArrayList) r6     // Catch:{ Exception -> 0x060e }
            java.lang.String r10 = "['\"]?type['\"]?\\s*:\\s*['\"]video[^'\"]*['\"]\\s*,\\s*['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]"
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r10, (int) r8, (int) r7)     // Catch:{ Exception -> 0x060e }
            java.lang.Object r0 = r0.get(r9)     // Catch:{ Exception -> 0x060e }
            java.util.Collection r0 = (java.util.Collection) r0     // Catch:{ Exception -> 0x060e }
            r6.addAll(r0)     // Catch:{ Exception -> 0x060e }
            java.util.ArrayList r0 = com.original.tase.utils.Utils.a(r6)     // Catch:{ Exception -> 0x060e }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x060e }
        L_0x059c:
            boolean r6 = r0.hasNext()     // Catch:{ Exception -> 0x060e }
            if (r6 == 0) goto L_0x060a
            java.lang.Object r6 = r0.next()     // Catch:{ Exception -> 0x060e }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x060e }
            boolean r7 = r6.isEmpty()     // Catch:{ Exception -> 0x060e }
            if (r7 != 0) goto L_0x0605
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.k(r6)     // Catch:{ Exception -> 0x060e }
            com.original.tase.model.media.MediaSource r8 = new com.original.tase.model.media.MediaSource     // Catch:{ Exception -> 0x060e }
            java.lang.String r9 = r27.a()     // Catch:{ Exception -> 0x060e }
            if (r7 == 0) goto L_0x05bd
            java.lang.String r10 = "GoogleVideo"
            goto L_0x05bf
        L_0x05bd:
            java.lang.String r10 = "CDN-FastServer"
        L_0x05bf:
            r25 = r4
            r4 = 0
            r8.<init>(r9, r10, r4)     // Catch:{ Exception -> 0x0603 }
            r8.setStreamLink(r6)     // Catch:{ Exception -> 0x0603 }
            if (r7 == 0) goto L_0x05cf
            java.lang.String r4 = com.original.tase.helper.GoogleVideoHelper.h(r6)     // Catch:{ Exception -> 0x0603 }
            goto L_0x05d0
        L_0x05cf:
            r4 = r5
        L_0x05d0:
            r8.setQuality((java.lang.String) r4)     // Catch:{ Exception -> 0x0603 }
            if (r7 == 0) goto L_0x05e2
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Exception -> 0x0603 }
            r4.<init>()     // Catch:{ Exception -> 0x0603 }
            java.lang.String r9 = com.original.Constants.f5838a     // Catch:{ Exception -> 0x0603 }
            r4.put(r15, r9)     // Catch:{ Exception -> 0x0603 }
            r8.setPlayHeader(r4)     // Catch:{ Exception -> 0x0603 }
        L_0x05e2:
            r2.onNext(r8)     // Catch:{ Exception -> 0x0603 }
            if (r7 == 0) goto L_0x0607
            java.lang.String r4 = r27.a()     // Catch:{ Exception -> 0x0603 }
            java.util.List r4 = com.original.tase.helper.GoogleVideoHelper.b(r6, r4)     // Catch:{ Exception -> 0x0603 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ Exception -> 0x0603 }
        L_0x05f3:
            boolean r6 = r4.hasNext()     // Catch:{ Exception -> 0x0603 }
            if (r6 == 0) goto L_0x0607
            java.lang.Object r6 = r4.next()     // Catch:{ Exception -> 0x0603 }
            com.original.tase.model.media.MediaSource r6 = (com.original.tase.model.media.MediaSource) r6     // Catch:{ Exception -> 0x0603 }
            r2.onNext(r6)     // Catch:{ Exception -> 0x0603 }
            goto L_0x05f3
        L_0x0603:
            r0 = move-exception
            goto L_0x0611
        L_0x0605:
            r25 = r4
        L_0x0607:
            r4 = r25
            goto L_0x059c
        L_0x060a:
            r25 = r4
            r6 = 0
            goto L_0x0617
        L_0x060e:
            r0 = move-exception
            r25 = r4
        L_0x0611:
            r6 = 0
            boolean[] r4 = new boolean[r6]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r4)
        L_0x0617:
            r4 = r25
            r10 = 2
            goto L_0x0569
        L_0x061c:
            r19 = r8
            r26 = r9
            r6 = 0
            boolean[] r0 = new boolean[r6]
            r1.a(r2, r4, r5, r0)
            goto L_0x062b
        L_0x0627:
            r19 = r8
            r26 = r9
        L_0x062b:
            r0 = r21
            goto L_0x063a
        L_0x062e:
            r23 = r4
            r24 = r6
            r20 = r7
            r19 = r8
            r26 = r9
            r22 = r10
        L_0x063a:
            r8 = r19
            r7 = r20
            r10 = r22
            r4 = r23
            r6 = r24
            r9 = r26
            r5 = 0
            goto L_0x034a
        L_0x0649:
            r23 = r4
            r24 = r6
            r20 = r7
            java.lang.String r0 = "div#download"
            r4 = r24
            org.jsoup.nodes.Element r0 = r4.h(r0)
            if (r0 == 0) goto L_0x0859
            java.lang.String r4 = "li"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r4)
            java.util.Iterator r4 = r0.iterator()
        L_0x0663:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0859
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x0849 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ Exception -> 0x0849 }
            r6 = r23
            org.jsoup.nodes.Element r0 = r0.h(r6)     // Catch:{ Exception -> 0x0847 }
            if (r0 == 0) goto L_0x0843
            java.lang.String r7 = "href"
            java.lang.String r0 = r0.b((java.lang.String) r7)     // Catch:{ Exception -> 0x0847 }
            boolean r7 = r0.startsWith(r12)     // Catch:{ Exception -> 0x0847 }
            if (r7 == 0) goto L_0x069a
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r7.<init>()     // Catch:{ Exception -> 0x0693 }
            r7.append(r11)     // Catch:{ Exception -> 0x0693 }
            r7.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = r7.toString()     // Catch:{ Exception -> 0x0693 }
            goto L_0x06cc
        L_0x0693:
            r0 = move-exception
            r7 = 34
        L_0x0696:
            r8 = 0
        L_0x0697:
            r9 = 1
            goto L_0x0850
        L_0x069a:
            boolean r7 = r0.startsWith(r13)     // Catch:{ Exception -> 0x0847 }
            if (r7 == 0) goto L_0x06b2
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r7.<init>()     // Catch:{ Exception -> 0x0693 }
            java.lang.String r8 = r1.c     // Catch:{ Exception -> 0x0693 }
            r7.append(r8)     // Catch:{ Exception -> 0x0693 }
            r7.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = r7.toString()     // Catch:{ Exception -> 0x0693 }
            goto L_0x06cc
        L_0x06b2:
            boolean r7 = r0.startsWith(r3)     // Catch:{ Exception -> 0x0847 }
            if (r7 != 0) goto L_0x06cc
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r7.<init>()     // Catch:{ Exception -> 0x0693 }
            java.lang.String r8 = r1.c     // Catch:{ Exception -> 0x0693 }
            r7.append(r8)     // Catch:{ Exception -> 0x0693 }
            r7.append(r13)     // Catch:{ Exception -> 0x0693 }
            r7.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = r7.toString()     // Catch:{ Exception -> 0x0693 }
        L_0x06cc:
            r7 = r0
            java.lang.String r0 = "(?:\\?|&|/)(?:r|d|url)=(.*?)(?:&|#|$)"
            r8 = 1
            java.lang.String r9 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r0, (int) r8)     // Catch:{ Exception -> 0x0847 }
            boolean r0 = r9.isEmpty()     // Catch:{ Exception -> 0x0847 }
            if (r0 != 0) goto L_0x0843
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x06e8 }
            r8 = 8
            byte[] r8 = android.util.Base64.decode(r9, r8)     // Catch:{ Exception -> 0x06e8 }
            java.lang.String r10 = "UTF-8"
            r0.<init>(r8, r10)     // Catch:{ Exception -> 0x06e8 }
            goto L_0x0704
        L_0x06e8:
            r0 = move-exception
            r8 = 0
            boolean[] r10 = new boolean[r8]     // Catch:{ Exception -> 0x083e }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r10)     // Catch:{ Exception -> 0x0847 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x06fb }
            r8 = 8
            byte[] r8 = android.util.Base64.decode(r9, r8)     // Catch:{ Exception -> 0x06fb }
            r0.<init>(r8)     // Catch:{ Exception -> 0x06fb }
            goto L_0x0704
        L_0x06fb:
            r0 = move-exception
            r8 = 0
            boolean[] r9 = new boolean[r8]     // Catch:{ Exception -> 0x083e }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)     // Catch:{ Exception -> 0x0847 }
            r0 = r20
        L_0x0704:
            boolean r8 = r0.isEmpty()     // Catch:{ Exception -> 0x0847 }
            if (r8 != 0) goto L_0x0843
            boolean r8 = r0.startsWith(r12)     // Catch:{ Exception -> 0x0847 }
            if (r8 == 0) goto L_0x0720
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r8.<init>()     // Catch:{ Exception -> 0x0693 }
            r8.append(r11)     // Catch:{ Exception -> 0x0693 }
            r8.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = r8.toString()     // Catch:{ Exception -> 0x0693 }
            goto L_0x0752
        L_0x0720:
            boolean r8 = r0.startsWith(r13)     // Catch:{ Exception -> 0x0847 }
            if (r8 == 0) goto L_0x0738
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r8.<init>()     // Catch:{ Exception -> 0x0693 }
            java.lang.String r9 = r1.c     // Catch:{ Exception -> 0x0693 }
            r8.append(r9)     // Catch:{ Exception -> 0x0693 }
            r8.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = r8.toString()     // Catch:{ Exception -> 0x0693 }
            goto L_0x0752
        L_0x0738:
            boolean r8 = r0.startsWith(r3)     // Catch:{ Exception -> 0x0847 }
            if (r8 != 0) goto L_0x0752
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r8.<init>()     // Catch:{ Exception -> 0x0693 }
            java.lang.String r9 = r1.c     // Catch:{ Exception -> 0x0693 }
            r8.append(r9)     // Catch:{ Exception -> 0x0693 }
            r8.append(r13)     // Catch:{ Exception -> 0x0693 }
            r8.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = r8.toString()     // Catch:{ Exception -> 0x0693 }
        L_0x0752:
            java.lang.String r8 = "//href.li/?"
            boolean r8 = r0.contains(r8)     // Catch:{ Exception -> 0x0847 }
            if (r8 == 0) goto L_0x076e
            java.lang.String r8 = "/\\?(.*?)$"
            r9 = 1
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r8, (int) r9)     // Catch:{ Exception -> 0x0769 }
            boolean r9 = r8.isEmpty()     // Catch:{ Exception -> 0x0693 }
            if (r9 != 0) goto L_0x076e
            r0 = r8
            goto L_0x076e
        L_0x0769:
            r0 = move-exception
            r7 = 34
            goto L_0x084f
        L_0x076e:
            r8 = 0
            boolean[] r9 = new boolean[r8]     // Catch:{ Exception -> 0x083e }
            r1.a(r2, r0, r5, r9)     // Catch:{ Exception -> 0x083e }
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x083e }
            java.lang.String r10 = com.original.Constants.f5838a     // Catch:{ Exception -> 0x083e }
            java.util.Map[] r14 = new java.util.Map[r8]     // Catch:{ Exception -> 0x083e }
            java.lang.String r8 = r9.a((java.lang.String) r0, (java.lang.String) r10, (java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r14)     // Catch:{ Exception -> 0x0847 }
            boolean r9 = com.original.tase.helper.http.sucuri.SucuriCloudProxyHelper.c(r8)     // Catch:{ Exception -> 0x0847 }
            if (r9 == 0) goto L_0x07bf
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r8.<init>()     // Catch:{ Exception -> 0x0693 }
            java.lang.String r9 = "https://"
            r8.append(r9)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r9 = "//([^/]+)"
            r10 = 1
            java.lang.String r9 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r9, (int) r10)     // Catch:{ Exception -> 0x0693 }
            r8.append(r9)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0693 }
            com.original.tase.helper.http.sucuri.SucuriCloudProxyHelper.b(r8, r0)     // Catch:{ Exception -> 0x0693 }
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0693 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0693 }
            r9.<init>()     // Catch:{ Exception -> 0x0693 }
            r9.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = "##forceNoCache##"
            r9.append(r0)     // Catch:{ Exception -> 0x0693 }
            java.lang.String r0 = r9.toString()     // Catch:{ Exception -> 0x0693 }
            java.lang.String r9 = com.original.Constants.f5838a     // Catch:{ Exception -> 0x0693 }
            r10 = 0
            java.util.Map[] r14 = new java.util.Map[r10]     // Catch:{ Exception -> 0x0693 }
            java.lang.String r8 = r8.a((java.lang.String) r0, (java.lang.String) r9, (java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r14)     // Catch:{ Exception -> 0x0693 }
        L_0x07bf:
            java.lang.String r0 = "<a[^>]*href\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>Continue"
            r7 = 34
            r9 = 1
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r8, (java.lang.String) r0, (int) r9, (int) r7)     // Catch:{ Exception -> 0x083c }
            boolean r8 = r0.isEmpty()     // Catch:{ Exception -> 0x083a }
            if (r8 != 0) goto L_0x0845
            boolean r8 = r0.startsWith(r12)     // Catch:{ Exception -> 0x083a }
            if (r8 == 0) goto L_0x07e7
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07e4 }
            r8.<init>()     // Catch:{ Exception -> 0x07e4 }
            r8.append(r11)     // Catch:{ Exception -> 0x07e4 }
            r8.append(r0)     // Catch:{ Exception -> 0x07e4 }
            java.lang.String r0 = r8.toString()     // Catch:{ Exception -> 0x07e4 }
            goto L_0x0819
        L_0x07e4:
            r0 = move-exception
            goto L_0x0696
        L_0x07e7:
            boolean r8 = r0.startsWith(r13)     // Catch:{ Exception -> 0x083a }
            if (r8 == 0) goto L_0x07ff
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07e4 }
            r8.<init>()     // Catch:{ Exception -> 0x07e4 }
            java.lang.String r9 = r1.c     // Catch:{ Exception -> 0x07e4 }
            r8.append(r9)     // Catch:{ Exception -> 0x07e4 }
            r8.append(r0)     // Catch:{ Exception -> 0x07e4 }
            java.lang.String r0 = r8.toString()     // Catch:{ Exception -> 0x07e4 }
            goto L_0x0819
        L_0x07ff:
            boolean r8 = r0.startsWith(r3)     // Catch:{ Exception -> 0x083a }
            if (r8 != 0) goto L_0x0819
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07e4 }
            r8.<init>()     // Catch:{ Exception -> 0x07e4 }
            java.lang.String r9 = r1.c     // Catch:{ Exception -> 0x07e4 }
            r8.append(r9)     // Catch:{ Exception -> 0x07e4 }
            r8.append(r13)     // Catch:{ Exception -> 0x07e4 }
            r8.append(r0)     // Catch:{ Exception -> 0x07e4 }
            java.lang.String r0 = r8.toString()     // Catch:{ Exception -> 0x07e4 }
        L_0x0819:
            java.lang.String r8 = "//href.li/?"
            boolean r8 = r0.contains(r8)     // Catch:{ Exception -> 0x083a }
            if (r8 == 0) goto L_0x0830
            java.lang.String r8 = "/\\?(.*?)$"
            r9 = 1
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r8, (int) r9)     // Catch:{ Exception -> 0x083c }
            boolean r10 = r8.isEmpty()     // Catch:{ Exception -> 0x083c }
            if (r10 != 0) goto L_0x0831
            r0 = r8
            goto L_0x0831
        L_0x0830:
            r9 = 1
        L_0x0831:
            r8 = 0
            boolean[] r10 = new boolean[r8]     // Catch:{ Exception -> 0x0838 }
            r1.a(r2, r0, r5, r10)     // Catch:{ Exception -> 0x083c }
            goto L_0x0855
        L_0x0838:
            r0 = move-exception
            goto L_0x0850
        L_0x083a:
            r0 = move-exception
            goto L_0x084e
        L_0x083c:
            r0 = move-exception
            goto L_0x084f
        L_0x083e:
            r0 = move-exception
            r7 = 34
            goto L_0x0697
        L_0x0843:
            r7 = 34
        L_0x0845:
            r9 = 1
            goto L_0x0855
        L_0x0847:
            r0 = move-exception
            goto L_0x084c
        L_0x0849:
            r0 = move-exception
            r6 = r23
        L_0x084c:
            r7 = 34
        L_0x084e:
            r9 = 1
        L_0x084f:
            r8 = 0
        L_0x0850:
            boolean[] r10 = new boolean[r8]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r10)
        L_0x0855:
            r23 = r6
            goto L_0x0663
        L_0x0859:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.TV21.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo):void");
    }
}
