package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class OdbMovies extends BaseProvider {
    private String c = Utils.getProvider(97);

    public String a() {
        return "OdbMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        String replace = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+");
        String e = BaseProvider.e(HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]), this.c);
        Iterator it2 = Jsoup.b(HttpHelper.e().b(String.format(e, new Object[]{replace}), this.c)).g("div.post-thumb").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("href");
            String b2 = element.b("title");
            if (b2.contains(movieInfo.name + " (" + movieInfo.year + ")")) {
                return b;
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0])).g("h2").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String a2 = Regex.a(element.toString(), "(\\d{3,4})p", 1);
            if (a2.isEmpty()) {
                a2 = "HQ";
            }
            String b = element.b("href");
            if (b != null && !b.isEmpty() && !b.contains(this.c)) {
                String a3 = Regex.a(b, "go(?:=)([0-9a-zA-Z]+)", 1);
                if (!a3.isEmpty()) {
                    try {
                        b = new String(Base64.decode(a3, 10), "UTF-8").trim();
                    } catch (Throwable unused) {
                    }
                }
                Iterator it3 = Jsoup.b(HttpHelper.e().a(b, (Map<String, String>[]) new Map[0])).g("div.entry-content").b("p").b("a").iterator();
                while (it3.hasNext()) {
                    String b2 = ((Element) it3.next()).b("href");
                    if (!b2.isEmpty()) {
                        a(observableEmitter, b2, a2, false);
                    }
                }
            }
        }
    }
}
