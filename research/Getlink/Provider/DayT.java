package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DayT extends BaseProvider {
    private String c = Utils.getProvider(65);

    public String a() {
        return "DayT";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo, this.c + "/watch/" + TitleHelper.g(movieInfo.name));
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo, this.c + "/watch/" + TitleHelper.g(TitleHelper.e(movieInfo.name.replace("Marvel's ", "").replace("Da Vinci's Demons", "Da Vincis Demons"))) + "/s" + movieInfo.session + "/e" + movieInfo.eps);
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        MediaSource mediaSource;
        String str2;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        MovieInfo movieInfo2 = movieInfo;
        String str3 = str;
        boolean z = !movieInfo2.session.isEmpty();
        String a2 = HttpHelper.e().a(str3, (Map<String, String>[]) new Map[0]);
        if (!z || Regex.a(a2, "Date\\s*:\\s*.+?>.+?(\\d{4})", 1).equals(movieInfo2.year)) {
            ArrayList<String> arrayList = new ArrayList<>();
            try {
                arrayList.addAll(a(str3, (String) null, (List<String>[]) new List[0]));
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            Element h = Jsoup.b(a2).h("a#playthevid[href]");
            if (h != null) {
                String replace = this.c.replace("http://", "").replace("https://", "");
                String b = h.b("href");
                if (b.startsWith("//")) {
                    b = "http:" + b;
                } else if (b.startsWith("/")) {
                    b = this.c + b;
                }
                ArrayList arrayList2 = Regex.b(HttpHelper.e().a(b, (Map<String, String>[]) new Map[0]), "var ([0-9A-Za-z]+)\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 2, true).get(1);
                String str4 = arrayList2.get(3).toString() + arrayList2.get(1).toString() + arrayList2.get(0).toString() + arrayList2.get(2).toString();
                if (str4.startsWith("/")) {
                    str4 = this.c + str4;
                }
                HttpHelper e = HttpHelper.e();
                String str5 = this.c;
                e.c(str5, str5);
                HttpHelper.e().c(this.c, "__test");
                HttpHelper.e().c(this.c, "trione=1");
                HttpHelper.e().c(this.c, "tritwo=1");
                String a3 = HttpHelper.e().a(this.c);
                HashMap hashMap = new HashMap();
                hashMap.put("Cookie", a3);
                hashMap.put("Referer", str4);
                hashMap.put("User-Agent", Constants.f5838a);
                String b2 = Jsoup.b(HttpHelper.e().a(str4, (Map<String, String>[]) new Map[]{hashMap})).h("iframe").b("src");
                if (b2.startsWith("/")) {
                    b2 = this.c + b2;
                }
                hashMap.put("Referer", str2);
                while (str2.contains(replace)) {
                    str2 = Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[]{hashMap})).h("iframe").b("src");
                    if (str2.startsWith("/")) {
                        str2 = this.c + str2;
                    }
                    hashMap.put("Referer", str2);
                }
                if (!str2.isEmpty()) {
                    arrayList.add(str2);
                }
            }
            for (String str6 : arrayList) {
                if (!observableEmitter.isDisposed()) {
                    if (GoogleVideoHelper.j(str6)) {
                        HashMap<String, String> g = GoogleVideoHelper.g(str6);
                        if (g != null) {
                            for (Map.Entry next : g.entrySet()) {
                                MediaSource mediaSource2 = new MediaSource(a(), "GoogleVideo", false);
                                mediaSource2.setStreamLink((String) next.getKey());
                                mediaSource2.setQuality((String) next.getValue());
                                HashMap hashMap2 = new HashMap();
                                hashMap2.put("User-Agent", Constants.f5838a);
                                hashMap2.put("Cookie", GoogleVideoHelper.c(str6, (String) next.getKey()));
                                mediaSource2.setPlayHeader(hashMap2);
                                observableEmitter2.onNext(mediaSource2);
                            }
                        }
                        mediaSource = null;
                    } else if (GoogleVideoHelper.k(str6)) {
                        mediaSource = new MediaSource(a(), "GoogleVideo", false);
                        mediaSource.setQuality(GoogleVideoHelper.h(str6));
                    } else {
                        MediaSource mediaSource3 = new MediaSource(a(), "CDN", false);
                        mediaSource3.setQuality("HD");
                        mediaSource = mediaSource3;
                    }
                    if (mediaSource != null) {
                        mediaSource.setStreamLink(str6);
                        observableEmitter2.onNext(mediaSource);
                    }
                } else {
                    return;
                }
            }
        }
    }

    @SafeVarargs
    private final List<String> a(String str, String str2, List<String>... listArr) {
        String str3;
        Elements g;
        if (str2 == null || str2.isEmpty()) {
            str3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
        } else {
            str3 = HttpHelper.e().b(str, str2);
        }
        Document b = Jsoup.b(str3);
        ArrayList arrayList = new ArrayList();
        if (!str3.contains("part1") && !str3.contains("part2") && (g = b.g("iframe[src]")) != null && !g.isEmpty()) {
            if (!(listArr == null || listArr.length <= 0 || listArr[0] == null)) {
                arrayList.addAll(listArr[0]);
            }
            Iterator it2 = g.iterator();
            while (it2.hasNext()) {
                String b2 = ((Element) it2.next()).b("src");
                if (b2.contains("docs.google.com") || b2.contains("/file/d/")) {
                    arrayList.add(b2);
                } else {
                    if (!b2.startsWith("/")) {
                        b2 = "/" + b2;
                    } else if (b2.startsWith("/") || !b2.startsWith(UriUtil.HTTP_SCHEME)) {
                        b2 = "http://xpau.se" + b2;
                    }
                    try {
                        return a(b2, str, (List<String>[]) new List[]{arrayList});
                    } catch (Throwable th) {
                        Logger.a(th, "DayT:StackOverflowError", new boolean[0]);
                        return arrayList;
                    }
                }
            }
        }
        return arrayList;
    }
}
