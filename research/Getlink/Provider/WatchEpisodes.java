package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class WatchEpisodes extends BaseProvider {
    private String c = Utils.getProvider(66);

    public String a() {
        return "WatchEpisodes";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x01fa A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01fb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r10, com.movie.data.model.MovieInfo r11) {
        /*
            r9 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r9.c
            r0.append(r1)
            java.lang.String r1 = "/search/ajax_search?q="
            r0.append(r1)
            java.lang.String r2 = r11.getName()
            r3 = 0
            boolean[] r4 = new boolean[r3]
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r4)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            r4 = 1
            java.util.Map[] r5 = new java.util.Map[r4]
            java.util.HashMap r6 = com.original.Constants.a()
            r5[r3] = r6
            java.lang.String r0 = r2.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r5)
            java.lang.String r2 = r0.toLowerCase()
            java.lang.String r5 = "series"
            boolean r2 = r2.contains(r5)
            if (r2 != 0) goto L_0x00e8
            java.lang.String r0 = "https://watchepisodes.unblocked.mx"
            r9.c = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = r9.c
            r0.append(r2)
            r0.append(r1)
            java.lang.String r2 = r11.getName()
            boolean[] r6 = new boolean[r3]
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r6)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r6 = new java.util.Map[r4]
            java.util.HashMap r7 = com.original.Constants.a()
            r6[r3] = r7
            java.lang.String r0 = r2.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r6)
            java.lang.String r2 = r0.toLowerCase()
            boolean r2 = r2.contains(r5)
            if (r2 != 0) goto L_0x00e8
            java.lang.String r0 = "https://watchepisodes.bypassed.org"
            r9.c = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = r9.c
            r0.append(r2)
            r0.append(r1)
            java.lang.String r2 = r11.getName()
            boolean[] r6 = new boolean[r3]
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r6)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r6 = new java.util.Map[r4]
            java.util.HashMap r7 = com.original.Constants.a()
            r6[r3] = r7
            java.lang.String r0 = r2.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r6)
            java.lang.String r2 = r0.toLowerCase()
            boolean r2 = r2.contains(r5)
            if (r2 != 0) goto L_0x00e8
            java.lang.String r0 = "https://watchepisodes.bypassed.bz"
            r9.c = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = r9.c
            r0.append(r2)
            r0.append(r1)
            java.lang.String r1 = r11.getName()
            boolean[] r2 = new boolean[r3]
            java.lang.String r1 = com.original.tase.utils.Utils.a((java.lang.String) r1, (boolean[]) r2)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.original.tase.helper.http.HttpHelper r1 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r2 = new java.util.Map[r4]
            java.util.HashMap r6 = com.original.Constants.a()
            r2[r3] = r6
            java.lang.String r0 = r1.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r2)
        L_0x00e8:
            com.google.gson.JsonParser r1 = new com.google.gson.JsonParser
            r1.<init>()
            com.google.gson.JsonElement r0 = r1.a((java.lang.String) r0)
            java.lang.String r1 = ""
            java.lang.String r2 = "/"
            if (r0 == 0) goto L_0x01be
            boolean r6 = r0.l()
            if (r6 == 0) goto L_0x01be
            com.google.gson.JsonObject r0 = r0.f()
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r5)
            if (r0 == 0) goto L_0x01be
            boolean r5 = r0.j()
            if (r5 == 0) goto L_0x01be
            com.google.gson.JsonArray r0 = r0.e()
            java.util.Iterator r0 = r0.iterator()
            r5 = r1
        L_0x0116:
            boolean r6 = r0.hasNext()
            if (r6 == 0) goto L_0x01bf
            java.lang.Object r6 = r0.next()
            com.google.gson.JsonElement r6 = (com.google.gson.JsonElement) r6
            boolean r7 = r6.l()
            if (r7 == 0) goto L_0x0116
            com.google.gson.JsonObject r6 = r6.f()
            java.lang.String r7 = "label"
            com.google.gson.JsonElement r7 = r6.a((java.lang.String) r7)
            java.lang.String r8 = "seo"
            com.google.gson.JsonElement r6 = r6.a((java.lang.String) r8)
            if (r7 == 0) goto L_0x0116
            boolean r8 = r7.k()
            if (r8 != 0) goto L_0x0116
            java.lang.String r8 = r7.i()
            if (r8 == 0) goto L_0x0116
            if (r6 == 0) goto L_0x0116
            boolean r8 = r6.k()
            if (r8 != 0) goto L_0x0116
            java.lang.String r8 = r6.i()
            if (r8 == 0) goto L_0x0116
            java.lang.String r7 = r7.i()
            java.lang.String r7 = com.original.tase.helper.TitleHelper.f(r7)
            java.lang.String r8 = r11.getName()
            java.lang.String r8 = com.original.tase.helper.TitleHelper.f(r8)
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x0116
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r5 = r9.c
            r1.append(r5)
            r1.append(r2)
            java.lang.String r5 = r6.i()
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            com.original.tase.helper.http.HttpHelper r5 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r6 = new java.util.Map[r3]
            java.lang.String r5 = r5.a((java.lang.String) r1, (java.util.Map<java.lang.String, java.lang.String>[]) r6)
            r6 = 34
            java.lang.String r7 = "<span>\\s*Year\\s*:\\s*</span>.*?<a[^>]*>\\s*(\\d{4})\\s*</a>"
            java.lang.String r6 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r7, (int) r4, (int) r6)
            boolean r7 = r6.isEmpty()
            if (r7 != 0) goto L_0x01bc
            boolean r7 = com.original.tase.utils.Utils.b(r6)
            if (r7 == 0) goto L_0x01bc
            java.lang.Integer r7 = r11.getSessionYear()
            int r7 = r7.intValue()
            if (r7 <= 0) goto L_0x01bc
            java.lang.String r6 = r6.trim()
            int r6 = java.lang.Integer.parseInt(r6)
            java.lang.Integer r7 = r11.getSessionYear()
            int r7 = r7.intValue()
            if (r6 != r7) goto L_0x0116
        L_0x01bc:
            r0 = 1
            goto L_0x01c0
        L_0x01be:
            r5 = r1
        L_0x01bf:
            r0 = 0
        L_0x01c0:
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x01ce
            boolean r1 = r5.isEmpty()
            if (r1 != 0) goto L_0x01ce
            if (r0 != 0) goto L_0x01f4
        L_0x01ce:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r5 = r9.c
            r1.append(r5)
            r1.append(r2)
            java.lang.String r5 = r11.getName()
            java.lang.String r5 = com.original.tase.helper.TitleHelper.g(r5)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            java.util.Map[] r5 = new java.util.Map[r3]
            java.lang.String r5 = r0.a((java.lang.String) r1, (java.util.Map<java.lang.String, java.lang.String>[]) r5)
        L_0x01f4:
            boolean r0 = r5.isEmpty()
            if (r0 == 0) goto L_0x01fb
            return
        L_0x01fb:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "href=\"([^\"]*-[sS]"
            r0.append(r1)
            java.lang.Integer r1 = r11.getSession()
            int r1 = r1.intValue()
            java.lang.String r1 = com.original.tase.utils.Utils.a((int) r1)
            r0.append(r1)
            java.lang.String r1 = "[eE]"
            r0.append(r1)
            java.lang.Integer r11 = r11.getEps()
            int r11 = r11.intValue()
            java.lang.String r11 = com.original.tase.utils.Utils.a((int) r11)
            r0.append(r11)
            java.lang.String r11 = "(?!\\d)[^\"]*)"
            r0.append(r11)
            java.lang.String r11 = r0.toString()
            java.lang.String r11 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r11, (int) r4)
            boolean r0 = r11.startsWith(r2)
            if (r0 == 0) goto L_0x024c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r9.c
            r0.append(r1)
            r0.append(r11)
            java.lang.String r11 = r0.toString()
        L_0x024c:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r1 = new java.util.Map[r3]
            java.lang.String r11 = r0.a((java.lang.String) r11, (java.util.Map<java.lang.String, java.lang.String>[]) r1)
            org.jsoup.nodes.Document r11 = org.jsoup.Jsoup.b(r11)
            java.lang.String r0 = "div[class*=\"ldr-item\"]"
            org.jsoup.select.Elements r11 = r11.g((java.lang.String) r0)
            java.util.Iterator r11 = r11.iterator()
            r0 = 0
        L_0x0265:
            boolean r1 = r11.hasNext()
            if (r1 == 0) goto L_0x0292
            java.lang.Object r1 = r11.next()
            org.jsoup.nodes.Element r1 = (org.jsoup.nodes.Element) r1
            boolean r2 = r10.isDisposed()
            if (r2 == 0) goto L_0x0278
            return
        L_0x0278:
            java.lang.String r2 = "a[data-actuallink*=\"http\"]"
            org.jsoup.nodes.Element r1 = r1.h(r2)
            if (r1 == 0) goto L_0x028d
            java.lang.String r2 = "data-actuallink"
            java.lang.String r1 = r1.b((java.lang.String) r2)
            boolean[] r2 = new boolean[r3]
            java.lang.String r5 = "HQ"
            r9.a(r10, r1, r5, r2)
        L_0x028d:
            int r0 = r0 + r4
            r1 = 20
            if (r0 <= r1) goto L_0x0265
        L_0x0292:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.WatchEpisodes.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo):void");
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }
}
