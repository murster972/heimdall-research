package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class CMovies extends BaseProvider {
    public String c = Utils.getProvider(38);

    public String a() {
        return "CMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, "", false);
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, movieInfo.session, false);
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, movieInfo.eps);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r6, java.lang.String r7, com.movie.data.model.MovieInfo r8, java.lang.String r9) {
        /*
            r5 = this;
            java.lang.Integer r8 = r8.getType()
            int r8 = r8.intValue()
            r0 = 1
            r1 = 0
            if (r8 != r0) goto L_0x000e
            r8 = 1
            goto L_0x000f
        L_0x000e:
            r8 = 0
        L_0x000f:
            java.lang.String r2 = ".html"
            boolean r2 = r7.endsWith(r2)
            if (r2 == 0) goto L_0x0022
            int r2 = r7.length()
            int r2 = r2 + -5
            java.lang.String r2 = r7.substring(r1, r2)
            goto L_0x0023
        L_0x0022:
            r2 = r7
        L_0x0023:
            java.lang.String r3 = "/"
            boolean r4 = r2.endsWith(r3)
            if (r4 == 0) goto L_0x0034
            int r4 = r2.length()
            int r4 = r4 - r0
            java.lang.String r2 = r2.substring(r1, r4)
        L_0x0034:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r2)
            java.lang.String r2 = "/watching.html"
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r7 = r4.b((java.lang.String) r2, (java.lang.String) r7)
            org.jsoup.nodes.Document r7 = org.jsoup.Jsoup.b(r7)
            java.lang.String r2 = "span.quality"
            org.jsoup.nodes.Element r2 = r7.h(r2)
            if (r2 == 0) goto L_0x0077
            java.lang.String r2 = r2.G()
            java.lang.String r2 = r2.trim()
            java.lang.String r2 = r2.toLowerCase()
            java.lang.String r4 = "cam"
            boolean r4 = r2.contains(r4)
            if (r4 != 0) goto L_0x0075
            java.lang.String r4 = "ts"
            boolean r2 = r2.contains(r4)
            if (r2 == 0) goto L_0x0077
        L_0x0075:
            r2 = 1
            goto L_0x0078
        L_0x0077:
            r2 = 0
        L_0x0078:
            if (r8 == 0) goto L_0x0081
            java.lang.String r8 = "a.btn-eps[player-data]"
            org.jsoup.select.Elements r7 = r7.g((java.lang.String) r8)
            goto L_0x009b
        L_0x0081:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r4 = "a.btn-eps[player-data][episode-data=\""
            r8.append(r4)
            r8.append(r9)
            java.lang.String r9 = "\"]"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            org.jsoup.select.Elements r7 = r7.g((java.lang.String) r8)
        L_0x009b:
            java.util.Iterator r7 = r7.iterator()
        L_0x009f:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto L_0x00f1
            java.lang.Object r8 = r7.next()
            org.jsoup.nodes.Element r8 = (org.jsoup.nodes.Element) r8
            java.lang.String r9 = "player-data"
            java.lang.String r8 = r8.b((java.lang.String) r9)
            java.lang.String r9 = "//"
            boolean r9 = r8.startsWith(r9)
            if (r9 == 0) goto L_0x00cb
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r4 = "https:"
            r9.append(r4)
            r9.append(r8)
            java.lang.String r8 = r9.toString()
            goto L_0x00e7
        L_0x00cb:
            boolean r9 = r8.startsWith(r3)
            if (r9 == 0) goto L_0x00e7
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r4 = r5.c
            r9.append(r4)
            java.lang.String r4 = ""
            r9.append(r4)
            r9.append(r8)
            java.lang.String r8 = r9.toString()
        L_0x00e7:
            boolean[] r9 = new boolean[r0]
            r9[r1] = r2
            java.lang.String r4 = "HD"
            r5.a(r6, r8, r4, r9)
            goto L_0x009f
        L_0x00f1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.CMovies.a(io.reactivex.ObservableEmitter, java.lang.String, com.movie.data.model.MovieInfo, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x01d9 A[Catch:{ Exception -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01ee A[Catch:{ Exception -> 0x0241 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x024d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(com.movie.data.model.MovieInfo r25, java.lang.String r26, boolean r27) {
        /*
            r24 = this;
            r1 = r24
            java.lang.String r2 = "?link_web="
            java.lang.String r3 = "/"
            java.lang.Integer r0 = r25.getType()
            int r0 = r0.intValue()
            r4 = 1
            r5 = 0
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r5)
            if (r0 != r4) goto L_0x0018
            r0 = 1
            goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            if (r0 == 0) goto L_0x0027
            r0 = 2
            java.lang.Boolean[] r0 = new java.lang.Boolean[r0]
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r4)
            r0[r5] = r7
            r0[r4] = r6
            goto L_0x002b
        L_0x0027:
            java.lang.Boolean[] r0 = new java.lang.Boolean[r4]
            r0[r5] = r6
        L_0x002b:
            r6 = r0
            int r7 = r6.length
            r8 = 0
        L_0x002e:
            java.lang.String r9 = ""
            if (r8 >= r7) goto L_0x036f
            r0 = r6[r8]
            boolean r0 = r0.booleanValue()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.util.HashMap r11 = new java.util.HashMap
            r11.<init>()
            java.lang.String r12 = "Accept"
            java.lang.String r13 = "*/*"
            r11.put(r12, r13)
            java.lang.String r12 = r1.c
            java.lang.String r13 = "Origin"
            r11.put(r13, r12)
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = r1.c
            r13.append(r14)
            java.lang.String r14 = "/movie/search/"
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            r12.append(r13)
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = r25.getName()
            java.lang.String r15 = "'"
            java.lang.String r4 = "-"
            java.lang.String r14 = r14.replace(r15, r4)
            java.lang.String r5 = "[^A-Za-z0-9- .]"
            java.lang.String r14 = r14.replaceAll(r5, r9)
            r16 = r6
            java.lang.String r6 = "."
            java.lang.String r14 = r14.replace(r6, r4)
            r17 = r7
            java.lang.String r7 = "  "
            r18 = r8
            java.lang.String r8 = " "
            java.lang.String r14 = r14.replace(r7, r8)
            java.lang.String r14 = com.original.tase.helper.TitleHelper.g(r14)
            java.lang.String r14 = r14.replace(r8, r4)
            r19 = r3
            java.lang.String r3 = "--"
            java.lang.String r14 = r14.replace(r3, r4)
            r13.append(r14)
            if (r0 == 0) goto L_0x00c1
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r4)
            r20 = r2
            java.lang.Integer r2 = r25.getYear()
            r14.append(r2)
            java.lang.String r2 = r14.toString()
            goto L_0x00c4
        L_0x00c1:
            r20 = r2
            r2 = r9
        L_0x00c4:
            r13.append(r2)
            java.lang.String r13 = r13.toString()
            r21 = r2
            r14 = 0
            boolean[] r2 = new boolean[r14]
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r13, (boolean[]) r2)
            r12.append(r2)
            java.lang.String r2 = r12.toString()
            java.lang.String r12 = "Referer"
            r11.put(r12, r2)
            java.lang.String r12 = r1.c
            java.lang.String r2 = com.original.tase.helper.http.sucuri.SucuriCloudProxyHelper.b(r12, r2)
            r10.add(r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019c }
            r2.<init>()     // Catch:{ Exception -> 0x019c }
            java.lang.String r12 = "https://api.ocloud.stream/series/movie/search/"
            r2.append(r12)     // Catch:{ Exception -> 0x019c }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019c }
            r12.<init>()     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r25.getName()     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r13.replace(r15, r4)     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r13.replaceAll(r5, r9)     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r13.replace(r6, r4)     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r13.replace(r7, r8)     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = com.original.tase.helper.TitleHelper.g(r13)     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r13.replace(r8, r4)     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r13.replace(r3, r4)     // Catch:{ Exception -> 0x019c }
            r12.append(r13)     // Catch:{ Exception -> 0x019c }
            if (r0 == 0) goto L_0x0131
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019c }
            r13.<init>()     // Catch:{ Exception -> 0x019c }
            r13.append(r4)     // Catch:{ Exception -> 0x019c }
            java.lang.Integer r14 = r25.getYear()     // Catch:{ Exception -> 0x019c }
            r13.append(r14)     // Catch:{ Exception -> 0x019c }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x019c }
            goto L_0x0132
        L_0x0131:
            r13 = r9
        L_0x0132:
            com.original.tase.helper.http.HttpHelper r14 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0193 }
            r22 = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0195 }
            r0.<init>()     // Catch:{ Exception -> 0x0195 }
            r12.append(r13)     // Catch:{ Exception -> 0x0195 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0195 }
            r23 = r3
            r21 = r13
            r13 = 0
            boolean[] r3 = new boolean[r13]     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r3 = com.original.tase.utils.Utils.a((java.lang.String) r12, (boolean[]) r3)     // Catch:{ Exception -> 0x01a0 }
            r2.append(r3)     // Catch:{ Exception -> 0x01a0 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01a0 }
            r0.append(r2)     // Catch:{ Exception -> 0x01a0 }
            r2 = r20
            r0.append(r2)     // Catch:{ Exception -> 0x018d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x018d }
            r3.<init>()     // Catch:{ Exception -> 0x018d }
            java.lang.String r12 = r1.c     // Catch:{ Exception -> 0x018d }
            r3.append(r12)     // Catch:{ Exception -> 0x018d }
            r12 = r19
            r3.append(r12)     // Catch:{ Exception -> 0x0190 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0190 }
            r19 = r12
            r13 = 0
            boolean[] r12 = new boolean[r13]     // Catch:{ Exception -> 0x018d }
            java.lang.String r3 = com.original.tase.utils.Utils.a((java.lang.String) r3, (boolean[]) r12)     // Catch:{ Exception -> 0x018d }
            r0.append(r3)     // Catch:{ Exception -> 0x018d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x018d }
            r3 = 1
            java.util.Map[] r12 = new java.util.Map[r3]     // Catch:{ Exception -> 0x018d }
            r12[r13] = r11     // Catch:{ Exception -> 0x018d }
            java.lang.String r0 = r14.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r12)     // Catch:{ Exception -> 0x018d }
            r10.add(r0)     // Catch:{ Exception -> 0x018d }
        L_0x018d:
            r13 = r21
            goto L_0x01a3
        L_0x0190:
            r19 = r12
            goto L_0x018d
        L_0x0193:
            r22 = r0
        L_0x0195:
            r23 = r3
            r21 = r13
            r2 = r20
            goto L_0x01a3
        L_0x019c:
            r22 = r0
            r23 = r3
        L_0x01a0:
            r2 = r20
            goto L_0x018d
        L_0x01a3:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0241 }
            r0.<init>()     // Catch:{ Exception -> 0x0241 }
            java.lang.String r3 = "https://api.yesmovie.io/movie/search/"
            r0.append(r3)     // Catch:{ Exception -> 0x0241 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0241 }
            r3.<init>()     // Catch:{ Exception -> 0x0241 }
            java.lang.String r12 = r25.getName()     // Catch:{ Exception -> 0x0241 }
            java.lang.String r12 = r12.replace(r15, r4)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r5 = r12.replaceAll(r5, r9)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r5 = r5.replace(r6, r4)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r5 = r5.replace(r7, r8)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r5 = com.original.tase.helper.TitleHelper.g(r5)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r5 = r5.replace(r8, r4)     // Catch:{ Exception -> 0x0241 }
            r6 = r23
            java.lang.String r5 = r5.replace(r6, r4)     // Catch:{ Exception -> 0x0241 }
            r3.append(r5)     // Catch:{ Exception -> 0x0241 }
            if (r22 == 0) goto L_0x01ee
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0241 }
            r5.<init>()     // Catch:{ Exception -> 0x0241 }
            r5.append(r4)     // Catch:{ Exception -> 0x0241 }
            java.lang.Integer r4 = r25.getYear()     // Catch:{ Exception -> 0x0241 }
            r5.append(r4)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r4 = r5.toString()     // Catch:{ Exception -> 0x0241 }
            r13 = r4
            goto L_0x01ef
        L_0x01ee:
            r13 = r9
        L_0x01ef:
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0241 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0241 }
            r5.<init>()     // Catch:{ Exception -> 0x0241 }
            r3.append(r13)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0241 }
            r6 = 0
            boolean[] r7 = new boolean[r6]     // Catch:{ Exception -> 0x0241 }
            java.lang.String r3 = com.original.tase.utils.Utils.a((java.lang.String) r3, (boolean[]) r7)     // Catch:{ Exception -> 0x0241 }
            r0.append(r3)     // Catch:{ Exception -> 0x0241 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0241 }
            r5.append(r0)     // Catch:{ Exception -> 0x0241 }
            r5.append(r2)     // Catch:{ Exception -> 0x0241 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0241 }
            r0.<init>()     // Catch:{ Exception -> 0x0241 }
            java.lang.String r3 = r1.c     // Catch:{ Exception -> 0x0241 }
            r0.append(r3)     // Catch:{ Exception -> 0x0241 }
            r3 = r19
            r0.append(r3)     // Catch:{ Exception -> 0x0243 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0243 }
            r6 = 0
            boolean[] r7 = new boolean[r6]     // Catch:{ Exception -> 0x0243 }
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r7)     // Catch:{ Exception -> 0x0243 }
            r5.append(r0)     // Catch:{ Exception -> 0x0243 }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x0243 }
            r5 = 1
            java.util.Map[] r7 = new java.util.Map[r5]     // Catch:{ Exception -> 0x0243 }
            r7[r6] = r11     // Catch:{ Exception -> 0x0243 }
            java.lang.String r0 = r4.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r7)     // Catch:{ Exception -> 0x0243 }
            r10.add(r0)     // Catch:{ Exception -> 0x0243 }
            goto L_0x0243
        L_0x0241:
            r3 = r19
        L_0x0243:
            java.util.Iterator r4 = r10.iterator()
        L_0x0247:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x035e
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r5 = r0.isEmpty()
            if (r5 != 0) goto L_0x0356
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r5 = "div.ml-item"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r5)
            java.util.Iterator r5 = r0.iterator()
        L_0x0267:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0356
            java.lang.Object r0 = r5.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r6 = "a[href]"
            org.jsoup.nodes.Element r6 = r0.h(r6)
            if (r6 != 0) goto L_0x027c
            goto L_0x0267
        L_0x027c:
            java.lang.String r7 = "span.mli-info"
            org.jsoup.select.Elements r8 = r0.g((java.lang.String) r7)
            int r8 = r8.size()
            if (r8 <= 0) goto L_0x0291
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r7)
            java.lang.String r0 = r0.c()
            goto L_0x0292
        L_0x0291:
            r0 = r9
        L_0x0292:
            java.lang.String r7 = "href"
            java.lang.String r6 = r6.b((java.lang.String) r7)
            java.lang.String r7 = "\\s*(?:\\:|-)?\\s*(?:S|s)eason\\s*(\\d+)"
            r8 = 1
            java.lang.String r7 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r7, (int) r8)
            boolean r10 = r0.isEmpty()
            if (r10 != 0) goto L_0x034f
            boolean r10 = r6.isEmpty()
            if (r10 != 0) goto L_0x034f
            java.lang.String r10 = "\\s*(?:\\:|-)?\\s*(?:S|s)eason\\s*\\d+"
            java.lang.String r0 = r0.replaceAll(r10, r9)     // Catch:{ Exception -> 0x0342 }
            r10 = r26
            boolean r7 = r7.contains(r10)     // Catch:{ Exception -> 0x033e }
            if (r7 == 0) goto L_0x033b
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033e }
            r7.<init>()     // Catch:{ Exception -> 0x033e }
            r11 = r25
            java.lang.String r12 = r11.name     // Catch:{ Exception -> 0x0339 }
            r7.append(r12)     // Catch:{ Exception -> 0x0339 }
            r7.append(r13)     // Catch:{ Exception -> 0x0339 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0339 }
            java.lang.String r7 = com.original.tase.helper.TitleHelper.a(r7, r9)     // Catch:{ Exception -> 0x0339 }
            java.lang.String r12 = "Marvel's "
            java.lang.String r0 = r0.replace(r12, r9)     // Catch:{ Exception -> 0x0339 }
            java.lang.String r12 = "DC's "
            java.lang.String r0 = r0.replace(r12, r9)     // Catch:{ Exception -> 0x0339 }
            java.lang.String r0 = com.original.tase.helper.TitleHelper.a(r0, r9)     // Catch:{ Exception -> 0x0339 }
            boolean r0 = r7.equals(r0)     // Catch:{ Exception -> 0x0339 }
            if (r0 == 0) goto L_0x0353
            java.lang.String r0 = "//"
            boolean r0 = r6.startsWith(r0)     // Catch:{ Exception -> 0x0339 }
            if (r0 == 0) goto L_0x0300
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0339 }
            r0.<init>()     // Catch:{ Exception -> 0x0339 }
            java.lang.String r7 = "http:"
            r0.append(r7)     // Catch:{ Exception -> 0x0339 }
            r0.append(r6)     // Catch:{ Exception -> 0x0339 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0339 }
            return r0
        L_0x0300:
            boolean r0 = r6.startsWith(r3)     // Catch:{ Exception -> 0x0339 }
            if (r0 == 0) goto L_0x031b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0339 }
            r0.<init>()     // Catch:{ Exception -> 0x0339 }
            java.lang.String r7 = r1.c     // Catch:{ Exception -> 0x0339 }
            r0.append(r7)     // Catch:{ Exception -> 0x0339 }
            r0.append(r9)     // Catch:{ Exception -> 0x0339 }
            r0.append(r6)     // Catch:{ Exception -> 0x0339 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0339 }
            return r0
        L_0x031b:
            java.lang.String r0 = "http"
            boolean r0 = r6.startsWith(r0)     // Catch:{ Exception -> 0x0339 }
            if (r0 == 0) goto L_0x0324
            return r6
        L_0x0324:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0339 }
            r0.<init>()     // Catch:{ Exception -> 0x0339 }
            java.lang.String r7 = r1.c     // Catch:{ Exception -> 0x0339 }
            r0.append(r7)     // Catch:{ Exception -> 0x0339 }
            r0.append(r3)     // Catch:{ Exception -> 0x0339 }
            r0.append(r6)     // Catch:{ Exception -> 0x0339 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0339 }
            return r0
        L_0x0339:
            r0 = move-exception
            goto L_0x0347
        L_0x033b:
            r11 = r25
            goto L_0x0353
        L_0x033e:
            r0 = move-exception
            r11 = r25
            goto L_0x0347
        L_0x0342:
            r0 = move-exception
            r11 = r25
            r10 = r26
        L_0x0347:
            r6 = 0
            boolean[] r7 = new boolean[r6]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r7)
            goto L_0x0267
        L_0x034f:
            r11 = r25
            r10 = r26
        L_0x0353:
            r6 = 0
            goto L_0x0267
        L_0x0356:
            r11 = r25
            r10 = r26
            r6 = 0
            r8 = 1
            goto L_0x0247
        L_0x035e:
            r11 = r25
            r10 = r26
            r6 = 0
            r8 = 1
            int r0 = r18 + 1
            r8 = r0
            r6 = r16
            r7 = r17
            r4 = 1
            r5 = 0
            goto L_0x002e
        L_0x036f:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.CMovies.a(com.movie.data.model.MovieInfo, java.lang.String, boolean):java.lang.String");
    }
}
