package com.utils.Getlink.Provider;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Vliver extends BaseProvider {
    public String c = "https://www.cliver.to";

    public String a() {
        return "Vliver";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        Document b = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]));
        String b2 = b.h("div[data-numcap=\"" + movieInfo.eps + "\"][data-numtemp=\"" + movieInfo.session + "\"]").b("data-url-vose");
        if (!b2.isEmpty()) {
            String replace = b2.replace("\\/", "/");
            boolean k = GoogleVideoHelper.k(replace);
            MediaSource mediaSource = new MediaSource(a() + " [ES]", k ? "GoogleVideo" : "CDN-FastServer", false);
            mediaSource.setStreamLink(replace);
            if (k) {
                mediaSource.setPlayHeader(hashMap);
            }
            mediaSource.setQuality(k ? GoogleVideoHelper.h(replace) : "HD");
            observableEmitter.onNext(mediaSource);
        }
    }

    public String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        String replace = Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+");
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.c);
        String str = this.c + "/buscar/?txt=" + replace;
        if (z) {
            HttpHelper.e().c(str, "tipo_contenido=peliculas;");
        } else {
            HttpHelper.e().c(str, "tipo_contenido=series;");
        }
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("div.titulo-p").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String G = element.h("h2").G();
            String G2 = element.h("span").G();
            if (G.equals(movieInfo.name) && G2.contains(movieInfo.year)) {
                String b = element.h("a").b("href");
                if (!b.startsWith("/")) {
                    return b;
                }
                return this.c + b;
            }
        }
        return "";
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        HashMap<String, String> a2 = Constants.a();
        a2.put("referer", str);
        String a3 = Regex.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]), "Idpelicula\\s*=\\s*[\"']([^\"']+[^\"'])[\"']", 1);
        JsonElement a4 = new JsonParser().a(HttpHelper.e().a(this.c + "/frm/obtener-enlaces-pelicula.php", "pelicula=" + a3, (Map<String, String>[]) new Map[]{a2}));
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        Iterator<JsonElement> it2 = a4.f().b("vose").iterator();
        while (it2.hasNext()) {
            String i = it2.next().f().a("token").i();
            String a5 = Regex.a(HttpHelper.e().a("https://directvideo.stream/getFile.php", "hash=" + i, (Map<String, String>[]) new Map[]{a2}), "[\"']url[\"']\\s*:\\s*[\"']([^\"']+[^\"'])[\"']", 1);
            if (!a5.isEmpty()) {
                String replace = a5.replace("\\/", "/");
                boolean k = GoogleVideoHelper.k(replace);
                MediaSource mediaSource = new MediaSource(a() + " [ES]", k ? "GoogleVideo" : "CDN-FastServer", false);
                mediaSource.setStreamLink(replace);
                if (k) {
                    mediaSource.setPlayHeader(hashMap);
                }
                mediaSource.setQuality(k ? GoogleVideoHelper.h(replace) : "HD");
                observableEmitter.onNext(mediaSource);
            }
        }
    }
}
