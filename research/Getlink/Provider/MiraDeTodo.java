package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.event.ReCaptchaRequiredEvent;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MiraDeTodo extends BaseProvider {
    private String c = Utils.getProvider(67);

    public String a() {
        return "MiraDeTodo";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo, "", "", true);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo, movieInfo.session, movieInfo.eps, false);
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str, String str2, boolean z) {
        String str3;
        String str4;
        String str5;
        String str6;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        MovieInfo movieInfo2 = movieInfo;
        String str7 = movieInfo2.name;
        if (str7.equalsIgnoreCase("Wonder Woman") && z) {
            str7 = "Mujer Maravilla";
        }
        if (z) {
            String a2 = HttpHelper.e().a(this.c + "/?s=" + com.original.tase.utils.Utils.a(str7, new boolean[0]), Constants.f5838a, this.c, (Map<String, String>[]) new Map[0]);
            if (a2.contains("Please complete the security check to access")) {
                RxBus.b().a(new ReCaptchaRequiredEvent(a(), this.c));
                return;
            }
            Iterator it2 = Jsoup.b(a2).g("div.item").iterator();
            str3 = "";
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Element element = (Element) it2.next();
                if (element.g("a[href]").size() > 0) {
                    str4 = element.g("a[href]").a("href");
                } else {
                    str4 = "";
                }
                if (element.g("span.tt").size() > 0) {
                    str5 = element.g("span.tt").c();
                } else {
                    str5 = "";
                }
                if (element.g("span.year").size() > 0) {
                    str6 = element.g("span.year").c().trim();
                } else {
                    str6 = Regex.a(str5, ".*?\\s+\\((\\d{4})\\)", 2);
                }
                TitleHelper.f(str5);
                TitleHelper.f(str7);
                if (str4.isEmpty() || str5.isEmpty() || str5.matches("\\d+\\s*x\\s*\\d+") || !TitleHelper.f(str5).equals(TitleHelper.f(str7)) || (!str6.trim().isEmpty() && com.original.tase.utils.Utils.b(str6.trim()) && Integer.parseInt(movieInfo2.year) > 0 && Integer.parseInt(str6.trim()) != Integer.parseInt(movieInfo2.year))) {
                    str3 = str4;
                }
            }
            str3 = str4;
        } else {
            String g = TitleHelper.g(TitleHelper.e(str7));
            String a3 = HttpHelper.e().a(this.c + "/series/" + g, Constants.f5838a, this.c, (Map<String, String>[]) new Map[0]);
            if (a3.contains("Please complete the security check to access")) {
                RxBus.b().a(new ReCaptchaRequiredEvent(a(), this.c));
                return;
            }
            String a4 = Regex.a(a3, "tag\"[^>]*>(\\d{4})", 1, true);
            if (a4.isEmpty()) {
                a4 = Regex.a(a3, "<h1\\s*[^>]*?itemprop=['\"]name['\"][^>]*?>.*?\\(\\s*(\\d{4})\\s*-?.*?\\)\\s*<", 1, true);
            }
            if (!a4.isEmpty() && Integer.parseInt(a4.trim()) == Integer.parseInt(movieInfo2.year)) {
                str3 = this.c + "/episodio/" + g + "-" + str + "x" + str2;
            } else {
                return;
            }
        }
        String a5 = Regex.a(str3, "(?://.+?|)(/.+)", 1);
        if (!a5.isEmpty()) {
            String str8 = this.c + a5;
            Document b = Jsoup.b(HttpHelper.e().a(str8, Constants.f5838a, this.c, (Map<String, String>[]) new Map[0]));
            Elements g2 = b.g("div.movieplay");
            g2.addAll(b.g("div.embed2").b("div"));
            ArrayList arrayList = new ArrayList();
            try {
                Iterator it3 = b.g("ul.idTabs").b("a[href]").iterator();
                while (it3.hasNext()) {
                    Element element2 = (Element) it3.next();
                    if (element2.G().toLowerCase().contains("dob") || element2.G().toLowerCase().contains("ads") || element2.G().toLowerCase().contains("audio") || element2.G().toLowerCase().contains("latino")) {
                        arrayList.add(element2.b("href").replace("#", ""));
                    }
                }
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            Iterator it4 = g2.iterator();
            while (it4.hasNext()) {
                Element element3 = (Element) it4.next();
                Element n = element3.n();
                if (n.z() == null || n.z().isEmpty() || !arrayList.contains(n.z())) {
                    Element h = element3.h("iframe");
                    if (h != null) {
                        if (h.toString().contains("data-lazy-src")) {
                            String b2 = h.b("data-lazy-src");
                            if (b2.startsWith("/")) {
                                b2 = this.c + b2;
                            }
                            if (!b2.toLowerCase().contains("miradetodo")) {
                                a(observableEmitter2, b2, "HD", new boolean[0]);
                            }
                        }
                        if (h.d("src")) {
                            String b3 = h.b("src");
                            if (b3.startsWith("/")) {
                                b3 = this.c + b3;
                            }
                            if (!b3.toLowerCase().contains("miradetodo")) {
                                a(observableEmitter2, b3, "HD", new boolean[0]);
                            }
                        }
                    }
                    Iterator it5 = Regex.b(element3.y(), "(?:\"|')(https.+?miradetodo\\..+?)(?:\"|')", 1, true).get(0).iterator();
                    while (it5.hasNext()) {
                        String str9 = (String) it5.next();
                        if (!str9.endsWith(".gif")) {
                            try {
                                a(observableEmitter2, str8, str9);
                            } catch (Exception unused) {
                            }
                            Document b4 = Jsoup.b(HttpHelper.e().a(str9, Constants.f5838a, str8, (Map<String, String>[]) new Map[0]));
                            b4.g("iframe[src]").isEmpty();
                            b4.toString().contains("nav");
                            if (b4.g("iframe[src]").isEmpty() && b4.y().contains("nav")) {
                                Iterator it6 = b4.g("li").iterator();
                                while (it6.hasNext()) {
                                    Iterator it7 = ((Element) it6.next()).g("a[href]").iterator();
                                    while (it7.hasNext()) {
                                        String b5 = ((Element) it7.next()).b("href");
                                        if (b5.contains("miradetodo")) {
                                            try {
                                                a(observableEmitter2, str8, b5);
                                            } catch (Exception unused2) {
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:134:0x0351 A[Catch:{ all -> 0x03b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x037b A[Catch:{ all -> 0x03b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x039c A[Catch:{ all -> 0x03b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x03a1 A[Catch:{ all -> 0x03b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0289 A[SYNTHETIC, Splitter:B:92:0x0289] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02d7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r23, java.lang.String r24, java.lang.String r25) {
        /*
            r22 = this;
            r1 = r22
            r2 = r23
            r3 = r24
            java.lang.String r4 = "\n"
            java.lang.String r5 = "src"
            java.lang.String r6 = "iframe[src]"
            java.lang.String r7 = ""
            boolean r0 = r23.isDisposed()
            if (r0 != 0) goto L_0x03c2
            java.lang.String r0 = "&amp;"
            java.lang.String r8 = "&"
            r9 = r25
            java.lang.String r0 = r9.replace(r0, r8)
            java.lang.String r8 = ".gif"
            boolean r9 = r0.endsWith(r8)
            if (r9 != 0) goto L_0x03c2
            java.lang.String r9 = ".png"
            boolean r10 = r0.endsWith(r9)
            if (r10 != 0) goto L_0x03c2
            java.lang.String r10 = "//"
            boolean r11 = r0.startsWith(r10)
            java.lang.String r12 = "https:"
            java.lang.String r13 = "/"
            if (r11 == 0) goto L_0x004b
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r12)
            r11.append(r0)
            java.lang.String r0 = r11.toString()
        L_0x0049:
            r11 = r0
            goto L_0x0063
        L_0x004b:
            boolean r11 = r0.startsWith(r13)
            if (r11 == 0) goto L_0x0049
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r14 = r1.c
            r11.append(r14)
            r11.append(r0)
            java.lang.String r0 = r11.toString()
            goto L_0x0049
        L_0x0063:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.HashMap r14 = new java.util.HashMap
            r14.<init>()
            java.lang.String r15 = "Connection"
            r25 = r0
            java.lang.String r0 = "close"
            r14.put(r15, r0)
            java.lang.String r0 = com.original.Constants.f5838a
            java.lang.String r15 = "User-Agent"
            r14.put(r15, r0)
            java.lang.String r0 = "player.miradetodo.net/api/mp4.php?"
            boolean r16 = r11.contains(r0)
            if (r16 == 0) goto L_0x0091
            r16 = r15
            java.lang.String r15 = "miradetodo.net/stream/mp4play.php?"
            java.lang.String r0 = r11.replace(r0, r15)
            r1.a(r2, r3, r0)
            goto L_0x00a4
        L_0x0091:
            r16 = r15
            java.lang.String r0 = "player.miradetodo.net/api/openload.php?"
            boolean r15 = r11.contains(r0)
            if (r15 == 0) goto L_0x00a4
            java.lang.String r15 = "player.miradetodo.net/api/ol.php?"
            java.lang.String r0 = r11.replace(r0, r15)
            r1.a(r2, r3, r0)
        L_0x00a4:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r15 = com.original.Constants.f5838a
            r17 = r14
            r14 = 0
            r18 = r9
            java.util.Map[] r9 = new java.util.Map[r14]
            java.lang.String r0 = r0.a((java.lang.String) r11, (java.lang.String) r15, (java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r9)
            org.jsoup.nodes.Document r9 = org.jsoup.Jsoup.b(r0)
            r15 = 0
            java.util.ArrayList r15 = r1.g(r0)     // Catch:{ Exception -> 0x00d3 }
            org.jsoup.select.Elements r0 = r9.g((java.lang.String) r6)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r0 = r0.a(r5)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r0 = r0.replace(r4, r7)     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r14 = "\r"
            java.lang.String r0 = r0.replace(r14, r7)     // Catch:{ Exception -> 0x00d3 }
            r1.a((io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource>) r2, (java.lang.String) r0)     // Catch:{ Exception -> 0x00d3 }
        L_0x00d3:
            java.lang.String r0 = "a[href]"
            org.jsoup.select.Elements r0 = r9.g((java.lang.String) r0)
            java.util.Iterator r0 = r0.iterator()
        L_0x00dd:
            boolean r9 = r0.hasNext()
            if (r9 == 0) goto L_0x01b0
            java.lang.Object r9 = r0.next()     // Catch:{ Exception -> 0x01a2 }
            org.jsoup.nodes.Element r9 = (org.jsoup.nodes.Element) r9     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r14 = "href"
            java.lang.String r9 = r9.b((java.lang.String) r14)     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r14 = r9.trim()     // Catch:{ Exception -> 0x01a2 }
            java.lang.String r14 = r14.toLowerCase()     // Catch:{ Exception -> 0x01a2 }
            r19 = r0
            java.lang.String r0 = "javascript"
            boolean r0 = r14.contains(r0)     // Catch:{ Exception -> 0x01a4 }
            if (r0 != 0) goto L_0x01a4
            boolean r0 = r9.startsWith(r10)     // Catch:{ Exception -> 0x01a4 }
            if (r0 == 0) goto L_0x0117
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            r0.<init>()     // Catch:{ Exception -> 0x01a4 }
            r0.append(r12)     // Catch:{ Exception -> 0x01a4 }
            r0.append(r9)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r9 = r0.toString()     // Catch:{ Exception -> 0x01a4 }
            goto L_0x012e
        L_0x0117:
            boolean r0 = r9.startsWith(r13)     // Catch:{ Exception -> 0x01a4 }
            if (r0 == 0) goto L_0x012e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a4 }
            r0.<init>()     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r14 = r1.c     // Catch:{ Exception -> 0x01a4 }
            r0.append(r14)     // Catch:{ Exception -> 0x01a4 }
            r0.append(r9)     // Catch:{ Exception -> 0x01a4 }
            java.lang.String r9 = r0.toString()     // Catch:{ Exception -> 0x01a4 }
        L_0x012e:
            boolean r0 = r9.endsWith(r8)     // Catch:{ Exception -> 0x01a4 }
            if (r0 != 0) goto L_0x01a4
            r0 = r18
            boolean r14 = r9.endsWith(r0)     // Catch:{ Exception -> 0x019f }
            if (r14 != 0) goto L_0x019c
            com.original.tase.helper.http.HttpHelper r14 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x019f }
            r18 = r0
            java.lang.String r0 = com.original.Constants.f5838a     // Catch:{ Exception -> 0x01a4 }
            r20 = r8
            r21 = r10
            r8 = 1
            java.util.Map[] r10 = new java.util.Map[r8]     // Catch:{ Exception -> 0x01a8 }
            r8 = 0
            r10[r8] = r17     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r0 = r14.a((java.lang.String) r9, (java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r10)     // Catch:{ Exception -> 0x01a8 }
            java.util.ArrayList r8 = r1.g(r0)     // Catch:{ Exception -> 0x01a8 }
            r15.addAll(r8)     // Catch:{ Exception -> 0x01a8 }
            boolean r8 = com.original.tase.helper.js.JsUnpacker.m30920(r0)     // Catch:{ Exception -> 0x01a8 }
            if (r8 == 0) goto L_0x017b
            java.lang.String r8 = "MiraDeTodo "
            java.util.ArrayList r9 = com.original.tase.helper.js.JsUnpacker.m30916(r0)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x01a8 }
            com.original.tase.Logger.a((java.lang.String) r8, (java.lang.String) r9)     // Catch:{ Exception -> 0x01a8 }
            java.util.ArrayList r8 = com.original.tase.helper.js.JsUnpacker.m30916(r0)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x01a8 }
            java.util.ArrayList r8 = r1.g(r8)     // Catch:{ Exception -> 0x01a8 }
            r15.add(r8)     // Catch:{ Exception -> 0x01a8 }
        L_0x017b:
            java.lang.String r8 = "<iframe.*?src=['\"]([^'\"]+[^'\"])['\"]"
            r9 = 1
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r8, (int) r9)     // Catch:{ Exception -> 0x01a8 }
            boolean r9 = r8.isEmpty()     // Catch:{ Exception -> 0x01a8 }
            if (r9 != 0) goto L_0x018b
            r15.add(r8)     // Catch:{ Exception -> 0x01a8 }
        L_0x018b:
            java.lang.String r8 = "AmazonPlayer.*?file\\s*:\\s*\"([^\"]+)"
            r9 = 1
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r8, (int) r9, (boolean) r9)     // Catch:{ Exception -> 0x01a8 }
            boolean r8 = r0.isEmpty()     // Catch:{ Exception -> 0x01a8 }
            if (r8 != 0) goto L_0x01a8
            r15.add(r0)     // Catch:{ Exception -> 0x01a8 }
            goto L_0x01a8
        L_0x019c:
            r18 = r0
            goto L_0x01a4
        L_0x019f:
            r18 = r0
            goto L_0x01a4
        L_0x01a2:
            r19 = r0
        L_0x01a4:
            r20 = r8
            r21 = r10
        L_0x01a8:
            r0 = r19
            r8 = r20
            r10 = r21
            goto L_0x00dd
        L_0x01b0:
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x026f }
            r0.<init>(r11)     // Catch:{ all -> 0x026f }
            java.util.Map r0 = com.original.tase.utils.Utils.a((java.net.URL) r0)     // Catch:{ all -> 0x026f }
            java.lang.String r8 = "id"
            boolean r8 = r0.containsKey(r8)     // Catch:{ all -> 0x026f }
            if (r8 == 0) goto L_0x0279
            java.lang.String r8 = "id"
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x026f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x026f }
            r8 = r25
            boolean r9 = r8.contains(r0)     // Catch:{ all -> 0x026f }
            if (r9 != 0) goto L_0x0279
            r8.add(r0)     // Catch:{ all -> 0x026f }
            java.util.HashMap r8 = com.original.Constants.a()     // Catch:{ all -> 0x026f }
            java.lang.String r9 = "Referer"
            r8.put(r9, r11)     // Catch:{ all -> 0x026f }
            java.lang.String r9 = com.original.Constants.f5838a     // Catch:{ all -> 0x026f }
            r10 = r16
            r8.put(r10, r9)     // Catch:{ all -> 0x026b }
            r9 = 0
            boolean[] r12 = new boolean[r9]     // Catch:{ all -> 0x026b }
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r12)     // Catch:{ all -> 0x026b }
            java.lang.String r9 = "%3D"
            java.lang.String r12 = "="
            java.lang.String r0 = r0.replace(r9, r12)     // Catch:{ all -> 0x026b }
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x026b }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x026b }
            r12.<init>()     // Catch:{ all -> 0x026b }
            java.lang.String r13 = r1.c     // Catch:{ all -> 0x026b }
            r12.append(r13)     // Catch:{ all -> 0x026b }
            java.lang.String r13 = "/stream/plugins/gkpluginsphp.php"
            r12.append(r13)     // Catch:{ all -> 0x026b }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x026b }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x026b }
            r13.<init>()     // Catch:{ all -> 0x026b }
            java.lang.String r14 = "link="
            r13.append(r14)     // Catch:{ all -> 0x026b }
            r13.append(r0)     // Catch:{ all -> 0x026b }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x026b }
            r16 = r10
            r14 = 1
            java.util.Map[] r10 = new java.util.Map[r14]     // Catch:{ all -> 0x026f }
            r14 = 0
            r10[r14] = r8     // Catch:{ all -> 0x026f }
            java.lang.String r9 = r9.a((java.lang.String) r12, (java.lang.String) r13, (java.util.Map<java.lang.String, java.lang.String>[]) r10)     // Catch:{ all -> 0x026f }
            java.util.HashMap r10 = com.original.tase.helper.GkPluginsHelper.a(r9)     // Catch:{ all -> 0x026f }
            java.util.Set r10 = r10.keySet()     // Catch:{ all -> 0x026f }
            r15.addAll(r10)     // Catch:{ all -> 0x026f }
            java.lang.String r10 = "amazon"
            boolean r10 = r9.contains(r10)     // Catch:{ all -> 0x026f }
            if (r10 == 0) goto L_0x0279
            java.lang.String r10 = "clouddrive"
            boolean r9 = r9.contains(r10)     // Catch:{ all -> 0x026f }
            if (r9 == 0) goto L_0x0279
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x026f }
            r9.<init>()     // Catch:{ all -> 0x026f }
            java.lang.String r10 = "http://player.miradetodo.net/api/get.php?id="
            r9.append(r10)     // Catch:{ all -> 0x026f }
            r9.append(r0)     // Catch:{ all -> 0x026f }
            java.lang.String r0 = r9.toString()     // Catch:{ all -> 0x026f }
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x026f }
            r10 = 1
            java.util.Map[] r12 = new java.util.Map[r10]     // Catch:{ all -> 0x026f }
            r10 = 0
            r12[r10] = r8     // Catch:{ all -> 0x026f }
            java.lang.String r8 = r9.a((java.lang.String) r0, (boolean) r10, (java.util.Map<java.lang.String, java.lang.String>[]) r12)     // Catch:{ all -> 0x026f }
            boolean r0 = r8.equalsIgnoreCase(r0)     // Catch:{ all -> 0x026f }
            if (r0 != 0) goto L_0x0279
            r15.add(r8)     // Catch:{ all -> 0x026f }
            goto L_0x0279
        L_0x026b:
            r0 = move-exception
            r16 = r10
            goto L_0x0270
        L_0x026f:
            r0 = move-exception
        L_0x0270:
            r8 = 1
            boolean[] r9 = new boolean[r8]
            r10 = 0
            r9[r10] = r8
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)
        L_0x0279:
            java.lang.String r0 = r11.trim()
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r8 = "gd.php"
            boolean r0 = r0.contains(r8)
            if (r0 == 0) goto L_0x02bd
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x02b6 }
            java.lang.String r9 = "gdplay.php"
            java.lang.String r8 = r11.replace(r8, r9)     // Catch:{ all -> 0x02b6 }
            java.lang.String r9 = com.original.Constants.f5838a     // Catch:{ all -> 0x02b6 }
            r10 = 0
            java.util.Map[] r11 = new java.util.Map[r10]     // Catch:{ all -> 0x02b6 }
            java.lang.String r0 = r0.a((java.lang.String) r8, (java.lang.String) r9, (java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r11)     // Catch:{ all -> 0x02b6 }
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)     // Catch:{ all -> 0x02b6 }
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r6)     // Catch:{ all -> 0x02b6 }
            java.lang.String r0 = r0.a(r5)     // Catch:{ all -> 0x02b6 }
            java.lang.String r0 = r0.replace(r4, r7)     // Catch:{ all -> 0x02b6 }
            java.lang.String r3 = "\r"
            java.lang.String r0 = r0.replace(r3, r7)     // Catch:{ all -> 0x02b6 }
            r1.a((io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource>) r2, (java.lang.String) r0)     // Catch:{ all -> 0x02b6 }
            goto L_0x02bd
        L_0x02b6:
            r0 = move-exception
            r3 = 0
            boolean[] r4 = new boolean[r3]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r4)
        L_0x02bd:
            java.util.ArrayList r0 = com.original.tase.utils.Utils.a(r15)
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r4 = com.original.Constants.f5838a
            r5 = r16
            r3.put(r5, r4)
            java.util.Iterator r4 = r0.iterator()
        L_0x02d1:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x03c2
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r5 = r23.isDisposed()     // Catch:{ all -> 0x03b8 }
            if (r5 == 0) goto L_0x02e4
            return
        L_0x02e4:
            boolean r5 = com.original.tase.helper.GoogleVideoHelper.k(r0)     // Catch:{ all -> 0x03b8 }
            if (r5 == 0) goto L_0x03ab
            com.original.tase.helper.http.HttpHelper r5 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x03b8 }
            r8 = 1
            java.util.Map[] r6 = new java.util.Map[r8]     // Catch:{ all -> 0x03b6 }
            r9 = 0
            r6[r9] = r3     // Catch:{ all -> 0x03b6 }
            java.lang.String r0 = r5.a((java.lang.String) r0, (boolean) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r6)     // Catch:{ all -> 0x03b6 }
            if (r0 == 0) goto L_0x02d1
            boolean r5 = r0.isEmpty()     // Catch:{ all -> 0x03b6 }
            if (r5 != 0) goto L_0x02d1
            java.lang.String r5 = "srt"
            boolean r5 = r0.endsWith(r5)     // Catch:{ all -> 0x03b6 }
            if (r5 != 0) goto L_0x02d1
            java.lang.String r5 = "png"
            boolean r5 = r0.endsWith(r5)     // Catch:{ all -> 0x03b6 }
            if (r5 != 0) goto L_0x02d1
            boolean r5 = com.original.tase.helper.GoogleVideoHelper.k(r0)     // Catch:{ all -> 0x03b6 }
            java.lang.String r6 = r0.trim()     // Catch:{ all -> 0x03b6 }
            java.lang.String r6 = r6.toLowerCase()     // Catch:{ all -> 0x03b6 }
            java.lang.String r9 = "amazon"
            boolean r6 = r6.contains(r9)     // Catch:{ all -> 0x03b6 }
            java.lang.String r9 = r22.a()     // Catch:{ all -> 0x03b6 }
            if (r5 == 0) goto L_0x032b
            java.lang.String r10 = "GoogleVideo"
            goto L_0x0332
        L_0x032b:
            if (r6 == 0) goto L_0x0330
            java.lang.String r10 = "AWS-FastServer"
            goto L_0x0332
        L_0x0330:
            java.lang.String r10 = "CDN-FastServer"
        L_0x0332:
            if (r5 != 0) goto L_0x0349
            if (r6 != 0) goto L_0x0349
            java.lang.String r11 = ".mp4"
            boolean r11 = r0.endsWith(r11)     // Catch:{ all -> 0x03b6 }
            if (r11 != 0) goto L_0x0349
            java.lang.String r11 = "yandex"
            boolean r11 = r0.contains(r11)     // Catch:{ all -> 0x03b6 }
            if (r11 == 0) goto L_0x0347
            goto L_0x0349
        L_0x0347:
            r11 = 1
            goto L_0x034a
        L_0x0349:
            r11 = 0
        L_0x034a:
            com.original.tase.model.media.MediaSource r12 = new com.original.tase.model.media.MediaSource     // Catch:{ all -> 0x03b6 }
            r12.<init>(r9, r10, r11)     // Catch:{ all -> 0x03b6 }
            if (r6 == 0) goto L_0x0379
            java.lang.String r6 = "?download=TRUE&"
            java.lang.String r9 = "?"
            java.lang.String r0 = r0.replace(r6, r9)     // Catch:{ all -> 0x03b6 }
            java.lang.String r6 = "?download=TRUE"
            java.lang.String r0 = r0.replace(r6, r7)     // Catch:{ all -> 0x03b6 }
            java.lang.String r6 = "?download=true&"
            java.lang.String r9 = "?"
            java.lang.String r0 = r0.replace(r6, r9)     // Catch:{ all -> 0x03b6 }
            java.lang.String r6 = "?download=true"
            java.lang.String r0 = r0.replace(r6, r7)     // Catch:{ all -> 0x03b6 }
            java.lang.String r6 = "&download=TRUE"
            java.lang.String r0 = r0.replace(r6, r7)     // Catch:{ all -> 0x03b6 }
            java.lang.String r6 = "&download=true"
            java.lang.String r0 = r0.replace(r6, r7)     // Catch:{ all -> 0x03b6 }
        L_0x0379:
            if (r5 == 0) goto L_0x0397
            java.lang.String r6 = r22.a()     // Catch:{ all -> 0x03b6 }
            java.util.List r6 = com.original.tase.helper.GoogleVideoHelper.b(r0, r6)     // Catch:{ all -> 0x03b6 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x03b6 }
        L_0x0387:
            boolean r9 = r6.hasNext()     // Catch:{ all -> 0x03b6 }
            if (r9 == 0) goto L_0x0397
            java.lang.Object r9 = r6.next()     // Catch:{ all -> 0x03b6 }
            com.original.tase.model.media.MediaSource r9 = (com.original.tase.model.media.MediaSource) r9     // Catch:{ all -> 0x03b6 }
            r2.onNext(r9)     // Catch:{ all -> 0x03b6 }
            goto L_0x0387
        L_0x0397:
            r12.setStreamLink(r0)     // Catch:{ all -> 0x03b6 }
            if (r5 == 0) goto L_0x03a1
            java.lang.String r0 = com.original.tase.helper.GoogleVideoHelper.h(r0)     // Catch:{ all -> 0x03b6 }
            goto L_0x03a3
        L_0x03a1:
            java.lang.String r0 = "HD"
        L_0x03a3:
            r12.setQuality((java.lang.String) r0)     // Catch:{ all -> 0x03b6 }
            r2.onNext(r12)     // Catch:{ all -> 0x03b6 }
            goto L_0x02d1
        L_0x03ab:
            r8 = 1
            java.lang.String r5 = "HD"
            r6 = 0
            boolean[] r9 = new boolean[r6]     // Catch:{ all -> 0x03b6 }
            r1.a(r2, r0, r5, r9)     // Catch:{ all -> 0x03b6 }
            goto L_0x02d1
        L_0x03b6:
            r0 = move-exception
            goto L_0x03ba
        L_0x03b8:
            r0 = move-exception
            r8 = 1
        L_0x03ba:
            r5 = 0
            boolean[] r6 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r6)
            goto L_0x02d1
        L_0x03c2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.MiraDeTodo.a(io.reactivex.ObservableEmitter, java.lang.String, java.lang.String):void");
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        String str2;
        String replace = str.replace(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "").replace("\r", "");
        if (replace != null && !replace.equals("")) {
            if (GoogleVideoHelper.j(replace)) {
                HashMap<String, String> g = GoogleVideoHelper.g(replace);
                if (g != null && !g.isEmpty()) {
                    for (Map.Entry next : g.entrySet()) {
                        MediaSource mediaSource = new MediaSource(a(), "GoogleVideo", false);
                        mediaSource.setOriginalLink(replace);
                        mediaSource.setStreamLink((String) next.getKey());
                        if (((String) next.getValue()).isEmpty()) {
                            str2 = "HD";
                        } else {
                            str2 = (String) next.getValue();
                        }
                        mediaSource.setQuality(str2);
                        HashMap hashMap = new HashMap();
                        hashMap.put("User-Agent", Constants.f5838a);
                        hashMap.put("Cookie", GoogleVideoHelper.c(replace, (String) next.getKey()));
                        mediaSource.setPlayHeader(hashMap);
                        observableEmitter.onNext(mediaSource);
                    }
                    return;
                }
                return;
            }
            a(observableEmitter, replace, "HD", new boolean[0]);
        }
    }
}
