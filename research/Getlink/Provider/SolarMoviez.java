package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class SolarMoviez extends BaseProvider {
    public static String d = "";
    private String c = Utils.getProvider(21);

    private static String c() {
        String str = d;
        if (str == null || str.isEmpty()) {
            try {
                d = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL1NvbGFyTW92aWV6LnR4dA==", 10), "UTF-8");
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                d = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL1NvbGFyTW92aWV6LnR4dA==", 10));
            }
        }
        return d;
    }

    public String a() {
        return "SolarMoviez";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str = "";
        String a2 = TitleHelper.a(movieInfo.name.toLowerCase(), "+");
        HashMap<String, String> a3 = Constants.a();
        a3.put("origin", this.c);
        a3.put("referer", this.c + "/");
        a3.put("accept", "application/json, text/javascript, */*; q=0.01");
        try {
            JSONArray jSONArray = new JSONArray(HttpHelper.e().a(this.c + "/ajax/search.php", String.format("q=%s&limit=8&timestamp=%s&verifiedCheck=", new Object[]{a2, DateTimeHelper.c()}), (Map<String, String>[]) new Map[]{a3}));
            if (jSONArray.length() > 0) {
                int i = 0;
                while (true) {
                    if (i >= jSONArray.length()) {
                        break;
                    }
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string = jSONObject.getString("permalink");
                    String string2 = jSONObject.getString("title");
                    int i2 = jSONObject.getInt("year");
                    if (!string2.isEmpty() && TitleHelper.a(string2.toLowerCase(), str).startsWith(TitleHelper.a(movieInfo.name.toLowerCase(), str)) && i2 == movieInfo.getYear().intValue()) {
                        str = string;
                        break;
                    }
                    i++;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!str.isEmpty()) {
            HashMap hashMap = new HashMap();
            hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            hashMap.put("referer", this.c + "/");
            hashMap.put("accept-language", "en-US;q=0.9,en;q=0.8");
            hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
            HttpHelper.e().c(this.c, "SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1;");
            String a4 = Regex.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap}), "href\\s*=\\s*['\"]([^'\"]*?" + ("s" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.session)) + "e" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.eps))) + ".*\\/\\d+[^'\"]+)['\"]", 1);
            if (!a4.isEmpty()) {
                String b = HttpHelper.e().b(a4, str);
                String a5 = HttpHelper.e().a(c(), (Map<String, String>[]) new Map[0]);
                if (!a5.isEmpty()) {
                    Iterator it2 = Jsoup.b(b).g("div#listlink").b("a[onclick]").iterator();
                    while (it2.hasNext()) {
                        try {
                            String f = f(a5, Regex.a(((Element) it2.next()).b("onclick"), "window\\.open\\((.*['\"]\\))\\s*\\,", 1));
                            if (!f.isEmpty()) {
                                a(observableEmitter, f, "HD", false);
                            }
                        } catch (Throwable unused) {
                            return;
                        }
                    }
                }
            }
        }
    }

    public String f(String str, String str2) {
        Duktape create = Duktape.create();
        try {
            Object evaluate = create.evaluate(String.format("%s function acb(){return %s; }acb();", new Object[]{str, str2}));
            if (evaluate != null) {
                String obj = evaluate.toString();
                create.close();
                return obj;
            }
        } catch (Throwable unused) {
        }
        create.close();
        return "";
    }
}
