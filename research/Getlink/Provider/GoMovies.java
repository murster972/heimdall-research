package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class GoMovies extends BaseProvider {
    private String c = Utils.getProvider(92);
    private String d = (this.c + "/");
    private String e = "HQ";

    public String a() {
        return "GoMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        String str2;
        boolean z;
        String b;
        boolean z2 = movieInfo.getType().intValue() == 1;
        HashMap hashMap = new HashMap();
        hashMap.put("referer", str);
        hashMap.put("user-agent", Constants.f5838a);
        try {
            str = Jsoup.b(HttpHelper.e().b(str, this.d)).h("div.ds_seriesplay.dsclear").h("a").b("href");
        } catch (Throwable unused) {
        }
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap});
        if (!z2) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put("referer", str + "watching.html");
            hashMap2.put("user-agent", Constants.f5838a);
            Iterator it2 = Jsoup.b(a2).g("div[class=les-content]").b("a").iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                Element element = (Element) it2.next();
                b = element.b("href");
                String lowerCase = element.G().toLowerCase();
                if (lowerCase.contains("episode " + movieInfo.eps + " -")) {
                    break;
                }
                if (lowerCase.equalsIgnoreCase("episode " + movieInfo.eps)) {
                    break;
                }
                if (!lowerCase.contains(movieInfo.eps + " -")) {
                    if (!lowerCase.equals(movieInfo.eps)) {
                        if (com.original.tase.utils.Utils.a("000", movieInfo.getEps().intValue()).equals(lowerCase)) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            a2 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[]{hashMap2});
            z = true;
            if (!z) {
                return;
            }
        }
        String a3 = Regex.a(a2, "iframe\\s*src\\s*=\\s*[\"']([^\"']+[^\"'])[\"']", 1);
        String a4 = HttpHelper.e().a(a3, (Map<String, String>[]) new Map[0]);
        String a5 = Regex.a(a4, "var\\s*tc\\s*=\\s*[\"']([^\"']+[^\"'])[\"']", 1);
        String a6 = Regex.a(a4, "url\\s*:\\s*[\"']([^\"']+[^\"'])[\"']", 1);
        String a7 = Regex.a(a4, "[\"']_token[\"']\\s*:\\s*[\"']([^\"']+[^\"'])[\"']", 1);
        Duktape create = Duktape.create();
        try {
            Object evaluate = create.evaluate("function _tsd_tsd_ds(n){return _35qx74(_F94xF3(_99x233g(n.slice(2,13))))+\"13295763\"}function _99x233g(n){return n.split(\"\")}function _F94xF3(n){return n.reverse()}function _35qx74(n){return n.join(\"\")}function acb(){return _tsd_tsd_ds(\"####\")}acb();".replace("####", a5));
            if (evaluate == null) {
                create.close();
                return;
            }
            str2 = evaluate.toString();
            create.close();
            HashMap<String, String> a8 = Constants.a();
            a8.put("x-token", str2);
            a8.put("origin", BaseProvider.i(a6));
            a8.put("referer", a3.replace(BaseProvider.i(a3), BaseProvider.i(a6)));
            Iterator it3 = Regex.b(HttpHelper.e().a(a6, String.format("tokenCode=%s&_token=%s", new Object[]{a5, a7}), (Map<String, String>[]) new Map[]{a8}), "[\"']([^\"']+[^\"'])[\"']", 1, true).get(0).iterator();
            while (it3.hasNext()) {
                a(observableEmitter, (String) it3.next(), this.e, true);
            }
        } catch (Throwable unused2) {
            create.close();
            str2 = "";
        }
    }

    private String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.c);
        hashMap.put("user-agent", Constants.f5838a);
        String str = this.d + "search?s=" + TitleHelper.d(movieInfo.name).replace("-", "+");
        HttpHelper.e().a(str);
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap});
        if (a2.contains("Attention Required! | Cloudflare")) {
            Logger.a("Need Verify Recaptcha", str);
            Utils.a(str, BaseProvider.h(str));
            return "";
        }
        Iterator it2 = Jsoup.b(a2).g("div.featuredItems.singleVideo").iterator();
        while (it2.hasNext()) {
            try {
                Element element = (Element) it2.next();
                Element h = element.h("a");
                String b = h.b("href");
                String b2 = h.b("title");
                this.e = element.h("span[class*=mli-quality]").G();
                String a3 = Regex.a(element.toString(), "jt-info\\\">?\\r?\\n?\\s*(\\d+)?\\r?\\n?\\s*<", 1);
                if (b2.isEmpty()) {
                    b2 = element.h("h4").G();
                }
                if (!z) {
                    if (b2.toLowerCase().contains(movieInfo.name.toLowerCase() + " - season " + movieInfo.session)) {
                        return b;
                    }
                } else if (b2.equalsIgnoreCase(movieInfo.name) && a3.contains(movieInfo.year)) {
                    return b;
                }
            } catch (Throwable unused) {
            }
        }
        return "";
    }
}
