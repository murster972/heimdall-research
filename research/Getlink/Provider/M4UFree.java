package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class M4UFree extends BaseProvider {
    public String c = Utils.getProvider(58);

    public String a() {
        return "M4UFree";
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x043e  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0447  */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x05b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.movie.data.model.MovieInfo r20, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r21) {
        /*
            r19 = this;
            r1 = r19
            r0 = r20
            r2 = r21
            java.lang.String r3 = r0.name
            java.lang.String r3 = r3.toLowerCase()
            java.lang.String r4 = "-"
            java.lang.String r3 = com.original.tase.helper.TitleHelper.a(r3, r4)
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.lang.String r6 = "Accept"
            java.lang.String r7 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
            r5.put(r6, r7)
            java.lang.String r7 = r1.c
            java.lang.String r8 = "http://"
            java.lang.String r9 = ""
            java.lang.String r7 = r7.replace(r8, r9)
            java.lang.String r10 = "Host"
            r5.put(r10, r7)
            java.lang.String r7 = "Upgrade-Insecure-Requests"
            java.lang.String r11 = "1"
            r5.put(r7, r11)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r11 = r1.c
            r7.append(r11)
            java.lang.String r11 = "/tag/"
            r7.append(r11)
            r7.append(r3)
            java.lang.String r3 = r7.toString()
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            r11 = 1
            java.util.Map[] r12 = new java.util.Map[r11]
            r13 = 0
            r12[r13] = r5
            java.lang.String r7 = r7.a((java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r12)
            java.lang.String r12 = "Please input more keywords"
            boolean r12 = r7.contains(r12)
            if (r12 == 0) goto L_0x0080
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r3)
            r7.append(r4)
            java.lang.String r3 = r0.year
            r7.append(r3)
            java.lang.String r3 = r7.toString()
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r7 = new java.util.Map[r11]
            r7[r13] = r5
            java.lang.String r7 = r4.a((java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r7)
        L_0x0080:
            org.jsoup.nodes.Document r4 = org.jsoup.Jsoup.b(r7)
            java.lang.String r12 = "a.top-item[href]"
            org.jsoup.select.Elements r4 = r4.g((java.lang.String) r12)
            java.util.Iterator r4 = r4.iterator()
            r12 = r9
        L_0x008f:
            boolean r14 = r4.hasNext()
            if (r14 == 0) goto L_0x0109
            java.lang.Object r14 = r4.next()
            org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
            java.lang.String r15 = "href"
            java.lang.String r15 = r14.b((java.lang.String) r15)
            java.lang.String r16 = r15.trim()
            java.lang.String r13 = r16.toLowerCase()
            java.lang.String r11 = "-tvshow-"
            boolean r11 = r13.contains(r11)
            if (r11 != 0) goto L_0x0106
            java.lang.String r11 = r14.y()
            java.lang.String r12 = "</?[^>]*>"
            java.lang.String r11 = r11.replaceAll(r12, r9)
            java.lang.String r12 = "^Watch\\s*"
            java.lang.String r11 = r11.replaceAll(r12, r9)
            java.lang.String r12 = "(.+)\\s+\\(?(\\d{4})\\)?"
            r13 = 1
            java.lang.String r14 = com.original.tase.utils.Regex.a((java.lang.String) r11, (java.lang.String) r12, (int) r13)
            boolean r13 = r7.isEmpty()
            if (r13 == 0) goto L_0x00cf
            r14 = r11
        L_0x00cf:
            r13 = 2
            java.lang.String r11 = com.original.tase.utils.Regex.a((java.lang.String) r11, (java.lang.String) r12, (int) r13)
            java.lang.String r12 = r0.name
            java.lang.String r12 = com.original.tase.helper.TitleHelper.f(r12)
            java.lang.String r13 = com.original.tase.helper.TitleHelper.f(r14)
            boolean r12 = r12.equals(r13)
            if (r12 == 0) goto L_0x0105
            java.lang.String r12 = r11.trim()
            boolean r12 = r12.isEmpty()
            if (r12 != 0) goto L_0x010a
            java.lang.String r12 = r11.trim()
            boolean r12 = com.original.tase.utils.Utils.b(r12)
            if (r12 == 0) goto L_0x010a
            java.lang.String r11 = r11.trim()
            java.lang.String r12 = r0.year
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x0105
            goto L_0x010a
        L_0x0105:
            r12 = r14
        L_0x0106:
            r11 = 1
            r13 = 0
            goto L_0x008f
        L_0x0109:
            r15 = r12
        L_0x010a:
            java.lang.String r4 = "/"
            boolean r0 = r15.startsWith(r4)
            if (r0 == 0) goto L_0x0124
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r7 = r1.c
            r0.append(r7)
            r0.append(r15)
            java.lang.String r15 = r0.toString()
            goto L_0x012b
        L_0x0124:
            boolean r0 = r7.isEmpty()
            if (r0 == 0) goto L_0x012b
            return
        L_0x012b:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            r7 = 0
            java.util.Map[] r11 = new java.util.Map[r7]
            java.lang.String r0 = r0.a((java.lang.String) r15, (java.util.Map<java.lang.String, java.lang.String>[]) r11)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r7 = "h3.h3-detail[title]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r7)
            java.util.Iterator r0 = r0.iterator()
        L_0x0144:
            boolean r7 = r0.hasNext()
            if (r7 == 0) goto L_0x018c
            java.lang.Object r7 = r0.next()
            org.jsoup.nodes.Element r7 = (org.jsoup.nodes.Element) r7
            java.lang.String r11 = "title"
            java.lang.String r11 = r7.b((java.lang.String) r11)
            java.lang.String r11 = r11.trim()
            java.lang.String r11 = r11.toLowerCase()
            java.lang.String r12 = "quality"
            boolean r11 = r11.startsWith(r12)
            if (r11 == 0) goto L_0x0144
            java.lang.String r11 = "span"
            org.jsoup.nodes.Element r7 = r7.h(r11)
            if (r7 == 0) goto L_0x0144
            java.lang.String r7 = r7.G()
            java.lang.String r7 = r7.trim()
            java.lang.String r7 = r7.toLowerCase()
            java.lang.String r11 = "ts"
            boolean r11 = r7.contains(r11)
            if (r11 != 0) goto L_0x018a
            java.lang.String r11 = "cam"
            boolean r7 = r7.contains(r11)
            if (r7 == 0) goto L_0x0144
        L_0x018a:
            r7 = 1
            goto L_0x018d
        L_0x018c:
            r7 = 0
        L_0x018d:
            boolean r0 = r15.isEmpty()
            if (r0 == 0) goto L_0x0194
            return
        L_0x0194:
            java.lang.String r0 = "Referer"
            r5.put(r0, r3)
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r11 = r11.a((java.lang.String) r3)
            java.lang.String r12 = "Cookie"
            r5.put(r12, r11)
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()
            r13 = 1
            java.util.Map[] r14 = new java.util.Map[r13]
            r13 = 0
            r14[r13] = r5
            java.lang.String r5 = r11.a((java.lang.String) r15, (java.util.Map<java.lang.String, java.lang.String>[]) r14)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r11.add(r5)
            org.jsoup.nodes.Document r5 = org.jsoup.Jsoup.b(r5)
            java.util.HashMap r13 = new java.util.HashMap
            r13.<init>()
            java.lang.String r14 = "*/*"
            r13.put(r6, r14)
            java.lang.String r2 = "Accept-Encoding"
            r20 = r7
            java.lang.String r7 = "identity;q=1, *;q=0"
            r13.put(r2, r7)
            java.lang.String r2 = "Accept-Language"
            java.lang.String r7 = "en-US"
            r13.put(r2, r7)
            java.lang.String r2 = "no-cache"
            java.lang.String r7 = "Cache-Control"
            r13.put(r7, r2)
            java.lang.String r7 = "Connection"
            r17 = r4
            java.lang.String r4 = "keep-alive"
            r13.put(r7, r4)
            java.lang.String r4 = "Pragma"
            r13.put(r4, r2)
            java.lang.String r2 = "X-TTV-Custom"
            java.lang.String r4 = "rangeFromZero"
            r13.put(r2, r4)
            r13.put(r0, r3)
            java.lang.String r2 = com.original.Constants.f5838a
            java.lang.String r4 = "User-Agent"
            r13.put(r4, r2)
            java.util.HashMap r2 = com.original.Constants.a()
            r2.put(r6, r14)
            java.lang.String r6 = r1.c
            java.lang.String r6 = r6.replace(r8, r9)
            r2.put(r10, r6)
            java.lang.String r6 = r1.c
            java.lang.String r7 = "Origin"
            r2.put(r7, r6)
            r2.put(r0, r15)
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r0.a((java.lang.String) r3)
            r2.put(r12, r0)
            java.lang.String r0 = com.original.Constants.f5838a
            r2.put(r4, r0)
            java.lang.String r0 = "span[class=btn-eps][link]"
            org.jsoup.select.Elements r0 = r5.g((java.lang.String) r0)
            java.util.Iterator r0 = r0.iterator()
            java.lang.String r3 = "span[class*=btn-eps][data]"
            org.jsoup.select.Elements r3 = r5.g((java.lang.String) r3)
            java.util.Iterator r3 = r3.iterator()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
        L_0x0243:
            boolean r7 = r0.hasNext()
            if (r7 == 0) goto L_0x0259
            java.lang.Object r7 = r0.next()
            org.jsoup.nodes.Element r7 = (org.jsoup.nodes.Element) r7
            java.lang.String r8 = "link"
            java.lang.String r7 = r7.b((java.lang.String) r8)
            r6.add(r7)
            goto L_0x0243
        L_0x0259:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x026f
            java.lang.Object r0 = r3.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r7 = "data"
            java.lang.String r0 = r0.b((java.lang.String) r7)
            r6.add(r0)
            goto L_0x0259
        L_0x026f:
            java.lang.String r0 = "span[class*=singlemv][data]"
            org.jsoup.select.Elements r0 = r5.g((java.lang.String) r0)
            java.util.Iterator r0 = r0.iterator()
        L_0x0279:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x028f
            java.lang.Object r3 = r0.next()
            org.jsoup.nodes.Element r3 = (org.jsoup.nodes.Element) r3
            java.lang.String r7 = "data"
            java.lang.String r3 = r3.b((java.lang.String) r7)
            r6.add(r3)
            goto L_0x0279
        L_0x028f:
            java.lang.String r0 = "meta[name=csrf-token]"
            org.jsoup.select.Elements r0 = r5.g((java.lang.String) r0)
            java.lang.String r3 = "content"
            java.lang.String r0 = r0.a(r3)
            java.util.Iterator r3 = r6.iterator()
        L_0x029f:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x02f1
            java.lang.Object r5 = r3.next()
            java.lang.String r5 = r5.toString()
            com.original.tase.helper.http.HttpHelper r6 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = r1.c
            r7.append(r8)
            java.lang.String r8 = "/ajax"
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "m4u="
            r8.append(r9)
            r9 = 0
            boolean[] r14 = new boolean[r9]
            java.lang.String r5 = com.original.tase.utils.Utils.a((java.lang.String) r5, (boolean[]) r14)
            r8.append(r5)
            java.lang.String r5 = "&_token="
            r8.append(r5)
            r8.append(r0)
            java.lang.String r5 = r8.toString()
            r8 = 1
            java.util.Map[] r14 = new java.util.Map[r8]
            r14[r9] = r2
            java.lang.String r5 = r6.a((java.lang.String) r7, (java.lang.String) r5, (boolean) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r14)
            r11.add(r5)
            goto L_0x029f
        L_0x02f1:
            java.util.Iterator r2 = r11.iterator()
        L_0x02f5:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x05d1
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.lang.String r5 = "<iframe\\s+src\\s*=\\s*\"([^\"]+)"
            r6 = 1
            java.util.ArrayList r5 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r5, (int) r6, (boolean) r6)
            r6 = 0
            java.lang.Object r5 = r5.get(r6)
            java.util.ArrayList r5 = (java.util.ArrayList) r5
            java.util.Iterator r5 = r5.iterator()
        L_0x0318:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x0334
            java.lang.Object r0 = r5.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r6 = r0.isEmpty()
            if (r6 != 0) goto L_0x0318
            boolean r6 = r3.contains(r0)
            if (r6 != 0) goto L_0x0318
            r3.add(r0)
            goto L_0x0318
        L_0x0334:
            java.lang.String r5 = "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)"
            r6 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r5, (int) r6, (boolean) r6)
            r5 = 0
            java.lang.Object r0 = r0.get(r5)
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            java.util.Iterator r0 = r0.iterator()
        L_0x0346:
            boolean r5 = r0.hasNext()
            if (r5 == 0) goto L_0x0362
            java.lang.Object r5 = r0.next()
            java.lang.String r5 = (java.lang.String) r5
            boolean r6 = r5.isEmpty()
            if (r6 != 0) goto L_0x0346
            boolean r6 = r3.contains(r5)
            if (r6 != 0) goto L_0x0346
            r3.add(r5)
            goto L_0x0346
        L_0x0362:
            java.util.Iterator r3 = r3.iterator()
        L_0x0366:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x05c5
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r5 = r21.isDisposed()
            if (r5 == 0) goto L_0x0379
            return
        L_0x0379:
            java.lang.String r5 = "\\/"
            r6 = r17
            java.lang.String r0 = r0.replace(r5, r6)
            java.lang.String r5 = "./"
            boolean r5 = r0.startsWith(r5)
            if (r5 == 0) goto L_0x03a1
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = r1.c
            r5.append(r7)
            r7 = 1
            java.lang.String r0 = r0.substring(r7)
            r5.append(r0)
            java.lang.String r0 = r5.toString()
        L_0x039f:
            r5 = r0
            goto L_0x03f0
        L_0x03a1:
            java.lang.String r5 = "//"
            boolean r5 = r0.startsWith(r5)
            if (r5 == 0) goto L_0x03bb
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = "http:"
            r5.append(r7)
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            goto L_0x039f
        L_0x03bb:
            boolean r5 = r0.startsWith(r6)
            if (r5 == 0) goto L_0x03d3
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = r1.c
            r5.append(r7)
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            goto L_0x039f
        L_0x03d3:
            java.lang.String r5 = "http"
            boolean r5 = r0.contains(r5)
            if (r5 != 0) goto L_0x039f
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r7 = r1.c
            r5.append(r7)
            r5.append(r6)
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            goto L_0x039f
        L_0x03f0:
            java.lang.String r0 = "iomovies.info?v="
            boolean r0 = r5.contains(r0)
            if (r0 == 0) goto L_0x0401
            java.lang.String r0 = "iomovies.info?v="
            java.lang.String r7 = "iomovies.info/?v="
            java.lang.String r0 = r5.replace(r0, r7)
            goto L_0x0402
        L_0x0401:
            r0 = r5
        L_0x0402:
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.k(r5)     // Catch:{ all -> 0x042e }
            if (r7 != 0) goto L_0x041c
            java.lang.String r7 = "them4ufree"
            boolean r7 = r5.contains(r7)     // Catch:{ all -> 0x042e }
            if (r7 == 0) goto L_0x041c
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x042e }
            r7 = 0
            java.lang.String r0 = r0.a((java.lang.String) r5, (boolean) r7, (java.lang.String) r15)     // Catch:{ all -> 0x041a }
            goto L_0x041c
        L_0x041a:
            r0 = move-exception
            goto L_0x0430
        L_0x041c:
            java.lang.String r7 = "openx.tv"
            boolean r7 = r0.contains(r7)     // Catch:{ all -> 0x042e }
            if (r7 == 0) goto L_0x0436
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x042e }
            r8 = 0
            java.lang.String r0 = r7.a((java.lang.String) r0, (boolean) r8, (java.lang.String) r0)     // Catch:{ all -> 0x042e }
            goto L_0x0436
        L_0x042e:
            r0 = move-exception
            r7 = 0
        L_0x0430:
            boolean[] r8 = new boolean[r7]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r8)
            r0 = r5
        L_0x0436:
            java.lang.String r7 = "http"
            boolean r7 = r0.startsWith(r7)
            if (r7 != 0) goto L_0x043f
            r0 = r5
        L_0x043f:
            java.lang.String r7 = "padstm"
            boolean r7 = r0.contains(r7)
            if (r7 != 0) goto L_0x05b3
            java.lang.String r7 = "openload."
            boolean r7 = r0.contains(r7)
            java.lang.String r8 = "HD"
            if (r7 == 0) goto L_0x045e
            r7 = 1
            boolean[] r0 = new boolean[r7]
            r7 = 0
            r0[r7] = r20
            r7 = r21
            r1.a(r7, r5, r8, r0)
            goto L_0x05b5
        L_0x045e:
            r7 = r21
            boolean r5 = com.original.tase.helper.GoogleVideoHelper.j(r0)
            if (r5 == 0) goto L_0x04dd
            java.util.HashMap r5 = com.original.tase.helper.GoogleVideoHelper.g(r0)
            if (r5 == 0) goto L_0x05b5
            boolean r9 = r5.isEmpty()
            if (r9 != 0) goto L_0x05b5
            java.util.Set r5 = r5.entrySet()
            java.util.Iterator r5 = r5.iterator()
        L_0x047a:
            boolean r9 = r5.hasNext()
            if (r9 == 0) goto L_0x05b5
            java.lang.Object r9 = r5.next()
            java.util.Map$Entry r9 = (java.util.Map.Entry) r9
            java.lang.Object r11 = r9.getKey()
            java.lang.String r11 = (java.lang.String) r11
            com.original.tase.model.media.MediaSource r14 = new com.original.tase.model.media.MediaSource
            java.lang.String r1 = r19.a()
            r17 = r2
            java.lang.String r2 = "GoogleVideo"
            r18 = r3
            r3 = 0
            r14.<init>(r1, r2, r3)
            r14.setOriginalLink(r0)
            r14.setStreamLink(r11)
            java.lang.Object r1 = r9.getValue()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x04b0
            r1 = r8
            goto L_0x04b6
        L_0x04b0:
            java.lang.Object r1 = r9.getValue()
            java.lang.String r1 = (java.lang.String) r1
        L_0x04b6:
            r14.setQuality((java.lang.String) r1)
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.lang.String r2 = com.original.Constants.f5838a
            r1.put(r4, r2)
            java.lang.Object r2 = r9.getKey()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r2 = com.original.tase.helper.GoogleVideoHelper.c(r0, r2)
            r1.put(r12, r2)
            r14.setPlayHeader(r1)
            r7.onNext(r14)
            r1 = r19
            r2 = r17
            r3 = r18
            goto L_0x047a
        L_0x04dd:
            r17 = r2
            r18 = r3
            boolean r1 = com.original.tase.helper.GoogleVideoHelper.k(r0)
            java.lang.String r2 = "openload"
            boolean r2 = r0.contains(r2)
            if (r2 != 0) goto L_0x0514
            java.lang.String r2 = "oload"
            boolean r2 = r0.contains(r2)
            if (r2 != 0) goto L_0x0514
            java.lang.String r2 = "streamango"
            boolean r2 = r0.contains(r2)
            if (r2 != 0) goto L_0x0514
            java.lang.String r2 = "thevid"
            boolean r2 = r0.contains(r2)
            if (r2 != 0) goto L_0x0514
            java.lang.String r2 = "//hydrax."
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x050e
            goto L_0x0514
        L_0x050e:
            r2 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            goto L_0x0516
        L_0x0514:
            r2 = 1
            r3 = 0
        L_0x0516:
            com.original.tase.model.media.MediaSource r5 = new com.original.tase.model.media.MediaSource
            if (r20 == 0) goto L_0x0530
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r11 = r19.a()
            r9.append(r11)
            java.lang.String r11 = " (CAM)"
            r9.append(r11)
            java.lang.String r9 = r9.toString()
            goto L_0x0534
        L_0x0530:
            java.lang.String r9 = r19.a()
        L_0x0534:
            if (r1 == 0) goto L_0x0539
            java.lang.String r11 = "GoogleVideo"
            goto L_0x053b
        L_0x0539:
            java.lang.String r11 = "FastServer"
        L_0x053b:
            if (r3 != 0) goto L_0x053f
            r14 = 1
            goto L_0x0540
        L_0x053f:
            r14 = 0
        L_0x0540:
            r5.<init>(r9, r11, r14)
            r5.setStreamLink(r0)
            if (r1 == 0) goto L_0x054c
            java.lang.String r8 = com.original.tase.helper.GoogleVideoHelper.h(r0)
        L_0x054c:
            r5.setQuality((java.lang.String) r8)
            if (r1 == 0) goto L_0x0585
            if (r20 == 0) goto L_0x0569
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = r19.a()
            r8.append(r9)
            java.lang.String r9 = " (CAM)"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            goto L_0x056d
        L_0x0569:
            java.lang.String r8 = r19.a()
        L_0x056d:
            java.util.List r8 = com.original.tase.helper.GoogleVideoHelper.b(r0, r8)
            java.util.Iterator r8 = r8.iterator()
        L_0x0575:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L_0x0585
            java.lang.Object r9 = r8.next()
            com.original.tase.model.media.MediaSource r9 = (com.original.tase.model.media.MediaSource) r9
            r7.onNext(r9)
            goto L_0x0575
        L_0x0585:
            r7.onNext(r5)
            if (r1 != 0) goto L_0x05ba
            if (r3 == 0) goto L_0x05ba
            com.original.tase.model.media.MediaSource r1 = r5.cloneDeeply()
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r3.putAll(r13)
            okhttp3.HttpUrl r0 = okhttp3.HttpUrl.parse(r0)     // Catch:{ all -> 0x05a5 }
            java.lang.String r0 = r0.host()     // Catch:{ all -> 0x05a5 }
            r3.put(r10, r0)     // Catch:{ all -> 0x05a5 }
            r5 = 0
            goto L_0x05ac
        L_0x05a5:
            r0 = move-exception
            r5 = 0
            boolean[] r8 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r8)
        L_0x05ac:
            r1.setPlayHeader(r3)
            r7.onNext(r1)
            goto L_0x05bb
        L_0x05b3:
            r7 = r21
        L_0x05b5:
            r17 = r2
            r18 = r3
            r2 = 1
        L_0x05ba:
            r5 = 0
        L_0x05bb:
            r1 = r19
            r2 = r17
            r3 = r18
            r17 = r6
            goto L_0x0366
        L_0x05c5:
            r7 = r21
            r6 = r17
            r17 = r2
            r1 = r19
            r17 = r6
            goto L_0x02f5
        L_0x05d1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.M4UFree.a(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }
}
