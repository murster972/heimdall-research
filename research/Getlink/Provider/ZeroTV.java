package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class ZeroTV extends BaseProvider {
    public static OkHttpClient e;
    private String c;
    private int d;

    public ZeroTV() {
        this.c = "";
        this.d = 0;
        this.c = Utils.u() + "link/get";
        if (e == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.cache(new Cache(new File(Utils.i().getCacheDir(), "httprd"), 10485760)).connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS);
            e = builder.build();
        }
    }

    public String a() {
        return "ZeroTV";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(Long.valueOf(movieInfo.tmdbID), -1, -1, Utils.b(), observableEmitter);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(Long.valueOf(movieInfo.tmdbID), movieInfo.getSession(), movieInfo.getEps(), Utils.b(), observableEmitter);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0149 A[Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x014a A[Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Long r18, java.lang.Integer r19, java.lang.Integer r20, java.lang.String r21, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r22) {
        /*
            r17 = this;
            r1 = r17
            r0 = r22
            java.lang.String r2 = "h"
            java.lang.String r3 = "\" \""
            com.movie.data.api.GlobalVariable r4 = com.movie.data.api.GlobalVariable.c()
            com.movie.data.model.AppConfig r4 = r4.a()
            com.movie.data.model.AppConfig$AdsBean r4 = r4.getAds()
            if (r4 == 0) goto L_0x0029
            com.movie.data.api.GlobalVariable r4 = com.movie.data.api.GlobalVariable.c()
            com.movie.data.model.AppConfig r4 = r4.a()
            com.movie.data.model.AppConfig$SyncBean r4 = r4.getSync()
            boolean r4 = r4.isFor_member_only()
            if (r4 == 0) goto L_0x0029
            return
        L_0x0029:
            com.original.tase.model.debrid.realdebrid.RealDebridCredentialsInfo r4 = com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.c()
            boolean r4 = r4.isValid()
            com.original.tase.model.debrid.premiumize.PremiumizeCredentialsInfo r5 = com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper.b()
            boolean r5 = r5.isValid()
            if (r5 == 0) goto L_0x003d
            r5 = 2
            goto L_0x003e
        L_0x003d:
            r5 = 0
        L_0x003e:
            com.original.tase.model.debrid.alldebrid.AllDebridCredentialsInfo r7 = com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper.b()
            boolean r7 = r7.isValid()
            if (r7 == 0) goto L_0x004a
            r7 = 4
            goto L_0x004b
        L_0x004a:
            r7 = 0
        L_0x004b:
            r4 = r4 | r5
            r4 = r4 | r7
            r1.d = r4
            okhttp3.Request$Builder r4 = new okhttp3.Request$Builder
            r4.<init>()
            java.lang.String r5 = java.lang.String.valueOf(r18)
            java.lang.String r7 = "i"
            okhttp3.Request$Builder r4 = r4.header(r7, r5)
            java.lang.String r5 = java.lang.String.valueOf(r19)
            java.lang.String r7 = "s"
            okhttp3.Request$Builder r4 = r4.header(r7, r5)
            java.lang.String r5 = java.lang.String.valueOf(r20)
            java.lang.String r7 = "e"
            okhttp3.Request$Builder r4 = r4.header(r7, r5)
            java.lang.String r5 = com.utils.Utils.b()
            java.lang.String r7 = "a"
            okhttp3.Request$Builder r4 = r4.header(r7, r5)
            java.lang.String r5 = com.utils.Utils.f()
            java.lang.String r7 = "device_id"
            okhttp3.Request$Builder r4 = r4.header(r7, r5)
            int r5 = r1.d
            java.lang.String r5 = java.lang.String.valueOf(r5)
            java.lang.String r7 = "p"
            okhttp3.Request$Builder r4 = r4.header(r7, r5)
            java.lang.String r5 = "Content-Type"
            java.lang.String r8 = "application/json"
            okhttp3.Request$Builder r4 = r4.header(r5, r8)
            java.lang.String r5 = r1.c
            okhttp3.Request$Builder r4 = r4.url((java.lang.String) r5)
            okhttp3.Request r4 = r4.build()
            okhttp3.OkHttpClient r5 = e     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            okhttp3.Call r4 = r5.newCall(r4)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            okhttp3.Response r4 = r4.execute()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r4 == 0) goto L_0x01e1
            int r5 = r4.code()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r8 = 200(0xc8, float:2.8E-43)
            if (r5 != r8) goto L_0x01e1
            okhttp3.ResponseBody r4 = r4.body()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r4 = r4.string()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r4.<init>()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            android.content.SharedPreferences r8 = com.movie.FreeMoviesApp.l()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r9 = "pref_zerotv_enabled"
            r10 = 1
            boolean r8 = r8.getBoolean(r9, r10)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.util.Set r9 = com.utils.Utils.k()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r11 = 0
        L_0x00da:
            int r12 = r5.length()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r13 = "CachedTorrent"
            if (r11 >= r12) goto L_0x01b5
            java.lang.String r12 = r5.getString(r11)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            boolean r14 = r12.contains(r3)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r14 == 0) goto L_0x00f2
            java.lang.String r14 = "\""
            java.lang.String r12 = r12.replace(r3, r14)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
        L_0x00f2:
            org.json.JSONObject r14 = new org.json.JSONObject     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r14.<init>(r12)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r12 = "t"
            r14.getLong(r12)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r12 = "l"
            java.lang.String r12 = r14.getString(r12)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r15 = "d"
            r14.getLong(r15)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r15 = "z"
            r19 = r11
            long r10 = r14.getLong(r15)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r15 = "q"
            java.lang.String r15 = r14.getString(r15)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r14.getString(r7)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            boolean r16 = r14.has(r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r6 = ""
            if (r16 == 0) goto L_0x013e
            java.lang.String r14 = r14.getString(r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r1 = "unknow"
            boolean r1 = r14.contains(r1)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r1 != 0) goto L_0x013e
            java.lang.String r1 = "_"
            r16 = r2
            java.lang.String r2 = " "
            java.lang.String r1 = r14.replace(r1, r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            boolean r2 = com.utils.Utils.a((java.lang.String) r1, (java.util.Set<java.lang.String>) r9)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r2 == 0) goto L_0x0141
            goto L_0x01ac
        L_0x013e:
            r16 = r2
            r1 = r6
        L_0x0141:
            if (r8 != 0) goto L_0x014a
            boolean r2 = r1.isEmpty()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r2 == 0) goto L_0x014a
            goto L_0x01ac
        L_0x014a:
            boolean r2 = r1.isEmpty()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r2 == 0) goto L_0x0154
            java.lang.String r1 = r17.a()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
        L_0x0154:
            java.lang.String r2 = "magnet:"
            boolean r2 = r12.contains(r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r2 == 0) goto L_0x0170
            java.lang.String r2 = "(magnet:\\?xt=urn:btih:[^&.]+)"
            r6 = 1
            java.lang.String r2 = com.original.tase.utils.Regex.a((java.lang.String) r12, (java.lang.String) r2, (int) r6)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            com.movie.data.model.realdebrid.MagnetObject r6 = new com.movie.data.model.realdebrid.MagnetObject     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r6.<init>(r13, r2, r15, r1)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r4.add(r6)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            goto L_0x01ac
        L_0x0170:
            java.lang.String r2 = "\\"
            java.lang.String r2 = r12.replace(r2, r6)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r2 = com.utils.Utils.f(r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            com.original.tase.model.media.MediaSource r6 = new com.original.tase.model.media.MediaSource     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r13 = r17.a()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r14 = "google"
            boolean r14 = r12.contains(r14)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r14 == 0) goto L_0x018b
            java.lang.String r2 = "GoogleVideo"
            goto L_0x018f
        L_0x018b:
            java.lang.String r2 = r2.toUpperCase()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
        L_0x018f:
            r14 = 0
            r6.<init>(r13, r2, r14)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r6.setStreamLink(r12)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r6.setQuality((java.lang.String) r15)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r6.setFileSize(r10)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r2 = 1
            r6.setCachedLink(r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            boolean r2 = r1.isEmpty()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r2 != 0) goto L_0x01a9
            r6.setProviderName(r1)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
        L_0x01a9:
            r0.onNext(r6)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
        L_0x01ac:
            int r11 = r19 + 1
            r10 = 1
            r1 = r17
            r2 = r16
            goto L_0x00da
        L_0x01b5:
            int r1 = r4.size()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            if (r1 <= 0) goto L_0x01e1
            com.original.tase.model.media.MediaSource r1 = new com.original.tase.model.media.MediaSource     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r2 = r17.a()     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r3 = 0
            r1.<init>(r2, r13, r3)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r2 = 1
            r1.setTorrent(r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r1.setMagnetObjects(r4)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            java.lang.String r2 = "magnet:cachedLink"
            r1.setStreamLink(r2)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r1.setNeedToSync(r3)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            r0.onNext(r1)     // Catch:{ IOException -> 0x01dd, JSONException -> 0x01d8 }
            goto L_0x01e1
        L_0x01d8:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01e1
        L_0x01dd:
            r0 = move-exception
            r0.printStackTrace()
        L_0x01e1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.ZeroTV.a(java.lang.Long, java.lang.Integer, java.lang.Integer, java.lang.String, io.reactivex.ObservableEmitter):void");
    }
}
