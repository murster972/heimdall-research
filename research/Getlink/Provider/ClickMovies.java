package com.utils.Getlink.Provider;

import com.google.gson.Gson;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class ClickMovies extends BaseProvider {
    private String c = Utils.getProvider(105);
    private String d = "HD";

    static class SourceModel {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f6508a;
        /* access modifiers changed from: private */
        public String b;

        SourceModel() {
        }

        public String a() {
            return this.f6508a;
        }
    }

    public String a() {
        return "ClickMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        boolean z;
        boolean z2 = movieInfo.getType().intValue() == 1;
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        if (this.d.toLowerCase().contains("ts") || this.d.toLowerCase().contains("cam")) {
            this.d = "CAM";
        }
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap});
        if (!z2) {
            Iterator it2 = Jsoup.b(a2).g("div.ep_link.full").b("a").iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                Element element = (Element) it2.next();
                String b = element.b("href");
                if (Integer.parseInt(Regex.a(element.G(), "Episode\\s*(\\d+)", 1)) == movieInfo.getEps().intValue()) {
                    a2 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[]{hashMap});
                    str = b;
                    z = true;
                    break;
                }
                str = b;
            }
            if (!z) {
                return;
            }
        }
        Iterator it3 = Jsoup.b(a2).g("select#selectServer").b("option").iterator();
        String replace = Regex.a(a2, "episode_id\\s*:\\s*([0-Z]*)[,]", 1).replace(",", "");
        while (it3.hasNext()) {
            String lowerCase = ((Element) it3.next()).G().toLowerCase();
            String format = String.format(this.c + "/ajax/movie/load_player_v5?s=%s&id=%s&_=" + DateTimeHelper.c(), new Object[]{lowerCase, replace});
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("?episode_id=%s&s=%s");
            String format2 = String.format(sb.toString(), new Object[]{replace, lowerCase});
            hashMap.put("Referer", format2);
            hashMap.put("X-Requested-With", "XMLHttpRequest");
            String a3 = Regex.a(HttpHelper.e().a(format, (Map<String, String>[]) new Map[]{hashMap}).replace("\\/", "/"), "['\"]?value['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1);
            Logger.a("http ", a3);
            HashMap hashMap2 = new HashMap();
            hashMap2.put("Referer", format2);
            hashMap2.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
            String a4 = Regex.a(HttpHelper.e().a(a3, (Map<String, String>[]) new Map[]{hashMap2}).replace("\\/", "/"), "sources: (\\[.*\\])", 1);
            if (!a4.isEmpty()) {
                Logger.a("ClickMovies", a4);
                for (SourceModel sourceModel : (SourceModel[]) new Gson().a(a4, SourceModel[].class)) {
                    MediaSource mediaSource = new MediaSource(a(), GoogleVideoHelper.k(sourceModel.a()) ? "GoogleVideo" : "CDN-FastServer", false);
                    mediaSource.setStreamLink(sourceModel.f6508a);
                    mediaSource.setQuality(sourceModel.b);
                    observableEmitter.onNext(mediaSource);
                }
            }
        }
    }

    private String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        String str = this.c + "/movies/search?s=" + com.original.tase.utils.Utils.a(movieInfo.getName(), new boolean[0]).replace("%20", "+");
        if (!z) {
            str = str + "+season+" + movieInfo.session;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", this.c + "/");
        hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        hashMap.put("Host", this.c.replace("https://", "").replace("http://", ""));
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("div.list_movies").b("div.item_movie").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            Element h = element.h("h2.tit").h("a");
            this.d = element.h("a.thumb").h("span.res").G();
            String G = h.G();
            String b = h.b("href");
            if (z) {
                if (!G.equalsIgnoreCase(movieInfo.name)) {
                    if (G.equalsIgnoreCase(movieInfo.name + " (" + movieInfo.year + ")")) {
                    }
                }
                return b;
            }
            if (!G.equalsIgnoreCase(movieInfo.name + " - season " + movieInfo.session)) {
                if (G.equalsIgnoreCase(movieInfo.name + " - season " + movieInfo.session + " (" + movieInfo.sessionYear + ")")) {
                }
            }
            return b;
        }
        return "";
    }
}
