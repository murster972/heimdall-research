package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.search.SearchHelper;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class DaxivMovies extends BaseProvider {
    public static String e = "";
    private static String f = "";
    public String c = Utils.getProvider(49);
    private int d = 2;

    private static String c() {
        String str = e;
        if (str == null || str.isEmpty()) {
            try {
                e = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL2R2bQ==", 10), "UTF-8");
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                e = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL2R2bQ==", 10));
            }
        }
        return e;
    }

    public String a() {
        return "DaxivMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (b.isEmpty()) {
            String str = movieInfo.name;
            String str2 = movieInfo.year;
            b = SearchHelper.a(str, str2, str2, this.c, "");
        }
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (b.isEmpty()) {
            String str = movieInfo.name;
            String str2 = movieInfo.year;
            b = SearchHelper.a(str, str2, str2, this.c, "");
        }
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    public Object j(String str) {
        int i;
        String a2 = HttpHelper.e().a(c(), (Map<String, String>[]) new Map[0]);
        String replaceAll = a2.replaceAll("#####", str);
        Duktape create = Duktape.create();
        try {
            Object evaluate = create.evaluate(replaceAll);
            if (evaluate != null) {
                create.close();
                return evaluate;
            }
        } catch (Throwable unused) {
            i *= 2;
        }
        create.close();
        return null;
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        Object[] objArr;
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        if (!(movieInfo.getType().intValue() == 1)) {
            str = String.format(str.replace("-watch-", "/watch-") + "-season-%s-episode-%s", new Object[]{movieInfo.session, movieInfo.eps});
        }
        String a2 = Regex.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap}), "<span\\s*id=\"user-data\"\\s*v\\s*=\\s*[\"']([^\"']+[^\"'])[\"']", 1);
        if (!a2.isEmpty() && (objArr = (Object[]) j(a2)) != null) {
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                String str2 = (String) objArr[i];
                if (!observableEmitter.isDisposed()) {
                    HttpHelper e2 = HttpHelper.e();
                    a(observableEmitter, Regex.a(e2.b(String.format(this.c + "/links/go/%s?embed=true", new Object[]{str2}), str), "[\"']link[\"']\\s*:\\s*[\"']([^\"']+[^\"'])[\"']", 1), "HD", false);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    private String b(MovieInfo movieInfo) {
        String format = String.format(BaseProvider.e(HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]), this.c), new Object[]{com.original.tase.utils.Utils.a(movieInfo.getName(), new boolean[0]).replace("+", "%20")});
        if (f.isEmpty()) {
            f = HttpHelper.e().a("https://raw.githubusercontent.com/TeruSetephen/cinemaapk/master//provider/dvms", (Map<String, String>[]) new Map[0]);
        }
        if (!f.isEmpty()) {
            format = format + f;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("referer", this.c + "/");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(format, (Map<String, String>[]) new Map[]{hashMap})).g("div.index_item.index_item_ie").b("a").iterator();
        while (it2.hasNext()) {
            try {
                Element element = (Element) it2.next();
                String b = element.b("href");
                if (TitleHelper.a(element.b("title").toLowerCase(), "").equals(TitleHelper.a(movieInfo.name.toLowerCase() + movieInfo.year, ""))) {
                    if (!b.startsWith("/")) {
                        return b;
                    }
                    return this.c + b;
                }
            } catch (Throwable unused) {
            }
        }
        return "";
    }
}
