package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class TwoDDL extends BaseProvider {
    private HashMap<String, String> c = new HashMap<>();
    private String d = Utils.getProvider(86);

    public TwoDDL() {
        Utils.getProvider(86);
        this.c.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        this.c.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.9,en;q=0.8");
        HashMap<String, String> hashMap = this.c;
        hashMap.put("Referer", this.d + "/");
        this.c.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        this.c.put("User-Agent", Constants.f5838a);
    }

    public String a() {
        return "TwoDDL";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo, "-1", "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo, movieInfo.session, movieInfo.eps);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str, String str2) {
        String str3;
        String str4;
        int i;
        boolean z;
        DirectoryIndexHelper.ParsedLinkModel parsedLinkModel;
        String str5;
        String str6;
        String str7;
        String str8;
        MovieInfo movieInfo2 = movieInfo;
        String str9 = "title";
        boolean z2 = movieInfo.getType().intValue() == 1;
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        if (z2) {
            str3 = "";
        } else {
            str3 = "S" + com.original.tase.utils.Utils.a(Integer.parseInt(str)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(str2));
        }
        String name = movieInfo.getName();
        if (z2) {
            str4 = "";
        } else {
            str4 = str3;
        }
        String replace = (name.replace("'", "") + " " + str4).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", " ").replace("  ", " ");
        String str10 = this.d + "/?q=" + com.original.tase.utils.Utils.a(replace.trim(), new boolean[0]);
        String a2 = HttpHelper.e().a(str10, (Map<String, String>[]) new Map[]{this.c});
        HashMap hashMap = new HashMap();
        Iterator it2 = Jsoup.b(a2).g("div.wrap-main-content").b("a[href]").iterator();
        ArrayList arrayList = new ArrayList();
        while (it2.hasNext()) {
            try {
                Element element = (Element) it2.next();
                if (element != null) {
                    String b = element.b("href");
                    if (!arrayList.contains(b)) {
                        arrayList.add(b);
                        if (element.d(str9)) {
                            str8 = element.b(str9);
                        } else {
                            str8 = element.G();
                        }
                        String replaceAll = str8.replaceAll("\\<[uibp]\\>", "").replaceAll("\\</[uibp]\\>", "");
                        String lowerCase = replaceAll.toLowerCase();
                        if (!z2 || (!lowerCase.contains(" cam") && !lowerCase.contains("cam ") && !lowerCase.contains("hdts ") && !lowerCase.contains(" hdts") && !lowerCase.contains(" ts ") && !lowerCase.contains(" telesync") && !lowerCase.contains("telesync ") && !lowerCase.contains("hdtc ") && !lowerCase.contains(" hdtc") && !lowerCase.contains(" tc ") && !lowerCase.contains(" telecine") && !lowerCase.contains("telecine "))) {
                            if (b.startsWith("//")) {
                                b = "http:" + b;
                            } else if (b.startsWith("/")) {
                                b = this.d + b;
                            }
                            if (replaceAll.toLowerCase().startsWith("goto")) {
                                replaceAll = replaceAll.substring(4, replaceAll.length()).trim();
                            }
                            if (z2) {
                                String f = TitleHelper.f(replaceAll);
                                StringBuilder sb = new StringBuilder();
                                str7 = str9;
                                try {
                                    sb.append(TitleHelper.e(movieInfo.getName()));
                                    sb.append(movieInfo2.year);
                                    if (f.startsWith(TitleHelper.f(sb.toString()))) {
                                        hashMap.put(b, replaceAll);
                                    }
                                } catch (Exception e) {
                                    e = e;
                                    Logger.a((Throwable) e, new boolean[0]);
                                    str9 = str7;
                                }
                            } else {
                                str7 = str9;
                                if (TitleHelper.a(replaceAll.toLowerCase().replace(movieInfo2.year, ""), "").startsWith(TitleHelper.a(movieInfo.getName().toLowerCase() + str3.toLowerCase(), ""))) {
                                    hashMap.put(b, replaceAll);
                                }
                            }
                            str9 = str7;
                        }
                    }
                }
                str7 = str9;
            } catch (Exception e2) {
                e = e2;
                str7 = str9;
                Logger.a((Throwable) e, new boolean[0]);
                str9 = str7;
            }
            str9 = str7;
        }
        if (!hashMap.isEmpty()) {
            HashMap hashMap2 = new HashMap();
            for (Map.Entry entry : hashMap.entrySet()) {
                try {
                    try {
                        Iterator it3 = Jsoup.b(HttpHelper.e().b((String) entry.getKey(), str10, this.c)).g("div.single-link").b("a").iterator();
                        while (it3.hasNext()) {
                            String b2 = ((Element) it3.next()).b("href");
                            if (!b2.isEmpty()) {
                                hashMap2.put(b2, entry.getValue());
                            }
                        }
                    } catch (Exception e3) {
                        e = e3;
                        Logger.a((Throwable) e, new boolean[0]);
                    }
                } catch (Exception e4) {
                    e = e4;
                    Logger.a((Throwable) e, new boolean[0]);
                }
            }
            if (!hashMap2.isEmpty()) {
                for (Map.Entry entry2 : hashMap2.entrySet()) {
                    try {
                        String str11 = (String) entry2.getKey();
                        String str12 = (String) entry2.getValue();
                        if (!str11.contains(".7z") && !str11.contains(".rar") && !str11.contains(".zip") && !str11.contains(".iso") && !str11.contains(".avi") && !str11.contains(".flv") && !str11.contains("imdb.")) {
                            String[] split = str12.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                            int length = split.length;
                            String str13 = "HQ";
                            int i2 = 0;
                            while (true) {
                                if (i2 >= length) {
                                    z = false;
                                    break;
                                }
                                String lowerCase2 = split[i2].toLowerCase();
                                if (lowerCase2.endsWith("subs") || lowerCase2.endsWith("sub") || lowerCase2.endsWith("dubbed") || lowerCase2.endsWith("dub") || lowerCase2.contains("dvdscr") || lowerCase2.contains("r5") || lowerCase2.contains("r6") || lowerCase2.contains("camrip") || lowerCase2.contains("tsrip") || lowerCase2.contains("hdcam") || lowerCase2.contains("hdts") || lowerCase2.contains("dvdcam") || lowerCase2.contains("dvdts") || lowerCase2.contains("cam") || lowerCase2.contains("telesync")) {
                                    break;
                                } else if (lowerCase2.contains("ts")) {
                                    break;
                                } else {
                                    if (!lowerCase2.contains("1080p")) {
                                        if (!lowerCase2.equals("1080")) {
                                            if (lowerCase2.contains("720p") || lowerCase2.equals("720") || lowerCase2.contains("brrip") || lowerCase2.contains("bdrip") || lowerCase2.contains("hdrip") || lowerCase2.contains("web-dl")) {
                                                str13 = "HD";
                                                i2++;
                                            } else {
                                                i2++;
                                            }
                                        }
                                    }
                                    str13 = "1080p";
                                    i2++;
                                }
                            }
                            z = true;
                            if (!z) {
                                String a3 = a();
                                if (z2) {
                                    parsedLinkModel = directoryIndexHelper.a(str12);
                                } else {
                                    parsedLinkModel = directoryIndexHelper.b(str12);
                                }
                                if (parsedLinkModel != null) {
                                    String c2 = parsedLinkModel.c();
                                    if (c2.equalsIgnoreCase("HQ")) {
                                        c2 = str13;
                                    }
                                    str5 = f(parsedLinkModel.b());
                                    str6 = c2;
                                } else {
                                    str5 = a3;
                                    str6 = str13;
                                }
                                i = 0;
                                try {
                                    a(observableEmitter, str11, str6, str5, new boolean[0]);
                                } catch (Exception e5) {
                                    e = e5;
                                    Logger.a((Throwable) e, new boolean[i]);
                                }
                            }
                        }
                    } catch (Exception e6) {
                        e = e6;
                        i = 0;
                    }
                }
            }
        }
    }
}
