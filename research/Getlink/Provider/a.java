package com.utils.Getlink.Provider;

import com.movie.data.model.providers.Provider;
import com.movie.data.model.providers.TProviderData;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class a implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ a f6512a = new a();

    private /* synthetic */ a() {
    }

    public final Object apply(Object obj) {
        return Observable.fromArray(((TProviderData) obj).getProviders().values().toArray(new Provider[0]));
    }
}
