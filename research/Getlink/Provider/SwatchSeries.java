package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SwatchSeries extends BaseProvider {
    public static String e = "";
    private String c = (Utils.getProvider(94) + "/");
    String d = "";

    private static String c() {
        String str = e;
        if (str == null || str.isEmpty()) {
            try {
                e = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL3N3YXRjaHNlcmllcy50eHQ=", 10), "UTF-8");
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                e = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL3N3YXRjaHNlcmllcy50eHQ=", 10));
            }
        }
        return e;
    }

    public String a() {
        return "SwatchSeries";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        Elements g = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0])).g("div[itemprop=season]");
        Iterator it2 = g.b("ul[id=listing_" + movieInfo.session + "]").b("li[itemprop=episode]").iterator();
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).h("a").b("href");
            String lowerCase = b.toLowerCase();
            if (lowerCase.contains("_s" + movieInfo.session + "_e" + movieInfo.eps + ".html")) {
                Iterator it3 = Jsoup.b(HttpHelper.e().a(b, (Map<String, String>[]) new Map[0])).g("table[id=myTable]").b("tr").b("a[class=watchlink]").iterator();
                int i = 0;
                while (it3.hasNext()) {
                    String b2 = ((Element) it3.next()).b("href");
                    if (!b2.isEmpty()) {
                        String a2 = Regex.a(b2, "(?:\\?)((?:r).*)", 1);
                        String str2 = "";
                        Duktape create = Duktape.create();
                        try {
                            Object evaluate = create.evaluate(this.d.replace("#####", a2));
                            if (evaluate != null) {
                                str2 = evaluate.toString();
                            }
                        } catch (Throwable unused) {
                        }
                        create.close();
                        if (str2 != null && !str2.isEmpty()) {
                            a(observableEmitter, str2, "HD", false);
                        }
                        i++;
                        if (i > 20) {
                            return;
                        }
                    }
                }
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            this.d = HttpHelper.e().a(c(), (Map<String, String>[]) new Map[0]);
            if (!this.d.isEmpty()) {
                a(observableEmitter, b, movieInfo);
            }
        }
    }

    public String b(MovieInfo movieInfo) {
        HttpHelper e2 = HttpHelper.e();
        String str = this.c;
        e2.c(str, str);
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        String str2 = this.c + "search/" + com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("+", "%20");
        hashMap.put("referer", str2);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[]{hashMap})).g("div[class=search-item-left]").b("div[valign=top]").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("href");
            String c2 = element.g("strong").c();
            if (TitleHelper.f(c2).equals(TitleHelper.f(movieInfo.name + " (" + movieInfo.year + ")")) && c2.contains(movieInfo.year)) {
                return b;
            }
        }
        return "";
    }
}
