package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class YesGG extends BaseProvider {
    private String c = Utils.getProvider(89);
    private String d = (this.c + "/");

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        YesGG yesGG = this;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String str2 = str;
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        int i = 1;
        char c2 = 0;
        HttpHelper.e().a(str2, (Map<String, String>[]) new Map[]{hashMap});
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        String str3 = "/watching.html";
        sb.append(str3);
        String sb2 = sb.toString();
        Iterator it2 = Jsoup.b(HttpHelper.e().a(sb2, (Map<String, String>[]) new Map[]{hashMap})).g("div[class=pas-list]").b("li").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            if (h.b("title").toLowerCase().equals("episode " + movieInfo.eps)) {
                hashMap.put("referer", sb2 + str3);
                if (b.startsWith("/")) {
                    b = yesGG.c + b;
                }
                HttpHelper e = HttpHelper.e();
                Map[] mapArr = new Map[i];
                mapArr[c2] = hashMap;
                Document b2 = Jsoup.b(e.a(b, (Map<String, String>[]) mapArr));
                Iterator it3 = b2.g("div[class=pa-main anime_muti_link]").b("li").iterator();
                String G = b2.h("span[class=quality]").G();
                boolean z = G != null && !G.isEmpty() && (G.toLowerCase().contains("cam") || G.toLowerCase().contains("ts"));
                while (it3.hasNext()) {
                    String b3 = ((Element) it3.next()).b("data-video");
                    if (b3.startsWith("//")) {
                        b3 = "https:" + b3;
                    }
                    String str4 = str3;
                    if (!b3.isEmpty() && !b3.contains("streaming.php")) {
                        yesGG.a(observableEmitter2, b3, G, z);
                    } else if (b3.contains("streaming.php") && !b3.contains("sub.")) {
                        if (b3.startsWith("//")) {
                            b3 = "https:" + b3;
                        }
                        Iterator<String> it4 = yesGG.g(HttpHelper.e().a(b3, (Map<String, String>[]) new Map[]{hashMap})).iterator();
                        while (it4.hasNext()) {
                            String next = it4.next();
                            boolean k = GoogleVideoHelper.k(next);
                            HashMap hashMap2 = new HashMap();
                            hashMap2.put("User-Agent", Constants.f5838a);
                            Iterator<String> it5 = it4;
                            MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", z);
                            mediaSource.setStreamLink(next);
                            if (k) {
                                mediaSource.setPlayHeader(hashMap2);
                            }
                            mediaSource.setQuality(k ? GoogleVideoHelper.h(next) : G);
                            observableEmitter2.onNext(mediaSource);
                            it4 = it5;
                        }
                    }
                    yesGG = this;
                    str3 = str4;
                }
            }
            i = 1;
            c2 = 0;
            yesGG = this;
            str3 = str3;
        }
    }

    public String a() {
        return "YesGG";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    private String b(MovieInfo movieInfo) {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        HttpHelper.e().a(this.d, (Map<String, String>[]) new Map[]{hashMap});
        String a2 = TitleHelper.a(movieInfo.name.toLowerCase(), "-");
        String a3 = com.original.tase.utils.Utils.a(this.d, new boolean[0]);
        String str = this.d + "movie/search/" + a2;
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("div[class=ml-item]").iterator();
        if (!it2.hasNext()) {
            hashMap.put("Referer", str);
            it2 = Jsoup.b(HttpHelper.e().a("https://api.ocloud.stream/movie/search/" + a2 + "?link_web=" + a3, (Map<String, String>[]) new Map[]{hashMap})).g("div[class=ml-item]").iterator();
        }
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            if (h.b("title").toLowerCase().equals(movieInfo.name.toLowerCase() + " - season " + movieInfo.session)) {
                return b;
            }
        }
        return "";
    }
}
