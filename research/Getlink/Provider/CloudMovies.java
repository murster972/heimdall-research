package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class CloudMovies extends BaseProvider {
    private String c = Utils.getProvider(40);
    String d = "HD";

    public String a() {
        return "CloudMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("user-agent", Constants.f5838a);
        String b = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).h("iframe").b("src");
        if (!b.isEmpty()) {
            if (b.startsWith("//")) {
                b = "https:" + b;
            }
            Iterator it2 = d(b, str).iterator();
            while (it2.hasNext()) {
                String obj = it2.next().toString();
                boolean k = GoogleVideoHelper.k(obj);
                MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                mediaSource.setStreamLink(obj);
                if (k) {
                    mediaSource.setPlayHeader(hashMap);
                }
                mediaSource.setQuality(k ? GoogleVideoHelper.h(obj) : this.d);
                observableEmitter.onNext(mediaSource);
            }
        }
    }

    private String b(MovieInfo movieInfo) {
        if (movieInfo.getType().intValue() == 1) {
            String a2 = TitleHelper.a(movieInfo.name.toLowerCase(), "-");
            return this.c + "/videos/" + a2;
        }
        String a3 = TitleHelper.a(movieInfo.name + " Season " + movieInfo.session + " Episode " + movieInfo.eps, "-");
        StringBuilder sb = new StringBuilder();
        sb.append(this.c);
        sb.append("/search.html?keyword=");
        sb.append(a3);
        String sb2 = sb.toString();
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        try {
            Iterator it2 = Jsoup.b(HttpHelper.e().a(sb2, (Map<String, String>[]) new Map[]{hashMap})).g("div.wpb_wrapper").b("li.video-block").b("a").iterator();
            while (it2.hasNext()) {
                Element element = (Element) it2.next();
                String b = element.b("href");
                if (TitleHelper.f(element.h("div.name").G()).startsWith(TitleHelper.f(a3))) {
                    if (b.contains("-" + movieInfo.eps + "-")) {
                        if (!b.startsWith("/")) {
                            return b;
                        }
                        return this.c + b;
                    }
                }
            }
            return "";
        } catch (Throwable th) {
            Logger.a(th, false);
            return "";
        }
    }
}
