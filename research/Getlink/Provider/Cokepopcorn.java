package com.utils.Getlink.Provider;

import com.movie.FreeMoviesApp;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.vungle.warren.model.CookieDBAdapter;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Cokepopcorn extends BaseProvider {
    public String[] c = Utils.getProvider(75).split(",");
    public String d = "";

    public String a() {
        return "Cokepopcorn";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        for (String str : this.c) {
            this.d = str;
            a(movieInfo, observableEmitter, b(movieInfo));
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        for (String str : this.c) {
            this.d = str;
            a(movieInfo, observableEmitter, b(movieInfo));
        }
    }

    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        String str2;
        Iterator it2;
        boolean z;
        String str3;
        DirectoryIndexHelper directoryIndexHelper;
        String str4;
        MovieInfo movieInfo2;
        String str5;
        String str6;
        String str7;
        DirectoryIndexHelper directoryIndexHelper2;
        MovieInfo movieInfo3 = movieInfo;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String str8 = str;
        if (!str.isEmpty()) {
            boolean z2 = movieInfo.getType().intValue() == 1;
            HashMap hashMap = new HashMap();
            hashMap.put("Referer", this.d);
            String a2 = HttpHelper.e().a(str8, (Map<String, String>[]) new Map[]{hashMap});
            if (a2.isEmpty()) {
                FreeMoviesApp.l().edit().putString("pref_keycode_" + a(), "").apply();
            }
            Document b = Jsoup.b(a2);
            String a3 = Regex.a(a2, "<span\\s+[^>]*class='qual'[^>]*>\\s*(.*)\\s*<\\/span>", 1);
            String str9 = "p";
            String str10 = "HQ";
            if (a3.isEmpty()) {
                str2 = "HD";
            } else {
                String a4 = Regex.a(a3, "(\\d+)", 1);
                if (a4.isEmpty()) {
                    str2 = str10;
                } else {
                    str2 = a4 + str9;
                }
            }
            Iterator it3 = b.g("div#badge").b("a").iterator();
            DirectoryIndexHelper directoryIndexHelper3 = new DirectoryIndexHelper();
            while (it3.hasNext()) {
                Element element = (Element) it3.next();
                String b2 = element.b("href");
                String b3 = element.b("class");
                if (b2.startsWith("//")) {
                    StringBuilder sb = new StringBuilder();
                    it2 = it3;
                    sb.append("https:");
                    sb.append(b2);
                    b2 = sb.toString();
                } else {
                    it2 = it3;
                }
                if (b2.startsWith("/")) {
                    b2 = this.d + b2;
                }
                if (b3.contains("dl")) {
                    Iterator it4 = Regex.b(HttpHelper.e().b(b2, str8), "href=['\"]([^'\"]+(mka|mkv|mp4|avi))['\"]", 1, true).get(0).iterator();
                    String str11 = str10;
                    while (it4.hasNext()) {
                        String str12 = (String) it4.next();
                        Iterator it5 = it4;
                        if (str12.toLowerCase().contains("trailer") || str12.toLowerCase().contains("comment")) {
                            directoryIndexHelper2 = directoryIndexHelper3;
                            str7 = str10;
                        } else {
                            String a5 = a();
                            DirectoryIndexHelper.ParsedLinkModel a6 = directoryIndexHelper3.a(str12);
                            if (a6 != null) {
                                if (!a6.c().equalsIgnoreCase(str10)) {
                                    str11 = a6.c();
                                }
                                a5 = f(a6.b());
                            }
                            HashMap hashMap2 = new HashMap();
                            directoryIndexHelper2 = directoryIndexHelper3;
                            hashMap2.put("User-Agent", Constants.f5838a);
                            str7 = str10;
                            MediaSource mediaSource = new MediaSource(a5, "CDN-FastServer", d(str12));
                            mediaSource.setStreamLink(str12);
                            mediaSource.setPlayHeader(hashMap2);
                            mediaSource.setQuality(str11);
                            observableEmitter2.onNext(mediaSource);
                        }
                        String str13 = str;
                        it4 = it5;
                        directoryIndexHelper3 = directoryIndexHelper2;
                        str10 = str7;
                    }
                    directoryIndexHelper = directoryIndexHelper3;
                    str3 = str10;
                    z = z2;
                    str4 = str9;
                    movieInfo2 = movieInfo3;
                } else {
                    directoryIndexHelper = directoryIndexHelper3;
                    str3 = str10;
                    String str14 = b2 + "&referrer=link&server=alternate";
                    if (!z2) {
                        str14 = str14 + "&episode=" + movieInfo3.eps;
                        str5 = movieInfo3.eps;
                    } else {
                        str5 = "";
                    }
                    String b4 = HttpHelper.e().b(str14, b2);
                    String str15 = "CDN-FastServer";
                    String str16 = str9;
                    String a7 = Regex.a(b4, "var id\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                    Object obj = "User-Agent";
                    String a8 = Regex.a(b4, "var lang\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                    String a9 = Regex.a(b4, "var cat\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                    String str17 = "&episode=";
                    String a10 = Regex.a(b4, "var links\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                    StringBuilder sb2 = new StringBuilder();
                    z = z2;
                    String str18 = b2;
                    sb2.append(this.d.replace("https://", "https://cdn."));
                    sb2.append("/embed/movieStreams/?id=");
                    sb2.append(a7);
                    sb2.append("&e=");
                    sb2.append(str5);
                    sb2.append("&lang=");
                    sb2.append(a8);
                    sb2.append("&cat=");
                    sb2.append(a9);
                    sb2.append("&links=");
                    sb2.append(a10);
                    String sb3 = sb2.toString();
                    HashMap<String, String> a11 = Constants.a();
                    a11.put("Referer", str14);
                    a11.put("Origin", this.d.replace("https://", "https://cdn."));
                    Iterator it6 = Jsoup.b(HttpHelper.e().a(sb3, "", (Map<String, String>[]) new Map[]{a11})).g("a[href]").iterator();
                    while (it6.hasNext()) {
                        String b5 = ((Element) it6.next()).b("href");
                        if (!b5.isEmpty()) {
                            a(observableEmitter2, b5, str2, c(str2));
                        }
                    }
                    StringBuilder sb4 = new StringBuilder();
                    String str19 = str18;
                    sb4.append(str19);
                    sb4.append("&referrer=link&server=1");
                    String sb5 = sb4.toString();
                    if (!z) {
                        String replace = sb5.replace("&episode=0", "");
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append(replace);
                        sb6.append(str17);
                        movieInfo2 = movieInfo;
                        sb6.append(movieInfo2.eps);
                        sb5 = sb6.toString();
                    } else {
                        movieInfo2 = movieInfo;
                    }
                    String b6 = HttpHelper.e().b(sb5, str19);
                    HashMap hashMap3 = new HashMap();
                    hashMap3.put("Referer", sb5);
                    hashMap3.put(obj, Constants.f5838a);
                    if (z) {
                        Iterator it7 = Jsoup.b(b6).g("source[src]").iterator();
                        while (it7.hasNext()) {
                            Element element2 = (Element) it7.next();
                            String b7 = element2.b("src");
                            StringBuilder sb7 = new StringBuilder();
                            sb7.append(element2.b("data-res"));
                            String str20 = str16;
                            sb7.append(str20);
                            String sb8 = sb7.toString();
                            if (!b7.isEmpty()) {
                                str6 = str15;
                                MediaSource mediaSource2 = new MediaSource(a(), str6, c(str2));
                                mediaSource2.setStreamLink(b7);
                                mediaSource2.setPlayHeader(hashMap3);
                                mediaSource2.setQuality(sb8);
                                observableEmitter2.onNext(mediaSource2);
                            } else {
                                str6 = str15;
                            }
                            str16 = str20;
                            str15 = str6;
                        }
                        str4 = str16;
                    } else {
                        String str21 = str15;
                        str4 = str16;
                        Iterator it8 = Regex.b(b6, "src\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0).iterator();
                        Iterator it9 = Regex.b(b6, "label\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0).iterator();
                        while (it8.hasNext()) {
                            String str22 = (String) it8.next();
                            String str23 = (String) it9.next();
                            if (str22.endsWith("&e=" + movieInfo2.eps)) {
                                MediaSource mediaSource3 = new MediaSource(a(), str21, c(str23));
                                mediaSource3.setStreamLink(str22);
                                mediaSource3.setPlayHeader(hashMap3);
                                mediaSource3.setQuality(str23.toLowerCase());
                                observableEmitter2.onNext(mediaSource3);
                            }
                            str2 = str23;
                        }
                    }
                }
                str8 = str;
                movieInfo3 = movieInfo2;
                str9 = str4;
                it3 = it2;
                directoryIndexHelper3 = directoryIndexHelper;
                str10 = str3;
                z2 = z;
            }
        }
    }

    public String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        String a2 = a(TitleHelper.a(movieInfo.name.toLowerCase(), "#").split("#"));
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("referer", this.d + "/");
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, "approve_search=yes; approve=1");
        String str = this.d + "/?c=movie&m=filter&keyword=" + a2;
        HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap});
        hashMap.put("accept", "*/*");
        hashMap.put("origin", this.d);
        hashMap.put("x-requested-with", "XMLHttpRequest");
        hashMap.put("referer", str);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.d + "/?c=movie&m=filter2", "page=1&position=0&order_by=&keyword=" + a2 + "&date=&quality=&res=&genre=&directors=&cast=&subtitles=&country=&year=&yrf=&yrt=", (Map<String, String>[]) new Map[]{hashMap})).g("div[id*=post-][class*=item]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.h("a").b("title");
            String G = element.h("div.f_year").G();
            element.h("div[title=Quality]").G();
            if (!z) {
                if (TitleHelper.f(b).equals(TitleHelper.f(TitleHelper.e(movieInfo.getName() + " Season " + movieInfo.session)))) {
                    return element.h("a").b("href");
                }
            } else if (TitleHelper.f(b).equals(TitleHelper.f(TitleHelper.e(movieInfo.getName()))) && G.equals(movieInfo.year)) {
                return element.h("a").b("href");
            }
        }
        return "";
    }

    public String a(String[] strArr) {
        String str = null;
        int i = 0;
        for (String str2 : strArr) {
            if (str2.length() > i) {
                i = str2.length();
                str = str2;
            }
        }
        return str;
    }
}
