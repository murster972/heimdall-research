package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.vungle.warren.model.CookieDBAdapter;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class OneMovie extends BaseProvider {
    private String c = Utils.getProvider(80);

    public String a() {
        return "OneMovie";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("referer", str);
        hashMap.put("user-agent", Constants.f5838a);
        String a2 = Regex.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap}), "\\(\\{\\s*url\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
        HashMap<String, String> a3 = Constants.a();
        a3.put("referer", str);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(a2, (Map<String, String>[]) new Map[]{a3})).g("ul[class=episodios]").iterator();
        String a4 = HttpHelper.e().a(this.c);
        if (!a4.isEmpty()) {
            hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, a4);
        }
        while (it2.hasNext()) {
            Iterator it3 = ((Element) it2.next()).g("div[class=numerando]").iterator();
            while (true) {
                if (!it3.hasNext()) {
                    break;
                }
                Element element = (Element) it3.next();
                if (element.h("a").G().equals(movieInfo.eps)) {
                    String b = element.h("a").b("onclick");
                    String substring = b.substring(b.indexOf(UriUtil.HTTP_SCHEME), b.indexOf(",") - 1);
                    hashMap.put("referer", str);
                    String a5 = Jsoup.b(HttpHelper.e().a(substring, (Map<String, String>[]) new Map[]{hashMap})).g("iframe").a("src");
                    hashMap.put("referer", a5);
                    String a6 = Regex.a(HttpHelper.e().b(a5, substring), "<iframe.*src\\s*=\\s*['\"]([^'\"]+)['\"]", 1);
                    if (a6 != null && !a6.isEmpty() && a6.contains(UriUtil.HTTP_SCHEME)) {
                        if (a6.contains("api")) {
                            hashMap.put("referer", a5);
                            hashMap.put("user-agent", Constants.f5838a);
                            a6 = HttpHelper.a(a6, hashMap);
                        }
                        a(observableEmitter, a6, "HD", false);
                    }
                }
            }
        }
    }

    private String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        String e = BaseProvider.e(HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]), this.c);
        if (movieInfo.name.equalsIgnoreCase("Venom") && movieInfo.getYear().intValue() != 2018) {
            return "";
        }
        Iterator it2 = Jsoup.b(HttpHelper.e().a(String.format(e, new Object[]{com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+")}), (Map<String, String>[]) new Map[0])).g("div.ml-item").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("oldtitle");
            if (b.isEmpty()) {
                b = element.b("title");
            }
            if (b.isEmpty()) {
                b = element.h("h2").G();
            }
            String b2 = element.b("href");
            if (!z) {
                String lowerCase = b.toLowerCase();
                if (lowerCase.equals(movieInfo.name.toLowerCase() + ": season " + movieInfo.session)) {
                    return b2;
                }
            } else if (b.toLowerCase().equals(movieInfo.name.toLowerCase())) {
                return b2;
            }
        }
        String replace = TitleHelper.d(movieInfo.name).replace("-", "%20");
        HashMap<String, String> a2 = Constants.a();
        a2.put("referer", this.c);
        HttpHelper e2 = HttpHelper.e();
        String a3 = e2.a(this.c + "/wp-json/dooplay/search/?keyword=" + replace + "&nonce=ffec50ad43", (Map<String, String>[]) new Map[]{a2});
        Iterator it3 = Regex.b(a3, "['\"]?url['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(0).iterator();
        Iterator it4 = Regex.b(a3, "['\"]?title['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(0).iterator();
        Iterator it5 = Regex.b(a3, "['\"]?date['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(0).iterator();
        while (it3.hasNext()) {
            String replace2 = ((String) it3.next()).replace("\\/", "/");
            String str = (String) it4.next();
            String str2 = (String) it5.next();
            if (!z) {
                String lowerCase2 = str.toLowerCase();
                if (lowerCase2.equals(movieInfo.name.toLowerCase() + ": season " + movieInfo.session) && str2.equals(movieInfo.sessionYear)) {
                    return replace2;
                }
            } else if (str.toLowerCase().equals(movieInfo.name.toLowerCase()) && str2.equals(movieInfo.year)) {
                return replace2;
            }
        }
        return "";
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        HashMap hashMap = new HashMap();
        if (str.startsWith("/")) {
            str = this.c + str;
        }
        hashMap.put("referer", str);
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(Regex.a(HttpHelper.e().b(str, this.c + "/"), "\\(\\{\\s*url\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1), (Map<String, String>[]) new Map[]{hashMap})).g("ul[class=episodios]").iterator();
        String a2 = HttpHelper.e().a(this.c);
        if (!a2.isEmpty()) {
            hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, a2);
        }
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("div[class=numerando]");
            String G = h.h("a").G();
            String b = h.h("a").b("onclick");
            String a3 = Jsoup.b(HttpHelper.e().a(b.substring(b.indexOf(UriUtil.HTTP_SCHEME), b.indexOf(",") - 1), (Map<String, String>[]) new Map[]{hashMap})).g("iframe").a("src");
            String a4 = HttpHelper.e().a(a3, (Map<String, String>[]) new Map[]{hashMap});
            ArrayList arrayList = new ArrayList();
            String a5 = Regex.a(a4, "href\\s*=\\s*['\"]([^'\"]*http+[^'\"]*)['\"]>", 1);
            if (!a5.isEmpty()) {
                arrayList.add(a5);
            }
            if (arrayList.size() == 0) {
                arrayList.addAll(Regex.b(a4, "file\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0));
            }
            if (arrayList.size() == 0) {
                arrayList.addAll(Regex.b(a4, "pageUrl*\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0));
            }
            if (arrayList.size() == 0) {
                arrayList.add(Regex.a(a4, "content*\\s*=\\s*['\"]([^'\"]*openload+[^'\"]*)['\"]", 1));
            }
            arrayList.add(Regex.a(a4, "<iframe.*src\\s*=\\s*['\"]([^'\"]+)['\"]", 1));
            Iterator it3 = arrayList.iterator();
            while (it3.hasNext()) {
                String str2 = (String) it3.next();
                if (str2 != null && !str2.isEmpty() && str2.contains(UriUtil.HTTP_SCHEME)) {
                    if (str2.contains("api")) {
                        HashMap hashMap2 = new HashMap();
                        hashMap2.put("referer", a3);
                        hashMap2.put("user-agent", Constants.f5838a);
                        str2 = HttpHelper.a(str2, hashMap2);
                    }
                    a(observableEmitter, str2, G, false);
                }
            }
        }
    }
}
