package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.movie.data.model.providers.Provider;
import io.reactivex.functions.Consumer;
import java.util.Set;

/* compiled from: lambda */
public final /* synthetic */ class d implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ RemoteJS f6515a;
    private final /* synthetic */ Set b;
    private final /* synthetic */ MovieInfo c;

    public /* synthetic */ d(RemoteJS remoteJS, Set set, MovieInfo movieInfo) {
        this.f6515a = remoteJS;
        this.b = set;
        this.c = movieInfo;
    }

    public final void accept(Object obj) {
        this.f6515a.a(this.b, this.c, (Provider) obj);
    }
}
