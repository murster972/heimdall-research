package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class Zooqle extends BaseProvider {
    public String c = Utils.getProvider(3);

    public String a() {
        return "Zooqle";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02a9, code lost:
        r16 = r2;
        r1 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(com.movie.data.model.MovieInfo r18, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            java.lang.String r2 = "1080p"
            java.lang.String r3 = "."
            java.lang.Integer r4 = r18.getType()
            int r4 = r4.intValue()
            r5 = 0
            r6 = 1
            if (r4 != r6) goto L_0x0016
            r4 = 1
            goto L_0x0017
        L_0x0016:
            r4 = 0
        L_0x0017:
            if (r4 == 0) goto L_0x002d
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = " "
            r7.append(r8)
            java.lang.String r8 = r1.year
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            goto L_0x005a
        L_0x002d:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = " S"
            r7.append(r8)
            java.lang.String r8 = r1.session
            int r8 = java.lang.Integer.parseInt(r8)
            java.lang.String r8 = com.original.tase.utils.Utils.a((int) r8)
            r7.append(r8)
            java.lang.String r8 = "E"
            r7.append(r8)
            java.lang.String r8 = r1.eps
            int r8 = java.lang.Integer.parseInt(r8)
            java.lang.String r8 = com.original.tase.utils.Utils.a((int) r8)
            r7.append(r8)
            java.lang.String r7 = r7.toString()
        L_0x005a:
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            java.lang.String r9 = "accept"
            java.lang.String r10 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
            r8.put(r9, r10)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = r0.c
            r9.append(r10)
            java.lang.String r10 = "/"
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            java.lang.String r11 = "referer"
            r8.put(r11, r9)
            java.lang.String r9 = "upgrade-insecure-requests"
            java.lang.String r11 = "1"
            r8.put(r9, r11)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = r1.name
            r8.append(r9)
            r8.append(r7)
            java.lang.String r8 = r8.toString()
            boolean[] r9 = new boolean[r5]
            java.lang.String r8 = com.original.tase.utils.Utils.a((java.lang.String) r8, (boolean[]) r9)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r11 = r0.c
            r9.append(r11)
            java.lang.String r11 = "/search?q="
            r9.append(r11)
            r9.append(r8)
            java.lang.String r8 = r9.toString()
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = r0.c
            r11.append(r12)
            r11.append(r10)
            java.lang.String r10 = r11.toString()
            java.lang.String r8 = r9.b((java.lang.String) r8, (java.lang.String) r10)
            java.lang.String r9 = "522: Connection timed out"
            boolean r9 = r8.contains(r9)
            if (r9 == 0) goto L_0x00f8
            android.content.SharedPreferences r1 = com.movie.FreeMoviesApp.l()
            android.content.SharedPreferences$Editor r1 = r1.edit()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "pref_use_unblock"
            r2.append(r3)
            java.lang.String r3 = r17.a()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.content.SharedPreferences$Editor r1 = r1.putBoolean(r2, r6)
            r1.apply()
            return
        L_0x00f8:
            org.jsoup.nodes.Document r8 = org.jsoup.Jsoup.b(r8)
            java.lang.String r9 = "tbody"
            org.jsoup.select.Elements r8 = r8.g((java.lang.String) r9)
            java.lang.String r9 = "tr"
            org.jsoup.select.Elements r8 = r8.b(r9)
            java.util.Iterator r8 = r8.iterator()
            com.original.tase.helper.DirectoryIndexHelper r9 = new com.original.tase.helper.DirectoryIndexHelper
            r9.<init>()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.util.HashMap r11 = new java.util.HashMap
            r11.<init>()
        L_0x011b:
            boolean r12 = r8.hasNext()
            java.lang.String r13 = ""
            if (r12 == 0) goto L_0x01b8
            java.lang.Object r12 = r8.next()
            org.jsoup.nodes.Element r12 = (org.jsoup.nodes.Element) r12
            java.lang.String r14 = "a[class= small][href]"
            org.jsoup.nodes.Element r14 = r12.h(r14)
            java.lang.String r14 = r14.G()
            java.lang.String r15 = "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?"
            if (r4 == 0) goto L_0x0171
            java.lang.String r5 = r14.toLowerCase()
            java.lang.String r5 = com.original.tase.helper.TitleHelper.a(r5, r13)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r16 = r8
            java.lang.String r8 = r1.name
            java.lang.String r8 = r8.toLowerCase()
            r6.append(r8)
            java.lang.String r8 = r7.toLowerCase()
            r6.append(r8)
            java.lang.String r6 = r6.toString()
            java.lang.String r6 = com.original.tase.helper.TitleHelper.a(r6, r13)
            boolean r5 = r5.startsWith(r6)
            if (r5 == 0) goto L_0x01b2
            java.lang.String r5 = r12.toString()
            r6 = 1
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r15, (int) r6)
            r11.put(r5, r14)
            goto L_0x01b2
        L_0x0171:
            r16 = r8
            java.lang.String r5 = r14.toLowerCase()
            java.lang.String r6 = r1.year
            java.lang.String r5 = r5.replace(r6, r13)
            java.lang.String r5 = com.original.tase.helper.TitleHelper.a(r5, r13)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r8 = r18.getName()
            java.lang.String r8 = r8.toLowerCase()
            r6.append(r8)
            java.lang.String r8 = r7.toLowerCase()
            r6.append(r8)
            java.lang.String r6 = r6.toString()
            java.lang.String r6 = com.original.tase.helper.TitleHelper.a(r6, r13)
            boolean r5 = r5.startsWith(r6)
            if (r5 == 0) goto L_0x01b2
            java.lang.String r5 = r12.toString()
            r6 = 1
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r15, (int) r6)
            r11.put(r5, r14)
        L_0x01b2:
            r8 = r16
            r5 = 0
            r6 = 1
            goto L_0x011b
        L_0x01b8:
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.Set r1 = r11.entrySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x01c5:
            boolean r5 = r1.hasNext()
            if (r5 == 0) goto L_0x0316
            java.lang.Object r5 = r1.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            java.lang.Object r6 = r5.getValue()     // Catch:{ all -> 0x030c }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x030c }
            java.lang.String r6 = com.original.tase.helper.TitleHelper.a(r6, r3)     // Catch:{ all -> 0x030c }
            java.lang.String r7 = ".."
            java.lang.String r6 = r6.replace(r7, r3)     // Catch:{ all -> 0x030c }
            java.lang.String r7 = r6.toUpperCase()     // Catch:{ all -> 0x030c }
            java.lang.String r8 = "(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)"
            java.lang.String r7 = r7.replaceAll(r8, r13)     // Catch:{ all -> 0x030c }
            java.lang.String r8 = "\\.|\\(|\\)|\\[|\\]|\\s|\\-"
            java.lang.String[] r7 = r7.split(r8)     // Catch:{ all -> 0x030c }
            int r8 = r7.length     // Catch:{ all -> 0x030c }
            java.lang.String r11 = "HQ"
            r14 = r11
            r12 = 0
        L_0x01f6:
            if (r12 >= r8) goto L_0x02ad
            r15 = r7[r12]     // Catch:{ all -> 0x030c }
            java.lang.String r15 = r15.toLowerCase()     // Catch:{ all -> 0x030c }
            r18 = r1
            java.lang.String r1 = "dvdscr"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "camrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "tsrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "hdcam"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "hdtc"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "hdts"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "dvdcam"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "dvdts"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "cam"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "telesync"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 != 0) goto L_0x02a9
            java.lang.String r1 = "ts"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x030e }
            if (r1 == 0) goto L_0x0259
            goto L_0x02a9
        L_0x0259:
            boolean r1 = r15.contains(r2)     // Catch:{ all -> 0x030e }
            r16 = r2
            java.lang.String r2 = "720p"
            if (r1 != 0) goto L_0x029f
            java.lang.String r1 = "1080"
            boolean r1 = r15.equals(r1)     // Catch:{ all -> 0x0310 }
            if (r1 == 0) goto L_0x026c
            goto L_0x029f
        L_0x026c:
            boolean r1 = r15.contains(r2)     // Catch:{ all -> 0x0310 }
            if (r1 != 0) goto L_0x029d
            java.lang.String r1 = "720"
            boolean r1 = r15.equals(r1)     // Catch:{ all -> 0x0310 }
            if (r1 == 0) goto L_0x027b
            goto L_0x029d
        L_0x027b:
            java.lang.String r1 = "brrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0310 }
            if (r1 != 0) goto L_0x029b
            java.lang.String r1 = "bdrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0310 }
            if (r1 != 0) goto L_0x029b
            java.lang.String r1 = "hdrip"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0310 }
            if (r1 != 0) goto L_0x029b
            java.lang.String r1 = "web-dl"
            boolean r1 = r15.contains(r1)     // Catch:{ all -> 0x0310 }
            if (r1 == 0) goto L_0x02a1
        L_0x029b:
            java.lang.String r2 = "HD"
        L_0x029d:
            r14 = r2
            goto L_0x02a1
        L_0x029f:
            r14 = r16
        L_0x02a1:
            int r12 = r12 + 1
            r1 = r18
            r2 = r16
            goto L_0x01f6
        L_0x02a9:
            r16 = r2
            r1 = 1
            goto L_0x02b2
        L_0x02ad:
            r18 = r1
            r16 = r2
            r1 = 0
        L_0x02b2:
            java.lang.Object r2 = r5.getKey()     // Catch:{ all -> 0x0310 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0310 }
            java.lang.String r5 = r17.a()     // Catch:{ all -> 0x0310 }
            if (r4 == 0) goto L_0x02c3
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r7 = r9.a(r6)     // Catch:{ all -> 0x0310 }
            goto L_0x02c7
        L_0x02c3:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r7 = r9.b(r6)     // Catch:{ all -> 0x0310 }
        L_0x02c7:
            if (r7 == 0) goto L_0x02de
            java.lang.String r5 = r7.c()     // Catch:{ all -> 0x0310 }
            boolean r8 = r5.equalsIgnoreCase(r11)     // Catch:{ all -> 0x0310 }
            if (r8 != 0) goto L_0x02d4
            r14 = r5
        L_0x02d4:
            java.lang.String r5 = r7.b()     // Catch:{ all -> 0x0310 }
            r7 = 1
            java.lang.String r5 = r0.a((java.lang.String) r5, (boolean) r7)     // Catch:{ all -> 0x0310 }
            goto L_0x02df
        L_0x02de:
            r7 = 1
        L_0x02df:
            java.lang.String r8 = "(magnet:\\?xt=urn:btih:[^&.]+)"
            java.lang.String r2 = com.original.tase.utils.Regex.a((java.lang.String) r2, (java.lang.String) r8, (int) r7)     // Catch:{ all -> 0x0310 }
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ all -> 0x0310 }
            com.movie.data.model.realdebrid.MagnetObject r7 = new com.movie.data.model.realdebrid.MagnetObject     // Catch:{ all -> 0x0310 }
            if (r1 == 0) goto L_0x02fe
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0310 }
            r1.<init>()     // Catch:{ all -> 0x0310 }
            java.lang.String r8 = "CAM-"
            r1.append(r8)     // Catch:{ all -> 0x0310 }
            r1.append(r14)     // Catch:{ all -> 0x0310 }
            java.lang.String r14 = r1.toString()     // Catch:{ all -> 0x0310 }
        L_0x02fe:
            java.lang.String r1 = r17.a()     // Catch:{ all -> 0x0310 }
            r7.<init>(r5, r2, r14, r1)     // Catch:{ all -> 0x0310 }
            r7.setFileName(r6)     // Catch:{ all -> 0x0310 }
            r10.add(r7)     // Catch:{ all -> 0x0310 }
            goto L_0x0310
        L_0x030c:
            r18 = r1
        L_0x030e:
            r16 = r2
        L_0x0310:
            r1 = r18
            r2 = r16
            goto L_0x01c5
        L_0x0316:
            int r1 = r10.size()
            if (r1 <= 0) goto L_0x0337
            com.original.tase.model.media.MediaSource r1 = new com.original.tase.model.media.MediaSource
            java.lang.String r2 = r17.a()
            r3 = 0
            r1.<init>(r2, r13, r3)
            r2 = 1
            r1.setTorrent(r2)
            r1.setMagnetObjects(r10)
            java.lang.String r2 = "magnet:Zooqle"
            r1.setStreamLink(r2)
            r2 = r19
            r2.onNext(r1)
        L_0x0337:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Zooqle.c(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }
}
