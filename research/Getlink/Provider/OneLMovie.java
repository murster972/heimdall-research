package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.crypto.AESEncrypter;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class OneLMovie extends BaseProvider {
    private String c = "";
    private String d = "";
    private String e = "";
    private String f = "";
    private String g = "";

    public String a() {
        return "OneLMovie";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (!c().isEmpty() && BaseProvider.b() && !this.g.isEmpty()) {
            a(movieInfo, observableEmitter, "0", "0", movieInfo.year, true);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (!c().isEmpty() && BaseProvider.b() && !this.g.isEmpty()) {
            a(movieInfo, observableEmitter, movieInfo.session, movieInfo.eps, "0", true);
        }
    }

    public String c() {
        try {
            String a2 = HttpHelper.e().a("https://raw.githubusercontent.com/TeruSetephen/cinemaapk/master/provider/hmv.txt", (Map<String, String>[]) new Map[0]);
            String a3 = AESEncrypter.a(a2, Utils.ae());
            Logger.a("OneLMovie ", a3);
            if (a3 != null) {
                if (!a3.isEmpty()) {
                    String[] split = a3.split("##");
                    this.c = split[0];
                    this.d = split[1];
                    this.e = split[2];
                    this.f = split[3];
                    this.g = split[4].replace(" ", "");
                    return a2;
                }
            }
            return "";
        } catch (Throwable th) {
            Logger.a(th, "OneLMovie cccc", new boolean[0]);
            return "";
        }
    }

    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter, String str, String str2, String str3, boolean z) {
        String format = String.format(this.d, new Object[]{movieInfo.name.toLowerCase(), str, str2, Utils.md5(movieInfo.name.toLowerCase() + str.toString() + this.c + str2.toString()).toString(), str3});
        if (z) {
            format = format + this.g;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", this.e);
        Iterator it2 = Regex.b(HttpHelper.e().a(this.f, format, (Map<String, String>[]) new Map[]{hashMap}).replace("\\/", "/"), "link[\"]\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0).iterator();
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        String str4 = "HD";
        while (it2.hasNext()) {
            String str5 = (String) it2.next();
            String a2 = a();
            DirectoryIndexHelper.ParsedLinkModel a3 = directoryIndexHelper.a(str5);
            a3.a(str4);
            if (a3 != null) {
                if (!a3.c().equalsIgnoreCase("HQ")) {
                    str4 = a3.c();
                }
                a2 = f(a3.b());
            }
            HashMap hashMap2 = new HashMap();
            hashMap2.put("user-agent", Constants.f5838a);
            if (a2.isEmpty()) {
                a2 = a();
            }
            MediaSource mediaSource = new MediaSource(a2, "CDN", false);
            mediaSource.setStreamLink(str5);
            mediaSource.setPlayHeader(hashMap2);
            mediaSource.setQuality(str4);
            observableEmitter.onNext(mediaSource);
        }
    }
}
