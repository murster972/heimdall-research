package com.utils.Getlink.Provider;

import com.movie.FreeMoviesApp;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.realdebrid.MagnetObject;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class TTPB extends BaseProvider {
    public String[] c = Utils.getProvider(24).split(",");
    public String d = this.c[0];

    public String a() {
        return "TTPB";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(movieInfo, observableEmitter, true);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(movieInfo, observableEmitter, false);
        }
    }

    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter, boolean z) {
        String str;
        Iterator it2;
        String str2;
        boolean z2;
        DirectoryIndexHelper.ParsedLinkModel parsedLinkModel;
        int i;
        MovieInfo movieInfo2 = movieInfo;
        String str3 = "1080p";
        if (FreeMoviesApp.l().getBoolean("pref_use_unblock" + a(), false)) {
            this.d = this.c[1];
        }
        boolean z3 = movieInfo.getType().intValue() == 1;
        if (z3) {
            str = " " + movieInfo2.year;
        } else {
            str = " S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.eps));
        }
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("referer", this.d + "/");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        String str4 = this.d + "/s/?q=%s&category=%s&page=0&orderby=99";
        Object[] objArr = new Object[2];
        objArr[0] = com.original.tase.utils.Utils.a(movieInfo2.name + str, new boolean[0]);
        objArr[1] = Integer.valueOf(z ? 201 : 205);
        String format = String.format(str4, objArr);
        Iterator it3 = Jsoup.b(HttpHelper.e().b(format, this.d + "/")).g("tbody").b("tr").iterator();
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        ArrayList arrayList = new ArrayList();
        HashMap hashMap2 = new HashMap();
        while (it3.hasNext()) {
            Element element = (Element) it3.next();
            Element h = element.h("div.detName").h("a");
            h.b("href");
            String G = h.G();
            if (z3) {
                if (TitleHelper.f(G).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName()) + movieInfo2.year))) {
                    hashMap2.put(Regex.a(element.toString(), "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?", 1), G);
                }
            } else {
                if (TitleHelper.a(G.toLowerCase().replace(movieInfo2.year, ""), "").startsWith(TitleHelper.a(movieInfo.getName().toLowerCase() + str.toLowerCase(), ""))) {
                    hashMap2.put(Regex.a(element.toString(), "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?", 1), G);
                }
            }
            movieInfo2 = movieInfo;
        }
        new HashMap();
        Iterator it4 = hashMap2.entrySet().iterator();
        while (it4.hasNext()) {
            Map.Entry entry = (Map.Entry) it4.next();
            try {
                String replace = TitleHelper.a((String) entry.getValue(), ".").replace("..", ".");
                String[] split = replace.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                int length = split.length;
                String str5 = "HQ";
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        it2 = it4;
                        str2 = str3;
                        z2 = false;
                        break;
                    }
                    String lowerCase = split[i2].toLowerCase();
                    it2 = it4;
                    try {
                        if (lowerCase.contains("dvdscr") || lowerCase.contains("camrip") || lowerCase.contains("tsrip") || lowerCase.contains("hdcam") || lowerCase.contains("hdtc") || lowerCase.contains("hdts") || lowerCase.contains("dvdcam") || lowerCase.contains("dvdts") || lowerCase.contains("cam") || lowerCase.contains("telesync")) {
                            break;
                        } else if (lowerCase.contains("ts")) {
                            break;
                        } else {
                            boolean contains = lowerCase.contains(str3);
                            str2 = str3;
                            String str6 = "720p";
                            if (!contains) {
                                try {
                                    if (!lowerCase.equals("1080")) {
                                        if (!lowerCase.contains(str6)) {
                                            if (!lowerCase.equals("720")) {
                                                if (lowerCase.contains("brrip") || lowerCase.contains("bdrip") || lowerCase.contains("hdrip") || lowerCase.contains("web-dl")) {
                                                    str6 = "HD";
                                                } else {
                                                    i2++;
                                                    it4 = it2;
                                                    str3 = str2;
                                                }
                                            }
                                        }
                                        str5 = str6;
                                        i2++;
                                        it4 = it2;
                                        str3 = str2;
                                    }
                                } catch (Throwable unused) {
                                }
                            }
                            str5 = str2;
                            i2++;
                            it4 = it2;
                            str3 = str2;
                        }
                    } catch (Throwable unused2) {
                        str2 = str3;
                        it4 = it2;
                        str3 = str2;
                    }
                }
                str2 = str3;
                z2 = true;
                String str7 = (String) entry.getKey();
                String a2 = a();
                if (z3) {
                    parsedLinkModel = directoryIndexHelper.a(replace);
                } else {
                    parsedLinkModel = directoryIndexHelper.b(replace);
                }
                if (parsedLinkModel != null) {
                    String c2 = parsedLinkModel.c();
                    if (!c2.equalsIgnoreCase("HQ")) {
                        str5 = c2;
                    }
                    String b = parsedLinkModel.b();
                    i = 1;
                    a2 = a(b, true);
                } else {
                    i = 1;
                }
                String lowerCase2 = Regex.a(str7, "(magnet:\\?xt=urn:btih:[^&.]+)", i).toLowerCase();
                if (z2) {
                    str5 = "CAM-" + str5;
                }
                MagnetObject magnetObject = new MagnetObject(a2, lowerCase2, str5, a());
                magnetObject.setFileName(replace);
                arrayList.add(magnetObject);
            } catch (Throwable unused3) {
                it2 = it4;
                str2 = str3;
                it4 = it2;
                str3 = str2;
            }
            it4 = it2;
            str3 = str2;
        }
        if (arrayList.size() > 0) {
            MediaSource mediaSource = new MediaSource(a(), "", false);
            mediaSource.setTorrent(true);
            mediaSource.setMagnetObjects(arrayList);
            mediaSource.setStreamLink("magnet:TTPB");
            observableEmitter.onNext(mediaSource);
        }
    }
}
