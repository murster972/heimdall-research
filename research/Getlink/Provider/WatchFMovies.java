package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class WatchFMovies extends BaseProvider {
    public String c = Utils.getProvider(36);
    public HashMap d = new HashMap();

    /* JADX WARNING: type inference failed for: r26v15, types: [boolean] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r34, com.movie.data.model.MovieInfo r35, java.lang.String r36) {
        /*
            r33 = this;
            r1 = r33
            r2 = r34
            r0 = r36
            java.lang.String r3 = "onclick"
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            r5 = 0
            java.util.Map[] r6 = new java.util.Map[r5]
            java.lang.String r4 = r4.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r6)
            r6 = 1
            java.lang.String r7 = "<a.*?onclick.*?watching.*?href\\s*=\\s*['\"]([^'\"]+)['\"]"
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r4, (java.lang.String) r7, (int) r6)
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r7.b((java.lang.String) r4, (java.lang.String) r0)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.lang.String r8 = "a[title][episode-id][id*=episode]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r8)
            java.util.Iterator r8 = r0.iterator()
        L_0x0035:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x0489
            java.lang.Object r0 = r8.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            boolean r9 = r34.isDisposed()
            if (r9 != 0) goto L_0x0489
            java.lang.String r9 = "data-server"
            java.lang.String r9 = r0.b((java.lang.String) r9)     // Catch:{ all -> 0x046c }
            java.lang.String r10 = "title"
            java.lang.String r10 = r0.b((java.lang.String) r10)     // Catch:{ all -> 0x046c }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x046c }
            r11.<init>()     // Catch:{ all -> 0x046c }
            java.lang.String r12 = "(Episode\\s+0*"
            r11.append(r12)     // Catch:{ all -> 0x046c }
            r12 = r35
            java.lang.String r13 = r12.eps     // Catch:{ all -> 0x046c }
            r11.append(r13)     // Catch:{ all -> 0x046c }
            java.lang.String r13 = "(?!\\d))"
            r11.append(r13)     // Catch:{ all -> 0x046c }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x046c }
            r13 = 2
            java.lang.String r10 = com.original.tase.utils.Regex.a((java.lang.String) r10, (java.lang.String) r11, (int) r6, (int) r13)     // Catch:{ all -> 0x046c }
            boolean r10 = r10.isEmpty()     // Catch:{ all -> 0x046c }
            if (r10 != 0) goto L_0x0462
            boolean r10 = r0.d((java.lang.String) r3)     // Catch:{ all -> 0x046c }
            r11 = 34
            java.lang.String r14 = ""
            if (r10 == 0) goto L_0x008d
            java.lang.String r10 = r0.b((java.lang.String) r3)     // Catch:{ all -> 0x046c }
            java.lang.String r15 = "load_episode_iframe\\s*\\(\\d+\\s*,\\s*(\\d+)\\s*\\)"
            java.lang.String r10 = com.original.tase.utils.Regex.a((java.lang.String) r10, (java.lang.String) r15, (int) r6, (int) r11)     // Catch:{ all -> 0x046c }
            goto L_0x008e
        L_0x008d:
            r10 = r14
        L_0x008e:
            boolean r15 = r10.isEmpty()     // Catch:{ all -> 0x046c }
            if (r15 == 0) goto L_0x0095
            goto L_0x0096
        L_0x0095:
            r9 = r10
        L_0x0096:
            org.jsoup.nodes.Attributes r0 = r0.a()     // Catch:{ all -> 0x046c }
            java.util.Iterator r15 = r0.iterator()     // Catch:{ all -> 0x046c }
        L_0x009e:
            boolean r0 = r15.hasNext()     // Catch:{ all -> 0x046c }
            if (r0 == 0) goto L_0x0462
            java.lang.Object r0 = r15.next()     // Catch:{ all -> 0x046c }
            org.jsoup.nodes.Attribute r0 = (org.jsoup.nodes.Attribute) r0     // Catch:{ all -> 0x046c }
            java.lang.String r11 = r0.getKey()     // Catch:{ all -> 0x046c }
            java.lang.String r0 = r0.getValue()     // Catch:{ all -> 0x046c }
            if (r11 == 0) goto L_0x043b
            if (r0 == 0) goto L_0x043b
            boolean r16 = r11.isEmpty()     // Catch:{ all -> 0x046c }
            if (r16 != 0) goto L_0x043b
            boolean r16 = r0.isEmpty()     // Catch:{ all -> 0x046c }
            if (r16 != 0) goto L_0x043b
            java.lang.String r13 = "data-drive"
            boolean r13 = r11.equalsIgnoreCase(r13)     // Catch:{ all -> 0x046c }
            java.lang.String r6 = "Accept"
            java.lang.String r5 = "Referer"
            r17 = r3
            java.lang.String r3 = "User-Agent"
            r18 = r8
            java.lang.String r8 = ".vtt"
            r19 = r10
            java.lang.String r10 = ".srt"
            java.lang.String r12 = "/"
            java.lang.String r20 = "GoogleVideo"
            java.lang.String r21 = "CDN-FastServer"
            r22 = r15
            java.lang.String r15 = "HD"
            if (r13 == 0) goto L_0x01ba
            boolean r13 = r9.isEmpty()     // Catch:{ all -> 0x01b3 }
            if (r13 != 0) goto L_0x01ba
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b3 }
            r11.<init>()     // Catch:{ all -> 0x01b3 }
            java.lang.String r13 = "https://play.fmoviesfree.net/"
            r11.append(r13)     // Catch:{ all -> 0x01b3 }
            r11.append(r9)     // Catch:{ all -> 0x01b3 }
            r11.append(r12)     // Catch:{ all -> 0x01b3 }
            r11.append(r0)     // Catch:{ all -> 0x01b3 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x01b3 }
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x01b3 }
            java.lang.String r0 = r0.b((java.lang.String) r11, (java.lang.String) r4)     // Catch:{ all -> 0x01b3 }
            java.util.ArrayList r0 = r1.g(r0)     // Catch:{ all -> 0x01b3 }
            java.util.Iterator r12 = r0.iterator()     // Catch:{ all -> 0x01b3 }
        L_0x0111:
            boolean r0 = r12.hasNext()     // Catch:{ all -> 0x01b3 }
            if (r0 == 0) goto L_0x01ab
            java.lang.Object r0 = r12.next()     // Catch:{ all -> 0x01b3 }
            r13 = r0
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x01b3 }
            boolean r0 = r13.endsWith(r10)     // Catch:{ all -> 0x01b3 }
            if (r0 != 0) goto L_0x019d
            boolean r0 = r13.startsWith(r8)     // Catch:{ all -> 0x01b3 }
            if (r0 != 0) goto L_0x019d
            boolean r0 = r7.contains(r13)     // Catch:{ all -> 0x01b3 }
            if (r0 != 0) goto L_0x019d
            r7.add(r13)     // Catch:{ all -> 0x01b3 }
            boolean r23 = com.original.tase.helper.GoogleVideoHelper.k(r13)     // Catch:{ all -> 0x01b3 }
            java.lang.String r0 = r33.a()     // Catch:{ all -> 0x0140 }
            r24 = r9
            r25 = r12
            goto L_0x014e
        L_0x0140:
            r0 = move-exception
            r24 = r9
            r9 = r0
            r25 = r12
            r12 = 0
            boolean[] r0 = new boolean[r12]     // Catch:{ all -> 0x01b3 }
            com.original.tase.Logger.a((java.lang.Throwable) r9, (boolean[]) r0)     // Catch:{ all -> 0x01b3 }
            r0 = r19
        L_0x014e:
            com.original.tase.model.media.MediaSource r9 = new com.original.tase.model.media.MediaSource     // Catch:{ all -> 0x01b3 }
            r26 = r7
            if (r23 == 0) goto L_0x0157
            r12 = r20
            goto L_0x0159
        L_0x0157:
            r12 = r21
        L_0x0159:
            r7 = 0
            r9.<init>(r0, r12, r7)     // Catch:{ all -> 0x0435 }
            r9.setStreamLink(r13)     // Catch:{ all -> 0x0435 }
            if (r23 == 0) goto L_0x0167
            java.lang.String r7 = com.original.tase.helper.GoogleVideoHelper.h(r13)     // Catch:{ all -> 0x0435 }
            goto L_0x0168
        L_0x0167:
            r7 = r15
        L_0x0168:
            r9.setQuality((java.lang.String) r7)     // Catch:{ all -> 0x0435 }
            if (r23 != 0) goto L_0x0197
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ all -> 0x0435 }
            r7.<init>()     // Catch:{ all -> 0x0435 }
            java.lang.String r12 = com.original.Constants.f5838a     // Catch:{ all -> 0x0435 }
            r7.put(r3, r12)     // Catch:{ all -> 0x0435 }
            r7.put(r5, r11)     // Catch:{ all -> 0x0435 }
            java.lang.String r12 = "*/*"
            r7.put(r6, r12)     // Catch:{ all -> 0x0435 }
            java.lang.String r12 = "Accept-Encoding"
            java.lang.String r13 = "identity;q=1, *;q=0"
            r7.put(r12, r13)     // Catch:{ all -> 0x0435 }
            java.lang.String r12 = "Accept-Language"
            java.lang.String r13 = "en-US,en;q=0.9,zh-HK;q=0.8,zh-TW;q=0.7,zh;q=0.6"
            r7.put(r12, r13)     // Catch:{ all -> 0x0435 }
            java.lang.String r12 = "X-TTV-Custom"
            java.lang.String r13 = "rangeFromZero"
            r7.put(r12, r13)     // Catch:{ all -> 0x0435 }
            r9.setPlayHeader(r7)     // Catch:{ all -> 0x0435 }
        L_0x0197:
            r2.onNext(r9)     // Catch:{ all -> 0x0435 }
            r19 = r0
            goto L_0x01a3
        L_0x019d:
            r26 = r7
            r24 = r9
            r25 = r12
        L_0x01a3:
            r9 = r24
            r12 = r25
            r7 = r26
            goto L_0x0111
        L_0x01ab:
            r24 = r9
            r23 = r4
            r27 = r7
            goto L_0x0449
        L_0x01b3:
            r0 = move-exception
            r23 = r4
            r27 = r7
            goto L_0x0475
        L_0x01ba:
            r26 = r7
            r24 = r9
            java.lang.String r7 = "data-openload"
            boolean r7 = r11.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0435 }
            if (r7 == 0) goto L_0x01e6
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0435 }
            r3.<init>()     // Catch:{ all -> 0x0435 }
            java.lang.String r5 = "https://openload.co/embed/"
            r3.append(r5)     // Catch:{ all -> 0x0435 }
            r3.append(r0)     // Catch:{ all -> 0x0435 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0435 }
            r3 = 1
            boolean[] r5 = new boolean[r3]     // Catch:{ all -> 0x0435 }
            r3 = 0
            r5[r3] = r3     // Catch:{ all -> 0x0435 }
            r1.a(r2, r0, r15, r5)     // Catch:{ all -> 0x0435 }
        L_0x01e0:
            r23 = r4
            r27 = r26
            goto L_0x0449
        L_0x01e6:
            java.lang.String r7 = "data-smango2"
            boolean r7 = r11.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0435 }
            if (r7 == 0) goto L_0x0209
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0435 }
            r3.<init>()     // Catch:{ all -> 0x0435 }
            java.lang.String r5 = "https://streamango.com/embed/"
            r3.append(r5)     // Catch:{ all -> 0x0435 }
            r3.append(r0)     // Catch:{ all -> 0x0435 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0435 }
            r3 = 1
            boolean[] r5 = new boolean[r3]     // Catch:{ all -> 0x0435 }
            r3 = 0
            r5[r3] = r3     // Catch:{ all -> 0x0435 }
            r1.a(r2, r0, r15, r5)     // Catch:{ all -> 0x0435 }
            goto L_0x01e0
        L_0x0209:
            java.lang.String r7 = "data-vcloud2"
            boolean r7 = r11.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0435 }
            if (r7 == 0) goto L_0x022c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0435 }
            r3.<init>()     // Catch:{ all -> 0x0435 }
            java.lang.String r5 = "https://vidcloud.co/embed/"
            r3.append(r5)     // Catch:{ all -> 0x0435 }
            r3.append(r0)     // Catch:{ all -> 0x0435 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0435 }
            r3 = 1
            boolean[] r5 = new boolean[r3]     // Catch:{ all -> 0x0435 }
            r3 = 0
            r5[r3] = r3     // Catch:{ all -> 0x0435 }
            r1.a(r2, r0, r15, r5)     // Catch:{ all -> 0x0435 }
            goto L_0x01e0
        L_0x022c:
            java.lang.String r7 = "data-estream"
            boolean r7 = r11.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0435 }
            if (r7 == 0) goto L_0x0254
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0435 }
            r3.<init>()     // Catch:{ all -> 0x0435 }
            java.lang.String r5 = "https://estream.to/embed-"
            r3.append(r5)     // Catch:{ all -> 0x0435 }
            r3.append(r0)     // Catch:{ all -> 0x0435 }
            java.lang.String r0 = ".html"
            r3.append(r0)     // Catch:{ all -> 0x0435 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x0435 }
            r3 = 1
            boolean[] r5 = new boolean[r3]     // Catch:{ all -> 0x0435 }
            r3 = 0
            r5[r3] = r3     // Catch:{ all -> 0x0435 }
            r1.a(r2, r0, r15, r5)     // Catch:{ all -> 0x0435 }
            goto L_0x01e0
        L_0x0254:
            java.lang.String r7 = "data-strgo"
            boolean r7 = r11.equalsIgnoreCase(r7)     // Catch:{ all -> 0x0435 }
            if (r7 == 0) goto L_0x01e0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0435 }
            r7.<init>()     // Catch:{ all -> 0x0435 }
            java.lang.String r9 = "https://harpy.tv/player/"
            r7.append(r9)     // Catch:{ all -> 0x0435 }
            r7.append(r0)     // Catch:{ all -> 0x0435 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x0435 }
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x0435 }
            java.lang.String r7 = r7.b((java.lang.String) r0, (java.lang.String) r4)     // Catch:{ all -> 0x0435 }
            java.lang.String r9 = "(JuicyCodes\\.Run\\s*\\(.*?\\)\\s*;)"
            r11 = 2
            r13 = 1
            java.lang.String r9 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r9, (int) r13, (int) r11)     // Catch:{ all -> 0x0435 }
            java.lang.String r9 = com.original.tase.helper.js.JuicyDecoder.m30924(r9)     // Catch:{ all -> 0x0435 }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ all -> 0x0435 }
            r11.<init>()     // Catch:{ all -> 0x0435 }
            r11.add(r7)     // Catch:{ all -> 0x0435 }
            boolean r7 = r9.isEmpty()     // Catch:{ all -> 0x0435 }
            if (r7 != 0) goto L_0x029f
            r11.add(r9)     // Catch:{ all -> 0x0435 }
            boolean r7 = com.original.tase.helper.js.JsUnpacker.m30920(r9)     // Catch:{ all -> 0x0435 }
            if (r7 == 0) goto L_0x029f
            java.util.ArrayList r7 = com.original.tase.helper.js.JsUnpacker.m30916(r9)     // Catch:{ all -> 0x0435 }
            r11.addAll(r7)     // Catch:{ all -> 0x0435 }
        L_0x029f:
            java.util.Iterator r7 = r11.iterator()     // Catch:{ all -> 0x0435 }
        L_0x02a3:
            boolean r9 = r7.hasNext()     // Catch:{ all -> 0x0435 }
            if (r9 == 0) goto L_0x01e0
            java.lang.Object r9 = r7.next()     // Catch:{ all -> 0x0435 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0435 }
            java.lang.String r11 = "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*.*?['\"]?label['\"]?\\s*:\\s*['\"]?(\\d{3,4})p"
            r1 = 2
            r13 = 34
            java.util.ArrayList r11 = com.original.tase.utils.Regex.b((java.lang.String) r9, (java.lang.String) r11, (int) r1, (int) r13)     // Catch:{ all -> 0x0435 }
            r1 = 0
            java.lang.Object r23 = r11.get(r1)     // Catch:{ all -> 0x042f }
            r1 = r23
            java.util.ArrayList r1 = (java.util.ArrayList) r1     // Catch:{ all -> 0x0435 }
            r13 = 1
            java.lang.Object r11 = r11.get(r13)     // Catch:{ all -> 0x0435 }
            java.util.ArrayList r11 = (java.util.ArrayList) r11     // Catch:{ all -> 0x0435 }
            r23 = r4
            java.lang.String r4 = "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+\\.m3u8[^'\"]*)['\"]"
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r9, (java.lang.String) r4, (int) r13, (boolean) r13)     // Catch:{ all -> 0x042d }
            boolean r9 = r4.isEmpty()     // Catch:{ all -> 0x042d }
            if (r9 != 0) goto L_0x02dc
            r1.add(r4)     // Catch:{ all -> 0x042d }
            r11.add(r15)     // Catch:{ all -> 0x042d }
        L_0x02dc:
            r4 = 0
        L_0x02dd:
            int r9 = r1.size()     // Catch:{ all -> 0x042d }
            if (r4 >= r9) goto L_0x0427
            java.lang.Object r9 = r1.get(r4)     // Catch:{ all -> 0x042d }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x042d }
            java.lang.String r13 = "\\/"
            java.lang.String r9 = r9.replace(r13, r12)     // Catch:{ all -> 0x042d }
            java.lang.String r13 = "\\\""
            java.lang.String r9 = r9.replace(r13, r14)     // Catch:{ all -> 0x042d }
            java.lang.Object r13 = r11.get(r4)     // Catch:{ Exception -> 0x02fe }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ Exception -> 0x02fe }
            r25 = r1
            goto L_0x0301
        L_0x02fe:
            r25 = r1
            r13 = r15
        L_0x0301:
            java.lang.String r1 = "1080"
            boolean r1 = r13.contains(r1)     // Catch:{ all -> 0x042d }
            if (r1 != 0) goto L_0x0311
            java.lang.String r1 = "720"
            boolean r1 = r13.contains(r1)     // Catch:{ all -> 0x042d }
            if (r1 == 0) goto L_0x0312
        L_0x0311:
            r13 = r15
        L_0x0312:
            boolean r1 = r9.endsWith(r10)     // Catch:{ all -> 0x042d }
            if (r1 != 0) goto L_0x040d
            boolean r1 = r9.startsWith(r8)     // Catch:{ all -> 0x042d }
            if (r1 != 0) goto L_0x040d
            r1 = r26
            boolean r26 = r1.contains(r9)     // Catch:{ all -> 0x0408 }
            if (r26 != 0) goto L_0x03fd
            r1.add(r9)     // Catch:{ all -> 0x0408 }
            boolean r26 = com.original.tase.helper.GoogleVideoHelper.k(r9)     // Catch:{ all -> 0x0408 }
            if (r26 != 0) goto L_0x03cf
            java.util.HashMap r13 = new java.util.HashMap     // Catch:{ all -> 0x0408 }
            r13.<init>()     // Catch:{ all -> 0x0408 }
            r27 = r1
            java.lang.String r1 = com.original.Constants.f5838a     // Catch:{ all -> 0x03fa }
            r13.put(r3, r1)     // Catch:{ all -> 0x03fa }
            r13.put(r5, r0)     // Catch:{ all -> 0x03fa }
            java.lang.String r1 = "*/*"
            r13.put(r6, r1)     // Catch:{ all -> 0x03fa }
            java.lang.String r1 = "Accept-Encoding"
            r28 = r0
            java.lang.String r0 = "identity;q=1, *;q=0"
            r13.put(r1, r0)     // Catch:{ all -> 0x03fa }
            java.lang.String r0 = "Accept-Language"
            java.lang.String r1 = "en-US,en;q=0.9,zh-HK;q=0.8,zh-TW;q=0.7,zh;q=0.6"
            r13.put(r0, r1)     // Catch:{ all -> 0x03fa }
            java.lang.String r0 = "X-TTV-Custom"
            java.lang.String r1 = "rangeFromZero"
            r13.put(r0, r1)     // Catch:{ all -> 0x03fa }
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x03fa }
            r29 = r3
            r1 = 1
            java.util.Map[] r3 = new java.util.Map[r1]     // Catch:{ all -> 0x03fa }
            r1 = 0
            r3[r1] = r13     // Catch:{ all -> 0x03cc }
            java.lang.String r0 = r0.a((java.lang.String) r9, (java.util.Map<java.lang.String, java.lang.String>[]) r3)     // Catch:{ all -> 0x03fa }
            java.lang.String r1 = "\n"
            java.lang.String r3 = ";"
            java.lang.String r0 = r0.replace(r1, r3)     // Catch:{ all -> 0x03fa }
            java.lang.String r1 = "([0-9]+)(.m3u8)"
            r3 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r1, (int) r3, (boolean) r3)     // Catch:{ all -> 0x03fa }
            r1 = 0
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x03cc }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x03fa }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x03fa }
        L_0x0384:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x03fa }
            if (r1 == 0) goto L_0x0403
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x03fa }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x03fa }
            com.original.tase.model.media.MediaSource r3 = new com.original.tase.model.media.MediaSource     // Catch:{ all -> 0x03fa }
            r30 = r0
            java.lang.String r0 = r33.a()     // Catch:{ all -> 0x03fa }
            r31 = r5
            r32 = r6
            if (r26 == 0) goto L_0x03a3
            r5 = r20
            goto L_0x03a5
        L_0x03a3:
            r5 = r21
        L_0x03a5:
            r6 = 0
            r3.<init>(r0, r5, r6)     // Catch:{ all -> 0x03fa }
            java.lang.String r0 = "video"
            java.lang.String r0 = r9.replace(r0, r1)     // Catch:{ all -> 0x03fa }
            r3.setStreamLink(r0)     // Catch:{ all -> 0x03fa }
            if (r26 == 0) goto L_0x03b8
            java.lang.String r1 = com.original.tase.helper.GoogleVideoHelper.h(r9)     // Catch:{ all -> 0x03fa }
        L_0x03b8:
            r3.setQuality((java.lang.String) r1)     // Catch:{ all -> 0x03fa }
            r2.onNext(r3)     // Catch:{ all -> 0x03fa }
            r3.setPlayHeader(r13)     // Catch:{ all -> 0x03fa }
            r2.onNext(r3)     // Catch:{ all -> 0x03fa }
            r0 = r30
            r5 = r31
            r6 = r32
            r3 = 1
            goto L_0x0384
        L_0x03cc:
            r0 = move-exception
            goto L_0x0476
        L_0x03cf:
            r28 = r0
            r27 = r1
            r29 = r3
            r31 = r5
            r32 = r6
            com.original.tase.model.media.MediaSource r0 = new com.original.tase.model.media.MediaSource     // Catch:{ all -> 0x03fa }
            java.lang.String r1 = r33.a()     // Catch:{ all -> 0x03fa }
            if (r26 == 0) goto L_0x03e4
            r3 = r20
            goto L_0x03e6
        L_0x03e4:
            r3 = r21
        L_0x03e6:
            r5 = 0
            r0.<init>(r1, r3, r5)     // Catch:{ all -> 0x03fa }
            r0.setStreamLink(r9)     // Catch:{ all -> 0x03fa }
            if (r26 == 0) goto L_0x03f3
            java.lang.String r13 = com.original.tase.helper.GoogleVideoHelper.h(r9)     // Catch:{ all -> 0x03fa }
        L_0x03f3:
            r0.setQuality((java.lang.String) r13)     // Catch:{ all -> 0x03fa }
            r2.onNext(r0)     // Catch:{ all -> 0x03fa }
            goto L_0x0417
        L_0x03fa:
            r0 = move-exception
            goto L_0x0475
        L_0x03fd:
            r28 = r0
            r27 = r1
            r29 = r3
        L_0x0403:
            r31 = r5
            r32 = r6
            goto L_0x0417
        L_0x0408:
            r0 = move-exception
            r27 = r1
            goto L_0x0475
        L_0x040d:
            r28 = r0
            r29 = r3
            r31 = r5
            r32 = r6
            r27 = r26
        L_0x0417:
            int r4 = r4 + 1
            r1 = r25
            r26 = r27
            r0 = r28
            r3 = r29
            r5 = r31
            r6 = r32
            goto L_0x02dd
        L_0x0427:
            r1 = r33
            r4 = r23
            goto L_0x02a3
        L_0x042d:
            r0 = move-exception
            goto L_0x0438
        L_0x042f:
            r0 = move-exception
            r23 = r4
            r27 = r26
            goto L_0x0476
        L_0x0435:
            r0 = move-exception
            r23 = r4
        L_0x0438:
            r27 = r26
            goto L_0x0475
        L_0x043b:
            r17 = r3
            r23 = r4
            r27 = r7
            r18 = r8
            r24 = r9
            r19 = r10
            r22 = r15
        L_0x0449:
            r10 = r19
            r1 = r33
            r12 = r35
            r3 = r17
            r8 = r18
            r15 = r22
            r4 = r23
            r9 = r24
            r7 = r27
            r5 = 0
            r6 = 1
            r11 = 34
            r13 = 2
            goto L_0x009e
        L_0x0462:
            r17 = r3
            r23 = r4
            r27 = r7
            r18 = r8
            r1 = 0
            goto L_0x047b
        L_0x046c:
            r0 = move-exception
            r17 = r3
            r23 = r4
            r27 = r7
            r18 = r8
        L_0x0475:
            r1 = 0
        L_0x0476:
            boolean[] r3 = new boolean[r1]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r3)
        L_0x047b:
            r1 = r33
            r3 = r17
            r8 = r18
            r4 = r23
            r7 = r27
            r5 = 0
            r6 = 1
            goto L_0x0035
        L_0x0489:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.WatchFMovies.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo, java.lang.String):void");
    }

    public String a() {
        return "WatchFMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    private String b(MovieInfo movieInfo) {
        int intValue = movieInfo.getType().intValue();
        this.d.put("Referer", this.c + "/");
        this.d.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        this.d.put("User-Agent", Constants.f5838a);
        String replace = com.original.tase.utils.Utils.a(TitleHelper.e(movieInfo.getName()), new boolean[0]).replace("%20", "+");
        HttpHelper e = HttpHelper.e();
        Iterator it2 = Jsoup.b(e.a(this.c + "/search-query/" + replace.toLowerCase(), "g-recaptcha-response=03AOLTBLRAO34thW-Dwc-sQ74r0ShCIHECcz4sImuv-1i5ma7Zs9uwlyYIQ1u4bxDsdrjanmbtHL1aTck_UrlNOcXXsByRZ7VaDLMWzn2DwX82gA58khcWNWZowFi6iF0bxcQgHekoJn1nWnb6GyJCwZQqFe_NMjrZpvFBmhHE9ye8Y172SxczcXQvLp-seW40IVKlcEU62Fv-umXBjmiYDOlVXYAiXfYIc90RhXzHKj05Yq8dXSGlTl5TGCJnpO82UNHQY38geeUWmszyvxIoOK0LJrUwl4bPqsZJlE--" + DateTimeHelper.c() + "QKLm3NkpcCCTenol5xPUKCuoK_o_", (Map<String, String>[]) new Map[]{this.d})).g("div.ml-item").iterator();
        while (it2.hasNext()) {
            try {
                Element h = ((Element) it2.next()).h("a[href]");
                if (h != null) {
                    String b = h.b("href");
                    String b2 = h.b("title");
                    TitleHelper.f(b2);
                    TitleHelper.f(movieInfo.name + "season" + movieInfo.session);
                    if (TitleHelper.f(b2).equals(TitleHelper.f(movieInfo.name + "season" + movieInfo.session))) {
                        return b;
                    }
                }
            } catch (Throwable unused) {
            }
        }
        return "";
    }
}
