package com.utils.Getlink.Provider;

import com.movie.data.api.GlobalVariable;
import com.movie.data.model.MovieInfo;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class CDNRelease extends BaseProvider {
    public String[] c = null;
    public int d = -1;
    private int e = 0;

    private static String[] c() {
        String str = GlobalVariable.c().a().getCbflist().get("CDNRelease");
        if (str == null || str.isEmpty()) {
            return Utils.getProvider(22).split(",");
        }
        return str.split(",");
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        this.c = c();
        if (!Utils.b && !BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    public String b(MovieInfo movieInfo, String str, String str2) {
        Iterator it2 = Jsoup.b(str).g("div[class=r]").iterator();
        if (!it2.hasNext()) {
            return "";
        }
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            try {
                String f = TitleHelper.f(h.h("span").G());
                if (f.contains(TitleHelper.f(TitleHelper.e(movieInfo.getName()) + movieInfo.year))) {
                    String b = h.b("href");
                    if (b.contains(str2)) {
                        return b;
                    }
                } else {
                    continue;
                }
            } catch (Throwable unused) {
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public String a(MovieInfo movieInfo, String str, String str2) {
        Iterator it2 = Jsoup.b(str).g("h2").iterator();
        while (it2.hasNext()) {
            try {
                Element h = ((Element) it2.next()).h("a");
                String f = TitleHelper.f(h.G());
                if (f.contains(TitleHelper.f(TitleHelper.e(movieInfo.getName()) + movieInfo.year))) {
                    String b = h.b("href");
                    if (b.contains(str2)) {
                        return b;
                    }
                } else {
                    continue;
                }
            } catch (Throwable unused) {
            }
        }
        return "";
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a8  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(com.movie.data.model.MovieInfo r20, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r21) {
        /*
            r19 = this;
            r0 = r19
            r1 = r20
            java.lang.String r2 = r20.getName()
            r3 = 0
            boolean[] r4 = new boolean[r3]
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r4)
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.lang.String r6 = "Accept"
            java.lang.String r7 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
            r5.put(r6, r7)
            java.lang.String r6 = com.original.Constants.f5838a
            java.lang.String r7 = "User-Agent"
            r5.put(r7, r6)
            com.original.tase.helper.DirectoryIndexHelper r6 = new com.original.tase.helper.DirectoryIndexHelper
            r6.<init>()
            r8 = -1
            r0.d = r8
            r8 = 0
        L_0x0030:
            java.lang.String[] r9 = r0.c
            int r9 = r9.length
            if (r8 >= r9) goto L_0x0225
            int r9 = r8 / 2
            r0.d = r9
            boolean r9 = r21.isDisposed()
            if (r9 == 0) goto L_0x0040
            return
        L_0x0040:
            java.lang.String[] r9 = r0.c
            r9 = r9[r8]
            r10 = 1
            java.lang.String r11 = "(http.+\\.\\w+)\\/"
            java.lang.String r11 = com.original.tase.utils.Regex.a((java.lang.String) r9, (java.lang.String) r11, (int) r10)
            com.original.tase.helper.http.HttpHelper r12 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r13 = new java.util.Map[r3]
            java.lang.String r12 = r12.a((java.lang.String) r11, (java.util.Map<java.lang.String, java.lang.String>[]) r13)
            java.lang.String r12 = com.utils.Getlink.Provider.BaseProvider.e(r12, r11)
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r11)
            java.lang.String r14 = "/"
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            java.lang.String r14 = "referer"
            r4.put(r14, r13)
            boolean r13 = r12.isEmpty()
            if (r13 != 0) goto L_0x0087
            java.lang.String r13 = "cdn-cgi/l"
            boolean r13 = r12.contains(r13)
            if (r13 == 0) goto L_0x007e
            goto L_0x0087
        L_0x007e:
            java.lang.Object[] r13 = new java.lang.Object[r10]
            r13[r3] = r2
            java.lang.String r12 = java.lang.String.format(r12, r13)
            goto L_0x0096
        L_0x0087:
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            r12.append(r9)
            r12.append(r2)
            java.lang.String r12 = r12.toString()
        L_0x0096:
            com.original.tase.helper.http.HttpHelper r13 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r14 = new java.util.Map[r10]
            r14[r3] = r4
            java.lang.String r12 = r13.a((java.lang.String) r12, (java.util.Map<java.lang.String, java.lang.String>[]) r14)
            boolean r13 = r12.isEmpty()
            if (r13 == 0) goto L_0x00ae
            r11 = r21
            r18 = r2
            goto L_0x021f
        L_0x00ae:
            int r13 = r0.e
            r14 = 3
            if (r13 <= r14) goto L_0x00b4
            return
        L_0x00b4:
            java.lang.String r13 = ""
            org.jsoup.nodes.Document r12 = org.jsoup.Jsoup.b(r12)     // Catch:{ all -> 0x016c }
            java.lang.String r14 = "h2"
            org.jsoup.select.Elements r12 = r12.g((java.lang.String) r14)     // Catch:{ all -> 0x016c }
            java.lang.String r14 = "a[href]"
            org.jsoup.select.Elements r12 = r12.b(r14)     // Catch:{ all -> 0x016c }
            java.util.Iterator r12 = r12.iterator()     // Catch:{ all -> 0x016c }
        L_0x00ca:
            boolean r14 = r12.hasNext()     // Catch:{ all -> 0x016c }
            if (r14 == 0) goto L_0x016c
            java.lang.Object r14 = r12.next()     // Catch:{ all -> 0x0164 }
            org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14     // Catch:{ all -> 0x0164 }
            java.lang.String r15 = "title"
            java.lang.String r15 = r14.b((java.lang.String) r15)     // Catch:{ all -> 0x0164 }
            boolean r16 = r15.isEmpty()     // Catch:{ all -> 0x0164 }
            if (r16 == 0) goto L_0x00e6
            java.lang.String r15 = r14.G()     // Catch:{ all -> 0x0164 }
        L_0x00e6:
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r15)     // Catch:{ all -> 0x0164 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0164 }
            r10.<init>()     // Catch:{ all -> 0x0164 }
            java.lang.String r17 = r20.getName()     // Catch:{ all -> 0x0164 }
            r18 = r2
            java.lang.String r2 = com.original.tase.helper.TitleHelper.e(r17)     // Catch:{ all -> 0x0166 }
            r10.append(r2)     // Catch:{ all -> 0x0166 }
            java.lang.String r2 = r1.year     // Catch:{ all -> 0x0166 }
            r10.append(r2)     // Catch:{ all -> 0x0166 }
            java.lang.String r2 = r10.toString()     // Catch:{ all -> 0x0166 }
            java.lang.String r2 = com.original.tase.helper.TitleHelper.f(r2)     // Catch:{ all -> 0x0166 }
            boolean r2 = r3.endsWith(r2)     // Catch:{ all -> 0x0166 }
            if (r2 != 0) goto L_0x015d
            java.lang.String r2 = com.original.tase.helper.TitleHelper.f(r15)     // Catch:{ all -> 0x0166 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0166 }
            r3.<init>()     // Catch:{ all -> 0x0166 }
            java.lang.String r10 = r20.getName()     // Catch:{ all -> 0x0166 }
            java.lang.String r10 = com.original.tase.helper.TitleHelper.e(r10)     // Catch:{ all -> 0x0166 }
            r3.append(r10)     // Catch:{ all -> 0x0166 }
            java.lang.String r10 = r1.year     // Catch:{ all -> 0x0166 }
            r3.append(r10)     // Catch:{ all -> 0x0166 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0166 }
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r3)     // Catch:{ all -> 0x0166 }
            boolean r2 = r2.startsWith(r3)     // Catch:{ all -> 0x0166 }
            if (r2 != 0) goto L_0x015d
            java.lang.String r2 = com.original.tase.helper.TitleHelper.f(r15)     // Catch:{ all -> 0x0166 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0166 }
            r3.<init>()     // Catch:{ all -> 0x0166 }
            java.lang.String r10 = r20.getName()     // Catch:{ all -> 0x0166 }
            java.lang.String r10 = com.original.tase.helper.TitleHelper.e(r10)     // Catch:{ all -> 0x0166 }
            r3.append(r10)     // Catch:{ all -> 0x0166 }
            java.lang.String r10 = r1.year     // Catch:{ all -> 0x0166 }
            r3.append(r10)     // Catch:{ all -> 0x0166 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0166 }
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r3)     // Catch:{ all -> 0x0166 }
            boolean r2 = r2.contains(r3)     // Catch:{ all -> 0x0166 }
            if (r2 == 0) goto L_0x0166
        L_0x015d:
            java.lang.String r2 = "href"
            java.lang.String r13 = r14.b((java.lang.String) r2)     // Catch:{ all -> 0x0166 }
            goto L_0x016e
        L_0x0164:
            r18 = r2
        L_0x0166:
            r2 = r18
            r3 = 0
            r10 = 1
            goto L_0x00ca
        L_0x016c:
            r18 = r2
        L_0x016e:
            boolean r2 = r13.isEmpty()
            if (r2 == 0) goto L_0x0188
            java.lang.String r2 = "CDNRelease movie not found "
            com.original.tase.Logger.a((java.lang.String) r2, (java.lang.String) r9)
            java.lang.String r13 = r0.a((com.movie.data.model.MovieInfo) r1, (java.lang.String) r11)
            boolean r2 = r13.isEmpty()
            if (r2 == 0) goto L_0x0188
            r11 = r21
            r3 = 0
            goto L_0x021f
        L_0x0188:
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r2 = r2.b((java.lang.String) r13, (java.lang.String) r11)
            java.lang.String r3 = "href=['\"]([^'\"]+(mkv|mp4|avi))['\"]"
            r9 = 1
            java.util.ArrayList r2 = com.original.tase.utils.Regex.b((java.lang.String) r2, (java.lang.String) r3, (int) r9, (boolean) r9)
            r3 = 0
            java.lang.Object r2 = r2.get(r3)
            java.util.ArrayList r2 = (java.util.ArrayList) r2
            java.util.Iterator r2 = r2.iterator()
            boolean r10 = r2.hasNext()
            if (r10 == 0) goto L_0x01ad
            int r10 = r0.e
            int r10 = r10 + r9
            r0.e = r10
        L_0x01ad:
            java.lang.String r9 = "Referer"
            r5.put(r9, r13)
            java.lang.String r9 = com.original.Constants.f5838a
            r5.put(r7, r9)
            java.lang.String r9 = "HQ"
            r10 = r9
        L_0x01ba:
            boolean r11 = r2.hasNext()
            if (r11 == 0) goto L_0x021d
            java.lang.Object r11 = r2.next()
            java.lang.String r11 = (java.lang.String) r11
            java.lang.String r12 = r11.toLowerCase()
            java.lang.String r13 = "trailer"
            boolean r12 = r12.contains(r13)
            if (r12 != 0) goto L_0x021a
            java.lang.String r12 = r11.toLowerCase()
            java.lang.String r13 = "comment"
            boolean r12 = r12.contains(r13)
            if (r12 == 0) goto L_0x01df
            goto L_0x021a
        L_0x01df:
            java.lang.String r12 = r19.a()
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r13 = r6.a(r11)
            if (r13 == 0) goto L_0x01fc
            java.lang.String r12 = r13.c()
            boolean r14 = r12.equalsIgnoreCase(r9)
            if (r14 != 0) goto L_0x01f4
            r10 = r12
        L_0x01f4:
            java.lang.String r12 = r13.b()
            java.lang.String r12 = r0.f(r12)
        L_0x01fc:
            com.original.tase.model.media.MediaSource r13 = new com.original.tase.model.media.MediaSource
            java.lang.String[] r14 = r0.c
            int r15 = r8 + 1
            r14 = r14[r15]
            boolean r15 = r0.d(r11)
            r13.<init>(r12, r14, r15)
            r13.setStreamLink(r11)
            r13.setPlayHeader(r5)
            r13.setQuality((java.lang.String) r10)
            r11 = r21
            r11.onNext(r13)
            goto L_0x01ba
        L_0x021a:
            r11 = r21
            goto L_0x01ba
        L_0x021d:
            r11 = r21
        L_0x021f:
            int r8 = r8 + 2
            r2 = r18
            goto L_0x0030
        L_0x0225:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.CDNRelease.c(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    public String a(MovieInfo movieInfo, String str) {
        String str2 = movieInfo.name + " " + movieInfo.year + " site:" + str;
        String a2 = a(movieInfo, HttpHelper.e().b("https://www.bing.com/search?q=" + str2, "https://www.bing.com"), str);
        if (!a2.isEmpty()) {
            return a2;
        }
        String a3 = com.original.tase.utils.Utils.a(Utils.l() + "/search?q=" + str2, new boolean[0]);
        String b = b(movieInfo, HttpHelper.e().a(a3 + "##forceNoCache##", (Map<String, String>[]) new Map[0]), str);
        return !b.isEmpty() ? b : "";
    }

    public String a() {
        if (this.d == -1) {
            return "CDNRelease";
        }
        return "CDNRelease" + com.original.tase.utils.Utils.a(this.d);
    }
}
