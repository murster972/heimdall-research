package com.utils.Getlink.Provider;

import android.util.Base64;
import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

public class Pubfilm extends BaseProvider {
    private static String d = c();
    private String c = Utils.getProvider(2);

    private static String c() {
        String str = d;
        if (str == null || str.isEmpty()) {
            try {
                d = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL3BmYy50eHQ=", 10), "UTF-8");
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                d = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Byb3ZpZGVyL3BmYy50eHQ=", 10));
            }
        }
        return d;
    }

    private String j(String str) {
        Duktape create = Duktape.create();
        try {
            Object evaluate = create.evaluate("function acb(){for(var result=\"####\";result.includes(\";eval\");)result=result.replace(/;eval/gi,\"\"),result=eval(result);return result}acb()".replace("####", str));
            if (evaluate != null) {
                String obj = evaluate.toString();
                create.close();
                return obj;
            }
        } catch (Throwable unused) {
        }
        create.close();
        return "";
    }

    public String a() {
        return "Pubfilm";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, "-1");
        if (a2.isEmpty()) {
            a2 = b(movieInfo, "-1");
        }
        if (a2.isEmpty()) {
            a2 = a(observableEmitter, movieInfo, "-1", "-1");
        }
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, movieInfo.session);
        if (a2.isEmpty()) {
            a2 = b(movieInfo, movieInfo.session);
        }
        if (a2.isEmpty()) {
            a2 = a(observableEmitter, movieInfo, movieInfo.session, movieInfo.eps);
        }
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, movieInfo.eps);
        }
    }

    private String a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str, String str2) {
        String str3;
        boolean z = movieInfo.getType().intValue() == 1;
        new DirectoryIndexHelper();
        String name = movieInfo.getName();
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        if (z) {
            str3 = String.valueOf(movieInfo.getYear());
        } else {
            str3 = "S" + com.original.tase.utils.Utils.a(Integer.parseInt(str)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(str2));
        }
        String a2 = HttpHelper.e().a(String.format(this.c + "/search/%s/feed/rss2/", new Object[]{com.original.tase.utils.Utils.a((name + " " + str3).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", " ").replace("  ", " "), new boolean[0])}), (Map<String, String>[]) new Map[]{hashMap});
        new ArrayList();
        Iterator it2 = Jsoup.a(a2, "", Parser.b()).g("item").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            Element h = element.h("title");
            if (h != null) {
                String G = h.G();
                if (!z) {
                    if (TitleHelper.a(G.toLowerCase(), "").equals(TitleHelper.a(movieInfo.name.toLowerCase() + movieInfo.sessionYear + " season" + movieInfo.session, ""))) {
                        String a3 = Regex.a(element.toString(), "<link>(http.*(?:\\.|\\/)(?:htm|html|))", 1);
                        if (!a3.isEmpty()) {
                            return a3;
                        }
                    } else {
                        continue;
                    }
                } else if (TitleHelper.f(name).equals(TitleHelper.f(G.replaceAll("(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)", "")))) {
                    String a4 = Regex.a(element.toString(), "<link>(http.*(?:\\.|\\/)(?:htm|html|))", 1);
                    if (!a4.isEmpty()) {
                        return a4;
                    }
                } else {
                    continue;
                }
            }
        }
        return "";
    }

    private String b(MovieInfo movieInfo, String str) {
        String str2;
        boolean z = movieInfo.getType().intValue() == 1;
        String replace = movieInfo.getName().replace("Marvel's ", "").replace("DC's ", "");
        if (z && replace.equals("Furious 7")) {
            replace = "Fast and Furious 7";
        }
        String str3 = replace;
        StringBuilder sb = new StringBuilder();
        sb.append(com.original.tase.utils.Utils.a(str3, new boolean[0]).replace("%20", "+"));
        if (z) {
            str2 = "+" + movieInfo.getYear();
        } else {
            str2 = "";
        }
        sb.append(str2);
        String sb2 = sb.toString();
        Iterator it2 = Jsoup.b(HttpHelper.e().b(String.format(this.c + "/?s=%s", new Object[]{sb2}), this.c)).g("a[href][rel=\"bookmark\"]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            try {
                String b = element.b("href");
                String G = element.G();
                if (!b.isEmpty() && !G.isEmpty()) {
                    if (b.startsWith("//")) {
                        b = "https:" + b;
                    } else if (b.startsWith("/")) {
                        b = this.c + b;
                    }
                    String a2 = Regex.a(G, "(.*?)\\s+(\\d{4})\\s*(?:$|:)", 1);
                    String a3 = Regex.a(G, "(.*?)\\s+(\\d{4})\\s*(?:$|:)", 2);
                    String a4 = Regex.a(G, "\\s+\\d{4}\\s*:\\s*(?:S|s)eason\\s*(\\d+)$", 1);
                    boolean z2 = !a4.isEmpty();
                    if ((!z || !z2) && (z || z2)) {
                        boolean z3 = z2 && !a4.isEmpty() && Integer.parseInt(a4) == Integer.parseInt(str);
                        if ((z || z3) && TitleHelper.f(str3).equals(TitleHelper.f(a2))) {
                            if (!z || a3.trim().isEmpty() || !com.original.tase.utils.Utils.b(a3.trim()) || movieInfo.getYear().intValue() <= 0 || Integer.parseInt(a3.trim()) == movieInfo.getYear().intValue()) {
                                if (b.startsWith("//")) {
                                    b = "https:" + b;
                                }
                                if (b.startsWith("/")) {
                                    return this.c + b;
                                } else if (b.startsWith(UriUtil.HTTP_SCHEME)) {
                                    return b;
                                } else {
                                    return this.c + "/" + b;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Logger.a((Throwable) e, new boolean[0]);
            }
        }
        return "";
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r12, java.lang.String r13, com.movie.data.model.MovieInfo r14, java.lang.String r15) {
        /*
            r11 = this;
            java.lang.Integer r14 = r14.getType()
            int r14 = r14.intValue()
            r0 = 0
            r1 = 1
            if (r14 != r1) goto L_0x000e
            r14 = 1
            goto L_0x000f
        L_0x000e:
            r14 = 0
        L_0x000f:
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r3 = r11.c
            r2.a((java.lang.String) r13, (java.lang.String) r3)
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r3 = r11.c
            java.lang.String r2 = r2.b((java.lang.String) r13, (java.lang.String) r3)
            org.jsoup.nodes.Document r2 = org.jsoup.Jsoup.b(r2)
            if (r14 == 0) goto L_0x005a
            java.lang.String r3 = "span[itemprop=\"name\"]"
            org.jsoup.nodes.Element r3 = r2.h(r3)
            if (r3 == 0) goto L_0x005a
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0054 }
            java.lang.String r4 = ".+?\\d{4}(.+)"
            java.lang.String r3 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r4, (int) r1)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x0054 }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x0054 }
            java.lang.String r4 = "cam"
            boolean r4 = r3.contains(r4)     // Catch:{ Exception -> 0x0054 }
            if (r4 != 0) goto L_0x0052
            java.lang.String r4 = "ts"
            boolean r3 = r3.contains(r4)     // Catch:{ Exception -> 0x0054 }
            if (r3 == 0) goto L_0x005a
        L_0x0052:
            r3 = 1
            goto L_0x005b
        L_0x0054:
            r3 = move-exception
            boolean[] r4 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r3, (boolean[]) r4)
        L_0x005a:
            r3 = 0
        L_0x005b:
            java.lang.String r4 = "a.orange.abutton[href]"
            org.jsoup.select.Elements r2 = r2.g((java.lang.String) r4)
            java.util.Iterator r2 = r2.iterator()
        L_0x0065:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x011b
            java.lang.Object r4 = r2.next()
            org.jsoup.nodes.Element r4 = (org.jsoup.nodes.Element) r4
            if (r14 != 0) goto L_0x0099
            java.lang.String r5 = r4.toString()     // Catch:{ Exception -> 0x0093 }
            java.lang.String r5 = r5.trim()     // Catch:{ Exception -> 0x0093 }
            java.lang.String r6 = ">\\s*(?:Episode|EPISODE)?\\s*(\\d+)\\s*<"
            r7 = 2
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r6, (int) r1, (int) r7)     // Catch:{ Exception -> 0x0093 }
            boolean r6 = r5.isEmpty()     // Catch:{ Exception -> 0x0093 }
            if (r6 != 0) goto L_0x0065
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x0093 }
            int r6 = java.lang.Integer.parseInt(r15)     // Catch:{ Exception -> 0x0093 }
            if (r5 == r6) goto L_0x0099
            goto L_0x0065
        L_0x0093:
            r5 = move-exception
            boolean[] r6 = new boolean[r0]
            com.original.tase.Logger.a((java.lang.Throwable) r5, (boolean[]) r6)
        L_0x0099:
            java.lang.String r5 = "href"
            java.lang.String r4 = r4.b((java.lang.String) r5)
            java.lang.String r5 = "//"
            boolean r5 = r4.startsWith(r5)
            if (r5 == 0) goto L_0x00b9
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "https:"
            r5.append(r6)
            r5.append(r4)
            java.lang.String r4 = r5.toString()
            goto L_0x00d2
        L_0x00b9:
            java.lang.String r5 = "/"
            boolean r5 = r4.startsWith(r5)
            if (r5 == 0) goto L_0x00d2
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = r11.c
            r5.append(r6)
            r5.append(r4)
            java.lang.String r4 = r5.toString()
        L_0x00d2:
            r8 = r4
            java.lang.String r4 = ".imdb.com/"
            boolean r4 = r8.contains(r4)
            if (r4 != 0) goto L_0x0065
            java.lang.String r4 = "//imdb.com/"
            boolean r4 = r8.contains(r4)
            if (r4 != 0) goto L_0x0065
            java.lang.String r4 = ""
            java.lang.String r5 = "\\?source=v\\d+"
            java.lang.String r5 = r8.replaceAll(r5, r4)
            java.lang.String r6 = "\\&source=v\\d+"
            java.lang.String r5 = r5.replaceAll(r6, r4)
            java.lang.String r6 = "\\?source=a\\d+"
            java.lang.String r5 = r5.replaceAll(r6, r4)
            java.lang.String r6 = "\\&source=a\\d+"
            java.lang.String r4 = r5.replaceAll(r6, r4)
            java.lang.String r5 = "&"
            boolean r6 = r4.contains(r5)
            if (r6 == 0) goto L_0x0111
            java.lang.String r6 = "?"
            boolean r7 = r4.contains(r6)
            if (r7 != 0) goto L_0x0111
            java.lang.String r4 = r4.replaceFirst(r5, r6)
        L_0x0111:
            r9 = r4
            r5 = r11
            r6 = r12
            r7 = r13
            r10 = r3
            r5.a(r6, r7, r8, r9, r10)
            goto L_0x0065
        L_0x011b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Pubfilm.a(io.reactivex.ObservableEmitter, java.lang.String, com.movie.data.model.MovieInfo, java.lang.String):void");
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, String str2, String str3, boolean z) {
        String str4;
        String a2 = Regex.a(HttpHelper.e().b(str2, str), "(;eval\\(.+\\);)", 1);
        if (!a2.isEmpty()) {
            String j = j(a2);
            if (!j.isEmpty()) {
                String a3 = Regex.a(j, "=['\"]([^'\"]+[^'\"])['\"]", 1);
                if (!a3.isEmpty()) {
                    String a4 = HttpHelper.e().a((str2 + String.format("&ref=fmoviesto.to&ref2=vidcloud.net&ref3=fmovies.ru.com&token=%s", new Object[]{a3})).replace("get.php", "apikey_v3.php"), (Map<String, String>[]) new Map[0]);
                    if (!a4.isEmpty()) {
                        String j2 = j(a4);
                        if (!j2.isEmpty()) {
                            Iterator it2 = Regex.b(j2, "['\"]source['\"]\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1, true).get(0).iterator();
                            while (it2.hasNext()) {
                                String replace = ((String) it2.next()).replace("\\/", "/");
                                if (replace.startsWith("//")) {
                                    replace = "https:" + replace;
                                }
                                if (replace.contains("vidcloud9.shop")) {
                                    String a5 = Regex.a(replace, "id=(\\w+)", 1);
                                    String format = String.format(BaseProvider.i(replace) + "/hls/%s/%s.playlist.m3u8", new Object[]{a5, a5});
                                    if (z) {
                                        str4 = a() + " (CAM)";
                                    } else {
                                        str4 = a();
                                    }
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("User-Agent", Constants.f5838a);
                                    hashMap.put("accept", "*/*");
                                    hashMap.put("referer", str);
                                    hashMap.put("Origin", BaseProvider.i(format));
                                    MediaSource mediaSource = new MediaSource(str4, "CDN-FastServer", false);
                                    mediaSource.setStreamLink(format);
                                    mediaSource.setPlayHeader(hashMap);
                                    mediaSource.setQuality("HD");
                                    observableEmitter.onNext(mediaSource);
                                } else {
                                    a(observableEmitter, replace, "HD", z);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:76:0x032c A[SYNTHETIC, Splitter:B:76:0x032c] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0346  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(com.movie.data.model.MovieInfo r22, java.lang.String r23) {
        /*
            r21 = this;
            r1 = r23
            java.lang.String r2 = "-"
            java.lang.Integer r0 = r22.getType()
            int r0 = r0.intValue()
            r3 = 1
            r4 = 0
            if (r0 != r3) goto L_0x0012
            r5 = 1
            goto L_0x0013
        L_0x0012:
            r5 = 0
        L_0x0013:
            java.lang.String r0 = r22.getName()
            java.lang.String r6 = ""
            java.lang.String r7 = "Marvel's "
            java.lang.String r0 = r0.replace(r7, r6)
            java.lang.String r7 = "DC's "
            java.lang.String r0 = r0.replace(r7, r6)
            if (r5 == 0) goto L_0x0030
            java.lang.Integer r7 = r22.getYear()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            goto L_0x0041
        L_0x0030:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Season "
            r7.append(r8)
            r7.append(r1)
            java.lang.String r7 = r7.toString()
        L_0x0041:
            r8 = r7
            r7 = r21
            java.lang.String r9 = r7.c
            java.lang.String r10 = "https://"
            java.lang.String r9 = r9.replace(r10, r10)
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "https://google.ch/search?q="
            r12.append(r13)
            boolean[] r13 = new boolean[r4]
            java.lang.String r13 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r13)
            java.lang.String r14 = "%20"
            java.lang.String r15 = "+"
            java.lang.String r13 = r13.replace(r14, r15)
            r12.append(r13)
            r12.append(r15)
            boolean[] r13 = new boolean[r4]
            java.lang.String r13 = com.original.tase.utils.Utils.a((java.lang.String) r8, (boolean[]) r13)
            java.lang.String r13 = r13.replace(r14, r15)
            r12.append(r13)
            java.lang.String r13 = "+site:"
            r12.append(r13)
            r12.append(r9)
            java.lang.String r12 = r12.toString()
            java.lang.String r3 = "https://google.ch"
            java.lang.String r3 = r11.b((java.lang.String) r12, (java.lang.String) r3)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "kl=us-en&b=&q="
            r11.append(r12)
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            r12.append(r0)
            java.lang.String r4 = " "
            r12.append(r4)
            r12.append(r8)
            java.lang.String r7 = " site:"
            r12.append(r7)
            java.lang.String r7 = r9.replace(r10, r6)
            java.lang.String r1 = "http://"
            java.lang.String r7 = r7.replace(r1, r6)
            r12.append(r7)
            java.lang.String r7 = r12.toString()
            r16 = r2
            r12 = 0
            boolean[] r2 = new boolean[r12]
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r7, (boolean[]) r2)
            r11.append(r2)
            r11.toString()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r7 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
            java.lang.String r11 = "Accept"
            r2.put(r11, r7)
            java.lang.String r12 = "en-US"
            r17 = r5
            java.lang.String r5 = "Accept-Language"
            r2.put(r5, r12)
            r18 = r3
            java.lang.String r3 = "Origin"
            r19 = r5
            java.lang.String r5 = "https://duckduckgo.com"
            r2.put(r3, r5)
            java.lang.String r3 = "Referer"
            java.lang.String r5 = "https://duckduckgo.com/"
            r2.put(r3, r5)
            java.lang.String r3 = "Upgrade-Insecure-Requests"
            java.lang.String r5 = "1"
            r2.put(r3, r5)
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "https://www.bing.com/search?q="
            r3.append(r5)
            r20 = r12
            r5 = 0
            boolean[] r12 = new boolean[r5]
            java.lang.String r12 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r12)
            java.lang.String r12 = r12.replace(r14, r15)
            r3.append(r12)
            r3.append(r15)
            boolean[] r12 = new boolean[r5]
            java.lang.String r5 = com.original.tase.utils.Utils.a((java.lang.String) r8, (boolean[]) r12)
            java.lang.String r5 = r5.replace(r14, r15)
            r3.append(r5)
            r3.append(r13)
            r3.append(r9)
            java.lang.String r3 = r3.toString()
            java.lang.String r5 = "https://www.bing.com"
            java.lang.String r2 = r2.b((java.lang.String) r3, (java.lang.String) r5)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "cmd=process_search&language=english&enginecount=1&pl=&abp=1&hmb=1&ff=&theme=&flag_ac=0&cat=web&ycc=0&t=air&nj=0&query="
            r3.append(r5)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r0)
            r5.append(r4)
            r5.append(r8)
            r5.append(r4)
            java.lang.String r4 = r9.replace(r10, r6)
            java.lang.String r4 = r4.replace(r1, r6)
            r5.append(r4)
            java.lang.String r4 = r5.toString()
            r5 = 0
            boolean[] r8 = new boolean[r5]
            java.lang.String r4 = com.original.tase.utils.Utils.a((java.lang.String) r4, (boolean[]) r8)
            r3.append(r4)
            r3.toString()
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r3.put(r11, r7)
            r5 = r19
            r4 = r20
            r3.put(r5, r4)
            java.lang.String r4 = "Host"
            java.lang.String r5 = "www.startpage.com"
            r3.put(r4, r5)
            java.lang.String r4 = "Origin"
            java.lang.String r5 = "https://www.startpage.com"
            r3.put(r4, r5)
            java.lang.String r4 = "Referer"
            java.lang.String r5 = "https://www.startpage.com/do/asearch"
            r3.put(r4, r5)
            java.lang.String r4 = "Upgrade-Insecure-Requests"
            java.lang.String r5 = "1"
            r3.put(r4, r5)
            java.lang.String r4 = "User-Agent"
            java.lang.String r5 = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36"
            r3.put(r4, r5)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r4 = r18
            r3.add(r4)
            java.lang.String r4 = "(</?\\w{1,7}>)"
            java.lang.String r2 = r2.replaceAll(r4, r6)
            r3.add(r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "(?:/\\w+-|/)("
            r2.append(r4)
            java.lang.String r4 = "$"
            java.lang.String r5 = "\\$"
            java.lang.String r0 = r0.replace(r4, r5)
            java.lang.String r4 = "*"
            java.lang.String r5 = "\\*"
            java.lang.String r0 = r0.replace(r4, r5)
            java.lang.String r4 = "("
            java.lang.String r5 = "\\("
            java.lang.String r0 = r0.replace(r4, r5)
            java.lang.String r4 = ")"
            java.lang.String r5 = "\\)"
            java.lang.String r0 = r0.replace(r4, r5)
            java.lang.String r4 = "["
            java.lang.String r5 = "\\["
            java.lang.String r0 = r0.replace(r4, r5)
            java.lang.String r4 = "]"
            java.lang.String r5 = "\\]"
            java.lang.String r0 = r0.replace(r4, r5)
            java.lang.String r0 = com.original.tase.helper.TitleHelper.g(r0)
            r2.append(r0)
            java.lang.String r0 = ")"
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            java.util.Iterator r3 = r3.iterator()
        L_0x0202:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0370
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r4 = "href=['\"](.+?)['\"]"
            r5 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r4, (int) r5, (boolean) r5)
            r4 = 0
            java.lang.Object r0 = r0.get(r4)
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            java.util.Iterator r4 = r0.iterator()
        L_0x0220:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x036a
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r5 = "http"
            boolean r5 = r0.startsWith(r5)     // Catch:{ Exception -> 0x0358 }
            if (r5 == 0) goto L_0x0350
            java.lang.String r5 = "pubfilm"
            boolean r5 = r0.contains(r5)     // Catch:{ Exception -> 0x0358 }
            if (r5 == 0) goto L_0x0350
            java.lang.String r5 = "//translate."
            boolean r5 = r0.contains(r5)     // Catch:{ Exception -> 0x0358 }
            if (r5 != 0) goto L_0x0350
            java.lang.String r5 = "startpage.com"
            boolean r5 = r0.contains(r5)     // Catch:{ Exception -> 0x0358 }
            if (r5 != 0) goto L_0x0350
            java.lang.String r5 = r0.replace(r10, r1)     // Catch:{ Exception -> 0x0358 }
            boolean r5 = r5.contains(r9)     // Catch:{ Exception -> 0x0358 }
            if (r5 == 0) goto L_0x0350
            java.lang.String r5 = "-season-"
            if (r17 == 0) goto L_0x02e3
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x02d9 }
            boolean r5 = r7.contains(r5)     // Catch:{ Exception -> 0x02d9 }
            r7 = 1
            r5 = r5 ^ r7
            if (r5 == 0) goto L_0x02af
            java.lang.String r5 = r0.toLowerCase()     // Catch:{ Exception -> 0x02d9 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d9 }
            r7.<init>()     // Catch:{ Exception -> 0x02d9 }
            r8 = r16
            r7.append(r8)     // Catch:{ Exception -> 0x02d1 }
            java.lang.Integer r11 = r22.getYear()     // Catch:{ Exception -> 0x02d1 }
            r7.append(r11)     // Catch:{ Exception -> 0x02d1 }
            r7.append(r8)     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x02d1 }
            boolean r5 = r5.contains(r7)     // Catch:{ Exception -> 0x02d1 }
            if (r5 != 0) goto L_0x02ad
            java.lang.String r5 = r0.toLowerCase()     // Catch:{ Exception -> 0x02d1 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02d1 }
            r7.<init>()     // Catch:{ Exception -> 0x02d1 }
            r7.append(r8)     // Catch:{ Exception -> 0x02d1 }
            java.lang.Integer r11 = r22.getYear()     // Catch:{ Exception -> 0x02d1 }
            r7.append(r11)     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r11 = ".htm"
            r7.append(r11)     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x02d1 }
            boolean r5 = r5.contains(r7)     // Catch:{ Exception -> 0x02d1 }
            if (r5 == 0) goto L_0x02ab
            goto L_0x02ad
        L_0x02ab:
            r5 = 0
            goto L_0x02b1
        L_0x02ad:
            r5 = 1
            goto L_0x02b1
        L_0x02af:
            r8 = r16
        L_0x02b1:
            if (r5 == 0) goto L_0x02d3
            java.lang.String r5 = r0.trim()     // Catch:{ Exception -> 0x02d1 }
            java.lang.String r5 = r5.toLowerCase()     // Catch:{ Exception -> 0x02d1 }
            r7 = 2
            r11 = 1
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r2, (int) r11, (int) r7)     // Catch:{ Exception -> 0x02cd }
            boolean r5 = r5.isEmpty()     // Catch:{ Exception -> 0x02d1 }
            if (r5 == 0) goto L_0x02ca
            r12 = r1
            r5 = 0
            goto L_0x02d4
        L_0x02ca:
            r12 = r1
            r5 = 1
            goto L_0x02d4
        L_0x02cd:
            r0 = move-exception
            r12 = r1
            r5 = 0
            goto L_0x02df
        L_0x02d1:
            r0 = move-exception
            goto L_0x02dc
        L_0x02d3:
            r12 = r1
        L_0x02d4:
            r11 = 1
            r1 = r23
            goto L_0x0347
        L_0x02d9:
            r0 = move-exception
            r8 = r16
        L_0x02dc:
            r12 = r1
            r5 = 0
            r11 = 1
        L_0x02df:
            r1 = r23
            goto L_0x0360
        L_0x02e3:
            r8 = r16
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x034d }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x034d }
            r11.<init>()     // Catch:{ Exception -> 0x034d }
            r11.append(r5)     // Catch:{ Exception -> 0x034d }
            r12 = r1
            r1 = r23
            r11.append(r1)     // Catch:{ Exception -> 0x034a }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x034a }
            boolean r7 = r7.contains(r11)     // Catch:{ Exception -> 0x034a }
            if (r7 != 0) goto L_0x0329
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x0325 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0325 }
            r11.<init>()     // Catch:{ Exception -> 0x0325 }
            r11.append(r5)     // Catch:{ Exception -> 0x0325 }
            int r5 = java.lang.Integer.parseInt(r23)     // Catch:{ Exception -> 0x0325 }
            java.lang.String r5 = com.original.tase.utils.Utils.a((int) r5)     // Catch:{ Exception -> 0x0325 }
            r11.append(r5)     // Catch:{ Exception -> 0x0325 }
            java.lang.String r5 = r11.toString()     // Catch:{ Exception -> 0x0325 }
            boolean r5 = r7.contains(r5)     // Catch:{ Exception -> 0x0325 }
            if (r5 == 0) goto L_0x0323
            goto L_0x0329
        L_0x0323:
            r5 = 0
            goto L_0x032a
        L_0x0325:
            r0 = move-exception
            r5 = 0
            r11 = 1
            goto L_0x0360
        L_0x0329:
            r5 = 1
        L_0x032a:
            if (r5 == 0) goto L_0x0346
            java.lang.String r5 = r0.trim()     // Catch:{ Exception -> 0x034a }
            java.lang.String r5 = r5.toLowerCase()     // Catch:{ Exception -> 0x034a }
            r7 = 2
            r11 = 1
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r2, (int) r11, (int) r7)     // Catch:{ Exception -> 0x0344 }
            boolean r5 = r5.isEmpty()     // Catch:{ Exception -> 0x0344 }
            if (r5 == 0) goto L_0x0342
            r5 = 0
            goto L_0x0347
        L_0x0342:
            r5 = 1
            goto L_0x0347
        L_0x0344:
            r0 = move-exception
            goto L_0x035f
        L_0x0346:
            r11 = 1
        L_0x0347:
            if (r5 == 0) goto L_0x0356
            return r0
        L_0x034a:
            r0 = move-exception
            r11 = 1
            goto L_0x035f
        L_0x034d:
            r0 = move-exception
            r12 = r1
            goto L_0x035c
        L_0x0350:
            r12 = r1
            r8 = r16
            r11 = 1
            r1 = r23
        L_0x0356:
            r5 = 0
            goto L_0x0365
        L_0x0358:
            r0 = move-exception
            r12 = r1
            r8 = r16
        L_0x035c:
            r11 = 1
            r1 = r23
        L_0x035f:
            r5 = 0
        L_0x0360:
            boolean[] r7 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r7)
        L_0x0365:
            r16 = r8
            r1 = r12
            goto L_0x0220
        L_0x036a:
            r12 = r1
            r1 = r23
            r1 = r12
            goto L_0x0202
        L_0x0370:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Pubfilm.a(com.movie.data.model.MovieInfo, java.lang.String):java.lang.String");
    }
}
