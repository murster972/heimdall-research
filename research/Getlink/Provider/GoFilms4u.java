package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class GoFilms4u extends BaseProvider {
    private String c = Utils.getProvider(82);
    private String d = "";

    private String b(MovieInfo movieInfo) {
        String format = String.format(BaseProvider.e(HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]), this.c), new Object[]{com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+").toLowerCase()});
        HashMap<String, String> a2 = Constants.a();
        a2.put("origin", this.c);
        a2.put("referer", format);
        HttpHelper e = HttpHelper.e();
        e.a(this.c + "/wp-admin/admin-ajax.php", "action=cid", (Map<String, String>[]) new Map[]{a2});
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.c + "/");
        String a3 = HttpHelper.e().a(format, (Map<String, String>[]) new Map[]{hashMap});
        this.d = this.c + "/";
        Iterator it2 = Jsoup.b(a3).g("div[class=entry-header]").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("h2[class=entry-title]");
            String b = h.h("a").b("href");
            String lowerCase = h.h("a").G().toLowerCase();
            if (lowerCase.startsWith(movieInfo.name.toLowerCase() + " (" + movieInfo.year + ")")) {
                return b;
            }
        }
        return "";
    }

    public String a() {
        return "GoFilms4u";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.d);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("ol[id=player-tabs]").b("a[data-href]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("data-href");
            String b2 = element.b("data-target");
            if (!b.isEmpty() && !b2.contains("trailer")) {
                a(observableEmitter, b, "HQ", false);
            }
        }
    }
}
