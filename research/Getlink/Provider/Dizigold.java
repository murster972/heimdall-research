package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import com.uwetrottmann.trakt5.TraktV2;
import io.reactivex.ObservableEmitter;
import okhttp3.internal.cache.DiskLruCache;

public class Dizigold extends BaseProvider {
    private static final String[] d = {DiskLruCache.VERSION_1, TraktV2.API_VERSION, "3"};
    private static final String[] e = {"tr", "or", "en"};
    private String c = Utils.getProvider(96);

    public String a() {
        return "Dizigold";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01f5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.movie.data.model.MovieInfo r27, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r28) {
        /*
            r26 = this;
            r1 = r26
            r2 = r28
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = r1.c
            r0.append(r3)
            java.lang.String r3 = "/"
            r0.append(r3)
            java.lang.String r4 = r27.getName()
            java.lang.String r4 = r4.toLowerCase()
            java.lang.String r5 = "Marvel's "
            java.lang.String r6 = ""
            java.lang.String r4 = r4.replace(r5, r6)
            java.lang.String r4 = com.original.tase.helper.TitleHelper.e(r4)
            java.lang.String r5 = "-"
            java.lang.String r6 = "'"
            java.lang.String r4 = r4.replace(r6, r5)
            java.lang.String r6 = "P.D."
            java.lang.String r7 = "PD"
            java.lang.String r4 = r4.replace(r6, r7)
            java.lang.String r4 = com.original.tase.helper.TitleHelper.a(r4, r5)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r0)
            r4.append(r3)
            java.lang.Integer r3 = r27.getSession()
            r4.append(r3)
            java.lang.String r3 = "-sezon/"
            r4.append(r3)
            java.lang.Integer r3 = r27.getEps()
            r4.append(r3)
            java.lang.String r3 = "-bolum"
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            r5 = 0
            java.util.Map[] r6 = new java.util.Map[r5]
            java.lang.String r4 = r4.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r6)
            r6 = 1
            java.lang.String r7 = ">\\s*(\\d{4})\\s*(?:–|)\\s*<"
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r4, (java.lang.String) r7, (int) r6)
            boolean r7 = r4.isEmpty()
            if (r7 != 0) goto L_0x029e
            java.lang.Integer r7 = r27.getYear()
            int r7 = r7.intValue()
            if (r7 <= 0) goto L_0x009b
            int r4 = java.lang.Integer.parseInt(r4)
            java.lang.Integer r7 = r27.getYear()
            int r7 = r7.intValue()
            if (r4 == r7) goto L_0x009b
            goto L_0x029e
        L_0x009b:
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r4.b((java.lang.String) r3, (java.lang.String) r0)
            java.lang.String r4 = "var\\s+view_id\\s*=\\s*\"(\\d*)\""
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r4, (int) r6)
            boolean r0 = r4.isEmpty()
            if (r0 == 0) goto L_0x00b0
            return
        L_0x00b0:
            java.lang.String[] r7 = e
            int r8 = r7.length
            r9 = 0
        L_0x00b4:
            if (r9 >= r8) goto L_0x029e
            r10 = r7[r9]
            java.lang.String[] r11 = d
            int r12 = r11.length
            r13 = 0
        L_0x00bc:
            if (r13 >= r12) goto L_0x028d
            r0 = r11[r13]
            boolean r14 = r28.isDisposed()
            if (r14 == 0) goto L_0x00c7
            return
        L_0x00c7:
            com.original.tase.helper.http.HttpHelper r14 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r6 = "https://player.dizigold1.net/?id="
            r15.append(r6)
            r15.append(r4)
            java.lang.String r6 = "&s="
            r15.append(r6)
            r15.append(r0)
            java.lang.String r0 = "&dil="
            r15.append(r0)
            r15.append(r10)
            java.lang.String r0 = "&ref=golden-bird"
            r15.append(r0)
            java.lang.String r0 = r15.toString()
            java.lang.String r0 = r14.b((java.lang.String) r0, (java.lang.String) r3)
            boolean r6 = r0.isEmpty()
            if (r6 != 0) goto L_0x026e
            org.jsoup.nodes.Document r6 = org.jsoup.Jsoup.b(r0)
            java.lang.String r14 = "iframe[src]"
            org.jsoup.select.Elements r6 = r6.g((java.lang.String) r14)
            java.util.Iterator r6 = r6.iterator()
        L_0x0109:
            boolean r14 = r6.hasNext()
            java.lang.String r15 = "HD"
            if (r14 == 0) goto L_0x0139
            java.lang.Object r14 = r6.next()
            org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
            java.lang.String r5 = "src"
            java.lang.String r5 = r14.b((java.lang.String) r5)
            java.lang.String r14 = "or"
            boolean r14 = r10.equals(r14)
            if (r14 == 0) goto L_0x0137
            java.lang.String r14 = "adserver"
            boolean r14 = r5.contains(r14)
            if (r14 != 0) goto L_0x0137
            r16 = r3
            r14 = 0
            boolean[] r3 = new boolean[r14]
            r1.a(r2, r5, r15, r3)
            r3 = r16
        L_0x0137:
            r5 = 0
            goto L_0x0109
        L_0x0139:
            r16 = r3
            java.lang.String r3 = "tr"
            boolean r3 = r10.equals(r3)
            if (r3 == 0) goto L_0x014e
            java.lang.String r3 = "['\"]?kind['\"]?\\s*:\\s*['\"]?(captions|subtitles)['\"]?"
            r5 = 1
            java.lang.String r3 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r3, (int) r5, (boolean) r5)
            r3.isEmpty()
            goto L_0x014f
        L_0x014e:
            r5 = 1
        L_0x014f:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.lang.String r6 = "['\"]?sources[\"']?\\s*:\\s*\\[(.*?)\\}\\s*,?\\s*\\]"
            java.util.ArrayList r6 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r6, (int) r5, (boolean) r5)
            r5 = 0
            java.lang.Object r6 = r6.get(r5)
            java.util.ArrayList r6 = (java.util.ArrayList) r6
            java.util.Iterator r5 = r6.iterator()
        L_0x0165:
            boolean r6 = r5.hasNext()
            java.lang.String r14 = "GoogleVideo"
            java.lang.String r17 = "CDN"
            if (r6 == 0) goto L_0x0215
            java.lang.Object r6 = r5.next()
            java.lang.String r6 = (java.lang.String) r6
            r27 = r4
            r4 = 2
            r18 = r5
            java.lang.String r5 = "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*['\"]?label['\"]?\\s*:\\s*['\"]?(\\d{3,4})p"
            r19 = r7
            r7 = 1
            java.util.ArrayList r4 = com.original.tase.utils.Regex.b((java.lang.String) r6, (java.lang.String) r5, (int) r4, (boolean) r7)
            r5 = 0
            java.lang.Object r6 = r4.get(r5)
            java.util.ArrayList r6 = (java.util.ArrayList) r6
            java.lang.Object r4 = r4.get(r7)
            java.util.ArrayList r4 = (java.util.ArrayList) r4
            r5 = 0
        L_0x0191:
            int r7 = r6.size()
            if (r5 >= r7) goto L_0x020d
            java.lang.Object r7 = r6.get(r5)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r20 = r4.get(r5)     // Catch:{ Exception -> 0x01a9 }
            java.lang.String r20 = (java.lang.String) r20     // Catch:{ Exception -> 0x01a9 }
            boolean r20 = r20.isEmpty()     // Catch:{ Exception -> 0x01a9 }
            if (r20 == 0) goto L_0x01ae
        L_0x01a9:
            r22 = r4
            r20 = r6
            goto L_0x01ce
        L_0x01ae:
            r20 = r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cc }
            r6.<init>()     // Catch:{ Exception -> 0x01cc }
            java.lang.Object r21 = r4.get(r5)     // Catch:{ Exception -> 0x01cc }
            r22 = r4
            r4 = r21
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x01ce }
            r6.append(r4)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = "p"
            r6.append(r4)     // Catch:{ Exception -> 0x01ce }
            java.lang.String r4 = r6.toString()     // Catch:{ Exception -> 0x01ce }
            goto L_0x01cf
        L_0x01cc:
            r22 = r4
        L_0x01ce:
            r4 = r15
        L_0x01cf:
            boolean r6 = com.original.tase.helper.GoogleVideoHelper.k(r7)
            r21 = r4
            com.original.tase.model.media.MediaSource r4 = new com.original.tase.model.media.MediaSource
            r23 = r8
            java.lang.String r8 = r26.a()
            r24 = r10
            r25 = r11
            if (r6 == 0) goto L_0x01e5
            r10 = r14
            goto L_0x01e7
        L_0x01e5:
            r10 = r17
        L_0x01e7:
            r11 = 0
            r4.<init>(r8, r10, r11)
            r4.setStreamLink(r7)
            if (r6 == 0) goto L_0x01f5
            java.lang.String r6 = com.original.tase.helper.GoogleVideoHelper.h(r7)
            goto L_0x01f7
        L_0x01f5:
            r6 = r21
        L_0x01f7:
            r4.setQuality((java.lang.String) r6)
            r2.onNext(r4)
            r3.add(r7)
            int r5 = r5 + 1
            r6 = r20
            r4 = r22
            r8 = r23
            r10 = r24
            r11 = r25
            goto L_0x0191
        L_0x020d:
            r4 = r27
            r5 = r18
            r7 = r19
            goto L_0x0165
        L_0x0215:
            r27 = r4
            r19 = r7
            r23 = r8
            r24 = r10
            r25 = r11
            java.util.ArrayList r0 = r1.g(r0)
            java.util.Iterator r0 = r0.iterator()
        L_0x0227:
            boolean r4 = r0.hasNext()
            if (r4 == 0) goto L_0x025f
            java.lang.Object r4 = r0.next()
            java.lang.String r4 = (java.lang.String) r4
            boolean r5 = r3.contains(r4)
            if (r5 != 0) goto L_0x0227
            boolean r5 = com.original.tase.helper.GoogleVideoHelper.k(r4)
            com.original.tase.model.media.MediaSource r6 = new com.original.tase.model.media.MediaSource
            java.lang.String r7 = r26.a()
            if (r5 == 0) goto L_0x0247
            r8 = r14
            goto L_0x0249
        L_0x0247:
            r8 = r17
        L_0x0249:
            r10 = 0
            r6.<init>(r7, r8, r10)
            r6.setStreamLink(r4)
            if (r5 == 0) goto L_0x0257
            java.lang.String r4 = com.original.tase.helper.GoogleVideoHelper.h(r4)
            goto L_0x0258
        L_0x0257:
            r4 = r15
        L_0x0258:
            r6.setQuality((java.lang.String) r4)
            r2.onNext(r6)
            goto L_0x0227
        L_0x025f:
            r3 = 10000(0x2710, double:4.9407E-320)
            java.lang.Thread.sleep(r3)     // Catch:{ all -> 0x0265 }
            goto L_0x027a
        L_0x0265:
            r0 = move-exception
            r3 = r0
            r4 = 0
            boolean[] r0 = new boolean[r4]
            com.original.tase.Logger.a((java.lang.Throwable) r3, (boolean[]) r0)
            goto L_0x027b
        L_0x026e:
            r16 = r3
            r27 = r4
            r19 = r7
            r23 = r8
            r24 = r10
            r25 = r11
        L_0x027a:
            r4 = 0
        L_0x027b:
            int r13 = r13 + 1
            r4 = r27
            r3 = r16
            r7 = r19
            r8 = r23
            r10 = r24
            r11 = r25
            r5 = 0
            r6 = 1
            goto L_0x00bc
        L_0x028d:
            r16 = r3
            r27 = r4
            r19 = r7
            r23 = r8
            r4 = 0
            int r9 = r9 + 1
            r4 = r27
            r5 = 0
            r6 = 1
            goto L_0x00b4
        L_0x029e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Dizigold.b(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }
}
