package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Hdmega extends BaseProvider {
    private String c = Utils.getProvider(83);
    private String d = "";
    private HashMap e = new HashMap();

    private String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        if (movieInfo.name.equals("Avengers: Infinity War")) {
            movieInfo.name = "Los Vengadores 3: La guerra del infinito";
        }
        String lowerCase = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+").toLowerCase();
        this.e.put("referer", this.c);
        this.e.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        this.e.put("user-Agent", Constants.f5838a);
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[]{this.e});
        this.d = this.c + "/?s=" + lowerCase;
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.d, (Map<String, String>[]) new Map[]{this.e})).g("div.col-xs-2").b("div.row").b("a[title][href]").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            String b2 = h.b("title");
            String a2 = Regex.a(b2, "\\((\\d+)\\)", 1);
            if (!z) {
                String lowerCase2 = b2.toLowerCase();
                if (lowerCase2.equals(movieInfo.name.toLowerCase() + ": ...") && a2.contains(movieInfo.sessionYear)) {
                    if (b.contains("season-" + movieInfo.getSession())) {
                        return b;
                    }
                }
            } else if (b2.toLowerCase().startsWith(movieInfo.name.toLowerCase()) && a2.equals(movieInfo.year)) {
                return b;
            }
        }
        return "";
    }

    public String a() {
        return "Hdmega";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        this.e.put("referer", this.d);
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{this.e});
        if (!(movieInfo.getType().intValue() == 1)) {
            Iterator it2 = Jsoup.b(a2).g("div[id=details]").b("a[class=episode episode_series_link]").iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Element element = (Element) it2.next();
                String b = element.h("a").b("href");
                if (element.h("a").G().equals(movieInfo.eps)) {
                    a2 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[]{this.e});
                    break;
                }
            }
        }
        Iterator it3 = Jsoup.b(a2).g("li.no-active").b("a[data-src]").iterator();
        while (it3.hasNext()) {
            String b2 = ((Element) it3.next()).b("data-src");
            if (!b2.isEmpty()) {
                try {
                    String trim = new String(Base64.decode(Regex.a(b2, "id=(\\w+)", 1), 10), "UTF-8").trim();
                    if (!trim.isEmpty()) {
                        Logger.a("http getlink: ", trim);
                        a(observableEmitter, trim, "HD", false);
                    }
                } catch (Throwable unused) {
                }
            }
        }
    }
}
