package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class WorldUS extends BaseProvider {
    private String c = Utils.getProvider(56);
    private HashMap d = new HashMap();
    private String e = "HD";

    private ArrayList<String> b(MovieInfo movieInfo) {
        this.d.put("User-Agent", Constants.f5838a);
        int intValue = movieInfo.getType().intValue();
        String str = this.c + "/search?query=" + com.original.tase.utils.Utils.a(movieInfo.getName().toLowerCase(), new boolean[0]).replace("+", "%20");
        this.d.put("referer", this.c + "/");
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{this.d});
        this.d.put("referer", str);
        Iterator it2 = Jsoup.b(a2).g("div#movie-featured").b("div.ml-item").iterator();
        ArrayList<String> arrayList = new ArrayList<>();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            String b2 = h.b("title");
            String element = h.h("span.mli-info").toString();
            if (b2.toLowerCase().startsWith(movieInfo.name.toLowerCase()) && element.contains(movieInfo.year)) {
                if (b.startsWith("/")) {
                    b = this.c + b;
                }
                if (!arrayList.contains(b)) {
                    arrayList.add(b);
                }
            }
        }
        return arrayList;
    }

    public String a() {
        return "WorldUS";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        ArrayList<String> b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, ArrayList<String> arrayList) {
        String str;
        Iterator<String> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            String b = Jsoup.b(HttpHelper.e().a(next, (Map<String, String>[]) new Map[]{this.d})).h("div.movies-list-wrap.mlw-topview.post-share").h("iframe").b("src");
            if (b.startsWith("/")) {
                b = this.c + b;
            }
            this.d.put("referer", next);
            String a2 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[]{this.d});
            try {
                str = Jsoup.b(a2).h("iframe").b("src");
            } catch (Throwable unused) {
                str = "";
            }
            if (str.isEmpty()) {
                Iterator it3 = Regex.b(a2, "file['\"]\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0).iterator();
                if (!it3.hasNext()) {
                    a2 = JsUnpacker.m30918(Regex.a(a2, "<script .*>(eval.*)", 1)).toString();
                    it3 = Regex.b(a2, "['\"]([^'\"]+(mka|mkv|mp4|avi))['\"]", 1, true).get(0).iterator();
                }
                while (it3.hasNext()) {
                    String str2 = (String) it3.next();
                    HashMap hashMap = new HashMap();
                    boolean k = GoogleVideoHelper.k(str2);
                    hashMap.put("User-Agent", Constants.f5838a);
                    MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                    mediaSource.setStreamLink(str2);
                    if (k) {
                        mediaSource.setPlayHeader(hashMap);
                    }
                    mediaSource.setQuality(k ? GoogleVideoHelper.h(str2) : this.e);
                    observableEmitter.onNext(mediaSource);
                }
                String a3 = Regex.a(a2, "code.*link:\\s*[\"'](http[^\"']+)", 1);
                if (!a3.isEmpty()) {
                    a(observableEmitter, a3, this.e, false);
                }
            } else if (str.contains("vidcloud.icu")) {
                Iterator it4 = d(str, next).iterator();
                while (it4.hasNext()) {
                    String obj = it4.next().toString();
                    boolean k2 = GoogleVideoHelper.k(obj);
                    MediaSource mediaSource2 = new MediaSource(a(), k2 ? "GoogleVideo" : "CDN-FastServer", c(this.e));
                    mediaSource2.setStreamLink(obj);
                    if (k2) {
                        mediaSource2.setPlayHeader(this.d);
                    }
                    mediaSource2.setQuality(k2 ? GoogleVideoHelper.h(obj) : this.e);
                    observableEmitter.onNext(mediaSource2);
                }
            } else {
                a(observableEmitter, str, this.e, false);
            }
        }
    }
}
