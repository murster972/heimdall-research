package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.movie.data.model.realdebrid.MagnetObject;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.vungle.warren.model.CookieDBAdapter;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Rarbg extends BaseProvider {
    public String c = Utils.getProvider(6);

    public String a() {
        return "KickassTorrents";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* JADX INFO: finally extract failed */
    public void c(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String str2;
        Iterator it2;
        DirectoryIndexHelper directoryIndexHelper;
        ArrayList arrayList;
        String str3;
        boolean z;
        DirectoryIndexHelper.ParsedLinkModel parsedLinkModel;
        int i;
        DirectoryIndexHelper directoryIndexHelper2;
        ArrayList arrayList2;
        Iterator it3;
        MovieInfo movieInfo2 = movieInfo;
        String str4 = "1080p";
        boolean z2 = movieInfo.getType().intValue() == 1;
        if (z2) {
            str = " " + movieInfo2.year;
        } else {
            str = " S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.eps));
        }
        String a2 = HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        String a3 = Regex.a(a2, "\\(['\"](\\w+)['\"]\\s*\\,\\s*['\"](\\w+)['\"]\\s*\\,\\s*[\\d+]\\)", 1);
        String a4 = Regex.a(a2, "\\(['\"](\\w+)['\"]\\s*\\,\\s*['\"](\\w+)['\"]\\s*\\,\\s*[\\d+]\\)", 2);
        if (a3.isEmpty()) {
            a3 = "gaDts48g";
            a4 = "q8h5pp9t";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.9,en;q=0.8");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        Duktape create = Duktape.create();
        try {
            Object evaluate = create.evaluate(String.format("var cookie=\"\";function mcpslar(t,e,n){if(n){var a=new Date;a.setTime(a.getTime()+24*n*60*60*1e3);var r=\"; expires=\"+a.toGMTString()}else r=\"\";cookie=\"\"==cookie?t+\"=\"+e+r+\"; path=/\":cookie+\"### \"+t+\"=\"+e+r+\"; path=/\"}function mBhJuWMW(t,e,n){var a=new Date;a.setTime(a.getTime()+1e3*n);var r=\"; expires=\"+a.toGMTString();cookie=cookie+\"### \"+t+\"=\"+e+r+\"; path=/\"}function a8nJXDwsf(t){for(var e=t+\"=\",n=cookie.split(\";\"),a=0;a<n.length;a++){for(var r=n[a];\" \"==r.charAt(0);)r=r.substring(1,r.length);if(0==r.indexOf(e))return r.substring(e.length,r.length)}return null}mcpslar(\"%s\",\"%s\",5);var gxsYTxpt=0,mtdwnVGt=a8nJXDwsf(\"expla\");if(mtdwnVGt?(mtdwnVGt=parseInt(mtdwnVGt))<4&&(1<=mtdwnVGt&&mtdwnVGt<=3&&(mBhJuWMW(\"expla\",mtdwnVGt+1,45),gxsYTxpt=1),gxsYTxpt=1):mBhJuWMW(\"expla\",gxsYTxpt=1,45),1==gxsYTxpt){var expla3=a8nJXDwsf(\"expla3\");1==expla3&&(gxsYTxpt=0)}function acb(){return cookie}1==gxsYTxpt&&(gxsYTxpt=0),1==gxsYTxpt&&mBhJuWMW(\"expla3\",1,3),mcpslar(\"aby\",\"1\",7),acb();", new Object[]{a3, a4}));
            if (evaluate != null) {
                for (String c2 : evaluate.toString().split("###")) {
                    HttpHelper.e().c(this.c, c2);
                }
            }
            create.close();
            HttpHelper.e().c(this.c, "tcc=;");
            String a5 = com.original.tase.utils.Utils.a(movieInfo2.name + str, new boolean[0]);
            hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, HttpHelper.e().a(this.c));
            String str5 = this.c + "/torrents.php?search=" + a5;
            String a6 = HttpHelper.e().a(str5, (Map<String, String>[]) new Map[]{hashMap});
            if (a6.contains("Please wait while we try to verify your browser")) {
                HttpHelper.e().c(this.c, Regex.a(a6, "var\\s*name\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1) + "=" + Regex.a(a6, "var\\s*value_sk\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1));
                a6 = HttpHelper.e().a(str5, (Map<String, String>[]) new Map[]{hashMap});
            }
            Iterator it4 = Jsoup.b(a6).g("tbody").b("a[onmouseover][title]").iterator();
            DirectoryIndexHelper directoryIndexHelper3 = new DirectoryIndexHelper();
            ArrayList arrayList3 = new ArrayList();
            HashMap hashMap2 = new HashMap();
            while (it4.hasNext()) {
                Element element = (Element) it4.next();
                String b = element.b("href");
                String G = element.G();
                if (G.isEmpty()) {
                    G = element.b("title");
                }
                String str6 = G;
                if (!str6.isEmpty()) {
                    if (z2) {
                        it3 = it4;
                        String a7 = TitleHelper.a(str6.toLowerCase(), "");
                        arrayList2 = arrayList3;
                        StringBuilder sb = new StringBuilder();
                        directoryIndexHelper2 = directoryIndexHelper3;
                        sb.append(movieInfo2.name.toLowerCase());
                        sb.append(str);
                        if (a7.startsWith(TitleHelper.a(sb.toString(), ""))) {
                            if (b.startsWith("/")) {
                                b = this.c + b;
                            }
                            hashMap2.put(b, str6);
                        }
                    } else {
                        it3 = it4;
                        directoryIndexHelper2 = directoryIndexHelper3;
                        arrayList2 = arrayList3;
                        if (TitleHelper.a(str6.toLowerCase(), "").replace(movieInfo2.year, "").replace("  ", " ").startsWith(TitleHelper.a(movieInfo2.name.toLowerCase() + str.toLowerCase(), ""))) {
                            if (b.startsWith("/")) {
                                b = this.c + b;
                            }
                            hashMap2.put(b, str6);
                        }
                    }
                    it4 = it3;
                    arrayList3 = arrayList2;
                    directoryIndexHelper3 = directoryIndexHelper2;
                }
            }
            DirectoryIndexHelper directoryIndexHelper4 = directoryIndexHelper3;
            ArrayList arrayList4 = arrayList3;
            new HashMap();
            Iterator it5 = hashMap2.entrySet().iterator();
            int i2 = 3;
            while (it5.hasNext()) {
                Map.Entry entry = (Map.Entry) it5.next();
                try {
                    String b2 = HttpHelper.e().b(((String) entry.getKey()) + "##forceNoCache##", str5);
                    HttpHelper e = HttpHelper.e();
                    String str7 = this.c;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("expla=");
                    int i3 = i2 - 1;
                    try {
                        sb2.append(i2);
                        e.c(str7, sb2.toString());
                        i2 = i3 <= 0 ? 3 : i3;
                        String a8 = Regex.a(b2, "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?", 1);
                        if (!a8.isEmpty()) {
                            String replace = TitleHelper.a((String) entry.getValue(), ".").replace("..", ".");
                            String[] split = replace.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                            int length = split.length;
                            String str8 = "HQ";
                            int i4 = 0;
                            while (true) {
                                if (i4 >= length) {
                                    it2 = it5;
                                    str2 = str4;
                                    z = false;
                                    break;
                                }
                                String lowerCase = split[i4].toLowerCase();
                                it2 = it5;
                                try {
                                    if (lowerCase.contains("dvdscr") || lowerCase.contains("camrip") || lowerCase.contains("tsrip") || lowerCase.contains("hdcam") || lowerCase.contains("hdtc") || lowerCase.contains("hdts") || lowerCase.contains("dvdcam") || lowerCase.contains("dvdts") || lowerCase.contains("cam") || lowerCase.contains("telesync")) {
                                        break;
                                    } else if (lowerCase.contains("ts")) {
                                        break;
                                    } else {
                                        boolean contains = lowerCase.contains(str4);
                                        str3 = str4;
                                        String str9 = "720p";
                                        if (!contains) {
                                            try {
                                                if (!lowerCase.equals("1080")) {
                                                    if (!lowerCase.contains(str9)) {
                                                        if (!lowerCase.equals("720")) {
                                                            if (lowerCase.contains("brrip") || lowerCase.contains("bdrip") || lowerCase.contains("hdrip") || lowerCase.contains("web-dl")) {
                                                                str9 = "HD";
                                                            } else {
                                                                i4++;
                                                                it5 = it2;
                                                                str4 = str3;
                                                            }
                                                        }
                                                    }
                                                    str8 = str9;
                                                    i4++;
                                                    it5 = it2;
                                                    str4 = str3;
                                                }
                                            } catch (Throwable unused) {
                                                arrayList = arrayList4;
                                                directoryIndexHelper = directoryIndexHelper4;
                                                arrayList4 = arrayList;
                                                directoryIndexHelper4 = directoryIndexHelper;
                                                it5 = it2;
                                                str4 = str2;
                                            }
                                        }
                                        str8 = str3;
                                        i4++;
                                        it5 = it2;
                                        str4 = str3;
                                    }
                                } catch (Throwable unused2) {
                                    str3 = str4;
                                    arrayList = arrayList4;
                                    directoryIndexHelper = directoryIndexHelper4;
                                    arrayList4 = arrayList;
                                    directoryIndexHelper4 = directoryIndexHelper;
                                    it5 = it2;
                                    str4 = str2;
                                }
                            }
                            str2 = str4;
                            z = true;
                            String a9 = a();
                            if (z2) {
                                directoryIndexHelper = directoryIndexHelper4;
                                try {
                                    parsedLinkModel = directoryIndexHelper.a(replace);
                                } catch (Throwable unused3) {
                                    arrayList = arrayList4;
                                }
                            } else {
                                directoryIndexHelper = directoryIndexHelper4;
                                parsedLinkModel = directoryIndexHelper.b(replace);
                            }
                            if (parsedLinkModel != null) {
                                String c3 = parsedLinkModel.c();
                                if (!c3.equalsIgnoreCase("HQ")) {
                                    str8 = c3;
                                }
                                String b3 = parsedLinkModel.b();
                                i = 1;
                                a9 = a(b3, true);
                            } else {
                                i = 1;
                            }
                            String lowerCase2 = Regex.a(a8, "(magnet:\\?xt=urn:btih:[^&.]+)", i).toLowerCase();
                            if (z) {
                                str8 = "CAM-" + str8;
                            }
                            arrayList = arrayList4;
                            try {
                                arrayList.add(new MagnetObject(a9, lowerCase2, str8, a()));
                            } catch (Throwable unused4) {
                            }
                            arrayList4 = arrayList;
                            directoryIndexHelper4 = directoryIndexHelper;
                            it5 = it2;
                            str4 = str2;
                        }
                    } catch (Throwable unused5) {
                        it2 = it5;
                        str2 = str4;
                        arrayList = arrayList4;
                        directoryIndexHelper = directoryIndexHelper4;
                        i2 = i3;
                    }
                } catch (Throwable unused6) {
                    it2 = it5;
                    str3 = str4;
                    arrayList = arrayList4;
                    directoryIndexHelper = directoryIndexHelper4;
                    arrayList4 = arrayList;
                    directoryIndexHelper4 = directoryIndexHelper;
                    it5 = it2;
                    str4 = str2;
                }
            }
            ArrayList arrayList5 = arrayList4;
            if (arrayList5.size() > 0) {
                MediaSource mediaSource = new MediaSource(a(), "", false);
                mediaSource.setTorrent(true);
                mediaSource.setMagnetObjects(arrayList5);
                mediaSource.setStreamLink("magnet:KickassTorrents");
                observableEmitter.onNext(mediaSource);
            }
        } catch (Throwable th) {
            create.close();
            throw th;
        }
    }
}
