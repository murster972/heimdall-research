package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class AnimeTop extends BaseProvider {
    public String[] c = Utils.getProvider(69).split(",");
    public String d;

    private int j(String str) {
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            i += Character.codePointAt(str, i2) + i2;
        }
        return i;
    }

    public String a() {
        return "AnimeTop";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (a(movieInfo.genres)) {
            for (String str : this.c) {
                this.d = str;
                String b = b(movieInfo);
                if (!b.isEmpty()) {
                    a(movieInfo, observableEmitter, b);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (a(movieInfo.genres)) {
            for (String str : this.c) {
                this.d = str;
                String b = b(movieInfo);
                if (!b.isEmpty()) {
                    a(movieInfo, observableEmitter, b);
                }
            }
        }
    }

    private void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        AnimeTop animeTop;
        boolean z;
        ObservableEmitter<? super MediaSource> observableEmitter2;
        String str2;
        AnimeTop animeTop2 = this;
        ObservableEmitter<? super MediaSource> observableEmitter3 = observableEmitter;
        String str3 = str;
        boolean z2 = movieInfo.getType().intValue() == 1;
        String a2 = Regex.a(str3, "\\/.*\\.(\\w+)", 1);
        if (!a2.isEmpty()) {
            String str4 = "/";
            if (str3.startsWith(str4)) {
                str3 = animeTop2.d + str3;
            }
            String c2 = DateTimeHelper.c();
            Iterator it2 = null;
            Document b = Jsoup.b(HttpHelper.e().b(animeTop2.d + "/ajax/film/servers/" + a2 + "?ts=" + c2 + "&_=632", str3).replaceAll("(\r\n|\n)", ""));
            if (!z2) {
                it2 = b.g("li").b("a").iterator();
            }
            Iterator it3 = b.g("div[class*=server]").iterator();
            HashMap<String, String> a3 = Constants.a();
            a3.put("referer", str3);
            while (it3.hasNext()) {
                Element element = (Element) it3.next();
                String str5 = "\\n";
                String replace = element.b("data-id").replace("\\\"", "").replace(str5, "");
                String str6 = "/ajax/episode/info?ts=%s&_=%s&id=%s&server=%s";
                Iterator it4 = it3;
                String str7 = "ts";
                Iterator it5 = it2;
                String str8 = str4;
                HashMap<String, String> hashMap = a3;
                if (z2) {
                    String replace2 = element.h("a").b("data-id").replace("\\\"", "").replace(str5, "");
                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                    linkedHashMap.put("id", replace2);
                    linkedHashMap.put("server", replace);
                    linkedHashMap.put("update", "0");
                    linkedHashMap.put(str7, c2);
                    int a4 = animeTop.a(linkedHashMap);
                    String str9 = str8;
                    observableEmitter2 = observableEmitter;
                    animeTop.a(observableEmitter2, Regex.a(HttpHelper.e().a(String.format(animeTop.d + str6, new Object[]{c2, Integer.valueOf(a4), replace2, replace}), (Map<String, String>[]) new Map[]{hashMap}), "['\"]target['\"]\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1).replace("\\/", str9), "HD", false);
                    z = z2;
                    str2 = str9;
                } else {
                    Object obj = "update";
                    ObservableEmitter<? super MediaSource> observableEmitter4 = observableEmitter;
                    String str10 = str8;
                    z = z2;
                    String str11 = str10;
                    while (it5.hasNext()) {
                        Element element2 = (Element) it5.next();
                        String str12 = str11;
                        String str13 = str6;
                        if (!movieInfo.eps.equalsIgnoreCase(element2.b("data-base").replace("\\\"", "").replace(str5, ""))) {
                            animeTop = this;
                            observableEmitter4 = observableEmitter;
                            str11 = str12;
                            str6 = str13;
                        } else {
                            String replace3 = element2.h("a").b("data-id").replace("\\\"", "").replace(str5, "");
                            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
                            linkedHashMap2.put("id", replace3);
                            linkedHashMap2.put("server", replace);
                            linkedHashMap2.put(obj, "0");
                            linkedHashMap2.put(str7, c2);
                            int a5 = a(linkedHashMap2);
                            Object obj2 = obj;
                            String str14 = str7;
                            String str15 = str12;
                            ObservableEmitter<? super MediaSource> observableEmitter5 = observableEmitter;
                            a(observableEmitter5, Regex.a(HttpHelper.e().a(String.format(this.d + str13, new Object[]{c2, Integer.valueOf(a5), replace3, replace}), (Map<String, String>[]) new Map[]{hashMap}), "['\"]target['\"]\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1).replace("\\/", str15), "HD", false);
                            animeTop = this;
                            observableEmitter4 = observableEmitter5;
                            str6 = str13;
                            str5 = str5;
                            str7 = str14;
                            str11 = str15;
                            obj = obj2;
                        }
                    }
                    observableEmitter2 = observableEmitter4;
                    str2 = str11;
                }
                str4 = str2;
                animeTop2 = animeTop;
                ObservableEmitter<? super MediaSource> observableEmitter6 = observableEmitter2;
                it3 = it4;
                it2 = it5;
                z2 = z;
                a3 = hashMap;
            }
            AnimeTop animeTop3 = animeTop;
        }
    }

    private String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        Iterator it2 = Jsoup.b(HttpHelper.e().b(String.format(this.d + "/search?keyword=%s", new Object[]{TitleHelper.a(movieInfo.name, "+")}), this.d + "/")).g("div.item").b("a[data-jtitle]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String G = element.G();
            if (z) {
                if (TitleHelper.f(G).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName())))) {
                    String b = element.b("href");
                    if (!b.startsWith("/")) {
                        return b;
                    }
                    return this.d + b;
                }
            } else if (movieInfo.getSession().intValue() != 1 || !TitleHelper.f(G).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName())))) {
                if (TitleHelper.f(G).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName() + " Season " + movieInfo.session)))) {
                    String b2 = element.b("href");
                    if (!b2.startsWith("/")) {
                        return b2;
                    }
                    return this.d + b2;
                }
            } else {
                String b3 = element.b("href");
                if (!b3.startsWith("/")) {
                    return b3;
                }
                return this.d + b3;
            }
        }
        return "";
    }

    private int a(Map<String, String> map) {
        try {
            int j = j("f2d16d4e");
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getValue();
                String str2 = "f2d16d4e" + ((String) next.getKey());
                int i = 0;
                int i2 = 0;
                while (i < Math.max(str2.length(), str.length())) {
                    int codePointAt = i < str.length() ? i2 + Character.codePointAt(str, i) : i2 + 0;
                    i2 = i < str2.length() ? codePointAt + Character.codePointAt(str2, i) : codePointAt + 0;
                    i++;
                }
                j += j(Integer.toHexString(i2));
            }
            return j;
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return -1;
        }
    }
}
