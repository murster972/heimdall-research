package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class VidUpMe extends BaseResolver {
    public String a() {
        return "VidUpMe";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(HttpHelper.e().a(streamLink, (Map<String, String>[]) new Map[0]), "value?s*=['\"]([^'\"]*[^'\"]*vidup.io[^'\"]+)['\"]", 1);
        if (!a2.isEmpty()) {
            String a3 = Regex.a(a2, "(?://|\\.)(vidup\\.(?:me|tv|io))/(?:embed-)?([0-9a-zA-Z]+)", 2, 2);
            HashMap hashMap = new HashMap();
            hashMap.put("referer", streamLink);
            hashMap.put("referers", streamLink);
            HttpHelper e = HttpHelper.e();
            String a4 = e.a("https://vidup.io/api/serve/video/" + a3, "", (Map<String, String>[]) new Map[]{hashMap});
            Iterator it2 = Regex.b(a4, "['\"]([0-9a-zA-Z]+)['\"]?\\s*:\\s*['\"]([^'\"]*[^'\"]*" + a3 + ".mp4[^'\"]+)['\"]", 2, true).get(0).iterator();
            Iterator it3 = Regex.b(a4, "['\"]([0-9a-zA-Z]+)['\"]?\\s*:\\s*['\"]([^'\"]*[^'\"]*" + a3 + ".mp4[^'\"]+)['\"]", 2, true).get(1).iterator();
            while (it3.hasNext()) {
                observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), (String) it3.next(), (String) it2.next())));
            }
        }
    }
}
