package com.utils.Getlink.Resolver;

public class Watchvideo extends GenericResolver {
    public String a() {
        return "Watchvideo";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    public String g() {
        return "(?://|\\.)(watchvideo\\.us)/([a-zA-Z0-9]+)";
    }

    public String h() {
        return "https://watchvideo.us";
    }
}
