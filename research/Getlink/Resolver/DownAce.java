package com.utils.Getlink.Resolver;

import com.facebook.common.util.UriUtil;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.Map;

public class DownAce extends BaseResolver {
    public String a() {
        return "DownAce";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(downace\\.com)/(?:embed/)?([0-9a-zA-Z]+)", 2);
        if (!a2.isEmpty()) {
            try {
                String a3 = Regex.a(HttpHelper.e().a("https://downace.com/embed/" + a2 + "/", (Map<String, String>[]) new Map[0]), "controls\\s+preload.*?src=\"([^\"]+)", 1, 34);
                if (BaseResolver.b(a3) && !a3.isEmpty()) {
                    if (a3.startsWith("//")) {
                        a3 = "http:" + a3;
                    } else if (a3.startsWith("/")) {
                        a3 = "https://downace.com" + a3;
                    } else if (!a3.startsWith(UriUtil.HTTP_SCHEME)) {
                        a3 = "https://downace.com/" + a3;
                    }
                    observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), a3, "HD")));
                }
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
        }
    }
}
