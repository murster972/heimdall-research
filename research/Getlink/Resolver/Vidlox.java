package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;

public class Vidlox extends PremiumResolver {
    public String a() {
        return "Vidlox";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?://|\\.)(vidlox\\.(?:tv|me))/(?:embed-)?([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            String i = BaseProvider.i(streamLink);
            super.a(mediaSource, observableEmitter);
            if (!Utils.d) {
                String a3 = HttpHelper.e().a(i + "/embed-" + a2, (Map<String, String>[]) new Map[0]);
                String a4 = Regex.a(a3, "document.cookie\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
                if (!a4.isEmpty()) {
                    String a5 = Regex.a(a3, "location.href\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
                    HttpHelper.e().c(a5, a4);
                    str = HttpHelper.e().a(a5, (Map<String, String>[]) new Map[0]);
                } else {
                    str = HttpHelper.e().a(i + "/" + a2, (Map<String, String>[]) new Map[0]);
                }
                Iterator it2 = Regex.b(Regex.a(str, "sources\\s*:\\s*\\[(.+?)\\]", 1, true), "[\"'](http[^\"']+)", 1, true).get(0).iterator();
                while (it2.hasNext()) {
                    observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), (String) it2.next(), mediaSource.getQuality())));
                }
            }
        }
    }
}
