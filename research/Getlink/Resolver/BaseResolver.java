package com.utils.Getlink.Resolver;

import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import com.utils.Getlink.Resolver.premium.AlfaFile;
import com.utils.Getlink.Resolver.premium.ClicknUpload;
import com.utils.Getlink.Resolver.premium.DDLTO;
import com.utils.Getlink.Resolver.premium.Dailymotion;
import com.utils.Getlink.Resolver.premium.Dailyuploads;
import com.utils.Getlink.Resolver.premium.Down4files;
import com.utils.Getlink.Resolver.premium.Earn4files;
import com.utils.Getlink.Resolver.premium.FileFactory;
import com.utils.Getlink.Resolver.premium.FileUp;
import com.utils.Getlink.Resolver.premium.FlashX;
import com.utils.Getlink.Resolver.premium.Mediafire;
import com.utils.Getlink.Resolver.premium.MegaZN;
import com.utils.Getlink.Resolver.premium.NitroFlare;
import com.utils.Getlink.Resolver.premium.Oboom;
import com.utils.Getlink.Resolver.premium.OneFichier;
import com.utils.Getlink.Resolver.premium.RapidGator;
import com.utils.Getlink.Resolver.premium.RockFile;
import com.utils.Getlink.Resolver.premium.TurboBit;
import com.utils.Getlink.Resolver.premium.Uploaded;
import com.utils.Getlink.Resolver.premium.Uploadgig;
import com.utils.Getlink.Resolver.premium.VK;
import com.utils.Getlink.Resolver.premium.ZTONet;
import com.utils.Getlink.Resolver.premium.torrent.Torrent;
import com.utils.Utils;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import okhttp3.Response;

public abstract class BaseResolver {

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f6518a = false;
    private static final String[] b = {".jpg", ".jpeg", ".gif", ".png", ".js", ".css", ".htm", ".html", ".php", ".srt", ".sub", ".xml", ".swf", ".vtt"};
    private static final String[] c = {"[\"']?\\s*file\\s*[\"']?\\s*[:=,]?\\s*[\"']([^\"']+)(?:[^\\}>\\]]+)[\"']?\\s*label\\s*[\"']?\\s*[:=]\\s*[\"']?([^\"',]+)", "[\"']?src[\"']?\\s*:\\s*[\"']?([^\\}\"']+)[\"']?\\s*,\\s*[\"']?height[\"']?\\s*:\\s*['\"]?\\s*(\\d+)\\s*['\"]?", "src\\s*:\\s*[\"']([^\"']+)[^\\{\\}]+(?<=height:)(\\d+)", "source\\s+src\\s*=\\s*['\"]([^'\"]+)['\"](?:.*?data-res\\s*=\\s*['\"]([^'\"]+))?", "video[^><]+src\\s*=\\s*['\"]([^'\"]+)", "sources\\s*:\\s*\\[\\s*['\"]\\s*(.*?)\\s*['\"],", "sources\\s*:\\s*\\[.*?\\s*,\\s*['\"]\\s*(.*?)\\s*['\"]\\s*,?.*?\\]", "sources\\s*:\\s*\\[s*['\"]\\s*(http.*?)\\s*['\"]\\]", "sources\\s*:\\s*\\[\\s*\\{\\s*['\"]?src['\"]?\\s*:\\s*['\"](.*?)['\"]\\s*,\\s*type\\s*:", "[\"']?\\s*(?:file|url)\\s*[\"']?\\s*[:=]\\s*[\"']([^\"']+)", "param\\s+name\\s*=\\s*\"src\"\\s*value\\s*=\\s*\"([^\"]+)", "src\\s*:\\s*['\"](.*?)['\"]\\s*,\\s*type\\s*:.*,\\s*label\\s*:\\s*['\"](\\w+)['\"]", "[\"']?src[\"']?\\s*:\\s*[\"']?([^\\}\"']+)[\"']?\\s*,\\s*[\"']?type[\"']?\\s*:\\s*[\"']?[^\\}\"']+[\"']?\\s*,\\s*[\"']?label[\"']?\\s*:\\s*[\"']?(\\d+)[\"']?\\s*", "['\"]src['\"]:['\"]([^'\"]+[^'\"])['\"],\"\\w+\":\"\\w+\",\"\\w+\":\"(\\w+)\""};
    private static String[] d;
    private static String[] e;

    public static MediaSource a(MediaSource mediaSource, ResolveResult resolveResult) {
        MediaSource cloneDeeply = mediaSource.cloneDeeply();
        if (resolveResult.getFilesize() > 0) {
            cloneDeeply.setFileSize(resolveResult.getFilesize());
        }
        cloneDeeply.setStreamLink(resolveResult.getResolvedLink());
        cloneDeeply.setQuality(resolveResult.getResolvedQuality());
        if (cloneDeeply.getQuality() == null || cloneDeeply.getQuality().isEmpty()) {
            cloneDeeply.setQuality(mediaSource.getQuality());
        }
        cloneDeeply.setPlayHeader(resolveResult.getPlayHeader());
        cloneDeeply.setHostName(resolveResult.getResolverName());
        cloneDeeply.setRealdebrid(resolveResult.isRealdebrid());
        cloneDeeply.setAlldebrid(resolveResult.isAlldebrid());
        cloneDeeply.setPremiumize(resolveResult.isPremiumize());
        return cloneDeeply;
    }

    public static Observable<MediaSource> b(MediaSource mediaSource) {
        MediaSource cloneDeeply = mediaSource.cloneDeeply();
        String streamLink = cloneDeeply.getStreamLink();
        if (streamLink.endsWith("rar") || streamLink.contains(".part")) {
            Logger.a("Resolvelink rar ", cloneDeeply.toStringAllObjs());
            return Observable.just(cloneDeeply);
        }
        if (BaseProvider.b() && Utils.d) {
            String[] d2 = d();
            int length = d2.length;
            boolean z = false;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                String str = d2[i];
                if (TitleHelper.f(streamLink).contains(TitleHelper.f(str)) || TitleHelper.f(str).contains(TitleHelper.f(streamLink))) {
                    z = true;
                } else {
                    i++;
                }
            }
            z = true;
            if (!z) {
                return Observable.just(cloneDeeply);
            }
        }
        f6518a = FreeMoviesApp.l().getBoolean("pref_off_premium_resolve4", true);
        if (streamLink.contains("magnet:") || streamLink.contains(".torrent")) {
            return new Torrent().a(cloneDeeply);
        }
        if (streamLink.contains("streamx.live") || streamLink.contains("stream365.live")) {
            return new HydraX().a(cloneDeeply);
        }
        if (streamLink.contains("schoolbalebale2") || streamLink.contains("streamloverx") || streamLink.contains(".voxzer")) {
            return new ClubPlay().a(cloneDeeply);
        }
        if (streamLink.contains("hydrax")) {
            return new T3Play().a(cloneDeeply);
        }
        if (streamLink.contains("videobin")) {
            return new VideoBin().a(cloneDeeply);
        }
        if (streamLink.contains("prostream") || streamLink.contains("procdnvids")) {
            return new Prostream().a(cloneDeeply);
        }
        if (streamLink.contains("mixdrop")) {
            return new MixDrop().a(cloneDeeply);
        }
        if (streamLink.contains("vidia")) {
            return new VidiaTV().a(cloneDeeply);
        }
        if (streamLink.contains("openload") || streamLink.contains("oload.")) {
            return new Openload().a(cloneDeeply);
        }
        if (streamLink.contains("govid.")) {
            return new GoVid().a(cloneDeeply);
        }
        if (streamLink.contains("downace")) {
            return new DownAce().a(cloneDeeply);
        }
        if (streamLink.contains("userscloud")) {
            return new UsersCloud().a(cloneDeeply);
        }
        if (streamLink.contains("uptobox") || streamLink.contains("uptostream")) {
            return new UpToBox().a(cloneDeeply);
        }
        if (streamLink.contains("rapidvideo") || streamLink.contains("raptu") || streamLink.contains("rapidvid") || streamLink.contains("bitporno")) {
            return new RapidVideo().a(cloneDeeply);
        }
        if (streamLink.contains("streamango") || streamLink.contains("streamcherry") || streamLink.contains("fruithosts")) {
            return new Streamango().a(cloneDeeply);
        }
        if (streamLink.contains("vidto.me")) {
            return new Vidto().a(cloneDeeply);
        }
        if (streamLink.contains("vidlox")) {
            return new Vidlox().a(cloneDeeply);
        }
        if (streamLink.contains("vixtodo") || streamLink.contains("vidtodo") || streamLink.contains("vidstodo") || streamLink.contains("vidtoup")) {
            return new VidTodo().a(cloneDeeply);
        }
        if (streamLink.contains("vodlock")) {
            return new Vodlock().a(cloneDeeply);
        }
        if (streamLink.contains("powvideo")) {
            return new PowerVideo().a(cloneDeeply);
        }
        if (streamLink.contains("estream")) {
            return new EStream().a(cloneDeeply);
        }
        if (streamLink.contains("daclips")) {
            return new DaClips().a(cloneDeeply);
        }
        if (streamLink.contains("movpod")) {
            return new MovPod().a(cloneDeeply);
        }
        if (streamLink.contains("thevideo.me")) {
            return new TheVideo().a(cloneDeeply);
        }
        if (streamLink.contains("vidzi")) {
            return new Vidzi().a(cloneDeeply);
        }
        if (streamLink.contains("vidoza")) {
            return new Vidoza().a(cloneDeeply);
        }
        if (streamLink.contains("them4ufree")) {
            return new Them4uFree().a(cloneDeeply);
        }
        if (streamLink.contains("vidup.me") || streamLink.contains("vidup.io") || streamLink.contains("vidup.tv")) {
            return new VidUpMe().a(cloneDeeply);
        }
        if (streamLink.contains("ok.ru") || streamLink.contains("odnoklassniki")) {
            return new Okru().a(cloneDeeply);
        }
        if (streamLink.contains("vidsrc.me")) {
            return new GCLoud().a(cloneDeeply);
        }
        if (streamLink.contains("vidcloud.") || streamLink.contains("loadvid.")) {
            return new VidCloud().a(cloneDeeply);
        }
        if (streamLink.contains("vcstream")) {
            return new VcStream().a(cloneDeeply);
        }
        if (streamLink.contains("gorillavid")) {
            return new GorillaVid().a(cloneDeeply);
        }
        if (streamLink.contains("yourupload")) {
            return new YourUpload().a(cloneDeeply);
        }
        if (streamLink.contains("entervideo")) {
            return new EnterVideo().a(cloneDeeply);
        }
        if (streamLink.contains("mp4upload")) {
            return new Mp4Upload().a(cloneDeeply);
        }
        if (streamLink.contains("fastplay.")) {
            return new FastPlay().a(cloneDeeply);
        }
        if (streamLink.contains("vshare.eu")) {
            return new VShareEU().a(cloneDeeply);
        }
        if (streamLink.contains("thevideobee.to")) {
            return new TheVideoBee().a(cloneDeeply);
        }
        if (streamLink.contains("novamov.com") || streamLink.contains("auroravid.to")) {
            return new NovaMov().a(cloneDeeply);
        }
        if (streamLink.contains("nowvideo.sx")) {
            return new NowVideo().a(cloneDeeply);
        }
        if (streamLink.contains("putload.tv")) {
            return new Putload().a(cloneDeeply);
        }
        if (streamLink.contains("rapidgator")) {
            return new RapidGator().a(cloneDeeply);
        }
        if (streamLink.contains("alfafile")) {
            return new AlfaFile().a(cloneDeeply);
        }
        if (streamLink.contains("clicknupload")) {
            return new ClicknUpload().a(cloneDeeply);
        }
        if (streamLink.contains("flashx")) {
            return new FlashX().a(cloneDeeply);
        }
        if (streamLink.contains("nitroflare")) {
            return new NitroFlare().a(cloneDeeply);
        }
        if (streamLink.contains("1fichier")) {
            return new OneFichier().a(cloneDeeply);
        }
        if (streamLink.contains("oboom")) {
            return new Oboom().a(cloneDeeply);
        }
        if (streamLink.contains("oneFichier")) {
            return new OneFichier().a(cloneDeeply);
        }
        if (streamLink.contains("rockfile")) {
            return new RockFile().a(cloneDeeply);
        }
        if (streamLink.contains("ddl.to")) {
            return new DDLTO().a(cloneDeeply);
        }
        if (streamLink.contains("turbobit")) {
            return new TurboBit().a(cloneDeeply);
        }
        if (streamLink.contains("uploaded") || streamLink.contains("ul.to")) {
            return new Uploaded().a(cloneDeeply);
        }
        if (streamLink.contains("uploadrocket")) {
            return new RapidGator().a(cloneDeeply);
        }
        if (streamLink.contains("filefactory")) {
            return new FileFactory().a(cloneDeeply);
        }
        if (streamLink.contains("cloudvideo")) {
            return new CloudVideo().a(cloneDeeply);
        }
        if (streamLink.contains("amazon")) {
            return new AmazonDrive().a(cloneDeeply);
        }
        if (streamLink.contains("vidmoly")) {
            return new VidMoly().a(cloneDeeply);
        }
        if (streamLink.contains("gounlimited")) {
            return new Gounlimited().a(cloneDeeply);
        }
        if (streamLink.contains("fembed.") || streamLink.contains("24hd.") || streamLink.contains("gcloud.") || streamLink.contains("t21.link") || streamLink.contains("feurl") || streamLink.contains("bmoviesfree") || streamLink.contains("mediashore")) {
            return new Fembed().a(cloneDeeply);
        }
        if (streamLink.contains("jawcloud")) {
            return new Jawcloud().a(cloneDeeply);
        }
        if (streamLink.contains("watchvideo")) {
            return new Watchvideo().a(cloneDeeply);
        }
        if (streamLink.contains("clipwatching")) {
            return new Clipwatching().a(cloneDeeply);
        }
        if (streamLink.contains("mcloud")) {
            return new MCloud().a(cloneDeeply);
        }
        if (streamLink.contains("idtbox")) {
            return new Idtbox().a(cloneDeeply);
        }
        if (streamLink.contains("nofile")) {
            return new Nofile().a(cloneDeeply);
        }
        if (streamLink.contains("xstreamcdn")) {
            return new Xstreamcdn().a(cloneDeeply);
        }
        if (streamLink.contains("viduplayer")) {
            return new ViduPlayer().a(cloneDeeply);
        }
        if (streamLink.contains("vcdn.io")) {
            return new VCDN().a(cloneDeeply);
        }
        if (streamLink.contains("jetload")) {
            return new JetLoad().a(cloneDeeply);
        }
        if (streamLink.contains("verystream")) {
            return new VeryStream().a(cloneDeeply);
        }
        if (streamLink.contains("flix555")) {
            return new Flix555().a(cloneDeeply);
        }
        if (streamLink.contains("ostream")) {
            return new OStream().a(cloneDeeply);
        }
        if (streamLink.contains("onlystream")) {
            return new StreamTv().a(cloneDeeply);
        }
        if (streamLink.contains("gamovideo")) {
            return new GamoVideo().a(cloneDeeply);
        }
        if (streamLink.contains("real-debrid")) {
            return new DebCached().a(cloneDeeply);
        }
        if (streamLink.contains("ulozto")) {
            return new ZTONet().a(cloneDeeply);
        }
        if (streamLink.contains("mega.nz")) {
            return new MegaZN().a(cloneDeeply);
        }
        if (streamLink.contains("mediafire")) {
            return new Mediafire().a(cloneDeeply);
        }
        if (streamLink.contains("sendit.cloud")) {
            return new SenditCloud().a(cloneDeeply);
        }
        if (streamLink.contains("vk.")) {
            return new VK().a(cloneDeeply);
        }
        if (streamLink.contains("hqq.tv")) {
            return new HdTv().a(cloneDeeply);
        }
        if (streamLink.contains("gdriveplayer")) {
            return new Gdriver().a(cloneDeeply);
        }
        if (streamLink.contains("upstream")) {
            return new UpStream().a(cloneDeeply);
        }
        if (streamLink.contains("abcvideo")) {
            return new ABCvideo().a(cloneDeeply);
        }
        if (streamLink.contains("supervideo")) {
            return new Supervideo().a(cloneDeeply);
        }
        if (streamLink.contains("anavids")) {
            return new Anavids().a(cloneDeeply);
        }
        if (streamLink.contains("dropapk")) {
            return new DropApk().a(cloneDeeply);
        }
        if (streamLink.contains("api.hdv.fun") || streamLink.contains("ffull.pw")) {
            return new P2PCDN().a(cloneDeeply);
        }
        if (streamLink.contains("vup.to")) {
            return new VupTo().a(cloneDeeply);
        }
        if (streamLink.contains("letsupload")) {
            return new Letsupload().a(cloneDeeply);
        }
        if (streamLink.contains("eplayvid.")) {
            return new EPlayvid().a(cloneDeeply);
        }
        if (streamLink.contains("streamwire.")) {
            return new Streamwire().a(cloneDeeply);
        }
        if (streamLink.contains("earn4files.")) {
            return new Earn4files().a(cloneDeeply);
        }
        if (streamLink.contains("file-up.")) {
            return new FileUp().a(cloneDeeply);
        }
        if (streamLink.contains("dailymotion.")) {
            return new Dailymotion().a(cloneDeeply);
        }
        if (streamLink.contains("dailyuploads.")) {
            return new Dailyuploads().a(cloneDeeply);
        }
        if (streamLink.contains("uploadgig.")) {
            return new Uploadgig().a(cloneDeeply);
        }
        if (streamLink.contains("4downfiles.")) {
            return new Down4files().a(cloneDeeply);
        }
        if (streamLink.contains("movcloud.")) {
            return new Movcloud().a(cloneDeeply);
        }
        if (streamLink.contains("streamtape.")) {
            return new Streamtape().a(cloneDeeply);
        }
        if (streamLink.contains("aparat.")) {
            return new Aparat().a(cloneDeeply);
        }
        if (streamLink.contains("oogly.")) {
            return new Oogly().a(cloneDeeply);
        }
        if (streamLink.contains("mstream.")) {
            return new MstreamTo().a(cloneDeeply);
        }
        Logger.a("NEEDIMPLEMENT", cloneDeeply.toStringAllObjs());
        return Observable.just(cloneDeeply);
    }

    public static String c() {
        return "RealDebird,AllDebird,PREMIUMIZE,GoogleVideo,Amazone,ok.ru,RapidVideo,HLS,Streamango,openload,CDN-FastServer,FastPlay,UpToBox,DaClips,GorillaVid,HD SlowServer,Vidzi,VidToDo,EnterVideo,MovPod,VcStream,FB-CDN";
    }

    public static String[] d() {
        String[] strArr = e;
        if (strArr == null || strArr.length == 0) {
            e = GlobalVariable.c().a().getRd_config().getList().split(",");
        }
        return e;
    }

    public static String[] e() {
        return a(FreeMoviesApp.l().getBoolean("pref_show_hd_sources_only", false));
    }

    public abstract String a();

    /* access modifiers changed from: protected */
    public abstract void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter);

    public static HashMap<String, String> a(String str, String str2) {
        String str3;
        HashMap<String, String> hashMap = new HashMap<>();
        if (str2 == null || str2.isEmpty()) {
            str3 = "(?s)<form[^>]*>(.*?)</form>";
        } else {
            str3 = "(?s)<form [^>]*(?:id|name)\\s*=\\s*['\"]?" + str2 + "['\"]?[^>]*>(.*?)</form>";
        }
        Iterator it2 = Regex.b(str, str3, 1, 34).get(0).iterator();
        while (it2.hasNext()) {
            Iterator it3 = Regex.b((String) it2.next(), "<input[^>]*type=['\"]?hidden['\"]?[^>]*>", 0).get(0).iterator();
            while (it3.hasNext()) {
                String str4 = (String) it3.next();
                String a2 = Regex.a(str4, "name\\s*=\\s*['\"]([^'\"]+)", 1);
                String a3 = Regex.a(str4, "value\\s*=\\s*['\"]([^'\"]*)", 1);
                if (!a2.isEmpty() && !a3.isEmpty()) {
                    hashMap.put(a2, a3);
                }
            }
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public ArrayList<ResolveResult> a(String str, String str2, boolean z, HashMap<String, String> hashMap, String[]... strArr) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(str2);
        String[][] strArr2 = new String[1][];
        strArr2[0] = (strArr == null || strArr.length <= 0) ? null : strArr[0];
        return a(str, (ArrayList<String>) arrayList, z, hashMap, strArr2);
    }

    /* JADX WARNING: type inference failed for: r4v2 */
    /* JADX WARNING: type inference failed for: r4v23 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=?, for r4v21, types: [boolean, int] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.original.tase.model.ResolveResult> a(java.lang.String r19, java.util.ArrayList<java.lang.String> r20, boolean r21, java.util.HashMap<java.lang.String, java.lang.String> r22, java.lang.String[]... r23) {
        /*
            r18 = this;
            r0 = r23
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.lang.String[] r3 = b
            r4 = 0
            if (r0 == 0) goto L_0x0024
            int r5 = r0.length
            if (r5 <= 0) goto L_0x0024
            r5 = r0[r4]
            if (r5 == 0) goto L_0x0024
            r5 = r0[r4]
            int r5 = r5.length
            if (r5 > 0) goto L_0x001e
            goto L_0x0024
        L_0x001e:
            r0 = r0[r4]
            java.lang.String[] r3 = com.original.tase.utils.Utils.a((java.lang.String[]) r3, (java.lang.String[]) r0)
        L_0x0024:
            java.util.Iterator r5 = r20.iterator()
        L_0x0028:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0165
            java.lang.Object r0 = r5.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String[] r6 = c
            int r7 = r6.length
            r8 = 0
        L_0x0038:
            if (r8 >= r7) goto L_0x0161
            r9 = r6[r8]
            java.lang.String r10 = "\\/"
            java.lang.String r11 = "/"
            java.lang.String r12 = r0.replace(r10, r11)
            r0 = 32
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r9, r0)
            java.util.regex.Matcher r9 = r0.matcher(r12)
            java.lang.String r13 = ""
            r14 = r13
        L_0x0051:
            boolean r0 = r9.find()
            if (r0 == 0) goto L_0x0155
            r15 = 2
            r4 = 1
            int r0 = r9.groupCount()     // Catch:{ all -> 0x0083 }
            if (r0 != r4) goto L_0x0064
            java.lang.String r14 = r9.group(r4)     // Catch:{ all -> 0x0083 }
            goto L_0x007f
        L_0x0064:
            int r0 = r9.groupCount()     // Catch:{ all -> 0x0083 }
            if (r0 != r15) goto L_0x007f
            java.lang.String r14 = r9.group(r4)     // Catch:{ all -> 0x0083 }
            java.lang.String r0 = r9.group(r15)     // Catch:{ all -> 0x0073 }
            goto L_0x0080
        L_0x0073:
            r0 = move-exception
            r15 = r0
            r4 = 0
            boolean[] r0 = new boolean[r4]     // Catch:{ all -> 0x007c }
            com.original.tase.Logger.a((java.lang.Throwable) r15, (boolean[]) r0)     // Catch:{ all -> 0x007c }
            goto L_0x007f
        L_0x007c:
            r0 = move-exception
            r4 = 1
            goto L_0x0084
        L_0x007f:
            r0 = r13
        L_0x0080:
            r16 = 0
            goto L_0x008e
        L_0x0083:
            r0 = move-exception
        L_0x0084:
            boolean[] r15 = new boolean[r4]
            r16 = 0
            r15[r16] = r4
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r15)
            r0 = r13
        L_0x008e:
            boolean r4 = r14.isEmpty()
            if (r4 == 0) goto L_0x009a
            r15 = r22
            r19 = r5
            goto L_0x0150
        L_0x009a:
            java.lang.String r4 = r14.replace(r10, r11)
            java.lang.String r14 = "\\\\"
            java.lang.String r4 = r4.replace(r14, r13)
            java.lang.String r14 = "&amp;"
            java.lang.String r15 = "&"
            java.lang.String r14 = r4.replace(r14, r15)
            boolean r4 = r14.contains(r11)
            if (r4 == 0) goto L_0x00e4
            java.lang.String[] r4 = r14.split(r11)
            int r15 = r4.length
            r19 = r5
            r5 = 2
            if (r15 < r5) goto L_0x00e6
            int r5 = r4.length
            r15 = 1
            int r5 = r5 - r15
            r4 = r4[r5]
            java.lang.String r4 = r4.toLowerCase()
            int r5 = r3.length
            r15 = 0
        L_0x00c7:
            if (r15 >= r5) goto L_0x00df
            r17 = r5
            r5 = r3[r15]
            boolean r5 = r4.contains(r5)
            if (r5 == 0) goto L_0x00d9
            r5 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            goto L_0x00e0
        L_0x00d9:
            r5 = 1
            int r15 = r15 + 1
            r5 = r17
            goto L_0x00c7
        L_0x00df:
            r5 = 0
        L_0x00e0:
            if (r5 == 0) goto L_0x00e6
            goto L_0x0150
        L_0x00e4:
            r19 = r5
        L_0x00e6:
            java.lang.String r4 = "//"
            boolean r4 = r14.startsWith(r4)
            if (r4 == 0) goto L_0x0100
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "http:"
            r4.append(r5)
            r4.append(r14)
            java.lang.String r14 = r4.toString()
            goto L_0x0119
        L_0x0100:
            java.lang.String r4 = ":"
            boolean r4 = r14.startsWith(r4)
            if (r4 == 0) goto L_0x0119
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "http"
            r4.append(r5)
            r4.append(r14)
            java.lang.String r14 = r4.toString()
        L_0x0119:
            java.lang.String r4 = "://"
            boolean r4 = r14.contains(r4)
            if (r4 == 0) goto L_0x014e
            java.lang.String r4 = " "
            java.lang.String r5 = "%20"
            java.lang.String r4 = r14.replace(r4, r5)
            boolean r5 = r2.contains(r4)
            if (r5 == 0) goto L_0x0132
            r15 = r22
            goto L_0x014c
        L_0x0132:
            r2.add(r4)
            com.original.tase.model.ResolveResult r5 = new com.original.tase.model.ResolveResult
            java.lang.String r14 = r18.a()
            if (r0 == 0) goto L_0x013e
            goto L_0x013f
        L_0x013e:
            r0 = r13
        L_0x013f:
            r5.<init>((java.lang.String) r14, (java.lang.String) r4, (java.lang.String) r0)
            r15 = r22
            if (r21 == 0) goto L_0x0149
            r5.setPlayHeader(r15)
        L_0x0149:
            r1.add(r5)
        L_0x014c:
            r14 = r4
            goto L_0x0150
        L_0x014e:
            r15 = r22
        L_0x0150:
            r5 = r19
            r4 = 0
            goto L_0x0051
        L_0x0155:
            r15 = r22
            r19 = r5
            r16 = 0
            int r8 = r8 + 1
            r0 = r12
            r4 = 0
            goto L_0x0038
        L_0x0161:
            r15 = r22
            goto L_0x0028
        L_0x0165:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Resolver.BaseResolver.a(java.lang.String, java.util.ArrayList, boolean, java.util.HashMap, java.lang.String[][]):java.util.ArrayList");
    }

    private static String[] a(boolean z) {
        String[] strArr = d;
        if (strArr == null || strArr.length == 0) {
            d = GlobalVariable.c().a().getReslover().getList().split(",");
        }
        return d;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        Map<String, List<String>> multimap;
        List list;
        HashMap hashMap = new HashMap();
        hashMap.put("Range", "bytes=0-1");
        Response a2 = HttpHelper.e().a(str, false, (Map<String, String>) hashMap);
        if (a2 == null) {
            return false;
        }
        if (a2.body() != null) {
            a2.body().close();
        }
        try {
            if (a2.body() == null || (multimap = a2.headers().toMultimap()) == null) {
                return false;
            }
            if (!hashMap.containsKey("Range") && !hashMap.containsKey("range")) {
                return false;
            }
            if (!multimap.containsKey("Content-Range")) {
                if (!multimap.containsKey("content-range")) {
                    return false;
                }
            }
            if (multimap.containsKey("Content-Range")) {
                list = multimap.get("Content-Range");
            } else {
                list = multimap.get("content-range");
            }
            if (list != null) {
                if (list.size() > 0) {
                    String str2 = (String) list.get(0);
                    if (str2 != null) {
                        if (!str2.isEmpty()) {
                            String[] strArr = null;
                            if (str2.contains("/")) {
                                strArr = str2.split("/");
                            }
                            if (strArr != null && strArr.length == 2) {
                                str2 = strArr[1].trim();
                            }
                            long j = -1;
                            if (com.original.tase.utils.Utils.b(str2)) {
                                j = Long.valueOf(str2).longValue();
                            }
                            if (j >= 52428800) {
                                return true;
                            }
                            return false;
                        }
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return false;
        }
    }

    public Observable<MediaSource> a(final MediaSource mediaSource) {
        return Observable.create(new ObservableOnSubscribe<MediaSource>() {
            public void subscribe(ObservableEmitter<MediaSource> observableEmitter) throws Exception {
                try {
                    BaseResolver.this.a(mediaSource, (ObservableEmitter<? super MediaSource>) observableEmitter);
                } catch (Exception e) {
                    Logger.a("BaseResolver", e.getMessage());
                }
                observableEmitter.onComplete();
            }
        });
    }

    public static boolean b(String str) {
        String[] split = str.split("/");
        if (split.length >= 2) {
            String lowerCase = split[split.length - 1].toLowerCase();
            for (String contains : b) {
                if (lowerCase.contains(contains)) {
                    return false;
                }
            }
        }
        return true;
    }

    public ArrayList b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("240");
        arrayList.add("144");
        arrayList.add("240p");
        arrayList.add("144p");
        return arrayList;
    }
}
