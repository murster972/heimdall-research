package com.utils.Getlink.Resolver;

public class Clipwatching extends GenericResolver {
    public String a() {
        return "Clipwatching";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    public String g() {
        return "(?://|\\.)(clipwatching\\.com)(?:/embed-|/)([a-zA-Z0-9]+)";
    }

    public String h() {
        return "https://clipwatching.com";
    }
}
