package com.utils.Getlink.Resolver;

public class UpStream extends GenericResolver {
    public String a() {
        return "UpStream";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    public String g() {
        return "(?:\\/|\\.)(upstream)\\.(?:com|be|co|to)(?:/embed-|/)([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "https://upstream.to";
    }
}
