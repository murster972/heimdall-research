package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.Http2Helper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import okhttp3.internal.cache.DiskLruCache;

public class MCloud extends PremiumResolver {
    public String a() {
        return "MCloud";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        if (!Regex.a(streamLink, "(?://|\\.)(my?cloud\\.to)/embed/(.*?)(?:$|\\?)", 2).isEmpty()) {
            HashMap hashMap = new HashMap();
            hashMap.put("referer", "https://www7.putlockertv.to");
            hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
            hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            String a2 = Http2Helper.b().a(streamLink, hashMap);
            ArrayList arrayList = new ArrayList();
            arrayList.add(a2);
            if (JsUnpacker.m30920(a2)) {
                arrayList.addAll(JsUnpacker.m30916(a2));
            }
            Iterator<ResolveResult> it2 = a(streamLink, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                observableEmitter.onNext(BaseResolver.a(mediaSource, next));
            }
        }
    }
}
