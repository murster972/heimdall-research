package com.utils.Getlink.Resolver;

public class StreamTv extends GenericResolver {
    public String a() {
        return "OnlyStream";
    }

    public boolean f() {
        return false;
    }

    public String g() {
        return "(?://|\\.)(onlystream\\.(?:tv|cc|si|me))/(?:emded-|e/|)([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://onlystream.tv";
    }
}
