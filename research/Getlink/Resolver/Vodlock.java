package com.utils.Getlink.Resolver;

import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Vodlock extends BaseResolver {
    public String a() {
        return "Vodlock";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(vodlock\\.co)/(?:embed-)?([a-zA-Z0-9]+)", 2);
        if (!a2.isEmpty()) {
            String str = "http://vodlock.co/" + a2;
            String a3 = Utils.a((Map<String, String>) BaseResolver.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]), (String) null));
            HashMap hashMap = new HashMap();
            hashMap.put("Referer", str);
            try {
                Thread.sleep(3500);
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            String a4 = HttpHelper.e().a(str, a3, (Map<String, String>[]) new Map[]{hashMap});
            ArrayList arrayList = new ArrayList();
            arrayList.add(a4);
            if (JsUnpacker.m30920(a4)) {
                arrayList.addAll(JsUnpacker.m30916(a4));
            }
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                Iterator it3 = Regex.b((String) it2.next(), "sources:\\s*\\[([^\\]]+)", 1).get(0).iterator();
                while (it3.hasNext()) {
                    Iterator it4 = Regex.b((String) it3.next(), "[\"']([^\"'\\s,]+)", 1).get(0).iterator();
                    while (it4.hasNext()) {
                        String str2 = (String) it4.next();
                        if (str2.startsWith("//")) {
                            str2 = "http:" + str2;
                        } else if (str2.startsWith("/")) {
                            str2 = "http://vodlock.co" + str2;
                        }
                        observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), str2, "HQ")));
                    }
                }
            }
        }
    }
}
