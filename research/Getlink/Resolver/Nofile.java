package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public class Nofile extends BaseResolver {
    public String a() {
        return "Nofile";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(HttpHelper.e().a(mediaSource.getStreamLink(), (Map<String, String>[]) new Map[0]), "<source[^>]*src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>", 1, 34);
        HashMap hashMap = new HashMap();
        hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap.put("Host", "nofile.io");
        hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        hashMap.put("User-Agent", Constants.f5838a);
        if (!a2.isEmpty()) {
            if (a2.startsWith("/")) {
                a2 = f() + a2;
            }
            ResolveResult resolveResult = new ResolveResult(a(), a2, mediaSource.getQuality());
            resolveResult.setPlayHeader(hashMap);
            observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
        }
    }

    public String f() {
        return "https://nofile.io";
    }
}
