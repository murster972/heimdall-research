package com.utils.Getlink.Resolver;

import android.util.Base64;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;

public class TheVideo extends PremiumResolver {
    private static String i;
    private static String j;

    class C50231 implements Log {
        C50231(TheVideo theVideo) {
        }
    }

    class C50242 implements Http {
        C50242(TheVideo theVideo) {
        }
    }

    private interface Http {
    }

    private interface Log {
    }

    private interface TheVideoDecoder {
        String a(String str, String str2);

        boolean isEnabled();
    }

    private String f() {
        String str = j;
        if (str == null || str.isEmpty()) {
            try {
                j = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Jlc29sdmVyL3RoZXZpZGVvbWUuanM=", 10), "UTF-8");
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                j = new String(Base64.decode("aHR0cHM6Ly9yYXcuZ2l0aHVidXNlcmNvbnRlbnQuY29tL1RlcnVTZXRlcGhlbi9jaW5lbWFhcGsvbWFzdGVyL3Jlc29sdmVyL3RoZXZpZGVvbWUuanM=", 10));
            }
        }
        return j;
    }

    public String a() {
        return "TheVideo";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)((?:thevideo\\.(?:me|cc|ch|us|io|net|website))|tvad\\.me|thevid\\.net)/(?:embed-|download/)?([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            super.a(mediaSource, observableEmitter);
            if (!Utils.d) {
                String str2 = "https://thevideo.me/embed-" + a2 + "-640x360.html";
                String a3 = HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0]);
                if (!a3.isEmpty() && !a3.contains("File was deleted") && !a3.contains("File not found") && !a3.contains("Page Cannot Be Found")) {
                    String str3 = i;
                    if (str3 == null || str3.isEmpty()) {
                        str = HttpHelper.e().a(f(), (Map<String, String>[]) new Map[0]);
                        i = str;
                    } else {
                        str = i;
                    }
                    if (!str.isEmpty()) {
                        Duktape create = Duktape.create();
                        try {
                            create.set("Log", Log.class, new C50231(this));
                            create.set("Http", Http.class, new C50242(this));
                            create.evaluate(str);
                            TheVideoDecoder theVideoDecoder = (TheVideoDecoder) create.get("TheVideoDecoder", TheVideoDecoder.class);
                            if (theVideoDecoder.isEnabled()) {
                                String a4 = theVideoDecoder.a(str2, a3);
                                if (a4.isEmpty()) {
                                    create.close();
                                    return;
                                }
                                Iterator<JsonElement> it2 = new JsonParser().a(a4).e().iterator();
                                while (it2.hasNext()) {
                                    JsonObject f = it2.next().f();
                                    JsonElement a5 = f.a("quality");
                                    JsonElement a6 = f.a("link");
                                    if (a6 != null && !a6.k() && a5 != null && !a5.k()) {
                                        String trim = a5.i().trim();
                                        String trim2 = a6.i().trim();
                                        if (a(trim2) && !trim2.contains(".srt") && !trim2.contains(".vtt") && !trim2.contains(".ass") && !trim2.contains(".zip") && !trim2.contains(".rar") && !trim2.contains(".7z")) {
                                            observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), trim2, trim)));
                                        }
                                    }
                                }
                                create.close();
                                return;
                            }
                            create.close();
                        } catch (Throwable th) {
                            create.close();
                            throw th;
                        }
                    }
                }
            }
        }
    }
}
