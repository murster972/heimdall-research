package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Gdriver extends PremiumResolver {
    public static String i = "";

    private static String f() {
        String str = i;
        if (str == null || str.isEmpty()) {
            i = HttpHelper.e().a("https://raw.githubusercontent.com/TeruSetephen/cinemaapk/master/resolver/bgd.js", (Map<String, String>[]) new Map[0]);
        }
        return i;
    }

    public String a() {
        return "GoogleVideo";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = HttpHelper.e().a(mediaSource.getStreamLink(), (Map<String, String>[]) new Map[0]);
        ArrayList arrayList = new ArrayList();
        if (JsUnpacker.m30920(a2)) {
            arrayList.addAll(JsUnpacker.m30918(a2));
        }
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        if (!arrayList.isEmpty()) {
            f();
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                String a3 = Regex.a((String) it2.next(), "data\\s*=\\s*(?:'|\")[{]([^{]+[^}])[}](?:'|\")", 1);
                if (!a3.isEmpty()) {
                    String str = ("var data = \"{" + a3.replace("\"", "\\\"") + "}\";\n") + i;
                    Duktape create = Duktape.create();
                    try {
                        Object evaluate = create.evaluate(str);
                        if (evaluate != null) {
                            ArrayList arrayList2 = new ArrayList();
                            if (JsUnpacker.m30920(a2)) {
                                arrayList2.addAll(JsUnpacker.m30918(evaluate.toString()));
                                if (arrayList2.size() > 0) {
                                    ArrayList arrayList3 = new ArrayList();
                                    Iterator it3 = Regex.b(((String) arrayList2.get(0)).toString(), "['\"]file['\"]\\s*:\\s*['\\\"]([^'\"]+[^'\"]+(?:res=default|res=\\d{3,4}|original=yes))['\"]", 1, true).get(0).iterator();
                                    while (it3.hasNext()) {
                                        String replace = ((String) it3.next()).replace("\\/", "/");
                                        if (!arrayList3.contains(replace)) {
                                            arrayList3.add(replace);
                                            ResolveResult resolveResult = new ResolveResult(a(), replace, mediaSource.getQuality());
                                            resolveResult.setPlayHeader(hashMap);
                                            observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
                                        }
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                    } finally {
                        create.close();
                    }
                }
            }
        }
    }
}
