package com.utils.Getlink.Resolver;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import com.unity3d.ads.metadata.MediationMetaData;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.vungle.warren.model.ReportDBAdapter;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Okru extends BaseResolver {
    private HashMap<String, String> f() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("mobile", "144p");
        hashMap.put("lowest", "240p");
        hashMap.put("low", "360p");
        hashMap.put("sd", "480p");
        hashMap.put("hd", "720p");
        hashMap.put("full", "1080p");
        hashMap.put("quad", "QuadHD");
        hashMap.put("ultra", "4K");
        return hashMap;
    }

    public String a() {
        return "ok.ru";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        JsonElement a2;
        JsonElement a3;
        String a4 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(ok\\.ru|odnoklassniki\\.ru)/(?:videoembed|video|moviePlayer)/([A-Za-z0-9-]+)", 2, 2);
        if (a4.isEmpty()) {
            a4 = Regex.a(mediaSource.getStreamLink(), "[\\?\\&]mid=(\\d+)", 1);
            if (a4.isEmpty()) {
                a4 = Regex.a(mediaSource.getStreamLink(), "st\\.mvId=(\\d+)", 1);
            }
        }
        if (!a4.isEmpty()) {
            HashMap hashMap = new HashMap();
            hashMap.put("Referer", "https://www.ok.ru/videoembed/" + a4);
            HttpHelper e = HttpHelper.e();
            String replaceAll = e.a("https://www.ok.ru/dk?cmd=videoPlayerMetadata&mid=" + Utils.a(a4, new boolean[0]), "cmd=videoPlayerMetadata&mid=" + Utils.a(a4, new boolean[0]), (Map<String, String>[]) new Map[]{hashMap}).replaceAll("[^\\x00-\\x7F]+", " ");
            if (!replaceAll.isEmpty() && !replaceAll.contains("error") && (a2 = new JsonParser().a(replaceAll)) != null && !a2.k() && a2.l() && (a3 = a2.f().a("videos")) != null && !a3.k() && a3.j()) {
                HashMap<String, String> f = f();
                Iterator<JsonElement> it2 = a3.e().iterator();
                ArrayList<ResolveResult> arrayList = new ArrayList<>();
                while (it2.hasNext()) {
                    JsonElement next = it2.next();
                    if (next != null && !next.k() && next.l()) {
                        String lowerCase = next.f().a(MediationMetaData.KEY_NAME).i().trim().toLowerCase();
                        String i = next.f().a(ReportDBAdapter.ReportColumns.COLUMN_URL).i();
                        if (!i.isEmpty()) {
                            String str = f.containsKey(lowerCase) ? f.get(lowerCase) : "HQ";
                            if (!b().contains(str)) {
                                ResolveResult resolveResult = new ResolveResult(a(), i, str);
                                HashMap hashMap2 = new HashMap();
                                hashMap2.put(TheTvdb.HEADER_ACCEPT, "*/*");
                                hashMap2.put("User-Agent", Constants.f5838a);
                                hashMap2.put("Origin", "https://www.ok.ru");
                                hashMap2.put("Referer", "https://www.ok.ru/");
                                hashMap2.put("Cookie", HttpHelper.e().a("https://www.ok.ru/"));
                                resolveResult.setPlayHeader(hashMap2);
                                arrayList.add(resolveResult);
                            }
                        }
                    }
                }
                if (arrayList.size() > 0) {
                    Collections.reverse(arrayList);
                    for (ResolveResult a5 : arrayList) {
                        observableEmitter.onNext(BaseResolver.a(mediaSource, a5));
                    }
                }
            }
        }
    }
}
