package com.utils.Getlink.Resolver;

import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import io.reactivex.ObservableEmitter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Movcloud extends BaseResolver {
    public String a() {
        return "Movcloud";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?:\\/|\\.)(movcloud)\\.(?:com|net|live|link|io)\\/(?:v|embed)\\/([0-9a-zA-Z-]+)", 2, 2);
        if (!a2.isEmpty()) {
            BaseProvider.i(streamLink);
            try {
                new URL(streamLink).getHost();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            HashMap hashMap = new HashMap();
            hashMap.put("referer", "origin");
            String replace = HttpHelper.e().a("https://api.movcloud.net/stream/" + a2, (Map<String, String>[]) new Map[]{hashMap}).replace("\\/", "/");
            ArrayList arrayList = new ArrayList();
            arrayList.add(replace);
            if (JsUnpacker.m30920(replace)) {
                arrayList.addAll(JsUnpacker.m30916(replace));
            }
            Iterator<ResolveResult> it2 = a(streamLink, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                observableEmitter.onNext(BaseResolver.a(mediaSource, next));
            }
        }
    }
}
