package com.utils.Getlink.Resolver;

public class Supervideo extends GenericResolver {
    public String a() {
        return "Supervideo";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/e/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    public String g() {
        return "(?:\\/|\\.)(supervideo)\\.(?:com|be|co|to|tv)\\/(?:e|v)\\/([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "https://supervideo.tv";
    }
}
