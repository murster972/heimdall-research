package com.utils.Getlink.Resolver;

public class FastPlay extends GenericResolver {
    public String a() {
        return "FastPlay";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String g() {
        return "(?://|\\.)(fastplay\\.(?:sx|cc|to))/(?:flash-|embed-)?([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "http://fastplay.to";
    }
}
