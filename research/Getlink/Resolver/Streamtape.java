package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import io.reactivex.ObservableEmitter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Streamtape extends BaseResolver {
    public String a() {
        return "Streamtape";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?:\\/|\\.)(streamtape)\\.(?:com|be|co|to|cc)\\/(?:v|e)\\/([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            String i = BaseProvider.i(streamLink);
            try {
                i = new URL(streamLink).getHost();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            String str = "https://" + i + "/e/" + a2;
            HashMap hashMap = new HashMap();
            hashMap.put("referer", str);
            hashMap.put("User-Agent", Constants.f5838a);
            String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
            ArrayList arrayList = new ArrayList();
            arrayList.add(a3);
            if (JsUnpacker.m30920(a3)) {
                arrayList.addAll(JsUnpacker.m30916(a3));
            }
            Element h = Jsoup.b(a3).h("div#videolink");
            if (h != null) {
                String G = h.G();
                if (G.startsWith("//")) {
                    G = "https://" + G;
                }
                ResolveResult resolveResult = new ResolveResult(a(), G, mediaSource.getQuality());
                resolveResult.setPlayHeader(hashMap);
                observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
            }
            if (JsUnpacker.m30920(a3)) {
                arrayList.addAll(JsUnpacker.m30916(a3));
            }
            Iterator<ResolveResult> it2 = a(str, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                if (next.getResolvedQuality() == null || !next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality("HD");
                } else {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                next.setPlayHeader(hashMap);
                observableEmitter.onNext(BaseResolver.a(mediaSource, next));
            }
        }
    }
}
