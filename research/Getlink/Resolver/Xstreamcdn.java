package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Xstreamcdn extends BaseResolver {
    public String a() {
        return "Xstreamcdn";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(xstreamcdn\\.com)/(?:v)?/?([0-9A-Za-z]+)", 2);
        if (!a2.isEmpty()) {
            HashMap<String, String> a3 = Constants.a();
            a3.put("referer", mediaSource.getStreamLink());
            HttpHelper e = HttpHelper.e();
            ArrayList<ArrayList<String>> b = Regex.b(e.a("https://xstreamcdn.com/api/source/" + a2, "r=&d=xstreamcdn.com", (Map<String, String>[]) new Map[]{a3}).replace("\\/", "/"), "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*['\"]?label['\"]?\\s*:\\s*['\"]?(\\d{3,4})p", 2, true);
            ArrayList arrayList = b.get(0);
            ArrayList arrayList2 = b.get(1);
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", Constants.f5838a);
            hashMap.put("referer", mediaSource.getStreamLink());
            for (int i = 0; i < arrayList.size(); i++) {
                try {
                    String str = (String) arrayList.get(i);
                    String str2 = (String) arrayList2.get(i);
                    if (!str.isEmpty()) {
                        ResolveResult resolveResult = new ResolveResult(a(), str, str2);
                        resolveResult.setPlayHeader(hashMap);
                        observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
                    }
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                }
            }
        }
    }
}
