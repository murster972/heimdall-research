package com.utils.Getlink.Resolver;

import com.facebook.common.util.UriUtil;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MovPod extends BaseResolver {
    public String a() {
        return "MovPod";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(movpod\\.(?:net|in))/(?:embed-)?([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            String str = "http://movpod.in/" + a2;
            String a3 = Utils.a((Map<String, String>) BaseResolver.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]), (String) null));
            HashMap hashMap = new HashMap();
            hashMap.put("Referer", str);
            String a4 = HttpHelper.e().a(str, a3, true, (Map<String, String>[]) new Map[]{hashMap});
            ArrayList arrayList = Regex.b(a4, "file:\\s+\"http(.+?)\"", 1).get(0);
            arrayList.addAll(Regex.b(a4, "\\{\\s*['\"]?src['\"]?:\\s*['\"]([^'\"]+)\\s*,?", 1, true).get(0));
            Iterator it2 = arrayList.iterator();
            ArrayList arrayList2 = new ArrayList();
            while (it2.hasNext()) {
                String str2 = (String) it2.next();
                if (!str2.isEmpty()) {
                    if (str2.startsWith("//")) {
                        str2 = "http:" + str2;
                    } else if (str2.startsWith(":")) {
                        str2 = UriUtil.HTTP_SCHEME + str2;
                    } else if (str2.startsWith("/")) {
                        str2 = "http://movpod.in" + str2;
                    }
                    if (!arrayList2.contains(str2)) {
                        arrayList2.add(str2);
                        observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), str2, "HQ")));
                    }
                }
            }
            if (arrayList2.isEmpty()) {
                Iterator<ResolveResult> it3 = a(str, a4, false, (HashMap<String, String>) null, new String[0][]).iterator();
                while (it3.hasNext()) {
                    observableEmitter.onNext(BaseResolver.a(mediaSource, it3.next()));
                }
            }
        }
    }
}
