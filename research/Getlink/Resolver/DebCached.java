package com.utils.Getlink.Resolver;

import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import io.reactivex.ObservableEmitter;

public class DebCached extends PremiumResolver {
    public String a() {
        return "Deb-Cached";
    }

    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        mediaSource.setHostName(a());
        try {
            if (!BaseResolver.f6518a) {
                if (!mediaSource.isTorrent()) {
                    PremiumResolver.b(mediaSource, observableEmitter, true, false, false);
                    return;
                }
            }
            PremiumResolver.a(mediaSource, observableEmitter, true, false, false);
        } catch (Exception e) {
            Logger.a(e.getMessage());
        }
    }
}
