package com.utils.Getlink.Resolver;

public class Jawcloud extends GenericResolver {
    public String a() {
        return "Jawcloud";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    public String g() {
        return "(?://|\\.)(jawcloud\\.co)/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://jawcloud.co";
    }
}
