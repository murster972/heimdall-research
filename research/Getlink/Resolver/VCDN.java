package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Map;

public class VCDN extends BaseResolver {
    public String a() {
        return "VCDN";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(vcdn\\.(?:com|io))/(?:v)/([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            ArrayList<ArrayList<String>> b = Regex.b(HttpHelper.e().a("https://vcdn.io/api/source/" + a2, "r=", false, (Map<String, String>[]) new Map[0]).replace("\\/", "/"), "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*['\"]?label['\"]?\\s*:\\s*['\"]?(\\d{3,4})p", 2, true);
            ArrayList arrayList = b.get(0);
            ArrayList arrayList2 = b.get(1);
            for (int i = 0; i < arrayList.size(); i++) {
                try {
                    String str = (String) arrayList.get(i);
                    if (str.startsWith("/")) {
                        str = "https://vcdn.io" + str;
                    }
                    observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), str, (String) arrayList2.get(i))));
                } catch (Throwable unused) {
                }
            }
        }
    }
}
