package com.utils.Getlink.Resolver;

public class Oogly extends GenericResolver {
    public String a() {
        return "Oogly";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    public String g() {
        return "(?:\\/|\\.)(oogly)\\.(?:cam|com|be|co|to|io)\\/(?:embed-)([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "https://oogly.io";
    }
}
