package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public class VShareEU extends BaseResolver {
    public String a() {
        return "VShareEU";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(vshare\\.eu)/(?:embed-|)?([0-9a-zA-Z/]+)", 2, 2);
        if (!a2.isEmpty()) {
            String str = "http://vshare.eu/" + a2;
            HashMap hashMap = new HashMap();
            hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
            String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap});
            if (!a3.contains("404 Not Found") && !a3.contains("could not be found") && !a3.contains("either expired or has been deleted") && !a3.contains("<div id=\"deleted\">")) {
                hashMap.put("Referer", str);
                HashMap<String, String> a4 = BaseResolver.a(a3, (String) null);
                a4.put("method_free", "Proceed to video");
                String a5 = HttpHelper.e().a(str, Utils.a((Map<String, String>) a4), false, (Map<String, String>[]) new Map[]{hashMap});
                ArrayList arrayList = new ArrayList();
                arrayList.add(a5);
                if (JsUnpacker.m30920(a5)) {
                    arrayList.addAll(JsUnpacker.m30916(a5));
                }
                ArrayList arrayList2 = new ArrayList();
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    String str2 = (String) it2.next();
                    ArrayList arrayList3 = Regex.b(str2, "file\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*flashplayer", 1, 34).get(0);
                    arrayList3.addAll(Regex.b(str2, "config\\s*:\\s*\\{\\s*file\\s*:\\s*[\"']([^\"']+)", 1, true).get(0));
                    arrayList3.addAll(Regex.b(str2, "<source\\s+src=['\"]([^'\"]+)['\"]", 1, true).get(0));
                    Iterator it3 = arrayList3.iterator();
                    while (it3.hasNext()) {
                        String replace = ((String) it3.next()).replace(" ", "%20");
                        if (!replace.endsWith(".vtt") && !replace.endsWith(".srt") && !replace.endsWith(".png") && !replace.endsWith(".jpg") && !arrayList2.contains(replace)) {
                            arrayList2.add(replace);
                            observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), replace, "HQ")));
                        }
                    }
                    Iterator<ResolveResult> it4 = a(str, str2, false, (HashMap<String, String>) null, new String[0][]).iterator();
                    while (it4.hasNext()) {
                        observableEmitter.onNext(BaseResolver.a(mediaSource, it4.next()));
                    }
                }
            }
        }
    }
}
