package com.utils.Getlink.Resolver;

public class Anavids extends GenericResolver {
    public String a() {
        return "Anavids";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    public String g() {
        return "(?:\\/|\\.)(anavids)\\.(?:com|be|co|to|tv)\\/(?:embed-)([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "https://anavids.com";
    }
}
