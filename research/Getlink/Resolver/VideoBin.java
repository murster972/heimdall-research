package com.utils.Getlink.Resolver;

public class VideoBin extends GenericResolver {
    public String a() {
        return "VideoBin";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return "https://videobin.co/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String g() {
        return "(?://|\\.)(videobin\\.(?:to|xyz|co))/(?:embed-)?([a-zA-Z0-9]+)";
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "https://videobin.co";
    }
}
