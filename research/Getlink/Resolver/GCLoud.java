package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GCLoud extends BaseResolver {
    public String a() {
        return "VidSrc";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?:\\/|\\.)(vidsrc\\.me)\\/(?:embed)\\/([0-9a-zA-Z-]+)", 2);
        if (!a2.isEmpty()) {
            String format = String.format("https://vidsrc.me/server1/%s/", new Object[]{a2});
            String format2 = String.format("https://vidsrc.me/watching?i=%s&srv=1", new Object[]{a2});
            HashMap hashMap = new HashMap();
            hashMap.put("referer", format);
            hashMap.put("user-agent", Constants.f5838a);
            String a3 = HttpHelper.a(format2, hashMap);
            String a4 = Regex.a(a3, "(?:\\/|\\.)(\\w+\\.\\w+)\\/(?:embed|v)\\/([0-9a-zA-Z-]+)", 2);
            try {
                str = new URL(a3).getHost();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                str = "https://vidsrc.me";
            }
            HashMap<String, String> a5 = Constants.a();
            hashMap.put("referer", a3);
            String a6 = HttpHelper.e().a(String.format("https://%s/api/source/%s", new Object[]{str, a4}), Utils.a(String.format("r=%s&d=%s", new Object[]{a3, str}), new boolean[0]), (Map<String, String>[]) new Map[]{a5});
            ArrayList arrayList = new ArrayList();
            arrayList.add(a6);
            if (JsUnpacker.m30920(a6)) {
                arrayList.addAll(JsUnpacker.m30916(a6));
            }
            Iterator<ResolveResult> it2 = a(a3, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                if (!next.getResolvedLink().endsWith("vidsrc.me")) {
                    MediaSource mediaSource2 = mediaSource;
                    observableEmitter.onNext(BaseResolver.a(mediaSource, next));
                }
            }
        }
    }
}
