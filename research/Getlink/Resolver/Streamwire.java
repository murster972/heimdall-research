package com.utils.Getlink.Resolver;

public class Streamwire extends GenericResolver {
    public String a() {
        return "Streamwire";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/e/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    public String g() {
        return "(?:\\/|\\.)(streamwire)\\.(?:com|be|co|to|tv|net)\\/(?:e|v|watch)\\/([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "https://streamwire.net";
    }
}
