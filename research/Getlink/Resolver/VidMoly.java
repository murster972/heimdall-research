package com.utils.Getlink.Resolver;

public class VidMoly extends GenericResolver {
    public String a() {
        return "VidMoly";
    }

    public boolean f() {
        return true;
    }

    public String g() {
        return "(?://|\\.)(vidmoly\\.(?:me|to))/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://vidmoly.me";
    }

    /* access modifiers changed from: protected */
    public String[] i() {
        return new String[]{".player,dl?op="};
    }
}
