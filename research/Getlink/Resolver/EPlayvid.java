package com.utils.Getlink.Resolver;

public class EPlayvid extends GenericResolver {
    public String a() {
        return "EPlayvid";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/watch/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    public String g() {
        return "(?:\\/|\\.)(eplayvid)\\.(?:com|be|co|to|tv)\\/(?:e|v|watch)\\/([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "http://eplayvid.com";
    }
}
