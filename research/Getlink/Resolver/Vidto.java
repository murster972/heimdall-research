package com.utils.Getlink.Resolver;

public class Vidto extends GenericResolver {
    public String a() {
        return "Vidto";
    }

    public boolean f() {
        return false;
    }

    public String g() {
        return "(?://|\\.)(vidto\\.me)/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "http://vidto.me";
    }
}
