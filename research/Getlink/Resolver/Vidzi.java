package com.utils.Getlink.Resolver;

public class Vidzi extends GenericResolver {
    public String a() {
        return "Vidzi";
    }

    public boolean f() {
        return false;
    }

    public String g() {
        return "(?://|\\.)(vidzi\\.(?:tv|cc|si))/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://vidzi.tv";
    }
}
