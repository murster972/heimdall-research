package com.utils.Getlink.Resolver;

import android.util.Base64;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.squareup.duktape.Duktape;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Map;

public class Openload extends PremiumResolver {
    private static String i;
    private static String j;

    class C50351 implements Log {
        C50351(Openload openload, Openload openload2) {
        }

        public void d(String str) {
        }
    }

    class C50362 implements JavaRegex {
        C50362(Openload openload, Openload openload2) {
        }

        public String findAll(String str, String str2, int i) {
            return new Gson().a((Object) Utils.a(str, str2, i));
        }

        public String findAllWithMode(String str, String str2, int i, int i2) {
            return new Gson().a((Object) Utils.b(str, str2, i, i2));
        }
    }

    class C50373 implements JavaUrlDecoder {
        C50373(Openload openload, Openload openload2) {
        }

        public String decode(String str) {
            try {
                return URLDecoder.decode(str, "UTF-8");
            } catch (Throwable th) {
                String decode = URLDecoder.decode(str);
                Logger.a(th, new boolean[0]);
                return decode;
            }
        }
    }

    private static String f() {
        String str = j;
        if (str == null || str.isEmpty()) {
            try {
                j = new String(Base64.decode(Utils.getOpenloadCode(), 10), "UTF-8");
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                j = new String(Base64.decode("Utils.getOpenloadCode()", 10));
            }
        }
        return j;
    }

    public String a() {
        return "openload";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        super.a(mediaSource, observableEmitter);
        if (!Utils.d) {
            HttpHelper e = HttpHelper.e();
            String a2 = Utils.a(mediaSource.getStreamLink(), "(?://|\\.)(o(?:pen)??load\\.(?:io|co|tv|stream|link|cloud|pw))/(?:embed|f)/([0-9a-zA-Z-_]+)", 2, 2);
            String a3 = e.a("https://openload.co/embed/" + a2, (Map<String, String>[]) new Map[0]);
            String str2 = i;
            if (str2 == null || str2.isEmpty()) {
                str = e.a(f(), (Map<String, String>[]) new Map[0]);
                i = str;
            } else {
                str = i;
            }
            Duktape create = Duktape.create();
            try {
                create.set("Log", Log.class, new C50351(this, this));
                create.set("JavaRegex", JavaRegex.class, new C50362(this, this));
                create.set("JavaUrlDecoder", JavaUrlDecoder.class, new C50373(this, this));
                create.evaluate(str);
                OpenloadDecoder openloadDecoder = (OpenloadDecoder) create.get("OpenloadDecoder", OpenloadDecoder.class);
                if (openloadDecoder.isEnabled()) {
                    String decode = openloadDecoder.decode(a3);
                    decode.isEmpty();
                    Iterator<JsonElement> it2 = new JsonParser().a(decode).e().iterator();
                    while (it2.hasNext()) {
                        String i2 = it2.next().i();
                        MediaSource cloneDeeply = mediaSource.cloneDeeply();
                        cloneDeeply.setStreamLink(i2);
                        cloneDeeply.setHostName("openload");
                        observableEmitter.onNext(cloneDeeply);
                    }
                }
            } catch (Throwable unused) {
            }
            create.close();
        }
    }
}
