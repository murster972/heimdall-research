package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Them4uFree extends BaseResolver {
    public String a() {
        return "Them4uFree";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        if (streamLink.contains("them4ufree")) {
            String a2 = Regex.a(streamLink, "\\?v=(.*)", 1);
            HashMap<String, String> a3 = Constants.a();
            a3.put("User-Agent", Constants.f5838a);
            a3.put("Origin", "http://them4ufree.com");
            a3.put("Referer", streamLink);
            a3.put(TheTvdb.HEADER_ACCEPT, "application/json, text/javascript, */*; q=0.01");
            HttpHelper e = HttpHelper.e();
            try {
                String str = new JSONObject(e.a("http://them4ufree.com/api-tt/playlist.php", "v=" + a2, true, (Map<String, String>[]) new Map[]{a3})).getString("playlist").toString();
                if (str != null) {
                    mediaSource.setStreamLink(str);
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            observableEmitter.onNext(mediaSource);
        }
    }
}
