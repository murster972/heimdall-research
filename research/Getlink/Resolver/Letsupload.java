package com.utils.Getlink.Resolver;

import android.util.Base64;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Letsupload extends BaseResolver {
    public String a() {
        try {
            return new String(Base64.decode("SHlkcmFY", 10), "UTF-8");
        } catch (Exception unused) {
            return new String(Base64.decode("SHlkcmFY", 10));
        }
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        mediaSource.getStreamLink();
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?:\\/|\\.)([0-9a-zA-Z-]+)\\.(?:com|be|live|to|co)\\/([0-9a-zA-Z-]+)", 2, 2);
        if (!a2.isEmpty()) {
            String str = BaseProvider.i(streamLink) + "/plugins/mediaplayer/site/_embed.php?u=" + a2 + "&w=640&h=320";
            ArrayList arrayList = new ArrayList();
            String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
            arrayList.add(a3);
            if (JsUnpacker.m30920(a3)) {
                arrayList.addAll(JsUnpacker.m30916(a3));
            }
            Iterator<ResolveResult> it2 = a(str, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                observableEmitter.onNext(BaseResolver.a(mediaSource, next));
            }
        }
    }
}
