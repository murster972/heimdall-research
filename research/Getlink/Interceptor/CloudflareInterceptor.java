package com.utils.Getlink.Interceptor;

import java.io.IOException;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Response;

public class CloudflareInterceptor implements Interceptor {
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response proceed = chain.proceed(chain.request());
        return (proceed.code() != 503 || proceed.header("Server") == null || !proceed.header("Server").toLowerCase().contains("cloudflare")) ? proceed : chain.proceed(proceed.request().newBuilder().cacheControl(CacheControl.FORCE_NETWORK).build());
    }
}
