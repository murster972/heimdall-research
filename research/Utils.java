package com.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Point;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import com.ads.videoreward.AdsManager;
import com.facebook.common.util.ByteConstants;
import com.facebook.common.util.UriUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.ItemHelpCaptcha;
import com.movie.data.model.tmvdb.FindResult;
import com.movie.ui.activity.BarcodeActivity;
import com.movie.ui.activity.SplashActivity;
import com.movie.ui.activity.settings.SettingsActivity;
import com.original.tase.Logger;
import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.crypto.AESEncrypter;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.player.BasePlayerHelper;
import com.original.tase.utils.NetworkUtils;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import com.utils.cast.CastHelper;
import com.yoku.marumovie.R;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import org.json.JSONObject;
import org.mozilla.universalchardet.CharsetListener;
import org.mozilla.universalchardet.UniversalDetector;
import timber.log.Timber;

public final class Utils {

    /* renamed from: a  reason: collision with root package name */
    public static int f6550a = 120;
    public static boolean b = false;
    public static boolean c = false;
    public static boolean d = true;
    public static final SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private static String f = "key_pref_captcha";
    public static ArrayList<ItemHelpCaptcha> g;
    public static ArrayList<Integer> h = new ArrayList<>();
    public static boolean i = false;
    private static String j = "";
    public static String k = "Cinema-AGENT";
    private static Context l;
    private static String m = "";
    private static int n = -1;
    public static LinkedHashMap<String, String> o = new LinkedHashMap<>();
    public static String p = "";

    /* renamed from: com.utils.Utils$5  reason: invalid class name */
    static /* synthetic */ class AnonymousClass5 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6552a = new int[RDTYPE.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.utils.Utils$RDTYPE[] r0 = com.utils.Utils.RDTYPE.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6552a = r0
                int[] r0 = f6552a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.utils.Utils$RDTYPE r1 = com.utils.Utils.RDTYPE.REAL_DEBRID     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6552a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.utils.Utils$RDTYPE r1 = com.utils.Utils.RDTYPE.ALL_DEBRID     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6552a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.utils.Utils$RDTYPE r1 = com.utils.Utils.RDTYPE.PREMIUMIZE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.utils.Utils.AnonymousClass5.<clinit>():void");
        }
    }

    public enum RDTYPE {
        REAL_DEBRID,
        ALL_DEBRID,
        PREMIUMIZE
    }

    static {
        new HashMap();
    }

    private Utils() {
        throw new AssertionError("No instances.");
    }

    public static boolean A() {
        return NetworkUtils.a();
    }

    public static boolean B() {
        ActivityManager activityManager = (ActivityManager) l.getSystemService("activity");
        if (Build.VERSION.SDK_INT < 19 || activityManager.isLowRamDevice() || Runtime.getRuntime().availableProcessors() < 4 || activityManager.getMemoryClass() < 128) {
            return false;
        }
        return true;
    }

    public static void C() {
        ArrayList<ItemHelpCaptcha> arrayList = g;
        if (arrayList == null || arrayList.isEmpty()) {
            PrefUtils.a(f, "");
        } else {
            PrefUtils.a(f, g);
        }
    }

    public static native String a(Context context);

    public static void a(String str, String str2) {
        a(new ItemHelpCaptcha(str2, str));
    }

    public static native void a877c(Context context, boolean z);

    public static native String a98c();

    public static native void ac(int i2);

    public static native String ae();

    public static boolean b(String str) {
        if (h == null) {
            h = new ArrayList<>();
        }
        int hashCode = str.hashCode();
        if (h.contains(Integer.valueOf(hashCode))) {
            return false;
        }
        h.add(Integer.valueOf(hashCode));
        return true;
    }

    public static void c(Context context) {
        l = context;
        a877c(context, false);
        saveOpenCout(PrefUtils.b(context));
    }

    public static String cc() {
        return PrefUtils.g(i());
    }

    public static native String ccc();

    public static void d() {
        ArrayList<Integer> arrayList = h;
        if (arrayList != null) {
            arrayList.clear();
        }
        h = null;
    }

    public static String de(String str) {
        return AESEncrypter.c(str).replace(" ", "");
    }

    public static String e(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            Calendar instance = Calendar.getInstance();
            instance.setTime(e.parse(str));
            return instance.getDisplayName(2, 2, Locale.ENGLISH) + " " + instance.get(1);
        } catch (ParseException e2) {
            Timber.a(e2, "Failed to parse release date.", new Object[0]);
            return "";
        }
    }

    public static String en(String str) {
        return AESEncrypter.b(str);
    }

    public static String f() {
        return getAndroidID(l);
    }

    public static String g() {
        return a98c();
    }

    public static native String getAndroidID(Context context);

    public static native String getApiKey2();

    public static native String getConfigKey();

    public static native String getFallbackCf();

    public static native String getOpenloadCode();

    public static native String getProvider(int i2);

    public static native String getRedirectList();

    public static native String getServerUrl(boolean z);

    public static List<BaseProvider> h() {
        int i2;
        Set<String> r = r();
        Set<String> stringSet = FreeMoviesApp.l().getStringSet("pref_choose_provider_disabled", new HashSet());
        stringSet.remove(new String("ZeroTV"));
        HashSet hashSet = new HashSet();
        Iterator<String> it2 = r.iterator();
        while (true) {
            i2 = 0;
            if (!it2.hasNext()) {
                break;
            }
            String next = it2.next();
            Iterator<String> it3 = stringSet.iterator();
            while (true) {
                if (it3.hasNext()) {
                    if (next.equals(it3.next())) {
                        i2 = 1;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (i2 == 0) {
                hashSet.add(next);
            }
        }
        ArrayList arrayList = new ArrayList();
        BaseProvider[] baseProviderArr = BaseProvider.b;
        int length = baseProviderArr.length;
        while (i2 < length) {
            BaseProvider baseProvider = baseProviderArr[i2];
            Iterator it4 = hashSet.iterator();
            while (true) {
                if (it4.hasNext()) {
                    if (((String) it4.next()).toLowerCase().contains(baseProvider.a().toLowerCase())) {
                        arrayList.add(baseProvider);
                        break;
                    }
                } else {
                    break;
                }
            }
            i2++;
        }
        return arrayList;
    }

    public static Context i() {
        return l;
    }

    public static String j() {
        if (j.isEmpty()) {
            String str = Build.MANUFACTURER;
            String str2 = Build.MODEL;
            if (str2.startsWith(str)) {
                j = a(str2);
            } else {
                j = a(str) + "-" + str2;
            }
        }
        return j;
    }

    public static native String jha();

    public static Set<String> k() {
        return FreeMoviesApp.l().getStringSet("pref_choose_provider_disabled", r());
    }

    public static String k87x7() {
        return l.getPackageName();
    }

    public static String l() {
        String string = FreeMoviesApp.l().getString("pref_default_domain_gg", "");
        if (string.isEmpty()) {
            String a2 = a();
            Iterator<Map.Entry<String, String>> it2 = p().entrySet().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                Map.Entry next = it2.next();
                if (a2.toLowerCase().equals((String) next.getKey())) {
                    string = (String) next.getValue();
                    break;
                }
            }
        }
        if (string.isEmpty()) {
            string = "https://google.ch";
        }
        if (!string.startsWith(UriUtil.HTTPS_SCHEME)) {
            string = "https://" + string;
        }
        FreeMoviesApp.l().edit().putString("pref_default_domain_gg", string).apply();
        return string;
    }

    public static long m() {
        long j2;
        long j3;
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            j3 = statFs.getBlockSizeLong();
            j2 = statFs.getAvailableBlocksLong();
        } else {
            j3 = (long) statFs.getBlockSize();
            j2 = (long) statFs.getAvailableBlocks();
        }
        return j3 * j2;
    }

    public static String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b2 : digest) {
                String hexString = Integer.toHexString(b2 & 255);
                while (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                sb.append(hexString);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static String n() {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File file = new File(externalStorageDirectory.getAbsolutePath() + "/cinemahd/memberkey.txt");
        if (!file.exists()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            bufferedReader.close();
        } catch (IOException unused) {
        }
        return sb.toString();
    }

    public static String o() {
        int ipAddress;
        try {
            WifiInfo connectionInfo = ((WifiManager) l.getApplicationContext().getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo != null && (ipAddress = connectionInfo.getIpAddress()) > 0) {
                if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                    ipAddress = Integer.reverseBytes(ipAddress);
                }
                return InetAddress.getByAddress(BigInteger.valueOf((long) ipAddress).toByteArray()).getHostAddress();
            }
        } catch (Exception e2) {
            Logger.a((Throwable) e2, new boolean[0]);
        }
        try {
            Iterator<T> it2 = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
            while (it2.hasNext()) {
                Iterator<T> it3 = Collections.list(((NetworkInterface) it2.next()).getInetAddresses()).iterator();
                while (true) {
                    if (it3.hasNext()) {
                        InetAddress inetAddress = (InetAddress) it3.next();
                        if (!inetAddress.isLoopbackAddress()) {
                            String hostAddress = inetAddress.getHostAddress();
                            if (hostAddress.indexOf(58) < 0) {
                                return hostAddress;
                            }
                        }
                    }
                }
            }
            return "";
        } catch (Exception e3) {
            Logger.a((Throwable) e3, new boolean[0]);
            return "";
        }
    }

    public static LinkedHashMap<String, String> p() {
        if (o.size() > 0) {
            return o;
        }
        o.put("ac", "google.ac");
        o.put("ad", "google.ad");
        o.put("ae", "google.ae");
        o.put("af", "google.com.af");
        o.put("ag", "google.com.ag");
        o.put("ai", "google.com.ai");
        o.put("al", "google.al");
        o.put("am", "google.am");
        o.put("ao", "google.co.ao");
        o.put("ar", "google.com.ar");
        o.put("as", "google.as");
        o.put("at", "google.at");
        o.put("au", "google.com.au");
        o.put("az", "google.az");
        o.put("ba", "google.ba");
        o.put("bd", "google.com.bd");
        o.put("be", "google.be");
        o.put("bf", "google.bf");
        o.put("bg", "google.bg");
        o.put("bh", "google.com.bh");
        o.put("bi", "google.bi");
        o.put("bj", "google.bj");
        o.put("bn", "google.com.bn");
        o.put("bo", "google.com.bo");
        o.put("br", "google.com.br");
        o.put("bs", "google.bs");
        o.put("bt", "google.bt");
        o.put("bw", "google.co.bw");
        o.put("by", "google.by");
        o.put("bz", "google.com.bz");
        o.put("ca", "google.ca");
        o.put("kh", "google.com.kh");
        o.put("cc", "google.cc");
        o.put("cd", "google.cd");
        o.put("cf", "google.cf");
        o.put("cat", "google.cat");
        o.put("cg", "google.cg");
        o.put("ch", "google.ch");
        o.put("ci", "google.ci");
        o.put("ck", "google.co.ck");
        o.put("cl", "google.cl");
        o.put("cm", "google.cm");
        o.put("cn", "google.com.hk");
        o.put("co", "google.com.co");
        o.put("cr", "google.co.cr");
        o.put("cu", "google.com.cu");
        o.put("cv", "google.cv");
        o.put("cy", "google.com.cy");
        o.put("cz", "google.cz");
        o.put("de", "google.de");
        o.put("dj", "google.dj");
        o.put("dk", "google.dk");
        o.put("dm", "google.dm");
        o.put("do", "google.com.do");
        o.put("dz", "google.dz");
        o.put("ec", "google.com.ec");
        o.put("ee", "google.ee");
        o.put("eg", "google.com.eg");
        o.put("es", "google.es");
        o.put("et", "google.com.et");
        o.put("fi", "google.fi");
        o.put("fj", "google.com.fj");
        o.put("fm", "google.fm");
        o.put("fr", "google.fr");
        o.put("ga", "google.ga");
        o.put("ge", "google.ge");
        o.put("gf", "google.gf");
        o.put("gg", "google.gg");
        o.put("gh", "google.com.gh");
        o.put("gi", "google.com.gi");
        o.put("gl", "google.gl");
        o.put("gm", "google.gm");
        o.put("gp", "google.gp");
        o.put("gr", "google.gr");
        o.put("gt", "google.com.gt");
        o.put("gy", "google.gy");
        o.put("hk", "google.com.hk");
        o.put("hn", "google.hn");
        o.put("hr", "google.hr");
        o.put("ht", "google.ht");
        o.put("hu", "google.hu");
        o.put("id", "google.co.id");
        o.put("iq", "google.iq");
        o.put("ie", "google.ie");
        o.put("il", "google.co.il");
        o.put("im", "google.im");
        o.put("in", "google.co.in");
        o.put("io", "google.io");
        o.put("is", "google.is");
        o.put("it", "google.it");
        o.put("je", "google.je");
        o.put("jm", "google.com.jm");
        o.put("jo", "google.jo");
        o.put("jp", "google.co.jp");
        o.put("ke", "google.co.ke");
        o.put("ki", "google.ki");
        o.put("kg", "google.kg");
        o.put("kr", "google.co.kr");
        o.put("kw", "google.com.kw");
        o.put("kz", "google.kz");
        o.put("la", "google.la");
        o.put("lb", "google.com.lb");
        o.put("lc", "google.com.lc");
        o.put("li", "google.li");
        o.put("lk", "google.lk");
        o.put("ls", "google.co.ls");
        o.put("lt", "google.lt");
        o.put("lu", "google.lu");
        o.put("lv", "google.lv");
        o.put("ly", "google.com.ly");
        o.put("ma", "google.co.ma");
        o.put("md", "google.md");
        o.put("me", "google.me");
        o.put("mg", "google.mg");
        o.put("mk", "google.mk");
        o.put("ml", "google.ml");
        o.put("mm", "google.com.mm");
        o.put("mn", "google.mn");
        o.put("ms", "google.ms");
        o.put("mt", "google.com.mt");
        o.put("mu", "google.mu");
        o.put("mv", "google.mv");
        o.put("mw", "google.mw");
        o.put("mx", "google.com.mx");
        o.put("my", "google.com.my");
        o.put("mz", "google.co.mz");
        o.put("na", "google.com.na");
        o.put("ne", "google.ne");
        o.put("nf", "google.com.nf");
        o.put("ng", "google.com.ng");
        o.put("ni", "google.com.ni");
        o.put("nl", "google.nl");
        o.put("no", "google.no");
        o.put("np", "google.com.np");
        o.put("nr", "google.nr");
        o.put("nu", "google.nu");
        o.put("nz", "google.co.nz");
        o.put("om", "google.com.om");
        o.put("pk", "google.com.pk");
        o.put("pa", "google.com.pa");
        o.put("pe", "google.com.pe");
        o.put("ph", "google.com.ph");
        o.put("pl", "google.pl");
        o.put("pg", "google.com.pg");
        o.put("pn", "google.pn");
        o.put("pr", "google.com.pr");
        o.put("ps", "google.ps");
        o.put("pt", "google.pt");
        o.put("py", "google.com.py");
        o.put("qa", "google.com.qa");
        o.put("ro", "google.ro");
        o.put("rs", "google.rs");
        o.put("ru", "google.ru");
        o.put("rw", "google.rw");
        o.put("sa", "google.com.sa");
        o.put("sb", "google.com.sb");
        o.put("sc", "google.sc");
        o.put("se", "google.se");
        o.put("sg", "google.com.sg");
        o.put("sh", "google.sh");
        o.put("si", "google.si");
        o.put("sk", "google.sk");
        o.put("sl", "google.com.sl");
        o.put("sn", "google.sn");
        o.put("sm", "google.sm");
        o.put("so", "google.so");
        o.put("st", "google.st");
        o.put("sr", "google.sr");
        o.put("sv", "google.com.sv");
        o.put("td", "google.td");
        o.put("tg", "google.tg");
        o.put("th", "google.co.th");
        o.put("tj", "google.com.tj");
        o.put("tk", "google.tk");
        o.put("tl", "google.tl");
        o.put("tm", "google.tm");
        o.put("to", "google.to");
        o.put("tn", "google.tn");
        o.put("tr", "google.com.tr");
        o.put("tt", "google.tt");
        o.put("tw", "google.com.tw");
        o.put("tz", "google.co.tz");
        o.put("ua", "google.com.ua");
        o.put("ug", "google.co.ug");
        o.put("uk", "google.co.uk");
        o.put("com", "google.com");
        o.put("uy", "google.com.uy");
        o.put("uz", "google.co.uz");
        o.put("vc", "google.com.vc");
        o.put("ve", "google.co.ve");
        o.put("vg", "google.vg");
        o.put("vi", "google.co.vi");
        o.put("vn", "google.com.vn");
        o.put("vu", "google.vu");
        o.put("ws", "google.ws");
        o.put("za", "google.co.za");
        o.put("zm", "google.co.zm");
        o.put("zw", "google.co.zw");
        return o;
    }

    public static String p3434() {
        try {
            String str = "";
            for (Signature byteArray : l.getPackageManager().getPackageInfo(l.getPackageName(), 64).signatures) {
                MessageDigest instance = MessageDigest.getInstance("SHA");
                instance.update(byteArray.toByteArray());
                str = new String(Base64.encode(instance.digest(), 0));
                str.length();
            }
            return str;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("name not found", e2.toString());
            return "";
        } catch (NoSuchAlgorithmException e3) {
            Log.e("no such an algorithm", e3.toString());
            return "";
        } catch (Exception e4) {
            Log.e("exception", e4.toString());
            return "";
        }
    }

    public static String q() {
        String string = FreeMoviesApp.l().getString("pref_cc_unlock_full_version", "");
        if (!string.isEmpty()) {
            return string;
        }
        String n2 = n();
        if (n2.indexOf(" ") >= 0) {
            return "";
        }
        FreeMoviesApp.l().edit().putString("pref_cc_unlock_full_version", n2).apply();
        return n2;
    }

    public static Set<String> r() {
        boolean z;
        String list = GlobalVariable.c().a().getProvider().getList();
        Set<String> stringSet = FreeMoviesApp.l().getStringSet("pref_addition_providers", new HashSet());
        HashSet hashSet = new HashSet();
        hashSet.addAll(stringSet);
        for (String str : list.split(",")) {
            Iterator<String> it2 = stringSet.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (str.toLowerCase().equals(it2.next().toLowerCase())) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!str.isEmpty() && !z) {
                hashSet.add(str);
            }
        }
        return hashSet;
    }

    public static String s() {
        return k87x7();
    }

    public static native void saveOpenCout(int i2);

    public static LinkedHashMap<String, Integer> t() {
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("Small", 10);
        linkedHashMap.put("Medium", 8);
        linkedHashMap.put("Large", 5);
        linkedHashMap.put("Extra Large", 3);
        return linkedHashMap;
    }

    public static String u() {
        return getServerUrl(i);
    }

    public static int v() {
        if (n == -1) {
            try {
                PackageInfo packageInfo = l.getPackageManager().getPackageInfo(s(), 0);
                m = packageInfo.versionName;
                n = packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
        }
        return n;
    }

    public static String w() {
        if (m.isEmpty()) {
            try {
                PackageInfo packageInfo = l.getPackageManager().getPackageInfo(s(), 0);
                m = packageInfo.versionName;
                n = packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
        }
        return m;
    }

    public static ArrayList<ItemHelpCaptcha> x() {
        ItemHelpCaptcha[] itemHelpCaptchaArr = (ItemHelpCaptcha[]) PrefUtils.a(f, (Class<T[]>) ItemHelpCaptcha[].class);
        if (g == null) {
            g = new ArrayList<>();
        }
        if (itemHelpCaptchaArr != null) {
            g.addAll(Arrays.asList(itemHelpCaptchaArr));
        }
        return g;
    }

    public static ArrayList y() {
        return new ArrayList(Arrays.asList(GlobalVariable.c().a().getProvider().getList().split(",")));
    }

    public static boolean z() {
        return GoogleApiAvailability.a().c(l) == 0;
    }

    public static void a(ItemHelpCaptcha itemHelpCaptcha) {
        if (g == null) {
            g = new ArrayList<>();
        }
        boolean z = false;
        Iterator<ItemHelpCaptcha> it2 = g.iterator();
        while (true) {
            if (it2.hasNext()) {
                if (it2.next().getProviderName().equals(itemHelpCaptcha.getProviderName())) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (!z) {
            g.add(itemHelpCaptcha);
        }
    }

    public static String f(String str) {
        try {
            String host = new URI(str).getHost();
            int lastIndexOf = host.lastIndexOf(".");
            if (lastIndexOf > -1) {
                host = host.substring(0, lastIndexOf);
            }
            return host.startsWith("www.") ? host.substring(4) : host;
        } catch (URISyntaxException unused) {
            return "UNKNOW HOST";
        }
    }

    public static String g(String str) {
        String lowerCase = Regex.a(str, "(\\.\\w{3,4})(?:$|\\?)", 1).trim().toLowerCase();
        if (lowerCase.isEmpty()) {
            return ".mp4";
        }
        if (lowerCase.equalsIgnoreCase(".avi") || lowerCase.equalsIgnoreCase(".rmvb") || lowerCase.equalsIgnoreCase(".flv") || lowerCase.equalsIgnoreCase(".mkv") || lowerCase.equalsIgnoreCase(".mp2") || lowerCase.equalsIgnoreCase(".mp2v") || lowerCase.equalsIgnoreCase(".mp4") || lowerCase.equalsIgnoreCase(".mp4v") || lowerCase.equalsIgnoreCase(".mpe") || lowerCase.equalsIgnoreCase(".mpeg") || lowerCase.equalsIgnoreCase(".mpeg1") || lowerCase.equalsIgnoreCase(".mpeg2") || lowerCase.equalsIgnoreCase(".mpeg4") || lowerCase.equalsIgnoreCase(".mpg") || lowerCase.equalsIgnoreCase(".ts") || lowerCase.equalsIgnoreCase(".m3u8")) {
            return lowerCase;
        }
        return ".mp4";
    }

    public static boolean i(String str) {
        try {
            l.getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static FindResult c(String str) {
        String str2 = "https://api.themoviedb.org/3/find/" + str;
        String string = FreeMoviesApp.l().getString("last_tmdb_api_key", GlobalVariable.c().a().getTmdb_api_keys().get(0));
        return (FindResult) new Gson().a(HttpHelper.e().a(str2 + "?language=" + "en-US" + "&api_key=" + string + "&external_source=imdb_id", (Map<String, String>[]) new Map[0]), FindResult.class);
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0048 A[SYNTHETIC, Splitter:B:28:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0052 A[SYNTHETIC, Splitter:B:33:0x0052] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String d(java.lang.String r5) {
        /*
            java.lang.String r0 = "UTF-8"
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x003f, all -> 0x003b }
            r2.<init>(r5)     // Catch:{ Exception -> 0x003f, all -> 0x003b }
            com.utils.UnicodeBOMInputStream r5 = new com.utils.UnicodeBOMInputStream     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0036, all -> 0x0033 }
            java.lang.String r1 = a((java.io.InputStream) r5)     // Catch:{ Exception -> 0x0031 }
            if (r1 != 0) goto L_0x001b
            com.utils.UnicodeBOMInputStream$BOM r1 = r5.s()     // Catch:{ Exception -> 0x0031 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0031 }
        L_0x001b:
            java.lang.String r3 = "NONE"
            boolean r3 = r1.equals(r3)     // Catch:{ Exception -> 0x0031 }
            if (r3 == 0) goto L_0x0024
            goto L_0x0025
        L_0x0024:
            r0 = r1
        L_0x0025:
            r2.close()     // Catch:{ IOException -> 0x002c }
            r5.close()     // Catch:{ IOException -> 0x002c }
            goto L_0x004e
        L_0x002c:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x004e
        L_0x0031:
            r1 = move-exception
            goto L_0x0043
        L_0x0033:
            r0 = move-exception
            r5 = r1
            goto L_0x0050
        L_0x0036:
            r5 = move-exception
            r4 = r1
            r1 = r5
            r5 = r4
            goto L_0x0043
        L_0x003b:
            r0 = move-exception
            r5 = r1
            r2 = r5
            goto L_0x0050
        L_0x003f:
            r5 = move-exception
            r2 = r1
            r1 = r5
            r5 = r2
        L_0x0043:
            r1.printStackTrace()     // Catch:{ all -> 0x004f }
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ IOException -> 0x002c }
            r5.close()     // Catch:{ IOException -> 0x002c }
        L_0x004e:
            return r0
        L_0x004f:
            r0 = move-exception
        L_0x0050:
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x0059 }
            r5.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x005d
        L_0x0059:
            r5 = move-exception
            r5.printStackTrace()
        L_0x005d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Utils.d(java.lang.String):java.lang.String");
    }

    public static String b() {
        return a(l);
    }

    public static LinkedHashMap<String, Integer> e() {
        return a(false);
    }

    public static String a(List<String> list, String str, StringBuilder sb) {
        if (list == null) {
            return "";
        }
        sb.setLength(0);
        if (!Lists.a(list)) {
            for (String next : list) {
                if (sb.length() > 0) {
                    sb.append(str);
                }
                sb.append(next);
            }
        }
        return sb.toString();
    }

    public static ArrayList<String> b(String str, String str2, int i2, int i3) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (str != null && !str.isEmpty()) {
            Matcher matcher = Pattern.compile(str2, i3).matcher(str);
            while (matcher.find()) {
                arrayList.add(matcher.group(i2));
            }
        }
        return arrayList;
    }

    public static void e(Context context) {
        context.stopService(MyUnboundService.a(context));
    }

    public static void c(Activity activity) {
        activity.startActivity(new Intent(activity, SplashActivity.class));
        b((Context) activity);
        AdsManager.l().a();
        activity.finishAffinity();
    }

    public static void j(String str) {
        FreeMoviesApp.l().edit().putString("pref_cc_unlock_full_version", str).apply();
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File file = new File(externalStorageDirectory.getAbsolutePath() + "/cinemahd");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            new FileOutputStream(new File(file, "memberkey.txt")).write(str.getBytes());
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    public static void b(Activity activity) {
        Window window = activity.getWindow();
        if (window != null) {
            window.clearFlags(2048);
            window.addFlags(ByteConstants.KB);
            View decorView = window.getDecorView();
            if (decorView != null) {
                int systemUiVisibility = decorView.getSystemUiVisibility();
                if (Build.VERSION.SDK_INT >= 14) {
                    systemUiVisibility |= 1;
                }
                if (Build.VERSION.SDK_INT >= 16) {
                    systemUiVisibility |= 2;
                }
                if (Build.VERSION.SDK_INT >= 19) {
                    systemUiVisibility |= 4096;
                }
                decorView.setSystemUiVisibility(systemUiVisibility);
            }
        }
    }

    public static void a(ContextWrapper contextWrapper) {
        if (contextWrapper.getClass() != FreeMoviesApp.class) {
            a877c(contextWrapper, true);
        }
    }

    public static boolean c() {
        for (String i2 : GlobalVariable.c().a().getBlocks_package()) {
            if (i(i2)) {
                return true;
            }
        }
        return false;
    }

    public static String h(String str) {
        return Regex.a(str, "(2160|4K|1080|720|480|360|CAM|2K)", 1);
    }

    public static ArrayList<String> a(String str, String str2, int i2) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (str != null && !str.isEmpty()) {
            Matcher matcher = Pattern.compile(str2).matcher(str);
            while (matcher.find()) {
                arrayList.add(matcher.group(i2));
            }
        }
        return arrayList;
    }

    public static void c(final Activity activity, String str) {
        String str2;
        String str3;
        if (str.contains("429")) {
            str2 = "Error 429";
            str3 = "Many requests to premium server. \nYou can reduce it in Setting/Limit Request calls per second";
        } else if (str.contains("403")) {
            str2 = "Error 403";
            str3 = "Permission denied (or account locked).\nPlease check your subscription";
        } else if (str.contains("503")) {
            str2 = "Error 503";
            str3 = "Having issue with Premium server";
        } else {
            str3 = str;
            str2 = "Error";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.b((CharSequence) str2);
        builder.a((CharSequence) str3);
        builder.a(false);
        builder.b((CharSequence) "Close", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.a((CharSequence) "Setting", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                activity.startActivity(new Intent(activity, SettingsActivity.class));
            }
        });
        builder.c();
    }

    public static void d(Activity activity) {
        View decorView;
        Window window = activity.getWindow();
        if (window != null && (decorView = window.getDecorView()) != null) {
            int systemUiVisibility = decorView.getSystemUiVisibility();
            if (Build.VERSION.SDK_INT >= 14) {
                systemUiVisibility &= -2;
            }
            if (Build.VERSION.SDK_INT >= 16) {
                systemUiVisibility &= -3;
            }
            decorView.setSystemUiVisibility(systemUiVisibility);
        }
    }

    public static String a(String str, String str2, int i2, int i3) {
        if (str != null && !str.isEmpty()) {
            Matcher matcher = Pattern.compile(str2, i3).matcher(str);
            if (matcher.find()) {
                return matcher.group(i2);
            }
        }
        return "";
    }

    public static long b(File file) {
        long j2;
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            j2 = Build.VERSION.SDK_INT >= 18 ? (statFs.getBlockSizeLong() * statFs.getBlockSizeLong()) / 50 : (((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 50;
        } catch (IllegalArgumentException unused) {
            j2 = 10485760;
        }
        return Math.max(Math.min(j2, 52428800), 10485760);
    }

    public static String a(long j2, boolean z) {
        String str;
        String str2 = z ? "https://api.themoviedb.org/3/movie/" : "https://api.themoviedb.org/3/tv/";
        try {
            String string = FreeMoviesApp.l().getString("last_tmdb_api_key", GlobalVariable.c().a().getTmdb_api_keys().get(0));
            HttpHelper e2 = HttpHelper.e();
            String a2 = e2.a(str2 + j2 + "?append_to_response=external_ids,content_ratings,videos&language=" + "en-US" + "&api_key=" + string, (Map<String, String>[]) new Map[0]);
            boolean isEmpty = Regex.a(a2, "\\\"overview\\\"\\s*:\\s*(null|\\\"\\\")\\s*,", 1).isEmpty();
            if (a2.isEmpty()) {
                return "-1";
            }
            JSONObject jSONObject = new JSONObject(a2);
            if (z) {
                str = jSONObject.getString("imdb_id");
            } else {
                str = jSONObject.getJSONObject("external_ids").getString("imdb_id");
            }
            if (!str.isEmpty()) {
                return str.replaceAll("[^0-9]", "");
            }
            return "-1";
        } catch (Exception e3) {
            Logger.a((Throwable) e3, new boolean[0]);
            return "-1";
        }
    }

    public static void d(Context context) {
        a(context, MyUnboundService.a(context));
    }

    public static void b(Context context) {
        try {
            a(context.getCacheDir());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void b(Activity activity, String str) {
        activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    public static String a() {
        String str = p;
        if (str == null || str.isEmpty()) {
            try {
                TelephonyManager telephonyManager = (TelephonyManager) i().getSystemService("phone");
                p = telephonyManager.getSimCountryIso().trim();
                if (p == null || p.isEmpty()) {
                    p = telephonyManager.getNetworkCountryIso().trim();
                }
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
        }
        return p;
    }

    public static int a(Activity activity) {
        Point point = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(point);
        int i2 = point.x;
        int i3 = point.y;
        if (i2 < i3) {
            return 1;
        }
        return i2 > i3 ? 2 : 3;
    }

    public static LinkedHashMap<String, Integer> a(boolean z) {
        String string = FreeMoviesApp.l().getString("pref_choose_default_player", BasePlayerHelper.d().b());
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();
        if (z) {
            linkedHashMap.put("Always ask", -1);
            linkedHashMap.put("Play", 0);
            if (!string.equalsIgnoreCase("cinema")) {
                linkedHashMap.put("Play with subtitles", 1);
            }
        } else if (!CastHelper.a(i())) {
            linkedHashMap.put("Play", 0);
            if (!string.equalsIgnoreCase("cinema")) {
                linkedHashMap.put("Play with subtitles", 1);
            }
        } else {
            linkedHashMap.put("Cast", 5);
            linkedHashMap.put("Cast with subtitles", 6);
        }
        linkedHashMap.put("Open with...", 2);
        linkedHashMap.put("Download", 3);
        linkedHashMap.put("Copy to Clipboard", 4);
        if (FreeMoviesApp.l().getBoolean("use_player_plugin", false)) {
            linkedHashMap.put("Open with Player Plugin", 7);
        }
        return linkedHashMap;
    }

    public static String a(InputStream inputStream) throws IOException {
        if (!inputStream.markSupported()) {
            inputStream = new BufferedInputStream(inputStream);
        }
        UniversalDetector universalDetector = new UniversalDetector((CharsetListener) null);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0 || universalDetector.c()) {
                universalDetector.a();
                String b2 = universalDetector.b();
            } else {
                universalDetector.a(bArr, 0, read);
            }
        }
        universalDetector.a();
        String b22 = universalDetector.b();
        if (b22 != null) {
            PrintStream printStream = System.out;
            printStream.println("Detected encoding = " + b22);
        } else {
            System.out.println("No encoding detected.");
        }
        universalDetector.d();
        return b22;
    }

    public static boolean a(ArrayList<String> arrayList) {
        if (arrayList == null) {
            return false;
        }
        for (ApplicationInfo next : l.getPackageManager().getInstalledApplications(0)) {
            Iterator<String> it2 = arrayList.iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (next.packageName.equals(it2.next())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean a(String str, Set<String> set) {
        for (String lowerCase : set) {
            if (lowerCase.toLowerCase().contains(str.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public static void a(Activity activity, String str, DialogInterface.OnClickListener onClickListener, EditText editText, DialogInterface.OnDismissListener onDismissListener) {
        editText.setInputType(129);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.b((CharSequence) str);
        builder.b((View) editText);
        builder.b((CharSequence) "Ok", onClickListener);
        if (onDismissListener != null) {
            builder.a(onDismissListener);
        }
        builder.c();
    }

    public static float a(double d2, int i2) {
        return new BigDecimal(Double.toString(d2)).setScale(i2, 4).floatValue();
    }

    private static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String str2 = "";
        boolean z = true;
        for (char c2 : str.toCharArray()) {
            if (!z || !Character.isLetter(c2)) {
                if (Character.isWhitespace(c2)) {
                    z = true;
                }
                str2 = str2 + c2;
            } else {
                str2 = str2 + Character.toUpperCase(c2);
                z = false;
            }
        }
        return str2;
    }

    public static void a(TextView textView, String str, String str2, int i2, float f2) {
        textView.setText(str, TextView.BufferType.SPANNABLE);
        Spannable spannable = (Spannable) textView.getText();
        int indexOf = str.indexOf(str2);
        spannable.setSpan(new ForegroundColorSpan(i2), indexOf, str2.length() + indexOf, 33);
        spannable.setSpan(new RelativeSizeSpan(f2), indexOf, str2.length() + indexOf, 33);
    }

    public static void a(Activity activity, String str, boolean z) {
        ((ClipboardManager) activity.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("Copied Address", str));
        a(activity, "Copied " + str);
        if (z) {
            Intent intent = new Intent(activity, BarcodeActivity.class);
            intent.putExtra("extra_barcode", str);
            activity.startActivity(intent);
        }
    }

    public static boolean a(RDTYPE rdtype) {
        int i2 = AnonymousClass5.f6552a[rdtype.ordinal()];
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 == 3 && PremiumizeCredentialsHelper.b().isValid() && FreeMoviesApp.l().getBoolean("pref_premiumize_type", false) && PremiumizeCredentialsHelper.b().getPremium_until() > 0) {
                    return DateTimeHelper.a(DateTimeHelper.a(PremiumizeCredentialsHelper.b().getPremium_until()));
                }
                return false;
            } else if (!AllDebridCredentialsHelper.b().isValid() || !FreeMoviesApp.l().getBoolean("pref_alldebrid_type", false)) {
                return false;
            } else {
                String string = FreeMoviesApp.l().getString("pref_alldebrid_expiration_str", "");
                if (!string.isEmpty()) {
                    return DateTimeHelper.a(DateTimeHelper.c(string));
                }
                return false;
            }
        } else if (!RealDebridCredentialsHelper.c().isValid() || !FreeMoviesApp.l().getBoolean("pref_realdebrid_type", false)) {
            return false;
        } else {
            String string2 = FreeMoviesApp.l().getString("pref_realdebrid_expiration_str", "");
            Logger.a("checkExprirationDB", string2);
            if (!string2.isEmpty()) {
                return DateTimeHelper.a(DateTimeHelper.b(string2));
            }
            return false;
        }
    }

    public static boolean a(File file) {
        if (file != null && file.isDirectory()) {
            String[] list = file.list();
            for (String file2 : list) {
                if (!a(new File(file, file2))) {
                    return false;
                }
            }
            return file.delete();
        } else if (file == null || !file.isFile()) {
            return false;
        } else {
            return file.delete();
        }
    }

    public static void a(Activity activity, String str) {
        try {
            Snackbar a2 = Snackbar.a(activity.findViewById(16908290), str, 0);
            View f2 = a2.f();
            TextView textView = (TextView) f2.findViewById(R.id.snackbar_text);
            if (textView != null) {
                textView.setTextColor(activity.getResources().getColor(R.color.white));
                f2.setBackgroundColor(activity.getResources().getColor(R.color.primaryBG));
                a2.k();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(Activity activity, int i2) {
        a(activity, activity.getString(i2));
    }

    public static String a(int i2) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder(i2);
        for (int i3 = 0; i3 < i2; i3++) {
            sb.append("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(random.nextInt(62)));
        }
        return sb.toString();
    }

    public static String a(ArrayList<String> arrayList, String str, boolean z) {
        if (arrayList == null || arrayList.size() == 0) {
            return "";
        }
        if (z) {
            return arrayList.get(0);
        }
        return TextUtils.join(",", arrayList);
    }

    public static ComponentName a(Context context, Intent intent) {
        if (Util.f3651a >= 26) {
            return context.startForegroundService(intent);
        }
        return context.startService(intent);
    }

    public static void a(OkHttpClient okHttpClient) {
        for (Call cancel : okHttpClient.dispatcher().queuedCalls()) {
            cancel.cancel();
        }
        for (Call cancel2 : okHttpClient.dispatcher().runningCalls()) {
            cancel2.cancel();
        }
        okHttpClient.dispatcher().executorService().shutdown();
    }
}
