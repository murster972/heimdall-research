import urllib.request as request
import urllib.parse as parse

import time

from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class VideoNextDownloader:
    ''' wrapper to download mp4 files directly from videonext main-server
        https://vidnext.net/
        e.g. https://storage.googleapis.com/tribal-sign-302203/1MI4LM13KRV5/22m_1612105000336029.mp4 '''

    # URL = "https://vidnext.net/videos/john-wick-hd-720p"
    URL = "https://vidnext.net/videos/seal-team-season-4-episode-6-horror-has-a-face"
    

    def __init__(self):
        pass

    def _get_source(self, url=None):
        ''' gets HTMl source using default headers '''
        url = url if url else self.URL

        # NOTE: the headers are required to prevent a 403 error
        headers = {
            "authority": "vidnext.net", 
            "cache-control": "max-age=0", 
            "dnt": "1", 
            "upgrade-insecure-requests": "1", 
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36", 
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", 
            "sec-fetch-site": "same-origin", 
            "sec-fetch-mode": "navigate", 
            "sec-fetch-user": "?1", 
            "sec-fetch-dest": "document", 
            "referer": url,
            "accept-language": "en-US,en;q=0.9"
        }

        req = request.Request(url, headers=headers)
        con = request.urlopen(req)
        html = con.read()

        return html


    def get_link(self):
        ''' get the download link for a tv/movie-show

            :param url: the url of the movie/tv-episode '''
        source = self._get_source()
        parser = BeautifulSoup(source, "html.parser")

        # extract iframe url - will start with "//"
        iframe_url = parser.find("iframe")["src"]
        iframe_url = f"https:{iframe_url}"

        # download source of i-frame
        source = self._get_source(iframe_url)
        # parser = BeautifulSoup(source, "html.parser")


        # trigger the i-frame to load the sources
        #
        # NOTE: if we get the source with get_requests then it will
        #       not load the iframe, but it works with selenium
        #       i assume its something todo with some js loading
        #       on page load
        options = Options()
        options.headless = True
        driver = webdriver.Chrome("./chromedriver", options=options)
        driver.get(iframe_url)

        # give any onload JS a chance to run
        time.sleep(1)

        # extract the main link using the following JS
        #   jwplayer("myVideo").getPlaylist()[0].file
        #
        link = driver.execute_script('return jwplayer("myVideo").getPlaylist()[0].file')
        
        print("Link is:", link)


if __name__ == '__main__':
    v = VideoNextDownloader()

    # results = v.search("john wick")

    v.get_link()