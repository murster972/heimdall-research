import csv
import re
import requests
import time
import datetime

from multiprocessing import Process


'''
we've got a list of provider names from the cinema-hd decompiled APK, however
the code that converts the names into the urls is obfuscated so we need another way
to get the URLs.

The current ides is a brute-force approach:
    - for every provider try it against a list of domain ends, e.g. url.com url.org url.net, etc.
    - using the standard format of index.php?search=name
    - we're looking for a response that is not 404
        - 403/500 etc. is good since we know the server is responding
        - and we can worry about getting auth etc. later
'''

class FindProviderUrls:
    def __init__(self):
        self.provider_names = []
        self.domain_endings = []

        # only allow domain-endings that contain alpha, rules out
        # any foreign chars, e.g. cyrlic
        # 
        # QUESTION: what happens if we don't do this???
        self.valid_domain_ending = re.compile("^.[a-zA-Z]*$")

    def load_provider_names(self, fn):
        ''' load all provider names from the providers list '''
        f = open(fn, "r")
        lines = f.readlines()
        f.close()

        self.provider_names = []

        for line in lines:
            line = line.strip()

            if line and not line.startswith("#"):
                self.provider_names.append(line)

    def load_domain_endings(self, fn):
        ''' load in possible domain-endings from CSV file,
            see: https://datahub.io/core/top-level-domain-names#curl '''
        f = open(fn, "r")
        lines = f.readlines()
        f.close()

        self.domain_endings = []

        for line in lines:
            line = line.strip()

            if line and not line.startswith("#"):
                self.domain_endings.append(line)

    def _check_provider_links(self, provider, endings, number):
        ''' check a subset of endings for a given provider '''
        # attr = getattr(self, f"proc_{number}_res")
        valid = []

        for ending in endings:
                try:
                    url = f"https://{provider}{ending}"

                    a = requests.get(url, timeout=1)

                    valid.append(url)

                    # print(f"{url} - pass", attr)
                except Exception:
                    # print(f"{url} - fail")
                    pass
        
        if valid:
            print(valid)


        # if valid:
        #     with open(f"valid-urls/{number}", "a") as f:
        #         f.write("\n".join(valid))
        #         f.write("\n")
        
    def find_links(self):

        self.provider_names = ["BobMovies"]

        p_l = len(self.provider_names)

        for p_ind in range(p_l):
            provider = self.provider_names[p_ind]
            start_time = time.time()

            proc_count = 50

            l = len(self.domain_endings) // proc_count
            ind = 0

            print(f"\n{p_ind + 1} of {p_l}")
            print(f"name: {provider}  start: {datetime.datetime.now()}")

            procs = []

            for i in range(proc_count):
                ends = self.domain_endings[ind:ind + l]
                ind += l

                # print(len(ends))

                p = Process(target=self._check_provider_links, args=(provider, ends, i))
                procs.append(p)
                p.start()
        
            for p in procs:
                p.join()
                p.terminate()

            # print(f"name: {provider}  end: {datetime.datetime.now()}")
            print(f"name: {provider}  ttime: {time.time() - start_time}")


if __name__ == '__main__':
    f = FindProviderUrls()
    f.load_provider_names("provider-names")
    f.load_domain_endings("domain-endings")

    f.find_links()