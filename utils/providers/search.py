import requests
import re
import urllib.parse as parse

from abc import abstractmethod


class Search:
    ''' extract a href to the given item for the given provider '''
    @staticmethod
    def search(url, item):
        try:
            req = requests.get(url, timeout=2, headers={"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"})
            
            hrefs = Search._get_hrefs(req.text, item)
            links = Search._remove_false_positives(hrefs)

            return Search._get_shortest(links)
        except:
            return ""

    @staticmethod
    def _get_hrefs(html, item):
        ''' get all hres matching out item from the providers html '''
        anything = "[a-zA-Z0-9+\/\-_:.]*?"
        name = item.strip().replace(" ", anything)

        regex = f'href="{anything}{name}{anything}"'
        regex = re.compile(regex)

        return regex.findall(html)
    
    @staticmethod
    def _remove_false_positives(hrefs):
        ''' attempt to remove any false positives, e.g. searches, actors, etc. '''
        false_pos = ["/search/", "/actor/"]
        valid = []

        for i in hrefs:
            is_in = sum([1 for j in false_pos if j in i])

            if is_in == 0:
                valid.append(i.strip())

        return valid

    @staticmethod
    def _get_shortest(hrefs):
        # if we have multiple hrefs we can assume that the item has
        # been matched ot multiple items, to get libnk that is most likely our
        # item we will use the shorest item and assume the rest just contain our
        # items name but are not our item e.g. a sequel to the item
        #
        # BUG: only issue is that if we have matches but not our item then
        #      we'll return a false positive
        shortest = ""

        for i in hrefs:
            if not shortest or len(i) < len(shortest):
                shortest = i

        return shortest
    

class CreateURL:
    @abstractmethod
    def create_url(self, provider, item):
        ''' creates the url we use to search the provider '''
        pass

class MethodOne(CreateURL):
    def create_url(self, provider, item):
        return f"{provider}/search/{item}"

class MethodTwo(CreateURL):
    def create_url(self, provider, item):
        return f"{provider}/search/{item.replace(' ', '-')}"

class MethodThree(CreateURL):
    def create_url(self, provider, item):
        return f"{provider}/search/{item.replace(' ', '+')}"

class MethodFour(CreateURL):
    def create_url(self, provider, item):
        keyword = parse.urlencode({"keyword": item})
        return f"{provider}/search.html?{keyword}"


def href_to_url(provider, href):
    ''' convert href to url '''
    href = href.replace('href="', "").replace('"', "")

    if href.startswith("https://"):
        return href
    else:
        return f"{provider}{href}"


class GetLinks:
    ''' Get links for a given movie from the given subscribers '''

    def __init__(self):
        self.providers = ['https://Afdah.click', 'https://Afdah.design', 'https://Afdah.wiki', 'https://BobMovies.cc',
        'https://BobMovies.com', 'https://Fmovies.co', 'https://Fmovies.date', 'https://Fmovies.li',
        'https://Fmovies.movie', 'https://Fmovies.news', 'https://Fmovies.town', 'https://Fmovies.win',
        'https://GoMovies.ai', 'https://GoMovies.beer', 'https://GoMovies.click', 'https://GoMovies.com',
        'https://GoMovies.design', 'https://GoMovies.film', 'https://GoMovies.help', 'https://GoMovies.one',
        'https://GoMovies.re', 'https://Hdpopcorns.me', 'https://M4UFree.rest', 'https://M4UFree.work',
        'https://PrimeWire.st', 'https://Pubfilm.nl', 'https://Pubfilm.one', 'https://Pubfilm.ru', 'https://Pubfilm.su', 
        'https://Rarbg.cc', 'https://SeeHD.nl', 'https://vidcloud9.com', 'https://vidnext.net',  'https://WatchMovieHD.org', 'https://WatchSeries.rocks', 
        'https://WatchSeries.video', 'https://XMovies8.cz', 'https://XMovies8.uno', 'https://XMovies8.work', 
        'https://YesMovies.asia', 'https://YesMovies.best', 'https://YesMovies.page', 'https://YesMovies.press', 
        'https://YesMovies.tech']

        self._create_urls = [
            MethodOne(),
            MethodTwo(),
            MethodThree(),
            MethodFour()
        ]

    def get(self, item):
        provider_results = {}

        self.providers = ["https://vidcloud9.com"]

        for prov in self.providers:
            for creator in self._create_urls:
                url = creator.create_url(prov, item)
                link = Search.search(url, item)

                if link:
                    print(prov, " ", href_to_url(prov, link))
                    provider_results[prov] = link
                    break
        
        return provider_results

if __name__ == '__main__':
    g = GetLinks()
    g.get("seal team season 1 episode 22")