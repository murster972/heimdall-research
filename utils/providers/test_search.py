import requests
import re
import urllib.parse as parse

# list if URLS we know we can query
URLS = ['https://Afdah.click', 'https://Afdah.design', 'https://Afdah.wiki', 'https://BobMovies.cc',
        'https://BobMovies.com', 'https://Fmovies.co', 'https://Fmovies.date', 'https://Fmovies.li',
        'https://Fmovies.movie', 'https://Fmovies.news', 'https://Fmovies.town', 'https://Fmovies.win',
        'https://GoMovies.ai', 'https://GoMovies.beer', 'https://GoMovies.click', 'https://GoMovies.com',
        'https://GoMovies.design', 'https://GoMovies.film', 'https://GoMovies.help', 'https://GoMovies.one',
        'https://GoMovies.re', 'https://Hdpopcorns.me', 'https://M4UFree.rest', 'https://M4UFree.work',
        'https://PrimeWire.st', 'https://Pubfilm.nl', 'https://Pubfilm.one', 'https://Pubfilm.ru', 'https://Pubfilm.su', 
        'https://Rarbg.cc', 'https://SeeHD.nl', 'https://vidcloud9.com', 'https://WatchMovieHD.org', 'https://WatchSeries.rocks', 
        'https://WatchSeries.video', 'https://XMovies8.cz', 'https://XMovies8.uno', 'https://XMovies8.work', 
        'https://YesMovies.asia', 'https://YesMovies.best', 'https://YesMovies.page', 'https://YesMovies.press', 
        'https://YesMovies.tech']

"""
# methods results
{1: ['https://vidcloud9.com'], 2: ['https://Afdah.click', 'https://Afdah.design', 'https://Afdah.wiki', 'https://Fmovies.li', 'https://Fmovies.movie', 'https://Fmovies.town', 'https://Fmovies.win', 'https://GoMovies.ai', 'https://GoMovies.beer', 'https://GoMovies.design', 'https://GoMovies.film', 'https://GoMovies.help', 'https://GoMovies.one', 'https://GoMovies.re', 'https://Hdpopcorns.me', 'https://M4UFree.rest', 'https://M4UFree.work', 'https://PrimeWire.st', 'https://Pubfilm.nl', 'https://Rarbg.cc', 'https://WatchSeries.rocks', 'https://XMovies8.work', 'https://YesMovies.asia', 'https://YesMovies.best']}
"""

def get_method():
    ''' try and figure out the method of searching for each url '''
    movie = "john wick"

    methods = {
        1: [], # search.html?keyword=movie
        2: [], # url/search/movie (replace space with +)
        3: [], # url/search/movie (replace space with -)
    }

    fails = []


    for i in URLS:
        try:
            print(i)

            tmp = i.replace("https://", "")

            # method 3 - url/search/movie
            movie = "john-wick"
            r = requests.get(f"{i}/search/{movie}", timeout=5, headers={"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"})
            reg = _contains_href(r.text, movie)

            if reg:
                save_file = open(f"link-examples/{tmp}", "w")
                save_file.write("\n\n".join(reg))
                save_file.close()
                methods[2].append(i)

                continue

            # method 2 - url/search/movie
            movie = "john wick"
            r = requests.get(f"{i}/search/{movie}", timeout=5, headers={"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"})
            reg = _contains_href(r.text, movie)

            if reg:
                save_file = open(f"link-examples/{tmp}", "w")
                save_file.write("\n\n".join(reg))
                save_file.close()
                methods[2].append(i)

                continue

            # # method-1 - index.php?keyword=movie
            movie = "john wick"
            keyword = parse.urlencode({"keyword": movie})
            r = requests.get(f"{i}/search.html?{keyword}", timeout=5, headers={"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"})
            reg = _contains_href(r.text, movie)

            if reg:
                save_file = open(f"link-examples/{tmp}", "w")
                save_file.write("\n\n".join(reg))
                save_file.close()
                methods[1].append(i)
                continue
            
        except Exception as e:
            fails.append(i)

    print(methods)
    print(fails)

def _contains_href(html, name):
    ''' checks if the page contains an href using the movies name '''
    # we can't just match anything with "." or we end up with false posositves
    anything = "[a-zA-Z0-9+\/\-_:.]*?"
    name = name.strip().replace(" ", anything)

    regex = f'href="{anything}{name}{anything}"'
    regex = re.compile(regex)

    # strip out anything that will give false posisitve
    match = regex.findall(html)
    hrefs = []

    for i in match:
        if "/search/" in i or "/actor/" in i:
            continue
        hrefs.append(i.strip())

    return hrefs
    
def main():
    get_method()

if __name__ == '__main__':
    main()