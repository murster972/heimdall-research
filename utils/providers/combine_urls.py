import glob

def main():
    ''' combine the valid-urls into a single file '''
    urls = []
    
    for fn in glob.glob("valid-urls/*"):
        with open(fn, "r") as f:
            urls += f.readlines()

    urls = list(set(urls))
    urls.sort()


    with open("all-valid-urls", "w") as f:
        f.write("".join(urls))

if __name__ == '__main__':
    main()