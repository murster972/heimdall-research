import requests
import re

class QueryURLS:
    ''' test if we can query the list of valid-urls we found,
        e.g. https://url.com/index.php?keyword=test '''

    def __init__(self):
        self.urls = []

        self.queryable_urls = []

    def load_urls(self, fn):
        with open(fn, "r") as f:
            self.urls += [i.strip() for i in f.readlines()]

    def query_urls(self):
        for url in self.urls:
            try:
                r = requests.get(f"{url}/index.php?keyword=test", timeout=1)
                html = r.text.lower()

                if "free movies" not in html and "tv shows" not in html:
                    raise Exception()

                self.queryable_urls.append(url)

                print(url, "success")

            except:
                print(url, "fail")

if __name__ == '__main__':
    q = QueryURLS()
    q.load_urls("all-valid-urls")
    
    q.query_urls()

    with open("queryable-urls", "w") as f:
        f.write("\n".join(q.queryable_urls))

    print(q.queryable_urls)
    print(len(q.queryable_urls))