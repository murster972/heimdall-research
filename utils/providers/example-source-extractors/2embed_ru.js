window.jwplayer = function (a) {
    function e(e) {
        for (var t, n, r = e[0], i = e[1], o = 0, u = []; o < r.length; o++) n = r[o], c[n] && u.push(c[n][0]), c[n] = 0;
        for (t in i) Object.prototype.hasOwnProperty.call(i, t) && (a[t] = i[t]);
        for (s && s(e); u.length;) u.shift()()
    }
    var n = {},
        c = {
            0: 0
        };

    function i(e) {
        if (n[e]) return n[e].exports;
        var t = n[e] = {
            i: e,
            l: !1,
            exports: {}
        };
        return a[e].call(t.exports, t, t.exports, i), t.l = !0, t.exports
    }
    i.e = function (o) {
        var e, t, u, a, n = [],
            r = c[o];
        return 0 !== r && (r ? n.push(r[2]) : (e = new Promise(function (e, t) {
            r = c[o] = [e, t]
        }), n.push(r[2] = e), (u = document.createElement("script")).charset = "utf-8", u.timeout = 55, i.nc && u.setAttribute("nonce", i.nc), u.src = i.p + "" + ({
            1: "jwplayer.controls",
            2: "jwplayer.core",
            3: "jwplayer.core.controls",
            4: "jwplayer.core.controls.html5",
            5: "jwplayer.core.controls.polyfills",
            6: "jwplayer.core.controls.polyfills.html5",
            7: "jwplayer.vr",
            8: "polyfills.intersection-observer",
            9: "polyfills.webvtt",
            10: "provider.airplay",
            11: "provider.cast",
            12: "provider.flash",
            13: "provider.hlsjs",
            14: "provider.hlsjs-progressive",
            15: "provider.html5",
            16: "provider.shaka",
            17: "related",
            18: "vttparser"
        } [o] || o) + ".js", t = function (e) {
            u.onerror = u.onload = null, clearTimeout(a);
            var t, n, r, i = c[o];
            0 !== i && (i && (t = e && ("load" === e.type ? "missing" : e.type), n = e && e.target && e.target.src, (r = new Error("Loading chunk " + o + " failed.\n(" + t + ": " + n + ")")).type = t, r.request = n, i[1](r)), c[o] = void 0)
        }, a = setTimeout(function () {
            t({
                type: "timeout",
                target: u
            })
        }, 55e3), u.onerror = u.onload = t, document.head.appendChild(u))), Promise.all(n)
    }, i.m = a, i.c = n, i.d = function (e, t, n) {
        i.o(e, t) || Object.defineProperty(e, t, {
            enumerable: !0,
            get: n
        })
    }, i.r = function (e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }, i.t = function (t, e) {
        if (1 & e && (t = i(t)), 8 & e) return t;
        if (4 & e && "object" == typeof t && t && t.__esModule) return t;
        var n = Object.create(null);
        if (i.r(n), Object.defineProperty(n, "default", {
                enumerable: !0,
                value: t
            }), 2 & e && "string" != typeof t)
            for (var r in t) i.d(n, r, function (e) {
                return t[e]
            }.bind(null, r));
        return n
    }, i.n = function (e) {
        var t = e && e.__esModule ? function () {
            return e.default
        } : function () {
            return e
        };
        return i.d(t, "a", t), t
    }, i.o = function (e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, i.p = "", i.oe = function (e) {
        throw console.error(e), e
    };
    var t = window.webpackJsonpjwplayer = window.webpackJsonpjwplayer || [],
        r = t.push.bind(t);
    t.push = e, t = t.slice();
    for (var o = 0; o < t.length; o++) e(t[o]);
    var s = r;
    return i(i.s = 69)
}([function (e, t, n) {
    "use strict";
    n.d(t, "j", function () {
        return T
    }), n.d(t, "B", function () {
        return _
    }), n.d(t, "G", function () {
        return M
    }), n.d(t, "m", function () {
        return N
    }), n.d(t, "l", function () {
        return R
    }), n.d(t, "a", function () {
        return q
    }), n.d(t, "b", function () {
        return z
    }), n.d(t, "H", function () {
        return Q
    }), n.d(t, "o", function () {
        return $
    }), n.d(t, "I", function () {
        return X
    }), n.d(t, "e", function () {
        return U
    }), n.d(t, "K", function () {
        return K
    }), n.d(t, "n", function () {
        return Y
    }), n.d(t, "i", function () {
        return Z
    }), n.d(t, "q", function () {
        return G
    }), n.d(t, "c", function () {
        return ee
    }), n.d(t, "D", function () {
        return ne
    }), n.d(t, "J", function () {
        return ie
    }), n.d(t, "r", function () {
        return ae
    }), n.d(t, "h", function () {
        return ce
    }), n.d(t, "k", function () {
        return se
    }), n.d(t, "E", function () {
        return le
    }), n.d(t, "x", function () {
        return de
    }), n.d(t, "u", function () {
        return ge
    }), n.d(t, "w", function () {
        return be
    }), n.d(t, "y", function () {
        return me
    }), n.d(t, "t", function () {
        return ye
    }), n.d(t, "v", function () {
        return we
    }), n.d(t, "s", function () {
        return je
    }), n.d(t, "z", function () {
        return Oe
    }), n.d(t, "p", function () {
        return Ce
    }), n.d(t, "d", function () {
        return Pe
    }), n.d(t, "F", function () {
        return xe
    }), n.d(t, "C", function () {
        return Se
    }), n.d(t, "A", function () {
        return Te
    }), n.d(t, "f", function () {
        return Ae
    });

    function r(e, t) {
        var n;
        return function () {
            return 0 < --e && (n = t.apply(this, arguments)), e <= 1 && (t = null), n
        }
    }

    function c(e) {
        return null == e ? Ce : ge(e) ? e : xe(e)
    }

    function i(a) {
        return function (r, i, o) {
            var u = {};
            return i = c(i), T(r, function (e, t) {
                var n = i.call(o, e, t, r);
                a(u, n, e)
            }), u
        }
    }

    function u() {}

    function o(i) {
        var o = h.call(arguments, 1);
        return function () {
            for (var e = 0, t = o.slice(), n = 0, r = t.length; n < r; n++) ke(t[n], "partial") && (t[n] = arguments[e++]);
            for (; e < arguments.length;) t.push(arguments[e++]);
            return i.apply(this, t)
        }
    }

    function a(e, t) {
        var n = h.call(arguments, 2);
        return setTimeout(function () {
            return e.apply(null, n)
        }, t)
    }
    var s = n(18),
        l = {},
        f = Array.prototype,
        d = Object.prototype,
        p = Function.prototype,
        h = f.slice,
        v = f.concat,
        g = d.toString,
        b = d.hasOwnProperty,
        m = f.map,
        y = f.reduce,
        w = f.forEach,
        j = f.filter,
        O = f.every,
        k = f.some,
        C = f.indexOf,
        P = Array.isArray,
        x = Object.keys,
        S = p.bind,
        E = window.isFinite,
        T = function (e, t, n) {
            if (null == e) return e;
            if (w && e.forEach === w) e.forEach(t, n);
            else if (e.length === +e.length) {
                for (i = 0, o = e.length; i < o; i++)
                    if (t.call(n, e[i], i, e) === l) return
            } else
                for (var r = oe(e), i = 0, o = r.length; i < o; i++)
                    if (t.call(n, e[r[i]], r[i], e) === l) return;
            return e
        },
        A = T,
        _ = function (e, r, i) {
            var o = [];
            return null == e ? o : m && e.map === m ? e.map(r, i) : (T(e, function (e, t, n) {
                o.push(r.call(i, e, t, n))
            }), o)
        },
        F = _,
        M = function (e, r, i, o) {
            var u = 2 < arguments.length;
            if (null == e && (e = []), y && e.reduce === y) return o && (r = ee(r, o)), u ? e.reduce(r, i) : e.reduce(r);
            if (T(e, function (e, t, n) {
                    u ? i = r.call(o, i, e, t, n) : (i = e, u = !0)
                }), !u) throw new TypeError("Reduce of empty array with no initial value");
            return i
        },
        I = M,
        L = M,
        N = function (e, r, i) {
            var o;
            return z(e, function (e, t, n) {
                if (r.call(i, e, t, n)) return o = e, !0
            }), o
        },
        D = N,
        R = function (e, r, i) {
            var o = [];
            return null == e ? o : j && e.filter === j ? e.filter(r, i) : (T(e, function (e, t, n) {
                r.call(i, e, t, n) && o.push(e)
            }), o)
        },
        B = R,
        q = function (e, r, i) {
            r = r || Ce;
            var o = !0;
            return null == e ? o : O && e.every === O ? e.every(r, i) : (T(e, function (e, t, n) {
                if (!(o = o && r.call(i, e, t, n))) return l
            }), !!o)
        },
        V = q,
        z = function (e, r, i) {
            r = r || Ce;
            var o = !1;
            return null == e ? o : k && e.some === k ? e.some(r, i) : (T(e, function (e, t, n) {
                if (o = o || r.call(i, e, t, n)) return l
            }), !!o)
        },
        H = z,
        Q = function (e) {
            return null == e ? 0 : e.length === +e.length ? e.length : oe(e).length
        },
        $ = i(function (e, t, n) {
            ke(e, t) ? e[t].push(n) : e[t] = [n]
        }),
        W = i(function (e, t, n) {
            e[t] = n
        }),
        X = function (e, t, n, r) {
            for (var i = (n = c(n)).call(r, t), o = 0, u = e.length; o < u;) {
                var a = o + u >>> 1;
                n.call(r, e[a]) < i ? o = 1 + a : u = a
            }
            return o
        },
        U = function (e, t) {
            return null != e && (e.length !== +e.length && (e = ue(e)), 0 <= G(e, t))
        },
        J = U,
        K = function (e, t) {
            return R(e, Se(t))
        },
        Y = function (e, t) {
            return N(e, Se(t))
        },
        Z = function (e) {
            var t = v.apply(f, h.call(arguments, 1));
            return R(e, function (e) {
                return !U(t, e)
            })
        },
        G = function (e, t, n) {
            if (null == e) return -1;
            var r = 0,
                i = e.length;
            if (n) {
                if ("number" != typeof n) return e[r = X(e, t)] === t ? r : -1;
                r = n < 0 ? Math.max(0, i + n) : n
            }
            if (C && e.indexOf === C) return e.indexOf(t, n);
            for (; r < i; r++)
                if (e[r] === t) return r;
            return -1
        },
        ee = function (n, r) {
            var i, o;
            if (S && n.bind === S) return S.apply(n, h.call(arguments, 1));
            if (!ge(n)) throw new TypeError;
            return i = h.call(arguments, 2), o = function () {
                if (!(this instanceof o)) return n.apply(r, i.concat(h.call(arguments)));
                u.prototype = n.prototype;
                var e = new u;
                u.prototype = null;
                var t = n.apply(e, i.concat(h.call(arguments)));
                return Object(t) === t ? t : e
            }
        },
        te = o(r, 2),
        ne = function (t, n) {
            var r = {};
            return n = n || Ce,
                function () {
                    var e = n.apply(this, arguments);
                    return ke(r, e) ? r[e] : r[e] = t.apply(this, arguments)
                }
        },
        re = o(a, {
            partial: o
        }, 1),
        ie = function (t, n, r) {
            var i, o, u, a = null,
                c = 0;
            r = r || {};

            function s() {
                c = !1 === r.leading ? 0 : Ee(), a = null, u = t.apply(i, o), i = o = null
            }
            return function () {
                c || !1 !== r.leading || (c = Ee);
                var e = n - (Ee - c);
                return i = this, o = arguments, e <= 0 ? (clearTimeout(a), a = null, c = Ee, u = t.apply(i, o), i = o = null) : a || !1 === r.trailing || (a = setTimeout(s, e)), u
            }
        },
        oe = function (e) {
            if (!de(e)) return [];
            if (x) return x(e);
            var t = [];
            for (var n in e) ke(e, n) && t.push(n);
            return t
        },
        ue = function (e) {
            for (var t = oe(e), n = oe.length, r = Array(n), i = 0; i < n; i++) r[i] = e[t[i]];
            return r
        },
        ae = function (e) {
            for (var t = {}, n = oe(e), r = 0, i = n.length; r < i; r++) t[e[n[r]]] = n[r];
            return t
        },
        ce = function (n) {
            return T(h.call(arguments, 1), function (e) {
                if (e)
                    for (var t in e) void 0 === n[t] && (n[t] = e[t])
            }), n
        },
        se = Object.assign || function (n) {
            return T(h.call(arguments, 1), function (e) {
                if (e)
                    for (var t in e) Object.prototype.hasOwnProperty.call(e, t) && (n[t] = e[t])
            }), n
        },
        le = function (t) {
            var n = {},
                e = v.apply(f, h.call(arguments, 1));
            return T(e, function (e) {
                e in t && (n[e] = t[e])
            }), n
        },
        fe = P || function (e) {
            return "[object Array]" == g.call(e)
        },
        de = function (e) {
            return e === Object(e)
        },
        pe = [];
    T(["Function", "String", "Number", "Date", "RegExp"], function (t) {
        pe[t] = function (e) {
            return g.call(e) == "[object " + t + "]"
        }
    }), pe.Function = function (e) {
        return "function" == typeof e
    };
    var he = pe.Date,
        ve = pe.RegExp,
        ge = pe.Function,
        be = pe.Number,
        me = pe.String,
        ye = function (e) {
            return E(e) && !we(parseFloat(e))
        },
        we = function (e) {
            return be(e) && e != +e
        },
        je = function (e) {
            return !0 === e || !1 === e || "[object Boolean]" == g.call(e)
        },
        Oe = function (e) {
            return void 0 === e
        },
        ke = function (e, t) {
            return b.call(e, t)
        },
        Ce = function (e) {
            return e
        },
        Pe = function (e) {
            return function () {
                return e
            }
        },
        xe = function (t) {
            return function (e) {
                return e[t]
            }
        },
        Se = function (n) {
            return function (e) {
                if (e === n) return !0;
                for (var t in n)
                    if (n[t] !== e[t]) return !1;
                return !0
            }
        },
        Ee = s.a,
        Te = function (e) {
            return be(e) && !we(e)
        },
        Ae = function (i) {
            var o, u = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 100;
            return function () {
                for (var e = this, t = arguments.length, n = new Array(t), r = 0; r < t; r++) n[r] = arguments[r];
                clearTimeout(o), o = setTimeout(function () {
                    i.apply(e, n)
                }, u)
            }
        };
    t.g = {
        after: function (e, t) {
            return function () {
                if (--e < 1) return t.apply(this, arguments)
            }
        },
        all: q,
        any: z,
        before: r,
        bind: ee,
        clone: function (e) {
            return de(e) ? fe(e) ? e.slice() : se({}, e) : e
        },
        collect: F,
        compact: function (e) {
            return R(e, Ce)
        },
        constant: Pe,
        contains: U,
        debounce: Ae,
        defaults: ce,
        defer: re,
        delay: a,
        detect: D,
        difference: Z,
        each: T,
        every: V,
        extend: se,
        filter: R,
        find: N,
        findWhere: Y,
        foldl: I,
        forEach: A,
        groupBy: $,
        has: ke,
        identity: Ce,
        include: J,
        indexBy: W,
        indexOf: G,
        inject: L,
        invert: ae,
        isArray: fe,
        isBoolean: je,
        isDate: he,
        isFinite: ye,
        isFunction: ge,
        isNaN: we,
        isNull: function (e) {
            return null === e
        },
        isNumber: be,
        isObject: de,
        isRegExp: ve,
        isString: me,
        isUndefined: Oe,
        isValidNumber: Te,
        keys: oe,
        last: function (e, t, n) {
            if (null != e) return null == t || n ? e[e.length - 1] : h.call(e, Math.max(e.length - t, 0))
        },
        map: _,
        matches: Se,
        max: function (e, i, o) {
            if (!i && fe(e) && e[0] === +e[0] && e.length < 65535) return Math.max.apply(Math, e);
            var u = -1 / 0,
                a = -1 / 0;
            return T(e, function (e, t, n) {
                var r = i ? i.call(o, e, t, n) : e;
                a < r && (u = e, a = r)
            }), u
        },
        memoize: ne,
        now: Ee,
        omit: function (e) {
            var t = {},
                n = v.apply(f, h.call(arguments, 1));
            for (var r in e) U(n, r) || (t[r] = e[r]);
            return t
        },
        once: te,
        partial: o,
        pick: le,
        pluck: function (e, t) {
            return _(e, xe(t))
        },
        property: xe,
        propertyOf: function (t) {
            return null == t ? function () {} : function (e) {
                return t[e]
            }
        },
        reduce: M,
        reject: function (e, r, i) {
            return R(e, function (e, t, n) {
                return !r.call(i, e, t, n)
            }, i)
        },
        result: function (e, t) {
            if (null != e) {
                var n = e[t];
                return ge(n) ? n.call(e) : n
            }
        },
        select: B,
        size: Q,
        some: H,
        sortedIndex: X,
        throttle: ie,
        where: K,
        without: function (e) {
            return Z(e, h.call(arguments, 1))
        }
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "y", function () {
        return o
    }), n.d(t, "x", function () {
        return u
    }), n.d(t, "w", function () {
        return a
    }), n.d(t, "t", function () {
        return c
    }), n.d(t, "u", function () {
        return s
    }), n.d(t, "a", function () {
        return l
    }), n.d(t, "c", function () {
        return f
    }), n.d(t, "v", function () {
        return d
    }), n.d(t, "d", function () {
        return p
    }), n.d(t, "h", function () {
        return h
    }), n.d(t, "e", function () {
        return v
    }), n.d(t, "k", function () {
        return g
    }), n.d(t, "i", function () {
        return b
    }), n.d(t, "j", function () {
        return m
    }), n.d(t, "b", function () {
        return P
    }), n.d(t, "f", function () {
        return x
    }), n.d(t, "g", function () {
        return S
    }), n.d(t, "o", function () {
        return E
    }), n.d(t, "l", function () {
        return T
    }), n.d(t, "m", function () {
        return A
    }), n.d(t, "n", function () {
        return _
    }), n.d(t, "p", function () {
        return F
    }), n.d(t, "q", function () {
        return M
    }), n.d(t, "r", function () {
        return I
    }), n.d(t, "s", function () {
        return L
    }), n.d(t, "A", function () {
        return D
    }), n.d(t, "z", function () {
        return R
    }), n.d(t, "B", function () {
        return B
    });
    var r = n(0);

    function i(e, t) {
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
        }
    }
    var o = 1e5,
        u = 100001,
        a = 100002,
        c = 101e3,
        s = 102e3,
        l = 200001,
        f = 202e3,
        d = 104e3,
        p = 203e3,
        h = 203640,
        v = 204e3,
        g = 210001,
        b = 21e4,
        m = 214e3,
        y = 303200,
        w = 303210,
        j = 303212,
        O = 303213,
        k = 303220,
        C = 303230,
        P = 306e3,
        x = 308e3,
        S = 308640,
        E = "cantPlayVideo",
        T = "badConnection",
        A = "cantLoadPlayer",
        _ = "cantPlayInBrowser",
        F = "liveStreamDown",
        M = "protectedContent",
        I = "technicalError",
        L = (i(N, [{
            key: "logMessage",
            value: function (e) {
                var t = e % 1e3,
                    n = Math.floor((e - t) / 1e3),
                    r = e;
                return 400 <= t && t < 600 && (r = "".concat(n, "400-").concat(n, "599")), "JW Player ".concat(299999 < e && e < 4e5 ? "Warning" : "Error", " ").concat(e, ". For more information see https://developer.jwplayer.com/jw-player/docs/developer-guide/api/errors-reference#").concat(r)
            }
        }]), N);

    function N(e, t) {
        var n = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : null;
        ! function (e) {
            if (!(e instanceof N)) throw new TypeError("Cannot call a class as a function")
        }(this), this.code = Object(r.A)(t) ? t : 0, this.sourceError = n, e && (this.key = e)
    }

    function D(e, t, n) {
        return n instanceof L && n.code ? n : new L(e, t, n)
    }

    function R(e, t) {
        var n = D(I, t, e);
        return n.code = (e && e.code || 0) + t, n
    }

    function B(e) {
        var t = e.name,
            n = e.message;
        switch (t) {
        case "AbortError":
            return /pause/.test(n) ? O : /load/.test(n) ? j : w;
        case "NotAllowedError":
            return k;
        case "NotSupportedError":
            return C;
        default:
            return y
        }
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "i", function () {
        return r
    }), n.d(t, "e", function () {
        return i
    }), n.d(t, "j", function () {
        return o
    }), n.d(t, "a", function () {
        return u
    }), n.d(t, "b", function () {
        return a
    }), n.d(t, "g", function () {
        return l
    }), n.d(t, "d", function () {
        return f
    }), n.d(t, "f", function () {
        return d
    }), n.d(t, "h", function () {
        return p
    }), n.d(t, "c", function () {
        return h
    });
    var c = n(0),
        s = window.parseFloat;

    function r(e) {
        return e.replace(/^\s+|\s+$/g, "")
    }

    function i(e, t, n) {
        for (e = "" + e, n = n || "0"; e.length < t;) e = n + e;
        return e
    }

    function o(e, t) {
        for (var n = e.attributes, r = 0; r < n.length; r++)
            if (n[r].name && n[r].name.toLowerCase() === t.toLowerCase()) return n[r].value.toString();
        return ""
    }

    function u(e) {
        if (!e || "rtmp" === e.substr(0, 4)) return "";
        var t = /[(,]format=(m3u8|mpd)-/i.exec(e);
        return t ? t[1] : -1 < (e = e.split("?")[0].split("#")[0]).lastIndexOf(".") ? e.substr(e.lastIndexOf(".") + 1, e.length).toLowerCase() : void 0
    }

    function a(e) {
        var t = (e / 60 | 0) % 60,
            n = e % 60;
        return i(e / 3600 | 0, 2) + ":" + i(t, 2) + ":" + i(n.toFixed(3), 6)
    }

    function l(e, t) {
        if (!e) return 0;
        if (Object(c.A)(e)) return e;
        var n, r = e.replace(",", "."),
            i = r.slice(-1),
            o = r.split(":"),
            u = o.length,
            a = 0;
        return "s" === i ? a = s(r) : "m" === i ? a = 60 * s(r) : "h" === i ? a = 3600 * s(r) : 1 < u ? (n = u - 1, 4 === u && (t && (a = s(o[n]) / t), --n), a += s(o[n]), a += 60 * s(o[n - 1]), 3 <= u && (a += 3600 * s(o[n - 2]))) : a = s(r), Object(c.A)(a) ? a : 0
    }

    function f(e, t, n) {
        if (Object(c.y)(e) && "%" === e.slice(-1)) {
            var r = s(e);
            return t && Object(c.A)(t) && Object(c.A)(r) ? t * r / 100 : null
        }
        return l(e, n)
    }

    function d(e, t) {
        return e.map(function (e) {
            return t + e
        })
    }

    function p(e, t) {
        return e.map(function (e) {
            return e + t
        })
    }

    function h(e) {
        return "string" == typeof e && "%" === e.slice(-1)
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "kb", function () {
        return r
    }), n.d(t, "nb", function () {
        return i
    }), n.d(t, "lb", function () {
        return o
    }), n.d(t, "pb", function () {
        return u
    }), n.d(t, "qb", function () {
        return a
    }), n.d(t, "mb", function () {
        return c
    }), n.d(t, "ob", function () {
        return s
    }), n.d(t, "rb", function () {
        return l
    }), n.d(t, "s", function () {
        return f
    }), n.d(t, "u", function () {
        return d
    }), n.d(t, "t", function () {
        return p
    }), n.d(t, "n", function () {
        return h
    }), n.d(t, "q", function () {
        return v
    }), n.d(t, "sb", function () {
        return g
    }), n.d(t, "r", function () {
        return b
    }), n.d(t, "Z", function () {
        return m
    }), n.d(t, "W", function () {
        return y
    }), n.d(t, "v", function () {
        return w
    }), n.d(t, "Y", function () {
        return j
    }), n.d(t, "w", function () {
        return O
    }), n.d(t, "ub", function () {
        return k
    }), n.d(t, "a", function () {
        return C
    }), n.d(t, "b", function () {
        return P
    }), n.d(t, "c", function () {
        return x
    }), n.d(t, "d", function () {
        return S
    }), n.d(t, "e", function () {
        return E
    }), n.d(t, "h", function () {
        return T
    }), n.d(t, "F", function () {
        return A
    }), n.d(t, "hb", function () {
        return _
    }), n.d(t, "Q", function () {
        return F
    }), n.d(t, "C", function () {
        return M
    }), n.d(t, "B", function () {
        return I
    }), n.d(t, "E", function () {
        return L
    }), n.d(t, "p", function () {
        return N
    }), n.d(t, "cb", function () {
        return D
    }), n.d(t, "m", function () {
        return R
    }), n.d(t, "G", function () {
        return B
    }), n.d(t, "H", function () {
        return q
    }), n.d(t, "N", function () {
        return V
    }), n.d(t, "O", function () {
        return z
    }), n.d(t, "R", function () {
        return H
    }), n.d(t, "jb", function () {
        return Q
    }), n.d(t, "bb", function () {
        return $
    }), n.d(t, "D", function () {
        return W
    }), n.d(t, "S", function () {
        return X
    }), n.d(t, "P", function () {
        return U
    }), n.d(t, "T", function () {
        return J
    }), n.d(t, "V", function () {
        return K
    }), n.d(t, "M", function () {
        return Y
    }), n.d(t, "L", function () {
        return Z
    }), n.d(t, "K", function () {
        return G
    }), n.d(t, "I", function () {
        return ee
    }), n.d(t, "J", function () {
        return te
    }), n.d(t, "U", function () {
        return ne
    }), n.d(t, "o", function () {
        return re
    }), n.d(t, "y", function () {
        return ie
    }), n.d(t, "ib", function () {
        return oe
    }), n.d(t, "db", function () {
        return ue
    }), n.d(t, "eb", function () {
        return ae
    }), n.d(t, "f", function () {
        return ce
    }), n.d(t, "g", function () {
        return se
    }), n.d(t, "ab", function () {
        return le
    }), n.d(t, "A", function () {
        return fe
    }), n.d(t, "l", function () {
        return de
    }), n.d(t, "k", function () {
        return pe
    }), n.d(t, "fb", function () {
        return he
    }), n.d(t, "gb", function () {
        return ve
    }), n.d(t, "tb", function () {
        return ge
    }), n.d(t, "z", function () {
        return be
    }), n.d(t, "j", function () {
        return me
    }), n.d(t, "X", function () {
        return ye
    }), n.d(t, "i", function () {
        return we
    }), n.d(t, "x", function () {
        return je
    });
    var r = "buffering",
        i = "idle",
        o = "complete",
        u = "paused",
        a = "playing",
        c = "error",
        s = "loading",
        l = "stalled",
        f = "drag",
        d = "dragStart",
        p = "dragEnd",
        h = "click",
        v = "doubleClick",
        g = "tap",
        b = "doubleTap",
        m = "over",
        y = "move",
        w = "enter",
        j = "out",
        O = c,
        k = "warning",
        C = "adClick",
        P = "adPause",
        x = "adPlay",
        S = "adSkipped",
        E = "adTime",
        T = "autostartNotAllowed",
        A = o,
        _ = "ready",
        F = "seek",
        M = "beforePlay",
        I = "beforeComplete",
        L = "bufferFull",
        N = "displayClick",
        D = "playlistComplete",
        R = "cast",
        B = "mediaError",
        q = "firstFrame",
        V = "playAttempt",
        z = "playAttemptFailed",
        H = "seeked",
        Q = "setupError",
        $ = "state",
        W = "bufferChange",
        X = "time",
        U = "ratechange",
        J = "mediaType",
        K = "volume",
        Y = "mute",
        Z = "metadataCueParsed",
        G = "meta",
        ee = "levels",
        te = "levelsChanged",
        ne = "visualQuality",
        re = "controls",
        ie = "fullscreen",
        oe = "resize",
        ue = "playlistItem",
        ae = "playlist",
        ce = "audioTracks",
        se = "audioTrackChanged",
        le = "playbackRateChanged",
        fe = "logoClick",
        de = "captionsList",
        pe = "captionsChanged",
        he = "providerChanged",
        ve = "providerFirstFrame",
        ge = "userAction",
        be = "instreamClick",
        me = "breakpoint",
        ye = "fullscreenchange",
        we = "bandwidthEstimate",
        je = "float"
}, function (e, t, n) {
    "use strict";
    n.d(t, "b", function () {
        return i
    }), n.d(t, "d", function () {
        return o
    }), n.d(t, "a", function () {
        return u
    }), n.d(t, "c", function () {
        return a
    });
    var r = n(2);

    function i(e) {
        var t = "";
        return e && (e.localName ? t = e.localName : e.baseName && (t = e.baseName)), t
    }

    function o(e) {
        var t = "";
        return e && (e.textContent ? t = Object(r.i)(e.textContent) : e.text && (t = Object(r.i)(e.text))), t
    }

    function u(e, t) {
        return e.childNodes[t]
    }

    function a(e) {
        return e.childNodes ? e.childNodes.length : 0
    }
}, function (e, t, n) {
    "use strict";
    n.r(t);
    var r = n(7);

    function u(e, t) {
        if (e && e.length > t) return e[t]
    }
    var i = n(0);
    n.d(t, "Browser", function () {
        return a
    }), n.d(t, "OS", function () {
        return c
    }), n.d(t, "Features", function () {
        return s
    });
    var o = navigator.userAgent,
        a = {},
        c = {},
        s = {};
    Object.defineProperties(a, {
        androidNative: {
            get: Object(i.D)(r.c),
            enumerable: !0
        },
        chrome: {
            get: Object(i.D)(r.d),
            enumerable: !0
        },
        edge: {
            get: Object(i.D)(r.e),
            enumerable: !0
        },
        facebook: {
            get: Object(i.D)(r.g),
            enumerable: !0
        },
        firefox: {
            get: Object(i.D)(r.f),
            enumerable: !0
        },
        ie: {
            get: Object(i.D)(r.i),
            enumerable: !0
        },
        msie: {
            get: Object(i.D)(r.n),
            enumerable: !0
        },
        safari: {
            get: Object(i.D)(r.q),
            enumerable: !0
        },
        version: {
            get: Object(i.D)(function (e, t) {
                var n, r, i, o;
                return e.chrome ? n = -1 !== t.indexOf("Chrome") ? t.substring(t.indexOf("Chrome") + 7) : t.substring(t.indexOf("CriOS") + 6) : e.safari ? n = t.substring(t.indexOf("Version") + 8) : e.firefox ? n = t.substring(t.indexOf("Firefox") + 8) : e.edge ? n = t.substring(t.indexOf("Edge") + 5) : e.ie && (-1 !== t.indexOf("rv:") ? n = t.substring(t.indexOf("rv:") + 3) : -1 !== t.indexOf("MSIE") && (n = t.substring(t.indexOf("MSIE") + 5))), n && (-1 !== (o = n.indexOf(";")) && (n = n.substring(0, o)), -1 !== (o = n.indexOf(" ")) && (n = n.substring(0, o)), -1 !== (o = n.indexOf(")")) && (n = n.substring(0, o)), r = parseInt(n, 10), i = parseInt(n.split(".")[1], 10)), {
                    version: n,
                    major: r,
                    minor: i
                }
            }.bind(void 0, a, o)),
            enumerable: !0
        }
    }), Object.defineProperties(c, {
        android: {
            get: Object(i.D)(r.b),
            enumerable: !0
        },
        iOS: {
            get: Object(i.D)(r.j),
            enumerable: !0
        },
        mobile: {
            get: Object(i.D)(r.o),
            enumerable: !0
        },
        mac: {
            get: Object(i.D)(r.p),
            enumerable: !0
        },
        iPad: {
            get: Object(i.D)(r.k),
            enumerable: !0
        },
        iPhone: {
            get: Object(i.D)(r.l),
            enumerable: !0
        },
        windows: {
            get: Object(i.D)(function () {
                return -1 < o.indexOf("Windows")
            }),
            enumerable: !0
        },
        version: {
            get: Object(i.D)(function (e, t) {
                var n, r, i, o;
                if (e.windows) switch (n = u(/Windows(?: NT|)? ([._\d]+)/.exec(t), 1)) {
                case "6.1":
                    n = "7.0";
                    break;
                case "6.2":
                    n = "8.0";
                    break;
                case "6.3":
                    n = "8.1"
                } else e.android ? n = u(/Android ([._\d]+)/.exec(t), 1) : e.iOS ? n = u(/OS ([._\d]+)/.exec(t), 1) : e.mac && (n = u(/Mac OS X (10[._\d]+)/.exec(t), 1));
                return n && (r = parseInt(n, 10), (o = n.split(/[._]/)) && (i = parseInt(o[1], 10))), {
                    version: n,
                    major: r,
                    minor: i
                }
            }.bind(void 0, c, o)),
            enumerable: !0
        }
    }), Object.defineProperties(s, {
        flash: {
            get: Object(i.D)(r.h),
            enumerable: !0
        },
        flashVersion: {
            get: Object(i.D)(r.a),
            enumerable: !0
        },
        iframe: {
            get: Object(i.D)(r.m),
            enumerable: !0
        },
        passiveEvents: {
            get: Object(i.D)(function () {
                var e = !1;
                try {
                    var t = Object.defineProperty({}, "passive", {
                        get: function () {
                            return e = !0
                        }
                    });
                    window.addEventListener("testPassive", null, t), window.removeEventListener("testPassive", null, t)
                } catch (e) {}
                return e
            }),
            enumerable: !0
        },
        backgroundLoading: {
            get: Object(i.D)(function () {
                return !(c.iOS || a.safari)
            }),
            enumerable: !0
        }
    })
}, function (e, t, n) {
    "use strict";
    n.d(t, "i", function () {
        return a
    }), n.d(t, "e", function () {
        return c
    }), n.d(t, "q", function () {
        return s
    }), n.d(t, "j", function () {
        return l
    }), n.d(t, "s", function () {
        return f
    }), n.d(t, "r", function () {
        return d
    }), n.d(t, "u", function () {
        return p
    }), n.d(t, "d", function () {
        return g
    }), n.d(t, "a", function () {
        return b
    }), n.d(t, "o", function () {
        return m
    }), n.d(t, "p", function () {
        return y
    }), n.d(t, "v", function () {
        return w
    }), n.d(t, "t", function () {
        return j
    }), n.d(t, "h", function () {
        return O
    }), n.d(t, "b", function () {
        return k
    }), n.d(t, "g", function () {
        return C
    }), n.d(t, "c", function () {
        return P
    }), n.d(t, "m", function () {
        return x
    }), n.d(t, "k", function () {
        return S
    }), n.d(t, "n", function () {
        return E
    }), n.d(t, "l", function () {
        return T
    }), n.d(t, "f", function () {
        return A
    });
    var i, o = n(0),
        r = n(2),
        u = n(5);

    function a(e, t) {
        return e.classList.contains(t)
    }

    function c(e) {
        return l(e).firstChild
    }

    function s(e, t) {
        O(e),
            function (e, t) {
                if (t) {
                    for (var n = document.createDocumentFragment(), r = l(t).childNodes, i = 0; i < r.length; i++) n.appendChild(r[i].cloneNode(!0));
                    e.appendChild(n)
                }
            }(e, t)
    }

    function l(e) {
        var t = (i = i || new DOMParser).parseFromString(e, "text/html").body;
        f(t);
        for (var n = t.querySelectorAll("*"), r = n.length; r--;) d(n[r]);
        return t
    }

    function f(e) {
        for (var t = e.querySelectorAll("script,object,iframe"), n = t.length; n--;) {
            var r = t[n];
            r.parentNode.removeChild(r)
        }
        return e
    }

    function d(e) {
        for (var t = e.attributes, n = t.length; n--;) {
            var r = t[n].name;
            /^on/.test(r) && e.removeAttribute(r)
        }
        return e
    }

    function p(e) {
        return e + (0 < e.toString().indexOf("%") ? "" : "px")
    }

    function h(e) {
        return Object(o.y)(e.className) ? e.className.split(" ") : []
    }

    function v(e, t) {
        t = Object(r.i)(t), e.className !== t && (e.className = t)
    }

    function g(e) {
        return e.classList ? e.classList : h(e)
    }

    function b(e, t) {
        var n = h(e);
        (Array.isArray(t) ? t : t.split(" ")).forEach(function (e) {
            Object(o.e)(n, e) || n.push(e)
        }), v(e, n.join(" "))
    }

    function m(e, t) {
        var n = h(e),
            r = Array.isArray(t) ? t : t.split(" ");
        v(e, Object(o.i)(n, r).join(" "))
    }

    function y(e, t, n) {
        var r = e.className || "";
        t.test(r) ? r = r.replace(t, n) : n && (r += " " + n), v(e, r)
    }

    function w(e, t, n) {
        var r = a(e, t);
        (n = Object(o.s)(n) ? n : !r) !== r && (n ? b : m)(e, t)
    }

    function j(e, t, n) {
        e.setAttribute(t, n)
    }

    function O(e) {
        for (; e.firstChild;) e.removeChild(e.firstChild)
    }

    function k(e) {
        var t = document.createElement("link");
        t.rel = "stylesheet", t.href = e, document.getElementsByTagName("head")[0].appendChild(t)
    }

    function C(e) {
        e && O(e)
    }

    function P(e) {
        var t = {
            left: 0,
            right: 0,
            width: 0,
            height: 0,
            top: 0,
            bottom: 0
        };
        if (!e || !document.body.contains(e)) return t;
        var n = e.getBoundingClientRect(),
            r = window.pageYOffset,
            i = window.pageXOffset;
        return (n.width || n.height || n.left || n.top) && (t.left = n.left + i, t.right = n.right + i, t.top = n.top + r, t.bottom = n.bottom + r, t.width = n.right - n.left, t.height = n.bottom - n.top), t
    }

    function x(e, t) {
        e.insertBefore(t, e.firstChild)
    }

    function S(e) {
        return e.nextElementSibling
    }

    function E(e) {
        return e.previousElementSibling
    }

    function T(e, t) {
        var n = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {},
            r = document.createElement("a");
        r.href = e, r.target = t, r = Object(o.k)(r, n), u.Browser.firefox ? r.dispatchEvent(new MouseEvent("click", {
            bubbles: !0,
            cancelable: !0,
            view: window
        })) : r.click()
    }

    function A() {
        var e = window.screen.orientation;
        return !!e && ("landscape-primary" === e.type || "landscape-secondary" === e.type) || 90 === window.orientation || -90 === window.orientation
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "h", function () {
        return u
    }), n.d(t, "f", function () {
        return a
    }), n.d(t, "l", function () {
        return s
    }), n.d(t, "k", function () {
        return l
    }), n.d(t, "p", function () {
        return f
    }), n.d(t, "g", function () {
        return d
    }), n.d(t, "e", function () {
        return p
    }), n.d(t, "n", function () {
        return h
    }), n.d(t, "d", function () {
        return v
    }), n.d(t, "i", function () {
        return g
    }), n.d(t, "q", function () {
        return b
    }), n.d(t, "j", function () {
        return m
    }), n.d(t, "c", function () {
        return y
    }), n.d(t, "b", function () {
        return w
    }), n.d(t, "o", function () {
        return j
    }), n.d(t, "m", function () {
        return O
    }), n.d(t, "a", function () {
        return k
    });
    var r = navigator.userAgent;

    function i(e) {
        return null !== r.match(e)
    }

    function o(e) {
        return function () {
            return i(e)
        }
    }

    function u() {
        var e = k();
        return !!(e && 18 <= e)
    }
    var a = o(/gecko\//i),
        c = o(/trident\/.+rv:\s*11/i),
        s = o(/iP(hone|od)/i),
        l = o(/iPad/i),
        f = o(/Macintosh/i),
        d = o(/FBAV/i);

    function p() {
        return i(/\sEdge\/\d+/i)
    }

    function h() {
        return i(/msie/i)
    }

    function v() {
        return i(/\s(?:(?:Headless)?Chrome|CriOS)\//i) && !p() && !i(/UCBrowser/i)
    }

    function g() {
        return p() || c() || h()
    }

    function b() {
        return i(/safari/i) && !i(/(?:Chrome|CriOS|chromium|android|phantom)/i)
    }

    function m() {
        return i(/iP(hone|ad|od)/i)
    }

    function y() {
        return !(i(/chrome\/[123456789]/i) && !i(/chrome\/18/i) && !a()) && w()
    }

    function w() {
        return i(/Android/i) && !i(/Windows Phone/i)
    }

    function j() {
        return m() || w() || i(/Windows Phone/i)
    }

    function O() {
        try {
            return window.self !== window.top
        } catch (e) {
            return !0
        }
    }

    function k() {
        if (w()) return 0;
        var e, t = navigator.plugins;
        if (t && (e = t["Shockwave Flash"]) && e.description) return parseFloat(e.description.replace(/\D+(\d+\.?\d*).*/, "$1"));
        if (void 0 === window.ActiveXObject) return 0;
        try {
            if (e = new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash")) return parseFloat(e.GetVariable("$version").split(" ")[1].replace(/\s*,\s*/, "."))
        } catch (e) {
            return 0
        }
        return e
    }
}, function (e, t, n) {
    "use strict";

    function u(e) {
        return (u = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }
    n.r(t), n.d(t, "exists", function () {
        return i
    }), n.d(t, "isHTTPS", function () {
        return o
    }), n.d(t, "isFileProtocol", function () {
        return a
    }), n.d(t, "isRtmp", function () {
        return c
    }), n.d(t, "isYouTube", function () {
        return s
    }), n.d(t, "typeOf", function () {
        return l
    }), n.d(t, "isDeepKeyCompliant", function () {
        return f
    });
    var r = window.location.protocol;

    function i(e) {
        switch (u(e)) {
        case "string":
            return 0 < e.length;
        case "object":
            return null !== e;
        case "undefined":
            return !1;
        default:
            return !0
        }
    }

    function o() {
        return "https:" === r
    }

    function a() {
        return "file:" === r
    }

    function c(e, t) {
        return 0 === e.indexOf("rtmp:") || "rtmp" === t
    }

    function s(e, t) {
        return "youtube" === t || /^(http|\/\/).*(youtube\.com|youtu\.be)\/.+/.test(e)
    }

    function l(e) {
        if (null === e) return "null";
        var t = u(e);
        return "object" === t && Array.isArray(e) ? "array" : t
    }

    function f(r, i, o) {
        var e = Object.keys(r);
        return Object.keys(i).length >= e.length && e.every(function (e) {
            var t = r[e],
                n = i[e];
            return t && "object" === u(t) ? !(!n || "object" !== u(n)) && f(t, n, o) : o(e, r)
        })
    }
}, function (e, t, n) {
    "use strict";

    function c(e) {
        return (c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function r(e, t) {
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
        }
    }
    n.d(t, "a", function () {
        return o
    }), n.d(t, "c", function () {
        return u
    }), n.d(t, "d", function () {
        return a
    }), n.d(t, "b", function () {
        return s
    }), n.d(t, "e", function () {
        return l
    }), n.d(t, "f", function () {
        return f
    });
    var i = [].slice,
        o = (r(d.prototype, [{
            key: "on",
            value: function (e, t, n) {
                if (!h(this, "on", e, [t, n]) || !t) return this;
                var r = this._events || (this._events = {});
                return (r[e] || (r[e] = [])).push({
                    callback: t,
                    context: n
                }), this
            }
        }, {
            key: "once",
            value: function (e, t, n) {
                if (!h(this, "once", e, [t, n]) || !t) return this;

                function r() {
                    i++ || (o.off(e, r), t.apply(this, arguments))
                }
                var i = 0,
                    o = this;
                return r._callback = t, this.on(e, r, n)
            }
        }, {
            key: "off",
            value: function (e, t, n) {
                if (!this._events || !h(this, "off", e, [t, n])) return this;
                if (!e && !t && !n) return delete this._events, this;
                for (var r = e ? [e] : Object.keys(this._events), i = 0, o = r.length; i < o; i++) {
                    e = r[i];
                    var u = this._events[e];
                    if (u) {
                        var a = this._events[e] = [];
                        if (t || n)
                            for (var c = 0, s = u.length; c < s; c++) {
                                var l = u[c];
                                (t && t !== l.callback && t !== l.callback._callback || n && n !== l.context) && a.push(l)
                            }
                        a.length || delete this._events[e]
                    }
                }
                return this
            }
        }, {
            key: "trigger",
            value: function (e) {
                if (!this._events) return this;
                var t = i.call(arguments, 1);
                if (!h(this, "trigger", e, t)) return this;
                var n = this._events[e],
                    r = this._events.all;
                return n && v(n, t, this), r && v(r, arguments, this), this
            }
        }, {
            key: "triggerSafe",
            value: function (e) {
                if (!this._events) return this;
                var t = i.call(arguments, 1);
                if (!h(this, "trigger", e, t)) return this;
                var n = this._events[e],
                    r = this._events.all;
                return n && v(n, t, this, e), r && v(r, arguments, this, e), this
            }
        }]), d),
        u = o.prototype.on,
        a = o.prototype.once,
        s = o.prototype.off,
        l = o.prototype.trigger,
        f = o.prototype.triggerSafe;

    function d() {
        ! function (e) {
            if (!(e instanceof d)) throw new TypeError("Cannot call a class as a function")
        }(this)
    }
    o.on = u, o.once = a, o.off = s, o.trigger = l;
    var p = /\s+/;

    function h(e, t, n, r) {
        if (!n) return 1;
        if ("object" !== c(n)) {
            if (!p.test(n)) return 1;
            for (var i = n.split(p), o = 0, u = i.length; o < u; o++) e[t].apply(e, [i[o]].concat(r))
        } else
            for (var a in n) Object.prototype.hasOwnProperty.call(n, a) && e[t].apply(e, [a, n[a]].concat(r))
    }

    function v(e, t, n, r) {
        for (var i = -1, o = e.length; ++i < o;) {
            var u = e[i];
            if (r) try {
                u.callback.apply(u.context || n, t)
            } catch (e) {
                console.log('Error in "' + r + '" event handler:', e)
            } else u.callback.apply(u.context || n, t)
        }
    }
}, function (e, t, a) {
    "use strict";
    a.d(t, "a", function () {
        return v
    }), a.d(t, "d", function () {
        return n
    }), a.d(t, "b", function () {
        return g
    }), a.d(t, "c", function () {
        return r
    });
    var c = a(29),
        s = a(30),
        l = a(17),
        f = a(13),
        d = a(38),
        p = a(1),
        h = null,
        v = {};

    function n(e) {
        return h || (i = (t = e).get("controls"), o = b(), u = function (e) {
            var t = e.get("playlist");
            if (Array.isArray(t) && t.length)
                for (var n = Object(s.c)(Object(c.a)(t[0]), e), r = 0; r < n.length; r++)
                    for (var i = n[r], o = e.getProviders(), u = 0; u < l.a.length; u++) {
                        var a = l.a[u];
                        if (o.providerSupports(a, i)) return "html5" === a.name
                    }
            return !1
        }(t), h = i && o && u ? (n = a.e(6).then(function (e) {
            a(37);
            var t = a(22).default;
            return d.a.controls = a(21).default, Object(f.a)(a(51).default), t
        }.bind(null, a)).catch(g(p.t + 105)), v.html5 = n) : i && u ? (r = a.e(4).then(function (e) {
            var t = a(22).default;
            return d.a.controls = a(21).default, Object(f.a)(a(51).default), t
        }.bind(null, a)).catch(g(p.t + 104)), v.html5 = r) : i && o ? a.e(5).then(function (e) {
            a(37);
            var t = a(22).default;
            return d.a.controls = a(21).default, t
        }.bind(null, a)).catch(g(p.t + 103)) : i ? a.e(3).then(function (e) {
            var t = a(22).default;
            return d.a.controls = a(21).default, t
        }.bind(null, a)).catch(g(p.t + 102)) : (b() ? a.e(8).then(function (e) {
            return a(37)
        }.bind(null, a)).catch(g(p.t + 120)) : Promise.resolve()).then(function () {
            return a.e(2).then(function (e) {
                return a(22).default
            }.bind(null, a)).catch(g(p.t + 101))
        })), h;
        var t, n, r, i, o, u
    }

    function g(e, t) {
        return function () {
            throw new p.s(p.m, e, t)
        }
    }

    function r(e, t) {
        return function () {
            throw new p.s(null, e, t)
        }
    }

    function b() {
        var e = window.IntersectionObserverEntry;
        return !(e && "IntersectionObserver" in window && "intersectionRatio" in e.prototype)
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return i
    }), n.d(t, "b", function () {
        return o
    }), n.d(t, "d", function () {
        return l
    }), n.d(t, "e", function () {
        return f
    }), n.d(t, "c", function () {
        return d
    });
    var s = n(2),
        r = n(41),
        a = n.n(r);

    function c(e) {
        return (c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }
    var u, i = a.a.clear;

    function o(e, t, n, r) {
        n = n || "all-players";
        var i, o, u = "";
        "object" === c(t) ? (l(i = document.createElement("div"), t), o = i.style.cssText, Object.prototype.hasOwnProperty.call(t, "content") && o && (o = "".concat(o, ' content: "').concat(t.content, '";')), r && o && (o = o.replace(/;/g, " !important;")), u = "{" + o + "}") : "string" == typeof t && (u = t), "" !== u && "{}" !== u ? a.a.style([
            [e, e + u]
        ], n) : a.a.clear(n, e)
    }

    function l(e, t) {
        if (null != e) {
            var n;
            void 0 === e.length && (e = [e]);
            var r = {};
            for (n in t) Object.prototype.hasOwnProperty.call(t, n) && (r[n] = "" === (c = t[a = n]) || null == c ? "" : "string" == typeof c && isNaN(c) ? /png|gif|jpe?g/i.test(c) && c.indexOf("url") < 0 ? "url(" + c + ")" : c : 0 === c || "z-index" === a || "opacity" === a ? "" + c : /color/i.test(a) ? "#" + Object(s.e)(c.toString(16).replace(/^0x/i, ""), 6) : Math.ceil(c) + "px");
            for (var i = 0; i < e.length; i++) {
                var o, u = e[i];
                if (null != u)
                    for (n in r) Object.prototype.hasOwnProperty.call(r, n) && (o = function (e) {
                        e = e.split("-");
                        for (var t = 1; t < e.length; t++) e[t] = e[t].charAt(0).toUpperCase() + e[t].slice(1);
                        return e.join("")
                    }(n), u.style[o] !== r[n] && (u.style[o] = r[n]))
            }
        }
        var a, c
    }

    function f(e, t) {
        l(e, {
            transform: t,
            webkitTransform: t,
            msTransform: t,
            mozTransform: t,
            oTransform: t
        })
    }

    function d(e, t) {
        var n, r = "rgb",
            i = void 0 !== t && 100 !== t;
        i && (r += "a"), u || ((n = document.createElement("canvas")).height = 1, n.width = 1, u = n.getContext("2d")), e ? isNaN(parseInt(e, 16)) || (e = "#" + e) : e = "#000000", u.clearRect(0, 0, 1, 1), u.fillStyle = e, u.fillRect(0, 0, 1, 1);
        var o = u.getImageData(0, 0, 1, 1).data;
        return r += "(" + o[0] + ", " + o[1] + ", " + o[2], i && (r += ", " + t / 100), r + ")"
    }
}, function (e, t, n) {
    "use strict";
    n.r(t), n.d(t, "getAbsolutePath", function () {
        return r
    }), n.d(t, "isAbsolutePath", function () {
        return s
    }), n.d(t, "parseXML", function () {
        return i
    }), n.d(t, "serialize", function () {
        return o
    }), n.d(t, "parseDimension", function () {
        return a
    }), n.d(t, "timeFormat", function () {
        return l
    });
    var c = n(8),
        u = n(0);

    function r(e, t) {
        if (Object(c.exists)(t) || (t = document.location.href), Object(c.exists)(e)) {
            if (s(e)) return e;
            var n, r, i = t.substring(0, t.indexOf("://") + 3),
                o = t.substring(i.length, t.indexOf("/", i.length + 1));
            r = 0 === e.indexOf("/") ? e.split("/") : (n = (n = t.split("?")[0]).substring(i.length + o.length + 1, n.lastIndexOf("/"))).split("/").concat(e.split("/"));
            for (var u = [], a = 0; a < r.length; a++) r[a] && Object(c.exists)(r[a]) && "." !== r[a] && (".." === r[a] ? u.pop() : u.push(r[a]));
            return i + o + "/" + u.join("/")
        }
    }

    function s(e) {
        return /^(?:(?:https?|file):)?\/\//.test(e)
    }

    function i(e) {
        var t = null;
        try {
            (t = (new window.DOMParser).parseFromString(e, "text/xml")).querySelector("parsererror") && (t = null)
        } catch (e) {}
        return t
    }

    function o(e) {
        if (void 0 === e) return null;
        if ("string" == typeof e && e.length < 6) {
            var t = e.toLowerCase();
            if ("true" === t) return !0;
            if ("false" === t) return !1;
            if (!Object(u.v)(Number(e)) && !Object(u.v)(parseFloat(e))) return Number(e)
        }
        return e
    }

    function a(e) {
        return "string" == typeof e ? "" === e ? 0 : -1 < e.lastIndexOf("%") ? e : parseInt(e.replace("px", ""), 10) : e
    }

    function l(e, t) {
        if (e <= 0 && !t || Object(u.v)(parseInt(e))) return "00:00";
        var n = e < 0 ? "-" : "";
        e = Math.abs(e);
        var r = Math.floor(e / 3600),
            i = Math.floor((e - 3600 * r) / 60),
            o = Math.floor(e % 60);
        return n + (r ? r + ":" : "") + (i < 10 ? "0" : "") + i + ":" + (o < 10 ? "0" : "") + o
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return a
    });
    var r = n(33),
        i = n(17),
        o = n(57),
        u = n(0);

    function a(e) {
        var t = e.getName().name;
        if (!r.a[t]) {
            if (!Object(u.m)(i.a, Object(u.C)({
                    name: t
                }))) {
                if (!Object(u.u)(e.supports)) throw new Error("Tried to register a provider with an invalid object");
                i.a.unshift({
                    name: t,
                    supports: e.supports
                })
            }
            Object(u.h)(e.prototype, o.a), r.a[t] = e
        }
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "j", function () {
        return p
    }), n.d(t, "d", function () {
        return h
    }), n.d(t, "b", function () {
        return v
    }), n.d(t, "e", function () {
        return b
    }), n.d(t, "g", function () {
        return y
    }), n.d(t, "h", function () {
        return w
    }), n.d(t, "c", function () {
        return j
    }), n.d(t, "f", function () {
        return k
    }), n.d(t, "i", function () {
        return C
    }), n.d(t, "a", function () {
        return P
    });
    var a = n(0),
        r = n(7),
        u = n(28),
        i = n(8),
        o = n(40),
        c = {},
        s = {
            zh: "Chinese",
            nl: "Dutch",
            en: "English",
            fr: "French",
            de: "German",
            it: "Italian",
            ja: "Japanese",
            pt: "Portuguese",
            ru: "Russian",
            es: "Spanish",
            el: "Greek",
            fi: "Finnish",
            id: "Indonesian",
            ko: "Korean",
            th: "Thai",
            vi: "Vietnamese"
        },
        l = Object(a.r)(s);

    function f(e) {
        var t = d(e),
            n = t.indexOf("_");
        return -1 === n ? t : t.substring(0, n)
    }

    function d(e) {
        return e.toLowerCase().replace("-", "_")
    }

    function p(n) {
        return n ? Object.keys(n).reduce(function (e, t) {
            return e[d(t)] = n[t], e
        }, {}) : {}
    }

    function h(e) {
        if (e) return 3 !== e.length && s[f(e)] || e
    }

    function v(e) {
        return l[e] || ""
    }

    function g(e) {
        var t = e.querySelector("html");
        return t ? t.getAttribute("lang") : null
    }

    function b() {
        var e = g(document);
        if (!e && Object(r.m)()) try {
            e = g(window.top.document)
        } catch (e) {}
        return e || navigator.language || "en"
    }
    var m = ["ar", "da", "de", "el", "es", "fi", "fr", "he", "id", "it", "ja", "ko", "nl", "no", "oc", "pt", "ro", "ru", "sl", "sv", "th", "tr", "vi", "zh"];

    function y(e) {
        return 8207 === e.charCodeAt(0) || /^[\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC]/.test(e)
    }

    function w(e) {
        return 0 <= m.indexOf(f(e))
    }

    function j(e, t, n) {
        return Object(a.k)({}, function (e) {
            var t = e.advertising,
                n = e.related,
                r = e.sharing,
                i = e.abouttext,
                o = Object(a.k)({}, e.localization);
            t && (o.advertising = o.advertising || {}, O(o.advertising, t, "admessage"), O(o.advertising, t, "cuetext"), O(o.advertising, t, "loadingAd"), O(o.advertising, t, "podmessage"), O(o.advertising, t, "skipmessage"), O(o.advertising, t, "skiptext")), "string" == typeof o.related ? o.related = {
                heading: o.related
            } : o.related = o.related || {}, n && O(o.related, n, "autoplaymessage"), r && (o.sharing = o.sharing || {}, O(o.sharing, r, "heading"), O(o.sharing, r, "copied")), i && O(o, e, "abouttext");
            var u = o.close || o.nextUpClose;
            return u && (o.close = u), o
        }(e), t[f(n)], t[d(n)])
    }

    function O(e, t, n) {
        var r = e[n] || t[n];
        r && (e[n] = r)
    }

    function k(e) {
        return Object(i.isDeepKeyCompliant)(o.a, e, function (e, t) {
            return "string" == typeof t[e]
        })
    }

    function C(e, o) {
        var t, n = c[o];
        return n || (t = "".concat(e, "translations/").concat(f(o), ".json"), c[o] = n = new Promise(function (e, i) {
            Object(u.a)({
                url: t,
                oncomplete: e,
                onerror: function (e, t, n, r) {
                    c[o] = null, i(r)
                },
                responseType: "json"
            })
        })), n
    }

    function P(e, t) {
        var n = Object(a.k)({}, e, t);
        return x(n, "errors", e, t), x(n, "related", e, t), x(n, "sharing", e, t), x(n, "advertising", e, t), x(n, "shortcuts", e, t), n
    }

    function x(e, t, n, r) {
        e[t] = Object(a.k)({}, n[t], r[t])
    }
}, function (e, t, n) {
    "use strict";
    t.a = []
}, function (e, t, n) {
    "use strict";
    t.a = {
        debug: !1
    }
}, function (e, t, n) {
    "use strict";
    var r = n(27),
        u = n(5),
        o = n(23),
        i = n(0),
        a = n(8),
        c = n(36),
        s = Object(i.m)(r.a, Object(i.C)({
            name: "html5"
        })),
        l = s.supports;

    function f(e) {
        var t = window.MediaSource;
        return Object(i.a)(e, function (e) {
            return !!t && !!t.isTypeSupported && t.isTypeSupported(e)
        })
    }

    function d(e) {
        if (e.drm) return !1;
        var t = -1 < e.file.indexOf(".m3u8"),
            n = "hls" === e.type || "m3u8" === e.type;
        if (!t && !n) return !1;
        var r = u.Browser.chrome || u.Browser.firefox || u.Browser.edge || u.Browser.ie && 11 === u.Browser.version.major,
            i = u.OS.android && !1 === e.hlsjsdefault,
            o = u.Browser.safari && !!e.safarihlsjs;
        return f(e.mediaTypes || ['video/mp4;codecs="avc1.4d400d,mp4a.40.2"']) && (r || o) && !i
    }
    s.supports = function (e, t) {
        var n = l.apply(this, arguments);
        if (n && e.drm && "hls" === e.type) {
            var r = Object(o.a)(t)("drm");
            if (r && e.drm.fairplay) {
                var i = window.WebKitMediaKeys;
                return i && i.isTypeSupported && i.isTypeSupported("com.apple.fps.1_0", "video/mp4")
            }
            return r
        }
        return n
    }, r.a.push({
        name: "shaka",
        supports: function (e) {
            return !(e.drm && !Object(c.a)(e.drm)) && !(!window.HTMLVideoElement || !window.MediaSource) && f(e.mediaTypes) && ("dash" === e.type || "mpd" === e.type || -1 < (e.file || "").indexOf("mpd-time-csf"))
        }
    }), r.a.unshift({
        name: "hlsjs",
        supports: d
    }), r.a.unshift({
        name: "hlsjsProgressive",
        supports: function (e) {
            return e._hlsjsProgressive && d(e)
        }
    }), r.a.push({
        name: "flash",
        supports: function (e) {
            if (!u.Features.flash || e.drm) return !1;
            var t = e.type;
            return "hls" === t || "m3u8" === t || !Object(a.isRtmp)(e.file, t) && -1 < ["flv", "f4v", "mov", "m4a", "m4v", "mp4", "aac", "f4a", "mp3", "mpeg", "smil"].indexOf(t)
        }
    }), t.a = r.a
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return r
    });
    var r = Date.now || function () {
        return (new Date).getTime()
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "b", function () {
        return r
    }), n.d(t, "c", function () {
        return i
    }), n.d(t, "a", function () {
        return o
    });
    var a = n(0),
        r = function (e) {
            return e.replace(/^(.*\/)?([^-]*)-?.*\.(js)$/, "$2")
        };

    function i(e) {
        var t = 305e3;
        if (!e) return t;
        switch (r(e)) {
        case "jwpsrv":
            t = 305001;
            break;
        case "googima":
            t = 305002;
            break;
        case "vast":
            t = 305003;
            break;
        case "freewheel":
            t = 305004;
            break;
        case "dai":
            t = 305005;
            break;
        case "gapro":
            t = 305006
        }
        return t
    }

    function o(e, t, n) {
        var r = e.name,
            i = document.createElement("div");
        i.id = n.id + "_" + r, i.className = "jw-plugin jw-reset";
        var o = Object(a.k)({}, t),
            u = e.getNewInstance(n, o, i);
        return n.addPlugin(r, u), u
    }
}, function (e, t, n) {
    "use strict";
    n.r(t), n.d(t, "getScriptPath", function () {
        return i
    }), n.d(t, "repo", function () {
        return u
    }), n.d(t, "versionCheck", function () {
        return a
    }), n.d(t, "loadFrom", function () {
        return c
    });
    var o = n(31),
        r = n(8),
        i = function (e) {
            for (var t = document.getElementsByTagName("script"), n = 0; n < t.length; n++) {
                var r = t[n].src;
                if (r) {
                    var i = r.lastIndexOf("/" + e);
                    if (0 <= i) return r.substr(0, i + 1)
                }
            }
            return ""
        },
        u = function () {
            var e = Object(r.isFileProtocol)() ? "https:" : "";
            return "".concat(e).concat("//ssl.p.jwpcdn.com/player/v/8.11.3/")
        },
        a = function (e) {
            var t = ("0" + e).split(/\W/),
                n = o.a.split(/\W/),
                r = parseFloat(t[0]),
                i = parseFloat(n[0]);
            return !(i < r || r === i && parseFloat("0" + t[1]) > parseFloat(n[1]))
        },
        c = function () {
            return u()
        }
}, , , function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return h
    });
    var r = "free",
        i = "starter",
        o = "business",
        u = "premium",
        a = "enterprise",
        c = "developer",
        s = "platinum",
        l = "ads",
        f = "unlimited",
        d = "trial",
        p = "invalid";

    function h(t) {
        var n = {
            setup: [r, i, o, u, a, c, l, f, d, s],
            drm: [a, c, l, f, d],
            ads: [l, f, d, s, a, c, o],
            jwpsrv: [r, i, o, u, a, c, l, d, s, p],
            discovery: [l, a, c, d, f]
        };
        return function (e) {
            return n[e] && -1 < n[e].indexOf(t)
        }
    }
}, function (e, t, n) {
    "use strict";
    var r = n(0),
        i = n(9),
        o = n(3),
        u = {};

    function a(a, c, s) {
        var t = this,
            n = 0;

        function l(e) {
            n = 2, t.trigger(o.w, e).off()
        }

        function f(e) {
            n = 3, t.trigger(o.lb, e).off()
        }
        this.getStatus = function () {
            return n
        }, this.load = function () {
            var e = u[a];
            return 0 !== n || (e && e.then(f).catch(l), n = 1, e = new Promise(function (t, n) {
                function r() {
                    i.onerror = i.onload = null, clearTimeout(o)
                }

                function e(e) {
                    r(), l(e), n(e)
                }
                var i = (c ? function (e) {
                        var t = document.createElement("link");
                        return t.type = "text/css", t.rel = "stylesheet", t.href = e, t
                    } : function (e, t) {
                        var n = document.createElement("script");
                        return n.type = "text/javascript", n.charset = "utf-8", n.async = !0, n.timeout = t || 45e3, n.src = e, n
                    })(a, s),
                    o = setTimeout(function () {
                        e(new Error("Network timeout ".concat(a)))
                    }, 45e3);
                i.onerror = function () {
                    e(new Error("Failed to load ".concat(a)))
                }, i.onload = function (e) {
                    r(), f(e), t(e)
                };
                var u = document.getElementsByTagName("head")[0] || document.documentElement;
                u.insertBefore(i, u.firstChild)
            }), u[a] = e), e
        }
    }
    Object(r.k)(a.prototype, i.a), t.a = a
}, function (e, t, n) {
    "use strict";
    var u = n(1),
        a = n(19);

    function c(e) {
        return (c = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function r() {
        this.load = function (r, i, e, o) {
            return e && "object" === c(e) ? Promise.all(Object.keys(e).filter(function (e) {
                return e
            }).map(function (t) {
                var n = e[t];
                return i.setupPlugin(t).then(function (e) {
                    if (!o.attributes._destroyed) return Object(a.a)(e, n, r)
                }).catch(function (e) {
                    return i.removePlugin(t), e.code ? e : new u.s(null, Object(a.c)(t), e)
                })
            })) : Promise.resolve()
        }
    }

    function i() {}
    var o = n(58),
        s = n(47),
        l = {},
        f = i.prototype;
    f.setupPlugin = function (e) {
        var t = this.getPlugin(e);
        return t ? (t.url !== e && Object(s.a)('JW Plugin "'.concat(Object(a.b)(e), '" already loaded from "').concat(t.url, '". Ignoring "').concat(e, '."')), t.promise) : this.addPlugin(e).load()
    }, f.addPlugin = function (e) {
        var t = Object(a.b)(e),
            n = l[t];
        return n || (n = new o.a(e), l[t] = n), n
    }, f.getPlugin = function (e) {
        return l[Object(a.b)(e)]
    }, f.removePlugin = function (e) {
        delete l[Object(a.b)(e)]
    }, f.getPlugins = function () {
        return l
    };
    var d = i;
    n.d(t, "b", function () {
        return h
    }), n.d(t, "a", function () {
        return v
    });
    var p = new d,
        h = function (e, t, n) {
            var r = p.addPlugin(e);
            r.js || r.registerPlugin(e, t, n)
        };

    function v(t, e) {
        var n = t.get("plugins");
        return window.jwplayerPluginJsonp = h, (t.pluginLoader = t.pluginLoader || new r).load(e, p, n, t).then(function (e) {
            if (!t.attributes._destroyed) return delete window.jwplayerPluginJsonp, e
        })
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return l
    });
    var u = n(48),
        a = n(23),
        c = n(45),
        s = n(1),
        l = 100013;
    t.b = function (t) {
        var n, r, e;
        try {
            var i, o = Object(u.a)(t || "", Object(c.a)("NDh2aU1Cb0NHRG5hcDFRZQ==")).split("/");
            "pro" === (n = o[0]) && (n = "premium"), Object(a.a)(n)("setup") || (n = "invalid"), 2 < o.length && (r = o[1], 0 < (i = parseInt(o[2])) && (e = new Date).setTime(i))
        } catch (t) {
            n = "invalid"
        }
        this.edition = function () {
            return n
        }, this.token = function () {
            return r
        }, this.expiration = function () {
            return e
        }, this.duration = function () {
            return e ? e.getTime() - (new Date).getTime() : 0
        }, this.error = function () {
            var e;
            return void 0 === t ? e = 100011 : "invalid" !== n && r ? this.duration() < 0 && (e = l) : e = 100012, e ? new s.s(s.m, e) : null
        }
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "b", function () {
        return i
    });
    var o = n(65),
        u = n(8),
        a = n(39),
        c = {
            aac: "audio/mp4",
            mp4: "video/mp4",
            f4v: "video/mp4",
            m4v: "video/mp4",
            mov: "video/mp4",
            mp3: "audio/mpeg",
            mpeg: "audio/mpeg",
            ogv: "video/ogg",
            ogg: "video/ogg",
            oga: "video/ogg",
            vorbis: "video/ogg",
            webm: "video/webm",
            f4a: "video/aac",
            m3u8: "application/vnd.apple.mpegurl",
            m3u: "application/vnd.apple.mpegurl",
            hls: "application/vnd.apple.mpegurl"
        },
        r = [{
            name: "html5",
            supports: i
        }];

    function i(e) {
        if (!1 === Object(o.a)(e)) return !1;
        if (!a.a.canPlayType) return !1;
        var t = e.file,
            n = e.type;
        if (Object(u.isRtmp)(t, n)) return !1;
        var r = e.mimeType || c[n];
        if (!r) return !1;
        var i = e.mediaTypes;
        return i && i.length && (r = [r].concat(i.slice()).join("; ")), !!a.a.canPlayType(r)
    }
    t.a = r
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return C
    });
    var l = n(0),
        f = n(12),
        d = n(8),
        p = n(1),
        h = 1,
        v = 2,
        g = 3,
        b = 4,
        m = 5,
        y = 6,
        w = 7,
        i = 601,
        j = 602,
        O = 611,
        k = function () {};

    function C(e, t, n, r) {
        var i;
        e === Object(e) && (e = (r = e).url);
        var o, u, a, c = Object(l.k)({
                xhr: null,
                url: e,
                withCredentials: !1,
                retryWithoutCredentials: !1,
                timeout: 6e4,
                timeoutId: -1,
                oncomplete: t || k,
                onerror: n || k,
                mimeType: r && !r.responseType ? "text/xml" : "",
                requireValidXML: !1,
                responseType: r && r.plainText ? "text" : "",
                useDomParser: !1,
                requestFilter: null
            }, r),
            s = (o = c, function (e, t) {
                var n = e.currentTarget || o.xhr;
                if (clearTimeout(o.timeoutId), o.retryWithoutCredentials && o.xhr.withCredentials) return P(n), void C(Object(l.k)({}, o, {
                    xhr: null,
                    withCredentials: !1,
                    retryWithoutCredentials: !1
                }));
                !t && 400 <= n.status && n.status < 600 && (t = n.status), x(o, t ? p.o : p.r, t || y, e)
            });
        if ("XMLHttpRequest" in window) {
            if (i = c.xhr = c.xhr || new window.XMLHttpRequest, "function" == typeof c.requestFilter) {
                try {
                    u = c.requestFilter({
                        url: e,
                        xhr: i
                    })
                } catch (e) {
                    return s(e, m), i
                }
                u && "open" in u && "send" in u && (i = c.xhr = u)
            }
            i.onreadystatechange = (a = c, function (e) {
                var i, t = e.currentTarget || a.xhr;
                if (4 === t.readyState) {
                    clearTimeout(a.timeoutId);
                    var n = t.status;
                    if (400 <= n) return void x(a, p.o, n < 600 ? n : y);
                    if (200 === n) return i = a,
                        function (e) {
                            var t = e.currentTarget || i.xhr;
                            if (clearTimeout(i.timeoutId), i.responseType) {
                                if ("json" === i.responseType) return function (e, t) {
                                    if (!e.response || "string" == typeof e.response && '"' !== e.responseText.substr(1)) try {
                                        e = Object(l.k)({}, e, {
                                            response: JSON.parse(e.responseText)
                                        })
                                    } catch (e) {
                                        return void x(t, p.o, O, e)
                                    }
                                    return t.oncomplete(e)
                                }(t, i)
                            } else {
                                var n, r = t.responseXML;
                                if (r) try {
                                    n = r.firstChild
                                } catch (e) {}
                                if (r && n) return S(t, r, i);
                                if (i.useDomParser && t.responseText && !r && (r = Object(f.parseXML)(t.responseText)) && r.firstChild) return S(t, r, i);
                                if (i.requireValidXML) return void x(i, p.o, j)
                            }
                            i.oncomplete(t)
                        }(e);
                    0 === n && Object(d.isFileProtocol)() && !/^[a-z][a-z0-9+.-]*:/.test(a.url) && x(a, p.o, w)
                }
            }), i.onerror = s, "overrideMimeType" in i ? c.mimeType && i.overrideMimeType(c.mimeType) : c.useDomParser = !0;
            try {
                e = e.replace(/#.*$/, ""), i.open("GET", e, !0)
            } catch (e) {
                return s(e, g), i
            }
            if (c.responseType) try {
                i.responseType = c.responseType
            } catch (e) {}
            c.timeout && (c.timeoutId = setTimeout(function () {
                P(i), x(c, p.r, h)
            }, c.timeout), i.onabort = function () {
                clearTimeout(c.timeoutId)
            });
            try {
                c.withCredentials && "withCredentials" in i && (i.withCredentials = !0), i.send()
            } catch (e) {
                s(e, b)
            }
            return i
        }
        x(c, p.r, v)
    }

    function P(e) {
        e.onload = null, e.onprogress = null, e.onreadystatechange = null, e.onerror = null, "abort" in e && e.abort()
    }

    function x(e, t, n, r) {
        e.onerror(t, e.url, e.xhr, new p.s(t, n, r))
    }

    function S(e, t, n) {
        var r = t.documentElement;
        if (!n.requireValidXML || "parsererror" !== r.nodeName && !r.getElementsByTagName("parsererror").length) return e.responseXML || (e = Object(l.k)({}, e, {
            responseXML: t
        })), n.oncomplete(e);
        x(n, p.o, i)
    }
}, function (e, t, n) {
    "use strict";

    function o(e) {
        if (e && e.file) return Object(u.k)({}, {
            kind: "captions",
            default: !1
        }, e)
    }
    var u = n(0),
        a = n(34),
        c = Array.isArray;
    t.a = function (e) {
        c((e = e || {}).tracks) || delete e.tracks;
        var t = Object(u.k)({}, {
            sources: [],
            tracks: [],
            minDvrWindow: 120,
            dvrSeekLimit: 25
        }, e);
        t.dvrSeekLimit < 5 && (t.dvrSeekLimit = 5), t.sources !== Object(t.sources) || c(t.sources) || (t.sources = [Object(a.a)(t.sources)]), c(t.sources) && 0 !== t.sources.length || (e.levels ? t.sources = e.levels : t.sources = [Object(a.a)(e)]);
        for (var n = 0; n < t.sources.length; n++) {
            var r, i = t.sources[n];
            i && (r = i.default, i.default = !!r && "true" === r.toString(), t.sources[n].label || (t.sources[n].label = n.toString()), t.sources[n] = Object(a.a)(t.sources[n]))
        }
        return t.sources = t.sources.filter(function (e) {
            return !!e
        }), c(t.tracks) || (t.tracks = []), c(t.captions) && (t.tracks = t.tracks.concat(t.captions), delete t.captions), t.tracks = t.tracks.map(o).filter(function (e) {
            return !!e
        }), t
    }
}, function (e, t, n) {
    "use strict";
    var u = n(0),
        r = {
            none: !0,
            metadata: !0,
            auto: !0
        };

    function c(e, t) {
        return r[e] ? e : r[t] ? t : "metadata"
    }
    var i = n(29),
        s = n(34),
        o = n(42),
        a = n(1);

    function l(e, t, n) {
        var r = Object(u.k)({}, n);
        return delete r.playlist, e.map(function (e) {
            return d(t, e, r)
        }).filter(function (e) {
            return !!e
        })
    }

    function f(e) {
        if (!Array.isArray(e) || 0 === e.length) throw new a.s(a.o, 630)
    }

    function d(e, t, n) {
        var r = e.getProviders(),
            i = e.get("preload"),
            o = Object(u.k)({}, t);
        if (o.preload = c(t.preload, i), o.allSources = h(t, e), o.sources = v(o.allSources, r), o.sources.length) return o.file = o.sources[0].file, o.feedData = n, o
    }
    n.d(t, "b", function () {
        return l
    }), n.d(t, "e", function () {
        return f
    }), n.d(t, "d", function () {
        return d
    }), n.d(t, "c", function () {
        return p
    });
    var p = function (e, t) {
        return v(h(e, t), t.getProviders())
    };

    function h(e, t) {
        var r = t.attributes,
            n = e.sources,
            i = e.allSources,
            o = e.preload,
            u = e.drm,
            a = g(e.withCredentials, r.withCredentials);
        return (i || n).map(function (e) {
            if (e !== Object(e)) return null;
            b(e, r, "androidhls"), b(e, r, "hlsjsdefault"), b(e, r, "safarihlsjs"), b(e, r, "_hlsjsProgressive"), e.preload = c(e.preload, o);
            var t = e.drm || u || r.drm;
            t && (e.drm = t);
            var n = g(e.withCredentials, a);
            return void 0 !== n && (e.withCredentials = n), Object(s.a)(e)
        }).filter(function (e) {
            return !!e
        })
    }

    function v(e, t) {
        t && t.choose || (t = new o.a);
        var n = function (e, t) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n],
                    i = t.choose(r).providerToCheck;
                if (i) return {
                    type: r.type,
                    provider: i
                }
            }
            return null
        }(e, t);
        if (!n) return [];
        var r = n.provider,
            i = n.type;
        return e.filter(function (e) {
            return e.type === i && t.providerSupports(r, e)
        })
    }

    function g(e, t) {
        return void 0 === e ? t : e
    }

    function b(e, t, n) {
        n in t && (e[n] = t[n])
    }
    t.a = function (e) {
        return (Array.isArray(e) ? e : [e]).map(i.a)
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return r
    });
    var r = "8.11.3+commercial_v8-11-3.359.commercial.bfe7aa9.hlsjs..hlsjsprogressive..jwplayer.cb71e15.dai.45542e3.freewheel.556af86.googima.1dc0927.vast.3e049bc.analytics.2d55e20.gapro.141397a"
}, function (e, t, n) {
    "use strict";
    var a = n(0),
        r = n(18),
        i = window.performance || {
            timing: {}
        },
        o = i.timing.navigationStart || Object(r.a)();

    function c() {
        return o + i.now()
    }
    "now" in i || (i.now = function () {
        return Object(r.a)() - o
    }), t.a = function () {
        var r = {},
            i = {},
            o = {},
            u = {};
        return {
            start: function (e) {
                r[e] = c(), o[e] = o[e] + 1 || 1
            },
            end: function (e) {
                var t;
                r[e] && (t = c() - r[e], delete r[e], i[e] = i[e] + t || t)
            },
            dump: function () {
                var e, t = Object(a.k)({}, i);
                for (var n in r) {
                    Object.prototype.hasOwnProperty.call(r, n) && (e = c() - r[n], t[n] = t[n] + e || e)
                }
                return {
                    counts: Object(a.k)({}, o),
                    sums: t,
                    events: Object(a.k)({}, u)
                }
            },
            tick: function (e) {
                u[e] = c()
            },
            clear: function (e) {
                delete u[e]
            },
            between: function (e, t) {
                return u[t] && u[e] ? u[t] - u[e] : null
            }
        }
    }
}, function (e, t, n) {
    "use strict";
    t.a = {}
}, function (e, t, n) {
    "use strict";
    var r = n(0),
        i = n(8),
        o = n(2);
    t.a = function (e) {
        if (e && e.file) {
            var t = Object(r.k)({}, {
                default: !1
            }, e);
            t.file = Object(o.i)("" + t.file);
            var n = /^[^/]+\/(?:x-)?([^/]+)$/;
            if (n.test(t.type) && (t.mimeType = t.type, t.type = t.type.replace(n, "$1")), Object(i.isYouTube)(t.file) ? t.type = "youtube" : Object(i.isRtmp)(t.file) ? t.type = "rtmp" : t.type || (t.type = Object(o.a)(t.file)), t.type) {
                switch (t.type) {
                case "m3u8":
                case "vnd.apple.mpegurl":
                    t.type = "hls";
                    break;
                case "dash+xml":
                    t.type = "dash";
                    break;
                case "m4a":
                    t.type = "aac";
                    break;
                case "smil":
                    t.type = "rtmp"
                }
                return Object.keys(t).forEach(function (e) {
                    "" === t[e] && delete t[e]
                }), t
            }
        }
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return k
    }), n.d(t, "b", function () {
        return E
    });
    var r = n(5),
        u = n(3),
        i = n(9),
        h = n(18),
        v = n(6);

    function a(e) {
        return (a = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function o(e, t) {
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
        }
    }

    function c(e, t, n) {
        return (c = "undefined" != typeof Reflect && Reflect.get ? Reflect.get : function (e, t, n) {
            var r = function (e, t) {
                for (; !Object.prototype.hasOwnProperty.call(e, t) && null !== (e = s(e)););
                return e
            }(e, t);
            if (r) {
                var i = Object.getOwnPropertyDescriptor(r, t);
                return i.get ? i.get.call(n) : i.value
            }
        })(e, t, n || e)
    }

    function s(e) {
        return (s = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    function l(e, t) {
        return (l = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }
    var g, b, f = "ontouchstart" in window,
        m = "PointerEvent" in window && !r.OS.android,
        y = !(m || f && r.OS.mobile),
        w = "window",
        j = "keydown",
        O = r.Features.passiveEvents,
        d = !!O && {
            passive: !0
        },
        k = (function (e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    writable: !0,
                    configurable: !0
                }
            }), t && l(e, t)
        }(C, i.a), o(C.prototype, [{
            key: "on",
            value: function (e, t, n) {
                return P(e) && (this.handlers[e] || S[e](this)), c(s(C.prototype), "on", this).call(this, e, t, n)
            }
        }, {
            key: "off",
            value: function (e, t, n) {
                var r, i = this;
                return P(e) ? A(this, e) : e || (r = this.handlers, Object.keys(r).forEach(function (e) {
                    A(i, e)
                })), c(s(C.prototype), "off", this).call(this, e, t, n)
            }
        }, {
            key: "destroy",
            value: function () {
                this.off(), m && _(this), this.el = null
            }
        }]), C),
        p = /\s+/;

    function C(e, t) {
        var n, r, i;
        ! function (e) {
            if (!(e instanceof C)) throw new TypeError("Cannot call a class as a function")
        }(this), r = this, n = !(i = s(C).call(this)) || "object" !== a(i) && "function" != typeof i ? function (e) {
            if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return e
        }(r) : i;
        var o = !(t = t || {}).preventScrolling;
        return n.directSelect = !!t.directSelect, n.dragged = !1, n.enableDoubleTap = !1, n.el = e, n.handlers = {}, n.options = {}, n.lastClick = 0, n.lastStart = 0, n.passive = o, n.pointerId = null, n.startX = 0, n.startY = 0, n.event = null, n
    }

    function P(e) {
        return e && !p.test(e) && "object" !== a(e)
    }

    function x(c) {
        var s, l, f, e, d, p;
        c.handlers.init || (s = c.el, l = c.passive, f = !!O && {
            passive: l
        }, e = function (e) {
            var t, n, r, i, o, u, a;
            Object(v.o)(s, "jw-tab-focus"), ("which" in (a = e) ? 3 === a.which : "button" in a && 2 === a.button) || (t = e.target, n = e.type, c.directSelect && t !== s || (i = (r = I(e)).pageX, o = r.pageY, c.dragged = !1, c.lastStart = Object(h.a)(), c.startX = i, c.startY = o, A(c, w), "pointerdown" === n && e.isPrimary ? (l || (u = e.pointerId, c.pointerId = u, s.setPointerCapture(u)), T(c, w, "pointermove", d, f), T(c, w, "pointercancel", p), T(c, w, "pointerup", p), "BUTTON" === s.tagName && s.focus()) : "mousedown" === n ? (T(c, w, "mousemove", d, f), T(c, w, "mouseup", p)) : "touchstart" === n && (T(c, w, "touchmove", d, f), T(c, w, "touchcancel", p), T(c, w, "touchend", p), l || L(e))))
        }, d = function (e) {
            var t, n, r, i, o;
            c.dragged ? M(c, u.s, e) : (n = (t = I(e)).pageX, r = t.pageY, 36 < (i = n - c.startX) * i + (o = r - c.startY) * o && (M(c, u.u, e), c.dragged = !0, M(c, u.s, e))), l || "touchmove" !== e.type || L(e)
        }, p = function (e) {
            if (clearTimeout(g), c.el)
                if (_(c), A(c, w), c.dragged) c.dragged = !1, M(c, u.t, e);
                else if (-1 === e.type.indexOf("cancel") && s.contains(e.target)) {
                if (500 < Object(h.a)() - c.lastStart) return;
                var t = "pointerup" === e.type || "pointercancel" === e.type,
                    n = "mouseup" === e.type || t && "mouse" === e.pointerType;
                i = e, o = n, (r = c).enableDoubleTap && (Object(h.a)() - r.lastClick < 300 ? (M(r, o ? u.q : u.r, i), r.lastClick = 0) : r.lastClick = Object(h.a)()), n ? M(c, u.n, e) : (M(c, u.sb, e), "touchend" !== e.type || O || L(e))
            }
            var r, i, o
        }, m ? T(c, "init", "pointerdown", e, f) : (y && T(c, "init", "mousedown", e, f), T(c, "init", "touchstart", e, f)), b = b || new k(document).on("interaction"), T(c, "init", "blur", function () {
            Object(v.o)(s, "jw-tab-focus")
        }), T(c, "init", "focus", function () {
            b.event && b.event.type === j && Object(v.a)(s, "jw-tab-focus")
        }))
    }
    var S = {
        drag: function (e) {
            x(e)
        },
        dragStart: function (e) {
            x(e)
        },
        dragEnd: function (e) {
            x(e)
        },
        click: function (e) {
            x(e)
        },
        tap: function (e) {
            x(e)
        },
        doubleTap: function (e) {
            e.enableDoubleTap = !0, x(e)
        },
        doubleClick: function (e) {
            e.enableDoubleTap = !0, x(e)
        },
        longPress: function (t) {
            var n;
            r.OS.iOS ? (n = function () {
                clearTimeout(g)
            }, T(t, "longPress", "touchstart", function (e) {
                n(), g = setTimeout(function () {
                    M(t, "longPress", e)
                }, 500)
            }), T(t, "longPress", "touchmove", n), T(t, "longPress", "touchcancel", n), T(t, "longPress", "touchend", n)) : t.el.oncontextmenu = function (e) {
                return M(t, "longPress", e), !1
            }
        },
        focus: function (t) {
            T(t, "focus", "focus", function (e) {
                F(t, "focus", e)
            })
        },
        blur: function (t) {
            T(t, "blur", "blur", function (e) {
                F(t, "blur", e)
            })
        },
        over: function (t) {
            (m || y) && T(t, u.Z, m ? "pointerover" : "mouseover", function (e) {
                "touch" !== e.pointerType && M(t, u.Z, e)
            })
        },
        out: function (n) {
            var r;
            m ? (r = n.el, T(n, u.Y, "pointerout", function (e) {
                var t;
                "touch" !== e.pointerType && "x" in e && (t = document.elementFromPoint(e.x, e.y), r.contains(t) || M(n, u.Y, e))
            })) : y && T(n, u.Y, "mouseout", function (e) {
                M(n, u.Y, e)
            })
        },
        move: function (t) {
            (m || y) && T(t, u.W, m ? "pointermove" : "mousemove", function (e) {
                "touch" !== e.pointerType && M(t, u.W, e)
            })
        },
        enter: function (t) {
            T(t, u.v, j, function (e) {
                "Enter" !== e.key && 13 !== e.keyCode || (e.stopPropagation(), F(t, u.v, e))
            })
        },
        keydown: function (t) {
            T(t, j, j, function (e) {
                F(t, j, e)
            }, !1)
        },
        gesture: function (t) {
            function e(e) {
                return M(t, "gesture", e)
            }
            T(t, "gesture", "click", e), T(t, "gesture", j, e)
        },
        interaction: function (t) {
            function e(e) {
                t.event = e
            }
            T(t, "interaction", "mousedown", e, !0), T(t, "interaction", j, e, !0)
        }
    };

    function E(e) {
        var t = e.ownerDocument || e;
        return t.defaultView || t.parentWindow || window
    }

    function T(e, t, n, r, i) {
        var o = 4 < arguments.length && void 0 !== i ? i : d,
            u = e.handlers[t],
            a = e.options[t];
        if (u || (u = e.handlers[t] = {}, a = e.options[t] = {}), u[n]) throw new Error("".concat(t, " ").concat(n, " already registered"));
        u[n] = r, a[n] = o;
        var c = e.el;
        (t === w ? E(c) : c).addEventListener(n, r, o)
    }

    function A(e, t) {
        var n = e.el,
            r = e.handlers,
            i = e.options,
            o = t === w ? E(n) : n,
            u = r[t],
            a = i[t];
        u && (Object.keys(u).forEach(function (e) {
            var t = a[e];
            "boolean" == typeof t ? o.removeEventListener(e, u[e], t) : o.removeEventListener(e, u[e])
        }), r[t] = null, i[t] = null)
    }

    function _(e) {
        var t = e.el;
        null !== e.pointerId && (t.releasePointerCapture(e.pointerId), e.pointerId = null)
    }

    function F(e, t, n) {
        var r = e.el,
            i = n.target;
        e.trigger(t, {
            type: t,
            sourceEvent: n,
            currentTarget: r,
            target: i
        })
    }

    function M(e, t, n) {
        var r, i, o, u, a, c, s = (r = e.el, o = n.target, u = n.touches, a = n.changedTouches, c = n.pointerType, {
            type: t,
            pointerType: c = u || a ? (i = u && u.length ? u[0] : a[0], c || "touch") : (i = n, c || "mouse"),
            pageX: i.pageX,
            pageY: i.pageY,
            sourceEvent: n,
            currentTarget: r,
            target: o
        });
        e.trigger(t, s)
    }

    function I(e) {
        return 0 === e.type.indexOf("touch") ? (e.originalEvent || e).changedTouches[0] : e
    }

    function L(e) {
        e.preventDefault && e.preventDefault()
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "b", function () {
        return c
    }), n.d(t, "d", function () {
        return s
    }), n.d(t, "c", function () {
        return l
    }), n.d(t, "a", function () {
        return f
    });
    var r, i = n(23),
        o = [{
            configName: "clearkey",
            keyName: "org.w3.clearkey"
        }, {
            configName: "widevine",
            keyName: "com.widevine.alpha"
        }, {
            configName: "playready",
            keyName: "com.microsoft.playready"
        }],
        u = [],
        a = {};

    function c(e) {
        return e.some(function (e) {
            return !!e.drm || e.sources.some(function (e) {
                return !!e.drm
            })
        })
    }

    function s(e) {
        return r || ((navigator.requestMediaKeySystemAccess && MediaKeySystemAccess.prototype.getConfiguration || window.MSMediaKeys) && Object(i.a)(e)("drm") ? (o.forEach(function (e) {
            var r, t, n = (r = e.keyName, t = [{
                initDataTypes: ["cenc"],
                videoCapabilities: [{
                    contentType: 'video/mp4;codecs="avc1.4d401e"'
                }],
                audioCapabilities: [{
                    contentType: 'audio/mp4;codecs="mp4a.40.2"'
                }]
            }], (navigator.requestMediaKeySystemAccess ? navigator.requestMediaKeySystemAccess(r, t) : new Promise(function (e, t) {
                var n;
                try {
                    n = new window.MSMediaKeys(r)
                } catch (e) {
                    return void t(e)
                }
                e(n)
            })).then(function () {
                a[e.configName] = !0
            }).catch(function () {
                a[e.configName] = !1
            }));
            u.push(n)
        }), r = Promise.all(u)) : Promise.resolve())
    }

    function l(e) {
        return a[e]
    }

    function f(e) {
        if (r) return Object.keys(e).some(l)
    }
}, , function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return o
    }), n.d(t, "b", function () {
        return u
    });
    var r = n(10),
        i = null,
        o = {};

    function u() {
        return i = i || n.e(1).then(function (e) {
            var t = n(21).default;
            return o.controls = t
        }.bind(null, n)).catch(function () {
            i = null, Object(r.c)(301130)()
        })
    }
}, function (e, t, n) {
    "use strict";
    var r = document.createElement("video");
    t.a = r
}, function (e, t, n) {
    "use strict";
    t.a = {
        advertising: {
            admessage: "This ad will end in xx",
            cuetext: "Advertisement",
            displayHeading: "Advertisement",
            loadingAd: "Loading ad",
            podmessage: "Ad __AD_POD_CURRENT__ of __AD_POD_LENGTH__.",
            skipmessage: "Skip ad in xx",
            skiptext: "Skip"
        },
        airplay: "AirPlay",
        audioTracks: "Audio Tracks",
        auto: "Auto",
        buffer: "Loading",
        cast: "Chromecast",
        cc: "Closed Captions",
        close: "Close",
        errors: {
            badConnection: "This video cannot be played because of a problem with your internet connection.",
            cantLoadPlayer: "Sorry, the video player failed to load.",
            cantPlayInBrowser: "The video cannot be played in this browser.",
            cantPlayVideo: "This video file cannot be played.",
            errorCode: "Error Code",
            liveStreamDown: "The live stream is either down or has ended.",
            protectedContent: "There was a problem providing access to protected content.",
            technicalError: "This video cannot be played because of a technical error."
        },
        exitFullscreen: "Exit Fullscreen",
        fullscreen: "Fullscreen",
        hd: "Quality",
        liveBroadcast: "Live",
        logo: "Logo",
        mute: "Mute",
        next: "Next",
        nextUp: "Next Up",
        notLive: "Not Live",
        off: "Off",
        pause: "Pause",
        play: "Play",
        playback: "Play",
        playbackRates: "Playback Rates",
        player: "Video Player",
        poweredBy: "Powered by",
        prev: "Previous",
        related: {
            autoplaymessage: "Next up in xx",
            heading: "More Videos"
        },
        replay: "Replay",
        rewind: "Rewind 10 Seconds",
        settings: "Settings",
        sharing: {
            copied: "Copied",
            email: "Email",
            embed: "Embed",
            heading: "Share",
            link: "Link"
        },
        slider: "Seek",
        stop: "Stop",
        unmute: "Unmute",
        videoInfo: "About This Video",
        volume: "Volume",
        volumeSlider: "Volume",
        shortcuts: {
            playPause: "Play/Pause",
            volumeToggle: "Mute/Unmute",
            fullscreenToggle: "Fullscreen/Exit Fullscreen",
            seekPercent: "Seek %",
            keyboardShortcuts: "Keyboard Shortcuts",
            increaseVolume: "Increase Volume",
            decreaseVolume: "Decrease Volume",
            seekForward: "Seek Forward",
            seekBackward: "Seek Backward",
            spacebar: "SPACE",
            captionsToggle: "Captions On/Off"
        }
    }
}, function (e, t) {
    var n, r, s = {},
        l = {},
        f = (n = function () {
            return document.head || document.getElementsByTagName("head")[0]
        }, function () {
            return void 0 === r && (r = n.apply(this, arguments)), r
        });

    function a(e, t) {
        var n, r, i, o, u = l[e],
            a = (u = u || (l[e] = {
                element: (r = e, (o = document.createElement("style")).type = "text/css", o.setAttribute("data-jwplayer-id", r), i = o, f().appendChild(i), o),
                counter: 0
            })).counter++,
            c = u.element,
            s = function () {
                d(c, a, "")
            };
        return (n = function (e) {
                d(c, a, e)
            })(t.css),
            function (e) {
                if (e) {
                    if (e.css === t.css && e.media === t.media) return;
                    n((t = e).css)
                } else s()
            }
    }
    e.exports = {
        style: function (e, t) {
            ! function (e, t) {
                for (var n = 0; n < t.length; n++) {
                    var r = t[n],
                        i = (s[e] || {})[r.id];
                    if (i) {
                        for (var o = 0; o < i.parts.length; o++) i.parts[o](r.parts[o]);
                        for (; o < r.parts.length; o++) i.parts.push(a(e, r.parts[o]))
                    } else {
                        for (var u = [], o = 0; o < r.parts.length; o++) u.push(a(e, r.parts[o]));
                        s[e] = s[e] || {}, s[e][r.id] = {
                            id: r.id,
                            parts: u
                        }
                    }
                }
            }(t, function (e) {
                for (var t = [], n = {}, r = 0; r < e.length; r++) {
                    var i = e[r],
                        o = i[0],
                        u = {
                            css: i[1],
                            media: i[2]
                        };
                    n[o] ? n[o].parts.push(u) : t.push(n[o] = {
                        id: o,
                        parts: [u]
                    })
                }
                return t
            }(e))
        },
        clear: function (e, t) {
            var n = s[e];
            if (n)
                if (t) {
                    var r = n[t];
                    if (r)
                        for (var i = 0; i < r.parts.length; i += 1) r.parts[i]()
                } else {
                    for (var o = Object.keys(n), u = 0; u < o.length; u += 1)
                        for (var a = n[o[u]], c = 0; c < a.parts.length; c += 1) a.parts[c]();
                    delete s[e]
                }
        }
    };
    var i, o = (i = [], function (e, t) {
        return i[e] = t, i.filter(Boolean).join("\n")
    });

    function d(e, t, n) {
        var r, i;
        e.styleSheet ? e.styleSheet.cssText = o(t, n) : (r = document.createTextNode(n), (i = e.childNodes[t]) ? e.replaceChild(r, i) : e.appendChild(r))
    }
}, function (e, t, n) {
    "use strict";
    var r = n(0),
        i = n(17),
        o = n(13),
        u = n(33),
        a = n(10);

    function c(e) {
        this.config = e || {}
    }
    var s = {
        html5: function () {
            return n.e(15).then(function (e) {
                var t = n(51).default;
                return Object(o.a)(t), t
            }.bind(null, n)).catch(Object(a.b)(152))
        }
    };
    Object(r.k)(c.prototype, {
        load: function (e) {
            function t() {
                return Promise.reject(new Error("Failed to load media"))
            }
            var n = s[e];
            return n ? n().then(function () {
                return u.a[e] || t()
            }) : t()
        },
        providerSupports: function (e, t) {
            return e.supports(t)
        },
        choose: function (e) {
            if (e === Object(e))
                for (var t = i.a.length, n = 0; n < t; n++) {
                    var r = i.a[n];
                    if (this.providerSupports(r, e)) return {
                        priority: t - n - 1,
                        name: r.name,
                        type: e.type,
                        providerToCheck: r,
                        provider: u.a[r.name]
                    }
                }
            return {}
        }
    });
    var l, f = c;
    Object(r.k)(s, {
        shaka: function () {
            return n.e(16).then(function (e) {
                var t = n(161).default;
                return Object(o.a)(t), t
            }.bind(null, n)).catch(Object(a.b)(154))
        },
        hlsjs: function () {
            return n.e(13).then(function (e) {
                var t = n(156).default;
                return t.setEdition && t.setEdition(l), Object(o.a)(t), t
            }.bind(null, n)).catch(Object(a.b)(153))
        },
        flash: function () {
            return n.e(12).then(function (e) {
                var t = n(163).default;
                return Object(o.a)(t), t
            }.bind(null, n)).catch(Object(a.b)(151))
        },
        hlsjsProgressive: function () {
            return n.e(14).then(function (e) {
                var t = n(157).default;
                return t.setEdition(l), Object(o.a)(t), t
            }.bind(null, n)).catch(Object(a.b)(155))
        }
    }), f.prototype.providerSupports = function (e, t) {
        return l = this.config.edition, e.supports(t, l)
    }, t.a = f
}, function (e, t, n) {
    "use strict";
    var p = n(6),
        h = n(11);

    function r(e, t) {
        var n, r, i, o, u, a = t.message,
            c = t.code,
            s = (n = e.get("id"), r = a, i = e.get("localization").errors.errorCode, u = (o = c) ? "(".concat(i, ": ").concat(o, ")").replace(/\s+/g, "&nbsp;") : "", '<div id="'.concat(n, '" class="jw-error jw-reset">') + '<div class="jw-error-msg jw-info-overlay jw-reset"><style>' + '[id="'.concat(n, '"].jw-error{background:#000;overflow:hidden;position:relative}') + '[id="'.concat(n, '"] .jw-error-msg{top:50%;left:50%;position:absolute;transform:translate(-50%,-50%)}') + '[id="'.concat(n, '"] .jw-error-text{text-align:start;color:#FFF;font:14px/1.35 Arial,Helvetica,sans-serif}') + '</style><div class="jw-icon jw-reset"></div><div class="jw-info-container jw-reset">' + '<div class="jw-error-text jw-reset-text" dir="auto">'.concat(r || "", '<span class="jw-break jw-reset"></span>').concat(u, "</div>") + "</div></div></div>"),
            l = e.get("width"),
            f = e.get("height"),
            d = Object(p.e)(s);
        return Object(h.d)(d, {
            width: 0 < l.toString().indexOf("%") ? l : "".concat(l, "px"),
            height: 0 < f.toString().indexOf("%") ? f : "".concat(f, "px")
        }), d
    }
    n.d(t, "a", function () {
        return r
    })
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return e.slice && "px" === e.slice(-2) && (e = e.slice(0, -2)), e
    }

    function i(e, t) {
        if (-1 === t.toString().indexOf("%")) return 0;
        if ("string" != typeof e || !e) return 0;
        if (/^\d*\.?\d+%$/.test(e)) return e;
        var n = e.indexOf(":");
        if (-1 === n) return 0;
        var r = parseFloat(e.substr(0, n)),
            i = parseFloat(e.substr(n + 1));
        return r <= 0 || i <= 0 ? 0 : i / r * 100 + "%"
    }
    n.d(t, "b", function () {
        return r
    }), n.d(t, "a", function () {
        return i
    })
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return r
    });
    var r = window.atob
}, function (e, t, n) {
    "use strict";
    var d = n(4),
        p = n(2);
    var a = function e(t, n) {
            for (var r, i, o = [], u = 0; u < Object(d.c)(t); u++) {
                var a, c, s = t.childNodes[u];
                if ("media" === s.prefix) {
                    if (!Object(d.b)(s)) continue;
                    switch (Object(d.b)(s).toLowerCase()) {
                    case "content":
                        Object(p.j)(s, "duration") && (n.duration = Object(p.g)(Object(p.j)(s, "duration"))), Object(p.j)(s, "url") && (n.sources || (n.sources = []), a = {
                            file: Object(p.j)(s, "url"),
                            type: Object(p.j)(s, "type"),
                            width: Object(p.j)(s, "width"),
                            label: Object(p.j)(s, "label")
                        }, (c = function (e) {
                            for (var t = [], n = 0; n < Object(d.c)(e); n++) {
                                var r = e.childNodes[n];
                                "jwplayer" === r.prefix && "mediatypes" === Object(d.b)(r).toLowerCase() && t.push(Object(d.d)(r))
                            }
                            return t
                        }(s)).length && (a.mediaTypes = c), n.sources.push(a)), 0 < Object(d.c)(s) && (n = e(s, n));
                        break;
                    case "title":
                        n.title = Object(d.d)(s);
                        break;
                    case "description":
                        n.description = Object(d.d)(s);
                        break;
                    case "guid":
                        n.mediaid = Object(d.d)(s);
                        break;
                    case "thumbnail":
                        n.image || (n.image = Object(p.j)(s, "url"));
                        break;
                    case "group":
                        e(s, n);
                        break;
                    case "subtitle":
                        var l = {};
                        l.file = Object(p.j)(s, "url"), l.kind = "captions", 0 < Object(p.j)(s, "lang").length && (l.label = (i = {
                            zh: "Chinese",
                            nl: "Dutch",
                            en: "English",
                            fr: "French",
                            de: "German",
                            it: "Italian",
                            ja: "Japanese",
                            pt: "Portuguese",
                            ru: "Russian",
                            es: "Spanish"
                        })[r = Object(p.j)(s, "lang")] ? i[r] : r), o.push(l)
                    }
                }
            }
            n.hasOwnProperty("tracks") || (n.tracks = []);
            for (var f = 0; f < o.length; f++) n.tracks.push(o[f]);
            return n
        },
        l = n(12),
        c = function (e, t) {
            for (var n = "default", r = [], i = [], o = 0; o < e.childNodes.length; o++) {
                var u, a = e.childNodes[o];
                "jwplayer" === a.prefix && ("source" === (u = Object(d.b)(a)) ? (delete t.sources, r.push({
                    file: Object(p.j)(a, "file"),
                    default: Object(p.j)(a, n),
                    label: Object(p.j)(a, "label"),
                    type: Object(p.j)(a, "type")
                })) : "track" === u ? (delete t.tracks, i.push({
                    file: Object(p.j)(a, "file"),
                    default: Object(p.j)(a, n),
                    kind: Object(p.j)(a, "kind"),
                    label: Object(p.j)(a, "label")
                })) : (t[u] = Object(l.serialize)(Object(d.d)(a)), "file" === u && t.sources && delete t.sources)), t.file || (t.file = t.link)
            }
            if (r.length) {
                t.sources = [];
                for (var c = 0; c < r.length; c++) 0 < r[c].file.length && (r[c][n] = "true" === r[c][n], r[c].label.length || delete r[c].label, t.sources.push(r[c]))
            }
            if (i.length) {
                t.tracks = [];
                for (var s = 0; s < i.length; s++) 0 < i[s].file.length && (i[s][n] = "true" === i[s][n], i[s].kind = i[s].kind.length ? i[s].kind : "captions", i[s].label.length || delete i[s].label, t.tracks.push(i[s]))
            }
            return t
        },
        s = n(29);

    function r(e) {
        var t = [];
        t.feedData = {};
        for (var n = 0; n < Object(d.c)(e); n++) {
            var r = Object(d.a)(e, n);
            if ("channel" === Object(d.b)(r).toLowerCase())
                for (var i = 0; i < Object(d.c)(r); i++) {
                    var o = Object(d.a)(r, i),
                        u = Object(d.b)(o).toLowerCase();
                    "item" === u ? t.push(function (e) {
                        for (var t = {}, n = 0; n < e.childNodes.length; n++) {
                            var r = e.childNodes[n],
                                i = Object(d.b)(r);
                            if (i) switch (i.toLowerCase()) {
                            case "enclosure":
                                t.file = Object(p.j)(r, "url");
                                break;
                            case "title":
                                t.title = Object(d.d)(r);
                                break;
                            case "guid":
                                t.mediaid = Object(d.d)(r);
                                break;
                            case "pubdate":
                                t.date = Object(d.d)(r);
                                break;
                            case "description":
                                t.description = Object(d.d)(r);
                                break;
                            case "link":
                                t.link = Object(d.d)(r);
                                break;
                            case "category":
                                t.tags ? t.tags += Object(d.d)(r) : t.tags = Object(d.d)(r)
                            }
                        }
                        return new s.a(c(e, a(e, t)))
                    }(o)) : u && (t.feedData[u] = Object(d.d)(o))
                }
        }
        return t
    }
    n.d(t, "a", function () {
        return r
    })
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return r
    });
    var r = "function" == typeof console.log ? console.log.bind(console) : function () {}
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return r
    });
    var f = n(45);

    function d(e) {
        for (var t = new Array(Math.ceil(e.length / 4)), n = 0; n < t.length; n++) t[n] = e.charCodeAt(4 * n) + (e.charCodeAt(4 * n + 1) << 8) + (e.charCodeAt(4 * n + 2) << 16) + (e.charCodeAt(4 * n + 3) << 24);
        return t
    }

    function r(e, t) {
        if (e = String(e), t = String(t), 0 === e.length) return "";
        for (var n, r, i = d(Object(f.a)(e)), o = d(unescape(encodeURIComponent(t)).slice(0, 16)), u = i.length, a = i[u - 1], c = i[0], s = 2654435769 * Math.floor(6 + 52 / u); s;) {
            r = s >>> 2 & 3;
            for (var l = u - 1; 0 <= l; l--) n = ((a = i[0 < l ? l - 1 : u - 1]) >>> 5 ^ c << 2) + (c >>> 3 ^ a << 4) ^ (s ^ c) + (o[3 & l ^ r] ^ a), c = i[l] -= n;
            s -= 2654435769
        }
        return function (t) {
            try {
                return decodeURIComponent(escape(t))
            } catch (e) {
                return t
            }
        }(function (e) {
            for (var t = new Array(e.length), n = 0; n < e.length; n++) t[n] = String.fromCharCode(255 & e[n], e[n] >>> 8 & 255, e[n] >>> 16 & 255, e[n] >>> 24 & 255);
            return t.join("")
        }(i).replace(/\0+$/, ""))
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "b", function () {
        return r
    }), n.d(t, "a", function () {
        return i
    });
    var r = window.requestAnimationFrame || function (e) {
            return setTimeout(e, 17)
        },
        i = window.cancelAnimationFrame || clearTimeout
}, function (e, t, n) {
    "use strict";
    n.d(t, "b", function () {
        return r
    }), n.d(t, "a", function () {
        return i
    });
    var r = {
            audioMode: !1,
            flashBlocked: !1,
            item: 0,
            itemMeta: {},
            playbackRate: 1,
            playRejected: !1,
            state: n(3).nb,
            itemReady: !1,
            controlsEnabled: !1
        },
        i = {
            position: 0,
            duration: 0,
            buffer: 0,
            currentTime: 0
        }
}, , function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return r
    });
    var r = function (e, t, n) {
        return Math.max(Math.min(e, n), t)
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return r
    }), n.d(t, "b", function () {
        return i
    });
    var r = 12;

    function i(e) {
        for (var t = ""; t.length < e;) t += function () {
            try {
                var e = window.crypto || window.msCrypto;
                if (e && e.getRandomValues) return e.getRandomValues(new Uint32Array(1))[0].toString(36)
            } catch (e) {}
            return Math.random().toString(36).slice(2, 9)
        }();
        return t.slice(0, e)
    }
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return (r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        })(e)
    }

    function i(e, t) {
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
        }
    }

    function o(e) {
        return (o = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e)
        })(e)
    }

    function u(e, t) {
        return (u = Object.setPrototypeOf || function (e, t) {
            return e.__proto__ = t, e
        })(e, t)
    }
    n.d(t, "a", function () {
        return a
    });
    var a = (function (e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
        e.prototype = Object.create(t && t.prototype, {
            constructor: {
                value: e,
                writable: !0,
                configurable: !0
            }
        }), t && u(e, t)
    }(c, n(9).a), i(c.prototype, [{
        key: "addAttributes",
        value: function (t) {
            var n = this;
            Object.keys(t).forEach(function (e) {
                n.add(e, t[e])
            })
        }
    }, {
        key: "add",
        value: function (t, e) {
            var n = this;
            Object.defineProperty(this, t, {
                get: function () {
                    return n.attributes[t]
                },
                set: function (e) {
                    return n.set(t, e)
                },
                enumerable: !1
            }), this.attributes[t] = e
        }
    }, {
        key: "get",
        value: function (e) {
            return this.attributes[e]
        }
    }, {
        key: "set",
        value: function (e, t) {
            var n;
            this.attributes[e] !== t && (n = this.attributes[e], this.attributes[e] = t, this.trigger("change:" + e, this, t, n))
        }
    }, {
        key: "clone",
        value: function () {
            var e = {},
                t = this.attributes;
            if (t)
                for (var n in t) e[n] = t[n];
            return e
        }
    }, {
        key: "change",
        value: function (e, t, n) {
            this.on("change:" + e, t, n);
            var r = this.get(e);
            return t.call(n, this, r, r), this
        }
    }]), c);

    function c() {
        var e, t, n;
        return function (e) {
            if (!(e instanceof c)) throw new TypeError("Cannot call a class as a function")
        }(this), t = this, (e = !(n = o(c).call(this)) || "object" !== r(n) && "function" != typeof n ? function (e) {
            if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return e
        }(t) : n).attributes = Object.create(null), e
    }
}, function (e, t, n) {
    "use strict";

    function r(r, e, i) {
        var o = [],
            u = {};

        function a() {
            for (; 0 < o.length;) {
                var e = o.shift(),
                    t = e.command,
                    n = e.args;
                (u[t] || r[t]).apply(r, n)
            }
        }
        e.forEach(function (t) {
            var n = r[t];
            u[t] = n, r[t] = function () {
                var e = Array.prototype.slice.call(arguments, 0);
                i() ? o.push({
                    command: t,
                    args: e
                }) : (a(), n && n.apply(this, e))
            }
        }), Object.defineProperty(this, "queue", {
            enumerable: !0,
            get: function () {
                return o
            }
        }), this.flush = a, this.empty = function () {
            o.length = 0
        }, this.off = function () {
            e.forEach(function (e) {
                var t = u[e];
                t && (r[e] = t, delete u[e])
            })
        }, this.destroy = function () {
            this.off(), this.empty()
        }
    }
    n.d(t, "a", function () {
        return r
    })
}, function (e, t, n) {
    "use strict";
    n.d(t, "c", function () {
        return r
    }), n.d(t, "b", function () {
        return i
    }), n.d(t, "a", function () {
        return o
    });
    var r = 4,
        i = 2,
        o = 1
}, function (e, t, n) {
    "use strict";

    function r() {}

    function i() {
        return !1
    }
    var o = n(3),
        u = {
            name: "default"
        },
        a = {
            supports: i,
            play: r,
            pause: r,
            preload: r,
            load: r,
            stop: r,
            volume: r,
            mute: r,
            seek: r,
            resize: r,
            remove: r,
            destroy: r,
            setVisibility: r,
            setFullscreen: r,
            getFullscreen: i,
            supportsFullscreen: i,
            getContainer: r,
            setContainer: r,
            getName: function () {
                return u
            },
            getQualityLevels: r,
            getCurrentQuality: r,
            setCurrentQuality: r,
            getAudioTracks: r,
            getCurrentAudioTrack: r,
            setCurrentAudioTrack: r,
            getSeekRange: function () {
                return {
                    start: 0,
                    end: this.getDuration()
                }
            },
            setPlaybackRate: r,
            getPlaybackRate: function () {
                return 1
            },
            getBandwidthEstimate: function () {
                return null
            },
            getLiveLatency: function () {
                return null
            },
            setControls: r,
            attachMedia: r,
            detachMedia: r,
            init: r,
            setState: function (e) {
                this.state = e, this.trigger(o.bb, {
                    newstate: e
                })
            },
            sendMediaType: function (e) {
                var t = e[0],
                    n = t.type,
                    r = t.mimeType,
                    i = "aac" === n || "mp3" === n || "mpeg" === n || r && 0 === r.indexOf("audio/");
                this.trigger(o.T, {
                    mediaType: i ? "audio" : "video"
                })
            }
        };
    t.a = a
}, function (e, t, n) {
    "use strict";

    function r(e) {
        if ("string" == typeof e) {
            var t = (e = e.split("?")[0]).indexOf("://");
            if (0 < t) return 0;
            var n = e.indexOf("/"),
                r = Object(c.a)(e);
            return !(t < 0 && n < 0) || r && isNaN(r) ? 1 : 2
        }
    }

    function i(e) {
        this.url = e, this.promise_ = null
    }
    var o = n(0),
        u = n(24),
        a = n(12),
        c = n(2),
        s = n(1),
        l = n(19);
    Object.defineProperties(i.prototype, {
        promise: {
            get: function () {
                return this.promise_ || this.load()
            },
            set: function () {}
        }
    }), Object(o.k)(i.prototype, {
        load: function () {
            var e, t = this,
                n = this.promise_;
            return n || (n = 2 === r(this.url) ? Promise.resolve(this) : (e = new u.a(function (e) {
                switch (r(e)) {
                case 0:
                    return e;
                case 1:
                    return Object(a.getAbsolutePath)(e, window.location.href)
                }
            }(this.url)), (this.loader = e).load().then(function () {
                return t
            })), this.promise_ = n), n
        },
        registerPlugin: function (e, t, n) {
            this.name = e, this.target = t, this.js = n
        },
        getNewInstance: function (t, e, n) {
            var r = this.js;
            if ("function" != typeof r) throw new s.s(null, Object(l.c)(this.url) + 100);
            var i = new r(t, e, n);
            return i.addToPlayer = function () {
                var e = t.getContainer().querySelector(".jw-overlays");
                e && (n.left = e.style.left, n.top = e.style.top, e.appendChild(n), i.displayArea = e)
            }, i.resizeHandler = function () {
                var e = i.displayArea;
                e && i.resize(e.clientWidth, e.clientHeight)
            }, i
        }
    }), t.a = i
}, function (e, t, n) {
    "use strict";
    var s = n(0),
        l = n(3),
        f = n(4),
        d = n(46),
        r = n(28),
        i = n(9),
        p = n(1);
    t.a = function () {
        var a = Object(s.k)(this, i.a);

        function t(e) {
            try {
                var t = e.responseXML ? e.responseXML.childNodes : null,
                    n = "";
                if (t) {
                    for (var r, i, o = 0; o < t.length && 8 === (n = t[o]).nodeType; o++);
                    "xml" === Object(f.b)(n) && (n = n.nextSibling), "rss" === Object(f.b)(n) && (r = Object(d.a)(n), i = Object(s.k)({
                        playlist: r
                    }, r.feedData))
                }
                if (!i) try {
                    var u = JSON.parse(e.responseText);
                    if (Array.isArray(u)) i = {
                        playlist: u
                    };
                    else {
                        if (!Array.isArray(u.playlist)) throw Error("Playlist is not an array");
                        i = u
                    }
                } catch (e) {
                    throw new p.s(p.o, 621, e)
                }
                a.trigger(l.eb, i)
            } catch (e) {
                c(e)
            }
        }

        function c(e) {
            e.code || (e = new p.s(p.o, 0)), a.trigger(l.w, e)
        }
        this.load = function (e) {
            Object(r.a)(e, t, function (e, t, n, r) {
                c(r)
            })
        }, this.destroy = function () {
            this.off()
        }
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "b", function () {
        return r
    }), n.d(t, "a", function () {
        return l
    });
    var c = n(56);

    function r() {
        for (var e = c.c, n = [], r = [], t = 0; t < e; t++) {
            var i = l();
            n.push(i), r.push(i), s(i)
        }
        var o = r.shift(),
            u = r.shift(),
            a = !1;
        return {
            primed: function () {
                return a
            },
            prime: function () {
                n.forEach(s), a = !0
            },
            played: function () {
                a = !0
            },
            getPrimedElement: function () {
                return r.length ? r.shift() : null
            },
            getAdElement: function () {
                return o
            },
            getTestElement: function () {
                return u
            },
            clean: function (e) {
                if (e.src) {
                    e.removeAttribute("src");
                    try {
                        e.load()
                    } catch (e) {}
                }
            },
            recycle: function (t) {
                t && !r.some(function (e) {
                    return e === t
                }) && (this.clean(t), r.push(t))
            },
            syncVolume: function (e) {
                var t = Math.min(Math.max(0, e / 100), 1);
                n.forEach(function (e) {
                    e.volume = t
                })
            },
            syncMute: function (t) {
                n.forEach(function (e) {
                    e.muted = t
                })
            }
        }
    }

    function s(e) {
        e.src || e.load()
    }

    function l(t) {
        var n = document.createElement("video");
        return n.className = "jw-video jw-reset", n.setAttribute("tabindex", "-1"), n.setAttribute("disableRemotePlayback", ""), n.setAttribute("webkit-playsinline", ""), n.setAttribute("playsinline", ""), t && Object.keys(t).forEach(function (e) {
            n.setAttribute(e, t[e])
        }), n
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return i
    });
    var r = n(0);

    function i(e, t) {
        return Object(r.k)({}, t, {
            prime: function () {
                e.src || e.load()
            },
            getPrimedElement: function () {
                return e
            },
            clean: function () {
                t.clean(e)
            },
            recycle: function () {
                t.clean(e)
            }
        })
    }
}, function (e, t, n) {
    "use strict";
    t.a = "hidden" in document ? function () {
        return !document.hidden
    } : "webkitHidden" in document ? function () {
        return !document.webkitHidden
    } : function () {
        return !0
    }
}, function (e, t, n) {
    "use strict";

    function r(e) {
        return (e = e || window.event) && /^(?:mouse|pointer|touch|gesture|click|key)/.test(e.type)
    }
    n.d(t, "a", function () {
        return r
    })
}, function (e, t, w) {
    "use strict";
    var j = w(0),
        n = w(55),
        O = w(44),
        k = w(20),
        C = w(12),
        P = w(5),
        x = w(40),
        S = w(14),
        E = {
            autoPause: {
                viewability: !1,
                pauseAds: !1
            },
            autostart: !1,
            bandwidthEstimate: null,
            bitrateSelection: null,
            castAvailable: !1,
            controls: !0,
            cues: [],
            defaultPlaybackRate: 1,
            displaydescription: !0,
            displaytitle: !0,
            displayPlaybackLabel: !1,
            height: 360,
            intl: {},
            language: "en",
            liveTimeout: null,
            localization: x.a,
            mute: !1,
            nextUpDisplay: !0,
            playbackRateControls: !1,
            playbackRates: [.5, 1, 1.25, 1.5, 2],
            renderCaptionsNatively: !1,
            repeat: !1,
            stretching: "uniform",
            volume: 90,
            width: 640
        },
        T = w(8),
        A = w(26),
        _ = w(23),
        F = "__CONTEXTUAL__";

    function M(e, t) {
        var n = e.querySelector(t);
        if (n) return n.getAttribute("content")
    }
    var r = w(66),
        I = w.n(r);

    function L(e) {
        return "string" == typeof e && /^\/\/(?:content\.jwplatform|cdn\.jwplayer)\.com\//.test(e)
    }

    function N(e) {
        return "https:" + e
    }

    function D(e) {
        var t = "file:" === window.location.protocol ? "https:" : "",
            n = {
                jwpsrv: "//ssl.p.jwpcdn.com/player/v/8.11.3/jwpsrv.js",
                dai: "//ssl.p.jwpcdn.com/player/plugins/dai/v/0.4.9/dai.js",
                vast: "//ssl.p.jwpcdn.com/player/plugins/vast/v/8.7.1/vast.js",
                googima: "//ssl.p.jwpcdn.com/player/plugins/googima/v/8.7.1/googima.js",
                freewheel: "//ssl.p.jwpcdn.com/player/plugins/freewheel/v/2.2.6/freewheel.js",
                gapro: "//ssl.p.jwpcdn.com/player/plugins/gapro/v/2.1.5/gapro.js"
            } [e];
        return n ? t + n : ""
    }

    function R(e, t, n) {
        t && delete(e[t.client || D(n)] = t).client
    }

    function l(e, t) {
        var n, r, i, o, u, a, c, s, l, f, d, p, h, v = function (e, t) {
                var n, r = Object(j.k)({}, (window.jwplayer || {}).defaults, t, e);
                n = r, Object.keys(n).forEach(function (e) {
                    "id" !== e && (n[e] = Object(C.serialize)(n[e]))
                });
                var i = r.forceLocalizationDefaults ? E.language : Object(S.e)(),
                    o = Object(S.j)(r.intl);
                r.localization = Object(S.a)(x.a, Object(S.c)(r, o, i));
                var u = Object(j.k)({}, E, r);
                "." === u.base && (u.base = Object(k.getScriptPath)("jwplayer.js")), u.base = (u.base || Object(k.loadFrom)()).replace(/\/?$/, "/"), w.p = u.base, u.width = Object(O.b)(u.width), u.height = Object(O.b)(u.height), u.aspectratio = Object(O.a)(u.aspectratio, u.width), u.volume = Object(j.A)(u.volume) ? Math.min(Math.max(0, u.volume), 100) : E.volume, u.mute = !!u.mute, u.language = i, u.intl = o;
                var a = r.autoPause;
                a && (u.autoPause.viewability = !("viewability" in a && !a.viewability));
                var c, s = u.playbackRateControls;
                s && (c = u.playbackRates, Array.isArray(s) && (c = s), (c = c.filter(function (e) {
                    return Object(j.w)(e) && .25 <= e && e <= 4
                }).map(function (e) {
                    return Math.round(100 * e) / 100
                })).indexOf(1) < 0 && c.push(1), c.sort(), u.playbackRateControls = !0, u.playbackRates = c), (!u.playbackRateControls || u.playbackRates.indexOf(u.defaultPlaybackRate) < 0) && (u.defaultPlaybackRate = 1), u.playbackRate = u.defaultPlaybackRate, u.aspectratio || delete u.aspectratio;
                var l, f = u.playlist;
                f ? Array.isArray(f.playlist) && (u.feedData = f, u.playlist = f.playlist) : (l = Object(j.E)(u, ["title", "description", "type", "mediaid", "image", "file", "sources", "tracks", "preload", "duration"]), u.playlist = [l]), u.qualityLabels = u.qualityLabels || u.hlslabels, delete u.duration;
                var d = u.liveTimeout;
                null !== d && (Object(j.A)(d) ? 0 !== d && (d = Math.max(30, d)) : d = null, u.liveTimeout = d);
                var p, h, v = parseFloat(u.bandwidthEstimate),
                    g = parseFloat(u.bitrateSelection);
                return u.bandwidthEstimate = Object(j.A)(v) ? v : (p = u.defaultBandwidthEstimate, h = parseFloat(p), Object(j.A)(h) ? Math.max(h, 1) : E.bandwidthEstimate), u.bitrateSelection = Object(j.A)(g) ? g : E.bitrateSelection, u.backgroundLoading = Object(j.s)(u.backgroundLoading) ? u.backgroundLoading : P.Features.backgroundLoading, u
            }(e, t),
            g = v.key || window.jwplayer && window.jwplayer.key,
            b = new A.b(g),
            m = b.edition();
        if ("free" === b.edition() && (v = Object(j.k)({
                skin: {
                    active: "#ff0046",
                    timeslider: {
                        progress: "none"
                    }
                },
                logo: {
                    position: "control-bar",
                    file: I.a
                }
            }, E, Object(j.E)(v, ["analytics", "aspectratio", "base", "file", "height", "playlist", "sources", "width"]))), v.key = g, v.edition = m, v.error = b.error(), v.generateSEOMetadata = v.generateSEOMetadata || !1, "unlimited" === m) {
            var y = Object(k.getScriptPath)("jwplayer.js");
            if (!y) throw new Error("Error setting up player: Could not locate jwplayer.js script tag");
            w.p = y
        }
        return v.flashplayer = (h = (h = (p = v).flashplayer) || (Object(k.getScriptPath)("jwplayer.js") || p.base) + "jwplayer.flash.swf", "http:" === window.location.protocol && (h = h.replace(/^https/, "http")), h), v.plugins = function (e) {
            var t, n, r, i = Object(j.k)({}, e.plugins),
                o = Object(_.a)(e.edition);
            !o("ads") || (n = (t = Object(j.k)({}, e.advertising)).client) && delete(i[D(n) || n] = t).client, o("jwpsrv") && ((r = e.analytics) !== Object(r) && (r = {}), R(i, r, "jwpsrv")), R(i, e.ga, "gapro");
            var u = e.related,
                a = !o("discovery") || u !== Object(u),
                c = !u || "none" !== u.displayMode,
                s = u || {},
                l = void 0 === s.oncomplete ? "none" : s.oncomplete,
                f = s.autoplaytimer;
            !1 === l || e.repeat ? l = "hide" : "none" === l && (f = 0);
            var d = "autoplay" === l && f <= 0 || "none" === l;
            return e.related = Object(j.k)({}, u, {
                disableRelated: a,
                showButton: c,
                oncomplete: l,
                autoplaytimer: f,
                shouldAutoAdvance: d
            }), i
        }(v), v.ab && (v.ab = ((d = (f = v).ab).clone && (d = d.clone()), Object.keys(d.tests).forEach(function (e) {
            d.tests[e].forEach(function (e) {
                e.addConfig && e.addConfig(f, e.selection)
            })
        }), d)), n = v.playlist, Object(j.y)(n) && -1 < n.indexOf(F) && (v.playlist = (o = document, u = v.playlist, a = (o.querySelector("title") || {}).textContent, c = M(o, 'meta[property="og:title"]'), s = encodeURIComponent(c || a || ""), (l = M(o, 'meta[property="og:description"]') || M(o, 'meta[name="description"]')) && (s += "&page_description=" + encodeURIComponent(l)), u.replace(F, s)), v.contextual = !0), Object(T.isFileProtocol)() && (r = v.playlist, i = v.related, L(r) && (v.playlist = N(r)), i && L(i.file) && (i.file = N(i.file))), v
    }
    var g = w(10),
        b = w(25),
        f = w(3),
        u = w(59),
        m = w(30),
        y = w(24),
        B = w(1);

    function q(i) {
        var o = i.get("playlist");
        return new Promise(function (n, t) {
            if ("string" != typeof o) {
                var e = i.get("feedData") || {};
                return a(i, o, e), n()
            }
            var r = new u.a;
            r.on(f.eb, function (e) {
                var t = e.playlist;
                delete e.playlist, a(i, t, e), n()
            }), r.on(f.w, function (e) {
                a(i, [], {}), t(Object(B.z)(e, B.u))
            }), r.load(o)
        })
    }

    function a(e, t, n) {
        var r = e.attributes;
        r.playlist = Object(m.a)(t), r.feedData = n
    }

    function V(e) {
        return e.attributes._destroyed
    }
    var z = w(36);

    function H(e) {
        var t = [function (e) {
            var n = e.attributes,
                t = n.error;
            if (t && t.code === A.a) {
                var r = n.pid,
                    i = n.ph,
                    o = new A.b(n.key);
                if (0 < i && i < 4 && r && -7776e6 < o.duration()) return new y.a("//content.jwplatform.com/libraries/".concat(r, ".js")).load().then(function () {
                    var e = window.jwplayer.defaults.key,
                        t = new A.b(e);
                    t.error() || t.token() !== o.token() || (n.key = e, n.edition = t.edition(), n.error = t.error())
                }).catch(function () {})
            }
            return Promise.resolve()
        }(e)];
        return Q(e) || t.push(Promise.resolve()), Promise.all(t)
    }

    function Q(e) {
        var t = e.get("advertising");
        return t && t.outstream
    }

    function i(h) {
        var v;
        this.start = function (e) {
            var r, i, t, o, n, u, a, c, s, l, f = Object(b.a)(h, e),
                d = Promise.all([Object(g.d)(h), f, Q(l = h) ? Promise.resolve() : q(l).then(function () {
                    if (l.get("drm") || Object(z.b)(l.get("playlist"))) return Object(z.d)(l.get("edition"))
                }).then(function () {
                    return q(o = l).then(function () {
                        if (!V(o)) {
                            var e = Object(m.b)(o.get("playlist"), o);
                            o.attributes.playlist = e;
                            try {
                                Object(m.e)(e)
                            } catch (e) {
                                throw e.code += B.u, e
                            }
                            var t = o.getProviders(),
                                n = t.choose(e[0].sources[0]),
                                r = n.provider,
                                i = n.name;
                            return "function" == typeof r ? r : g.a.html5 && "html5" === i ? g.a.html5 : t.load(i).catch(function (e) {
                                throw Object(B.z)(e, B.v)
                            })
                        }
                    });
                    var o
                }), H(h), "string" != typeof (s = (c = h).get("skin") ? c.get("skin").url : void 0) || function (e) {
                    for (var t = document.styleSheets, n = 0, r = t.length; n < r; n++)
                        if (t[n].href === e) return 1
                }(s) ? Promise.resolve() : new y.a(s, !0).load().catch(function (e) {
                    return e
                }), (i = (r = h).attributes, t = i.language, o = i.base, n = i.setupConfig, u = i.intl, a = Object(S.c)(n, u, t), !Object(S.h)(t) || Object(S.f)(a) ? Promise.resolve() : new Promise(function (n) {
                    return Object(S.i)(o, t).then(function (e) {
                        var t = e.response;
                        if (!V(r)) {
                            if (!t) throw new B.s(null, B.g);
                            i.localization = Object(S.a)(t, a), n()
                        }
                    }).catch(function (e) {
                        n(e.code === B.g ? e : Object(B.z)(e, B.f))
                    })
                }))]),
                p = new Promise(function (e, t) {
                    v = setTimeout(function () {
                        t(new B.s(B.m, B.x))
                    }, 6e4);

                    function n() {
                        clearTimeout(v), setTimeout(e, 6e4)
                    }
                    d.then(n).catch(n)
                });
            return Promise.race([d, p]).catch(function (e) {
                function t() {
                    throw e
                }
                return f.then(t).catch(t)
            }).then(function (e) {
                if (!e || !e.length) return {
                    core: null,
                    warnings: []
                };
                var t = e.reduce(function (e, t) {
                    return e.concat(t)
                }, []).filter(function (e) {
                    return e && e.code
                });
                return {
                    core: e[0],
                    warnings: t
                }
            })
        }, this.destroy = function () {
            clearTimeout(v), h.set("_destroyed", !0), h = null
        }
    }
    var d = w(42),
        o = w(32),
        c = w(16),
        s = {
            removeItem: function () {}
        };
    try {
        s = window.localStorage || s
    } catch (e) {}

    function p(e, t) {
        this.namespace = e, this.items = t
    }
    Object(j.k)(p.prototype, {
        getAllItems: function () {
            var r = this;
            return this.items.reduce(function (e, t) {
                var n = s["".concat(r.namespace, ".").concat(t)];
                return n && (e[t] = Object(C.serialize)(n)), e
            }, {})
        },
        track: function (e) {
            var r = this;
            this.items.forEach(function (n) {
                e.on("change:".concat(n), function (e, t) {
                    try {
                        s["".concat(r.namespace, ".").concat(n)] = t
                    } catch (e) {
                        c.a.debug && console.error(e)
                    }
                })
            })
        },
        clear: function () {
            var t = this;
            this.items.forEach(function (e) {
                s.removeItem("".concat(t.namespace, ".").concat(e))
            })
        }
    });
    var h = p,
        v = w(54),
        $ = w(50),
        W = w(9),
        X = w(43),
        U = w(60),
        J = w(61),
        K = w(35);
    w(67), w(68), w.d(t, "b", function () {
        return ee
    });

    function Y(e) {
        this._events = {}, this.modelShim = new v.a, this.modelShim._qoeItem = new o.a, this.mediaShim = {}, this.setup = new i(this.modelShim), this.currentContainer = this.originalContainer = e, this.apiQueue = new n.a(this, ["load", "play", "pause", "seek", "stop", "playlistItem", "playlistNext", "playlistPrev", "next", "preload", "setConfig", "setCurrentAudioTrack", "setCurrentCaptions", "setCurrentQuality", "setFullscreen", "addButton", "removeButton", "castToggle", "setMute", "setVolume", "setPlaybackRate", "addCues", "setCues", "setPlaylistItem", "resize", "setCaptions", "setControls"], function () {
            return !0
        })
    }

    function Z(e, t) {
        t && t.code && (t.sourceError && console.error(t.sourceError), console.error(B.s.logMessage(t.code)))
    }

    function G(e) {
        e && e.code && console.warn(B.s.logMessage(e.code))
    }

    function ee(e, t) {
        var n;
        document.body.contains(e.currentContainer) || (n = document.getElementById(e.get("id"))) && (e.currentContainer = n), e.currentContainer.parentElement && e.currentContainer.parentElement.replaceChild(t, e.currentContainer), e.currentContainer = t
    }
    Object(j.k)(Y.prototype, {
        on: W.a.on,
        once: W.a.once,
        off: W.a.off,
        trigger: W.a.trigger,
        init: function (e, a) {
            var c = this,
                o = this.modelShim,
                u = new h("jwplayer", ["volume", "mute", "captionLabel", "bandwidthEstimate", "bitrateSelection", "qualityLabel"]),
                t = u && u.getAllItems();
            o.attributes = o.attributes || {}, Object(j.k)(this.mediaShim, $.a);
            var n = e,
                r = l(Object(j.k)({}, e), t);
            r.id = a.id, r.setupConfig = n, Object(j.k)(o.attributes, r, $.b), o.getProviders = function () {
                return new d.a(r)
            }, o.setProvider = function () {};
            var s = Object(U.b)();
            o.get("backgroundLoading") || (s = Object(J.a)(s.getPrimedElement(), s));
            var i = new K.a(Object(K.b)(this.originalContainer)).once("gesture", function () {
                s.prime(), c.preload(), i.destroy()
            });
            return o.on("change:errorEvent", Z), this.setup.start(a).then(function (e) {
                var t = e.core;
                if (!t) throw Object(B.z)(null, B.w);
                if (c.setup) {
                    c.on(f.ub, G), e.warnings.forEach(function (e) {
                        c.trigger(f.ub, e)
                    });
                    var n = c.modelShim.clone();
                    if (n.error) throw n.error;
                    var r = c.apiQueue.queue.slice(0);
                    c.apiQueue.destroy(), Object(j.k)(c, t.prototype), c.setup(n, a, c.originalContainer, c._events, r, s);
                    var i = c._model;
                    return o.off("change:errorEvent", Z), i.on("change:errorEvent", Z), u.track(i), c.updatePlaylist(i.get("playlist"), i.get("feedData")).catch(function (e) {
                        throw Object(B.z)(e, B.u)
                    })
                }
            }).then(function () {
                c.setup && c.playerReady()
            }).catch(function (e) {
                var i, o, u;
                c.setup && (i = c, o = a, u = e, Promise.resolve().then(function () {
                    var e = Object(B.A)(B.r, B.y, u),
                        t = i._model || i.modelShim;
                    e.message = e.message || t.get("localization").errors[e.key], delete e.key;
                    var n, r = t.get("contextual");
                    r || (n = Object(X.a)(i, e), X.a.cloneIcon && n.querySelector(".jw-icon").appendChild(X.a.cloneIcon("error")), ee(i, n)), t.set("errorEvent", e), t.set("state", f.mb), i.trigger(f.jb, e), r && o.remove()
                }))
            })
        },
        playerDestroy: function () {
            this.apiQueue && this.apiQueue.destroy(), this.setup && this.setup.destroy(), this.currentContainer !== this.originalContainer && ee(this, this.originalContainer), this.off(), this._events = this._model = this.modelShim = this.apiQueue = this.setup = null
        },
        getContainer: function () {
            return this.currentContainer
        },
        get: function (e) {
            if (this.modelShim) return e in this.mediaShim ? this.mediaShim[e] : this.modelShim.get(e)
        },
        getItemQoe: function () {
            return this.modelShim._qoeItem
        },
        getConfig: function () {
            return Object(j.k)({}, this.modelShim.attributes, this.mediaShim)
        },
        getCurrentCaptions: function () {
            return this.get("captionsIndex")
        },
        getWidth: function () {
            return this.get("containerWidth")
        },
        getHeight: function () {
            return this.get("containerHeight")
        },
        getMute: function () {
            return this.get("mute")
        },
        getProvider: function () {
            return this.get("provider")
        },
        getState: function () {
            return this.get("state")
        },
        getAudioTracks: function () {
            return null
        },
        getCaptionsList: function () {
            return null
        },
        getQualityLevels: function () {
            return null
        },
        getVisualQuality: function () {
            return null
        },
        getCurrentQuality: function () {
            return -1
        },
        getCurrentAudioTrack: function () {
            return -1
        },
        getSafeRegion: function () {
            return {
                x: 0,
                y: 0,
                width: 0,
                height: 0
            }
        },
        isBeforeComplete: function () {
            return !1
        },
        isBeforePlay: function () {
            return !1
        },
        createInstream: function () {
            return null
        },
        skipAd: function () {},
        attachMedia: function () {},
        detachMedia: function () {}
    }), t.a = Y
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return i
    });
    var r = n(5);

    function i(e) {
        return "hls" === e.type && r.OS.android ? !1 !== e.androidhls && !r.Browser.firefox && 4.4 <= parseFloat(r.OS.version.version) : null
    }
}, function (e, t) {
    e.exports = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 150 29.3" class="jw-svg-icon jw-svg-icon-watermark" focusable="false"><path d="M37,16.68c0,2.4-.59,3.43-2.4,3.43a5.75,5.75,0,0,1-3.38-1.23v3.58a7.39,7.39,0,0,0,3.67,1c3.67,0,5.73-1.91,5.73-6.32V5.86H37Z"></path><polygon points="58.33 17.61 55.39 6.01 52.55 6.01 49.52 17.61 46.73 6.01 43.06 6.01 47.56 23.29 50.89 23.29 53.92 11.88 56.96 23.29 60.24 23.29 64.74 6.01 61.17 6.01 58.33 17.61"></polygon><path d="M73.84,6H67.47V23.29h2.2v-6.9h4.17c3.47,0,5.77-1.77,5.77-5.19S77.31,6,73.84,6Zm0,8.47H69.72V8h4.12c2.3,0,3.57,1.22,3.62,3.28C77.46,13.21,76.19,14.48,73.84,14.48Z"></path><path d="M99.2,6l-6,15.27H85V6H82.8V23.29H94.7l2-5.19h7.09l2,5.19H108L101.26,6ZM97.39,16.14l2.84-7.39L103,16.14Z"></path><polygon points="113.98 14.18 108.99 6.01 106.59 6.01 112.81 16.14 112.81 23.29 115.01 23.29 115.01 16.14 121.33 6.01 118.98 6.01 113.98 14.18"></polygon><polygon points="123.14 23.29 134.1 23.29 134.1 21.28 125.29 21.28 125.29 15.41 133.32 15.41 133.32 13.45 125.29 13.45 125.29 7.97 134.1 7.97 134.1 6.01 123.14 6.01 123.14 23.29"></polygon><path d="M144.86,15.85c2.74-.39,4.41-2,4.41-4.85,0-3.23-2.26-5-5.73-5h-6.32V23.29h2.22V16h3.08l4.94,7.29H150Zm-5.42-1.71V8h4.06c2.3,0,3.62,1.17,3.62,3.08s-1.32,3.09-3.62,3.09Z"></path><path d="M27.63.09a1,1,0,0,0-1.32.48c-.24.51-6.35,15.3-6.35,15.3-.2.46-.33.41-.33-.07,0,0,0-5.15,0-9.39,0-2.31-1.12-3.61-2.73-3.88A3.12,3.12,0,0,0,14.83,3a4.57,4.57,0,0,0-1.5,1.79c-.48.94-3.47,9.66-3.47,9.66-.16.46-.31.44-.31,0,0,0-.09-3.76-.18-4.64-.13-1.36-.44-3.59-2.2-3.7S4.77,8,4.36,9.24c-.29.84-1.65,5.35-1.65,5.35l-.2.46h0c-.06.24-.17.24-.24,0l-.11-.42Q2,14,1.74,13.31a1.71,1.71,0,0,0-.33-.66.83.83,0,0,0-.88-.22.82.82,0,0,0-.53.69,4.22,4.22,0,0,0,.07.79,29,29,0,0,0,1,4.6,1.31,1.31,0,0,0,1.8.66,3.43,3.43,0,0,0,1.24-1.81c.33-.81,2-5.48,2-5.48.18-.46.31-.44.29,0,0,0-.09,4.57-.09,6.64a13.11,13.11,0,0,0,.28,2.93,2.41,2.41,0,0,0,.82,1.27,2,2,0,0,0,1.41.4,2,2,0,0,0,.7-.24,3.15,3.15,0,0,0,.79-.71,12.52,12.52,0,0,0,1.26-2.11c.81-1.6,2.92-6.58,2.92-6.58.2-.46.33-.41.33.07,0,0-.26,8.36-.26,11.55a6.39,6.39,0,0,0,.44,2.33,2.8,2.8,0,0,0,1.45,1.61A2.57,2.57,0,0,0,18.79,29a3.76,3.76,0,0,0,1.28-1.32,15.12,15.12,0,0,0,1.07-2.31c.64-1.65,1.17-3.33,1.7-5s5-17.65,5.28-19a1.79,1.79,0,0,0,0-.46A1,1,0,0,0,27.63.09Z"></path></svg>'
}, function (e, t, n) {
    "use strict";
    var r, i = n(62),
        o = n(5),
        u = n(6),
        a = [],
        c = [],
        s = [],
        l = {},
        f = "screen" in window && "orientation" in window.screen,
        d = o.OS.android && o.Browser.chrome,
        p = !1;

    function h(e, t) {
        for (var n = t.length; n--;) {
            var r = t[n];
            if (e.target === r.getContainer()) {
                r.setIntersection(e);
                break
            }
        }
    }

    function v() {
        a.forEach(function (e) {
            var t, n, r = e.model;
            r.get("audioMode") || !r.get("controls") || r.get("visibility") < .75 || (t = r.get("state"), !(n = Object(u.f)()) && "paused" === t && e.api.getFullscreen() ? e.api.setFullscreen(!1) : "playing" === t && e.api.setFullscreen(n))
        })
    }

    function g() {
        a.forEach(function (e) {
            e.model.set("activeTab", Object(i.a)())
        })
    }

    function b(e, t) {
        var n = t.indexOf(e); - 1 !== n && t.splice(n, 1)
    }

    function m(t) {
        s.forEach(function (e) {
            e(t)
        })
    }
    document.addEventListener("visibilitychange", g), document.addEventListener("webkitvisibilitychange", g), d && f && window.screen.orientation.addEventListener("change", v), window.addEventListener("beforeunload", function () {
        document.removeEventListener("visibilitychange", g), document.removeEventListener("webkitvisibilitychange", g), window.removeEventListener("scroll", m), d && f && window.screen.orientation.removeEventListener("change", v)
    }), t.a = {
        add: function (e) {
            a.push(e)
        },
        remove: function (e) {
            b(e, a)
        },
        addScrollHandler: function (e) {
            p || (p = !0, window.addEventListener("scroll", m)), s.push(e)
        },
        removeScrollHandler: function (e) {
            var t = s.indexOf(e); - 1 !== t && s.splice(t, 1)
        },
        addWidget: function (e) {
            c.push(e)
        },
        removeWidget: function (e) {
            b(e, c)
        },
        size: function () {
            return a.length
        },
        observe: function (e) {
            var t = window.IntersectionObserver;
            r = r || new t(function (e) {
                if (e && e.length)
                    for (var t = e.length; t--;) {
                        var n = e[t];
                        h(n, a), h(n, c)
                    }
            }, {
                threshold: [0, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1]
            }), l[e.id] || (l[e.id] = !0, r.observe(e))
        },
        unobserve: function (e) {
            r && l[e.id] && (delete l[e.id], r.unobserve(e))
        }
    }
}, function (e, t, n) {
    "use strict";
    n.d(t, "a", function () {
        return u
    });
    var s = n(0),
        r = n(49),
        l = n(6),
        f = n(11);

    function i(e, t) {
        for (var n = 0; n < t.length; n++) {
            var r = t[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
        }
    }
    var d = [],
        o = -1;

    function p() {
        Object(r.a)(o), o = Object(r.b)(function () {
            d.forEach(function (e) {
                e.view.updateBounds();
                var t = e.view.model.get("containerWidth");
                e.resized = e.width !== t, e.width = t
            }), d.forEach(function (e) {
                e.contractElement.scrollLeft = 2 * e.width
            }), d.forEach(function (e) {
                Object(f.d)(e.expandChild, {
                    width: e.width + 1
                }), e.resized && e.view.model.get("visibility") && e.view.updateStyles()
            }), d.forEach(function (e) {
                e.expandElement.scrollLeft = e.width + 1
            }), d.forEach(function (e) {
                e.resized && e.view.checkResized()
            })
        })
    }
    var u = (i(h.prototype, [{
        key: "destroy",
        value: function () {
            var e;
            this.view && (-1 !== (e = d.indexOf(this)) && d.splice(e, 1), this.element.removeEventListener("scroll", p, !0), this.element.removeChild(this.hiddenElement), this.view = this.model = null)
        }
    }]), h);

    function h(e, t, n) {
        ! function (e) {
            if (!(e instanceof h)) throw new TypeError("Cannot call a class as a function")
        }(this);
        var r = {
                display: "block",
                position: "absolute",
                top: 0,
                left: 0
            },
            i = {
                width: "100%",
                height: "100%"
            },
            o = Object(l.e)('<div style="opacity:0;visibility:hidden;overflow:hidden;"><div><div style="height:1px;"></div></div><div class="jw-contract-trigger"></div></div>'),
            u = o.firstChild,
            a = u.firstChild,
            c = u.nextSibling;
        Object(f.d)([u, c], Object(s.k)({
            overflow: "auto"
        }, r, i)), Object(f.d)(o, Object(s.k)({}, r, i)), this.expandElement = u, this.expandChild = a, this.contractElement = c, this.hiddenElement = o, this.element = e, this.view = t, this.model = n, this.width = 0, this.resized = !1, e.firstChild ? e.insertBefore(o, e.firstChild) : e.appendChild(o), e.addEventListener("scroll", p, !0), d.push(this), p()
    }
}, function (e, t, n) {
    "use strict";
    n.r(t);
    var u = n(0),
        r = setTimeout;

    function i() {}

    function o(e) {
        if (!(this instanceof o)) throw new TypeError("Promises must be constructed via new");
        if ("function" != typeof e) throw new TypeError("not a function");
        this._state = 0, this._handled = !1, this._value = void 0, this._deferreds = [], d(e, this)
    }

    function a(n, r) {
        for (; 3 === n._state;) n = n._value;
        0 !== n._state ? (n._handled = !0, o._immediateFn(function () {
            var e, t = 1 === n._state ? r.onFulfilled : r.onRejected;
            if (null !== t) {
                try {
                    e = t(n._value)
                } catch (e) {
                    return void s(r.promise, e)
                }
                c(r.promise, e)
            } else(1 === n._state ? c : s)(r.promise, n._value)
        })) : n._deferreds.push(r)
    }

    function c(e, t) {
        try {
            if (t === e) throw new TypeError("A promise cannot be resolved with itself.");
            if (t && ("object" == typeof t || "function" == typeof t)) {
                var n = t.then;
                if (t instanceof o) return e._state = 3, e._value = t, void l(e);
                if ("function" == typeof n) return void d((r = n, i = t, function () {
                    r.apply(i, arguments)
                }), e)
            }
            e._state = 1, e._value = t, l(e)
        } catch (t) {
            s(e, t)
        }
        var r, i
    }

    function s(e, t) {
        e._state = 2, e._value = t, l(e)
    }

    function l(e) {
        2 === e._state && 0 === e._deferreds.length && o._immediateFn(function () {
            e._handled || o._unhandledRejectionFn(e._value)
        });
        for (var t = 0, n = e._deferreds.length; t < n; t++) a(e, e._deferreds[t]);
        e._deferreds = null
    }

    function f(e, t, n) {
        this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof t ? t : null, this.promise = n
    }

    function d(e, t) {
        var n = !1;
        try {
            e(function (e) {
                n || (n = !0, c(t, e))
            }, function (e) {
                n || (n = !0, s(t, e))
            })
        } catch (e) {
            if (n) return;
            n = !0, s(t, e)
        }
    }
    o.prototype.catch = function (e) {
        return this.then(null, e)
    }, o.prototype.then = function (e, t) {
        var n = new this.constructor(i);
        return a(this, new f(e, t, n)), n
    }, o.prototype.finally = function (t) {
        var n = this.constructor;
        return this.then(function (e) {
            return n.resolve(t()).then(function () {
                return e
            })
        }, function (e) {
            return n.resolve(t()).then(function () {
                return n.reject(e)
            })
        })
    }, o.all = function (t) {
        return new o(function (i, o) {
            if (!t || void 0 === t.length) throw new TypeError("Promise.all accepts an array");
            var u = Array.prototype.slice.call(t);
            if (0 === u.length) return i([]);
            var a = u.length;
            for (var e = 0; e < u.length; e++) ! function t(n, e) {
                try {
                    if (e && ("object" == typeof e || "function" == typeof e)) {
                        var r = e.then;
                        if ("function" == typeof r) return r.call(e, function (e) {
                            t(n, e)
                        }, o), 0
                    }
                    u[n] = e, 0 == --a && i(u)
                } catch (n) {
                    o(n)
                }
            }(e, u[e])
        })
    }, o.resolve = function (t) {
        return t && "object" == typeof t && t.constructor === o ? t : new o(function (e) {
            e(t)
        })
    }, o.reject = function (n) {
        return new o(function (e, t) {
            t(n)
        })
    }, o.race = function (i) {
        return new o(function (e, t) {
            for (var n = 0, r = i.length; n < r; n++) i[n].then(e, t)
        })
    }, o._immediateFn = "function" == typeof setImmediate ? function (e) {
        setImmediate(e)
    } : function (e) {
        r(e, 0)
    }, o._unhandledRejectionFn = function (e) {
        "undefined" != typeof console && console && console.warn("Possible Unhandled Promise Rejection:", e)
    }, window.Promise || (window.Promise = o);
    var p = n(20),
        h = n(15),
        v = n(17),
        g = n(13),
        b = {
            availableProviders: v.a,
            registerProvider: g.a
        },
        m = n(25);
    b.registerPlugin = function (e, t, n) {
        "jwpsrv" !== e && Object(m.b)(e, t, n)
    };
    var y = b,
        w = n(31),
        j = n(16),
        O = n(5),
        k = n(64),
        C = n(3),
        P = n(32),
        x = n(9),
        S = n(8),
        E = n(12),
        T = n(2);

    function A(e, t) {
        this.name = e, this.message = t.message || t.toString(), this.error = t
    }
    var _ = n(7),
        F = n(6),
        M = n(11),
        I = n(28),
        L = n(52),
        N = n(47),
        D = n(53),
        R = Object(u.k)({}, E, S, p, {
            addClass: F.a,
            hasClass: F.i,
            removeClass: F.o,
            replaceClass: F.p,
            toggleClass: F.v,
            classList: F.d,
            styleDimension: F.u,
            createElement: F.e,
            emptyElement: F.h,
            addStyleSheet: F.b,
            bounds: F.c,
            openLink: F.l,
            replaceInnerHtml: F.q,
            css: M.b,
            clearCss: M.a,
            style: M.d,
            transform: M.e,
            getRgba: M.c,
            ajax: I.a,
            crossdomain: function (e) {
                var t = document.createElement("a"),
                    n = document.createElement("a");
                t.href = location.href;
                try {
                    return n.href = e, n.href = n.href, t.protocol + "//" + t.host != n.protocol + "//" + n.host
                } catch (e) {}
                return !0
            },
            tryCatch: function (e, t) {
                var n = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : [];
                if (j.a.debug) return e.apply(t || this, n);
                try {
                    return e.apply(t || this, n)
                } catch (t) {
                    return new A(e.name, t)
                }
            },
            Error: A,
            Timer: P.a,
            log: N.a,
            genId: D.b,
            between: L.a,
            foreach: function (e, t) {
                for (var n in e) Object.prototype.hasOwnProperty.call(e, n) && t(n, e[n])
            },
            flashVersion: _.a,
            isIframe: _.m,
            indexOf: u.q,
            trim: T.i,
            pad: T.e,
            extension: T.a,
            hms: T.b,
            seconds: T.g,
            prefix: T.f,
            suffix: T.h,
            noop: function () {}
        }),
        B = 0;

    function q(n, e) {
        var t = new k.a(e);
        return t.on(C.hb, function (e) {
            n._qoe.tick("ready"), e.setupTime = n._qoe.between("setup", "ready")
        }), t.on("all", function (e, t) {
            n.trigger(e, t)
        }), t
    }

    function V(e, t) {
        var n = e.plugins;
        Object.keys(n).forEach(function (e) {
            delete n[e]
        }), t.get("setupConfig") && e.trigger("remove"), e.off(), t.playerDestroy(), t.getContainer().removeAttribute("data-jwplayer-id")
    }

    function z(t) {
        var e = ++B,
            n = t.id || "player-".concat(e),
            r = new P.a,
            i = {},
            o = q(this, t);
        r.tick("init"), t.setAttribute("data-jwplayer-id", n), Object.defineProperties(this, {
            id: {
                enumerable: !0,
                get: function () {
                    return n
                }
            },
            uniqueId: {
                enumerable: !0,
                get: function () {
                    return e
                }
            },
            plugins: {
                enumerable: !0,
                get: function () {
                    return i
                }
            },
            _qoe: {
                enumerable: !0,
                get: function () {
                    return r
                }
            },
            version: {
                enumerable: !0,
                get: function () {
                    return w.a
                }
            },
            Events: {
                enumerable: !0,
                get: function () {
                    return x.a
                }
            },
            utils: {
                enumerable: !0,
                get: function () {
                    return R
                }
            },
            _: {
                enumerable: !0,
                get: function () {
                    return u.g
                }
            }
        }), Object(u.k)(this, {
            _events: {},
            setup: function (e) {
                return r.clear("ready"), r.tick("setup"), V(this, o), (o = q(this, t)).init(e, this), this.on(e.events, null, this)
            },
            remove: function () {
                return function (e) {
                    for (var t = h.a.length; t--;)
                        if (h.a[t].uniqueId === e.uniqueId) {
                            h.a.splice(t, 1);
                            break
                        }
                }(this), V(this, o), this
            },
            qoe: function () {
                var e = o.getItemQoe();
                return {
                    setupTime: this._qoe.between("setup", "ready"),
                    firstFrame: e.getFirstFrame ? e.getFirstFrame() : null,
                    player: this._qoe.dump(),
                    item: e.dump()
                }
            },
            addCues: function (e) {
                return Array.isArray(e) && o.addCues(e), this
            },
            getAudioTracks: function () {
                return o.getAudioTracks()
            },
            getBuffer: function () {
                return o.get("buffer")
            },
            getCaptions: function () {
                return o.get("captions")
            },
            getCaptionsList: function () {
                return o.getCaptionsList()
            },
            getConfig: function () {
                return o.getConfig()
            },
            getContainer: function () {
                return o.getContainer()
            },
            getControls: function () {
                return o.get("controls")
            },
            getCues: function () {
                return o.get("cues")
            },
            getCurrentAudioTrack: function () {
                return o.getCurrentAudioTrack()
            },
            getCurrentCaptions: function () {
                return o.getCurrentCaptions()
            },
            getCurrentQuality: function () {
                return o.getCurrentQuality()
            },
            getCurrentTime: function () {
                return o.get("currentTime")
            },
            getDuration: function () {
                return o.get("duration")
            },
            getEnvironment: function () {
                return O
            },
            getFullscreen: function () {
                return o.get("fullscreen")
            },
            getHeight: function () {
                return o.getHeight()
            },
            getItemMeta: function () {
                return o.get("itemMeta") || {}
            },
            getMute: function () {
                return o.getMute()
            },
            getPlaybackRate: function () {
                return o.get("playbackRate")
            },
            getPlaylist: function () {
                return o.get("playlist")
            },
            getPlaylistIndex: function () {
                return o.get("item")
            },
            getPlaylistItem: function (e) {
                if (!R.exists(e)) return o.get("playlistItem");
                var t = this.getPlaylist();
                return t ? t[e] : null
            },
            getPosition: function () {
                return o.get("position")
            },
            getProvider: function () {
                return o.getProvider()
            },
            getQualityLevels: function () {
                return o.getQualityLevels()
            },
            getSafeRegion: function () {
                var e = !(0 < arguments.length && void 0 !== arguments[0]) || arguments[0];
                return o.getSafeRegion(e)
            },
            getState: function () {
                return o.getState()
            },
            getStretching: function () {
                return o.get("stretching")
            },
            getViewable: function () {
                return o.get("viewable")
            },
            getVisualQuality: function () {
                return o.getVisualQuality()
            },
            getVolume: function () {
                return o.get("volume")
            },
            getWidth: function () {
                return o.getWidth()
            },
            setCaptions: function (e) {
                return o.setCaptions(e), this
            },
            setConfig: function (e) {
                return o.setConfig(e), this
            },
            setControls: function (e) {
                return o.setControls(e), this
            },
            setCurrentAudioTrack: function (e) {
                o.setCurrentAudioTrack(e)
            },
            setCurrentCaptions: function (e) {
                o.setCurrentCaptions(e)
            },
            setCurrentQuality: function (e) {
                o.setCurrentQuality(e)
            },
            setFullscreen: function (e) {
                return o.setFullscreen(e), this
            },
            setMute: function (e) {
                return o.setMute(e), this
            },
            setPlaybackRate: function (e) {
                return o.setPlaybackRate(e), this
            },
            setPlaylistItem: function (e, t) {
                return o.setPlaylistItem(e, t), this
            },
            setCues: function (e) {
                return Array.isArray(e) && o.setCues(e), this
            },
            setVolume: function (e) {
                return o.setVolume(e), this
            },
            load: function (e, t) {
                return o.load(e, t), this
            },
            play: function (e) {
                return o.play(e), this
            },
            pause: function (e) {
                return o.pause(e), this
            },
            playToggle: function (e) {
                switch (this.getState()) {
                case C.qb:
                case C.kb:
                    return this.pause(e);
                default:
                    return this.play(e)
                }
            },
            seek: function (e, t) {
                return o.seek(e, t), this
            },
            playlistItem: function (e, t) {
                return o.playlistItem(e, t), this
            },
            playlistNext: function (e) {
                return o.playlistNext(e), this
            },
            playlistPrev: function (e) {
                return o.playlistPrev(e), this
            },
            next: function (e) {
                return o.next(e), this
            },
            castToggle: function () {
                return o.castToggle(), this
            },
            createInstream: function () {
                return o.createInstream()
            },
            stop: function () {
                return o.stop(), this
            },
            resize: function (e, t) {
                return o.resize(e, t), this
            },
            addButton: function (e, t, n, r, i) {
                return o.addButton(e, t, n, r, i), this
            },
            removeButton: function (e) {
                return o.removeButton(e), this
            },
            attachMedia: function () {
                return o.attachMedia(), this
            },
            detachMedia: function () {
                return o.detachMedia(), this
            },
            isBeforeComplete: function () {
                return o.isBeforeComplete()
            },
            isBeforePlay: function () {
                return o.isBeforePlay()
            }
        })
    }
    Object(u.k)(z.prototype, {
        on: function (e, t, n) {
            return x.c.call(this, e, t, n)
        },
        once: function (e, t, n) {
            return x.d.call(this, e, t, n)
        },
        off: function (e, t, n) {
            return x.b.call(this, e, t, n)
        },
        trigger: function (e, t) {
            return (t = u.g.isObject(t) ? Object(u.k)({}, t) : {}).type = e, j.a.debug ? x.e.call(this, e, t) : x.f.call(this, e, t)
        },
        getPlugin: function (e) {
            return this.plugins[e]
        },
        addPlugin: function (e, t) {
            this.plugins[e] = t, this.on("ready", t.addToPlayer), t.resize && this.on("resize", t.resizeHandler)
        },
        registerPlugin: function (e, t, n) {
            Object(m.b)(e, t, n)
        },
        getAdBlock: function () {
            return !1
        },
        playAd: function (e) {},
        pauseAd: function (e) {},
        skipAd: function () {}
    }), n.p = Object(p.loadFrom)();

    function H(e) {
        var t, n;
        if (e ? "string" == typeof e ? (t = Q(e)) || (n = document.getElementById(e)) : "number" == typeof e ? t = h.a[e] : e.nodeType && (t = Q((n = e).id || n.getAttribute("data-jwplayer-id"))) : t = h.a[0], t) return t;
        if (n) {
            var r = new z(n);
            return h.a.push(r), r
        }
        return {
            registerPlugin: m.b
        }
    }

    function Q(e) {
        for (var t = 0; t < h.a.length; t++)
            if (h.a[t].id === e) return h.a[t];
        return null
    }

    function $(e) {
        Object.defineProperties(e, {
            api: {
                get: function () {
                    return y
                },
                set: function () {}
            },
            version: {
                get: function () {
                    return w.a
                },
                set: function () {}
            },
            debug: {
                get: function () {
                    return j.a.debug
                },
                set: function (e) {
                    j.a.debug = !!e
                }
            }
        })
    }
    $(H);
    var W = H,
        X = n(35),
        U = n(26),
        J = n(24),
        K = n(48),
        Y = n(46),
        Z = n(39),
        G = u.g.extend,
        ee = {};
    ee._ = u.g, ee.utils = Object(u.k)(R, {
        key: U.b,
        extend: G,
        scriptloader: J.a,
        rssparser: {
            parse: Y.a
        },
        tea: K.a,
        UI: X.a
    }), ee.utils.css.style = ee.utils.style, ee.vid = Z.a;
    var te = ee,
        ne = n(63),
        re = /^(?:on(?:ce)?|off|trigger)$/;

    function ie(e) {
        var t = {};
        oe(this, e, e, t), oe(this, e, z.prototype, t)
    }

    function oe(t, n, r, i) {
        var o = Object.keys(r);
        o.forEach(function (e) {
            "function" == typeof r[e] && "Events" !== e ? t[e] = function s(l, f, d, p, h) {
                return function () {
                    var e, t, n, r = Array.prototype.slice.call(arguments),
                        i = r[0],
                        o = f._trackCallQueue || (f._trackCallQueue = []),
                        u = re.test(d),
                        a = u && r[1] && r[1]._callback;
                    if ("free" === (h.edition || (e = h, t = "edition", n = f.getConfig()[t], e[t] = n))) {
                        if (-1 < ["addButton", "addCues", "detachMedia", "load", "next", "pause", "play", "playlistItem", "playlistNext", "playlistPrev", "playToggle", "resize", "seek", "setCaptions", "setConfig", "setControls", "setCues", "setFullscreen", "setMute", "setPlaybackRate", "setPlaylistItem", "setVolume", "stop"].indexOf(d)) return ue(d), l;
                        if (-1 < ["createInstream", "setCurrentAudioTrack", "setCurrentCaptions", "setCurrentQuality"].indexOf(d)) return ue(d), null
                    }
                    if (a || o.push([d, i]), u) return ae(f, o), f[d].apply(l, r);
                    ! function (e, t) {
                        var n = {
                            reason: Object(ne.a)() ? "interaction" : "external"
                        };
                        switch (e) {
                        case "play":
                        case "pause":
                        case "playToggle":
                        case "playlistNext":
                        case "playlistPrev":
                        case "next":
                            t[0] = n;
                            break;
                        case "seek":
                        case "playlistItem":
                            t[1] = n
                        }
                    }(d, r);
                    var c = f[d].apply(f, r);
                    return "remove" === d ? f.off.call(l) : "setup" === d && (f.off.call(l), f.off(i.events, null, f), f.on.call(l, i.events, null, l), f.on("all", function (e, t) {
                        var n, r;
                        "ready" === e && (n = Object.keys(f).filter(function (e) {
                            return "_" !== e[0] && -1 === p.indexOf(e) && "function" == typeof f[e]
                        }), r = p.concat(n), n.forEach(function (e) {
                            l[e] = s(l, f, e, r, h)
                        })), f.trigger.call(l, e, t), ae(f, o)
                    })), ae(f, o), c === f ? l : c
                }
            }(t, n, e, o, i) : "_events" === e ? t._events = {} : Object.defineProperty(t, e, {
                enumerable: !0,
                get: function () {
                    return r[e]
                }
            })
        })
    }

    function ue(e) {
        console.warn("The API method jwplayer().".concat(e, "() is disabled in the free edition of JW Player."))
    }

    function ae(e, t) {
        var n;
        !t.length || (n = e.getPlugin("jwpsrv")) && n.trackExternalAPIUsage && (t.forEach(function (e) {
            ! function (e, t, n) {
                try {
                    var r = function (e) {
                        switch (t) {
                        case "setup":
                            return !!e;
                        case "getSafeRegion":
                        case "pauseAd":
                        case "setControls":
                        case "setFullscreen":
                        case "setMute":
                            return !!e === e ? e : void 0;
                        case "setPlaylistItem":
                        case "getPlaylistItem":
                            return (0 | e) === e ? e : void 0;
                        case "setPlaybackRate":
                        case "setVolume":
                            return Number(e);
                        case "setConfig":
                            return Object.keys(Object(e)).join(",");
                        case "on":
                        case "once":
                        case "off":
                        case "trigger":
                        case "getPlugin":
                        case "addPlugin":
                        case "registerPlugin":
                            return "" + e
                        }
                        return null
                    }(n);
                    e.trackExternalAPIUsage(t, r)
                } catch (e) {
                    j.a.debug && console.warn(e)
                }
            }(n, e[0], e[1])
        }), t.length = 0)
    }
    var ce = window;
    Object(u.k)(W, te);

    function se(e) {
        var t = W(e);
        return t.uniqueId ? t._publicApi || (t._publicApi = new ie(t)) : t
    }
    Object(u.k)(se, te), $(se), "function" == typeof ce.define && ce.define.amd && ce.define([], function () {
        return se
    });
    var le = se;
    ce.jwplayer && (le = ce.jwplayer), t.default = le
}]).default;
var jwDefaults = {
    aspectratio: "16:9",
    autostart: !1,
    controls: !0,
    displaydescription: !1,
    displaytitle: !1,
    flashplayer: "//ssl.p.jwpcdn.com/player/v/8.11.3/jwplayer.flash.swf",
    key: "MBvrieqNdmVL4jV0x6LPJ0wKB/Nbz2Qq/lqm3g==",
    playbackRateControls: !0,
    stretching: "uniform",
    width: "100%"
};
jwplayer.defaults = jwDefaults;
var sources, tracks, player = jwplayer("player");

function setup_player() {
    var e = {
        autostart: !1,
        sources: sources,
        tracks: tracks,
        cast: {},
        width: "100%",
        height: "100%",
        captions: {
            color: "#f3f378",
            fontSize: 14,
            backgroundOpacity: 0,
            fontfamily: "Helvetica",
            edgeStyle: "raised"
        }
    };
    player.setup(e), player.on("complete", function () {}), player.on("play", function () {}), player.on("ready", function () {
        $("#watch-player").prepend('<div id="overlay-container"></div>')
    }), player.on("pause", function (e) {}), player.on("time", function () {}), player.on("error", function (e) {
        console.log("player error"), changeServer()
    }), player.on("setupError", function (e) {
        console.log("player setup error: " + JSON.stringify(e)), changeServer()
    })
}

function closeBanner() {
    $("#overlay-center").hide(), player.play()
}

function changeServer() {}

function getSource(t) {
    $("#iframe").hide()
    $("#player").hide(), $("#iframe-embed").attr("src", "");

    var e = $("body").attr("data-recaptcha-key");

    e ? grecaptcha.execute(e, {
        action: "get_link"
    }).then(function (e) {
        request(t, e)
    }) : request(t, "")
}

function request(e, t) {
    $.get("/ajax/embed/play?id=" + e + "&_token=" + t, function (e) {

        "direct" == e.type ? (
            sources = e.sources,
            tracks = e.tracks,
            setup_player(),
            $("#iframe-embed").attr("src", ""),
            $("#player").show()) : (player.stop(),
            $("#iframe-embed").attr("src", e.link),
            $("#iframe").show()),
            $("#content-loading").hide()
    })
}

function testing(e, t){

    $.get("/ajax/embed/play?id=" + e + "&_token=" + t, function (e) {
        console.log(e);
    })
}



$(".item-server").click(function () {
    $("#content-pending").remove();
    $("#content-loading").show();
    var e = $(this).attr("data-id");
    $(".item-server").removeClass("active")
    $(this).addClass("active")
    getSource(e)
}),

$("#play-now").click(function () {
    $(".item-server").first().click()
});