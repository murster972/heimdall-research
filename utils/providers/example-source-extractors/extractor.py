

'''
    throw away test file we're using to figure out the logic of extracting
    all sources. Ignore the design of the program right now and focuss on
    the logic.

    since it's unlikely that all providers will use the same method we can assume
    we'll have to write indivual extractors for a given provider.
    

    NOTE: we're assuming that we've extracted the inital iframe somewhere else and that
          the url being passed in has the media-player directly on the page.

    logic for extracting from vidcloud9/vidnext (may extend across more providers):
        1. extract the main-server

        2. extract any media sources
            - seperate into iframes and media-sources (.mp4, .blob etc.)

        3. add any media-sources to our list

        4. call an extractor for the other iframes if we have them else discard them
'''

import time
import re

from abc import abstractmethod

from selenium import webdriver
from selenium.webdriver.firefox.options import Options


class ExtractorFactory:
    @staticmethod
    def get(provider):
        ''' get an extractor for the given provider, return None if provider does not have one '''
        extractors = {
            "https://vidnext.net": VidNextExtractor,
            "https://vidcloud9.com": VidNextExtractor,
            "https://www.2embed.ru": TwoEmbedExtractor,
            "https://vidcloud.pro": JWPlayerExtractor,
            "https://upstream.to": JWPlayerExtractor,  
            "https://streamsb.net": JWPlayerExtractor,
            "https://streamtape.com": StreamTape,
            "https://movcloud.net": MovCloudExtractor,
            
        }

        return extractors.get(provider, None)


class SourceExtractor:
    ''' extract sources from the an media-items iframe '''
    
    def __init__(self, url):
        self.url = url

        # load the website
        self._driver = None
        self._load_webdriver()

    def __del__(self):
        if self._driver:
            self._driver.quit()

    @abstractmethod
    def extract(self):
        pass

    def _load_webdriver(self):
        options = Options()
        options.headless = True

        # NOTE: With the chrome-driver we need to run ublock with a custom filter to subvert anti-debugging
        #       measures, however chrome-driver doesn't load extenstion in headless mode. So, we're instead
        #       using firefox, where the anti-debugging measures don't seem to work so we don't need to load
        #       any extensions.
        self._driver = webdriver.Firefox(executable_path="/home/muzza-972/gitlab/heimdall/utils/providers/drivers/firefox", options=options)
        self._driver.get(self.url)

        # sleep to allow any JS to load
        time.sleep(1)

    def _extract_provider(self, url):
        ''' extracts a provider base-url from a source link '''
        regex = r"https:\/\/[a-z0-9]*?\.[a-z]*"

        match = re.match(regex, url)

        return match.group() if match else None



class JWPlayerExtractor(SourceExtractor):
    ''' every provider that we encounter that uses a jw-player we can
        extract the source in the same way '''
    def extract(self):
        return [self._get_main_source()]

    def _get_main_source(self):
        return self._driver.execute_script('return jwplayer().getPlaylist()[0].file')


class TwoEmbedExtractor(SourceExtractor):
    ''' extract media sources from within 2embed.ru, all sources are within
        i-frames so we'll be calling out to other extractors '''
    def extract(self):
        sources = []

        for iframe in self._get_iframes():
            provider = self._extract_provider(iframe)
            extractor = ExtractorFactory.get(provider)

            if extractor:
                print(f"{provider} has an extractor")
                sources += extractor(iframe).extract()
            else:
                print(f"{provider} has no extractor")

        return sources

    def _get_iframes(self):
        ''' extract all i-frame sources '''
        return self._driver.execute_script("""
            var item_elems = document.querySelectorAll(".item-server");
            var iframes = [];

            for(i = 0; i < item_elems.length; i++){
                let data_id = item_elems[i].getAttribute("data-id");

                $.ajax({
                    url: "/ajax/embed/play?id=" + data_id + "&_token=",
                    type: 'GET',
                    async: false, // NOTE: this needs to be set otherwise we'll exit before we get the results
                    success: function(e){ iframes.push(e.link) }
                })
            }

            return iframes """)


class VidNextExtractor(SourceExtractor):
    ''' extract media sources from vidcloud and vidnext '''
    
    def extract(self):
        ''' extract all media-sources from the URL '''
        all_sources = []
        
        # get the main-server source
        all_sources.append(self._get_main_source())

        # extract a list of alternaitve sources
        #
        for source in self._get_other_sources():
            if source.startswith("//"):
                source = source.replace("//", "https://")

            provider = self._extract_provider(source)
            extractor = ExtractorFactory.get(provider)

            if extractor:
                print("using extractor for ", provider)
                all_sources += extractor(source).extract()
            else:
                print("no extracttor for ", provider)

        return all_sources

    def _get_main_source(self):
        ''' extract the main-server source '''
        return self._driver.execute_script('return jwplayer("myVideo").getPlaylist()[0].file')

    def _get_other_sources(self):
        ''' extract all other sources '''
        return self._driver.execute_script("""
            var elements = document.querySelectorAll(".linkserver");
            var sources = new Array();

            for(i = 0; i < elements.length; i++){
                let val = elements[i].getAttribute("data-video");

                if(val != ""){
                    sources.push(val);
                }
            }

            return sources """)



class StreamTape(SourceExtractor):
    ''' extract media-source from streamtape.com '''
   
    def extract(self):
        return [self._get_main_source()]

    def _get_main_source(self):
        # click the overlay to trigger the source being loaded
        elm = self._driver.find_element_by_css_selector(".plyr-overlay")
        elm.click()

        # give a split second for the players source to be updated
        time.sleep(1)

        return self._driver.execute_script('return player.source')



class MovCloudExtractor(JWPlayerExtractor):
    ''' extract meida-source from movcloud.net - small variaton on jw-player
        we can get a blob from the jw-player which we then need to do a GET
        to extract the m3u8 file '''

    def extract(self):
        return self._get_main_source()

    def _get_main_source(self):
        # the jw-player file will be a blob source
        blob = super()._get_main_source()

        # if we load the blob-source it will return an HTML page
        # with the m3u8 sources within the body
        self._driver.get(blob)

        # extract and return the m3u8 sources
        return re.findall("https:\/\/.*?.m3u8", self._driver.page_source)


if __name__ == '__main__':
    # direct url extracted from the iframe of https://vidcloud9.com/videos/tonight-show-starring-jimmy-fallon-season-9-episode-20-drew-barrymore-talib-kweli-nilufer-yanya
    # url = "https://vidnext.net/streaming.php?id=MzM2MjIw&title=Tonight+Show+Starring+Jimmy+Fallon+-+Season+9+Episode+20+-+Drew+Barrymore%2C+Talib+Kweli%2C+Nil%C3%BCfer+Yanya.&typesub=SUB&sub=&cover=Y292ZXIvdG9uaWdodC1zaG93LXN0YXJyaW5nLWppbW15LWZhbGxvbi1zZWFzb24tOS5wbmc="

    # url = "https://vidnext.net/streaming.php?id=MjU3OTQ5&title=John+Wick%3A+Chapter+3+-+Parabellum+&typesub=SUB&sub=L2pvaG4td2ljay1jaGFwdGVyLTMtcGFyYWJlbGx1bS9qb2huLXdpY2stY2hhcHRlci0zLXBhcmFiZWxsdW0udnR0&cover=Y292ZXIvam9obi13aWNrLWNoYXB0ZXItMy1wYXJhYmVsbHVtLnBuZw=="

    # print(f"Getting media-sources for {url}")

    # e = VidNextExtractor(url)
    
    # sources = e.extract()

    # returns the following sources
    #
    # ['https://storage.googleapis.com/theta-album-302207/VLFF834HBAYG/22m_1612355485336220.mp4',
    #  'https://hls2x.vidcloud9.com/load/hls/NBq6smKKhmsL7LUDLHEhxg/1612411105/336220/463efd8a8ad5461052e563abbba6d233/ep.20.1612359655.m3u8']
    # print(sources)

    # url = "https://vidcloud.pro/embed5/7hnxtyjbun2ty?i=2a17b6b8b07611022f63acf24069ae04&el=1613501"

    # t = VidcloudExtractor(url)
    # sources = t.extract()

    # print(sources)

    # url = "https://www.2embed.ru/embed/tmdb/movie?id=245891"

    # t = TwoEmbedExtractor(url)
    # sources = t.extract()

    # print(sources)


    # url = "https://streamtape.com/e/6a8RRjPj06h9zdM/for-life-season-2-episode-7-say-his-name.mp4"

    # t = StreamTape(url)
    # sources = t.extract()

    # print(sources)

    url = "https://movcloud.net/embed/ss-MCQE4cTgg"
    m = MovCloudExtractor(url)
    source = m.extract()

    print(source)